/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xni.parser;

import org.apache.xerces.xni.XMLDTDHandler;
import org.apache.xerces.xni.parser.XMLDTDSource;

public interface XMLDTDFilter
extends XMLDTDHandler,
XMLDTDSource {
}

