/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xni.parser;

import org.apache.xerces.xni.XMLDocumentHandler;
import org.apache.xerces.xni.parser.XMLDocumentSource;

public interface XMLDocumentFilter
extends XMLDocumentHandler,
XMLDocumentSource {
}

