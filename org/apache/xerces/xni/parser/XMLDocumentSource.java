/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xni.parser;

import org.apache.xerces.xni.XMLDocumentHandler;

public interface XMLDocumentSource {
    public void setDocumentHandler(XMLDocumentHandler var1);

    public XMLDocumentHandler getDocumentHandler();
}

