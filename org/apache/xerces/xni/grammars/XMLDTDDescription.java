/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xni.grammars;

import org.apache.xerces.xni.grammars.XMLGrammarDescription;

public interface XMLDTDDescription
extends XMLGrammarDescription {
    public String getRootName();
}

