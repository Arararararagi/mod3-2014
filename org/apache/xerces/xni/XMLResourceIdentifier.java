/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xni;

public interface XMLResourceIdentifier {
    public void setPublicId(String var1);

    public String getPublicId();

    public void setExpandedSystemId(String var1);

    public String getExpandedSystemId();

    public void setLiteralSystemId(String var1);

    public String getLiteralSystemId();

    public void setBaseSystemId(String var1);

    public String getBaseSystemId();

    public void setNamespace(String var1);

    public String getNamespace();
}

