/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.parsers;

import java.io.CharConversionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Locale;
import org.apache.xerces.dom.DOMMessageFormatter;
import org.apache.xerces.parsers.AbstractDOMParser;
import org.apache.xerces.parsers.ObjectFactory;
import org.apache.xerces.util.EntityResolver2Wrapper;
import org.apache.xerces.util.EntityResolverWrapper;
import org.apache.xerces.util.ErrorHandlerWrapper;
import org.apache.xerces.util.SAXMessageFormatter;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.xni.XNIException;
import org.apache.xerces.xni.grammars.XMLGrammarPool;
import org.apache.xerces.xni.parser.XMLConfigurationException;
import org.apache.xerces.xni.parser.XMLEntityResolver;
import org.apache.xerces.xni.parser.XMLErrorHandler;
import org.apache.xerces.xni.parser.XMLInputSource;
import org.apache.xerces.xni.parser.XMLParseException;
import org.apache.xerces.xni.parser.XMLParserConfiguration;
import org.w3c.dom.Node;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ext.EntityResolver2;
import org.xml.sax.helpers.LocatorImpl;

public class DOMParser
extends AbstractDOMParser {
    protected static final String USE_ENTITY_RESOLVER2 = "http://xml.org/sax/features/use-entity-resolver2";
    protected static final String SYMBOL_TABLE = "http://apache.org/xml/properties/internal/symbol-table";
    protected static final String XMLGRAMMAR_POOL = "http://apache.org/xml/properties/internal/grammar-pool";
    private static final String[] RECOGNIZED_PROPERTIES = new String[]{"http://apache.org/xml/properties/internal/symbol-table", "http://apache.org/xml/properties/internal/grammar-pool"};
    protected boolean fUseEntityResolver2 = true;

    public DOMParser(XMLParserConfiguration xMLParserConfiguration) {
        super(xMLParserConfiguration);
    }

    public DOMParser() {
        this(null, null);
    }

    public DOMParser(SymbolTable symbolTable) {
        this(symbolTable, null);
    }

    public DOMParser(SymbolTable symbolTable, XMLGrammarPool xMLGrammarPool) {
        super((XMLParserConfiguration)ObjectFactory.createObject("org.apache.xerces.xni.parser.XMLParserConfiguration", "org.apache.xerces.parsers.XIncludeAwareParserConfiguration"));
        this.fConfiguration.addRecognizedProperties(RECOGNIZED_PROPERTIES);
        if (symbolTable != null) {
            this.fConfiguration.setProperty("http://apache.org/xml/properties/internal/symbol-table", symbolTable);
        }
        if (xMLGrammarPool != null) {
            this.fConfiguration.setProperty("http://apache.org/xml/properties/internal/grammar-pool", xMLGrammarPool);
        }
    }

    public void parse(String string) throws SAXException, IOException {
        XMLInputSource xMLInputSource = new XMLInputSource(null, string, null);
        try {
            this.parse(xMLInputSource);
        }
        catch (XMLParseException var3_3) {
            Exception exception = var3_3.getException();
            if (exception == null || exception instanceof CharConversionException) {
                LocatorImpl locatorImpl = new LocatorImpl();
                locatorImpl.setPublicId(var3_3.getPublicId());
                locatorImpl.setSystemId(var3_3.getExpandedSystemId());
                locatorImpl.setLineNumber(var3_3.getLineNumber());
                locatorImpl.setColumnNumber(var3_3.getColumnNumber());
                throw exception == null ? new SAXParseException(var3_3.getMessage(), locatorImpl) : new SAXParseException(var3_3.getMessage(), locatorImpl, exception);
            }
            if (exception instanceof SAXException) {
                throw (SAXException)exception;
            }
            if (exception instanceof IOException) {
                throw (IOException)exception;
            }
            throw new SAXException(exception);
        }
        catch (XNIException var4_5) {
            var4_5.printStackTrace();
            Exception exception = var4_5.getException();
            if (exception == null) {
                throw new SAXException(var4_5.getMessage());
            }
            if (exception instanceof SAXException) {
                throw (SAXException)exception;
            }
            if (exception instanceof IOException) {
                throw (IOException)exception;
            }
            throw new SAXException(exception);
        }
    }

    public void parse(InputSource inputSource) throws SAXException, IOException {
        try {
            XMLInputSource xMLInputSource = new XMLInputSource(inputSource.getPublicId(), inputSource.getSystemId(), null);
            xMLInputSource.setByteStream(inputSource.getByteStream());
            xMLInputSource.setCharacterStream(inputSource.getCharacterStream());
            xMLInputSource.setEncoding(inputSource.getEncoding());
            this.parse(xMLInputSource);
        }
        catch (XMLParseException var2_3) {
            Exception exception = var2_3.getException();
            if (exception == null || exception instanceof CharConversionException) {
                LocatorImpl locatorImpl = new LocatorImpl();
                locatorImpl.setPublicId(var2_3.getPublicId());
                locatorImpl.setSystemId(var2_3.getExpandedSystemId());
                locatorImpl.setLineNumber(var2_3.getLineNumber());
                locatorImpl.setColumnNumber(var2_3.getColumnNumber());
                throw exception == null ? new SAXParseException(var2_3.getMessage(), locatorImpl) : new SAXParseException(var2_3.getMessage(), locatorImpl, exception);
            }
            if (exception instanceof SAXException) {
                throw (SAXException)exception;
            }
            if (exception instanceof IOException) {
                throw (IOException)exception;
            }
            throw new SAXException(exception);
        }
        catch (XNIException var3_5) {
            Exception exception = var3_5.getException();
            if (exception == null) {
                throw new SAXException(var3_5.getMessage());
            }
            if (exception instanceof SAXException) {
                throw (SAXException)exception;
            }
            if (exception instanceof IOException) {
                throw (IOException)exception;
            }
            throw new SAXException(exception);
        }
    }

    public void setEntityResolver(EntityResolver entityResolver) {
        try {
            XMLEntityResolver xMLEntityResolver = (XMLEntityResolver)this.fConfiguration.getProperty("http://apache.org/xml/properties/internal/entity-resolver");
            if (this.fUseEntityResolver2 && entityResolver instanceof EntityResolver2) {
                if (xMLEntityResolver instanceof EntityResolver2Wrapper) {
                    EntityResolver2Wrapper entityResolver2Wrapper = (EntityResolver2Wrapper)xMLEntityResolver;
                    entityResolver2Wrapper.setEntityResolver((EntityResolver2)entityResolver);
                } else {
                    this.fConfiguration.setProperty("http://apache.org/xml/properties/internal/entity-resolver", new EntityResolver2Wrapper((EntityResolver2)entityResolver));
                }
            } else if (xMLEntityResolver instanceof EntityResolverWrapper) {
                EntityResolverWrapper entityResolverWrapper = (EntityResolverWrapper)xMLEntityResolver;
                entityResolverWrapper.setEntityResolver(entityResolver);
            } else {
                this.fConfiguration.setProperty("http://apache.org/xml/properties/internal/entity-resolver", new EntityResolverWrapper(entityResolver));
            }
        }
        catch (XMLConfigurationException var2_3) {
            // empty catch block
        }
    }

    public EntityResolver getEntityResolver() {
        EntityResolver entityResolver = null;
        try {
            XMLEntityResolver xMLEntityResolver = (XMLEntityResolver)this.fConfiguration.getProperty("http://apache.org/xml/properties/internal/entity-resolver");
            if (xMLEntityResolver != null) {
                if (xMLEntityResolver instanceof EntityResolverWrapper) {
                    entityResolver = ((EntityResolverWrapper)xMLEntityResolver).getEntityResolver();
                } else if (xMLEntityResolver instanceof EntityResolver2Wrapper) {
                    entityResolver = ((EntityResolver2Wrapper)xMLEntityResolver).getEntityResolver();
                }
            }
        }
        catch (XMLConfigurationException var2_3) {
            // empty catch block
        }
        return entityResolver;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        try {
            XMLErrorHandler xMLErrorHandler = (XMLErrorHandler)this.fConfiguration.getProperty("http://apache.org/xml/properties/internal/error-handler");
            if (xMLErrorHandler instanceof ErrorHandlerWrapper) {
                ErrorHandlerWrapper errorHandlerWrapper = (ErrorHandlerWrapper)xMLErrorHandler;
                errorHandlerWrapper.setErrorHandler(errorHandler);
            } else {
                this.fConfiguration.setProperty("http://apache.org/xml/properties/internal/error-handler", new ErrorHandlerWrapper(errorHandler));
            }
        }
        catch (XMLConfigurationException var2_3) {
            // empty catch block
        }
    }

    public ErrorHandler getErrorHandler() {
        ErrorHandler errorHandler = null;
        try {
            XMLErrorHandler xMLErrorHandler = (XMLErrorHandler)this.fConfiguration.getProperty("http://apache.org/xml/properties/internal/error-handler");
            if (xMLErrorHandler != null && xMLErrorHandler instanceof ErrorHandlerWrapper) {
                errorHandler = ((ErrorHandlerWrapper)xMLErrorHandler).getErrorHandler();
            }
        }
        catch (XMLConfigurationException var2_3) {
            // empty catch block
        }
        return errorHandler;
    }

    public void setFeature(String string, boolean bl) throws SAXNotRecognizedException, SAXNotSupportedException {
        try {
            if (string.equals("http://xml.org/sax/features/use-entity-resolver2")) {
                if (bl != this.fUseEntityResolver2) {
                    this.fUseEntityResolver2 = bl;
                    this.setEntityResolver(this.getEntityResolver());
                }
                return;
            }
            this.fConfiguration.setFeature(string, bl);
        }
        catch (XMLConfigurationException var3_3) {
            String string2 = var3_3.getIdentifier();
            if (var3_3.getType() == 0) {
                throw new SAXNotRecognizedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "feature-not-recognized", new Object[]{string2}));
            }
            throw new SAXNotSupportedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "feature-not-supported", new Object[]{string2}));
        }
    }

    public boolean getFeature(String string) throws SAXNotRecognizedException, SAXNotSupportedException {
        try {
            if (string.equals("http://xml.org/sax/features/use-entity-resolver2")) {
                return this.fUseEntityResolver2;
            }
            return this.fConfiguration.getFeature(string);
        }
        catch (XMLConfigurationException var2_2) {
            String string2 = var2_2.getIdentifier();
            if (var2_2.getType() == 0) {
                throw new SAXNotRecognizedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "feature-not-recognized", new Object[]{string2}));
            }
            throw new SAXNotSupportedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "feature-not-supported", new Object[]{string2}));
        }
    }

    public void setProperty(String string, Object object) throws SAXNotRecognizedException, SAXNotSupportedException {
        try {
            this.fConfiguration.setProperty(string, object);
        }
        catch (XMLConfigurationException var3_3) {
            String string2 = var3_3.getIdentifier();
            if (var3_3.getType() == 0) {
                throw new SAXNotRecognizedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "property-not-recognized", new Object[]{string2}));
            }
            throw new SAXNotSupportedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "property-not-supported", new Object[]{string2}));
        }
    }

    public Object getProperty(String string) throws SAXNotRecognizedException, SAXNotSupportedException {
        if (string.equals("http://apache.org/xml/properties/dom/current-element-node")) {
            boolean bl = false;
            try {
                bl = this.getFeature("http://apache.org/xml/features/dom/defer-node-expansion");
            }
            catch (XMLConfigurationException var3_4) {
                // empty catch block
            }
            if (bl) {
                throw new SAXNotSupportedException(DOMMessageFormatter.formatMessage("http://www.w3.org/dom/DOMTR", "CannotQueryDeferredNode", null));
            }
            return this.fCurrentNode != null && this.fCurrentNode.getNodeType() == 1 ? this.fCurrentNode : null;
        }
        try {
            return this.fConfiguration.getProperty(string);
        }
        catch (XMLConfigurationException var2_3) {
            String string2 = var2_3.getIdentifier();
            if (var2_3.getType() == 0) {
                throw new SAXNotRecognizedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "property-not-recognized", new Object[]{string2}));
            }
            throw new SAXNotSupportedException(SAXMessageFormatter.formatMessage(this.fConfiguration.getLocale(), "property-not-supported", new Object[]{string2}));
        }
    }

    public XMLParserConfiguration getXMLParserConfiguration() {
        return this.fConfiguration;
    }
}

