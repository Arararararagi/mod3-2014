/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.dom;

import org.apache.xerces.dom.CharacterDataImpl;
import org.apache.xerces.dom.ChildNode;
import org.apache.xerces.dom.CoreDocumentImpl;
import org.apache.xerces.dom.DOMMessageFormatter;
import org.apache.xerces.dom.NodeImpl;
import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class TextImpl
extends CharacterDataImpl
implements CharacterData,
Text {
    static final long serialVersionUID = -5294980852957403469L;

    public TextImpl() {
    }

    public TextImpl(CoreDocumentImpl coreDocumentImpl, String string) {
        super(coreDocumentImpl, string);
    }

    public void setValues(CoreDocumentImpl coreDocumentImpl, String string) {
        this.flags = 0;
        this.nextSibling = null;
        this.previousSibling = null;
        this.setOwnerDocument(coreDocumentImpl);
        this.data = string;
    }

    public short getNodeType() {
        return 3;
    }

    public String getNodeName() {
        return "#text";
    }

    public void setIgnorableWhitespace(boolean bl) {
        if (this.needsSyncData()) {
            this.synchronizeData();
        }
        this.isIgnorableWhitespace(bl);
    }

    public boolean isElementContentWhitespace() {
        if (this.needsSyncData()) {
            this.synchronizeData();
        }
        return this.internalIsIgnorableWhitespace();
    }

    public String getWholeText() {
        if (this.needsSyncData()) {
            this.synchronizeData();
        }
        StringBuffer stringBuffer = new StringBuffer();
        if (this.data != null && this.data.length() != 0) {
            stringBuffer.append(this.data);
        }
        this.getWholeTextBackward(this.getPreviousSibling(), stringBuffer, this.getParentNode());
        String string = stringBuffer.toString();
        stringBuffer.setLength(0);
        this.getWholeTextForward(this.getNextSibling(), stringBuffer, this.getParentNode());
        return string + stringBuffer.toString();
    }

    protected void insertTextContent(StringBuffer stringBuffer) throws DOMException {
        String string = this.getNodeValue();
        if (string != null) {
            stringBuffer.insert(0, string);
        }
    }

    private boolean getWholeTextForward(Node node, StringBuffer stringBuffer, Node node2) {
        boolean bl = false;
        if (node2 != null) {
            bl = node2.getNodeType() == 5;
        }
        while (node != null) {
            short s = node.getNodeType();
            if (s == 5) {
                if (this.getWholeTextForward(node.getFirstChild(), stringBuffer, node)) {
                    return true;
                }
            } else if (s == 3 || s == 4) {
                ((NodeImpl)node).getTextContent(stringBuffer);
            } else {
                return true;
            }
            node = node.getNextSibling();
        }
        if (bl) {
            this.getWholeTextForward(node2.getNextSibling(), stringBuffer, node2.getParentNode());
            return true;
        }
        return false;
    }

    private boolean getWholeTextBackward(Node node, StringBuffer stringBuffer, Node node2) {
        boolean bl = false;
        if (node2 != null) {
            bl = node2.getNodeType() == 5;
        }
        while (node != null) {
            short s = node.getNodeType();
            if (s == 5) {
                if (this.getWholeTextBackward(node.getLastChild(), stringBuffer, node)) {
                    return true;
                }
            } else if (s == 3 || s == 4) {
                ((TextImpl)node).insertTextContent(stringBuffer);
            } else {
                return true;
            }
            node = node.getPreviousSibling();
        }
        if (bl) {
            this.getWholeTextBackward(node2.getPreviousSibling(), stringBuffer, node2.getParentNode());
            return true;
        }
        return false;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public Text replaceWholeText(String string) throws DOMException {
        Node node;
        void var3_6;
        if (this.needsSyncData()) {
            this.synchronizeData();
        }
        Node node2 = this.getParentNode();
        if (string == null || string.length() == 0) {
            if (node2 == null) return null;
            node2.removeChild(this);
            return null;
        }
        if (this.ownerDocument().errorChecking) {
            if (!this.canModifyPrev(this)) {
                throw new DOMException(7, DOMMessageFormatter.formatMessage("http://www.w3.org/dom/DOMTR", "NO_MODIFICATION_ALLOWED_ERR", null));
            }
            if (!this.canModifyNext(this)) {
                throw new DOMException(7, DOMMessageFormatter.formatMessage("http://www.w3.org/dom/DOMTR", "NO_MODIFICATION_ALLOWED_ERR", null));
            }
        }
        java.lang.Object var3_3 = null;
        if (this.isReadOnly()) {
            node = this.ownerDocument().createTextNode(string);
            if (node2 == null) return node;
            node2.insertBefore(node, this);
            node2.removeChild(this);
            Node node3 = node;
        } else {
            this.setData(string);
            TextImpl textImpl = this;
        }
        for (node = var3_6.getPreviousSibling(); node != null; node = node.getPreviousSibling()) {
            if (node.getNodeType() != 3 && node.getNodeType() != 4 && (node.getNodeType() != 5 || !this.hasTextOnlyChildren(node))) break;
            node2.removeChild(node);
            node = var3_6;
        }
        for (Node node4 = var3_6.getNextSibling(); node4 != null; node4 = node4.getNextSibling()) {
            if (node4.getNodeType() != 3 && node4.getNodeType() != 4 && (node4.getNodeType() != 5 || !this.hasTextOnlyChildren(node4))) return var3_6;
            node2.removeChild(node4);
            node4 = var3_6;
        }
        return var3_6;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private boolean canModifyPrev(Node var1_1) {
        var2_2 = false;
        for (var3_3 = var1_1.getPreviousSibling(); var3_3 != null; var3_3 = var3_3.getPreviousSibling()) {
            var4_4 = var3_3.getNodeType();
            if (var4_4 == 5) {
                var5_5 = var3_3.getLastChild();
                if (var5_5 == null) {
                    return false;
                } else {
                    ** GOTO lbl22
                }
            }
            if (var4_4 == 3) continue;
            if (var4_4 != 4) return true;
            continue;
lbl-1000: // 1 sources:
            {
                var6_6 = var5_5.getNodeType();
                if (var6_6 == 3 || var6_6 == 4) {
                    var2_2 = true;
                } else {
                    if (var6_6 != 5) {
                        if (var2_2 == false) return true;
                        return false;
                    }
                    if (!this.canModifyPrev(var5_5)) {
                        return false;
                    }
                    var2_2 = true;
                }
                var5_5 = var5_5.getPreviousSibling();
lbl22: // 3 sources:
                ** while (var5_5 != null)
            }
lbl23: // 1 sources:
        }
        return true;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private boolean canModifyNext(Node var1_1) {
        var2_2 = false;
        for (var3_3 = var1_1.getNextSibling(); var3_3 != null; var3_3 = var3_3.getNextSibling()) {
            var4_4 = var3_3.getNodeType();
            if (var4_4 == 5) {
                var5_5 = var3_3.getFirstChild();
                if (var5_5 == null) {
                    return false;
                } else {
                    ** GOTO lbl22
                }
            }
            if (var4_4 == 3) continue;
            if (var4_4 != 4) return true;
            continue;
lbl-1000: // 1 sources:
            {
                var6_6 = var5_5.getNodeType();
                if (var6_6 == 3 || var6_6 == 4) {
                    var2_2 = true;
                } else {
                    if (var6_6 != 5) {
                        if (var2_2 == false) return true;
                        return false;
                    }
                    if (!this.canModifyNext(var5_5)) {
                        return false;
                    }
                    var2_2 = true;
                }
                var5_5 = var5_5.getNextSibling();
lbl22: // 3 sources:
                ** while (var5_5 != null)
            }
lbl23: // 1 sources:
        }
        return true;
    }

    private boolean hasTextOnlyChildren(Node node) {
        Node node2 = node;
        if (node2 == null) {
            return false;
        }
        for (node2 = node2.getFirstChild(); node2 != null; node2 = node2.getNextSibling()) {
            short s = node2.getNodeType();
            if (s == 5) {
                return this.hasTextOnlyChildren(node2);
            }
            if (s == 3 || s == 4 || s == 5) continue;
            return false;
        }
        return true;
    }

    public boolean isIgnorableWhitespace() {
        if (this.needsSyncData()) {
            this.synchronizeData();
        }
        return this.internalIsIgnorableWhitespace();
    }

    public Text splitText(int n) throws DOMException {
        if (this.isReadOnly()) {
            throw new DOMException(7, DOMMessageFormatter.formatMessage("http://www.w3.org/dom/DOMTR", "NO_MODIFICATION_ALLOWED_ERR", null));
        }
        if (this.needsSyncData()) {
            this.synchronizeData();
        }
        if (n < 0 || n > this.data.length()) {
            throw new DOMException(1, DOMMessageFormatter.formatMessage("http://www.w3.org/dom/DOMTR", "INDEX_SIZE_ERR", null));
        }
        Text text = this.getOwnerDocument().createTextNode(this.data.substring(n));
        this.setNodeValue(this.data.substring(0, n));
        Node node = this.getParentNode();
        if (node != null) {
            node.insertBefore(text, this.nextSibling);
        }
        return text;
    }

    public void replaceData(String string) {
        this.data = string;
    }

    public String removeData() {
        String string = this.data;
        this.data = "";
        return string;
    }
}

