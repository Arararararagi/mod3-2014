/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl;

import java.io.PrintStream;

public class Version {
    public static String fVersion = "Xerces-J 2.10.0";
    private static final String fImmutableVersion = "Xerces-J 2.10.0";

    public static String getVersion() {
        return "Xerces-J 2.10.0";
    }

    public static void main(String[] arrstring) {
        System.out.println(fVersion);
    }
}

