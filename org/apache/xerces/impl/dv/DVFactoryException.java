/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv;

public class DVFactoryException
extends RuntimeException {
    static final long serialVersionUID = -3738854697928682412L;

    public DVFactoryException() {
    }

    public DVFactoryException(String string) {
        super(string);
    }
}

