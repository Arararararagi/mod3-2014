/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv;

import org.apache.xerces.impl.dv.DatatypeException;

public class InvalidDatatypeFacetException
extends DatatypeException {
    static final long serialVersionUID = -4104066085909970654L;

    public InvalidDatatypeFacetException(String string, Object[] arrobject) {
        super(string, arrobject);
    }
}

