/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv;

import org.apache.xerces.impl.dv.XSSimpleType;
import org.apache.xerces.xs.ShortList;

public class ValidatedInfo {
    public String normalizedValue;
    public Object actualValue;
    public short actualValueType;
    public XSSimpleType memberType;
    public XSSimpleType[] memberTypes;
    public ShortList itemValueTypes;

    public void reset() {
        this.normalizedValue = null;
        this.actualValue = null;
        this.actualValueType = 45;
        this.memberType = null;
        this.memberTypes = null;
        this.itemValueTypes = null;
    }

    public String stringValue() {
        if (this.actualValue == null) {
            return this.normalizedValue;
        }
        return this.actualValue.toString();
    }

    public static boolean isComparable(ValidatedInfo validatedInfo, ValidatedInfo validatedInfo2) {
        short s;
        short s2 = ValidatedInfo.convertToPrimitiveKind(validatedInfo.actualValueType);
        if (s2 != (s = ValidatedInfo.convertToPrimitiveKind(validatedInfo2.actualValueType))) {
            return s2 == 1 && s == 2 || s2 == 2 && s == 1;
        }
        if (s2 == 44 || s2 == 43) {
            int n;
            ShortList shortList = validatedInfo.itemValueTypes;
            ShortList shortList2 = validatedInfo2.itemValueTypes;
            int n2 = shortList != null ? shortList.getLength() : 0;
            int n3 = n = shortList2 != null ? shortList2.getLength() : 0;
            if (n2 != n) {
                return false;
            }
            for (int i = 0; i < n2; ++i) {
                short s3;
                short s4 = ValidatedInfo.convertToPrimitiveKind(shortList.item(i));
                if (s4 == (s3 = ValidatedInfo.convertToPrimitiveKind(shortList2.item(i))) || s4 == 1 && s3 == 2) continue;
                if (s4 == 2 && s3 == 1) continue;
                return false;
            }
        }
        return true;
    }

    private static short convertToPrimitiveKind(short s) {
        if (s <= 20) {
            return s;
        }
        if (s <= 29) {
            return 2;
        }
        if (s <= 42) {
            return 4;
        }
        return s;
    }
}

