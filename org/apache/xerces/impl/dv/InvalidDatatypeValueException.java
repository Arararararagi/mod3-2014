/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv;

import org.apache.xerces.impl.dv.DatatypeException;

public class InvalidDatatypeValueException
extends DatatypeException {
    static final long serialVersionUID = -5523739426958236125L;

    public InvalidDatatypeValueException(String string, Object[] arrobject) {
        super(string, arrobject);
    }
}

