/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

import java.io.UnsupportedEncodingException;
import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.xs.TypeValidator;
import org.apache.xerces.util.URI;

public class AnyURIDV
extends TypeValidator {
    private static final URI BASE_URI;
    private static boolean[] gNeedEscaping;
    private static char[] gAfterEscaping1;
    private static char[] gAfterEscaping2;
    private static char[] gHexChs;

    public short getAllowedFacets() {
        return 2079;
    }

    public Object getActualValue(String string, ValidationContext validationContext) throws InvalidDatatypeValueException {
        try {
            if (string.length() != 0) {
                String string2 = AnyURIDV.encode(string);
                new URI(BASE_URI, string2);
            }
        }
        catch (URI.MalformedURIException var3_4) {
            throw new InvalidDatatypeValueException("cvc-datatype-valid.1.2.1", new Object[]{string, "anyURI"});
        }
        return string;
    }

    private static String encode(String string) {
        Object object;
        int n;
        int n2 = string.length();
        StringBuffer stringBuffer = new StringBuffer(n2 * 3);
        for (n = 0; n < n2; ++n) {
            object = string.charAt(n);
            if (object >= '') break;
            if (gNeedEscaping[object]) {
                stringBuffer.append('%');
                stringBuffer.append(gAfterEscaping1[object]);
                stringBuffer.append(gAfterEscaping2[object]);
                continue;
            }
            stringBuffer.append((char)object);
        }
        if (n < n2) {
            Object object2 = null;
            try {
                object2 = string.substring(n).getBytes("UTF-8");
            }
            catch (UnsupportedEncodingException var7_6) {
                return string;
            }
            n2 = object2.length;
            for (n = 0; n < n2; ++n) {
                Object object3 = object2[n];
                if (object3 < 0) {
                    object = object3 + 256;
                    stringBuffer.append('%');
                    stringBuffer.append(gHexChs[object >> 4]);
                    stringBuffer.append(gHexChs[object & 15]);
                    continue;
                }
                if (gNeedEscaping[object3]) {
                    stringBuffer.append('%');
                    stringBuffer.append(gAfterEscaping1[object3]);
                    stringBuffer.append(gAfterEscaping2[object3]);
                    continue;
                }
                stringBuffer.append((char)object3);
            }
        }
        if (stringBuffer.length() != n2) {
            return stringBuffer.toString();
        }
        return string;
    }

    static {
        URI uRI = null;
        try {
            uRI = new URI("abc://def.ghi.jkl");
        }
        catch (URI.MalformedURIException var1_2) {
            // empty catch block
        }
        BASE_URI = uRI;
        gNeedEscaping = new boolean[128];
        gAfterEscaping1 = new char[128];
        gAfterEscaping2 = new char[128];
        gHexChs = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        for (int i = 0; i <= 31; ++i) {
            AnyURIDV.gNeedEscaping[i] = true;
            AnyURIDV.gAfterEscaping1[i] = gHexChs[i >> 4];
            AnyURIDV.gAfterEscaping2[i] = gHexChs[i & 15];
        }
        AnyURIDV.gNeedEscaping[127] = true;
        AnyURIDV.gAfterEscaping1[127] = 55;
        AnyURIDV.gAfterEscaping2[127] = 70;
        char[] arrc = new char[]{' ', '<', '>', '\"', '{', '}', '|', '\\', '^', '~', '`'};
        int n = arrc.length;
        for (int j = 0; j < n; ++j) {
            char c = arrc[j];
            AnyURIDV.gNeedEscaping[c] = true;
            AnyURIDV.gAfterEscaping1[c] = gHexChs[c >> 4];
            AnyURIDV.gAfterEscaping2[c] = gHexChs[c & 15];
        }
    }
}

