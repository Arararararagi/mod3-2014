/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.util.ByteListImpl;
import org.apache.xerces.impl.dv.util.HexBin;
import org.apache.xerces.impl.dv.xs.TypeValidator;

public class HexBinaryDV
extends TypeValidator {
    public short getAllowedFacets() {
        return 2079;
    }

    public Object getActualValue(String string, ValidationContext validationContext) throws InvalidDatatypeValueException {
        byte[] arrby = HexBin.decode(string);
        if (arrby == null) {
            throw new InvalidDatatypeValueException("cvc-datatype-valid.1.2.1", new Object[]{string, "hexBinary"});
        }
        return new XHex(arrby);
    }

    public int getDataLength(Object object) {
        return ((XHex)object).getLength();
    }

    private static final class XHex
    extends ByteListImpl {
        public XHex(byte[] arrby) {
            super(arrby);
        }

        public synchronized String toString() {
            if (this.canonical == null) {
                this.canonical = HexBin.encode(this.data);
            }
            return this.canonical;
        }

        public boolean equals(Object object) {
            if (!(object instanceof XHex)) {
                return false;
            }
            int n = this.data.length;
            byte[] arrby = ((XHex)object).data;
            if (n != arrby.length) {
                return false;
            }
            for (int i = 0; i < n; ++i) {
                if (this.data[i] == arrby[i]) continue;
                return false;
            }
            return true;
        }

        public int hashCode() {
            int n = 0;
            for (int i = 0; i < this.data.length; ++i) {
                n = n * 37 + (this.data[i] & 255);
            }
            return n;
        }
    }

}

