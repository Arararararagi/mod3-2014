/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

public class SchemaDateTimeException
extends RuntimeException {
    static final long serialVersionUID = -8520832235337769040L;

    public SchemaDateTimeException() {
    }

    public SchemaDateTimeException(String string) {
        super(string);
    }
}

