/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.xs.TypeValidator;

class PrecisionDecimalDV
extends TypeValidator {
    PrecisionDecimalDV() {
    }

    public short getAllowedFacets() {
        return 4088;
    }

    public Object getActualValue(String string, ValidationContext validationContext) throws InvalidDatatypeValueException {
        try {
            return new XPrecisionDecimal(string);
        }
        catch (NumberFormatException var3_3) {
            throw new InvalidDatatypeValueException("cvc-datatype-valid.1.2.1", new Object[]{string, "precisionDecimal"});
        }
    }

    public int compare(Object object, Object object2) {
        return ((XPrecisionDecimal)object).compareTo((XPrecisionDecimal)object2);
    }

    public int getFractionDigits(Object object) {
        return ((XPrecisionDecimal)object).fracDigits;
    }

    public int getTotalDigits(Object object) {
        return ((XPrecisionDecimal)object).totalDigits;
    }

    public boolean isIdentical(Object object, Object object2) {
        if (!(object2 instanceof XPrecisionDecimal && object instanceof XPrecisionDecimal)) {
            return false;
        }
        return ((XPrecisionDecimal)object).isIdentical((XPrecisionDecimal)object2);
    }

    static class XPrecisionDecimal {
        int sign = 1;
        int totalDigits = 0;
        int intDigits = 0;
        int fracDigits = 0;
        String ivalue = "";
        String fvalue = "";
        int pvalue = 0;
        private String canonical;

        XPrecisionDecimal(String string) throws NumberFormatException {
            if (string.equals("NaN")) {
                this.ivalue = string;
                this.sign = 0;
            }
            if (string.equals("+INF") || string.equals("INF") || string.equals("-INF")) {
                this.ivalue = string.charAt(0) == '+' ? string.substring(1) : string;
                return;
            }
            this.initD(string);
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Lifted jumps to return sites
         */
        void initD(String var1_1) throws NumberFormatException {
            var2_2 = var1_1.length();
            if (var2_2 == 0) {
                throw new NumberFormatException();
            }
            var3_3 = 0;
            var4_4 = 0;
            var5_5 = 0;
            var6_6 = 0;
            if (var1_1.charAt(0) == '+') {
                var3_3 = 1;
            } else if (var1_1.charAt(0) == '-') {
                var3_3 = 1;
                this.sign = -1;
            }
            for (var7_7 = var3_3; var7_7 < var2_2 && var1_1.charAt(var7_7) == '0'; ++var7_7) {
            }
            for (var4_4 = var7_7; var4_4 < var2_2 && TypeValidator.isDigit(var1_1.charAt(var4_4)); ++var4_4) {
            }
            if (var4_4 >= var2_2) ** GOTO lbl28
            if (var1_1.charAt(var4_4) != '.' && var1_1.charAt(var4_4) != 'E' && var1_1.charAt(var4_4) != 'e') {
                throw new NumberFormatException();
            }
            if (var1_1.charAt(var4_4) != '.') ** GOTO lbl24
            var6_6 = var5_5 = var4_4 + 1;
            ** GOTO lbl27
lbl24: // 1 sources:
            this.pvalue = Integer.parseInt(var1_1.substring(var4_4 + 1, var2_2));
            ** GOTO lbl28
lbl-1000: // 1 sources:
            {
                ++var6_6;
lbl27: // 2 sources:
                ** while (var6_6 < var2_2 && TypeValidator.isDigit((char)var1_1.charAt((int)var6_6)))
            }
lbl28: // 3 sources:
            if (var3_3 == var4_4 && var5_5 == var6_6) {
                throw new NumberFormatException();
            }
            for (var8_8 = var5_5; var8_8 < var6_6; ++var8_8) {
                if (TypeValidator.isDigit(var1_1.charAt(var8_8))) continue;
                throw new NumberFormatException();
            }
            this.intDigits = var4_4 - var7_7;
            this.fracDigits = var6_6 - var5_5;
            if (this.intDigits > 0) {
                this.ivalue = var1_1.substring(var7_7, var4_4);
            }
            if (this.fracDigits > 0) {
                this.fvalue = var1_1.substring(var5_5, var6_6);
                if (var6_6 < var2_2) {
                    this.pvalue = Integer.parseInt(var1_1.substring(var6_6 + 1, var2_2));
                }
            }
            this.totalDigits = this.intDigits + this.fracDigits;
        }

        public boolean equals(Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof XPrecisionDecimal)) {
                return false;
            }
            XPrecisionDecimal xPrecisionDecimal = (XPrecisionDecimal)object;
            return this.compareTo(xPrecisionDecimal) == 0;
        }

        private int compareFractionalPart(XPrecisionDecimal xPrecisionDecimal) {
            if (this.fvalue.equals(xPrecisionDecimal.fvalue)) {
                return 0;
            }
            StringBuffer stringBuffer = new StringBuffer(this.fvalue);
            StringBuffer stringBuffer2 = new StringBuffer(xPrecisionDecimal.fvalue);
            this.truncateTrailingZeros(stringBuffer, stringBuffer2);
            return stringBuffer.toString().compareTo(stringBuffer2.toString());
        }

        private void truncateTrailingZeros(StringBuffer stringBuffer, StringBuffer stringBuffer2) {
            for (int i = stringBuffer.length() - 1; i >= 0; --i) {
                if (stringBuffer.charAt(i) != '0') break;
                stringBuffer.deleteCharAt(i);
            }
            for (int j = stringBuffer2.length() - 1; j >= 0; --j) {
                if (stringBuffer2.charAt(j) != '0') break;
                stringBuffer2.deleteCharAt(j);
            }
        }

        public int compareTo(XPrecisionDecimal xPrecisionDecimal) {
            if (this.sign == 0) {
                return 2;
            }
            if (this.ivalue.equals("INF") || xPrecisionDecimal.ivalue.equals("INF")) {
                if (this.ivalue.equals(xPrecisionDecimal.ivalue)) {
                    return 0;
                }
                if (this.ivalue.equals("INF")) {
                    return 1;
                }
                return -1;
            }
            if (this.ivalue.equals("-INF") || xPrecisionDecimal.ivalue.equals("-INF")) {
                if (this.ivalue.equals(xPrecisionDecimal.ivalue)) {
                    return 0;
                }
                if (this.ivalue.equals("-INF")) {
                    return -1;
                }
                return 1;
            }
            if (this.sign != xPrecisionDecimal.sign) {
                return this.sign > xPrecisionDecimal.sign ? 1 : -1;
            }
            return this.sign * this.compare(xPrecisionDecimal);
        }

        private int compare(XPrecisionDecimal xPrecisionDecimal) {
            if (this.pvalue != 0 || xPrecisionDecimal.pvalue != 0) {
                if (this.pvalue == xPrecisionDecimal.pvalue) {
                    return this.intComp(xPrecisionDecimal);
                }
                if (this.intDigits + this.pvalue != xPrecisionDecimal.intDigits + xPrecisionDecimal.pvalue) {
                    return this.intDigits + this.pvalue > xPrecisionDecimal.intDigits + xPrecisionDecimal.pvalue ? 1 : -1;
                }
                if (this.pvalue > xPrecisionDecimal.pvalue) {
                    int n = this.pvalue - xPrecisionDecimal.pvalue;
                    StringBuffer stringBuffer = new StringBuffer(this.ivalue);
                    StringBuffer stringBuffer2 = new StringBuffer(this.fvalue);
                    for (int i = 0; i < n; ++i) {
                        if (i < this.fracDigits) {
                            stringBuffer.append(this.fvalue.charAt(i));
                            stringBuffer2.deleteCharAt(i);
                            continue;
                        }
                        stringBuffer.append('0');
                    }
                    return this.compareDecimal(stringBuffer.toString(), xPrecisionDecimal.ivalue, stringBuffer2.toString(), xPrecisionDecimal.fvalue);
                }
                int n = xPrecisionDecimal.pvalue - this.pvalue;
                StringBuffer stringBuffer = new StringBuffer(xPrecisionDecimal.ivalue);
                StringBuffer stringBuffer3 = new StringBuffer(xPrecisionDecimal.fvalue);
                for (int i = 0; i < n; ++i) {
                    if (i < xPrecisionDecimal.fracDigits) {
                        stringBuffer.append(xPrecisionDecimal.fvalue.charAt(i));
                        stringBuffer3.deleteCharAt(i);
                        continue;
                    }
                    stringBuffer.append('0');
                }
                return this.compareDecimal(this.ivalue, stringBuffer.toString(), this.fvalue, stringBuffer3.toString());
            }
            return this.intComp(xPrecisionDecimal);
        }

        private int intComp(XPrecisionDecimal xPrecisionDecimal) {
            if (this.intDigits != xPrecisionDecimal.intDigits) {
                return this.intDigits > xPrecisionDecimal.intDigits ? 1 : -1;
            }
            return this.compareDecimal(this.ivalue, xPrecisionDecimal.ivalue, this.fvalue, xPrecisionDecimal.fvalue);
        }

        private int compareDecimal(String string, String string2, String string3, String string4) {
            int n = string.compareTo(string3);
            if (n != 0) {
                return n > 0 ? 1 : -1;
            }
            if (string2.equals(string4)) {
                return 0;
            }
            StringBuffer stringBuffer = new StringBuffer(string2);
            StringBuffer stringBuffer2 = new StringBuffer(string4);
            this.truncateTrailingZeros(stringBuffer, stringBuffer2);
            n = stringBuffer.toString().compareTo(stringBuffer2.toString());
            return n == 0 ? 0 : (n > 0 ? 1 : -1);
        }

        public synchronized String toString() {
            if (this.canonical == null) {
                this.makeCanonical();
            }
            return this.canonical;
        }

        private void makeCanonical() {
            this.canonical = "TBD by Working Group";
        }

        public boolean isIdentical(XPrecisionDecimal xPrecisionDecimal) {
            if (this.ivalue.equals(xPrecisionDecimal.ivalue) && (this.ivalue.equals("INF") || this.ivalue.equals("-INF") || this.ivalue.equals("NaN"))) {
                return true;
            }
            if (this.sign == xPrecisionDecimal.sign && this.intDigits == xPrecisionDecimal.intDigits && this.fracDigits == xPrecisionDecimal.fracDigits && this.pvalue == xPrecisionDecimal.pvalue && this.ivalue.equals(xPrecisionDecimal.ivalue) && this.fvalue.equals(xPrecisionDecimal.fvalue)) {
                return true;
            }
            return false;
        }
    }

}

