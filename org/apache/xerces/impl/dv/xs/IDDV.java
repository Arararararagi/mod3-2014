/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.xs.TypeValidator;
import org.apache.xerces.util.XMLChar;

public class IDDV
extends TypeValidator {
    public short getAllowedFacets() {
        return 2079;
    }

    public Object getActualValue(String string, ValidationContext validationContext) throws InvalidDatatypeValueException {
        if (!XMLChar.isValidNCName(string)) {
            throw new InvalidDatatypeValueException("cvc-datatype-valid.1.2.1", new Object[]{string, "NCName"});
        }
        return string;
    }

    public void checkExtraRules(Object object, ValidationContext validationContext) throws InvalidDatatypeValueException {
        String string = (String)object;
        if (validationContext.isIdDeclared(string)) {
            throw new InvalidDatatypeValueException("cvc-id.2", new Object[]{string});
        }
        validationContext.addId(string);
    }
}

