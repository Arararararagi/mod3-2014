/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

import java.util.AbstractList;
import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.xs.TypeValidator;
import org.apache.xerces.xs.datatypes.ObjectList;

public class ListDV
extends TypeValidator {
    public short getAllowedFacets() {
        return 2079;
    }

    public Object getActualValue(String string, ValidationContext validationContext) throws InvalidDatatypeValueException {
        return string;
    }

    public int getDataLength(Object object) {
        return ((ListData)object).getLength();
    }

    static final class ListData
    extends AbstractList
    implements ObjectList {
        final Object[] data;
        private String canonical;

        public ListData(Object[] arrobject) {
            this.data = arrobject;
        }

        public synchronized String toString() {
            if (this.canonical == null) {
                int n = this.data.length;
                StringBuffer stringBuffer = new StringBuffer();
                if (n > 0) {
                    stringBuffer.append(this.data[0].toString());
                }
                for (int i = 1; i < n; ++i) {
                    stringBuffer.append(' ');
                    stringBuffer.append(this.data[i].toString());
                }
                this.canonical = stringBuffer.toString();
            }
            return this.canonical;
        }

        public int getLength() {
            return this.data.length;
        }

        public boolean equals(Object object) {
            if (!(object instanceof ListData)) {
                return false;
            }
            int n = this.data.length;
            Object[] arrobject = ((ListData)object).data;
            if (n != arrobject.length) {
                return false;
            }
            for (int i = 0; i < n; ++i) {
                if (this.data[i].equals(arrobject[i])) continue;
                return false;
            }
            return true;
        }

        public int hashCode() {
            int n = 0;
            for (int i = 0; i < this.data.length; ++i) {
                n^=this.data[i].hashCode();
            }
            return n;
        }

        public boolean contains(Object object) {
            for (int i = 0; i < this.data.length; ++i) {
                if (object != this.data[i]) continue;
                return true;
            }
            return false;
        }

        public Object item(int n) {
            if (n < 0 || n >= this.data.length) {
                return null;
            }
            return this.data[n];
        }

        public Object get(int n) {
            if (n >= 0 && n < this.data.length) {
                return this.data[n];
            }
            throw new IndexOutOfBoundsException("Index: " + n);
        }

        public int size() {
            return this.getLength();
        }
    }

}

