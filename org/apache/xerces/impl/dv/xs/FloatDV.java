/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.xs.DoubleDV;
import org.apache.xerces.impl.dv.xs.TypeValidator;
import org.apache.xerces.xs.datatypes.XSFloat;

public class FloatDV
extends TypeValidator {
    public short getAllowedFacets() {
        return 2552;
    }

    public Object getActualValue(String string, ValidationContext validationContext) throws InvalidDatatypeValueException {
        try {
            return new XFloat(string);
        }
        catch (NumberFormatException var3_3) {
            throw new InvalidDatatypeValueException("cvc-datatype-valid.1.2.1", new Object[]{string, "float"});
        }
    }

    public int compare(Object object, Object object2) {
        return ((XFloat)object).compareTo((XFloat)object2);
    }

    public boolean isIdentical(Object object, Object object2) {
        if (object2 instanceof XFloat) {
            return ((XFloat)object).isIdentical((XFloat)object2);
        }
        return false;
    }

    private static final class XFloat
    implements XSFloat {
        private final float value;
        private String canonical;

        public XFloat(String string) throws NumberFormatException {
            if (DoubleDV.isPossibleFP(string)) {
                this.value = Float.parseFloat(string);
            } else if (string.equals("INF")) {
                this.value = Infinityf;
            } else if (string.equals("-INF")) {
                this.value = -Infinityf;
            } else if (string.equals("NaN")) {
                this.value = NaNf;
            } else {
                throw new NumberFormatException(string);
            }
        }

        public boolean equals(Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof XFloat)) {
                return false;
            }
            XFloat xFloat = (XFloat)object;
            if (this.value == xFloat.value) {
                return true;
            }
            if (this.value != this.value && xFloat.value != xFloat.value) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return this.value == 0.0f ? 0 : Float.floatToIntBits(this.value);
        }

        public boolean isIdentical(XFloat xFloat) {
            if (xFloat == this) {
                return true;
            }
            if (this.value == xFloat.value) {
                return this.value != 0.0f || Float.floatToIntBits(this.value) == Float.floatToIntBits(xFloat.value);
            }
            if (this.value != this.value && xFloat.value != xFloat.value) {
                return true;
            }
            return false;
        }

        private int compareTo(XFloat xFloat) {
            float f = xFloat.value;
            if (this.value < f) {
                return -1;
            }
            if (this.value > f) {
                return 1;
            }
            if (this.value == f) {
                return 0;
            }
            if (this.value != this.value) {
                if (f != f) {
                    return 0;
                }
                return 2;
            }
            return 2;
        }

        public synchronized String toString() {
            if (this.canonical == null) {
                if (this.value == Infinityf) {
                    this.canonical = "INF";
                } else if (this.value == -Infinityf) {
                    this.canonical = "-INF";
                } else if (this.value != this.value) {
                    this.canonical = "NaN";
                } else if (this.value == 0.0f) {
                    this.canonical = "0.0E1";
                } else {
                    this.canonical = Float.toString(this.value);
                    if (this.canonical.indexOf(69) == -1) {
                        int n;
                        int n2 = this.canonical.length();
                        char[] arrc = new char[n2 + 3];
                        this.canonical.getChars(0, n2, arrc, 0);
                        int n3 = n = arrc[0] == '-' ? 2 : 1;
                        if (this.value >= 1.0f || this.value <= -1.0f) {
                            int n4;
                            for (int i = n4 = this.canonical.indexOf((int)46); i > n; --i) {
                                arrc[i] = arrc[i - 1];
                            }
                            arrc[n] = 46;
                            while (arrc[n2 - 1] == '0') {
                                --n2;
                            }
                            if (arrc[n2 - 1] == '.') {
                                ++n2;
                            }
                            arrc[n2++] = 69;
                            int n5 = n4 - n;
                            arrc[n2++] = (char)(n5 + 48);
                        } else {
                            int n6 = n + 1;
                            while (arrc[n6] == '0') {
                                ++n6;
                            }
                            arrc[n - 1] = arrc[n6];
                            arrc[n] = 46;
                            int n7 = n6 + 1;
                            int n8 = n + 1;
                            while (n7 < n2) {
                                arrc[n8] = arrc[n7];
                                ++n7;
                                ++n8;
                            }
                            if ((n2-=n6 - n) == n + 1) {
                                arrc[n2++] = 48;
                            }
                            arrc[n2++] = 69;
                            arrc[n2++] = 45;
                            int n9 = n6 - n;
                            arrc[n2++] = (char)(n9 + 48);
                        }
                        this.canonical = new String(arrc, 0, n2);
                    }
                }
            }
            return this.canonical;
        }

        public float getValue() {
            return this.value;
        }
    }

}

