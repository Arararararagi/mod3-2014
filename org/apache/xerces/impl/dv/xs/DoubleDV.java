/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.xs;

import org.apache.xerces.impl.dv.InvalidDatatypeValueException;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.xs.TypeValidator;
import org.apache.xerces.xs.datatypes.XSDouble;

public class DoubleDV
extends TypeValidator {
    public short getAllowedFacets() {
        return 2552;
    }

    public Object getActualValue(String string, ValidationContext validationContext) throws InvalidDatatypeValueException {
        try {
            return new XDouble(string);
        }
        catch (NumberFormatException var3_3) {
            throw new InvalidDatatypeValueException("cvc-datatype-valid.1.2.1", new Object[]{string, "double"});
        }
    }

    public int compare(Object object, Object object2) {
        return ((XDouble)object).compareTo((XDouble)object2);
    }

    public boolean isIdentical(Object object, Object object2) {
        if (object2 instanceof XDouble) {
            return ((XDouble)object).isIdentical((XDouble)object2);
        }
        return false;
    }

    static boolean isPossibleFP(String string) {
        int n = string.length();
        for (int i = 0; i < n; ++i) {
            char c = string.charAt(i);
            if (c >= '0' && c <= '9' || c == '.' || c == '-' || c == '+' || c == 'E' || c == 'e') continue;
            return false;
        }
        return true;
    }

    private static final class XDouble
    implements XSDouble {
        private final double value;
        private String canonical;

        public XDouble(String string) throws NumberFormatException {
            if (DoubleDV.isPossibleFP(string)) {
                this.value = Double.parseDouble(string);
            } else if (string.equals("INF")) {
                this.value = Infinity;
            } else if (string.equals("-INF")) {
                this.value = -Infinity;
            } else if (string.equals("NaN")) {
                this.value = NaN;
            } else {
                throw new NumberFormatException(string);
            }
        }

        public boolean equals(Object object) {
            if (object == this) {
                return true;
            }
            if (!(object instanceof XDouble)) {
                return false;
            }
            XDouble xDouble = (XDouble)object;
            if (this.value == xDouble.value) {
                return true;
            }
            if (this.value != this.value && xDouble.value != xDouble.value) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            if (this.value == 0.0) {
                return 0;
            }
            long l = Double.doubleToLongBits(this.value);
            return (int)(l ^ l >>> 32);
        }

        public boolean isIdentical(XDouble xDouble) {
            if (xDouble == this) {
                return true;
            }
            if (this.value == xDouble.value) {
                return this.value != 0.0 || Double.doubleToLongBits(this.value) == Double.doubleToLongBits(xDouble.value);
            }
            if (this.value != this.value && xDouble.value != xDouble.value) {
                return true;
            }
            return false;
        }

        private int compareTo(XDouble xDouble) {
            double d = xDouble.value;
            if (this.value < d) {
                return -1;
            }
            if (this.value > d) {
                return 1;
            }
            if (this.value == d) {
                return 0;
            }
            if (this.value != this.value) {
                if (d != d) {
                    return 0;
                }
                return 2;
            }
            return 2;
        }

        public synchronized String toString() {
            if (this.canonical == null) {
                if (this.value == Infinity) {
                    this.canonical = "INF";
                } else if (this.value == -Infinity) {
                    this.canonical = "-INF";
                } else if (this.value != this.value) {
                    this.canonical = "NaN";
                } else if (this.value == 0.0) {
                    this.canonical = "0.0E1";
                } else {
                    this.canonical = Double.toString(this.value);
                    if (this.canonical.indexOf(69) == -1) {
                        int n;
                        int n2 = this.canonical.length();
                        char[] arrc = new char[n2 + 3];
                        this.canonical.getChars(0, n2, arrc, 0);
                        int n3 = n = arrc[0] == '-' ? 2 : 1;
                        if (this.value >= 1.0 || this.value <= -1.0) {
                            int n4;
                            for (int i = n4 = this.canonical.indexOf((int)46); i > n; --i) {
                                arrc[i] = arrc[i - 1];
                            }
                            arrc[n] = 46;
                            while (arrc[n2 - 1] == '0') {
                                --n2;
                            }
                            if (arrc[n2 - 1] == '.') {
                                ++n2;
                            }
                            arrc[n2++] = 69;
                            int n5 = n4 - n;
                            arrc[n2++] = (char)(n5 + 48);
                        } else {
                            int n6 = n + 1;
                            while (arrc[n6] == '0') {
                                ++n6;
                            }
                            arrc[n - 1] = arrc[n6];
                            arrc[n] = 46;
                            int n7 = n6 + 1;
                            int n8 = n + 1;
                            while (n7 < n2) {
                                arrc[n8] = arrc[n7];
                                ++n7;
                                ++n8;
                            }
                            if ((n2-=n6 - n) == n + 1) {
                                arrc[n2++] = 48;
                            }
                            arrc[n2++] = 69;
                            arrc[n2++] = 45;
                            int n9 = n6 - n;
                            arrc[n2++] = (char)(n9 + 48);
                        }
                        this.canonical = new String(arrc, 0, n2);
                    }
                }
            }
            return this.canonical;
        }

        public double getValue() {
            return this.value;
        }
    }

}

