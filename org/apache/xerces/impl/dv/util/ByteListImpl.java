/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.util;

import java.util.AbstractList;
import org.apache.xerces.xs.XSException;
import org.apache.xerces.xs.datatypes.ByteList;

public class ByteListImpl
extends AbstractList
implements ByteList {
    protected final byte[] data;
    protected String canonical;

    public ByteListImpl(byte[] arrby) {
        this.data = arrby;
    }

    public int getLength() {
        return this.data.length;
    }

    public boolean contains(byte by) {
        for (int i = 0; i < this.data.length; ++i) {
            if (this.data[i] != by) continue;
            return true;
        }
        return false;
    }

    public byte item(int n) throws XSException {
        if (n < 0 || n > this.data.length - 1) {
            throw new XSException(2, null);
        }
        return this.data[n];
    }

    public Object get(int n) {
        if (n >= 0 && n < this.data.length) {
            return new Byte(this.data[n]);
        }
        throw new IndexOutOfBoundsException("Index: " + n);
    }

    public int size() {
        return this.getLength();
    }
}

