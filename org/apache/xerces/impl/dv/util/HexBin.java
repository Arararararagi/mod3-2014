/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.util;

public final class HexBin {
    private static final int BASELENGTH = 128;
    private static final int LOOKUPLENGTH = 16;
    private static final byte[] hexNumberTable = new byte[128];
    private static final char[] lookUpHexAlphabet = new char[16];

    public static String encode(byte[] arrby) {
        if (arrby == null) {
            return null;
        }
        int n = arrby.length;
        int n2 = n * 2;
        char[] arrc = new char[n2];
        for (int i = 0; i < n; ++i) {
            int n3 = arrby[i];
            if (n3 < 0) {
                n3+=256;
            }
            arrc[i * 2] = lookUpHexAlphabet[n3 >> 4];
            arrc[i * 2 + 1] = lookUpHexAlphabet[n3 & 15];
        }
        return new String(arrc);
    }

    public static byte[] decode(String string) {
        if (string == null) {
            return null;
        }
        int n = string.length();
        if (n % 2 != 0) {
            return null;
        }
        char[] arrc = string.toCharArray();
        int n2 = n / 2;
        byte[] arrby = new byte[n2];
        for (int i = 0; i < n2; ++i) {
            int n3;
            int n4;
            char c = arrc[i * 2];
            int n5 = n4 = c < '' ? hexNumberTable[c] : -1;
            if (n4 == -1) {
                return null;
            }
            c = arrc[i * 2 + 1];
            int n6 = n3 = c < '' ? hexNumberTable[c] : -1;
            if (n3 == -1) {
                return null;
            }
            arrby[i] = (byte)(n4 << 4 | n3);
        }
        return arrby;
    }

    static {
        for (int i = 0; i < 128; ++i) {
            HexBin.hexNumberTable[i] = -1;
        }
        for (int j = 57; j >= 48; --j) {
            HexBin.hexNumberTable[j] = (byte)(j - 48);
        }
        for (int k = 70; k >= 65; --k) {
            HexBin.hexNumberTable[k] = (byte)(k - 65 + 10);
        }
        for (int i2 = 102; i2 >= 97; --i2) {
            HexBin.hexNumberTable[i2] = (byte)(i2 - 97 + 10);
        }
        for (int i3 = 0; i3 < 10; ++i3) {
            HexBin.lookUpHexAlphabet[i3] = (char)(48 + i3);
        }
        for (int i4 = 10; i4 <= 15; ++i4) {
            HexBin.lookUpHexAlphabet[i4] = (char)(65 + i4 - 10);
        }
    }
}

