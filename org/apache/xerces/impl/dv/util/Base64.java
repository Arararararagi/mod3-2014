/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dv.util;

public final class Base64 {
    private static final int BASELENGTH = 128;
    private static final int LOOKUPLENGTH = 64;
    private static final int TWENTYFOURBITGROUP = 24;
    private static final int EIGHTBIT = 8;
    private static final int SIXTEENBIT = 16;
    private static final int SIXBIT = 6;
    private static final int FOURBYTE = 4;
    private static final int SIGN = -128;
    private static final char PAD = '=';
    private static final boolean fDebug = false;
    private static final byte[] base64Alphabet = new byte[128];
    private static final char[] lookUpBase64Alphabet = new char[64];

    protected static boolean isWhiteSpace(char c) {
        return c == ' ' || c == '\r' || c == '\n' || c == '\t';
    }

    protected static boolean isPad(char c) {
        return c == '=';
    }

    protected static boolean isData(char c) {
        return c < '' && base64Alphabet[c] != -1;
    }

    protected static boolean isBase64(char c) {
        return Base64.isWhiteSpace(c) || Base64.isPad(c) || Base64.isData(c);
    }

    public static String encode(byte[] arrby) {
        byte by;
        byte by2;
        if (arrby == null) {
            return null;
        }
        int n = arrby.length * 8;
        if (n == 0) {
            return "";
        }
        int n2 = n % 24;
        int n3 = n / 24;
        int n4 = n2 != 0 ? n3 + 1 : n3;
        Object object = null;
        object = new char[n4 * 4];
        byte by3 = 0;
        byte by4 = 0;
        byte by5 = 0;
        byte by6 = 0;
        byte by7 = 0;
        int n5 = 0;
        int n6 = 0;
        for (int i = 0; i < n3; ++i) {
            by5 = arrby[n6++];
            by6 = arrby[n6++];
            by7 = arrby[n6++];
            by4 = (byte)(by6 & 15);
            by3 = (byte)(by5 & 3);
            by2 = (by5 & -128) == 0 ? (byte)(by5 >> 2) : (byte)(by5 >> 2 ^ 192);
            by = (by6 & -128) == 0 ? (byte)(by6 >> 4) : (byte)(by6 >> 4 ^ 240);
            byte by8 = (by7 & -128) == 0 ? (byte)(by7 >> 6) : (byte)(by7 >> 6 ^ 252);
            object[n5++] = lookUpBase64Alphabet[by2];
            object[n5++] = lookUpBase64Alphabet[by | by3 << 4];
            object[n5++] = lookUpBase64Alphabet[by4 << 2 | by8];
            object[n5++] = lookUpBase64Alphabet[by7 & 63];
        }
        if (n2 == 8) {
            by5 = arrby[n6];
            by3 = (byte)(by5 & 3);
            by2 = (by5 & -128) == 0 ? (byte)(by5 >> 2) : (byte)(by5 >> 2 ^ 192);
            object[n5++] = lookUpBase64Alphabet[by2];
            object[n5++] = lookUpBase64Alphabet[by3 << 4];
            object[n5++] = 61;
            object[n5++] = 61;
        } else if (n2 == 16) {
            by5 = arrby[n6];
            by6 = arrby[n6 + 1];
            by4 = (byte)(by6 & 15);
            by3 = (byte)(by5 & 3);
            by2 = (by5 & -128) == 0 ? (byte)(by5 >> 2) : (byte)(by5 >> 2 ^ 192);
            by = (by6 & -128) == 0 ? (byte)(by6 >> 4) : (byte)(by6 >> 4 ^ 240);
            object[n5++] = lookUpBase64Alphabet[by2];
            object[n5++] = lookUpBase64Alphabet[by | by3 << 4];
            object[n5++] = lookUpBase64Alphabet[by4 << 2];
            object[n5++] = 61;
        }
        return new String((char[])object);
    }

    public static byte[] decode(String string) {
        int n;
        if (string == null) {
            return null;
        }
        char[] arrc = string.toCharArray();
        int n2 = Base64.removeWhiteSpace(arrc);
        if (n2 % 4 != 0) {
            return null;
        }
        int n3 = n2 / 4;
        if (n3 == 0) {
            return new byte[0];
        }
        Object object = null;
        byte by = 0;
        byte by2 = 0;
        byte by3 = 0;
        byte by4 = 0;
        char c = '\u0000';
        char c2 = '\u0000';
        char c3 = '\u0000';
        char c4 = '\u0000';
        int n4 = 0;
        int n5 = 0;
        object = new byte[n3 * 3];
        for (n = 0; n < n3 - 1; ++n) {
            if (!(Base64.isData(c = arrc[n5++]) && Base64.isData(c2 = arrc[n5++]) && Base64.isData(c3 = arrc[n5++]) && Base64.isData(c4 = arrc[n5++]))) {
                return null;
            }
            by = base64Alphabet[c];
            by2 = base64Alphabet[c2];
            by3 = base64Alphabet[c3];
            by4 = base64Alphabet[c4];
            object[n4++] = (byte)(by << 2 | by2 >> 4);
            object[n4++] = (byte)((by2 & 15) << 4 | by3 >> 2 & 15);
            object[n4++] = (byte)(by3 << 6 | by4);
        }
        if (!(Base64.isData(c = arrc[n5++]) && Base64.isData(c2 = arrc[n5++]))) {
            return null;
        }
        by = base64Alphabet[c];
        by2 = base64Alphabet[c2];
        c3 = arrc[n5++];
        c4 = arrc[n5++];
        if (!(Base64.isData(c3) && Base64.isData(c4))) {
            if (Base64.isPad(c3) && Base64.isPad(c4)) {
                if ((by2 & 15) != 0) {
                    return null;
                }
                byte[] arrby = new byte[n * 3 + 1];
                System.arraycopy(object, 0, arrby, 0, n * 3);
                arrby[n4] = (byte)(by << 2 | by2 >> 4);
                return arrby;
            }
            if (!Base64.isPad(c3) && Base64.isPad(c4)) {
                by3 = base64Alphabet[c3];
                if ((by3 & 3) != 0) {
                    return null;
                }
                byte[] arrby = new byte[n * 3 + 2];
                System.arraycopy(object, 0, arrby, 0, n * 3);
                arrby[n4++] = (byte)(by << 2 | by2 >> 4);
                arrby[n4] = (byte)((by2 & 15) << 4 | by3 >> 2 & 15);
                return arrby;
            }
            return null;
        }
        by3 = base64Alphabet[c3];
        by4 = base64Alphabet[c4];
        object[n4++] = (byte)(by << 2 | by2 >> 4);
        object[n4++] = (byte)((by2 & 15) << 4 | by3 >> 2 & 15);
        object[n4++] = (byte)(by3 << 6 | by4);
        return object;
    }

    protected static int removeWhiteSpace(char[] arrc) {
        if (arrc == null) {
            return 0;
        }
        int n = 0;
        int n2 = arrc.length;
        for (int i = 0; i < n2; ++i) {
            if (Base64.isWhiteSpace(arrc[i])) continue;
            arrc[n++] = arrc[i];
        }
        return n;
    }

    static {
        for (int i = 0; i < 128; ++i) {
            Base64.base64Alphabet[i] = -1;
        }
        for (int j = 90; j >= 65; --j) {
            Base64.base64Alphabet[j] = (byte)(j - 65);
        }
        for (int k = 122; k >= 97; --k) {
            Base64.base64Alphabet[k] = (byte)(k - 97 + 26);
        }
        for (int i2 = 57; i2 >= 48; --i2) {
            Base64.base64Alphabet[i2] = (byte)(i2 - 48 + 52);
        }
        Base64.base64Alphabet[43] = 62;
        Base64.base64Alphabet[47] = 63;
        for (int i3 = 0; i3 <= 25; ++i3) {
            Base64.lookUpBase64Alphabet[i3] = (char)(65 + i3);
        }
        int n = 26;
        int n2 = 0;
        while (n <= 51) {
            Base64.lookUpBase64Alphabet[n] = (char)(97 + n2);
            ++n;
            ++n2;
        }
        int n3 = 52;
        int n4 = 0;
        while (n3 <= 61) {
            Base64.lookUpBase64Alphabet[n3] = (char)(48 + n4);
            ++n3;
            ++n4;
        }
        Base64.lookUpBase64Alphabet[62] = 43;
        Base64.lookUpBase64Alphabet[63] = 47;
    }
}

