/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.validation;

public interface EntityState {
    public boolean isEntityDeclared(String var1);

    public boolean isEntityUnparsed(String var1);
}

