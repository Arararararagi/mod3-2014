/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xpath.regex;

import java.io.PrintStream;
import java.io.Serializable;
import org.apache.xerces.impl.xpath.regex.Token;

final class RangeToken
extends Token
implements Serializable {
    private static final long serialVersionUID = -553983121197679934L;
    int[] ranges;
    boolean sorted;
    boolean compacted;
    RangeToken icaseCache = null;
    int[] map = null;
    int nonMapIndex;
    private static final int MAPSIZE = 256;

    RangeToken(int n) {
        super(n);
        this.setSorted(false);
    }

    protected void addRange(int n, int n2) {
        int n3;
        int n4;
        this.icaseCache = null;
        if (n <= n2) {
            n4 = n;
            n3 = n2;
        } else {
            n4 = n2;
            n3 = n;
        }
        int n5 = 0;
        if (this.ranges == null) {
            this.ranges = new int[2];
            this.ranges[0] = n4;
            this.ranges[1] = n3;
            this.setSorted(true);
        } else {
            n5 = this.ranges.length;
            if (this.ranges[n5 - 1] + 1 == n4) {
                this.ranges[n5 - 1] = n3;
                return;
            }
            int[] arrn = new int[n5 + 2];
            System.arraycopy(this.ranges, 0, arrn, 0, n5);
            this.ranges = arrn;
            if (this.ranges[n5 - 1] >= n4) {
                this.setSorted(false);
            }
            this.ranges[n5++] = n4;
            this.ranges[n5] = n3;
            if (!this.sorted) {
                this.sortRanges();
            }
        }
    }

    private final boolean isSorted() {
        return this.sorted;
    }

    private final void setSorted(boolean bl) {
        this.sorted = bl;
        if (!bl) {
            this.compacted = false;
        }
    }

    private final boolean isCompacted() {
        return this.compacted;
    }

    private final void setCompacted() {
        this.compacted = true;
    }

    protected void sortRanges() {
        if (this.isSorted()) {
            return;
        }
        if (this.ranges == null) {
            return;
        }
        for (int i = this.ranges.length - 4; i >= 0; i-=2) {
            for (int j = 0; j <= i; j+=2) {
                if (this.ranges[j] <= this.ranges[j + 2] && (this.ranges[j] != this.ranges[j + 2] || this.ranges[j + 1] <= this.ranges[j + 3])) continue;
                int n = this.ranges[j + 2];
                this.ranges[j + 2] = this.ranges[j];
                this.ranges[j] = n;
                n = this.ranges[j + 3];
                this.ranges[j + 3] = this.ranges[j + 1];
                this.ranges[j + 1] = n;
            }
        }
        this.setSorted(true);
    }

    protected void compactRanges() {
        boolean bl = false;
        if (this.ranges == null || this.ranges.length <= 2) {
            return;
        }
        if (this.isCompacted()) {
            return;
        }
        int n = 0;
        int n2 = 0;
        while (n2 < this.ranges.length) {
            if (n != n2) {
                this.ranges[n] = this.ranges[n2++];
                this.ranges[n + 1] = this.ranges[n2++];
            } else {
                n2+=2;
            }
            int n3 = this.ranges[n + 1];
            while (n2 < this.ranges.length) {
                if (n3 + 1 < this.ranges[n2]) break;
                if (n3 + 1 == this.ranges[n2]) {
                    if (bl) {
                        System.err.println("Token#compactRanges(): Compaction: [" + this.ranges[n] + ", " + this.ranges[n + 1] + "], [" + this.ranges[n2] + ", " + this.ranges[n2 + 1] + "] -> [" + this.ranges[n] + ", " + this.ranges[n2 + 1] + "]");
                    }
                    this.ranges[n + 1] = this.ranges[n2 + 1];
                    n3 = this.ranges[n + 1];
                    n2+=2;
                    continue;
                }
                if (n3 >= this.ranges[n2 + 1]) {
                    if (bl) {
                        System.err.println("Token#compactRanges(): Compaction: [" + this.ranges[n] + ", " + this.ranges[n + 1] + "], [" + this.ranges[n2] + ", " + this.ranges[n2 + 1] + "] -> [" + this.ranges[n] + ", " + this.ranges[n + 1] + "]");
                    }
                    n2+=2;
                    continue;
                }
                if (n3 < this.ranges[n2 + 1]) {
                    if (bl) {
                        System.err.println("Token#compactRanges(): Compaction: [" + this.ranges[n] + ", " + this.ranges[n + 1] + "], [" + this.ranges[n2] + ", " + this.ranges[n2 + 1] + "] -> [" + this.ranges[n] + ", " + this.ranges[n2 + 1] + "]");
                    }
                    this.ranges[n + 1] = this.ranges[n2 + 1];
                    n3 = this.ranges[n + 1];
                    n2+=2;
                    continue;
                }
                throw new RuntimeException("Token#compactRanges(): Internel Error: [" + this.ranges[n] + "," + this.ranges[n + 1] + "] [" + this.ranges[n2] + "," + this.ranges[n2 + 1] + "]");
            }
            n+=2;
        }
        if (n != this.ranges.length) {
            int[] arrn = new int[n];
            System.arraycopy(this.ranges, 0, arrn, 0, n);
            this.ranges = arrn;
        }
        this.setCompacted();
    }

    protected void mergeRanges(Token token) {
        RangeToken rangeToken = (RangeToken)token;
        this.sortRanges();
        rangeToken.sortRanges();
        if (rangeToken.ranges == null) {
            return;
        }
        this.icaseCache = null;
        this.setSorted(true);
        if (this.ranges == null) {
            this.ranges = new int[rangeToken.ranges.length];
            System.arraycopy(rangeToken.ranges, 0, this.ranges, 0, rangeToken.ranges.length);
            return;
        }
        int[] arrn = new int[this.ranges.length + rangeToken.ranges.length];
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        while (n < this.ranges.length || n2 < rangeToken.ranges.length) {
            if (n >= this.ranges.length) {
                arrn[n3++] = rangeToken.ranges[n2++];
                arrn[n3++] = rangeToken.ranges[n2++];
                continue;
            }
            if (n2 >= rangeToken.ranges.length) {
                arrn[n3++] = this.ranges[n++];
                arrn[n3++] = this.ranges[n++];
                continue;
            }
            if (rangeToken.ranges[n2] < this.ranges[n] || rangeToken.ranges[n2] == this.ranges[n] && rangeToken.ranges[n2 + 1] < this.ranges[n + 1]) {
                arrn[n3++] = rangeToken.ranges[n2++];
                arrn[n3++] = rangeToken.ranges[n2++];
                continue;
            }
            arrn[n3++] = this.ranges[n++];
            arrn[n3++] = this.ranges[n++];
        }
        this.ranges = arrn;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    protected void subtractRanges(Token var1_1) {
        if (var1_1.type == 5) {
            this.intersectRanges(var1_1);
            return;
        }
        var2_2 = (RangeToken)var1_1;
        if (var2_2.ranges == null) return;
        if (this.ranges == null) {
            return;
        }
        this.icaseCache = null;
        this.sortRanges();
        this.compactRanges();
        var2_2.sortRanges();
        var2_2.compactRanges();
        var3_3 = new int[this.ranges.length + var2_2.ranges.length];
        var4_4 = 0;
        var5_5 = 0;
        var6_6 = 0;
        ** GOTO lbl46
lbl-1000: // 1 sources:
        {
            var7_7 = this.ranges[var5_5];
            var8_8 = this.ranges[var5_5 + 1];
            var9_9 = var2_2.ranges[var6_6];
            var10_10 = var2_2.ranges[var6_6 + 1];
            if (var8_8 < var9_9) {
                var3_3[var4_4++] = this.ranges[var5_5++];
                var3_3[var4_4++] = this.ranges[var5_5++];
            } else if (var8_8 >= var9_9 && var7_7 <= var10_10) {
                if (var9_9 <= var7_7 && var8_8 <= var10_10) {
                    var5_5+=2;
                } else if (var9_9 <= var7_7) {
                    this.ranges[var5_5] = var10_10 + 1;
                    var6_6+=2;
                } else if (var8_8 <= var10_10) {
                    var3_3[var4_4++] = var7_7;
                    var3_3[var4_4++] = var9_9 - 1;
                    var5_5+=2;
                } else {
                    var3_3[var4_4++] = var7_7;
                    var3_3[var4_4++] = var9_9 - 1;
                    this.ranges[var5_5] = var10_10 + 1;
                    var6_6+=2;
                }
            } else {
                if (var10_10 >= var7_7) throw new RuntimeException("Token#subtractRanges(): Internal Error: [" + this.ranges[var5_5] + "," + this.ranges[var5_5 + 1] + "] - [" + var2_2.ranges[var6_6] + "," + var2_2.ranges[var6_6 + 1] + "]");
                var6_6+=2;
            }
lbl46: // 7 sources:
            if (var5_5 >= this.ranges.length) break;
            ** while (var6_6 < var2_2.ranges.length)
        }
lbl48: // 3 sources:
        while (var5_5 < this.ranges.length) {
            var3_3[var4_4++] = this.ranges[var5_5++];
            var3_3[var4_4++] = this.ranges[var5_5++];
        }
        this.ranges = new int[var4_4];
        System.arraycopy(var3_3, 0, this.ranges, 0, var4_4);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    protected void intersectRanges(Token var1_1) {
        var2_2 = (RangeToken)var1_1;
        if (var2_2.ranges == null) return;
        if (this.ranges == null) {
            return;
        }
        this.icaseCache = null;
        this.sortRanges();
        this.compactRanges();
        var2_2.sortRanges();
        var2_2.compactRanges();
        var3_3 = new int[this.ranges.length + var2_2.ranges.length];
        var4_4 = 0;
        var5_5 = 0;
        var6_6 = 0;
        ** GOTO lbl45
lbl-1000: // 1 sources:
        {
            var7_7 = this.ranges[var5_5];
            var8_8 = this.ranges[var5_5 + 1];
            var9_9 = var2_2.ranges[var6_6];
            var10_10 = var2_2.ranges[var6_6 + 1];
            if (var8_8 < var9_9) {
                var5_5+=2;
            } else if (var8_8 >= var9_9 && var7_7 <= var10_10) {
                if (var9_9 <= var7_7 && var8_8 <= var10_10) {
                    var3_3[var4_4++] = var7_7;
                    var3_3[var4_4++] = var8_8;
                    var5_5+=2;
                } else if (var9_9 <= var7_7) {
                    var3_3[var4_4++] = var7_7;
                    var3_3[var4_4++] = var10_10;
                    this.ranges[var5_5] = var10_10 + 1;
                    var6_6+=2;
                } else if (var8_8 <= var10_10) {
                    var3_3[var4_4++] = var9_9;
                    var3_3[var4_4++] = var8_8;
                    var5_5+=2;
                } else {
                    var3_3[var4_4++] = var9_9;
                    var3_3[var4_4++] = var10_10;
                    this.ranges[var5_5] = var10_10 + 1;
                }
            } else {
                if (var10_10 >= var7_7) throw new RuntimeException("Token#intersectRanges(): Internal Error: [" + this.ranges[var5_5] + "," + this.ranges[var5_5 + 1] + "] & [" + var2_2.ranges[var6_6] + "," + var2_2.ranges[var6_6 + 1] + "]");
                var6_6+=2;
            }
lbl45: // 7 sources:
            if (var5_5 >= this.ranges.length) break;
            ** while (var6_6 < var2_2.ranges.length)
        }
lbl47: // 3 sources:
        while (var5_5 < this.ranges.length) {
            var3_3[var4_4++] = this.ranges[var5_5++];
            var3_3[var4_4++] = this.ranges[var5_5++];
        }
        this.ranges = new int[var4_4];
        System.arraycopy(var3_3, 0, this.ranges, 0, var4_4);
    }

    static Token complementRanges(Token token) {
        int n;
        if (token.type != 4 && token.type != 5) {
            throw new IllegalArgumentException("Token#complementRanges(): must be RANGE: " + token.type);
        }
        RangeToken rangeToken = (RangeToken)token;
        rangeToken.sortRanges();
        rangeToken.compactRanges();
        int n2 = rangeToken.ranges.length + 2;
        if (rangeToken.ranges[0] == 0) {
            n2-=2;
        }
        if ((n = rangeToken.ranges[rangeToken.ranges.length - 1]) == 1114111) {
            n2-=2;
        }
        RangeToken rangeToken2 = Token.createRange();
        rangeToken2.ranges = new int[n2];
        int n3 = 0;
        if (rangeToken.ranges[0] > 0) {
            rangeToken2.ranges[n3++] = 0;
            rangeToken2.ranges[n3++] = rangeToken.ranges[0] - 1;
        }
        for (int i = 1; i < rangeToken.ranges.length - 2; i+=2) {
            rangeToken2.ranges[n3++] = rangeToken.ranges[i] + 1;
            rangeToken2.ranges[n3++] = rangeToken.ranges[i + 1] - 1;
        }
        if (n != 1114111) {
            rangeToken2.ranges[n3++] = n + 1;
            rangeToken2.ranges[n3] = 1114111;
        }
        rangeToken2.setCompacted();
        return rangeToken2;
    }

    synchronized RangeToken getCaseInsensitiveToken() {
        int n;
        if (this.icaseCache != null) {
            return this.icaseCache;
        }
        RangeToken rangeToken = this.type == 4 ? Token.createRange() : Token.createNRange();
        for (int i = 0; i < this.ranges.length; i+=2) {
            for (int j = this.ranges[i]; j <= this.ranges[i + 1]; ++j) {
                if (j > 65535) {
                    rangeToken.addRange(j, j);
                    continue;
                }
                n = Character.toUpperCase((char)j);
                rangeToken.addRange(n, n);
            }
        }
        RangeToken rangeToken2 = this.type == 4 ? Token.createRange() : Token.createNRange();
        for (n = 0; n < rangeToken.ranges.length; n+=2) {
            for (int j = rangeToken.ranges[n]; j <= rangeToken.ranges[n + 1]; ++j) {
                if (j > 65535) {
                    rangeToken2.addRange(j, j);
                    continue;
                }
                char c = Character.toLowerCase((char)j);
                rangeToken2.addRange(c, c);
            }
        }
        rangeToken2.mergeRanges(rangeToken);
        rangeToken2.mergeRanges(this);
        rangeToken2.compactRanges();
        this.icaseCache = rangeToken2;
        return rangeToken2;
    }

    void dumpRanges() {
        System.err.print("RANGE: ");
        if (this.ranges == null) {
            System.err.println(" NULL");
            return;
        }
        for (int i = 0; i < this.ranges.length; i+=2) {
            System.err.print("[" + this.ranges[i] + "," + this.ranges[i + 1] + "] ");
        }
        System.err.println("");
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    boolean match(int n) {
        boolean bl;
        if (this.map == null) {
            this.createMap();
        }
        if (this.type == 4) {
            if (n < 256) {
                if ((this.map[n / 32] & 1 << (n & 31)) == 0) return false;
                return true;
            }
            bl = false;
            for (int i = this.nonMapIndex; i < this.ranges.length; i+=2) {
                if (this.ranges[i] > n || n > this.ranges[i + 1]) continue;
                return true;
            }
            return bl;
        } else {
            if (n < 256) {
                if ((this.map[n / 32] & 1 << (n & 31)) != 0) return false;
                return true;
            }
            bl = true;
            for (int i = this.nonMapIndex; i < this.ranges.length; i+=2) {
                if (this.ranges[i] > n || n > this.ranges[i + 1]) continue;
                return false;
            }
        }
        return bl;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private void createMap() {
        var1_1 = 8;
        var2_2 = new int[var1_1];
        var3_3 = this.ranges.length;
        for (var4_4 = 0; var4_4 < var1_1; ++var4_4) {
            var2_2[var4_4] = 0;
        }
        for (var5_5 = 0; var5_5 < this.ranges.length; var5_5+=2) {
            var6_6 = this.ranges[var5_5];
            var7_7 = this.ranges[var5_5 + 1];
            if (var6_6 >= 256) ** GOTO lbl13
            var8_8 = var6_6;
            ** GOTO lbl19
lbl13: // 1 sources:
            var3_3 = var5_5;
            break;
lbl-1000: // 1 sources:
            {
                v0 = var2_2;
                v1 = var8_8 / 32;
                v0[v1] = v0[v1] | 1 << (var8_8 & 31);
                ++var8_8;
lbl19: // 2 sources:
                ** while (var8_8 <= var7_7 && var8_8 < 256)
            }
lbl20: // 1 sources:
            if (var7_7 < 256) continue;
            var3_3 = var5_5;
            break;
        }
        this.map = var2_2;
        this.nonMapIndex = var3_3;
    }

    public String toString(int n) {
        String string;
        if (this.type == 4) {
            if (this == Token.token_dot) {
                string = ".";
            } else if (this == Token.token_0to9) {
                string = "\\d";
            } else if (this == Token.token_wordchars) {
                string = "\\w";
            } else if (this == Token.token_spaces) {
                string = "\\s";
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("[");
                for (int i = 0; i < this.ranges.length; i+=2) {
                    if ((n & 1024) != 0 && i > 0) {
                        stringBuffer.append(",");
                    }
                    if (this.ranges[i] == this.ranges[i + 1]) {
                        stringBuffer.append(RangeToken.escapeCharInCharClass(this.ranges[i]));
                        continue;
                    }
                    stringBuffer.append(RangeToken.escapeCharInCharClass(this.ranges[i]));
                    stringBuffer.append('-');
                    stringBuffer.append(RangeToken.escapeCharInCharClass(this.ranges[i + 1]));
                }
                stringBuffer.append("]");
                string = stringBuffer.toString();
            }
        } else if (this == Token.token_not_0to9) {
            string = "\\D";
        } else if (this == Token.token_not_wordchars) {
            string = "\\W";
        } else if (this == Token.token_not_spaces) {
            string = "\\S";
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("[^");
            for (int i = 0; i < this.ranges.length; i+=2) {
                if ((n & 1024) != 0 && i > 0) {
                    stringBuffer.append(",");
                }
                if (this.ranges[i] == this.ranges[i + 1]) {
                    stringBuffer.append(RangeToken.escapeCharInCharClass(this.ranges[i]));
                    continue;
                }
                stringBuffer.append(RangeToken.escapeCharInCharClass(this.ranges[i]));
                stringBuffer.append('-');
                stringBuffer.append(RangeToken.escapeCharInCharClass(this.ranges[i + 1]));
            }
            stringBuffer.append("]");
            string = stringBuffer.toString();
        }
        return string;
    }

    private static String escapeCharInCharClass(int n) {
        String string;
        switch (n) {
            case 44: 
            case 45: 
            case 91: 
            case 92: 
            case 93: 
            case 94: {
                string = "\\" + (char)n;
                break;
            }
            case 12: {
                string = "\\f";
                break;
            }
            case 10: {
                string = "\\n";
                break;
            }
            case 13: {
                string = "\\r";
                break;
            }
            case 9: {
                string = "\\t";
                break;
            }
            case 27: {
                string = "\\e";
                break;
            }
            default: {
                if (n < 32) {
                    String string2 = "0" + Integer.toHexString(n);
                    string = "\\x" + string2.substring(string2.length() - 2, string2.length());
                    break;
                }
                if (n >= 65536) {
                    String string3 = "0" + Integer.toHexString(n);
                    string = "\\v" + string3.substring(string3.length() - 6, string3.length());
                    break;
                }
                string = "" + (char)n;
            }
        }
        return string;
    }
}

