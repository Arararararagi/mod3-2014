/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xpath.regex;

import java.text.CharacterIterator;

public class BMPattern {
    final char[] pattern;
    final int[] shiftTable;
    final boolean ignoreCase;

    public BMPattern(String string, boolean bl) {
        this(string, 256, bl);
    }

    public BMPattern(String string, int n, boolean bl) {
        this.pattern = string.toCharArray();
        this.shiftTable = new int[n];
        this.ignoreCase = bl;
        int n2 = this.pattern.length;
        for (int i = 0; i < this.shiftTable.length; ++i) {
            this.shiftTable[i] = n2;
        }
        for (int j = 0; j < n2; ++j) {
            int n3 = n2 - j - 1;
            char c = this.pattern[j];
            int n4 = c % this.shiftTable.length;
            if (n3 < this.shiftTable[n4]) {
                this.shiftTable[n4] = n3;
            }
            if (!this.ignoreCase) continue;
            n4 = (c = Character.toUpperCase(c)) % this.shiftTable.length;
            if (n3 < this.shiftTable[n4]) {
                this.shiftTable[n4] = n3;
            }
            if (n3 >= this.shiftTable[n4 = (c = Character.toLowerCase(c)) % this.shiftTable.length]) continue;
            this.shiftTable[n4] = n3;
        }
    }

    public int matches(CharacterIterator characterIterator, int n, int n2) {
        if (this.ignoreCase) {
            return this.matchesIgnoreCase(characterIterator, n, n2);
        }
        int n3 = this.pattern.length;
        if (n3 == 0) {
            return n;
        }
        int n4 = n + n3;
        while (n4 <= n2) {
            char c;
            int n5 = n3;
            int n6 = n4 + 1;
            while ((c = characterIterator.setIndex(--n4)) == this.pattern[--n5]) {
                if (n5 == 0) {
                    return n4;
                }
                if (n5 > 0) continue;
            }
            if ((n4+=this.shiftTable[c % this.shiftTable.length] + 1) >= n6) continue;
            n4 = n6;
        }
        return -1;
    }

    public int matches(String string, int n, int n2) {
        if (this.ignoreCase) {
            return this.matchesIgnoreCase(string, n, n2);
        }
        int n3 = this.pattern.length;
        if (n3 == 0) {
            return n;
        }
        int n4 = n + n3;
        while (n4 <= n2) {
            char c;
            int n5 = n3;
            int n6 = n4 + 1;
            while ((c = string.charAt(--n4)) == this.pattern[--n5]) {
                if (n5 == 0) {
                    return n4;
                }
                if (n5 > 0) continue;
            }
            if ((n4+=this.shiftTable[c % this.shiftTable.length] + 1) >= n6) continue;
            n4 = n6;
        }
        return -1;
    }

    public int matches(char[] arrc, int n, int n2) {
        if (this.ignoreCase) {
            return this.matchesIgnoreCase(arrc, n, n2);
        }
        int n3 = this.pattern.length;
        if (n3 == 0) {
            return n;
        }
        int n4 = n + n3;
        while (n4 <= n2) {
            char c;
            int n5 = n3;
            int n6 = n4 + 1;
            while ((c = arrc[--n4]) == this.pattern[--n5]) {
                if (n5 == 0) {
                    return n4;
                }
                if (n5 > 0) continue;
            }
            if ((n4+=this.shiftTable[c % this.shiftTable.length] + 1) >= n6) continue;
            n4 = n6;
        }
        return -1;
    }

    int matchesIgnoreCase(CharacterIterator characterIterator, int n, int n2) {
        int n3 = this.pattern.length;
        if (n3 == 0) {
            return n;
        }
        int n4 = n + n3;
        while (n4 <= n2) {
            char c;
            char c2;
            char c3;
            int n5 = n3;
            int n6 = n4 + 1;
            while ((c2 = (c3 = characterIterator.setIndex(--n4))) == (c = this.pattern[--n5]) || (c2 = Character.toUpperCase(c2)) == (c = Character.toUpperCase(c)) || Character.toLowerCase(c2) == Character.toLowerCase(c)) {
                if (n5 == 0) {
                    return n4;
                }
                if (n5 > 0) continue;
            }
            if ((n4+=this.shiftTable[c3 % this.shiftTable.length] + 1) >= n6) continue;
            n4 = n6;
        }
        return -1;
    }

    int matchesIgnoreCase(String string, int n, int n2) {
        int n3 = this.pattern.length;
        if (n3 == 0) {
            return n;
        }
        int n4 = n + n3;
        while (n4 <= n2) {
            char c;
            char c2;
            char c3;
            int n5 = n3;
            int n6 = n4 + 1;
            while ((c2 = (c3 = string.charAt(--n4))) == (c = this.pattern[--n5]) || (c2 = Character.toUpperCase(c2)) == (c = Character.toUpperCase(c)) || Character.toLowerCase(c2) == Character.toLowerCase(c)) {
                if (n5 == 0) {
                    return n4;
                }
                if (n5 > 0) continue;
            }
            if ((n4+=this.shiftTable[c3 % this.shiftTable.length] + 1) >= n6) continue;
            n4 = n6;
        }
        return -1;
    }

    int matchesIgnoreCase(char[] arrc, int n, int n2) {
        int n3 = this.pattern.length;
        if (n3 == 0) {
            return n;
        }
        int n4 = n + n3;
        while (n4 <= n2) {
            char c;
            char c2;
            char c3;
            int n5 = n3;
            int n6 = n4 + 1;
            while ((c2 = (c3 = arrc[--n4])) == (c = this.pattern[--n5]) || (c2 = Character.toUpperCase(c2)) == (c = Character.toUpperCase(c)) || Character.toLowerCase(c2) == Character.toLowerCase(c)) {
                if (n5 == 0) {
                    return n4;
                }
                if (n5 > 0) continue;
            }
            if ((n4+=this.shiftTable[c3 % this.shiftTable.length] + 1) >= n6) continue;
            n4 = n6;
        }
        return -1;
    }
}

