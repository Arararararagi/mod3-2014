/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xpath.regex;

final class CaseInsensitiveMap {
    private static int CHUNK_SHIFT = 10;
    private static int CHUNK_SIZE = 1 << CHUNK_SHIFT;
    private static int CHUNK_MASK = CHUNK_SIZE - 1;
    private static int INITIAL_CHUNK_COUNT = 64;
    private static int[][][] caseInsensitiveMap;
    private static int LOWER_CASE_MATCH;
    private static int UPPER_CASE_MATCH;

    CaseInsensitiveMap() {
    }

    public static int[] get(int n) {
        return n < 65536 ? (Object)CaseInsensitiveMap.getMapping(n) : null;
    }

    private static int[] getMapping(int n) {
        int n2 = n >>> CHUNK_SHIFT;
        int n3 = n & CHUNK_MASK;
        return caseInsensitiveMap[n2][n3];
    }

    private static void buildCaseInsensitiveMap() {
        caseInsensitiveMap = new int[INITIAL_CHUNK_COUNT][CHUNK_SIZE][];
        for (int i = 0; i < 65536; ++i) {
            char c;
            int[] arrn;
            int n = Character.toLowerCase((char)i);
            if (n == (c = Character.toUpperCase((char)i)) && n == i) continue;
            int[] arrn2 = new int[2];
            int n2 = 0;
            if (n != i) {
                arrn2[n2++] = n;
                arrn2[n2++] = LOWER_CASE_MATCH;
                arrn = CaseInsensitiveMap.getMapping(n);
                if (arrn != null) {
                    arrn2 = CaseInsensitiveMap.updateMap(i, arrn2, n, arrn, LOWER_CASE_MATCH);
                }
            }
            if (c != i) {
                if (n2 == arrn2.length) {
                    arrn2 = CaseInsensitiveMap.expandMap(arrn2, 2);
                }
                arrn2[n2++] = c;
                arrn2[n2++] = UPPER_CASE_MATCH;
                arrn = CaseInsensitiveMap.getMapping(c);
                if (arrn != null) {
                    arrn2 = CaseInsensitiveMap.updateMap(i, arrn2, c, arrn, UPPER_CASE_MATCH);
                }
            }
            CaseInsensitiveMap.set(i, arrn2);
        }
    }

    private static int[] expandMap(int[] arrn, int n) {
        int n2 = arrn.length;
        int[] arrn2 = new int[n2 + n];
        System.arraycopy(arrn, 0, arrn2, 0, n2);
        return arrn2;
    }

    private static void set(int n, int[] arrn) {
        int n2 = n >>> CHUNK_SHIFT;
        int n3 = n & CHUNK_MASK;
        CaseInsensitiveMap.caseInsensitiveMap[n2][n3] = arrn;
    }

    private static int[] updateMap(int n, int[] arrn, int n2, int[] arrn2, int n3) {
        for (int i = 0; i < arrn2.length; i+=2) {
            int n4 = arrn2[i];
            int[] arrn3 = CaseInsensitiveMap.getMapping(n4);
            if (arrn3 == null || !CaseInsensitiveMap.contains(arrn3, n2, n3)) continue;
            if (!CaseInsensitiveMap.contains(arrn3, n)) {
                arrn3 = CaseInsensitiveMap.expandAndAdd(arrn3, n, n3);
                CaseInsensitiveMap.set(n4, arrn3);
            }
            if (CaseInsensitiveMap.contains(arrn, n4)) continue;
            arrn = CaseInsensitiveMap.expandAndAdd(arrn, n4, n3);
        }
        if (!CaseInsensitiveMap.contains(arrn2, n)) {
            arrn2 = CaseInsensitiveMap.expandAndAdd(arrn2, n, n3);
            CaseInsensitiveMap.set(n2, arrn2);
        }
        return arrn;
    }

    private static boolean contains(int[] arrn, int n) {
        for (int i = 0; i < arrn.length; i+=2) {
            if (arrn[i] != n) continue;
            return true;
        }
        return false;
    }

    private static boolean contains(int[] arrn, int n, int n2) {
        for (int i = 0; i < arrn.length; i+=2) {
            if (arrn[i] != n || arrn[i + 1] != n2) continue;
            return true;
        }
        return false;
    }

    private static int[] expandAndAdd(int[] arrn, int n, int n2) {
        int n3 = arrn.length;
        int[] arrn2 = new int[n3 + 2];
        System.arraycopy(arrn, 0, arrn2, 0, n3);
        arrn2[n3] = n;
        arrn2[n3 + 1] = n2;
        return arrn2;
    }

    static {
        LOWER_CASE_MATCH = 1;
        UPPER_CASE_MATCH = 2;
        CaseInsensitiveMap.buildCaseInsensitiveMap();
    }
}

