/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xpath.regex;

import java.io.Serializable;
import java.text.CharacterIterator;
import java.util.Locale;
import java.util.Stack;
import org.apache.xerces.impl.xpath.regex.BMPattern;
import org.apache.xerces.impl.xpath.regex.Match;
import org.apache.xerces.impl.xpath.regex.Op;
import org.apache.xerces.impl.xpath.regex.ParseException;
import org.apache.xerces.impl.xpath.regex.ParserForXMLSchema;
import org.apache.xerces.impl.xpath.regex.REUtil;
import org.apache.xerces.impl.xpath.regex.RangeToken;
import org.apache.xerces.impl.xpath.regex.RegexParser;
import org.apache.xerces.impl.xpath.regex.Token;
import org.apache.xerces.util.IntStack;

public class RegularExpression
implements Serializable {
    private static final long serialVersionUID = 6242499334195006401L;
    static final boolean DEBUG = false;
    String regex;
    int options;
    int nofparen;
    Token tokentree;
    boolean hasBackReferences = false;
    transient int minlength;
    transient Op operations = null;
    transient int numberOfClosures;
    transient Context context = null;
    transient RangeToken firstChar = null;
    transient String fixedString = null;
    transient int fixedStringOptions;
    transient BMPattern fixedStringTable = null;
    transient boolean fixedStringOnly = false;
    static final int IGNORE_CASE = 2;
    static final int SINGLE_LINE = 4;
    static final int MULTIPLE_LINES = 8;
    static final int EXTENDED_COMMENT = 16;
    static final int USE_UNICODE_CATEGORY = 32;
    static final int UNICODE_WORD_BOUNDARY = 64;
    static final int PROHIBIT_HEAD_CHARACTER_OPTIMIZATION = 128;
    static final int PROHIBIT_FIXED_STRING_OPTIMIZATION = 256;
    static final int XMLSCHEMA_MODE = 512;
    static final int SPECIAL_COMMA = 1024;
    private static final int WT_IGNORE = 0;
    private static final int WT_LETTER = 1;
    private static final int WT_OTHER = 2;
    static final int LINE_FEED = 10;
    static final int CARRIAGE_RETURN = 13;
    static final int LINE_SEPARATOR = 8232;
    static final int PARAGRAPH_SEPARATOR = 8233;

    private synchronized void compile(Token token) {
        if (this.operations != null) {
            return;
        }
        this.numberOfClosures = 0;
        this.operations = this.compile(token, null, false);
    }

    private Op compile(Token token, Op op, boolean bl) {
        Op op2;
        switch (token.type) {
            case 11: {
                op2 = Op.createDot();
                op2.next = op;
                break;
            }
            case 0: {
                op2 = Op.createChar(token.getChar());
                op2.next = op;
                break;
            }
            case 8: {
                op2 = Op.createAnchor(token.getChar());
                op2.next = op;
                break;
            }
            case 4: 
            case 5: {
                op2 = Op.createRange(token);
                op2.next = op;
                break;
            }
            case 1: {
                op2 = op;
                if (!bl) {
                    for (int i = token.size() - 1; i >= 0; --i) {
                        op2 = this.compile(token.getChild(i), op2, false);
                    }
                } else {
                    for (int i = 0; i < token.size(); ++i) {
                        op2 = this.compile(token.getChild(i), op2, true);
                    }
                }
                break;
            }
            case 2: {
                Op.UnionOp unionOp = Op.createUnion(token.size());
                for (int i = 0; i < token.size(); ++i) {
                    unionOp.addElement(this.compile(token.getChild(i), op, bl));
                }
                op2 = unionOp;
                break;
            }
            case 3: 
            case 9: {
                int n;
                Token token2 = token.getChild(0);
                int n2 = token.getMin();
                int n3 = token.getMax();
                if (n2 >= 0 && n2 == n3) {
                    op2 = op;
                    for (n = 0; n < n2; ++n) {
                        op2 = this.compile(token2, op2, bl);
                    }
                } else {
                    if (n2 > 0 && n3 > 0) {
                        n3-=n2;
                    }
                    if (n3 > 0) {
                        op2 = op;
                        for (int i = 0; i < n3; ++i) {
                            Op.ChildOp childOp = Op.createQuestion(token.type == 9);
                            childOp.next = op;
                            childOp.setChild(this.compile(token2, op2, bl));
                            op2 = childOp;
                        }
                    } else {
                        Op.ChildOp childOp = token.type == 9 ? Op.createNonGreedyClosure() : Op.createClosure(this.numberOfClosures++);
                        childOp.next = op;
                        childOp.setChild(this.compile(token2, childOp, bl));
                        op2 = childOp;
                    }
                    if (n2 <= 0) break;
                    for (n = 0; n < n2; ++n) {
                        op2 = this.compile(token2, op2, bl);
                    }
                }
                break;
            }
            case 7: {
                op2 = op;
                break;
            }
            case 10: {
                op2 = Op.createString(token.getString());
                op2.next = op;
                break;
            }
            case 12: {
                op2 = Op.createBackReference(token.getReferenceNumber());
                op2.next = op;
                break;
            }
            case 6: {
                if (token.getParenNumber() == 0) {
                    op2 = this.compile(token.getChild(0), op, bl);
                    break;
                }
                if (bl) {
                    op = Op.createCapture(token.getParenNumber(), op);
                    op = this.compile(token.getChild(0), op, bl);
                    op2 = Op.createCapture(- token.getParenNumber(), op);
                    break;
                }
                op = Op.createCapture(- token.getParenNumber(), op);
                op = this.compile(token.getChild(0), op, bl);
                op2 = Op.createCapture(token.getParenNumber(), op);
                break;
            }
            case 20: {
                op2 = Op.createLook(20, op, this.compile(token.getChild(0), null, false));
                break;
            }
            case 21: {
                op2 = Op.createLook(21, op, this.compile(token.getChild(0), null, false));
                break;
            }
            case 22: {
                op2 = Op.createLook(22, op, this.compile(token.getChild(0), null, true));
                break;
            }
            case 23: {
                op2 = Op.createLook(23, op, this.compile(token.getChild(0), null, true));
                break;
            }
            case 24: {
                op2 = Op.createIndependent(op, this.compile(token.getChild(0), null, bl));
                break;
            }
            case 25: {
                op2 = Op.createModifier(op, this.compile(token.getChild(0), null, bl), ((Token.ModifierToken)token).getOptions(), ((Token.ModifierToken)token).getOptionsMask());
                break;
            }
            case 26: {
                Token.ConditionToken conditionToken = (Token.ConditionToken)token;
                int n = conditionToken.refNumber;
                Op op3 = conditionToken.condition == null ? null : this.compile(conditionToken.condition, null, bl);
                Op op4 = this.compile(conditionToken.yes, op, bl);
                Op op5 = conditionToken.no == null ? null : this.compile(conditionToken.no, op, bl);
                op2 = Op.createCondition(op, n, op3, op4, op5);
                break;
            }
            default: {
                throw new RuntimeException("Unknown token type: " + token.type);
            }
        }
        return op2;
    }

    public boolean matches(char[] arrc) {
        return this.matches(arrc, 0, arrc.length, (Match)null);
    }

    public boolean matches(char[] arrc, int n, int n2) {
        return this.matches(arrc, n, n2, (Match)null);
    }

    public boolean matches(char[] arrc, Match match) {
        return this.matches(arrc, 0, arrc.length, match);
    }

    public boolean matches(char[] arrc, int n, int n2, Match match) {
        int n3;
        int n4;
        RegularExpression regularExpression = this;
        synchronized (regularExpression) {
            if (this.operations == null) {
                this.prepare();
            }
            if (this.context == null) {
                this.context = new Context();
            }
        }
        Context context = null;
        Context context2 = this.context;
        synchronized (context2) {
            context = this.context.inuse ? new Context() : this.context;
            context.reset(arrc, n, n2, this.numberOfClosures);
        }
        if (match != null) {
            match.setNumberOfGroups(this.nofparen);
            match.setSource(arrc);
        } else if (this.hasBackReferences) {
            match = new Match();
            match.setNumberOfGroups(this.nofparen);
        }
        context.match = match;
        if (RegularExpression.isSet(this.options, 512)) {
            int n5 = this.match(context, this.operations, context.start, 1, this.options);
            if (n5 == context.limit) {
                if (context.match != null) {
                    context.match.setBeginning(0, context.start);
                    context.match.setEnd(0, n5);
                }
                context.setInUse(false);
                return true;
            }
            return false;
        }
        if (this.fixedStringOnly) {
            int n6 = this.fixedStringTable.matches(arrc, context.start, context.limit);
            if (n6 >= 0) {
                if (context.match != null) {
                    context.match.setBeginning(0, n6);
                    context.match.setEnd(0, n6 + this.fixedString.length());
                }
                context.setInUse(false);
                return true;
            }
            context.setInUse(false);
            return false;
        }
        if (this.fixedString != null && (n4 = this.fixedStringTable.matches(arrc, context.start, context.limit)) < 0) {
            context.setInUse(false);
            return false;
        }
        n4 = context.limit - this.minlength;
        int n7 = -1;
        if (this.operations != null && this.operations.type == 7 && this.operations.getChild().type == 0) {
            if (RegularExpression.isSet(this.options, 4)) {
                n3 = context.start;
                n7 = this.match(context, this.operations, context.start, 1, this.options);
            } else {
                boolean bl = true;
                for (n3 = context.start; n3 <= n4; ++n3) {
                    char c = arrc[n3];
                    if (RegularExpression.isEOLChar(c)) {
                        bl = true;
                        continue;
                    }
                    if (!(bl && 0 <= (n7 = this.match(context, this.operations, n3, 1, this.options)))) {
                        bl = false;
                        continue;
                    } else {
                        break;
                    }
                }
            }
        } else if (this.firstChar != null) {
            RangeToken rangeToken = this.firstChar;
            for (n3 = context.start; n3 <= n4; ++n3) {
                int n8 = arrc[n3];
                if (REUtil.isHighSurrogate(n8) && n3 + 1 < context.limit) {
                    n8 = REUtil.composeFromSurrogates(n8, arrc[n3 + 1]);
                }
                if (!(rangeToken.match(n8) && 0 <= (n7 = this.match(context, this.operations, n3, 1, this.options)))) {
                    continue;
                } else {
                    break;
                }
            }
        } else {
            for (n3 = context.start; n3 <= n4; ++n3) {
                n7 = this.match(context, this.operations, n3, 1, this.options);
                if (0 > n7) {
                    continue;
                } else {
                    break;
                }
            }
        }
        if (n7 >= 0) {
            if (context.match != null) {
                context.match.setBeginning(0, n3);
                context.match.setEnd(0, n7);
            }
            context.setInUse(false);
            return true;
        }
        context.setInUse(false);
        return false;
    }

    public boolean matches(String string) {
        return this.matches(string, 0, string.length(), (Match)null);
    }

    public boolean matches(String string, int n, int n2) {
        return this.matches(string, n, n2, (Match)null);
    }

    public boolean matches(String string, Match match) {
        return this.matches(string, 0, string.length(), match);
    }

    public boolean matches(String string, int n, int n2, Match match) {
        int n3;
        int n4;
        RegularExpression regularExpression = this;
        synchronized (regularExpression) {
            if (this.operations == null) {
                this.prepare();
            }
            if (this.context == null) {
                this.context = new Context();
            }
        }
        Context context = null;
        Context context2 = this.context;
        synchronized (context2) {
            context = this.context.inuse ? new Context() : this.context;
            context.reset(string, n, n2, this.numberOfClosures);
        }
        if (match != null) {
            match.setNumberOfGroups(this.nofparen);
            match.setSource(string);
        } else if (this.hasBackReferences) {
            match = new Match();
            match.setNumberOfGroups(this.nofparen);
        }
        context.match = match;
        if (RegularExpression.isSet(this.options, 512)) {
            int n5 = this.match(context, this.operations, context.start, 1, this.options);
            if (n5 == context.limit) {
                if (context.match != null) {
                    context.match.setBeginning(0, context.start);
                    context.match.setEnd(0, n5);
                }
                context.setInUse(false);
                return true;
            }
            return false;
        }
        if (this.fixedStringOnly) {
            int n6 = this.fixedStringTable.matches(string, context.start, context.limit);
            if (n6 >= 0) {
                if (context.match != null) {
                    context.match.setBeginning(0, n6);
                    context.match.setEnd(0, n6 + this.fixedString.length());
                }
                context.setInUse(false);
                return true;
            }
            context.setInUse(false);
            return false;
        }
        if (this.fixedString != null && (n4 = this.fixedStringTable.matches(string, context.start, context.limit)) < 0) {
            context.setInUse(false);
            return false;
        }
        n4 = context.limit - this.minlength;
        int n7 = -1;
        if (this.operations != null && this.operations.type == 7 && this.operations.getChild().type == 0) {
            if (RegularExpression.isSet(this.options, 4)) {
                n3 = context.start;
                n7 = this.match(context, this.operations, context.start, 1, this.options);
            } else {
                boolean bl = true;
                for (n3 = context.start; n3 <= n4; ++n3) {
                    char c = string.charAt(n3);
                    if (RegularExpression.isEOLChar(c)) {
                        bl = true;
                        continue;
                    }
                    if (!(bl && 0 <= (n7 = this.match(context, this.operations, n3, 1, this.options)))) {
                        bl = false;
                        continue;
                    } else {
                        break;
                    }
                }
            }
        } else if (this.firstChar != null) {
            RangeToken rangeToken = this.firstChar;
            for (n3 = context.start; n3 <= n4; ++n3) {
                int n8 = string.charAt(n3);
                if (REUtil.isHighSurrogate(n8) && n3 + 1 < context.limit) {
                    n8 = REUtil.composeFromSurrogates(n8, string.charAt(n3 + 1));
                }
                if (!(rangeToken.match(n8) && 0 <= (n7 = this.match(context, this.operations, n3, 1, this.options)))) {
                    continue;
                } else {
                    break;
                }
            }
        } else {
            for (n3 = context.start; n3 <= n4; ++n3) {
                n7 = this.match(context, this.operations, n3, 1, this.options);
                if (0 > n7) {
                    continue;
                } else {
                    break;
                }
            }
        }
        if (n7 >= 0) {
            if (context.match != null) {
                context.match.setBeginning(0, n3);
                context.match.setEnd(0, n7);
            }
            context.setInUse(false);
            return true;
        }
        context.setInUse(false);
        return false;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private int match(Context var1_1, Op var2_2, int var3_3, int var4_4, int var5_5) {
        var6_6 = var1_1.target;
        var7_7 = new Stack<Op>();
        var8_8 = new IntStack();
        var9_9 = RegularExpression.isSet(var5_5, 2);
        var10_10 = -1;
        var11_11 = false;
        do {
            if (var2_2 != null && var3_3 <= var1_1.limit && var3_3 >= var1_1.start) ** GOTO lbl12
            var10_10 = var2_2 == null ? (RegularExpression.isSet(var5_5, 512) != false && var3_3 != var1_1.limit ? -1 : var3_3) : -1;
            var11_11 = true;
            ** GOTO lbl21
lbl12: // 1 sources:
            var10_10 = -1;
            switch (var2_2.type) {
                case 1: {
                    v0 = var12_12 = var4_4 > 0 ? var3_3 : var3_3 - 1;
                    if (!(var12_12 < var1_1.limit && var12_12 >= 0 && this.matchChar(var2_2.getData(), var6_6.charAt(var12_12), var9_9))) {
                        var11_11 = true;
                    } else {
                        var3_3+=var4_4;
                        var2_2 = var2_2.next;
                    }
lbl21: // 2 sources:
                    ** GOTO lbl239
                }
                case 0: {
                    v1 = var12_12 = var4_4 > 0 ? var3_3 : var3_3 - 1;
                    if (var12_12 < var1_1.limit && var12_12 >= 0) ** GOTO lbl27
                    var11_11 = true;
                    ** GOTO lbl239
lbl27: // 1 sources:
                    if (!RegularExpression.isSet(var5_5, 4)) ** GOTO lbl31
                    if (REUtil.isHighSurrogate(var6_6.charAt(var12_12)) && var12_12 + var4_4 >= 0 && var12_12 + var4_4 < var1_1.limit) {
                        var12_12+=var4_4;
                    } else {
                        ** GOTO lbl-1000
                    }
                    ** GOTO lbl-1000
lbl31: // 1 sources:
                    var13_15 = var6_6.charAt(var12_12);
                    if (REUtil.isHighSurrogate(var13_15) && var12_12 + var4_4 >= 0 && var12_12 + var4_4 < var1_1.limit) {
                        var13_15 = REUtil.composeFromSurrogates(var13_15, var6_6.charAt(var12_12+=var4_4));
                    }
                    if (RegularExpression.isEOLChar(var13_15)) {
                        var11_11 = true;
                    } else lbl-1000: // 4 sources:
                    {
                        var3_3 = var4_4 > 0 ? var12_12 + 1 : var12_12;
                        var2_2 = var2_2.next;
                    }
                    ** GOTO lbl239
                }
                case 3: 
                case 4: {
                    v2 = var12_12 = var4_4 > 0 ? var3_3 : var3_3 - 1;
                    if (var12_12 >= var1_1.limit || var12_12 < 0) {
                        var11_11 = true;
                    } else {
                        var13_15 = var6_6.charAt(var3_3);
                        if (REUtil.isHighSurrogate(var13_15) && var12_12 + var4_4 < var1_1.limit && var12_12 + var4_4 >= 0) {
                            var13_15 = REUtil.composeFromSurrogates(var13_15, var6_6.charAt(var12_12+=var4_4));
                        }
                        if (!(var14_16 = var2_2.getToken()).match(var13_15)) {
                            var11_11 = true;
                        } else {
                            var3_3 = var4_4 > 0 ? var12_12 + 1 : var12_12;
                            var2_2 = var2_2.next;
                        }
                    }
                    ** GOTO lbl239
                }
                case 5: {
                    if (!this.matchAnchor(var6_6, var2_2, var1_1, var3_3, var5_5)) {
                        var11_11 = true;
                    } else {
                        var2_2 = var2_2.next;
                    }
                    ** GOTO lbl239
                }
                case 16: {
                    var12_12 = var2_2.getData();
                    if (var12_12 <= 0) throw new RuntimeException("Internal Error: Reference number must be more than zero: " + var12_12);
                    if (var12_12 >= this.nofparen) {
                        throw new RuntimeException("Internal Error: Reference number must be more than zero: " + var12_12);
                    }
                    if (var1_1.match.getBeginning(var12_12) >= 0 && var1_1.match.getEnd(var12_12) >= 0) ** GOTO lbl68
                    var11_11 = true;
                    ** GOTO lbl239
lbl68: // 1 sources:
                    var13_15 = var1_1.match.getBeginning(var12_12);
                    var14_17 = var1_1.match.getEnd(var12_12) - var13_15;
                    if (var4_4 <= 0) ** GOTO lbl76
                    if (var6_6.regionMatches(var9_9, var3_3, var1_1.limit, var13_15, var14_17)) ** GOTO lbl74
                    var11_11 = true;
                    ** GOTO lbl239
lbl74: // 1 sources:
                    var3_3+=var14_17;
                    ** GOTO lbl80
lbl76: // 1 sources:
                    if (!var6_6.regionMatches(var9_9, var3_3 - var14_17, var1_1.limit, var13_15, var14_17)) {
                        var11_11 = true;
                    } else {
                        var3_3-=var14_17;
lbl80: // 2 sources:
                        var2_2 = var2_2.next;
                    }
                    ** GOTO lbl239
                }
                case 6: {
                    var12_13 = var2_2.getString();
                    var13_15 = var12_13.length();
                    if (var4_4 <= 0) ** GOTO lbl91
                    if (var6_6.regionMatches(var9_9, var3_3, var1_1.limit, var12_13, var13_15)) ** GOTO lbl89
                    var11_11 = true;
                    ** GOTO lbl239
lbl89: // 1 sources:
                    var3_3+=var13_15;
                    ** GOTO lbl95
lbl91: // 1 sources:
                    if (!var6_6.regionMatches(var9_9, var3_3 - var13_15, var1_1.limit, var12_13, var13_15)) {
                        var11_11 = true;
                    } else {
                        var3_3-=var13_15;
lbl95: // 2 sources:
                        var2_2 = var2_2.next;
                    }
                    ** GOTO lbl239
                }
                case 7: {
                    var12_12 = var2_2.getData();
                    var13_15 = var1_1.offsets[var12_12];
                    if (var13_15 != var3_3) ** GOTO lbl103
                    var11_11 = true;
                    ** GOTO lbl239
lbl103: // 1 sources:
                    var1_1.offsets[var12_12] = var3_3;
                    if (var3_3 >= var13_15) ** GOTO lbl107
                    var2_2 = var2_2.next;
                    ** GOTO lbl111
                }
lbl107: // 2 sources:
                case 9: {
                    var7_7.push(var2_2);
                    var8_8.push(var3_3);
                    var2_2 = var2_2.getChild();
lbl111: // 2 sources:
                    ** GOTO lbl239
                }
                case 8: 
                case 10: {
                    var7_7.push(var2_2);
                    var8_8.push(var3_3);
                    var2_2 = var2_2.next;
                    ** GOTO lbl239
                }
                case 11: {
                    if (var2_2.size() == 0) {
                        var11_11 = true;
                    } else {
                        var7_7.push(var2_2);
                        var8_8.push(0);
                        var8_8.push(var3_3);
                        var2_2 = var2_2.elementAt(0);
                    }
                    ** GOTO lbl239
                }
                case 15: {
                    var12_12 = var2_2.getData();
                    if (var1_1.match != null) {
                        if (var12_12 > 0) {
                            var8_8.push(var1_1.match.getBeginning(var12_12));
                            var1_1.match.setBeginning(var12_12, var3_3);
                        } else {
                            var13_15 = - var12_12;
                            var8_8.push(var1_1.match.getEnd(var13_15));
                            var1_1.match.setEnd(var13_15, var3_3);
                        }
                        var7_7.push(var2_2);
                        var8_8.push(var3_3);
                    }
                    var2_2 = var2_2.next;
                    ** GOTO lbl239
                }
                case 20: 
                case 21: 
                case 22: 
                case 23: {
                    var7_7.push(var2_2);
                    var8_8.push(var4_4);
                    var8_8.push(var3_3);
                    var4_4 = var2_2.type == 20 || var2_2.type == 21 ? 1 : -1;
                    var2_2 = var2_2.getChild();
                    ** GOTO lbl239
                }
                case 24: {
                    var7_7.push(var2_2);
                    var8_8.push(var3_3);
                    var2_2 = var2_2.getChild();
                    ** GOTO lbl239
                }
                case 25: {
                    var12_12 = var5_5;
                    var12_12|=var2_2.getData();
                    var7_7.push(var2_2);
                    var8_8.push(var5_5);
                    var8_8.push(var3_3);
                    var5_5 = var12_12&=~ var2_2.getData2();
                    var2_2 = var2_2.getChild();
                    ** GOTO lbl239
                }
                case 26: {
                    var12_14 = (Op.ConditionOp)var2_2;
                    if (var12_14.refNumber > 0) {
                        if (var12_14.refNumber >= this.nofparen) {
                            throw new RuntimeException("Internal Error: Reference number must be more than zero: " + var12_14.refNumber);
                        }
                        var2_2 = var1_1.match.getBeginning(var12_14.refNumber) >= 0 && var1_1.match.getEnd(var12_14.refNumber) >= 0 ? var12_14.yes : (var12_14.no != null ? var12_14.no : var12_14.next);
                    } else {
                        var7_7.push(var2_2);
                        var8_8.push(var3_3);
                        var2_2 = var12_14.condition;
                    }
                    ** GOTO lbl239
                }
                default: {
                    throw new RuntimeException("Unknown operation type: " + var2_2.type);
                }
            }
lbl-1000: // 1 sources:
            {
                if (var7_7.isEmpty()) {
                    return var10_10;
                }
                var2_2 = (Op)var7_7.pop();
                var3_3 = var8_8.pop();
                switch (var2_2.type) {
                    case 7: {
                        var1_1.offsets[var2_2.getData()] = var3_3;
                    }
                    case 9: {
                        if (var10_10 >= 0) break;
                        var2_2 = var2_2.next;
                        var11_11 = false;
                        break;
                    }
                    case 8: 
                    case 10: {
                        if (var10_10 >= 0) break;
                        var2_2 = var2_2.getChild();
                        var11_11 = false;
                        break;
                    }
                    case 11: {
                        var12_12 = var8_8.pop();
                        if (var10_10 >= 0) break;
                        if (++var12_12 < var2_2.size()) {
                            var7_7.push(var2_2);
                            var8_8.push(var12_12);
                            var8_8.push(var3_3);
                            var2_2 = var2_2.elementAt(var12_12);
                            var11_11 = false;
                            break;
                        }
                        var10_10 = -1;
                        break;
                    }
                    case 15: {
                        var12_12 = var2_2.getData();
                        var13_15 = var8_8.pop();
                        if (var10_10 >= 0) break;
                        if (var12_12 > 0) {
                            var1_1.match.setBeginning(var12_12, var13_15);
                            break;
                        }
                        var1_1.match.setEnd(- var12_12, var13_15);
                        break;
                    }
                    case 20: 
                    case 22: {
                        var4_4 = var8_8.pop();
                        if (0 <= var10_10) {
                            var2_2 = var2_2.next;
                            var11_11 = false;
                        }
                        var10_10 = -1;
                        break;
                    }
                    case 21: 
                    case 23: {
                        var4_4 = var8_8.pop();
                        if (0 > var10_10) {
                            var2_2 = var2_2.next;
                            var11_11 = false;
                        }
                        var10_10 = -1;
                        break;
                    }
                    case 25: {
                        var5_5 = var8_8.pop();
                    }
                    case 24: {
                        if (var10_10 < 0) break;
                        var3_3 = var10_10;
                        var2_2 = var2_2.next;
                        var11_11 = false;
                        break;
                    }
                    case 26: {
                        var14_16 = (Op.ConditionOp)var2_2;
                        var2_2 = 0 <= var10_10 ? var14_16.yes : (var14_16.no != null ? var14_16.no : var14_16.next);
                        var11_11 = false;
                        break;
                    }
                }
lbl239: // 44 sources:
                ** while (var11_11)
            }
lbl240: // 1 sources:
        } while (true);
    }

    private boolean matchChar(int n, int n2, boolean bl) {
        return bl ? RegularExpression.matchIgnoreCase(n, n2) : n == n2;
    }

    boolean matchAnchor(ExpressionTarget expressionTarget, Op op, Context context, int n, int n2) {
        boolean bl = false;
        switch (op.getData()) {
            case 94: {
                if (!(RegularExpression.isSet(n2, 8) ? n != context.start && (n <= context.start || n >= context.limit || !RegularExpression.isEOLChar(expressionTarget.charAt(n - 1))) : n != context.start)) break;
                return false;
            }
            case 64: {
                if (n == context.start || n > context.start && RegularExpression.isEOLChar(expressionTarget.charAt(n - 1))) break;
                return false;
            }
            case 36: {
                if (!(RegularExpression.isSet(n2, 8) ? n != context.limit && (n >= context.limit || !RegularExpression.isEOLChar(expressionTarget.charAt(n))) : n != context.limit && (n + 1 != context.limit || !RegularExpression.isEOLChar(expressionTarget.charAt(n))) && (n + 2 != context.limit || expressionTarget.charAt(n) != '\r' || expressionTarget.charAt(n + 1) != '\n'))) break;
                return false;
            }
            case 65: {
                if (n == context.start) break;
                return false;
            }
            case 90: {
                if (n == context.limit || n + 1 == context.limit && RegularExpression.isEOLChar(expressionTarget.charAt(n)) || n + 2 == context.limit && expressionTarget.charAt(n) == '\r' && expressionTarget.charAt(n + 1) == '\n') break;
                return false;
            }
            case 122: {
                if (n == context.limit) break;
                return false;
            }
            case 98: {
                if (context.length == 0) {
                    return false;
                }
                int n3 = RegularExpression.getWordType(expressionTarget, context.start, context.limit, n, n2);
                if (n3 == 0) {
                    return false;
                }
                int n4 = RegularExpression.getPreviousWordType(expressionTarget, context.start, context.limit, n, n2);
                if (n3 != n4) break;
                return false;
            }
            case 66: {
                if (context.length == 0) {
                    bl = true;
                } else {
                    int n5 = RegularExpression.getWordType(expressionTarget, context.start, context.limit, n, n2);
                    boolean bl2 = bl = n5 == 0 || n5 == RegularExpression.getPreviousWordType(expressionTarget, context.start, context.limit, n, n2);
                }
                if (bl) break;
                return false;
            }
            case 60: {
                if (context.length == 0 || n == context.limit) {
                    return false;
                }
                if (RegularExpression.getWordType(expressionTarget, context.start, context.limit, n, n2) == 1 && RegularExpression.getPreviousWordType(expressionTarget, context.start, context.limit, n, n2) == 2) break;
                return false;
            }
            case 62: {
                if (context.length == 0 || n == context.start) {
                    return false;
                }
                if (RegularExpression.getWordType(expressionTarget, context.start, context.limit, n, n2) == 2 && RegularExpression.getPreviousWordType(expressionTarget, context.start, context.limit, n, n2) == 1) break;
                return false;
            }
        }
        return true;
    }

    private static final int getPreviousWordType(ExpressionTarget expressionTarget, int n, int n2, int n3, int n4) {
        int n5 = RegularExpression.getWordType(expressionTarget, n, n2, --n3, n4);
        while (n5 == 0) {
            n5 = RegularExpression.getWordType(expressionTarget, n, n2, --n3, n4);
        }
        return n5;
    }

    private static final int getWordType(ExpressionTarget expressionTarget, int n, int n2, int n3, int n4) {
        if (n3 < n || n3 >= n2) {
            return 2;
        }
        return RegularExpression.getWordType0(expressionTarget.charAt(n3), n4);
    }

    public boolean matches(CharacterIterator characterIterator) {
        return this.matches(characterIterator, (Match)null);
    }

    public boolean matches(CharacterIterator characterIterator, Match match) {
        int n;
        int n2;
        int n3 = characterIterator.getBeginIndex();
        int n4 = characterIterator.getEndIndex();
        RegularExpression regularExpression = this;
        synchronized (regularExpression) {
            if (this.operations == null) {
                this.prepare();
            }
            if (this.context == null) {
                this.context = new Context();
            }
        }
        Context context = null;
        Context context2 = this.context;
        synchronized (context2) {
            context = this.context.inuse ? new Context() : this.context;
            context.reset(characterIterator, n3, n4, this.numberOfClosures);
        }
        if (match != null) {
            match.setNumberOfGroups(this.nofparen);
            match.setSource(characterIterator);
        } else if (this.hasBackReferences) {
            match = new Match();
            match.setNumberOfGroups(this.nofparen);
        }
        context.match = match;
        if (RegularExpression.isSet(this.options, 512)) {
            int n5 = this.match(context, this.operations, context.start, 1, this.options);
            if (n5 == context.limit) {
                if (context.match != null) {
                    context.match.setBeginning(0, context.start);
                    context.match.setEnd(0, n5);
                }
                context.setInUse(false);
                return true;
            }
            return false;
        }
        if (this.fixedStringOnly) {
            int n6 = this.fixedStringTable.matches(characterIterator, context.start, context.limit);
            if (n6 >= 0) {
                if (context.match != null) {
                    context.match.setBeginning(0, n6);
                    context.match.setEnd(0, n6 + this.fixedString.length());
                }
                context.setInUse(false);
                return true;
            }
            context.setInUse(false);
            return false;
        }
        if (this.fixedString != null && (n2 = this.fixedStringTable.matches(characterIterator, context.start, context.limit)) < 0) {
            context.setInUse(false);
            return false;
        }
        n2 = context.limit - this.minlength;
        int n7 = -1;
        if (this.operations != null && this.operations.type == 7 && this.operations.getChild().type == 0) {
            if (RegularExpression.isSet(this.options, 4)) {
                n = context.start;
                n7 = this.match(context, this.operations, context.start, 1, this.options);
            } else {
                boolean bl = true;
                for (n = context.start; n <= n2; ++n) {
                    char c = characterIterator.setIndex(n);
                    if (RegularExpression.isEOLChar(c)) {
                        bl = true;
                        continue;
                    }
                    if (!(bl && 0 <= (n7 = this.match(context, this.operations, n, 1, this.options)))) {
                        bl = false;
                        continue;
                    } else {
                        break;
                    }
                }
            }
        } else if (this.firstChar != null) {
            RangeToken rangeToken = this.firstChar;
            for (n = context.start; n <= n2; ++n) {
                int n8 = characterIterator.setIndex(n);
                if (REUtil.isHighSurrogate(n8) && n + 1 < context.limit) {
                    n8 = REUtil.composeFromSurrogates(n8, characterIterator.setIndex(n + 1));
                }
                if (!(rangeToken.match(n8) && 0 <= (n7 = this.match(context, this.operations, n, 1, this.options)))) {
                    continue;
                } else {
                    break;
                }
            }
        } else {
            for (n = context.start; n <= n2; ++n) {
                n7 = this.match(context, this.operations, n, 1, this.options);
                if (0 > n7) {
                    continue;
                } else {
                    break;
                }
            }
        }
        if (n7 >= 0) {
            if (context.match != null) {
                context.match.setBeginning(0, n);
                context.match.setEnd(0, n7);
            }
            context.setInUse(false);
            return true;
        }
        context.setInUse(false);
        return false;
    }

    void prepare() {
        Object object;
        int n;
        this.compile(this.tokentree);
        this.minlength = this.tokentree.getMinLength();
        this.firstChar = null;
        if (!(RegularExpression.isSet(this.options, 128) || RegularExpression.isSet(this.options, 512) || (n = this.tokentree.analyzeFirstCharacter((RangeToken)(object = Token.createRange()), this.options)) != 1)) {
            object.compactRanges();
            this.firstChar = object;
        }
        if (this.operations != null && (this.operations.type == 6 || this.operations.type == 1) && this.operations.next == null) {
            this.fixedStringOnly = true;
            if (this.operations.type == 6) {
                this.fixedString = this.operations.getString();
            } else if (this.operations.getData() >= 65536) {
                this.fixedString = REUtil.decomposeToSurrogates(this.operations.getData());
            } else {
                object = new char[1];
                object[0] = (char)this.operations.getData();
                this.fixedString = new String((char[])object);
            }
            this.fixedStringOptions = this.options;
            this.fixedStringTable = new BMPattern(this.fixedString, 256, RegularExpression.isSet(this.fixedStringOptions, 2));
        } else if (!(RegularExpression.isSet(this.options, 256) || RegularExpression.isSet(this.options, 512))) {
            object = new Token.FixedStringContainer();
            this.tokentree.findFixedString((Token.FixedStringContainer)object, this.options);
            this.fixedString = object.token == null ? null : object.token.getString();
            this.fixedStringOptions = object.options;
            if (this.fixedString != null && this.fixedString.length() < 2) {
                this.fixedString = null;
            }
            if (this.fixedString != null) {
                this.fixedStringTable = new BMPattern(this.fixedString, 256, RegularExpression.isSet(this.fixedStringOptions, 2));
            }
        }
    }

    private static final boolean isSet(int n, int n2) {
        return (n & n2) == n2;
    }

    public RegularExpression(String string) throws ParseException {
        this(string, null);
    }

    public RegularExpression(String string, String string2) throws ParseException {
        this.setPattern(string, string2);
    }

    public RegularExpression(String string, String string2, Locale locale) throws ParseException {
        this.setPattern(string, string2, locale);
    }

    RegularExpression(String string, Token token, int n, boolean bl, int n2) {
        this.regex = string;
        this.tokentree = token;
        this.nofparen = n;
        this.options = n2;
        this.hasBackReferences = bl;
    }

    public void setPattern(String string) throws ParseException {
        this.setPattern(string, Locale.getDefault());
    }

    public void setPattern(String string, Locale locale) throws ParseException {
        this.setPattern(string, this.options, locale);
    }

    private void setPattern(String string, int n, Locale locale) throws ParseException {
        this.regex = string;
        this.options = n;
        RegexParser regexParser = RegularExpression.isSet(this.options, 512) ? new ParserForXMLSchema(locale) : new RegexParser(locale);
        this.tokentree = regexParser.parse(this.regex, this.options);
        this.nofparen = regexParser.parennumber;
        this.hasBackReferences = regexParser.hasBackReferences;
        this.operations = null;
        this.context = null;
    }

    public void setPattern(String string, String string2) throws ParseException {
        this.setPattern(string, string2, Locale.getDefault());
    }

    public void setPattern(String string, String string2, Locale locale) throws ParseException {
        this.setPattern(string, REUtil.parseOptions(string2), locale);
    }

    public String getPattern() {
        return this.regex;
    }

    public String toString() {
        return this.tokentree.toString(this.options);
    }

    public String getOptions() {
        return REUtil.createOptionString(this.options);
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof RegularExpression)) {
            return false;
        }
        RegularExpression regularExpression = (RegularExpression)object;
        return this.regex.equals(regularExpression.regex) && this.options == regularExpression.options;
    }

    boolean equals(String string, int n) {
        return this.regex.equals(string) && this.options == n;
    }

    public int hashCode() {
        return (this.regex + "/" + this.getOptions()).hashCode();
    }

    public int getNumberOfGroups() {
        return this.nofparen;
    }

    private static final int getWordType0(char c, int n) {
        if (!RegularExpression.isSet(n, 64)) {
            if (RegularExpression.isSet(n, 32)) {
                return Token.getRange("IsWord", true).match(c) ? 1 : 2;
            }
            return RegularExpression.isWordChar(c) ? 1 : 2;
        }
        switch (Character.getType(c)) {
            case 1: 
            case 2: 
            case 3: 
            case 4: 
            case 5: 
            case 8: 
            case 9: 
            case 10: 
            case 11: {
                return 1;
            }
            case 6: 
            case 7: 
            case 16: {
                return 0;
            }
            case 15: {
                switch (c) {
                    case '\t': 
                    case '\n': 
                    case '\u000b': 
                    case '\f': 
                    case '\r': {
                        return 2;
                    }
                }
                return 0;
            }
        }
        return 2;
    }

    private static final boolean isEOLChar(int n) {
        return n == 10 || n == 13 || n == 8232 || n == 8233;
    }

    private static final boolean isWordChar(int n) {
        if (n == 95) {
            return true;
        }
        if (n < 48) {
            return false;
        }
        if (n > 122) {
            return false;
        }
        if (n <= 57) {
            return true;
        }
        if (n < 65) {
            return false;
        }
        if (n <= 90) {
            return true;
        }
        if (n < 97) {
            return false;
        }
        return true;
    }

    private static final boolean matchIgnoreCase(int n, int n2) {
        char c;
        if (n == n2) {
            return true;
        }
        if (n > 65535 || n2 > 65535) {
            return false;
        }
        char c2 = Character.toUpperCase((char)n);
        if (c2 == (c = Character.toUpperCase((char)n2))) {
            return true;
        }
        return Character.toLowerCase(c2) == Character.toLowerCase(c);
    }

    static final class CharArrayTarget
    extends ExpressionTarget {
        char[] target;

        CharArrayTarget(char[] arrc) {
            this.target = arrc;
        }

        final void resetTarget(char[] arrc) {
            this.target = arrc;
        }

        char charAt(int n) {
            return this.target[n];
        }

        final boolean regionMatches(boolean bl, int n, int n2, String string, int n3) {
            if (n < 0 || n2 - n < n3) {
                return false;
            }
            return bl ? this.regionMatchesIgnoreCase(n, n2, string, n3) : this.regionMatches(n, n2, string, n3);
        }

        private final boolean regionMatches(int n, int n2, String string, int n3) {
            int n4 = 0;
            while (n3-- > 0) {
                if (this.target[n++] == string.charAt(n4++)) continue;
                return false;
            }
            return true;
        }

        private final boolean regionMatchesIgnoreCase(int n, int n2, String string, int n3) {
            int n4 = 0;
            while (n3-- > 0) {
                char c;
                char c2;
                char c3;
                char c4;
                if ((c = this.target[n++]) == (c3 = string.charAt(n4++)) || (c4 = Character.toUpperCase(c)) == (c2 = Character.toUpperCase(c3)) || Character.toLowerCase(c4) == Character.toLowerCase(c2)) continue;
                return false;
            }
            return true;
        }

        final boolean regionMatches(boolean bl, int n, int n2, int n3, int n4) {
            if (n < 0 || n2 - n < n4) {
                return false;
            }
            return bl ? this.regionMatchesIgnoreCase(n, n2, n3, n4) : this.regionMatches(n, n2, n3, n4);
        }

        private final boolean regionMatches(int n, int n2, int n3, int n4) {
            int n5 = n3;
            while (n4-- > 0) {
                if (this.target[n++] == this.target[n5++]) continue;
                return false;
            }
            return true;
        }

        private final boolean regionMatchesIgnoreCase(int n, int n2, int n3, int n4) {
            int n5 = n3;
            while (n4-- > 0) {
                char c;
                char c2;
                char c3;
                char c4;
                if ((c = this.target[n++]) == (c3 = this.target[n5++]) || (c4 = Character.toUpperCase(c)) == (c2 = Character.toUpperCase(c3)) || Character.toLowerCase(c4) == Character.toLowerCase(c2)) continue;
                return false;
            }
            return true;
        }
    }

    static final class CharacterIteratorTarget
    extends ExpressionTarget {
        CharacterIterator target;

        CharacterIteratorTarget(CharacterIterator characterIterator) {
            this.target = characterIterator;
        }

        final void resetTarget(CharacterIterator characterIterator) {
            this.target = characterIterator;
        }

        final char charAt(int n) {
            return this.target.setIndex(n);
        }

        final boolean regionMatches(boolean bl, int n, int n2, String string, int n3) {
            if (n < 0 || n2 - n < n3) {
                return false;
            }
            return bl ? this.regionMatchesIgnoreCase(n, n2, string, n3) : this.regionMatches(n, n2, string, n3);
        }

        private final boolean regionMatches(int n, int n2, String string, int n3) {
            int n4 = 0;
            while (n3-- > 0) {
                if (this.target.setIndex(n++) == string.charAt(n4++)) continue;
                return false;
            }
            return true;
        }

        private final boolean regionMatchesIgnoreCase(int n, int n2, String string, int n3) {
            int n4 = 0;
            while (n3-- > 0) {
                char c;
                char c2;
                char c3;
                char c4;
                if ((c = this.target.setIndex(n++)) == (c3 = string.charAt(n4++)) || (c4 = Character.toUpperCase(c)) == (c2 = Character.toUpperCase(c3)) || Character.toLowerCase(c4) == Character.toLowerCase(c2)) continue;
                return false;
            }
            return true;
        }

        final boolean regionMatches(boolean bl, int n, int n2, int n3, int n4) {
            if (n < 0 || n2 - n < n4) {
                return false;
            }
            return bl ? this.regionMatchesIgnoreCase(n, n2, n3, n4) : this.regionMatches(n, n2, n3, n4);
        }

        private final boolean regionMatches(int n, int n2, int n3, int n4) {
            int n5 = n3;
            while (n4-- > 0) {
                if (this.target.setIndex(n++) == this.target.setIndex(n5++)) continue;
                return false;
            }
            return true;
        }

        private final boolean regionMatchesIgnoreCase(int n, int n2, int n3, int n4) {
            int n5 = n3;
            while (n4-- > 0) {
                char c;
                char c2;
                char c3;
                char c4;
                if ((c = this.target.setIndex(n++)) == (c3 = this.target.setIndex(n5++)) || (c4 = Character.toUpperCase(c)) == (c2 = Character.toUpperCase(c3)) || Character.toLowerCase(c4) == Character.toLowerCase(c2)) continue;
                return false;
            }
            return true;
        }
    }

    static final class Context {
        int start;
        int limit;
        int length;
        Match match;
        boolean inuse = false;
        int[] offsets;
        private StringTarget stringTarget;
        private CharArrayTarget charArrayTarget;
        private CharacterIteratorTarget characterIteratorTarget;
        ExpressionTarget target;

        Context() {
        }

        private void resetCommon(int n) {
            this.length = this.limit - this.start;
            this.setInUse(true);
            this.match = null;
            if (this.offsets == null || this.offsets.length != n) {
                this.offsets = new int[n];
            }
            for (int i = 0; i < n; ++i) {
                this.offsets[i] = -1;
            }
        }

        void reset(CharacterIterator characterIterator, int n, int n2, int n3) {
            if (this.characterIteratorTarget == null) {
                this.characterIteratorTarget = new CharacterIteratorTarget(characterIterator);
            } else {
                this.characterIteratorTarget.resetTarget(characterIterator);
            }
            this.target = this.characterIteratorTarget;
            this.start = n;
            this.limit = n2;
            this.resetCommon(n3);
        }

        void reset(String string, int n, int n2, int n3) {
            if (this.stringTarget == null) {
                this.stringTarget = new StringTarget(string);
            } else {
                this.stringTarget.resetTarget(string);
            }
            this.target = this.stringTarget;
            this.start = n;
            this.limit = n2;
            this.resetCommon(n3);
        }

        void reset(char[] arrc, int n, int n2, int n3) {
            if (this.charArrayTarget == null) {
                this.charArrayTarget = new CharArrayTarget(arrc);
            } else {
                this.charArrayTarget.resetTarget(arrc);
            }
            this.target = this.charArrayTarget;
            this.start = n;
            this.limit = n2;
            this.resetCommon(n3);
        }

        synchronized void setInUse(boolean bl) {
            this.inuse = bl;
        }
    }

    static abstract class ExpressionTarget {
        ExpressionTarget() {
        }

        abstract char charAt(int var1);

        abstract boolean regionMatches(boolean var1, int var2, int var3, String var4, int var5);

        abstract boolean regionMatches(boolean var1, int var2, int var3, int var4, int var5);
    }

    static final class StringTarget
    extends ExpressionTarget {
        private String target;

        StringTarget(String string) {
            this.target = string;
        }

        final void resetTarget(String string) {
            this.target = string;
        }

        final char charAt(int n) {
            return this.target.charAt(n);
        }

        final boolean regionMatches(boolean bl, int n, int n2, String string, int n3) {
            if (n2 - n < n3) {
                return false;
            }
            return bl ? this.target.regionMatches(true, n, string, 0, n3) : this.target.regionMatches(n, string, 0, n3);
        }

        final boolean regionMatches(boolean bl, int n, int n2, int n3, int n4) {
            if (n2 - n < n4) {
                return false;
            }
            return bl ? this.target.regionMatches(true, n, this.target, n3, n4) : this.target.regionMatches(n, this.target, n3, n4);
        }
    }

}

