/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Locale;
import org.apache.xerces.impl.io.MalformedByteSequenceException;
import org.apache.xerces.impl.msg.XMLMessageFormatter;
import org.apache.xerces.util.MessageFormatter;

public final class UTF8Reader
extends Reader {
    public static final int DEFAULT_BUFFER_SIZE = 2048;
    private static final boolean DEBUG_READ = false;
    protected final InputStream fInputStream;
    protected final byte[] fBuffer;
    protected int fOffset;
    private int fSurrogate = -1;
    private final MessageFormatter fFormatter;
    private final Locale fLocale;

    public UTF8Reader(InputStream inputStream) {
        this(inputStream, 2048, (MessageFormatter)new XMLMessageFormatter(), Locale.getDefault());
    }

    public UTF8Reader(InputStream inputStream, MessageFormatter messageFormatter, Locale locale) {
        this(inputStream, 2048, messageFormatter, locale);
    }

    public UTF8Reader(InputStream inputStream, int n, MessageFormatter messageFormatter, Locale locale) {
        this(inputStream, new byte[n], messageFormatter, locale);
    }

    public UTF8Reader(InputStream inputStream, byte[] arrby, MessageFormatter messageFormatter, Locale locale) {
        this.fInputStream = inputStream;
        this.fBuffer = arrby;
        this.fFormatter = messageFormatter;
        this.fLocale = locale;
    }

    public int read() throws IOException {
        int n = this.fSurrogate;
        if (this.fSurrogate == -1) {
            int n2;
            int n3 = 0;
            int n4 = n2 = n3 == this.fOffset ? this.fInputStream.read() : this.fBuffer[n3++] & 255;
            if (n2 == -1) {
                return -1;
            }
            if (n2 < 128) {
                n = (char)n2;
            } else if ((n2 & 224) == 192 && (n2 & 30) != 0) {
                int n5;
                int n6 = n5 = n3 == this.fOffset ? this.fInputStream.read() : this.fBuffer[n3++] & 255;
                if (n5 == -1) {
                    this.expectedByte(2, 2);
                }
                if ((n5 & 192) != 128) {
                    this.invalidByte(2, 2, n5);
                }
                n = n2 << 6 & 1984 | n5 & 63;
            } else if ((n2 & 240) == 224) {
                int n7;
                int n8;
                int n9 = n7 = n3 == this.fOffset ? this.fInputStream.read() : this.fBuffer[n3++] & 255;
                if (n7 == -1) {
                    this.expectedByte(2, 3);
                }
                if ((n7 & 192) != 128 || n2 == 237 && n7 >= 160 || (n2 & 15) == 0 && (n7 & 32) == 0) {
                    this.invalidByte(2, 3, n7);
                }
                int n10 = n8 = n3 == this.fOffset ? this.fInputStream.read() : this.fBuffer[n3++] & 255;
                if (n8 == -1) {
                    this.expectedByte(3, 3);
                }
                if ((n8 & 192) != 128) {
                    this.invalidByte(3, 3, n8);
                }
                n = n2 << 12 & 61440 | n7 << 6 & 4032 | n8 & 63;
            } else if ((n2 & 248) == 240) {
                int n11;
                int n12;
                int n13;
                int n14;
                int n15 = n11 = n3 == this.fOffset ? this.fInputStream.read() : this.fBuffer[n3++] & 255;
                if (n11 == -1) {
                    this.expectedByte(2, 4);
                }
                if ((n11 & 192) != 128 || (n11 & 48) == 0 && (n2 & 7) == 0) {
                    this.invalidByte(2, 3, n11);
                }
                int n16 = n13 = n3 == this.fOffset ? this.fInputStream.read() : this.fBuffer[n3++] & 255;
                if (n13 == -1) {
                    this.expectedByte(3, 4);
                }
                if ((n13 & 192) != 128) {
                    this.invalidByte(3, 3, n13);
                }
                int n17 = n12 = n3 == this.fOffset ? this.fInputStream.read() : this.fBuffer[n3++] & 255;
                if (n12 == -1) {
                    this.expectedByte(4, 4);
                }
                if ((n12 & 192) != 128) {
                    this.invalidByte(4, 4, n12);
                }
                if ((n14 = n2 << 2 & 28 | n11 >> 4 & 3) > 16) {
                    this.invalidSurrogate(n14);
                }
                int n18 = n14 - 1;
                int n19 = 55296 | n18 << 6 & 960 | n11 << 2 & 60 | n13 >> 4 & 3;
                int n20 = 56320 | n13 << 6 & 960 | n12 & 63;
                n = n19;
                this.fSurrogate = n20;
            } else {
                this.invalidByte(1, 1, n2);
            }
        } else {
            this.fSurrogate = -1;
        }
        return n;
    }

    public int read(char[] arrc, int n, int n2) throws IOException {
        int n3;
        char c;
        int n4 = n;
        int n5 = 0;
        if (this.fOffset == 0) {
            if (n2 > this.fBuffer.length) {
                n2 = this.fBuffer.length;
            }
            if (this.fSurrogate != -1) {
                arrc[n4++] = (char)this.fSurrogate;
                this.fSurrogate = -1;
                --n2;
            }
            if ((n5 = this.fInputStream.read(this.fBuffer, 0, n2)) == -1) {
                return -1;
            }
            n5+=n4 - n;
        } else {
            n5 = this.fOffset;
            this.fOffset = 0;
        }
        int n6 = n5;
        for (n3 = 0; n3 < n6; ++n3) {
            c = this.fBuffer[n3];
            if (c < '\u0000') break;
            arrc[n4++] = c;
        }
        while (n3 < n6) {
            c = this.fBuffer[n3];
            if (c >= '\u0000') {
                arrc[n4++] = c;
            } else {
                int n7;
                int n8;
                int n9;
                int n10 = c & 255;
                if ((n10 & 224) == 192 && (n10 & 30) != 0) {
                    n9 = -1;
                    if (++n3 < n6) {
                        n9 = this.fBuffer[n3] & 255;
                    } else {
                        n9 = this.fInputStream.read();
                        if (n9 == -1) {
                            if (n4 > n) {
                                this.fBuffer[0] = (byte)n10;
                                this.fOffset = 1;
                                return n4 - n;
                            }
                            this.expectedByte(2, 2);
                        }
                        ++n5;
                    }
                    if ((n9 & 192) != 128) {
                        if (n4 > n) {
                            this.fBuffer[0] = (byte)n10;
                            this.fBuffer[1] = (byte)n9;
                            this.fOffset = 2;
                            return n4 - n;
                        }
                        this.invalidByte(2, 2, n9);
                    }
                    n8 = n10 << 6 & 1984 | n9 & 63;
                    arrc[n4++] = (char)n8;
                    --n5;
                } else if ((n10 & 240) == 224) {
                    n9 = -1;
                    if (++n3 < n6) {
                        n9 = this.fBuffer[n3] & 255;
                    } else {
                        n9 = this.fInputStream.read();
                        if (n9 == -1) {
                            if (n4 > n) {
                                this.fBuffer[0] = (byte)n10;
                                this.fOffset = 1;
                                return n4 - n;
                            }
                            this.expectedByte(2, 3);
                        }
                        ++n5;
                    }
                    if ((n9 & 192) != 128 || n10 == 237 && n9 >= 160 || (n10 & 15) == 0 && (n9 & 32) == 0) {
                        if (n4 > n) {
                            this.fBuffer[0] = (byte)n10;
                            this.fBuffer[1] = (byte)n9;
                            this.fOffset = 2;
                            return n4 - n;
                        }
                        this.invalidByte(2, 3, n9);
                    }
                    n8 = -1;
                    if (++n3 < n6) {
                        n8 = this.fBuffer[n3] & 255;
                    } else {
                        n8 = this.fInputStream.read();
                        if (n8 == -1) {
                            if (n4 > n) {
                                this.fBuffer[0] = (byte)n10;
                                this.fBuffer[1] = (byte)n9;
                                this.fOffset = 2;
                                return n4 - n;
                            }
                            this.expectedByte(3, 3);
                        }
                        ++n5;
                    }
                    if ((n8 & 192) != 128) {
                        if (n4 > n) {
                            this.fBuffer[0] = (byte)n10;
                            this.fBuffer[1] = (byte)n9;
                            this.fBuffer[2] = (byte)n8;
                            this.fOffset = 3;
                            return n4 - n;
                        }
                        this.invalidByte(3, 3, n8);
                    }
                    n7 = n10 << 12 & 61440 | n9 << 6 & 4032 | n8 & 63;
                    arrc[n4++] = (char)n7;
                    n5-=2;
                } else if ((n10 & 248) == 240) {
                    int n11;
                    n9 = -1;
                    if (++n3 < n6) {
                        n9 = this.fBuffer[n3] & 255;
                    } else {
                        n9 = this.fInputStream.read();
                        if (n9 == -1) {
                            if (n4 > n) {
                                this.fBuffer[0] = (byte)n10;
                                this.fOffset = 1;
                                return n4 - n;
                            }
                            this.expectedByte(2, 4);
                        }
                        ++n5;
                    }
                    if ((n9 & 192) != 128 || (n9 & 48) == 0 && (n10 & 7) == 0) {
                        if (n4 > n) {
                            this.fBuffer[0] = (byte)n10;
                            this.fBuffer[1] = (byte)n9;
                            this.fOffset = 2;
                            return n4 - n;
                        }
                        this.invalidByte(2, 4, n9);
                    }
                    n8 = -1;
                    if (++n3 < n6) {
                        n8 = this.fBuffer[n3] & 255;
                    } else {
                        n8 = this.fInputStream.read();
                        if (n8 == -1) {
                            if (n4 > n) {
                                this.fBuffer[0] = (byte)n10;
                                this.fBuffer[1] = (byte)n9;
                                this.fOffset = 2;
                                return n4 - n;
                            }
                            this.expectedByte(3, 4);
                        }
                        ++n5;
                    }
                    if ((n8 & 192) != 128) {
                        if (n4 > n) {
                            this.fBuffer[0] = (byte)n10;
                            this.fBuffer[1] = (byte)n9;
                            this.fBuffer[2] = (byte)n8;
                            this.fOffset = 3;
                            return n4 - n;
                        }
                        this.invalidByte(3, 4, n8);
                    }
                    n7 = -1;
                    if (++n3 < n6) {
                        n7 = this.fBuffer[n3] & 255;
                    } else {
                        n7 = this.fInputStream.read();
                        if (n7 == -1) {
                            if (n4 > n) {
                                this.fBuffer[0] = (byte)n10;
                                this.fBuffer[1] = (byte)n9;
                                this.fBuffer[2] = (byte)n8;
                                this.fOffset = 3;
                                return n4 - n;
                            }
                            this.expectedByte(4, 4);
                        }
                        ++n5;
                    }
                    if ((n7 & 192) != 128) {
                        if (n4 > n) {
                            this.fBuffer[0] = (byte)n10;
                            this.fBuffer[1] = (byte)n9;
                            this.fBuffer[2] = (byte)n8;
                            this.fBuffer[3] = (byte)n7;
                            this.fOffset = 4;
                            return n4 - n;
                        }
                        this.invalidByte(4, 4, n8);
                    }
                    if ((n11 = n10 << 2 & 28 | n9 >> 4 & 3) > 16) {
                        this.invalidSurrogate(n11);
                    }
                    int n12 = n11 - 1;
                    int n13 = n9 & 15;
                    int n14 = n8 & 63;
                    int n15 = n7 & 63;
                    int n16 = 55296 | n12 << 6 & 960 | n13 << 2 | n14 >> 4;
                    int n17 = 56320 | n14 << 6 & 960 | n15;
                    arrc[n4++] = (char)n16;
                    if ((n5-=2) <= n2) {
                        arrc[n4++] = (char)n17;
                    } else {
                        this.fSurrogate = n17;
                        --n5;
                    }
                } else {
                    if (n4 > n) {
                        this.fBuffer[0] = (byte)n10;
                        this.fOffset = 1;
                        return n4 - n;
                    }
                    this.invalidByte(1, 1, n10);
                }
            }
            ++n3;
        }
        return n5;
    }

    public long skip(long l) throws IOException {
        int n;
        int n2;
        long l2 = l;
        char[] arrc = new char[this.fBuffer.length];
        while ((n = this.read(arrc, 0, n2 = (long)arrc.length < l2 ? arrc.length : (int)l2)) > 0 && (l2-=(long)n) > 0) {
        }
        long l3 = l - l2;
        return l3;
    }

    public boolean ready() throws IOException {
        return false;
    }

    public boolean markSupported() {
        return false;
    }

    public void mark(int n) throws IOException {
        throw new IOException(this.fFormatter.formatMessage(this.fLocale, "OperationNotSupported", new Object[]{"mark()", "UTF-8"}));
    }

    public void reset() throws IOException {
        this.fOffset = 0;
        this.fSurrogate = -1;
    }

    public void close() throws IOException {
        this.fInputStream.close();
    }

    private void expectedByte(int n, int n2) throws MalformedByteSequenceException {
        throw new MalformedByteSequenceException(this.fFormatter, this.fLocale, "http://www.w3.org/TR/1998/REC-xml-19980210", "ExpectedByte", new Object[]{Integer.toString(n), Integer.toString(n2)});
    }

    private void invalidByte(int n, int n2, int n3) throws MalformedByteSequenceException {
        throw new MalformedByteSequenceException(this.fFormatter, this.fLocale, "http://www.w3.org/TR/1998/REC-xml-19980210", "InvalidByte", new Object[]{Integer.toString(n), Integer.toString(n2)});
    }

    private void invalidSurrogate(int n) throws MalformedByteSequenceException {
        throw new MalformedByteSequenceException(this.fFormatter, this.fLocale, "http://www.w3.org/TR/1998/REC-xml-19980210", "InvalidHighSurrogate", new Object[]{Integer.toHexString(n)});
    }
}

