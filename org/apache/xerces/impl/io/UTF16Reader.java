/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Locale;
import org.apache.xerces.impl.io.MalformedByteSequenceException;
import org.apache.xerces.impl.msg.XMLMessageFormatter;
import org.apache.xerces.util.MessageFormatter;

public final class UTF16Reader
extends Reader {
    public static final int DEFAULT_BUFFER_SIZE = 4096;
    protected final InputStream fInputStream;
    protected final byte[] fBuffer;
    protected final boolean fIsBigEndian;
    private final MessageFormatter fFormatter;
    private final Locale fLocale;

    public UTF16Reader(InputStream inputStream, boolean bl) {
        this(inputStream, 4096, bl, (MessageFormatter)new XMLMessageFormatter(), Locale.getDefault());
    }

    public UTF16Reader(InputStream inputStream, boolean bl, MessageFormatter messageFormatter, Locale locale) {
        this(inputStream, 4096, bl, messageFormatter, locale);
    }

    public UTF16Reader(InputStream inputStream, int n, boolean bl, MessageFormatter messageFormatter, Locale locale) {
        this(inputStream, new byte[n], bl, messageFormatter, locale);
    }

    public UTF16Reader(InputStream inputStream, byte[] arrby, boolean bl, MessageFormatter messageFormatter, Locale locale) {
        this.fInputStream = inputStream;
        this.fBuffer = arrby;
        this.fIsBigEndian = bl;
        this.fFormatter = messageFormatter;
        this.fLocale = locale;
    }

    public int read() throws IOException {
        int n = this.fInputStream.read();
        if (n == -1) {
            return -1;
        }
        int n2 = this.fInputStream.read();
        if (n2 == -1) {
            this.expectedTwoBytes();
        }
        if (this.fIsBigEndian) {
            return n << 8 | n2;
        }
        return n2 << 8 | n;
    }

    public int read(char[] arrc, int n, int n2) throws IOException {
        int n3;
        int n4;
        int n5 = n2 << 1;
        if (n5 > this.fBuffer.length) {
            n5 = this.fBuffer.length;
        }
        if ((n3 = this.fInputStream.read(this.fBuffer, 0, n5)) == -1) {
            return -1;
        }
        if ((n3 & 1) != 0) {
            n4 = this.fInputStream.read();
            if (n4 == -1) {
                this.expectedTwoBytes();
            }
            this.fBuffer[n3++] = (byte)n4;
        }
        n4 = n3 >> 1;
        if (this.fIsBigEndian) {
            this.processBE(arrc, n, n4);
        } else {
            this.processLE(arrc, n, n4);
        }
        return n4;
    }

    public long skip(long l) throws IOException {
        long l2 = this.fInputStream.skip(l << true);
        if ((l2 & 1) != 0) {
            int n = this.fInputStream.read();
            if (n == -1) {
                this.expectedTwoBytes();
            }
            ++l2;
        }
        return l2 >> true;
    }

    public boolean ready() throws IOException {
        return false;
    }

    public boolean markSupported() {
        return false;
    }

    public void mark(int n) throws IOException {
        throw new IOException(this.fFormatter.formatMessage(this.fLocale, "OperationNotSupported", new Object[]{"mark()", "UTF-16"}));
    }

    public void reset() throws IOException {
    }

    public void close() throws IOException {
        this.fInputStream.close();
    }

    private void processBE(char[] arrc, int n, int n2) {
        int n3 = 0;
        for (int i = 0; i < n2; ++i) {
            int n4 = this.fBuffer[n3++] & 255;
            int n5 = this.fBuffer[n3++] & 255;
            arrc[n++] = (char)(n4 << 8 | n5);
        }
    }

    private void processLE(char[] arrc, int n, int n2) {
        int n3 = 0;
        for (int i = 0; i < n2; ++i) {
            int n4 = this.fBuffer[n3++] & 255;
            int n5 = this.fBuffer[n3++] & 255;
            arrc[n++] = (char)(n5 << 8 | n4);
        }
    }

    private void expectedTwoBytes() throws MalformedByteSequenceException {
        throw new MalformedByteSequenceException(this.fFormatter, this.fLocale, "http://www.w3.org/TR/1998/REC-xml-19980210", "ExpectedByte", new Object[]{"2", "2"});
    }
}

