/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dtd;

import org.apache.xerces.impl.XMLErrorReporter;
import org.apache.xerces.impl.dtd.XML11DTDValidator;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.util.XMLSymbols;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.NamespaceContext;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XMLDocumentHandler;
import org.apache.xerces.xni.XNIException;

public class XML11NSDTDValidator
extends XML11DTDValidator {
    private final QName fAttributeQName = new QName();

    protected final void startNamespaceScope(QName qName, XMLAttributes xMLAttributes, Augmentations augmentations) throws XNIException {
        String string;
        String string2;
        this.fNamespaceContext.pushContext();
        if (qName.prefix == XMLSymbols.PREFIX_XMLNS) {
            this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "ElementXMLNSPrefix", new Object[]{qName.rawname}, 2);
        }
        int n = xMLAttributes.getLength();
        for (int i = 0; i < n; ++i) {
            string = xMLAttributes.getLocalName(i);
            String string3 = xMLAttributes.getPrefix(i);
            if (string3 != XMLSymbols.PREFIX_XMLNS && (string3 != XMLSymbols.EMPTY_STRING || string != XMLSymbols.PREFIX_XMLNS)) continue;
            string2 = this.fSymbolTable.addSymbol(xMLAttributes.getValue(i));
            if (string3 == XMLSymbols.PREFIX_XMLNS && string == XMLSymbols.PREFIX_XMLNS) {
                this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "CantBindXMLNS", new Object[]{xMLAttributes.getQName(i)}, 2);
            }
            if (string2 == NamespaceContext.XMLNS_URI) {
                this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "CantBindXMLNS", new Object[]{xMLAttributes.getQName(i)}, 2);
            }
            if (string == XMLSymbols.PREFIX_XML) {
                if (string2 != NamespaceContext.XML_URI) {
                    this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "CantBindXML", new Object[]{xMLAttributes.getQName(i)}, 2);
                }
            } else if (string2 == NamespaceContext.XML_URI) {
                this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "CantBindXML", new Object[]{xMLAttributes.getQName(i)}, 2);
            }
            string3 = string != XMLSymbols.PREFIX_XMLNS ? string : XMLSymbols.EMPTY_STRING;
            this.fNamespaceContext.declarePrefix(string3, string2.length() != 0 ? string2 : null);
        }
        string = qName.prefix != null ? qName.prefix : XMLSymbols.EMPTY_STRING;
        qName.uri = this.fNamespaceContext.getURI(string);
        if (qName.prefix == null && qName.uri != null) {
            qName.prefix = XMLSymbols.EMPTY_STRING;
        }
        if (qName.prefix != null && qName.uri == null) {
            this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "ElementPrefixUnbound", new Object[]{qName.prefix, qName.rawname}, 2);
        }
        for (int j = 0; j < n; ++j) {
            xMLAttributes.getName(j, this.fAttributeQName);
            string2 = this.fAttributeQName.prefix != null ? this.fAttributeQName.prefix : XMLSymbols.EMPTY_STRING;
            String string4 = this.fAttributeQName.rawname;
            if (string4 == XMLSymbols.PREFIX_XMLNS) {
                this.fAttributeQName.uri = this.fNamespaceContext.getURI(XMLSymbols.PREFIX_XMLNS);
                xMLAttributes.setName(j, this.fAttributeQName);
                continue;
            }
            if (string2 == XMLSymbols.EMPTY_STRING) continue;
            this.fAttributeQName.uri = this.fNamespaceContext.getURI(string2);
            if (this.fAttributeQName.uri == null) {
                this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "AttributePrefixUnbound", new Object[]{qName.rawname, string4, string2}, 2);
            }
            xMLAttributes.setName(j, this.fAttributeQName);
        }
        int n2 = xMLAttributes.getLength();
        for (int k = 0; k < n2 - 1; ++k) {
            String string5 = xMLAttributes.getURI(k);
            if (string5 == null) continue;
            if (string5 == NamespaceContext.XMLNS_URI) continue;
            String string6 = xMLAttributes.getLocalName(k);
            for (int i2 = k + 1; i2 < n2; ++i2) {
                String string7 = xMLAttributes.getLocalName(i2);
                String string8 = xMLAttributes.getURI(i2);
                if (string6 != string7 || string5 != string8) continue;
                this.fErrorReporter.reportError("http://www.w3.org/TR/1999/REC-xml-names-19990114", "AttributeNSNotUnique", new Object[]{qName.rawname, string6, string5}, 2);
            }
        }
    }

    protected void endNamespaceScope(QName qName, Augmentations augmentations, boolean bl) throws XNIException {
        String string = qName.prefix != null ? qName.prefix : XMLSymbols.EMPTY_STRING;
        qName.uri = this.fNamespaceContext.getURI(string);
        if (qName.uri != null) {
            qName.prefix = string;
        }
        if (!(this.fDocumentHandler == null || bl)) {
            this.fDocumentHandler.endElement(qName, augmentations);
        }
        this.fNamespaceContext.popContext();
    }
}

