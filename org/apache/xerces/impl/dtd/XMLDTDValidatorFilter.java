/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dtd;

import org.apache.xerces.xni.parser.XMLDocumentFilter;

public interface XMLDTDValidatorFilter
extends XMLDocumentFilter {
    public boolean hasGrammar();

    public boolean validate();
}

