/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dtd;

import org.apache.xerces.impl.dtd.DTDGrammar;
import org.apache.xerces.impl.dtd.XMLDTDDescription;
import org.apache.xerces.impl.dtd.XMLElementDecl;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.XNIException;

final class BalancedDTDGrammar
extends DTDGrammar {
    private boolean fMixed;
    private int fDepth = 0;
    private short[] fOpStack = null;
    private int[][] fGroupIndexStack;
    private int[] fGroupIndexStackSizes;

    public BalancedDTDGrammar(SymbolTable symbolTable, XMLDTDDescription xMLDTDDescription) {
        super(symbolTable, xMLDTDDescription);
    }

    public final void startContentModel(String string, Augmentations augmentations) throws XNIException {
        this.fDepth = 0;
        this.initializeContentModelStacks();
        super.startContentModel(string, augmentations);
    }

    public final void startGroup(Augmentations augmentations) throws XNIException {
        ++this.fDepth;
        this.initializeContentModelStacks();
        this.fMixed = false;
    }

    public final void pcdata(Augmentations augmentations) throws XNIException {
        this.fMixed = true;
    }

    public final void element(String string, Augmentations augmentations) throws XNIException {
        this.addToCurrentGroup(this.addUniqueLeafNode(string));
    }

    public final void separator(short s, Augmentations augmentations) throws XNIException {
        if (s == 0) {
            this.fOpStack[this.fDepth] = 4;
        } else if (s == 1) {
            this.fOpStack[this.fDepth] = 5;
        }
    }

    public final void occurrence(short s, Augmentations augmentations) throws XNIException {
        if (!this.fMixed) {
            int n = this.fGroupIndexStackSizes[this.fDepth] - 1;
            if (s == 2) {
                this.fGroupIndexStack[this.fDepth][n] = this.addContentSpecNode(1, this.fGroupIndexStack[this.fDepth][n], -1);
            } else if (s == 3) {
                this.fGroupIndexStack[this.fDepth][n] = this.addContentSpecNode(2, this.fGroupIndexStack[this.fDepth][n], -1);
            } else if (s == 4) {
                this.fGroupIndexStack[this.fDepth][n] = this.addContentSpecNode(3, this.fGroupIndexStack[this.fDepth][n], -1);
            }
        }
    }

    public final void endGroup(Augmentations augmentations) throws XNIException {
        int n = this.fGroupIndexStackSizes[this.fDepth];
        int n2 = n > 0 ? this.addContentSpecNodes(0, n - 1) : this.addUniqueLeafNode(null);
        --this.fDepth;
        this.addToCurrentGroup(n2);
    }

    public final void endDTD(Augmentations augmentations) throws XNIException {
        super.endDTD(augmentations);
        this.fOpStack = null;
        this.fGroupIndexStack = null;
        this.fGroupIndexStackSizes = null;
    }

    protected final void addContentSpecToElement(XMLElementDecl xMLElementDecl) {
        int n = this.fGroupIndexStackSizes[0] > 0 ? this.fGroupIndexStack[0][0] : -1;
        this.setContentSpecIndex(this.fCurrentElementIndex, n);
    }

    private int addContentSpecNodes(int n, int n2) {
        if (n == n2) {
            return this.fGroupIndexStack[this.fDepth][n];
        }
        int n3 = (n + n2) / 2;
        return this.addContentSpecNode(this.fOpStack[this.fDepth], this.addContentSpecNodes(n, n3), this.addContentSpecNodes(n3 + 1, n2));
    }

    private void initializeContentModelStacks() {
        if (this.fOpStack == null) {
            this.fOpStack = new short[8];
            this.fGroupIndexStack = new int[8][];
            this.fGroupIndexStackSizes = new int[8];
        } else if (this.fDepth == this.fOpStack.length) {
            short[] arrs = new short[this.fDepth * 2];
            System.arraycopy(this.fOpStack, 0, arrs, 0, this.fDepth);
            this.fOpStack = arrs;
            int[][] arrn = new int[this.fDepth * 2][];
            System.arraycopy(this.fGroupIndexStack, 0, arrn, 0, this.fDepth);
            this.fGroupIndexStack = arrn;
            int[] arrn2 = new int[this.fDepth * 2];
            System.arraycopy(this.fGroupIndexStackSizes, 0, arrn2, 0, this.fDepth);
            this.fGroupIndexStackSizes = arrn2;
        }
        this.fOpStack[this.fDepth] = -1;
        this.fGroupIndexStackSizes[this.fDepth] = 0;
    }

    private void addToCurrentGroup(int n) {
        int[] arrn = this.fGroupIndexStack[this.fDepth];
        int[] arrn2 = this.fGroupIndexStackSizes;
        int n2 = this.fDepth;
        int n3 = arrn2[n2];
        arrn2[n2] = n3 + 1;
        int n4 = n3;
        if (arrn == null) {
            this.fGroupIndexStack[this.fDepth] = arrn = new int[8];
        } else if (n4 == arrn.length) {
            int[] arrn3 = new int[arrn.length * 2];
            System.arraycopy(arrn, 0, arrn3, 0, arrn.length);
            this.fGroupIndexStack[this.fDepth] = arrn = arrn3;
        }
        arrn[n4] = n;
    }
}

