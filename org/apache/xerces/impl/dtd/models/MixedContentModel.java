/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dtd.models;

import org.apache.xerces.impl.dtd.models.ContentModelValidator;
import org.apache.xerces.xni.QName;

public class MixedContentModel
implements ContentModelValidator {
    private final int fCount;
    private final QName[] fChildren;
    private final int[] fChildrenType;
    private final boolean fOrdered;

    public MixedContentModel(QName[] arrqName, int[] arrn, int n, int n2, boolean bl) {
        this.fCount = n2;
        this.fChildren = new QName[this.fCount];
        this.fChildrenType = new int[this.fCount];
        for (int i = 0; i < this.fCount; ++i) {
            this.fChildren[i] = new QName(arrqName[n + i]);
            this.fChildrenType[i] = arrn[n + i];
        }
        this.fOrdered = bl;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public int validate(QName[] arrqName, int n, int n2) {
        if (this.fOrdered) {
            int n3 = 0;
            for (int i = 0; i < n2; ++i) {
                String string;
                QName qName = arrqName[n + i];
                if (qName.localpart == null) continue;
                int n4 = this.fChildrenType[n3];
                if (n4 == 0 ? this.fChildren[n3].rawname != arrqName[n + i].rawname : (n4 == 6 ? (string = this.fChildren[n3].uri) != null && string != arrqName[i].uri : (n4 == 8 ? arrqName[i].uri != null : n4 == 7 && this.fChildren[n3].uri == arrqName[i].uri))) {
                    return i;
                }
                ++n3;
            }
            return -1;
        } else {
            for (int i = 0; i < n2; ++i) {
                int n5;
                QName qName = arrqName[n + i];
                if (qName.localpart == null) continue;
                for (n5 = 0; n5 < this.fCount; ++n5) {
                    int n6 = this.fChildrenType[n5];
                    if (n6 == 0) {
                        if (qName.rawname != this.fChildren[n5].rawname) continue;
                        break;
                    }
                    if (n6 == 6) {
                        String string = this.fChildren[n5].uri;
                        if (string == null) break;
                        if (string != arrqName[i].uri) continue;
                        break;
                    }
                    if (n6 == 8 ? arrqName[i].uri == null : n6 == 7 && this.fChildren[n5].uri != arrqName[i].uri) break;
                }
                if (n5 != this.fCount) continue;
                return i;
            }
        }
        return -1;
    }
}

