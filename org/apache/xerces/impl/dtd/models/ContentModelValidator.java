/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dtd.models;

import org.apache.xerces.xni.QName;

public interface ContentModelValidator {
    public int validate(QName[] var1, int var2, int var3);
}

