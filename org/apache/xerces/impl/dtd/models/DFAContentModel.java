/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.dtd.models;

import java.io.PrintStream;
import java.util.HashMap;
import org.apache.xerces.impl.dtd.models.CMAny;
import org.apache.xerces.impl.dtd.models.CMBinOp;
import org.apache.xerces.impl.dtd.models.CMLeaf;
import org.apache.xerces.impl.dtd.models.CMNode;
import org.apache.xerces.impl.dtd.models.CMStateSet;
import org.apache.xerces.impl.dtd.models.CMUniOp;
import org.apache.xerces.impl.dtd.models.ContentModelValidator;
import org.apache.xerces.xni.QName;

public class DFAContentModel
implements ContentModelValidator {
    private static String fEpsilonString = "<<CMNODE_EPSILON>>";
    private static String fEOCString = "<<CMNODE_EOC>>";
    private static final boolean DEBUG_VALIDATE_CONTENT = false;
    private QName[] fElemMap = null;
    private int[] fElemMapType = null;
    private int fElemMapSize = 0;
    private boolean fMixed;
    private int fEOCPos = 0;
    private boolean[] fFinalStateFlags = null;
    private CMStateSet[] fFollowList = null;
    private CMNode fHeadNode = null;
    private int fLeafCount = 0;
    private CMLeaf[] fLeafList = null;
    private int[] fLeafListType = null;
    private int[][] fTransTable = null;
    private int fTransTableSize = 0;
    private boolean fEmptyContentIsValid = false;
    private final QName fQName = new QName();

    public DFAContentModel(CMNode cMNode, int n, boolean bl) {
        this.fLeafCount = n;
        this.fMixed = bl;
        this.buildDFA(cMNode);
    }

    public int validate(QName[] arrqName, int n, int n2) {
        if (n2 == 0) {
            return this.fEmptyContentIsValid ? -1 : 0;
        }
        int n3 = 0;
        for (int i = 0; i < n2; ++i) {
            int n4;
            QName qName = arrqName[n + i];
            if (this.fMixed && qName.localpart == null) continue;
            for (n4 = 0; n4 < this.fElemMapSize; ++n4) {
                int n5 = this.fElemMapType[n4] & 15;
                if (n5 == 0) {
                    if (this.fElemMap[n4].rawname != qName.rawname) continue;
                    break;
                }
                if (n5 == 6) {
                    String string = this.fElemMap[n4].uri;
                    if (string == null) break;
                    if (string != qName.uri) continue;
                    break;
                }
                if (n5 == 8 ? qName.uri == null : n5 == 7 && this.fElemMap[n4].uri != qName.uri) break;
            }
            if (n4 == this.fElemMapSize) {
                return i;
            }
            if ((n3 = this.fTransTable[n3][n4]) != -1) continue;
            return i;
        }
        if (!this.fFinalStateFlags[n3]) {
            return n2;
        }
        return -1;
    }

    private void buildDFA(CMNode cMNode) {
        int n;
        Object object;
        CMStateSet[] arrcMStateSet;
        Object object2;
        int n2;
        this.fQName.setValues(null, fEOCString, fEOCString, null);
        CMLeaf cMLeaf = new CMLeaf(this.fQName);
        this.fHeadNode = new CMBinOp(5, cMNode, cMLeaf);
        this.fEOCPos = this.fLeafCount;
        cMLeaf.setPosition(this.fLeafCount++);
        this.fLeafList = new CMLeaf[this.fLeafCount];
        this.fLeafListType = new int[this.fLeafCount];
        this.postTreeBuildInit(this.fHeadNode, 0);
        this.fFollowList = new CMStateSet[this.fLeafCount];
        for (int i = 0; i < this.fLeafCount; ++i) {
            this.fFollowList[i] = new CMStateSet(this.fLeafCount);
        }
        this.calcFollowList(this.fHeadNode);
        this.fElemMap = new QName[this.fLeafCount];
        this.fElemMapType = new int[this.fLeafCount];
        this.fElemMapSize = 0;
        for (int j = 0; j < this.fLeafCount; ++j) {
            this.fElemMap[j] = new QName();
            object2 = this.fLeafList[j].getElement();
            for (n = 0; n < this.fElemMapSize; ++n) {
                if (this.fElemMap[n].rawname == object2.rawname) break;
            }
            if (n != this.fElemMapSize) continue;
            this.fElemMap[this.fElemMapSize].setValues((QName)object2);
            this.fElemMapType[this.fElemMapSize] = this.fLeafListType[j];
            ++this.fElemMapSize;
        }
        object2 = new int[this.fLeafCount + this.fElemMapSize];
        n = 0;
        for (int k = 0; k < this.fElemMapSize; ++k) {
            for (n2 = 0; n2 < this.fLeafCount; ++n2) {
                arrcMStateSet = this.fLeafList[n2].getElement();
                object = this.fElemMap[k];
                if (arrcMStateSet.rawname != object.rawname) continue;
                object2[n++] = n2;
            }
            object2[n++] = -1;
        }
        n2 = this.fLeafCount * 4;
        arrcMStateSet = new CMStateSet[n2];
        this.fFinalStateFlags = new boolean[n2];
        this.fTransTable = new int[n2][];
        object = this.fHeadNode.firstPos();
        int n3 = 0;
        int n4 = 0;
        this.fTransTable[n4] = this.makeDefStateList();
        arrcMStateSet[n4] = object;
        HashMap<CMStateSet, Integer> hashMap = new HashMap<CMStateSet, Integer>();
        while (n3 < ++n4) {
            object = arrcMStateSet[n3];
            int[] arrn = this.fTransTable[n3];
            this.fFinalStateFlags[n3] = object.getBit(this.fEOCPos);
            ++n3;
            CMStateSet cMStateSet = null;
            int n5 = 0;
            for (int i2 = 0; i2 < this.fElemMapSize; ++i2) {
                int n6;
                if (cMStateSet == null) {
                    cMStateSet = new CMStateSet(this.fLeafCount);
                } else {
                    cMStateSet.zeroBits();
                }
                Object object3 = object2[n5++];
                while (object3 != -1) {
                    if (object.getBit((int)object3)) {
                        cMStateSet.union(this.fFollowList[object3]);
                    }
                    object3 = object2[n5++];
                }
                if (cMStateSet.isEmpty()) continue;
                Integer n7 = (Integer)hashMap.get(cMStateSet);
                int n8 = n6 = n7 == null ? n4 : n7;
                if (n6 == n4) {
                    arrcMStateSet[n4] = cMStateSet;
                    this.fTransTable[n4] = this.makeDefStateList();
                    hashMap.put(cMStateSet, new Integer(n4));
                    ++n4;
                    cMStateSet = null;
                }
                arrn[i2] = n6;
                if (n4 != n2) continue;
                int n9 = (int)((double)n2 * 1.5);
                CMStateSet[] arrcMStateSet2 = new CMStateSet[n9];
                boolean[] arrbl = new boolean[n9];
                int[][] arrn2 = new int[n9][];
                System.arraycopy(arrcMStateSet, 0, arrcMStateSet2, 0, n2);
                System.arraycopy(this.fFinalStateFlags, 0, arrbl, 0, n2);
                System.arraycopy(this.fTransTable, 0, arrn2, 0, n2);
                n2 = n9;
                arrcMStateSet = arrcMStateSet2;
                this.fFinalStateFlags = arrbl;
                this.fTransTable = arrn2;
            }
        }
        this.fEmptyContentIsValid = ((CMBinOp)this.fHeadNode).getLeft().isNullable();
        this.fHeadNode = null;
        this.fLeafList = null;
        this.fFollowList = null;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void calcFollowList(CMNode cMNode) {
        if (cMNode.type() == 4) {
            this.calcFollowList(((CMBinOp)cMNode).getLeft());
            this.calcFollowList(((CMBinOp)cMNode).getRight());
            return;
        } else if (cMNode.type() == 5) {
            this.calcFollowList(((CMBinOp)cMNode).getLeft());
            this.calcFollowList(((CMBinOp)cMNode).getRight());
            CMStateSet cMStateSet = ((CMBinOp)cMNode).getLeft().lastPos();
            CMStateSet cMStateSet2 = ((CMBinOp)cMNode).getRight().firstPos();
            for (int i = 0; i < this.fLeafCount; ++i) {
                if (!cMStateSet.getBit(i)) continue;
                this.fFollowList[i].union(cMStateSet2);
            }
            return;
        } else if (cMNode.type() == 2 || cMNode.type() == 3) {
            this.calcFollowList(((CMUniOp)cMNode).getChild());
            CMStateSet cMStateSet = cMNode.firstPos();
            CMStateSet cMStateSet3 = cMNode.lastPos();
            for (int i = 0; i < this.fLeafCount; ++i) {
                if (!cMStateSet3.getBit(i)) continue;
                this.fFollowList[i].union(cMStateSet);
            }
            return;
        } else {
            if (cMNode.type() != 1) return;
            this.calcFollowList(((CMUniOp)cMNode).getChild());
        }
    }

    private void dumpTree(CMNode cMNode, int n) {
        for (int i = 0; i < n; ++i) {
            System.out.print("   ");
        }
        int n2 = cMNode.type();
        if (n2 == 4 || n2 == 5) {
            if (n2 == 4) {
                System.out.print("Choice Node ");
            } else {
                System.out.print("Seq Node ");
            }
            if (cMNode.isNullable()) {
                System.out.print("Nullable ");
            }
            System.out.print("firstPos=");
            System.out.print(cMNode.firstPos().toString());
            System.out.print(" lastPos=");
            System.out.println(cMNode.lastPos().toString());
            this.dumpTree(((CMBinOp)cMNode).getLeft(), n + 1);
            this.dumpTree(((CMBinOp)cMNode).getRight(), n + 1);
        } else if (cMNode.type() == 2) {
            System.out.print("Rep Node ");
            if (cMNode.isNullable()) {
                System.out.print("Nullable ");
            }
            System.out.print("firstPos=");
            System.out.print(cMNode.firstPos().toString());
            System.out.print(" lastPos=");
            System.out.println(cMNode.lastPos().toString());
            this.dumpTree(((CMUniOp)cMNode).getChild(), n + 1);
        } else if (cMNode.type() == 0) {
            System.out.print("Leaf: (pos=" + ((CMLeaf)cMNode).getPosition() + "), " + ((CMLeaf)cMNode).getElement() + "(elemIndex=" + ((CMLeaf)cMNode).getElement() + ") ");
            if (cMNode.isNullable()) {
                System.out.print(" Nullable ");
            }
            System.out.print("firstPos=");
            System.out.print(cMNode.firstPos().toString());
            System.out.print(" lastPos=");
            System.out.println(cMNode.lastPos().toString());
        } else {
            throw new RuntimeException("ImplementationMessages.VAL_NIICM");
        }
    }

    private int[] makeDefStateList() {
        int[] arrn = new int[this.fElemMapSize];
        for (int i = 0; i < this.fElemMapSize; ++i) {
            arrn[i] = -1;
        }
        return arrn;
    }

    private int postTreeBuildInit(CMNode cMNode, int n) {
        cMNode.setMaxStates(this.fLeafCount);
        if ((cMNode.type() & 15) == 6 || (cMNode.type() & 15) == 8 || (cMNode.type() & 15) == 7) {
            QName qName = new QName(null, null, null, ((CMAny)cMNode).getURI());
            this.fLeafList[n] = new CMLeaf(qName, ((CMAny)cMNode).getPosition());
            this.fLeafListType[n] = cMNode.type();
            ++n;
        } else if (cMNode.type() == 4 || cMNode.type() == 5) {
            n = this.postTreeBuildInit(((CMBinOp)cMNode).getLeft(), n);
            n = this.postTreeBuildInit(((CMBinOp)cMNode).getRight(), n);
        } else if (cMNode.type() == 2 || cMNode.type() == 3 || cMNode.type() == 1) {
            n = this.postTreeBuildInit(((CMUniOp)cMNode).getChild(), n);
        } else if (cMNode.type() == 0) {
            QName qName = ((CMLeaf)cMNode).getElement();
            if (qName.localpart != fEpsilonString) {
                this.fLeafList[n] = (CMLeaf)cMNode;
                this.fLeafListType[n] = 0;
                ++n;
            }
        } else {
            throw new RuntimeException("ImplementationMessages.VAL_NIICM: type=" + cMNode.type());
        }
        return n;
    }

    static {
        fEpsilonString = fEpsilonString.intern();
        fEOCString = fEOCString.intern();
    }
}

