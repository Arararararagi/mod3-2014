/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.util;

import java.util.AbstractList;
import java.util.Vector;
import org.apache.xerces.xs.StringList;

public final class StringListImpl
extends AbstractList
implements StringList {
    public static final StringListImpl EMPTY_LIST = new StringListImpl(new String[0], 0);
    private final String[] fArray;
    private final int fLength;
    private final Vector fVector;

    public StringListImpl(Vector vector) {
        this.fVector = vector;
        this.fLength = vector == null ? 0 : vector.size();
        this.fArray = null;
    }

    public StringListImpl(String[] arrstring, int n) {
        this.fArray = arrstring;
        this.fLength = n;
        this.fVector = null;
    }

    public int getLength() {
        return this.fLength;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public boolean contains(String string) {
        if (this.fVector != null) {
            return this.fVector.contains(string);
        }
        if (string == null) {
            for (int i = 0; i < this.fLength; ++i) {
                if (this.fArray[i] != null) continue;
                return true;
            }
            return false;
        } else {
            for (int i = 0; i < this.fLength; ++i) {
                if (!string.equals(this.fArray[i])) continue;
                return true;
            }
        }
        return false;
    }

    public String item(int n) {
        if (n < 0 || n >= this.fLength) {
            return null;
        }
        if (this.fVector != null) {
            return (String)this.fVector.elementAt(n);
        }
        return this.fArray[n];
    }

    public Object get(int n) {
        if (n >= 0 && n < this.fLength) {
            if (this.fVector != null) {
                return this.fVector.elementAt(n);
            }
            return this.fArray[n];
        }
        throw new IndexOutOfBoundsException("Index: " + n);
    }

    public int size() {
        return this.getLength();
    }

    public Object[] toArray() {
        if (this.fVector != null) {
            return this.fVector.toArray();
        }
        Object[] arrobject = new Object[this.fLength];
        this.toArray0(arrobject);
        return arrobject;
    }

    public Object[] toArray(Object[] arrobject) {
        if (this.fVector != null) {
            return this.fVector.toArray(arrobject);
        }
        if (arrobject.length < this.fLength) {
            Class class_ = arrobject.getClass();
            Class class_2 = class_.getComponentType();
            arrobject = (Object[])Array.newInstance(class_2, this.fLength);
        }
        this.toArray0(arrobject);
        if (arrobject.length > this.fLength) {
            arrobject[this.fLength] = null;
        }
        return arrobject;
    }

    private void toArray0(Object[] arrobject) {
        if (this.fLength > 0) {
            System.arraycopy(this.fArray, 0, arrobject, 0, this.fLength);
        }
    }
}

