/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.util;

import java.util.ArrayList;
import org.apache.xerces.impl.xs.SchemaGrammar;
import org.apache.xerces.impl.xs.XSModelImpl;
import org.apache.xerces.util.XMLGrammarPoolImpl;
import org.apache.xerces.xni.grammars.Grammar;
import org.apache.xerces.xni.grammars.XMLGrammarDescription;
import org.apache.xerces.xs.XSModel;

public class XSGrammarPool
extends XMLGrammarPoolImpl {
    public XSModel toXSModel() {
        return this.toXSModel(1);
    }

    public XSModel toXSModel(short s) {
        ArrayList<Grammar> arrayList = new ArrayList<Grammar>();
        for (int i = 0; i < this.fGrammars.length; ++i) {
            XMLGrammarPoolImpl.Entry entry = this.fGrammars[i];
            while (entry != null) {
                if (entry.desc.getGrammarType().equals("http://www.w3.org/2001/XMLSchema")) {
                    arrayList.add(entry.grammar);
                }
                entry = entry.next;
            }
        }
        int n = arrayList.size();
        if (n == 0) {
            return this.toXSModel(new SchemaGrammar[0], s);
        }
        SchemaGrammar[] arrschemaGrammar = arrayList.toArray(new SchemaGrammar[n]);
        return this.toXSModel(arrschemaGrammar, s);
    }

    protected XSModel toXSModel(SchemaGrammar[] arrschemaGrammar, short s) {
        return new XSModelImpl(arrschemaGrammar, s);
    }
}

