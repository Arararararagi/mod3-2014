/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.util;

import java.util.AbstractList;
import org.apache.xerces.xs.datatypes.ObjectList;

public final class ObjectListImpl
extends AbstractList
implements ObjectList {
    public static final ObjectListImpl EMPTY_LIST = new ObjectListImpl(new Object[0], 0);
    private final Object[] fArray;
    private final int fLength;

    public ObjectListImpl(Object[] arrobject, int n) {
        this.fArray = arrobject;
        this.fLength = n;
    }

    public int getLength() {
        return this.fLength;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public boolean contains(Object object) {
        if (object == null) {
            for (int i = 0; i < this.fLength; ++i) {
                if (this.fArray[i] != null) continue;
                return true;
            }
            return false;
        } else {
            for (int i = 0; i < this.fLength; ++i) {
                if (!object.equals(this.fArray[i])) continue;
                return true;
            }
        }
        return false;
    }

    public Object item(int n) {
        if (n < 0 || n >= this.fLength) {
            return null;
        }
        return this.fArray[n];
    }

    public Object get(int n) {
        if (n >= 0 && n < this.fLength) {
            return this.fArray[n];
        }
        throw new IndexOutOfBoundsException("Index: " + n);
    }

    public int size() {
        return this.getLength();
    }

    public Object[] toArray() {
        Object[] arrobject = new Object[this.fLength];
        this.toArray0(arrobject);
        return arrobject;
    }

    public Object[] toArray(Object[] arrobject) {
        if (arrobject.length < this.fLength) {
            Class class_ = arrobject.getClass();
            Class class_2 = class_.getComponentType();
            arrobject = (Object[])Array.newInstance(class_2, this.fLength);
        }
        this.toArray0(arrobject);
        if (arrobject.length > this.fLength) {
            arrobject[this.fLength] = null;
        }
        return arrobject;
    }

    private void toArray0(Object[] arrobject) {
        if (this.fLength > 0) {
            System.arraycopy(this.fArray, 0, arrobject, 0, this.fLength);
        }
    }
}

