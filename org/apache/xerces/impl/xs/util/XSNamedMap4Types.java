/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.util;

import org.apache.xerces.impl.xs.util.XSNamedMapImpl;
import org.apache.xerces.util.SymbolHash;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSTypeDefinition;

public final class XSNamedMap4Types
extends XSNamedMapImpl {
    private final short fType;

    public XSNamedMap4Types(String string, SymbolHash symbolHash, short s) {
        super(string, symbolHash);
        this.fType = s;
    }

    public XSNamedMap4Types(String[] arrstring, SymbolHash[] arrsymbolHash, int n, short s) {
        super(arrstring, arrsymbolHash, n);
        this.fType = s;
    }

    public synchronized int getLength() {
        if (this.fLength == -1) {
            int n = 0;
            for (int i = 0; i < this.fNSNum; ++i) {
                n+=this.fMaps[i].getLength();
            }
            int n2 = 0;
            Object[] arrobject = new XSObject[n];
            for (int j = 0; j < this.fNSNum; ++j) {
                n2+=this.fMaps[j].getValues(arrobject, n2);
            }
            this.fLength = 0;
            this.fArray = new XSObject[n];
            for (int k = 0; k < n; ++k) {
                XSTypeDefinition xSTypeDefinition = (XSTypeDefinition)arrobject[k];
                if (xSTypeDefinition.getTypeCategory() != this.fType) continue;
                this.fArray[this.fLength++] = xSTypeDefinition;
            }
        }
        return this.fLength;
    }

    public XSObject itemByName(String string, String string2) {
        for (int i = 0; i < this.fNSNum; ++i) {
            if (!XSNamedMapImpl.isEqual(string, this.fNamespaces[i])) continue;
            XSTypeDefinition xSTypeDefinition = (XSTypeDefinition)this.fMaps[i].get(string2);
            if (xSTypeDefinition != null && xSTypeDefinition.getTypeCategory() == this.fType) {
                return xSTypeDefinition;
            }
            return null;
        }
        return null;
    }

    public synchronized XSObject item(int n) {
        if (this.fArray == null) {
            this.getLength();
        }
        if (n < 0 || n >= this.fLength) {
            return null;
        }
        return this.fArray[n];
    }
}

