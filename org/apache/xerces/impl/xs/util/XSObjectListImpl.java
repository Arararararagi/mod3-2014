/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.util;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;

public class XSObjectListImpl
extends AbstractList
implements XSObjectList {
    public static final XSObjectListImpl EMPTY_LIST = new XSObjectListImpl(new XSObject[0], 0);
    private static final ListIterator EMPTY_ITERATOR = new ListIterator(){

        public boolean hasNext() {
            return false;
        }

        public Object next() {
            throw new NoSuchElementException();
        }

        public boolean hasPrevious() {
            return false;
        }

        public Object previous() {
            throw new NoSuchElementException();
        }

        public int nextIndex() {
            return 0;
        }

        public int previousIndex() {
            return -1;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public void set(Object object) {
            throw new UnsupportedOperationException();
        }

        public void add(Object object) {
            throw new UnsupportedOperationException();
        }
    };
    private static final int DEFAULT_SIZE = 4;
    private XSObject[] fArray = null;
    private int fLength = 0;

    public XSObjectListImpl() {
        this.fArray = new XSObject[4];
        this.fLength = 0;
    }

    public XSObjectListImpl(XSObject[] arrxSObject, int n) {
        this.fArray = arrxSObject;
        this.fLength = n;
    }

    public int getLength() {
        return this.fLength;
    }

    public XSObject item(int n) {
        if (n < 0 || n >= this.fLength) {
            return null;
        }
        return this.fArray[n];
    }

    public void clearXSObjectList() {
        for (int i = 0; i < this.fLength; ++i) {
            this.fArray[i] = null;
        }
        this.fArray = null;
        this.fLength = 0;
    }

    public void addXSObject(XSObject xSObject) {
        if (this.fLength == this.fArray.length) {
            XSObject[] arrxSObject = new XSObject[this.fLength + 4];
            System.arraycopy(this.fArray, 0, arrxSObject, 0, this.fLength);
            this.fArray = arrxSObject;
        }
        this.fArray[this.fLength++] = xSObject;
    }

    public void addXSObject(int n, XSObject xSObject) {
        this.fArray[n] = xSObject;
    }

    public boolean contains(Object object) {
        return object == null ? this.containsNull() : this.containsObject(object);
    }

    public Object get(int n) {
        if (n >= 0 && n < this.fLength) {
            return this.fArray[n];
        }
        throw new IndexOutOfBoundsException("Index: " + n);
    }

    public int size() {
        return this.getLength();
    }

    public Iterator iterator() {
        return this.listIterator0(0);
    }

    public ListIterator listIterator() {
        return this.listIterator0(0);
    }

    public ListIterator listIterator(int n) {
        if (n >= 0 && n < this.fLength) {
            return this.listIterator0(n);
        }
        throw new IndexOutOfBoundsException("Index: " + n);
    }

    private ListIterator listIterator0(int n) {
        return this.fLength == 0 ? EMPTY_ITERATOR : new XSObjectListIterator(n);
    }

    private boolean containsObject(Object object) {
        for (int i = this.fLength - 1; i >= 0; --i) {
            if (!object.equals(this.fArray[i])) continue;
            return true;
        }
        return false;
    }

    private boolean containsNull() {
        for (int i = this.fLength - 1; i >= 0; --i) {
            if (this.fArray[i] != null) continue;
            return true;
        }
        return false;
    }

    public Object[] toArray() {
        Object[] arrobject = new Object[this.fLength];
        this.toArray0(arrobject);
        return arrobject;
    }

    public Object[] toArray(Object[] arrobject) {
        if (arrobject.length < this.fLength) {
            Class class_ = arrobject.getClass();
            Class class_2 = class_.getComponentType();
            arrobject = (Object[])Array.newInstance(class_2, this.fLength);
        }
        this.toArray0(arrobject);
        if (arrobject.length > this.fLength) {
            arrobject[this.fLength] = null;
        }
        return arrobject;
    }

    private void toArray0(Object[] arrobject) {
        if (this.fLength > 0) {
            System.arraycopy(this.fArray, 0, arrobject, 0, this.fLength);
        }
    }

    private final class XSObjectListIterator
    implements ListIterator {
        private int index;

        public XSObjectListIterator(int n) {
            this.index = n;
        }

        public boolean hasNext() {
            return this.index < XSObjectListImpl.this.fLength;
        }

        public Object next() {
            if (this.index < XSObjectListImpl.this.fLength) {
                return XSObjectListImpl.this.fArray[this.index++];
            }
            throw new NoSuchElementException();
        }

        public boolean hasPrevious() {
            return this.index > 0;
        }

        public Object previous() {
            if (this.index > 0) {
                return XSObjectListImpl.this.fArray[--this.index];
            }
            throw new NoSuchElementException();
        }

        public int nextIndex() {
            return this.index;
        }

        public int previousIndex() {
            return this.index - 1;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public void set(Object object) {
            throw new UnsupportedOperationException();
        }

        public void add(Object object) {
            throw new UnsupportedOperationException();
        }
    }

}

