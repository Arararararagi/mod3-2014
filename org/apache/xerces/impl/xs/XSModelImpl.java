/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Vector;
import org.apache.xerces.impl.xs.SchemaGrammar;
import org.apache.xerces.impl.xs.SchemaSymbols;
import org.apache.xerces.impl.xs.SubstitutionGroupHandler;
import org.apache.xerces.impl.xs.XSAnnotationImpl;
import org.apache.xerces.impl.xs.XSAttributeDecl;
import org.apache.xerces.impl.xs.XSAttributeGroupDecl;
import org.apache.xerces.impl.xs.XSElementDecl;
import org.apache.xerces.impl.xs.XSGrammarBucket;
import org.apache.xerces.impl.xs.XSGroupDecl;
import org.apache.xerces.impl.xs.XSNotationDecl;
import org.apache.xerces.impl.xs.util.StringListImpl;
import org.apache.xerces.impl.xs.util.XSNamedMap4Types;
import org.apache.xerces.impl.xs.util.XSNamedMapImpl;
import org.apache.xerces.impl.xs.util.XSObjectListImpl;
import org.apache.xerces.util.SymbolHash;
import org.apache.xerces.util.XMLSymbols;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSAttributeGroupDefinition;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSModelGroupDefinition;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSNamespaceItem;
import org.apache.xerces.xs.XSNamespaceItemList;
import org.apache.xerces.xs.XSNotationDeclaration;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;

public final class XSModelImpl
extends AbstractList
implements XSModel,
XSNamespaceItemList {
    private static final short MAX_COMP_IDX = 16;
    private static final boolean[] GLOBAL_COMP = new boolean[]{false, true, true, true, false, true, true, false, false, false, false, true, false, false, false, true, true};
    private final int fGrammarCount;
    private final String[] fNamespaces;
    private final SchemaGrammar[] fGrammarList;
    private final SymbolHash fGrammarMap;
    private final SymbolHash fSubGroupMap;
    private final XSNamedMap[] fGlobalComponents;
    private final XSNamedMap[][] fNSComponents;
    private final StringList fNamespacesList;
    private XSObjectList fAnnotations = null;
    private final boolean fHasIDC;

    public XSModelImpl(SchemaGrammar[] arrschemaGrammar) {
        this(arrschemaGrammar, 1);
    }

    public XSModelImpl(SchemaGrammar[] arrschemaGrammar, short s) {
        Object object;
        int n;
        int n2 = arrschemaGrammar.length;
        int n3 = Math.max(n2 + 1, 5);
        String[] arrstring = new String[n3];
        SchemaGrammar[] arrschemaGrammar2 = new SchemaGrammar[n3];
        boolean bl = false;
        for (int i = 0; i < n2; ++i) {
            SchemaGrammar schemaGrammar = arrschemaGrammar[i];
            arrstring[i] = object = schemaGrammar.getTargetNamespace();
            arrschemaGrammar2[i] = schemaGrammar;
            if (object != SchemaSymbols.URI_SCHEMAFORSCHEMA) continue;
            bl = true;
        }
        if (!bl) {
            arrstring[n2] = SchemaSymbols.URI_SCHEMAFORSCHEMA;
            arrschemaGrammar2[n2++] = SchemaGrammar.getS4SGrammar(s);
        }
        for (n = 0; n < n2; ++n) {
            for (int j = (vector = (schemaGrammar = arrschemaGrammar2[n]).getImportedGrammars()) == null ? -1 : vector.size() - 1; j >= 0; --j) {
                int n4;
                Vector vector;
                object = (SchemaGrammar)vector.elementAt(j);
                for (n4 = 0; n4 < n2; ++n4) {
                    if (object == arrschemaGrammar2[n4]) break;
                }
                if (n4 != n2) continue;
                if (n2 == arrschemaGrammar2.length) {
                    String[] arrstring2 = new String[n2 * 2];
                    System.arraycopy(arrstring, 0, arrstring2, 0, n2);
                    arrstring = arrstring2;
                    SchemaGrammar[] arrschemaGrammar3 = new SchemaGrammar[n2 * 2];
                    System.arraycopy(arrschemaGrammar2, 0, arrschemaGrammar3, 0, n2);
                    arrschemaGrammar2 = arrschemaGrammar3;
                }
                arrstring[n2] = object.getTargetNamespace();
                arrschemaGrammar2[n2] = object;
                ++n2;
            }
        }
        this.fNamespaces = arrstring;
        this.fGrammarList = arrschemaGrammar2;
        boolean bl2 = false;
        this.fGrammarMap = new SymbolHash(n2 * 2);
        for (n = 0; n < n2; ++n) {
            this.fGrammarMap.put(XSModelImpl.null2EmptyString(this.fNamespaces[n]), this.fGrammarList[n]);
            if (!this.fGrammarList[n].hasIDConstraints()) continue;
            bl2 = true;
        }
        this.fHasIDC = bl2;
        this.fGrammarCount = n2;
        this.fGlobalComponents = new XSNamedMap[17];
        this.fNSComponents = new XSNamedMap[n2][17];
        this.fNamespacesList = new StringListImpl(this.fNamespaces, this.fGrammarCount);
        this.fSubGroupMap = this.buildSubGroups();
    }

    private SymbolHash buildSubGroups_Org() {
        SubstitutionGroupHandler substitutionGroupHandler = new SubstitutionGroupHandler(null);
        for (int i = 0; i < this.fGrammarCount; ++i) {
            substitutionGroupHandler.addSubstitutionGroup(this.fGrammarList[i].getSubstitutionGroups());
        }
        XSNamedMap xSNamedMap = this.getComponents(2);
        int n = xSNamedMap.getLength();
        SymbolHash symbolHash = new SymbolHash(n * 2);
        for (int j = 0; j < n; ++j) {
            XSElementDecl xSElementDecl;
            XSObject[] arrxSObject = substitutionGroupHandler.getSubstitutionGroup(xSElementDecl = (XSElementDecl)xSNamedMap.item(j));
            symbolHash.put(xSElementDecl, arrxSObject.length > 0 ? new XSObjectListImpl(arrxSObject, arrxSObject.length) : XSObjectListImpl.EMPTY_LIST);
        }
        return symbolHash;
    }

    private SymbolHash buildSubGroups() {
        SubstitutionGroupHandler substitutionGroupHandler = new SubstitutionGroupHandler(null);
        for (int i = 0; i < this.fGrammarCount; ++i) {
            substitutionGroupHandler.addSubstitutionGroup(this.fGrammarList[i].getSubstitutionGroups());
        }
        XSObjectListImpl xSObjectListImpl = this.getGlobalElements();
        int n = xSObjectListImpl.getLength();
        SymbolHash symbolHash = new SymbolHash(n * 2);
        for (int j = 0; j < n; ++j) {
            XSElementDecl xSElementDecl;
            XSObject[] arrxSObject = substitutionGroupHandler.getSubstitutionGroup(xSElementDecl = (XSElementDecl)xSObjectListImpl.item(j));
            symbolHash.put(xSElementDecl, arrxSObject.length > 0 ? new XSObjectListImpl(arrxSObject, arrxSObject.length) : XSObjectListImpl.EMPTY_LIST);
        }
        return symbolHash;
    }

    private XSObjectListImpl getGlobalElements() {
        SymbolHash[] arrsymbolHash = new SymbolHash[this.fGrammarCount];
        int n = 0;
        for (int i = 0; i < this.fGrammarCount; ++i) {
            arrsymbolHash[i] = this.fGrammarList[i].fAllGlobalElemDecls;
            n+=arrsymbolHash[i].getLength();
        }
        if (n == 0) {
            return XSObjectListImpl.EMPTY_LIST;
        }
        Object[] arrobject = new XSObject[n];
        int n2 = 0;
        for (int j = 0; j < this.fGrammarCount; ++j) {
            arrsymbolHash[j].getValues(arrobject, n2);
            n2+=arrsymbolHash[j].getLength();
        }
        return new XSObjectListImpl((XSObject[])arrobject, n);
    }

    public StringList getNamespaces() {
        return this.fNamespacesList;
    }

    public XSNamespaceItemList getNamespaceItems() {
        return this;
    }

    public synchronized XSNamedMap getComponents(short s) {
        if (!(s > 0 && s <= 16 && GLOBAL_COMP[s])) {
            return XSNamedMapImpl.EMPTY_MAP;
        }
        SymbolHash[] arrsymbolHash = new SymbolHash[this.fGrammarCount];
        if (this.fGlobalComponents[s] == null) {
            block8 : for (int i = 0; i < this.fGrammarCount; ++i) {
                switch (s) {
                    case 3: 
                    case 15: 
                    case 16: {
                        arrsymbolHash[i] = this.fGrammarList[i].fGlobalTypeDecls;
                        continue block8;
                    }
                    case 1: {
                        arrsymbolHash[i] = this.fGrammarList[i].fGlobalAttrDecls;
                        continue block8;
                    }
                    case 2: {
                        arrsymbolHash[i] = this.fGrammarList[i].fGlobalElemDecls;
                        continue block8;
                    }
                    case 5: {
                        arrsymbolHash[i] = this.fGrammarList[i].fGlobalAttrGrpDecls;
                        continue block8;
                    }
                    case 6: {
                        arrsymbolHash[i] = this.fGrammarList[i].fGlobalGroupDecls;
                        continue block8;
                    }
                    case 11: {
                        arrsymbolHash[i] = this.fGrammarList[i].fGlobalNotationDecls;
                    }
                }
            }
            this.fGlobalComponents[s] = s == 15 || s == 16 ? new XSNamedMap4Types(this.fNamespaces, arrsymbolHash, this.fGrammarCount, s) : new XSNamedMapImpl(this.fNamespaces, arrsymbolHash, this.fGrammarCount);
        }
        return this.fGlobalComponents[s];
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public synchronized XSNamedMap getComponentsByNamespace(short var1_1, String var2_2) {
        if (var1_1 <= 0) return XSNamedMapImpl.EMPTY_MAP;
        if (var1_1 > 16) return XSNamedMapImpl.EMPTY_MAP;
        if (!XSModelImpl.GLOBAL_COMP[var1_1]) {
            return XSNamedMapImpl.EMPTY_MAP;
        }
        if (var2_2 == null) ** GOTO lbl12
        for (var3_3 = 0; var3_3 < this.fGrammarCount; ++var3_3) {
            if (!var2_2.equals(this.fNamespaces[var3_3])) {
                continue;
            } else {
                ** GOTO lbl13
            }
        }
        ** GOTO lbl13
        while (this.fNamespaces[var3_3] != null) {
            ++var3_3;
lbl12: // 2 sources:
            if (var3_3 < this.fGrammarCount) continue;
        }
lbl13: // 5 sources:
        if (var3_3 == this.fGrammarCount) {
            return XSNamedMapImpl.EMPTY_MAP;
        }
        if (this.fNSComponents[var3_3][var1_1] != null) return this.fNSComponents[var3_3][var1_1];
        var4_4 = null;
        switch (var1_1) {
            case 3: 
            case 15: 
            case 16: {
                var4_4 = this.fGrammarList[var3_3].fGlobalTypeDecls;
                break;
            }
            case 1: {
                var4_4 = this.fGrammarList[var3_3].fGlobalAttrDecls;
                break;
            }
            case 2: {
                var4_4 = this.fGrammarList[var3_3].fGlobalElemDecls;
                break;
            }
            case 5: {
                var4_4 = this.fGrammarList[var3_3].fGlobalAttrGrpDecls;
                break;
            }
            case 6: {
                var4_4 = this.fGrammarList[var3_3].fGlobalGroupDecls;
                break;
            }
            case 11: {
                var4_4 = this.fGrammarList[var3_3].fGlobalNotationDecls;
            }
        }
        if (var1_1 != 15 && var1_1 != 16) {
            this.fNSComponents[var3_3][var1_1] = new XSNamedMapImpl(var2_2, var4_4);
            return this.fNSComponents[var3_3][var1_1];
        }
        this.fNSComponents[var3_3][var1_1] = new XSNamedMap4Types(var2_2, var4_4, var1_1);
        return this.fNSComponents[var3_3][var1_1];
    }

    public XSTypeDefinition getTypeDefinition(String string, String string2) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return (XSTypeDefinition)schemaGrammar.fGlobalTypeDecls.get(string);
    }

    public XSTypeDefinition getTypeDefinition(String string, String string2, String string3) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return schemaGrammar.getGlobalTypeDecl(string, string3);
    }

    public XSAttributeDeclaration getAttributeDeclaration(String string, String string2) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return (XSAttributeDeclaration)schemaGrammar.fGlobalAttrDecls.get(string);
    }

    public XSAttributeDeclaration getAttributeDeclaration(String string, String string2, String string3) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return schemaGrammar.getGlobalAttributeDecl(string, string3);
    }

    public XSElementDeclaration getElementDeclaration(String string, String string2) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return (XSElementDeclaration)schemaGrammar.fGlobalElemDecls.get(string);
    }

    public XSElementDeclaration getElementDeclaration(String string, String string2, String string3) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return schemaGrammar.getGlobalElementDecl(string, string3);
    }

    public XSAttributeGroupDefinition getAttributeGroup(String string, String string2) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return (XSAttributeGroupDefinition)schemaGrammar.fGlobalAttrGrpDecls.get(string);
    }

    public XSAttributeGroupDefinition getAttributeGroup(String string, String string2, String string3) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return schemaGrammar.getGlobalAttributeGroupDecl(string, string3);
    }

    public XSModelGroupDefinition getModelGroupDefinition(String string, String string2) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return (XSModelGroupDefinition)schemaGrammar.fGlobalGroupDecls.get(string);
    }

    public XSModelGroupDefinition getModelGroupDefinition(String string, String string2, String string3) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return schemaGrammar.getGlobalGroupDecl(string, string3);
    }

    public XSNotationDeclaration getNotationDeclaration(String string, String string2) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return (XSNotationDeclaration)schemaGrammar.fGlobalNotationDecls.get(string);
    }

    public XSNotationDeclaration getNotationDeclaration(String string, String string2, String string3) {
        SchemaGrammar schemaGrammar = (SchemaGrammar)this.fGrammarMap.get(XSModelImpl.null2EmptyString(string2));
        if (schemaGrammar == null) {
            return null;
        }
        return schemaGrammar.getGlobalNotationDecl(string, string3);
    }

    public synchronized XSObjectList getAnnotations() {
        if (this.fAnnotations != null) {
            return this.fAnnotations;
        }
        int n = 0;
        for (int i = 0; i < this.fGrammarCount; ++i) {
            n+=this.fGrammarList[i].fNumAnnotations;
        }
        if (n == 0) {
            this.fAnnotations = XSObjectListImpl.EMPTY_LIST;
            return this.fAnnotations;
        }
        XSObject[] arrxSObject = new XSAnnotationImpl[n];
        int n2 = 0;
        for (int j = 0; j < this.fGrammarCount; ++j) {
            SchemaGrammar schemaGrammar = this.fGrammarList[j];
            if (schemaGrammar.fNumAnnotations <= 0) continue;
            System.arraycopy(schemaGrammar.fAnnotations, 0, arrxSObject, n2, schemaGrammar.fNumAnnotations);
            n2+=schemaGrammar.fNumAnnotations;
        }
        this.fAnnotations = new XSObjectListImpl(arrxSObject, arrxSObject.length);
        return this.fAnnotations;
    }

    private static final String null2EmptyString(String string) {
        return string == null ? XMLSymbols.EMPTY_STRING : string;
    }

    public boolean hasIDConstraints() {
        return this.fHasIDC;
    }

    public XSObjectList getSubstitutionGroup(XSElementDeclaration xSElementDeclaration) {
        return (XSObjectList)this.fSubGroupMap.get(xSElementDeclaration);
    }

    public int getLength() {
        return this.fGrammarCount;
    }

    public XSNamespaceItem item(int n) {
        if (n < 0 || n >= this.fGrammarCount) {
            return null;
        }
        return this.fGrammarList[n];
    }

    public Object get(int n) {
        if (n >= 0 && n < this.fGrammarCount) {
            return this.fGrammarList[n];
        }
        throw new IndexOutOfBoundsException("Index: " + n);
    }

    public int size() {
        return this.getLength();
    }

    public Iterator iterator() {
        return this.listIterator0(0);
    }

    public ListIterator listIterator() {
        return this.listIterator0(0);
    }

    public ListIterator listIterator(int n) {
        if (n >= 0 && n < this.fGrammarCount) {
            return this.listIterator0(n);
        }
        throw new IndexOutOfBoundsException("Index: " + n);
    }

    private ListIterator listIterator0(int n) {
        return new XSNamespaceItemListIterator(n);
    }

    public Object[] toArray() {
        Object[] arrobject = new Object[this.fGrammarCount];
        this.toArray0(arrobject);
        return arrobject;
    }

    public Object[] toArray(Object[] arrobject) {
        if (arrobject.length < this.fGrammarCount) {
            Class class_ = arrobject.getClass();
            Class class_2 = class_.getComponentType();
            arrobject = (Object[])Array.newInstance(class_2, this.fGrammarCount);
        }
        this.toArray0(arrobject);
        if (arrobject.length > this.fGrammarCount) {
            arrobject[this.fGrammarCount] = null;
        }
        return arrobject;
    }

    private void toArray0(Object[] arrobject) {
        if (this.fGrammarCount > 0) {
            System.arraycopy(this.fGrammarList, 0, arrobject, 0, this.fGrammarCount);
        }
    }

    private final class XSNamespaceItemListIterator
    implements ListIterator {
        private int index;

        public XSNamespaceItemListIterator(int n) {
            this.index = n;
        }

        public boolean hasNext() {
            return this.index < XSModelImpl.this.fGrammarCount;
        }

        public Object next() {
            if (this.index < XSModelImpl.this.fGrammarCount) {
                return XSModelImpl.this.fGrammarList[this.index++];
            }
            throw new NoSuchElementException();
        }

        public boolean hasPrevious() {
            return this.index > 0;
        }

        public Object previous() {
            if (this.index > 0) {
                return XSModelImpl.this.fGrammarList[--this.index];
            }
            throw new NoSuchElementException();
        }

        public int nextIndex() {
            return this.index;
        }

        public int previousIndex() {
            return this.index - 1;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public void set(Object object) {
            throw new UnsupportedOperationException();
        }

        public void add(Object object) {
            throw new UnsupportedOperationException();
        }
    }

}

