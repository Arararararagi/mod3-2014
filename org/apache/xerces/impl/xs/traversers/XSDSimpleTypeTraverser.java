/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.traversers;

import java.util.ArrayList;
import java.util.Vector;
import org.apache.xerces.impl.dv.InvalidDatatypeFacetException;
import org.apache.xerces.impl.dv.SchemaDVFactory;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.XSFacets;
import org.apache.xerces.impl.dv.XSSimpleType;
import org.apache.xerces.impl.dv.xs.XSSimpleTypeDecl;
import org.apache.xerces.impl.validation.ValidationState;
import org.apache.xerces.impl.xs.SchemaGrammar;
import org.apache.xerces.impl.xs.SchemaNamespaceSupport;
import org.apache.xerces.impl.xs.SchemaSymbols;
import org.apache.xerces.impl.xs.XSAnnotationImpl;
import org.apache.xerces.impl.xs.traversers.XSAttributeChecker;
import org.apache.xerces.impl.xs.traversers.XSDAbstractTraverser;
import org.apache.xerces.impl.xs.traversers.XSDHandler;
import org.apache.xerces.impl.xs.traversers.XSDocumentInfo;
import org.apache.xerces.impl.xs.util.XInt;
import org.apache.xerces.impl.xs.util.XSObjectListImpl;
import org.apache.xerces.util.DOMUtil;
import org.apache.xerces.xni.NamespaceContext;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;
import org.w3c.dom.Element;

class XSDSimpleTypeTraverser
extends XSDAbstractTraverser {
    private boolean fIsBuiltIn = false;

    XSDSimpleTypeTraverser(XSDHandler xSDHandler, XSAttributeChecker xSAttributeChecker) {
        super(xSDHandler, xSAttributeChecker);
    }

    XSSimpleType traverseGlobal(Element element, XSDocumentInfo xSDocumentInfo, SchemaGrammar schemaGrammar) {
        Object[] arrobject = this.fAttrChecker.checkAttributes(element, true, xSDocumentInfo);
        String string = (String)arrobject[XSAttributeChecker.ATTIDX_NAME];
        if (string == null) {
            arrobject[XSAttributeChecker.ATTIDX_NAME] = "(no name)";
        }
        XSSimpleType xSSimpleType = this.traverseSimpleTypeDecl(element, arrobject, xSDocumentInfo, schemaGrammar);
        this.fAttrChecker.returnAttrArray(arrobject, xSDocumentInfo);
        if (string == null) {
            this.reportSchemaError("s4s-att-must-appear", new Object[]{SchemaSymbols.ELT_SIMPLETYPE, SchemaSymbols.ATT_NAME}, element);
            xSSimpleType = null;
        }
        if (xSSimpleType != null) {
            if (schemaGrammar.getGlobalTypeDecl(xSSimpleType.getName()) == null) {
                schemaGrammar.addGlobalSimpleTypeDecl(xSSimpleType);
            }
            String string2 = this.fSchemaHandler.schemaDocument2SystemId(xSDocumentInfo);
            XSTypeDefinition xSTypeDefinition = schemaGrammar.getGlobalTypeDecl(xSSimpleType.getName(), string2);
            if (xSTypeDefinition == null) {
                schemaGrammar.addGlobalSimpleTypeDecl(xSSimpleType, string2);
            }
            if (this.fSchemaHandler.fTolerateDuplicates) {
                if (xSTypeDefinition != null && xSTypeDefinition instanceof XSSimpleType) {
                    xSSimpleType = (XSSimpleType)xSTypeDefinition;
                }
                this.fSchemaHandler.addGlobalTypeDecl(xSSimpleType);
            }
        }
        return xSSimpleType;
    }

    XSSimpleType traverseLocal(Element element, XSDocumentInfo xSDocumentInfo, SchemaGrammar schemaGrammar) {
        Object[] arrobject = this.fAttrChecker.checkAttributes(element, false, xSDocumentInfo);
        String string = this.genAnonTypeName(element);
        XSSimpleType xSSimpleType = this.getSimpleType(string, element, arrobject, xSDocumentInfo, schemaGrammar);
        if (xSSimpleType instanceof XSSimpleTypeDecl) {
            ((XSSimpleTypeDecl)xSSimpleType).setAnonymous(true);
        }
        this.fAttrChecker.returnAttrArray(arrobject, xSDocumentInfo);
        return xSSimpleType;
    }

    private XSSimpleType traverseSimpleTypeDecl(Element element, Object[] arrobject, XSDocumentInfo xSDocumentInfo, SchemaGrammar schemaGrammar) {
        String string = (String)arrobject[XSAttributeChecker.ATTIDX_NAME];
        return this.getSimpleType(string, element, arrobject, xSDocumentInfo, schemaGrammar);
    }

    private String genAnonTypeName(Element element) {
        StringBuffer stringBuffer = new StringBuffer("#AnonType_");
        Element element2 = DOMUtil.getParent(element);
        while (element2 != null && element2 != DOMUtil.getRoot(DOMUtil.getDocument(element2))) {
            stringBuffer.append(element2.getAttribute(SchemaSymbols.ATT_NAME));
            element2 = DOMUtil.getParent(element2);
        }
        return stringBuffer.toString();
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    private XSSimpleType getSimpleType(String var1_1, Element var2_2, Object[] var3_3, XSDocumentInfo var4_4, SchemaGrammar var5_5) {
        var6_6 = (XInt)var3_3[XSAttributeChecker.ATTIDX_FINAL];
        var7_7 = var6_6 == null ? var4_4.fFinalDefault : var6_6.intValue();
        var8_8 = DOMUtil.getFirstChildElement(var2_2);
        var9_9 = null;
        if (var8_8 != null && DOMUtil.getLocalName(var8_8).equals(SchemaSymbols.ELT_ANNOTATION)) {
            var10_10 = this.traverseAnnotationDecl(var8_8, var3_3, false, var4_4);
            if (var10_10 != null) {
                var9_9 = new XSAnnotationImpl[]{var10_10};
            }
            var8_8 = DOMUtil.getNextSiblingElement(var8_8);
        } else {
            var10_10 = DOMUtil.getSyntheticAnnotation(var2_2);
            if (var10_10 != null) {
                var11_11 = this.traverseSyntheticAnnotation(var2_2, (String)var10_10, var3_3, false, var4_4);
                var9_9 = new XSAnnotationImpl[]{var11_11};
            }
        }
        if (var8_8 == null) {
            this.reportSchemaError("s4s-elt-must-match.2", new Object[]{SchemaSymbols.ELT_SIMPLETYPE, "(annotation?, (restriction | list | union))"}, var2_2);
            return this.errorType(var1_1, var4_4.fTargetNamespace, 2);
        }
        var10_10 = DOMUtil.getLocalName(var8_8);
        var11_12 = 2;
        var12_13 = false;
        var13_14 = false;
        var14_15 = false;
        if (var10_10.equals(SchemaSymbols.ELT_RESTRICTION)) {
            var11_12 = 2;
            var12_13 = true;
        } else if (var10_10.equals(SchemaSymbols.ELT_LIST)) {
            var11_12 = 16;
            var13_14 = true;
        } else {
            if (!var10_10.equals(SchemaSymbols.ELT_UNION)) {
                this.reportSchemaError("s4s-elt-must-match.1", new Object[]{SchemaSymbols.ELT_SIMPLETYPE, "(annotation?, (restriction | list | union))", var10_10}, var2_2);
                return this.errorType(var1_1, var4_4.fTargetNamespace, 2);
            }
            var11_12 = 8;
            var14_15 = true;
        }
        var15_16 = DOMUtil.getNextSiblingElement(var8_8);
        if (var15_16 != null) {
            this.reportSchemaError("s4s-elt-must-match.1", new Object[]{SchemaSymbols.ELT_SIMPLETYPE, "(annotation?, (restriction | list | union))", DOMUtil.getLocalName(var15_16)}, var15_16);
        }
        var16_17 = this.fAttrChecker.checkAttributes(var8_8, false, var4_4);
        var17_18 = (QName)var16_17[var12_13 != false ? XSAttributeChecker.ATTIDX_BASE : XSAttributeChecker.ATTIDX_ITEMTYPE];
        var18_19 = (Vector)var16_17[XSAttributeChecker.ATTIDX_MEMBERTYPES];
        var19_20 = DOMUtil.getFirstChildElement(var8_8);
        if (var19_20 != null && DOMUtil.getLocalName(var19_20).equals(SchemaSymbols.ELT_ANNOTATION)) {
            var20_21 = this.traverseAnnotationDecl(var19_20, var16_17, false, var4_4);
            if (var20_21 != null) {
                if (var9_9 == null) {
                    var9_9 = new XSAnnotationImpl[]{var20_21};
                } else {
                    var21_22 = new XSAnnotationImpl[2];
                    var21_22[0] = var9_9[0];
                    var9_9 = var21_22;
                    var9_9[1] = var20_21;
                }
            }
            var19_20 = DOMUtil.getNextSiblingElement(var19_20);
        } else {
            var20_21 = DOMUtil.getSyntheticAnnotation(var8_8);
            if (var20_21 != null) {
                var21_22 = this.traverseSyntheticAnnotation(var8_8, (String)var20_21, var16_17, false, var4_4);
                if (var9_9 == null) {
                    var9_9 = new XSAnnotationImpl[]{var21_22};
                } else {
                    var22_23 = new XSAnnotationImpl[2];
                    var22_23[0] = var9_9[0];
                    var9_9 = var22_23;
                    var9_9[1] = var21_22;
                }
            }
        }
        var20_21 = null;
        if ((var12_13 || var13_14) && var17_18 != null && (var20_21 = this.findDTValidator(var8_8, var1_1, var17_18, var11_12, var4_4)) == null && this.fIsBuiltIn) {
            this.fIsBuiltIn = false;
            return null;
        }
        var21_22 = null;
        var22_23 = null;
        if (!var14_15 || var18_19 == null || var18_19.size() <= 0) ** GOTO lbl87
        var24_24 = var18_19.size();
        var21_22 = new ArrayList<E>(var24_24);
        for (var25_26 = 0; var25_26 < var24_24; ++var25_26) {
            var22_23 = this.findDTValidator(var8_8, var1_1, (QName)var18_19.elementAt(var25_26), 8, var4_4);
            if (var22_23 == null) continue;
            if (var22_23.getVariety() != 3) ** GOTO lbl81
            var23_29 = var22_23.getMemberTypes();
            var26_30 = 0;
            ** GOTO lbl85
lbl81: // 1 sources:
            var21_22.add(var22_23);
            continue;
lbl-1000: // 1 sources:
            {
                var21_22.add(var23_29.item(var26_30));
                ++var26_30;
lbl85: // 2 sources:
                ** while (var26_30 < var23_29.getLength())
            }
lbl86: // 1 sources:
        }
lbl87: // 2 sources:
        if (var19_20 == null || !DOMUtil.getLocalName(var19_20).equals(SchemaSymbols.ELT_SIMPLETYPE)) ** GOTO lbl99
        if (!var12_13 && !var13_14) ** GOTO lbl95
        if (var17_18 != null) {
            this.reportSchemaError(var13_14 != false ? "src-simple-type.3.a" : "src-simple-type.2.a", null, var19_20);
        }
        if (var20_21 == null) {
            var20_21 = this.traverseLocal(var19_20, var4_4, var5_5);
        }
        var19_20 = DOMUtil.getNextSiblingElement(var19_20);
        ** GOTO lbl117
lbl95: // 1 sources:
        if (!var14_15) ** GOTO lbl117
        if (var21_22 != null) ** GOTO lbl105
        var21_22 = new ArrayList<Object>(2);
        ** GOTO lbl105
lbl99: // 1 sources:
        if ((var12_13 || var13_14) && var17_18 == null) {
            this.reportSchemaError(var13_14 != false ? "src-simple-type.3.b" : "src-simple-type.2.b", null, var8_8);
        } else if (var14_15 && (var18_19 == null || var18_19.size() == 0)) {
            this.reportSchemaError("src-union-memberTypes-or-simpleTypes", null, var8_8);
        } else {
            ** GOTO lbl117
        }
        ** GOTO lbl117
lbl105: // 2 sources:
        do {
            if ((var22_23 = this.traverseLocal(var19_20, var4_4, var5_5)) == null) continue;
            if (var22_23.getVariety() != 3) ** GOTO lbl111
            var23_29 = var22_23.getMemberTypes();
            var24_24 = 0;
            ** GOTO lbl115
lbl111: // 1 sources:
            var21_22.add(var22_23);
            continue;
lbl-1000: // 1 sources:
            {
                var21_22.add(var23_29.item(var24_24));
                ++var24_24;
lbl115: // 2 sources:
                ** while (var24_24 < var23_29.getLength())
            }
lbl116: // 3 sources:
        } while ((var19_20 = DOMUtil.getNextSiblingElement(var19_20)) != null && DOMUtil.getLocalName(var19_20).equals(SchemaSymbols.ELT_SIMPLETYPE));
lbl117: // 7 sources:
        if ((var12_13 || var13_14) && var20_21 == null) {
            this.fAttrChecker.returnAttrArray(var16_17, var4_4);
            if (var12_13) {
                v0 = 2;
                return this.errorType(var1_1, var4_4.fTargetNamespace, v0);
            }
            v0 = 16;
            return this.errorType(var1_1, var4_4.fTargetNamespace, v0);
        }
        if (var14_15 && (var21_22 == null || var21_22.size() == 0)) {
            this.fAttrChecker.returnAttrArray(var16_17, var4_4);
            return this.errorType(var1_1, var4_4.fTargetNamespace, 8);
        }
        if (var13_14 && this.isListDatatype((XSSimpleType)var20_21)) {
            this.reportSchemaError("cos-st-restricts.2.1", new Object[]{var1_1, var20_21.getName()}, var8_8);
            this.fAttrChecker.returnAttrArray(var16_17, var4_4);
            return this.errorType(var1_1, var4_4.fTargetNamespace, 16);
        }
        var24_25 = null;
        if (var12_13) {
            var24_25 = this.fSchemaHandler.fDVFactory.createTypeRestriction(var1_1, var4_4.fTargetNamespace, (short)var7_7, (XSSimpleType)var20_21, var9_9 == null ? null : new XSObjectListImpl(var9_9, var9_9.length));
        } else if (var13_14) {
            var24_25 = this.fSchemaHandler.fDVFactory.createTypeList(var1_1, var4_4.fTargetNamespace, (short)var7_7, (XSSimpleType)var20_21, var9_9 == null ? null : new XSObjectListImpl(var9_9, var9_9.length));
        } else if (var14_15) {
            var25_27 = var21_22.toArray(new XSSimpleType[var21_22.size()]);
            var24_25 = this.fSchemaHandler.fDVFactory.createTypeUnion(var1_1, var4_4.fTargetNamespace, (short)var7_7, var25_27, var9_9 == null ? null : new XSObjectListImpl(var9_9, var9_9.length));
        }
        if (var12_13 && var19_20 != null) {
            var25_28 = this.traverseFacets(var19_20, (XSSimpleType)var20_21, var4_4);
            var19_20 = var25_28.nodeAfterFacets;
            try {
                this.fValidationState.setNamespaceSupport(var4_4.fNamespaceSupport);
                var24_25.applyFacets(var25_28.facetdata, var25_28.fPresentFacets, var25_28.fFixedFacets, this.fValidationState);
            }
            catch (InvalidDatatypeFacetException var26_31) {
                this.reportSchemaError(var26_31.getKey(), var26_31.getArgs(), var8_8);
                var24_25 = this.fSchemaHandler.fDVFactory.createTypeRestriction(var1_1, var4_4.fTargetNamespace, (short)var7_7, (XSSimpleType)var20_21, var9_9 == null ? null : new XSObjectListImpl(var9_9, var9_9.length));
            }
        }
        if (var19_20 != null) {
            if (var12_13) {
                this.reportSchemaError("s4s-elt-must-match.1", new Object[]{SchemaSymbols.ELT_RESTRICTION, "(annotation?, (simpleType?, (minExclusive | minInclusive | maxExclusive | maxInclusive | totalDigits | fractionDigits | length | minLength | maxLength | enumeration | whiteSpace | pattern)*))", DOMUtil.getLocalName(var19_20)}, var19_20);
            } else if (var13_14) {
                this.reportSchemaError("s4s-elt-must-match.1", new Object[]{SchemaSymbols.ELT_LIST, "(annotation?, (simpleType?))", DOMUtil.getLocalName(var19_20)}, var19_20);
            } else if (var14_15) {
                this.reportSchemaError("s4s-elt-must-match.1", new Object[]{SchemaSymbols.ELT_UNION, "(annotation?, (simpleType*))", DOMUtil.getLocalName(var19_20)}, var19_20);
            }
        }
        this.fAttrChecker.returnAttrArray(var16_17, var4_4);
        return var24_25;
    }

    private XSSimpleType findDTValidator(Element element, String string, QName qName, short s, XSDocumentInfo xSDocumentInfo) {
        if (qName == null) {
            return null;
        }
        XSTypeDefinition xSTypeDefinition = (XSTypeDefinition)this.fSchemaHandler.getGlobalDecl(xSDocumentInfo, 7, qName, element);
        if (xSTypeDefinition == null) {
            return null;
        }
        if (xSTypeDefinition.getTypeCategory() != 16) {
            this.reportSchemaError("cos-st-restricts.1.1", new Object[]{qName.rawname, string}, element);
            return null;
        }
        if (xSTypeDefinition == SchemaGrammar.fAnySimpleType && s == 2) {
            if (this.checkBuiltIn(string, xSDocumentInfo.fTargetNamespace)) {
                return null;
            }
            this.reportSchemaError("cos-st-restricts.1.1", new Object[]{qName.rawname, string}, element);
            return null;
        }
        if ((xSTypeDefinition.getFinal() & s) != 0) {
            if (s == 2) {
                this.reportSchemaError("st-props-correct.3", new Object[]{string, qName.rawname}, element);
            } else if (s == 16) {
                this.reportSchemaError("cos-st-restricts.2.3.1.1", new Object[]{qName.rawname, string}, element);
            } else if (s == 8) {
                this.reportSchemaError("cos-st-restricts.3.3.1.1", new Object[]{qName.rawname, string}, element);
            }
            return null;
        }
        return (XSSimpleType)xSTypeDefinition;
    }

    private final boolean checkBuiltIn(String string, String string2) {
        if (string2 != SchemaSymbols.URI_SCHEMAFORSCHEMA) {
            return false;
        }
        if (SchemaGrammar.SG_SchemaNS.getGlobalTypeDecl(string) != null) {
            this.fIsBuiltIn = true;
        }
        return this.fIsBuiltIn;
    }

    private boolean isListDatatype(XSSimpleType xSSimpleType) {
        if (xSSimpleType.getVariety() == 2) {
            return true;
        }
        if (xSSimpleType.getVariety() == 3) {
            XSObjectList xSObjectList = xSSimpleType.getMemberTypes();
            for (int i = 0; i < xSObjectList.getLength(); ++i) {
                if (((XSSimpleType)xSObjectList.item(i)).getVariety() != 2) continue;
                return true;
            }
        }
        return false;
    }

    private XSSimpleType errorType(String string, String string2, short s) {
        XSSimpleType xSSimpleType = (XSSimpleType)SchemaGrammar.SG_SchemaNS.getTypeDefinition("string");
        switch (s) {
            case 2: {
                return this.fSchemaHandler.fDVFactory.createTypeRestriction(string, string2, 0, xSSimpleType, null);
            }
            case 16: {
                return this.fSchemaHandler.fDVFactory.createTypeList(string, string2, 0, xSSimpleType, null);
            }
            case 8: {
                return this.fSchemaHandler.fDVFactory.createTypeUnion(string, string2, 0, new XSSimpleType[]{xSSimpleType}, null);
            }
        }
        return null;
    }
}

