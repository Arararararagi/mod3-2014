/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.traversers;

import org.apache.xerces.impl.dv.InvalidDatatypeFacetException;
import org.apache.xerces.impl.dv.SchemaDVFactory;
import org.apache.xerces.impl.dv.ValidationContext;
import org.apache.xerces.impl.dv.XSFacets;
import org.apache.xerces.impl.dv.XSSimpleType;
import org.apache.xerces.impl.dv.xs.XSSimpleTypeDecl;
import org.apache.xerces.impl.validation.ValidationState;
import org.apache.xerces.impl.xs.SchemaGrammar;
import org.apache.xerces.impl.xs.SchemaNamespaceSupport;
import org.apache.xerces.impl.xs.SchemaSymbols;
import org.apache.xerces.impl.xs.XSAnnotationImpl;
import org.apache.xerces.impl.xs.XSAttributeDecl;
import org.apache.xerces.impl.xs.XSAttributeGroupDecl;
import org.apache.xerces.impl.xs.XSAttributeUseImpl;
import org.apache.xerces.impl.xs.XSComplexTypeDecl;
import org.apache.xerces.impl.xs.XSConstraints;
import org.apache.xerces.impl.xs.XSModelGroupImpl;
import org.apache.xerces.impl.xs.XSParticleDecl;
import org.apache.xerces.impl.xs.XSWildcardDecl;
import org.apache.xerces.impl.xs.traversers.XSAttributeChecker;
import org.apache.xerces.impl.xs.traversers.XSDAbstractParticleTraverser;
import org.apache.xerces.impl.xs.traversers.XSDAbstractTraverser;
import org.apache.xerces.impl.xs.traversers.XSDGroupTraverser;
import org.apache.xerces.impl.xs.traversers.XSDHandler;
import org.apache.xerces.impl.xs.traversers.XSDSimpleTypeTraverser;
import org.apache.xerces.impl.xs.traversers.XSDocumentInfo;
import org.apache.xerces.impl.xs.util.SimpleLocator;
import org.apache.xerces.impl.xs.util.XInt;
import org.apache.xerces.impl.xs.util.XSObjectListImpl;
import org.apache.xerces.util.DOMUtil;
import org.apache.xerces.xni.NamespaceContext;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xs.XSAttributeUse;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTerm;
import org.apache.xerces.xs.XSTypeDefinition;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

class XSDComplexTypeTraverser
extends XSDAbstractParticleTraverser {
    private static final int GLOBAL_NUM = 11;
    private static XSParticleDecl fErrorContent = null;
    private static XSWildcardDecl fErrorWildcard = null;
    private String fName = null;
    private String fTargetNamespace = null;
    private short fDerivedBy = 2;
    private short fFinal = 0;
    private short fBlock = 0;
    private short fContentType = 0;
    private XSTypeDefinition fBaseType = null;
    private XSAttributeGroupDecl fAttrGrp = null;
    private XSSimpleType fXSSimpleType = null;
    private XSParticleDecl fParticle = null;
    private boolean fIsAbstract = false;
    private XSComplexTypeDecl fComplexTypeDecl = null;
    private XSAnnotationImpl[] fAnnotations = null;
    private Object[] fGlobalStore = null;
    private int fGlobalStorePos = 0;
    private static final boolean DEBUG = false;

    private static XSParticleDecl getErrorContent() {
        if (fErrorContent == null) {
            XSParticleDecl xSParticleDecl = new XSParticleDecl();
            xSParticleDecl.fType = 2;
            xSParticleDecl.fValue = XSDComplexTypeTraverser.getErrorWildcard();
            xSParticleDecl.fMinOccurs = 0;
            xSParticleDecl.fMaxOccurs = -1;
            XSModelGroupImpl xSModelGroupImpl = new XSModelGroupImpl();
            xSModelGroupImpl.fCompositor = 102;
            xSModelGroupImpl.fParticleCount = 1;
            xSModelGroupImpl.fParticles = new XSParticleDecl[1];
            xSModelGroupImpl.fParticles[0] = xSParticleDecl;
            XSParticleDecl xSParticleDecl2 = new XSParticleDecl();
            xSParticleDecl2.fType = 3;
            xSParticleDecl2.fValue = xSModelGroupImpl;
            fErrorContent = xSParticleDecl2;
        }
        return fErrorContent;
    }

    private static XSWildcardDecl getErrorWildcard() {
        if (fErrorWildcard == null) {
            XSWildcardDecl xSWildcardDecl = new XSWildcardDecl();
            xSWildcardDecl.fProcessContents = 2;
            fErrorWildcard = xSWildcardDecl;
        }
        return fErrorWildcard;
    }

    XSDComplexTypeTraverser(XSDHandler xSDHandler, XSAttributeChecker xSAttributeChecker) {
        super(xSDHandler, xSAttributeChecker);
    }

    XSComplexTypeDecl traverseLocal(Element element, XSDocumentInfo xSDocumentInfo, SchemaGrammar schemaGrammar) {
        Object[] arrobject = this.fAttrChecker.checkAttributes(element, false, xSDocumentInfo);
        String string = this.genAnonTypeName(element);
        this.contentBackup();
        XSComplexTypeDecl xSComplexTypeDecl = this.traverseComplexTypeDecl(element, string, arrobject, xSDocumentInfo, schemaGrammar);
        this.contentRestore();
        schemaGrammar.addComplexTypeDecl(xSComplexTypeDecl, this.fSchemaHandler.element2Locator(element));
        xSComplexTypeDecl.setIsAnonymous();
        this.fAttrChecker.returnAttrArray(arrobject, xSDocumentInfo);
        return xSComplexTypeDecl;
    }

    XSComplexTypeDecl traverseGlobal(Element element, XSDocumentInfo xSDocumentInfo, SchemaGrammar schemaGrammar) {
        Object[] arrobject = this.fAttrChecker.checkAttributes(element, true, xSDocumentInfo);
        String string = (String)arrobject[XSAttributeChecker.ATTIDX_NAME];
        this.contentBackup();
        XSComplexTypeDecl xSComplexTypeDecl = this.traverseComplexTypeDecl(element, string, arrobject, xSDocumentInfo, schemaGrammar);
        this.contentRestore();
        schemaGrammar.addComplexTypeDecl(xSComplexTypeDecl, this.fSchemaHandler.element2Locator(element));
        if (string == null) {
            this.reportSchemaError("s4s-att-must-appear", new Object[]{SchemaSymbols.ELT_COMPLEXTYPE, SchemaSymbols.ATT_NAME}, element);
            xSComplexTypeDecl = null;
        } else {
            if (schemaGrammar.getGlobalTypeDecl(xSComplexTypeDecl.getName()) == null) {
                schemaGrammar.addGlobalComplexTypeDecl(xSComplexTypeDecl);
            }
            String string2 = this.fSchemaHandler.schemaDocument2SystemId(xSDocumentInfo);
            XSTypeDefinition xSTypeDefinition = schemaGrammar.getGlobalTypeDecl(xSComplexTypeDecl.getName(), string2);
            if (xSTypeDefinition == null) {
                schemaGrammar.addGlobalComplexTypeDecl(xSComplexTypeDecl, string2);
            }
            if (this.fSchemaHandler.fTolerateDuplicates) {
                if (xSTypeDefinition != null && xSTypeDefinition instanceof XSComplexTypeDecl) {
                    xSComplexTypeDecl = (XSComplexTypeDecl)xSTypeDefinition;
                }
                this.fSchemaHandler.addGlobalTypeDecl(xSComplexTypeDecl);
            }
        }
        this.fAttrChecker.returnAttrArray(arrobject, xSDocumentInfo);
        return xSComplexTypeDecl;
    }

    private XSComplexTypeDecl traverseComplexTypeDecl(Element element, String string, Object[] arrobject, XSDocumentInfo xSDocumentInfo, SchemaGrammar schemaGrammar) {
        this.fComplexTypeDecl = new XSComplexTypeDecl();
        this.fAttrGrp = new XSAttributeGroupDecl();
        Boolean bl = (Boolean)arrobject[XSAttributeChecker.ATTIDX_ABSTRACT];
        XInt xInt = (XInt)arrobject[XSAttributeChecker.ATTIDX_BLOCK];
        Boolean bl2 = (Boolean)arrobject[XSAttributeChecker.ATTIDX_MIXED];
        XInt xInt2 = (XInt)arrobject[XSAttributeChecker.ATTIDX_FINAL];
        this.fName = string;
        this.fComplexTypeDecl.setName(this.fName);
        this.fTargetNamespace = xSDocumentInfo.fTargetNamespace;
        this.fBlock = xInt == null ? xSDocumentInfo.fBlockDefault : xInt.shortValue();
        this.fFinal = xInt2 == null ? xSDocumentInfo.fFinalDefault : xInt2.shortValue();
        this.fBlock = (short)(this.fBlock & 3);
        this.fFinal = (short)(this.fFinal & 3);
        this.fIsAbstract = bl != null && bl != false;
        this.fAnnotations = null;
        Element element2 = null;
        try {
            Object object;
            element2 = DOMUtil.getFirstChildElement(element);
            if (element2 != null) {
                if (DOMUtil.getLocalName(element2).equals(SchemaSymbols.ELT_ANNOTATION)) {
                    this.addAnnotation(this.traverseAnnotationDecl(element2, arrobject, false, xSDocumentInfo));
                    element2 = DOMUtil.getNextSiblingElement(element2);
                } else {
                    object = DOMUtil.getSyntheticAnnotation(element);
                    if (object != null) {
                        this.addAnnotation(this.traverseSyntheticAnnotation(element, (String)object, arrobject, false, xSDocumentInfo));
                    }
                }
                if (element2 != null && DOMUtil.getLocalName(element2).equals(SchemaSymbols.ELT_ANNOTATION)) {
                    throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, SchemaSymbols.ELT_ANNOTATION}, element2);
                }
            } else {
                object = DOMUtil.getSyntheticAnnotation(element);
                if (object != null) {
                    this.addAnnotation(this.traverseSyntheticAnnotation(element, (String)object, arrobject, false, xSDocumentInfo));
                }
            }
            if (element2 == null) {
                this.fBaseType = SchemaGrammar.fAnyType;
                this.fDerivedBy = 2;
                this.processComplexContent(element2, bl2, false, xSDocumentInfo, schemaGrammar);
            } else if (DOMUtil.getLocalName(element2).equals(SchemaSymbols.ELT_SIMPLECONTENT)) {
                this.traverseSimpleContent(element2, xSDocumentInfo, schemaGrammar);
                object = DOMUtil.getNextSiblingElement(element2);
                if (object != null) {
                    String string2 = DOMUtil.getLocalName((Node)object);
                    throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, string2}, (Element)object);
                }
            } else if (DOMUtil.getLocalName(element2).equals(SchemaSymbols.ELT_COMPLEXCONTENT)) {
                this.traverseComplexContent(element2, bl2, xSDocumentInfo, schemaGrammar);
                object = DOMUtil.getNextSiblingElement(element2);
                if (object != null) {
                    String string3 = DOMUtil.getLocalName((Node)object);
                    throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, string3}, (Element)object);
                }
            } else {
                this.fBaseType = SchemaGrammar.fAnyType;
                this.fDerivedBy = 2;
                this.processComplexContent(element2, bl2, false, xSDocumentInfo, schemaGrammar);
            }
        }
        catch (ComplexTypeRecoverableError var11_12) {
            this.handleComplexTypeError(var11_12.getMessage(), var11_12.errorSubstText, var11_12.errorElem);
        }
        this.fComplexTypeDecl.setValues(this.fName, this.fTargetNamespace, this.fBaseType, this.fDerivedBy, this.fFinal, this.fBlock, this.fContentType, this.fIsAbstract, this.fAttrGrp, this.fXSSimpleType, this.fParticle, new XSObjectListImpl(this.fAnnotations, this.fAnnotations == null ? 0 : this.fAnnotations.length));
        return this.fComplexTypeDecl;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    private void traverseSimpleContent(Element var1_1, XSDocumentInfo var2_2, SchemaGrammar var3_3) throws ComplexTypeRecoverableError {
        var4_4 = this.fAttrChecker.checkAttributes(var1_1, false, var2_2);
        this.fContentType = 1;
        this.fParticle = null;
        var5_5 = DOMUtil.getFirstChildElement(var1_1);
        if (var5_5 != null && DOMUtil.getLocalName(var5_5).equals(SchemaSymbols.ELT_ANNOTATION)) {
            this.addAnnotation(this.traverseAnnotationDecl(var5_5, var4_4, false, var2_2));
            var5_5 = DOMUtil.getNextSiblingElement(var5_5);
        } else {
            var6_6 = DOMUtil.getSyntheticAnnotation(var1_1);
            if (var6_6 != null) {
                this.addAnnotation(this.traverseSyntheticAnnotation(var1_1, var6_6, var4_4, false, var2_2));
            }
        }
        if (var5_5 == null) {
            this.fAttrChecker.returnAttrArray(var4_4, var2_2);
            throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.2", new Object[]{this.fName, SchemaSymbols.ELT_SIMPLECONTENT}, var1_1);
        }
        var6_6 = DOMUtil.getLocalName(var5_5);
        if (var6_6.equals(SchemaSymbols.ELT_RESTRICTION)) {
            this.fDerivedBy = 2;
        } else {
            if (!var6_6.equals(SchemaSymbols.ELT_EXTENSION)) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, var6_6}, var5_5);
            }
            this.fDerivedBy = 1;
        }
        var7_7 = DOMUtil.getNextSiblingElement(var5_5);
        if (var7_7 != null) {
            this.fAttrChecker.returnAttrArray(var4_4, var2_2);
            var8_8 = DOMUtil.getLocalName(var7_7);
            throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, var8_8}, var7_7);
        }
        var8_9 = this.fAttrChecker.checkAttributes(var5_5, false, var2_2);
        var9_10 = (QName)var8_9[XSAttributeChecker.ATTIDX_BASE];
        if (var9_10 == null) {
            this.fAttrChecker.returnAttrArray(var4_4, var2_2);
            this.fAttrChecker.returnAttrArray(var8_9, var2_2);
            throw new ComplexTypeRecoverableError("s4s-att-must-appear", new Object[]{var6_6, "base"}, var5_5);
        }
        var10_11 = (XSTypeDefinition)this.fSchemaHandler.getGlobalDecl(var2_2, 7, var9_10, var5_5);
        if (var10_11 == null) {
            this.fAttrChecker.returnAttrArray(var4_4, var2_2);
            this.fAttrChecker.returnAttrArray(var8_9, var2_2);
            throw new ComplexTypeRecoverableError();
        }
        this.fBaseType = var10_11;
        var11_12 = null;
        var12_13 = null;
        var13_14 = 0;
        if (var10_11.getTypeCategory() == 15) {
            var12_13 = (XSComplexTypeDecl)var10_11;
            var13_14 = var12_13.getFinal();
            if (var12_13.getContentType() == 1) {
                var11_12 = (XSSimpleType)var12_13.getSimpleType();
            } else if (!(this.fDerivedBy == 2 && var12_13.getContentType() == 3 && ((XSParticleDecl)var12_13.getParticle()).emptiable())) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw new ComplexTypeRecoverableError("src-ct.2.1", new Object[]{this.fName, var12_13.getName()}, var5_5);
            }
        } else {
            var11_12 = (XSSimpleType)var10_11;
            if (this.fDerivedBy == 2) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw new ComplexTypeRecoverableError("src-ct.2.1", new Object[]{this.fName, var11_12.getName()}, var5_5);
            }
            var13_14 = var11_12.getFinal();
        }
        if ((var13_14 & this.fDerivedBy) != 0) {
            this.fAttrChecker.returnAttrArray(var4_4, var2_2);
            this.fAttrChecker.returnAttrArray(var8_9, var2_2);
            var14_15 = this.fDerivedBy == 1 ? "cos-ct-extends.1.1" : "derivation-ok-restriction.1";
            throw new ComplexTypeRecoverableError(var14_15, new Object[]{this.fName, this.fBaseType.getName()}, var5_5);
        }
        var14_16 = var5_5;
        if ((var5_5 = DOMUtil.getFirstChildElement(var5_5)) != null) {
            if (DOMUtil.getLocalName(var5_5).equals(SchemaSymbols.ELT_ANNOTATION)) {
                this.addAnnotation(this.traverseAnnotationDecl(var5_5, var8_9, false, var2_2));
                var5_5 = DOMUtil.getNextSiblingElement(var5_5);
            } else {
                var15_17 = DOMUtil.getSyntheticAnnotation(var14_16);
                if (var15_17 != null) {
                    this.addAnnotation(this.traverseSyntheticAnnotation(var14_16, (String)var15_17, var8_9, false, var2_2));
                }
            }
            if (var5_5 != null && DOMUtil.getLocalName(var5_5).equals(SchemaSymbols.ELT_ANNOTATION)) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, SchemaSymbols.ELT_ANNOTATION}, var5_5);
            }
        } else {
            var15_17 = DOMUtil.getSyntheticAnnotation(var14_16);
            if (var15_17 != null) {
                this.addAnnotation(this.traverseSyntheticAnnotation(var14_16, (String)var15_17, var8_9, false, var2_2));
            }
        }
        if (this.fDerivedBy == 2) {
            if (var5_5 != null && DOMUtil.getLocalName(var5_5).equals(SchemaSymbols.ELT_SIMPLETYPE)) {
                var15_17 = this.fSchemaHandler.fSimpleTypeTraverser.traverseLocal(var5_5, var2_2, var3_3);
                if (var15_17 == null) {
                    this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                    this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                    throw new ComplexTypeRecoverableError();
                }
                if (!(var11_12 == null || XSConstraints.checkSimpleDerivationOk((XSSimpleType)var15_17, (XSTypeDefinition)var11_12, var11_12.getFinal()))) {
                    this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                    this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                    throw new ComplexTypeRecoverableError("derivation-ok-restriction.5.2.2.1", new Object[]{this.fName, var15_17.getName(), var11_12.getName()}, var5_5);
                }
                var11_12 = var15_17;
                var5_5 = DOMUtil.getNextSiblingElement(var5_5);
            }
            if (var11_12 == null) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw new ComplexTypeRecoverableError("src-ct.2.2", new Object[]{this.fName}, var5_5);
            }
            var15_17 = null;
            var16_19 = null;
            var17_21 = 0;
            var18_22 = 0;
            if (var5_5 != null) {
                var19_23 = this.traverseFacets(var5_5, (XSSimpleType)var11_12, var2_2);
                var15_17 = var19_23.nodeAfterFacets;
                var16_19 = var19_23.facetdata;
                var17_21 = var19_23.fPresentFacets;
                var18_22 = var19_23.fFixedFacets;
            }
            var19_23 = this.genAnonTypeName(var1_1);
            this.fXSSimpleType = this.fSchemaHandler.fDVFactory.createTypeRestriction((String)var19_23, var2_2.fTargetNamespace, 0, (XSSimpleType)var11_12, null);
            try {
                this.fValidationState.setNamespaceSupport(var2_2.fNamespaceSupport);
                this.fXSSimpleType.applyFacets(var16_19, var17_21, var18_22, this.fValidationState);
            }
            catch (InvalidDatatypeFacetException var20_24) {
                this.reportSchemaError(var20_24.getKey(), var20_24.getArgs(), var5_5);
                this.fXSSimpleType = this.fSchemaHandler.fDVFactory.createTypeRestriction((String)var19_23, var2_2.fTargetNamespace, 0, (XSSimpleType)var11_12, null);
            }
            if (this.fXSSimpleType instanceof XSSimpleTypeDecl) {
                ((XSSimpleTypeDecl)this.fXSSimpleType).setAnonymous(true);
            }
            if (var15_17 != null) {
                if (!this.isAttrOrAttrGroup((Element)var15_17)) {
                    this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                    this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                    throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, DOMUtil.getLocalName((Node)var15_17)}, (Element)var15_17);
                }
                var20_25 = this.traverseAttrsAndAttrGrps((Element)var15_17, this.fAttrGrp, var2_2, var3_3, this.fComplexTypeDecl);
                if (var20_25 != null) {
                    this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                    this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                    throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, DOMUtil.getLocalName((Node)var20_25)}, (Element)var20_25);
                }
            }
            try {
                this.mergeAttributes(var12_13.getAttrGrp(), this.fAttrGrp, this.fName, false, var1_1);
            }
            catch (ComplexTypeRecoverableError var20_26) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw var20_26;
            }
            this.fAttrGrp.removeProhibitedAttrs();
            var20_25 = this.fAttrGrp.validRestrictionOf(this.fName, var12_13.getAttrGrp());
            if (var20_25 != null) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw new ComplexTypeRecoverableError((String)var20_25[var20_25.length - 1], var20_25, (Element)var15_17);
            } else {
                ** GOTO lbl162
            }
        }
        this.fXSSimpleType = var11_12;
        if (var5_5 != null) {
            var15_17 = var5_5;
            if (!this.isAttrOrAttrGroup((Element)var15_17)) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, DOMUtil.getLocalName((Node)var15_17)}, (Element)var15_17);
            }
            var16_20 = this.traverseAttrsAndAttrGrps((Element)var15_17, this.fAttrGrp, var2_2, var3_3, this.fComplexTypeDecl);
            if (var16_20 != null) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, DOMUtil.getLocalName(var16_20)}, var16_20);
            }
            this.fAttrGrp.removeProhibitedAttrs();
        }
        if (var12_13 != null) {
            try {
                this.mergeAttributes(var12_13.getAttrGrp(), this.fAttrGrp, this.fName, true, var1_1);
            }
            catch (ComplexTypeRecoverableError var15_18) {
                this.fAttrChecker.returnAttrArray(var4_4, var2_2);
                this.fAttrChecker.returnAttrArray(var8_9, var2_2);
                throw var15_18;
            }
        }
lbl162: // 5 sources:
        this.fAttrChecker.returnAttrArray(var4_4, var2_2);
        this.fAttrChecker.returnAttrArray(var8_9, var2_2);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    private void traverseComplexContent(Element var1_1, boolean var2_2, XSDocumentInfo var3_3, SchemaGrammar var4_4) throws ComplexTypeRecoverableError {
        var5_5 = this.fAttrChecker.checkAttributes(var1_1, false, var3_3);
        var6_6 = var2_2;
        var7_7 = (Boolean)var5_5[XSAttributeChecker.ATTIDX_MIXED];
        if (var7_7 != null) {
            var6_6 = var7_7;
        }
        this.fXSSimpleType = null;
        var8_8 = DOMUtil.getFirstChildElement(var1_1);
        if (var8_8 != null && DOMUtil.getLocalName(var8_8).equals(SchemaSymbols.ELT_ANNOTATION)) {
            this.addAnnotation(this.traverseAnnotationDecl(var8_8, var5_5, false, var3_3));
            var8_8 = DOMUtil.getNextSiblingElement(var8_8);
        } else {
            var9_9 = DOMUtil.getSyntheticAnnotation(var1_1);
            if (var9_9 != null) {
                this.addAnnotation(this.traverseSyntheticAnnotation(var1_1, var9_9, var5_5, false, var3_3));
            }
        }
        if (var8_8 == null) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.2", new Object[]{this.fName, SchemaSymbols.ELT_COMPLEXCONTENT}, var1_1);
        }
        var9_9 = DOMUtil.getLocalName(var8_8);
        if (var9_9.equals(SchemaSymbols.ELT_RESTRICTION)) {
            this.fDerivedBy = 2;
        } else {
            if (!var9_9.equals(SchemaSymbols.ELT_EXTENSION)) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, var9_9}, var8_8);
            }
            this.fDerivedBy = 1;
        }
        var10_10 = DOMUtil.getNextSiblingElement(var8_8);
        if (var10_10 != null) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            var11_11 = DOMUtil.getLocalName(var10_10);
            throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, var11_11}, var10_10);
        }
        var11_12 = this.fAttrChecker.checkAttributes(var8_8, false, var3_3);
        var12_13 = (QName)var11_12[XSAttributeChecker.ATTIDX_BASE];
        if (var12_13 == null) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            this.fAttrChecker.returnAttrArray(var11_12, var3_3);
            throw new ComplexTypeRecoverableError("s4s-att-must-appear", new Object[]{var9_9, "base"}, var8_8);
        }
        var13_14 = (XSTypeDefinition)this.fSchemaHandler.getGlobalDecl(var3_3, 7, var12_13, var8_8);
        if (var13_14 == null) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            this.fAttrChecker.returnAttrArray(var11_12, var3_3);
            throw new ComplexTypeRecoverableError();
        }
        if (!(var13_14 instanceof XSComplexTypeDecl)) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            this.fAttrChecker.returnAttrArray(var11_12, var3_3);
            throw new ComplexTypeRecoverableError("src-ct.1", new Object[]{this.fName, var13_14.getName()}, var8_8);
        }
        this.fBaseType = var14_15 = (XSComplexTypeDecl)var13_14;
        if ((var14_15.getFinal() & this.fDerivedBy) != 0) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            this.fAttrChecker.returnAttrArray(var11_12, var3_3);
            var15_16 = this.fDerivedBy == 1 ? "cos-ct-extends.1.1" : "derivation-ok-restriction.1";
            throw new ComplexTypeRecoverableError(var15_16, new Object[]{this.fName, this.fBaseType.getName()}, var8_8);
        }
        if ((var8_8 = DOMUtil.getFirstChildElement(var8_8)) != null) {
            if (DOMUtil.getLocalName(var8_8).equals(SchemaSymbols.ELT_ANNOTATION)) {
                this.addAnnotation(this.traverseAnnotationDecl(var8_8, var11_12, false, var3_3));
                var8_8 = DOMUtil.getNextSiblingElement(var8_8);
            } else {
                var15_17 = DOMUtil.getSyntheticAnnotation(var8_8);
                if (var15_17 != null) {
                    this.addAnnotation(this.traverseSyntheticAnnotation(var8_8, (String)var15_17, var11_12, false, var3_3));
                }
            }
            if (var8_8 != null && DOMUtil.getLocalName(var8_8).equals(SchemaSymbols.ELT_ANNOTATION)) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                this.fAttrChecker.returnAttrArray(var11_12, var3_3);
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, SchemaSymbols.ELT_ANNOTATION}, var8_8);
            }
        } else {
            var15_17 = DOMUtil.getSyntheticAnnotation(var8_8);
            if (var15_17 != null) {
                this.addAnnotation(this.traverseSyntheticAnnotation(var8_8, (String)var15_17, var11_12, false, var3_3));
            }
        }
        try {
            this.processComplexContent(var8_8, var6_6, true, var3_3, var4_4);
        }
        catch (ComplexTypeRecoverableError var15_18) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            this.fAttrChecker.returnAttrArray(var11_12, var3_3);
            throw var15_18;
        }
        var15_17 = (XSParticleDecl)var14_15.getParticle();
        if (this.fDerivedBy == 2) {
            if (this.fContentType == 3 && var14_15.getContentType() != 3) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                this.fAttrChecker.returnAttrArray(var11_12, var3_3);
                throw new ComplexTypeRecoverableError("derivation-ok-restriction.5.4.1.2", new Object[]{this.fName, var14_15.getName()}, var8_8);
            }
            try {
                this.mergeAttributes(var14_15.getAttrGrp(), this.fAttrGrp, this.fName, false, var8_8);
            }
            catch (ComplexTypeRecoverableError var16_19) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                this.fAttrChecker.returnAttrArray(var11_12, var3_3);
                throw var16_19;
            }
            this.fAttrGrp.removeProhibitedAttrs();
            if (var14_15 != SchemaGrammar.fAnyType && (var16_20 = this.fAttrGrp.validRestrictionOf(this.fName, var14_15.getAttrGrp())) != null) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                this.fAttrChecker.returnAttrArray(var11_12, var3_3);
                throw new ComplexTypeRecoverableError((String)var16_20[var16_20.length - 1], var16_20, var8_8);
            } else {
                ** GOTO lbl130
            }
        }
        if (this.fParticle == null) {
            this.fContentType = var14_15.getContentType();
            this.fXSSimpleType = (XSSimpleType)var14_15.getSimpleType();
            this.fParticle = var15_17;
        } else if (var14_15.getContentType() != 0) {
            if (this.fContentType == 2 && var14_15.getContentType() != 2) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                this.fAttrChecker.returnAttrArray(var11_12, var3_3);
                throw new ComplexTypeRecoverableError("cos-ct-extends.1.4.3.2.2.1.a", new Object[]{this.fName}, var8_8);
            }
            if (this.fContentType == 3 && var14_15.getContentType() != 3) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                this.fAttrChecker.returnAttrArray(var11_12, var3_3);
                throw new ComplexTypeRecoverableError("cos-ct-extends.1.4.3.2.2.1.b", new Object[]{this.fName}, var8_8);
            }
            if (this.fParticle.fType == 3 && ((XSModelGroupImpl)this.fParticle.fValue).fCompositor == 103 || ((XSParticleDecl)var14_15.getParticle()).fType == 3 && ((XSModelGroupImpl)((XSParticleDecl)var14_15.getParticle()).fValue).fCompositor == 103) {
                this.fAttrChecker.returnAttrArray(var5_5, var3_3);
                this.fAttrChecker.returnAttrArray(var11_12, var3_3);
                throw new ComplexTypeRecoverableError("cos-all-limited.1.2", new Object[0], var8_8);
            }
            var16_21 = new XSModelGroupImpl();
            var16_21.fCompositor = 102;
            var16_21.fParticleCount = 2;
            var16_21.fParticles = new XSParticleDecl[2];
            var16_21.fParticles[0] = (XSParticleDecl)var14_15.getParticle();
            var16_21.fParticles[1] = this.fParticle;
            var16_21.fAnnotations = XSObjectListImpl.EMPTY_LIST;
            var17_23 = new XSParticleDecl();
            var17_23.fType = 3;
            var17_23.fValue = var16_21;
            var17_23.fAnnotations = XSObjectListImpl.EMPTY_LIST;
            this.fParticle = var17_23;
        }
        this.fAttrGrp.removeProhibitedAttrs();
        try {
            this.mergeAttributes(var14_15.getAttrGrp(), this.fAttrGrp, this.fName, true, var8_8);
        }
        catch (ComplexTypeRecoverableError var16_22) {
            this.fAttrChecker.returnAttrArray(var5_5, var3_3);
            this.fAttrChecker.returnAttrArray(var11_12, var3_3);
            throw var16_22;
        }
lbl130: // 3 sources:
        this.fAttrChecker.returnAttrArray(var5_5, var3_3);
        this.fAttrChecker.returnAttrArray(var11_12, var3_3);
    }

    private void mergeAttributes(XSAttributeGroupDecl xSAttributeGroupDecl, XSAttributeGroupDecl xSAttributeGroupDecl2, String string, boolean bl, Element element) throws ComplexTypeRecoverableError {
        XSObjectList xSObjectList = xSAttributeGroupDecl.getAttributeUses();
        XSAttributeUseImpl xSAttributeUseImpl = null;
        int n = xSObjectList.getLength();
        for (int i = 0; i < n; ++i) {
            xSAttributeUseImpl = (XSAttributeUseImpl)xSObjectList.item(i);
            XSAttributeUse xSAttributeUse = xSAttributeGroupDecl2.getAttributeUse(xSAttributeUseImpl.fAttrDecl.getNamespace(), xSAttributeUseImpl.fAttrDecl.getName());
            if (xSAttributeUse == null) {
                String string2 = xSAttributeGroupDecl2.addAttributeUse(xSAttributeUseImpl);
                if (string2 == null) continue;
                throw new ComplexTypeRecoverableError("ct-props-correct.5", new Object[]{string, string2, xSAttributeUseImpl.fAttrDecl.getName()}, element);
            }
            if (xSAttributeUse == xSAttributeUseImpl || !bl) continue;
            this.reportSchemaError("ct-props-correct.4", new Object[]{string, xSAttributeUseImpl.fAttrDecl.getName()}, element);
            xSAttributeGroupDecl2.replaceAttributeUse(xSAttributeUse, xSAttributeUseImpl);
        }
        if (bl) {
            if (xSAttributeGroupDecl2.fAttributeWC == null) {
                xSAttributeGroupDecl2.fAttributeWC = xSAttributeGroupDecl.fAttributeWC;
            } else if (xSAttributeGroupDecl.fAttributeWC != null) {
                xSAttributeGroupDecl2.fAttributeWC = xSAttributeGroupDecl2.fAttributeWC.performUnionWith(xSAttributeGroupDecl.fAttributeWC, xSAttributeGroupDecl2.fAttributeWC.fProcessContents);
                if (xSAttributeGroupDecl2.fAttributeWC == null) {
                    throw new ComplexTypeRecoverableError("src-ct.5", new Object[]{string}, element);
                }
            }
        }
    }

    private void processComplexContent(Element element, boolean bl, boolean bl2, XSDocumentInfo xSDocumentInfo, SchemaGrammar schemaGrammar) throws ComplexTypeRecoverableError {
        Object object;
        Element element2 = null;
        XSParticleDecl xSParticleDecl = null;
        boolean bl3 = false;
        if (element != null) {
            object = DOMUtil.getLocalName(element);
            if (object.equals(SchemaSymbols.ELT_GROUP)) {
                xSParticleDecl = this.fSchemaHandler.fGroupTraverser.traverseLocal(element, xSDocumentInfo, schemaGrammar);
                element2 = DOMUtil.getNextSiblingElement(element);
            } else if (object.equals(SchemaSymbols.ELT_SEQUENCE)) {
                xSParticleDecl = this.traverseSequence(element, xSDocumentInfo, schemaGrammar, 0, this.fComplexTypeDecl);
                if (xSParticleDecl != null) {
                    XSModelGroupImpl xSModelGroupImpl = (XSModelGroupImpl)xSParticleDecl.fValue;
                    if (xSModelGroupImpl.fParticleCount == 0) {
                        bl3 = true;
                    }
                }
                element2 = DOMUtil.getNextSiblingElement(element);
            } else if (object.equals(SchemaSymbols.ELT_CHOICE)) {
                xSParticleDecl = this.traverseChoice(element, xSDocumentInfo, schemaGrammar, 0, this.fComplexTypeDecl);
                if (xSParticleDecl != null && xSParticleDecl.fMinOccurs == 0) {
                    XSModelGroupImpl xSModelGroupImpl = (XSModelGroupImpl)xSParticleDecl.fValue;
                    if (xSModelGroupImpl.fParticleCount == 0) {
                        bl3 = true;
                    }
                }
                element2 = DOMUtil.getNextSiblingElement(element);
            } else if (object.equals(SchemaSymbols.ELT_ALL)) {
                xSParticleDecl = this.traverseAll(element, xSDocumentInfo, schemaGrammar, 8, this.fComplexTypeDecl);
                if (xSParticleDecl != null) {
                    XSModelGroupImpl xSModelGroupImpl = (XSModelGroupImpl)xSParticleDecl.fValue;
                    if (xSModelGroupImpl.fParticleCount == 0) {
                        bl3 = true;
                    }
                }
                element2 = DOMUtil.getNextSiblingElement(element);
            } else {
                element2 = element;
            }
        }
        if (bl3) {
            object = DOMUtil.getFirstChildElement(element);
            if (object != null && DOMUtil.getLocalName((Node)object).equals(SchemaSymbols.ELT_ANNOTATION)) {
                object = DOMUtil.getNextSiblingElement((Node)object);
            }
            if (object == null) {
                xSParticleDecl = null;
            }
        }
        if (xSParticleDecl == null && bl) {
            xSParticleDecl = XSConstraints.getEmptySequence();
        }
        this.fParticle = xSParticleDecl;
        this.fContentType = this.fParticle == null ? 0 : (bl ? 3 : 2);
        if (element2 != null) {
            if (!this.isAttrOrAttrGroup(element2)) {
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, DOMUtil.getLocalName(element2)}, element2);
            }
            object = this.traverseAttrsAndAttrGrps(element2, this.fAttrGrp, xSDocumentInfo, schemaGrammar, this.fComplexTypeDecl);
            if (object != null) {
                throw new ComplexTypeRecoverableError("s4s-elt-invalid-content.1", new Object[]{this.fName, DOMUtil.getLocalName((Node)object)}, (Element)object);
            }
            if (!bl2) {
                this.fAttrGrp.removeProhibitedAttrs();
            }
        }
    }

    private boolean isAttrOrAttrGroup(Element element) {
        String string = DOMUtil.getLocalName(element);
        if (string.equals(SchemaSymbols.ELT_ATTRIBUTE) || string.equals(SchemaSymbols.ELT_ATTRIBUTEGROUP) || string.equals(SchemaSymbols.ELT_ANYATTRIBUTE)) {
            return true;
        }
        return false;
    }

    private void traverseSimpleContentDecl(Element element) {
    }

    private void traverseComplexContentDecl(Element element, boolean bl) {
    }

    private String genAnonTypeName(Element element) {
        StringBuffer stringBuffer = new StringBuffer("#AnonType_");
        Element element2 = DOMUtil.getParent(element);
        while (element2 != null && element2 != DOMUtil.getRoot(DOMUtil.getDocument(element2))) {
            stringBuffer.append(element2.getAttribute(SchemaSymbols.ATT_NAME));
            element2 = DOMUtil.getParent(element2);
        }
        return stringBuffer.toString();
    }

    private void handleComplexTypeError(String string, Object[] arrobject, Element element) {
        if (string != null) {
            this.reportSchemaError(string, arrobject, element);
        }
        this.fBaseType = SchemaGrammar.fAnyType;
        this.fContentType = 3;
        this.fXSSimpleType = null;
        this.fParticle = XSDComplexTypeTraverser.getErrorContent();
        this.fAttrGrp.fAttributeWC = XSDComplexTypeTraverser.getErrorWildcard();
    }

    private void contentBackup() {
        if (this.fGlobalStore == null) {
            this.fGlobalStore = new Object[11];
            this.fGlobalStorePos = 0;
        }
        if (this.fGlobalStorePos == this.fGlobalStore.length) {
            Object[] arrobject = new Object[this.fGlobalStorePos + 11];
            System.arraycopy(this.fGlobalStore, 0, arrobject, 0, this.fGlobalStorePos);
            this.fGlobalStore = arrobject;
        }
        this.fGlobalStore[this.fGlobalStorePos++] = this.fComplexTypeDecl;
        this.fGlobalStore[this.fGlobalStorePos++] = this.fIsAbstract ? Boolean.TRUE : Boolean.FALSE;
        this.fGlobalStore[this.fGlobalStorePos++] = this.fName;
        this.fGlobalStore[this.fGlobalStorePos++] = this.fTargetNamespace;
        this.fGlobalStore[this.fGlobalStorePos++] = new Integer((this.fDerivedBy << 16) + this.fFinal);
        this.fGlobalStore[this.fGlobalStorePos++] = new Integer((this.fBlock << 16) + this.fContentType);
        this.fGlobalStore[this.fGlobalStorePos++] = this.fBaseType;
        this.fGlobalStore[this.fGlobalStorePos++] = this.fAttrGrp;
        this.fGlobalStore[this.fGlobalStorePos++] = this.fParticle;
        this.fGlobalStore[this.fGlobalStorePos++] = this.fXSSimpleType;
        this.fGlobalStore[this.fGlobalStorePos++] = this.fAnnotations;
    }

    private void contentRestore() {
        this.fAnnotations = (XSAnnotationImpl[])this.fGlobalStore[--this.fGlobalStorePos];
        this.fXSSimpleType = (XSSimpleType)this.fGlobalStore[--this.fGlobalStorePos];
        this.fParticle = (XSParticleDecl)this.fGlobalStore[--this.fGlobalStorePos];
        this.fAttrGrp = (XSAttributeGroupDecl)this.fGlobalStore[--this.fGlobalStorePos];
        this.fBaseType = (XSTypeDefinition)this.fGlobalStore[--this.fGlobalStorePos];
        int n = (Integer)this.fGlobalStore[--this.fGlobalStorePos];
        this.fBlock = (short)(n >> 16);
        this.fContentType = (short)n;
        n = (Integer)this.fGlobalStore[--this.fGlobalStorePos];
        this.fDerivedBy = (short)(n >> 16);
        this.fFinal = (short)n;
        this.fTargetNamespace = (String)this.fGlobalStore[--this.fGlobalStorePos];
        this.fName = (String)this.fGlobalStore[--this.fGlobalStorePos];
        this.fIsAbstract = (Boolean)this.fGlobalStore[--this.fGlobalStorePos];
        this.fComplexTypeDecl = (XSComplexTypeDecl)this.fGlobalStore[--this.fGlobalStorePos];
    }

    private void addAnnotation(XSAnnotationImpl xSAnnotationImpl) {
        if (xSAnnotationImpl == null) {
            return;
        }
        if (this.fAnnotations == null) {
            this.fAnnotations = new XSAnnotationImpl[1];
        } else {
            XSAnnotationImpl[] arrxSAnnotationImpl = new XSAnnotationImpl[this.fAnnotations.length + 1];
            System.arraycopy(this.fAnnotations, 0, arrxSAnnotationImpl, 0, this.fAnnotations.length);
            this.fAnnotations = arrxSAnnotationImpl;
        }
        this.fAnnotations[this.fAnnotations.length - 1] = xSAnnotationImpl;
    }

    private static final class ComplexTypeRecoverableError
    extends Exception {
        private static final long serialVersionUID = 6802729912091130335L;
        Object[] errorSubstText = null;
        Element errorElem = null;

        ComplexTypeRecoverableError() {
        }

        ComplexTypeRecoverableError(String string, Object[] arrobject, Element element) {
            super(string);
            this.errorSubstText = arrobject;
            this.errorElem = element;
        }
    }

}

