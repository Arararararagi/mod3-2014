/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs;

import java.io.File;
import java.io.UnsupportedEncodingException;

final class FilePathToURI {
    private static boolean[] gNeedEscaping = new boolean[128];
    private static char[] gAfterEscaping1 = new char[128];
    private static char[] gAfterEscaping2 = new char[128];
    private static char[] gHexChs = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private FilePathToURI() {
    }

    public static String filepath2URI(String string) {
        int n;
        Object object;
        if (string == null) {
            return null;
        }
        char c = File.separatorChar;
        string = string.replace(c, '/');
        int n2 = string.length();
        StringBuffer stringBuffer = new StringBuffer(n2 * 3);
        stringBuffer.append("file://");
        if (n2 >= 2 && string.charAt(1) == ':' && (object = Character.toUpperCase(string.charAt(0))) >= 'A' && object <= 'Z') {
            stringBuffer.append('/');
        }
        for (n = 0; n < n2; ++n) {
            object = string.charAt(n);
            if (object >= '') break;
            if (gNeedEscaping[object]) {
                stringBuffer.append('%');
                stringBuffer.append(gAfterEscaping1[object]);
                stringBuffer.append(gAfterEscaping2[object]);
                continue;
            }
            stringBuffer.append((char)object);
        }
        if (n < n2) {
            Object object2 = null;
            try {
                object2 = string.substring(n).getBytes("UTF-8");
            }
            catch (UnsupportedEncodingException var8_7) {
                return string;
            }
            n2 = object2.length;
            for (n = 0; n < n2; ++n) {
                Object object3 = object2[n];
                if (object3 < 0) {
                    object = object3 + 256;
                    stringBuffer.append('%');
                    stringBuffer.append(gHexChs[object >> 4]);
                    stringBuffer.append(gHexChs[object & 15]);
                    continue;
                }
                if (gNeedEscaping[object3]) {
                    stringBuffer.append('%');
                    stringBuffer.append(gAfterEscaping1[object3]);
                    stringBuffer.append(gAfterEscaping2[object3]);
                    continue;
                }
                stringBuffer.append((char)object3);
            }
        }
        return stringBuffer.toString();
    }

    static {
        for (int i = 0; i <= 31; ++i) {
            FilePathToURI.gNeedEscaping[i] = true;
            FilePathToURI.gAfterEscaping1[i] = gHexChs[i >> 4];
            FilePathToURI.gAfterEscaping2[i] = gHexChs[i & 15];
        }
        FilePathToURI.gNeedEscaping[127] = true;
        FilePathToURI.gAfterEscaping1[127] = 55;
        FilePathToURI.gAfterEscaping2[127] = 70;
        char[] arrc = new char[]{' ', '<', '>', '#', '%', '\"', '{', '}', '|', '\\', '^', '~', '[', ']', '`'};
        int n = arrc.length;
        for (int j = 0; j < n; ++j) {
            char c = arrc[j];
            FilePathToURI.gNeedEscaping[c] = true;
            FilePathToURI.gAfterEscaping1[c] = gHexChs[c >> 4];
            FilePathToURI.gAfterEscaping2[c] = gHexChs[c & 15];
        }
    }
}

