/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs;

import org.apache.xerces.impl.xs.SchemaGrammar;
import org.apache.xerces.impl.xs.XMLSchemaLoader;
import org.apache.xerces.impl.xs.XSAnnotationImpl;
import org.apache.xerces.impl.xs.XSAttributeDecl;
import org.apache.xerces.impl.xs.XSAttributeGroupDecl;
import org.apache.xerces.impl.xs.XSElementDecl;
import org.apache.xerces.impl.xs.XSGroupDecl;
import org.apache.xerces.impl.xs.XSNotationDecl;
import org.apache.xerces.impl.xs.util.XSGrammarPool;
import org.apache.xerces.xni.grammars.Grammar;
import org.apache.xerces.xni.grammars.XMLGrammarDescription;
import org.apache.xerces.xni.grammars.XSGrammar;
import org.apache.xerces.xni.parser.XMLInputSource;
import org.apache.xerces.xs.LSInputList;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSLoader;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSTypeDefinition;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMStringList;
import org.w3c.dom.ls.LSInput;

public final class XSLoaderImpl
implements XSLoader,
DOMConfiguration {
    private final XSGrammarPool fGrammarPool = new XSGrammarMerger();
    private final XMLSchemaLoader fSchemaLoader = new XMLSchemaLoader();

    public XSLoaderImpl() {
        this.fSchemaLoader.setProperty("http://apache.org/xml/properties/internal/grammar-pool", this.fGrammarPool);
    }

    public DOMConfiguration getConfig() {
        return this;
    }

    public XSModel loadURIList(StringList stringList) {
        int n = stringList.getLength();
        try {
            this.fGrammarPool.clear();
            for (int i = 0; i < n; ++i) {
                this.fSchemaLoader.loadGrammar(new XMLInputSource(null, stringList.item(i), null));
            }
            return this.fGrammarPool.toXSModel();
        }
        catch (Exception var3_4) {
            this.fSchemaLoader.reportDOMFatalError(var3_4);
            return null;
        }
    }

    public XSModel loadInputList(LSInputList lSInputList) {
        int n = lSInputList.getLength();
        try {
            this.fGrammarPool.clear();
            for (int i = 0; i < n; ++i) {
                this.fSchemaLoader.loadGrammar(this.fSchemaLoader.dom2xmlInputSource(lSInputList.item(i)));
            }
            return this.fGrammarPool.toXSModel();
        }
        catch (Exception var3_4) {
            this.fSchemaLoader.reportDOMFatalError(var3_4);
            return null;
        }
    }

    public XSModel loadURI(String string) {
        try {
            this.fGrammarPool.clear();
            return ((XSGrammar)this.fSchemaLoader.loadGrammar(new XMLInputSource(null, string, null))).toXSModel();
        }
        catch (Exception var2_2) {
            this.fSchemaLoader.reportDOMFatalError(var2_2);
            return null;
        }
    }

    public XSModel load(LSInput lSInput) {
        try {
            this.fGrammarPool.clear();
            return ((XSGrammar)this.fSchemaLoader.loadGrammar(this.fSchemaLoader.dom2xmlInputSource(lSInput))).toXSModel();
        }
        catch (Exception var2_2) {
            this.fSchemaLoader.reportDOMFatalError(var2_2);
            return null;
        }
    }

    public void setParameter(String string, Object object) throws DOMException {
        this.fSchemaLoader.setParameter(string, object);
    }

    public Object getParameter(String string) throws DOMException {
        return this.fSchemaLoader.getParameter(string);
    }

    public boolean canSetParameter(String string, Object object) {
        return this.fSchemaLoader.canSetParameter(string, object);
    }

    public DOMStringList getParameterNames() {
        return this.fSchemaLoader.getParameterNames();
    }

    private static final class XSGrammarMerger
    extends XSGrammarPool {
        public void putGrammar(Grammar grammar) {
            SchemaGrammar schemaGrammar = this.toSchemaGrammar(super.getGrammar(grammar.getGrammarDescription()));
            if (schemaGrammar != null) {
                SchemaGrammar schemaGrammar2 = this.toSchemaGrammar(grammar);
                if (schemaGrammar2 != null) {
                    this.mergeSchemaGrammars(schemaGrammar, schemaGrammar2);
                }
            } else {
                super.putGrammar(grammar);
            }
        }

        private SchemaGrammar toSchemaGrammar(Grammar grammar) {
            return grammar instanceof SchemaGrammar ? (SchemaGrammar)grammar : null;
        }

        private void mergeSchemaGrammars(SchemaGrammar schemaGrammar, SchemaGrammar schemaGrammar2) {
            Object object;
            XSNamedMap xSNamedMap = schemaGrammar2.getComponents(2);
            int n = xSNamedMap.getLength();
            for (int i = 0; i < n; ++i) {
                XSElementDecl xSElementDecl = (XSElementDecl)xSNamedMap.item(i);
                if (schemaGrammar.getGlobalElementDecl(xSElementDecl.getName()) != null) continue;
                schemaGrammar.addGlobalElementDecl(xSElementDecl);
            }
            xSNamedMap = schemaGrammar2.getComponents(1);
            n = xSNamedMap.getLength();
            for (int j = 0; j < n; ++j) {
                XSAttributeDecl xSAttributeDecl = (XSAttributeDecl)xSNamedMap.item(j);
                if (schemaGrammar.getGlobalAttributeDecl(xSAttributeDecl.getName()) != null) continue;
                schemaGrammar.addGlobalAttributeDecl(xSAttributeDecl);
            }
            xSNamedMap = schemaGrammar2.getComponents(3);
            n = xSNamedMap.getLength();
            for (int k = 0; k < n; ++k) {
                XSTypeDefinition xSTypeDefinition = (XSTypeDefinition)xSNamedMap.item(k);
                if (schemaGrammar.getGlobalTypeDecl(xSTypeDefinition.getName()) != null) continue;
                schemaGrammar.addGlobalTypeDecl(xSTypeDefinition);
            }
            xSNamedMap = schemaGrammar2.getComponents(5);
            n = xSNamedMap.getLength();
            for (int i2 = 0; i2 < n; ++i2) {
                XSAttributeGroupDecl xSAttributeGroupDecl = (XSAttributeGroupDecl)xSNamedMap.item(i2);
                if (schemaGrammar.getGlobalAttributeGroupDecl(xSAttributeGroupDecl.getName()) != null) continue;
                schemaGrammar.addGlobalAttributeGroupDecl(xSAttributeGroupDecl);
            }
            xSNamedMap = schemaGrammar2.getComponents(7);
            n = xSNamedMap.getLength();
            for (int i3 = 0; i3 < n; ++i3) {
                XSGroupDecl xSGroupDecl = (XSGroupDecl)xSNamedMap.item(i3);
                if (schemaGrammar.getGlobalGroupDecl(xSGroupDecl.getName()) != null) continue;
                schemaGrammar.addGlobalGroupDecl(xSGroupDecl);
            }
            xSNamedMap = schemaGrammar2.getComponents(11);
            n = xSNamedMap.getLength();
            for (int i4 = 0; i4 < n; ++i4) {
                object = (XSNotationDecl)xSNamedMap.item(i4);
                if (schemaGrammar.getGlobalNotationDecl(object.getName()) != null) continue;
                schemaGrammar.addGlobalNotationDecl((XSNotationDecl)object);
            }
            object = schemaGrammar2.getAnnotations();
            n = object.getLength();
            for (int i5 = 0; i5 < n; ++i5) {
                schemaGrammar.addAnnotation((XSAnnotationImpl)object.item(i5));
            }
        }

        public boolean containsGrammar(XMLGrammarDescription xMLGrammarDescription) {
            return false;
        }

        public Grammar getGrammar(XMLGrammarDescription xMLGrammarDescription) {
            return null;
        }

        public Grammar retrieveGrammar(XMLGrammarDescription xMLGrammarDescription) {
            return null;
        }

        public Grammar[] retrieveInitialGrammarSet(String string) {
            return new Grammar[0];
        }
    }

}

