/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.models;

import java.util.Vector;
import org.apache.xerces.impl.xs.SubstitutionGroupHandler;
import org.apache.xerces.impl.xs.XMLSchemaException;
import org.apache.xerces.impl.xs.XSConstraints;
import org.apache.xerces.impl.xs.XSElementDecl;
import org.apache.xerces.impl.xs.models.XSCMValidator;
import org.apache.xerces.xni.QName;

public class XSAllCM
implements XSCMValidator {
    private static final short STATE_START = 0;
    private static final short STATE_VALID = 1;
    private static final short STATE_CHILD = 1;
    private final XSElementDecl[] fAllElements;
    private final boolean[] fIsOptionalElement;
    private final boolean fHasOptionalContent;
    private int fNumElements = 0;

    public XSAllCM(boolean bl, int n) {
        this.fHasOptionalContent = bl;
        this.fAllElements = new XSElementDecl[n];
        this.fIsOptionalElement = new boolean[n];
    }

    public void addElement(XSElementDecl xSElementDecl, boolean bl) {
        this.fAllElements[this.fNumElements] = xSElementDecl;
        this.fIsOptionalElement[this.fNumElements] = bl;
        ++this.fNumElements;
    }

    public int[] startContentModel() {
        int[] arrn = new int[this.fNumElements + 1];
        for (int i = 0; i <= this.fNumElements; ++i) {
            arrn[i] = 0;
        }
        return arrn;
    }

    Object findMatchingDecl(QName qName, SubstitutionGroupHandler substitutionGroupHandler) {
        XSElementDecl xSElementDecl = null;
        for (int i = 0; i < this.fNumElements; ++i) {
            xSElementDecl = substitutionGroupHandler.getMatchingElemDecl(qName, this.fAllElements[i]);
            if (xSElementDecl != null) break;
        }
        return xSElementDecl;
    }

    public Object oneTransition(QName qName, int[] arrn, SubstitutionGroupHandler substitutionGroupHandler) {
        if (arrn[0] < 0) {
            arrn[0] = -2;
            return this.findMatchingDecl(qName, substitutionGroupHandler);
        }
        arrn[0] = 1;
        XSElementDecl xSElementDecl = null;
        for (int i = 0; i < this.fNumElements; ++i) {
            if (arrn[i + 1] != 0 || (xSElementDecl = substitutionGroupHandler.getMatchingElemDecl(qName, this.fAllElements[i])) == null) continue;
            arrn[i + 1] = 1;
            return xSElementDecl;
        }
        arrn[0] = -1;
        return this.findMatchingDecl(qName, substitutionGroupHandler);
    }

    public boolean endContentModel(int[] arrn) {
        int n = arrn[0];
        if (n == -1 || n == -2) {
            return false;
        }
        if (this.fHasOptionalContent && n == 0) {
            return true;
        }
        for (int i = 0; i < this.fNumElements; ++i) {
            if (this.fIsOptionalElement[i] || arrn[i + 1] != 0) continue;
            return false;
        }
        return true;
    }

    public boolean checkUniqueParticleAttribution(SubstitutionGroupHandler substitutionGroupHandler) throws XMLSchemaException {
        for (int i = 0; i < this.fNumElements; ++i) {
            for (int j = i + 1; j < this.fNumElements; ++j) {
                if (!XSConstraints.overlapUPA(this.fAllElements[i], this.fAllElements[j], substitutionGroupHandler)) continue;
                throw new XMLSchemaException("cos-nonambig", new Object[]{this.fAllElements[i].toString(), this.fAllElements[j].toString()});
            }
        }
        return false;
    }

    public Vector whatCanGoHere(int[] arrn) {
        Vector<XSElementDecl> vector = new Vector<XSElementDecl>();
        for (int i = 0; i < this.fNumElements; ++i) {
            if (arrn[i + 1] != 0) continue;
            vector.addElement(this.fAllElements[i]);
        }
        return vector;
    }

    public int[] occurenceInfo(int[] arrn) {
        return null;
    }

    public String getTermName(int n) {
        return null;
    }

    public boolean isCompactedForUPA() {
        return false;
    }
}

