/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl.xs.models;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Vector;
import org.apache.xerces.impl.dtd.models.CMNode;
import org.apache.xerces.impl.dtd.models.CMStateSet;
import org.apache.xerces.impl.xs.SubstitutionGroupHandler;
import org.apache.xerces.impl.xs.XMLSchemaException;
import org.apache.xerces.impl.xs.XSConstraints;
import org.apache.xerces.impl.xs.XSElementDecl;
import org.apache.xerces.impl.xs.XSWildcardDecl;
import org.apache.xerces.impl.xs.models.XSCMBinOp;
import org.apache.xerces.impl.xs.models.XSCMLeaf;
import org.apache.xerces.impl.xs.models.XSCMRepeatingLeaf;
import org.apache.xerces.impl.xs.models.XSCMUniOp;
import org.apache.xerces.impl.xs.models.XSCMValidator;
import org.apache.xerces.xni.QName;

public class XSDFACM
implements XSCMValidator {
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_VALIDATE_CONTENT = false;
    private Object[] fElemMap = null;
    private int[] fElemMapType = null;
    private int[] fElemMapId = null;
    private int fElemMapSize = 0;
    private boolean[] fFinalStateFlags = null;
    private CMStateSet[] fFollowList = null;
    private CMNode fHeadNode = null;
    private int fLeafCount = 0;
    private XSCMLeaf[] fLeafList = null;
    private int[] fLeafListType = null;
    private int[][] fTransTable = null;
    private Occurence[] fCountingStates = null;
    private int fTransTableSize = 0;
    private boolean fIsCompactedForUPA;
    private static long time = 0;

    public XSDFACM(CMNode cMNode, int n) {
        this.fLeafCount = n;
        this.fIsCompactedForUPA = cMNode.isCompactedForUPA();
        this.buildDFA(cMNode);
    }

    public boolean isFinalState(int n) {
        return n < 0 ? false : this.fFinalStateFlags[n];
    }

    public Object oneTransition(QName qName, int[] arrn, SubstitutionGroupHandler substitutionGroupHandler) {
        int n;
        int n2 = arrn[0];
        if (n2 == -1 || n2 == -2) {
            if (n2 == -1) {
                arrn[0] = -2;
            }
            return this.findMatchingDecl(qName, substitutionGroupHandler);
        }
        int n3 = 0;
        Object object = null;
        for (n = 0; n < this.fElemMapSize; ++n) {
            n3 = this.fTransTable[n2][n];
            if (n3 == -1) continue;
            int n4 = this.fElemMapType[n];
            if (n4 == 1) {
                object = substitutionGroupHandler.getMatchingElemDecl(qName, (XSElementDecl)this.fElemMap[n]);
                if (object == null) continue;
                break;
            }
            if (n4 != 2 || !((XSWildcardDecl)this.fElemMap[n]).allowNamespace(qName.uri)) continue;
            object = this.fElemMap[n];
            break;
        }
        if (n == this.fElemMapSize) {
            arrn[1] = arrn[0];
            arrn[0] = -1;
            return this.findMatchingDecl(qName, substitutionGroupHandler);
        }
        if (this.fCountingStates != null) {
            Occurence occurence = this.fCountingStates[n2];
            if (occurence != null) {
                if (n2 == n3) {
                    arrn[2] = arrn[2] + 1;
                    if (arrn[2] > occurence.maxOccurs && occurence.maxOccurs != -1) {
                        return this.findMatchingDecl(qName, arrn, substitutionGroupHandler, n);
                    }
                } else {
                    if (arrn[2] < occurence.minOccurs) {
                        arrn[1] = arrn[0];
                        arrn[0] = -1;
                        return this.findMatchingDecl(qName, substitutionGroupHandler);
                    }
                    occurence = this.fCountingStates[n3];
                    if (occurence != null) {
                        arrn[2] = n == occurence.elemIndex ? 1 : 0;
                    }
                }
            } else {
                occurence = this.fCountingStates[n3];
                if (occurence != null) {
                    arrn[2] = n == occurence.elemIndex ? 1 : 0;
                }
            }
        }
        arrn[0] = n3;
        return object;
    }

    Object findMatchingDecl(QName qName, SubstitutionGroupHandler substitutionGroupHandler) {
        XSElementDecl xSElementDecl = null;
        for (int i = 0; i < this.fElemMapSize; ++i) {
            int n = this.fElemMapType[i];
            if (n == 1) {
                xSElementDecl = substitutionGroupHandler.getMatchingElemDecl(qName, (XSElementDecl)this.fElemMap[i]);
                if (xSElementDecl == null) continue;
                return xSElementDecl;
            }
            if (n != 2 || !((XSWildcardDecl)this.fElemMap[i]).allowNamespace(qName.uri)) continue;
            return this.fElemMap[i];
        }
        return null;
    }

    Object findMatchingDecl(QName qName, int[] arrn, SubstitutionGroupHandler substitutionGroupHandler, int n) {
        int n2 = arrn[0];
        int n3 = 0;
        Object object = null;
        while (++n < this.fElemMapSize) {
            n3 = this.fTransTable[n2][n];
            if (n3 == -1) continue;
            int n4 = this.fElemMapType[n];
            if (n4 == 1) {
                object = substitutionGroupHandler.getMatchingElemDecl(qName, (XSElementDecl)this.fElemMap[n]);
                if (object == null) continue;
                break;
            }
            if (n4 != 2 || !((XSWildcardDecl)this.fElemMap[n]).allowNamespace(qName.uri)) continue;
            object = this.fElemMap[n];
            break;
        }
        if (n == this.fElemMapSize) {
            arrn[1] = arrn[0];
            arrn[0] = -1;
            return this.findMatchingDecl(qName, substitutionGroupHandler);
        }
        arrn[0] = n3;
        Occurence occurence = this.fCountingStates[n3];
        if (occurence != null) {
            arrn[2] = n == occurence.elemIndex ? 1 : 0;
        }
        return object;
    }

    public int[] startContentModel() {
        return new int[3];
    }

    public boolean endContentModel(int[] arrn) {
        int n = arrn[0];
        if (this.fFinalStateFlags[n]) {
            Occurence occurence;
            if (this.fCountingStates != null && (occurence = this.fCountingStates[n]) != null && arrn[2] < occurence.minOccurs) {
                return false;
            }
            return true;
        }
        return false;
    }

    private void buildDFA(CMNode cMNode) {
        Object object;
        int n;
        int n2;
        int n3;
        int n4 = this.fLeafCount;
        XSCMLeaf xSCMLeaf = new XSCMLeaf(1, null, -1, this.fLeafCount++);
        this.fHeadNode = new XSCMBinOp(102, cMNode, xSCMLeaf);
        this.fLeafList = new XSCMLeaf[this.fLeafCount];
        this.fLeafListType = new int[this.fLeafCount];
        this.postTreeBuildInit(this.fHeadNode);
        this.fFollowList = new CMStateSet[this.fLeafCount];
        for (int i = 0; i < this.fLeafCount; ++i) {
            this.fFollowList[i] = new CMStateSet(this.fLeafCount);
        }
        this.calcFollowList(this.fHeadNode);
        this.fElemMap = new Object[this.fLeafCount];
        this.fElemMapType = new int[this.fLeafCount];
        this.fElemMapId = new int[this.fLeafCount];
        this.fElemMapSize = 0;
        Occurence[] arroccurence = null;
        for (int j = 0; j < this.fLeafCount; ++j) {
            int n5;
            this.fElemMap[j] = null;
            n3 = this.fLeafList[j].getParticleId();
            for (n5 = 0; n5 < this.fElemMapSize; ++n5) {
                if (n3 == this.fElemMapId[n5]) break;
            }
            if (n5 != this.fElemMapSize) continue;
            XSCMLeaf xSCMLeaf2 = this.fLeafList[j];
            this.fElemMap[this.fElemMapSize] = xSCMLeaf2.getLeaf();
            if (xSCMLeaf2 instanceof XSCMRepeatingLeaf) {
                if (arroccurence == null) {
                    arroccurence = new Occurence[this.fLeafCount];
                }
                arroccurence[this.fElemMapSize] = new Occurence((XSCMRepeatingLeaf)xSCMLeaf2, this.fElemMapSize);
            }
            this.fElemMapType[this.fElemMapSize] = this.fLeafListType[j];
            this.fElemMapId[this.fElemMapSize] = n3;
            ++this.fElemMapSize;
        }
        --this.fElemMapSize;
        int[] arrn = new int[this.fLeafCount + this.fElemMapSize];
        n3 = 0;
        for (int k = 0; k < this.fElemMapSize; ++k) {
            n2 = this.fElemMapId[k];
            for (int i2 = 0; i2 < this.fLeafCount; ++i2) {
                if (n2 != this.fLeafList[i2].getParticleId()) continue;
                arrn[n3++] = i2;
            }
            arrn[n3++] = -1;
        }
        n2 = this.fLeafCount * 4;
        CMStateSet[] arrcMStateSet = new CMStateSet[n2];
        this.fFinalStateFlags = new boolean[n2];
        this.fTransTable = new int[n2][];
        CMStateSet cMStateSet = this.fHeadNode.firstPos();
        int n6 = 0;
        int n7 = 0;
        this.fTransTable[n7] = this.makeDefStateList();
        arrcMStateSet[n7] = cMStateSet;
        HashMap<Object, Integer> hashMap = new HashMap<Object, Integer>();
        while (n6 < ++n7) {
            cMStateSet = arrcMStateSet[n6];
            int[] arrn2 = this.fTransTable[n6];
            this.fFinalStateFlags[n6] = cMStateSet.getBit(n4);
            ++n6;
            object = null;
            n = 0;
            for (int i3 = 0; i3 < this.fElemMapSize; ++i3) {
                int n8;
                if (object == null) {
                    object = new CMStateSet(this.fLeafCount);
                } else {
                    object.zeroBits();
                }
                int n9 = arrn[n++];
                while (n9 != -1) {
                    if (cMStateSet.getBit(n9)) {
                        object.union(this.fFollowList[n9]);
                    }
                    n9 = arrn[n++];
                }
                if (object.isEmpty()) continue;
                Integer n10 = (Integer)hashMap.get(object);
                int n11 = n8 = n10 == null ? n7 : n10;
                if (n8 == n7) {
                    arrcMStateSet[n7] = object;
                    this.fTransTable[n7] = this.makeDefStateList();
                    hashMap.put(object, new Integer(n7));
                    ++n7;
                    object = null;
                }
                arrn2[i3] = n8;
                if (n7 != n2) continue;
                int n12 = (int)((double)n2 * 1.5);
                CMStateSet[] arrcMStateSet2 = new CMStateSet[n12];
                boolean[] arrbl = new boolean[n12];
                int[][] arrn3 = new int[n12][];
                System.arraycopy(arrcMStateSet, 0, arrcMStateSet2, 0, n2);
                System.arraycopy(this.fFinalStateFlags, 0, arrbl, 0, n2);
                System.arraycopy(this.fTransTable, 0, arrn3, 0, n2);
                n2 = n12;
                arrcMStateSet = arrcMStateSet2;
                this.fFinalStateFlags = arrbl;
                this.fTransTable = arrn3;
            }
        }
        if (arroccurence != null) {
            this.fCountingStates = new Occurence[n7];
            block8 : for (int i4 = 0; i4 < n7; ++i4) {
                object = this.fTransTable[i4];
                for (n = 0; n < object.length; ++n) {
                    if (i4 != object[n]) continue;
                    this.fCountingStates[i4] = arroccurence[n];
                    continue block8;
                }
            }
        }
        this.fHeadNode = null;
        this.fLeafList = null;
        this.fFollowList = null;
        this.fLeafListType = null;
        this.fElemMapId = null;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void calcFollowList(CMNode cMNode) {
        if (cMNode.type() == 101) {
            this.calcFollowList(((XSCMBinOp)cMNode).getLeft());
            this.calcFollowList(((XSCMBinOp)cMNode).getRight());
            return;
        } else if (cMNode.type() == 102) {
            this.calcFollowList(((XSCMBinOp)cMNode).getLeft());
            this.calcFollowList(((XSCMBinOp)cMNode).getRight());
            CMStateSet cMStateSet = ((XSCMBinOp)cMNode).getLeft().lastPos();
            CMStateSet cMStateSet2 = ((XSCMBinOp)cMNode).getRight().firstPos();
            for (int i = 0; i < this.fLeafCount; ++i) {
                if (!cMStateSet.getBit(i)) continue;
                this.fFollowList[i].union(cMStateSet2);
            }
            return;
        } else if (cMNode.type() == 4 || cMNode.type() == 6) {
            this.calcFollowList(((XSCMUniOp)cMNode).getChild());
            CMStateSet cMStateSet = cMNode.firstPos();
            CMStateSet cMStateSet3 = cMNode.lastPos();
            for (int i = 0; i < this.fLeafCount; ++i) {
                if (!cMStateSet3.getBit(i)) continue;
                this.fFollowList[i].union(cMStateSet);
            }
            return;
        } else {
            if (cMNode.type() != 5) return;
            this.calcFollowList(((XSCMUniOp)cMNode).getChild());
        }
    }

    private void dumpTree(CMNode cMNode, int n) {
        for (int i = 0; i < n; ++i) {
            System.out.print("   ");
        }
        int n2 = cMNode.type();
        switch (n2) {
            case 101: 
            case 102: {
                if (n2 == 101) {
                    System.out.print("Choice Node ");
                } else {
                    System.out.print("Seq Node ");
                }
                if (cMNode.isNullable()) {
                    System.out.print("Nullable ");
                }
                System.out.print("firstPos=");
                System.out.print(cMNode.firstPos().toString());
                System.out.print(" lastPos=");
                System.out.println(cMNode.lastPos().toString());
                this.dumpTree(((XSCMBinOp)cMNode).getLeft(), n + 1);
                this.dumpTree(((XSCMBinOp)cMNode).getRight(), n + 1);
                break;
            }
            case 4: 
            case 5: 
            case 6: {
                System.out.print("Rep Node ");
                if (cMNode.isNullable()) {
                    System.out.print("Nullable ");
                }
                System.out.print("firstPos=");
                System.out.print(cMNode.firstPos().toString());
                System.out.print(" lastPos=");
                System.out.println(cMNode.lastPos().toString());
                this.dumpTree(((XSCMUniOp)cMNode).getChild(), n + 1);
                break;
            }
            case 1: {
                System.out.print("Leaf: (pos=" + ((XSCMLeaf)cMNode).getPosition() + "), " + "(elemIndex=" + ((XSCMLeaf)cMNode).getLeaf() + ") ");
                if (cMNode.isNullable()) {
                    System.out.print(" Nullable ");
                }
                System.out.print("firstPos=");
                System.out.print(cMNode.firstPos().toString());
                System.out.print(" lastPos=");
                System.out.println(cMNode.lastPos().toString());
                break;
            }
            case 2: {
                System.out.print("Any Node: ");
                System.out.print("firstPos=");
                System.out.print(cMNode.firstPos().toString());
                System.out.print(" lastPos=");
                System.out.println(cMNode.lastPos().toString());
                break;
            }
            default: {
                throw new RuntimeException("ImplementationMessages.VAL_NIICM");
            }
        }
    }

    private int[] makeDefStateList() {
        int[] arrn = new int[this.fElemMapSize];
        for (int i = 0; i < this.fElemMapSize; ++i) {
            arrn[i] = -1;
        }
        return arrn;
    }

    private void postTreeBuildInit(CMNode cMNode) throws RuntimeException {
        cMNode.setMaxStates(this.fLeafCount);
        XSCMLeaf xSCMLeaf = null;
        int n = 0;
        if (cMNode.type() == 2) {
            xSCMLeaf = (XSCMLeaf)cMNode;
            n = xSCMLeaf.getPosition();
            this.fLeafList[n] = xSCMLeaf;
            this.fLeafListType[n] = 2;
        } else if (cMNode.type() == 101 || cMNode.type() == 102) {
            this.postTreeBuildInit(((XSCMBinOp)cMNode).getLeft());
            this.postTreeBuildInit(((XSCMBinOp)cMNode).getRight());
        } else if (cMNode.type() == 4 || cMNode.type() == 6 || cMNode.type() == 5) {
            this.postTreeBuildInit(((XSCMUniOp)cMNode).getChild());
        } else if (cMNode.type() == 1) {
            xSCMLeaf = (XSCMLeaf)cMNode;
            n = xSCMLeaf.getPosition();
            this.fLeafList[n] = xSCMLeaf;
            this.fLeafListType[n] = 1;
        } else {
            throw new RuntimeException("ImplementationMessages.VAL_NIICM");
        }
    }

    public boolean checkUniqueParticleAttribution(SubstitutionGroupHandler substitutionGroupHandler) throws XMLSchemaException {
        int n;
        int n2;
        Object object;
        byte[][] arrby = new byte[this.fElemMapSize][this.fElemMapSize];
        for (int i = 0; i < this.fTransTable.length && this.fTransTable[i] != null; ++i) {
            for (n = 0; n < this.fElemMapSize; ++n) {
                for (n2 = n + 1; n2 < this.fElemMapSize; ++n2) {
                    if (this.fTransTable[i][n] == -1 || this.fTransTable[i][n2] == -1 || arrby[n][n2] != 0) continue;
                    if (XSConstraints.overlapUPA(this.fElemMap[n], this.fElemMap[n2], substitutionGroupHandler)) {
                        if (this.fCountingStates != null && (object = this.fCountingStates[i]) != null && this.fTransTable[i][n] == i ^ this.fTransTable[i][n2] == i && object.minOccurs == object.maxOccurs) {
                            arrby[n][n2] = -1;
                            continue;
                        }
                        arrby[n][n2] = 1;
                        continue;
                    }
                    arrby[n][n2] = -1;
                }
            }
        }
        for (n = 0; n < this.fElemMapSize; ++n) {
            for (n2 = 0; n2 < this.fElemMapSize; ++n2) {
                if (arrby[n][n2] != 1) continue;
                throw new XMLSchemaException("cos-nonambig", new Object[]{this.fElemMap[n].toString(), this.fElemMap[n2].toString()});
            }
        }
        for (n2 = 0; n2 < this.fElemMapSize; ++n2) {
            if (this.fElemMapType[n2] != 2) continue;
            object = (XSWildcardDecl)this.fElemMap[n2];
            if (object.fType != 3 && object.fType != 2) continue;
            return true;
        }
        return false;
    }

    public Vector whatCanGoHere(int[] arrn) {
        int n = arrn[0];
        if (n < 0) {
            n = arrn[1];
        }
        Occurence occurence = this.fCountingStates != null ? this.fCountingStates[n] : null;
        int n2 = arrn[2];
        Vector<Object> vector = new Vector<Object>();
        for (int i = 0; i < this.fElemMapSize; ++i) {
            int n3 = this.fTransTable[n][i];
            if (n3 == -1) continue;
            if (occurence != null && (n == n3 ? n2 >= occurence.maxOccurs && occurence.maxOccurs != -1 : n2 < occurence.minOccurs)) continue;
            vector.addElement(this.fElemMap[i]);
        }
        return vector;
    }

    public int[] occurenceInfo(int[] arrn) {
        if (this.fCountingStates != null) {
            Occurence occurence;
            int n = arrn[0];
            if (n < 0) {
                n = arrn[1];
            }
            if ((occurence = this.fCountingStates[n]) != null) {
                int[] arrn2 = new int[]{occurence.minOccurs, occurence.maxOccurs, arrn[2], occurence.elemIndex};
                return arrn2;
            }
        }
        return null;
    }

    public String getTermName(int n) {
        Object object = this.fElemMap[n];
        return object != null ? object.toString() : null;
    }

    public boolean isCompactedForUPA() {
        return this.fIsCompactedForUPA;
    }

    static final class Occurence {
        final int minOccurs;
        final int maxOccurs;
        final int elemIndex;

        public Occurence(XSCMRepeatingLeaf xSCMRepeatingLeaf, int n) {
            this.minOccurs = xSCMRepeatingLeaf.getMinOccurs();
            this.maxOccurs = xSCMRepeatingLeaf.getMaxOccurs();
            this.elemIndex = n;
        }

        public String toString() {
            return "minOccurs=" + this.minOccurs + ";maxOccurs=" + (this.maxOccurs != -1 ? Integer.toString(this.maxOccurs) : "unbounded");
        }
    }

}

