/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.impl;

import java.io.CharConversionException;
import java.io.EOFException;
import java.io.IOException;
import org.apache.xerces.impl.ExternalSubsetResolver;
import org.apache.xerces.impl.XMLDocumentFragmentScannerImpl;
import org.apache.xerces.impl.XMLEntityHandler;
import org.apache.xerces.impl.XMLEntityManager;
import org.apache.xerces.impl.XMLEntityScanner;
import org.apache.xerces.impl.XMLErrorReporter;
import org.apache.xerces.impl.dtd.XMLDTDDescription;
import org.apache.xerces.impl.io.MalformedByteSequenceException;
import org.apache.xerces.impl.validation.ValidationManager;
import org.apache.xerces.util.NamespaceSupport;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.util.XMLChar;
import org.apache.xerces.util.XMLStringBuffer;
import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.NamespaceContext;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLDocumentHandler;
import org.apache.xerces.xni.XMLLocator;
import org.apache.xerces.xni.XMLResourceIdentifier;
import org.apache.xerces.xni.XMLString;
import org.apache.xerces.xni.XNIException;
import org.apache.xerces.xni.parser.XMLComponentManager;
import org.apache.xerces.xni.parser.XMLConfigurationException;
import org.apache.xerces.xni.parser.XMLDTDScanner;
import org.apache.xerces.xni.parser.XMLInputSource;

public class XMLDocumentScannerImpl
extends XMLDocumentFragmentScannerImpl {
    protected static final int SCANNER_STATE_XML_DECL = 0;
    protected static final int SCANNER_STATE_PROLOG = 5;
    protected static final int SCANNER_STATE_TRAILING_MISC = 12;
    protected static final int SCANNER_STATE_DTD_INTERNAL_DECLS = 17;
    protected static final int SCANNER_STATE_DTD_EXTERNAL = 18;
    protected static final int SCANNER_STATE_DTD_EXTERNAL_DECLS = 19;
    protected static final String LOAD_EXTERNAL_DTD = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
    protected static final String DISALLOW_DOCTYPE_DECL_FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
    protected static final String DTD_SCANNER = "http://apache.org/xml/properties/internal/dtd-scanner";
    protected static final String VALIDATION_MANAGER = "http://apache.org/xml/properties/internal/validation-manager";
    protected static final String NAMESPACE_CONTEXT = "http://apache.org/xml/properties/internal/namespace-context";
    private static final String[] RECOGNIZED_FEATURES = new String[]{"http://apache.org/xml/features/nonvalidating/load-external-dtd", "http://apache.org/xml/features/disallow-doctype-decl"};
    private static final Boolean[] FEATURE_DEFAULTS = new Boolean[]{Boolean.TRUE, Boolean.FALSE};
    private static final String[] RECOGNIZED_PROPERTIES = new String[]{"http://apache.org/xml/properties/internal/dtd-scanner", "http://apache.org/xml/properties/internal/validation-manager", "http://apache.org/xml/properties/internal/namespace-context"};
    private static final Object[] PROPERTY_DEFAULTS = new Object[]{null, null, null};
    protected XMLDTDScanner fDTDScanner;
    protected ValidationManager fValidationManager;
    protected boolean fScanningDTD;
    protected String fDoctypeName;
    protected String fDoctypePublicId;
    protected String fDoctypeSystemId;
    protected NamespaceContext fNamespaceContext = new NamespaceSupport();
    protected boolean fLoadExternalDTD = true;
    protected boolean fDisallowDoctype = false;
    protected boolean fSeenDoctypeDecl;
    protected final XMLDocumentFragmentScannerImpl.Dispatcher fXMLDeclDispatcher;
    protected final XMLDocumentFragmentScannerImpl.Dispatcher fPrologDispatcher;
    protected final XMLDocumentFragmentScannerImpl.Dispatcher fDTDDispatcher;
    protected final XMLDocumentFragmentScannerImpl.Dispatcher fTrailingMiscDispatcher;
    private final String[] fStrings;
    private final XMLString fString;
    private final XMLStringBuffer fStringBuffer;
    private XMLInputSource fExternalSubsetSource;
    private final XMLDTDDescription fDTDDescription;

    public XMLDocumentScannerImpl() {
        this.fXMLDeclDispatcher = new XMLDeclDispatcher();
        this.fPrologDispatcher = new PrologDispatcher();
        this.fDTDDispatcher = new DTDDispatcher();
        this.fTrailingMiscDispatcher = new TrailingMiscDispatcher();
        this.fStrings = new String[3];
        this.fString = new XMLString();
        this.fStringBuffer = new XMLStringBuffer();
        this.fExternalSubsetSource = null;
        this.fDTDDescription = new XMLDTDDescription(null, null, null, null, null);
    }

    public void setInputSource(XMLInputSource xMLInputSource) throws IOException {
        this.fEntityManager.setEntityHandler(this);
        this.fEntityManager.startDocumentEntity(xMLInputSource);
    }

    public void reset(XMLComponentManager xMLComponentManager) throws XMLConfigurationException {
        super.reset(xMLComponentManager);
        this.fDoctypeName = null;
        this.fDoctypePublicId = null;
        this.fDoctypeSystemId = null;
        this.fSeenDoctypeDecl = false;
        this.fScanningDTD = false;
        this.fExternalSubsetSource = null;
        if (!this.fParserSettings) {
            this.fNamespaceContext.reset();
            this.setScannerState(0);
            this.setDispatcher(this.fXMLDeclDispatcher);
            return;
        }
        try {
            this.fLoadExternalDTD = xMLComponentManager.getFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd");
        }
        catch (XMLConfigurationException var2_2) {
            this.fLoadExternalDTD = true;
        }
        try {
            this.fDisallowDoctype = xMLComponentManager.getFeature("http://apache.org/xml/features/disallow-doctype-decl");
        }
        catch (XMLConfigurationException var2_3) {
            this.fDisallowDoctype = false;
        }
        this.fDTDScanner = (XMLDTDScanner)xMLComponentManager.getProperty("http://apache.org/xml/properties/internal/dtd-scanner");
        try {
            this.fValidationManager = (ValidationManager)xMLComponentManager.getProperty("http://apache.org/xml/properties/internal/validation-manager");
        }
        catch (XMLConfigurationException var2_4) {
            this.fValidationManager = null;
        }
        try {
            this.fNamespaceContext = (NamespaceContext)xMLComponentManager.getProperty("http://apache.org/xml/properties/internal/namespace-context");
        }
        catch (XMLConfigurationException var2_5) {
            // empty catch block
        }
        if (this.fNamespaceContext == null) {
            this.fNamespaceContext = new NamespaceSupport();
        }
        this.fNamespaceContext.reset();
        this.setScannerState(0);
        this.setDispatcher(this.fXMLDeclDispatcher);
    }

    public String[] getRecognizedFeatures() {
        String[] arrstring = super.getRecognizedFeatures();
        int n = arrstring != null ? arrstring.length : 0;
        String[] arrstring2 = new String[n + RECOGNIZED_FEATURES.length];
        if (arrstring != null) {
            System.arraycopy(arrstring, 0, arrstring2, 0, arrstring.length);
        }
        System.arraycopy(RECOGNIZED_FEATURES, 0, arrstring2, n, RECOGNIZED_FEATURES.length);
        return arrstring2;
    }

    public void setFeature(String string, boolean bl) throws XMLConfigurationException {
        super.setFeature(string, bl);
        if (string.startsWith("http://apache.org/xml/features/")) {
            int n = string.length() - "http://apache.org/xml/features/".length();
            if (n == "nonvalidating/load-external-dtd".length() && string.endsWith("nonvalidating/load-external-dtd")) {
                this.fLoadExternalDTD = bl;
                return;
            }
            if (n == "disallow-doctype-decl".length() && string.endsWith("disallow-doctype-decl")) {
                this.fDisallowDoctype = bl;
                return;
            }
        }
    }

    public String[] getRecognizedProperties() {
        String[] arrstring = super.getRecognizedProperties();
        int n = arrstring != null ? arrstring.length : 0;
        String[] arrstring2 = new String[n + RECOGNIZED_PROPERTIES.length];
        if (arrstring != null) {
            System.arraycopy(arrstring, 0, arrstring2, 0, arrstring.length);
        }
        System.arraycopy(RECOGNIZED_PROPERTIES, 0, arrstring2, n, RECOGNIZED_PROPERTIES.length);
        return arrstring2;
    }

    public void setProperty(String string, Object object) throws XMLConfigurationException {
        super.setProperty(string, object);
        if (string.startsWith("http://apache.org/xml/properties/")) {
            int n = string.length() - "http://apache.org/xml/properties/".length();
            if (n == "internal/dtd-scanner".length() && string.endsWith("internal/dtd-scanner")) {
                this.fDTDScanner = (XMLDTDScanner)object;
            }
            if (n == "internal/namespace-context".length() && string.endsWith("internal/namespace-context") && object != null) {
                this.fNamespaceContext = (NamespaceContext)object;
            }
            return;
        }
    }

    public Boolean getFeatureDefault(String string) {
        for (int i = 0; i < RECOGNIZED_FEATURES.length; ++i) {
            if (!RECOGNIZED_FEATURES[i].equals(string)) continue;
            return FEATURE_DEFAULTS[i];
        }
        return super.getFeatureDefault(string);
    }

    public Object getPropertyDefault(String string) {
        for (int i = 0; i < RECOGNIZED_PROPERTIES.length; ++i) {
            if (!RECOGNIZED_PROPERTIES[i].equals(string)) continue;
            return PROPERTY_DEFAULTS[i];
        }
        return super.getPropertyDefault(string);
    }

    public void startEntity(String string, XMLResourceIdentifier xMLResourceIdentifier, String string2, Augmentations augmentations) throws XNIException {
        super.startEntity(string, xMLResourceIdentifier, string2, augmentations);
        if (!string.equals("[xml]") && this.fEntityScanner.isExternal()) {
            this.setScannerState(16);
        }
        if (this.fDocumentHandler != null && string.equals("[xml]")) {
            this.fDocumentHandler.startDocument(this.fEntityScanner, string2, this.fNamespaceContext, null);
        }
    }

    public void endEntity(String string, Augmentations augmentations) throws XNIException {
        super.endEntity(string, augmentations);
        if (this.fDocumentHandler != null && string.equals("[xml]")) {
            this.fDocumentHandler.endDocument(null);
        }
    }

    protected XMLDocumentFragmentScannerImpl.Dispatcher createContentDispatcher() {
        return new ContentDispatcher();
    }

    protected boolean scanDoctypeDecl() throws IOException, XNIException {
        if (!this.fEntityScanner.skipSpaces()) {
            this.reportFatalError("MSG_SPACE_REQUIRED_BEFORE_ROOT_ELEMENT_TYPE_IN_DOCTYPEDECL", null);
        }
        this.fDoctypeName = this.fEntityScanner.scanName();
        if (this.fDoctypeName == null) {
            this.reportFatalError("MSG_ROOT_ELEMENT_TYPE_REQUIRED", null);
        }
        if (this.fEntityScanner.skipSpaces()) {
            this.scanExternalID(this.fStrings, false);
            this.fDoctypeSystemId = this.fStrings[0];
            this.fDoctypePublicId = this.fStrings[1];
            this.fEntityScanner.skipSpaces();
        }
        boolean bl = this.fHasExternalDTD = this.fDoctypeSystemId != null;
        if (!(this.fHasExternalDTD || this.fExternalSubsetResolver == null)) {
            this.fDTDDescription.setValues(null, null, this.fEntityManager.getCurrentResourceIdentifier().getExpandedSystemId(), null);
            this.fDTDDescription.setRootName(this.fDoctypeName);
            this.fExternalSubsetSource = this.fExternalSubsetResolver.getExternalSubset(this.fDTDDescription);
            boolean bl2 = this.fHasExternalDTD = this.fExternalSubsetSource != null;
        }
        if (this.fDocumentHandler != null) {
            if (this.fExternalSubsetSource == null) {
                this.fDocumentHandler.doctypeDecl(this.fDoctypeName, this.fDoctypePublicId, this.fDoctypeSystemId, null);
            } else {
                this.fDocumentHandler.doctypeDecl(this.fDoctypeName, this.fExternalSubsetSource.getPublicId(), this.fExternalSubsetSource.getSystemId(), null);
            }
        }
        boolean bl3 = true;
        if (!this.fEntityScanner.skipChar(91)) {
            bl3 = false;
            this.fEntityScanner.skipSpaces();
            if (!this.fEntityScanner.skipChar(62)) {
                this.reportFatalError("DoctypedeclUnterminated", new Object[]{this.fDoctypeName});
            }
            --this.fMarkupDepth;
        }
        return bl3;
    }

    protected String getScannerStateName(int n) {
        switch (n) {
            case 0: {
                return "SCANNER_STATE_XML_DECL";
            }
            case 5: {
                return "SCANNER_STATE_PROLOG";
            }
            case 12: {
                return "SCANNER_STATE_TRAILING_MISC";
            }
            case 17: {
                return "SCANNER_STATE_DTD_INTERNAL_DECLS";
            }
            case 18: {
                return "SCANNER_STATE_DTD_EXTERNAL";
            }
            case 19: {
                return "SCANNER_STATE_DTD_EXTERNAL_DECLS";
            }
        }
        return super.getScannerStateName(n);
    }

    static /* synthetic */ XMLStringBuffer access$000(XMLDocumentScannerImpl xMLDocumentScannerImpl) {
        return xMLDocumentScannerImpl.fStringBuffer;
    }

    static /* synthetic */ XMLString access$100(XMLDocumentScannerImpl xMLDocumentScannerImpl) {
        return xMLDocumentScannerImpl.fString;
    }

    protected class ContentDispatcher
    extends XMLDocumentFragmentScannerImpl.FragmentContentDispatcher {
        protected ContentDispatcher() {
        }

        protected boolean scanForDoctypeHook() throws IOException, XNIException {
            if (XMLDocumentScannerImpl.this.fEntityScanner.skipString("DOCTYPE")) {
                XMLDocumentScannerImpl.this.setScannerState(4);
                return true;
            }
            return false;
        }

        protected boolean elementDepthIsZeroHook() throws IOException, XNIException {
            XMLDocumentScannerImpl.this.setScannerState(12);
            XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fTrailingMiscDispatcher);
            return true;
        }

        protected boolean scanRootElementHook() throws IOException, XNIException {
            if (XMLDocumentScannerImpl.this.fExternalSubsetResolver != null && !XMLDocumentScannerImpl.this.fSeenDoctypeDecl && !XMLDocumentScannerImpl.this.fDisallowDoctype && (XMLDocumentScannerImpl.this.fValidation || XMLDocumentScannerImpl.this.fLoadExternalDTD)) {
                XMLDocumentScannerImpl.this.scanStartElementName();
                this.resolveExternalSubsetAndRead();
                if (XMLDocumentScannerImpl.this.scanStartElementAfterName()) {
                    XMLDocumentScannerImpl.this.setScannerState(12);
                    XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fTrailingMiscDispatcher);
                    return true;
                }
            } else if (XMLDocumentScannerImpl.this.scanStartElement()) {
                XMLDocumentScannerImpl.this.setScannerState(12);
                XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fTrailingMiscDispatcher);
                return true;
            }
            return false;
        }

        protected void endOfFileHook(EOFException eOFException) throws IOException, XNIException {
            XMLDocumentScannerImpl.this.reportFatalError("PrematureEOF", null);
        }

        protected void resolveExternalSubsetAndRead() throws IOException, XNIException {
            XMLDocumentScannerImpl.this.fDTDDescription.setValues(null, null, XMLDocumentScannerImpl.this.fEntityManager.getCurrentResourceIdentifier().getExpandedSystemId(), null);
            XMLDocumentScannerImpl.this.fDTDDescription.setRootName(XMLDocumentScannerImpl.this.fElementQName.rawname);
            XMLInputSource xMLInputSource = XMLDocumentScannerImpl.this.fExternalSubsetResolver.getExternalSubset(XMLDocumentScannerImpl.this.fDTDDescription);
            if (xMLInputSource != null) {
                XMLDocumentScannerImpl.this.fDoctypeName = XMLDocumentScannerImpl.this.fElementQName.rawname;
                XMLDocumentScannerImpl.this.fDoctypePublicId = xMLInputSource.getPublicId();
                XMLDocumentScannerImpl.this.fDoctypeSystemId = xMLInputSource.getSystemId();
                if (XMLDocumentScannerImpl.this.fDocumentHandler != null) {
                    XMLDocumentScannerImpl.this.fDocumentHandler.doctypeDecl(XMLDocumentScannerImpl.this.fDoctypeName, XMLDocumentScannerImpl.this.fDoctypePublicId, XMLDocumentScannerImpl.this.fDoctypeSystemId, null);
                }
                try {
                    if (!(XMLDocumentScannerImpl.this.fValidationManager != null && XMLDocumentScannerImpl.this.fValidationManager.isCachedDTD())) {
                        XMLDocumentScannerImpl.this.fDTDScanner.setInputSource(xMLInputSource);
                        while (XMLDocumentScannerImpl.this.fDTDScanner.scanDTDExternalSubset(true)) {
                        }
                    } else {
                        XMLDocumentScannerImpl.this.fDTDScanner.setInputSource(null);
                    }
                    Object var3_2 = null;
                    XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                }
                catch (Throwable var2_4) {
                    Object var3_3 = null;
                    XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                    throw var2_4;
                }
            }
        }
    }

    protected final class DTDDispatcher
    implements XMLDocumentFragmentScannerImpl.Dispatcher {
        protected DTDDispatcher() {
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Lifted jumps to return sites
         */
        public boolean dispatch(boolean var1_1) throws IOException, XNIException {
            block20 : {
                XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(null);
                try {
                    ** try [egrp 1[TRYBLOCK] [0, 1, 2 : 11->663)] { 
lbl4: // 1 sources:
                    break block20;
lbl5: // 1 sources:
                    catch (MalformedByteSequenceException var2_3) {
                        XMLDocumentScannerImpl.this.fErrorReporter.reportError(var2_3.getDomain(), var2_3.getKey(), var2_3.getArguments(), 2, var2_3);
                        var3_6 = false;
                        var8_17 = null;
                        XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                        return var3_6;
                    }
                }
                catch (Throwable var7_21) {
                    var8_20 = null;
                    XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                    throw var7_21;
                }
            }
            block11 : do {
                var2_2 = false;
                switch (XMLDocumentScannerImpl.this.fScannerState) {
                    case 17: {
                        var3_4 = true;
                        var4_8 = (XMLDocumentScannerImpl.this.fValidation != false || XMLDocumentScannerImpl.this.fLoadExternalDTD != false) && (XMLDocumentScannerImpl.this.fValidationManager == null || XMLDocumentScannerImpl.this.fValidationManager.isCachedDTD() == false);
                        var5_11 = XMLDocumentScannerImpl.this.fDTDScanner.scanDTDInternalSubset(var3_4, XMLDocumentScannerImpl.this.fStandalone, XMLDocumentScannerImpl.this.fHasExternalDTD != false && var4_8 != false);
                        if (var5_11) continue block11;
                        if (!XMLDocumentScannerImpl.this.fEntityScanner.skipChar(93)) {
                            XMLDocumentScannerImpl.this.reportFatalError("EXPECTED_SQUARE_BRACKET_TO_CLOSE_INTERNAL_SUBSET", null);
                        }
                        XMLDocumentScannerImpl.this.fEntityScanner.skipSpaces();
                        if (!XMLDocumentScannerImpl.this.fEntityScanner.skipChar(62)) {
                            XMLDocumentScannerImpl.this.reportFatalError("DoctypedeclUnterminated", new Object[]{XMLDocumentScannerImpl.this.fDoctypeName});
                        }
                        --XMLDocumentScannerImpl.this.fMarkupDepth;
                        if (XMLDocumentScannerImpl.this.fDoctypeSystemId != null) {
                            v0 = XMLDocumentScannerImpl.this.fIsEntityDeclaredVC = XMLDocumentScannerImpl.this.fStandalone == false;
                            if (var4_8) {
                                XMLDocumentScannerImpl.this.setScannerState(18);
                                break;
                            }
                        } else if (XMLDocumentScannerImpl.access$200(XMLDocumentScannerImpl.this) != null) {
                            v1 = XMLDocumentScannerImpl.this.fIsEntityDeclaredVC = XMLDocumentScannerImpl.this.fStandalone == false;
                            if (var4_8) {
                                XMLDocumentScannerImpl.this.fDTDScanner.setInputSource(XMLDocumentScannerImpl.access$200(XMLDocumentScannerImpl.this));
                                XMLDocumentScannerImpl.access$202(XMLDocumentScannerImpl.this, null);
                                XMLDocumentScannerImpl.this.setScannerState(19);
                                break;
                            }
                        } else {
                            XMLDocumentScannerImpl.this.fIsEntityDeclaredVC = XMLDocumentScannerImpl.this.fEntityManager.hasPEReferences() != false && XMLDocumentScannerImpl.this.fStandalone == false;
                        }
                        XMLDocumentScannerImpl.this.setScannerState(5);
                        XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fPrologDispatcher);
                        XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                        var6_13 = true;
                        var8_14 = null;
                        XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                        return var6_13;
                    }
                    case 18: {
                        XMLDocumentScannerImpl.access$300(XMLDocumentScannerImpl.this).setValues(XMLDocumentScannerImpl.this.fDoctypePublicId, XMLDocumentScannerImpl.this.fDoctypeSystemId, null, null);
                        XMLDocumentScannerImpl.access$300(XMLDocumentScannerImpl.this).setRootName(XMLDocumentScannerImpl.this.fDoctypeName);
                        var3_5 = XMLDocumentScannerImpl.this.fEntityManager.resolveEntity(XMLDocumentScannerImpl.access$300(XMLDocumentScannerImpl.this));
                        XMLDocumentScannerImpl.this.fDTDScanner.setInputSource(var3_5);
                        XMLDocumentScannerImpl.this.setScannerState(19);
                        var2_2 = true;
                        break;
                    }
                    case 19: {
                        var3_4 = true;
                        var4_8 = XMLDocumentScannerImpl.this.fDTDScanner.scanDTDExternalSubset(var3_4);
                        if (var4_8) continue block11;
                        XMLDocumentScannerImpl.this.setScannerState(5);
                        XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fPrologDispatcher);
                        XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                        var5_11 = true;
                        var8_15 = null;
                        XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                        return var5_11;
                    }
                    default: {
                        throw new XNIException("DTDDispatcher#dispatch: scanner state=" + XMLDocumentScannerImpl.this.fScannerState + " (" + XMLDocumentScannerImpl.this.getScannerStateName(XMLDocumentScannerImpl.this.fScannerState) + ')');
                    }
                }
            } while (var1_1 || var2_2);
            var8_16 = null;
            XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
            return true;
lbl80: // 1 sources:
            catch (CharConversionException var3_7) {
                XMLDocumentScannerImpl.this.fErrorReporter.reportError("http://www.w3.org/TR/1998/REC-xml-19980210", "CharConversionFailure", null, 2, var3_7);
                var4_9 = false;
                var8_18 = null;
                XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                return var4_9;
            }
lbl87: // 1 sources:
            catch (EOFException var4_10) {
                XMLDocumentScannerImpl.this.reportFatalError("PrematureEOF", null);
                var5_12 = false;
                var8_19 = null;
                XMLDocumentScannerImpl.this.fEntityManager.setEntityHandler(XMLDocumentScannerImpl.this);
                return var5_12;
            }
        }
    }

    protected final class PrologDispatcher
    implements XMLDocumentFragmentScannerImpl.Dispatcher {
        protected PrologDispatcher() {
        }

        public boolean dispatch(boolean bl) throws IOException, XNIException {
            try {
                boolean bl2;
                do {
                    bl2 = false;
                    switch (XMLDocumentScannerImpl.this.fScannerState) {
                        case 5: {
                            XMLDocumentScannerImpl.this.fEntityScanner.skipSpaces();
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(60)) {
                                XMLDocumentScannerImpl.this.setScannerState(1);
                                bl2 = true;
                                break;
                            }
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(38)) {
                                XMLDocumentScannerImpl.this.setScannerState(8);
                                bl2 = true;
                                break;
                            }
                            XMLDocumentScannerImpl.this.setScannerState(7);
                            bl2 = true;
                            break;
                        }
                        case 1: {
                            ++XMLDocumentScannerImpl.this.fMarkupDepth;
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(33)) {
                                if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(45)) {
                                    if (!XMLDocumentScannerImpl.this.fEntityScanner.skipChar(45)) {
                                        XMLDocumentScannerImpl.this.reportFatalError("InvalidCommentStart", null);
                                    }
                                    XMLDocumentScannerImpl.this.setScannerState(2);
                                    bl2 = true;
                                    break;
                                }
                                if (XMLDocumentScannerImpl.this.fEntityScanner.skipString("DOCTYPE")) {
                                    XMLDocumentScannerImpl.this.setScannerState(4);
                                    bl2 = true;
                                    break;
                                }
                                XMLDocumentScannerImpl.this.reportFatalError("MarkupNotRecognizedInProlog", null);
                                break;
                            }
                            if (XMLDocumentScannerImpl.this.isValidNameStartChar(XMLDocumentScannerImpl.this.fEntityScanner.peekChar())) {
                                XMLDocumentScannerImpl.this.setScannerState(6);
                                XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fContentDispatcher);
                                return true;
                            }
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(63)) {
                                XMLDocumentScannerImpl.this.setScannerState(3);
                                bl2 = true;
                                break;
                            }
                            if (XMLDocumentScannerImpl.this.isValidNameStartHighSurrogate(XMLDocumentScannerImpl.this.fEntityScanner.peekChar())) {
                                XMLDocumentScannerImpl.this.setScannerState(6);
                                XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fContentDispatcher);
                                return true;
                            }
                            XMLDocumentScannerImpl.this.reportFatalError("MarkupNotRecognizedInProlog", null);
                            break;
                        }
                        case 2: {
                            XMLDocumentScannerImpl.this.scanComment();
                            XMLDocumentScannerImpl.this.setScannerState(5);
                            break;
                        }
                        case 3: {
                            XMLDocumentScannerImpl.this.scanPI();
                            XMLDocumentScannerImpl.this.setScannerState(5);
                            break;
                        }
                        case 4: {
                            if (XMLDocumentScannerImpl.this.fDisallowDoctype) {
                                XMLDocumentScannerImpl.this.reportFatalError("DoctypeNotAllowed", null);
                            }
                            if (XMLDocumentScannerImpl.this.fSeenDoctypeDecl) {
                                XMLDocumentScannerImpl.this.reportFatalError("AlreadySeenDoctype", null);
                            }
                            XMLDocumentScannerImpl.this.fSeenDoctypeDecl = true;
                            if (XMLDocumentScannerImpl.this.scanDoctypeDecl()) {
                                XMLDocumentScannerImpl.this.setScannerState(17);
                                XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fDTDDispatcher);
                                return true;
                            }
                            if (XMLDocumentScannerImpl.this.fDoctypeSystemId != null) {
                                boolean bl3 = XMLDocumentScannerImpl.this.fIsEntityDeclaredVC = !XMLDocumentScannerImpl.this.fStandalone;
                                if ((XMLDocumentScannerImpl.this.fValidation || XMLDocumentScannerImpl.this.fLoadExternalDTD) && (XMLDocumentScannerImpl.this.fValidationManager == null || !XMLDocumentScannerImpl.this.fValidationManager.isCachedDTD())) {
                                    XMLDocumentScannerImpl.this.setScannerState(18);
                                    XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fDTDDispatcher);
                                    return true;
                                }
                            } else if (XMLDocumentScannerImpl.this.fExternalSubsetSource != null) {
                                boolean bl4 = XMLDocumentScannerImpl.this.fIsEntityDeclaredVC = !XMLDocumentScannerImpl.this.fStandalone;
                                if ((XMLDocumentScannerImpl.this.fValidation || XMLDocumentScannerImpl.this.fLoadExternalDTD) && (XMLDocumentScannerImpl.this.fValidationManager == null || !XMLDocumentScannerImpl.this.fValidationManager.isCachedDTD())) {
                                    XMLDocumentScannerImpl.this.fDTDScanner.setInputSource(XMLDocumentScannerImpl.this.fExternalSubsetSource);
                                    XMLDocumentScannerImpl.this.fExternalSubsetSource = null;
                                    XMLDocumentScannerImpl.this.setScannerState(19);
                                    XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fDTDDispatcher);
                                    return true;
                                }
                            }
                            XMLDocumentScannerImpl.this.fDTDScanner.setInputSource(null);
                            XMLDocumentScannerImpl.this.setScannerState(5);
                            break;
                        }
                        case 7: {
                            XMLDocumentScannerImpl.this.reportFatalError("ContentIllegalInProlog", null);
                            XMLDocumentScannerImpl.this.fEntityScanner.scanChar();
                        }
                        case 8: {
                            XMLDocumentScannerImpl.this.reportFatalError("ReferenceIllegalInProlog", null);
                        }
                    }
                } while (bl || bl2);
                if (bl) {
                    if (XMLDocumentScannerImpl.this.fEntityScanner.scanChar() != 60) {
                        XMLDocumentScannerImpl.this.reportFatalError("RootElementRequired", null);
                    }
                    XMLDocumentScannerImpl.this.setScannerState(6);
                    XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fContentDispatcher);
                }
            }
            catch (MalformedByteSequenceException var2_3) {
                XMLDocumentScannerImpl.this.fErrorReporter.reportError(var2_3.getDomain(), var2_3.getKey(), var2_3.getArguments(), 2, var2_3);
                return false;
            }
            catch (CharConversionException var3_4) {
                XMLDocumentScannerImpl.this.fErrorReporter.reportError("http://www.w3.org/TR/1998/REC-xml-19980210", "CharConversionFailure", null, 2, var3_4);
                return false;
            }
            catch (EOFException var4_5) {
                XMLDocumentScannerImpl.this.reportFatalError("PrematureEOF", null);
                return false;
            }
            return true;
        }
    }

    protected final class TrailingMiscDispatcher
    implements XMLDocumentFragmentScannerImpl.Dispatcher {
        protected TrailingMiscDispatcher() {
        }

        public boolean dispatch(boolean bl) throws IOException, XNIException {
            try {
                boolean bl2;
                do {
                    bl2 = false;
                    switch (XMLDocumentScannerImpl.this.fScannerState) {
                        case 12: {
                            XMLDocumentScannerImpl.this.fEntityScanner.skipSpaces();
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(60)) {
                                XMLDocumentScannerImpl.this.setScannerState(1);
                                bl2 = true;
                                break;
                            }
                            XMLDocumentScannerImpl.this.setScannerState(7);
                            bl2 = true;
                            break;
                        }
                        case 1: {
                            ++XMLDocumentScannerImpl.this.fMarkupDepth;
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(63)) {
                                XMLDocumentScannerImpl.this.setScannerState(3);
                                bl2 = true;
                                break;
                            }
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(33)) {
                                XMLDocumentScannerImpl.this.setScannerState(2);
                                bl2 = true;
                                break;
                            }
                            if (XMLDocumentScannerImpl.this.fEntityScanner.skipChar(47)) {
                                XMLDocumentScannerImpl.this.reportFatalError("MarkupNotRecognizedInMisc", null);
                                bl2 = true;
                                break;
                            }
                            if (XMLDocumentScannerImpl.this.isValidNameStartChar(XMLDocumentScannerImpl.this.fEntityScanner.peekChar())) {
                                XMLDocumentScannerImpl.this.reportFatalError("MarkupNotRecognizedInMisc", null);
                                XMLDocumentScannerImpl.this.scanStartElement();
                                XMLDocumentScannerImpl.this.setScannerState(7);
                                break;
                            }
                            if (XMLDocumentScannerImpl.this.isValidNameStartHighSurrogate(XMLDocumentScannerImpl.this.fEntityScanner.peekChar())) {
                                XMLDocumentScannerImpl.this.reportFatalError("MarkupNotRecognizedInMisc", null);
                                XMLDocumentScannerImpl.this.scanStartElement();
                                XMLDocumentScannerImpl.this.setScannerState(7);
                                break;
                            }
                            XMLDocumentScannerImpl.this.reportFatalError("MarkupNotRecognizedInMisc", null);
                            break;
                        }
                        case 3: {
                            XMLDocumentScannerImpl.this.scanPI();
                            XMLDocumentScannerImpl.this.setScannerState(12);
                            break;
                        }
                        case 2: {
                            if (!XMLDocumentScannerImpl.this.fEntityScanner.skipString("--")) {
                                XMLDocumentScannerImpl.this.reportFatalError("InvalidCommentStart", null);
                            }
                            XMLDocumentScannerImpl.this.scanComment();
                            XMLDocumentScannerImpl.this.setScannerState(12);
                            break;
                        }
                        case 7: {
                            int n = XMLDocumentScannerImpl.this.fEntityScanner.peekChar();
                            if (n == -1) {
                                XMLDocumentScannerImpl.this.setScannerState(14);
                                return false;
                            }
                            XMLDocumentScannerImpl.this.reportFatalError("ContentIllegalInTrailingMisc", null);
                            XMLDocumentScannerImpl.this.fEntityScanner.scanChar();
                            XMLDocumentScannerImpl.this.setScannerState(12);
                            break;
                        }
                        case 8: {
                            XMLDocumentScannerImpl.this.reportFatalError("ReferenceIllegalInTrailingMisc", null);
                            XMLDocumentScannerImpl.this.setScannerState(12);
                            break;
                        }
                        case 14: {
                            return false;
                        }
                    }
                } while (bl || bl2);
            }
            catch (MalformedByteSequenceException var2_3) {
                XMLDocumentScannerImpl.this.fErrorReporter.reportError(var2_3.getDomain(), var2_3.getKey(), var2_3.getArguments(), 2, var2_3);
                return false;
            }
            catch (CharConversionException var3_5) {
                XMLDocumentScannerImpl.this.fErrorReporter.reportError("http://www.w3.org/TR/1998/REC-xml-19980210", "CharConversionFailure", null, 2, var3_5);
                return false;
            }
            catch (EOFException var4_6) {
                if (XMLDocumentScannerImpl.this.fMarkupDepth != 0) {
                    XMLDocumentScannerImpl.this.reportFatalError("PrematureEOF", null);
                    return false;
                }
                XMLDocumentScannerImpl.this.setScannerState(14);
                return false;
            }
            return true;
        }
    }

    protected final class XMLDeclDispatcher
    implements XMLDocumentFragmentScannerImpl.Dispatcher {
        protected XMLDeclDispatcher() {
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Lifted jumps to return sites
         */
        public boolean dispatch(boolean var1_1) throws IOException, XNIException {
            XMLDocumentScannerImpl.this.setScannerState(5);
            XMLDocumentScannerImpl.this.setDispatcher(XMLDocumentScannerImpl.this.fPrologDispatcher);
            try {
                if (!XMLDocumentScannerImpl.this.fEntityScanner.skipString("<?xml")) ** GOTO lbl12
                ++XMLDocumentScannerImpl.this.fMarkupDepth;
                if (!XMLChar.isName(XMLDocumentScannerImpl.this.fEntityScanner.peekChar())) ** GOTO lbl11
                XMLDocumentScannerImpl.access$000(XMLDocumentScannerImpl.this).clear();
                XMLDocumentScannerImpl.access$000(XMLDocumentScannerImpl.this).append("xml");
                if (!XMLDocumentScannerImpl.this.fNamespaces) ** GOTO lbl28
                ** GOTO lbl25
lbl11: // 1 sources:
                XMLDocumentScannerImpl.this.scanXMLDeclOrTextDecl(false);
lbl12: // 3 sources:
                do {
                    XMLDocumentScannerImpl.this.fEntityManager.fCurrentEntity.mayReadChunks = true;
                    return true;
                    break;
                } while (true);
            }
            catch (MalformedByteSequenceException var2_3) {
                XMLDocumentScannerImpl.this.fErrorReporter.reportError(var2_3.getDomain(), var2_3.getKey(), var2_3.getArguments(), 2, var2_3);
                return false;
            }
            catch (CharConversionException var3_4) {
                XMLDocumentScannerImpl.this.fErrorReporter.reportError("http://www.w3.org/TR/1998/REC-xml-19980210", "CharConversionFailure", null, 2, var3_4);
                return false;
            }
            catch (EOFException var4_5) {
                XMLDocumentScannerImpl.this.reportFatalError("PrematureEOF", null);
                return false;
            }
lbl-1000: // 1 sources:
            {
                XMLDocumentScannerImpl.access$000(XMLDocumentScannerImpl.this).append((char)XMLDocumentScannerImpl.this.fEntityScanner.scanChar());
lbl25: // 2 sources:
                ** while (XMLChar.isNCName((int)XMLDocumentScannerImpl.this.fEntityScanner.peekChar()))
            }
lbl26: // 1 sources:
            ** GOTO lbl29
lbl-1000: // 1 sources:
            {
                XMLDocumentScannerImpl.access$000(XMLDocumentScannerImpl.this).append((char)XMLDocumentScannerImpl.this.fEntityScanner.scanChar());
lbl28: // 2 sources:
                ** while (XMLChar.isName((int)XMLDocumentScannerImpl.this.fEntityScanner.peekChar()))
            }
lbl29: // 2 sources:
            var2_2 = XMLDocumentScannerImpl.this.fSymbolTable.addSymbol(XMLDocumentScannerImpl.access$000((XMLDocumentScannerImpl)XMLDocumentScannerImpl.this).ch, XMLDocumentScannerImpl.access$000((XMLDocumentScannerImpl)XMLDocumentScannerImpl.this).offset, XMLDocumentScannerImpl.access$000((XMLDocumentScannerImpl)XMLDocumentScannerImpl.this).length);
            XMLDocumentScannerImpl.this.scanPIData(var2_2, XMLDocumentScannerImpl.access$100(XMLDocumentScannerImpl.this));
            ** while (true)
        }
    }

}

