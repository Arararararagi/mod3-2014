/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.stax.events;

import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.DTD;
import org.apache.xerces.stax.events.XMLEventImpl;

public final class DTDImpl
extends XMLEventImpl
implements DTD {
    private final String fDTD;

    public DTDImpl(String string, Location location) {
        super(11, location);
        this.fDTD = string != null ? string : null;
    }

    public String getDocumentTypeDeclaration() {
        return this.fDTD;
    }

    public Object getProcessedDTD() {
        return null;
    }

    public List getNotations() {
        return Collections.EMPTY_LIST;
    }

    public List getEntities() {
        return Collections.EMPTY_LIST;
    }

    public void writeAsEncodedUnicode(Writer writer) throws XMLStreamException {
        try {
            writer.write(this.fDTD);
        }
        catch (IOException var2_2) {
            throw new XMLStreamException(var2_2);
        }
    }
}

