/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.stax.events;

import java.io.IOException;
import java.io.Writer;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EntityDeclaration;
import javax.xml.stream.events.EntityReference;
import org.apache.xerces.stax.events.XMLEventImpl;

public final class EntityReferenceImpl
extends XMLEventImpl
implements EntityReference {
    private final String fName;
    private final EntityDeclaration fDecl;

    public EntityReferenceImpl(EntityDeclaration entityDeclaration, Location location) {
        this(entityDeclaration != null ? entityDeclaration.getName() : "", entityDeclaration, location);
    }

    public EntityReferenceImpl(String string, EntityDeclaration entityDeclaration, Location location) {
        super(9, location);
        this.fName = string != null ? string : "";
        this.fDecl = entityDeclaration;
    }

    public EntityDeclaration getDeclaration() {
        return this.fDecl;
    }

    public String getName() {
        return this.fName;
    }

    public void writeAsEncodedUnicode(Writer writer) throws XMLStreamException {
        try {
            writer.write(38);
            writer.write(this.fName);
            writer.write(59);
        }
        catch (IOException var2_2) {
            throw new XMLStreamException(var2_2);
        }
    }
}

