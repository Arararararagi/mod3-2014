/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xs;

import java.util.List;

public interface StringList
extends List {
    public int getLength();

    public boolean contains(String var1);

    public String item(int var1);
}

