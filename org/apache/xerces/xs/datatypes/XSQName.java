/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xs.datatypes;

import javax.xml.namespace.QName;

public interface XSQName {
    public org.apache.xerces.xni.QName getXNIQName();

    public QName getJAXPQName();
}

