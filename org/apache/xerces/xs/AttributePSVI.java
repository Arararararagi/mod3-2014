/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.xs;

import org.apache.xerces.xs.ItemPSVI;
import org.apache.xerces.xs.XSAttributeDeclaration;

public interface AttributePSVI
extends ItemPSVI {
    public XSAttributeDeclaration getAttributeDeclaration();
}

