/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.dom3.as;

import org.apache.xerces.dom3.as.NodeEditAS;

public interface DocumentEditAS
extends NodeEditAS {
    public boolean getContinuousValidityChecking();

    public void setContinuousValidityChecking(boolean var1);
}

