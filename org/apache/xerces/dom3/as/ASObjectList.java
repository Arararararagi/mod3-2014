/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.dom3.as;

import org.apache.xerces.dom3.as.ASObject;

public interface ASObjectList {
    public int getLength();

    public ASObject item(int var1);
}

