/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.util;

public class SymbolHash {
    protected int fTableSize = 101;
    protected Entry[] fBuckets;
    protected int fNum = 0;

    public SymbolHash() {
        this.fBuckets = new Entry[this.fTableSize];
    }

    public SymbolHash(int n) {
        this.fTableSize = n;
        this.fBuckets = new Entry[this.fTableSize];
    }

    public void put(Object object, Object object2) {
        int n = (object.hashCode() & Integer.MAX_VALUE) % this.fTableSize;
        Entry entry = this.search(object, n);
        if (entry != null) {
            entry.value = object2;
        } else {
            this.fBuckets[n] = entry = new Entry(object, object2, this.fBuckets[n]);
            ++this.fNum;
        }
    }

    public Object get(Object object) {
        int n = (object.hashCode() & Integer.MAX_VALUE) % this.fTableSize;
        Entry entry = this.search(object, n);
        if (entry != null) {
            return entry.value;
        }
        return null;
    }

    public int getLength() {
        return this.fNum;
    }

    public int getValues(Object[] arrobject, int n) {
        int n2 = 0;
        for (int i = 0; i < this.fTableSize && n2 < this.fNum; ++i) {
            Entry entry = this.fBuckets[i];
            while (entry != null) {
                arrobject[n + n2] = entry.value;
                ++n2;
                entry = entry.next;
            }
        }
        return this.fNum;
    }

    public Object[] getEntries() {
        Object[] arrobject = new Object[this.fNum << 1];
        int n = 0;
        for (int i = 0; i < this.fTableSize && n < this.fNum << 1; ++i) {
            Entry entry = this.fBuckets[i];
            while (entry != null) {
                arrobject[n] = entry.key;
                arrobject[++n] = entry.value;
                ++n;
                entry = entry.next;
            }
        }
        return arrobject;
    }

    public SymbolHash makeClone() {
        SymbolHash symbolHash = new SymbolHash(this.fTableSize);
        symbolHash.fNum = this.fNum;
        for (int i = 0; i < this.fTableSize; ++i) {
            if (this.fBuckets[i] == null) continue;
            symbolHash.fBuckets[i] = this.fBuckets[i].makeClone();
        }
        return symbolHash;
    }

    public void clear() {
        for (int i = 0; i < this.fTableSize; ++i) {
            this.fBuckets[i] = null;
        }
        this.fNum = 0;
    }

    protected Entry search(Object object, int n) {
        Entry entry = this.fBuckets[n];
        while (entry != null) {
            if (object.equals(entry.key)) {
                return entry;
            }
            entry = entry.next;
        }
        return null;
    }

    protected static final class Entry {
        public Object key;
        public Object value;
        public Entry next;

        public Entry() {
            this.key = null;
            this.value = null;
            this.next = null;
        }

        public Entry(Object object, Object object2, Entry entry) {
            this.key = object;
            this.value = object2;
            this.next = entry;
        }

        public Entry makeClone() {
            Entry entry = new Entry();
            entry.key = this.key;
            entry.value = this.value;
            if (this.next != null) {
                entry.next = this.next.makeClone();
            }
            return entry;
        }
    }

}

