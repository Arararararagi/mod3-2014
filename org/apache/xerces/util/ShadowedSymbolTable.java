/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.util;

import org.apache.xerces.util.SymbolTable;

public final class ShadowedSymbolTable
extends SymbolTable {
    protected SymbolTable fSymbolTable;

    public ShadowedSymbolTable(SymbolTable symbolTable) {
        this.fSymbolTable = symbolTable;
    }

    public String addSymbol(String string) {
        if (this.fSymbolTable.containsSymbol(string)) {
            return this.fSymbolTable.addSymbol(string);
        }
        return super.addSymbol(string);
    }

    public String addSymbol(char[] arrc, int n, int n2) {
        if (this.fSymbolTable.containsSymbol(arrc, n, n2)) {
            return this.fSymbolTable.addSymbol(arrc, n, n2);
        }
        return super.addSymbol(arrc, n, n2);
    }

    public int hash(String string) {
        return this.fSymbolTable.hash(string);
    }

    public int hash(char[] arrc, int n, int n2) {
        return this.fSymbolTable.hash(arrc, n, n2);
    }
}

