/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.util;

import org.apache.xerces.util.SymbolTable;

public class SoftReferenceSymbolTable
extends SymbolTable {
    protected SREntry[] fBuckets = null;
    private final ReferenceQueue fReferenceQueue;

    public SoftReferenceSymbolTable(int n, float f) {
        if (n < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + n);
        }
        if (f <= 0.0f || Float.isNaN(f)) {
            throw new IllegalArgumentException("Illegal Load: " + f);
        }
        if (n == 0) {
            n = 1;
        }
        this.fLoadFactor = f;
        this.fTableSize = n;
        this.fBuckets = new SREntry[this.fTableSize];
        this.fThreshold = (int)((float)this.fTableSize * f);
        this.fCount = 0;
        this.fReferenceQueue = new ReferenceQueue();
    }

    public SoftReferenceSymbolTable(int n) {
        this(n, 0.75f);
    }

    public SoftReferenceSymbolTable() {
        this(101, 0.75f);
    }

    public String addSymbol(String string) {
        Object object;
        this.clean();
        int n = this.hash(string) % this.fTableSize;
        SREntry sREntry = this.fBuckets[n];
        while (sREntry != null) {
            object = (SREntryData)sREntry.get();
            if (object != null && object.symbol.equals(string)) {
                return object.symbol;
            }
            sREntry = sREntry.next;
        }
        if (this.fCount >= this.fThreshold) {
            this.rehash();
            n = this.hash(string) % this.fTableSize;
        }
        string = string.intern();
        this.fBuckets[n] = object = new SREntry(string, this.fBuckets[n], n, this.fReferenceQueue);
        ++this.fCount;
        return string;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public String addSymbol(char[] var1_1, int var2_2, int var3_3) {
        this.clean();
        var4_4 = this.hash(var1_1, var2_2, var3_3) % this.fTableSize;
        var5_5 = this.fBuckets[var4_4];
        while (var5_5 != null) {
            var6_6 = (SREntryData)var5_5.get();
            if (var6_6 != null && var3_3 == var6_6.characters.length) {
                for (var7_7 = 0; var7_7 < var3_3; ++var7_7) {
                    if (var1_1[var2_2 + var7_7] == var6_6.characters[var7_7]) {
                        continue;
                    } else {
                        ** GOTO lbl11
                    }
                }
                return var6_6.symbol;
            }
lbl11: // 4 sources:
            var5_5 = var5_5.next;
        }
        if (this.fCount >= this.fThreshold) {
            this.rehash();
            var4_4 = this.hash(var1_1, var2_2, var3_3) % this.fTableSize;
        }
        var6_6 = new String(var1_1, var2_2, var3_3).intern();
        this.fBuckets[var4_4] = var7_8 = new SREntry((String)var6_6, var1_1, var2_2, var3_3, this.fBuckets[var4_4], var4_4, this.fReferenceQueue);
        ++this.fCount;
        return var6_6;
    }

    protected void rehash() {
        int n = this.fBuckets.length;
        SREntry[] arrsREntry = this.fBuckets;
        int n2 = n * 2 + 1;
        SREntry[] arrsREntry2 = new SREntry[n2];
        this.fThreshold = (int)((float)n2 * this.fLoadFactor);
        this.fBuckets = arrsREntry2;
        this.fTableSize = this.fBuckets.length;
        int n3 = n;
        while (n3-- > 0) {
            SREntry sREntry = arrsREntry[n3];
            while (sREntry != null) {
                SREntry sREntry2 = sREntry;
                sREntry = sREntry.next;
                SREntryData sREntryData = (SREntryData)sREntry2.get();
                if (sREntryData != null) {
                    int n4 = this.hash(sREntryData.characters, 0, sREntryData.characters.length) % n2;
                    if (arrsREntry2[n4] != null) {
                        arrsREntry2[n4].prev = sREntry2;
                    }
                    sREntry2.next = arrsREntry2[n4];
                    sREntry2.prev = null;
                    arrsREntry2[n4] = sREntry2;
                    continue;
                }
                --this.fCount;
            }
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public boolean containsSymbol(String var1_1) {
        var2_2 = this.hash(var1_1) % this.fTableSize;
        var3_3 = var1_1.length();
        var4_4 = this.fBuckets[var2_2];
        while (var4_4 != null) {
            var5_5 = (SREntryData)var4_4.get();
            if (var5_5 != null && var3_3 == var5_5.characters.length) {
                for (var6_6 = 0; var6_6 < var3_3; ++var6_6) {
                    if (var1_1.charAt(var6_6) == var5_5.characters[var6_6]) {
                        continue;
                    } else {
                        ** GOTO lbl11
                    }
                }
                return true;
            }
lbl11: // 4 sources:
            var4_4 = var4_4.next;
        }
        return false;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public boolean containsSymbol(char[] var1_1, int var2_2, int var3_3) {
        var4_4 = this.hash(var1_1, var2_2, var3_3) % this.fTableSize;
        var5_5 = this.fBuckets[var4_4];
        while (var5_5 != null) {
            var6_6 = (SREntryData)var5_5.get();
            if (var6_6 != null && var3_3 == var6_6.characters.length) {
                for (var7_7 = 0; var7_7 < var3_3; ++var7_7) {
                    if (var1_1[var2_2 + var7_7] == var6_6.characters[var7_7]) {
                        continue;
                    } else {
                        ** GOTO lbl10
                    }
                }
                return true;
            }
lbl10: // 4 sources:
            var5_5 = var5_5.next;
        }
        return false;
    }

    private void removeEntry(SREntry sREntry) {
        if (sREntry.next != null) {
            sREntry.next.prev = sREntry.prev;
        }
        if (sREntry.prev != null) {
            sREntry.prev.next = sREntry.next;
        } else {
            this.fBuckets[sREntry.bucket] = sREntry.next;
        }
        --this.fCount;
    }

    private void clean() {
        SREntry sREntry = (SREntry)this.fReferenceQueue.poll();
        while (sREntry != null) {
            this.removeEntry(sREntry);
            sREntry = (SREntry)this.fReferenceQueue.poll();
        }
    }

    protected static final class SREntry
    extends SoftReference {
        public SREntry next;
        public SREntry prev;
        public int bucket;

        public SREntry(String string, SREntry sREntry, int n, ReferenceQueue referenceQueue) {
            super(new SREntryData(string), referenceQueue);
            this.initialize(sREntry, n);
        }

        public SREntry(String string, char[] arrc, int n, int n2, SREntry sREntry, int n3, ReferenceQueue referenceQueue) {
            super(new SREntryData(string, arrc, n, n2), referenceQueue);
            this.initialize(sREntry, n3);
        }

        private void initialize(SREntry sREntry, int n) {
            this.next = sREntry;
            if (sREntry != null) {
                sREntry.prev = this;
            }
            this.prev = null;
            this.bucket = n;
        }
    }

    protected static final class SREntryData {
        public final String symbol;
        public final char[] characters;

        public SREntryData(String string) {
            this.symbol = string;
            this.characters = new char[this.symbol.length()];
            this.symbol.getChars(0, this.characters.length, this.characters, 0);
        }

        public SREntryData(String string, char[] arrc, int n, int n2) {
            this.symbol = string;
            this.characters = new char[n2];
            System.arraycopy(arrc, n, this.characters, 0, n2);
        }
    }

}

