/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.util;

public class SymbolTable {
    protected static final int TABLE_SIZE = 101;
    protected Entry[] fBuckets = null;
    protected int fTableSize;
    protected transient int fCount;
    protected int fThreshold;
    protected float fLoadFactor;

    public SymbolTable(int n, float f) {
        if (n < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + n);
        }
        if (f <= 0.0f || Float.isNaN(f)) {
            throw new IllegalArgumentException("Illegal Load: " + f);
        }
        if (n == 0) {
            n = 1;
        }
        this.fLoadFactor = f;
        this.fTableSize = n;
        this.fBuckets = new Entry[this.fTableSize];
        this.fThreshold = (int)((float)this.fTableSize * f);
        this.fCount = 0;
    }

    public SymbolTable(int n) {
        this(n, 0.75f);
    }

    public SymbolTable() {
        this(101, 0.75f);
    }

    public String addSymbol(String string) {
        Entry entry;
        int n = this.hash(string) % this.fTableSize;
        Entry entry2 = this.fBuckets[n];
        while (entry2 != null) {
            if (entry2.symbol.equals(string)) {
                return entry2.symbol;
            }
            entry2 = entry2.next;
        }
        if (this.fCount >= this.fThreshold) {
            this.rehash();
            n = this.hash(string) % this.fTableSize;
        }
        this.fBuckets[n] = entry = new Entry(string, this.fBuckets[n]);
        ++this.fCount;
        return entry.symbol;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public String addSymbol(char[] var1_1, int var2_2, int var3_3) {
        var4_4 = this.hash(var1_1, var2_2, var3_3) % this.fTableSize;
        var5_5 = this.fBuckets[var4_4];
        while (var5_5 != null) {
            if (var3_3 == var5_5.characters.length) {
                for (var6_6 = 0; var6_6 < var3_3; ++var6_6) {
                    if (var1_1[var2_2 + var6_6] == var5_5.characters[var6_6]) {
                        continue;
                    } else {
                        ** GOTO lbl9
                    }
                }
                return var5_5.symbol;
            }
lbl9: // 4 sources:
            var5_5 = var5_5.next;
        }
        if (this.fCount >= this.fThreshold) {
            this.rehash();
            var4_4 = this.hash(var1_1, var2_2, var3_3) % this.fTableSize;
        }
        this.fBuckets[var4_4] = var6_7 = new Entry(var1_1, var2_2, var3_3, this.fBuckets[var4_4]);
        ++this.fCount;
        return var6_7.symbol;
    }

    public int hash(String string) {
        return string.hashCode() & 134217727;
    }

    public int hash(char[] arrc, int n, int n2) {
        int n3 = 0;
        for (int i = 0; i < n2; ++i) {
            n3 = n3 * 31 + arrc[n + i];
        }
        return n3 & 134217727;
    }

    protected void rehash() {
        int n = this.fBuckets.length;
        Entry[] arrentry = this.fBuckets;
        int n2 = n * 2 + 1;
        Entry[] arrentry2 = new Entry[n2];
        this.fThreshold = (int)((float)n2 * this.fLoadFactor);
        this.fBuckets = arrentry2;
        this.fTableSize = this.fBuckets.length;
        int n3 = n;
        while (n3-- > 0) {
            Entry entry = arrentry[n3];
            while (entry != null) {
                Entry entry2 = entry;
                entry = entry.next;
                int n4 = this.hash(entry2.characters, 0, entry2.characters.length) % n2;
                entry2.next = arrentry2[n4];
                arrentry2[n4] = entry2;
            }
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public boolean containsSymbol(String var1_1) {
        var2_2 = this.hash(var1_1) % this.fTableSize;
        var3_3 = var1_1.length();
        var4_4 = this.fBuckets[var2_2];
        while (var4_4 != null) {
            if (var3_3 == var4_4.characters.length) {
                for (var5_5 = 0; var5_5 < var3_3; ++var5_5) {
                    if (var1_1.charAt(var5_5) == var4_4.characters[var5_5]) {
                        continue;
                    } else {
                        ** GOTO lbl10
                    }
                }
                return true;
            }
lbl10: // 4 sources:
            var4_4 = var4_4.next;
        }
        return false;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public boolean containsSymbol(char[] var1_1, int var2_2, int var3_3) {
        var4_4 = this.hash(var1_1, var2_2, var3_3) % this.fTableSize;
        var5_5 = this.fBuckets[var4_4];
        while (var5_5 != null) {
            if (var3_3 == var5_5.characters.length) {
                for (var6_6 = 0; var6_6 < var3_3; ++var6_6) {
                    if (var1_1[var2_2 + var6_6] == var5_5.characters[var6_6]) {
                        continue;
                    } else {
                        ** GOTO lbl9
                    }
                }
                return true;
            }
lbl9: // 4 sources:
            var5_5 = var5_5.next;
        }
        return false;
    }

    protected static final class Entry {
        public final String symbol;
        public final char[] characters;
        public Entry next;

        public Entry(String string, Entry entry) {
            this.symbol = string.intern();
            this.characters = new char[string.length()];
            string.getChars(0, this.characters.length, this.characters, 0);
            this.next = entry;
        }

        public Entry(char[] arrc, int n, int n2, Entry entry) {
            this.characters = new char[n2];
            System.arraycopy(arrc, n, this.characters, 0, n2);
            this.symbol = new String(this.characters).intern();
            this.next = entry;
        }
    }

}

