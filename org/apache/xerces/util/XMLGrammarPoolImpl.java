/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xerces.util;

import org.apache.xerces.xni.grammars.Grammar;
import org.apache.xerces.xni.grammars.XMLGrammarDescription;
import org.apache.xerces.xni.grammars.XMLGrammarPool;

public class XMLGrammarPoolImpl
implements XMLGrammarPool {
    protected static final int TABLE_SIZE = 11;
    protected Entry[] fGrammars = null;
    protected boolean fPoolIsLocked;
    protected int fGrammarCount = 0;
    private static final boolean DEBUG = false;

    public XMLGrammarPoolImpl() {
        this.fGrammars = new Entry[11];
        this.fPoolIsLocked = false;
    }

    public XMLGrammarPoolImpl(int n) {
        this.fGrammars = new Entry[n];
        this.fPoolIsLocked = false;
    }

    public Grammar[] retrieveInitialGrammarSet(String string) {
        Entry[] arrentry = this.fGrammars;
        synchronized (arrentry) {
            Object object;
            int n = this.fGrammars.length;
            Grammar[] arrgrammar = new Grammar[this.fGrammarCount];
            int n2 = 0;
            for (int i = 0; i < n; ++i) {
                object = this.fGrammars[i];
                while (object != null) {
                    if (object.desc.getGrammarType().equals(string)) {
                        arrgrammar[n2++] = object.grammar;
                    }
                    object = object.next;
                }
            }
            object = new Grammar[n2];
            System.arraycopy(arrgrammar, 0, object, 0, n2);
            Object object2 = object;
            return object2;
        }
    }

    public void cacheGrammars(String string, Grammar[] arrgrammar) {
        if (!this.fPoolIsLocked) {
            for (int i = 0; i < arrgrammar.length; ++i) {
                this.putGrammar(arrgrammar[i]);
            }
        }
    }

    public Grammar retrieveGrammar(XMLGrammarDescription xMLGrammarDescription) {
        return this.getGrammar(xMLGrammarDescription);
    }

    public void putGrammar(Grammar grammar) {
        if (!this.fPoolIsLocked) {
            Entry[] arrentry = this.fGrammars;
            synchronized (arrentry) {
                Entry entry;
                XMLGrammarDescription xMLGrammarDescription = grammar.getGrammarDescription();
                int n = this.hashCode(xMLGrammarDescription);
                int n2 = (n & Integer.MAX_VALUE) % this.fGrammars.length;
                Entry entry2 = this.fGrammars[n2];
                while (entry2 != null) {
                    if (entry2.hash == n && this.equals(entry2.desc, xMLGrammarDescription)) {
                        entry2.grammar = grammar;
                        return;
                    }
                    entry2 = entry2.next;
                }
                this.fGrammars[n2] = entry = new Entry(n, xMLGrammarDescription, grammar, this.fGrammars[n2]);
                ++this.fGrammarCount;
            }
        }
    }

    public Grammar getGrammar(XMLGrammarDescription xMLGrammarDescription) {
        Entry[] arrentry = this.fGrammars;
        synchronized (arrentry) {
            int n = this.hashCode(xMLGrammarDescription);
            int n2 = (n & Integer.MAX_VALUE) % this.fGrammars.length;
            Entry entry = this.fGrammars[n2];
            while (entry != null) {
                if (entry.hash == n && this.equals(entry.desc, xMLGrammarDescription)) {
                    Grammar grammar = entry.grammar;
                    return grammar;
                }
                entry = entry.next;
            }
            Grammar grammar = null;
            return grammar;
        }
    }

    public Grammar removeGrammar(XMLGrammarDescription xMLGrammarDescription) {
        Entry[] arrentry = this.fGrammars;
        synchronized (arrentry) {
            int n = this.hashCode(xMLGrammarDescription);
            int n2 = (n & Integer.MAX_VALUE) % this.fGrammars.length;
            Entry entry = this.fGrammars[n2];
            Entry entry2 = null;
            while (entry != null) {
                if (entry.hash == n && this.equals(entry.desc, xMLGrammarDescription)) {
                    if (entry2 != null) {
                        entry2.next = entry.next;
                    } else {
                        this.fGrammars[n2] = entry.next;
                    }
                    Grammar grammar = entry.grammar;
                    entry.grammar = null;
                    --this.fGrammarCount;
                    Grammar grammar2 = grammar;
                    return grammar2;
                }
                entry2 = entry;
                entry = entry.next;
            }
            Grammar grammar = null;
            return grammar;
        }
    }

    public boolean containsGrammar(XMLGrammarDescription xMLGrammarDescription) {
        Entry[] arrentry = this.fGrammars;
        synchronized (arrentry) {
            int n = this.hashCode(xMLGrammarDescription);
            int n2 = (n & Integer.MAX_VALUE) % this.fGrammars.length;
            Entry entry = this.fGrammars[n2];
            while (entry != null) {
                if (entry.hash == n && this.equals(entry.desc, xMLGrammarDescription)) {
                    boolean bl = true;
                    return bl;
                }
                entry = entry.next;
            }
            boolean bl = false;
            return bl;
        }
    }

    public void lockPool() {
        this.fPoolIsLocked = true;
    }

    public void unlockPool() {
        this.fPoolIsLocked = false;
    }

    public void clear() {
        for (int i = 0; i < this.fGrammars.length; ++i) {
            if (this.fGrammars[i] == null) continue;
            this.fGrammars[i].clear();
            this.fGrammars[i] = null;
        }
        this.fGrammarCount = 0;
    }

    public boolean equals(XMLGrammarDescription xMLGrammarDescription, XMLGrammarDescription xMLGrammarDescription2) {
        return xMLGrammarDescription.equals(xMLGrammarDescription2);
    }

    public int hashCode(XMLGrammarDescription xMLGrammarDescription) {
        return xMLGrammarDescription.hashCode();
    }

    protected static final class Entry {
        public int hash;
        public XMLGrammarDescription desc;
        public Grammar grammar;
        public Entry next;

        protected Entry(int n, XMLGrammarDescription xMLGrammarDescription, Grammar grammar, Entry entry) {
            this.hash = n;
            this.desc = xMLGrammarDescription;
            this.grammar = grammar;
            this.next = entry;
        }

        protected void clear() {
            this.desc = null;
            this.grammar = null;
            if (this.next != null) {
                this.next.clear();
                this.next = null;
            }
        }
    }

}

