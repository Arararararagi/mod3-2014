/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.w3c.dom.Element;

public interface WMLElement
extends Element {
    public void setId(String var1);

    public String getId();

    public void setClassName(String var1);

    public String getClassName();
}

