/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLFieldsetElement
extends WMLElement {
    public void setTitle(String var1);

    public String getTitle();

    public void setXmlLang(String var1);

    public String getXmlLang();
}

