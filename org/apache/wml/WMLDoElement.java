/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLDoElement
extends WMLElement {
    public void setOptional(String var1);

    public String getOptional();

    public void setLabel(String var1);

    public String getLabel();

    public void setType(String var1);

    public String getType();

    public void setName(String var1);

    public String getName();

    public void setXmlLang(String var1);

    public String getXmlLang();
}

