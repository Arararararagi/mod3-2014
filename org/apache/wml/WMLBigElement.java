/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLBigElement
extends WMLElement {
    public void setXmlLang(String var1);

    public String getXmlLang();
}

