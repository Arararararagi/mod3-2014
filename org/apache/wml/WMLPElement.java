/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLPElement
extends WMLElement {
    public void setMode(String var1);

    public String getMode();

    public void setAlign(String var1);

    public String getAlign();

    public void setXmlLang(String var1);

    public String getXmlLang();
}

