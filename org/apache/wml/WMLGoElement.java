/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLGoElement
extends WMLElement {
    public void setSendreferer(String var1);

    public String getSendreferer();

    public void setAcceptCharset(String var1);

    public String getAcceptCharset();

    public void setHref(String var1);

    public String getHref();

    public void setMethod(String var1);

    public String getMethod();
}

