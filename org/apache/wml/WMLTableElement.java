/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLTableElement
extends WMLElement {
    public void setTitle(String var1);

    public String getTitle();

    public void setAlign(String var1);

    public String getAlign();

    public void setColumns(int var1);

    public int getColumns();

    public void setXmlLang(String var1);

    public String getXmlLang();
}

