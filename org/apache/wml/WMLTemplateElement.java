/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLTemplateElement
extends WMLElement {
    public void setOnTimer(String var1);

    public String getOnTimer();

    public void setOnEnterBackward(String var1);

    public String getOnEnterBackward();

    public void setOnEnterForward(String var1);

    public String getOnEnterForward();
}

