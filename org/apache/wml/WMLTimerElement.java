/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml;

import org.apache.wml.WMLElement;

public interface WMLTimerElement
extends WMLElement {
    public void setName(String var1);

    public String getName();

    public void setValue(String var1);

    public String getValue();
}

