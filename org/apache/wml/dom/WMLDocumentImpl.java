/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml.dom;

import java.io.PrintStream;
import java.util.Hashtable;
import org.apache.wml.WMLDocument;
import org.apache.wml.dom.WMLElementImpl;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xerces.dom.ElementImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

public class WMLDocumentImpl
extends DocumentImpl
implements WMLDocument {
    private static final long serialVersionUID = -6582904849512384104L;
    private static Hashtable _elementTypesWML;
    private static final Class[] _elemClassSigWML;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLDocumentImpl;
    static /* synthetic */ Class class$java$lang$String;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLBElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLNoopElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLAElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLSetvarElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLAccessElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLStrongElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLPostfieldElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLDoElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLWmlElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLTrElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLGoElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLBigElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLAnchorElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLTimerElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLSmallElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLOptgroupElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLHeadElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLTdElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLFieldsetElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLImgElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLRefreshElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLOneventElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLInputElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLPrevElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLTableElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLMetaElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLTemplateElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLBrElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLOptionElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLUElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLPElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLSelectElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLEmElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLIElementImpl;
    static /* synthetic */ Class class$org$apache$wml$dom$WMLCardElementImpl;

    public Element createElement(String string) throws DOMException {
        Class class_ = (Class)_elementTypesWML.get(string);
        if (class_ != null) {
            try {
                Constructor constructor = class_.getConstructor(_elemClassSigWML);
                return (Element)constructor.newInstance(this, string);
            }
            catch (Exception var4_4 /* !! */ ) {
                Object object = var4_4 /* !! */  instanceof InvocationTargetException ? ((InvocationTargetException)var4_4 /* !! */ ).getTargetException() : var4_4 /* !! */ ;
                System.out.println("Exception " + object.getClass().getName());
                System.out.println(object.getMessage());
                throw new IllegalStateException("Tag '" + string + "' associated with an Element class that failed to construct.");
            }
        }
        return new WMLElementImpl(this, string);
    }

    protected boolean canRenameElements(String string, String string2, ElementImpl elementImpl) {
        return _elementTypesWML.get(string2) == _elementTypesWML.get(elementImpl.getTagName());
    }

    public WMLDocumentImpl(DocumentType documentType) {
        super(documentType, false);
    }

    static /* synthetic */ Class class$(String string) {
        try {
            return Class.forName(string);
        }
        catch (ClassNotFoundException var1_1) {
            throw new NoClassDefFoundError(var1_1.getMessage());
        }
    }

    static {
        Class[] arrclass = new Class[2];
        Class class_ = class$org$apache$wml$dom$WMLDocumentImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLDocumentImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLDocumentImpl")) : class$org$apache$wml$dom$WMLDocumentImpl;
        arrclass[0] = class_;
        Class class_2 = class$java$lang$String == null ? (WMLDocumentImpl.class$java$lang$String = WMLDocumentImpl.class$("java.lang.String")) : class$java$lang$String;
        arrclass[1] = class_2;
        _elemClassSigWML = arrclass;
        _elementTypesWML = new Hashtable();
        Class class_3 = class$org$apache$wml$dom$WMLBElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLBElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLBElementImpl")) : class$org$apache$wml$dom$WMLBElementImpl;
        _elementTypesWML.put("b", class_3);
        Class class_4 = class$org$apache$wml$dom$WMLNoopElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLNoopElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLNoopElementImpl")) : class$org$apache$wml$dom$WMLNoopElementImpl;
        _elementTypesWML.put("noop", class_4);
        Class class_5 = class$org$apache$wml$dom$WMLAElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLAElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLAElementImpl")) : class$org$apache$wml$dom$WMLAElementImpl;
        _elementTypesWML.put("a", class_5);
        Class class_6 = class$org$apache$wml$dom$WMLSetvarElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLSetvarElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLSetvarElementImpl")) : class$org$apache$wml$dom$WMLSetvarElementImpl;
        _elementTypesWML.put("setvar", class_6);
        Class class_7 = class$org$apache$wml$dom$WMLAccessElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLAccessElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLAccessElementImpl")) : class$org$apache$wml$dom$WMLAccessElementImpl;
        _elementTypesWML.put("access", class_7);
        Class class_8 = class$org$apache$wml$dom$WMLStrongElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLStrongElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLStrongElementImpl")) : class$org$apache$wml$dom$WMLStrongElementImpl;
        _elementTypesWML.put("strong", class_8);
        Class class_9 = class$org$apache$wml$dom$WMLPostfieldElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLPostfieldElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLPostfieldElementImpl")) : class$org$apache$wml$dom$WMLPostfieldElementImpl;
        _elementTypesWML.put("postfield", class_9);
        Class class_10 = class$org$apache$wml$dom$WMLDoElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLDoElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLDoElementImpl")) : class$org$apache$wml$dom$WMLDoElementImpl;
        _elementTypesWML.put("do", class_10);
        Class class_11 = class$org$apache$wml$dom$WMLWmlElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLWmlElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLWmlElementImpl")) : class$org$apache$wml$dom$WMLWmlElementImpl;
        _elementTypesWML.put("wml", class_11);
        Class class_12 = class$org$apache$wml$dom$WMLTrElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLTrElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLTrElementImpl")) : class$org$apache$wml$dom$WMLTrElementImpl;
        _elementTypesWML.put("tr", class_12);
        Class class_13 = class$org$apache$wml$dom$WMLGoElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLGoElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLGoElementImpl")) : class$org$apache$wml$dom$WMLGoElementImpl;
        _elementTypesWML.put("go", class_13);
        Class class_14 = class$org$apache$wml$dom$WMLBigElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLBigElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLBigElementImpl")) : class$org$apache$wml$dom$WMLBigElementImpl;
        _elementTypesWML.put("big", class_14);
        Class class_15 = class$org$apache$wml$dom$WMLAnchorElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLAnchorElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLAnchorElementImpl")) : class$org$apache$wml$dom$WMLAnchorElementImpl;
        _elementTypesWML.put("anchor", class_15);
        Class class_16 = class$org$apache$wml$dom$WMLTimerElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLTimerElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLTimerElementImpl")) : class$org$apache$wml$dom$WMLTimerElementImpl;
        _elementTypesWML.put("timer", class_16);
        Class class_17 = class$org$apache$wml$dom$WMLSmallElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLSmallElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLSmallElementImpl")) : class$org$apache$wml$dom$WMLSmallElementImpl;
        _elementTypesWML.put("small", class_17);
        Class class_18 = class$org$apache$wml$dom$WMLOptgroupElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLOptgroupElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLOptgroupElementImpl")) : class$org$apache$wml$dom$WMLOptgroupElementImpl;
        _elementTypesWML.put("optgroup", class_18);
        Class class_19 = class$org$apache$wml$dom$WMLHeadElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLHeadElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLHeadElementImpl")) : class$org$apache$wml$dom$WMLHeadElementImpl;
        _elementTypesWML.put("head", class_19);
        Class class_20 = class$org$apache$wml$dom$WMLTdElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLTdElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLTdElementImpl")) : class$org$apache$wml$dom$WMLTdElementImpl;
        _elementTypesWML.put("td", class_20);
        Class class_21 = class$org$apache$wml$dom$WMLFieldsetElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLFieldsetElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLFieldsetElementImpl")) : class$org$apache$wml$dom$WMLFieldsetElementImpl;
        _elementTypesWML.put("fieldset", class_21);
        Class class_22 = class$org$apache$wml$dom$WMLImgElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLImgElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLImgElementImpl")) : class$org$apache$wml$dom$WMLImgElementImpl;
        _elementTypesWML.put("img", class_22);
        Class class_23 = class$org$apache$wml$dom$WMLRefreshElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLRefreshElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLRefreshElementImpl")) : class$org$apache$wml$dom$WMLRefreshElementImpl;
        _elementTypesWML.put("refresh", class_23);
        Class class_24 = class$org$apache$wml$dom$WMLOneventElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLOneventElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLOneventElementImpl")) : class$org$apache$wml$dom$WMLOneventElementImpl;
        _elementTypesWML.put("onevent", class_24);
        Class class_25 = class$org$apache$wml$dom$WMLInputElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLInputElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLInputElementImpl")) : class$org$apache$wml$dom$WMLInputElementImpl;
        _elementTypesWML.put("input", class_25);
        Class class_26 = class$org$apache$wml$dom$WMLPrevElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLPrevElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLPrevElementImpl")) : class$org$apache$wml$dom$WMLPrevElementImpl;
        _elementTypesWML.put("prev", class_26);
        Class class_27 = class$org$apache$wml$dom$WMLTableElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLTableElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLTableElementImpl")) : class$org$apache$wml$dom$WMLTableElementImpl;
        _elementTypesWML.put("table", class_27);
        Class class_28 = class$org$apache$wml$dom$WMLMetaElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLMetaElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLMetaElementImpl")) : class$org$apache$wml$dom$WMLMetaElementImpl;
        _elementTypesWML.put("meta", class_28);
        Class class_29 = class$org$apache$wml$dom$WMLTemplateElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLTemplateElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLTemplateElementImpl")) : class$org$apache$wml$dom$WMLTemplateElementImpl;
        _elementTypesWML.put("template", class_29);
        Class class_30 = class$org$apache$wml$dom$WMLBrElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLBrElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLBrElementImpl")) : class$org$apache$wml$dom$WMLBrElementImpl;
        _elementTypesWML.put("br", class_30);
        Class class_31 = class$org$apache$wml$dom$WMLOptionElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLOptionElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLOptionElementImpl")) : class$org$apache$wml$dom$WMLOptionElementImpl;
        _elementTypesWML.put("option", class_31);
        Class class_32 = class$org$apache$wml$dom$WMLUElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLUElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLUElementImpl")) : class$org$apache$wml$dom$WMLUElementImpl;
        _elementTypesWML.put("u", class_32);
        Class class_33 = class$org$apache$wml$dom$WMLPElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLPElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLPElementImpl")) : class$org$apache$wml$dom$WMLPElementImpl;
        _elementTypesWML.put("p", class_33);
        Class class_34 = class$org$apache$wml$dom$WMLSelectElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLSelectElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLSelectElementImpl")) : class$org$apache$wml$dom$WMLSelectElementImpl;
        _elementTypesWML.put("select", class_34);
        Class class_35 = class$org$apache$wml$dom$WMLEmElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLEmElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLEmElementImpl")) : class$org$apache$wml$dom$WMLEmElementImpl;
        _elementTypesWML.put("em", class_35);
        Class class_36 = class$org$apache$wml$dom$WMLIElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLIElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLIElementImpl")) : class$org$apache$wml$dom$WMLIElementImpl;
        _elementTypesWML.put("i", class_36);
        Class class_37 = class$org$apache$wml$dom$WMLCardElementImpl == null ? (WMLDocumentImpl.class$org$apache$wml$dom$WMLCardElementImpl = WMLDocumentImpl.class$("org.apache.wml.dom.WMLCardElementImpl")) : class$org$apache$wml$dom$WMLCardElementImpl;
        _elementTypesWML.put("card", class_37);
    }
}

