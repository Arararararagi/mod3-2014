/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml.dom;

import org.apache.wml.WMLOneventElement;
import org.apache.wml.dom.WMLDocumentImpl;
import org.apache.wml.dom.WMLElementImpl;

public class WMLOneventElementImpl
extends WMLElementImpl
implements WMLOneventElement {
    private static final long serialVersionUID = -4279215241146570871L;

    public WMLOneventElementImpl(WMLDocumentImpl wMLDocumentImpl, String string) {
        super(wMLDocumentImpl, string);
    }

    public void setClassName(String string) {
        this.setAttribute("class", string);
    }

    public String getClassName() {
        return this.getAttribute("class");
    }

    public void setId(String string) {
        this.setAttribute("id", string);
    }

    public String getId() {
        return this.getAttribute("id");
    }

    public void setType(String string) {
        this.setAttribute("type", string);
    }

    public String getType() {
        return this.getAttribute("type");
    }
}

