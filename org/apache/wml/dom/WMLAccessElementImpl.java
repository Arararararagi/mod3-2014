/*
 * Decompiled with CFR 0_102.
 */
package org.apache.wml.dom;

import org.apache.wml.WMLAccessElement;
import org.apache.wml.dom.WMLDocumentImpl;
import org.apache.wml.dom.WMLElementImpl;

public class WMLAccessElementImpl
extends WMLElementImpl
implements WMLAccessElement {
    private static final long serialVersionUID = -235627181817012806L;

    public WMLAccessElementImpl(WMLDocumentImpl wMLDocumentImpl, String string) {
        super(wMLDocumentImpl, string);
    }

    public void setClassName(String string) {
        this.setAttribute("class", string);
    }

    public String getClassName() {
        return this.getAttribute("class");
    }

    public void setDomain(String string) {
        this.setAttribute("domain", string);
    }

    public String getDomain() {
        return this.getAttribute("domain");
    }

    public void setId(String string) {
        this.setAttribute("id", string);
    }

    public String getId() {
        return this.getAttribute("id");
    }

    public void setPath(String string) {
        this.setAttribute("path", string);
    }

    public String getPath() {
        return this.getAttribute("path");
    }
}

