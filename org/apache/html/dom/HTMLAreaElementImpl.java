/*
 * Decompiled with CFR 0_102.
 */
package org.apache.html.dom;

import org.apache.html.dom.HTMLDocumentImpl;
import org.apache.html.dom.HTMLElementImpl;
import org.w3c.dom.html.HTMLAreaElement;

public class HTMLAreaElementImpl
extends HTMLElementImpl
implements HTMLAreaElement {
    private static final long serialVersionUID = 7164004431531608995L;

    public String getAccessKey() {
        String string = this.getAttribute("accesskey");
        if (string != null && string.length() > 1) {
            string = string.substring(0, 1);
        }
        return string;
    }

    public void setAccessKey(String string) {
        if (string != null && string.length() > 1) {
            string = string.substring(0, 1);
        }
        this.setAttribute("accesskey", string);
    }

    public String getAlt() {
        return this.getAttribute("alt");
    }

    public void setAlt(String string) {
        this.setAttribute("alt", string);
    }

    public String getCoords() {
        return this.getAttribute("coords");
    }

    public void setCoords(String string) {
        this.setAttribute("coords", string);
    }

    public String getHref() {
        return this.getAttribute("href");
    }

    public void setHref(String string) {
        this.setAttribute("href", string);
    }

    public boolean getNoHref() {
        return this.getBinary("nohref");
    }

    public void setNoHref(boolean bl) {
        this.setAttribute("nohref", bl);
    }

    public String getShape() {
        return this.capitalize(this.getAttribute("shape"));
    }

    public void setShape(String string) {
        this.setAttribute("shape", string);
    }

    public int getTabIndex() {
        return this.getInteger(this.getAttribute("tabindex"));
    }

    public void setTabIndex(int n) {
        this.setAttribute("tabindex", String.valueOf(n));
    }

    public String getTarget() {
        return this.getAttribute("target");
    }

    public void setTarget(String string) {
        this.setAttribute("target", string);
    }

    public HTMLAreaElementImpl(HTMLDocumentImpl hTMLDocumentImpl, String string) {
        super(hTMLDocumentImpl, string);
    }
}

