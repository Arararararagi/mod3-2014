/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xml.serialize;

public final class Method {
    public static final String XML = "xml";
    public static final String HTML = "html";
    public static final String XHTML = "xhtml";
    public static final String TEXT = "text";
    public static final String FOP = "fop";
}

