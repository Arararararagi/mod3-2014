/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xml.serialize;

public final class LineSeparator {
    public static final String Unix = "\n";
    public static final String Windows = "\r\n";
    public static final String Macintosh = "\r";
    public static final String Web = "\n";
}

