/*
 * Decompiled with CFR 0_102.
 */
package org.apache.xml.serialize;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import org.apache.xerces.util.EncodingMap;

public class EncodingInfo {
    private Object[] fArgsForMethod = null;
    String ianaName;
    String javaName;
    int lastPrintable;
    Object fCharsetEncoder = null;
    Object fCharToByteConverter = null;
    boolean fHaveTriedCToB = false;
    boolean fHaveTriedCharsetEncoder = false;

    public EncodingInfo(String string, String string2, int n) {
        this.ianaName = string;
        this.javaName = EncodingMap.getIANA2JavaMapping(string);
        this.lastPrintable = n;
    }

    public String getIANAName() {
        return this.ianaName;
    }

    public Writer getWriter(OutputStream outputStream) throws UnsupportedEncodingException {
        if (this.javaName != null) {
            return new OutputStreamWriter(outputStream, this.javaName);
        }
        this.javaName = EncodingMap.getIANA2JavaMapping(this.ianaName);
        if (this.javaName == null) {
            return new OutputStreamWriter(outputStream, "UTF8");
        }
        return new OutputStreamWriter(outputStream, this.javaName);
    }

    public boolean isPrintable(char c) {
        if (c <= this.lastPrintable) {
            return true;
        }
        return this.isPrintable0(c);
    }

    private boolean isPrintable0(char c) {
        if (this.fCharsetEncoder == null && fgNIOCharsetAvailable && !this.fHaveTriedCharsetEncoder) {
            if (this.fArgsForMethod == null) {
                this.fArgsForMethod = new Object[1];
            }
            try {
                this.fArgsForMethod[0] = this.javaName;
                Object object = fgCharsetForNameMethod.invoke(null, this.fArgsForMethod);
                if (((Boolean)fgCharsetCanEncodeMethod.invoke(object, null)).booleanValue()) {
                    this.fCharsetEncoder = fgCharsetNewEncoderMethod.invoke(object, null);
                } else {
                    this.fHaveTriedCharsetEncoder = true;
                }
            }
            catch (Exception var2_3) {
                this.fHaveTriedCharsetEncoder = true;
            }
        }
        if (this.fCharsetEncoder != null) {
            try {
                this.fArgsForMethod[0] = new Character(c);
                return (Boolean)fgCharsetEncoderCanEncodeMethod.invoke(this.fCharsetEncoder, this.fArgsForMethod);
            }
            catch (Exception var2_4) {
                this.fCharsetEncoder = null;
                this.fHaveTriedCharsetEncoder = false;
            }
        }
        if (this.fCharToByteConverter == null) {
            if (this.fHaveTriedCToB || !fgConvertersAvailable) {
                return false;
            }
            if (this.fArgsForMethod == null) {
                this.fArgsForMethod = new Object[1];
            }
            try {
                this.fArgsForMethod[0] = this.javaName;
                this.fCharToByteConverter = fgGetConverterMethod.invoke(null, this.fArgsForMethod);
            }
            catch (Exception var2_5) {
                this.fHaveTriedCToB = true;
                return false;
            }
        }
        try {
            this.fArgsForMethod[0] = new Character(c);
            return (Boolean)fgCanConvertMethod.invoke(this.fCharToByteConverter, this.fArgsForMethod);
        }
        catch (Exception var2_6) {
            this.fCharToByteConverter = null;
            this.fHaveTriedCToB = false;
            return false;
        }
    }

    public static void testJavaEncodingName(String string) throws UnsupportedEncodingException {
        byte[] arrby = new byte[]{118, 97, 108, 105, 100};
        String string2 = new String(arrby, string);
    }

    static class CharToByteConverterMethods {
        private static Method fgGetConverterMethod = null;
        private static Method fgCanConvertMethod = null;
        private static boolean fgConvertersAvailable = false;
        static /* synthetic */ Class class$java$lang$String;

        private CharToByteConverterMethods() {
        }

        static /* synthetic */ Class class$(String string) {
            try {
                return Class.forName(string);
            }
            catch (ClassNotFoundException var1_1) {
                throw new NoClassDefFoundError(var1_1.getMessage());
            }
        }

        static {
            try {
                Class class_ = Class.forName("sun.io.CharToByteConverter");
                Class[] arrclass = new Class[1];
                Class class_2 = class$java$lang$String == null ? (CharToByteConverterMethods.class$java$lang$String = CharToByteConverterMethods.class$("java.lang.String")) : class$java$lang$String;
                arrclass[0] = class_2;
                fgGetConverterMethod = class_.getMethod("getConverter", arrclass);
                fgCanConvertMethod = class_.getMethod("canConvert", Character.TYPE);
                fgConvertersAvailable = true;
            }
            catch (Exception var0_1) {
                fgGetConverterMethod = null;
                fgCanConvertMethod = null;
                fgConvertersAvailable = false;
            }
        }
    }

    static class CharsetMethods {
        private static Method fgCharsetForNameMethod = null;
        private static Method fgCharsetCanEncodeMethod = null;
        private static Method fgCharsetNewEncoderMethod = null;
        private static Method fgCharsetEncoderCanEncodeMethod = null;
        private static boolean fgNIOCharsetAvailable = false;
        static /* synthetic */ Class class$java$lang$String;

        private CharsetMethods() {
        }

        static /* synthetic */ Class class$(String string) {
            try {
                return Class.forName(string);
            }
            catch (ClassNotFoundException var1_1) {
                throw new NoClassDefFoundError(var1_1.getMessage());
            }
        }

        static {
            try {
                Class class_ = Class.forName("java.nio.charset.Charset");
                Class class_2 = Class.forName("java.nio.charset.CharsetEncoder");
                Class[] arrclass = new Class[1];
                Class class_3 = class$java$lang$String == null ? (CharsetMethods.class$java$lang$String = CharsetMethods.class$("java.lang.String")) : class$java$lang$String;
                arrclass[0] = class_3;
                fgCharsetForNameMethod = class_.getMethod("forName", arrclass);
                fgCharsetCanEncodeMethod = class_.getMethod("canEncode", new Class[0]);
                fgCharsetNewEncoderMethod = class_.getMethod("newEncoder", new Class[0]);
                fgCharsetEncoderCanEncodeMethod = class_2.getMethod("canEncode", Character.TYPE);
                fgNIOCharsetAvailable = true;
            }
            catch (Exception var0_1) {
                fgCharsetForNameMethod = null;
                fgCharsetCanEncodeMethod = null;
                fgCharsetEncoderCanEncodeMethod = null;
                fgCharsetNewEncoderMethod = null;
                fgNIOCharsetAvailable = false;
            }
        }
    }

}

