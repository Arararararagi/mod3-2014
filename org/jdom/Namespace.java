/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.util.HashMap;
import org.jdom.IllegalNameException;
import org.jdom.Verifier;

public final class Namespace {
    private static final String CVS_ID = "@(#) $RCSfile: Namespace.java,v $ $Revision: 1.32 $ $Date: 2002/03/12 07:57:06 $ $Name: jdom_1_0_b8 $";
    private static HashMap namespaces;
    public static final Namespace NO_NAMESPACE;
    public static final Namespace XML_NAMESPACE;
    private String prefix;
    private String uri;

    static {
        NO_NAMESPACE = new Namespace("", "");
        XML_NAMESPACE = new Namespace("xml", "http://www.w3.org/XML/1998/namespace");
        namespaces = new HashMap();
        namespaces.put("&", NO_NAMESPACE);
        namespaces.put("xml&http://www.w3.org/XML/1998/namespace", XML_NAMESPACE);
    }

    private Namespace(String string, String string2) {
        this.prefix = string;
        this.uri = string2;
    }

    public boolean equals(Object object) {
        if (object instanceof Namespace) {
            return this.uri.equals(((Namespace)object).uri);
        }
        return false;
    }

    public static Namespace getNamespace(String string) {
        return Namespace.getNamespace("", string);
    }

    public static Namespace getNamespace(String string, String string2) {
        if (string == null || string.trim().equals("")) {
            string = "";
        }
        if (string2 == null || string2.trim().equals("")) {
            string2 = "";
        }
        if (string.equals("xml")) {
            return XML_NAMESPACE;
        }
        String string3 = new StringBuffer(64).append(string).append('&').append(string2).toString();
        Namespace namespace = (Namespace)namespaces.get(string3);
        if (namespace != null) {
            return namespace;
        }
        String string4 = Verifier.checkNamespacePrefix(string);
        if (string4 != null) {
            throw new IllegalNameException(string, "Namespace prefix", string4);
        }
        string4 = Verifier.checkNamespaceURI(string2);
        if (string4 != null) {
            throw new IllegalNameException(string2, "Namespace URI", string4);
        }
        if (!string.equals("") && string2.equals("")) {
            throw new IllegalNameException("", "namespace", "Namespace URIs must be non-null and non-empty Strings");
        }
        Namespace namespace2 = new Namespace(string, string2);
        namespaces.put(string3, namespace2);
        return namespace2;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getURI() {
        return this.uri;
    }

    public int hashCode() {
        return this.uri.hashCode();
    }

    public String toString() {
        return "[Namespace: prefix \"" + this.prefix + "\" is mapped to URI \"" + this.uri + "\"]";
    }
}

