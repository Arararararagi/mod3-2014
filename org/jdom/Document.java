/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import org.jdom.Comment;
import org.jdom.ContentList;
import org.jdom.DocType;
import org.jdom.Element;
import org.jdom.IllegalAddException;
import org.jdom.ProcessingInstruction;
import org.jdom.filter.Filter;

public class Document
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: Document.java,v $ $Revision: 1.55 $ $Date: 2002/03/28 11:08:12 $ $Name: jdom_1_0_b8 $";
    protected ContentList content;
    protected DocType docType;

    public Document() {
        this.content = new ContentList(this);
    }

    public Document(List list) {
        this(list, null);
    }

    public Document(List list, DocType docType) {
        this.content = new ContentList(this);
        this.setContent(list);
        this.setDocType(docType);
    }

    public Document(Element element) {
        this(element, null);
    }

    public Document(Element element, DocType docType) {
        this.content = new ContentList(this);
        if (element != null) {
            this.setRootElement(element);
        }
        this.setDocType(docType);
    }

    public Document addContent(Comment comment) {
        this.content.add(comment);
        return this;
    }

    public Document addContent(ProcessingInstruction processingInstruction) {
        this.content.add(processingInstruction);
        return this;
    }

    public Object clone() {
        Document document;
        document = null;
        try {
            document = (Document)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        if (this.docType != null) {
            document.docType = (DocType)this.docType.clone();
        }
        document.content = new ContentList(document);
        for (int i = 0; i < this.content.size(); ++i) {
            Object object = this.content.get(i);
            if (object instanceof Element) {
                Element element = (Element)((Element)object).clone();
                document.content.add(element);
                continue;
            }
            if (object instanceof Comment) {
                Comment comment = (Comment)((Comment)object).clone();
                document.content.add(comment);
                continue;
            }
            if (!(object instanceof ProcessingInstruction)) continue;
            ProcessingInstruction processingInstruction = (ProcessingInstruction)((ProcessingInstruction)object).clone();
            document.content.add(processingInstruction);
        }
        return document;
    }

    public Element detachRootElement() {
        int n = this.content.indexOfFirstElement();
        if (n < 0) {
            return null;
        }
        return (Element)this.removeContent(n);
    }

    public final boolean equals(Object object) {
        return object == this;
    }

    public List getContent() {
        if (!this.hasRootElement()) {
            throw new IllegalStateException("Root element not set");
        }
        return this.content;
    }

    public List getContent(Filter filter) {
        if (!this.hasRootElement()) {
            throw new IllegalStateException("Root element not set");
        }
        return this.content.getView(filter);
    }

    public DocType getDocType() {
        return this.docType;
    }

    public Element getRootElement() {
        int n = this.content.indexOfFirstElement();
        if (n < 0) {
            throw new IllegalStateException("Root element not set");
        }
        return (Element)this.content.get(n);
    }

    public boolean hasRootElement() {
        return this.content.indexOfFirstElement() >= 0;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    private Object removeContent(int n) {
        return this.content.remove(n);
    }

    public boolean removeContent(Comment comment) {
        return this.content.remove(comment);
    }

    public boolean removeContent(ProcessingInstruction processingInstruction) {
        return this.content.remove(processingInstruction);
    }

    public Document setContent(List list) {
        this.content.clearAndSet(list);
        return this;
    }

    public Document setDocType(DocType docType) {
        if (docType != null) {
            if (docType.getDocument() != null) {
                throw new IllegalAddException(this, docType, "The docType already is attached to a document");
            }
            docType.setDocument(this);
        }
        if (this.docType != null) {
            this.docType.setDocument(null);
        }
        this.docType = docType;
        return this;
    }

    public Document setRootElement(Element element) {
        int n = this.content.indexOfFirstElement();
        if (n < 0) {
            this.content.add(element);
        } else {
            this.content.set(n, element);
        }
        return this;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer().append("[Document: ");
        if (this.docType != null) {
            stringBuffer.append(this.docType.toString()).append(", ");
        } else {
            stringBuffer.append(" No DOCTYPE declaration, ");
        }
        Element element = this.getRootElement();
        if (element != null) {
            stringBuffer.append("Root is ").append(element.toString());
        } else {
            stringBuffer.append(" No root element");
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}

