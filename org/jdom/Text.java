/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalDataException;
import org.jdom.Verifier;

public class Text
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: Text.java,v $ $Revision: 1.11 $ $Date: 2002/03/12 07:11:39 $ $Name: jdom_1_0_b8 $";
    private static final String EMPTY_STRING = "";
    protected String value;
    protected Object parent;

    protected Text() {
    }

    public Text(String string) {
        this.setText(string);
    }

    public void append(String string) {
        if (string == null) {
            return;
        }
        String string2 = Verifier.checkCharacterData(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "character content", string2);
        }
        this.value = string == "" ? string : String.valueOf(this.value) + string;
    }

    public void append(Text text) {
        if (text == null) {
            return;
        }
        this.value = String.valueOf(this.value) + text.getText();
    }

    public Object clone() {
        Text text;
        text = null;
        try {
            text = (Text)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        text.parent = null;
        text.value = this.value;
        return text;
    }

    public Text detach() {
        if (this.parent != null) {
            ((Element)this.parent).removeContent(this);
        }
        this.parent = null;
        return this;
    }

    public final boolean equals(Object object) {
        return this == object;
    }

    public Document getDocument() {
        if (this.parent != null) {
            return ((Element)this.parent).getDocument();
        }
        return null;
    }

    public Element getParent() {
        return (Element)this.parent;
    }

    public String getText() {
        return this.value;
    }

    public String getTextNormalize() {
        return Text.normalizeString(this.getText());
    }

    public String getTextTrim() {
        return this.getText().trim();
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public static String normalizeString(String string) {
        if (string == null) {
            return "";
        }
        char[] arrc = string.toCharArray();
        char[] arrc2 = new char[arrc.length];
        boolean bl = true;
        int n = 0;
        for (int i = 0; i < arrc.length; ++i) {
            if (" \t\n\r".indexOf(arrc[i]) != -1) {
                if (bl) continue;
                arrc2[n++] = 32;
                bl = true;
                continue;
            }
            arrc2[n++] = arrc[i];
            bl = false;
        }
        if (bl && n > 0) {
            --n;
        }
        return new String(arrc2, 0, n);
    }

    protected Text setParent(Element element) {
        this.parent = element;
        return this;
    }

    public Text setText(String string) {
        if (string == null) {
            this.value = "";
            return this;
        }
        String string2 = Verifier.checkCharacterData(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "character content", string2);
        }
        this.value = string;
        return this;
    }

    public String toString() {
        return new StringBuffer(64).append("[Text: ").append(this.getText()).append("]").toString();
    }
}

