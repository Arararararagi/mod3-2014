/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.Element;
import org.jdom.IllegalAddException;
import org.jdom.Namespace;
import org.jdom.Verifier;

class AttributeList
extends AbstractList
implements List,
Serializable {
    private static final String CVS_ID = "@(#) $RCSfile: AttributeList.java,v $ $Revision: 1.8 $ $Date: 2002/03/20 10:07:54 $ $Name: jdom_1_0_b8 $";
    private static final int INITIAL_ARRAY_SIZE = 3;
    protected ArrayList list;
    protected Element parent;

    private AttributeList() {
    }

    public AttributeList(Element element) {
        this.parent = element;
    }

    public void add(int n, Object object) {
        Attribute attribute;
        if (object instanceof Attribute) {
            attribute = (Attribute)object;
            int n2 = this.indexOfDuplicate(attribute);
            if (n2 >= 0) {
                throw new IllegalAddException("Cannot add duplicate attribute");
            }
        } else {
            if (object == null) {
                throw new IllegalAddException("Cannot add null attribute");
            }
            throw new IllegalAddException("Class " + object.getClass().getName() + " is not an attribute");
        }
        this.add(n, attribute);
        ++this.modCount;
    }

    protected void add(int n, Attribute attribute) {
        if (attribute.getParent() != null) {
            throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
        }
        String string = Verifier.checkNamespaceCollision(attribute, this.parent);
        if (string != null) {
            throw new IllegalAddException(this.parent, attribute, string);
        }
        if (this.list == null) {
            if (n == 0) {
                this.ensureCapacity(3);
            } else {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
            }
        }
        this.list.add(n, attribute);
        attribute.setParent(this.parent);
    }

    public boolean add(Object object) {
        if (object instanceof Attribute) {
            Attribute attribute = (Attribute)object;
            int n = this.indexOfDuplicate(attribute);
            if (n < 0) {
                this.add(this.size(), attribute);
            } else {
                this.set(n, attribute);
            }
        } else {
            if (object == null) {
                throw new IllegalAddException("Cannot add null attribute");
            }
            throw new IllegalAddException("Class " + object.getClass().getName() + " is not an attribute");
        }
        return true;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public boolean addAll(int var1_1, Collection var2_2) {
        if (this.list == null && var1_1 != 0) {
            throw new IndexOutOfBoundsException("Index: " + var1_1 + " Size: " + this.size());
        }
        if (var2_2 == null) return false;
        if (var2_2.size() == 0) {
            return false;
        }
        var3_3 = 0;
        try {
            var4_4 = var2_2.iterator();
            do {
                if (!var4_4.hasNext()) {
                    return true;
                }
                var5_6 = var4_4.next();
                this.add(var1_1 + var3_3, var5_6);
                ++var3_3;
            } while (true);
        }
        catch (RuntimeException var4_5) {
            ** for (var5_7 = 0;
            ; var5_7 < var3_3; ++var5_7)
        }
lbl-1000: // 1 sources:
        {
            this.remove(var1_1 + var5_7);
            continue;
        }
lbl20: // 1 sources:
        throw var4_5;
    }

    public boolean addAll(Collection collection) {
        return this.addAll(this.size(), collection);
    }

    public void clear() {
        if (this.list != null) {
            for (int i = 0; i < this.list.size(); ++i) {
                Attribute attribute = (Attribute)this.list.get(i);
                attribute.setParent(null);
            }
            this.list = null;
        }
        ++this.modCount;
    }

    public void clearAndSet(Collection collection) {
        ArrayList arrayList = this.list;
        this.list = null;
        if (collection != null && collection.size() != 0) {
            this.ensureCapacity(collection.size());
            try {
                this.addAll(0, collection);
            }
            catch (RuntimeException var3_3) {
                this.list = arrayList;
                throw var3_3;
            }
        }
        if (arrayList != null) {
            for (int i = 0; i < arrayList.size(); ++i) {
                Attribute attribute = (Attribute)arrayList.get(i);
                attribute.setParent(null);
            }
        }
    }

    protected void ensureCapacity(int n) {
        if (this.list == null) {
            this.list = new ArrayList(n);
        } else {
            this.list.ensureCapacity(n);
        }
    }

    public Object get(int n) {
        if (this.list == null) {
            throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
        }
        return this.list.get(n);
    }

    protected Object get(String string, Namespace namespace) {
        int n = this.indexOf(string, namespace);
        if (n < 0) {
            return null;
        }
        return this.list.get(n);
    }

    protected int indexOf(String string, Namespace namespace) {
        String string2 = namespace.getURI();
        if (this.list != null) {
            for (int i = 0; i < this.list.size(); ++i) {
                Attribute attribute = (Attribute)this.list.get(i);
                String string3 = attribute.getNamespaceURI();
                String string4 = attribute.getName();
                if (!string3.equals(string2) || !string4.equals(string)) continue;
                return i;
            }
        }
        return -1;
    }

    private int indexOfDuplicate(Attribute attribute) {
        int n = -1;
        String string = attribute.getName();
        Namespace namespace = attribute.getNamespace();
        n = this.indexOf(string, namespace);
        return n;
    }

    public Object remove(int n) {
        if (this.list == null) {
            throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
        }
        Attribute attribute = (Attribute)this.list.get(n);
        attribute.setParent(null);
        ++this.modCount;
        this.list.remove(n);
        return attribute;
    }

    protected boolean remove(String string, Namespace namespace) {
        int n = this.indexOf(string, namespace);
        if (n < 0) {
            return false;
        }
        this.remove(n);
        return true;
    }

    public Object set(int n, Object object) {
        if (object instanceof Attribute) {
            Attribute attribute = (Attribute)object;
            int n2 = this.indexOfDuplicate(attribute);
            if (n2 >= 0 && n2 != n) {
                throw new IllegalAddException("Cannot set duplicate attribute");
            }
            return this.set(n, attribute);
        }
        if (object == null) {
            throw new IllegalAddException("Cannot add null attribute");
        }
        throw new IllegalAddException("Class " + object.getClass().getName() + " is not an attribute");
    }

    protected Object set(int n, Attribute attribute) {
        if (this.list == null) {
            throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
        }
        if (attribute.getParent() != null) {
            throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
        }
        String string = Verifier.checkNamespaceCollision(attribute, this.parent);
        if (string != null) {
            throw new IllegalAddException(this.parent, attribute, string);
        }
        Attribute attribute2 = (Attribute)this.list.get(n);
        attribute2.setParent(null);
        this.list.set(n, attribute);
        return attribute2;
    }

    public int size() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public String toString() {
        if (this.list != null && this.list.size() > 0) {
            return this.list.toString();
        }
        return "[]";
    }
}

