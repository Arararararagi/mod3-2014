/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

public class IllegalNameException
extends IllegalArgumentException {
    private static final String CVS_ID = "@(#) $RCSfile: IllegalNameException.java,v $ $Revision: 1.7 $ $Date: 2002/01/08 09:17:10 $ $Name: jdom_1_0_b8 $";

    public IllegalNameException(String string, String string2) {
        super("The name \"" + string + "\" is not legal for JDOM/XML " + string2 + "s.");
    }

    public IllegalNameException(String string, String string2, String string3) {
        super("The name \"" + string + "\" is not legal for JDOM/XML " + string2 + "s: " + string3 + ".");
    }
}

