/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import org.jdom.DataConversionException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalDataException;
import org.jdom.IllegalNameException;
import org.jdom.Namespace;
import org.jdom.Verifier;

public class Attribute
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: Attribute.java,v $ $Revision: 1.40 $ $Date: 2002/03/19 04:25:37 $ $Name: jdom_1_0_b8 $";
    public static final int UNDECLARED_ATTRIBUTE = 0;
    public static final int CDATA_ATTRIBUTE = 1;
    public static final int ID_ATTRIBUTE = 2;
    public static final int IDREF_ATTRIBUTE = 3;
    public static final int IDREFS_ATTRIBUTE = 4;
    public static final int ENTITY_ATTRIBUTE = 5;
    public static final int ENTITIES_ATTRIBUTE = 6;
    public static final int NMTOKEN_ATTRIBUTE = 7;
    public static final int NMTOKENS_ATTRIBUTE = 8;
    public static final int NOTATION_ATTRIBUTE = 9;
    public static final int ENUMERATED_ATTRIBUTE = 10;
    protected String name;
    protected transient Namespace namespace;
    protected String value;
    protected int type = 0;
    protected Object parent;

    protected Attribute() {
    }

    public Attribute(String string, String string2) {
        this(string, string2, 0, Namespace.NO_NAMESPACE);
    }

    public Attribute(String string, String string2, int n) {
        this(string, string2, n, Namespace.NO_NAMESPACE);
    }

    public Attribute(String string, String string2, int n, Namespace namespace) {
        this.setName(string);
        this.setValue(string2);
        this.setAttributeType(n);
        this.setNamespace(namespace);
    }

    public Attribute(String string, String string2, Namespace namespace) {
        this.setName(string);
        this.setValue(string2);
        this.setNamespace(namespace);
    }

    public Object clone() {
        Attribute attribute;
        attribute = null;
        try {
            attribute = (Attribute)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        attribute.parent = null;
        return attribute;
    }

    public Attribute detach() {
        Element element = this.getParent();
        if (element != null) {
            element.removeAttribute(this.getName(), this.getNamespace());
        }
        return this;
    }

    public final boolean equals(Object object) {
        return object == this;
    }

    public int getAttributeType() {
        return this.type;
    }

    public boolean getBooleanValue() throws DataConversionException {
        if (this.value.equalsIgnoreCase("true") || this.value.equalsIgnoreCase("on") || this.value.equalsIgnoreCase("yes")) {
            return true;
        }
        if (this.value.equalsIgnoreCase("false") || this.value.equalsIgnoreCase("off") || this.value.equalsIgnoreCase("no")) {
            return false;
        }
        throw new DataConversionException(this.name, "boolean");
    }

    public Document getDocument() {
        if (this.parent != null) {
            return ((Element)this.parent).getDocument();
        }
        return null;
    }

    public double getDoubleValue() throws DataConversionException {
        try {
            return Double.valueOf(this.value);
        }
        catch (NumberFormatException v0) {
            throw new DataConversionException(this.name, "double");
        }
    }

    public float getFloatValue() throws DataConversionException {
        try {
            return Float.valueOf(this.value).floatValue();
        }
        catch (NumberFormatException v0) {
            throw new DataConversionException(this.name, "float");
        }
    }

    public int getIntValue() throws DataConversionException {
        try {
            return Integer.parseInt(this.value);
        }
        catch (NumberFormatException v0) {
            throw new DataConversionException(this.name, "int");
        }
    }

    public long getLongValue() throws DataConversionException {
        try {
            return Long.parseLong(this.value);
        }
        catch (NumberFormatException v0) {
            throw new DataConversionException(this.name, "long");
        }
    }

    public String getName() {
        return this.name;
    }

    public Namespace getNamespace() {
        return this.namespace;
    }

    public String getNamespacePrefix() {
        return this.namespace.getPrefix();
    }

    public String getNamespaceURI() {
        return this.namespace.getURI();
    }

    public Element getParent() {
        return (Element)this.parent;
    }

    public String getQualifiedName() {
        String string = this.namespace.getPrefix();
        if (!(string == null || string.equals(""))) {
            return string + ':' + this.getName();
        }
        return this.getName();
    }

    public String getValue() {
        return this.value;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.namespace = Namespace.getNamespace((String)objectInputStream.readObject(), (String)objectInputStream.readObject());
    }

    public Attribute setAttributeType(int n) {
        if (n < 0 || n > 10) {
            throw new IllegalDataException(String.valueOf(n), "attribute", "Illegal attribute type");
        }
        this.type = n;
        return this;
    }

    public Attribute setName(String string) {
        String string2 = Verifier.checkAttributeName(string);
        if (string2 != null) {
            throw new IllegalNameException(string, "attribute", string2);
        }
        this.name = string;
        return this;
    }

    public Attribute setNamespace(Namespace namespace) {
        if (namespace == null) {
            namespace = Namespace.NO_NAMESPACE;
        }
        if (namespace != Namespace.NO_NAMESPACE && namespace.getPrefix().equals("")) {
            throw new IllegalNameException("", "attribute namespace", "An attribute namespace without a prefix can only be the NO_NAMESPACE namespace");
        }
        this.namespace = namespace;
        return this;
    }

    protected Attribute setParent(Element element) {
        this.parent = element;
        return this;
    }

    public Attribute setValue(String string) {
        String string2 = null;
        string2 = Verifier.checkCharacterData(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "attribute", string2);
        }
        this.value = string;
        return this;
    }

    public String toString() {
        return "[Attribute: " + this.getQualifiedName() + "=\"" + this.value + "\"" + "]";
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.namespace.getPrefix());
        objectOutputStream.writeObject(this.namespace.getURI());
    }
}

