/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

public class IllegalTargetException
extends IllegalArgumentException {
    private static final String CVS_ID = "@(#) $RCSfile: IllegalTargetException.java,v $ $Revision: 1.8 $ $Date: 2002/01/08 09:17:10 $ $Name: jdom_1_0_b8 $";

    public IllegalTargetException(String string) {
        super("The name \"" + string + "\" is not legal for JDOM/XML Processing Instructions.");
    }

    public IllegalTargetException(String string, String string2) {
        super("The target \"" + string + "\" is not legal for JDOM/XML Processing Instructions: " + string2 + ".");
    }
}

