/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalDataException;
import org.jdom.Verifier;
import org.jdom.output.XMLOutputter;

public class Comment
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: Comment.java,v $ $Revision: 1.22 $ $Date: 2002/03/20 15:16:32 $ $Name: jdom_1_0_b8 $";
    protected String text;
    protected Object parent;

    protected Comment() {
    }

    public Comment(String string) {
        this.setText(string);
    }

    public Object clone() {
        Comment comment;
        comment = null;
        try {
            comment = (Comment)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        comment.parent = null;
        return comment;
    }

    public Comment detach() {
        if (this.parent instanceof Element) {
            ((Element)this.parent).removeContent(this);
        } else if (this.parent instanceof Document) {
            ((Document)this.parent).removeContent(this);
        }
        return this;
    }

    public final boolean equals(Object object) {
        return object == this;
    }

    public Document getDocument() {
        if (this.parent instanceof Document) {
            return (Document)this.parent;
        }
        if (this.parent instanceof Element) {
            return ((Element)this.parent).getDocument();
        }
        return null;
    }

    public Element getParent() {
        if (this.parent instanceof Element) {
            return (Element)this.parent;
        }
        return null;
    }

    public String getText() {
        return this.text;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    protected Comment setDocument(Document document) {
        this.parent = document;
        return this;
    }

    protected Comment setParent(Element element) {
        this.parent = element;
        return this;
    }

    public Comment setText(String string) {
        String string2 = Verifier.checkCommentData(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "comment", string2);
        }
        this.text = string;
        return this;
    }

    public String toString() {
        return "[Comment: " + new XMLOutputter().outputString(this) + "]";
    }
}

