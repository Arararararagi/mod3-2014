/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalTargetException;
import org.jdom.Verifier;
import org.jdom.output.XMLOutputter;

public class ProcessingInstruction
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: ProcessingInstruction.java,v $ $Revision: 1.27 $ $Date: 2002/03/20 15:16:32 $ $Name: jdom_1_0_b8 $";
    protected String target;
    protected String rawData;
    protected Map mapData;
    protected Object parent;

    protected ProcessingInstruction() {
    }

    public ProcessingInstruction(String string, String string2) {
        String string3 = Verifier.checkProcessingInstructionTarget(string);
        if (string3 != null) {
            throw new IllegalTargetException(string, string3);
        }
        this.target = string;
        this.setData(string2);
    }

    public ProcessingInstruction(String string, Map map) {
        String string2 = Verifier.checkProcessingInstructionTarget(string);
        if (string2 != null) {
            throw new IllegalTargetException(string, string2);
        }
        this.target = string;
        this.setData(map);
    }

    public Object clone() {
        ProcessingInstruction processingInstruction;
        processingInstruction = null;
        try {
            processingInstruction = (ProcessingInstruction)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        processingInstruction.parent = null;
        if (this.mapData != null) {
            processingInstruction.mapData = this.parseData(this.rawData);
        }
        return processingInstruction;
    }

    public ProcessingInstruction detach() {
        if (this.parent instanceof Element) {
            ((Element)this.parent).removeContent(this);
        } else if (this.parent instanceof Document) {
            ((Document)this.parent).removeContent(this);
        }
        return this;
    }

    public final boolean equals(Object object) {
        return object == this;
    }

    private String extractQuotedString(String string) {
        boolean bl = false;
        char c = '\"';
        int n = 0;
        for (int i = 0; i < string.length(); ++i) {
            char c2 = string.charAt(i);
            if (c2 != '\"' && c2 != '\'') continue;
            if (!bl) {
                c = c2;
                bl = true;
                n = i + 1;
                continue;
            }
            if (c != c2) continue;
            bl = false;
            return string.substring(n, i);
        }
        return null;
    }

    public String getData() {
        return this.rawData;
    }

    public Document getDocument() {
        if (this.parent instanceof Document) {
            return (Document)this.parent;
        }
        if (this.parent instanceof Element) {
            return ((Element)this.parent).getDocument();
        }
        return null;
    }

    public List getNames() {
        Set set = this.mapData.entrySet();
        ArrayList<String> arrayList = new ArrayList<String>();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            String string = iterator.next().toString();
            String string2 = string.substring(0, string.indexOf("="));
            arrayList.add(string2);
        }
        return arrayList;
    }

    public Element getParent() {
        if (this.parent instanceof Element) {
            return (Element)this.parent;
        }
        return null;
    }

    public String getTarget() {
        return this.target;
    }

    public String getValue(String string) {
        return (String)this.mapData.get(string);
    }

    public final int hashCode() {
        return super.hashCode();
    }

    private Map parseData(String string) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        String string2 = string.trim();
        while (!string2.trim().equals("")) {
            int n;
            String string3 = "";
            String string4 = "";
            int n2 = 0;
            char c = string2.charAt(n2);
            for (n = 1; n < string2.length(); ++n) {
                char c2 = string2.charAt(n);
                if (c2 == '=') {
                    string3 = string2.substring(n2, n).trim();
                    string4 = this.extractQuotedString(string2.substring(n + 1).trim());
                    if (string4 == null) {
                        return new HashMap();
                    }
                    n+=string4.length() + 1;
                    break;
                }
                if (Character.isWhitespace(c) && !Character.isWhitespace(c2)) {
                    n2 = n;
                }
                c = c2;
            }
            string2 = string2.substring(n);
            if (string3.length() <= 0 || string4 == null) continue;
            hashMap.put(string3, string4);
        }
        return hashMap;
    }

    public boolean removeValue(String string) {
        if (this.mapData.remove(string) != null) {
            this.rawData = this.toString(this.mapData);
            return true;
        }
        return false;
    }

    public ProcessingInstruction setData(String string) {
        this.rawData = string;
        this.mapData = this.parseData(string);
        return this;
    }

    public ProcessingInstruction setData(Map map) {
        this.rawData = this.toString(map);
        this.mapData = map;
        return this;
    }

    protected ProcessingInstruction setDocument(Document document) {
        this.parent = document;
        return this;
    }

    protected ProcessingInstruction setParent(Element element) {
        this.parent = element;
        return this;
    }

    public ProcessingInstruction setValue(String string, String string2) {
        this.mapData.put(string, string2);
        this.rawData = this.toString(this.mapData);
        return this;
    }

    public String toString() {
        return "[ProcessingInstruction: " + new XMLOutputter().outputString(this) + "]";
    }

    private String toString(Map map) {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            String string = (String)iterator.next();
            String string2 = (String)map.get(string);
            stringBuffer.append(string).append("=\"").append(string2).append("\" ");
        }
        stringBuffer.setLength(stringBuffer.length() - 1);
        return stringBuffer.toString();
    }
}

