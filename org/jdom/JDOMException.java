/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.sql.SQLException;
import org.xml.sax.SAXException;

public class JDOMException
extends Exception {
    private static final String CVS_ID = "@(#) $RCSfile: JDOMException.java,v $ $Revision: 1.11 $ $Date: 2002/01/08 09:17:10 $ $Name: jdom_1_0_b8 $";
    protected Throwable cause;

    public JDOMException() {
        super("Error occurred in JDOM application.");
    }

    public JDOMException(String string) {
        super(string);
    }

    public JDOMException(String string, Throwable throwable) {
        super(string);
        this.cause = throwable;
    }

    public Throwable getCause() {
        return this.cause;
    }

    public String getMessage() {
        Throwable throwable;
        String string = super.getMessage();
        Throwable throwable2 = this;
        while ((throwable = JDOMException.getNestedException(throwable2)) != null) {
            Exception exception;
            String string2 = throwable.getMessage();
            if (throwable instanceof SAXException && (exception = ((SAXException)throwable).getException()) != null && string2 != null && string2.equals(exception.getMessage())) {
                string2 = null;
            }
            if (string2 != null) {
                string = string != null ? String.valueOf(string) + ": " + string2 : string2;
            }
            if (throwable instanceof JDOMException) break;
            throwable2 = throwable;
        }
        return string;
    }

    private static Throwable getNestedException(Throwable throwable) {
        if (throwable instanceof JDOMException) {
            return ((JDOMException)throwable).getCause();
        }
        if (throwable instanceof SAXException) {
            return ((SAXException)throwable).getException();
        }
        if (throwable instanceof SQLException) {
            return ((SQLException)throwable).getNextException();
        }
        if (throwable instanceof InvocationTargetException) {
            return ((InvocationTargetException)throwable).getTargetException();
        }
        if (throwable instanceof ExceptionInInitializerError) {
            return ((ExceptionInInitializerError)throwable).getException();
        }
        if (throwable instanceof RemoteException) {
            return ((RemoteException)throwable).detail;
        }
        Throwable throwable2 = JDOMException.getNestedException(throwable, "javax.naming.NamingException", "getRootCause");
        if (throwable2 != null) {
            return throwable2;
        }
        throwable2 = JDOMException.getNestedException(throwable, "javax.servlet.ServletException", "getRootCause");
        if (throwable2 != null) {
            return throwable2;
        }
        return null;
    }

    private static Throwable getNestedException(Throwable throwable, String string, String string2) {
        try {
            Class class_ = Class.forName(string);
            Class class_2 = throwable.getClass();
            if (class_.isAssignableFrom(class_2)) {
                Class[] arrclass = new Class[]{};
                Method method = class_.getMethod(string2, arrclass);
                Object[] arrobject = new Object[]{};
                return (Throwable)method.invoke(throwable, arrobject);
            }
        }
        catch (Exception v0) {}
        return null;
    }

    public Throwable initCause(Throwable throwable) {
        this.cause = throwable;
        return throwable;
    }

    public void printStackTrace() {
        Throwable throwable;
        super.printStackTrace();
        Throwable throwable2 = this;
        while ((throwable = JDOMException.getNestedException(throwable2)) != null) {
            if (throwable == null) continue;
            System.err.print("Caused by: ");
            throwable.printStackTrace();
            if (throwable instanceof JDOMException) break;
            throwable2 = throwable;
        }
    }

    public void printStackTrace(PrintStream printStream) {
        Throwable throwable;
        super.printStackTrace(printStream);
        Throwable throwable2 = this;
        while ((throwable = JDOMException.getNestedException(throwable2)) != null) {
            if (throwable == null) continue;
            System.err.print("Caused by: ");
            throwable.printStackTrace(printStream);
            if (throwable instanceof JDOMException) break;
            throwable2 = throwable;
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        Throwable throwable;
        super.printStackTrace(printWriter);
        Throwable throwable2 = this;
        while ((throwable = JDOMException.getNestedException(throwable2)) != null) {
            if (throwable == null) continue;
            System.err.print("Caused by: ");
            throwable.printStackTrace(printWriter);
            if (throwable instanceof JDOMException) break;
            throwable2 = throwable;
        }
    }
}

