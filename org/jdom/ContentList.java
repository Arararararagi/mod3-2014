/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.EntityRef;
import org.jdom.IllegalAddException;
import org.jdom.ProcessingInstruction;
import org.jdom.Text;
import org.jdom.filter.Filter;

class ContentList
extends AbstractList
implements List,
Cloneable,
Serializable {
    private static final String CVS_ID = "@(#) $RCSfile: ContentList.java,v $ $Revision: 1.8 $ $Date: 2002/03/15 05:36:48 $ $Name:  $";
    private static final int INITIAL_ARRAY_SIZE = 5;
    private static final int CREATE = 0;
    private static final int HASPREV = 1;
    private static final int HASNEXT = 2;
    private static final int PREV = 3;
    private static final int NEXT = 4;
    private static final int ADD = 5;
    private static final int REMOVE = 6;
    private static final int SET = 7;
    protected ArrayList list;
    protected Object parent;

    private ContentList() {
    }

    protected ContentList(Document document) {
        this.parent = document;
        this.ensureCapacity(5);
    }

    protected ContentList(Element element) {
        this.parent = element;
        this.ensureCapacity(5);
    }

    public void add(int n, Object object) {
        if (object instanceof Element) {
            this.add(n, (Element)object);
        } else if (object instanceof Text) {
            this.add(n, (Text)object);
        } else if (object instanceof Comment) {
            this.add(n, (Comment)object);
        } else if (object instanceof ProcessingInstruction) {
            this.add(n, (ProcessingInstruction)object);
        } else if (object instanceof CDATA) {
            this.add(n, (CDATA)object);
        } else if (object instanceof EntityRef) {
            this.add(n, (EntityRef)object);
        } else {
            if (object == null) {
                throw new IllegalAddException("Cannot add null object");
            }
            throw new IllegalAddException("Class " + object.getClass().getName() + " is of unrecognized type and cannot be added");
        }
    }

    protected void add(int n, CDATA cDATA) {
        if (cDATA == null) {
            throw new IllegalAddException("Cannot add null object");
        }
        if (this.parent instanceof Document) {
            throw new IllegalAddException("A CDATA is not allowed at the document root");
        }
        if (cDATA.getParent() != null) {
            throw new IllegalAddException("The CDATA already has an existing parent \"" + cDATA.getParent().getQualifiedName() + "\"");
        }
        if (this.list == null) {
            if (n == 0) {
                this.ensureCapacity(5);
            } else {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
            }
        }
        this.list.add(n, cDATA);
        cDATA.setParent((Element)this.parent);
        ++this.modCount;
    }

    protected void add(int n, Comment comment) {
        if (comment == null) {
            throw new IllegalAddException("Cannot add null object");
        }
        if (comment.getParent() != null) {
            throw new IllegalAddException("The comment already has an existing parent \"" + comment.getParent().getQualifiedName() + "\"");
        }
        if (this.list == null) {
            if (n == 0) {
                this.ensureCapacity(5);
            } else {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
            }
        }
        if (this.parent instanceof Document) {
            comment.setDocument((Document)this.parent);
        } else {
            comment.setParent((Element)this.parent);
        }
        this.list.add(n, comment);
        ++this.modCount;
    }

    protected void add(int n, Element element) {
        if (element == null) {
            throw new IllegalAddException("Cannot add null object");
        }
        if (element.getParent() != null) {
            throw new IllegalAddException("The element already has an existing parent \"" + element.getParent().getQualifiedName() + "\"");
        }
        if (element == this.parent) {
            throw new IllegalAddException("The element cannot be added to itself");
        }
        if (this.parent instanceof Element && ((Element)this.parent).isAncestor(element)) {
            throw new IllegalAddException("The element cannot be added as a descendent of itself");
        }
        if (this.list == null) {
            if (n == 0) {
                this.ensureCapacity(5);
            } else {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
            }
        }
        if (this.parent instanceof Document) {
            if (this.indexOfFirstElement() >= 0) {
                throw new IllegalAddException("Cannot add a second root element, only one is allowed");
            }
            element.setDocument((Document)this.parent);
        } else {
            element.setParent((Element)this.parent);
        }
        this.list.add(n, element);
        ++this.modCount;
    }

    protected void add(int n, EntityRef entityRef) {
        if (entityRef == null) {
            throw new IllegalAddException("Cannot add null object");
        }
        if (this.parent instanceof Document) {
            throw new IllegalAddException("An EntityRef is not allowed at the document root");
        }
        if (entityRef.getParent() != null) {
            throw new IllegalAddException("The EntityRef already has an existing parent \"" + entityRef.getParent().getQualifiedName() + "\"");
        }
        if (this.list == null) {
            if (n == 0) {
                this.ensureCapacity(5);
            } else {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
            }
        }
        this.list.add(n, entityRef);
        entityRef.setParent((Element)this.parent);
        ++this.modCount;
    }

    protected void add(int n, ProcessingInstruction processingInstruction) {
        if (processingInstruction == null) {
            throw new IllegalAddException("Cannot add null object");
        }
        if (processingInstruction.getParent() != null) {
            throw new IllegalAddException("The PI already has an existing parent \"" + processingInstruction.getParent().getQualifiedName() + "\"");
        }
        if (this.list == null) {
            if (n == 0) {
                this.ensureCapacity(5);
            } else {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
            }
        }
        if (this.parent instanceof Document) {
            processingInstruction.setDocument((Document)this.parent);
        } else {
            processingInstruction.setParent((Element)this.parent);
        }
        this.list.add(n, processingInstruction);
        ++this.modCount;
    }

    protected void add(int n, Text text) {
        if (text == null) {
            throw new IllegalAddException("Cannot add null object");
        }
        if (this.parent instanceof Document) {
            throw new IllegalAddException("A Text not allowed at the document root");
        }
        if (text.getParent() != null) {
            throw new IllegalAddException("The Text already has an existing parent \"" + text.getParent().getQualifiedName() + "\"");
        }
        if (this.list == null) {
            if (n == 0) {
                this.ensureCapacity(5);
            } else {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
            }
        }
        this.list.add(n, text);
        text.setParent((Element)this.parent);
        ++this.modCount;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public boolean addAll(int var1_1, Collection var2_2) {
        if (this.list == null && var1_1 != 0) {
            throw new IndexOutOfBoundsException("Index: " + var1_1 + " Size: " + this.size());
        }
        if (var2_2 == null) return false;
        if (var2_2.size() == 0) {
            return false;
        }
        var3_3 = 0;
        try {
            var4_4 = var2_2.iterator();
            do {
                if (!var4_4.hasNext()) {
                    return true;
                }
                var5_6 = var4_4.next();
                this.add(var1_1 + var3_3, var5_6);
                ++var3_3;
            } while (true);
        }
        catch (RuntimeException var4_5) {
            ** for (var5_7 = 0;
            ; var5_7 < var3_3; ++var5_7)
        }
lbl-1000: // 1 sources:
        {
            this.remove(var1_1 + var5_7);
            continue;
        }
lbl20: // 1 sources:
        throw var4_5;
    }

    public boolean addAll(Collection collection) {
        return this.addAll(this.size(), collection);
    }

    protected void clearAndSet(Collection collection) {
        ArrayList arrayList = this.list;
        this.list = null;
        if (collection != null && collection.size() != 0) {
            this.ensureCapacity(collection.size());
            try {
                this.addAll(0, collection);
            }
            catch (RuntimeException var3_3) {
                this.list = arrayList;
                throw var3_3;
            }
        }
        if (arrayList != null) {
            for (int i = 0; i < arrayList.size(); ++i) {
                this.removeParent(arrayList.get(i));
            }
        }
    }

    protected void ensureCapacity(int n) {
        if (this.list == null) {
            this.list = new ArrayList(n);
        } else {
            this.list.ensureCapacity(n);
        }
    }

    public Object get(int n) {
        if (this.list == null) {
            throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
        }
        return this.list.get(n);
    }

    private int getModCount() {
        return this.modCount;
    }

    protected List getView(Filter filter) {
        return new FilterList(filter);
    }

    protected int indexOfFirstElement() {
        if (this.list != null) {
            for (int i = 0; i < this.list.size(); ++i) {
                if (!(this.list.get(i) instanceof Element)) continue;
                return i;
            }
        }
        return -1;
    }

    public Object remove(int n) {
        if (this.list == null) {
            throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
        }
        Object e = this.list.get(n);
        this.removeParent(e);
        this.list.remove(n);
        ++this.modCount;
        return e;
    }

    private void removeParent(Object object) {
        if (object instanceof Element) {
            Element element = (Element)object;
            element.setParent(null);
        } else if (object instanceof Text) {
            Text text = (Text)object;
            text.setParent(null);
        } else if (object instanceof Comment) {
            Comment comment = (Comment)object;
            comment.setParent(null);
        } else if (object instanceof ProcessingInstruction) {
            ProcessingInstruction processingInstruction = (ProcessingInstruction)object;
            processingInstruction.setParent(null);
        } else if (object instanceof CDATA) {
            CDATA cDATA = (CDATA)object;
            cDATA.setParent(null);
        } else if (object instanceof EntityRef) {
            EntityRef entityRef = (EntityRef)object;
            entityRef.setParent(null);
        } else {
            throw new IllegalArgumentException("Object '" + object + "' unknown");
        }
    }

    public Object set(int n, Object object) {
        int n2;
        if (this.list == null) {
            throw new IndexOutOfBoundsException("Index: " + n + " Size: " + this.size());
        }
        if (object instanceof Element && this.parent instanceof Document && (n2 = this.indexOfFirstElement()) >= 0 && n2 != n) {
            throw new IllegalAddException("Cannot add a second root element, only one is allowed");
        }
        Object object2 = this.remove(n);
        try {
            this.add(n, object);
        }
        catch (RuntimeException var4_5) {
            this.add(n, object2);
            throw var4_5;
        }
        return object2;
    }

    public int size() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public String toString() {
        if (this.list != null && this.list.size() > 0) {
            return this.list.toString();
        }
        return "[]";
    }

    class FilterList
    extends AbstractList {
        protected Filter filter;
        int count;
        int expected;

        FilterList(Filter filter) {
            this.count = 0;
            this.expected = 0;
            this.filter = filter;
        }

        public void add(int n, Object object) {
            if (this.filter.canAdd(object)) {
                int n2 = this.getAdjustedIndex(n);
                ContentList.this.add(n2, object);
                ++this.expected;
                ++this.count;
            } else {
                throw new IllegalAddException("Filter won't allow the " + object.getClass().getName() + " '" + object + "' to be added to the list");
            }
        }

        public Object get(int n) {
            int n2 = this.getAdjustedIndex(n);
            return ContentList.this.get(n2);
        }

        private final int getAdjustedIndex(int n) {
            int n2 = 0;
            for (int i = 0; i < ContentList.this.list.size(); ++i) {
                Object e = ContentList.this.list.get(i);
                if (!this.filter.matches(e)) continue;
                if (n == n2) {
                    return i;
                }
                ++n2;
            }
            if (n == n2) {
                return ContentList.this.list.size();
            }
            return ContentList.this.list.size() + 1;
        }

        public Iterator iterator() {
            return new FilterListIterator(this.filter, 0);
        }

        public ListIterator listIterator() {
            return new FilterListIterator(this.filter, 0);
        }

        public ListIterator listIterator(int n) {
            return new FilterListIterator(this.filter, n);
        }

        public Object remove(int n) {
            int n2 = this.getAdjustedIndex(n);
            Object object = ContentList.this.get(n2);
            if (this.filter.canRemove(object)) {
                object = ContentList.this.remove(n2);
                ++this.expected;
                --this.count;
            } else {
                throw new IllegalAddException("Filter won't allow the " + object.getClass().getName() + " '" + object + "' (index " + n + ") to be removed");
            }
            return object;
        }

        public Object set(int n, Object object) {
            Object object2 = null;
            if (this.filter.canAdd(object)) {
                int n2 = this.getAdjustedIndex(n);
                object2 = ContentList.this.get(n2);
                if (!this.filter.canRemove(object2)) {
                    throw new IllegalAddException("Filter won't allow the " + object2.getClass().getName() + " '" + object2 + "' (index " + n + ") to be removed");
                }
                object2 = ContentList.this.set(n2, object);
                this.expected+=2;
            } else {
                throw new IllegalAddException("Filter won't allow index " + n + " to be set to " + object.getClass().getName());
            }
            return object2;
        }

        public int size() {
            if (this.expected == ContentList.this.getModCount()) {
                return this.count;
            }
            this.count = 0;
            for (int i = 0; i < ContentList.this.size(); ++i) {
                Object e = ContentList.this.list.get(i);
                if (!this.filter.matches(e)) continue;
                ++this.count;
            }
            this.expected = ContentList.this.getModCount();
            return this.count;
        }
    }

    class FilterListIterator
    implements ListIterator {
        Filter filter;
        int lastOperation;
        int initialCursor;
        int cursor;
        int last;
        int expected;

        FilterListIterator(Filter filter, int n) {
            this.filter = filter;
            this.initialCursor = this.initializeCursor(n);
            this.last = -1;
            this.expected = ContentList.this.getModCount();
            this.lastOperation = 0;
        }

        public void add(Object object) {
            this.checkConcurrentModification();
            if (this.lastOperation != 3 && this.lastOperation != 4) {
                throw new IllegalStateException("no preceeding call to prev() or next()");
            }
            if (!this.filter.canAdd(object)) {
                throw new IllegalAddException("Filter won't allow add of " + object.getClass().getName());
            }
            ContentList.this.add(this.last, object);
            this.expected = ContentList.this.getModCount();
            this.lastOperation = 5;
        }

        private void checkConcurrentModification() {
            if (this.expected != ContentList.this.getModCount()) {
                throw new ConcurrentModificationException();
            }
        }

        public boolean hasNext() {
            this.checkConcurrentModification();
            switch (this.lastOperation) {
                case 0: {
                    this.cursor = this.initialCursor;
                    break;
                }
                case 3: {
                    this.cursor = this.last;
                    break;
                }
                case 4: {
                    this.cursor = this.moveForward(this.last + 1);
                    break;
                }
                case 5: 
                case 6: {
                    this.cursor = this.moveForward(this.last);
                    break;
                }
                case 1: {
                    this.cursor = this.moveForward(this.cursor + 1);
                    break;
                }
            }
            if (this.lastOperation != 0) {
                this.lastOperation = 2;
            }
            return this.cursor < ContentList.this.size();
        }

        public boolean hasPrevious() {
            this.checkConcurrentModification();
            switch (this.lastOperation) {
                case 0: {
                    this.cursor = this.initialCursor;
                    if (this.cursor < ContentList.this.size()) break;
                    this.cursor = this.moveBackward(this.initialCursor);
                    break;
                }
                case 3: 
                case 6: {
                    this.cursor = this.moveBackward(this.last - 1);
                    break;
                }
                case 2: {
                    this.cursor = this.moveBackward(this.cursor - 1);
                    break;
                }
                case 4: 
                case 5: {
                    this.cursor = this.last;
                    break;
                }
            }
            if (this.lastOperation != 0) {
                this.lastOperation = 1;
            }
            return this.cursor >= 0;
        }

        private int initializeCursor(int n) {
            if (n < 0) {
                throw new IndexOutOfBoundsException("Index: " + n);
            }
            int n2 = 0;
            for (int i = 0; i < ContentList.this.size(); ++i) {
                Object object = ContentList.this.get(i);
                if (!this.filter.matches(object)) continue;
                if (n == n2) {
                    return i;
                }
                ++n2;
            }
            if (n > n2) {
                throw new IndexOutOfBoundsException("Index: " + n + " Size: " + n2);
            }
            return ContentList.this.size();
        }

        private int moveBackward(int n) {
            if (n >= ContentList.this.size()) {
                n = ContentList.this.size() - 1;
            }
            for (int i = n; i >= 0; --i) {
                Object object = ContentList.this.get(i);
                if (!this.filter.matches(object)) continue;
                return i;
            }
            return -1;
        }

        private int moveForward(int n) {
            if (n < 0) {
                n = 0;
            }
            for (int i = n; i < ContentList.this.size(); ++i) {
                Object object = ContentList.this.get(i);
                if (!this.filter.matches(object)) continue;
                return i;
            }
            return ContentList.this.size();
        }

        public Object next() {
            this.checkConcurrentModification();
            if (!this.hasNext()) {
                throw new IllegalStateException("no next item");
            }
            this.last = this.cursor;
            this.lastOperation = 4;
            return ContentList.this.get(this.last);
        }

        public int nextIndex() {
            this.checkConcurrentModification();
            this.hasNext();
            int n = 0;
            for (int i = 0; i < ContentList.this.size(); ++i) {
                if (!this.filter.matches(ContentList.this.get(i))) continue;
                if (i == this.cursor) {
                    return n;
                }
                ++n;
            }
            this.expected = ContentList.this.getModCount();
            return n;
        }

        public Object previous() {
            this.checkConcurrentModification();
            if (!this.hasPrevious()) {
                throw new IllegalStateException("no previous item");
            }
            this.last = this.cursor;
            this.lastOperation = 3;
            return ContentList.this.get(this.last);
        }

        public int previousIndex() {
            this.checkConcurrentModification();
            if (this.hasPrevious()) {
                int n = 0;
                for (int i = 0; i < ContentList.this.size(); ++i) {
                    if (!this.filter.matches(ContentList.this.get(i))) continue;
                    if (i == this.cursor) {
                        return n;
                    }
                    ++n;
                }
            }
            return -1;
        }

        public void remove() {
            this.checkConcurrentModification();
            if (this.lastOperation != 3 && this.lastOperation != 4) {
                throw new IllegalStateException("no preceeding call to prev() or next()");
            }
            Object object = ContentList.this.get(this.last);
            if (!this.filter.canRemove(object)) {
                throw new IllegalAddException("Filter won't allow " + object.getClass().getName() + " (index " + this.last + ") to be removed");
            }
            ContentList.this.remove(this.last);
            this.expected = ContentList.this.getModCount();
            this.lastOperation = 6;
        }

        public void set(Object object) {
            this.checkConcurrentModification();
            if (this.lastOperation != 3 && this.lastOperation != 4) {
                throw new IllegalStateException("no preceeding call to prev() or next()");
            }
            if (this.filter.canAdd(object)) {
                Object object2 = ContentList.this.get(this.last);
                if (!this.filter.canRemove(object2)) {
                    throw new IllegalAddException("Filter won't allow " + object2.getClass().getName() + " (index " + this.last + ") to be removed");
                }
            } else {
                throw new IllegalAddException("Filter won't allow index " + this.last + " to be set to " + object.getClass().getName());
            }
            ContentList.this.set(this.last, object);
            this.expected = ContentList.this.getModCount();
            this.lastOperation = 6;
        }
    }

}

