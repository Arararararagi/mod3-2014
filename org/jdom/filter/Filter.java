/*
 * Decompiled with CFR 0_102.
 */
package org.jdom.filter;

public interface Filter {
    public boolean canAdd(Object var1);

    public boolean canRemove(Object var1);

    public boolean matches(Object var1);
}

