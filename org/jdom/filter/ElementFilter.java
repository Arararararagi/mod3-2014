/*
 * Decompiled with CFR 0_102.
 */
package org.jdom.filter;

import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.filter.Filter;

public class ElementFilter
implements Filter {
    private static final String CVS_ID = "@(#) $RCSfile: ElementFilter.java,v $ $Revision: 1.2 $ $Date: 2002/03/13 06:25:33 $ $Name: jdom_1_0_b8 $";
    protected String name;
    protected Namespace namespace;

    public ElementFilter() {
    }

    public ElementFilter(String string) {
        this.name = string;
    }

    public ElementFilter(String string, Namespace namespace) {
        this.name = string;
        this.namespace = namespace;
    }

    public ElementFilter(Namespace namespace) {
        this.namespace = namespace;
    }

    public boolean canAdd(Object object) {
        return this.matches(object);
    }

    public boolean canRemove(Object object) {
        if (object instanceof Element) {
            return true;
        }
        return false;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object instanceof ElementFilter) {
            ElementFilter elementFilter = (ElementFilter)object;
            if (this.name == elementFilter.name && this.namespace == elementFilter.namespace) {
                return true;
            }
        }
        return false;
    }

    public boolean matches(Object object) {
        if (object instanceof Element) {
            Element element = (Element)object;
            if (this.name == null) {
                if (this.namespace == null) {
                    return true;
                }
                return this.namespace.equals(element.getNamespace());
            }
            if (this.name.equals(element.getName())) {
                if (this.namespace == null) {
                    return true;
                }
                return this.namespace.equals(element.getNamespace());
            }
        }
        return false;
    }
}

