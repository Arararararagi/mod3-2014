/*
 * Decompiled with CFR 0_102.
 */
package org.jdom.output;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.EntityRef;
import org.jdom.Namespace;
import org.jdom.ProcessingInstruction;
import org.jdom.Text;

public class XMLOutputter
implements Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: XMLOutputter.java,v $ $Revision: 1.76 $ $Date: 2002/03/15 05:36:48 $ $Name: jdom_1_0_b8 $";
    private boolean omitDeclaration = false;
    private String encoding = "UTF-8";
    private boolean omitEncoding = false;
    private static final String STANDARD_INDENT = "  ";
    private static final String STANDARD_LINE_SEPARATOR = "\r\n";
    Format noFormatting;
    Format defaultFormat;
    Format currentFormat;

    public XMLOutputter() {
        this.noFormatting = new Format();
        this.currentFormat = this.defaultFormat = new Format();
    }

    public XMLOutputter(String string) {
        this.noFormatting = new Format();
        this.currentFormat = this.defaultFormat = new Format();
        this.setIndent(string);
    }

    public XMLOutputter(String string, boolean bl) {
        this.noFormatting = new Format();
        this.currentFormat = this.defaultFormat = new Format();
        this.setIndent(string);
        this.setNewlines(bl);
    }

    public XMLOutputter(String string, boolean bl, String string2) {
        this.noFormatting = new Format();
        this.currentFormat = this.defaultFormat = new Format();
        this.setEncoding(string2);
        this.setIndent(string);
        this.setNewlines(bl);
    }

    public XMLOutputter(XMLOutputter xMLOutputter) {
        this.noFormatting = new Format();
        this.currentFormat = this.defaultFormat = new Format();
        this.encoding = xMLOutputter.encoding;
        this.omitDeclaration = xMLOutputter.omitDeclaration;
        this.omitEncoding = xMLOutputter.omitEncoding;
        this.defaultFormat = (Format)xMLOutputter.defaultFormat.clone();
    }

    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException var1_1) {
            throw new RuntimeException(var1_1.toString());
        }
    }

    protected NamespaceStack createNamespaceStack() {
        return new NamespaceStack();
    }

    private boolean endsWithWhite(String string) {
        if (string != null && string.length() > 0 && this.isWhitespace(string.charAt(string.length() - 1))) {
            return true;
        }
        return false;
    }

    public String escapeAttributeEntities(String string) {
        StringBuffer stringBuffer = null;
        for (int i = 0; i < string.length(); ++i) {
            String string2;
            char c = string.charAt(i);
            switch (c) {
                case '<': {
                    string2 = "&lt;";
                    break;
                }
                case '>': {
                    string2 = "&gt;";
                    break;
                }
                case '\"': {
                    string2 = "&quot;";
                    break;
                }
                case '&': {
                    string2 = "&amp;";
                    break;
                }
                default: {
                    string2 = null;
                }
            }
            if (stringBuffer == null) {
                if (string2 == null) continue;
                stringBuffer = new StringBuffer(string.length() + 20);
                stringBuffer.append(string.substring(0, i));
                stringBuffer.append(string2);
                continue;
            }
            if (string2 == null) {
                stringBuffer.append(c);
                continue;
            }
            stringBuffer.append(string2);
        }
        return stringBuffer == null ? string : stringBuffer.toString();
    }

    public String escapeElementEntities(String string) {
        StringBuffer stringBuffer = null;
        for (int i = 0; i < string.length(); ++i) {
            String string2;
            char c = string.charAt(i);
            switch (c) {
                case '<': {
                    string2 = "&lt;";
                    break;
                }
                case '>': {
                    string2 = "&gt;";
                    break;
                }
                case '&': {
                    string2 = "&amp;";
                    break;
                }
                default: {
                    string2 = null;
                }
            }
            if (stringBuffer == null) {
                if (string2 == null) continue;
                stringBuffer = new StringBuffer(string.length() + 20);
                stringBuffer.append(string.substring(0, i));
                stringBuffer.append(string2);
                continue;
            }
            if (string2 == null) {
                stringBuffer.append(c);
                continue;
            }
            stringBuffer.append(string2);
        }
        return stringBuffer == null ? string : stringBuffer.toString();
    }

    protected void indent(Writer writer) throws IOException {
        this.indent(writer, 0);
    }

    protected void indent(Writer writer, int n) throws IOException {
        if (this.currentFormat.newlines) {
            if (this.currentFormat.indent == null || this.currentFormat.indent.equals("")) {
                return;
            }
            for (int i = 0; i < n; ++i) {
                writer.write(this.currentFormat.indent);
            }
        }
    }

    private boolean isAllWhitespace(Object object) {
        String string = null;
        if (object instanceof String) {
            string = (String)object;
        } else if (object instanceof CDATA) {
            string = ((CDATA)object).getText();
        } else if (object instanceof Text) {
            string = ((Text)object).getText();
        } else {
            return false;
        }
        for (int i = 0; i < string.length(); ++i) {
            if (this.isWhitespace(string.charAt(i))) continue;
            return false;
        }
        return true;
    }

    private boolean isWhitespace(char c) {
        if (" \t\n\r".indexOf(c) < 0) {
            return false;
        }
        return true;
    }

    protected Writer makeWriter(OutputStream outputStream) throws UnsupportedEncodingException {
        return this.makeWriter(outputStream, this.encoding);
    }

    protected Writer makeWriter(OutputStream outputStream, String string) throws UnsupportedEncodingException {
        if ("UTF-8".equals(string)) {
            string = "UTF8";
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter((OutputStream)new BufferedOutputStream(outputStream), string));
        return bufferedWriter;
    }

    protected void newline(Writer writer) throws IOException {
        if (this.currentFormat.newlines) {
            writer.write(this.currentFormat.lineSeparator);
        }
    }

    private int nextNonText(List list, int n) {
        int n2;
        if (n < 0) {
            n = 0;
        }
        for (n2 = n; n2 < list.size(); ++n2) {
            if (!(list.get(n2) instanceof CDATA) && !(list.get(n2) instanceof Text)) break;
        }
        return n2;
    }

    public void output(String string, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(string, writer);
    }

    public void output(String string, Writer writer) throws IOException {
        this.printString(string, writer);
        writer.flush();
    }

    public void output(List list, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(list, writer);
    }

    public void output(List list, Writer writer) throws IOException {
        this.printContent(list, writer, 0, this.createNamespaceStack());
        writer.flush();
    }

    public void output(CDATA cDATA, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(cDATA, writer);
    }

    public void output(CDATA cDATA, Writer writer) throws IOException {
        this.printCDATA(cDATA, writer);
        writer.flush();
    }

    public void output(Comment comment, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(comment, writer);
    }

    public void output(Comment comment, Writer writer) throws IOException {
        this.printComment(comment, writer);
        writer.flush();
    }

    public void output(DocType docType, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(docType, writer);
    }

    public void output(DocType docType, Writer writer) throws IOException {
        this.printDocType(docType, writer);
        writer.flush();
    }

    public void output(Document document, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(document, writer);
    }

    public void output(Document document, Writer writer) throws IOException {
        this.printDeclaration(document, writer, this.encoding);
        if (document.getDocType() != null) {
            this.printDocType(document.getDocType(), writer);
        }
        List list = document.getContent();
        for (int i = 0; i < list.size(); ++i) {
            Object e = list.get(i);
            if (e instanceof Element) {
                this.printElement(document.getRootElement(), writer, 0, this.createNamespaceStack());
            } else if (e instanceof Comment) {
                this.printComment((Comment)e, writer);
            } else if (e instanceof ProcessingInstruction) {
                this.printProcessingInstruction((ProcessingInstruction)e, writer);
            }
            this.newline(writer);
            this.indent(writer, 0);
        }
        writer.write(this.currentFormat.lineSeparator);
        writer.flush();
    }

    public void output(Element element, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(element, writer);
    }

    public void output(Element element, Writer writer) throws IOException {
        this.printElement(element, writer, 0, this.createNamespaceStack());
        writer.flush();
    }

    public void output(EntityRef entityRef, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(entityRef, writer);
    }

    public void output(EntityRef entityRef, Writer writer) throws IOException {
        this.printEntityRef(entityRef, writer);
        writer.flush();
    }

    public void output(ProcessingInstruction processingInstruction, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(processingInstruction, writer);
    }

    public void output(ProcessingInstruction processingInstruction, Writer writer) throws IOException {
        this.printProcessingInstruction(processingInstruction, writer);
        writer.flush();
    }

    public void output(Text text, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.output(text, writer);
    }

    public void output(Text text, Writer writer) throws IOException {
        this.printText(text, writer);
        writer.flush();
    }

    public void outputElementContent(Element element, OutputStream outputStream) throws IOException {
        Writer writer = this.makeWriter(outputStream);
        this.outputElementContent(element, writer);
    }

    public void outputElementContent(Element element, Writer writer) throws IOException {
        this.printContent(element.getContent(), writer, 0, this.createNamespaceStack());
        writer.flush();
    }

    public String outputString(String string) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(string, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(List list) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(list, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(CDATA cDATA) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(cDATA, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(Comment comment) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(comment, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(DocType docType) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(docType, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(Document document) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(document, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(Element element) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(element, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(EntityRef entityRef) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(entityRef, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(ProcessingInstruction processingInstruction) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(processingInstruction, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public String outputString(Text text) {
        StringWriter stringWriter;
        stringWriter = new StringWriter();
        try {
            this.output(text, (Writer)stringWriter);
        }
        catch (IOException v0) {}
        return stringWriter.toString();
    }

    public int parseArgs(String[] arrstring, int n) {
        while (n < arrstring.length) {
            if (arrstring[n].equals("-omitDeclaration")) {
                this.setOmitDeclaration(true);
            } else if (arrstring[n].equals("-omitEncoding")) {
                this.setOmitEncoding(true);
            } else if (arrstring[n].equals("-indent")) {
                this.setIndent(arrstring[++n]);
            } else if (arrstring[n].equals("-indentSize")) {
                this.setIndentSize(Integer.parseInt(arrstring[++n]));
            } else if (arrstring[n].startsWith("-expandEmpty")) {
                this.setExpandEmptyElements(true);
            } else if (arrstring[n].equals("-encoding")) {
                this.setEncoding(arrstring[++n]);
            } else if (arrstring[n].equals("-newlines")) {
                this.setNewlines(true);
            } else if (arrstring[n].equals("-lineSeparator")) {
                this.setLineSeparator(arrstring[++n]);
            } else if (arrstring[n].equals("-trimAllWhite")) {
                this.setTrimAllWhite(true);
            } else if (arrstring[n].equals("-textTrim")) {
                this.setTextTrim(true);
            } else if (arrstring[n].equals("-textNormalize")) {
                this.setTextNormalize(true);
            } else {
                return n;
            }
            ++n;
        }
        return n;
    }

    private void printAdditionalNamespaces(Element element, Writer writer, NamespaceStack namespaceStack) throws IOException {
        List list = element.getAdditionalNamespaces();
        if (list != null) {
            for (int i = 0; i < list.size(); ++i) {
                Namespace namespace = (Namespace)list.get(i);
                this.printNamespace(namespace, writer, namespaceStack);
            }
        }
    }

    protected void printAttributes(List list, Element element, Writer writer, NamespaceStack namespaceStack) throws IOException {
        for (int i = 0; i < list.size(); ++i) {
            Attribute attribute = (Attribute)list.get(i);
            Namespace namespace = attribute.getNamespace();
            if (namespace != Namespace.NO_NAMESPACE && namespace != Namespace.XML_NAMESPACE) {
                this.printNamespace(namespace, writer, namespaceStack);
            }
            writer.write(" ");
            writer.write(attribute.getQualifiedName());
            writer.write("=");
            writer.write("\"");
            writer.write(this.escapeAttributeEntities(attribute.getValue()));
            writer.write("\"");
        }
    }

    protected void printCDATA(CDATA cDATA, Writer writer) throws IOException {
        String string = this.currentFormat.textNormalize ? cDATA.getTextNormalize() : (this.currentFormat.textTrim ? cDATA.getText().trim() : cDATA.getText());
        writer.write("<![CDATA[");
        writer.write(string);
        writer.write("]]>");
    }

    protected void printComment(Comment comment, Writer writer) throws IOException {
        writer.write("<!--");
        writer.write(comment.getText());
        writer.write("-->");
    }

    protected void printContent(List list, Writer writer, int n, NamespaceStack namespaceStack) throws IOException {
        this.printContentRange(list, 0, list.size(), writer, n, namespaceStack);
    }

    protected void printContentRange(List list, int n, int n2, Writer writer, int n3, NamespaceStack namespaceStack) throws IOException {
        int n4 = n;
        while (n4 < n2) {
            boolean bl = n4 == n;
            Object e = list.get(n4);
            if (e instanceof CDATA || e instanceof Text) {
                int n5 = this.skipLeadingWhite(list, n4);
                n4 = this.nextNonText(list, n5);
                if (n5 >= n4) continue;
                if (!bl) {
                    this.newline(writer);
                }
                this.indent(writer, n3);
                this.printTextRange(list, n5, n4, writer);
                continue;
            }
            if (!bl) {
                this.newline(writer);
            }
            this.indent(writer, n3);
            if (e instanceof Comment) {
                this.printComment((Comment)e, writer);
            } else if (e instanceof Element) {
                this.printElement((Element)e, writer, n3, namespaceStack);
            } else if (e instanceof EntityRef) {
                this.printEntityRef((EntityRef)e, writer);
            } else if (e instanceof ProcessingInstruction) {
                this.printProcessingInstruction((ProcessingInstruction)e, writer);
            }
            ++n4;
        }
    }

    protected void printDeclaration(Document document, Writer writer, String string) throws IOException {
        if (!this.omitDeclaration) {
            writer.write("<?xml version=\"1.0\"");
            if (!this.omitEncoding) {
                writer.write(" encoding=\"" + string + "\"");
            }
            writer.write("?>");
            writer.write(this.currentFormat.lineSeparator);
        }
    }

    protected void printDocType(DocType docType, Writer writer) throws IOException {
        String string = docType.getPublicID();
        String string2 = docType.getSystemID();
        String string3 = docType.getInternalSubset();
        boolean bl = false;
        writer.write("<!DOCTYPE ");
        writer.write(docType.getElementName());
        if (string != null) {
            writer.write(" PUBLIC \"");
            writer.write(string);
            writer.write("\"");
            bl = true;
        }
        if (string2 != null) {
            if (!bl) {
                writer.write(" SYSTEM");
            }
            writer.write(" \"");
            writer.write(string2);
            writer.write("\"");
        }
        if (!(string3 == null || string3.equals(""))) {
            writer.write(" [\n");
            writer.write(docType.getInternalSubset());
            writer.write("]");
        }
        writer.write(">");
        writer.write(this.currentFormat.lineSeparator);
    }

    protected void printElement(Element element, Writer writer, int n, NamespaceStack namespaceStack) throws IOException {
        int n2;
        List list = element.getAttributes();
        List list2 = element.getContent();
        String string = null;
        if (list != null) {
            string = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
        }
        Format format = this.currentFormat;
        if ("default".equals(string)) {
            this.currentFormat = this.defaultFormat;
        } else if ("preserve".equals(string)) {
            this.currentFormat = this.noFormatting;
        }
        writer.write("<");
        writer.write(element.getQualifiedName());
        int n3 = namespaceStack.size();
        this.printElementNamespace(element, writer, namespaceStack);
        this.printAdditionalNamespaces(element, writer, namespaceStack);
        if (list != null) {
            this.printAttributes(list, element, writer, namespaceStack);
        }
        if ((n2 = this.skipLeadingWhite(list2, 0)) >= list2.size()) {
            if (this.currentFormat.expandEmptyElements) {
                writer.write("></");
                writer.write(element.getQualifiedName());
                writer.write(">");
            } else {
                writer.write(" />");
            }
        } else {
            writer.write(">");
            if (this.nextNonText(list2, n2) < list2.size()) {
                this.newline(writer);
                this.printContentRange(list2, n2, list2.size(), writer, n + 1, namespaceStack);
                this.newline(writer);
                this.indent(writer, n);
            } else {
                this.printTextRange(list2, n2, list2.size(), writer);
            }
            writer.write("</");
            writer.write(element.getQualifiedName());
            writer.write(">");
        }
        while (namespaceStack.size() > n3) {
            namespaceStack.pop();
        }
        this.currentFormat = format;
    }

    protected void printElementContent(Element element, Writer writer, int n, NamespaceStack namespaceStack) throws IOException {
        this.printContent(element.getContent(), writer, n, namespaceStack);
    }

    private void printElementNamespace(Element element, Writer writer, NamespaceStack namespaceStack) throws IOException {
        Namespace namespace = element.getNamespace();
        if (namespace == Namespace.XML_NAMESPACE) {
            return;
        }
        if (namespace != Namespace.NO_NAMESPACE || namespaceStack.getURI("") != null) {
            this.printNamespace(namespace, writer, namespaceStack);
        }
    }

    protected void printEntityRef(EntityRef entityRef, Writer writer) throws IOException {
        writer.write("&");
        writer.write(entityRef.getName());
        writer.write(";");
    }

    private void printNamespace(Namespace namespace, Writer writer, NamespaceStack namespaceStack) throws IOException {
        String string = namespace.getPrefix();
        String string2 = namespace.getURI();
        if (string2.equals(namespaceStack.getURI(string))) {
            return;
        }
        writer.write(" xmlns");
        if (!string.equals("")) {
            writer.write(":");
            writer.write(string);
        }
        writer.write("=\"");
        writer.write(string2);
        writer.write("\"");
        namespaceStack.push(namespace);
    }

    protected void printProcessingInstruction(ProcessingInstruction processingInstruction, Writer writer) throws IOException {
        String string = processingInstruction.getTarget();
        String string2 = processingInstruction.getData();
        if (!"".equals(string2)) {
            writer.write("<?");
            writer.write(string);
            writer.write(" ");
            writer.write(string2);
            writer.write("?>");
        } else {
            writer.write("<?");
            writer.write(string);
            writer.write("?>");
        }
    }

    protected void printString(String string, Writer writer) throws IOException {
        if (this.currentFormat.textNormalize) {
            string = Text.normalizeString(string);
        } else if (this.currentFormat.textTrim) {
            string = string.trim();
        }
        writer.write(this.escapeElementEntities(string));
    }

    protected void printText(Text text, Writer writer) throws IOException {
        String string = this.currentFormat.textNormalize ? text.getTextNormalize() : (this.currentFormat.textTrim ? text.getText().trim() : text.getText());
        writer.write(this.escapeElementEntities(string));
    }

    protected void printTextRange(List list, int n, int n2, Writer writer) throws IOException {
        String string = null;
        if ((n = this.skipLeadingWhite(list, n)) < list.size()) {
            n2 = this.skipTrialingWhite(list, n2);
            for (int i = n; i < n2; ++i) {
                Object e = list.get(i);
                String string2 = e instanceof CDATA ? ((CDATA)e).getText() : ((Text)e).getText();
                if (string2 == null || "".equals(string2)) continue;
                if (string != null && (this.currentFormat.textNormalize || this.currentFormat.textTrim) && (this.endsWithWhite(string) || this.startsWithWhite(string2))) {
                    writer.write(" ");
                }
                if (e instanceof CDATA) {
                    this.printCDATA((CDATA)e, writer);
                } else {
                    this.printString(string2, writer);
                }
                string = string2;
            }
        }
    }

    public void setEncoding(String string) {
        this.encoding = string;
    }

    public void setExpandEmptyElements(boolean bl) {
        this.defaultFormat.expandEmptyElements = bl;
    }

    public void setIndent(int n) {
        this.setIndentSize(n);
    }

    public void setIndent(String string) {
        if ("".equals(string)) {
            string = null;
        }
        this.defaultFormat.indent = string;
    }

    public void setIndent(boolean bl) {
        this.defaultFormat.indent = bl ? "  " : null;
    }

    public void setIndentLevel(int n) {
    }

    public void setIndentSize(int n) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < n; ++i) {
            stringBuffer.append(" ");
        }
        this.defaultFormat.indent = stringBuffer.toString();
    }

    public void setLineSeparator(String string) {
        this.defaultFormat.lineSeparator = string;
    }

    public void setNewlines(boolean bl) {
        this.defaultFormat.newlines = bl;
    }

    public void setOmitDeclaration(boolean bl) {
        this.omitDeclaration = bl;
    }

    public void setOmitEncoding(boolean bl) {
        this.omitEncoding = bl;
    }

    public void setPadText(boolean bl) {
    }

    public void setSuppressDeclaration(boolean bl) {
        this.omitDeclaration = bl;
    }

    public void setTextNormalize(boolean bl) {
        this.defaultFormat.textNormalize = bl;
    }

    public void setTextTrim(boolean bl) {
        this.defaultFormat.textTrim = bl;
    }

    public void setTrimAllWhite(boolean bl) {
        this.defaultFormat.trimAllWhite = bl;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private int skipLeadingWhite(List list, int n) {
        if (n < 0) {
            n = 0;
        }
        int n2 = n;
        if (!(this.currentFormat.trimAllWhite || this.currentFormat.textNormalize || this.currentFormat.textTrim)) {
            if (!this.currentFormat.newlines) return n2;
        }
        while (n2 < list.size()) {
            if (!this.isAllWhitespace(list.get(n2))) return n2;
            ++n2;
        }
        return n2;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private int skipTrialingWhite(List list, int n) {
        if (n > list.size()) {
            n = list.size();
        }
        int n2 = n;
        if (!(this.currentFormat.trimAllWhite || this.currentFormat.textNormalize || this.currentFormat.textTrim)) {
            if (!this.currentFormat.newlines) return n2;
        }
        while (n2 >= 0) {
            if (!this.isAllWhitespace(list.get(n2 - 1))) return n2;
            --n2;
        }
        return n2;
    }

    private boolean startsWithWhite(String string) {
        if (string != null && string.length() > 0 && this.isWhitespace(string.charAt(0))) {
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        block5 : for (int i = 0; i < this.defaultFormat.lineSeparator.length(); ++i) {
            char c = this.defaultFormat.lineSeparator.charAt(i);
            switch (c) {
                case '\r': {
                    stringBuffer.append("\\r");
                    continue block5;
                }
                case '\n': {
                    stringBuffer.append("\\n");
                    continue block5;
                }
                case '\t': {
                    stringBuffer.append("\\t");
                    continue block5;
                }
                default: {
                    stringBuffer.append("[" + c + "]");
                    continue block5;
                }
            }
        }
        return "XMLOutputter[omitDeclaration = " + this.omitDeclaration + ", " + "encoding = " + this.encoding + ", " + "omitEncoding = " + this.omitEncoding + ", " + "indent = '" + this.defaultFormat.indent + "'" + ", " + "expandEmptyElements = " + this.defaultFormat.expandEmptyElements + ", " + "newlines = " + this.defaultFormat.newlines + ", " + "lineSeparator = '" + stringBuffer.toString() + "', " + "trimAllWhite = " + this.defaultFormat.trimAllWhite + "textTrim = " + this.defaultFormat.textTrim + "textNormalize = " + this.defaultFormat.textNormalize + "]";
    }

    class Format
    implements Cloneable {
        String indent;
        boolean expandEmptyElements;
        String lineSeparator;
        boolean trimAllWhite;
        boolean textTrim;
        boolean textNormalize;
        boolean newlines;

        Format() {
            this.indent = null;
            this.expandEmptyElements = false;
            this.lineSeparator = "\r\n";
            this.trimAllWhite = false;
            this.textTrim = false;
            this.textNormalize = false;
            this.newlines = false;
        }

        protected Object clone() {
            Format format;
            format = null;
            try {
                format = (Format)super.clone();
            }
            catch (CloneNotSupportedException v0) {}
            return format;
        }
    }

    protected class NamespaceStack
    extends org.jdom.output.NamespaceStack {
        protected NamespaceStack() {
        }
    }

}

