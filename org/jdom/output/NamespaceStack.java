/*
 * Decompiled with CFR 0_102.
 */
package org.jdom.output;

import java.util.Stack;
import org.jdom.Namespace;

class NamespaceStack {
    private static final String CVS_ID = "@(#) $RCSfile: NamespaceStack.java,v $ $Revision: 1.8 $ $Date: 2002/03/12 07:57:06 $ $Name: jdom_1_0_b8 $";
    private Stack prefixes = new Stack();
    private Stack uris = new Stack();

    public String getURI(String string) {
        int n = this.prefixes.lastIndexOf(string);
        if (n == -1) {
            return null;
        }
        String string2 = (String)this.uris.elementAt(n);
        return string2;
    }

    public String pop() {
        String string = (String)this.prefixes.pop();
        this.uris.pop();
        return string;
    }

    public void push(Namespace namespace) {
        this.prefixes.push(namespace.getPrefix());
        this.uris.push(namespace.getURI());
    }

    public int size() {
        return this.prefixes.size();
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        String string = System.getProperty("line.separator");
        stringBuffer.append("Stack: " + this.prefixes.size() + string);
        for (int i = 0; i < this.prefixes.size(); ++i) {
            stringBuffer.append(String.valueOf(String.valueOf(this.prefixes.elementAt(i))) + "&" + this.uris.elementAt(i) + string);
        }
        return stringBuffer.toString();
    }
}

