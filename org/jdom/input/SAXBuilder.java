/*
 * Decompiled with CFR 0_102.
 */
package org.jdom.input;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.BuilderErrorHandler;
import org.jdom.input.JDOMFactory;
import org.jdom.input.SAXHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class SAXBuilder {
    private static final String CVS_ID = "@(#) $RCSfile: SAXBuilder.java,v $ $Revision: 1.64 $ $Date: 2002/02/26 04:10:33 $ $Name: jdom_1_0_b8 $";
    private static final String DEFAULT_SAX_DRIVER = "org.apache.xerces.parsers.SAXParser";
    private boolean validate;
    private boolean expand = true;
    private String saxDriverClass;
    private ErrorHandler saxErrorHandler = null;
    private EntityResolver saxEntityResolver = null;
    private DTDHandler saxDTDHandler = null;
    private XMLFilter saxXMLFilter = null;
    protected JDOMFactory factory = null;
    private boolean ignoringWhite = false;
    private HashMap features = new HashMap(5);
    private HashMap properties = new HashMap(5);

    public SAXBuilder() {
        this(false);
    }

    public SAXBuilder(String string) {
        this(string, false);
    }

    public SAXBuilder(String string, boolean bl) {
        this.saxDriverClass = string;
        this.validate = bl;
    }

    public SAXBuilder(boolean bl) {
        this.validate = bl;
    }

    public Document build(File file) throws JDOMException {
        try {
            URL uRL = this.fileToURL(file);
            return this.build(uRL);
        }
        catch (MalformedURLException var2_3) {
            throw new JDOMException("Error in building", var2_3);
        }
    }

    public Document build(InputStream inputStream) throws JDOMException {
        return this.build(new InputSource(inputStream));
    }

    public Document build(InputStream inputStream, String string) throws JDOMException {
        InputSource inputSource = new InputSource(inputStream);
        inputSource.setSystemId(string);
        return this.build(inputSource);
    }

    public Document build(Reader reader) throws JDOMException {
        return this.build(new InputSource(reader));
    }

    public Document build(Reader reader, String string) throws JDOMException {
        InputSource inputSource = new InputSource(reader);
        inputSource.setSystemId(string);
        return this.build(inputSource);
    }

    public Document build(String string) throws JDOMException {
        return this.build(new InputSource(string));
    }

    public Document build(URL uRL) throws JDOMException {
        String string = uRL.toExternalForm();
        return this.build(new InputSource(string));
    }

    public Document build(InputSource inputSource) throws JDOMException {
        SAXHandler sAXHandler = null;
        try {
            try {
                sAXHandler = this.createContentHandler();
                this.configureContentHandler(sAXHandler);
                XMLReader xMLReader = this.createParser();
                if (this.saxXMLFilter != null) {
                    XMLFilter xMLFilter = this.saxXMLFilter;
                    while (xMLFilter.getParent() instanceof XMLFilter) {
                        xMLFilter = (XMLFilter)xMLFilter.getParent();
                    }
                    xMLFilter.setParent(xMLReader);
                    xMLReader = this.saxXMLFilter;
                }
                this.configureParser(xMLReader, sAXHandler);
                xMLReader.parse(inputSource);
                Document document = sAXHandler.getDocument();
                Object var5_8 = null;
                sAXHandler = null;
                return document;
            }
            catch (Exception var6_4) {
                if (var6_4 instanceof SAXParseException) {
                    SAXParseException sAXParseException = (SAXParseException)var6_4;
                    String string = sAXParseException.getSystemId();
                    if (string != null) {
                        throw new JDOMException("Error on line " + sAXParseException.getLineNumber() + " of document " + string, var6_4);
                    }
                    throw new JDOMException("Error on line " + sAXParseException.getLineNumber(), var6_4);
                }
                if (var6_4 instanceof JDOMException) {
                    throw (JDOMException)var6_4;
                }
                throw new JDOMException("Error in building", var6_4);
            }
        }
        catch (Throwable var4_11) {
            Object var5_9 = null;
            sAXHandler = null;
            throw var4_11;
        }
    }

    protected void configureContentHandler(SAXHandler sAXHandler) throws Exception {
        sAXHandler.setExpandEntities(this.expand);
        sAXHandler.setIgnoringElementContentWhitespace(this.ignoringWhite);
    }

    protected void configureParser(XMLReader xMLReader, SAXHandler sAXHandler) throws Exception {
        block28 : {
            boolean bl;
            Object object;
            Object object2;
            xMLReader.setContentHandler(sAXHandler);
            if (this.saxEntityResolver != null) {
                xMLReader.setEntityResolver(this.saxEntityResolver);
            }
            if (this.saxDTDHandler != null) {
                xMLReader.setDTDHandler(this.saxDTDHandler);
            } else {
                xMLReader.setDTDHandler(sAXHandler);
            }
            if (this.saxErrorHandler != null) {
                xMLReader.setErrorHandler(this.saxErrorHandler);
            } else {
                xMLReader.setErrorHandler(new BuilderErrorHandler());
            }
            Iterator iterator = this.features.keySet().iterator();
            while (iterator.hasNext()) {
                object = (String)iterator.next();
                object2 = (Boolean)this.features.get(object);
                this.internalSetFeature(xMLReader, (String)object, object2.booleanValue(), (String)object);
            }
            object = this.properties.keySet().iterator();
            while (object.hasNext()) {
                object2 = (String)object.next();
                Object v = this.properties.get(object2);
                this.internalSetProperty(xMLReader, (String)object2, v, (String)object2);
            }
            bl = false;
            try {
                xMLReader.setProperty("http://xml.org/sax/handlers/LexicalHandler", sAXHandler);
                bl = true;
            }
            catch (SAXNotSupportedException v0) {
            }
            catch (SAXNotRecognizedException v1) {}
            if (!bl) {
                try {
                    xMLReader.setProperty("http://xml.org/sax/properties/lexical-handler", sAXHandler);
                    bl = true;
                }
                catch (SAXNotSupportedException v2) {
                }
                catch (SAXNotRecognizedException v3) {}
            }
            if (!this.expand) {
                try {
                    xMLReader.setProperty("http://xml.org/sax/properties/declaration-handler", sAXHandler);
                }
                catch (SAXNotSupportedException v4) {
                }
                catch (SAXNotRecognizedException v5) {}
            }
            try {
                this.internalSetFeature(xMLReader, "http://xml.org/sax/features/validation", this.validate, "Validation");
            }
            catch (JDOMException var6_8) {
                if (!this.validate) break block28;
                throw var6_8;
            }
        }
        this.internalSetFeature(xMLReader, "http://xml.org/sax/features/namespaces", true, "Namespaces");
        this.internalSetFeature(xMLReader, "http://xml.org/sax/features/namespace-prefixes", false, "Namespace prefixes");
        try {
            if (xMLReader.getFeature("http://xml.org/sax/features/external-general-entities") != this.expand) {
                xMLReader.setFeature("http://xml.org/sax/features/external-general-entities", this.expand);
            }
        }
        catch (SAXNotRecognizedException v6) {
        }
        catch (SAXNotSupportedException v7) {}
    }

    protected SAXHandler createContentHandler() throws Exception {
        SAXHandler sAXHandler = new SAXHandler(this.factory);
        return sAXHandler;
    }

    protected XMLReader createParser() throws Exception {
        XMLReader xMLReader;
        xMLReader = null;
        if (this.saxDriverClass != null) {
            xMLReader = XMLReaderFactory.createXMLReader(this.saxDriverClass);
        } else {
            try {
                Class class_ = Class.forName("javax.xml.parsers.SAXParserFactory");
                Method method = class_.getMethod("newInstance", null);
                Object object = method.invoke(null, null);
                Method method2 = class_.getMethod("setValidating", Boolean.TYPE);
                method2.invoke(object, new Boolean(this.validate));
                Method method3 = class_.getMethod("newSAXParser", null);
                Object object2 = method3.invoke(object, null);
                Class class_2 = object2.getClass();
                Method method4 = class_2.getMethod("getXMLReader", null);
                xMLReader = (XMLReader)method4.invoke(object2, null);
            }
            catch (ClassNotFoundException v0) {
            }
            catch (InvocationTargetException v1) {
            }
            catch (NoSuchMethodException v2) {
            }
            catch (IllegalAccessException v3) {}
        }
        if (xMLReader == null) {
            xMLReader = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
            this.saxDriverClass = xMLReader.getClass().getName();
        }
        return xMLReader;
    }

    protected URL fileToURL(File file) throws MalformedURLException {
        String string = file.getAbsolutePath();
        if (File.separatorChar != '/') {
            string = string.replace(File.separatorChar, '/');
        }
        if (!string.startsWith("/")) {
            string = "/" + string;
        }
        if (!string.endsWith("/") && file.isDirectory()) {
            string = String.valueOf(string) + "/";
        }
        return new URL("file", "", string);
    }

    private void internalSetFeature(XMLReader xMLReader, String string, boolean bl, String string2) throws JDOMException {
        try {
            xMLReader.setFeature(string, bl);
        }
        catch (SAXNotSupportedException v0) {
            throw new JDOMException(String.valueOf(string2) + " feature not supported for SAX driver " + xMLReader.getClass().getName());
        }
        catch (SAXNotRecognizedException v1) {
            throw new JDOMException(String.valueOf(string2) + " feature not recognized for SAX driver " + xMLReader.getClass().getName());
        }
    }

    private void internalSetProperty(XMLReader xMLReader, String string, Object object, String string2) throws JDOMException {
        try {
            xMLReader.setProperty(string, object);
        }
        catch (SAXNotSupportedException v0) {
            throw new JDOMException(String.valueOf(string2) + " property not supported for SAX driver " + xMLReader.getClass().getName());
        }
        catch (SAXNotRecognizedException v1) {
            throw new JDOMException(String.valueOf(string2) + " property not recognized for SAX driver " + xMLReader.getClass().getName());
        }
    }

    public void setDTDHandler(DTDHandler dTDHandler) {
        this.saxDTDHandler = dTDHandler;
    }

    public void setEntityResolver(EntityResolver entityResolver) {
        this.saxEntityResolver = entityResolver;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.saxErrorHandler = errorHandler;
    }

    public void setExpandEntities(boolean bl) {
        this.expand = bl;
    }

    public void setFactory(JDOMFactory jDOMFactory) {
        this.factory = jDOMFactory;
    }

    public void setFeature(String string, boolean bl) {
        this.features.put(string, new Boolean(bl));
    }

    public void setIgnoringElementContentWhitespace(boolean bl) {
        this.ignoringWhite = bl;
    }

    public void setProperty(String string, Object object) {
        this.properties.put(string, object);
    }

    public void setValidation(boolean bl) {
        this.validate = bl;
    }

    public void setXMLFilter(XMLFilter xMLFilter) {
        this.saxXMLFilter = xMLFilter;
    }
}

