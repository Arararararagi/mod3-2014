/*
 * Decompiled with CFR 0_102.
 */
package org.jdom.input;

import java.util.Map;
import org.jdom.Attribute;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.EntityRef;
import org.jdom.Namespace;
import org.jdom.ProcessingInstruction;
import org.jdom.Text;
import org.jdom.input.JDOMFactory;

public class DefaultJDOMFactory
implements JDOMFactory {
    private static final String CVS_ID = "@(#) $RCSfile: DefaultJDOMFactory.java,v $ $Revision: 1.6 $ $Date: 2002/03/12 07:57:06 $ $Name: jdom_1_0_b8 $";

    public Attribute attribute(String string, String string2) {
        return new Attribute(string, string2);
    }

    public Attribute attribute(String string, String string2, int n) {
        return new Attribute(string, string2, n);
    }

    public Attribute attribute(String string, String string2, int n, Namespace namespace) {
        return new Attribute(string, string2, n, namespace);
    }

    public Attribute attribute(String string, String string2, Namespace namespace) {
        return new Attribute(string, string2, namespace);
    }

    public CDATA cdata(String string) {
        return new CDATA(string);
    }

    public Comment comment(String string) {
        return new Comment(string);
    }

    public DocType docType(String string) {
        return new DocType(string);
    }

    public DocType docType(String string, String string2) {
        return new DocType(string, string2);
    }

    public DocType docType(String string, String string2, String string3) {
        return new DocType(string, string2, string3);
    }

    public Document document(Element element) {
        return new Document(element);
    }

    public Document document(Element element, DocType docType) {
        return new Document(element, docType);
    }

    public Element element(String string) {
        return new Element(string);
    }

    public Element element(String string, String string2) {
        return new Element(string, string2);
    }

    public Element element(String string, String string2, String string3) {
        return new Element(string, string2, string3);
    }

    public Element element(String string, Namespace namespace) {
        return new Element(string, namespace);
    }

    public EntityRef entityRef(String string) {
        return new EntityRef(string);
    }

    public EntityRef entityRef(String string, String string2, String string3) {
        return new EntityRef(string, string2, string3);
    }

    public ProcessingInstruction processingInstruction(String string, String string2) {
        return new ProcessingInstruction(string, string2);
    }

    public ProcessingInstruction processingInstruction(String string, Map map) {
        return new ProcessingInstruction(string, map);
    }

    public Text text(String string) {
        return new Text(string);
    }
}

