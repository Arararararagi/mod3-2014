/*
 * Decompiled with CFR 0_102.
 */
package org.jdom.input;

import java.io.IOException;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import org.jdom.Attribute;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.EntityRef;
import org.jdom.Namespace;
import org.jdom.ProcessingInstruction;
import org.jdom.Text;
import org.jdom.input.DefaultJDOMFactory;
import org.jdom.input.JDOMFactory;
import org.xml.sax.Attributes;
import org.xml.sax.DTDHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler
extends DefaultHandler
implements LexicalHandler,
DeclHandler,
DTDHandler {
    private static final String CVS_ID = "@(#) $RCSfile: SAXHandler.java,v $ $Revision: 1.39 $ $Date: 2002/03/15 05:36:48 $ $Name: jdom_1_0_b8 $";
    private static final Map attrNameToTypeMap = new HashMap(13);
    private Document document;
    protected Stack stack;
    protected boolean atRoot;
    protected boolean inDTD = false;
    protected boolean inInternalSubset = false;
    protected boolean previousCDATA = false;
    protected boolean inCDATA = false;
    private boolean expand = true;
    protected boolean suppress = false;
    private int entityDepth = 0;
    protected LinkedList declaredNamespaces;
    protected LinkedList availableNamespaces;
    private StringBuffer buffer = new StringBuffer();
    private StringBuffer textBuffer = new StringBuffer(4096);
    private Map externalEntities;
    private JDOMFactory factory;
    private boolean ignoringWhite = false;
    private Locator locator;

    static {
        attrNameToTypeMap.put("CDATA", new Integer(1));
        attrNameToTypeMap.put("ID", new Integer(2));
        attrNameToTypeMap.put("IDREF", new Integer(3));
        attrNameToTypeMap.put("IDREFS", new Integer(4));
        attrNameToTypeMap.put("ENTITY", new Integer(5));
        attrNameToTypeMap.put("ENTITIES", new Integer(6));
        attrNameToTypeMap.put("NMTOKEN", new Integer(7));
        attrNameToTypeMap.put("NMTOKENS", new Integer(8));
        attrNameToTypeMap.put("NOTATION", new Integer(9));
        attrNameToTypeMap.put("ENUMERATION", new Integer(10));
    }

    public SAXHandler() throws IOException {
        this((JDOMFactory)null);
    }

    public SAXHandler(Document document) throws IOException {
        this(new DefaultJDOMFactory());
        this.document = document;
    }

    public SAXHandler(JDOMFactory jDOMFactory) throws IOException {
        this.factory = jDOMFactory != null ? jDOMFactory : new DefaultJDOMFactory();
        this.atRoot = true;
        this.stack = new Stack();
        this.declaredNamespaces = new LinkedList();
        this.availableNamespaces = new LinkedList();
        this.availableNamespaces.add(Namespace.XML_NAMESPACE);
        this.externalEntities = new HashMap();
        this.document = this.factory.document(null);
    }

    protected void appendExternalId(String string, String string2) {
        if (string != null) {
            this.buffer.append(" PUBLIC \"").append(string).append("\"");
        }
        if (string2 != null) {
            if (string == null) {
                this.buffer.append(" SYSTEM ");
            } else {
                this.buffer.append(" ");
            }
            this.buffer.append("\"").append(string2).append("\"");
        }
    }

    public void attributeDecl(String string, String string2, String string3, String string4, String string5) throws SAXException {
        if (!this.inInternalSubset) {
            return;
        }
        this.buffer.append("  <!ATTLIST ").append(string).append(" ").append(string2).append(" ").append(string3).append(" ");
        if (string4 != null) {
            this.buffer.append(string4);
        } else {
            this.buffer.append("\"").append(string5).append("\"");
        }
        if (string4 != null && string4.equals("#FIXED")) {
            this.buffer.append(" \"").append(string5).append("\"");
        }
        this.buffer.append(">\n");
    }

    public void characters(char[] arrc, int n, int n2) throws SAXException {
        if (this.suppress || n2 == 0) {
            return;
        }
        if (this.previousCDATA != this.inCDATA) {
            this.flushCharacters();
        }
        this.textBuffer.append(arrc, n, n2);
    }

    public void comment(char[] arrc, int n, int n2) throws SAXException {
        if (this.suppress) {
            return;
        }
        this.flushCharacters();
        String string = new String(arrc, n, n2);
        if (this.inDTD && this.inInternalSubset && !this.expand) {
            this.buffer.append("  <!--").append(string).append("-->\n");
            return;
        }
        if (!(this.inDTD || string.equals(""))) {
            if (this.stack.empty()) {
                this.document.addContent(this.factory.comment(string));
            } else {
                this.getCurrentElement().addContent(this.factory.comment(string));
            }
        }
    }

    public void elementDecl(String string, String string2) throws SAXException {
        if (!this.inInternalSubset) {
            return;
        }
        this.buffer.append("  <!ELEMENT ").append(string).append(" ").append(string2).append(">\n");
    }

    public void endCDATA() throws SAXException {
        if (this.suppress) {
            return;
        }
        this.previousCDATA = true;
        this.inCDATA = false;
    }

    public void endDTD() throws SAXException {
        this.document.getDocType().setInternalSubset(this.buffer.toString());
        this.inDTD = false;
        this.inInternalSubset = false;
    }

    public void endElement(String string, String string2, String string3) throws SAXException {
        if (this.suppress) {
            return;
        }
        this.flushCharacters();
        try {
            Element element = (Element)this.stack.pop();
            List list = element.getAdditionalNamespaces();
            if (list.size() > 0) {
                this.availableNamespaces.removeAll(list);
            }
        }
        catch (EmptyStackException v0) {
            throw new SAXException("Ill-formed XML document (missing opening tag for " + string2 + ")");
        }
        if (this.stack.empty()) {
            this.atRoot = true;
        }
    }

    public void endEntity(String string) throws SAXException {
        --this.entityDepth;
        if (this.entityDepth == 0) {
            this.suppress = false;
        }
        if (string.equals("[dtd]")) {
            this.inInternalSubset = true;
        }
    }

    public void endPrefixMapping(String string) throws SAXException {
        if (this.suppress) {
            return;
        }
        Iterator iterator = this.availableNamespaces.iterator();
        while (iterator.hasNext()) {
            Namespace namespace = (Namespace)iterator.next();
            if (!string.equals(namespace.getPrefix())) continue;
            iterator.remove();
            return;
        }
    }

    public void externalEntityDecl(String string, String string2, String string3) throws SAXException {
        this.externalEntities.put(string, new String[]{string2, string3});
        if (!this.inInternalSubset) {
            return;
        }
        this.buffer.append("  <!ENTITY ").append(string);
        this.appendExternalId(string2, string3);
        this.buffer.append(">\n");
    }

    protected void flushCharacters() throws SAXException {
        if (this.textBuffer.length() == 0) {
            this.previousCDATA = this.inCDATA;
            return;
        }
        String string = this.textBuffer.toString();
        this.textBuffer.setLength(0);
        if (this.previousCDATA) {
            this.getCurrentElement().addContent(this.factory.cdata(string));
        } else {
            this.getCurrentElement().addContent(this.factory.text(string));
        }
        this.previousCDATA = this.inCDATA;
    }

    private int getAttributeType(String string) {
        Integer n = (Integer)attrNameToTypeMap.get(string);
        if (n == null) {
            if (string != null && string.length() > 0 && string.charAt(0) == '(') {
                return 10;
            }
            return 0;
        }
        return n;
    }

    protected Element getCurrentElement() throws SAXException {
        try {
            return (Element)this.stack.peek();
        }
        catch (EmptyStackException v0) {
            throw new SAXException("Ill-formed XML document (multiple root elements detected)");
        }
    }

    public Document getDocument() {
        return this.document;
    }

    public Locator getDocumentLocator() {
        return this.locator;
    }

    public boolean getExpandEntities() {
        return this.expand;
    }

    public JDOMFactory getFactory() {
        return this.factory;
    }

    public boolean getIgnoringElementContentWhitespace() {
        return this.ignoringWhite;
    }

    private Namespace getNamespace(String string) {
        Iterator iterator = this.availableNamespaces.iterator();
        while (iterator.hasNext()) {
            Namespace namespace = (Namespace)iterator.next();
            if (!string.equals(namespace.getPrefix())) continue;
            return namespace;
        }
        return Namespace.NO_NAMESPACE;
    }

    public void ignorableWhitespace(char[] arrc, int n, int n2) throws SAXException {
        if (this.suppress) {
            return;
        }
        if (this.ignoringWhite) {
            return;
        }
        if (n2 == 0) {
            return;
        }
        this.textBuffer.append(arrc, n, n2);
    }

    public void internalEntityDecl(String string, String string2) throws SAXException {
        if (!this.inInternalSubset) {
            return;
        }
        this.buffer.append("  <!ENTITY ");
        if (string.startsWith("%")) {
            this.buffer.append("% ").append(string.substring(1));
        } else {
            this.buffer.append(string);
        }
        this.buffer.append(" \"").append(string2).append("\">\n");
    }

    public void notationDecl(String string, String string2, String string3) throws SAXException {
        if (!this.inInternalSubset) {
            return;
        }
        this.buffer.append("  <!NOTATION ").append(string);
        this.appendExternalId(string2, string3);
        this.buffer.append(">\n");
    }

    public void processingInstruction(String string, String string2) throws SAXException {
        if (this.suppress) {
            return;
        }
        this.flushCharacters();
        if (this.atRoot) {
            this.document.addContent(this.factory.processingInstruction(string, string2));
        } else {
            this.getCurrentElement().addContent(this.factory.processingInstruction(string, string2));
        }
    }

    public void setDocumentLocator(Locator locator) {
        this.locator = locator;
    }

    public void setExpandEntities(boolean bl) {
        this.expand = bl;
    }

    public void setIgnoringElementContentWhitespace(boolean bl) {
        this.ignoringWhite = bl;
    }

    public void skippedEntity(String string) throws SAXException {
        if (string.startsWith("%")) {
            return;
        }
        this.flushCharacters();
        this.getCurrentElement().addContent(this.factory.entityRef(string));
    }

    public void startCDATA() throws SAXException {
        if (this.suppress) {
            return;
        }
        this.inCDATA = true;
    }

    public void startDTD(String string, String string2, String string3) throws SAXException {
        this.flushCharacters();
        this.document.setDocType(this.factory.docType(string, string2, string3));
        this.inDTD = true;
        this.inInternalSubset = true;
    }

    public void startElement(String string, String string2, String string3, Attributes attributes) throws SAXException {
        if (this.suppress) {
            return;
        }
        Element element = null;
        if (!(string == null || string.equals(""))) {
            String string4 = "";
            if (!string3.equals(string2)) {
                int n = string3.indexOf(":");
                string4 = string3.substring(0, n);
            }
            Namespace namespace = Namespace.getNamespace(string4, string);
            element = this.factory.element(string2, namespace);
        } else {
            element = this.factory.element(string2);
        }
        if (this.declaredNamespaces.size() > 0) {
            this.transferNamespaces(element);
        }
        int n = attributes.getLength();
        for (int i = 0; i < n; ++i) {
            Attribute attribute = null;
            String string5 = attributes.getLocalName(i);
            String string6 = attributes.getQName(i);
            int n2 = this.getAttributeType(attributes.getType(i));
            if (string6.startsWith("xmlns:") || string6.equals("xmlns")) continue;
            if (!string6.equals(string5)) {
                String string7 = string6.substring(0, string6.indexOf(":"));
                attribute = this.factory.attribute(string5, attributes.getValue(i), n2, this.getNamespace(string7));
            } else {
                attribute = this.factory.attribute(string5, attributes.getValue(i), n2);
            }
            element.setAttribute(attribute);
        }
        this.flushCharacters();
        if (this.atRoot) {
            this.document.setRootElement(element);
            this.stack.push(element);
            this.atRoot = false;
        } else {
            this.getCurrentElement().addContent(element);
            this.stack.push(element);
        }
    }

    public void startEntity(String string) throws SAXException {
        ++this.entityDepth;
        if (this.expand || this.entityDepth > 1) {
            return;
        }
        if (string.equals("[dtd]")) {
            this.inInternalSubset = false;
            return;
        }
        if (!(this.inDTD || string.equals("amp") || string.equals("lt") || string.equals("gt") || string.equals("apos") || string.equals("quot") || this.expand)) {
            String string2 = null;
            String string3 = null;
            String[] arrstring = (String[])this.externalEntities.get(string);
            if (arrstring != null) {
                string2 = arrstring[0];
                string3 = arrstring[1];
            }
            if (!(this.atRoot || this.stack.isEmpty())) {
                this.flushCharacters();
                EntityRef entityRef = this.factory.entityRef(string, string2, string3);
                this.getCurrentElement().addContent(entityRef);
            }
            this.suppress = true;
        }
    }

    public void startPrefixMapping(String string, String string2) throws SAXException {
        if (this.suppress) {
            return;
        }
        Namespace namespace = Namespace.getNamespace(string, string2);
        this.declaredNamespaces.add(namespace);
    }

    private void transferNamespaces(Element element) {
        Iterator iterator = this.declaredNamespaces.iterator();
        while (iterator.hasNext()) {
            Namespace namespace = (Namespace)iterator.next();
            this.availableNamespaces.addFirst(namespace);
            element.addNamespaceDeclaration(namespace);
        }
        this.declaredNamespaces.clear();
    }

    public void unparsedEntityDecl(String string, String string2, String string3, String string4) throws SAXException {
        if (!this.inInternalSubset) {
            return;
        }
        this.buffer.append("  <!ENTITY ").append(string);
        this.appendExternalId(string2, string3);
        this.buffer.append(" NDATA ").append(string4);
        this.buffer.append(">\n");
    }
}

