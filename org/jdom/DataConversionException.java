/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import org.jdom.JDOMException;

public class DataConversionException
extends JDOMException {
    private static final String CVS_ID = "@(#) $RCSfile: DataConversionException.java,v $ $Revision: 1.7 $ $Date: 2002/01/08 09:17:10 $ $Name: jdom_1_0_b8 $";

    public DataConversionException(String string, String string2) {
        super("The XML construct " + string + " could not be converted to a " + string2);
    }
}

