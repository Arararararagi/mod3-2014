/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.Element;
import org.jdom.Namespace;

public final class Verifier {
    private static final String CVS_ID = "@(#) $RCSfile: Verifier.java,v $ $Revision: 1.32 $ $Date: 2002/03/12 07:57:06 $ $Name: jdom_1_0_b8 $";

    private Verifier() {
    }

    public static final String checkAttributeName(String string) {
        String string2 = Verifier.checkXMLName(string);
        if (string2 != null) {
            return string2;
        }
        if (string.equals("xml:space") || string.equals("xml:lang")) {
            return null;
        }
        if (string.indexOf(":") != -1) {
            return "Attribute names cannot contain colons";
        }
        if (string.equals("xmlns")) {
            return "An Attribute name may not be \"xmlns\"; use the Namespace class to manage namespaces";
        }
        return null;
    }

    public static final String checkCDATASection(String string) {
        String string2 = null;
        string2 = Verifier.checkCharacterData(string);
        if (string2 != null) {
            return string2;
        }
        if (string.indexOf("]]>") != -1) {
            return "CDATA cannot internally contain a CDATA ending delimiter (]]>)";
        }
        return null;
    }

    public static final String checkCharacterData(String string) {
        if (string == null) {
            return "A null is not a legal XML value";
        }
        int n = string.length();
        for (int i = 0; i < n; ++i) {
            if (Verifier.isXMLCharacter(string.charAt(i))) continue;
            return "0x" + Integer.toHexString(string.charAt(i)) + " is not a legal XML character";
        }
        return null;
    }

    public static final String checkCommentData(String string) {
        String string2 = null;
        string2 = Verifier.checkCharacterData(string);
        if (string2 != null) {
            return string2;
        }
        if (string.indexOf("--") != -1) {
            return "Comments cannot contain double hyphens (--)";
        }
        return null;
    }

    public static final String checkElementName(String string) {
        String string2 = Verifier.checkXMLName(string);
        if (string2 != null) {
            return string2;
        }
        if (string.indexOf(":") != -1) {
            return "Element names cannot contain colons";
        }
        return null;
    }

    public static final String checkNamespaceCollision(Attribute attribute, Element element) {
        Namespace namespace = attribute.getNamespace();
        String string = namespace.getPrefix();
        if ("".equals(string)) {
            return null;
        }
        return Verifier.checkNamespaceCollision(namespace, element);
    }

    public static final String checkNamespaceCollision(Namespace namespace, List list) {
        if (list == null) {
            return null;
        }
        String string = null;
        Iterator iterator = list.iterator();
        while (string == null && iterator.hasNext()) {
            Object e = iterator.next();
            if (e instanceof Attribute) {
                string = Verifier.checkNamespaceCollision(namespace, (Attribute)e);
                continue;
            }
            if (e instanceof Element) {
                string = Verifier.checkNamespaceCollision(namespace, (Element)e);
                continue;
            }
            if (!(e instanceof Namespace) || (string = Verifier.checkNamespaceCollision(namespace, (Namespace)e)) == null) continue;
            string = String.valueOf(string) + " with an additional namespace declared by the element";
        }
        return string;
    }

    public static final String checkNamespaceCollision(Namespace namespace, Attribute attribute) {
        String string = Verifier.checkNamespaceCollision(namespace, attribute.getNamespace());
        if (string != null) {
            string = String.valueOf(string) + " with an attribute namespace prefix on the element";
        }
        return string;
    }

    public static final String checkNamespaceCollision(Namespace namespace, Element element) {
        String string = Verifier.checkNamespaceCollision(namespace, element.getNamespace());
        if (string != null) {
            return String.valueOf(string) + " with the element namespace prefix";
        }
        string = Verifier.checkNamespaceCollision(namespace, element.getAdditionalNamespaces());
        if (string != null) {
            return string;
        }
        string = Verifier.checkNamespaceCollision(namespace, element.getAttributes());
        if (string != null) {
            return string;
        }
        return null;
    }

    public static final String checkNamespaceCollision(Namespace namespace, Namespace namespace2) {
        if (namespace == Namespace.NO_NAMESPACE || namespace2 == Namespace.NO_NAMESPACE) {
            return null;
        }
        String string = null;
        String string2 = namespace.getPrefix();
        String string3 = namespace.getURI();
        String string4 = namespace2.getPrefix();
        String string5 = namespace2.getURI();
        if (string2.equals(string4) && !string3.equals(string5)) {
            string = "The namespace prefix \"" + string2 + "\" collides";
        }
        return string;
    }

    public static final String checkNamespacePrefix(String string) {
        if (string == null || string.equals("")) {
            return null;
        }
        char c = string.charAt(0);
        if (Verifier.isXMLDigit(c)) {
            return "Namespace prefixes cannot begin with a number";
        }
        if (c == '$') {
            return "Namespace prefixes cannot begin with a dollar sign ($)";
        }
        if (c == '-') {
            return "Namespace prefixes cannot begin with a hyphen (-)";
        }
        if (c == '.') {
            return "Namespace prefixes cannot begin with a period (.)";
        }
        if (string.toLowerCase().startsWith("xml")) {
            return "Namespace prefixes cannot begin with \"xml\" in any combination of case";
        }
        int n = string.length();
        for (int i = 0; i < n; ++i) {
            char c2 = string.charAt(i);
            if (Verifier.isXMLNameCharacter(c2)) continue;
            return "Namespace prefixes cannot contain the character \"" + c2 + "\"";
        }
        if (string.indexOf(":") != -1) {
            return "Namespace prefixes cannot contain colons";
        }
        return null;
    }

    public static final String checkNamespaceURI(String string) {
        if (string == null || string.equals("")) {
            return null;
        }
        char c = string.charAt(0);
        if (Character.isDigit(c)) {
            return "Namespace URIs cannot begin with a number";
        }
        if (c == '$') {
            return "Namespace URIs cannot begin with a dollar sign ($)";
        }
        if (c == '-') {
            return "Namespace URIs cannot begin with a hyphen (-)";
        }
        return null;
    }

    public static final String checkProcessingInstructionTarget(String string) {
        String string2 = Verifier.checkXMLName(string);
        if (string2 != null) {
            return string2;
        }
        if (string.indexOf(":") != -1) {
            return "Processing instruction targets cannot contain colons";
        }
        if (string.equalsIgnoreCase("xml")) {
            return "Processing instructions cannot have a target of \"xml\" in any combination of case. (Note that the \"<?xml ... ?>\" declaration at the beginning of a document is not a processing instruction and should not be added as one; it is written automatically during output, e.g. by XMLOutputter.)";
        }
        return null;
    }

    public static final String checkPublicID(String string) {
        String string2 = null;
        if (string == null) {
            return null;
        }
        for (int i = 0; i < string.length(); ++i) {
            char c = string.charAt(i);
            if (Verifier.isXMLPublicIDCharacter(c)) continue;
            string2 = String.valueOf(c) + " is not a legal character in public IDs";
            break;
        }
        return string2;
    }

    public static final String checkSystemLiteral(String string) {
        String string2 = null;
        if (string == null) {
            return null;
        }
        string2 = string.indexOf(39) != -1 && string.indexOf(34) != -1 ? "System literals cannot simultaneously contain both single and double quotes." : Verifier.checkCharacterData(string);
        return string2;
    }

    public static String checkXMLName(String string) {
        if (string == null || string.length() == 0 || string.trim().equals("")) {
            return "XML names cannot be null or empty";
        }
        char c = string.charAt(0);
        if (!Verifier.isXMLNameStartCharacter(c)) {
            return "XML names cannot begin with the character \"" + c + "\"";
        }
        int n = string.length();
        for (int i = 0; i < n; ++i) {
            char c2 = string.charAt(i);
            if (Verifier.isXMLNameCharacter(c2)) continue;
            return "XML names cannot contain the character \"" + c2 + "\"";
        }
        return null;
    }

    public static boolean isXMLCharacter(char c) {
        if (c == '\n') {
            return true;
        }
        if (c == '\r') {
            return true;
        }
        if (c == '\t') {
            return true;
        }
        if (c < ' ') {
            return false;
        }
        if (c <= '\ud7ff') {
            return true;
        }
        if (c < '\ue000') {
            return false;
        }
        if (c <= '\ufffd') {
            return true;
        }
        if (c < '\u10000') {
            return false;
        }
        if (c <= '\u10ffff') {
            return true;
        }
        return false;
    }

    private static boolean isXMLCharacterOld(char c) {
        if (c >= ' ' && c <= '\ud7ff') {
            return true;
        }
        if (c >= '\ue000' && c <= '\ufffd') {
            return true;
        }
        if (c >= '\u10000' && c <= '\u10ffff') {
            return true;
        }
        if (c == '\n') {
            return true;
        }
        if (c == '\r') {
            return true;
        }
        if (c == '\t') {
            return true;
        }
        return false;
    }

    public static boolean isXMLCombiningChar(char c) {
        if (c < '\u0300') {
            return false;
        }
        if (c <= '\u0345') {
            return true;
        }
        if (c < '\u0360') {
            return false;
        }
        if (c <= '\u0361') {
            return true;
        }
        if (c < '\u0483') {
            return false;
        }
        if (c <= '\u0486') {
            return true;
        }
        if (c < '\u0591') {
            return false;
        }
        if (c <= '\u05a1') {
            return true;
        }
        if (c < '\u05a3') {
            return false;
        }
        if (c <= '\u05b9') {
            return true;
        }
        if (c < '\u05bb') {
            return false;
        }
        if (c <= '\u05bd') {
            return true;
        }
        if (c == '\u05bf') {
            return true;
        }
        if (c < '\u05c1') {
            return false;
        }
        if (c <= '\u05c2') {
            return true;
        }
        if (c == '\u05c4') {
            return true;
        }
        if (c < '\u064b') {
            return false;
        }
        if (c <= '\u0652') {
            return true;
        }
        if (c == '\u0670') {
            return true;
        }
        if (c < '\u06d6') {
            return false;
        }
        if (c <= '\u06dc') {
            return true;
        }
        if (c < '\u06dd') {
            return false;
        }
        if (c <= '\u06df') {
            return true;
        }
        if (c < '\u06e0') {
            return false;
        }
        if (c <= '\u06e4') {
            return true;
        }
        if (c < '\u06e7') {
            return false;
        }
        if (c <= '\u06e8') {
            return true;
        }
        if (c < '\u06ea') {
            return false;
        }
        if (c <= '\u06ed') {
            return true;
        }
        if (c < '\u0901') {
            return false;
        }
        if (c <= '\u0903') {
            return true;
        }
        if (c == '\u093c') {
            return true;
        }
        if (c < '\u093e') {
            return false;
        }
        if (c <= '\u094c') {
            return true;
        }
        if (c == '\u094d') {
            return true;
        }
        if (c < '\u0951') {
            return false;
        }
        if (c <= '\u0954') {
            return true;
        }
        if (c < '\u0962') {
            return false;
        }
        if (c <= '\u0963') {
            return true;
        }
        if (c < '\u0981') {
            return false;
        }
        if (c <= '\u0983') {
            return true;
        }
        if (c == '\u09bc') {
            return true;
        }
        if (c == '\u09be') {
            return true;
        }
        if (c == '\u09bf') {
            return true;
        }
        if (c < '\u09c0') {
            return false;
        }
        if (c <= '\u09c4') {
            return true;
        }
        if (c < '\u09c7') {
            return false;
        }
        if (c <= '\u09c8') {
            return true;
        }
        if (c < '\u09cb') {
            return false;
        }
        if (c <= '\u09cd') {
            return true;
        }
        if (c == '\u09d7') {
            return true;
        }
        if (c < '\u09e2') {
            return false;
        }
        if (c <= '\u09e3') {
            return true;
        }
        if (c == '\u0a02') {
            return true;
        }
        if (c == '\u0a3c') {
            return true;
        }
        if (c == '\u0a3e') {
            return true;
        }
        if (c == '\u0a3f') {
            return true;
        }
        if (c < '\u0a40') {
            return false;
        }
        if (c <= '\u0a42') {
            return true;
        }
        if (c < '\u0a47') {
            return false;
        }
        if (c <= '\u0a48') {
            return true;
        }
        if (c < '\u0a4b') {
            return false;
        }
        if (c <= '\u0a4d') {
            return true;
        }
        if (c < '\u0a70') {
            return false;
        }
        if (c <= '\u0a71') {
            return true;
        }
        if (c < '\u0a81') {
            return false;
        }
        if (c <= '\u0a83') {
            return true;
        }
        if (c == '\u0abc') {
            return true;
        }
        if (c < '\u0abe') {
            return false;
        }
        if (c <= '\u0ac5') {
            return true;
        }
        if (c < '\u0ac7') {
            return false;
        }
        if (c <= '\u0ac9') {
            return true;
        }
        if (c < '\u0acb') {
            return false;
        }
        if (c <= '\u0acd') {
            return true;
        }
        if (c < '\u0b01') {
            return false;
        }
        if (c <= '\u0b03') {
            return true;
        }
        if (c == '\u0b3c') {
            return true;
        }
        if (c < '\u0b3e') {
            return false;
        }
        if (c <= '\u0b43') {
            return true;
        }
        if (c < '\u0b47') {
            return false;
        }
        if (c <= '\u0b48') {
            return true;
        }
        if (c < '\u0b4b') {
            return false;
        }
        if (c <= '\u0b4d') {
            return true;
        }
        if (c < '\u0b56') {
            return false;
        }
        if (c <= '\u0b57') {
            return true;
        }
        if (c < '\u0b82') {
            return false;
        }
        if (c <= '\u0b83') {
            return true;
        }
        if (c < '\u0bbe') {
            return false;
        }
        if (c <= '\u0bc2') {
            return true;
        }
        if (c < '\u0bc6') {
            return false;
        }
        if (c <= '\u0bc8') {
            return true;
        }
        if (c < '\u0bca') {
            return false;
        }
        if (c <= '\u0bcd') {
            return true;
        }
        if (c == '\u0bd7') {
            return true;
        }
        if (c < '\u0c01') {
            return false;
        }
        if (c <= '\u0c03') {
            return true;
        }
        if (c < '\u0c3e') {
            return false;
        }
        if (c <= '\u0c44') {
            return true;
        }
        if (c < '\u0c46') {
            return false;
        }
        if (c <= '\u0c48') {
            return true;
        }
        if (c < '\u0c4a') {
            return false;
        }
        if (c <= '\u0c4d') {
            return true;
        }
        if (c < '\u0c55') {
            return false;
        }
        if (c <= '\u0c56') {
            return true;
        }
        if (c < '\u0c82') {
            return false;
        }
        if (c <= '\u0c83') {
            return true;
        }
        if (c < '\u0cbe') {
            return false;
        }
        if (c <= '\u0cc4') {
            return true;
        }
        if (c < '\u0cc6') {
            return false;
        }
        if (c <= '\u0cc8') {
            return true;
        }
        if (c < '\u0cca') {
            return false;
        }
        if (c <= '\u0ccd') {
            return true;
        }
        if (c < '\u0cd5') {
            return false;
        }
        if (c <= '\u0cd6') {
            return true;
        }
        if (c < '\u0d02') {
            return false;
        }
        if (c <= '\u0d03') {
            return true;
        }
        if (c < '\u0d3e') {
            return false;
        }
        if (c <= '\u0d43') {
            return true;
        }
        if (c < '\u0d46') {
            return false;
        }
        if (c <= '\u0d48') {
            return true;
        }
        if (c < '\u0d4a') {
            return false;
        }
        if (c <= '\u0d4d') {
            return true;
        }
        if (c == '\u0d57') {
            return true;
        }
        if (c == '\u0e31') {
            return true;
        }
        if (c < '\u0e34') {
            return false;
        }
        if (c <= '\u0e3a') {
            return true;
        }
        if (c < '\u0e47') {
            return false;
        }
        if (c <= '\u0e4e') {
            return true;
        }
        if (c == '\u0eb1') {
            return true;
        }
        if (c < '\u0eb4') {
            return false;
        }
        if (c <= '\u0eb9') {
            return true;
        }
        if (c < '\u0ebb') {
            return false;
        }
        if (c <= '\u0ebc') {
            return true;
        }
        if (c < '\u0ec8') {
            return false;
        }
        if (c <= '\u0ecd') {
            return true;
        }
        if (c < '\u0f18') {
            return false;
        }
        if (c <= '\u0f19') {
            return true;
        }
        if (c == '\u0f35') {
            return true;
        }
        if (c == '\u0f37') {
            return true;
        }
        if (c == '\u0f39') {
            return true;
        }
        if (c == '\u0f3e') {
            return true;
        }
        if (c == '\u0f3f') {
            return true;
        }
        if (c < '\u0f71') {
            return false;
        }
        if (c <= '\u0f84') {
            return true;
        }
        if (c < '\u0f86') {
            return false;
        }
        if (c <= '\u0f8b') {
            return true;
        }
        if (c < '\u0f90') {
            return false;
        }
        if (c <= '\u0f95') {
            return true;
        }
        if (c == '\u0f97') {
            return true;
        }
        if (c < '\u0f99') {
            return false;
        }
        if (c <= '\u0fad') {
            return true;
        }
        if (c < '\u0fb1') {
            return false;
        }
        if (c <= '\u0fb7') {
            return true;
        }
        if (c == '\u0fb9') {
            return true;
        }
        if (c < '\u20d0') {
            return false;
        }
        if (c <= '\u20dc') {
            return true;
        }
        if (c == '\u20e1') {
            return true;
        }
        if (c < '\u302a') {
            return false;
        }
        if (c <= '\u302f') {
            return true;
        }
        if (c == '\u3099') {
            return true;
        }
        if (c == '\u309a') {
            return true;
        }
        return false;
    }

    private static boolean isXMLCombiningCharOld(char c) {
        if (c >= '\u0300' && c <= '\u0345') {
            return true;
        }
        if (c >= '\u0360' && c <= '\u0361') {
            return true;
        }
        if (c >= '\u0483' && c <= '\u0486') {
            return true;
        }
        if (c >= '\u0591' && c <= '\u05a1') {
            return true;
        }
        if (c >= '\u05a3' && c <= '\u05b9') {
            return true;
        }
        if (c >= '\u05bb' && c <= '\u05bd') {
            return true;
        }
        if (c == '\u05bf') {
            return true;
        }
        if (c >= '\u05c1' && c <= '\u05c2') {
            return true;
        }
        if (c == '\u05c4') {
            return true;
        }
        if (c >= '\u064b' && c <= '\u0652') {
            return true;
        }
        if (c == '\u0670') {
            return true;
        }
        if (c >= '\u06d6' && c <= '\u06dc') {
            return true;
        }
        if (c >= '\u06dd' && c <= '\u06df') {
            return true;
        }
        if (c >= '\u06e0' && c <= '\u06e4') {
            return true;
        }
        if (c >= '\u06e7' && c <= '\u06e8') {
            return true;
        }
        if (c >= '\u06ea' && c <= '\u06ed') {
            return true;
        }
        if (c >= '\u0901' && c <= '\u0903') {
            return true;
        }
        if (c == '\u093c') {
            return true;
        }
        if (c >= '\u093e' && c <= '\u094c') {
            return true;
        }
        if (c == '\u094d') {
            return true;
        }
        if (c >= '\u0951' && c <= '\u0954') {
            return true;
        }
        if (c >= '\u0962' && c <= '\u0963') {
            return true;
        }
        if (c >= '\u0981' && c <= '\u0983') {
            return true;
        }
        if (c == '\u09bc') {
            return true;
        }
        if (c == '\u09be') {
            return true;
        }
        if (c == '\u09bf') {
            return true;
        }
        if (c >= '\u09c0' && c <= '\u09c4') {
            return true;
        }
        if (c >= '\u09c7' && c <= '\u09c8') {
            return true;
        }
        if (c >= '\u09cb' && c <= '\u09cd') {
            return true;
        }
        if (c == '\u09d7') {
            return true;
        }
        if (c >= '\u09e2' && c <= '\u09e3') {
            return true;
        }
        if (c == '\u0a02') {
            return true;
        }
        if (c == '\u0a3c') {
            return true;
        }
        if (c == '\u0a3e') {
            return true;
        }
        if (c == '\u0a3f') {
            return true;
        }
        if (c >= '\u0a40' && c <= '\u0a42') {
            return true;
        }
        if (c >= '\u0a47' && c <= '\u0a48') {
            return true;
        }
        if (c >= '\u0a4b' && c <= '\u0a4d') {
            return true;
        }
        if (c >= '\u0a70' && c <= '\u0a71') {
            return true;
        }
        if (c >= '\u0a81' && c <= '\u0a83') {
            return true;
        }
        if (c == '\u0abc') {
            return true;
        }
        if (c >= '\u0abe' && c <= '\u0ac5') {
            return true;
        }
        if (c >= '\u0ac7' && c <= '\u0ac9') {
            return true;
        }
        if (c >= '\u0acb' && c <= '\u0acd') {
            return true;
        }
        if (c >= '\u0b01' && c <= '\u0b03') {
            return true;
        }
        if (c == '\u0b3c') {
            return true;
        }
        if (c >= '\u0b3e' && c <= '\u0b43') {
            return true;
        }
        if (c >= '\u0b47' && c <= '\u0b48') {
            return true;
        }
        if (c >= '\u0b4b' && c <= '\u0b4d') {
            return true;
        }
        if (c >= '\u0b56' && c <= '\u0b57') {
            return true;
        }
        if (c >= '\u0b82' && c <= '\u0b83') {
            return true;
        }
        if (c >= '\u0bbe' && c <= '\u0bc2') {
            return true;
        }
        if (c >= '\u0bc6' && c <= '\u0bc8') {
            return true;
        }
        if (c >= '\u0bca' && c <= '\u0bcd') {
            return true;
        }
        if (c == '\u0bd7') {
            return true;
        }
        if (c >= '\u0c01' && c <= '\u0c03') {
            return true;
        }
        if (c >= '\u0c3e' && c <= '\u0c44') {
            return true;
        }
        if (c >= '\u0c46' && c <= '\u0c48') {
            return true;
        }
        if (c >= '\u0c4a' && c <= '\u0c4d') {
            return true;
        }
        if (c >= '\u0c55' && c <= '\u0c56') {
            return true;
        }
        if (c >= '\u0c82' && c <= '\u0c83') {
            return true;
        }
        if (c >= '\u0cbe' && c <= '\u0cc4') {
            return true;
        }
        if (c >= '\u0cc6' && c <= '\u0cc8') {
            return true;
        }
        if (c >= '\u0cca' && c <= '\u0ccd') {
            return true;
        }
        if (c >= '\u0cd5' && c <= '\u0cd6') {
            return true;
        }
        if (c >= '\u0d02' && c <= '\u0d03') {
            return true;
        }
        if (c >= '\u0d3e' && c <= '\u0d43') {
            return true;
        }
        if (c >= '\u0d46' && c <= '\u0d48') {
            return true;
        }
        if (c >= '\u0d4a' && c <= '\u0d4d') {
            return true;
        }
        if (c == '\u0d57') {
            return true;
        }
        if (c == '\u0e31') {
            return true;
        }
        if (c >= '\u0e34' && c <= '\u0e3a') {
            return true;
        }
        if (c >= '\u0e47' && c <= '\u0e4e') {
            return true;
        }
        if (c == '\u0eb1') {
            return true;
        }
        if (c >= '\u0eb4' && c <= '\u0eb9') {
            return true;
        }
        if (c >= '\u0ebb' && c <= '\u0ebc') {
            return true;
        }
        if (c >= '\u0ec8' && c <= '\u0ecd') {
            return true;
        }
        if (c >= '\u0f18' && c <= '\u0f19') {
            return true;
        }
        if (c == '\u0f35') {
            return true;
        }
        if (c == '\u0f37') {
            return true;
        }
        if (c == '\u0f39') {
            return true;
        }
        if (c == '\u0f3e') {
            return true;
        }
        if (c == '\u0f3f') {
            return true;
        }
        if (c >= '\u0f71' && c <= '\u0f84') {
            return true;
        }
        if (c >= '\u0f86' && c <= '\u0f8b') {
            return true;
        }
        if (c >= '\u0f90' && c <= '\u0f95') {
            return true;
        }
        if (c == '\u0f97') {
            return true;
        }
        if (c >= '\u0f99' && c <= '\u0fad') {
            return true;
        }
        if (c >= '\u0fb1' && c <= '\u0fb7') {
            return true;
        }
        if (c == '\u0fb9') {
            return true;
        }
        if (c >= '\u20d0' && c <= '\u20dc') {
            return true;
        }
        if (c == '\u20e1') {
            return true;
        }
        if (c >= '\u302a' && c <= '\u302f') {
            return true;
        }
        if (c == '\u3099') {
            return true;
        }
        if (c == '\u309a') {
            return true;
        }
        return false;
    }

    public static boolean isXMLDigit(char c) {
        if (c < '0') {
            return false;
        }
        if (c <= '9') {
            return true;
        }
        if (c < '\u0660') {
            return false;
        }
        if (c <= '\u0669') {
            return true;
        }
        if (c < '\u06f0') {
            return false;
        }
        if (c <= '\u06f9') {
            return true;
        }
        if (c < '\u0966') {
            return false;
        }
        if (c <= '\u096f') {
            return true;
        }
        if (c < '\u09e6') {
            return false;
        }
        if (c <= '\u09ef') {
            return true;
        }
        if (c < '\u0a66') {
            return false;
        }
        if (c <= '\u0a6f') {
            return true;
        }
        if (c < '\u0ae6') {
            return false;
        }
        if (c <= '\u0aef') {
            return true;
        }
        if (c < '\u0b66') {
            return false;
        }
        if (c <= '\u0b6f') {
            return true;
        }
        if (c < '\u0be7') {
            return false;
        }
        if (c <= '\u0bef') {
            return true;
        }
        if (c < '\u0c66') {
            return false;
        }
        if (c <= '\u0c6f') {
            return true;
        }
        if (c < '\u0ce6') {
            return false;
        }
        if (c <= '\u0cef') {
            return true;
        }
        if (c < '\u0d66') {
            return false;
        }
        if (c <= '\u0d6f') {
            return true;
        }
        if (c < '\u0e50') {
            return false;
        }
        if (c <= '\u0e59') {
            return true;
        }
        if (c < '\u0ed0') {
            return false;
        }
        if (c <= '\u0ed9') {
            return true;
        }
        if (c < '\u0f20') {
            return false;
        }
        if (c <= '\u0f29') {
            return true;
        }
        return false;
    }

    private static boolean isXMLDigitOld(char c) {
        if (c >= '0' && c <= '9') {
            return true;
        }
        if (c >= '\u0660' && c <= '\u0669') {
            return true;
        }
        if (c >= '\u06f0' && c <= '\u06f9') {
            return true;
        }
        if (c >= '\u0966' && c <= '\u096f') {
            return true;
        }
        if (c >= '\u09e6' && c <= '\u09ef') {
            return true;
        }
        if (c >= '\u0a66' && c <= '\u0a6f') {
            return true;
        }
        if (c >= '\u0ae6' && c <= '\u0aef') {
            return true;
        }
        if (c >= '\u0b66' && c <= '\u0b6f') {
            return true;
        }
        if (c >= '\u0be7' && c <= '\u0bef') {
            return true;
        }
        if (c >= '\u0c66' && c <= '\u0c6f') {
            return true;
        }
        if (c >= '\u0ce6' && c <= '\u0cef') {
            return true;
        }
        if (c >= '\u0d66' && c <= '\u0d6f') {
            return true;
        }
        if (c >= '\u0e50' && c <= '\u0e59') {
            return true;
        }
        if (c >= '\u0ed0' && c <= '\u0ed9') {
            return true;
        }
        if (c >= '\u0f20' && c <= '\u0f29') {
            return true;
        }
        return false;
    }

    public static boolean isXMLExtender(char c) {
        if (c < '\u00b6') {
            return false;
        }
        if (c == '\u00b7') {
            return true;
        }
        if (c == '\u02d0') {
            return true;
        }
        if (c == '\u02d1') {
            return true;
        }
        if (c == '\u0387') {
            return true;
        }
        if (c == '\u0640') {
            return true;
        }
        if (c == '\u0e46') {
            return true;
        }
        if (c == '\u0ec6') {
            return true;
        }
        if (c == '\u3005') {
            return true;
        }
        if (c < '\u3031') {
            return false;
        }
        if (c <= '\u3035') {
            return true;
        }
        if (c < '\u309d') {
            return false;
        }
        if (c <= '\u309e') {
            return true;
        }
        if (c < '\u30fc') {
            return false;
        }
        if (c <= '\u30fe') {
            return true;
        }
        return false;
    }

    private static boolean isXMLExtenderOld(char c) {
        if (c == '\u00b7') {
            return true;
        }
        if (c == '\u02d0') {
            return true;
        }
        if (c == '\u02d1') {
            return true;
        }
        if (c == '\u0387') {
            return true;
        }
        if (c == '\u0640') {
            return true;
        }
        if (c == '\u0e46') {
            return true;
        }
        if (c == '\u0ec6') {
            return true;
        }
        if (c == '\u3005') {
            return true;
        }
        if (c >= '\u3031' && c <= '\u3035') {
            return true;
        }
        if (c >= '\u309d' && c <= '\u309e') {
            return true;
        }
        if (c >= '\u30fc' && c <= '\u30fe') {
            return true;
        }
        return false;
    }

    public static boolean isXMLLetter(char c) {
        if (c < 'A') {
            return false;
        }
        if (c <= 'Z') {
            return true;
        }
        if (c < 'a') {
            return false;
        }
        if (c <= 'z') {
            return true;
        }
        if (c < '\u00c0') {
            return false;
        }
        if (c <= '\u00d6') {
            return true;
        }
        if (c < '\u00d8') {
            return false;
        }
        if (c <= '\u00f6') {
            return true;
        }
        if (c < '\u00f8') {
            return false;
        }
        if (c <= '\u00ff') {
            return true;
        }
        if (c < '\u0100') {
            return false;
        }
        if (c <= '\u0131') {
            return true;
        }
        if (c < '\u0134') {
            return false;
        }
        if (c <= '\u013e') {
            return true;
        }
        if (c < '\u0141') {
            return false;
        }
        if (c <= '\u0148') {
            return true;
        }
        if (c < '\u014a') {
            return false;
        }
        if (c <= '\u017e') {
            return true;
        }
        if (c < '\u0180') {
            return false;
        }
        if (c <= '\u01c3') {
            return true;
        }
        if (c < '\u01cd') {
            return false;
        }
        if (c <= '\u01f0') {
            return true;
        }
        if (c < '\u01f4') {
            return false;
        }
        if (c <= '\u01f5') {
            return true;
        }
        if (c < '\u01fa') {
            return false;
        }
        if (c <= '\u0217') {
            return true;
        }
        if (c < '\u0250') {
            return false;
        }
        if (c <= '\u02a8') {
            return true;
        }
        if (c < '\u02bb') {
            return false;
        }
        if (c <= '\u02c1') {
            return true;
        }
        if (c == '\u0386') {
            return true;
        }
        if (c < '\u0388') {
            return false;
        }
        if (c <= '\u038a') {
            return true;
        }
        if (c == '\u038c') {
            return true;
        }
        if (c < '\u038e') {
            return false;
        }
        if (c <= '\u03a1') {
            return true;
        }
        if (c < '\u03a3') {
            return false;
        }
        if (c <= '\u03ce') {
            return true;
        }
        if (c < '\u03d0') {
            return false;
        }
        if (c <= '\u03d6') {
            return true;
        }
        if (c == '\u03da') {
            return true;
        }
        if (c == '\u03dc') {
            return true;
        }
        if (c == '\u03de') {
            return true;
        }
        if (c == '\u03e0') {
            return true;
        }
        if (c < '\u03e2') {
            return false;
        }
        if (c <= '\u03f3') {
            return true;
        }
        if (c < '\u0401') {
            return false;
        }
        if (c <= '\u040c') {
            return true;
        }
        if (c < '\u040e') {
            return false;
        }
        if (c <= '\u044f') {
            return true;
        }
        if (c < '\u0451') {
            return false;
        }
        if (c <= '\u045c') {
            return true;
        }
        if (c < '\u045e') {
            return false;
        }
        if (c <= '\u0481') {
            return true;
        }
        if (c < '\u0490') {
            return false;
        }
        if (c <= '\u04c4') {
            return true;
        }
        if (c < '\u04c7') {
            return false;
        }
        if (c <= '\u04c8') {
            return true;
        }
        if (c < '\u04cb') {
            return false;
        }
        if (c <= '\u04cc') {
            return true;
        }
        if (c < '\u04d0') {
            return false;
        }
        if (c <= '\u04eb') {
            return true;
        }
        if (c < '\u04ee') {
            return false;
        }
        if (c <= '\u04f5') {
            return true;
        }
        if (c < '\u04f8') {
            return false;
        }
        if (c <= '\u04f9') {
            return true;
        }
        if (c < '\u0531') {
            return false;
        }
        if (c <= '\u0556') {
            return true;
        }
        if (c == '\u0559') {
            return true;
        }
        if (c < '\u0561') {
            return false;
        }
        if (c <= '\u0586') {
            return true;
        }
        if (c < '\u05d0') {
            return false;
        }
        if (c <= '\u05ea') {
            return true;
        }
        if (c < '\u05f0') {
            return false;
        }
        if (c <= '\u05f2') {
            return true;
        }
        if (c < '\u0621') {
            return false;
        }
        if (c <= '\u063a') {
            return true;
        }
        if (c < '\u0641') {
            return false;
        }
        if (c <= '\u064a') {
            return true;
        }
        if (c < '\u0671') {
            return false;
        }
        if (c <= '\u06b7') {
            return true;
        }
        if (c < '\u06ba') {
            return false;
        }
        if (c <= '\u06be') {
            return true;
        }
        if (c < '\u06c0') {
            return false;
        }
        if (c <= '\u06ce') {
            return true;
        }
        if (c < '\u06d0') {
            return false;
        }
        if (c <= '\u06d3') {
            return true;
        }
        if (c == '\u06d5') {
            return true;
        }
        if (c < '\u06e5') {
            return false;
        }
        if (c <= '\u06e6') {
            return true;
        }
        if (c < '\u0905') {
            return false;
        }
        if (c <= '\u0939') {
            return true;
        }
        if (c == '\u093d') {
            return true;
        }
        if (c < '\u0958') {
            return false;
        }
        if (c <= '\u0961') {
            return true;
        }
        if (c < '\u0985') {
            return false;
        }
        if (c <= '\u098c') {
            return true;
        }
        if (c < '\u098f') {
            return false;
        }
        if (c <= '\u0990') {
            return true;
        }
        if (c < '\u0993') {
            return false;
        }
        if (c <= '\u09a8') {
            return true;
        }
        if (c < '\u09aa') {
            return false;
        }
        if (c <= '\u09b0') {
            return true;
        }
        if (c == '\u09b2') {
            return true;
        }
        if (c < '\u09b6') {
            return false;
        }
        if (c <= '\u09b9') {
            return true;
        }
        if (c < '\u09dc') {
            return false;
        }
        if (c <= '\u09dd') {
            return true;
        }
        if (c < '\u09df') {
            return false;
        }
        if (c <= '\u09e1') {
            return true;
        }
        if (c < '\u09f0') {
            return false;
        }
        if (c <= '\u09f1') {
            return true;
        }
        if (c < '\u0a05') {
            return false;
        }
        if (c <= '\u0a0a') {
            return true;
        }
        if (c < '\u0a0f') {
            return false;
        }
        if (c <= '\u0a10') {
            return true;
        }
        if (c < '\u0a13') {
            return false;
        }
        if (c <= '\u0a28') {
            return true;
        }
        if (c < '\u0a2a') {
            return false;
        }
        if (c <= '\u0a30') {
            return true;
        }
        if (c < '\u0a32') {
            return false;
        }
        if (c <= '\u0a33') {
            return true;
        }
        if (c < '\u0a35') {
            return false;
        }
        if (c <= '\u0a36') {
            return true;
        }
        if (c < '\u0a38') {
            return false;
        }
        if (c <= '\u0a39') {
            return true;
        }
        if (c < '\u0a59') {
            return false;
        }
        if (c <= '\u0a5c') {
            return true;
        }
        if (c == '\u0a5e') {
            return true;
        }
        if (c < '\u0a72') {
            return false;
        }
        if (c <= '\u0a74') {
            return true;
        }
        if (c < '\u0a85') {
            return false;
        }
        if (c <= '\u0a8b') {
            return true;
        }
        if (c == '\u0a8d') {
            return true;
        }
        if (c < '\u0a8f') {
            return false;
        }
        if (c <= '\u0a91') {
            return true;
        }
        if (c < '\u0a93') {
            return false;
        }
        if (c <= '\u0aa8') {
            return true;
        }
        if (c < '\u0aaa') {
            return false;
        }
        if (c <= '\u0ab0') {
            return true;
        }
        if (c < '\u0ab2') {
            return false;
        }
        if (c <= '\u0ab3') {
            return true;
        }
        if (c < '\u0ab5') {
            return false;
        }
        if (c <= '\u0ab9') {
            return true;
        }
        if (c == '\u0abd') {
            return true;
        }
        if (c == '\u0ae0') {
            return true;
        }
        if (c < '\u0b05') {
            return false;
        }
        if (c <= '\u0b0c') {
            return true;
        }
        if (c < '\u0b0f') {
            return false;
        }
        if (c <= '\u0b10') {
            return true;
        }
        if (c < '\u0b13') {
            return false;
        }
        if (c <= '\u0b28') {
            return true;
        }
        if (c < '\u0b2a') {
            return false;
        }
        if (c <= '\u0b30') {
            return true;
        }
        if (c < '\u0b32') {
            return false;
        }
        if (c <= '\u0b33') {
            return true;
        }
        if (c < '\u0b36') {
            return false;
        }
        if (c <= '\u0b39') {
            return true;
        }
        if (c == '\u0b3d') {
            return true;
        }
        if (c < '\u0b5c') {
            return false;
        }
        if (c <= '\u0b5d') {
            return true;
        }
        if (c < '\u0b5f') {
            return false;
        }
        if (c <= '\u0b61') {
            return true;
        }
        if (c < '\u0b85') {
            return false;
        }
        if (c <= '\u0b8a') {
            return true;
        }
        if (c < '\u0b8e') {
            return false;
        }
        if (c <= '\u0b90') {
            return true;
        }
        if (c < '\u0b92') {
            return false;
        }
        if (c <= '\u0b95') {
            return true;
        }
        if (c < '\u0b99') {
            return false;
        }
        if (c <= '\u0b9a') {
            return true;
        }
        if (c == '\u0b9c') {
            return true;
        }
        if (c < '\u0b9e') {
            return false;
        }
        if (c <= '\u0b9f') {
            return true;
        }
        if (c < '\u0ba3') {
            return false;
        }
        if (c <= '\u0ba4') {
            return true;
        }
        if (c < '\u0ba8') {
            return false;
        }
        if (c <= '\u0baa') {
            return true;
        }
        if (c < '\u0bae') {
            return false;
        }
        if (c <= '\u0bb5') {
            return true;
        }
        if (c < '\u0bb7') {
            return false;
        }
        if (c <= '\u0bb9') {
            return true;
        }
        if (c < '\u0c05') {
            return false;
        }
        if (c <= '\u0c0c') {
            return true;
        }
        if (c < '\u0c0e') {
            return false;
        }
        if (c <= '\u0c10') {
            return true;
        }
        if (c < '\u0c12') {
            return false;
        }
        if (c <= '\u0c28') {
            return true;
        }
        if (c < '\u0c2a') {
            return false;
        }
        if (c <= '\u0c33') {
            return true;
        }
        if (c < '\u0c35') {
            return false;
        }
        if (c <= '\u0c39') {
            return true;
        }
        if (c < '\u0c60') {
            return false;
        }
        if (c <= '\u0c61') {
            return true;
        }
        if (c < '\u0c85') {
            return false;
        }
        if (c <= '\u0c8c') {
            return true;
        }
        if (c < '\u0c8e') {
            return false;
        }
        if (c <= '\u0c90') {
            return true;
        }
        if (c < '\u0c92') {
            return false;
        }
        if (c <= '\u0ca8') {
            return true;
        }
        if (c < '\u0caa') {
            return false;
        }
        if (c <= '\u0cb3') {
            return true;
        }
        if (c < '\u0cb5') {
            return false;
        }
        if (c <= '\u0cb9') {
            return true;
        }
        if (c == '\u0cde') {
            return true;
        }
        if (c < '\u0ce0') {
            return false;
        }
        if (c <= '\u0ce1') {
            return true;
        }
        if (c < '\u0d05') {
            return false;
        }
        if (c <= '\u0d0c') {
            return true;
        }
        if (c < '\u0d0e') {
            return false;
        }
        if (c <= '\u0d10') {
            return true;
        }
        if (c < '\u0d12') {
            return false;
        }
        if (c <= '\u0d28') {
            return true;
        }
        if (c < '\u0d2a') {
            return false;
        }
        if (c <= '\u0d39') {
            return true;
        }
        if (c < '\u0d60') {
            return false;
        }
        if (c <= '\u0d61') {
            return true;
        }
        if (c < '\u0e01') {
            return false;
        }
        if (c <= '\u0e2e') {
            return true;
        }
        if (c == '\u0e30') {
            return true;
        }
        if (c < '\u0e32') {
            return false;
        }
        if (c <= '\u0e33') {
            return true;
        }
        if (c < '\u0e40') {
            return false;
        }
        if (c <= '\u0e45') {
            return true;
        }
        if (c < '\u0e81') {
            return false;
        }
        if (c <= '\u0e82') {
            return true;
        }
        if (c == '\u0e84') {
            return true;
        }
        if (c < '\u0e87') {
            return false;
        }
        if (c <= '\u0e88') {
            return true;
        }
        if (c == '\u0e8a') {
            return true;
        }
        if (c == '\u0e8d') {
            return true;
        }
        if (c < '\u0e94') {
            return false;
        }
        if (c <= '\u0e97') {
            return true;
        }
        if (c < '\u0e99') {
            return false;
        }
        if (c <= '\u0e9f') {
            return true;
        }
        if (c < '\u0ea1') {
            return false;
        }
        if (c <= '\u0ea3') {
            return true;
        }
        if (c == '\u0ea5') {
            return true;
        }
        if (c == '\u0ea7') {
            return true;
        }
        if (c < '\u0eaa') {
            return false;
        }
        if (c <= '\u0eab') {
            return true;
        }
        if (c < '\u0ead') {
            return false;
        }
        if (c <= '\u0eae') {
            return true;
        }
        if (c == '\u0eb0') {
            return true;
        }
        if (c < '\u0eb2') {
            return false;
        }
        if (c <= '\u0eb3') {
            return true;
        }
        if (c == '\u0ebd') {
            return true;
        }
        if (c < '\u0ec0') {
            return false;
        }
        if (c <= '\u0ec4') {
            return true;
        }
        if (c < '\u0f40') {
            return false;
        }
        if (c <= '\u0f47') {
            return true;
        }
        if (c < '\u0f49') {
            return false;
        }
        if (c <= '\u0f69') {
            return true;
        }
        if (c < '\u10a0') {
            return false;
        }
        if (c <= '\u10c5') {
            return true;
        }
        if (c < '\u10d0') {
            return false;
        }
        if (c <= '\u10f6') {
            return true;
        }
        if (c == '\u1100') {
            return true;
        }
        if (c < '\u1102') {
            return false;
        }
        if (c <= '\u1103') {
            return true;
        }
        if (c < '\u1105') {
            return false;
        }
        if (c <= '\u1107') {
            return true;
        }
        if (c == '\u1109') {
            return true;
        }
        if (c < '\u110b') {
            return false;
        }
        if (c <= '\u110c') {
            return true;
        }
        if (c < '\u110e') {
            return false;
        }
        if (c <= '\u1112') {
            return true;
        }
        if (c == '\u113c') {
            return true;
        }
        if (c == '\u113e') {
            return true;
        }
        if (c == '\u1140') {
            return true;
        }
        if (c == '\u114c') {
            return true;
        }
        if (c == '\u114e') {
            return true;
        }
        if (c == '\u1150') {
            return true;
        }
        if (c < '\u1154') {
            return false;
        }
        if (c <= '\u1155') {
            return true;
        }
        if (c == '\u1159') {
            return true;
        }
        if (c < '\u115f') {
            return false;
        }
        if (c <= '\u1161') {
            return true;
        }
        if (c == '\u1163') {
            return true;
        }
        if (c == '\u1165') {
            return true;
        }
        if (c == '\u1167') {
            return true;
        }
        if (c == '\u1169') {
            return true;
        }
        if (c < '\u116d') {
            return false;
        }
        if (c <= '\u116e') {
            return true;
        }
        if (c < '\u1172') {
            return false;
        }
        if (c <= '\u1173') {
            return true;
        }
        if (c == '\u1175') {
            return true;
        }
        if (c == '\u119e') {
            return true;
        }
        if (c == '\u11a8') {
            return true;
        }
        if (c == '\u11ab') {
            return true;
        }
        if (c < '\u11ae') {
            return false;
        }
        if (c <= '\u11af') {
            return true;
        }
        if (c < '\u11b7') {
            return false;
        }
        if (c <= '\u11b8') {
            return true;
        }
        if (c == '\u11ba') {
            return true;
        }
        if (c < '\u11bc') {
            return false;
        }
        if (c <= '\u11c2') {
            return true;
        }
        if (c == '\u11eb') {
            return true;
        }
        if (c == '\u11f0') {
            return true;
        }
        if (c == '\u11f9') {
            return true;
        }
        if (c < '\u1e00') {
            return false;
        }
        if (c <= '\u1e9b') {
            return true;
        }
        if (c < '\u1ea0') {
            return false;
        }
        if (c <= '\u1ef9') {
            return true;
        }
        if (c < '\u1f00') {
            return false;
        }
        if (c <= '\u1f15') {
            return true;
        }
        if (c < '\u1f18') {
            return false;
        }
        if (c <= '\u1f1d') {
            return true;
        }
        if (c < '\u1f20') {
            return false;
        }
        if (c <= '\u1f45') {
            return true;
        }
        if (c < '\u1f48') {
            return false;
        }
        if (c <= '\u1f4d') {
            return true;
        }
        if (c < '\u1f50') {
            return false;
        }
        if (c <= '\u1f57') {
            return true;
        }
        if (c == '\u1f59') {
            return true;
        }
        if (c == '\u1f5b') {
            return true;
        }
        if (c == '\u1f5d') {
            return true;
        }
        if (c < '\u1f5f') {
            return false;
        }
        if (c <= '\u1f7d') {
            return true;
        }
        if (c < '\u1f80') {
            return false;
        }
        if (c <= '\u1fb4') {
            return true;
        }
        if (c < '\u1fb6') {
            return false;
        }
        if (c <= '\u1fbc') {
            return true;
        }
        if (c == '\u1fbe') {
            return true;
        }
        if (c < '\u1fc2') {
            return false;
        }
        if (c <= '\u1fc4') {
            return true;
        }
        if (c < '\u1fc6') {
            return false;
        }
        if (c <= '\u1fcc') {
            return true;
        }
        if (c < '\u1fd0') {
            return false;
        }
        if (c <= '\u1fd3') {
            return true;
        }
        if (c < '\u1fd6') {
            return false;
        }
        if (c <= '\u1fdb') {
            return true;
        }
        if (c < '\u1fe0') {
            return false;
        }
        if (c <= '\u1fec') {
            return true;
        }
        if (c < '\u1ff2') {
            return false;
        }
        if (c <= '\u1ff4') {
            return true;
        }
        if (c < '\u1ff6') {
            return false;
        }
        if (c <= '\u1ffc') {
            return true;
        }
        if (c == '\u2126') {
            return true;
        }
        if (c < '\u212a') {
            return false;
        }
        if (c <= '\u212b') {
            return true;
        }
        if (c == '\u212e') {
            return true;
        }
        if (c < '\u2180') {
            return false;
        }
        if (c <= '\u2182') {
            return true;
        }
        if (c == '\u3007') {
            return true;
        }
        if (c < '\u3021') {
            return false;
        }
        if (c <= '\u3029') {
            return true;
        }
        if (c < '\u3041') {
            return false;
        }
        if (c <= '\u3094') {
            return true;
        }
        if (c < '\u30a1') {
            return false;
        }
        if (c <= '\u30fa') {
            return true;
        }
        if (c < '\u3105') {
            return false;
        }
        if (c <= '\u312c') {
            return true;
        }
        if (c < '\u4e00') {
            return false;
        }
        if (c <= '\u9fa5') {
            return true;
        }
        if (c < '\uac00') {
            return false;
        }
        if (c <= '\ud7a3') {
            return true;
        }
        return false;
    }

    private static boolean isXMLLetterOld(char c) {
        if (c >= 'A' && c <= 'Z') {
            return true;
        }
        if (c >= 'a' && c <= 'z') {
            return true;
        }
        if (c >= '\u00c0' && c <= '\u00d6') {
            return true;
        }
        if (c >= '\u00d8' && c <= '\u00f6') {
            return true;
        }
        if (c >= '\u00f8' && c <= '\u00ff') {
            return true;
        }
        if (c >= '\u0100' && c <= '\u0131') {
            return true;
        }
        if (c >= '\u0134' && c <= '\u013e') {
            return true;
        }
        if (c >= '\u0141' && c <= '\u0148') {
            return true;
        }
        if (c >= '\u014a' && c <= '\u017e') {
            return true;
        }
        if (c >= '\u0180' && c <= '\u01c3') {
            return true;
        }
        if (c >= '\u01cd' && c <= '\u01f0') {
            return true;
        }
        if (c >= '\u01f4' && c <= '\u01f5') {
            return true;
        }
        if (c >= '\u01fa' && c <= '\u0217') {
            return true;
        }
        if (c >= '\u0250' && c <= '\u02a8') {
            return true;
        }
        if (c >= '\u02bb' && c <= '\u02c1') {
            return true;
        }
        if (c >= '\u0388' && c <= '\u038a') {
            return true;
        }
        if (c == '\u0386') {
            return true;
        }
        if (c == '\u038c') {
            return true;
        }
        if (c >= '\u038e' && c <= '\u03a1') {
            return true;
        }
        if (c >= '\u03a3' && c <= '\u03ce') {
            return true;
        }
        if (c >= '\u03d0' && c <= '\u03d6') {
            return true;
        }
        if (c == '\u03da') {
            return true;
        }
        if (c == '\u03dc') {
            return true;
        }
        if (c == '\u03de') {
            return true;
        }
        if (c == '\u03e0') {
            return true;
        }
        if (c >= '\u03e2' && c <= '\u03f3') {
            return true;
        }
        if (c >= '\u0401' && c <= '\u040c') {
            return true;
        }
        if (c >= '\u040e' && c <= '\u044f') {
            return true;
        }
        if (c >= '\u0451' && c <= '\u045c') {
            return true;
        }
        if (c >= '\u045e' && c <= '\u0481') {
            return true;
        }
        if (c >= '\u0490' && c <= '\u04c4') {
            return true;
        }
        if (c >= '\u04c7' && c <= '\u04c8') {
            return true;
        }
        if (c >= '\u04cb' && c <= '\u04cc') {
            return true;
        }
        if (c >= '\u04d0' && c <= '\u04eb') {
            return true;
        }
        if (c >= '\u04ee' && c <= '\u04f5') {
            return true;
        }
        if (c >= '\u04f8' && c <= '\u04f9') {
            return true;
        }
        if (c >= '\u0531' && c <= '\u0556') {
            return true;
        }
        if (c == '\u0559') {
            return true;
        }
        if (c >= '\u0561' && c <= '\u0586') {
            return true;
        }
        if (c >= '\u05d0' && c <= '\u05ea') {
            return true;
        }
        if (c >= '\u05f0' && c <= '\u05f2') {
            return true;
        }
        if (c >= '\u0621' && c <= '\u063a') {
            return true;
        }
        if (c >= '\u0641' && c <= '\u064a') {
            return true;
        }
        if (c >= '\u0671' && c <= '\u06b7') {
            return true;
        }
        if (c >= '\u06ba' && c <= '\u06be') {
            return true;
        }
        if (c >= '\u06c0' && c <= '\u06ce') {
            return true;
        }
        if (c >= '\u06d0' && c <= '\u06d3') {
            return true;
        }
        if (c == '\u06d5') {
            return true;
        }
        if (c >= '\u06e5' && c <= '\u06e6') {
            return true;
        }
        if (c >= '\u0905' && c <= '\u0939') {
            return true;
        }
        if (c == '\u093d') {
            return true;
        }
        if (c >= '\u0958' && c <= '\u0961') {
            return true;
        }
        if (c >= '\u0985' && c <= '\u098c') {
            return true;
        }
        if (c >= '\u098f' && c <= '\u0990') {
            return true;
        }
        if (c >= '\u0993' && c <= '\u09a8') {
            return true;
        }
        if (c >= '\u09aa' && c <= '\u09b0') {
            return true;
        }
        if (c == '\u09b2') {
            return true;
        }
        if (c >= '\u09b6' && c <= '\u09b9') {
            return true;
        }
        if (c >= '\u09dc' && c <= '\u09dd') {
            return true;
        }
        if (c >= '\u09df' && c <= '\u09e1') {
            return true;
        }
        if (c >= '\u09f0' && c <= '\u09f1') {
            return true;
        }
        if (c >= '\u0a05' && c <= '\u0a0a') {
            return true;
        }
        if (c >= '\u0a0f' && c <= '\u0a10') {
            return true;
        }
        if (c >= '\u0a13' && c <= '\u0a28') {
            return true;
        }
        if (c >= '\u0a2a' && c <= '\u0a30') {
            return true;
        }
        if (c >= '\u0a32' && c <= '\u0a33') {
            return true;
        }
        if (c >= '\u0a35' && c <= '\u0a36') {
            return true;
        }
        if (c >= '\u0a38' && c <= '\u0a39') {
            return true;
        }
        if (c >= '\u0a59' && c <= '\u0a5c') {
            return true;
        }
        if (c == '\u0a5e') {
            return true;
        }
        if (c >= '\u0a72' && c <= '\u0a74') {
            return true;
        }
        if (c >= '\u0a85' && c <= '\u0a8b') {
            return true;
        }
        if (c == '\u0a8d') {
            return true;
        }
        if (c >= '\u0a8f' && c <= '\u0a91') {
            return true;
        }
        if (c >= '\u0a93' && c <= '\u0aa8') {
            return true;
        }
        if (c >= '\u0aaa' && c <= '\u0ab0') {
            return true;
        }
        if (c >= '\u0ab2' && c <= '\u0ab3') {
            return true;
        }
        if (c >= '\u0ab5' && c <= '\u0ab9') {
            return true;
        }
        if (c == '\u0abd') {
            return true;
        }
        if (c == '\u0ae0') {
            return true;
        }
        if (c >= '\u0b05' && c <= '\u0b0c') {
            return true;
        }
        if (c >= '\u0b0f' && c <= '\u0b10') {
            return true;
        }
        if (c >= '\u0b13' && c <= '\u0b28') {
            return true;
        }
        if (c >= '\u0b2a' && c <= '\u0b30') {
            return true;
        }
        if (c >= '\u0b32' && c <= '\u0b33') {
            return true;
        }
        if (c >= '\u0b36' && c <= '\u0b39') {
            return true;
        }
        if (c == '\u0b3d') {
            return true;
        }
        if (c >= '\u0b5c' && c <= '\u0b5d') {
            return true;
        }
        if (c >= '\u0b5f' && c <= '\u0b61') {
            return true;
        }
        if (c >= '\u0b85' && c <= '\u0b8a') {
            return true;
        }
        if (c >= '\u0b8e' && c <= '\u0b90') {
            return true;
        }
        if (c >= '\u0b92' && c <= '\u0b95') {
            return true;
        }
        if (c >= '\u0b99' && c <= '\u0b9a') {
            return true;
        }
        if (c == '\u0b9c') {
            return true;
        }
        if (c >= '\u0b9e' && c <= '\u0b9f') {
            return true;
        }
        if (c >= '\u0ba3' && c <= '\u0ba4') {
            return true;
        }
        if (c >= '\u0ba8' && c <= '\u0baa') {
            return true;
        }
        if (c >= '\u0bae' && c <= '\u0bb5') {
            return true;
        }
        if (c >= '\u0bb7' && c <= '\u0bb9') {
            return true;
        }
        if (c >= '\u0c05' && c <= '\u0c0c') {
            return true;
        }
        if (c >= '\u0c0e' && c <= '\u0c10') {
            return true;
        }
        if (c >= '\u0c12' && c <= '\u0c28') {
            return true;
        }
        if (c >= '\u0c2a' && c <= '\u0c33') {
            return true;
        }
        if (c >= '\u0c35' && c <= '\u0c39') {
            return true;
        }
        if (c >= '\u0c60' && c <= '\u0c61') {
            return true;
        }
        if (c >= '\u0c85' && c <= '\u0c8c') {
            return true;
        }
        if (c >= '\u0c8e' && c <= '\u0c90') {
            return true;
        }
        if (c >= '\u0c92' && c <= '\u0ca8') {
            return true;
        }
        if (c >= '\u0caa' && c <= '\u0cb3') {
            return true;
        }
        if (c >= '\u0cb5' && c <= '\u0cb9') {
            return true;
        }
        if (c == '\u0cde') {
            return true;
        }
        if (c >= '\u0ce0' && c <= '\u0ce1') {
            return true;
        }
        if (c >= '\u0d05' && c <= '\u0d0c') {
            return true;
        }
        if (c >= '\u0d0e' && c <= '\u0d10') {
            return true;
        }
        if (c >= '\u0d12' && c <= '\u0d28') {
            return true;
        }
        if (c >= '\u0d2a' && c <= '\u0d39') {
            return true;
        }
        if (c >= '\u0d60' && c <= '\u0d61') {
            return true;
        }
        if (c >= '\u0e01' && c <= '\u0e2e') {
            return true;
        }
        if (c == '\u0e30') {
            return true;
        }
        if (c >= '\u0e32' && c <= '\u0e33') {
            return true;
        }
        if (c >= '\u0e40' && c <= '\u0e45') {
            return true;
        }
        if (c >= '\u0e81' && c <= '\u0e82') {
            return true;
        }
        if (c == '\u0e84') {
            return true;
        }
        if (c >= '\u0e87' && c <= '\u0e88') {
            return true;
        }
        if (c == '\u0e8a') {
            return true;
        }
        if (c == '\u0e8d') {
            return true;
        }
        if (c >= '\u0e94' && c <= '\u0e97') {
            return true;
        }
        if (c >= '\u0e99' && c <= '\u0e9f') {
            return true;
        }
        if (c >= '\u0ea1' && c <= '\u0ea3') {
            return true;
        }
        if (c == '\u0ea5') {
            return true;
        }
        if (c == '\u0ea7') {
            return true;
        }
        if (c >= '\u0eaa' && c <= '\u0eab') {
            return true;
        }
        if (c >= '\u0ead' && c <= '\u0eae') {
            return true;
        }
        if (c == '\u0eb0') {
            return true;
        }
        if (c >= '\u0eb2' && c <= '\u0eb3') {
            return true;
        }
        if (c == '\u0ebd') {
            return true;
        }
        if (c >= '\u0ec0' && c <= '\u0ec4') {
            return true;
        }
        if (c >= '\u0f40' && c <= '\u0f47') {
            return true;
        }
        if (c >= '\u0f49' && c <= '\u0f69') {
            return true;
        }
        if (c >= '\u10a0' && c <= '\u10c5') {
            return true;
        }
        if (c >= '\u10d0' && c <= '\u10f6') {
            return true;
        }
        if (c == '\u1100') {
            return true;
        }
        if (c >= '\u1102' && c <= '\u1103') {
            return true;
        }
        if (c >= '\u1105' && c <= '\u1107') {
            return true;
        }
        if (c == '\u1109') {
            return true;
        }
        if (c >= '\u110b' && c <= '\u110c') {
            return true;
        }
        if (c >= '\u110e' && c <= '\u1112') {
            return true;
        }
        if (c == '\u113c') {
            return true;
        }
        if (c == '\u113e') {
            return true;
        }
        if (c == '\u1140') {
            return true;
        }
        if (c == '\u114c') {
            return true;
        }
        if (c == '\u114e') {
            return true;
        }
        if (c == '\u1150') {
            return true;
        }
        if (c >= '\u1154' && c <= '\u1155') {
            return true;
        }
        if (c == '\u1159') {
            return true;
        }
        if (c >= '\u115f' && c <= '\u1161') {
            return true;
        }
        if (c == '\u1163') {
            return true;
        }
        if (c == '\u1165') {
            return true;
        }
        if (c == '\u1167') {
            return true;
        }
        if (c == '\u1169') {
            return true;
        }
        if (c >= '\u116d' && c <= '\u116e') {
            return true;
        }
        if (c >= '\u1172' && c <= '\u1173') {
            return true;
        }
        if (c == '\u1175') {
            return true;
        }
        if (c == '\u119e') {
            return true;
        }
        if (c == '\u11a8') {
            return true;
        }
        if (c == '\u11ab') {
            return true;
        }
        if (c >= '\u11ae' && c <= '\u11af') {
            return true;
        }
        if (c >= '\u11b7' && c <= '\u11b8') {
            return true;
        }
        if (c == '\u11ba') {
            return true;
        }
        if (c >= '\u11bc' && c <= '\u11c2') {
            return true;
        }
        if (c == '\u11eb') {
            return true;
        }
        if (c == '\u11f0') {
            return true;
        }
        if (c == '\u11f9') {
            return true;
        }
        if (c >= '\u1e00' && c <= '\u1e9b') {
            return true;
        }
        if (c >= '\u1ea0' && c <= '\u1ef9') {
            return true;
        }
        if (c >= '\u1f00' && c <= '\u1f15') {
            return true;
        }
        if (c >= '\u1f18' && c <= '\u1f1d') {
            return true;
        }
        if (c >= '\u1f20' && c <= '\u1f45') {
            return true;
        }
        if (c >= '\u1f48' && c <= '\u1f4d') {
            return true;
        }
        if (c >= '\u1f50' && c <= '\u1f57') {
            return true;
        }
        if (c == '\u1f59') {
            return true;
        }
        if (c == '\u1f5b') {
            return true;
        }
        if (c == '\u1f5d') {
            return true;
        }
        if (c >= '\u1f5f' && c <= '\u1f7d') {
            return true;
        }
        if (c >= '\u1f80' && c <= '\u1fb4') {
            return true;
        }
        if (c >= '\u1fb6' && c <= '\u1fbc') {
            return true;
        }
        if (c == '\u1fbe') {
            return true;
        }
        if (c >= '\u1fc2' && c <= '\u1fc4') {
            return true;
        }
        if (c >= '\u1fc6' && c <= '\u1fcc') {
            return true;
        }
        if (c >= '\u1fd0' && c <= '\u1fd3') {
            return true;
        }
        if (c >= '\u1fd6' && c <= '\u1fdb') {
            return true;
        }
        if (c >= '\u1fe0' && c <= '\u1fec') {
            return true;
        }
        if (c >= '\u1ff2' && c <= '\u1ff4') {
            return true;
        }
        if (c >= '\u1ff6' && c <= '\u1ffc') {
            return true;
        }
        if (c == '\u2126') {
            return true;
        }
        if (c >= '\u212a' && c <= '\u212b') {
            return true;
        }
        if (c == '\u212e') {
            return true;
        }
        if (c >= '\u2180' && c <= '\u2182') {
            return true;
        }
        if (c >= '\u3041' && c <= '\u3094') {
            return true;
        }
        if (c >= '\u30a1' && c <= '\u30fa') {
            return true;
        }
        if (c >= '\u3105' && c <= '\u312c') {
            return true;
        }
        if (c >= '\uac00' && c <= '\ud7a3') {
            return true;
        }
        if (c >= '\u4e00' && c <= '\u9fa5') {
            return true;
        }
        if (c == '\u3007') {
            return true;
        }
        if (c >= '\u3021' && c <= '\u3029') {
            return true;
        }
        return false;
    }

    public static boolean isXMLLetterOrDigit(char c) {
        return Verifier.isXMLLetter(c) || Verifier.isXMLDigit(c);
    }

    public static boolean isXMLNameCharacter(char c) {
        return Verifier.isXMLLetter(c) || Verifier.isXMLDigit(c) || c == '.' || c == '-' || c == '_' || c == ':' || Verifier.isXMLCombiningChar(c) || Verifier.isXMLExtender(c);
    }

    public static boolean isXMLNameStartCharacter(char c) {
        return Verifier.isXMLLetter(c) || c == '_' || c == ':';
    }

    private static boolean isXMLPublicIDCharacter(char c) {
        if (c >= 'a' && c <= 'z') {
            return true;
        }
        if (c >= '?' && c <= 'Z') {
            return true;
        }
        if (c >= '\'' && c <= ';') {
            return true;
        }
        if (c == ' ') {
            return true;
        }
        if (c == '!') {
            return true;
        }
        if (c == '=') {
            return true;
        }
        if (c == '#') {
            return true;
        }
        if (c == '$') {
            return true;
        }
        if (c == '_') {
            return true;
        }
        if (c == '%') {
            return true;
        }
        if (c == '\n') {
            return true;
        }
        if (c == '\r') {
            return true;
        }
        if (c == '\t') {
            return true;
        }
        return false;
    }

    public static void main(String[] arrstring) {
        for (int i = 0; i < 65536; ++i) {
            if (Verifier.isXMLLetter((char)i) == Verifier.isXMLLetterOld((char)i)) continue;
            System.out.println("isXMLLetter mismatch: " + i + " hex: " + Integer.toHexString(i));
        }
        for (int j = 0; j < 65536; ++j) {
            if (Verifier.isXMLDigit((char)j) == Verifier.isXMLDigitOld((char)j)) continue;
            System.out.println("isXMLDigit mismatch: " + j + " hex: " + Integer.toHexString(j));
        }
        for (int k = 0; k < 65536; ++k) {
            if (Verifier.isXMLCombiningChar((char)k) == Verifier.isXMLCombiningCharOld((char)k)) continue;
            System.out.println("isXMLCombiningChar mismatch: " + k + " hex: " + Integer.toHexString(k));
        }
        for (int i2 = 0; i2 < 65536; ++i2) {
            if (Verifier.isXMLExtender((char)i2) == Verifier.isXMLExtenderOld((char)i2)) continue;
            System.out.println("isXMLExtender mismatch: " + i2 + " hex: " + Integer.toHexString(i2));
        }
        for (int i3 = 0; i3 < 65536; ++i3) {
            if (Verifier.isXMLCharacter((char)i3) == Verifier.isXMLCharacterOld((char)i3)) continue;
            System.out.println("isXMLCharacter mismatch: " + i3 + " hex: " + Integer.toHexString(i3));
        }
    }
}

