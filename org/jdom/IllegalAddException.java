/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import org.jdom.Attribute;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.EntityRef;
import org.jdom.Namespace;
import org.jdom.ProcessingInstruction;
import org.jdom.Text;

public class IllegalAddException
extends IllegalArgumentException {
    private static final String CVS_ID = "@(#) $RCSfile: IllegalAddException.java,v $ $Revision: 1.17 $ $Date: 2002/02/23 11:30:13 $ $Name: jdom_1_0_b8 $";

    public IllegalAddException(String string) {
        super(string);
    }

    public IllegalAddException(Document document, Comment comment, String string) {
        super("The comment \"" + comment.getText() + "\" could not be added to the top level of the document: " + string);
    }

    public IllegalAddException(Document document, DocType docType, String string) {
        super("The DOCTYPE " + docType.toString() + " could not be added to the document: " + string);
    }

    public IllegalAddException(Document document, Element element, String string) {
        super("The element \"" + element.getQualifiedName() + "\" could not be added as the root of the document: " + string);
    }

    public IllegalAddException(Document document, ProcessingInstruction processingInstruction, String string) {
        super("The PI \"" + processingInstruction.getTarget() + "\" could not be added to the top level of the document: " + string);
    }

    public IllegalAddException(Element element, Attribute attribute, String string) {
        super("The attribute \"" + attribute.getQualifiedName() + "\" could not be added to the element \"" + element.getQualifiedName() + "\": " + string);
    }

    public IllegalAddException(Element element, CDATA cDATA, String string) {
        super("The CDATA \"" + cDATA.getText() + "\" could not be added as content to \"" + element.getQualifiedName() + "\": " + string);
    }

    public IllegalAddException(Element element, Comment comment, String string) {
        super("The comment \"" + comment.getText() + "\" could not be added as content to \"" + element.getQualifiedName() + "\": " + string);
    }

    public IllegalAddException(Element element, Element element2, String string) {
        super("The element \"" + element2.getQualifiedName() + "\" could not be added as a child of \"" + element.getQualifiedName() + "\": " + string);
    }

    public IllegalAddException(Element element, EntityRef entityRef, String string) {
        super("The entity reference\"" + entityRef.getName() + "\" could not be added as content to \"" + element.getQualifiedName() + "\": " + string);
    }

    public IllegalAddException(Element element, Namespace namespace, String string) {
        super("The namespace xmlns" + (namespace.getPrefix() == null || namespace.getPrefix().equals("") ? "=" : new StringBuffer(":").append(namespace.getPrefix()).append("=").toString()) + "\"" + namespace.getURI() + "\" could not be added as content to \"" + element.getQualifiedName() + "\": " + string);
    }

    public IllegalAddException(Element element, ProcessingInstruction processingInstruction, String string) {
        super("The PI \"" + processingInstruction.getTarget() + "\" could not be added as content to \"" + element.getQualifiedName() + "\": " + string);
    }

    public IllegalAddException(Element element, Text text, String string) {
        super("The Text \"" + text.getText() + "\" could not be added as content to \"" + element.getQualifiedName() + "\": " + string);
    }
}

