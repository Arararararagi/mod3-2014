/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import org.jdom.Document;
import org.jdom.IllegalDataException;
import org.jdom.IllegalNameException;
import org.jdom.Verifier;
import org.jdom.output.XMLOutputter;

public class DocType
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: DocType.java,v $ $Revision: 1.18 $ $Date: 2002/02/05 08:03:18 $ $Name: jdom_1_0_b8 $";
    protected String elementName;
    protected String publicID;
    protected String systemID;
    protected Document document;
    protected String internalSubset;

    protected DocType() {
    }

    public DocType(String string) {
        this(string, null, null);
    }

    public DocType(String string, String string2) {
        this(string, null, string2);
    }

    public DocType(String string, String string2, String string3) {
        this.setElementName(string);
        this.setPublicID(string2);
        this.setSystemID(string3);
    }

    public Object clone() {
        DocType docType;
        docType = null;
        try {
            docType = (DocType)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        docType.document = null;
        return docType;
    }

    public final boolean equals(Object object) {
        if (object instanceof DocType) {
            DocType docType = (DocType)object;
            return this.stringEquals(docType.elementName, this.elementName) && this.stringEquals(docType.publicID, this.publicID) && this.stringEquals(docType.systemID, this.systemID);
        }
        return false;
    }

    public Document getDocument() {
        return this.document;
    }

    public String getElementName() {
        return this.elementName;
    }

    public String getInternalSubset() {
        return this.internalSubset;
    }

    public String getPublicID() {
        return this.publicID;
    }

    public String getSystemID() {
        return this.systemID;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    protected DocType setDocument(Document document) {
        this.document = document;
        return this;
    }

    public DocType setElementName(String string) {
        String string2 = Verifier.checkXMLName(string);
        if (string2 != null) {
            throw new IllegalNameException(string, "DocType", string2);
        }
        this.elementName = string;
        return this;
    }

    public void setInternalSubset(String string) {
        this.internalSubset = string;
    }

    public DocType setPublicID(String string) {
        String string2 = Verifier.checkPublicID(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "DocType", string2);
        }
        this.publicID = string;
        return this;
    }

    public DocType setSystemID(String string) {
        String string2 = Verifier.checkSystemLiteral(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "DocType", string2);
        }
        this.systemID = string;
        return this;
    }

    private boolean stringEquals(String string, String string2) {
        if (string == null && string2 == null) {
            return true;
        }
        if (string == null && string2 != null) {
            return false;
        }
        return string.equals(string2);
    }

    public String toString() {
        return "[DocType: " + new XMLOutputter().outputString(this) + "]";
    }
}

