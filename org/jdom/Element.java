/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.jdom.Attribute;
import org.jdom.AttributeList;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.ContentList;
import org.jdom.Document;
import org.jdom.EntityRef;
import org.jdom.IllegalAddException;
import org.jdom.IllegalNameException;
import org.jdom.Namespace;
import org.jdom.ProcessingInstruction;
import org.jdom.Text;
import org.jdom.Verifier;
import org.jdom.filter.ElementFilter;
import org.jdom.filter.Filter;

public class Element
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: Element.java,v $ $Revision: 1.115 $ $Date: 2002/03/28 11:08:12 $ $Name: jdom_1_0_b8 $";
    private static final int INITIAL_ARRAY_SIZE = 5;
    protected String name;
    protected transient Namespace namespace;
    protected transient List additionalNamespaces;
    protected Object parent;
    protected AttributeList attributes;
    protected ContentList content;

    protected Element() {
        this.attributes = new AttributeList(this);
        this.content = new ContentList(this);
    }

    public Element(String string) {
        this(string, (Namespace)null);
    }

    public Element(String string, String string2) {
        this(string, Namespace.getNamespace("", string2));
    }

    public Element(String string, String string2, String string3) {
        this(string, Namespace.getNamespace(string2, string3));
    }

    public Element(String string, Namespace namespace) {
        this.attributes = new AttributeList(this);
        this.content = new ContentList(this);
        this.setName(string);
        this.setNamespace(namespace);
    }

    public Element addContent(String string) {
        return this.addContent(new Text(string));
    }

    public Element addContent(CDATA cDATA) {
        this.content.add(cDATA);
        return this;
    }

    public Element addContent(Comment comment) {
        this.content.add(comment);
        return this;
    }

    public Element addContent(Element element) {
        this.content.add(element);
        return this;
    }

    public Element addContent(EntityRef entityRef) {
        this.content.add(entityRef);
        return this;
    }

    public Element addContent(ProcessingInstruction processingInstruction) {
        this.content.add(processingInstruction);
        return this;
    }

    public Element addContent(Text text) {
        this.content.add(text);
        return this;
    }

    public void addNamespaceDeclaration(Namespace namespace) {
        String string = Verifier.checkNamespaceCollision(namespace, this);
        if (string != null) {
            throw new IllegalAddException(this, namespace, string);
        }
        if (this.additionalNamespaces == null) {
            this.additionalNamespaces = new ArrayList(5);
        }
        this.additionalNamespaces.add(namespace);
    }

    public Object clone() {
        Element element;
        Object object;
        int n;
        element = null;
        try {
            element = (Element)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        element.parent = null;
        element.content = new ContentList(element);
        element.attributes = new AttributeList(element);
        if (this.attributes != null) {
            for (n = 0; n < this.attributes.size(); ++n) {
                object = this.attributes.get(n);
                Attribute attribute = (Attribute)((Attribute)object).clone();
                element.attributes.add(attribute);
            }
        }
        if (this.content != null) {
            for (n = 0; n < this.content.size(); ++n) {
                object = this.content.get(n);
                if (object instanceof Element) {
                    Element element2 = (Element)((Element)object).clone();
                    element.content.add(element2);
                    continue;
                }
                if (object instanceof Text) {
                    Text text = (Text)((Text)object).clone();
                    element.content.add(text);
                    continue;
                }
                if (object instanceof Comment) {
                    Comment comment = (Comment)((Comment)object).clone();
                    element.content.add(comment);
                    continue;
                }
                if (object instanceof CDATA) {
                    CDATA cDATA = (CDATA)((CDATA)object).clone();
                    element.content.add(cDATA);
                    continue;
                }
                if (object instanceof ProcessingInstruction) {
                    ProcessingInstruction processingInstruction = (ProcessingInstruction)((ProcessingInstruction)object).clone();
                    element.content.add(processingInstruction);
                    continue;
                }
                if (!(object instanceof EntityRef)) continue;
                EntityRef entityRef = (EntityRef)((EntityRef)object).clone();
                element.content.add(entityRef);
            }
        }
        if (this.additionalNamespaces != null) {
            element.additionalNamespaces = new ArrayList();
            element.additionalNamespaces.addAll(this.additionalNamespaces);
        }
        return element;
    }

    public Element detach() {
        if (this.parent instanceof Element) {
            ((Element)this.parent).removeContent(this);
        } else if (this.parent instanceof Document) {
            ((Document)this.parent).detachRootElement();
        }
        return this;
    }

    public final boolean equals(Object object) {
        return this == object;
    }

    public List getAdditionalNamespaces() {
        if (this.additionalNamespaces == null) {
            return Collections.EMPTY_LIST;
        }
        return Collections.unmodifiableList(this.additionalNamespaces);
    }

    public Attribute getAttribute(String string) {
        return (Attribute)this.attributes.get(string, Namespace.NO_NAMESPACE);
    }

    public Attribute getAttribute(String string, Namespace namespace) {
        return (Attribute)this.attributes.get(string, namespace);
    }

    public String getAttributeValue(String string) {
        Attribute attribute = (Attribute)this.attributes.get(string, Namespace.NO_NAMESPACE);
        return attribute == null ? null : attribute.getValue();
    }

    public String getAttributeValue(String string, String string2) {
        Attribute attribute = (Attribute)this.attributes.get(string, Namespace.NO_NAMESPACE);
        return attribute == null ? string2 : attribute.getValue();
    }

    public String getAttributeValue(String string, Namespace namespace) {
        Attribute attribute = (Attribute)this.attributes.get(string, namespace);
        return attribute == null ? null : attribute.getValue();
    }

    public String getAttributeValue(String string, Namespace namespace, String string2) {
        Attribute attribute = (Attribute)this.attributes.get(string, namespace);
        return attribute == null ? string2 : attribute.getValue();
    }

    public List getAttributes() {
        return this.attributes;
    }

    public Element getChild(String string) {
        return this.getChild(string, Namespace.NO_NAMESPACE);
    }

    public Element getChild(String string, Namespace namespace) {
        List list = this.content.getView(new ElementFilter(string, namespace));
        Iterator iterator = list.iterator();
        if (iterator.hasNext()) {
            return (Element)iterator.next();
        }
        return null;
    }

    public String getChildText(String string) {
        Element element = this.getChild(string);
        if (element == null) {
            return null;
        }
        return element.getText();
    }

    public String getChildText(String string, Namespace namespace) {
        Element element = this.getChild(string, namespace);
        if (element == null) {
            return null;
        }
        return element.getText();
    }

    public String getChildTextNormalize(String string) {
        Element element = this.getChild(string);
        if (element == null) {
            return null;
        }
        return element.getTextNormalize();
    }

    public String getChildTextNormalize(String string, Namespace namespace) {
        Element element = this.getChild(string, namespace);
        if (element == null) {
            return null;
        }
        return element.getTextNormalize();
    }

    public String getChildTextTrim(String string) {
        Element element = this.getChild(string);
        if (element == null) {
            return null;
        }
        return element.getTextTrim();
    }

    public String getChildTextTrim(String string, Namespace namespace) {
        Element element = this.getChild(string, namespace);
        if (element == null) {
            return null;
        }
        return element.getTextTrim();
    }

    public List getChildren() {
        return this.content.getView(new ElementFilter());
    }

    public List getChildren(String string) {
        return this.getChildren(string, Namespace.NO_NAMESPACE);
    }

    public List getChildren(String string, Namespace namespace) {
        return this.content.getView(new ElementFilter(string, namespace));
    }

    public List getContent() {
        return this.content;
    }

    public List getContent(Filter filter) {
        return this.content.getView(filter);
    }

    public Document getDocument() {
        if (this.parent instanceof Document) {
            return (Document)this.parent;
        }
        if (this.parent instanceof Element) {
            return ((Element)this.parent).getDocument();
        }
        return null;
    }

    public String getName() {
        return this.name;
    }

    public Namespace getNamespace() {
        return this.namespace;
    }

    public Namespace getNamespace(String string) {
        if (string == null) {
            return null;
        }
        if (string.equals(this.getNamespacePrefix())) {
            return this.getNamespace();
        }
        if (this.additionalNamespaces != null) {
            for (int i = 0; i < this.additionalNamespaces.size(); ++i) {
                Namespace namespace = (Namespace)this.additionalNamespaces.get(i);
                if (!string.equals(namespace.getPrefix())) continue;
                return namespace;
            }
        }
        if (this.parent instanceof Element) {
            return ((Element)this.parent).getNamespace(string);
        }
        return null;
    }

    public String getNamespacePrefix() {
        return this.namespace.getPrefix();
    }

    public String getNamespaceURI() {
        return this.namespace.getURI();
    }

    public Element getParent() {
        if (this.parent instanceof Element) {
            return (Element)this.parent;
        }
        return null;
    }

    public String getQualifiedName() {
        if (this.namespace.getPrefix().equals("")) {
            return this.getName();
        }
        return this.namespace.getPrefix() + ":" + this.name;
    }

    public String getText() {
        if (this.content.size() == 0) {
            return "";
        }
        if (this.content.size() == 1) {
            Object object = this.content.get(0);
            if (object instanceof Text) {
                return ((Text)object).getText();
            }
            if (object instanceof CDATA) {
                return ((CDATA)object).getText();
            }
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        boolean bl = false;
        for (int i = 0; i < this.content.size(); ++i) {
            Object object = this.content.get(i);
            if (object instanceof Text) {
                stringBuffer.append(((Text)object).getText());
                bl = true;
                continue;
            }
            if (!(object instanceof CDATA)) continue;
            stringBuffer.append(((CDATA)object).getText());
            bl = true;
        }
        if (!bl) {
            return "";
        }
        return stringBuffer.toString();
    }

    public String getTextNormalize() {
        return Text.normalizeString(this.getText());
    }

    public String getTextTrim() {
        return this.getText().trim();
    }

    public boolean hasChildren() {
        for (int i = 0; i < this.content.size(); ++i) {
            if (!(this.content.get(i) instanceof Element)) continue;
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public boolean isAncestor(Element element) {
        Object object = this.parent;
        while (object instanceof Element) {
            if (object == element) {
                return true;
            }
            object = ((Element)object).getParent();
        }
        return false;
    }

    public boolean isRootElement() {
        return this.parent instanceof Document;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.namespace = Namespace.getNamespace((String)objectInputStream.readObject(), (String)objectInputStream.readObject());
    }

    public boolean removeAttribute(String string) {
        return this.attributes.remove(string, Namespace.NO_NAMESPACE);
    }

    public boolean removeAttribute(String string, Namespace namespace) {
        return this.attributes.remove(string, namespace);
    }

    public boolean removeAttribute(Attribute attribute) {
        return this.attributes.remove(attribute);
    }

    public boolean removeChild(String string) {
        return this.removeChild(string, Namespace.NO_NAMESPACE);
    }

    public boolean removeChild(String string, Namespace namespace) {
        List list = this.content.getView(new ElementFilter(string, namespace));
        Iterator iterator = list.iterator();
        if (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
            return true;
        }
        return false;
    }

    public boolean removeChildren() {
        boolean bl = false;
        List list = this.content.getView(new ElementFilter());
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
            bl = true;
        }
        return bl;
    }

    public boolean removeChildren(String string) {
        return this.removeChildren(string, Namespace.NO_NAMESPACE);
    }

    public boolean removeChildren(String string, Namespace namespace) {
        boolean bl = false;
        List list = this.content.getView(new ElementFilter(string, namespace));
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
            bl = true;
        }
        return bl;
    }

    public boolean removeContent(CDATA cDATA) {
        return this.content.remove(cDATA);
    }

    public boolean removeContent(Comment comment) {
        return this.content.remove(comment);
    }

    public boolean removeContent(Element element) {
        return this.content.remove(element);
    }

    public boolean removeContent(EntityRef entityRef) {
        return this.content.remove(entityRef);
    }

    public boolean removeContent(ProcessingInstruction processingInstruction) {
        return this.content.remove(processingInstruction);
    }

    public boolean removeContent(Text text) {
        return this.content.remove(text);
    }

    public void removeNamespaceDeclaration(Namespace namespace) {
        if (this.additionalNamespaces == null) {
            return;
        }
        this.additionalNamespaces.remove(namespace);
    }

    private void removeRange(List list, int n, int n2) {
        ListIterator listIterator = list.listIterator(n);
        for (int i = 0; i < n2 - n; ++i) {
            listIterator.next();
            listIterator.remove();
        }
    }

    public Element setAttribute(String string, String string2) {
        return this.setAttribute(new Attribute(string, string2));
    }

    public Element setAttribute(String string, String string2, Namespace namespace) {
        return this.setAttribute(new Attribute(string, string2, namespace));
    }

    public Element setAttribute(Attribute attribute) {
        this.attributes.add(attribute);
        return this;
    }

    public Element setAttributes(List list) {
        this.attributes.clearAndSet(list);
        return this;
    }

    public Element setChildren(List list) {
        List list2 = this.content.getView(new ElementFilter());
        int n = list2.size();
        try {
            list2.addAll(list);
        }
        catch (RuntimeException var4_4) {
            this.removeRange(list2, n, list2.size());
            throw var4_4;
        }
        this.removeRange(list2, 0, n);
        return this;
    }

    public Element setContent(List list) {
        this.content.clearAndSet(list);
        return this;
    }

    protected Element setDocument(Document document) {
        this.parent = document;
        return this;
    }

    public Element setName(String string) {
        String string2 = Verifier.checkElementName(string);
        if (string2 != null) {
            throw new IllegalNameException(string, "element", string2);
        }
        this.name = string;
        return this;
    }

    public Element setNamespace(Namespace namespace) {
        if (namespace == null) {
            namespace = Namespace.NO_NAMESPACE;
        }
        this.namespace = namespace;
        return this;
    }

    protected Element setParent(Element element) {
        this.parent = element;
        return this;
    }

    public Element setText(String string) {
        this.content.clear();
        if (string != null) {
            this.addContent(new Text(string));
        }
        return this;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer(64).append("[Element: <").append(this.getQualifiedName());
        String string = this.getNamespaceURI();
        if (!string.equals("")) {
            stringBuffer.append(" [Namespace: ").append(string).append("]");
        }
        stringBuffer.append("/>]");
        return stringBuffer.toString();
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.namespace.getPrefix());
        objectOutputStream.writeObject(this.namespace.getURI());
    }
}

