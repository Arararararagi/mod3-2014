/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

public class IllegalDataException
extends IllegalArgumentException {
    private static final String CVS_ID = "@(#) $RCSfile: IllegalDataException.java,v $ $Revision: 1.7 $ $Date: 2002/01/08 09:17:10 $ $Name: jdom_1_0_b8 $";

    public IllegalDataException(String string, String string2) {
        super("The data \"" + string + "\" is not legal for a JDOM " + string2 + ".");
    }

    public IllegalDataException(String string, String string2, String string3) {
        super("The data \"" + string + "\" is not legal for a JDOM " + string2 + ": " + string3 + ".");
    }
}

