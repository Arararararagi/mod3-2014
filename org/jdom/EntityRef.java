/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalDataException;
import org.jdom.IllegalNameException;
import org.jdom.Verifier;

public class EntityRef
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: EntityRef.java,v $ $Revision: 1.8 $ $Date: 2002/03/12 07:11:39 $ $Name: jdom_1_0_b8 $";
    protected String name;
    protected String publicID;
    protected String systemID;
    protected Object parent;

    protected EntityRef() {
    }

    public EntityRef(String string) {
        this(string, null, null);
    }

    public EntityRef(String string, String string2) {
        this(string, null, string2);
    }

    public EntityRef(String string, String string2, String string3) {
        this.setName(string);
        this.setPublicID(string2);
        this.setSystemID(string3);
    }

    public Object clone() {
        EntityRef entityRef;
        entityRef = null;
        try {
            entityRef = (EntityRef)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        entityRef.parent = null;
        return entityRef;
    }

    public EntityRef detach() {
        Element element = this.getParent();
        if (element != null) {
            element.removeContent(this);
        }
        return this;
    }

    public final boolean equals(Object object) {
        return object == this;
    }

    public Document getDocument() {
        if (this.parent != null) {
            return ((Element)this.parent).getDocument();
        }
        return null;
    }

    public String getName() {
        return this.name;
    }

    public Element getParent() {
        return (Element)this.parent;
    }

    public String getPublicID() {
        return this.publicID;
    }

    public String getSystemID() {
        return this.systemID;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public EntityRef setName(String string) {
        String string2 = Verifier.checkXMLName(string);
        if (string2 != null) {
            throw new IllegalNameException(string, "EntityRef", string2);
        }
        this.name = string;
        return this;
    }

    protected EntityRef setParent(Element element) {
        this.parent = element;
        return this;
    }

    public EntityRef setPublicID(String string) {
        String string2 = Verifier.checkPublicID(this.publicID);
        if (string2 != null) {
            throw new IllegalDataException(this.publicID, "EntityRef", string2);
        }
        this.publicID = string;
        return this;
    }

    public EntityRef setSystemID(String string) {
        String string2 = Verifier.checkSystemLiteral(this.systemID);
        if (string2 != null) {
            throw new IllegalDataException(this.systemID, "EntityRef", string2);
        }
        this.systemID = string;
        return this;
    }

    public String toString() {
        return "[EntityRef: " + "&" + this.name + ";" + "]";
    }
}

