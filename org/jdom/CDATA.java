/*
 * Decompiled with CFR 0_102.
 */
package org.jdom;

import java.io.Serializable;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalDataException;
import org.jdom.Text;
import org.jdom.Verifier;

public class CDATA
implements Serializable,
Cloneable {
    private static final String CVS_ID = "@(#) $RCSfile: CDATA.java,v $ $Revision: 1.20 $ $Date: 2002/03/12 07:11:39 $ $Name: jdom_1_0_b8 $";
    private static final String EMPTY_STRING = "";
    protected String value;
    protected Object parent;

    protected CDATA() {
    }

    public CDATA(String string) {
        this.setText(string);
    }

    public void append(String string) {
        if (string == null) {
            return;
        }
        String string2 = Verifier.checkCDATASection(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "CDATA section", string2);
        }
        this.value = this.value == "" ? string : String.valueOf(this.value) + string;
    }

    public void append(CDATA cDATA) {
        if (cDATA == null) {
            return;
        }
        this.value = String.valueOf(this.value) + cDATA.getText();
    }

    public Object clone() {
        CDATA cDATA;
        cDATA = null;
        try {
            cDATA = (CDATA)super.clone();
        }
        catch (CloneNotSupportedException v0) {}
        cDATA.parent = null;
        cDATA.value = this.value;
        return cDATA;
    }

    public CDATA detach() {
        if (this.parent != null) {
            ((Element)this.parent).removeContent(this);
        }
        this.parent = null;
        return this;
    }

    public final boolean equals(Object object) {
        return this == object;
    }

    public Document getDocument() {
        if (this.parent != null) {
            return ((Element)this.parent).getDocument();
        }
        return null;
    }

    public Element getParent() {
        return (Element)this.parent;
    }

    public String getText() {
        return this.value;
    }

    public String getTextNormalize() {
        return Text.normalizeString(this.getText());
    }

    public String getTextTrim() {
        return this.getText().trim();
    }

    public final int hashCode() {
        return super.hashCode();
    }

    protected CDATA setParent(Element element) {
        this.parent = element;
        return this;
    }

    public CDATA setText(String string) {
        if (string == null) {
            this.value = "";
            return this;
        }
        String string2 = Verifier.checkCDATASection(string);
        if (string2 != null) {
            throw new IllegalDataException(string, "CDATA section", string2);
        }
        this.value = string;
        return this;
    }

    public String toString() {
        return new StringBuffer(64).append("[CDATA: ").append(this.getText()).append("]").toString();
    }
}

