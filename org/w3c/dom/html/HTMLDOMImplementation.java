/*
 * Decompiled with CFR 0_102.
 */
package org.w3c.dom.html;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.html.HTMLDocument;

public interface HTMLDOMImplementation
extends DOMImplementation {
    public HTMLDocument createHTMLDocument(String var1);
}

