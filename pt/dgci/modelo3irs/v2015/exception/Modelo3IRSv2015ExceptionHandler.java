/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.exception;

import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.taxclient.ui.exception.EventExceptionsHandler;

public class Modelo3IRSv2015ExceptionHandler
extends EventExceptionsHandler {
    @Override
    protected String getAppTitle() {
        return Modelo3IRSv2015Parameters.instance().getAppName();
    }

    @Override
    protected String getAppVersion() {
        return Modelo3IRSv2015Parameters.instance().getAppVersion();
    }

    @Override
    protected String getAppInternalVersion() {
        return Modelo3IRSv2015Parameters.instance().getAppInternalVersion();
    }
}

