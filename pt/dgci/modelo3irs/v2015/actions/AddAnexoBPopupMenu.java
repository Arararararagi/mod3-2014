/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import pt.dgci.modelo3irs.v2015.actions.AddAnexoHerancaIndivisaPopupMenu;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class AddAnexoBPopupMenu
extends AddAnexoHerancaIndivisaPopupMenu {
    private static final long serialVersionUID = -2415792547865038761L;

    public AddAnexoBPopupMenu(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model, question);
    }

    @Override
    protected void postAddAnexo(DeclaracaoModel model, String subID, boolean isHerancaIndivisa) {
        AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(new FormKey(AnexoBModel.class, subID));
        if (anexoBModel != null) {
            anexoBModel.getQuadro03().setAnexoBq03B1(isHerancaIndivisa ? "1" : "2");
        }
    }
}

