/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoActionBean;
import pt.opensoft.taxclient.actions.structural.add.multi.impl.popup.AddAnexoPopupMenu;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public class AddAnexoSSPopupMenu
extends AddAnexoPopupMenu {
    private static final long serialVersionUID = -4846887608919426649L;
    private String question;

    public AddAnexoSSPopupMenu(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model, question);
        this.question = question;
    }

    @Override
    public boolean validateSubId(DeclaracaoModel model, String subID) {
        if (StringUtil.isEmpty(subID)) {
            DialogFactory.instance().showErrorDialog("Por favor indique o n\u00famero fiscal.");
            return false;
        }
        if (!NifValidator.validate(subID)) {
            DialogFactory.instance().showErrorDialog("N\u00famero fiscal inv\u00e1lido.");
            return false;
        }
        if (!Modelo3IRSValidatorUtil.startsWith(subID, "1", "2")) {
            DialogFactory.instance().showErrorDialog("O n\u00famero fiscal deve come\u00e7ar por 1 ou 2.");
            return false;
        }
        return true;
    }

    @Override
    public AddMultipleAnexoActionBean obtainSubId(DeclaracaoModel model) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(this.question));
        JNIFTextField field = new JNIFTextField();
        field.setColumns(10);
        panel.add(field);
        AddAnexoSSPopupMenu.requestDeferredFocus(field);
        AddMultipleAnexoActionBean answer = JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), panel, "Novo Anexo", -1, 3, null, new Object[]{"OK", "Cancelar"}, "OK") != 0 ? new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.CANCELLED) : new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.OK);
        return answer;
    }
}

