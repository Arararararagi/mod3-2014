/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.admin;

import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import pt.dgci.modelo3irs.v2015.actions.SimulateAction;
import pt.dgci.modelo3irs.v2015.simulador.util.SimulateUtil;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.StringUtil;

public class SimulateDebugAction
extends SimulateAction {
    private static final long serialVersionUID = -358394186009880616L;

    public SimulateDebugAction() {
        super("Simular (debug)", "", "", null, true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (this.valuesSimul != null) {
            JDialog resultDialog = new JDialog(Session.getApplicationFrame());
            JTextArea textArea = new JTextArea();
            JScrollPane scrollPane = new JScrollPane(textArea);
            JSearchTextField searchTextField = new JSearchTextField(textArea);
            resultDialog.setTitle("Simular (debug)");
            textArea.setFont(new Font("Courier New", 0, 12));
            textArea.setEditable(false);
            resultDialog.add((Component)searchTextField, "North");
            resultDialog.add((Component)scrollPane, "Center");
            resultDialog.setSize(450, 700);
            resultDialog.setVisible(true);
            StringBuffer simulatorHTStrBuff = new StringBuffer();
            Hashtable<String, Object> simulatorHT = this.simulateUtil.getSimulatorHashtable();
            TreeMap<String, Object> simulatorSHT = new TreeMap<String, Object>(simulatorHT);
            Iterator<String> keyIter = simulatorSHT.keySet().iterator();
            while (keyIter.hasNext()) {
                String key = keyIter.next();
                Object value = simulatorSHT.get(key);
                simulatorHTStrBuff.append(StringUtil.padSpaces(key, 25)).append(StringUtil.padSpaces(String.valueOf(value), 15, true));
                simulatorHTStrBuff.append("    (").append(value.getClass().getSimpleName()).append(")");
                if (!keyIter.hasNext()) continue;
                simulatorHTStrBuff.append("\n");
            }
            textArea.setText(simulatorHTStrBuff.toString());
        }
    }

    @Override
    public String getText() {
        return "Simular";
    }
}

