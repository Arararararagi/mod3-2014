/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.admin;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import pt.dgci.modelo3irs.v2015.flatfile.WriteDeclarationToFlatFile;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.persistence.DeclarationWriter;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.WebServicesUtil;
import pt.opensoft.text.Base64Encoder;
import pt.opensoft.util.ZipUtil;

public class WriteDeclarationAction
extends EnhancedAction {
    private static final long serialVersionUID = -358394186009880616L;
    public static final String URL_ENCODING_STRING = "UTF-8";
    public static final String CLEAN = "CLEAN";
    public static final String FLAT_FILE = "FLAT FILE";
    public static final String APPLET_ENCODING = "APPLET ENCODING";
    public static final String BD_ENCODING = "BD ENCODING";

    public WriteDeclarationAction() {
        super("Escrever declara\u00e7\u00e3o...", (Icon)null, (Icon)null, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            JDialog resultDialog = new JDialog(Session.getApplicationFrame());
            final JTextArea textArea = new JTextArea();
            textArea.setLineWrap(true);
            textArea.setFont(new Font("Courier New", 0, 12));
            JScrollPane scrollPane = new JScrollPane(textArea);
            final JTextArea extraInfoArea = new JTextArea();
            textArea.setLineWrap(true);
            JScrollPane extraInfoScrollPane = new JScrollPane(extraInfoArea);
            final JComboBox<String> tiposEncoding = new JComboBox<String>();
            tiposEncoding.addItem("--- Seleccionar Formato ---");
            tiposEncoding.addItem("CLEAN");
            tiposEncoding.addItem("FLAT FILE");
            tiposEncoding.addItem("APPLET ENCODING");
            tiposEncoding.addItem("BD ENCODING");
            tiposEncoding.addItemListener(new ItemListener(){

                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == 2) {
                        return;
                    }
                    try {
                        DeclarationWriter declWriter = new DeclarationWriter();
                        StringWriter declXml = new StringWriter();
                        declWriter.write(declXml, Session.getCurrentDeclaracao().getDeclaracaoModel(), null, null, false, false);
                        if (e.getItem().equals("CLEAN")) {
                            textArea.setText(declXml.toString());
                            extraInfoArea.setText(null);
                        } else if (e.getItem().equals("FLAT FILE")) {
                            WriteDeclarationToFlatFile declarationWrite = new WriteDeclarationToFlatFile(Session.getCurrentDeclaracao().getDeclaracaoModel(), "", "", "");
                            if (!declarationWrite.process()) {
                                StringBuffer flatFileValidationErrorMsg = new StringBuffer();
                                flatFileValidationErrorMsg.append("<html>");
                                List<ValidationMessage> msgs = declarationWrite.getValidationResult().getErrors();
                                flatFileValidationErrorMsg.append("<b>Ocorreram erros de valida\u00e7\u00e3o</b><br>");
                                for (ValidationMessage msg : msgs) {
                                    for (String link : ((DeclValidationMessage)msg).get_links()) {
                                        flatFileValidationErrorMsg.append("<br>- " + link);
                                    }
                                }
                                flatFileValidationErrorMsg.append("</html>");
                                DialogFactory.instance().showErrorDialog(flatFileValidationErrorMsg.toString());
                                tiposEncoding.setSelectedIndex(0);
                                return;
                            }
                            textArea.setText(declarationWrite.getFlatFile());
                            extraInfoArea.setText(null);
                        } else if (e.getItem().equals("APPLET ENCODING")) {
                            textArea.setText(WebServicesUtil.getZipEncodedData(declXml.toString()));
                            extraInfoArea.setText(null);
                        } else if (e.getItem().equals("BD ENCODING")) {
                            String decl_encoded = new String(Base64Encoder.encodeBuffer(ZipUtil.deflate(declXml.toString().getBytes("UTF-8"))));
                            String res = "Tamanho:" + declXml.toString().length();
                            res = res + "  /  CRC:" + WriteDeclarationAction.this.getCRC32(declXml.toString().getBytes());
                            textArea.setText(decl_encoded);
                            extraInfoArea.setText(res);
                        } else {
                            textArea.setText(null);
                            extraInfoArea.setText(null);
                        }
                    }
                    catch (Exception e1) {
                        throw new RuntimeException(e1);
                    }
                }
            });
            tiposEncoding.setSelectedIndex(0);
            final JSearchTextField searchTextField = new JSearchTextField(textArea);
            searchTextField.setColumns(20);
            JPanel tiposEncodingPanel = new JPanel(new GridLayout(1, 2));
            tiposEncodingPanel.add(tiposEncoding, "West");
            tiposEncodingPanel.add((Component)searchTextField, "East");
            resultDialog.add((Component)tiposEncodingPanel, "North");
            resultDialog.add((Component)scrollPane, "Center");
            resultDialog.add((Component)extraInfoScrollPane, "South");
            textArea.setEditable(false);
            extraInfoArea.setEditable(false);
            extraInfoArea.setBackground(Color.LIGHT_GRAY);
            extraInfoScrollPane.setSize(600, 60);
            resultDialog.setTitle("Escrever declara\u00e7\u00e3o...");
            resultDialog.setSize(600, 400);
            resultDialog.setVisible(true);
            searchTextField.getRootPane().registerKeyboardAction(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    searchTextField.setText(null);
                }
            }, KeyStroke.getKeyStroke(27, 0), 2);
            resultDialog.getRootPane().registerKeyboardAction(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    searchTextField.requestFocus();
                }
            }, KeyStroke.getKeyStroke(70, 128), 2);
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
    }

    private long getCRC32(byte[] data) {
        CRC32 crc32 = new CRC32();
        crc32.update(data);
        return crc32.getValue();
    }

}

