/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.admin;

import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringReader;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.WebServicesUtil;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.ZipUtil;
import sun.misc.BASE64Decoder;

public class ReadDeclarationAction
extends EnhancedAction {
    private static final long serialVersionUID = -358394186009880616L;
    public static final String URL_ENCODING_STRING = "UTF-8";

    public ReadDeclarationAction() {
        super("Ler declara\u00e7\u00e3o...", (Icon)null, (Icon)null, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            JDialog resultDialog = new JDialog(Session.getApplicationFrame());
            final JTextArea textArea = new JTextArea();
            textArea.setLineWrap(true);
            JScrollPane scrollPane = new JScrollPane(textArea);
            JButton readButton = new JButton("Ler");
            JLabel infoLabel = new JLabel("<html>Formatos v\u00e1lidos:<br> CLEAN, APPLET_ENCODING, BD_ENCODING</html>");
            final JSearchTextField searchTextField = new JSearchTextField(textArea);
            searchTextField.setColumns(20);
            JPanel footerPanel = new JPanel(new GridLayout(1, 2));
            footerPanel.setBackground(Color.LIGHT_GRAY);
            footerPanel.add((Component)infoLabel, "West");
            footerPanel.add((Component)readButton, "East");
            resultDialog.add((Component)searchTextField, "North");
            resultDialog.add((Component)scrollPane, "Center");
            resultDialog.add((Component)footerPanel, "South");
            textArea.setEditable(true);
            resultDialog.setTitle("Ler declara\u00e7\u00e3o...");
            resultDialog.setSize(600, 400);
            resultDialog.setVisible(true);
            textArea.requestFocus();
            searchTextField.getRootPane().registerKeyboardAction(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    searchTextField.setText(null);
                }
            }, KeyStroke.getKeyStroke(27, 0), 2);
            resultDialog.getRootPane().registerKeyboardAction(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    searchTextField.requestFocus();
                }
            }, KeyStroke.getKeyStroke(70, 128), 2);
            readButton.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (StringUtil.isEmpty(textArea.getText())) {
                            return;
                        }
                        String declXml = null;
                        String formato = null;
                        try {
                            declXml = WebServicesUtil.getZipDecodedData(textArea.getText());
                            formato = "APPLET ENCODING";
                        }
                        catch (Exception exp1) {
                            try {
                                BASE64Decoder decoder = new BASE64Decoder();
                                declXml = new String(ZipUtil.inflate(decoder.decodeBuffer(textArea.getText())), "UTF-8");
                                formato = "BD ENCODING";
                            }
                            catch (Exception exp2) {
                                declXml = textArea.getText();
                                formato = "CLEAN";
                            }
                        }
                        StringReader reader = new StringReader(declXml);
                        Session.getCurrentDeclaracao().getDeclaracaoModel().resetModel();
                        DeclarationReader.read(reader, Session.getCurrentDeclaracao().getDeclaracaoModel(), true, false);
                        FormKey rootFormKey = new FormKey(RostoModel.class);
                        if (Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(rootFormKey) != null) {
                            Session.getCurrentDeclaracao().setSelectedRootAnexo(rootFormKey);
                        }
                        DialogFactory.instance().showInformationDialog("Declara\u00e7\u00e3o lida com sucesso (" + formato + ")");
                    }
                    catch (Exception exp) {
                        Session.getCurrentDeclaracao().getDeclaracaoModel().resetModel();
                        throw new RuntimeException(exp);
                    }
                }
            });
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
    }

}

