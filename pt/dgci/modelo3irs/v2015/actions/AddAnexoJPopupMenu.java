/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import pt.opensoft.field.NifValidator;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoActionBean;
import pt.opensoft.taxclient.actions.structural.add.multi.impl.popup.AddAnexoPopupMenu;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AddAnexoJPopupMenu
extends AddAnexoPopupMenu {
    private static final long serialVersionUID = -2415792547865038761L;
    private String question;

    public AddAnexoJPopupMenu(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model, question);
        this.question = question;
    }

    @Override
    public boolean validateSubId(DeclaracaoModel model, String subID) {
        if (!NifValidator.validate(subID)) {
            DialogFactory.instance().showErrorDialog("N\u00famero fiscal inv\u00e1lido.");
            return false;
        }
        return true;
    }

    @Override
    public AddMultipleAnexoActionBean obtainSubId(DeclaracaoModel model) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(this.question));
        JNIFTextField field = new JNIFTextField();
        field.setColumns(10);
        panel.add(field);
        AddAnexoJPopupMenu.requestDeferredFocus(field);
        AddMultipleAnexoActionBean answer = JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), panel, "Novo Anexo", -1, 3, null, new Object[]{"OK", "Cancelar"}, "OK") != 0 ? new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.CANCELLED) : new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.OK);
        return answer;
    }
}

