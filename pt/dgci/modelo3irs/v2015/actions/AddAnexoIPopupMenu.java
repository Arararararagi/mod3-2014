/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoActionBean;
import pt.opensoft.taxclient.actions.structural.add.multi.impl.popup.AddAnexoPopupMenu;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public class AddAnexoIPopupMenu
extends AddAnexoPopupMenu {
    private static final long serialVersionUID = -2415792547865038761L;
    private String question;

    public AddAnexoIPopupMenu(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model, question);
        this.question = question;
    }

    @Override
    public boolean validateSubId(DeclaracaoModel model, String subID) {
        if (StringUtil.isEmpty(subID)) {
            DialogFactory.instance().showErrorDialog("Por favor indique o NIPC da Heran\u00e7a.");
            return false;
        }
        if (!Modelo3IRSValidatorUtil.startsWith(subID, "7", "9")) {
            DialogFactory.instance().showErrorDialog("O NIPC da Heran\u00e7a deve come\u00e7ar por 7 ou  9.");
            return false;
        }
        return true;
    }

    @Override
    public AddMultipleAnexoActionBean obtainSubId(DeclaracaoModel model) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(this.question));
        JNIFTextField field = new JNIFTextField();
        field.setColumns(10);
        panel.add(field);
        AddAnexoIPopupMenu.requestDeferredFocus(field);
        AddMultipleAnexoActionBean answer = JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), panel, "Novo Anexo", -1, 3, null, new Object[]{"OK", "Cancelar"}, "OK") != 0 ? new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.CANCELLED) : new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.OK);
        return answer;
    }
}

