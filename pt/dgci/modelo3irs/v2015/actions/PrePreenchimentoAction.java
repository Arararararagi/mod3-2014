/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import org.xml.sax.SAXException;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Application;
import pt.dgci.modelo3irs.v2015.actions.util.PrePreenchimentoSubmitInfo;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.dgci.modelo3irs.v2015.gui.PrePreenchimentoResponse;
import pt.dgci.modelo3irs.v2015.gui.PrePreenchimentoTaxThread;
import pt.dgci.modelo3irs.v2015.gui.PrePreenchimentoTaxThreadHttpConnection;
import pt.dgci.modelo3irs.v2015.gui.dialogs.PrePreenchimentoReceiptDialog;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS1Dialog;
import pt.opensoft.http.ConnectionTimeoutException;
import pt.opensoft.logging.Logger;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class PrePreenchimentoAction
extends EnhancedAction {
    private static final long serialVersionUID = 5193360170879266073L;
    private static final int PROGRESS_REFRESH_TIME = 500;
    private static final int LINE_LENGHT = 120;
    private static final String MESSAGE_BREAK_LINE = "<br>";
    private static final String TAX_NOT_GETTED = "A declara\u00e7\u00e3o <b>n\u00e3o</b> foi obtida.";
    public static final String CONNECTION_TIMEOUT_MESSAGE = "<html><b>Ocorreu um erro na obten\u00e7\u00e3o da Declara\u00e7\u00e3o (Expirou o tempo de Comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>";
    public static final String IO_EXCEPTION_MESSAGE = "<html><b>Ocorreu um erro na obten\u00e7\u00e3o da Declara\u00e7\u00e3o (Problemas de comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>";
    public static final String EXCEPTION_MESSAGE = "<html><b>Ocorreu um erro na obten\u00e7\u00e3o da Declara\u00e7\u00e3o.</b><br>Por favor, tente novamente.</html>";
    public static final String PP_LOAD_EXCEPTION_MESSAGE = "Por motivos de ordem t\u00e9cnica o pr\u00e9-preenchimento n\u00e3o est\u00e1 dispon\u00edvel.";
    private String url;
    private String appletUrl;
    private String fileVersion;
    private Timer timer;
    private final JLabel statusCommunication_L = new JLabel();
    public static final String PREPREENCHIMENTO = "PrePreenchimento";
    private long anoExercicio;
    private PortalTaxClientRevampedSubmitUser submitUser;
    private QuadroAS1Dialog quadroAS1Dialog;

    public PrePreenchimentoAction() {
        super("PrePreenchimento", IconFactory.getIconSmall(GUIParameters.ICON_SUBMETER), IconFactory.getIconBig(GUIParameters.ICON_SUBMETER), "Pr\u00e9-preenchimento da Declara\u00e7\u00e3o", KeyStroke.getKeyStroke(120, 0));
    }

    public PrePreenchimentoAction(String url) {
        this(url, null);
    }

    public PrePreenchimentoAction(String url, String appletUrl) {
        this();
        this.url = url;
        this.appletUrl = appletUrl;
        this.fileVersion = Integer.toString(Session.getCurrentDeclaracao().getDeclaracaoModel().getVersion());
    }

    public String getFileVersion() {
        return this.fileVersion;
    }

    public String getSubmitUrl() {
        return this.url;
    }

    public String getAppletUrl() {
        return this.appletUrl;
    }

    public void setAnoExercicio(long exercicio) {
        this.anoExercicio = exercicio;
    }

    public long getAnoExercicio() {
        return this.anoExercicio;
    }

    public void setSumbitUser(PortalTaxClientRevampedSubmitUser submitUser) {
        this.submitUser = submitUser;
    }

    public PortalTaxClientRevampedSubmitUser getSubmitUser() {
        return this.submitUser;
    }

    public void setDialog(QuadroAS1Dialog quadroAS1Dialog) {
        this.quadroAS1Dialog = quadroAS1Dialog;
        PortalTaxClientRevampedSubmitUser newSubmitUser = new PortalTaxClientRevampedSubmitUser(quadroAS1Dialog.getNifA(), quadroAS1Dialog.getPassA());
        newSubmitUser.setNifB(quadroAS1Dialog.getNifB());
        newSubmitUser.setPasswordB(quadroAS1Dialog.getPassB());
        this.anoExercicio = Long.valueOf(quadroAS1Dialog.getAno());
        this.submitUser = newSubmitUser;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            super.actionPerformed(e);
            this.submitTax(true);
        }
        catch (Exception ex) {
            Logger.getDefault().error("Erro na obten\u00e7\u00e3o dos dados: " + ex.getCause());
            throw new RuntimeException("Ocorreu um erro grave na obten\u00e7\u00e3o dos dados da declara\u00e7\u00e3o. ", ex);
        }
    }

    protected PrePreenchimentoSubmitInfo initSubmitInfo() throws Exception {
        return new PrePreenchimentoSubmitInfo(this.submitUser, this.anoExercicio);
    }

    protected void showResult(PrePreenchimentoTaxThread submitTaxThread) {
        block28 : {
            if (submitTaxThread.isSucessfull()) {
                PrePreenchimentoResponse prePreenchimentoResponse = ((PrePreenchimentoTaxThreadHttpConnection)submitTaxThread).getPrePreenchimentoResponse();
                int errorCode = prePreenchimentoResponse.getErrorCode();
                switch (errorCode) {
                    case 0: {
                        try {
                            Session.getCurrentDeclaracao().getDeclaracaoModel().resetModel();
                            DeclarationReader.read(new StringReader(prePreenchimentoResponse.getModeloXML()), Session.getCurrentDeclaracao().getDeclaracaoModel(), false, false);
                            Quadro03 quadro03Model = ((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).getRosto().getQuadro03();
                            quadro03Model.setQ03C01(null);
                            quadro03Model.setQ03C02(null);
                            FormKey rootFormKey = new FormKey(RostoModel.class);
                            if (Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(rootFormKey) != null) {
                                Session.getCurrentDeclaracao().setSelectedRootAnexo(rootFormKey);
                            }
                            boolean hasAnexoF = false;
                            if (((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).getAnexoF() != null) {
                                hasAnexoF = true;
                            }
                            PrePreenchimentoReceiptDialog receiptDialog = new PrePreenchimentoReceiptDialog(DialogFactory.getDummyFrame(), String.valueOf(this.anoExercicio), this.submitUser.getNifA(), this.submitUser.getNifB(), prePreenchimentoResponse.getStatus(), hasAnexoF);
                            receiptDialog.showDialog();
                            if (hasAnexoF && !receiptDialog.keepAnexoF()) {
                                ((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).removeAnexo(new FormKey(AnexoFModel.class));
                            }
                            this.quadroAS1Dialog.dispose();
                            break block28;
                        }
                        catch (IOException e1) {
                            throw new RuntimeException(e1);
                        }
                        catch (SAXException e2) {
                            throw new RuntimeException(e2);
                        }
                    }
                    case 99: {
                        JPanel panelNOK = PrePreenchimentoAction.constructErrorsPanel("Por motivos de ordem t\u00e9cnica o pr\u00e9-preenchimento n\u00e3o est\u00e1 dispon\u00edvel.", null);
                        JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelNOK, "Falha na Obten\u00e7\u00e3o da Declara\u00e7\u00e3o", 0);
                        try {
                            this.quadroAS1Dialog.dispose();
                            break block28;
                        }
                        catch (Exception e) {
                            SimpleLog.log("User Canceled.");
                            return;
                        }
                    }
                    case 98: {
                        JPanel panelUnAuthorized = PrePreenchimentoAction.constructErrorsPanel("Erro de Autentica\u00e7\u00e3o", prePreenchimentoResponse.getMessage() != null ? ListUtil.toList(new String[]{prePreenchimentoResponse.getMessage()}) : null);
                        JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelUnAuthorized, "Falha na Obten\u00e7\u00e3o da Declara\u00e7\u00e3o", 0);
                        try {
                            this.quadroAS1Dialog.setVisible(true);
                            break block28;
                        }
                        catch (Exception e) {
                            SimpleLog.log("User Canceled.");
                            return;
                        }
                    }
                    case 96: {
                        JPanel panelInvalid = PrePreenchimentoAction.constructErrorsPanel("Os dados submetidos s\u00e3o inv\u00e1lidos.", prePreenchimentoResponse.getMessage() != null ? ListUtil.toList(new String[]{prePreenchimentoResponse.getMessage()}) : null);
                        JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelInvalid, "Falha na Obten\u00e7\u00e3o da Declara\u00e7\u00e3o", 0);
                        try {
                            this.quadroAS1Dialog.setVisible(true);
                            break block28;
                        }
                        catch (Exception e) {
                            SimpleLog.log("User Canceled.");
                            return;
                        }
                    }
                    case 97: {
                        JPanel panelException = PrePreenchimentoAction.constructErrorsPanel("Sistema central indispon\u00edvel. Por favor tente mais tarde.", null);
                        JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelException, "Falha na Obten\u00e7\u00e3o da Declara\u00e7\u00e3o", 0);
                        try {
                            this.quadroAS1Dialog.setVisible(true);
                            break block28;
                        }
                        catch (Exception e) {
                            SimpleLog.log("User Canceled.");
                            return;
                        }
                    }
                }
                JPanel panelDefaultException = PrePreenchimentoAction.constructErrorsPanel("C\u00f3digo de Resposta desconhecido. Por favor tente mais tarde.", null);
                JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelDefaultException, "Falha na Obten\u00e7\u00e3o da Declara\u00e7\u00e3o", 0);
                try {
                    this.quadroAS1Dialog.setVisible(true);
                }
                catch (Exception e) {
                    SimpleLog.log("User Canceled.");
                }
            } else {
                if (submitTaxThread.getException() instanceof MalformedURLException) {
                    throw new IllegalStateException("Url Inv\u00e1lido [" + submitTaxThread.getException().toString() + "]");
                }
                String message = null;
                message = submitTaxThread.getException() instanceof ConnectionTimeoutException ? "<html><b>Ocorreu um erro na obten\u00e7\u00e3o da Declara\u00e7\u00e3o (Expirou o tempo de Comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>" : (submitTaxThread.getException() instanceof IOException ? "<html><b>Ocorreu um erro na obten\u00e7\u00e3o da Declara\u00e7\u00e3o (Problemas de comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>" : "<html><b>Ocorreu um erro na obten\u00e7\u00e3o da Declara\u00e7\u00e3o.</b><br>Por favor, tente novamente.</html>");
                DialogFactory.instance().showErrorDialog(message);
                try {
                    this.quadroAS1Dialog.setVisible(true);
                }
                catch (Exception e) {
                    SimpleLog.log("User Canceled");
                }
            }
        }
    }

    protected void submitTax(boolean checkWarnings) throws Exception {
        String currentUrl = Session.isApplet() && this.getAppletUrl() != null ? this.getAppletUrl() : this.getSubmitUrl();
        PrePreenchimentoSubmitInfo ppsi = this.initSubmitInfo();
        if (Session.isApplet()) {
            ppsi.addOptionalField("isApplet", "true");
        }
        ppsi.setAppVersion(this.getFileVersion());
        ppsi.setCheckWarnings(checkWarnings);
        final JDialog dialog = PrePreenchimentoAction.submitDialog(this.statusCommunication_L);
        dialog.setVisible(true);
        DialogFactory.instance().showWaitCursor();
        final PrePreenchimentoTaxThreadHttpConnection submitTaxThread = new PrePreenchimentoTaxThreadHttpConnection(currentUrl, ppsi);
        submitTaxThread.start();
        this.timer = new Timer(500, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (submitTaxThread.isCompleted()) {
                    if (((Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame()).getApp() != null) {
                        ((Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame()).getApp().setEnabled(true);
                    }
                    DialogFactory.instance().dismissWaitCursor();
                    PrePreenchimentoAction.this.timer.stop();
                    dialog.setVisible(false);
                    PrePreenchimentoAction.this.showResult(submitTaxThread);
                } else {
                    PrePreenchimentoAction.this.statusCommunication_L.setText(submitTaxThread.getStatusDescr());
                }
            }
        });
        this.timer.start();
        if (((Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame()).getApp() != null) {
            ((Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame()).getApp().setEnabled(false);
        }
    }

    public static JPanel constructErrorsPanel(String msgTitle, List errors) {
        JPanel panel = new JPanel(new BorderLayout(5, 15));
        panel.add((Component)PrePreenchimentoAction.htmlJLabel(msgTitle, true, 120), "North");
        if (errors != null) {
            Iterator iter = errors.iterator();
            while (iter.hasNext()) {
                panel.add((Component)PrePreenchimentoAction.htmlJLabel("&nbsp;&nbsp;&nbsp;-&nbsp;" + iter.next().toString(), false, 120), "Center");
            }
        }
        panel.add((Component)PrePreenchimentoAction.htmlJLabel("A declara\u00e7\u00e3o <b>n\u00e3o</b> foi obtida.", false, 120), "South");
        return panel;
    }

    private static JLabel htmlJLabel(String message, boolean bold, int lineLenght) {
        String startTag = "<html>";
        String endTag = "</html>";
        if (bold) {
            startTag = startTag + "<b>";
            endTag = "</b>" + endTag;
        }
        return new JLabel(startTag + StringUtil.breakMessage(message, lineLenght, "<br>") + endTag);
    }

    private static JDialog submitDialog(JLabel descriptiveLabel) {
        JPanel panel = new JPanel(new GridLayout(2, 1, 0, 20));
        panel.add(new JLabel(IconFactory.getIconBig(GUIParameters.ICON_SUBMETER_ANIMACAO)));
        descriptiveLabel.setText("Comunicando com o servidor...");
        descriptiveLabel.setHorizontalAlignment(0);
        panel.add(descriptiveLabel);
        JOptionPane optionPane = new JOptionPane(panel, -1, 0, null, new Object[0], null);
        JDialog dialog = optionPane.createDialog(DialogFactory.getDummyFrame(), "");
        dialog.setTitle("A obter a declara\u00e7\u00e3o ...");
        dialog.setModal(false);
        dialog.setDefaultCloseOperation(0);
        return dialog;
    }

}

