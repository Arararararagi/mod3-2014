/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.file;

import java.awt.event.ActionEvent;
import pt.opensoft.swing.InvalidContextException;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAction;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAsAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Modelo3IRSv2015SaveFileAction
extends TaxClientRevampedSaveFileAction {
    private static final long serialVersionUID = -6783719309626045885L;

    @Override
    public void actionPerformed(ActionEvent e) {
        DeclaracaoModel currentModel = Session.getCurrentDeclaracao().getDeclaracaoModel();
        if (currentModel.getDeclaracaoFilePath() == null) {
            this.saveFileAsAction.actionPerformed(e);
            return;
        }
        try {
            super.actionPerformed(e);
        }
        catch (InvalidContextException ex) {
            return;
        }
    }
}

