/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.file;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Application;
import pt.dgci.modelo3irs.v2015.actions.file.Modelo3IRSv2015NewFileAction;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.util.Modelo3IRSAppletUtil;
import pt.opensoft.msg.ValidateException;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.TaxClientRevampedApplet;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedOpenFileAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.Pair;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class Modelo3IRSv2015OpenFileAction
extends TaxClientRevampedOpenFileAction {
    private static final long serialVersionUID = 2235011219236652291L;

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        Modelo3IRSv2015Model currentDeclaracaoModel = (Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel();
        try {
            Pair componentInfo;
            if (Session.isApplet()) {
                TaxClientRevampedApplet currentApplet = Session.getApplet();
                String nomeSujA = currentApplet.getParameter("nomeSujA");
                String nomeSujB = currentApplet.getParameter("nomeSujB");
                currentDeclaracaoModel.getRosto().getQuadro03().setQ03C01(!StringUtil.isEmpty(nomeSujA) ? nomeSujA : null);
                currentDeclaracaoModel.getRosto().getQuadro03().setQ03C02(!StringUtil.isEmpty(nomeSujB) ? nomeSujB : null);
                this.validateDeclFromAppletParameter();
            } else {
                currentDeclaracaoModel.getRosto().getQuadro03().setQ03C01(null);
                currentDeclaracaoModel.getRosto().getQuadro03().setQ03C02(null);
                List<AnexoSSModel> anexosSS = currentDeclaracaoModel.getAnexoSS();
                if (!(anexosSS == null || anexosSS.isEmpty())) {
                    for (AnexoSSModel anexoSSModel : anexosSS) {
                        if (anexoSSModel.getQuadro03() == null) continue;
                        anexoSSModel.getQuadro03().setAnexoSSq03C05(null);
                    }
                }
            }
            Modelo3IRSv2015ApplicationMainFrame mainframe = (Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame();
            AnexoTabbedPane rostoTabbedPane = mainframe.getDeclarationDisplayer().getAnexoTabbedPane(new FormKey(RostoModel.class));
            if (!(Session.getCurrentDeclaracao().getDeclaracaoModel().getDeclaracaoFilePath() == null || Session.isApplet() || mainframe.getApp() == null)) {
                mainframe.getApp().setTitle(this.getAppTitle() + " - " + Session.getCurrentDeclaracao().getDeclaracaoModel().getDeclaracaoFilePath());
            }
            if ((componentInfo = rostoTabbedPane.getComponentInfoByName(QuadroInicio.class.getSimpleName())) != null) {
                final JScrollPane quadroInicioScrollPane = (JScrollPane)componentInfo.getSecond();
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        quadroInicioScrollPane.getVerticalScrollBar().setValue(quadroInicioScrollPane.getVerticalScrollBar().getMinimum());
                    }
                });
            }
        }
        catch (RuntimeException e2) {
            SimpleLog.log("Erro ao abrir ficheiro.", e2);
            DialogFactory.instance().showErrorDialog(e2.getMessage());
            ((Modelo3IRSv2015NewFileAction)ActionsTaxClientManager.getAction("Novo")).actionPerformed(null);
        }
    }

    private void validateDeclFromAppletParameter() {
        Modelo3IRSv2015Model declFromAppletParameter = new Modelo3IRSv2015Model();
        Modelo3IRSAppletUtil.loadDeclFromAppletParameter(declFromAppletParameter, true);
        RostoModel rostoModelFromAppetParameter = declFromAppletParameter.getRosto();
        RostoModel rostoModelFromSession = ((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).getRosto();
        Long exercicioFromAppletParameter = rostoModelFromAppetParameter.getQuadro02().getQ02C02();
        Long exercicioFromSession = rostoModelFromSession.getQuadro02().getQ02C02();
        if (exercicioFromAppletParameter != null) {
            if (exercicioFromSession == null) {
                throw new ValidateException("O ficheiro est\u00e1 incompleto: N\u00e3o inclui o ano do exerc\u00edcio.");
            }
            if (exercicioFromSession.longValue() != exercicioFromAppletParameter.longValue()) {
                throw new ValidateException("O ano do exerc\u00edcio existente no ficheiro (" + exercicioFromSession + ") n\u00e3o corresponde ao ano do exerc\u00edcio da declara\u00e7\u00e3o (" + exercicioFromAppletParameter + ")");
            }
        }
        Long nifSPAFromAppletParameter = rostoModelFromAppetParameter.getQuadro03().getQ03C03();
        Long nifSPAFromSession = rostoModelFromSession.getQuadro03().getQ03C03();
        if (nifSPAFromAppletParameter != null) {
            if (nifSPAFromSession == null) {
                throw new ValidateException("O ficheiro est\u00e1 incompleto: N\u00e3o inclui o NIF do Sujeito Passivo A.");
            }
            if (nifSPAFromSession.longValue() != nifSPAFromAppletParameter.longValue()) {
                throw new ValidateException("O NIF do Sujeito Passivo A existente no ficheiro (" + nifSPAFromSession + ") n\u00e3o corresponde ao NIF do Sujeito Passivo A da declara\u00e7\u00e3o (" + nifSPAFromAppletParameter + ")");
            }
        }
        Long nifSPBFromAppletParameter = rostoModelFromAppetParameter.getQuadro03().getQ03C04();
        Long nifSPBFromSession = rostoModelFromSession.getQuadro03().getQ03C04();
        if (nifSPBFromAppletParameter != null) {
            if (nifSPBFromSession == null) {
                throw new ValidateException("O ficheiro est\u00e1 incompleto: N\u00e3o inclui o NIF do Sujeito Passivo B.");
            }
            if (nifSPBFromSession.longValue() != nifSPBFromAppletParameter.longValue()) {
                throw new ValidateException("O NIF do Sujeito Passivo B existente no ficheiro (" + nifSPBFromSession + ") n\u00e3o corresponde ao NIF do Sujeito Passivo B da declara\u00e7\u00e3o (" + nifSPBFromAppletParameter + ")");
            }
        } else if (nifSPBFromSession != null) {
            throw new ValidateException("Ficheiro inv\u00e1lido: o NIF do Sujeito Passivo B n\u00e3o pode estar preenchido.");
        }
    }

    private String getAppTitle() {
        return "Modelo 3 - IRS" + (!TaxclientParameters.isProduction() ? " - TESTES" : "");
    }

}

