/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.file;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Application;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS0Dialog;
import pt.dgci.modelo3irs.v2015.util.DeclaracaoModelInitializer;
import pt.dgci.modelo3irs.v2015.util.Modelo3IRSAppletUtil;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedNewFileAction;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.persistence.DeclarationWriter;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.Pair;

public class Modelo3IRSv2015NewFileAction
extends TaxClientRevampedNewFileAction {
    private static final long serialVersionUID = -1977371378985203041L;

    @Override
    public void actionPerformed(ActionEvent e) {
        DeclaracaoModel currentDeclaracao = Session.getCurrentDeclaracao().getDeclaracaoModel();
        if (currentDeclaracao.isDirty()) {
            String messageAsk = currentDeclaracao.getDeclaracaoFilePath() != null ? "Quer gravar as altera\u00e7\u00f5es efectuadas no ficheiro " + currentDeclaracao.getDeclaracaoFilePath() + "?" : "Quer gravar as altera\u00e7\u00f5es efectuadas?";
            int returnCode = DialogFactory.instance().showConfirmationDialog2(messageAsk);
            if (returnCode == 2 || returnCode == -1) {
                return;
            }
            if (returnCode == 0) {
                TaxClientRevampedSaveFileAction saveFileAction = (TaxClientRevampedSaveFileAction)ActionsTaxClientManager.getAction("Gravar");
                saveFileAction.actionPerformed(e);
                if (saveFileAction.wasCanceled()) {
                    return;
                }
            }
        }
        try {
            Thread.sleep(100);
            Session.getMainFrame().getMultiPurposeDisplayer().hideMultiPurposeDisplayer();
            if (Modelo3IRSv2015Parameters.instance().isPrePreenchimentoActive()) {
                Modelo3IRSv2015ApplicationMainFrame mainframe = (Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame();
                new QuadroAS0Dialog(JOptionPane.getFrameForComponent(mainframe)).setVisible(true);
            } else {
                currentDeclaracao.resetModel();
                currentDeclaracao = this.inicializaDeclarationModel();
            }
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public DeclaracaoModel getDeclaracaoInicializada() {
        return this.inicializaDeclarationModel();
    }

    @Override
    protected DeclaracaoModel inicializaDeclarationModel() {
        Modelo3IRSv2015Model model = this.init();
        if (Session.isApplet()) {
            Modelo3IRSAppletUtil.loadDeclFromAppletParameter(model, true);
        }
        try {
            DeclarationWriter dwriter = new DeclarationWriter();
            StringWriter strwriter = new StringWriter();
            dwriter.write(strwriter, model, null, null, false, false);
            DeclarationReader.read(new StringReader(strwriter.toString()), Session.getCurrentDeclaracao().getDeclaracaoModel(), true, false);
            Session.getCurrentDeclaracao().getDeclaracaoModel().resetDirty();
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
        Session.getCurrentDeclaracao().setSelectedRootAnexo(new FormKey(RostoModel.class));
        Session.getCurrentDeclaracao().setSelectedQuadro(this.getDefaultSelectedQuadroName());
        Modelo3IRSv2015ApplicationMainFrame mainframe = (Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame();
        AnexoTabbedPane rostoTabbedPane = mainframe.getDeclarationDisplayer().getAnexoTabbedPane(new FormKey(RostoModel.class));
        Pair componentInfo = rostoTabbedPane.getComponentInfoByName(QuadroInicio.class.getSimpleName());
        if (componentInfo != null) {
            final JScrollPane quadroInicioScrollPane = (JScrollPane)componentInfo.getSecond();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    quadroInicioScrollPane.getVerticalScrollBar().setValue(quadroInicioScrollPane.getVerticalScrollBar().getMinimum());
                }
            });
        }
        if (mainframe != null && !Session.isApplet() && Modelo3IRSv2015Parameters.instance().isPrePreenchimentoActive() && mainframe.getApp() != null) {
            mainframe.getApp().setTitle(this.getAppTitle());
        }
        return model;
    }

    protected Modelo3IRSv2015Model init() {
        return DeclaracaoModelInitializer.init();
    }

    protected String getDefaultSelectedQuadroName() {
        return QuadroInicio.class.getSimpleName();
    }

    private String getAppTitle() {
        return "Modelo 3 - IRS";
    }

}

