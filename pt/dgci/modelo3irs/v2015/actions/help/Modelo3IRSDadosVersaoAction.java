/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.help;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Application;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.dgci.modelo3irs.v2015.ui.help.DadosVersaoPanel;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.taxclient.util.Session;

public class Modelo3IRSDadosVersaoAction
extends EnhancedAction {
    private static final long serialVersionUID = 7444862052770256313L;
    private String applicationName;

    public Modelo3IRSDadosVersaoAction(String applicationName) {
        super("Dados da Vers\u00e3o", null);
        this.applicationName = applicationName;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JDialog dialog = this.createDialog(((Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame()).getApp(), "Dados da Vers\u00e3o", new DadosVersaoPanel(this.getAppVersion(), this.getAppVersionDate(), this.getAppName()));
        dialog.setVisible(true);
    }

    public JDialog createDialog(Component parentComponent, String title, JPanel About) {
        final JDialog dialog = new JDialog((JFrame)parentComponent, title, true);
        Container contentPane = dialog.getContentPane();
        dialog.setResizable(false);
        contentPane.setLayout(new BorderLayout());
        contentPane.add((Component)About, "Center");
        dialog.pack();
        dialog.setLocationRelativeTo(parentComponent);
        dialog.addWindowListener(new WindowAdapter(){
            boolean gotFocus;

            @Override
            public void windowClosing(WindowEvent we) {
            }

            @Override
            public void windowActivated(WindowEvent we) {
                if (!this.gotFocus) {
                    this.gotFocus = true;
                }
            }
        });
        dialog.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent event) {
                if (dialog.isVisible()) {
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            }
        });
        return dialog;
    }

    protected String getAppVersion() {
        return Modelo3IRSv2015Parameters.instance().getAppVersion();
    }

    protected String getAppVersionDate() {
        return Modelo3IRSv2015Parameters.instance().getAppVersionDate();
    }

    protected String getAppName() {
        return this.applicationName;
    }

}

