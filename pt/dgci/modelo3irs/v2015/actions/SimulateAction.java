/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationResult;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.gui.SimuladorPageDisplayer;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.simulador.util.SimulateUtil;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.taxclient.actions.util.ShowGenericPageAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoModelValidator;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.multi.impl.DefaultMultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.StringUtil;

public class SimulateAction
extends ShowGenericPageAction {
    private static final long serialVersionUID = 6996064798331829999L;
    private Modelo3IRSv2015Model model = (Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel();
    private int idadeSPA;
    private int idadeSPB;
    private int idadeSPF;
    private int idadeD1;
    private int idadeD2;
    private int idadeD3;
    private int nDep;
    private int nDepGC;
    protected SimulateUtil simulateUtil;
    protected Map<String, String> valuesSimul;

    public SimulateAction() {
        super("Simular", GUIParameters.ICON_SIMULAR, "Simular", KeyStroke.getKeyStroke(119, 0), true);
    }

    public SimulateAction(String name, String iconName, String tooltip, KeyStroke key, boolean maximize) {
        super(name, iconName, tooltip, key, maximize);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.clear();
        if (!Modelo3IRSv2015Parameters.instance().IsSimulatePossible()) {
            throw new RuntimeException("Simulador n\u00e3o dispon\u00edvel para este imposto.");
        }
        DeclaracaoModelValidator validator = Session.getDeclaracaoValidator();
        ValidationResult validationResult = validator.validate(this.model);
        if (validationResult.hasErrors()) {
            DialogFactory.instance().showWarningDialog("<html><b>A declara&ccedil;&atilde;o apresenta erros de preenchimento.</b><br>Por favor, corrija a declara\u00e7\u00e3o antes de efectuar a simula\u00e7\u00e3o.</html>");
            MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
            ValidationDisplayer validationDisplayer = multiPurposeDisplayer.getValidationDisplayer();
            validationDisplayer.fillErrorsList(validationResult);
            multiPurposeDisplayer.showValidationDisplayer();
            return;
        }
        this.simulateUtil = new SimulateUtil(this.model);
        String msg = this.simulateUtil.isSimulationPossible();
        if (msg != null) {
            DialogFactory.instance().showErrorDialog("<html><b>" + msg + "</b></html>");
            return;
        }
        boolean hasPensoes = this.hasPensoes();
        boolean hasDependentes = this.hasDependentes();
        boolean hasDependentesGC = this.hasDependentesGuardaConjunta();
        if ((hasPensoes || hasDependentes || hasDependentesGC) && !this.getAges(hasPensoes, hasDependentes, hasDependentesGC)) {
            return;
        }
        this.valuesSimul = this.simulateUtil.efectuaSimulacao(this.idadeSPA, this.idadeSPB, this.idadeSPF, this.idadeD1, this.idadeD2, this.idadeD3, this.nDep, this.nDepGC);
        if (this.valuesSimul != null) {
            DefaultMultiPurposeDisplayer multiPurposeDisplayer = (DefaultMultiPurposeDisplayer)Session.getMainFrame().getMultiPurposeDisplayer();
            SimuladorPageDisplayer simuladorDisplayer = (SimuladorPageDisplayer)multiPurposeDisplayer.getGenericCard(this.getText());
            simuladorDisplayer.setMap(this.valuesSimul);
            Session.getMainFrame().getMultiPurposeDisplayer().showMultiPurposeDisplayer();
            try {
                simuladorDisplayer.updateHelp();
            }
            catch (Exception e1) {
                throw new RuntimeException(e1);
            }
            multiPurposeDisplayer.showGenericCard(this.getText());
            if (this.isMaximize()) {
                multiPurposeDisplayer.maximizeMultiPurposeDisplayer();
            }
        }
    }

    private boolean hasPensoes() {
        if (this.model == null) {
            return false;
        }
        AnexoHModel anexoH = this.model.getAnexoH();
        if (anexoH == null) {
            return false;
        }
        for (AnexoHq07T7_Linha linhaQ7 : anexoH.getQuadro07().getAnexoHq07T7()) {
            if (linhaQ7.getCodBeneficio() == null || linhaQ7.getCodBeneficio() != 701 && linhaQ7.getCodBeneficio() != 711) continue;
            return true;
        }
        return false;
    }

    private boolean hasDependentes() {
        if (this.model == null) {
            return false;
        }
        RostoModel rosto = this.model.getRosto();
        if (rosto != null && rosto.getQuadro03() != null && rosto.getQuadro03().getCountDependentes() > 0) {
            return true;
        }
        return false;
    }

    private boolean hasDependentesGuardaConjunta() {
        if (this.model == null) {
            return false;
        }
        RostoModel rosto = this.model.getRosto();
        if (rosto != null && rosto.getQuadro03() != null && rosto.getQuadro03().getCountDependentesGuardaConjunta() > 0) {
            return true;
        }
        return false;
    }

    public boolean getAges(boolean hasPensoes, boolean hasDependentes, boolean hasDependentesGC) {
        boolean valid;
        RostoModel rosto = this.model.getRosto();
        long nifA = rosto.getQuadro03().getQ03C03() != null ? rosto.getQuadro03().getQ03C03() : -1;
        long nifB = rosto.getQuadro03().getQ03C04() != null ? rosto.getQuadro03().getQ03C04() : -1;
        long nifF = rosto.getQuadro07().getQ07C1() != null ? rosto.getQuadro07().getQ07C1() : -1;
        int size = 0;
        if (hasPensoes) {
            if (nifA != -1) {
                ++size;
            }
            if (nifB != -1) {
                ++size;
            }
            if (nifF != -1) {
                ++size;
            }
        }
        if (hasDependentes) {
            size+=rosto.getQuadro03().getCountDependentesNaoDeficientes();
            size+=rosto.getQuadro03().getCountDependentesDeficientes();
        }
        if (hasDependentesGC) {
            size+=rosto.getQuadro03().getCountDependentesGuardaConjunta();
        }
        JPanel panel = new JPanel(new GridLayout(size, 1));
        JPanel panelA = new JPanel();
        JPanel panelB = new JPanel();
        JPanel panelF = new JPanel();
        JLimitedTextField idadeSujA_TF = new JLimitedTextField(3, 3, true);
        JLimitedTextField idadeSujB_TF = new JLimitedTextField(3, 3, true);
        JLimitedTextField idadeSujF_TF = new JLimitedTextField(3, 3, true);
        HashMap<Long, JLimitedTextField> idadesSujD = new HashMap<Long, JLimitedTextField>(rosto.getQuadro03().getCountDependentesNaoDeficientes());
        HashMap<Long, JLimitedTextField> idadesSujDD = new HashMap<Long, JLimitedTextField>(rosto.getQuadro03().getCountDependentesDeficientes());
        HashMap<Long, JLimitedTextField> idadesSujGC = new HashMap<Long, JLimitedTextField>(rosto.getQuadro03().getCountDependentesGuardaConjunta());
        Long anoDeclaracao = rosto.getQuadro02().getQ02C02();
        if (hasPensoes) {
            idadeSujA_TF.addAncestorListener(new AncestorListener(){

                @Override
                public void ancestorAdded(AncestorEvent ae) {
                    ae.getComponent().requestFocus();
                }

                @Override
                public void ancestorRemoved(AncestorEvent ae) {
                }

                @Override
                public void ancestorMoved(AncestorEvent ae) {
                }
            });
            panelA.add(new JLabel("Idade do Sujeito Passivo A (" + nifA + ") em " + anoDeclaracao + "-01-01:                                                                 "));
            panelA.add(idadeSujA_TF);
            panel.add(panelA);
            if (nifB != -1) {
                panelB.add(new JLabel("Idade do Sujeito Passivo B (" + nifB + ") em " + anoDeclaracao + "-01-01:                                                                 "));
                panelB.add(idadeSujB_TF);
                panel.add(panelB);
            }
            if (nifF != -1) {
                panelF.add(new JLabel("Idade do Sujeito Falecido (" + nifF + ") em " + anoDeclaracao + "-01-01:                                                                  "));
                panelF.add(idadeSujF_TF);
                panel.add(panelF);
            }
        }
        if (hasDependentes) {
            boolean focusAdded = false;
            for (Long nif2 : rosto.getQuadro03().getNIFTitularesDependentesNaoDeficientes()) {
                JPanel panelD = new JPanel();
                JLimitedTextField idadeSujD = new JLimitedTextField(3, 3, true);
                if (!hasPensoes && focusAdded) {
                    idadeSujD.addAncestorListener(new AncestorListener(){

                        @Override
                        public void ancestorAdded(AncestorEvent ae) {
                            ae.getComponent().requestFocus();
                        }

                        @Override
                        public void ancestorRemoved(AncestorEvent ae) {
                        }

                        @Override
                        public void ancestorMoved(AncestorEvent ae) {
                        }
                    });
                    focusAdded = true;
                }
                panelD.add(new JLabel("Idade do Dependente n\u00e3o Deficiente (" + nif2 + ") em " + anoDeclaracao + "-12-31:                                               "));
                panelD.add(idadeSujD);
                panel.add(panelD);
                idadesSujD.put(nif2, idadeSujD);
            }
            for (Long nif : rosto.getQuadro03().getNIFTitularesDependentesDeficientes()) {
                JPanel panelDD = new JPanel();
                JLimitedTextField idadeSujDD = new JLimitedTextField(3, 3, true);
                if (!hasPensoes && focusAdded) {
                    idadeSujDD.addAncestorListener(new AncestorListener(){

                        @Override
                        public void ancestorAdded(AncestorEvent ae) {
                            ae.getComponent().requestFocus();
                        }

                        @Override
                        public void ancestorRemoved(AncestorEvent ae) {
                        }

                        @Override
                        public void ancestorMoved(AncestorEvent ae) {
                        }
                    });
                    focusAdded = true;
                }
                panelDD.add(new JLabel("Idade do Dependente Deficiente (" + nif + ") em " + anoDeclaracao + "-12-31:                                                       "));
                panelDD.add(idadeSujDD);
                panel.add(panelDD);
                idadesSujDD.put(nif, idadeSujDD);
            }
        }
        if (hasDependentesGC) {
            for (Rostoq03DT1_Linha linha : rosto.getQuadro03().getRostoq03DT1()) {
                boolean focusAdded = false;
                JPanel panelDGC = new JPanel();
                JLimitedTextField idadeSujGC = new JLimitedTextField(3, 3, true);
                if (!(hasPensoes || hasDependentes || !focusAdded)) {
                    idadeSujGC.addAncestorListener(new AncestorListener(){

                        @Override
                        public void ancestorAdded(AncestorEvent ae) {
                            ae.getComponent().requestFocus();
                        }

                        @Override
                        public void ancestorRemoved(AncestorEvent ae) {
                        }

                        @Override
                        public void ancestorMoved(AncestorEvent ae) {
                        }
                    });
                    focusAdded = true;
                }
                panelDGC.add(new JLabel("Idade do Dependente Guarda Conjunta (" + linha.getNIF() + ") em " + anoDeclaracao + "-12-31:                                          "));
                panelDGC.add(idadeSujGC);
                panel.add(panelDGC);
                idadesSujGC.put(linha.getNIF(), idadeSujGC);
            }
        }
        do {
            valid = true;
            if (JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), panel, "Informa\u00e7\u00f5es adicionais", -1, 3, null, new Object[]{"OK", "Cancelar"}, "OK") != 0) {
                return false;
            }
            if (hasPensoes) {
                String idadeSujA = idadeSujA_TF.getText();
                if (!(idadeSujA != null && StringUtil.isNumeric(idadeSujA) && idadeSujA.indexOf(45) == -1)) {
                    DialogFactory.instance().showErrorDialog("Idade do sujeito passivo A inv\u00e1lida.");
                    valid = false;
                } else {
                    this.idadeSPA = Integer.parseInt(idadeSujA);
                }
                if (nifB != -1 && valid) {
                    String idadeSujB = idadeSujB_TF.getText();
                    if (!(idadeSujB != null && StringUtil.isNumeric(idadeSujB) && idadeSujB.indexOf(45) == -1)) {
                        DialogFactory.instance().showErrorDialog("Idade do sujeito passivo B inv\u00e1lida.");
                        valid = false;
                    } else {
                        this.idadeSPB = Integer.parseInt(idadeSujB);
                    }
                }
                if (nifF != -1 && valid) {
                    String idadeSujF = idadeSujF_TF.getText();
                    if (!(idadeSujF != null && StringUtil.isNumeric(idadeSujF) && idadeSujF.indexOf(45) == -1)) {
                        DialogFactory.instance().showErrorDialog("Idade do sujeito passivo F inv\u00e1lida.");
                        valid = false;
                    } else {
                        this.idadeSPF = Integer.parseInt(idadeSujF);
                    }
                }
            }
            if (hasDependentes && valid) {
                int countDepComIdadeMenorOuIgual3 = 0;
                for (Map.Entry idadeSujD : idadesSujD.entrySet()) {
                    if (!valid) break;
                    String idadeDepD = ((JTextField)idadeSujD.getValue()).getText();
                    if (!(idadeDepD != null && StringUtil.isNumeric(idadeDepD) && idadeDepD.indexOf(45) == -1)) {
                        DialogFactory.instance().showErrorDialog("Idade do dependente n\u00e3o deficiente " + idadeSujD.getKey() + " inv\u00e1lida.");
                        valid = false;
                        continue;
                    }
                    countDepComIdadeMenorOuIgual3+=Integer.valueOf(idadeDepD) <= 3 ? 1 : 0;
                }
                this.nDep = countDepComIdadeMenorOuIgual3;
                int countDepDComIdadeMenorOuIgual3 = 0;
                for (Map.Entry idadeSujDD : idadesSujDD.entrySet()) {
                    if (!valid) break;
                    String idadeDepDD = ((JTextField)idadeSujDD.getValue()).getText();
                    if (!(idadeDepDD != null && StringUtil.isNumeric(idadeDepDD) && idadeDepDD.indexOf(45) == -1)) {
                        DialogFactory.instance().showErrorDialog("Idade do dependente deficiente " + idadeSujDD.getKey() + " inv\u00e1lida.");
                        valid = false;
                        continue;
                    }
                    countDepDComIdadeMenorOuIgual3+=Integer.valueOf(idadeDepDD) <= 3 ? 1 : 0;
                }
                this.nDep+=countDepDComIdadeMenorOuIgual3;
            }
            if (!hasDependentesGC || !valid) continue;
            int countDepGCComIdadeMenorOuIgual3 = 0;
            for (Map.Entry idadeSujGC : idadesSujGC.entrySet()) {
                if (!valid) break;
                String idadeDepGC = ((JTextField)idadeSujGC.getValue()).getText();
                if (!(idadeDepGC != null && StringUtil.isNumeric(idadeDepGC) && idadeDepGC.indexOf(45) == -1)) {
                    DialogFactory.instance().showErrorDialog("Idade do dependente guarda conjunta " + idadeSujGC.getKey() + " inv\u00e1lida.");
                    valid = false;
                    continue;
                }
                countDepGCComIdadeMenorOuIgual3+=Integer.valueOf(idadeDepGC) <= 3 ? 1 : 0;
            }
            this.nDepGC = countDepGCComIdadeMenorOuIgual3;
        } while (!valid);
        return true;
    }

    private void clear() {
        this.valuesSimul = null;
        this.idadeSPA = 0;
        this.idadeSPB = 0;
        this.idadeSPF = 0;
        this.idadeD1 = 0;
        this.idadeD2 = 0;
        this.idadeD3 = 0;
        this.nDep = 0;
        this.nDepGC = 0;
    }

}

