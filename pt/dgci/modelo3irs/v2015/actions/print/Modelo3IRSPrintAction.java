/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.print;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.opensoft.swing.BrowserControl;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Modelo3IRSPrintAction
extends EnhancedAction {
    private static final long serialVersionUID = 3199363764970693053L;

    public Modelo3IRSPrintAction() {
        super("Imprimir", IconFactory.getIconSmall(GUIParameters.ICON_IMPRIMIR), IconFactory.getIconBig(GUIParameters.ICON_IMPRIMIR), KeyStroke.getKeyStroke(80, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Modelo3IRSv2015Model model = (Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel();
        try {
            File tempFile = File.createTempFile("print", ".pdf");
            FileOutputStream os = new FileOutputStream(tempFile);
            Modelo3IRSPrinter renderer = new Modelo3IRSPrinter(os, model);
            tempFile.deleteOnExit();
            renderer.render();
            BrowserControl.displayURL("file:///" + tempFile.getCanonicalPath());
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
    }
}

