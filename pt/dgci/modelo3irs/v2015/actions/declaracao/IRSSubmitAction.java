/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  netscape.javascript.JSObject
 */
package pt.dgci.modelo3irs.v2015.actions.declaracao;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.applet.Applet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import netscape.javascript.JSObject;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.actions.util.IRSSubmitDialog;
import pt.dgci.modelo3irs.v2015.util.IRSPrintSubmitReceipt;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.TaxClientRevampedApplet;
import pt.opensoft.taxclient.actions.declaracao.pf.PortalTaxClientRevampedSubmitAction;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitDialog;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.gui.PortalTaxClientRevampedSubmitAuxDialogs;
import pt.opensoft.taxclient.gui.SubmitDialog;
import pt.opensoft.taxclient.gui.SubmitResponse;
import pt.opensoft.taxclient.http.SubmitReceipt;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.SubmitTaxUtils;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.StringUtil;

public class IRSSubmitAction
extends PortalTaxClientRevampedSubmitAction {
    private static final long serialVersionUID = 5002145149166494733L;
    public static final String SUJ_PASS_A = "SPA";
    public static final String SUJ_PASS_B = "SPB";
    public static final String TOC = "TOC";
    public static final String RECEIPT_DATE = "Data e Hora de Recep\u00e7\u00e3o";
    public static final String RECEIPT_DECLID = "Identifica\u00e7\u00e3o da Declara\u00e7\u00e3o";
    public static final String RECEIPT_YEAR = "Ano do Exerc\u00edcio";
    public static final int FECHAR_DIALOG_IRS = 1;
    public static final int IMPRIMIR_RECEIPT_IRS = 0;
    private long lastHashCodeWhenSubmited = 0;
    private boolean lastResponseHadWarnings = false;
    private IRSSubmitDialog dialogs = new IRSSubmitDialog();

    public IRSSubmitAction() {
        super(null, null, null, Modelo3IRSv2015Parameters.instance().getAppVersion());
        this.setSubmitDialogs(this.dialogs);
    }

    @Override
    protected void handleResponseWithWarnings(SubmitResponse submitResponse) {
        this.lastResponseHadWarnings = true;
        this.lastHashCodeWhenSubmited = Session.getCurrentDeclaracao().getDeclaracaoModel().hashCode();
        if (!(submitResponse.getValidationResult() == null || submitResponse.getValidationResult().isEmpty())) {
            if (submitResponse.getValidationResult().hasMessages()) {
                DialogFactory.instance().showWarningDialog("<html><b>A declara\u00e7\u00e3o n\u00e3o foi submetida.</b><br>Verifique os alertas detectados.</html>");
                MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
                WarningDisplayer warningDisplayer = multiPurposeDisplayer.getWarningDisplayer();
                warningDisplayer.fillWarningsList(submitResponse.getValidationResult());
                multiPurposeDisplayer.showWarningDisplayer();
                return;
            }
        } else if (!(submitResponse.getWarnings() == null || submitResponse.getWarnings().isEmpty())) {
            SubmitDialog dialog = PortalTaxClientRevampedSubmitAuxDialogs.getWarningSubmitDialog(JOptionPane.getFrameForComponent(DialogFactory.getDummyFrame()), submitResponse.getWarnings());
            dialog.setVisible(true);
            if (dialog.getActionPerfomed() == 2) {
                try {
                    this.submitTax(false);
                }
                catch (Exception e) {
                    throw new IllegalStateException(e.toString());
                }
            }
        } else {
            throw new IllegalStateException("Code is NOK, but list of Warnings are null");
        }
    }

    @Override
    public String getSubmitUrl() {
        if (Modelo3IRSv2015Parameters.instance().isFase2() && this.dialogs.isFromTOC()) {
            if (Modelo3IRSv2015Parameters.instance().isAppletOffline()) {
                return Modelo3IRSv2015Parameters.instance().getUrlTOCSubmitAppletOffline();
            }
            return Modelo3IRSv2015Parameters.instance().getUrlTOCSubmit();
        }
        if (Modelo3IRSv2015Parameters.instance().isAppletOffline()) {
            return Modelo3IRSv2015Parameters.instance().getUrlSubmitAppletOffline();
        }
        return Modelo3IRSv2015Parameters.instance().getUrlSubmit();
    }

    @Override
    public String getAppletUrl() {
        if (Modelo3IRSv2015Parameters.instance().isFase2() && this.dialogs.isFromTOC()) {
            return Modelo3IRSv2015Parameters.instance().getUrlTOCSubmitApplet();
        }
        return Modelo3IRSv2015Parameters.instance().getUrlSubmitApplet();
    }

    @Override
    protected void submit(boolean firstTime) throws Exception {
        if (this.lastResponseHadWarnings && this.lastHashCodeWhenSubmited == (long)Session.getCurrentDeclaracao().getDeclaracaoModel().hashCode()) {
            this.dialogs.showCheckWarningsPanel();
        } else {
            this.dialogs.hideCheckWarningsPanel();
        }
        super.submit(firstTime);
    }

    @Override
    protected void submitTax(boolean checkWarnings) throws Exception {
        super.submitTax(this.dialogs.checkWarnings());
    }

    @Override
    protected Object[] getRedirectParameters(SubmitResponse submitResponse) {
        if (submitResponse == null || submitResponse.getSubmitReceipt() == null) {
            return new Object[0];
        }
        SubmitReceipt receipt = submitResponse.getSubmitReceipt();
        return new Object[]{receipt.getDeclId()};
    }

    @Override
    protected TaxClientRevampedSubmitInfo initSubmitInfo() throws Exception {
        String declId;
        TaxClientRevampedSubmitInfo submitInfo = super.initSubmitInfo();
        if (Session.isApplet() && !StringUtil.isEmpty(declId = Session.getApplet().getParameter("declId"))) {
            submitInfo.addOptionalField("declId", declId);
        }
        return submitInfo;
    }

    @Override
    protected void handleUnknownCode(SubmitResponse submitResponse) {
        if (submitResponse.getErrorCode() == 90 && submitResponse.getValidationResult().hasMessages()) {
            DeclValidationMessage declValidationMessage = (DeclValidationMessage)submitResponse.getValidationResult().getMessages().get(0);
            ArrayList<String> errorTempList = new ArrayList<String>(1);
            errorTempList.add(StringUtil.toHtml(declValidationMessage.get_code()));
            JPanel panelDefaultException = SubmitTaxUtils.constructErrorsPanel("Ocorreu um erro na submiss\u00e3o da declara\u00e7\u00e3o.", errorTempList);
            JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelDefaultException, "Falha na Submiss\u00e3o", 0);
        } else {
            super.handleUnknownCode(submitResponse);
        }
    }

    @Override
    protected int showReceiptDialog(SubmitResponse submitResponse) {
        JPanel receiptPanel = this.dialogs.constructReceiptPanel(submitResponse);
        int option = JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), receiptPanel, "Dados da Declara\u00e7\u00e3o", -1, 1, null, new Object[]{"Imprimir", "Fechar"}, "Imprimir");
        return option;
    }

    @Override
    protected void handleResponseCodeOK(SubmitResponse submitResponse) {
        if (submitResponse.getSubmitReceipt() != null) {
            boolean close = false;
            while (!close) {
                int option = this.showReceiptDialog(submitResponse);
                boolean bl = close = option == 1;
                if (option == 0) {
                    IRSPrintSubmitReceipt.print(this, submitResponse.getSubmitReceipt());
                    continue;
                }
                if (!close || !Session.isApplet() || StringUtil.isEmpty(TaxclientParameters.getRedirectAfterSubmission())) continue;
                TaxClientRevampedApplet applet = Session.getApplet();
                JSObject.getWindow((Applet)applet).call(TaxclientParameters.getRedirectAfterSubmission(), this.getRedirectParameters(submitResponse));
            }
        } else {
            throw new IllegalArgumentException("SubmitResponse invalido: Code - OK; Receipt - Null");
        }
    }
}

