/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.declaracao;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro01;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.util.Modelo3IRSSessionDisableManager;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.TaxClientRevampedApplet;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.StringUtil;

public class IRSSubstituirAction
extends EnhancedAction {
    private static final long serialVersionUID = 3199363764970693053L;

    public IRSSubstituirAction() {
        super("Substituir", IconFactory.getIconSmall(GUIParameters.ICON_SUBMETER), IconFactory.getIconBig(GUIParameters.ICON_SUBMETER), "Substituir Declara\u00e7\u00e3o", null);
        this.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!DialogFactory.instance().showYesNoDialog("Pretende preencher uma declara\u00e7\u00e3o de substitui\u00e7\u00e3o?")) {
            return;
        }
        Modelo3IRSv2015Model currentDeclaracao = (Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel();
        currentDeclaracao.getRosto().getQuadro04().setQ04B1("2");
        TaxClientRevampedApplet currentApplet = Session.getApplet();
        String nomeSujA = currentApplet.getParameter("nomeSujA");
        currentDeclaracao.getRosto().getQuadro03().setQ03C01(!StringUtil.isEmpty(nomeSujA) ? nomeSujA : null);
        String nomeSujB = currentApplet.getParameter("nomeSujB");
        currentDeclaracao.getRosto().getQuadro03().setQ03C02(!StringUtil.isEmpty(nomeSujB) ? nomeSujB : null);
        String sf = currentApplet.getParameter("sf");
        if (!StringUtil.isEmpty(sf)) {
            currentDeclaracao.getRosto().getQuadro01().setQ01C01(Long.parseLong(sf));
        }
        Modelo3IRSSessionDisableManager.revertDisableAll();
        this.setEnabled(false);
        Session.getCurrentDeclaracao().setSelectedRootAnexo(new FormKey(RostoModel.class));
        Session.getCurrentDeclaracao().setSelectedQuadro(QuadroInicio.class.getSimpleName());
    }
}

