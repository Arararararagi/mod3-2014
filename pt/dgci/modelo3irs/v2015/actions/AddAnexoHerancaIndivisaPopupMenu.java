/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import pt.dgci.modelo3irs.v2015.actions.AddAnexoJPopupMenu;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoActionBean;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class AddAnexoHerancaIndivisaPopupMenu
extends AddAnexoJPopupMenu {
    private static final long serialVersionUID = -2415792547865038761L;
    private String subId;
    private boolean isHerancaIndivisa;

    public AddAnexoHerancaIndivisaPopupMenu(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model, question);
    }

    @Override
    public boolean validateSubId(DeclaracaoModel model, String subID) {
        if (!super.validateSubId(model, subID)) {
            return false;
        }
        if (!(this.isHerancaIndivisa || NifValidator.isSingular(String.valueOf(subID)))) {
            DialogFactory.instance().showErrorDialog("O NIF de titular de rendimentos deve ser de Pessoa Singular, \nquando o anexo n\u00e3o \u00e9 respeitante \u00e0 atividade de heran\u00e7a indivisa.");
            return false;
        }
        if (this.isHerancaIndivisa && !Modelo3IRSValidatorUtil.startsWith(subID, "7", "9")) {
            DialogFactory.instance().showErrorDialog("O NIPC da Heran\u00e7a deve come\u00e7ar por 7 ou 9.");
            return false;
        }
        return true;
    }

    @Override
    protected void doAddAnexoAction(DeclaracaoModel model) {
        AddMultipleAnexoActionBean userOutput = this.obtainHerancaIndivisa(model);
        if (!userOutput.getType().equals((Object)AddMultipleAnexoActionBean.UserAction.CANCELLED)) {
            super.doAddAnexoAction(model);
        }
    }

    @Override
    public AddMultipleAnexoActionBean obtainSubId(DeclaracaoModel model) {
        AddMultipleAnexoActionBean userOutput = super.obtainSubId(model);
        this.subId = userOutput.getSubID();
        return userOutput;
    }

    public AddMultipleAnexoActionBean obtainHerancaIndivisa(DeclaracaoModel model) {
        int herancaIndivisaOption = JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), new JLabel("Este anexo respeita \u00e0 actividade de heran\u00e7a indivisa?\n"), "Novo Anexo", 1, 3, null, new Object[]{"Sim", "N\u00e3o", "Cancelar"}, "N\u00e3o");
        if (herancaIndivisaOption == 2) {
            return new AddMultipleAnexoActionBean(null, AddMultipleAnexoActionBean.UserAction.CANCELLED);
        }
        this.isHerancaIndivisa = herancaIndivisaOption == 0;
        return new AddMultipleAnexoActionBean(new Boolean(this.isHerancaIndivisa).toString(), AddMultipleAnexoActionBean.UserAction.OK);
    }

    @Override
    public final void postAddAnexo(DeclaracaoModel model) {
        this.postAddAnexo(model, this.subId, this.isHerancaIndivisa);
    }

    protected abstract void postAddAnexo(DeclaracaoModel var1, String var2, boolean var3);
}

