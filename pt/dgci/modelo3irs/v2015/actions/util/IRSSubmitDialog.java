/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.util;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.actions.util.IRSSubmitPanelForm;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.opensoft.field.NifValidator;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.actions.util.pf.PFSubmitPanel;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitDialog;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.http.SubmitUser;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.StringUtil;

public class IRSSubmitDialog
extends PortalTaxClientRevampedSubmitDialog {
    public IRSSubmitDialog() {
        super(new IRSSubmitPanelForm());
        this.hideCheckWarningsPanel();
    }

    @Override
    public JPanel constructDialogPanel() {
        Modelo3IRSv2015Model modelo = (Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel();
        IRSSubmitPanelForm submitPanelForm = (IRSSubmitPanelForm)this.panel;
        RostoModel rosto = modelo.getRosto();
        submitPanelForm.getNifSujPass().setText(String.valueOf(rosto.getQuadro03().getQ03C03()));
        submitPanelForm.getNifSujPass().setEditable(false);
        if (rosto.getQuadro03().getQ03C04() == null) {
            submitPanelForm.getPanelSujPassB().setVisible(false);
            submitPanelForm.getTocAutorizadoSujB().setVisible(false);
            submitPanelForm.getTocAutorizadoSujBLabel().setVisible(false);
        } else {
            submitPanelForm.getNifSujPass2().setText(String.valueOf(rosto.getQuadro03().getQ03C04()));
            submitPanelForm.getNifSujPass2().setEditable(false);
            submitPanelForm.getPanelSujPassB().setVisible(true);
            submitPanelForm.getTocAutorizadoSujB().setVisible(submitPanelForm.getFromTOC().isSelected());
            submitPanelForm.getTocAutorizadoSujBLabel().setVisible(submitPanelForm.getFromTOC().isSelected());
        }
        if (Modelo3IRSv2015Parameters.instance().isFase2()) {
            if (Session.isApplet()) {
                String nifTOC = Session.getApplet().getParameter("nifTOC");
                boolean isAppletTocSubmit = NifValidator.validate(nifTOC);
                submitPanelForm.getFromTOC().setEnabled(false);
                submitPanelForm.getFromTOC().setVisible(false);
                submitPanelForm.getFromTOCLabel().setVisible(false);
                submitPanelForm.getFromTOC().setSelected(isAppletTocSubmit);
                submitPanelForm.alterarEstadoCamposTOC(submitPanelForm.getFromTOC().isSelected(), false);
                submitPanelForm.getTOCPanel().setVisible(isAppletTocSubmit);
                submitPanelForm.getNifTOC().setText(isAppletTocSubmit ? nifTOC : null);
                submitPanelForm.getNifTOC().setEditable(!isAppletTocSubmit);
            } else {
                submitPanelForm.getTOCPanel().setVisible(true);
            }
        } else {
            submitPanelForm.getTOCPanel().setVisible(false);
        }
        return super.constructDialogPanel();
    }

    @Override
    protected void buildSubmitUserObject(PFSubmitPanel panel) {
        super.buildSubmitUserObject(panel);
        this.submitUser.setNifB(((IRSSubmitPanelForm)panel).getNifSujPass2().getText());
        String passwordB = new String(((IRSSubmitPanelForm)panel).getPassSujPass2().getPassword());
        if (!(StringUtil.isEmpty(passwordB) || passwordB.length() <= 16)) {
            this.submitUser.setPasswordB(passwordB.substring(0, 16));
        } else {
            this.submitUser.setPasswordB(passwordB);
        }
        passwordB = null;
        String passwordA = new String(((IRSSubmitPanelForm)panel).getPassSujPass().getPassword());
        if (!(StringUtil.isEmpty(passwordA) || passwordA.length() <= 16)) {
            this.submitUser.setPasswordA(passwordA.substring(0, 16));
        } else {
            this.submitUser.setPasswordA(passwordA);
        }
        passwordA = null;
    }

    @Override
    public void resetFinalFields() {
        super.resetFinalFields();
        ((IRSSubmitPanelForm)this.panel).getNifSujPass2().setText("");
        ((IRSSubmitPanelForm)this.panel).getPassSujPass2().setText("");
        this.panel.getFromTOC().setSelected(false);
        ((IRSSubmitPanelForm)this.panel).alterarEstadoCamposTOC(this.panel.getFromTOC().isSelected());
    }

    @Override
    public boolean validateInputs() {
        IRSSubmitPanelForm submitPanelForm = (IRSSubmitPanelForm)this.panel;
        if (StringUtil.isEmpty(submitPanelForm.getNifSujPass().getText())) {
            DialogFactory.instance().showErrorDialog("Por favor preencha o NIF do Sujeito Passivo A.");
            return false;
        }
        if (!(submitPanelForm.getTocAutorizadoSujA().isSelected() || submitPanelForm.getPassSujPass().getPassword().length != 0)) {
            DialogFactory.instance().showErrorDialog("Por favor preencha a password do Sujeito Passivo A.");
            return false;
        }
        if (!(submitPanelForm.getTocAutorizadoSujB().isSelected() || StringUtil.isEmpty(submitPanelForm.getNifSujPass2().getText()) || submitPanelForm.getPassSujPass2().getPassword().length != 0)) {
            DialogFactory.instance().showErrorDialog("Por favor preencha a password do Sujeito Passivo B.");
            return false;
        }
        if (this.panel.getFromTOC().isSelected()) {
            if (StringUtil.isEmpty(submitPanelForm.getNifTOC().getText())) {
                DialogFactory.instance().showErrorDialog("Por favor preencha o NIF do TOC.");
                return false;
            }
            if (submitPanelForm.getPassTOC().getPassword().length == 0) {
                DialogFactory.instance().showErrorDialog("Por favor preencha a password do TOC.");
                return false;
            }
        }
        return true;
    }

    public void hideCheckWarningsPanel() {
        IRSSubmitPanelForm submitPanelForm = (IRSSubmitPanelForm)this.panel;
        submitPanelForm.getPanelCheckWarnings().setVisible(false);
        submitPanelForm.getCheckWarnings().setSelected(false);
    }

    public void showCheckWarningsPanel() {
        IRSSubmitPanelForm submitPanelForm = (IRSSubmitPanelForm)this.panel;
        submitPanelForm.getPanelCheckWarnings().setVisible(true);
    }

    public boolean checkWarnings() {
        IRSSubmitPanelForm submitPanelForm = (IRSSubmitPanelForm)this.panel;
        return !submitPanelForm.getCheckWarnings().isSelected();
    }

    public boolean isFromTOC() {
        return this.panel.getFromTOC().isSelected();
    }

    @Override
    public String getUserDescFromId(SubmitUser submitUser) {
        if (submitUser.getId() != null && submitUser.getId().equals("TOC")) {
            return "T\u00e9cnico Oficial de Contas";
        }
        if (submitUser.getId() != null && submitUser.getId().equals("SPB")) {
            return "Sujeito Passivo B";
        }
        return "Sujeito Passivo A";
    }
}

