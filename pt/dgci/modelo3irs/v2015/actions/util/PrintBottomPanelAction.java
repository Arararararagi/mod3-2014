/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.util;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.BrowserControl;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;

public class PrintBottomPanelAction
extends EnhancedAction {
    private static final long serialVersionUID = 7069012199969848271L;
    private String printHTML = "";

    public PrintBottomPanelAction(String title, String printHTML) {
        this(title, printHTML, IconFactory.getIconBig(GUIParameters.ICON_IMPRIMIR), IconFactory.getIconBig(GUIParameters.ICON_IMPRIMIR));
    }

    public PrintBottomPanelAction(String title, String printHTML, Icon iconSmall, Icon iconBig) {
        super(title, iconSmall, iconBig, KeyStroke.getKeyStroke(80, 3));
        this.printHTML = printHTML;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.printHTML != null) {
            try {
                File tempFile = File.createTempFile("print", ".html", new File(System.getProperty("user.home")));
                tempFile.deleteOnExit();
                FileWriter fWriter = new FileWriter(tempFile);
                fWriter.write(this.printHTML);
                fWriter.close();
                BrowserControl.displayURL("file:///" + tempFile.getCanonicalPath());
            }
            catch (Exception e1) {
                throw new RuntimeException(e1);
            }
        }
    }

    public void setPrintHTML(String printHTML) {
        this.printHTML = printHTML;
    }
}

