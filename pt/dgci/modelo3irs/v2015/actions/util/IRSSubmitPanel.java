/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.util;

import com.jgoodies.forms.layout.CellConstraints;
import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import pt.opensoft.taxclient.actions.util.pf.PFSubmitPanel;

public class IRSSubmitPanel
extends PFSubmitPanel {
    private JLabel labelNIFB;
    protected JTextField nifSujPassB;
    private JLabel labelPswB;
    protected JPasswordField passSujPassB;

    public IRSSubmitPanel() {
        super(false);
        this.fromTOC.setEnabled(false);
    }

    public JTextField getNifSujPassB() {
        return this.nifSujPassB;
    }

    public JPasswordField getPassSujPassB() {
        return this.passSujPassB;
    }

    @Override
    protected String getTextSujPass() {
        return "NIF do Sujeito Passivo A:";
    }

    @Override
    protected String getTextPassSujPass() {
        return "Senha do Sujeito Passivo A:";
    }

    @Override
    protected void addSujPassB() {
        this.labelNIFB = new JLabel();
        this.nifSujPassB = new JTextField();
        this.labelPswB = new JLabel();
        this.passSujPassB = new JPasswordField();
        CellConstraints cc = new CellConstraints();
        this.labelNIFB.setText("NIF do Sujeito Passivo B:");
        this.add((Component)this.labelNIFB, cc.xy(3, 7));
        this.add((Component)this.nifSujPassB, cc.xy(5, 7));
        this.labelPswB.setText("Senha do Sujeito Passivo B:");
        this.add((Component)this.labelPswB, cc.xy(3, 9));
        this.add((Component)this.passSujPassB, cc.xy(5, 9));
    }
}

