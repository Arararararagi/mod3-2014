/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitUser;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class PrePreenchimentoSubmitInfo {
    private String appVersion;
    private boolean checkWarnings;
    private List<Pair> optionalFields;
    private PortalTaxClientRevampedSubmitUser submitUser;
    private long exercicio;

    public PrePreenchimentoSubmitInfo(PortalTaxClientRevampedSubmitUser submitUser, long exercicio) throws IOException {
        this.submitUser = submitUser;
        this.exercicio = exercicio;
        this.optionalFields = new ArrayList<Pair>();
        this.checkWarnings = true;
    }

    public PortalTaxClientRevampedSubmitUser getSubmitUser() {
        return this.submitUser;
    }

    public void setSubmitUser(PortalTaxClientRevampedSubmitUser submitUser) {
        this.submitUser = submitUser;
    }

    public long getExercicio() {
        return this.exercicio;
    }

    public void setExercicio(long exercicio) {
        this.exercicio = exercicio;
    }

    public String getAppVersion() {
        return this.appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public boolean isCheckWarnings() {
        return this.checkWarnings;
    }

    public void setCheckWarnings(boolean checkWarnings) {
        this.checkWarnings = checkWarnings;
    }

    public List<Pair> getOptionalFields() {
        return this.optionalFields;
    }

    public void addOptionalField(String key, String value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("args are null");
        }
        Pair<String, String> pair = new Pair<String, String>(key, value);
        this.optionalFields.add(pair);
    }

    public boolean hasAppVersion() {
        return !StringUtil.isEmpty(this.getAppVersion());
    }
}

