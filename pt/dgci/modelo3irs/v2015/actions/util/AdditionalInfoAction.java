/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.util;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.dgci.modelo3irs.v2015.actions.util.ShowSimulationDataAction;
import pt.dgci.modelo3irs.v2015.gui.SimuladorPageDisplayer;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.multi.impl.DefaultMultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public class AdditionalInfoAction
extends EnhancedAction {
    private static final long serialVersionUID = 837613071530190318L;

    public AdditionalInfoAction(String title) {
        super(title, IconFactory.getIconBig(GUIParameters.ICON_INFO), IconFactory.getIconBig(GUIParameters.ICON_INFO), KeyStroke.getKeyStroke(521, 3));
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        DefaultMultiPurposeDisplayer multiPurposeDisplayer = (DefaultMultiPurposeDisplayer)Session.getMainFrame().getMultiPurposeDisplayer();
        SimuladorPageDisplayer simuladorDisplayer = (SimuladorPageDisplayer)multiPurposeDisplayer.getGenericCard("Simular");
        simuladorDisplayer.showAdditionalHelp();
        simuladorDisplayer.toggleAction(this, false);
        simuladorDisplayer.toggleAction(SimuladorPageDisplayer.simulationDataAction, true);
    }
}

