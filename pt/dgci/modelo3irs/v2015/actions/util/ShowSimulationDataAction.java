/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.util;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.dgci.modelo3irs.v2015.actions.util.AdditionalInfoAction;
import pt.dgci.modelo3irs.v2015.gui.SimuladorPageDisplayer;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.multi.impl.DefaultMultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public class ShowSimulationDataAction
extends EnhancedAction {
    private static final long serialVersionUID = 837613071530190318L;

    public /* varargs */ ShowSimulationDataAction(String title, EnhancedAction ... togglingActions) {
        super(title, IconFactory.getIconBig(GUIParameters.ICON_SIMULAR), IconFactory.getIconBig(GUIParameters.ICON_SIMULAR), KeyStroke.getKeyStroke(521, 3));
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        try {
            DefaultMultiPurposeDisplayer multiPurposeDisplayer = (DefaultMultiPurposeDisplayer)Session.getMainFrame().getMultiPurposeDisplayer();
            SimuladorPageDisplayer simuladorDisplayer = (SimuladorPageDisplayer)multiPurposeDisplayer.getGenericCard("Simular");
            simuladorDisplayer.updateHelp();
            simuladorDisplayer.toggleAction(this, false);
            simuladorDisplayer.toggleAction(SimuladorPageDisplayer.additionalInfoAction, true);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

