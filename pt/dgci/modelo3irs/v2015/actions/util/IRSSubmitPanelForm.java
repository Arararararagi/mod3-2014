/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.util;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.listeners.AutomaticPasswordFillingDocumentListener;
import pt.opensoft.taxclient.actions.util.pf.PFSubmitPanel;

public class IRSSubmitPanelForm
extends PFSubmitPanel {
    private JComponent separator1;
    private JLabel label1;
    protected JTextField nifSujPass;
    private JLabel label2;
    protected JPasswordField passSujPass;
    private JPanel panelSujPassB;
    private JComponent separator2;
    private JLabel label3;
    protected JTextField nifSujPass2;
    private JLabel label4;
    protected JPasswordField passSujPass2;
    public JPanel TOCPanel;
    private JComponent separator3;
    private JPanel TOCInnerPanel;
    public JLabel fromTOCLabel;
    protected JCheckBox fromTOC;
    private JLabel tocAutorizadoSujALabel;
    protected JCheckBox tocAutorizadoSujA;
    protected JLabel tocAutorizadoSujBLabel;
    protected JCheckBox tocAutorizadoSujB;
    private JLabel nifTOCLabel;
    protected JTextField nifTOC;
    private JLabel passTOCLabel;
    protected JPasswordField passTOC;
    protected JPanel panelCheckWarnings;
    protected JCheckBox checkWarnings;

    public IRSSubmitPanelForm() {
        this.initComponents();
        this.fromTOC.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                IRSSubmitPanelForm.this.fromTOCActionPerformed(e);
            }
        });
        this.tocAutorizadoSujA.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                IRSSubmitPanelForm.this.passSujPass.setEditable(!IRSSubmitPanelForm.this.tocAutorizadoSujA.isSelected());
                IRSSubmitPanelForm.this.passSujPass.setText(null);
            }
        });
        this.tocAutorizadoSujB.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                IRSSubmitPanelForm.this.passSujPass2.setEditable(!IRSSubmitPanelForm.this.tocAutorizadoSujB.isSelected());
                IRSSubmitPanelForm.this.passSujPass2.setText(null);
            }
        });
        this.passSujPass.addAncestorListener(new AncestorListener(){

            @Override
            public void ancestorAdded(AncestorEvent ae) {
                ae.getComponent().requestFocus();
            }

            @Override
            public void ancestorRemoved(AncestorEvent ae) {
            }

            @Override
            public void ancestorMoved(AncestorEvent ae) {
            }
        });
        if (Modelo3IRSv2015Parameters.instance().isAutomaticPasswordFillingEnabled()) {
            this.nifSujPass.getDocument().addDocumentListener(new AutomaticPasswordFillingDocumentListener(this.passSujPass));
            this.nifSujPass2.getDocument().addDocumentListener(new AutomaticPasswordFillingDocumentListener(this.passSujPass2));
            this.nifTOC.getDocument().addDocumentListener(new AutomaticPasswordFillingDocumentListener(this.passTOC));
        }
    }

    private void fromTOCActionPerformed(ActionEvent e) {
        this.alterarEstadoCamposTOC(this.fromTOC.isSelected());
    }

    @Override
    public void alterarEstadoCamposTOC(boolean estado) {
        this.alterarEstadoCamposTOC(estado, true);
    }

    public void alterarEstadoCamposTOC(boolean estado, boolean resetValues) {
        this.nifTOCLabel.setVisible(estado);
        this.nifTOC.setEditable(estado);
        this.nifTOC.setVisible(estado);
        if (resetValues) {
            this.nifTOC.setText(null);
        }
        this.passTOCLabel.setVisible(estado);
        this.passTOC.setEditable(estado);
        this.passTOC.setVisible(estado);
        if (resetValues) {
            this.passTOC.setText(null);
        }
        this.tocAutorizadoSujALabel.setVisible(estado);
        this.tocAutorizadoSujA.setVisible(estado);
        if (resetValues) {
            this.tocAutorizadoSujA.setSelected(false);
        }
        this.tocAutorizadoSujBLabel.setVisible(estado && this.panelSujPassB.isVisible());
        this.tocAutorizadoSujB.setVisible(estado && this.panelSujPassB.isVisible());
        if (resetValues) {
            this.tocAutorizadoSujB.setSelected(false);
        }
        this.passSujPass.setEditable(!this.tocAutorizadoSujA.isSelected());
        this.passSujPass2.setEditable(!this.tocAutorizadoSujB.isSelected());
    }

    @Override
    public JTextField getNifSujPass() {
        return this.nifSujPass;
    }

    @Override
    public JPasswordField getPassSujPass() {
        return this.passSujPass;
    }

    public JTextField getNifSujPass2() {
        return this.nifSujPass2;
    }

    public JPasswordField getPassSujPass2() {
        return this.passSujPass2;
    }

    public JPanel getPanelSujPassB() {
        return this.panelSujPassB;
    }

    public JPanel getPanelCheckWarnings() {
        return this.panelCheckWarnings;
    }

    public JCheckBox getCheckWarnings() {
        return this.checkWarnings;
    }

    public JLabel getTocAutorizadoSujBLabel() {
        return this.tocAutorizadoSujBLabel;
    }

    public JPanel getTOCPanel() {
        return this.TOCPanel;
    }

    public JPanel getTOCInnerPanel() {
        return this.TOCInnerPanel;
    }

    @Override
    public JCheckBox getFromTOC() {
        return this.fromTOC;
    }

    public JCheckBox getTocAutorizadoSujA() {
        return this.tocAutorizadoSujA;
    }

    public JCheckBox getTocAutorizadoSujB() {
        return this.tocAutorizadoSujB;
    }

    @Override
    public JTextField getNifTOC() {
        return this.nifTOC;
    }

    @Override
    public JPasswordField getPassTOC() {
        return this.passTOC;
    }

    public JLabel getFromTOCLabel() {
        return this.fromTOCLabel;
    }

    private void initComponents() {
        DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
        this.separator1 = compFactory.createSeparator("Sujeito Passivo A");
        this.label1 = new JLabel();
        this.nifSujPass = new JTextField();
        this.label2 = new JLabel();
        this.passSujPass = new JPasswordField();
        this.panelSujPassB = new JPanel();
        this.separator2 = compFactory.createSeparator("Sujeito Passivo B");
        this.label3 = new JLabel();
        this.nifSujPass2 = new JTextField();
        this.label4 = new JLabel();
        this.passSujPass2 = new JPasswordField();
        this.TOCPanel = new JPanel();
        this.separator3 = compFactory.createSeparator("TOC");
        this.TOCInnerPanel = new JPanel();
        this.fromTOCLabel = new JLabel();
        this.fromTOC = new JCheckBox();
        this.tocAutorizadoSujALabel = new JLabel();
        this.tocAutorizadoSujA = new JCheckBox();
        this.tocAutorizadoSujBLabel = new JLabel();
        this.tocAutorizadoSujB = new JCheckBox();
        this.nifTOCLabel = new JLabel();
        this.nifTOC = new JTextField();
        this.passTOCLabel = new JLabel();
        this.passTOC = new JPasswordField();
        this.panelCheckWarnings = new JPanel();
        this.checkWarnings = new JCheckBox();
        CellConstraints cc = new CellConstraints();
        this.setLayout(new FormLayout("10dlu:grow, $lcgap, 10dlu, $lcgap, 130dlu, $ugap, 90dlu, $lcgap, 20dlu:grow", "3*(default, $lgap), 3*(default), $lgap, default"));
        this.add((Component)this.separator1, cc.xywh(3, 3, 5, 1));
        this.label1.setText("NIF do Sujeito Passivo A:");
        this.add((Component)this.label1, cc.xy(5, 5));
        this.add((Component)this.nifSujPass, cc.xy(7, 5));
        this.label2.setText("Senha do Sujeito Passivo A:");
        this.add((Component)this.label2, cc.xy(5, 7));
        this.add((Component)this.passSujPass, cc.xy(7, 7));
        this.panelSujPassB.setOpaque(false);
        this.panelSujPassB.setLayout(new FormLayout("10dlu, $lcgap, 130dlu, $ugap, 90dlu:grow", "$ugap, 2*(default, $lgap), default"));
        this.panelSujPassB.add((Component)this.separator2, cc.xywh(1, 2, 5, 1));
        this.label3.setText("NIF do Sujeito Passivo B:");
        this.panelSujPassB.add((Component)this.label3, cc.xy(3, 4));
        this.panelSujPassB.add((Component)this.nifSujPass2, cc.xy(5, 4));
        this.label4.setText("Senha do Sujeito Passivo B:");
        this.panelSujPassB.add((Component)this.label4, cc.xy(3, 6));
        this.panelSujPassB.add((Component)this.passSujPass2, cc.xy(5, 6));
        this.add((Component)this.panelSujPassB, cc.xywh(3, 8, 5, 1));
        this.TOCPanel.setOpaque(false);
        this.TOCPanel.setLayout(new FormLayout("10dlu, $rgap, default", "$ugap, default, $rgap, 90dlu"));
        this.TOCPanel.add((Component)this.separator3, cc.xywh(1, 2, 3, 1));
        this.TOCInnerPanel.setOpaque(false);
        this.TOCInnerPanel.setLayout(new FormLayout("130dlu, $ugap, 89dlu:grow", "4*(default, $lgap), default"));
        this.fromTOCLabel.setText("Declara\u00e7\u00e3o Entregue por TOC:");
        this.TOCInnerPanel.add((Component)this.fromTOCLabel, cc.xy(1, 1));
        this.fromTOC.setContentAreaFilled(false);
        this.TOCInnerPanel.add((Component)this.fromTOC, cc.xy(3, 1));
        this.tocAutorizadoSujALabel.setText("TOC Autorizado Sujeito Passivo A:");
        this.TOCInnerPanel.add((Component)this.tocAutorizadoSujALabel, cc.xy(1, 3));
        this.tocAutorizadoSujA.setContentAreaFilled(false);
        this.TOCInnerPanel.add((Component)this.tocAutorizadoSujA, cc.xy(3, 3));
        this.tocAutorizadoSujBLabel.setText("TOC Autorizado Sujeito Passivo B:");
        this.TOCInnerPanel.add((Component)this.tocAutorizadoSujBLabel, cc.xy(1, 5));
        this.tocAutorizadoSujB.setContentAreaFilled(false);
        this.TOCInnerPanel.add((Component)this.tocAutorizadoSujB, cc.xy(3, 5));
        this.nifTOCLabel.setText("NIF do TOC:");
        this.TOCInnerPanel.add((Component)this.nifTOCLabel, cc.xy(1, 7));
        this.TOCInnerPanel.add((Component)this.nifTOC, cc.xy(3, 7));
        this.passTOCLabel.setText("Senha do TOC:");
        this.TOCInnerPanel.add((Component)this.passTOCLabel, cc.xy(1, 9));
        this.TOCInnerPanel.add((Component)this.passTOC, cc.xy(3, 9));
        this.TOCPanel.add((Component)this.TOCInnerPanel, cc.xywh(3, 4, 1, 1, CellConstraints.DEFAULT, CellConstraints.TOP));
        this.add((Component)this.TOCPanel, cc.xywh(3, 9, 5, 1, CellConstraints.DEFAULT, CellConstraints.TOP));
        this.panelCheckWarnings.setOpaque(false);
        this.panelCheckWarnings.setBorder(new TitledBorder("Alertas"));
        this.panelCheckWarnings.setLayout(new FormLayout("default", "35dlu:grow"));
        this.checkWarnings.setText("<html>Declaro que tomei conhecimento dos alertas gerados pelos dados desta declara\u00e7\u00e3o e desejo continuar sem alterar os elementos declarados.</html>");
        this.checkWarnings.setVerticalAlignment(1);
        this.checkWarnings.setContentAreaFilled(false);
        this.checkWarnings.setVerticalTextPosition(1);
        this.panelCheckWarnings.add((Component)this.checkWarnings, cc.xywh(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.TOP));
        this.add((Component)this.panelCheckWarnings, cc.xywh(3, 11, 5, 1, CellConstraints.DEFAULT, CellConstraints.TOP));
    }

}

