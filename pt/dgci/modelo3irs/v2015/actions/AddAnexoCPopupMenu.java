/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import pt.dgci.modelo3irs.v2015.actions.AddAnexoHerancaIndivisaPopupMenu;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class AddAnexoCPopupMenu
extends AddAnexoHerancaIndivisaPopupMenu {
    private static final long serialVersionUID = -2415792547865038761L;

    public AddAnexoCPopupMenu(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model, question);
    }

    @Override
    protected void postAddAnexo(DeclaracaoModel model, String subID, boolean isHerancaIndivisa) {
        AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(new FormKey(AnexoCModel.class, subID));
        if (anexoCModel != null) {
            anexoCModel.getQuadro03().setAnexoCq03B1(isHerancaIndivisa ? "1" : "2");
        }
    }
}

