/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions.search;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.util.IRenderableAction;
import pt.opensoft.taxclient.ui.help.impl.DefaultHelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.multi.impl.DefaultMultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public class SearchAction
extends EnhancedAction
implements IRenderableAction,
FocusListener {
    private static final long serialVersionUID = 8821828010757485544L;
    private static final String PATH_ROSTO = "/ajuda/Rosto/inputQuadroInicio.xml";
    private static final String PATH_ROSTO_Q01 = "/ajuda/Rosto/inputQuadro01.xml";
    private static final String PATH_ROSTO_Q02 = "/ajuda/Rosto/inputQuadro02.xml";
    private static final String PATH_ROSTO_Q03 = "/ajuda/Rosto/inputQuadro03.xml";
    private static final String PATH_ROSTO_Q04 = "/ajuda/Rosto/inputQuadro04.xml";
    private static final String PATH_ROSTO_Q05 = "/ajuda/Rosto/inputQuadro05.xml";
    private static final String PATH_ROSTO_Q06 = "/ajuda/Rosto/inputQuadro06.xml";
    private static final String PATH_ROSTO_Q07 = "/ajuda/Rosto/inputQuadro07.xml";
    private static final String PATH_ANEXO_A_Q03 = "/ajuda/AnexoA/inputQuadro03.xml";
    private static final String PATH_ANEXO_A_Q04 = "/ajuda/AnexoA/inputQuadro04.xml";
    private static final String PATH_ANEXO_A_Q05 = "/ajuda/AnexoA/inputQuadro05.xml";
    private static final String PATH_ANEXO_H_Q04 = "/ajuda/AnexoH/inputQuadro04.xml";
    private static final String PATH_ANEXO_H_Q05 = "/ajuda/AnexoH/inputQuadro05.xml";
    private static final String PATH_ANEXO_H_Q06 = "/ajuda/AnexoH/inputQuadro06.xml";
    private static final String PATH_ANEXO_H_Q07 = "/ajuda/AnexoH/inputQuadro07.xml";
    private static final String PATH_ANEXO_H_Q08 = "/ajuda/AnexoH/inputQuadro08.xml";
    private static final String PATH_ANEXO_H_Q09 = "/ajuda/AnexoH/inputQuadro09.xml";
    private static final String PATH_ANEXO_H_Q10 = "/ajuda/AnexoH/inputQuadro10.xml";
    private static final String PATH_ANEXO_J_Q03 = "/ajuda/AnexoJ/inputQuadro03.xml";
    private static final String PATH_ANEXO_J_Q04 = "/ajuda/AnexoJ/inputQuadro04.xml";
    private static final String PATH_ANEXO_J_Q05 = "/ajuda/AnexoJ/inputQuadro05.xml";
    private static final String PATH_ANEXO_J_Q06 = "/ajuda/AnexoJ/inputQuadro06.xml";
    private static final String PATH_ANEXO_J_Q07 = "/ajuda/AnexoJ/inputQuadro07.xml";
    private static final String PATH_ANEXO_J_Q08 = "/ajuda/AnexoJ/inputQuadro08.xml";
    private String PATH_STOP_WORDS = "/StopWords.txt";
    protected static final String DEFAULT_SEARCH_TEXT = "pesquisar";
    protected JTextField searchTextField;
    private List<String> listToSearch;

    public SearchAction() {
        this("Pesquisa");
    }

    protected SearchAction(String name) {
        super(name, new JTextField(), new JButton());
        this.initListToSearch();
        this._button.addActionListener(this);
        this._input.addFocusListener(this);
        this._input.setText("pesquisar");
        this._input.registerKeyboardAction(this, KeyStroke.getKeyStroke(10, 0), 0);
        this._button.setOpaque(false);
        this._iconBig = IconFactory.getIconBig(GUIParameters.ICON_PESQUISA_AJUDA);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        this._text = super.get_input().getText();
        DefaultMultiPurposeDisplayer defaultMultiPurposeDisplayer = (DefaultMultiPurposeDisplayer)Session.getMainFrame().getMultiPurposeDisplayer();
        DefaultHelpDisplayer searchDisplayer = (DefaultHelpDisplayer)defaultMultiPurposeDisplayer.getGenericCard("Pesquisar");
        try {
            searchDisplayer.showSearchHelp(this.listToSearch, this.PATH_STOP_WORDS, this._text);
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
        defaultMultiPurposeDisplayer.showGenericCard("Pesquisar");
    }

    private void initListToSearch() {
        this.listToSearch = new ArrayList<String>();
        this.listToSearch.add("/ajuda/Rosto/inputQuadroInicio.xml");
        this.listToSearch.add("/ajuda/Rosto/inputQuadro01.xml");
        this.listToSearch.add("/ajuda/Rosto/inputQuadro02.xml");
        this.listToSearch.add("/ajuda/Rosto/inputQuadro03.xml");
        this.listToSearch.add("/ajuda/Rosto/inputQuadro04.xml");
        this.listToSearch.add("/ajuda/Rosto/inputQuadro05.xml");
        this.listToSearch.add("/ajuda/Rosto/inputQuadro06.xml");
        this.listToSearch.add("/ajuda/Rosto/inputQuadro07.xml");
        this.listToSearch.add("/ajuda/AnexoA/inputQuadro03.xml");
        this.listToSearch.add("/ajuda/AnexoA/inputQuadro04.xml");
        this.listToSearch.add("/ajuda/AnexoA/inputQuadro05.xml");
        this.listToSearch.add("/ajuda/AnexoH/inputQuadro04.xml");
        this.listToSearch.add("/ajuda/AnexoH/inputQuadro05.xml");
        this.listToSearch.add("/ajuda/AnexoH/inputQuadro06.xml");
        this.listToSearch.add("/ajuda/AnexoH/inputQuadro07.xml");
        this.listToSearch.add("/ajuda/AnexoH/inputQuadro08.xml");
        this.listToSearch.add("/ajuda/AnexoH/inputQuadro09.xml");
        this.listToSearch.add("/ajuda/AnexoH/inputQuadro10.xml");
        this.listToSearch.add("/ajuda/AnexoJ/inputQuadro03.xml");
        this.listToSearch.add("/ajuda/AnexoJ/inputQuadro04.xml");
        this.listToSearch.add("/ajuda/AnexoJ/inputQuadro05.xml");
        this.listToSearch.add("/ajuda/AnexoJ/inputQuadro06.xml");
        this.listToSearch.add("/ajuda/AnexoJ/inputQuadro07.xml");
        this.listToSearch.add("/ajuda/AnexoJ/inputQuadro08.xml");
    }

    @Override
    public void focusGained(FocusEvent arg0) {
        if (this._input.getText().equals("pesquisar")) {
            this._input.setText("");
        }
    }

    @Override
    public void focusLost(FocusEvent arg0) {
    }
}

