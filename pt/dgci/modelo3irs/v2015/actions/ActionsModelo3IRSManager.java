/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.actions;

import pt.opensoft.taxclient.actions.ActionsTaxClientManager;

public class ActionsModelo3IRSManager
extends ActionsTaxClientManager {
    public static final String SUBSTITUIR = "Substituir";
    public static final String SIMULATE_DEBUG = "SimulateDebugAction";
    public static final String WRITE_DECLARATION = "WriteDeclarationAction";
    public static final String READ_DECLARATION = "ReadDeclarationAction";
    public static final String CHANGES = "Changes";
}

