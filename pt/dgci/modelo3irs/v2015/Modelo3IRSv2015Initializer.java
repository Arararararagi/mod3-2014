/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015;

import com.jgoodies.plaf.plastic.PlasticXPLookAndFeel;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.tree.MutableTreeNode;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.actions.ObterUltimaDeclSubmetidaAction;
import pt.dgci.modelo3irs.v2015.actions.PrePreenchimentoAction;
import pt.dgci.modelo3irs.v2015.actions.SimulateAction;
import pt.dgci.modelo3irs.v2015.actions.admin.ReadDeclarationAction;
import pt.dgci.modelo3irs.v2015.actions.admin.SimulateDebugAction;
import pt.dgci.modelo3irs.v2015.actions.admin.WriteDeclarationAction;
import pt.dgci.modelo3irs.v2015.actions.declaracao.IRSSubmitAction;
import pt.dgci.modelo3irs.v2015.actions.declaracao.IRSSubstituirAction;
import pt.dgci.modelo3irs.v2015.actions.file.Modelo3IRSv2015NewFileAction;
import pt.dgci.modelo3irs.v2015.actions.file.Modelo3IRSv2015OpenFileAction;
import pt.dgci.modelo3irs.v2015.actions.file.Modelo3IRSv2015SaveFileAction;
import pt.dgci.modelo3irs.v2015.actions.help.Modelo3IRSAboutAction;
import pt.dgci.modelo3irs.v2015.actions.help.Modelo3IRSDadosVersaoAction;
import pt.dgci.modelo3irs.v2015.actions.help.Modelo3IRSv2015HelpWizardAction;
import pt.dgci.modelo3irs.v2015.actions.print.Modelo3IRSPrintAction;
import pt.dgci.modelo3irs.v2015.actions.search.SearchAction;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015GUIParameters;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.util.DeclaracaoModelInitializer;
import pt.dgci.modelo3irs.v2015.util.Modelo3IRSCommonParametersLoader;
import pt.dgci.modelo3irs.v2015.util.Modelo3IRSv2015ErrorsParameters;
import pt.dgci.modelo3irs.v2015.validator.Modelo3IRSv2015Validator;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.plaf.pf.PFLookAndFeel;
import pt.opensoft.taxclient.TaxClientRevampedInitializer;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.help.TaxClientRevampedHelpPreenchimentoAction;
import pt.opensoft.taxclient.actions.util.OpenExternalPageAction;
import pt.opensoft.taxclient.actions.util.ShowGenericPageAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoModelValidator;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.navigation.impl.tree.AnexoTreeNode;
import pt.opensoft.taxclient.ui.navigation.impl.tree.DeclaracaoTreeNode;
import pt.opensoft.taxclient.ui.navigation.impl.tree.TaxNavigationTreeModel;
import pt.opensoft.taxclient.ui.validation.ErrorValidationRenderer;

public class Modelo3IRSv2015Initializer
extends TaxClientRevampedInitializer {
    @Override
    protected void initProperties() {
        super.initProperties();
        System.setProperty("writeTabelaEmptyLines", "true");
    }

    @Override
    protected void bootstrap() {
        Modelo3IRSCommonParametersLoader.loadCatalogsAndParameters();
        this.readGUIParameters();
        ErrorValidationRenderer.setParameters(Modelo3IRSv2015ErrorsParameters.instance);
        OverwriteBehaviorManager.loadParameters();
    }

    protected void readGUIParameters() {
        Modelo3IRSv2015GUIParameters.load("taxclient.properties");
    }

    @Override
    protected MainFrame createMainFrame(DeclaracaoModel declaracaoModel, NavigationModel navigationModel) {
        return new Modelo3IRSv2015ApplicationMainFrame(declaracaoModel, navigationModel);
    }

    @Override
    protected DeclaracaoModel createDeclaracaoModel() {
        return DeclaracaoModelInitializer.init();
    }

    @Override
    protected NavigationModel createNavigationModel(DeclaracaoModel declaracaoModel) {
        DeclaracaoTreeNode root = new DeclaracaoTreeNode(this.getApplicationName());
        root.add(new AnexoTreeNode(declaracaoModel.getAnexo(RostoModel.class)));
        TaxNavigationTreeModel taxNavigationTreeModel = new TaxNavigationTreeModel(root);
        return taxNavigationTreeModel;
    }

    @Override
    protected String getApplicationName() {
        return "Modelo 3 - IRS";
    }

    @Override
    protected DeclaracaoModelValidator createDeclaracaoModelValidator() {
        return new Modelo3IRSv2015Validator();
    }

    @Override
    protected void registerDefaultTaxClientActions() {
        super.registerDefaultTaxClientActions();
        ActionsTaxClientManager.registerAction("Novo", new Modelo3IRSv2015NewFileAction());
        ActionsTaxClientManager.registerAction("Abrir", new Modelo3IRSv2015OpenFileAction());
        ActionsTaxClientManager.registerAction("AjudaPreenchimento", new TaxClientRevampedHelpPreenchimentoAction());
        ActionsTaxClientManager.registerAction("Imprimir", new Modelo3IRSPrintAction());
        IRSSubmitAction subAction = new IRSSubmitAction();
        ActionsTaxClientManager.registerAction("Submeter", subAction);
        ActionsTaxClientManager.registerAction("Substituir", new IRSSubstituirAction());
        ActionsTaxClientManager.registerAction("Wizard", new Modelo3IRSv2015HelpWizardAction());
        PrePreenchimentoAction prePreenchimentoAction = new PrePreenchimentoAction(Modelo3IRSv2015Parameters.instance().getUrlPrePreenchimento());
        ActionsTaxClientManager.registerAction("PrePreenchimento", prePreenchimentoAction);
        ObterUltimaDeclSubmetidaAction obterUltimaDeclSubmetidaAction = new ObterUltimaDeclSubmetidaAction(Modelo3IRSv2015Parameters.instance.getUrlUltimaDeclSubmetida());
        ActionsTaxClientManager.registerAction("ObterUltimaDeclSubmetida", obterUltimaDeclSubmetidaAction);
        ShowGenericPageAction ajudaTemasAction = new ShowGenericPageAction("Ajuda por Temas", GUIParameters.ICON_AJUDA_TEMAS, "Ajuda por Temas", KeyStroke.getKeyStroke(113, 0));
        ActionsTaxClientManager.registerAction("Ajuda por Temas", ajudaTemasAction);
        ActionsTaxClientManager.registerAction("Simular", new SimulateAction());
        ActionsTaxClientManager.registerAction("Pesquisar", new SearchAction());
        ActionsTaxClientManager.registerAction("AbrirPF", new OpenExternalPageAction("http://www.portaldasfinancas.gov.pt/", "", IconFactory.getIconBig(GUIParameters.ICON_AJUDA_ONLINE), "Abrir o site do Portal das Finan\u00e7as no Browser."));
        ActionsTaxClientManager.registerAction("AcercaDe", new Modelo3IRSAboutAction(Modelo3IRSv2015Parameters.instance().getAppName()));
        ActionsTaxClientManager.registerAction("Dados da Versao", new Modelo3IRSDadosVersaoAction(Modelo3IRSv2015Parameters.instance().getAppName()));
        ActionsTaxClientManager.registerAction("Gravar", new Modelo3IRSv2015SaveFileAction());
        if (Modelo3IRSv2015Parameters.instance().isAdminActionsEnabled()) {
            ActionsTaxClientManager.registerAction("WriteDeclarationAction", new WriteDeclarationAction());
            ActionsTaxClientManager.registerAction("ReadDeclarationAction", new ReadDeclarationAction());
            ActionsTaxClientManager.registerAction("SimulateDebugAction", new SimulateDebugAction());
        }
    }

    @Override
    protected void initLookAndFeel(Component component) throws UnsupportedLookAndFeelException, InterruptedException, InvocationTargetException {
        PlasticXPLookAndFeel.setHighContrastFocusColorsEnabled(true);
        PlasticXPLookAndFeel.setTabStyle("metal");
        PFLookAndFeel pfLookAndFeel = new PFLookAndFeel();
        UIManager.setLookAndFeel(pfLookAndFeel);
    }
}

