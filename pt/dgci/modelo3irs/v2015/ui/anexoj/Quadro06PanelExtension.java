/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro06Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro06PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro06PanelExtension
extends Quadro06PanelBase
implements IBindablePanel<Quadro06> {
    public static final int MAX_LINES_Q06_T1 = 63;
    private static final long serialVersionUID = 5049295077030447746L;

    public Quadro06PanelExtension(Quadro06 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro06 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro06Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoJq06T1().getTableHeader().getColumnModel();
        ColumnGroup g_IdentificacaoPais = new ColumnGroup("Identifica\u00e7\u00e3o Pa\u00eds");
        g_IdentificacaoPais.add(cm.getColumn(1));
        g_IdentificacaoPais.add(cm.getColumn(2));
        g_IdentificacaoPais.add(cm.getColumn(3));
        ColumnGroup g_NoPaisDaFonte = new ColumnGroup("No Pa\u00eds da Fonte");
        g_NoPaisDaFonte.add(cm.getColumn(6));
        ColumnGroup g_NoPaisDoAgentePagador = new ColumnGroup("No Pa\u00eds do Agente Pagador Directiva da Poupan\u00e7a N.\u00ba 2003/48/CE");
        g_NoPaisDoAgentePagador.add(cm.getColumn(7));
        g_NoPaisDoAgentePagador.add(cm.getColumn(8));
        ColumnGroup g_ImpostoPagoNoEstrangeiro = new ColumnGroup("Imposto Pago no Estrangeiro");
        g_ImpostoPagoNoEstrangeiro.add(g_NoPaisDaFonte);
        g_ImpostoPagoNoEstrangeiro.add(g_NoPaisDoAgentePagador);
        if (this.getAnexoJq06T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoJq06T1().getTableHeader()).addColumnGroup(g_IdentificacaoPais);
            ((GroupableTableHeader)this.getAnexoJq06T1().getTableHeader()).addColumnGroup(g_ImpostoPagoNoEstrangeiro);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_IdentificacaoPais);
        header.addColumnGroup(g_ImpostoPagoNoEstrangeiro);
        this.getAnexoJq06T1().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoJq06T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoJq06T1() != null && this.model.getAnexoJq06T1().size() >= 63) {
            return;
        }
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            AnexoJq06T1_Linha linha = new AnexoJq06T1_Linha();
            linha.setInstalacaoFixa("N");
            this.model.getAnexoJq06T1().add(linha);
        }
    }
}

