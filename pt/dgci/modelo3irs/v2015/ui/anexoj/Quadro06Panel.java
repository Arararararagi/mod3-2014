/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoJq06T1Scroll;
    protected JTable anexoJq06T1;
    protected JPanel panel3;
    protected JLabel label1;
    protected JLabel anexoJq06C1_label;
    protected JPanel panel5;
    protected JLabel anexoJq06C2_label;
    protected JPanel panel4;
    protected JLabel anexoJq06C3_label;
    protected JLabel anexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label;
    protected JMoneyTextField anexoJq06C1;
    protected JMoneyTextField anexoJq06C2;
    protected JMoneyTextField anexoJq06C3;
    protected JPanel panel1;
    protected JLabel anexoJq06C4OP1_base_label;
    protected JLabelTextFieldNumbering label3;
    protected JRadioButton anexoJq06C4OP1;
    protected JLabelTextFieldNumbering label4;
    protected JRadioButton anexoJq06C4OP2;

    public Quadro06Panel() {
        this.initComponents();
    }

    protected void addLineAnexoJq06T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoJq06T1Scroll() {
        return this.anexoJq06T1Scroll;
    }

    public JTable getAnexoJq06T1() {
        return this.anexoJq06T1;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label() {
        return this.anexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label;
    }

    public JMoneyTextField getAnexoJq06C1() {
        return this.anexoJq06C1;
    }

    public JMoneyTextField getAnexoJq06C2() {
        return this.anexoJq06C2;
    }

    public JMoneyTextField getAnexoJq06C3() {
        return this.anexoJq06C3;
    }

    protected void removeLineAnexoJq06T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JLabel getAnexoJq06C1_label() {
        return this.anexoJq06C1_label;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoJq06C3_label() {
        return this.anexoJq06C3_label;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getAnexoJq06C2_label() {
        return this.anexoJq06C2_label;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getAnexoJq06C4OP1_base_label() {
        return this.anexoJq06C4OP1_base_label;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JRadioButton getAnexoJq06C4OP1() {
        return this.anexoJq06C4OP1;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JRadioButton getAnexoJq06C4OP2() {
        return this.anexoJq06C4OP2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoJ");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoJq06T1Scroll = new JScrollPane();
        this.anexoJq06T1 = new JTable();
        this.panel3 = new JPanel();
        this.label1 = new JLabel();
        this.anexoJq06C1_label = new JLabel();
        this.panel5 = new JPanel();
        this.anexoJq06C2_label = new JLabel();
        this.panel4 = new JPanel();
        this.anexoJq06C3_label = new JLabel();
        this.anexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label = new JLabel();
        this.anexoJq06C1 = new JMoneyTextField();
        this.anexoJq06C2 = new JMoneyTextField();
        this.anexoJq06C3 = new JMoneyTextField();
        this.panel1 = new JPanel();
        this.anexoJq06C4OP1_base_label = new JLabel();
        this.label3 = new JLabelTextFieldNumbering();
        this.anexoJq06C4OP1 = new JRadioButton();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoJq06C4OP2 = new JRadioButton();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 4*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $rgap, 600dlu, $lcgap, default:grow", "$rgap, default, $lgap, 192dlu"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro06Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro06Panel.this.addLineAnexoJq06T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro06Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro06Panel.this.removeLineAnexoJq06T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel2.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoJq06T1Scroll.setViewportView(this.anexoJq06T1);
        this.panel2.add((Component)this.anexoJq06T1Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, 77dlu, $ugap, 100dlu, $rgap, 2*(0px), 95dlu, 0px, $rgap, 110dlu, $lcgap, default:grow, $rgap", "2*($lgap, default), $glue, default, $lgap, default"));
        this.label1.setText(bundle.getString("Quadro06Panel.label1.text"));
        this.label1.setHorizontalAlignment(0);
        this.label1.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label1, cc.xywh(9, 4, 4, 1));
        this.anexoJq06C1_label.setText(bundle.getString("Quadro06Panel.anexoJq06C1_label.text"));
        this.anexoJq06C1_label.setBorder(new EtchedBorder());
        this.anexoJq06C1_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoJq06C1_label, cc.xywh(5, 4, 1, 3));
        this.panel5.setBorder(new EtchedBorder());
        this.panel5.setLayout(new FormLayout("100dlu", "default"));
        this.anexoJq06C2_label.setText(bundle.getString("Quadro06Panel.anexoJq06C2_label.text"));
        this.anexoJq06C2_label.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoJq06C2_label, cc.xy(1, 1));
        this.panel3.add((Component)this.panel5, cc.xy(9, 6));
        this.panel4.setBorder(new EtchedBorder());
        this.panel4.setLayout(new FormLayout("110dlu", "default"));
        this.anexoJq06C3_label.setText(bundle.getString("Quadro06Panel.anexoJq06C3_label.text"));
        this.anexoJq06C3_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoJq06C3_label, cc.xy(1, 1));
        this.panel3.add((Component)this.panel4, cc.xy(12, 6));
        this.anexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label.setText(bundle.getString("Quadro06Panel.anexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label.text"));
        this.anexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label.setHorizontalAlignment(4);
        this.panel3.add((Component)this.anexoJq06C1_base_label\u00a3anexoJq06C2_base_label\u00a3anexoJq06C3_base_label, cc.xy(3, 8));
        this.anexoJq06C1.setEditable(false);
        this.anexoJq06C1.setColumns(22);
        this.anexoJq06C1.setAllowNegative(true);
        this.panel3.add((Component)this.anexoJq06C1, cc.xy(5, 8));
        this.anexoJq06C2.setEditable(false);
        this.anexoJq06C2.setColumns(15);
        this.panel3.add((Component)this.anexoJq06C2, cc.xy(9, 8));
        this.anexoJq06C3.setEditable(false);
        this.anexoJq06C3.setColumns(15);
        this.panel3.add((Component)this.anexoJq06C3, cc.xy(12, 8));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.panel1.setLayout(new FormLayout("169dlu, $lcgap, 175dlu, 13dlu, 2*($lcgap), 175dlu, $lcgap, 123dlu", "2*(default, $lgap), default"));
        this.anexoJq06C4OP1_base_label.setText(bundle.getString("Quadro06Panel.anexoJq06C4OP1_base_label.text"));
        this.panel1.add((Component)this.anexoJq06C4OP1_base_label, cc.xywh(1, 1, 3, 1));
        this.label3.setText(bundle.getString("Quadro06Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.label3, cc.xy(4, 3));
        this.anexoJq06C4OP1.setText(bundle.getString("Quadro06Panel.anexoJq06C4OP1.text"));
        this.panel1.add((Component)this.anexoJq06C4OP1, cc.xywh(7, 3, 3, 1));
        this.label4.setText(bundle.getString("Quadro06Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.label4, cc.xy(4, 5));
        this.anexoJq06C4OP2.setText(bundle.getString("Quadro06Panel.anexoJq06C4OP2.text"));
        this.panel1.add((Component)this.anexoJq06C4OP2, cc.xy(7, 5));
        this.this2.add((Component)this.panel1, cc.xy(2, 8));
        this.add((Component)this.this2, "Center");
    }

}

