/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoJq05T1Scroll;
    protected JTable anexoJq05T1;
    protected JLabel label11;
    protected JPanel panel6;
    protected JToolBar toolBar3;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoJq05T2Scroll;
    protected JTable anexoJq05T2;

    public Quadro05Panel() {
        this.initComponents();
    }

    protected void addLineAnexoJq05T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoJq05T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoJq05T1Scroll() {
        return this.anexoJq05T1Scroll;
    }

    public JTable getAnexoJq05T1() {
        return this.anexoJq05T1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    protected void addLineAnexoJq05T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoJq05T2_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JToolBar getToolBar3() {
        return this.toolBar3;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoJq05T2Scroll() {
        return this.anexoJq05T2Scroll;
    }

    public JTable getAnexoJq05T2() {
        return this.anexoJq05T2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoJ");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoJq05T1Scroll = new JScrollPane();
        this.anexoJq05T1 = new JTable();
        this.label11 = new JLabel();
        this.panel6 = new JPanel();
        this.toolBar3 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoJq05T2Scroll = new JScrollPane();
        this.anexoJq05T2 = new JTable();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 4*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $rgap, 160dlu, $lcgap, default:grow", "$ugap, $lgap, default, $lgap, 165dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro05Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.addLineAnexoJq05T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro05Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.removeLineAnexoJq05T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel2.add((Component)this.toolBar1, CC.xy(3, 3));
        this.anexoJq05T1Scroll.setViewportView(this.anexoJq05T1);
        this.panel2.add((Component)this.anexoJq05T1Scroll, CC.xywh(3, 5, 5, 1));
        this.this2.add((Component)this.panel2, CC.xy(2, 2));
        this.label11.setText(bundle.getString("Quadro05Panel.label11.text"));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.this2.add((Component)this.label11, CC.xy(2, 4));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, default:grow, 2*($lcgap, default), $lcgap, 250dlu:grow, $lcgap, default:grow, $lcgap, $rgap", "default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar3.setFloatable(false);
        this.toolBar3.setRollover(true);
        this.button3.setText(bundle.getString("Quadro05Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.addLineAnexoJq05T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button3);
        this.button4.setText(bundle.getString("Quadro05Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.removeLineAnexoJq05T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button4);
        this.panel6.add((Component)this.toolBar3, CC.xy(4, 1));
        this.anexoJq05T2Scroll.setViewportView(this.anexoJq05T2);
        this.panel6.add((Component)this.anexoJq05T2Scroll, CC.xywh(4, 3, 5, 1));
        this.this2.add((Component)this.panel6, CC.xy(2, 6));
        this.add((Component)this.this2, "Center");
    }

}

