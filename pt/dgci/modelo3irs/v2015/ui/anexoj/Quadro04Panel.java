/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label20;
    protected JLabel label2;
    protected JLabel anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label;
    protected JLabel anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label;
    protected JLabel anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label;
    protected JLabel anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label;
    protected JLabelTextFieldNumbering anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num;
    protected JLabel anexoJq04C401a_base_label\u00a3anexoJq04C401b_base_label\u00a3anexoJq04C401c_base_label\u00a3anexoJq04C401d_base_label;
    protected JMoneyTextField anexoJq04C401a;
    protected JMoneyTextField anexoJq04C401b;
    protected JMoneyTextField anexoJq04C401c;
    protected JMoneyTextField anexoJq04C401d;
    protected JLabelTextFieldNumbering anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num;
    protected JLabel anexoJq04C402a_base_label\u00a3anexoJq04C402b_base_label\u00a3anexoJq04C402c_base_label\u00a3anexoJq04C402d_base_label;
    protected JMoneyTextField anexoJq04C402a;
    protected JMoneyTextField anexoJq04C402b;
    protected JMoneyTextField anexoJq04C402c;
    protected JMoneyTextField anexoJq04C402d;
    protected JLabelTextFieldNumbering anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num;
    protected JLabel anexoJq04C416a_base_label\u00a3anexoJq04C416b_base_label\u00a3anexoJq04C416c_base_label\u00a3anexoJq04C416d_base_label;
    protected JMoneyTextField anexoJq04C416a;
    protected JMoneyTextField anexoJq04C416b;
    protected JMoneyTextField anexoJq04C416c;
    protected JMoneyTextField anexoJq04C416d;
    protected JLabelTextFieldNumbering anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num;
    protected JLabel anexoJq04C417a_base_label\u00a3anexoJq04C417b_base_label\u00a3anexoJq04C417c_base_label\u00a3anexoJq04C417d_base_label;
    protected JMoneyTextField anexoJq04C417a;
    protected JMoneyTextField anexoJq04C417b;
    protected JMoneyTextField anexoJq04C417c;
    protected JMoneyTextField anexoJq04C417d;
    protected JLabelTextFieldNumbering anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num;
    protected JLabel anexoJq04C419b_base_label\u00a3anexoJq04C419c_base_label\u00a3anexoJq04C419d_base_label;
    protected JMoneyTextField anexoJq04C419b;
    protected JMoneyTextField anexoJq04C419c;
    protected JMoneyTextField anexoJq04C419d;
    protected JLabel label21;
    protected JLabelTextFieldNumbering anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num;
    protected JLabel anexoJq04C403b_base_label\u00a3anexoJq04C403c_base_label\u00a3anexoJq04C403d_base_label;
    protected JMoneyTextField anexoJq04C403b;
    protected JMoneyTextField anexoJq04C403c;
    protected JMoneyTextField anexoJq04C403d;
    protected JLabelTextFieldNumbering anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num;
    protected JLabel anexoJq04C404b_base_label\u00a3anexoJq04C404c_base_label\u00a3anexoJq04C404d_base_label;
    protected JMoneyTextField anexoJq04C404b;
    protected JMoneyTextField anexoJq04C404c;
    protected JMoneyTextField anexoJq04C404d;
    protected JLabelTextFieldNumbering anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num;
    protected JLabel anexoJq04C405b_base_label\u00a3anexoJq04C405c_base_label\u00a3anexoJq04C405d_base_label;
    protected JMoneyTextField anexoJq04C405b;
    protected JMoneyTextField anexoJq04C405c;
    protected JMoneyTextField anexoJq04C405d;
    protected JLabelTextFieldNumbering anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num;
    protected JLabel anexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label;
    protected JMoneyTextField anexoJq04C406c;
    protected JMoneyTextField anexoJq04C406d;
    protected JLabelTextFieldNumbering anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num;
    protected JLabel anexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label;
    protected JMoneyTextField anexoJq04C407b;
    protected JMoneyTextField anexoJq04C407c;
    protected JMoneyTextField anexoJq04C407d;
    protected JLabelTextFieldNumbering anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num;
    protected JLabel anexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label;
    protected JMoneyTextField anexoJq04C420b;
    protected JMoneyTextField anexoJq04C420c;
    protected JMoneyTextField anexoJq04C420d;
    protected JLabelTextFieldNumbering anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num;
    protected JLabel anexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label;
    protected JMoneyTextField anexoJq04C418b;
    protected JMoneyTextField anexoJq04C418c;
    protected JMoneyTextField anexoJq04C418d;
    protected JLabelTextFieldNumbering anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num;
    protected JLabel anexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label;
    protected JMoneyTextField anexoJq04C422b;
    protected JMoneyTextField anexoJq04C422c;
    protected JMoneyTextField anexoJq04C422d;
    protected JLabelTextFieldNumbering anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num;
    protected JLabel anexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label;
    protected JMoneyTextField anexoJq04C408b;
    protected JMoneyTextField anexoJq04C408c;
    protected JMoneyTextField anexoJq04C408d;
    protected JLabelTextFieldNumbering anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num;
    protected JLabel anexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label;
    protected JMoneyTextField anexoJq04C423b;
    protected JMoneyTextField anexoJq04C423c;
    protected JMoneyTextField anexoJq04C423d;
    protected JLabelTextFieldNumbering anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num;
    protected JLabel anexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label;
    protected JMoneyTextField anexoJq04C410b;
    protected JMoneyTextField anexoJq04C410c;
    protected JMoneyTextField anexoJq04C410d;
    protected JLabelTextFieldNumbering anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num;
    protected JLabel anexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label;
    protected JMoneyTextField anexoJq04C424b;
    protected JMoneyTextField anexoJq04C424c;
    protected JMoneyTextField anexoJq04C424d;
    protected JLabelTextFieldNumbering anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num;
    protected JLabel anexoJq04C409b_base_label\u00a3anexoJq04C409c_base_label\u00a3anexoJq04C409d_base_label;
    protected JMoneyTextField anexoJq04C409b;
    protected JMoneyTextField anexoJq04C409c;
    protected JMoneyTextField anexoJq04C409d;
    protected JLabelTextFieldNumbering anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num;
    protected JLabel anexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label;
    protected JMoneyTextField anexoJq04C411b;
    protected JMoneyTextField anexoJq04C411c;
    protected JMoneyTextField anexoJq04C411d;
    protected JLabel label23;
    protected JLabelTextFieldNumbering anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num;
    protected JLabel anexoJq04C412b_base_label\u00a3anexoJq04C412c_base_label\u00a3anexoJq04C412d_base_label;
    protected JMoneyTextField anexoJq04C412b;
    protected JMoneyTextField anexoJq04C412c;
    protected JMoneyTextField anexoJq04C412d;
    protected JLabelTextFieldNumbering anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num;
    protected JLabel anexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label;
    protected JMoneyTextField anexoJq04C425b;
    protected JMoneyTextField anexoJq04C425c;
    protected JMoneyTextField anexoJq04C425d;
    protected JLabelTextFieldNumbering anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num;
    protected JLabel anexoJq04C415b_base_label\u00a3anexoJq04C415c_base_label\u00a3anexoJq04C415d_base_label;
    protected JMoneyTextField anexoJq04C415b;
    protected JMoneyTextField anexoJq04C415c;
    protected JMoneyTextField anexoJq04C415d;
    protected JLabel anexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label;
    protected JMoneyTextField anexoJq04C1b;
    protected JMoneyTextField anexoJq04C1c;
    protected JMoneyTextField anexoJq04C1d;
    protected JLabelTextFieldNumbering anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num;
    protected JLabel label22;
    protected JMoneyTextField anexoJq04C406b;
    protected JLabel anexoJq04C426b_base_label\u00a3anexoJq04C426c_base_label\u00a3anexoJq04C426d_base_label;
    protected JMoneyTextField anexoJq04C426b;
    protected JMoneyTextField anexoJq04C426c;
    protected JMoneyTextField anexoJq04C426d;
    protected JPanel panel3;
    protected JLabelTextFieldNumbering anexoJq04C421_num;
    protected JLabel anexoJq04C421_label;
    protected JMoneyTextField anexoJq04C421;
    protected JPanel panel4;
    protected JLabel anexoJq04B1Sim_base_label;
    protected JLabelTextFieldNumbering label51;
    protected JRadioButton anexoJq04B1Sim;
    protected JLabelTextFieldNumbering label52;
    protected JRadioButton anexoJq04B1Nao;
    protected JLabel label53;
    protected JPanel panel5;
    protected JLabel label1;
    protected JLabel label3;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoJq04T41Scroll;
    protected JTable anexoJq04T1;
    protected JPanel panel6;
    protected JLabel label4;
    protected JLabel label5;
    protected JToolBar toolBar3;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoJq04T2Scroll;
    protected JTable anexoJq04T2;

    public Quadro04Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getAnexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label() {
        return this.anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label;
    }

    public JLabel getAnexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label() {
        return this.anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label;
    }

    public JLabel getAnexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label() {
        return this.anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label;
    }

    public JLabel getAnexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label() {
        return this.anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label;
    }

    public JLabelTextFieldNumbering getAnexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num() {
        return this.anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num;
    }

    public JLabel getAnexoJq04C401a_base_label\u00a3anexoJq04C401b_base_label\u00a3anexoJq04C401c_base_label\u00a3anexoJq04C401d_base_label() {
        return this.anexoJq04C401a_base_label\u00a3anexoJq04C401b_base_label\u00a3anexoJq04C401c_base_label\u00a3anexoJq04C401d_base_label;
    }

    public JMoneyTextField getAnexoJq04C401a() {
        return this.anexoJq04C401a;
    }

    public JMoneyTextField getAnexoJq04C401b() {
        return this.anexoJq04C401b;
    }

    public JMoneyTextField getAnexoJq04C401c() {
        return this.anexoJq04C401c;
    }

    public JMoneyTextField getAnexoJq04C401d() {
        return this.anexoJq04C401d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num() {
        return this.anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num;
    }

    public JLabel getAnexoJq04C402a_base_label\u00a3anexoJq04C402b_base_label\u00a3anexoJq04C402c_base_label\u00a3anexoJq04C402d_base_label() {
        return this.anexoJq04C402a_base_label\u00a3anexoJq04C402b_base_label\u00a3anexoJq04C402c_base_label\u00a3anexoJq04C402d_base_label;
    }

    public JMoneyTextField getAnexoJq04C402a() {
        return this.anexoJq04C402a;
    }

    public JMoneyTextField getAnexoJq04C402b() {
        return this.anexoJq04C402b;
    }

    public JMoneyTextField getAnexoJq04C402c() {
        return this.anexoJq04C402c;
    }

    public JMoneyTextField getAnexoJq04C402d() {
        return this.anexoJq04C402d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num() {
        return this.anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num;
    }

    public JLabel getAnexoJq04C416a_base_label\u00a3anexoJq04C416b_base_label\u00a3anexoJq04C416c_base_label\u00a3anexoJq04C416d_base_label() {
        return this.anexoJq04C416a_base_label\u00a3anexoJq04C416b_base_label\u00a3anexoJq04C416c_base_label\u00a3anexoJq04C416d_base_label;
    }

    public JMoneyTextField getAnexoJq04C416a() {
        return this.anexoJq04C416a;
    }

    public JMoneyTextField getAnexoJq04C416b() {
        return this.anexoJq04C416b;
    }

    public JMoneyTextField getAnexoJq04C416c() {
        return this.anexoJq04C416c;
    }

    public JMoneyTextField getAnexoJq04C416d() {
        return this.anexoJq04C416d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num() {
        return this.anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num;
    }

    public JLabel getAnexoJq04C417a_base_label\u00a3anexoJq04C417b_base_label\u00a3anexoJq04C417c_base_label\u00a3anexoJq04C417d_base_label() {
        return this.anexoJq04C417a_base_label\u00a3anexoJq04C417b_base_label\u00a3anexoJq04C417c_base_label\u00a3anexoJq04C417d_base_label;
    }

    public JMoneyTextField getAnexoJq04C417a() {
        return this.anexoJq04C417a;
    }

    public JMoneyTextField getAnexoJq04C417b() {
        return this.anexoJq04C417b;
    }

    public JMoneyTextField getAnexoJq04C417c() {
        return this.anexoJq04C417c;
    }

    public JMoneyTextField getAnexoJq04C417d() {
        return this.anexoJq04C417d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num() {
        return this.anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num;
    }

    public JLabel getAnexoJq04C403b_base_label\u00a3anexoJq04C403c_base_label\u00a3anexoJq04C403d_base_label() {
        return this.anexoJq04C403b_base_label\u00a3anexoJq04C403c_base_label\u00a3anexoJq04C403d_base_label;
    }

    public JMoneyTextField getAnexoJq04C403b() {
        return this.anexoJq04C403b;
    }

    public JMoneyTextField getAnexoJq04C403c() {
        return this.anexoJq04C403c;
    }

    public JMoneyTextField getAnexoJq04C403d() {
        return this.anexoJq04C403d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num() {
        return this.anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num;
    }

    public JLabel getAnexoJq04C404b_base_label\u00a3anexoJq04C404c_base_label\u00a3anexoJq04C404d_base_label() {
        return this.anexoJq04C404b_base_label\u00a3anexoJq04C404c_base_label\u00a3anexoJq04C404d_base_label;
    }

    public JMoneyTextField getAnexoJq04C404b() {
        return this.anexoJq04C404b;
    }

    public JMoneyTextField getAnexoJq04C404c() {
        return this.anexoJq04C404c;
    }

    public JMoneyTextField getAnexoJq04C404d() {
        return this.anexoJq04C404d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num() {
        return this.anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num;
    }

    public JLabel getAnexoJq04C405b_base_label\u00a3anexoJq04C405c_base_label\u00a3anexoJq04C405d_base_label() {
        return this.anexoJq04C405b_base_label\u00a3anexoJq04C405c_base_label\u00a3anexoJq04C405d_base_label;
    }

    public JMoneyTextField getAnexoJq04C405b() {
        return this.anexoJq04C405b;
    }

    public JMoneyTextField getAnexoJq04C405c() {
        return this.anexoJq04C405c;
    }

    public JMoneyTextField getAnexoJq04C405d() {
        return this.anexoJq04C405d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num() {
        return this.anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num;
    }

    public JLabel getAnexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label() {
        return this.anexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label;
    }

    public JMoneyTextField getAnexoJq04C426b() {
        return this.anexoJq04C426b;
    }

    public JMoneyTextField getAnexoJq04C406c() {
        return this.anexoJq04C406c;
    }

    public JMoneyTextField getAnexoJq04C406d() {
        return this.anexoJq04C406d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num() {
        return this.anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num;
    }

    public JLabel getAnexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label() {
        return this.anexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label;
    }

    public JMoneyTextField getAnexoJq04C407b() {
        return this.anexoJq04C407b;
    }

    public JMoneyTextField getAnexoJq04C407c() {
        return this.anexoJq04C407c;
    }

    public JMoneyTextField getAnexoJq04C407d() {
        return this.anexoJq04C407d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num() {
        return this.anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num;
    }

    public JLabel getAnexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label() {
        return this.anexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label;
    }

    public JMoneyTextField getAnexoJq04C420b() {
        return this.anexoJq04C420b;
    }

    public JMoneyTextField getAnexoJq04C420c() {
        return this.anexoJq04C420c;
    }

    public JMoneyTextField getAnexoJq04C420d() {
        return this.anexoJq04C420d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num() {
        return this.anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num;
    }

    public JLabel getAnexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label() {
        return this.anexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label;
    }

    public JMoneyTextField getAnexoJq04C408b() {
        return this.anexoJq04C408b;
    }

    public JMoneyTextField getAnexoJq04C408c() {
        return this.anexoJq04C408c;
    }

    public JMoneyTextField getAnexoJq04C408d() {
        return this.anexoJq04C408d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num() {
        return this.anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num;
    }

    public JLabel getAnexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label() {
        return this.anexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label;
    }

    public JMoneyTextField getAnexoJq04C418b() {
        return this.anexoJq04C418b;
    }

    public JMoneyTextField getAnexoJq04C418c() {
        return this.anexoJq04C418c;
    }

    public JMoneyTextField getAnexoJq04C418d() {
        return this.anexoJq04C418d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num() {
        return this.anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num;
    }

    public JLabel getAnexoJq04C409b_base_label\u00a3anexoJq04C409c_base_label\u00a3anexoJq04C409d_base_label() {
        return this.anexoJq04C409b_base_label\u00a3anexoJq04C409c_base_label\u00a3anexoJq04C409d_base_label;
    }

    public JMoneyTextField getAnexoJq04C409b() {
        return this.anexoJq04C409b;
    }

    public JMoneyTextField getAnexoJq04C409c() {
        return this.anexoJq04C409c;
    }

    public JMoneyTextField getAnexoJq04C409d() {
        return this.anexoJq04C409d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num() {
        return this.anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num;
    }

    public JLabel getAnexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label() {
        return this.anexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label;
    }

    public JMoneyTextField getAnexoJq04C410b() {
        return this.anexoJq04C410b;
    }

    public JMoneyTextField getAnexoJq04C410c() {
        return this.anexoJq04C410c;
    }

    public JMoneyTextField getAnexoJq04C410d() {
        return this.anexoJq04C410d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num() {
        return this.anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num;
    }

    public JLabel getAnexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label() {
        return this.anexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label;
    }

    public JMoneyTextField getAnexoJq04C411b() {
        return this.anexoJq04C411b;
    }

    public JMoneyTextField getAnexoJq04C411c() {
        return this.anexoJq04C411c;
    }

    public JMoneyTextField getAnexoJq04C411d() {
        return this.anexoJq04C411d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num() {
        return this.anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num;
    }

    public JLabel getAnexoJq04C412b_base_label\u00a3anexoJq04C412c_base_label\u00a3anexoJq04C412d_base_label() {
        return this.anexoJq04C412b_base_label\u00a3anexoJq04C412c_base_label\u00a3anexoJq04C412d_base_label;
    }

    public JMoneyTextField getAnexoJq04C412b() {
        return this.anexoJq04C412b;
    }

    public JMoneyTextField getAnexoJq04C412c() {
        return this.anexoJq04C412c;
    }

    public JMoneyTextField getAnexoJq04C412d() {
        return this.anexoJq04C412d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num() {
        return this.anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num;
    }

    public JLabel getAnexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label() {
        return this.anexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label;
    }

    public JMoneyTextField getAnexoJq04C425b() {
        return this.anexoJq04C425b;
    }

    public JMoneyTextField getAnexoJq04C425c() {
        return this.anexoJq04C425c;
    }

    public JMoneyTextField getAnexoJq04C425d() {
        return this.anexoJq04C425d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num() {
        return this.anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num;
    }

    public JLabel getAnexoJq04C415b_base_label\u00a3anexoJq04C415c_base_label\u00a3anexoJq04C415d_base_label() {
        return this.anexoJq04C415b_base_label\u00a3anexoJq04C415c_base_label\u00a3anexoJq04C415d_base_label;
    }

    public JMoneyTextField getAnexoJq04C415b() {
        return this.anexoJq04C415b;
    }

    public JMoneyTextField getAnexoJq04C415c() {
        return this.anexoJq04C415c;
    }

    public JMoneyTextField getAnexoJq04C415d() {
        return this.anexoJq04C415d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num() {
        return this.anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num;
    }

    public JLabel getAnexoJq04C419b_base_label\u00a3anexoJq04C419c_base_label\u00a3anexoJq04C419d_base_label() {
        return this.anexoJq04C419b_base_label\u00a3anexoJq04C419c_base_label\u00a3anexoJq04C419d_base_label;
    }

    public JMoneyTextField getAnexoJq04C419b() {
        return this.anexoJq04C419b;
    }

    public JMoneyTextField getAnexoJq04C419c() {
        return this.anexoJq04C419c;
    }

    public JMoneyTextField getAnexoJq04C419d() {
        return this.anexoJq04C419d;
    }

    public JLabel getAnexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label() {
        return this.anexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label;
    }

    public JMoneyTextField getAnexoJq04C1b() {
        return this.anexoJq04C1b;
    }

    public JMoneyTextField getAnexoJq04C1c() {
        return this.anexoJq04C1c;
    }

    public JMoneyTextField getAnexoJq04C1d() {
        return this.anexoJq04C1d;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabelTextFieldNumbering getAnexoJq04C421_num() {
        return this.anexoJq04C421_num;
    }

    public JLabel getAnexoJq04C421_label() {
        return this.anexoJq04C421_label;
    }

    public JMoneyTextField getAnexoJq04C421() {
        return this.anexoJq04C421;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoJq04B1Sim_base_label() {
        return this.anexoJq04B1Sim_base_label;
    }

    public JLabelTextFieldNumbering getLabel51() {
        return this.label51;
    }

    public JRadioButton getAnexoJq04B1Sim() {
        return this.anexoJq04B1Sim;
    }

    public JLabelTextFieldNumbering getLabel52() {
        return this.label52;
    }

    public JRadioButton getAnexoJq04B1Nao() {
        return this.anexoJq04B1Nao;
    }

    public JLabel getLabel53() {
        return this.label53;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JLabel getLabel20() {
        return this.label20;
    }

    public JLabel getLabel21() {
        return this.label21;
    }

    public JLabel getLabel22() {
        return this.label22;
    }

    public JLabelTextFieldNumbering getAnexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num() {
        return this.anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num;
    }

    public JLabel getAnexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label() {
        return this.anexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label;
    }

    public JMoneyTextField getAnexoJq04C422b() {
        return this.anexoJq04C422b;
    }

    public JMoneyTextField getAnexoJq04C422c() {
        return this.anexoJq04C422c;
    }

    public JMoneyTextField getAnexoJq04C422d() {
        return this.anexoJq04C422d;
    }

    public JLabelTextFieldNumbering getAnexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num() {
        return this.anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num;
    }

    public JLabel getAnexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label() {
        return this.anexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label;
    }

    public JMoneyTextField getAnexoJq04C423b() {
        return this.anexoJq04C423b;
    }

    public JMoneyTextField getAnexoJq04C423c() {
        return this.anexoJq04C423c;
    }

    public JMoneyTextField getAnexoJq04C423d() {
        return this.anexoJq04C423d;
    }

    public JLabel getLabel23() {
        return this.label23;
    }

    public JLabelTextFieldNumbering getAnexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num() {
        return this.anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num;
    }

    public JLabel getAnexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label() {
        return this.anexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label;
    }

    public JMoneyTextField getAnexoJq04C424b() {
        return this.anexoJq04C424b;
    }

    public JMoneyTextField getAnexoJq04C424c() {
        return this.anexoJq04C424c;
    }

    public JMoneyTextField getAnexoJq04C424d() {
        return this.anexoJq04C424d;
    }

    protected void addLineAnexoJq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoJq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoJq04T41Scroll() {
        return this.anexoJq04T41Scroll;
    }

    public JTable getAnexoJq04T1() {
        return this.anexoJq04T1;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    public JLabel getLabel5() {
        return this.label5;
    }

    public JToolBar getToolBar3() {
        return this.toolBar3;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoJq04T2Scroll() {
        return this.anexoJq04T2Scroll;
    }

    public JTable getAnexoJq04T2() {
        return this.anexoJq04T2;
    }

    protected void addLineAnexoJq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoJq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JLabelTextFieldNumbering getAnexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num() {
        return this.anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num;
    }

    public JMoneyTextField getAnexoJq04C406b() {
        return this.anexoJq04C406b;
    }

    public JLabel getAnexoJq04C426b_base_label\u00a3anexoJq04C426c_base_label\u00a3anexoJq04C426d_base_label() {
        return this.anexoJq04C426b_base_label\u00a3anexoJq04C426c_base_label\u00a3anexoJq04C426d_base_label;
    }

    public JMoneyTextField getAnexoJq04C426c() {
        return this.anexoJq04C426c;
    }

    public JMoneyTextField getAnexoJq04C426d() {
        return this.anexoJq04C426d;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoJ");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label20 = new JLabel();
        this.label2 = new JLabel();
        this.anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label = new JLabel();
        this.anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label = new JLabel();
        this.anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label = new JLabel();
        this.anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label = new JLabel();
        this.anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C401a_base_label\u00a3anexoJq04C401b_base_label\u00a3anexoJq04C401c_base_label\u00a3anexoJq04C401d_base_label = new JLabel();
        this.anexoJq04C401a = new JMoneyTextField();
        this.anexoJq04C401b = new JMoneyTextField();
        this.anexoJq04C401c = new JMoneyTextField();
        this.anexoJq04C401d = new JMoneyTextField();
        this.anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C402a_base_label\u00a3anexoJq04C402b_base_label\u00a3anexoJq04C402c_base_label\u00a3anexoJq04C402d_base_label = new JLabel();
        this.anexoJq04C402a = new JMoneyTextField();
        this.anexoJq04C402b = new JMoneyTextField();
        this.anexoJq04C402c = new JMoneyTextField();
        this.anexoJq04C402d = new JMoneyTextField();
        this.anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C416a_base_label\u00a3anexoJq04C416b_base_label\u00a3anexoJq04C416c_base_label\u00a3anexoJq04C416d_base_label = new JLabel();
        this.anexoJq04C416a = new JMoneyTextField();
        this.anexoJq04C416b = new JMoneyTextField();
        this.anexoJq04C416c = new JMoneyTextField();
        this.anexoJq04C416d = new JMoneyTextField();
        this.anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C417a_base_label\u00a3anexoJq04C417b_base_label\u00a3anexoJq04C417c_base_label\u00a3anexoJq04C417d_base_label = new JLabel();
        this.anexoJq04C417a = new JMoneyTextField();
        this.anexoJq04C417b = new JMoneyTextField();
        this.anexoJq04C417c = new JMoneyTextField();
        this.anexoJq04C417d = new JMoneyTextField();
        this.anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C419b_base_label\u00a3anexoJq04C419c_base_label\u00a3anexoJq04C419d_base_label = new JLabel();
        this.anexoJq04C419b = new JMoneyTextField();
        this.anexoJq04C419c = new JMoneyTextField();
        this.anexoJq04C419d = new JMoneyTextField();
        this.label21 = new JLabel();
        this.anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C403b_base_label\u00a3anexoJq04C403c_base_label\u00a3anexoJq04C403d_base_label = new JLabel();
        this.anexoJq04C403b = new JMoneyTextField();
        this.anexoJq04C403c = new JMoneyTextField();
        this.anexoJq04C403d = new JMoneyTextField();
        this.anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C404b_base_label\u00a3anexoJq04C404c_base_label\u00a3anexoJq04C404d_base_label = new JLabel();
        this.anexoJq04C404b = new JMoneyTextField();
        this.anexoJq04C404c = new JMoneyTextField();
        this.anexoJq04C404d = new JMoneyTextField();
        this.anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C405b_base_label\u00a3anexoJq04C405c_base_label\u00a3anexoJq04C405d_base_label = new JLabel();
        this.anexoJq04C405b = new JMoneyTextField();
        this.anexoJq04C405c = new JMoneyTextField();
        this.anexoJq04C405d = new JMoneyTextField();
        this.anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label = new JLabel();
        this.anexoJq04C406c = new JMoneyTextField();
        this.anexoJq04C406d = new JMoneyTextField();
        this.anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label = new JLabel();
        this.anexoJq04C407b = new JMoneyTextField();
        this.anexoJq04C407c = new JMoneyTextField();
        this.anexoJq04C407d = new JMoneyTextField();
        this.anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label = new JLabel();
        this.anexoJq04C420b = new JMoneyTextField();
        this.anexoJq04C420c = new JMoneyTextField();
        this.anexoJq04C420d = new JMoneyTextField();
        this.anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label = new JLabel();
        this.anexoJq04C418b = new JMoneyTextField();
        this.anexoJq04C418c = new JMoneyTextField();
        this.anexoJq04C418d = new JMoneyTextField();
        this.anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label = new JLabel();
        this.anexoJq04C422b = new JMoneyTextField();
        this.anexoJq04C422c = new JMoneyTextField();
        this.anexoJq04C422d = new JMoneyTextField();
        this.anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label = new JLabel();
        this.anexoJq04C408b = new JMoneyTextField();
        this.anexoJq04C408c = new JMoneyTextField();
        this.anexoJq04C408d = new JMoneyTextField();
        this.anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label = new JLabel();
        this.anexoJq04C423b = new JMoneyTextField();
        this.anexoJq04C423c = new JMoneyTextField();
        this.anexoJq04C423d = new JMoneyTextField();
        this.anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label = new JLabel();
        this.anexoJq04C410b = new JMoneyTextField();
        this.anexoJq04C410c = new JMoneyTextField();
        this.anexoJq04C410d = new JMoneyTextField();
        this.anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label = new JLabel();
        this.anexoJq04C424b = new JMoneyTextField();
        this.anexoJq04C424c = new JMoneyTextField();
        this.anexoJq04C424d = new JMoneyTextField();
        this.anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C409b_base_label\u00a3anexoJq04C409c_base_label\u00a3anexoJq04C409d_base_label = new JLabel();
        this.anexoJq04C409b = new JMoneyTextField();
        this.anexoJq04C409c = new JMoneyTextField();
        this.anexoJq04C409d = new JMoneyTextField();
        this.anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label = new JLabel();
        this.anexoJq04C411b = new JMoneyTextField();
        this.anexoJq04C411c = new JMoneyTextField();
        this.anexoJq04C411d = new JMoneyTextField();
        this.label23 = new JLabel();
        this.anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C412b_base_label\u00a3anexoJq04C412c_base_label\u00a3anexoJq04C412d_base_label = new JLabel();
        this.anexoJq04C412b = new JMoneyTextField();
        this.anexoJq04C412c = new JMoneyTextField();
        this.anexoJq04C412d = new JMoneyTextField();
        this.anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label = new JLabel();
        this.anexoJq04C425b = new JMoneyTextField();
        this.anexoJq04C425c = new JMoneyTextField();
        this.anexoJq04C425d = new JMoneyTextField();
        this.anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num = new JLabelTextFieldNumbering();
        this.anexoJq04C415b_base_label\u00a3anexoJq04C415c_base_label\u00a3anexoJq04C415d_base_label = new JLabel();
        this.anexoJq04C415b = new JMoneyTextField();
        this.anexoJq04C415c = new JMoneyTextField();
        this.anexoJq04C415d = new JMoneyTextField();
        this.anexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label = new JLabel();
        this.anexoJq04C1b = new JMoneyTextField();
        this.anexoJq04C1c = new JMoneyTextField();
        this.anexoJq04C1d = new JMoneyTextField();
        this.anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num = new JLabelTextFieldNumbering();
        this.label22 = new JLabel();
        this.anexoJq04C406b = new JMoneyTextField();
        this.anexoJq04C426b_base_label\u00a3anexoJq04C426c_base_label\u00a3anexoJq04C426d_base_label = new JLabel();
        this.anexoJq04C426b = new JMoneyTextField();
        this.anexoJq04C426c = new JMoneyTextField();
        this.anexoJq04C426d = new JMoneyTextField();
        this.panel3 = new JPanel();
        this.anexoJq04C421_num = new JLabelTextFieldNumbering();
        this.anexoJq04C421_label = new JLabel();
        this.anexoJq04C421 = new JMoneyTextField();
        this.panel4 = new JPanel();
        this.anexoJq04B1Sim_base_label = new JLabel();
        this.label51 = new JLabelTextFieldNumbering();
        this.anexoJq04B1Sim = new JRadioButton();
        this.label52 = new JLabelTextFieldNumbering();
        this.anexoJq04B1Nao = new JRadioButton();
        this.label53 = new JLabel();
        this.panel5 = new JPanel();
        this.label1 = new JLabel();
        this.label3 = new JLabel();
        this.toolBar2 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoJq04T41Scroll = new JScrollPane();
        this.anexoJq04T1 = new JTable();
        this.panel6 = new JPanel();
        this.label4 = new JLabel();
        this.label5 = new JLabel();
        this.toolBar3 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoJq04T2Scroll = new JScrollPane();
        this.anexoJq04T2 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, [550dlu,min]:grow, 5*($lcgap, default)", "$rgap, 7*(default, $lgap), default"));
        this.panel2.setPreferredSize(null);
        this.panel2.setOpaque(false);
        this.panel2.setLayout(new FormLayout("$rgap, 25dlu, $lcgap, default:grow, $lcgap, 75dlu, $lcgap, 95dlu, 2*($lcgap, default), $rgap, default", "$ugap, 32*($lgap, default)"));
        this.label20.setText(bundle.getString("Quadro04Panel.label20.text"));
        this.label20.setBorder(new EtchedBorder());
        this.label20.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label20, cc.xywh(2, 3, 11, 1));
        this.label2.setText(bundle.getString("Quadro04Panel.label2.text"));
        this.label2.setBorder(new EtchedBorder());
        this.label2.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label2, cc.xywh(2, 5, 3, 1));
        this.anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label.setText(bundle.getString("Quadro04Panel.anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label.text"));
        this.anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label.setBorder(new EtchedBorder());
        this.anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoJq04C401a_label\u00a3anexoJq04C402a_label\u00a3anexoJq04C416a_label\u00a3anexoJq04C417a_label, cc.xy(6, 5));
        this.anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label.setText(bundle.getString("Quadro04Panel.anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label.text"));
        this.anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label.setBorder(new EtchedBorder());
        this.anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoJq04C401b_label\u00a3anexoJq04C402b_label\u00a3anexoJq04C416b_label\u00a3anexoJq04C417b_label\u00a3anexoJq04C403b_label\u00a3anexoJq04C404b_label\u00a3anexoJq04C405b_label\u00a3anexoJq04C406b_label\u00a3anexoJq04C407b_label\u00a3anexoJq04C420b_label\u00a3anexoJq04C408b_label\u00a3anexoJq04C418b_label\u00a3anexoJq04C409b_label\u00a3anexoJq04C410b_label\u00a3anexoJq04C411b_label\u00a3anexoJq04C412b_label\u00a3anexoJq04C413b_label\u00a3anexoJq04C414b_label\u00a3anexoJq04C415b_label\u00a3anexoJq04C419b_label\u00a3anexoJq04C1b_label, cc.xy(8, 5));
        this.anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label.setText(bundle.getString("Quadro04Panel.anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label.text"));
        this.anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label.setBorder(new EtchedBorder());
        this.anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoJq04C401c_label\u00a3anexoJq04C402c_label\u00a3anexoJq04C416c_label\u00a3anexoJq04C417c_label\u00a3anexoJq04C403c_label\u00a3anexoJq04C404c_label\u00a3anexoJq04C405c_label\u00a3anexoJq04C406c_label\u00a3anexoJq04C407c_label\u00a3anexoJq04C420c_label\u00a3anexoJq04C408c_label\u00a3anexoJq04C418c_label\u00a3anexoJq04C409c_label\u00a3anexoJq04C410c_label\u00a3anexoJq04C411c_label\u00a3anexoJq04C412c_label\u00a3anexoJq04C413c_label\u00a3anexoJq04C414c_label\u00a3anexoJq04C415c_label\u00a3anexoJq04C419c_label\u00a3anexoJq04C1c_label, cc.xy(10, 5));
        this.anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label.setText(bundle.getString("Quadro04Panel.anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label.text"));
        this.anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label.setBorder(new EtchedBorder());
        this.anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoJq04C401d_label\u00a3anexoJq04C402d_label\u00a3anexoJq04C416d_label\u00a3anexoJq04C417d_label\u00a3anexoJq04C403d_label\u00a3anexoJq04C404d_label\u00a3anexoJq04C405d_label\u00a3anexoJq04C406d_label\u00a3anexoJq04C407d_label\u00a3anexoJq04C420d_label\u00a3anexoJq04C408d_label\u00a3anexoJq04C418d_label\u00a3anexoJq04C409d_label\u00a3anexoJq04C410d_label\u00a3anexoJq04C411d_label\u00a3anexoJq04C412d_label\u00a3anexoJq04C413d_label\u00a3anexoJq04C414d_label\u00a3anexoJq04C415d_label\u00a3anexoJq04C419d_label\u00a3anexoJq04C1d_label, cc.xy(12, 5));
        this.anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num.text"));
        this.anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num.setHorizontalAlignment(0);
        this.anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C401a_num\u00a3anexoJq04C401b_num\u00a3anexoJq04C401c_num\u00a3anexoJq04C401d_num, cc.xy(2, 7));
        this.anexoJq04C401a_base_label\u00a3anexoJq04C401b_base_label\u00a3anexoJq04C401c_base_label\u00a3anexoJq04C401d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C401a_base_label\u00a3anexoJq04C401b_base_label\u00a3anexoJq04C401c_base_label\u00a3anexoJq04C401d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C401a_base_label\u00a3anexoJq04C401b_base_label\u00a3anexoJq04C401c_base_label\u00a3anexoJq04C401d_base_label, cc.xywh(4, 7, 2, 1));
        this.panel2.add((Component)this.anexoJq04C401a, cc.xy(6, 7));
        this.panel2.add((Component)this.anexoJq04C401b, cc.xy(8, 7));
        this.panel2.add((Component)this.anexoJq04C401c, cc.xy(10, 7));
        this.panel2.add((Component)this.anexoJq04C401d, cc.xy(12, 7));
        this.anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num.text"));
        this.anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num.setHorizontalAlignment(0);
        this.anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C402a_num\u00a3anexoJq04C402b_num\u00a3anexoJq04C402c_num\u00a3anexoJq04C402d_num, cc.xy(2, 9));
        this.anexoJq04C402a_base_label\u00a3anexoJq04C402b_base_label\u00a3anexoJq04C402c_base_label\u00a3anexoJq04C402d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C402a_base_label\u00a3anexoJq04C402b_base_label\u00a3anexoJq04C402c_base_label\u00a3anexoJq04C402d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C402a_base_label\u00a3anexoJq04C402b_base_label\u00a3anexoJq04C402c_base_label\u00a3anexoJq04C402d_base_label, cc.xywh(4, 9, 2, 1));
        this.panel2.add((Component)this.anexoJq04C402a, cc.xy(6, 9));
        this.panel2.add((Component)this.anexoJq04C402b, cc.xy(8, 9));
        this.panel2.add((Component)this.anexoJq04C402c, cc.xy(10, 9));
        this.panel2.add((Component)this.anexoJq04C402d, cc.xy(12, 9));
        this.anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num.text"));
        this.anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num.setHorizontalAlignment(0);
        this.anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C416a_num\u00a3anexoJq04C416b_num\u00a3anexoJq04C416c_num\u00a3anexoJq04C416d_num, cc.xy(2, 11));
        this.anexoJq04C416a_base_label\u00a3anexoJq04C416b_base_label\u00a3anexoJq04C416c_base_label\u00a3anexoJq04C416d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C416a_base_label\u00a3anexoJq04C416b_base_label\u00a3anexoJq04C416c_base_label\u00a3anexoJq04C416d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C416a_base_label\u00a3anexoJq04C416b_base_label\u00a3anexoJq04C416c_base_label\u00a3anexoJq04C416d_base_label, cc.xywh(4, 11, 2, 1));
        this.panel2.add((Component)this.anexoJq04C416a, cc.xy(6, 11));
        this.panel2.add((Component)this.anexoJq04C416b, cc.xy(8, 11));
        this.panel2.add((Component)this.anexoJq04C416c, cc.xy(10, 11));
        this.panel2.add((Component)this.anexoJq04C416d, cc.xy(12, 11));
        this.anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num.text"));
        this.anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num.setHorizontalAlignment(0);
        this.anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C417a_num\u00a3anexoJq04C417b_num\u00a3anexoJq04C417c_num\u00a3anexoJq04C417d_num, cc.xy(2, 13));
        this.anexoJq04C417a_base_label\u00a3anexoJq04C417b_base_label\u00a3anexoJq04C417c_base_label\u00a3anexoJq04C417d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C417a_base_label\u00a3anexoJq04C417b_base_label\u00a3anexoJq04C417c_base_label\u00a3anexoJq04C417d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C417a_base_label\u00a3anexoJq04C417b_base_label\u00a3anexoJq04C417c_base_label\u00a3anexoJq04C417d_base_label, cc.xywh(4, 13, 2, 1));
        this.panel2.add((Component)this.anexoJq04C417a, cc.xy(6, 13));
        this.panel2.add((Component)this.anexoJq04C417b, cc.xy(8, 13));
        this.panel2.add((Component)this.anexoJq04C417c, cc.xy(10, 13));
        this.panel2.add((Component)this.anexoJq04C417d, cc.xy(12, 13));
        this.anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num.text"));
        this.anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num.setHorizontalAlignment(0);
        this.anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C419b_num\u00a3anexoJq04C419c_num\u00a3anexoJq04C419d_num, cc.xy(2, 15));
        this.anexoJq04C419b_base_label\u00a3anexoJq04C419c_base_label\u00a3anexoJq04C419d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C419b_base_label\u00a3anexoJq04C419c_base_label\u00a3anexoJq04C419d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C419b_base_label\u00a3anexoJq04C419c_base_label\u00a3anexoJq04C419d_base_label, cc.xywh(4, 15, 3, 1));
        this.panel2.add((Component)this.anexoJq04C419b, cc.xy(8, 15));
        this.panel2.add((Component)this.anexoJq04C419c, cc.xy(10, 15));
        this.panel2.add((Component)this.anexoJq04C419d, cc.xy(12, 15));
        this.label21.setText(bundle.getString("Quadro04Panel.label21.text"));
        this.label21.setBorder(new EtchedBorder());
        this.label21.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label21, cc.xywh(2, 17, 11, 1));
        this.anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num.text"));
        this.anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num.setHorizontalAlignment(0);
        this.anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C403b_num\u00a3anexoJq04C403c_num\u00a3anexoJq04C403d_num, cc.xy(2, 19));
        this.anexoJq04C403b_base_label\u00a3anexoJq04C403c_base_label\u00a3anexoJq04C403d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C403b_base_label\u00a3anexoJq04C403c_base_label\u00a3anexoJq04C403d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C403b_base_label\u00a3anexoJq04C403c_base_label\u00a3anexoJq04C403d_base_label, cc.xywh(4, 19, 3, 1));
        this.panel2.add((Component)this.anexoJq04C403b, cc.xy(8, 19));
        this.panel2.add((Component)this.anexoJq04C403c, cc.xy(10, 19));
        this.panel2.add((Component)this.anexoJq04C403d, cc.xy(12, 19));
        this.anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num.text"));
        this.anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num.setHorizontalAlignment(0);
        this.anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C404b_num\u00a3anexoJq04C404c_num\u00a3anexoJq04C404d_num, cc.xy(2, 21));
        this.anexoJq04C404b_base_label\u00a3anexoJq04C404c_base_label\u00a3anexoJq04C404d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C404b_base_label\u00a3anexoJq04C404c_base_label\u00a3anexoJq04C404d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C404b_base_label\u00a3anexoJq04C404c_base_label\u00a3anexoJq04C404d_base_label, cc.xywh(4, 21, 3, 1));
        this.panel2.add((Component)this.anexoJq04C404b, cc.xy(8, 21));
        this.panel2.add((Component)this.anexoJq04C404c, cc.xy(10, 21));
        this.panel2.add((Component)this.anexoJq04C404d, cc.xy(12, 21));
        this.anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num.text"));
        this.anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num.setHorizontalAlignment(0);
        this.anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C405b_num\u00a3anexoJq04C405c_num\u00a3anexoJq04C405d_num, cc.xy(2, 23));
        this.anexoJq04C405b_base_label\u00a3anexoJq04C405c_base_label\u00a3anexoJq04C405d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C405b_base_label\u00a3anexoJq04C405c_base_label\u00a3anexoJq04C405d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C405b_base_label\u00a3anexoJq04C405c_base_label\u00a3anexoJq04C405d_base_label, cc.xywh(4, 23, 3, 1));
        this.panel2.add((Component)this.anexoJq04C405b, cc.xy(8, 23));
        this.panel2.add((Component)this.anexoJq04C405c, cc.xy(10, 23));
        this.panel2.add((Component)this.anexoJq04C405d, cc.xy(12, 23));
        this.anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num.text"));
        this.anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num.setHorizontalAlignment(0);
        this.anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C406b_num\u00a3anexoJq04C406c_num\u00a3anexoJq04C406d_num, cc.xy(2, 25));
        this.anexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label.text"));
        this.anexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C406b_base_label\u00a3anexoJq04C406c_base_label\u00a3anexoJq04C406d_base_label, cc.xywh(4, 25, 3, 1));
        this.panel2.add((Component)this.anexoJq04C406c, cc.xy(10, 25));
        this.panel2.add((Component)this.anexoJq04C406d, cc.xy(12, 25));
        this.anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num.text"));
        this.anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num.setHorizontalAlignment(0);
        this.anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C407b_num\u00a3anexoJq04C407c_num\u00a3anexoJq04C407d_num, cc.xy(2, 33));
        this.anexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label.text"));
        this.anexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C407b_base_label\u00a3anexoJq04C407c_base_label\u00a3anexoJq04C407d_base_label, cc.xywh(4, 33, 3, 1));
        this.panel2.add((Component)this.anexoJq04C407b, cc.xy(8, 33));
        this.panel2.add((Component)this.anexoJq04C407c, cc.xy(10, 33));
        this.panel2.add((Component)this.anexoJq04C407d, cc.xy(12, 33));
        this.anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num.text"));
        this.anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num.setHorizontalAlignment(0);
        this.anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C420b_num\u00a3anexoJq04C420c_num\u00a3anexoJq04C420d_num, cc.xy(2, 35));
        this.anexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label.text"));
        this.anexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C420b_base_label\u00a3anexoJq04C420c_base_label\u00a3anexoJq04C420d_base_label, cc.xywh(4, 35, 3, 1));
        this.panel2.add((Component)this.anexoJq04C420b, cc.xy(8, 35));
        this.panel2.add((Component)this.anexoJq04C420c, cc.xy(10, 35));
        this.panel2.add((Component)this.anexoJq04C420d, cc.xy(12, 35));
        this.anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num.text"));
        this.anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num.setHorizontalAlignment(0);
        this.anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C418b_num\u00a3anexoJq04C418c_num\u00a3anexoJq04C418d_num, cc.xy(2, 37));
        this.anexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label.text"));
        this.anexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C418b_base_label\u00a3anexoJq04C418c_base_label\u00a3anexoJq04C418d_base_label, cc.xywh(4, 37, 3, 1));
        this.panel2.add((Component)this.anexoJq04C418b, cc.xy(8, 37));
        this.panel2.add((Component)this.anexoJq04C418c, cc.xy(10, 37));
        this.panel2.add((Component)this.anexoJq04C418d, cc.xy(12, 37));
        this.anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num.text"));
        this.anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num.setHorizontalAlignment(0);
        this.anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C422b_num\u00a3anexoJq04C422c_num\u00a3anexoJq04C422d_num, cc.xy(2, 39));
        this.anexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label.text"));
        this.anexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C422b_base_label\u00a3anexoJq04C422c_base_label\u00a3anexoJq04C422d_base_label, cc.xywh(4, 39, 3, 1));
        this.panel2.add((Component)this.anexoJq04C422b, cc.xy(8, 39));
        this.panel2.add((Component)this.anexoJq04C422c, cc.xy(10, 39));
        this.panel2.add((Component)this.anexoJq04C422d, cc.xy(12, 39));
        this.anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num.text"));
        this.anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num.setHorizontalAlignment(0);
        this.anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C408b_num\u00a3anexoJq04C408c_num\u00a3anexoJq04C408d_num, cc.xy(2, 41));
        this.anexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label.text"));
        this.anexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C408b_base_label\u00a3anexoJq04C408c_base_label\u00a3anexoJq04C408d_base_label, cc.xywh(4, 41, 3, 1));
        this.panel2.add((Component)this.anexoJq04C408b, cc.xy(8, 41));
        this.panel2.add((Component)this.anexoJq04C408c, cc.xy(10, 41));
        this.panel2.add((Component)this.anexoJq04C408d, cc.xy(12, 41));
        this.anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num.text"));
        this.anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num.setHorizontalAlignment(0);
        this.anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C423b_num\u00a3anexoJq04C423c_num\u00a3anexoJq04C423d_num, cc.xy(2, 43));
        this.anexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label.text"));
        this.anexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C423b_base_label\u00a3anexoJq04C423c_base_label\u00a3anexoJq04C423d_base_label, cc.xywh(4, 43, 3, 1));
        this.panel2.add((Component)this.anexoJq04C423b, cc.xy(8, 43));
        this.panel2.add((Component)this.anexoJq04C423c, cc.xy(10, 43));
        this.panel2.add((Component)this.anexoJq04C423d, cc.xy(12, 43));
        this.anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num.text"));
        this.anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num.setHorizontalAlignment(0);
        this.anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C410b_num\u00a3anexoJq04C410c_num\u00a3anexoJq04C410d_num, cc.xy(2, 45));
        this.anexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label.text"));
        this.anexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C410b_base_label\u00a3anexoJq04C410c_base_label\u00a3anexoJq04C410d_base_label, cc.xywh(4, 45, 3, 1));
        this.panel2.add((Component)this.anexoJq04C410b, cc.xy(8, 45));
        this.panel2.add((Component)this.anexoJq04C410c, cc.xy(10, 45));
        this.panel2.add((Component)this.anexoJq04C410d, cc.xy(12, 45));
        this.anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num.text"));
        this.anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num.setHorizontalAlignment(0);
        this.anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C424b_num\u00a3anexoJq04C424c_num\u00a3anexoJq04C424d_num, cc.xy(2, 47));
        this.anexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label.text"));
        this.anexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C424b_base_label\u00a3anexoJq04C424c_base_label\u00a3anexoJq04C424d_base_label, cc.xywh(4, 47, 3, 1));
        this.panel2.add((Component)this.anexoJq04C424b, cc.xy(8, 47));
        this.panel2.add((Component)this.anexoJq04C424c, cc.xy(10, 47));
        this.panel2.add((Component)this.anexoJq04C424d, cc.xy(12, 47));
        this.anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num.text"));
        this.anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num.setHorizontalAlignment(0);
        this.anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C409b_num\u00a3anexoJq04C409c_num\u00a3anexoJq04C409d_num, cc.xy(2, 49));
        this.anexoJq04C409b_base_label\u00a3anexoJq04C409c_base_label\u00a3anexoJq04C409d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C409b_base_label\u00a3anexoJq04C409c_base_label\u00a3anexoJq04C409d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C409b_base_label\u00a3anexoJq04C409c_base_label\u00a3anexoJq04C409d_base_label, cc.xywh(4, 49, 3, 1));
        this.panel2.add((Component)this.anexoJq04C409b, cc.xy(8, 49));
        this.panel2.add((Component)this.anexoJq04C409c, cc.xy(10, 49));
        this.panel2.add((Component)this.anexoJq04C409d, cc.xy(12, 49));
        this.anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num.text"));
        this.anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num.setHorizontalAlignment(0);
        this.anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C411b_num\u00a3anexoJq04C411c_num\u00a3anexoJq04C411d_num, cc.xy(2, 51));
        this.anexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label.text"));
        this.anexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C411b_base_label\u00a3anexoJq04C411c_base_label\u00a3anexoJq04C411d_base_label, cc.xywh(4, 51, 3, 1));
        this.panel2.add((Component)this.anexoJq04C411b, cc.xy(8, 51));
        this.panel2.add((Component)this.anexoJq04C411c, cc.xy(10, 51));
        this.panel2.add((Component)this.anexoJq04C411d, cc.xy(12, 51));
        this.label23.setText(bundle.getString("Quadro04Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label23, cc.xywh(2, 53, 11, 1));
        this.anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num.text"));
        this.anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num.setHorizontalAlignment(0);
        this.anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C412b_num\u00a3anexoJq04C412c_num\u00a3anexoJq04C412d_num, cc.xy(2, 55));
        this.anexoJq04C412b_base_label\u00a3anexoJq04C412c_base_label\u00a3anexoJq04C412d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C412b_base_label\u00a3anexoJq04C412c_base_label\u00a3anexoJq04C412d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C412b_base_label\u00a3anexoJq04C412c_base_label\u00a3anexoJq04C412d_base_label, cc.xywh(4, 55, 3, 1));
        this.anexoJq04C412b.setAllowNegative(true);
        this.panel2.add((Component)this.anexoJq04C412b, cc.xy(8, 55));
        this.panel2.add((Component)this.anexoJq04C412c, cc.xy(10, 55));
        this.panel2.add((Component)this.anexoJq04C412d, cc.xy(12, 55));
        this.anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num.text"));
        this.anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num.setHorizontalAlignment(0);
        this.anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C425b_num\u00a3anexoJq04C425c_num\u00a3anexoJq04C425d_num, cc.xy(2, 57));
        this.anexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label.text"));
        this.anexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label.setMinimumSize(new Dimension(200, 14));
        this.panel2.add((Component)this.anexoJq04C425b_base_label\u00a3anexoJq04C425c_base_label\u00a3anexoJq04C425d_base_label, cc.xywh(4, 57, 3, 1));
        this.anexoJq04C425b.setAllowNegative(true);
        this.panel2.add((Component)this.anexoJq04C425b, cc.xy(8, 57));
        this.panel2.add((Component)this.anexoJq04C425c, cc.xy(10, 57));
        this.panel2.add((Component)this.anexoJq04C425d, cc.xy(12, 57));
        this.anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num.text"));
        this.anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num.setHorizontalAlignment(0);
        this.anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C415b_num\u00a3anexoJq04C415c_num\u00a3anexoJq04C415d_num, cc.xy(2, 59));
        this.anexoJq04C415b_base_label\u00a3anexoJq04C415c_base_label\u00a3anexoJq04C415d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C415b_base_label\u00a3anexoJq04C415c_base_label\u00a3anexoJq04C415d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C415b_base_label\u00a3anexoJq04C415c_base_label\u00a3anexoJq04C415d_base_label, cc.xywh(4, 59, 3, 1));
        this.panel2.add((Component)this.anexoJq04C415b, cc.xy(8, 59));
        this.panel2.add((Component)this.anexoJq04C415c, cc.xy(10, 59));
        this.panel2.add((Component)this.anexoJq04C415d, cc.xy(12, 59));
        this.anexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label.text"));
        this.anexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoJq04C1b_base_label\u00a3anexoJq04C1c_base_label\u00a3anexoJq04C1d_base_label, cc.xy(6, 63));
        this.anexoJq04C1b.setColumns(15);
        this.anexoJq04C1b.setEditable(false);
        this.anexoJq04C1b.setAllowNegative(true);
        this.panel2.add((Component)this.anexoJq04C1b, cc.xy(8, 63));
        this.anexoJq04C1c.setColumns(15);
        this.anexoJq04C1c.setEditable(false);
        this.panel2.add((Component)this.anexoJq04C1c, cc.xy(10, 63));
        this.anexoJq04C1d.setColumns(15);
        this.anexoJq04C1d.setEditable(false);
        this.panel2.add((Component)this.anexoJq04C1d, cc.xy(12, 63));
        this.anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num.setText(bundle.getString("Quadro04Panel.anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num.text"));
        this.anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num.setHorizontalAlignment(0);
        this.anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoJq04C426b_num\u00a3anexoJq04C426c_num\u00a3anexoJq04C426d_num, cc.xy(2, 27));
        this.label22.setText(bundle.getString("Quadro04Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label22, cc.xywh(2, 31, 13, 1));
        this.panel2.add((Component)this.anexoJq04C406b, cc.xy(8, 25));
        this.anexoJq04C426b_base_label\u00a3anexoJq04C426c_base_label\u00a3anexoJq04C426d_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04C426b_base_label\u00a3anexoJq04C426c_base_label\u00a3anexoJq04C426d_base_label.text"));
        this.panel2.add((Component)this.anexoJq04C426b_base_label\u00a3anexoJq04C426c_base_label\u00a3anexoJq04C426d_base_label, cc.xywh(4, 27, 3, 1));
        this.panel2.add((Component)this.anexoJq04C426b, cc.xy(8, 27));
        this.panel2.add((Component)this.anexoJq04C426c, cc.xy(10, 27));
        this.panel2.add((Component)this.anexoJq04C426d, cc.xy(12, 27));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.panel3.setBorder(new EtchedBorder());
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 2*($lcgap, default), $lcgap, default:grow", "$ugap, $lgap, default, $lgap, $ugap"));
        this.anexoJq04C421_num.setText(bundle.getString("Quadro04Panel.anexoJq04C421_num.text"));
        this.anexoJq04C421_num.setHorizontalAlignment(0);
        this.anexoJq04C421_num.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoJq04C421_num, cc.xy(3, 3));
        this.anexoJq04C421_label.setText(bundle.getString("Quadro04Panel.anexoJq04C421_label.text"));
        this.anexoJq04C421_label.setMinimumSize(new Dimension(100, 14));
        this.panel3.add((Component)this.anexoJq04C421_label, cc.xy(5, 3));
        this.panel3.add((Component)this.anexoJq04C421, cc.xy(7, 3));
        this.this2.add((Component)this.panel3, cc.xy(2, 6));
        this.panel4.setBorder(new EtchedBorder());
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 13dlu, 0px, 2*(default, $lcgap), 13dlu, 0px, 3*(default, $lcgap), default:grow", "$ugap, 3*($lgap, default)"));
        this.anexoJq04B1Sim_base_label.setText(bundle.getString("Quadro04Panel.anexoJq04B1Sim_base_label.text"));
        this.anexoJq04B1Sim_base_label.setMinimumSize(new Dimension(100, 14));
        this.panel4.add((Component)this.anexoJq04B1Sim_base_label, cc.xy(3, 3));
        this.label51.setText(bundle.getString("Quadro04Panel.label51.text"));
        this.label51.setBorder(new EtchedBorder());
        this.label51.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label51, cc.xy(7, 3));
        this.anexoJq04B1Sim.setText(bundle.getString("Quadro04Panel.anexoJq04B1Sim.text"));
        this.panel4.add((Component)this.anexoJq04B1Sim, cc.xy(9, 3));
        this.label52.setText(bundle.getString("Quadro04Panel.label52.text"));
        this.label52.setBorder(new EtchedBorder());
        this.label52.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label52, cc.xy(13, 3));
        this.anexoJq04B1Nao.setText(bundle.getString("Quadro04Panel.anexoJq04B1Nao.text"));
        this.panel4.add((Component)this.anexoJq04B1Nao, cc.xy(15, 3));
        this.label53.setText(bundle.getString("Quadro04Panel.label53.text"));
        this.label53.setMinimumSize(new Dimension(100, 14));
        this.panel4.add((Component)this.label53, cc.xy(19, 3));
        this.this2.add((Component)this.panel4, cc.xy(2, 16));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, default:grow, 2*($lcgap, default), $lcgap, 250dlu:grow, $lcgap, default:grow, $lcgap, $rgap", "2*(default, $lgap), fill:165dlu, 3*($lgap, default)"));
        this.label1.setText(bundle.getString("Quadro04Panel.label1.text"));
        this.label1.setBorder(new EtchedBorder());
        this.label1.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label1, cc.xy(1, 1));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label3, cc.xywh(2, 1, 9, 1));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoJq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoJq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button2);
        this.panel5.add((Component)this.toolBar2, cc.xy(4, 3));
        this.anexoJq04T41Scroll.setViewportView(this.anexoJq04T1);
        this.panel5.add((Component)this.anexoJq04T41Scroll, cc.xywh(4, 5, 5, 1));
        this.this2.add((Component)this.panel5, cc.xywh(1, 12, 2, 1));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, default:grow, 2*($lcgap, default), $lcgap, 250dlu:grow, $lcgap, default:grow, $lcgap, $rgap", "2*(default, $lgap), fill:165dlu, $lgap, default"));
        this.label4.setText(bundle.getString("Quadro04Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label4, cc.xy(1, 1));
        this.label5.setText(bundle.getString("Quadro04Panel.label5.text"));
        this.label5.setBorder(new EtchedBorder());
        this.label5.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label5, cc.xywh(2, 1, 9, 1));
        this.toolBar3.setFloatable(false);
        this.toolBar3.setRollover(true);
        this.button3.setText(bundle.getString("Quadro04Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoJq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button3);
        this.button4.setText(bundle.getString("Quadro04Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoJq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button4);
        this.panel6.add((Component)this.toolBar3, cc.xy(4, 3));
        this.anexoJq04T2Scroll.setViewportView(this.anexoJq04T2);
        this.panel6.add((Component)this.anexoJq04T2Scroll, cc.xywh(4, 5, 5, 1));
        this.this2.add((Component)this.panel6, cc.xywh(1, 14, 12, 1));
        this.add((Component)this.this2, "Center");
    }

}

