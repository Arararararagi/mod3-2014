/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro02Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro02;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro02PanelBase;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro02PanelExtension
extends Quadro02PanelBase
implements IBindablePanel<Quadro02> {
    public Quadro02PanelExtension(Quadro02 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro02 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro02Bindings.doBindings(model, this);
            if (!Modelo3IRSv2015Parameters.instance().isDpapelIRS()) {
                this.getAnexoJq02C01().setEnabled(false);
            }
        }
    }
}

