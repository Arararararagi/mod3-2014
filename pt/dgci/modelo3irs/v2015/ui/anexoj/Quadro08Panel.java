/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro08Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoJq08T1Scroll;
    protected JTable anexoJq08T1;

    public Quadro08Panel() {
        this.initComponents();
    }

    protected void addLineAnexoJq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoJq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoJq08T1Scroll() {
        return this.anexoJq08T1Scroll;
    }

    public JTable getAnexoJq08T1() {
        return this.anexoJq08T1;
    }

    protected void addLineAnexoJq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoJq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoJ");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.toolBar2 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoJq08T1Scroll = new JScrollPane();
        this.anexoJq08T1 = new JTable();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro08Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro08Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $rgap, 327dlu, $lcgap, default:grow", "$rgap, default, $lgap, 122dlu, $lgap, default"));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button1.setText(bundle.getString("Quadro08Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.addLineAnexoJq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button1);
        this.button2.setText(bundle.getString("Quadro08Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.removeLineAnexoJq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button2);
        this.panel2.add((Component)this.toolBar2, CC.xy(3, 2));
        this.anexoJq08T1Scroll.setPreferredSize(new Dimension(400, 200));
        this.anexoJq08T1Scroll.setViewportView(this.anexoJq08T1);
        this.panel2.add((Component)this.anexoJq08T1Scroll, CC.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel2, CC.xy(2, 4));
        this.add((Component)this.this2, "Center");
    }

}

