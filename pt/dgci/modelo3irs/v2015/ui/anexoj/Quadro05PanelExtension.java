/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro05Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro05PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro05PanelExtension
extends Quadro05PanelBase
implements IBindablePanel<Quadro05> {
    private static final long serialVersionUID = 8789646887063908308L;
    public static final int MAX_LINES_Q05_T1 = 50;
    public static final int MAX_LINES_Q05_T2 = 50;

    public Quadro05PanelExtension(Quadro05 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro05 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro05Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoJq05T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoJq05T1() != null && this.model.getAnexoJq05T1().size() >= 50) {
            return;
        }
        super.addLineAnexoJq05T1_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoJq05T2_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoJq05T2() != null && this.model.getAnexoJq05T2().size() >= 50) {
            return;
        }
        super.addLineAnexoJq05T2_LinhaActionPerformed(e);
    }
}

