/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro07Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro07PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro07PanelExtension
extends Quadro07PanelBase
implements IBindablePanel<Quadro07> {
    public static final int MAX_LINES_Q07_T1 = 10;

    public Quadro07PanelExtension(Quadro07 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro07 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro07Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoJq07T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoJq07T1() != null && this.model.getAnexoJq07T1().size() >= 10) {
            return;
        }
        super.addLineAnexoJq07T1_LinhaActionPerformed(e);
    }
}

