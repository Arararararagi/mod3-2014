/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoj;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro08Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro08PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro08PanelExtension
extends Quadro08PanelBase
implements IBindablePanel<Quadro08> {
    public static final int MAX_LINES_Q08_T1 = 30;

    public Quadro08PanelExtension(Quadro08 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro08 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro08Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoJq08T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoJq08T1() != null && this.model.getAnexoJq08T1().size() >= 30) {
            return;
        }
        super.addLineAnexoJq08T1_LinhaActionPerformed(e);
    }
}

