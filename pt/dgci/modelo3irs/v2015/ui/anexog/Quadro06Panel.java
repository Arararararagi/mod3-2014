/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JLabel anexoGq06C601b_label\u00a3anexoGq06C602b_label;
    protected JLabel anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label;
    protected JLabel anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label;
    protected JLabel anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label;
    protected JLabelTextFieldNumbering anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num;
    protected JEditableComboBox anexoGq06C601b;
    protected JMoneyTextField anexoGq06C601c;
    protected JMoneyTextField anexoGq06C601d;
    protected JMoneyTextField anexoGq06C601e;
    protected JLabelTextFieldNumbering anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num;
    protected JEditableComboBox anexoGq06C602b;
    protected JMoneyTextField anexoGq06C602c;
    protected JMoneyTextField anexoGq06C602d;
    protected JMoneyTextField anexoGq06C602e;
    protected JLabel anexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label;
    protected JMoneyTextField anexoGq06C1c;
    protected JMoneyTextField anexoGq06C1d;
    protected JMoneyTextField anexoGq06C1e;

    public Quadro06Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoGq06C601b_label\u00a3anexoGq06C602b_label() {
        return this.anexoGq06C601b_label\u00a3anexoGq06C602b_label;
    }

    public JLabel getAnexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label() {
        return this.anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label;
    }

    public JLabel getAnexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label() {
        return this.anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label;
    }

    public JLabel getAnexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label() {
        return this.anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label;
    }

    public JLabelTextFieldNumbering getAnexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num() {
        return this.anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num;
    }

    public JMoneyTextField getAnexoGq06C601c() {
        return this.anexoGq06C601c;
    }

    public JMoneyTextField getAnexoGq06C601d() {
        return this.anexoGq06C601d;
    }

    public JMoneyTextField getAnexoGq06C601e() {
        return this.anexoGq06C601e;
    }

    public JLabelTextFieldNumbering getAnexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num() {
        return this.anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num;
    }

    public JMoneyTextField getAnexoGq06C602c() {
        return this.anexoGq06C602c;
    }

    public JMoneyTextField getAnexoGq06C602d() {
        return this.anexoGq06C602d;
    }

    public JMoneyTextField getAnexoGq06C602e() {
        return this.anexoGq06C602e;
    }

    public JLabel getAnexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label() {
        return this.anexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label;
    }

    public JMoneyTextField getAnexoGq06C1c() {
        return this.anexoGq06C1c;
    }

    public JMoneyTextField getAnexoGq06C1d() {
        return this.anexoGq06C1d;
    }

    public JEditableComboBox getAnexoGq06C601b() {
        return this.anexoGq06C601b;
    }

    public JEditableComboBox getAnexoGq06C602b() {
        return this.anexoGq06C602b;
    }

    public JMoneyTextField getAnexoGq06C1e() {
        return this.anexoGq06C1e;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.anexoGq06C601b_label\u00a3anexoGq06C602b_label = new JLabel();
        this.anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label = new JLabel();
        this.anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label = new JLabel();
        this.anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label = new JLabel();
        this.anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num = new JLabelTextFieldNumbering();
        this.anexoGq06C601b = new JEditableComboBox();
        this.anexoGq06C601c = new JMoneyTextField();
        this.anexoGq06C601d = new JMoneyTextField();
        this.anexoGq06C601e = new JMoneyTextField();
        this.anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num = new JLabelTextFieldNumbering();
        this.anexoGq06C602b = new JEditableComboBox();
        this.anexoGq06C602c = new JMoneyTextField();
        this.anexoGq06C602d = new JMoneyTextField();
        this.anexoGq06C602e = new JMoneyTextField();
        this.anexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label = new JLabel();
        this.anexoGq06C1c = new JMoneyTextField();
        this.anexoGq06C1d = new JMoneyTextField();
        this.anexoGq06C1e = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, default:grow", "default, $lgap, default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("$rgap, default:grow, $lcgap, 25dlu, 0px, default, $rgap, 2*(85dlu, $lcgap), 85dlu, $rgap, default:grow", "4*(default, $lgap), default"));
        this.anexoGq06C601b_label\u00a3anexoGq06C602b_label.setText(bundle.getString("Quadro06Panel.anexoGq06C601b_label\u00a3anexoGq06C602b_label.text"));
        this.anexoGq06C601b_label\u00a3anexoGq06C602b_label.setBorder(new EtchedBorder());
        this.anexoGq06C601b_label\u00a3anexoGq06C602b_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq06C601b_label\u00a3anexoGq06C602b_label, cc.xywh(4, 1, 3, 1));
        this.anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label.setText(bundle.getString("Quadro06Panel.anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label.text"));
        this.anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label.setBorder(new EtchedBorder());
        this.anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq06C601c_label\u00a3anexoGq06C602c_label\u00a3anexoGq06C1c_label, cc.xy(8, 1));
        this.anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label.setText(bundle.getString("Quadro06Panel.anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label.text"));
        this.anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label.setBorder(new EtchedBorder());
        this.anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq06C601d_label\u00a3anexoGq06C602d_label\u00a3anexoGq06C1d_label, cc.xy(10, 1));
        this.anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label.setText(bundle.getString("Quadro06Panel.anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label.text"));
        this.anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label.setBorder(new EtchedBorder());
        this.anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq06C601e_label\u00a3anexoGq06C602e_label\u00a3anexoGq06C1e_label, cc.xy(12, 1));
        this.anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num.setText(bundle.getString("Quadro06Panel.anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num.text"));
        this.anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num.setBorder(new EtchedBorder());
        this.anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq06C601b_num\u00a3anexoGq06C601c_num\u00a3anexoGq06C601d_num\u00a3anexoGq06C601e_num, cc.xy(4, 3));
        this.panel3.add((Component)this.anexoGq06C601b, cc.xy(6, 3));
        this.anexoGq06C601c.setColumns(13);
        this.panel3.add((Component)this.anexoGq06C601c, cc.xy(8, 3));
        this.anexoGq06C601d.setColumns(13);
        this.panel3.add((Component)this.anexoGq06C601d, cc.xy(10, 3));
        this.anexoGq06C601e.setColumns(13);
        this.panel3.add((Component)this.anexoGq06C601e, cc.xy(12, 3));
        this.anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num.setText(bundle.getString("Quadro06Panel.anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num.text"));
        this.anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num.setBorder(new EtchedBorder());
        this.anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq06C602b_num\u00a3anexoGq06C602c_num\u00a3anexoGq06C602d_num\u00a3anexoGq06C602e_num, cc.xy(4, 5));
        this.panel3.add((Component)this.anexoGq06C602b, cc.xy(6, 5));
        this.anexoGq06C602c.setColumns(13);
        this.panel3.add((Component)this.anexoGq06C602c, cc.xy(8, 5));
        this.anexoGq06C602d.setColumns(13);
        this.panel3.add((Component)this.anexoGq06C602d, cc.xy(10, 5));
        this.anexoGq06C602e.setColumns(13);
        this.panel3.add((Component)this.anexoGq06C602e, cc.xy(12, 5));
        this.anexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label.setText(bundle.getString("Quadro06Panel.anexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label.text"));
        this.anexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq06C1c_base_label\u00a3anexoGq06C1d_base_label\u00a3anexoGq06C1e_base_label, cc.xy(6, 7));
        this.anexoGq06C1c.setColumns(14);
        this.anexoGq06C1c.setEditable(false);
        this.panel3.add((Component)this.anexoGq06C1c, cc.xy(8, 7));
        this.anexoGq06C1d.setEditable(false);
        this.anexoGq06C1d.setColumns(14);
        this.panel3.add((Component)this.anexoGq06C1d, cc.xy(10, 7));
        this.anexoGq06C1e.setEditable(false);
        this.anexoGq06C1e.setColumns(14);
        this.panel3.add((Component)this.anexoGq06C1e, cc.xy(12, 7));
        this.this2.add((Component)this.panel3, cc.xy(2, 3));
        this.add((Component)this.this2, "Center");
    }
}

