/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro08Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro08PanelBase
extends Quadro08Panel
implements IBindablePanel<Quadro08> {
    private static final long serialVersionUID = 1;
    protected Quadro08 model;

    @Override
    public abstract void setModel(Quadro08 var1, boolean var2);

    @Override
    public Quadro08 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoGq08T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoGq08T1().add(new AnexoGq08T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoGq08T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoGq08T1.getSelectedRow() != -1 ? this.anexoGq08T1.getSelectedRow() : this.anexoGq08T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoGq08T1().remove(selectedRow);
            }
        }
    }

    @Override
    protected void addLineAnexoGq08T2_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoGq08T2().add(new AnexoGq08T2_Linha());
        }
    }

    @Override
    protected void removeLineAnexoGq08T2_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoGq08T2.getSelectedRow() != -1 ? this.anexoGq08T2.getSelectedRow() : this.anexoGq08T2.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoGq08T2().remove(selectedRow);
            }
        }
    }

    public Quadro08PanelBase(Quadro08 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

