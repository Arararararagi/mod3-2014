/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro08Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoGq08T1Scroll;
    protected JTable anexoGq08T1;
    protected JPanel panel4;
    protected JLabel anexoGq08C1_label;
    protected JLabel anexoGq08C2_label;
    protected JLabel anexoGq08C3_label;
    protected JLabel anexoGq08C1_base_label\u00a3anexoGq08C2_base_label\u00a3anexoGq08C3_base_label;
    protected JMoneyTextField anexoGq08C1;
    protected JMoneyTextField anexoGq08C2;
    protected JMoneyTextField anexoGq08C3;
    protected JPanel panel5;
    protected JPanel panel6;
    protected JLabel label13;
    protected JLabel label3;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoGq08T2Scroll;
    protected JTable anexoGq08T2;

    public Quadro08Panel() {
        this.initComponents();
    }

    protected void addLineAnexoGq08T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq08T2_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoGq08T1Scroll() {
        return this.anexoGq08T1Scroll;
    }

    public JTable getAnexoGq08T1() {
        return this.anexoGq08T1;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoGq08C1_label() {
        return this.anexoGq08C1_label;
    }

    public JLabel getAnexoGq08C2_label() {
        return this.anexoGq08C2_label;
    }

    public JLabel getAnexoGq08C3_label() {
        return this.anexoGq08C3_label;
    }

    public JLabel getAnexoGq08C1_base_label\u00a3anexoGq08C2_base_label\u00a3anexoGq08C3_base_label() {
        return this.anexoGq08C1_base_label\u00a3anexoGq08C2_base_label\u00a3anexoGq08C3_base_label;
    }

    public JMoneyTextField getAnexoGq08C1() {
        return this.anexoGq08C1;
    }

    public JMoneyTextField getAnexoGq08C2() {
        return this.anexoGq08C2;
    }

    public JMoneyTextField getAnexoGq08C3() {
        return this.anexoGq08C3;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoGq08T2Scroll() {
        return this.anexoGq08T2Scroll;
    }

    public JTable getAnexoGq08T2() {
        return this.anexoGq08T2;
    }

    protected void addLineAnexoGq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoGq08T1Scroll = new JScrollPane();
        this.anexoGq08T1 = new JTable();
        this.panel4 = new JPanel();
        this.anexoGq08C1_label = new JLabel();
        this.anexoGq08C2_label = new JLabel();
        this.anexoGq08C3_label = new JLabel();
        this.anexoGq08C1_base_label\u00a3anexoGq08C2_base_label\u00a3anexoGq08C3_base_label = new JLabel();
        this.anexoGq08C1 = new JMoneyTextField();
        this.anexoGq08C2 = new JMoneyTextField();
        this.anexoGq08C3 = new JMoneyTextField();
        this.panel5 = new JPanel();
        this.panel6 = new JPanel();
        this.label13 = new JLabel();
        this.label3 = new JLabel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoGq08T2Scroll = new JScrollPane();
        this.anexoGq08T2 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro08Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro08Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 5*(default, $lgap), default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 580dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:231dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro08Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.addLineAnexoGq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro08Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.removeLineAnexoGq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoGq08T1Scroll.setViewportView(this.anexoGq08T1);
        this.panel3.add((Component)this.anexoGq08T1Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, default, 3*($lcgap, 90dlu), $lcgap, default:grow", "2*(default, $lgap), default"));
        this.anexoGq08C1_label.setText(bundle.getString("Quadro08Panel.anexoGq08C1_label.text"));
        this.anexoGq08C1_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq08C1_label, cc.xywh(5, 1, 2, 1));
        this.anexoGq08C2_label.setText(bundle.getString("Quadro08Panel.anexoGq08C2_label.text"));
        this.anexoGq08C2_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq08C2_label, cc.xy(7, 1));
        this.anexoGq08C3_label.setText(bundle.getString("Quadro08Panel.anexoGq08C3_label.text"));
        this.anexoGq08C3_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq08C3_label, cc.xy(9, 1));
        this.anexoGq08C1_base_label\u00a3anexoGq08C2_base_label\u00a3anexoGq08C3_base_label.setText(bundle.getString("Quadro08Panel.anexoGq08C1_base_label\u00a3anexoGq08C2_base_label\u00a3anexoGq08C3_base_label.text"));
        this.panel4.add((Component)this.anexoGq08C1_base_label\u00a3anexoGq08C2_base_label\u00a3anexoGq08C3_base_label, cc.xy(3, 3));
        this.anexoGq08C1.setEditable(false);
        this.anexoGq08C1.setColumns(15);
        this.panel4.add((Component)this.anexoGq08C1, cc.xy(5, 3));
        this.anexoGq08C2.setEditable(false);
        this.anexoGq08C2.setColumns(15);
        this.panel4.add((Component)this.anexoGq08C2, cc.xy(7, 3));
        this.anexoGq08C3.setEditable(false);
        this.anexoGq08C3.setColumns(15);
        this.panel4.add((Component)this.anexoGq08C3, cc.xy(9, 3));
        this.this2.add((Component)this.panel4, cc.xy(2, 6));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 100dlu, $rgap, default:grow", "$rgap, 2*(default, $lgap), fill:231dlu, $lgap, default"));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap", "default"));
        this.label13.setText(bundle.getString("Quadro08Panel.label13.text"));
        this.label13.setBorder(new EtchedBorder());
        this.label13.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label13, cc.xy(1, 1));
        this.label3.setText(bundle.getString("Quadro08Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label3, cc.xy(3, 1));
        this.panel5.add((Component)this.panel6, cc.xywh(1, 2, 9, 1));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button3.setText(bundle.getString("Quadro08Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.addLineAnexoGq08T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro08Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.removeLineAnexoGq08T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel5.add((Component)this.toolBar2, cc.xy(3, 4));
        this.anexoGq08T2Scroll.setViewportView(this.anexoGq08T2);
        this.panel5.add((Component)this.anexoGq08T2Scroll, cc.xywh(3, 6, 5, 1));
        this.this2.add((Component)this.panel5, cc.xy(2, 8));
        this.add((Component)this.this2, "Center");
    }

}

