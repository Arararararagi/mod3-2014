/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import java.util.Observable;
import java.util.Observer;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro07Bindings;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro07PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro07PanelExtension
extends Quadro07PanelBase
implements IBindablePanel<Quadro07>,
RostoQuadroObserver {
    public Quadro07PanelExtension(Quadro07 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro07 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro07Bindings.doBindings(model, this);
        }
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoGModel anexoG = (AnexoGModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoGModel.class);
            if (anexoG != null) {
                anexoG.getQuadro07().setAnexoGq07C701b(this.refreshFieldValue(arg, label, anexoG.getQuadro07().getAnexoGq07C701b()));
                anexoG.getQuadro07().setAnexoGq07C702b(this.refreshFieldValue(arg, label, anexoG.getQuadro07().getAnexoGq07C702b()));
            }
        }
    }

    private String refreshFieldValue(Object eventArg, String label, String value) {
        String newValue = value;
        if (eventArg instanceof RostoQuadroChangeState) {
            int index = ((RostoQuadroChangeState)eventArg).getRowIndex();
            if (index == -1) {
                if (value != null && value.startsWith(label)) {
                    newValue = null;
                }
            } else {
                boolean isLastRow = ((RostoQuadroChangeState)eventArg).isLastRow();
                RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)eventArg).getOperation();
                if (value != null && value.startsWith(label + (index + 1))) {
                    newValue = null;
                } else if (!isLastRow && value != null && value.startsWith(label) && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                    newValue = this.reindexFieldLabel(value, label, index);
                }
            }
        } else if (value != null && value.startsWith(label)) {
            newValue = null;
        }
        return newValue;
    }

    private String reindexFieldLabel(String value, String labelPrefix, int index) {
        int currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(value, labelPrefix);
        if (currIndex != -1 && currIndex > index) {
            return labelPrefix + (currIndex - 1);
        }
        return value;
    }
}

