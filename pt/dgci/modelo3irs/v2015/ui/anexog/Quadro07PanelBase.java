/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro07Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro07PanelBase
extends Quadro07Panel
implements IBindablePanel<Quadro07> {
    private static final long serialVersionUID = 1;
    protected Quadro07 model;

    @Override
    public abstract void setModel(Quadro07 var1, boolean var2);

    @Override
    public Quadro07 getModel() {
        return this.model;
    }

    public Quadro07PanelBase(Quadro07 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

