/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JLabel anexoGq07C701b_label;
    protected JLabel anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label;
    protected JLabel anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label;
    protected JLabelTextFieldNumbering anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num;
    protected JEditableComboBox anexoGq07C701b;
    protected JMoneyTextField anexoGq07C701c;
    protected JMoneyTextField anexoGq07C701d;
    protected JLabelTextFieldNumbering anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num;
    protected JEditableComboBox anexoGq07C702b;
    protected JMoneyTextField anexoGq07C702c;
    protected JMoneyTextField anexoGq07C702d;
    protected JLabel anexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label;
    protected JMoneyTextField anexoGq07C1c;
    protected JMoneyTextField anexoGq07C1d;

    public Quadro07Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoGq07C701b_label() {
        return this.anexoGq07C701b_label;
    }

    public JLabel getAnexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label() {
        return this.anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label;
    }

    public JLabel getAnexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label() {
        return this.anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label;
    }

    public JLabelTextFieldNumbering getAnexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num() {
        return this.anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num;
    }

    public JEditableComboBox getAnexoGq07C701b() {
        return this.anexoGq07C701b;
    }

    public JMoneyTextField getAnexoGq07C701c() {
        return this.anexoGq07C701c;
    }

    public JMoneyTextField getAnexoGq07C701d() {
        return this.anexoGq07C701d;
    }

    public JLabelTextFieldNumbering getAnexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num() {
        return this.anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num;
    }

    public JEditableComboBox getAnexoGq07C702b() {
        return this.anexoGq07C702b;
    }

    public JMoneyTextField getAnexoGq07C702c() {
        return this.anexoGq07C702c;
    }

    public JMoneyTextField getAnexoGq07C702d() {
        return this.anexoGq07C702d;
    }

    public JLabel getAnexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label() {
        return this.anexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label;
    }

    public JMoneyTextField getAnexoGq07C1c() {
        return this.anexoGq07C1c;
    }

    public JMoneyTextField getAnexoGq07C1d() {
        return this.anexoGq07C1d;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.anexoGq07C701b_label = new JLabel();
        this.anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label = new JLabel();
        this.anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label = new JLabel();
        this.anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num = new JLabelTextFieldNumbering();
        this.anexoGq07C701b = new JEditableComboBox();
        this.anexoGq07C701c = new JMoneyTextField();
        this.anexoGq07C701d = new JMoneyTextField();
        this.anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num = new JLabelTextFieldNumbering();
        this.anexoGq07C702b = new JEditableComboBox();
        this.anexoGq07C702c = new JMoneyTextField();
        this.anexoGq07C702d = new JMoneyTextField();
        this.anexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label = new JLabel();
        this.anexoGq07C1c = new JMoneyTextField();
        this.anexoGq07C1d = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, default:grow, $lcgap, default:grow", "default, $lgap, default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("$rgap, default:grow, $lcgap, 25dlu, 0px, default, $rgap, default, $lcgap, default, $rgap", "$rgap, 4*(default, $lgap), default"));
        this.anexoGq07C701b_label.setText(bundle.getString("Quadro07Panel.anexoGq07C701b_label.text"));
        this.anexoGq07C701b_label.setBorder(new EtchedBorder());
        this.anexoGq07C701b_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq07C701b_label, cc.xywh(4, 2, 3, 1));
        this.anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label.setText(bundle.getString("Quadro07Panel.anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label.text"));
        this.anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label.setBorder(new EtchedBorder());
        this.anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq07C701c_label\u00a3anexoGq07C702c_label\u00a3anexoGq07C1c_label, cc.xy(8, 2));
        this.anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label.setText(bundle.getString("Quadro07Panel.anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label.text"));
        this.anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label.setBorder(new EtchedBorder());
        this.anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq07C701d_label\u00a3anexoGq07C702d_label\u00a3anexoGq07C1d_label, cc.xy(10, 2));
        this.anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num.setText(bundle.getString("Quadro07Panel.anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num.text"));
        this.anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num.setBorder(new EtchedBorder());
        this.anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq07C701b_num\u00a3anexoGq07C701c_num\u00a3anexoGq07C701d_num, cc.xy(4, 4));
        this.panel3.add((Component)this.anexoGq07C701b, cc.xy(6, 4));
        this.anexoGq07C701c.setColumns(13);
        this.panel3.add((Component)this.anexoGq07C701c, cc.xy(8, 4));
        this.anexoGq07C701d.setColumns(13);
        this.panel3.add((Component)this.anexoGq07C701d, cc.xy(10, 4));
        this.anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num.setText(bundle.getString("Quadro07Panel.anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num.text"));
        this.anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num.setBorder(new EtchedBorder());
        this.anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq07C702b_num\u00a3anexoGq07C702c_num\u00a3anexoGq07C702d_num, cc.xy(4, 6));
        this.panel3.add((Component)this.anexoGq07C702b, cc.xy(6, 6));
        this.anexoGq07C702c.setColumns(13);
        this.panel3.add((Component)this.anexoGq07C702c, cc.xy(8, 6));
        this.anexoGq07C702d.setColumns(13);
        this.panel3.add((Component)this.anexoGq07C702d, cc.xy(10, 6));
        this.anexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label.setText(bundle.getString("Quadro07Panel.anexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label.text"));
        this.anexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq07C1c_base_label\u00a3anexoGq07C1d_base_label, cc.xy(6, 8));
        this.anexoGq07C1c.setColumns(14);
        this.anexoGq07C1c.setEditable(false);
        this.panel3.add((Component)this.anexoGq07C1c, cc.xy(8, 8));
        this.anexoGq07C1d.setEditable(false);
        this.anexoGq07C1d.setColumns(14);
        this.panel3.add((Component)this.anexoGq07C1d, cc.xy(10, 8));
        this.this2.add((Component)this.panel3, cc.xy(2, 3));
        this.add((Component)this.this2, "Center");
    }
}

