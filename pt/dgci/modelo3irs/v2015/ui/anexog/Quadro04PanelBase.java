/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro04Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro04PanelBase
extends Quadro04Panel
implements IBindablePanel<Quadro04> {
    private static final long serialVersionUID = 1;
    protected Quadro04 model;

    @Override
    public abstract void setModel(Quadro04 var1, boolean var2);

    @Override
    public Quadro04 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoGq04T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoGq04T1().add(new AnexoGq04T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoGq04T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoGq04T1.getSelectedRow() != -1 ? this.anexoGq04T1.getSelectedRow() : this.anexoGq04T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoGq04T1().remove(selectedRow);
            }
        }
    }

    @Override
    protected void addLineAnexoGq04AT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoGq04AT1().add(new AnexoGq04AT1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoGq04AT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoGq04AT1.getSelectedRow() != -1 ? this.anexoGq04AT1.getSelectedRow() : this.anexoGq04AT1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoGq04AT1().remove(selectedRow);
            }
        }
    }

    @Override
    protected void addLineAnexoGq04T2_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoGq04T2().add(new AnexoGq04T2_Linha());
        }
    }

    @Override
    protected void removeLineAnexoGq04T2_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoGq04T2.getSelectedRow() != -1 ? this.anexoGq04T2.getSelectedRow() : this.anexoGq04T2.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoGq04T2().remove(selectedRow);
            }
        }
    }

    public Quadro04PanelBase(Quadro04 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

