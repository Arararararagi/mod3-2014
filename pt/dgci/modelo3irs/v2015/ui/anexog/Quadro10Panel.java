/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro10Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel5;
    protected JLabel anexoGq10C_base_label;
    protected JLabel anexoGq10C1001a_label\u00a3anexoGq10C1002a_label;
    protected JLabel anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label;
    protected JLabel anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label;
    protected JLabel anexoGq10C1001a_base_label\u00a3anexoGq10C1001b_base_label\u00a3anexoGq10C1001c_base_label;
    protected JLabelTextFieldNumbering anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num;
    protected JEditableComboBox anexoGq10C1001a;
    protected JMoneyTextField anexoGq10C1001b;
    protected JMoneyTextField anexoGq10C1001c;
    protected JLabel anexoGq10C1002a_base_label\u00a3anexoGq10C1002b_base_label\u00a3anexoGq10C1002c_base_label;
    protected JLabelTextFieldNumbering anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num;
    protected JEditableComboBox anexoGq10C1002a;
    protected JMoneyTextField anexoGq10C1002b;
    protected JMoneyTextField anexoGq10C1002c;
    protected JLabel anexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label;
    protected JMoneyTextField anexoGq10C1b;
    protected JMoneyTextField anexoGq10C1c;
    protected JPanel panel12;
    protected JLabel label1;
    protected JToolBar toolBar4;
    protected JAddLineIconableButton button7;
    protected JRemoveLineIconableButton button8;
    protected JScrollPane anexoGq10T1Scroll;
    protected JTable anexoGq10T1;

    public Quadro10Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getAnexoGq10C1001a_label\u00a3anexoGq10C1002a_label() {
        return this.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label;
    }

    public JLabel getAnexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label() {
        return this.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label;
    }

    public JLabel getAnexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label() {
        return this.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label;
    }

    public JLabel getAnexoGq10C1001a_base_label\u00a3anexoGq10C1001b_base_label\u00a3anexoGq10C1001c_base_label() {
        return this.anexoGq10C1001a_base_label\u00a3anexoGq10C1001b_base_label\u00a3anexoGq10C1001c_base_label;
    }

    public JLabelTextFieldNumbering getAnexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num() {
        return this.anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num;
    }

    public JEditableComboBox getAnexoGq10C1001a() {
        return this.anexoGq10C1001a;
    }

    public JMoneyTextField getAnexoGq10C1001b() {
        return this.anexoGq10C1001b;
    }

    public JMoneyTextField getAnexoGq10C1001c() {
        return this.anexoGq10C1001c;
    }

    public JLabel getAnexoGq10C1002a_base_label\u00a3anexoGq10C1002b_base_label\u00a3anexoGq10C1002c_base_label() {
        return this.anexoGq10C1002a_base_label\u00a3anexoGq10C1002b_base_label\u00a3anexoGq10C1002c_base_label;
    }

    public JLabelTextFieldNumbering getAnexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num() {
        return this.anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num;
    }

    public JEditableComboBox getAnexoGq10C1002a() {
        return this.anexoGq10C1002a;
    }

    public JMoneyTextField getAnexoGq10C1002b() {
        return this.anexoGq10C1002b;
    }

    public JMoneyTextField getAnexoGq10C1002c() {
        return this.anexoGq10C1002c;
    }

    public JLabel getAnexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label() {
        return this.anexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label;
    }

    public JMoneyTextField getAnexoGq10C1b() {
        return this.anexoGq10C1b;
    }

    public JMoneyTextField getAnexoGq10C1c() {
        return this.anexoGq10C1c;
    }

    public JLabel getAnexoGq10C_base_label() {
        return this.anexoGq10C_base_label;
    }

    protected void addLineAnexoGq04AT1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq04AT1_LinhaActionPerformed(ActionEvent e) {
    }

    public JScrollPane getAnexoGq10T1Scroll() {
        return this.anexoGq10T1Scroll;
    }

    public JTable getAnexoGq10T1() {
        return this.anexoGq10T1;
    }

    protected void addLineAnexoGq10T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq10T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel12() {
        return this.panel12;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JToolBar getToolBar4() {
        return this.toolBar4;
    }

    public JAddLineIconableButton getButton7() {
        return this.button7;
    }

    public JRemoveLineIconableButton getButton8() {
        return this.button8;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel5 = new JPanel();
        this.anexoGq10C_base_label = new JLabel();
        this.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label = new JLabel();
        this.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label = new JLabel();
        this.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label = new JLabel();
        this.anexoGq10C1001a_base_label\u00a3anexoGq10C1001b_base_label\u00a3anexoGq10C1001c_base_label = new JLabel();
        this.anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num = new JLabelTextFieldNumbering();
        this.anexoGq10C1001a = new JEditableComboBox();
        this.anexoGq10C1001b = new JMoneyTextField();
        this.anexoGq10C1001c = new JMoneyTextField();
        this.anexoGq10C1002a_base_label\u00a3anexoGq10C1002b_base_label\u00a3anexoGq10C1002c_base_label = new JLabel();
        this.anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num = new JLabelTextFieldNumbering();
        this.anexoGq10C1002a = new JEditableComboBox();
        this.anexoGq10C1002b = new JMoneyTextField();
        this.anexoGq10C1002c = new JMoneyTextField();
        this.anexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label = new JLabel();
        this.anexoGq10C1b = new JMoneyTextField();
        this.anexoGq10C1c = new JMoneyTextField();
        this.panel12 = new JPanel();
        this.label1 = new JLabel();
        this.toolBar4 = new JToolBar();
        this.button7 = new JAddLineIconableButton();
        this.button8 = new JRemoveLineIconableButton();
        this.anexoGq10T1Scroll = new JScrollPane();
        this.anexoGq10T1 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro10Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro10Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "4*(default, $lgap), default"));
        this.panel5.setPreferredSize(null);
        this.panel5.setMinimumSize(null);
        this.panel5.setLayout(new FormLayout("$rgap, default:grow, $lcgap, 0px, 25dlu, 0px, default, $lcgap, 0px, 80dlu, $lcgap, 75dlu, $rgap, 0px", "4*(default, $lgap), default"));
        this.anexoGq10C_base_label.setText(bundle.getString("Quadro10Panel.anexoGq10C_base_label.text"));
        this.anexoGq10C_base_label.setBorder(new EtchedBorder());
        this.anexoGq10C_base_label.setHorizontalAlignment(0);
        this.anexoGq10C_base_label.setHorizontalTextPosition(0);
        this.panel5.add((Component)this.anexoGq10C_base_label, cc.xywh(1, 1, 2, 1));
        this.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label.setText(bundle.getString("Quadro10Panel.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label.text"));
        this.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label.setBorder(new EtchedBorder());
        this.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label.setHorizontalAlignment(0);
        this.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label.setHorizontalTextPosition(0);
        this.panel5.add((Component)this.anexoGq10C1001a_label\u00a3anexoGq10C1002a_label, cc.xywh(5, 1, 3, 1));
        this.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label.setText(bundle.getString("Quadro10Panel.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label.text"));
        this.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label.setBorder(new EtchedBorder());
        this.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label.setHorizontalAlignment(0);
        this.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label.setHorizontalTextPosition(0);
        this.panel5.add((Component)this.anexoGq10C1001b_label\u00a3anexoGq10C1002b_label\u00a3anexoGq10C1b_label, cc.xy(10, 1));
        this.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label.setText(bundle.getString("Quadro10Panel.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label.text"));
        this.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label.setBorder(new EtchedBorder());
        this.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label.setHorizontalAlignment(0);
        this.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label.setHorizontalTextPosition(0);
        this.panel5.add((Component)this.anexoGq10C1001c_label\u00a3anexoGq10C1002c_label\u00a3anexoGq10C1c_label, cc.xy(12, 1));
        this.anexoGq10C1001a_base_label\u00a3anexoGq10C1001b_base_label\u00a3anexoGq10C1001c_base_label.setText(bundle.getString("Quadro10Panel.anexoGq10C1001a_base_label\u00a3anexoGq10C1001b_base_label\u00a3anexoGq10C1001c_base_label.text"));
        this.panel5.add((Component)this.anexoGq10C1001a_base_label\u00a3anexoGq10C1001b_base_label\u00a3anexoGq10C1001c_base_label, cc.xy(2, 3));
        this.anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num.setText(bundle.getString("Quadro10Panel.anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num.text"));
        this.anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num.setBorder(new EtchedBorder());
        this.anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq10C1001a_num\u00a3anexoGq10C1001b_num\u00a3anexoGq10C1001c_num, cc.xy(5, 3));
        this.panel5.add((Component)this.anexoGq10C1001a, cc.xy(7, 3));
        this.anexoGq10C1001b.setColumns(13);
        this.panel5.add((Component)this.anexoGq10C1001b, cc.xy(10, 3));
        this.anexoGq10C1001c.setColumns(13);
        this.panel5.add((Component)this.anexoGq10C1001c, cc.xy(12, 3));
        this.anexoGq10C1002a_base_label\u00a3anexoGq10C1002b_base_label\u00a3anexoGq10C1002c_base_label.setText(bundle.getString("Quadro10Panel.anexoGq10C1002a_base_label\u00a3anexoGq10C1002b_base_label\u00a3anexoGq10C1002c_base_label.text"));
        this.panel5.add((Component)this.anexoGq10C1002a_base_label\u00a3anexoGq10C1002b_base_label\u00a3anexoGq10C1002c_base_label, cc.xy(2, 5));
        this.anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num.setText(bundle.getString("Quadro10Panel.anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num.text"));
        this.anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num.setBorder(new EtchedBorder());
        this.anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq10C1002a_num\u00a3anexoGq10C1002b_num\u00a3anexoGq10C1002c_num, cc.xy(5, 5));
        this.panel5.add((Component)this.anexoGq10C1002a, cc.xy(7, 5));
        this.anexoGq10C1002b.setColumns(13);
        this.panel5.add((Component)this.anexoGq10C1002b, cc.xy(10, 5));
        this.anexoGq10C1002c.setColumns(13);
        this.panel5.add((Component)this.anexoGq10C1002c, cc.xy(12, 5));
        this.anexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label.setText(bundle.getString("Quadro10Panel.anexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label.text"));
        this.anexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq10C1b_base_label\u00a3anexoGq10C1c_base_label, cc.xy(7, 7));
        this.anexoGq10C1b.setEditable(false);
        this.anexoGq10C1b.setColumns(14);
        this.panel5.add((Component)this.anexoGq10C1b, cc.xy(10, 7));
        this.anexoGq10C1c.setEditable(false);
        this.anexoGq10C1c.setColumns(14);
        this.panel5.add((Component)this.anexoGq10C1c, cc.xy(12, 7));
        this.this2.add((Component)this.panel5, cc.xy(2, 3));
        this.panel12.setMinimumSize(null);
        this.panel12.setPreferredSize(null);
        this.panel12.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 50dlu, $lcgap, default:grow, $rgap", "3*(default, $lgap), 231dlu, $lgap, default"));
        this.label1.setText(bundle.getString("Quadro10Panel.label1.text"));
        this.label1.setHorizontalAlignment(0);
        this.label1.setBorder(new EtchedBorder());
        this.panel12.add((Component)this.label1, cc.xywh(1, 1, 9, 1));
        this.toolBar4.setFloatable(false);
        this.toolBar4.setRollover(true);
        this.button7.setText(bundle.getString("Quadro10Panel.button7.text"));
        this.button7.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro10Panel.this.addLineAnexoGq10T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar4.add(this.button7);
        this.button8.setText(bundle.getString("Quadro10Panel.button8.text"));
        this.button8.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro10Panel.this.removeLineAnexoGq10T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar4.add(this.button8);
        this.panel12.add((Component)this.toolBar4, cc.xy(3, 5));
        this.anexoGq10T1Scroll.setViewportView(this.anexoGq10T1);
        this.panel12.add((Component)this.anexoGq10T1Scroll, cc.xywh(3, 7, 5, 1));
        this.this2.add((Component)this.panel12, cc.xy(2, 9));
        this.add((Component)this.this2, "Center");
    }

}

