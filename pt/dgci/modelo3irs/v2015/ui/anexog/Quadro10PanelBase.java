/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro10Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro10PanelBase
extends Quadro10Panel
implements IBindablePanel<Quadro10> {
    private static final long serialVersionUID = 1;
    protected Quadro10 model;

    @Override
    public abstract void setModel(Quadro10 var1, boolean var2);

    @Override
    public Quadro10 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoGq10T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoGq10T1().add(new AnexoGq10T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoGq10T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoGq10T1.getSelectedRow() != -1 ? this.anexoGq10T1.getSelectedRow() : this.anexoGq10T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoGq10T1().remove(selectedRow);
            }
        }
    }

    public Quadro10PanelBase(Quadro10 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

