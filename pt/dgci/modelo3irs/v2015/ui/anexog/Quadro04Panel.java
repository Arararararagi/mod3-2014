/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoGq04T1Scroll;
    protected JTable anexoGq04T1;
    protected JPanel panel4;
    protected JLabel anexoGq04C1_label;
    protected JLabel anexoGq04C2_label;
    protected JLabel anexoGq04C3_label;
    protected JLabel anexoGq04C1_base_label\u00a3anexoGq04C2_base_label\u00a3anexoGq04C3_base_label;
    protected JMoneyTextField anexoGq04C1;
    protected JMoneyTextField anexoGq04C2;
    protected JMoneyTextField anexoGq04C3;
    protected JPanel panel5;
    protected JLabel label13;
    protected JLabel label3;
    protected JPanel panel10;
    protected JLabel anexoFq05C1_base_label;
    protected JPanel panel11;
    protected JToolBar toolBar3;
    protected JAddLineIconableButton button5;
    protected JRemoveLineIconableButton button6;
    protected JScrollPane anexoGq04AT1Scroll;
    protected JTable anexoGq04AT1;
    protected JPanel panel9;
    protected JLabel anexoGq04B1OPSim_base_label;
    protected JLabelTextFieldNumbering label22;
    protected JRadioButton anexoGq04B1OPSim;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton anexoGq04B1OPNao;
    protected JPanel panel6;
    protected JLabel label14;
    protected JLabel label4;
    protected JPanel panel8;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoGq04T2Scroll;
    protected JTable anexoGq04T2;

    public Quadro04Panel() {
        this.initComponents();
    }

    protected void addLineAnexoGq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoGq04T1Scroll() {
        return this.anexoGq04T1Scroll;
    }

    public JTable getAnexoGq04T1() {
        return this.anexoGq04T1;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoGq04C1_label() {
        return this.anexoGq04C1_label;
    }

    public JLabel getAnexoGq04C2_label() {
        return this.anexoGq04C2_label;
    }

    public JLabel getAnexoGq04C3_label() {
        return this.anexoGq04C3_label;
    }

    public JLabel getAnexoGq04C1_base_label\u00a3anexoGq04C2_base_label\u00a3anexoGq04C3_base_label() {
        return this.anexoGq04C1_base_label\u00a3anexoGq04C2_base_label\u00a3anexoGq04C3_base_label;
    }

    public JMoneyTextField getAnexoGq04C1() {
        return this.anexoGq04C1;
    }

    public JMoneyTextField getAnexoGq04C2() {
        return this.anexoGq04C2;
    }

    public JMoneyTextField getAnexoGq04C3() {
        return this.anexoGq04C3;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabel getAnexoGq04B1OPSim_base_label() {
        return this.anexoGq04B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JRadioButton getAnexoGq04B1OPSim() {
        return this.anexoGq04B1OPSim;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getAnexoGq04B1OPNao() {
        return this.anexoGq04B1OPNao;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    protected void addLineAnexoGq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoGq04T2Scroll() {
        return this.anexoGq04T2Scroll;
    }

    public JTable getAnexoGq04T2() {
        return this.anexoGq04T2;
    }

    protected void addLineAnexoGq04AT1_Linha(ActionEvent e) {
    }

    protected void removeLineAnexoFq05T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabel getAnexoFq05C1_base_label() {
        return this.anexoFq05C1_base_label;
    }

    public JPanel getPanel11() {
        return this.panel11;
    }

    public JToolBar getToolBar3() {
        return this.toolBar3;
    }

    public JAddLineIconableButton getButton5() {
        return this.button5;
    }

    public JRemoveLineIconableButton getButton6() {
        return this.button6;
    }

    public JScrollPane getAnexoGq04AT1Scroll() {
        return this.anexoGq04AT1Scroll;
    }

    public JTable getAnexoGq04AT1() {
        return this.anexoGq04AT1;
    }

    protected void addLineAnexoGq04AT1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq04AT1_LinhaActionPerformed(ActionEvent e) {
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoGq04T1Scroll = new JScrollPane();
        this.anexoGq04T1 = new JTable();
        this.panel4 = new JPanel();
        this.anexoGq04C1_label = new JLabel();
        this.anexoGq04C2_label = new JLabel();
        this.anexoGq04C3_label = new JLabel();
        this.anexoGq04C1_base_label\u00a3anexoGq04C2_base_label\u00a3anexoGq04C3_base_label = new JLabel();
        this.anexoGq04C1 = new JMoneyTextField();
        this.anexoGq04C2 = new JMoneyTextField();
        this.anexoGq04C3 = new JMoneyTextField();
        this.panel5 = new JPanel();
        this.label13 = new JLabel();
        this.label3 = new JLabel();
        this.panel10 = new JPanel();
        this.anexoFq05C1_base_label = new JLabel();
        this.panel11 = new JPanel();
        this.toolBar3 = new JToolBar();
        this.button5 = new JAddLineIconableButton();
        this.button6 = new JRemoveLineIconableButton();
        this.anexoGq04AT1Scroll = new JScrollPane();
        this.anexoGq04AT1 = new JTable();
        this.panel9 = new JPanel();
        this.anexoGq04B1OPSim_base_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoGq04B1OPSim = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.anexoGq04B1OPNao = new JRadioButton();
        this.panel6 = new JPanel();
        this.label14 = new JLabel();
        this.label4 = new JLabel();
        this.panel8 = new JPanel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoGq04T2Scroll = new JScrollPane();
        this.anexoGq04T2 = new JTable();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 10*(default, $lgap), default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 620dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoGq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoGq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, CC.xy(3, 2));
        this.anexoGq04T1Scroll.setViewportView(this.anexoGq04T1);
        this.panel3.add((Component)this.anexoGq04T1Scroll, CC.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel3, CC.xy(2, 4));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, default, 3*($lcgap, 80dlu), $lcgap, default:grow", "3*(default, $lgap), default"));
        this.anexoGq04C1_label.setText(bundle.getString("Quadro04Panel.anexoGq04C1_label.text"));
        this.anexoGq04C1_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq04C1_label, CC.xywh(5, 1, 2, 1));
        this.anexoGq04C2_label.setText(bundle.getString("Quadro04Panel.anexoGq04C2_label.text"));
        this.anexoGq04C2_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq04C2_label, CC.xy(7, 1));
        this.anexoGq04C3_label.setText(bundle.getString("Quadro04Panel.anexoGq04C3_label.text"));
        this.anexoGq04C3_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq04C3_label, CC.xy(9, 1));
        this.anexoGq04C1_base_label\u00a3anexoGq04C2_base_label\u00a3anexoGq04C3_base_label.setText(bundle.getString("Quadro04Panel.anexoGq04C1_base_label\u00a3anexoGq04C2_base_label\u00a3anexoGq04C3_base_label.text"));
        this.panel4.add((Component)this.anexoGq04C1_base_label\u00a3anexoGq04C2_base_label\u00a3anexoGq04C3_base_label, CC.xy(3, 3));
        this.anexoGq04C1.setEditable(false);
        this.anexoGq04C1.setColumns(15);
        this.panel4.add((Component)this.anexoGq04C1, CC.xy(5, 3));
        this.anexoGq04C2.setEditable(false);
        this.anexoGq04C2.setColumns(15);
        this.panel4.add((Component)this.anexoGq04C2, CC.xy(7, 3));
        this.anexoGq04C3.setEditable(false);
        this.anexoGq04C3.setColumns(15);
        this.panel4.add((Component)this.anexoGq04C3, CC.xy(9, 3));
        this.this2.add((Component)this.panel4, CC.xy(2, 6));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap", "default"));
        this.label13.setText(bundle.getString("Quadro04Panel.label13.text"));
        this.label13.setBorder(new EtchedBorder());
        this.label13.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label13, CC.xy(1, 1));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel5.add((Component)this.label3, CC.xy(3, 1));
        this.this2.add((Component)this.panel5, CC.xy(2, 8));
        this.panel10.setMinimumSize(null);
        this.panel10.setPreferredSize(null);
        this.panel10.setLayout(new FormLayout("$rgap, default:grow, $rgap, 13dlu, 0px, $lcgap, default, $lcgap, default:grow, 0px, $ugap, 0px", "$rgap, 0px, default, $lgap, default"));
        this.anexoFq05C1_base_label.setText(bundle.getString("Quadro04Panel.anexoFq05C1_base_label.text"));
        this.panel10.add((Component)this.anexoFq05C1_base_label, CC.xywh(2, 3, 8, 1));
        this.this2.add((Component)this.panel10, CC.xy(2, 10));
        this.panel11.setMinimumSize(null);
        this.panel11.setPreferredSize(null);
        this.panel11.setLayout(new FormLayout("default:grow, $lcgap, min, $lcgap, default:grow, $rgap", "2*(default, $lgap), 165dlu, $lgap, default"));
        this.toolBar3.setFloatable(false);
        this.toolBar3.setRollover(true);
        this.button5.setText(bundle.getString("Quadro04Panel.button5.text"));
        this.button5.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoGq04AT1_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button5);
        this.button6.setText(bundle.getString("Quadro04Panel.button6.text"));
        this.button6.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoGq04AT1_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button6);
        this.panel11.add((Component)this.toolBar3, CC.xy(3, 3));
        this.anexoGq04AT1Scroll.setViewportView(this.anexoGq04AT1);
        this.panel11.add((Component)this.anexoGq04AT1Scroll, CC.xywh(3, 5, 2, 1));
        this.this2.add((Component)this.panel11, CC.xy(2, 12));
        this.panel9.setLayout(new FormLayout("2*(default, $lcgap), 25dlu, $lcgap, 13dlu, 0px, default, $rgap, default, $lcgap, 13dlu, 0px, default, $lcgap, default:grow, $ugap", "$ugap, 2*($lgap, default)"));
        this.anexoGq04B1OPSim_base_label.setText(bundle.getString("Quadro04Panel.anexoGq04B1OPSim_base_label.text"));
        this.anexoGq04B1OPSim_base_label.setHorizontalAlignment(4);
        this.panel9.add((Component)this.anexoGq04B1OPSim_base_label, CC.xy(3, 3));
        this.label22.setText(bundle.getString("Quadro04Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel9.add((Component)this.label22, CC.xy(7, 3));
        this.anexoGq04B1OPSim.setText(bundle.getString("Quadro04Panel.anexoGq04B1OPSim.text"));
        this.panel9.add((Component)this.anexoGq04B1OPSim, CC.xy(9, 3));
        this.label23.setText(bundle.getString("Quadro04Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel9.add((Component)this.label23, CC.xy(13, 3));
        this.anexoGq04B1OPNao.setText(bundle.getString("Quadro04Panel.anexoGq04B1OPNao.text"));
        this.panel9.add((Component)this.anexoGq04B1OPNao, CC.xy(15, 3));
        this.this2.add((Component)this.panel9, CC.xy(2, 16));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap", "default"));
        this.label14.setText(bundle.getString("Quadro04Panel.label14.text"));
        this.label14.setBorder(new EtchedBorder());
        this.label14.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label14, CC.xy(1, 1));
        this.label4.setText(bundle.getString("Quadro04Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label4, CC.xy(3, 1));
        this.this2.add((Component)this.panel6, CC.xy(2, 20));
        this.panel8.setMinimumSize(null);
        this.panel8.setPreferredSize(null);
        this.panel8.setLayout(new FormLayout("3*(default, $lcgap), 385dlu:grow, $rgap, default", "$rgap, default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button3.setText(bundle.getString("Quadro04Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoGq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro04Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoGq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel8.add((Component)this.toolBar2, CC.xy(3, 2));
        this.anexoGq04T2Scroll.setViewportView(this.anexoGq04T2);
        this.panel8.add((Component)this.anexoGq04T2Scroll, CC.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel8, CC.xy(2, 22));
        this.add((Component)this.this2, "Center");
    }

}

