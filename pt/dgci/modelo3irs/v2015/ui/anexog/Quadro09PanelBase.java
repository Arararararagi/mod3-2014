/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro09Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro09PanelBase
extends Quadro09Panel
implements IBindablePanel<Quadro09> {
    private static final long serialVersionUID = 1;
    protected Quadro09 model;

    @Override
    public abstract void setModel(Quadro09 var1, boolean var2);

    @Override
    public Quadro09 getModel() {
        return this.model;
    }

    public Quadro09PanelBase(Quadro09 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

