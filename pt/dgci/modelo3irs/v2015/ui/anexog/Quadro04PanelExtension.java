/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro04PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q04_T1 = 93;
    public static final int MAX_LINES_Q04A_T1 = 20;
    public static final int MAX_LINES_Q04_T2 = 20;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeaderT1();
                this.setHeaderT2();
            }
        }
    }

    private void setHeaderT1() {
        TableColumnModel cm1 = this.getAnexoGq04T1().getTableHeader().getColumnModel();
        ColumnGroup g_Realizacao = new ColumnGroup("Realiza\u00e7\u00e3o");
        g_Realizacao.add(cm1.getColumn(3));
        g_Realizacao.add(cm1.getColumn(4));
        g_Realizacao.add(cm1.getColumn(5));
        ColumnGroup g_Aquisicao = new ColumnGroup("Aquisi\u00e7\u00e3o");
        g_Aquisicao.add(cm1.getColumn(6));
        g_Aquisicao.add(cm1.getColumn(7));
        g_Aquisicao.add(cm1.getColumn(8));
        ColumnGroup g_IdentificacaoBens = new ColumnGroup("Identifica\u00e7\u00e3o Matricial dos Bens");
        g_IdentificacaoBens.add(cm1.getColumn(10));
        g_IdentificacaoBens.add(cm1.getColumn(11));
        g_IdentificacaoBens.add(cm1.getColumn(12));
        g_IdentificacaoBens.add(cm1.getColumn(13));
        if (this.getAnexoGq04T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoGq04T1().getTableHeader()).addColumnGroup(g_Realizacao);
            ((GroupableTableHeader)this.getAnexoGq04T1().getTableHeader()).addColumnGroup(g_Aquisicao);
            ((GroupableTableHeader)this.getAnexoGq04T1().getTableHeader()).addColumnGroup(g_IdentificacaoBens);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm1);
        header.addColumnGroup(g_Realizacao);
        header.addColumnGroup(g_Aquisicao);
        header.addColumnGroup(g_IdentificacaoBens);
        this.getAnexoGq04T1().setTableHeader(header);
    }

    private void setHeaderT2() {
        TableColumnModel cm2 = this.getAnexoGq04T2().getTableHeader().getColumnModel();
        ColumnGroup g_Bens = new ColumnGroup("Bens");
        g_Bens.add(cm2.getColumn(2));
        ColumnGroup g_Afectacao = new ColumnGroup("Afeta\u00e7\u00e3o");
        g_Afectacao.add(cm2.getColumn(3));
        g_Afectacao.add(cm2.getColumn(4));
        g_Afectacao.add(cm2.getColumn(5));
        ColumnGroup g_Aquisicao = new ColumnGroup("Aquisi\u00e7\u00e3o");
        g_Aquisicao.add(cm2.getColumn(6));
        g_Aquisicao.add(cm2.getColumn(7));
        g_Aquisicao.add(cm2.getColumn(8));
        ColumnGroup g_IdentBensImoveis = new ColumnGroup("Identifica\u00e7\u00e3o Matricial dos Bens Im\u00f3veis");
        g_IdentBensImoveis.add(cm2.getColumn(9));
        g_IdentBensImoveis.add(cm2.getColumn(10));
        g_IdentBensImoveis.add(cm2.getColumn(11));
        g_IdentBensImoveis.add(cm2.getColumn(12));
        if (this.getAnexoGq04T2().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoGq04T2().getTableHeader()).addColumnGroup(g_Bens);
            ((GroupableTableHeader)this.getAnexoGq04T2().getTableHeader()).addColumnGroup(g_Afectacao);
            ((GroupableTableHeader)this.getAnexoGq04T2().getTableHeader()).addColumnGroup(g_Aquisicao);
            ((GroupableTableHeader)this.getAnexoGq04T2().getTableHeader()).addColumnGroup(g_IdentBensImoveis);
            return;
        }
        GroupableTableHeader header_1 = new GroupableTableHeader(cm2);
        header_1.addColumnGroup(g_Bens);
        header_1.addColumnGroup(g_Afectacao);
        header_1.addColumnGroup(g_Aquisicao);
        header_1.addColumnGroup(g_IdentBensImoveis);
        this.getAnexoGq04T2().setTableHeader(header_1);
    }

    @Override
    protected void addLineAnexoGq04T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoGq04T1() != null && this.model.getAnexoGq04T1().size() >= 93) {
            return;
        }
        super.addLineAnexoGq04T1_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoGq04AT1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoGq04AT1() != null && this.model.getAnexoGq04AT1().size() >= 20) {
            return;
        }
        super.addLineAnexoGq04AT1_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoGq04T2_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoGq04T2() != null && this.model.getAnexoGq04T2().size() >= 20) {
            return;
        }
        super.addLineAnexoGq04T2_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoGModel anexoG = (AnexoGModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoGModel.class);
            if (anexoG != null) {
                EventList<AnexoGq04T1_Linha> anexoGq04T1Rows = anexoG.getQuadro04().getAnexoGq04T1();
                EventList<AnexoGq04T2_Linha> anexoGq04T2Rows = anexoG.getQuadro04().getAnexoGq04T2();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInGq04T1(anexoGq04T1Rows, label);
                        this.removeNifByLabelInGq04T2(anexoGq04T2Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInGq04T1(anexoGq04T1Rows, label + (index + 1));
                        this.removeNifByLabelInGq04T2(anexoGq04T2Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexGq04T1Elements(anexoGq04T1Rows, label, index);
                            this.reindexGq04T2Elements(anexoGq04T2Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInGq04T1(anexoGq04T1Rows, label);
                    this.removeNifByLabelInGq04T2(anexoGq04T2Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInGq04T2(EventList<AnexoGq04T2_Linha> anexoGq04T2Rows, String labelPrefix) {
        if (anexoGq04T2Rows != null) {
            for (AnexoGq04T2_Linha anexoGq04T2_Linha : anexoGq04T2Rows) {
                if (anexoGq04T2_Linha == null || anexoGq04T2_Linha.getTitular() == null || !anexoGq04T2_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoGq04T2_Linha.setTitular(null);
            }
        }
    }

    private void removeNifByLabelInGq04T1(EventList<AnexoGq04T1_Linha> anexoGq04T1Rows, String labelPrefix) {
        if (anexoGq04T1Rows != null) {
            for (AnexoGq04T1_Linha anexoGq04T1_Linha : anexoGq04T1Rows) {
                if (anexoGq04T1_Linha == null || anexoGq04T1_Linha.getTitular() == null || !anexoGq04T1_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoGq04T1_Linha.setTitular(null);
            }
        }
    }

    private void reindexGq04T2Elements(EventList<AnexoGq04T2_Linha> anexoGq04T2Rows, String labelPrefix, int index) {
        if (anexoGq04T2Rows != null) {
            for (AnexoGq04T2_Linha anexoGq04T2_Linha : anexoGq04T2Rows) {
                int currIndex;
                if (anexoGq04T2_Linha == null || anexoGq04T2_Linha.getTitular() == null || !anexoGq04T2_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoGq04T2_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoGq04T2_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }

    private void reindexGq04T1Elements(EventList<AnexoGq04T1_Linha> anexoGq04T1Rows, String labelPrefix, int index) {
        if (anexoGq04T1Rows != null) {
            for (AnexoGq04T1_Linha anexoGq04T1_Linha : anexoGq04T1Rows) {
                int currIndex;
                if (anexoGq04T1_Linha == null || anexoGq04T1_Linha.getTitular() == null || !anexoGq04T1_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoGq04T1_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoGq04T1_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

