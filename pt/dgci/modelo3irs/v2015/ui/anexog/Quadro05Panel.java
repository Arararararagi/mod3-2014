/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JPercentTextField;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoGq05C501_label;
    protected JLabelTextFieldNumbering anexoGq05C501_num;
    protected JLimitedTextField anexoGq05C501;
    protected JLabel anexoGq05C502_label\u00a3anexoGq05C503_label\u00a3anexoGq05C504_label;
    protected JLabelTextFieldNumbering anexoGq05C502_num;
    protected JEditableComboBox anexoGq05C502;
    protected JLabelTextFieldNumbering anexoGq05C503_num;
    protected JEditableComboBox anexoGq05C503;
    protected JLabelTextFieldNumbering anexoGq05C504_num;
    protected JEditableComboBox anexoGq05C504;
    protected JPanel panel3;
    protected JLabel label2;
    protected JLabel anexoGq05C505_label;
    protected JLabelTextFieldNumbering anexoGq05C505_num;
    protected JMoneyTextField anexoGq05C505;
    protected JLabel anexoGq05C506_label;
    protected JLabelTextFieldNumbering anexoGq05C506_num;
    protected JMoneyTextField anexoGq05C506;
    protected JLabel label6;
    protected JLabel anexoGq05C507_label;
    protected JLabelTextFieldNumbering anexoGq05C507_num;
    protected JMoneyTextField anexoGq05C507;
    protected JLabel label7;
    protected JLabel anexoGq05C508_label;
    protected JLabelTextFieldNumbering anexoGq05C508_num;
    protected JMoneyTextField anexoGq05C508;
    protected JLabel anexoGq05C509_label;
    protected JLabelTextFieldNumbering anexoGq05C509_num;
    protected JMoneyTextField anexoGq05C509;
    protected JLabel anexoGq05C510_label;
    protected JLabelTextFieldNumbering anexoGq05C510_num;
    protected JMoneyTextField anexoGq05C510;
    protected JLabel anexoGq05C511_label;
    protected JLabelTextFieldNumbering anexoGq05C511_num;
    protected JMoneyTextField anexoGq05C511;
    protected JPanel panel4;
    protected JLabel anexoGq05C521_label;
    protected JLabelTextFieldNumbering anexoGq05C521_num;
    protected JLimitedTextField anexoGq05C521;
    protected JLabel anexoGq05C522_label\u00a3anexoGq05C523_label\u00a3anexoGq05C524_label;
    protected JLabelTextFieldNumbering anexoGq05C522_num;
    protected JEditableComboBox anexoGq05C522;
    protected JLabelTextFieldNumbering anexoGq05C523_num;
    protected JEditableComboBox anexoGq05C523;
    protected JLabelTextFieldNumbering anexoGq05C524_num;
    protected JEditableComboBox anexoGq05C524;
    protected JPanel panel7;
    protected JLabel label8;
    protected JLabel anexoGq05C525_label;
    protected JLabelTextFieldNumbering anexoGq05C525_num;
    protected JMoneyTextField anexoGq05C525;
    protected JLabel anexoGq05C526_label;
    protected JLabelTextFieldNumbering anexoGq05C526_num;
    protected JMoneyTextField anexoGq05C526;
    protected JLabel label9;
    protected JLabel anexoGq05C527_label;
    protected JLabelTextFieldNumbering anexoGq05C527_num;
    protected JMoneyTextField anexoGq05C527;
    protected JLabel label10;
    protected JLabel anexoGq05C528_label;
    protected JLabelTextFieldNumbering anexoGq05C528_num;
    protected JMoneyTextField anexoGq05C528;
    protected JLabel anexoGq05C529_label;
    protected JLabelTextFieldNumbering anexoGq05C529_num;
    protected JMoneyTextField anexoGq05C529;
    protected JLabel anexoGq05C530_label;
    protected JLabelTextFieldNumbering anexoGq05C530_num;
    protected JMoneyTextField anexoGq05C530;
    protected JLabel anexoGq05C531_label;
    protected JLabelTextFieldNumbering anexoGq05C531_num;
    protected JMoneyTextField anexoGq05C531;
    protected JPanel panel6;
    protected JLabel label14;
    protected JLabel label4;
    protected JPanel panel9;
    protected JLabel anexoGq05C525_label2;
    protected JLabel anexoGq05C525_label3;
    protected JLabel anexoGq05C525_label8;
    protected JLabel anexoGq05C525_label4;
    protected JLabel anexoGq05C525_label5;
    protected JLabel anexoGq05C525_label6;
    protected JLabel anexoGq05C525_label7;
    protected JLabel anexoGq05AC1_base_label;
    protected JEditableComboBox anexoGq05AC1;
    protected JLimitedTextField anexoGq05AC2;
    protected JEditableComboBox anexoGq05AC3;
    protected JLimitedTextField anexoGq05AC4;
    protected JLimitedTextField anexoGq05AC5;
    protected JPercentTextField anexoGq05AC6;
    protected JLabel anexoGq05AC7_base_label;
    protected JEditableComboBox anexoGq05AC7;
    protected JLimitedTextField anexoGq05AC8;
    protected JEditableComboBox anexoGq05AC9;
    protected JLimitedTextField anexoGq05AC10;
    protected JLimitedTextField anexoGq05AC11;
    protected JPercentTextField anexoGq05AC12;
    protected JLabel anexoGq05C527_label3;
    protected JEditableComboBox anexoGq05AC13;

    public Quadro05Panel() {
        this.initComponents();
    }

    protected void addLineAnexoGq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoGq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoGq05C501_label() {
        return this.anexoGq05C501_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C501_num() {
        return this.anexoGq05C501_num;
    }

    public JLabel getAnexoGq05C502_label\u00a3anexoGq05C503_label\u00a3anexoGq05C504_label() {
        return this.anexoGq05C502_label\u00a3anexoGq05C503_label\u00a3anexoGq05C504_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C502_num() {
        return this.anexoGq05C502_num;
    }

    public JLabelTextFieldNumbering getAnexoGq05C503_num() {
        return this.anexoGq05C503_num;
    }

    public JLabelTextFieldNumbering getAnexoGq05C504_num() {
        return this.anexoGq05C504_num;
    }

    public JLimitedTextField getAnexoGq05C501() {
        return this.anexoGq05C501;
    }

    public JEditableComboBox getAnexoGq05C502() {
        return this.anexoGq05C502;
    }

    public JEditableComboBox getAnexoGq05C503() {
        return this.anexoGq05C503;
    }

    public JEditableComboBox getAnexoGq05C504() {
        return this.anexoGq05C504;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoGq05C505_label() {
        return this.anexoGq05C505_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C505_num() {
        return this.anexoGq05C505_num;
    }

    public JMoneyTextField getAnexoGq05C505() {
        return this.anexoGq05C505;
    }

    public JLabel getAnexoGq05C506_label() {
        return this.anexoGq05C506_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C506_num() {
        return this.anexoGq05C506_num;
    }

    public JMoneyTextField getAnexoGq05C506() {
        return this.anexoGq05C506;
    }

    public JLabel getAnexoGq05C507_label() {
        return this.anexoGq05C507_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C507_num() {
        return this.anexoGq05C507_num;
    }

    public JMoneyTextField getAnexoGq05C507() {
        return this.anexoGq05C507;
    }

    public JLabel getAnexoGq05C508_label() {
        return this.anexoGq05C508_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C508_num() {
        return this.anexoGq05C508_num;
    }

    public JMoneyTextField getAnexoGq05C508() {
        return this.anexoGq05C508;
    }

    public JLabel getAnexoGq05C509_label() {
        return this.anexoGq05C509_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C509_num() {
        return this.anexoGq05C509_num;
    }

    public JMoneyTextField getAnexoGq05C509() {
        return this.anexoGq05C509;
    }

    public JLabel getAnexoGq05C510_label() {
        return this.anexoGq05C510_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C510_num() {
        return this.anexoGq05C510_num;
    }

    public JMoneyTextField getAnexoGq05C510() {
        return this.anexoGq05C510;
    }

    public JLabel getAnexoGq05C511_label() {
        return this.anexoGq05C511_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C511_num() {
        return this.anexoGq05C511_num;
    }

    public JMoneyTextField getAnexoGq05C511() {
        return this.anexoGq05C511;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoGq05C521_label() {
        return this.anexoGq05C521_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C521_num() {
        return this.anexoGq05C521_num;
    }

    public JLimitedTextField getAnexoGq05C521() {
        return this.anexoGq05C521;
    }

    public JLabel getAnexoGq05C522_label\u00a3anexoGq05C523_label\u00a3anexoGq05C524_label() {
        return this.anexoGq05C522_label\u00a3anexoGq05C523_label\u00a3anexoGq05C524_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C522_num() {
        return this.anexoGq05C522_num;
    }

    public JEditableComboBox getAnexoGq05C522() {
        return this.anexoGq05C522;
    }

    public JLabelTextFieldNumbering getAnexoGq05C523_num() {
        return this.anexoGq05C523_num;
    }

    public JEditableComboBox getAnexoGq05C523() {
        return this.anexoGq05C523;
    }

    public JLabelTextFieldNumbering getAnexoGq05C524_num() {
        return this.anexoGq05C524_num;
    }

    public JEditableComboBox getAnexoGq05C524() {
        return this.anexoGq05C524;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoGq05C525_label() {
        return this.anexoGq05C525_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C525_num() {
        return this.anexoGq05C525_num;
    }

    public JMoneyTextField getAnexoGq05C525() {
        return this.anexoGq05C525;
    }

    public JLabel getAnexoGq05C526_label() {
        return this.anexoGq05C526_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C526_num() {
        return this.anexoGq05C526_num;
    }

    public JMoneyTextField getAnexoGq05C526() {
        return this.anexoGq05C526;
    }

    public JLabel getAnexoGq05C527_label() {
        return this.anexoGq05C527_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C527_num() {
        return this.anexoGq05C527_num;
    }

    public JMoneyTextField getAnexoGq05C527() {
        return this.anexoGq05C527;
    }

    public JLabel getAnexoGq05C528_label() {
        return this.anexoGq05C528_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C528_num() {
        return this.anexoGq05C528_num;
    }

    public JMoneyTextField getAnexoGq05C528() {
        return this.anexoGq05C528;
    }

    public JLabel getAnexoGq05C529_label() {
        return this.anexoGq05C529_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C529_num() {
        return this.anexoGq05C529_num;
    }

    public JMoneyTextField getAnexoGq05C529() {
        return this.anexoGq05C529;
    }

    public JLabel getAnexoGq05C530_label() {
        return this.anexoGq05C530_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C530_num() {
        return this.anexoGq05C530_num;
    }

    public JMoneyTextField getAnexoGq05C530() {
        return this.anexoGq05C530;
    }

    public JLabel getAnexoGq05C531_label() {
        return this.anexoGq05C531_label;
    }

    public JLabelTextFieldNumbering getAnexoGq05C531_num() {
        return this.anexoGq05C531_num;
    }

    public JMoneyTextField getAnexoGq05C531() {
        return this.anexoGq05C531;
    }

    public JLabel getAnexoGq05C527_label3() {
        return this.anexoGq05C527_label3;
    }

    public JEditableComboBox getAnexoGq05AC13() {
        return this.anexoGq05AC13;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabel getAnexoGq05C525_label2() {
        return this.anexoGq05C525_label2;
    }

    public JLabel getAnexoGq05C525_label3() {
        return this.anexoGq05C525_label3;
    }

    public JLabel getAnexoGq05C525_label4() {
        return this.anexoGq05C525_label4;
    }

    public JLabel getAnexoGq05C525_label5() {
        return this.anexoGq05C525_label5;
    }

    public JLabel getAnexoGq05C525_label6() {
        return this.anexoGq05C525_label6;
    }

    public JLabel getAnexoGq05C525_label7() {
        return this.anexoGq05C525_label7;
    }

    public JLabel getAnexoGq05AC1_base_label() {
        return this.anexoGq05AC1_base_label;
    }

    public JEditableComboBox getAnexoGq05AC1() {
        return this.anexoGq05AC1;
    }

    public JEditableComboBox getAnexoGq05AC3() {
        return this.anexoGq05AC3;
    }

    public JLabel getAnexoGq05AC7_base_label() {
        return this.anexoGq05AC7_base_label;
    }

    public JEditableComboBox getAnexoGq05AC7() {
        return this.anexoGq05AC7;
    }

    public JEditableComboBox getAnexoGq05AC9() {
        return this.anexoGq05AC9;
    }

    public JPercentTextField getAnexoGq05AC6() {
        return this.anexoGq05AC6;
    }

    public JPercentTextField getAnexoGq05AC12() {
        return this.anexoGq05AC12;
    }

    public JLimitedTextField getAnexoGq05AC2() {
        return this.anexoGq05AC2;
    }

    public JLimitedTextField getAnexoGq05AC8() {
        return this.anexoGq05AC8;
    }

    public JLimitedTextField getAnexoGq05AC4() {
        return this.anexoGq05AC4;
    }

    public JLimitedTextField getAnexoGq05AC5() {
        return this.anexoGq05AC5;
    }

    public JLimitedTextField getAnexoGq05AC10() {
        return this.anexoGq05AC10;
    }

    public JLimitedTextField getAnexoGq05AC11() {
        return this.anexoGq05AC11;
    }

    public JLabel getAnexoGq05C525_label8() {
        return this.anexoGq05C525_label8;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JLabel getLabel9() {
        return this.label9;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoGq05C501_label = new JLabel();
        this.anexoGq05C501_num = new JLabelTextFieldNumbering();
        this.anexoGq05C501 = new JLimitedTextField();
        this.anexoGq05C502_label\u00a3anexoGq05C503_label\u00a3anexoGq05C504_label = new JLabel();
        this.anexoGq05C502_num = new JLabelTextFieldNumbering();
        this.anexoGq05C502 = new JEditableComboBox();
        this.anexoGq05C503_num = new JLabelTextFieldNumbering();
        this.anexoGq05C503 = new JEditableComboBox();
        this.anexoGq05C504_num = new JLabelTextFieldNumbering();
        this.anexoGq05C504 = new JEditableComboBox();
        this.panel3 = new JPanel();
        this.label2 = new JLabel();
        this.anexoGq05C505_label = new JLabel();
        this.anexoGq05C505_num = new JLabelTextFieldNumbering();
        this.anexoGq05C505 = new JMoneyTextField();
        this.anexoGq05C506_label = new JLabel();
        this.anexoGq05C506_num = new JLabelTextFieldNumbering();
        this.anexoGq05C506 = new JMoneyTextField();
        this.label6 = new JLabel();
        this.anexoGq05C507_label = new JLabel();
        this.anexoGq05C507_num = new JLabelTextFieldNumbering();
        this.anexoGq05C507 = new JMoneyTextField();
        this.label7 = new JLabel();
        this.anexoGq05C508_label = new JLabel();
        this.anexoGq05C508_num = new JLabelTextFieldNumbering();
        this.anexoGq05C508 = new JMoneyTextField();
        this.anexoGq05C509_label = new JLabel();
        this.anexoGq05C509_num = new JLabelTextFieldNumbering();
        this.anexoGq05C509 = new JMoneyTextField();
        this.anexoGq05C510_label = new JLabel();
        this.anexoGq05C510_num = new JLabelTextFieldNumbering();
        this.anexoGq05C510 = new JMoneyTextField();
        this.anexoGq05C511_label = new JLabel();
        this.anexoGq05C511_num = new JLabelTextFieldNumbering();
        this.anexoGq05C511 = new JMoneyTextField();
        this.panel4 = new JPanel();
        this.anexoGq05C521_label = new JLabel();
        this.anexoGq05C521_num = new JLabelTextFieldNumbering();
        this.anexoGq05C521 = new JLimitedTextField();
        this.anexoGq05C522_label\u00a3anexoGq05C523_label\u00a3anexoGq05C524_label = new JLabel();
        this.anexoGq05C522_num = new JLabelTextFieldNumbering();
        this.anexoGq05C522 = new JEditableComboBox();
        this.anexoGq05C523_num = new JLabelTextFieldNumbering();
        this.anexoGq05C523 = new JEditableComboBox();
        this.anexoGq05C524_num = new JLabelTextFieldNumbering();
        this.anexoGq05C524 = new JEditableComboBox();
        this.panel7 = new JPanel();
        this.label8 = new JLabel();
        this.anexoGq05C525_label = new JLabel();
        this.anexoGq05C525_num = new JLabelTextFieldNumbering();
        this.anexoGq05C525 = new JMoneyTextField();
        this.anexoGq05C526_label = new JLabel();
        this.anexoGq05C526_num = new JLabelTextFieldNumbering();
        this.anexoGq05C526 = new JMoneyTextField();
        this.label9 = new JLabel();
        this.anexoGq05C527_label = new JLabel();
        this.anexoGq05C527_num = new JLabelTextFieldNumbering();
        this.anexoGq05C527 = new JMoneyTextField();
        this.label10 = new JLabel();
        this.anexoGq05C528_label = new JLabel();
        this.anexoGq05C528_num = new JLabelTextFieldNumbering();
        this.anexoGq05C528 = new JMoneyTextField();
        this.anexoGq05C529_label = new JLabel();
        this.anexoGq05C529_num = new JLabelTextFieldNumbering();
        this.anexoGq05C529 = new JMoneyTextField();
        this.anexoGq05C530_label = new JLabel();
        this.anexoGq05C530_num = new JLabelTextFieldNumbering();
        this.anexoGq05C530 = new JMoneyTextField();
        this.anexoGq05C531_label = new JLabel();
        this.anexoGq05C531_num = new JLabelTextFieldNumbering();
        this.anexoGq05C531 = new JMoneyTextField();
        this.panel6 = new JPanel();
        this.label14 = new JLabel();
        this.label4 = new JLabel();
        this.panel9 = new JPanel();
        this.anexoGq05C525_label2 = new JLabel();
        this.anexoGq05C525_label3 = new JLabel();
        this.anexoGq05C525_label8 = new JLabel();
        this.anexoGq05C525_label4 = new JLabel();
        this.anexoGq05C525_label5 = new JLabel();
        this.anexoGq05C525_label6 = new JLabel();
        this.anexoGq05C525_label7 = new JLabel();
        this.anexoGq05AC1_base_label = new JLabel();
        this.anexoGq05AC1 = new JEditableComboBox();
        this.anexoGq05AC2 = new JLimitedTextField();
        this.anexoGq05AC3 = new JEditableComboBox();
        this.anexoGq05AC4 = new JLimitedTextField();
        this.anexoGq05AC5 = new JLimitedTextField();
        this.anexoGq05AC6 = new JPercentTextField(2);
        this.anexoGq05AC7_base_label = new JLabel();
        this.anexoGq05AC7 = new JEditableComboBox();
        this.anexoGq05AC8 = new JLimitedTextField();
        this.anexoGq05AC9 = new JEditableComboBox();
        this.anexoGq05AC10 = new JLimitedTextField();
        this.anexoGq05AC11 = new JLimitedTextField();
        this.anexoGq05AC12 = new JPercentTextField(2);
        this.anexoGq05C527_label3 = new JLabel();
        this.anexoGq05AC13 = new JEditableComboBox();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "7*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, default, $lcgap, 25dlu, 0px, 3*(default, $lcgap), 25dlu, 0px, 2*(default, $lcgap), 25dlu, 0px, 2*(default, $lcgap), 25dlu, 0px, default, $rgap, default:grow", "2*(default, $lgap), default"));
        this.anexoGq05C501_label.setText(bundle.getString("Quadro05Panel.anexoGq05C501_label.text"));
        this.anexoGq05C501_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoGq05C501_label, CC.xy(3, 3));
        this.anexoGq05C501_num.setText(bundle.getString("Quadro05Panel.anexoGq05C501_num.text"));
        this.anexoGq05C501_num.setBorder(new EtchedBorder());
        this.anexoGq05C501_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoGq05C501_num, CC.xy(5, 3));
        this.anexoGq05C501.setColumns(5);
        this.anexoGq05C501.setMaxLength(4);
        this.anexoGq05C501.setHorizontalAlignment(4);
        this.anexoGq05C501.setOnlyNumeric(true);
        this.panel2.add((Component)this.anexoGq05C501, CC.xy(7, 3));
        this.anexoGq05C502_label\u00a3anexoGq05C503_label\u00a3anexoGq05C504_label.setText(bundle.getString("Quadro05Panel.anexoGq05C502_label\u00a3anexoGq05C503_label\u00a3anexoGq05C504_label.text"));
        this.panel2.add((Component)this.anexoGq05C502_label\u00a3anexoGq05C503_label\u00a3anexoGq05C504_label, CC.xy(11, 3));
        this.anexoGq05C502_num.setText(bundle.getString("Quadro05Panel.anexoGq05C502_num.text"));
        this.anexoGq05C502_num.setBorder(new EtchedBorder());
        this.anexoGq05C502_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoGq05C502_num, CC.xy(13, 3));
        this.panel2.add((Component)this.anexoGq05C502, CC.xy(15, 3));
        this.anexoGq05C503_num.setText(bundle.getString("Quadro05Panel.anexoGq05C503_num.text"));
        this.anexoGq05C503_num.setBorder(new EtchedBorder());
        this.anexoGq05C503_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoGq05C503_num, CC.xy(19, 3));
        this.panel2.add((Component)this.anexoGq05C503, CC.xy(21, 3));
        this.anexoGq05C504_num.setText(bundle.getString("Quadro05Panel.anexoGq05C504_num.text"));
        this.anexoGq05C504_num.setBorder(new EtchedBorder());
        this.anexoGq05C504_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoGq05C504_num, CC.xy(25, 3));
        this.panel2.add((Component)this.anexoGq05C504, CC.xy(27, 3));
        this.this2.add((Component)this.panel2, CC.xy(2, 1));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("$ugap, default, $lcgap, default:grow, $ugap, 25dlu, 0px, 75dlu, $rgap", "11*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro05Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label2, CC.xywh(2, 3, 7, 1));
        this.anexoGq05C505_label.setText(bundle.getString("Quadro05Panel.anexoGq05C505_label.text"));
        this.anexoGq05C505_label.setHorizontalAlignment(2);
        this.panel3.add((Component)this.anexoGq05C505_label, CC.xy(2, 5));
        this.anexoGq05C505_num.setText(bundle.getString("Quadro05Panel.anexoGq05C505_num.text"));
        this.anexoGq05C505_num.setBorder(new EtchedBorder());
        this.anexoGq05C505_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq05C505_num, CC.xy(6, 5));
        this.anexoGq05C505.setColumns(13);
        this.panel3.add((Component)this.anexoGq05C505, CC.xy(8, 5));
        this.anexoGq05C506_label.setText(bundle.getString("Quadro05Panel.anexoGq05C506_label.text"));
        this.anexoGq05C506_label.setHorizontalAlignment(2);
        this.panel3.add((Component)this.anexoGq05C506_label, CC.xy(2, 7));
        this.anexoGq05C506_num.setText(bundle.getString("Quadro05Panel.anexoGq05C506_num.text"));
        this.anexoGq05C506_num.setBorder(new EtchedBorder());
        this.anexoGq05C506_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq05C506_num, CC.xy(6, 7));
        this.anexoGq05C506.setColumns(13);
        this.panel3.add((Component)this.anexoGq05C506, CC.xy(8, 7));
        this.label6.setText(bundle.getString("Quadro05Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.label6.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label6, CC.xywh(2, 9, 7, 1));
        this.anexoGq05C507_label.setText(bundle.getString("Quadro05Panel.anexoGq05C507_label.text"));
        this.anexoGq05C507_label.setHorizontalAlignment(2);
        this.panel3.add((Component)this.anexoGq05C507_label, CC.xy(2, 11));
        this.anexoGq05C507_num.setText(bundle.getString("Quadro05Panel.anexoGq05C507_num.text"));
        this.anexoGq05C507_num.setBorder(new EtchedBorder());
        this.anexoGq05C507_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq05C507_num, CC.xy(6, 11));
        this.anexoGq05C507.setColumns(13);
        this.panel3.add((Component)this.anexoGq05C507, CC.xy(8, 11));
        this.label7.setText(bundle.getString("Quadro05Panel.label7.text"));
        this.label7.setHorizontalAlignment(0);
        this.label7.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label7, CC.xywh(2, 13, 7, 1));
        this.anexoGq05C508_label.setText(bundle.getString("Quadro05Panel.anexoGq05C508_label.text"));
        this.anexoGq05C508_label.setHorizontalAlignment(2);
        this.panel3.add((Component)this.anexoGq05C508_label, CC.xy(2, 15));
        this.anexoGq05C508_num.setText(bundle.getString("Quadro05Panel.anexoGq05C508_num.text"));
        this.anexoGq05C508_num.setBorder(new EtchedBorder());
        this.anexoGq05C508_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq05C508_num, CC.xy(6, 15));
        this.anexoGq05C508.setColumns(13);
        this.panel3.add((Component)this.anexoGq05C508, CC.xy(8, 15));
        this.anexoGq05C509_label.setText(bundle.getString("Quadro05Panel.anexoGq05C509_label.text"));
        this.anexoGq05C509_label.setHorizontalAlignment(2);
        this.panel3.add((Component)this.anexoGq05C509_label, CC.xy(2, 17));
        this.anexoGq05C509_num.setText(bundle.getString("Quadro05Panel.anexoGq05C509_num.text"));
        this.anexoGq05C509_num.setBorder(new EtchedBorder());
        this.anexoGq05C509_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq05C509_num, CC.xy(6, 17));
        this.anexoGq05C509.setColumns(13);
        this.panel3.add((Component)this.anexoGq05C509, CC.xy(8, 17));
        this.anexoGq05C510_label.setText(bundle.getString("Quadro05Panel.anexoGq05C510_label.text"));
        this.anexoGq05C510_label.setHorizontalAlignment(2);
        this.panel3.add((Component)this.anexoGq05C510_label, CC.xy(2, 19));
        this.anexoGq05C510_num.setText(bundle.getString("Quadro05Panel.anexoGq05C510_num.text"));
        this.anexoGq05C510_num.setBorder(new EtchedBorder());
        this.anexoGq05C510_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq05C510_num, CC.xy(6, 19));
        this.anexoGq05C510.setColumns(13);
        this.panel3.add((Component)this.anexoGq05C510, CC.xy(8, 19));
        this.anexoGq05C511_label.setText(bundle.getString("Quadro05Panel.anexoGq05C511_label.text"));
        this.anexoGq05C511_label.setHorizontalAlignment(2);
        this.panel3.add((Component)this.anexoGq05C511_label, CC.xy(2, 21));
        this.anexoGq05C511_num.setText(bundle.getString("Quadro05Panel.anexoGq05C511_num.text"));
        this.anexoGq05C511_num.setBorder(new EtchedBorder());
        this.anexoGq05C511_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoGq05C511_num, CC.xy(6, 21));
        this.anexoGq05C511.setColumns(13);
        this.panel3.add((Component)this.anexoGq05C511, CC.xy(8, 21));
        this.this2.add((Component)this.panel3, CC.xy(2, 3));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, default, $lcgap, 25dlu, 0px, 3*(default, $lcgap), 25dlu, 0px, 2*(default, $lcgap), 25dlu, 0px, 2*(default, $lcgap), 25dlu, 0px, default, $rgap, default:grow", "4*(default, $lgap), default"));
        this.anexoGq05C521_label.setText(bundle.getString("Quadro05Panel.anexoGq05C521_label.text"));
        this.anexoGq05C521_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoGq05C521_label, CC.xy(3, 7));
        this.anexoGq05C521_num.setText(bundle.getString("Quadro05Panel.anexoGq05C521_num.text"));
        this.anexoGq05C521_num.setBorder(new EtchedBorder());
        this.anexoGq05C521_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq05C521_num, CC.xy(5, 7));
        this.anexoGq05C521.setColumns(5);
        this.anexoGq05C521.setMaxLength(4);
        this.anexoGq05C521.setHorizontalAlignment(4);
        this.anexoGq05C521.setOnlyNumeric(true);
        this.panel4.add((Component)this.anexoGq05C521, CC.xy(7, 7));
        this.anexoGq05C522_label\u00a3anexoGq05C523_label\u00a3anexoGq05C524_label.setText(bundle.getString("Quadro05Panel.anexoGq05C522_label\u00a3anexoGq05C523_label\u00a3anexoGq05C524_label.text"));
        this.panel4.add((Component)this.anexoGq05C522_label\u00a3anexoGq05C523_label\u00a3anexoGq05C524_label, CC.xy(11, 7));
        this.anexoGq05C522_num.setText(bundle.getString("Quadro05Panel.anexoGq05C522_num.text"));
        this.anexoGq05C522_num.setBorder(new EtchedBorder());
        this.anexoGq05C522_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq05C522_num, CC.xy(13, 7));
        this.panel4.add((Component)this.anexoGq05C522, CC.xy(15, 7));
        this.anexoGq05C523_num.setText(bundle.getString("Quadro05Panel.anexoGq05C523_num.text"));
        this.anexoGq05C523_num.setBorder(new EtchedBorder());
        this.anexoGq05C523_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq05C523_num, CC.xy(19, 7));
        this.panel4.add((Component)this.anexoGq05C523, CC.xy(21, 7));
        this.anexoGq05C524_num.setText(bundle.getString("Quadro05Panel.anexoGq05C524_num.text"));
        this.anexoGq05C524_num.setBorder(new EtchedBorder());
        this.anexoGq05C524_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoGq05C524_num, CC.xy(25, 7));
        this.panel4.add((Component)this.anexoGq05C524, CC.xy(27, 7));
        this.this2.add((Component)this.panel4, CC.xy(2, 5));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("$ugap, default, $lcgap, default:grow, $ugap, 25dlu, 0px, 75dlu, $rgap", "10*(default, $lgap), default"));
        this.label8.setText(bundle.getString("Quadro05Panel.label8.text"));
        this.label8.setBorder(new EtchedBorder());
        this.label8.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label8, CC.xywh(2, 1, 7, 1));
        this.anexoGq05C525_label.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label.text"));
        this.anexoGq05C525_label.setHorizontalAlignment(2);
        this.panel7.add((Component)this.anexoGq05C525_label, CC.xy(2, 3));
        this.anexoGq05C525_num.setText(bundle.getString("Quadro05Panel.anexoGq05C525_num.text"));
        this.anexoGq05C525_num.setBorder(new EtchedBorder());
        this.anexoGq05C525_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoGq05C525_num, CC.xy(6, 3));
        this.anexoGq05C525.setColumns(13);
        this.panel7.add((Component)this.anexoGq05C525, CC.xy(8, 3));
        this.anexoGq05C526_label.setText(bundle.getString("Quadro05Panel.anexoGq05C526_label.text"));
        this.anexoGq05C526_label.setHorizontalAlignment(2);
        this.panel7.add((Component)this.anexoGq05C526_label, CC.xy(2, 5));
        this.anexoGq05C526_num.setText(bundle.getString("Quadro05Panel.anexoGq05C526_num.text"));
        this.anexoGq05C526_num.setBorder(new EtchedBorder());
        this.anexoGq05C526_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoGq05C526_num, CC.xy(6, 5));
        this.anexoGq05C526.setColumns(13);
        this.panel7.add((Component)this.anexoGq05C526, CC.xy(8, 5));
        this.label9.setText(bundle.getString("Quadro05Panel.label9.text"));
        this.label9.setHorizontalAlignment(0);
        this.label9.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.label9, CC.xywh(2, 7, 7, 1));
        this.anexoGq05C527_label.setText(bundle.getString("Quadro05Panel.anexoGq05C527_label.text"));
        this.anexoGq05C527_label.setHorizontalAlignment(2);
        this.panel7.add((Component)this.anexoGq05C527_label, CC.xy(2, 9));
        this.anexoGq05C527_num.setText(bundle.getString("Quadro05Panel.anexoGq05C527_num.text"));
        this.anexoGq05C527_num.setBorder(new EtchedBorder());
        this.anexoGq05C527_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoGq05C527_num, CC.xy(6, 9));
        this.anexoGq05C527.setColumns(13);
        this.panel7.add((Component)this.anexoGq05C527, CC.xy(8, 9));
        this.label10.setText(bundle.getString("Quadro05Panel.label10.text"));
        this.label10.setHorizontalAlignment(0);
        this.label10.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.label10, CC.xywh(2, 11, 7, 1));
        this.anexoGq05C528_label.setText(bundle.getString("Quadro05Panel.anexoGq05C528_label.text"));
        this.anexoGq05C528_label.setHorizontalAlignment(2);
        this.panel7.add((Component)this.anexoGq05C528_label, CC.xy(2, 13));
        this.anexoGq05C528_num.setText(bundle.getString("Quadro05Panel.anexoGq05C528_num.text"));
        this.anexoGq05C528_num.setBorder(new EtchedBorder());
        this.anexoGq05C528_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoGq05C528_num, CC.xy(6, 13));
        this.anexoGq05C528.setColumns(13);
        this.panel7.add((Component)this.anexoGq05C528, CC.xy(8, 13));
        this.anexoGq05C529_label.setText(bundle.getString("Quadro05Panel.anexoGq05C529_label.text"));
        this.anexoGq05C529_label.setHorizontalAlignment(2);
        this.panel7.add((Component)this.anexoGq05C529_label, CC.xy(2, 15));
        this.anexoGq05C529_num.setText(bundle.getString("Quadro05Panel.anexoGq05C529_num.text"));
        this.anexoGq05C529_num.setBorder(new EtchedBorder());
        this.anexoGq05C529_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoGq05C529_num, CC.xy(6, 15));
        this.anexoGq05C529.setColumns(13);
        this.panel7.add((Component)this.anexoGq05C529, CC.xy(8, 15));
        this.anexoGq05C530_label.setText(bundle.getString("Quadro05Panel.anexoGq05C530_label.text"));
        this.anexoGq05C530_label.setHorizontalAlignment(2);
        this.panel7.add((Component)this.anexoGq05C530_label, CC.xy(2, 17));
        this.anexoGq05C530_num.setText(bundle.getString("Quadro05Panel.anexoGq05C530_num.text"));
        this.anexoGq05C530_num.setBorder(new EtchedBorder());
        this.anexoGq05C530_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoGq05C530_num, CC.xy(6, 17));
        this.anexoGq05C530.setColumns(13);
        this.panel7.add((Component)this.anexoGq05C530, CC.xy(8, 17));
        this.anexoGq05C531_label.setText(bundle.getString("Quadro05Panel.anexoGq05C531_label.text"));
        this.anexoGq05C531_label.setHorizontalAlignment(2);
        this.panel7.add((Component)this.anexoGq05C531_label, CC.xy(2, 19));
        this.anexoGq05C531_num.setText(bundle.getString("Quadro05Panel.anexoGq05C531_num.text"));
        this.anexoGq05C531_num.setBorder(new EtchedBorder());
        this.anexoGq05C531_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoGq05C531_num, CC.xy(6, 19));
        this.anexoGq05C531.setColumns(13);
        this.panel7.add((Component)this.anexoGq05C531, CC.xy(8, 19));
        this.this2.add((Component)this.panel7, CC.xy(2, 7));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap", "default"));
        this.label14.setText(bundle.getString("Quadro05Panel.label14.text"));
        this.label14.setBorder(new EtchedBorder());
        this.label14.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label14, CC.xy(1, 1));
        this.label4.setText(bundle.getString("Quadro05Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label4, CC.xy(3, 1));
        this.this2.add((Component)this.panel6, CC.xy(2, 11));
        this.panel9.setMinimumSize(null);
        this.panel9.setPreferredSize(null);
        this.panel9.setLayout(new FormLayout("$ugap, default:grow, 2*($lcgap, default), 0px, 4*($lcgap, default), $ugap", "4*(default, $lgap), default"));
        this.anexoGq05C525_label2.setHorizontalAlignment(0);
        this.anexoGq05C525_label2.setBorder(new EtchedBorder());
        this.anexoGq05C525_label2.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label2.text"));
        this.panel9.add((Component)this.anexoGq05C525_label2, CC.xy(2, 3));
        this.anexoGq05C525_label3.setHorizontalAlignment(0);
        this.anexoGq05C525_label3.setBorder(new EtchedBorder());
        this.anexoGq05C525_label3.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label3.text"));
        this.panel9.add((Component)this.anexoGq05C525_label3, CC.xy(4, 3));
        this.anexoGq05C525_label8.setHorizontalAlignment(0);
        this.anexoGq05C525_label8.setBorder(new EtchedBorder());
        this.anexoGq05C525_label8.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label8.text"));
        this.panel9.add((Component)this.anexoGq05C525_label8, CC.xy(6, 3));
        this.anexoGq05C525_label4.setHorizontalAlignment(0);
        this.anexoGq05C525_label4.setBorder(new EtchedBorder());
        this.anexoGq05C525_label4.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label4.text"));
        this.panel9.add((Component)this.anexoGq05C525_label4, CC.xy(9, 3));
        this.anexoGq05C525_label5.setHorizontalAlignment(0);
        this.anexoGq05C525_label5.setBorder(new EtchedBorder());
        this.anexoGq05C525_label5.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label5.text"));
        this.panel9.add((Component)this.anexoGq05C525_label5, CC.xy(11, 3));
        this.anexoGq05C525_label6.setHorizontalAlignment(0);
        this.anexoGq05C525_label6.setBorder(new EtchedBorder());
        this.anexoGq05C525_label6.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label6.text"));
        this.panel9.add((Component)this.anexoGq05C525_label6, CC.xy(13, 3));
        this.anexoGq05C525_label7.setHorizontalAlignment(0);
        this.anexoGq05C525_label7.setBorder(new EtchedBorder());
        this.anexoGq05C525_label7.setText(bundle.getString("Quadro05Panel.anexoGq05C525_label7.text"));
        this.panel9.add((Component)this.anexoGq05C525_label7, CC.xy(15, 3));
        this.anexoGq05AC1_base_label.setText(bundle.getString("Quadro05Panel.anexoGq05AC1_base_label.text"));
        this.anexoGq05AC1_base_label.setHorizontalAlignment(2);
        this.panel9.add((Component)this.anexoGq05AC1_base_label, CC.xy(2, 5));
        this.panel9.add((Component)this.anexoGq05AC1, CC.xy(4, 5));
        this.anexoGq05AC2.setColumns(7);
        this.anexoGq05AC2.setMaxLength(6);
        this.anexoGq05AC2.setHorizontalAlignment(4);
        this.panel9.add((Component)this.anexoGq05AC2, CC.xy(6, 5));
        this.panel9.add((Component)this.anexoGq05AC3, CC.xy(9, 5));
        this.anexoGq05AC4.setColumns(6);
        this.anexoGq05AC4.setMaxLength(5);
        this.anexoGq05AC4.setHorizontalAlignment(4);
        this.anexoGq05AC4.setOnlyNumeric(true);
        this.panel9.add((Component)this.anexoGq05AC4, CC.xy(11, 5));
        this.anexoGq05AC5.setColumns(8);
        this.anexoGq05AC5.setMaxLength(7);
        this.anexoGq05AC5.setHorizontalAlignment(4);
        this.panel9.add((Component)this.anexoGq05AC5, CC.xy(13, 5));
        this.anexoGq05AC6.setColumns(5);
        this.panel9.add((Component)this.anexoGq05AC6, CC.xy(15, 5));
        this.anexoGq05AC7_base_label.setText(bundle.getString("Quadro05Panel.anexoGq05AC7_base_label.text"));
        this.anexoGq05AC7_base_label.setHorizontalAlignment(2);
        this.panel9.add((Component)this.anexoGq05AC7_base_label, CC.xy(2, 7));
        this.panel9.add((Component)this.anexoGq05AC7, CC.xy(4, 7));
        this.anexoGq05AC8.setColumns(7);
        this.anexoGq05AC8.setMaxLength(6);
        this.anexoGq05AC8.setHorizontalAlignment(4);
        this.panel9.add((Component)this.anexoGq05AC8, CC.xy(6, 7));
        this.panel9.add((Component)this.anexoGq05AC9, CC.xy(9, 7));
        this.anexoGq05AC10.setColumns(6);
        this.anexoGq05AC10.setMaxLength(5);
        this.anexoGq05AC10.setHorizontalAlignment(4);
        this.anexoGq05AC10.setOnlyNumeric(true);
        this.panel9.add((Component)this.anexoGq05AC10, CC.xy(11, 7));
        this.anexoGq05AC11.setColumns(8);
        this.anexoGq05AC11.setMaxLength(7);
        this.anexoGq05AC11.setHorizontalAlignment(4);
        this.panel9.add((Component)this.anexoGq05AC11, CC.xy(13, 7));
        this.anexoGq05AC12.setColumns(5);
        this.panel9.add((Component)this.anexoGq05AC12, CC.xy(15, 7));
        this.anexoGq05C527_label3.setText(bundle.getString("Quadro05Panel.anexoGq05C527_label3.text"));
        this.anexoGq05C527_label3.setHorizontalAlignment(2);
        this.panel9.add((Component)this.anexoGq05C527_label3, CC.xy(2, 9));
        this.panel9.add((Component)this.anexoGq05AC13, CC.xywh(4, 9, 6, 1));
        this.this2.add((Component)this.panel9, CC.xy(2, 13));
        this.add((Component)this.this2, "Center");
    }
}

