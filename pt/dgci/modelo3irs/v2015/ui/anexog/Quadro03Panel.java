/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro03Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoGq03C02_label;
    protected JLabel label3;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField anexoGq03C02;
    protected JLabel anexoGq03C03_label;
    protected JLabel label6;
    protected JLabelTextFieldNumbering label7;
    protected JNIFTextField anexoGq03C03;

    public Quadro03Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoGq03C02_label() {
        return this.anexoGq03C02_label;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getAnexoGq03C02() {
        return this.anexoGq03C02;
    }

    public JLabel getAnexoGq03C03_label() {
        return this.anexoGq03C03_label;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabelTextFieldNumbering getLabel7() {
        return this.label7;
    }

    public JNIFTextField getAnexoGq03C03() {
        return this.anexoGq03C03;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoGq03C02_label = new JLabel();
        this.label3 = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoGq03C02 = new JNIFTextField();
        this.anexoGq03C03_label = new JLabel();
        this.label6 = new JLabel();
        this.label7 = new JLabelTextFieldNumbering();
        this.anexoGq03C03 = new JNIFTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro03Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro03Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("0px, default:grow, $lcgap, default, 15dlu, default, $rgap, 13dlu, 0px, 43dlu, $lcgap, default:grow, $lcgap, default, 15dlu, default, $lcgap, 13dlu, 0px, 43dlu, $lcgap, default:grow", "$rgap, default, $lgap, default"));
        this.anexoGq03C02_label.setText(bundle.getString("Quadro03Panel.anexoGq03C02_label.text"));
        this.panel2.add((Component)this.anexoGq03C02_label, CC.xy(4, 2));
        this.label3.setText(bundle.getString("Quadro03Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label3, CC.xy(6, 2));
        this.label4.setText(bundle.getString("Quadro03Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(8, 2));
        this.anexoGq03C02.setColumns(9);
        this.anexoGq03C02.setEditable(false);
        this.panel2.add((Component)this.anexoGq03C02, CC.xy(10, 2));
        this.anexoGq03C03_label.setText(bundle.getString("Quadro03Panel.anexoGq03C03_label.text"));
        this.panel2.add((Component)this.anexoGq03C03_label, CC.xy(14, 2));
        this.label6.setText(bundle.getString("Quadro03Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label6, CC.xy(16, 2));
        this.label7.setText(bundle.getString("Quadro03Panel.label7.text"));
        this.label7.setBorder(new EtchedBorder());
        this.label7.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label7, CC.xy(18, 2));
        this.anexoGq03C03.setColumns(9);
        this.anexoGq03C03.setEditable(false);
        this.panel2.add((Component)this.anexoGq03C03, CC.xy(20, 2));
        this.this2.add((Component)this.panel2, CC.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

