/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro09Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel5;
    protected JLabel anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label;
    protected JLabel anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label;
    protected JLabel anexoGq09C901a_base_label\u00a3anexoGq09C901b_base_label;
    protected JLabelTextFieldNumbering anexoGq09C901a_num\u00a3anexoGq09C901b_num;
    protected JEditableComboBox anexoGq09C901a;
    protected JMoneyTextField anexoGq09C901b;
    protected JLabel anexoGq09C902a_base_label\u00a3anexoGq09C902b_base_label;
    protected JLabelTextFieldNumbering anexoGq09C902a_num\u00a3anexoGq09C902b_num;
    protected JEditableComboBox anexoGq09C902a;
    protected JMoneyTextField anexoGq09C902b;
    protected JLabel anexoGq09C903a_base_label\u00a3anexoGq09C903b_base_label;
    protected JLabelTextFieldNumbering anexoGq09C903a_num\u00a3anexoGq09C903b_num;
    protected JEditableComboBox anexoGq09C903a;
    protected JMoneyTextField anexoGq09C903b;
    protected JLabel anexoGq09C904a_base_label\u00a3anexoGq09C904b_base_label;
    protected JLabelTextFieldNumbering anexoGq09C904a_num\u00a3anexoGq09C904b_num;
    protected JEditableComboBox anexoGq09C904a;
    protected JMoneyTextField anexoGq09C904b;
    protected JLabel anexoGq09C905a_base_label\u00a3anexoGq09C905b_base_label;
    protected JLabelTextFieldNumbering anexoGq09C905a_num\u00a3anexoGq09C905b_num;
    protected JEditableComboBox anexoGq09C905a;
    protected JMoneyTextField anexoGq09C905b;
    protected JLabel anexoGq09C1b_base_label;
    protected JMoneyTextField anexoGq09C1b;
    protected JLabel anexoGq09B1OPSim_base_label;
    protected JPanel panel10;
    protected JLabelTextFieldNumbering label24;
    protected JRadioButton anexoGq09B1OP1;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton anexoGq09B1OP2;
    protected JPanel panel9;

    public Quadro09Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getAnexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label() {
        return this.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label;
    }

    public JLabel getAnexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label() {
        return this.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label;
    }

    public JLabel getAnexoGq09C901a_base_label\u00a3anexoGq09C901b_base_label() {
        return this.anexoGq09C901a_base_label\u00a3anexoGq09C901b_base_label;
    }

    public JLabelTextFieldNumbering getAnexoGq09C901a_num\u00a3anexoGq09C901b_num() {
        return this.anexoGq09C901a_num\u00a3anexoGq09C901b_num;
    }

    public JEditableComboBox getAnexoGq09C901a() {
        return this.anexoGq09C901a;
    }

    public JLabel getAnexoGq09C902a_base_label\u00a3anexoGq09C902b_base_label() {
        return this.anexoGq09C902a_base_label\u00a3anexoGq09C902b_base_label;
    }

    public JLabelTextFieldNumbering getAnexoGq09C902a_num\u00a3anexoGq09C902b_num() {
        return this.anexoGq09C902a_num\u00a3anexoGq09C902b_num;
    }

    public JEditableComboBox getAnexoGq09C902a() {
        return this.anexoGq09C902a;
    }

    public JLabel getAnexoGq09C903a_base_label\u00a3anexoGq09C903b_base_label() {
        return this.anexoGq09C903a_base_label\u00a3anexoGq09C903b_base_label;
    }

    public JLabelTextFieldNumbering getAnexoGq09C903a_num\u00a3anexoGq09C903b_num() {
        return this.anexoGq09C903a_num\u00a3anexoGq09C903b_num;
    }

    public JEditableComboBox getAnexoGq09C903a() {
        return this.anexoGq09C903a;
    }

    public JLabel getAnexoGq09C904a_base_label\u00a3anexoGq09C904b_base_label() {
        return this.anexoGq09C904a_base_label\u00a3anexoGq09C904b_base_label;
    }

    public JLabelTextFieldNumbering getAnexoGq09C904a_num\u00a3anexoGq09C904b_num() {
        return this.anexoGq09C904a_num\u00a3anexoGq09C904b_num;
    }

    public JEditableComboBox getAnexoGq09C904a() {
        return this.anexoGq09C904a;
    }

    public JLabel getAnexoGq09C905a_base_label\u00a3anexoGq09C905b_base_label() {
        return this.anexoGq09C905a_base_label\u00a3anexoGq09C905b_base_label;
    }

    public JLabelTextFieldNumbering getAnexoGq09C905a_num\u00a3anexoGq09C905b_num() {
        return this.anexoGq09C905a_num\u00a3anexoGq09C905b_num;
    }

    public JEditableComboBox getAnexoGq09C905a() {
        return this.anexoGq09C905a;
    }

    public JLabel getAnexoGq09C1b_base_label() {
        return this.anexoGq09C1b_base_label;
    }

    public JMoneyTextField getAnexoGq09C1b() {
        return this.anexoGq09C1b;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabel getAnexoGq09B1OPSim_base_label() {
        return this.anexoGq09B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getAnexoGq09B1OP2() {
        return this.anexoGq09B1OP2;
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabelTextFieldNumbering getLabel24() {
        return this.label24;
    }

    public JRadioButton getAnexoGq09B1OP1() {
        return this.anexoGq09B1OP1;
    }

    public JMoneyTextField getAnexoGq09C901b() {
        return this.anexoGq09C901b;
    }

    public JMoneyTextField getAnexoGq09C902b() {
        return this.anexoGq09C902b;
    }

    public JMoneyTextField getAnexoGq09C903b() {
        return this.anexoGq09C903b;
    }

    public JMoneyTextField getAnexoGq09C904b() {
        return this.anexoGq09C904b;
    }

    public JMoneyTextField getAnexoGq09C905b() {
        return this.anexoGq09C905b;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel5 = new JPanel();
        this.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label = new JLabel();
        this.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label = new JLabel();
        this.anexoGq09C901a_base_label\u00a3anexoGq09C901b_base_label = new JLabel();
        this.anexoGq09C901a_num\u00a3anexoGq09C901b_num = new JLabelTextFieldNumbering();
        this.anexoGq09C901a = new JEditableComboBox();
        this.anexoGq09C901b = new JMoneyTextField();
        this.anexoGq09C902a_base_label\u00a3anexoGq09C902b_base_label = new JLabel();
        this.anexoGq09C902a_num\u00a3anexoGq09C902b_num = new JLabelTextFieldNumbering();
        this.anexoGq09C902a = new JEditableComboBox();
        this.anexoGq09C902b = new JMoneyTextField();
        this.anexoGq09C903a_base_label\u00a3anexoGq09C903b_base_label = new JLabel();
        this.anexoGq09C903a_num\u00a3anexoGq09C903b_num = new JLabelTextFieldNumbering();
        this.anexoGq09C903a = new JEditableComboBox();
        this.anexoGq09C903b = new JMoneyTextField();
        this.anexoGq09C904a_base_label\u00a3anexoGq09C904b_base_label = new JLabel();
        this.anexoGq09C904a_num\u00a3anexoGq09C904b_num = new JLabelTextFieldNumbering();
        this.anexoGq09C904a = new JEditableComboBox();
        this.anexoGq09C904b = new JMoneyTextField();
        this.anexoGq09C905a_base_label\u00a3anexoGq09C905b_base_label = new JLabel();
        this.anexoGq09C905a_num\u00a3anexoGq09C905b_num = new JLabelTextFieldNumbering();
        this.anexoGq09C905a = new JEditableComboBox();
        this.anexoGq09C905b = new JMoneyTextField();
        this.anexoGq09C1b_base_label = new JLabel();
        this.anexoGq09C1b = new JMoneyTextField();
        this.anexoGq09B1OPSim_base_label = new JLabel();
        this.panel10 = new JPanel();
        this.label24 = new JLabelTextFieldNumbering();
        this.anexoGq09B1OP1 = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.anexoGq09B1OP2 = new JRadioButton();
        this.panel9 = new JPanel();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro09Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro09Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "2*(default, $lgap), default"));
        this.panel5.setPreferredSize(null);
        this.panel5.setMinimumSize(null);
        this.panel5.setLayout(new FormLayout("default:grow, $lcgap, 0px, 25dlu, 0px, default, $lcgap, 0px, 80dlu, $rgap, 0px", "10*(default, $lgap), default"));
        this.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label.setText(bundle.getString("Quadro09Panel.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label.text"));
        this.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label.setBorder(new EtchedBorder());
        this.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label.setHorizontalAlignment(0);
        this.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label.setHorizontalTextPosition(0);
        this.panel5.add((Component)this.anexoGq09C901a_label\u00a3anexoGq09C902a_label\u00a3anexoGq09C903a_label\u00a3anexoGq09C904a_label\u00a3anexoGq09C905a_label\u00a3anexoGq09C1a_label, cc.xywh(4, 1, 3, 1));
        this.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label.setText(bundle.getString("Quadro09Panel.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label.text"));
        this.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label.setBorder(new EtchedBorder());
        this.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label.setHorizontalAlignment(0);
        this.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label.setHorizontalTextPosition(0);
        this.panel5.add((Component)this.anexoGq09C901b_label\u00a3anexoGq09C902b_label\u00a3anexoGq09C903b_label\u00a3anexoGq09C904b_label\u00a3anexoGq09C905b_label\u00a3anexoGq09C1b_label, cc.xy(9, 1));
        this.anexoGq09C901a_base_label\u00a3anexoGq09C901b_base_label.setText(bundle.getString("Quadro09Panel.anexoGq09C901a_base_label\u00a3anexoGq09C901b_base_label.text"));
        this.panel5.add((Component)this.anexoGq09C901a_base_label\u00a3anexoGq09C901b_base_label, cc.xy(1, 3));
        this.anexoGq09C901a_num\u00a3anexoGq09C901b_num.setText(bundle.getString("Quadro09Panel.anexoGq09C901a_num\u00a3anexoGq09C901b_num.text"));
        this.anexoGq09C901a_num\u00a3anexoGq09C901b_num.setBorder(new EtchedBorder());
        this.anexoGq09C901a_num\u00a3anexoGq09C901b_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq09C901a_num\u00a3anexoGq09C901b_num, cc.xy(4, 3));
        this.panel5.add((Component)this.anexoGq09C901a, cc.xy(6, 3));
        this.anexoGq09C901b.setAllowNegative(true);
        this.panel5.add((Component)this.anexoGq09C901b, cc.xy(9, 3));
        this.anexoGq09C902a_base_label\u00a3anexoGq09C902b_base_label.setText(bundle.getString("Quadro09Panel.anexoGq09C902a_base_label\u00a3anexoGq09C902b_base_label.text"));
        this.panel5.add((Component)this.anexoGq09C902a_base_label\u00a3anexoGq09C902b_base_label, cc.xy(1, 5));
        this.anexoGq09C902a_num\u00a3anexoGq09C902b_num.setText(bundle.getString("Quadro09Panel.anexoGq09C902a_num\u00a3anexoGq09C902b_num.text"));
        this.anexoGq09C902a_num\u00a3anexoGq09C902b_num.setBorder(new EtchedBorder());
        this.anexoGq09C902a_num\u00a3anexoGq09C902b_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq09C902a_num\u00a3anexoGq09C902b_num, cc.xy(4, 5));
        this.panel5.add((Component)this.anexoGq09C902a, cc.xy(6, 5));
        this.anexoGq09C902b.setAllowNegative(true);
        this.panel5.add((Component)this.anexoGq09C902b, cc.xy(9, 5));
        this.anexoGq09C903a_base_label\u00a3anexoGq09C903b_base_label.setText(bundle.getString("Quadro09Panel.anexoGq09C903a_base_label\u00a3anexoGq09C903b_base_label.text"));
        this.panel5.add((Component)this.anexoGq09C903a_base_label\u00a3anexoGq09C903b_base_label, cc.xy(1, 7));
        this.anexoGq09C903a_num\u00a3anexoGq09C903b_num.setText(bundle.getString("Quadro09Panel.anexoGq09C903a_num\u00a3anexoGq09C903b_num.text"));
        this.anexoGq09C903a_num\u00a3anexoGq09C903b_num.setBorder(new EtchedBorder());
        this.anexoGq09C903a_num\u00a3anexoGq09C903b_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq09C903a_num\u00a3anexoGq09C903b_num, cc.xy(4, 7));
        this.panel5.add((Component)this.anexoGq09C903a, cc.xy(6, 7));
        this.anexoGq09C903b.setAllowNegative(true);
        this.panel5.add((Component)this.anexoGq09C903b, cc.xy(9, 7));
        this.anexoGq09C904a_base_label\u00a3anexoGq09C904b_base_label.setText(bundle.getString("Quadro09Panel.anexoGq09C904a_base_label\u00a3anexoGq09C904b_base_label.text"));
        this.panel5.add((Component)this.anexoGq09C904a_base_label\u00a3anexoGq09C904b_base_label, cc.xy(1, 9));
        this.anexoGq09C904a_num\u00a3anexoGq09C904b_num.setText(bundle.getString("Quadro09Panel.anexoGq09C904a_num\u00a3anexoGq09C904b_num.text"));
        this.anexoGq09C904a_num\u00a3anexoGq09C904b_num.setBorder(new EtchedBorder());
        this.anexoGq09C904a_num\u00a3anexoGq09C904b_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq09C904a_num\u00a3anexoGq09C904b_num, cc.xy(4, 9));
        this.anexoGq09C904a.setEnabled(false);
        this.panel5.add((Component)this.anexoGq09C904a, cc.xy(6, 9));
        this.anexoGq09C904b.setAllowNegative(true);
        this.anexoGq09C904b.setEditable(false);
        this.panel5.add((Component)this.anexoGq09C904b, cc.xy(9, 9));
        this.anexoGq09C905a_base_label\u00a3anexoGq09C905b_base_label.setText(bundle.getString("Quadro09Panel.anexoGq09C905a_base_label\u00a3anexoGq09C905b_base_label.text"));
        this.panel5.add((Component)this.anexoGq09C905a_base_label\u00a3anexoGq09C905b_base_label, cc.xy(1, 11));
        this.anexoGq09C905a_num\u00a3anexoGq09C905b_num.setText(bundle.getString("Quadro09Panel.anexoGq09C905a_num\u00a3anexoGq09C905b_num.text"));
        this.anexoGq09C905a_num\u00a3anexoGq09C905b_num.setBorder(new EtchedBorder());
        this.anexoGq09C905a_num\u00a3anexoGq09C905b_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoGq09C905a_num\u00a3anexoGq09C905b_num, cc.xy(4, 11));
        this.anexoGq09C905a.setEnabled(false);
        this.panel5.add((Component)this.anexoGq09C905a, cc.xy(6, 11));
        this.anexoGq09C905b.setAllowNegative(true);
        this.anexoGq09C905b.setEditable(false);
        this.panel5.add((Component)this.anexoGq09C905b, cc.xy(9, 11));
        this.anexoGq09C1b_base_label.setText(bundle.getString("Quadro09Panel.anexoGq09C1b_base_label.text"));
        this.anexoGq09C1b_base_label.setHorizontalAlignment(4);
        this.panel5.add((Component)this.anexoGq09C1b_base_label, cc.xy(6, 13));
        this.anexoGq09C1b.setEditable(false);
        this.anexoGq09C1b.setAllowNegative(true);
        this.anexoGq09C1b.setColumns(13);
        this.panel5.add((Component)this.anexoGq09C1b, cc.xy(9, 13));
        this.anexoGq09B1OPSim_base_label.setText(bundle.getString("Quadro09Panel.anexoGq09B1OPSim_base_label.text"));
        this.anexoGq09B1OPSim_base_label.setHorizontalAlignment(4);
        this.panel5.add((Component)this.anexoGq09B1OPSim_base_label, cc.xy(1, 19));
        this.panel10.setLayout(new FormLayout("$rgap, 0px, 13dlu, 0px, default, $ugap, 13dlu, 0px, default", "default"));
        this.label24.setText(bundle.getString("Quadro09Panel.label24.text"));
        this.label24.setBorder(new EtchedBorder());
        this.label24.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label24, cc.xy(3, 1));
        this.anexoGq09B1OP1.setText(bundle.getString("Quadro09Panel.anexoGq09B1OP1.text"));
        this.panel10.add((Component)this.anexoGq09B1OP1, cc.xy(5, 1));
        this.label23.setText(bundle.getString("Quadro09Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label23, cc.xy(7, 1));
        this.anexoGq09B1OP2.setText(bundle.getString("Quadro09Panel.anexoGq09B1OP2.text"));
        this.panel10.add((Component)this.anexoGq09B1OP2, cc.xy(9, 1));
        this.panel5.add((Component)this.panel10, cc.xywh(4, 19, 6, 1));
        this.panel9.setLayout(new FormLayout("0px, 13dlu, 0px, default", "default"));
        this.panel5.add((Component)this.panel9, cc.xy(9, 19));
        this.this2.add((Component)this.panel5, cc.xy(2, 3));
        this.add((Component)this.this2, "Center");
    }
}

