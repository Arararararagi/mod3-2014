/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro08Bindings;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro08PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro08PanelExtension
extends Quadro08PanelBase
implements IBindablePanel<Quadro08>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q08_T1 = 99;
    public static final int MAX_LINES_Q08_T2 = 99;

    public Quadro08PanelExtension(Quadro08 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro08 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro08Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
                this.setColumnSizes();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoGq08T1().getTableHeader().getColumnModel();
        ColumnGroup g_Realizacao = new ColumnGroup("Realiza\u00e7\u00e3o");
        g_Realizacao.add(cm.getColumn(5));
        g_Realizacao.add(cm.getColumn(6));
        g_Realizacao.add(cm.getColumn(7));
        ColumnGroup g_Aquisicao = new ColumnGroup("Aquisi\u00e7\u00e3o");
        g_Aquisicao.add(cm.getColumn(8));
        g_Aquisicao.add(cm.getColumn(9));
        g_Aquisicao.add(cm.getColumn(10));
        if (this.getAnexoGq08T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoGq08T1().getTableHeader()).addColumnGroup(g_Realizacao);
            ((GroupableTableHeader)this.getAnexoGq08T1().getTableHeader()).addColumnGroup(g_Aquisicao);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_Realizacao);
        header.addColumnGroup(g_Aquisicao);
        this.getAnexoGq08T1().setTableHeader(header);
    }

    protected void setColumnSizes() {
        for (int i = 0; i < this.getAnexoGq08T1().getColumnCount(); ++i) {
            if (i == 0) continue;
            if (i == 2 || i == 3) {
                this.getAnexoGq08T1().getColumnModel().getColumn(i).setPreferredWidth(150);
                continue;
            }
            if (i == this.getAnexoGq08T1().getColumnCount() - 1) {
                this.getAnexoGq08T1().getColumnModel().getColumn(i).setPreferredWidth(160);
                continue;
            }
            this.getAnexoGq08T1().getColumnModel().getColumn(i).setPreferredWidth(100);
        }
    }

    @Override
    protected void addLineAnexoGq08T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoGq08T1() != null && this.model.getAnexoGq08T1().size() >= 99) {
            return;
        }
        super.addLineAnexoGq08T1_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoGq08T2_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoGq08T2() != null && this.model.getAnexoGq08T2().size() >= 99) {
            return;
        }
        super.addLineAnexoGq08T2_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoGModel anexoG = (AnexoGModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoGModel.class);
            if (anexoG != null) {
                EventList<AnexoGq08T1_Linha> anexoGq08T1Rows = anexoG.getQuadro08().getAnexoGq08T1();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInGq08T1(anexoGq08T1Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInGq08T1(anexoGq08T1Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexGq08T1Elements(anexoGq08T1Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInGq08T1(anexoGq08T1Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInGq08T1(EventList<AnexoGq08T1_Linha> anexoGq08T1Rows, String labelPrefix) {
        if (anexoGq08T1Rows != null) {
            for (AnexoGq08T1_Linha anexoGq08T1_Linha : anexoGq08T1Rows) {
                if (anexoGq08T1_Linha == null || anexoGq08T1_Linha.getTitular() == null || !anexoGq08T1_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoGq08T1_Linha.setTitular(null);
            }
        }
    }

    private void reindexGq08T1Elements(EventList<AnexoGq08T1_Linha> anexoGq08T1Rows, String labelPrefix, int index) {
        if (anexoGq08T1Rows != null) {
            for (AnexoGq08T1_Linha anexoGq08T1_Linha : anexoGq08T1Rows) {
                int currIndex;
                if (anexoGq08T1_Linha == null || anexoGq08T1_Linha.getTitular() == null || !anexoGq08T1_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoGq08T1_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoGq08T1_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

