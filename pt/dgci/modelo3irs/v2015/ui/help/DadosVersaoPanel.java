/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.help;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class DadosVersaoPanel
extends JPanel {
    private static final long serialVersionUID = 7047222821276636417L;
    private JSplitPane splitPane1;
    private JLabel label5;
    private JLabel label6;
    private JPanel panel1;
    private JLabel label1;
    private JLabel label3;
    private JLabel label7;
    private JLabel label4;
    private JLabel label8;
    private JLabel label2;
    private JButton button1;

    public DadosVersaoPanel(String AppVersion, String AppVersionDate, String AppName) {
        this.initComponents();
        this.label3.setText("Vers\u00e3o " + AppVersion);
        this.label7.setText(AppVersionDate);
        this.label6.setText(AppName);
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                for (Container parent = DadosVersaoPanel.this.getParent(); parent != null; parent = parent.getParent()) {
                    if (!(parent instanceof JDialog)) continue;
                    ((JDialog)parent).dispose();
                    break;
                }
            }
        });
    }

    private void initComponents() {
        this.splitPane1 = new JSplitPane();
        this.label5 = new JLabel();
        this.label6 = new JLabel();
        this.panel1 = new JPanel();
        this.label1 = new JLabel();
        this.label3 = new JLabel();
        this.label7 = new JLabel();
        this.label4 = new JLabel();
        this.label8 = new JLabel();
        this.label2 = new JLabel();
        this.button1 = new JButton();
        CellConstraints cc = new CellConstraints();
        this.setLayout(new FormLayout("[7dlu,pref], $lcgap, default, $lcgap, [7dlu,pref]", "[5dlu,pref], $lgap, default, $lgap, min, $lgap, 2*(default, [7dlu,pref])"));
        this.splitPane1.setBorder(null);
        this.splitPane1.setDividerSize(0);
        this.label5.setIcon(new ImageIcon(this.getClass().getResource("/icons-small/icon.png")));
        this.label5.setHorizontalAlignment(2);
        this.splitPane1.setLeftComponent(this.label5);
        this.label6.setText("Nome do Modelo");
        this.label6.setHorizontalAlignment(4);
        this.label6.setFont(new Font("Tahoma", 1, 14));
        this.label6.setForeground(SystemColor.textHighlight);
        this.splitPane1.setRightComponent(this.label6);
        this.add((Component)this.splitPane1, cc.xy(3, 3));
        this.panel1.setBorder(LineBorder.createBlackLineBorder());
        this.panel1.setLayout(new FormLayout("default, $lcgap, [20dlu,pref], $lcgap, center:default, $lcgap, [20dlu,pref], $lcgap, default", "[7dlu,pref], 3*($lgap, default), $lgap, $pgap, $lgap, default, 3dlu, default, 4dlu, default, [7dlu,pref]"));
        this.label1.setIcon(new ImageIcon(this.getClass().getResource("/icons/logo_at.png")));
        this.panel1.add((Component)this.label1, cc.xy(5, 3));
        this.label3.setText("Vers\u00e3o");
        this.label3.setFont(new Font("Tahoma", 0, 12));
        this.panel1.add((Component)this.label3, cc.xywh(5, 5, 1, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        this.label7.setText("data versao");
        this.label7.setFont(new Font("Tahoma", 0, 12));
        this.panel1.add((Component)this.label7, cc.xywh(5, 7, 1, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        this.label4.setText("Aplica\u00e7\u00e3o 2015");
        this.label4.setFont(new Font("Tahoma", 0, 12));
        this.panel1.add((Component)this.label4, cc.xywh(5, 11, 1, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        this.label8.setText("Esta aplica\u00e7\u00e3o l\u00ea ficheiros da vers\u00e3o 14 ");
        this.label8.setFont(new Font("Tahoma", 0, 12));
        this.panel1.add((Component)this.label8, cc.xywh(5, 13, 1, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        this.label2.setText("(impressos vigentes em 2015)");
        this.panel1.add((Component)this.label2, cc.xy(5, 15));
        this.add((Component)this.panel1, cc.xy(3, 7));
        this.button1.setAction(null);
        this.button1.setText("Fechar");
        this.add((Component)this.button1, cc.xywh(3, 9, 1, 1, CellConstraints.RIGHT, CellConstraints.DEFAULT));
    }

}

