/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro04Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro04PanelBase
extends Quadro04Panel
implements IBindablePanel<Quadro04> {
    private static final long serialVersionUID = 1;
    protected Quadro04 model;

    @Override
    public abstract void setModel(Quadro04 var1, boolean var2);

    @Override
    public Quadro04 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoHq04T4().add(new AnexoHq04T4_Linha());
        }
    }

    @Override
    protected void removeLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoHq04T4.getSelectedRow() != -1 ? this.anexoHq04T4.getSelectedRow() : this.anexoHq04T4.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoHq04T4().remove(selectedRow);
            }
        }
    }

    public Quadro04PanelBase(Quadro04 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

