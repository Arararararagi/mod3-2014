/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoHq04T4Scroll;
    protected JTable anexoHq04T4;
    protected JPanel panel3;
    protected JLabel anexoHq04C1_label;
    protected JLabel anexoHq04C2_label;
    protected JLabel anexoHq04C1_base_label\u00a3anexoHq04C2_base_label;
    protected JMoneyTextField anexoHq04C1;
    protected JMoneyTextField anexoHq04C2;

    public Quadro04Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JScrollPane getAnexoHq04T4Scroll() {
        return this.anexoHq04T4Scroll;
    }

    public JTable getAnexoHq04T4() {
        return this.anexoHq04T4;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoHq04C1_label() {
        return this.anexoHq04C1_label;
    }

    public JLabel getAnexoHq04C2_label() {
        return this.anexoHq04C2_label;
    }

    public JLabel getAnexoHq04C1_base_label\u00a3anexoHq04C2_base_label() {
        return this.anexoHq04C1_base_label\u00a3anexoHq04C2_base_label;
    }

    public JMoneyTextField getAnexoHq04C1() {
        return this.anexoHq04C1;
    }

    public JMoneyTextField getAnexoHq04C2() {
        return this.anexoHq04C2;
    }

    protected void button1ActionPerformed(ActionEvent e) {
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    protected void addLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoH");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoHq04T4Scroll = new JScrollPane();
        this.anexoHq04T4 = new JTable();
        this.panel3 = new JPanel();
        this.anexoHq04C1_label = new JLabel();
        this.anexoHq04C2_label = new JLabel();
        this.anexoHq04C1_base_label\u00a3anexoHq04C2_base_label = new JLabel();
        this.anexoHq04C1 = new JMoneyTextField();
        this.anexoHq04C2 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 520dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoHq04T4_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoHq04T4_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel2.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoHq04T4Scroll.setViewportView(this.anexoHq04T4);
        this.panel2.add((Component)this.anexoHq04T4Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, default, 2*($lcgap, 80dlu), $lcgap, default:grow", "$ugap, 3*($lgap, default)"));
        this.anexoHq04C1_label.setText(bundle.getString("Quadro04Panel.anexoHq04C1_label.text"));
        this.anexoHq04C1_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoHq04C1_label, cc.xywh(4, 3, 3, 1));
        this.anexoHq04C2_label.setText(bundle.getString("Quadro04Panel.anexoHq04C2_label.text"));
        this.anexoHq04C2_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoHq04C2_label, cc.xy(7, 3));
        this.anexoHq04C1_base_label\u00a3anexoHq04C2_base_label.setText(bundle.getString("Quadro04Panel.anexoHq04C1_base_label\u00a3anexoHq04C2_base_label.text"));
        this.panel3.add((Component)this.anexoHq04C1_base_label\u00a3anexoHq04C2_base_label, cc.xy(3, 5));
        this.anexoHq04C1.setEditable(false);
        this.anexoHq04C1.setColumns(15);
        this.panel3.add((Component)this.anexoHq04C1, cc.xy(5, 5));
        this.anexoHq04C2.setEditable(false);
        this.anexoHq04C2.setColumns(15);
        this.panel3.add((Component)this.anexoHq04C2, cc.xy(7, 5));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.add((Component)this.this2, "Center");
    }

}

