/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro10Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro10PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro10PanelExtension
extends Quadro10PanelBase
implements IBindablePanel<Quadro10> {
    public Quadro10PanelExtension(Quadro10 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro10 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro10Bindings.doBindings(model, this);
        }
    }
}

