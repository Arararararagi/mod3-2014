/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro10Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label;
    protected JLabel anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label;
    protected JLabel anexoHq10C1001_base_label\u00a3anexoHq10C1001a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1001_num\u00a3anexoHq10C1001a_num;
    protected JMoneyTextField anexoHq10C1001;
    protected JMoneyTextField anexoHq10C1001a;
    protected JLabel anexoHq10C1002_base_label\u00a3anexoHq10C1002a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1002_num\u00a3anexoHq10C1002a_num;
    protected JMoneyTextField anexoHq10C1002;
    protected JMoneyTextField anexoHq10C1002a;
    protected JLabel anexoHq10C1003_base_label\u00a3anexoHq10C1003a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1003_num\u00a3anexoHq10C1003a_num;
    protected JMoneyTextField anexoHq10C1003;
    protected JMoneyTextField anexoHq10C1003a;
    protected JLabel anexoHq10C1004_base_label\u00a3anexoHq10C1004a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1004_num\u00a3anexoHq10C1004a_num;
    protected JMoneyTextField anexoHq10C1004;
    protected JMoneyTextField anexoHq10C1004a;
    protected JLabel anexoHq10C1005_base_label\u00a3anexoHq10C1005a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1005_num\u00a3anexoHq10C1005a_num;
    protected JMoneyTextField anexoHq10C1005;
    protected JMoneyTextField anexoHq10C1005a;
    protected JLabel anexoHq10C1006_base_label\u00a3anexoHq10C1006a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1006_num\u00a3anexoHq10C1006a_num;
    protected JMoneyTextField anexoHq10C1006;
    protected JMoneyTextField anexoHq10C1006a;
    protected JLabel anexoHq10C1007_base_label\u00a3anexoHq10C1007a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1007_num\u00a3anexoHq10C1007a_num;
    protected JMoneyTextField anexoHq10C1007;
    protected JMoneyTextField anexoHq10C1007a;
    protected JLabel anexoHq10C1008_base_label\u00a3anexoHq10C1008a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1008_num\u00a3anexoHq10C1008a_num;
    protected JMoneyTextField anexoHq10C1008;
    protected JMoneyTextField anexoHq10C1008a;
    protected JLabel anexoHq10C1009_base_label\u00a3anexoHq10C1009a_base_label;
    protected JLabelTextFieldNumbering anexoHq10C1009_num\u00a3anexoHq10C1009a_num;
    protected JMoneyTextField anexoHq10C1009;
    protected JLabel anexoHq10C1_base_label\u00a3anexoHq10C1a_base_label;
    protected JMoneyTextField anexoHq10C1;
    protected JMoneyTextField anexoHq10C1a;

    public Quadro10Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoHq10C1001_base_label\u00a3anexoHq10C1001a_base_label() {
        return this.anexoHq10C1001_base_label\u00a3anexoHq10C1001a_base_label;
    }

    public JLabel getAnexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label() {
        return this.anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1001_num\u00a3anexoHq10C1001a_num() {
        return this.anexoHq10C1001_num\u00a3anexoHq10C1001a_num;
    }

    public JMoneyTextField getAnexoHq10C1001a() {
        return this.anexoHq10C1001a;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1002_num\u00a3anexoHq10C1002a_num() {
        return this.anexoHq10C1002_num\u00a3anexoHq10C1002a_num;
    }

    public JMoneyTextField getAnexoHq10C1002a() {
        return this.anexoHq10C1002a;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1003_num\u00a3anexoHq10C1003a_num() {
        return this.anexoHq10C1003_num\u00a3anexoHq10C1003a_num;
    }

    public JMoneyTextField getAnexoHq10C1003a() {
        return this.anexoHq10C1003a;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1004_num\u00a3anexoHq10C1004a_num() {
        return this.anexoHq10C1004_num\u00a3anexoHq10C1004a_num;
    }

    public JMoneyTextField getAnexoHq10C1004a() {
        return this.anexoHq10C1004a;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1005_num\u00a3anexoHq10C1005a_num() {
        return this.anexoHq10C1005_num\u00a3anexoHq10C1005a_num;
    }

    public JMoneyTextField getAnexoHq10C1005a() {
        return this.anexoHq10C1005a;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1009_num\u00a3anexoHq10C1009a_num() {
        return this.anexoHq10C1009_num\u00a3anexoHq10C1009a_num;
    }

    public JMoneyTextField getAnexoHq10C1a() {
        return this.anexoHq10C1a;
    }

    public JLabel getAnexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label() {
        return this.anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label;
    }

    public JMoneyTextField getAnexoHq10C1001() {
        return this.anexoHq10C1001;
    }

    public JMoneyTextField getAnexoHq10C1002() {
        return this.anexoHq10C1002;
    }

    public JMoneyTextField getAnexoHq10C1003() {
        return this.anexoHq10C1003;
    }

    public JMoneyTextField getAnexoHq10C1004() {
        return this.anexoHq10C1004;
    }

    public JMoneyTextField getAnexoHq10C1005() {
        return this.anexoHq10C1005;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1006_num\u00a3anexoHq10C1006a_num() {
        return this.anexoHq10C1006_num\u00a3anexoHq10C1006a_num;
    }

    public JMoneyTextField getAnexoHq10C1006() {
        return this.anexoHq10C1006;
    }

    public JMoneyTextField getAnexoHq10C1006a() {
        return this.anexoHq10C1006a;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1007_num\u00a3anexoHq10C1007a_num() {
        return this.anexoHq10C1007_num\u00a3anexoHq10C1007a_num;
    }

    public JMoneyTextField getAnexoHq10C1007() {
        return this.anexoHq10C1007;
    }

    public JMoneyTextField getAnexoHq10C1007a() {
        return this.anexoHq10C1007a;
    }

    public JLabelTextFieldNumbering getAnexoHq10C1008_num\u00a3anexoHq10C1008a_num() {
        return this.anexoHq10C1008_num\u00a3anexoHq10C1008a_num;
    }

    public JMoneyTextField getAnexoHq10C1008() {
        return this.anexoHq10C1008;
    }

    public JMoneyTextField getAnexoHq10C1008a() {
        return this.anexoHq10C1008a;
    }

    public JMoneyTextField getAnexoHq10C1009() {
        return this.anexoHq10C1009;
    }

    public JMoneyTextField getAnexoHq10C1() {
        return this.anexoHq10C1;
    }

    public JLabel getAnexoHq10C1002_base_label\u00a3anexoHq10C1002a_base_label() {
        return this.anexoHq10C1002_base_label\u00a3anexoHq10C1002a_base_label;
    }

    public JLabel getAnexoHq10C1003_base_label\u00a3anexoHq10C1003a_base_label() {
        return this.anexoHq10C1003_base_label\u00a3anexoHq10C1003a_base_label;
    }

    public JLabel getAnexoHq10C1004_base_label\u00a3anexoHq10C1004a_base_label() {
        return this.anexoHq10C1004_base_label\u00a3anexoHq10C1004a_base_label;
    }

    public JLabel getAnexoHq10C1005_base_label\u00a3anexoHq10C1005a_base_label() {
        return this.anexoHq10C1005_base_label\u00a3anexoHq10C1005a_base_label;
    }

    public JLabel getAnexoHq10C1006_base_label\u00a3anexoHq10C1006a_base_label() {
        return this.anexoHq10C1006_base_label\u00a3anexoHq10C1006a_base_label;
    }

    public JLabel getAnexoHq10C1007_base_label\u00a3anexoHq10C1007a_base_label() {
        return this.anexoHq10C1007_base_label\u00a3anexoHq10C1007a_base_label;
    }

    public JLabel getAnexoHq10C1008_base_label\u00a3anexoHq10C1008a_base_label() {
        return this.anexoHq10C1008_base_label\u00a3anexoHq10C1008a_base_label;
    }

    public JLabel getAnexoHq10C1009_base_label\u00a3anexoHq10C1009a_base_label() {
        return this.anexoHq10C1009_base_label\u00a3anexoHq10C1009a_base_label;
    }

    public JLabel getAnexoHq10C1_base_label\u00a3anexoHq10C1a_base_label() {
        return this.anexoHq10C1_base_label\u00a3anexoHq10C1a_base_label;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoH");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label = new JLabel();
        this.anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label = new JLabel();
        this.anexoHq10C1001_base_label\u00a3anexoHq10C1001a_base_label = new JLabel();
        this.anexoHq10C1001_num\u00a3anexoHq10C1001a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1001 = new JMoneyTextField();
        this.anexoHq10C1001a = new JMoneyTextField();
        this.anexoHq10C1002_base_label\u00a3anexoHq10C1002a_base_label = new JLabel();
        this.anexoHq10C1002_num\u00a3anexoHq10C1002a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1002 = new JMoneyTextField();
        this.anexoHq10C1002a = new JMoneyTextField();
        this.anexoHq10C1003_base_label\u00a3anexoHq10C1003a_base_label = new JLabel();
        this.anexoHq10C1003_num\u00a3anexoHq10C1003a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1003 = new JMoneyTextField();
        this.anexoHq10C1003a = new JMoneyTextField();
        this.anexoHq10C1004_base_label\u00a3anexoHq10C1004a_base_label = new JLabel();
        this.anexoHq10C1004_num\u00a3anexoHq10C1004a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1004 = new JMoneyTextField();
        this.anexoHq10C1004a = new JMoneyTextField();
        this.anexoHq10C1005_base_label\u00a3anexoHq10C1005a_base_label = new JLabel();
        this.anexoHq10C1005_num\u00a3anexoHq10C1005a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1005 = new JMoneyTextField();
        this.anexoHq10C1005a = new JMoneyTextField();
        this.anexoHq10C1006_base_label\u00a3anexoHq10C1006a_base_label = new JLabel();
        this.anexoHq10C1006_num\u00a3anexoHq10C1006a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1006 = new JMoneyTextField();
        this.anexoHq10C1006a = new JMoneyTextField();
        this.anexoHq10C1007_base_label\u00a3anexoHq10C1007a_base_label = new JLabel();
        this.anexoHq10C1007_num\u00a3anexoHq10C1007a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1007 = new JMoneyTextField();
        this.anexoHq10C1007a = new JMoneyTextField();
        this.anexoHq10C1008_base_label\u00a3anexoHq10C1008a_base_label = new JLabel();
        this.anexoHq10C1008_num\u00a3anexoHq10C1008a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1008 = new JMoneyTextField();
        this.anexoHq10C1008a = new JMoneyTextField();
        this.anexoHq10C1009_base_label\u00a3anexoHq10C1009a_base_label = new JLabel();
        this.anexoHq10C1009_num\u00a3anexoHq10C1009a_num = new JLabelTextFieldNumbering();
        this.anexoHq10C1009 = new JMoneyTextField();
        this.anexoHq10C1_base_label\u00a3anexoHq10C1a_base_label = new JLabel();
        this.anexoHq10C1 = new JMoneyTextField();
        this.anexoHq10C1a = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro10Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro10Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("$rgap, 200dlu:grow, $lcgap, 30dlu, 2*($rgap, 75dlu), $rgap", "5*(default, $lgap), default, 0px, 6*($lgap, default)"));
        this.anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label.text"));
        this.anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label.setBorder(new EtchedBorder());
        this.anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1001_label\u00a3anexoHq10C1002_label\u00a3anexoHq10C1003_label\u00a3anexoHq10C1004_label\u00a3anexoHq10C1005_label\u00a3anexoHq10C1006_label\u00a3anexoHq10C1007_label\u00a3anexoHq10C1008_label\u00a3anexoHq10C1009_label\u00a3anexoHq10C1_label, cc.xy(6, 1));
        this.anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label.text"));
        this.anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label.setBorder(new EtchedBorder());
        this.anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1001a_label\u00a3anexoHq10C1002a_label\u00a3anexoHq10C1003a_label\u00a3anexoHq10C1004a_label\u00a3anexoHq10C1005a_label\u00a3anexoHq10C1006a_label\u00a3anexoHq10C1007a_label\u00a3anexoHq10C1008a_label\u00a3anexoHq10C1009a_label\u00a3anexoHq10C1a_label, cc.xy(8, 1));
        this.anexoHq10C1001_base_label\u00a3anexoHq10C1001a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1001_base_label\u00a3anexoHq10C1001a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1001_base_label\u00a3anexoHq10C1001a_base_label, cc.xy(2, 3));
        this.anexoHq10C1001_num\u00a3anexoHq10C1001a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1001_num\u00a3anexoHq10C1001a_num.text"));
        this.anexoHq10C1001_num\u00a3anexoHq10C1001a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1001_num\u00a3anexoHq10C1001a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1001_num\u00a3anexoHq10C1001a_num, cc.xy(4, 3));
        this.panel2.add((Component)this.anexoHq10C1001, cc.xy(6, 3));
        this.panel2.add((Component)this.anexoHq10C1001a, cc.xy(8, 3));
        this.anexoHq10C1002_base_label\u00a3anexoHq10C1002a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1002_base_label\u00a3anexoHq10C1002a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1002_base_label\u00a3anexoHq10C1002a_base_label, cc.xy(2, 5));
        this.anexoHq10C1002_num\u00a3anexoHq10C1002a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1002_num\u00a3anexoHq10C1002a_num.text"));
        this.anexoHq10C1002_num\u00a3anexoHq10C1002a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1002_num\u00a3anexoHq10C1002a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1002_num\u00a3anexoHq10C1002a_num, cc.xy(4, 5));
        this.panel2.add((Component)this.anexoHq10C1002, cc.xy(6, 5));
        this.panel2.add((Component)this.anexoHq10C1002a, cc.xy(8, 5));
        this.anexoHq10C1003_base_label\u00a3anexoHq10C1003a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1003_base_label\u00a3anexoHq10C1003a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1003_base_label\u00a3anexoHq10C1003a_base_label, cc.xy(2, 7));
        this.anexoHq10C1003_num\u00a3anexoHq10C1003a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1003_num\u00a3anexoHq10C1003a_num.text"));
        this.anexoHq10C1003_num\u00a3anexoHq10C1003a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1003_num\u00a3anexoHq10C1003a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1003_num\u00a3anexoHq10C1003a_num, cc.xy(4, 7));
        this.panel2.add((Component)this.anexoHq10C1003, cc.xy(6, 7));
        this.panel2.add((Component)this.anexoHq10C1003a, cc.xy(8, 7));
        this.anexoHq10C1004_base_label\u00a3anexoHq10C1004a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1004_base_label\u00a3anexoHq10C1004a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1004_base_label\u00a3anexoHq10C1004a_base_label, cc.xy(2, 9));
        this.anexoHq10C1004_num\u00a3anexoHq10C1004a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1004_num\u00a3anexoHq10C1004a_num.text"));
        this.anexoHq10C1004_num\u00a3anexoHq10C1004a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1004_num\u00a3anexoHq10C1004a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1004_num\u00a3anexoHq10C1004a_num, cc.xy(4, 9));
        this.panel2.add((Component)this.anexoHq10C1004, cc.xy(6, 9));
        this.panel2.add((Component)this.anexoHq10C1004a, cc.xy(8, 9));
        this.anexoHq10C1005_base_label\u00a3anexoHq10C1005a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1005_base_label\u00a3anexoHq10C1005a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1005_base_label\u00a3anexoHq10C1005a_base_label, cc.xy(2, 11));
        this.anexoHq10C1005_num\u00a3anexoHq10C1005a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1005_num\u00a3anexoHq10C1005a_num.text"));
        this.anexoHq10C1005_num\u00a3anexoHq10C1005a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1005_num\u00a3anexoHq10C1005a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1005_num\u00a3anexoHq10C1005a_num, cc.xy(4, 11));
        this.panel2.add((Component)this.anexoHq10C1005, cc.xy(6, 11));
        this.panel2.add((Component)this.anexoHq10C1005a, cc.xy(8, 11));
        this.anexoHq10C1006_base_label\u00a3anexoHq10C1006a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1006_base_label\u00a3anexoHq10C1006a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1006_base_label\u00a3anexoHq10C1006a_base_label, cc.xy(2, 14));
        this.anexoHq10C1006_num\u00a3anexoHq10C1006a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1006_num\u00a3anexoHq10C1006a_num.text"));
        this.anexoHq10C1006_num\u00a3anexoHq10C1006a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1006_num\u00a3anexoHq10C1006a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1006_num\u00a3anexoHq10C1006a_num, cc.xy(4, 14));
        this.panel2.add((Component)this.anexoHq10C1006, cc.xy(6, 14));
        this.panel2.add((Component)this.anexoHq10C1006a, cc.xy(8, 14));
        this.anexoHq10C1007_base_label\u00a3anexoHq10C1007a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1007_base_label\u00a3anexoHq10C1007a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1007_base_label\u00a3anexoHq10C1007a_base_label, cc.xy(2, 16));
        this.anexoHq10C1007_num\u00a3anexoHq10C1007a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1007_num\u00a3anexoHq10C1007a_num.text"));
        this.anexoHq10C1007_num\u00a3anexoHq10C1007a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1007_num\u00a3anexoHq10C1007a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1007_num\u00a3anexoHq10C1007a_num, cc.xy(4, 16));
        this.panel2.add((Component)this.anexoHq10C1007, cc.xy(6, 16));
        this.panel2.add((Component)this.anexoHq10C1007a, cc.xy(8, 16));
        this.anexoHq10C1008_base_label\u00a3anexoHq10C1008a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1008_base_label\u00a3anexoHq10C1008a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1008_base_label\u00a3anexoHq10C1008a_base_label, cc.xy(2, 18));
        this.anexoHq10C1008_num\u00a3anexoHq10C1008a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1008_num\u00a3anexoHq10C1008a_num.text"));
        this.anexoHq10C1008_num\u00a3anexoHq10C1008a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1008_num\u00a3anexoHq10C1008a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1008_num\u00a3anexoHq10C1008a_num, cc.xy(4, 18));
        this.panel2.add((Component)this.anexoHq10C1008, cc.xy(6, 18));
        this.panel2.add((Component)this.anexoHq10C1008a, cc.xy(8, 18));
        this.anexoHq10C1009_base_label\u00a3anexoHq10C1009a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1009_base_label\u00a3anexoHq10C1009a_base_label.text"));
        this.panel2.add((Component)this.anexoHq10C1009_base_label\u00a3anexoHq10C1009a_base_label, cc.xy(2, 20));
        this.anexoHq10C1009_num\u00a3anexoHq10C1009a_num.setText(bundle.getString("Quadro10Panel.anexoHq10C1009_num\u00a3anexoHq10C1009a_num.text"));
        this.anexoHq10C1009_num\u00a3anexoHq10C1009a_num.setBorder(new EtchedBorder());
        this.anexoHq10C1009_num\u00a3anexoHq10C1009a_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq10C1009_num\u00a3anexoHq10C1009a_num, cc.xy(4, 20));
        this.panel2.add((Component)this.anexoHq10C1009, cc.xy(6, 20));
        this.anexoHq10C1_base_label\u00a3anexoHq10C1a_base_label.setText(bundle.getString("Quadro10Panel.anexoHq10C1_base_label\u00a3anexoHq10C1a_base_label.text"));
        this.anexoHq10C1_base_label\u00a3anexoHq10C1a_base_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoHq10C1_base_label\u00a3anexoHq10C1a_base_label, cc.xywh(2, 22, 3, 1));
        this.anexoHq10C1.setEditable(false);
        this.anexoHq10C1.setColumns(15);
        this.panel2.add((Component)this.anexoHq10C1, cc.xy(6, 22));
        this.anexoHq10C1a.setEditable(false);
        this.anexoHq10C1a.setColumns(15);
        this.panel2.add((Component)this.anexoHq10C1a, cc.xy(8, 22));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.add((Component)this.this2, "Center");
    }
}

