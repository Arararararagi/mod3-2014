/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro08Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro08PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro08PanelExtension
extends Quadro08PanelBase
implements IBindablePanel<Quadro08>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q08_T814 = 16;

    public Quadro08PanelExtension(Quadro08 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro08 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro08Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoHq08T814_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoHq08T814() != null && this.model.getAnexoHq08T814().size() >= 16) {
            return;
        }
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            AnexoHq08T814_Linha linha = new AnexoHq08T814_Linha();
            RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
            if (rosto.getQuadro02().getQ02C02() != null && rosto.getQuadro02().getQ02C02() >= 2008) {
                linha.setClassificacaoA("N");
            }
            this.model.getAnexoHq08T814().add(linha);
        }
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoHModel anexoH = (AnexoHModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoHModel.class);
            if (anexoH != null) {
                EventList<AnexoHq08T1_Linha> anexoHq08T1Rows = anexoH.getQuadro08().getAnexoHq08T1();
                EventList<AnexoHq08T814_Linha> anexoHq08T814Rows = anexoH.getQuadro08().getAnexoHq08T814();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInHq08T1(anexoHq08T1Rows, label);
                        this.removeNifByLabelInHq08T814(anexoHq08T814Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInHq08T1(anexoHq08T1Rows, label + (index + 1));
                        this.removeNifByLabelInHq08T814(anexoHq08T814Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexHq08T1Elements(anexoHq08T1Rows, label, index);
                            this.reindexHq08T814Elements(anexoHq08T814Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInHq08T1(anexoHq08T1Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInHq08T1(EventList<AnexoHq08T1_Linha> anexoHq08T1Rows, String labelPrefix) {
        if (anexoHq08T1Rows != null) {
            for (AnexoHq08T1_Linha anexoHq08T1_Linha : anexoHq08T1Rows) {
                if (anexoHq08T1_Linha == null || anexoHq08T1_Linha.getNIF() == null || !anexoHq08T1_Linha.getNIF().startsWith(labelPrefix)) continue;
                anexoHq08T1_Linha.setNIF(null);
            }
        }
    }

    private void removeNifByLabelInHq08T814(EventList<AnexoHq08T814_Linha> anexoHq08T814Rows, String labelPrefix) {
        if (anexoHq08T814Rows != null) {
            for (AnexoHq08T814_Linha anexoHq08T814_Linha : anexoHq08T814Rows) {
                if (anexoHq08T814_Linha == null || anexoHq08T814_Linha.getTitular() == null || !anexoHq08T814_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoHq08T814_Linha.setTitular(null);
            }
        }
    }

    private void reindexHq08T1Elements(EventList<AnexoHq08T1_Linha> anexoHq08T1Rows, String labelPrefix, int index) {
        if (anexoHq08T1Rows != null) {
            for (AnexoHq08T1_Linha anexoHq08T1_Linha : anexoHq08T1Rows) {
                int currIndex;
                if (anexoHq08T1_Linha == null || anexoHq08T1_Linha.getNIF() == null || !anexoHq08T1_Linha.getNIF().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoHq08T1_Linha.getNIF(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoHq08T1_Linha.setNIF(labelPrefix + (currIndex - 1));
            }
        }
    }

    private void reindexHq08T814Elements(EventList<AnexoHq08T814_Linha> anexoHq08T814Rows, String labelPrefix, int index) {
        if (anexoHq08T814Rows != null) {
            for (AnexoHq08T814_Linha anexoHq08T814_Linha : anexoHq08T814Rows) {
                int currIndex;
                if (anexoHq08T814_Linha == null || anexoHq08T814_Linha.getTitular() == null || !anexoHq08T814_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoHq08T814_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoHq08T814_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

