/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro09Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro09PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro09PanelExtension
extends Quadro09PanelBase
implements IBindablePanel<Quadro09> {
    public Quadro09PanelExtension(Quadro09 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro09 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro09Bindings.doBindings(model, this);
        }
    }
}

