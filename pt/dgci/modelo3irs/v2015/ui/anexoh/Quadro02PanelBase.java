/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro02;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro02Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro02PanelBase
extends Quadro02Panel
implements IBindablePanel<Quadro02> {
    private static final long serialVersionUID = 1;
    protected Quadro02 model;

    @Override
    public abstract void setModel(Quadro02 var1, boolean var2);

    @Override
    public Quadro02 getModel() {
        return this.model;
    }

    public Quadro02PanelBase(Quadro02 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

