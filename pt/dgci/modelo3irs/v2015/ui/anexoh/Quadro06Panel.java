/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label1;
    protected JLabel label2;
    protected JLabel anexoHq06C601_label;
    protected JLabelTextFieldNumbering anexoHq06C601_num;
    protected JMoneyTextField anexoHq06C601;
    protected JLabel anexoHq06C602_label;
    protected JLabelTextFieldNumbering anexoHq06C602_num;
    protected JMoneyTextField anexoHq06C602;
    protected JLabel anexoHq06C603_label;
    protected JLabelTextFieldNumbering anexoHq06C603_num;
    protected JMoneyTextField anexoHq06C603;
    protected JLabel anexoHq06C1_label;
    protected JMoneyTextField anexoHq06C1;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoHq06T1Scroll;
    protected JTable anexoHq06T1;

    public Quadro06Panel() {
        this.initComponents();
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getAnexoHq06C601_label() {
        return this.anexoHq06C601_label;
    }

    public JLabelTextFieldNumbering getAnexoHq06C601_num() {
        return this.anexoHq06C601_num;
    }

    public JMoneyTextField getAnexoHq06C601() {
        return this.anexoHq06C601;
    }

    public JLabel getAnexoHq06C602_label() {
        return this.anexoHq06C602_label;
    }

    public JLabelTextFieldNumbering getAnexoHq06C602_num() {
        return this.anexoHq06C602_num;
    }

    public JMoneyTextField getAnexoHq06C602() {
        return this.anexoHq06C602;
    }

    public JLabel getAnexoHq06C603_label() {
        return this.anexoHq06C603_label;
    }

    public JLabelTextFieldNumbering getAnexoHq06C603_num() {
        return this.anexoHq06C603_num;
    }

    public JMoneyTextField getAnexoHq06C603() {
        return this.anexoHq06C603;
    }

    public JLabel getAnexoHq06C1_label() {
        return this.anexoHq06C1_label;
    }

    public JMoneyTextField getAnexoHq06C1() {
        return this.anexoHq06C1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    protected void removeLineAnexoHq06T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoHq06T1Scroll() {
        return this.anexoHq06T1Scroll;
    }

    public JTable getAnexoHq06T1() {
        return this.anexoHq06T1;
    }

    protected void addLineAnexoHq06T1_LinhaActionPerformed(ActionEvent e) {
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoH");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label1 = new JLabel();
        this.label2 = new JLabel();
        this.anexoHq06C601_label = new JLabel();
        this.anexoHq06C601_num = new JLabelTextFieldNumbering();
        this.anexoHq06C601 = new JMoneyTextField();
        this.anexoHq06C602_label = new JLabel();
        this.anexoHq06C602_num = new JLabelTextFieldNumbering();
        this.anexoHq06C602 = new JMoneyTextField();
        this.anexoHq06C603_label = new JLabel();
        this.anexoHq06C603_num = new JLabelTextFieldNumbering();
        this.anexoHq06C603 = new JMoneyTextField();
        this.anexoHq06C1_label = new JLabel();
        this.anexoHq06C1 = new JMoneyTextField();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoHq06T1Scroll = new JScrollPane();
        this.anexoHq06T1 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 3*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default, $lcgap, default:grow, $lcgap, 25dlu, $rgap, 75dlu, $rgap", "4*(default, $lgap), default"));
        this.label1.setText(bundle.getString("Quadro06Panel.label1.text"));
        this.label1.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label1, cc.xywh(1, 1, 3, 1));
        this.label2.setText(bundle.getString("Quadro06Panel.label2.text"));
        this.label2.setBorder(new EtchedBorder());
        this.label2.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label2, cc.xywh(5, 1, 3, 1));
        this.anexoHq06C601_label.setText(bundle.getString("Quadro06Panel.anexoHq06C601_label.text"));
        this.panel2.add((Component)this.anexoHq06C601_label, cc.xy(1, 3));
        this.anexoHq06C601_num.setText(bundle.getString("Quadro06Panel.anexoHq06C601_num.text"));
        this.anexoHq06C601_num.setBorder(new EtchedBorder());
        this.anexoHq06C601_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq06C601_num, cc.xy(5, 3));
        this.panel2.add((Component)this.anexoHq06C601, cc.xy(7, 3));
        this.anexoHq06C602_label.setText(bundle.getString("Quadro06Panel.anexoHq06C602_label.text"));
        this.panel2.add((Component)this.anexoHq06C602_label, cc.xy(1, 5));
        this.anexoHq06C602_num.setText(bundle.getString("Quadro06Panel.anexoHq06C602_num.text"));
        this.anexoHq06C602_num.setBorder(new EtchedBorder());
        this.anexoHq06C602_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq06C602_num, cc.xy(5, 5));
        this.panel2.add((Component)this.anexoHq06C602, cc.xy(7, 5));
        this.anexoHq06C603_label.setText(bundle.getString("Quadro06Panel.anexoHq06C603_label.text"));
        this.panel2.add((Component)this.anexoHq06C603_label, cc.xy(1, 7));
        this.anexoHq06C603_num.setText(bundle.getString("Quadro06Panel.anexoHq06C603_num.text"));
        this.anexoHq06C603_num.setBorder(new EtchedBorder());
        this.anexoHq06C603_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoHq06C603_num, cc.xy(5, 7));
        this.panel2.add((Component)this.anexoHq06C603, cc.xy(7, 7));
        this.anexoHq06C1_label.setText(bundle.getString("Quadro06Panel.anexoHq06C1_label.text"));
        this.anexoHq06C1_label.setBorder(null);
        this.anexoHq06C1_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoHq06C1_label, cc.xy(5, 9));
        this.anexoHq06C1.setEditable(false);
        this.anexoHq06C1.setColumns(15);
        this.panel2.add((Component)this.anexoHq06C1, cc.xy(7, 9));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $rgap, 80dlu, $lcgap, default:grow", "$ugap, $lgap, default, $lgap, 165dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro06Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro06Panel.this.addLineAnexoHq06T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro06Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro06Panel.this.removeLineAnexoHq06T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 3));
        this.anexoHq06T1Scroll.setViewportView(this.anexoHq06T1);
        this.panel3.add((Component)this.anexoHq06T1Scroll, cc.xywh(3, 5, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 8));
        this.add((Component)this.this2, "Center");
    }

}

