/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro09Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label2;
    protected JLabel anexoHq09C901_label;
    protected JLabel anexoHq09C901_label2;
    protected JLabel anexoHq09B1OP1_label;
    protected JRadioButton anexoHq09B1OP1;
    protected JLabel anexoHq09C901_label3;
    protected JPanel panel5;
    protected JCheckBox anexoHq09C901IVA;
    protected JPanel panel4;
    protected JCheckBox anexoHq09C901IRS;
    protected JPanel panel3;
    protected JLabelTextFieldNumbering label6;
    protected JNIFTextField anexoHq09C901;
    protected JLabel anexoHq09B1OP2_label;
    protected JRadioButton anexoHq09B1OP2;

    public Quadro09Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoHq09B1OP2_label() {
        return this.anexoHq09B1OP2_label;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JRadioButton getAnexoHq09B1OP1() {
        return this.anexoHq09B1OP1;
    }

    public JRadioButton getAnexoHq09B1OP2() {
        return this.anexoHq09B1OP2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoHq09C901_label() {
        return this.anexoHq09C901_label;
    }

    public JLabelTextFieldNumbering getLabel6() {
        return this.label6;
    }

    public JNIFTextField getAnexoHq09C901() {
        return this.anexoHq09C901;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoHq09B1OP1_label() {
        return this.anexoHq09B1OP1_label;
    }

    public JLabel getAnexoHq09C901_label2() {
        return this.anexoHq09C901_label2;
    }

    public JLabel getAnexoHq09C901_label3() {
        return this.anexoHq09C901_label3;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JCheckBox getAnexoHq09C901IRS() {
        return this.anexoHq09C901IRS;
    }

    public JCheckBox getAnexoHq09C901IVA() {
        return this.anexoHq09C901IVA;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoH");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label2 = new JLabel();
        this.anexoHq09C901_label = new JLabel();
        this.anexoHq09C901_label2 = new JLabel();
        this.anexoHq09B1OP1_label = new JLabel();
        this.anexoHq09B1OP1 = new JRadioButton();
        this.anexoHq09C901_label3 = new JLabel();
        this.panel5 = new JPanel();
        this.anexoHq09C901IVA = new JCheckBox();
        this.panel4 = new JPanel();
        this.anexoHq09C901IRS = new JCheckBox();
        this.panel3 = new JPanel();
        this.label6 = new JLabelTextFieldNumbering();
        this.anexoHq09C901 = new JNIFTextField();
        this.anexoHq09B1OP2_label = new JLabel();
        this.anexoHq09B1OP2 = new JRadioButton();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro09Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro09Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("17dlu, $lcgap, left:288dlu:grow, $lcgap, right:15dlu, $lcgap, default, 2*($lcgap, min), $lcgap, default:grow", "$ugap, 6*($lgap, default)"));
        this.label2.setText(bundle.getString("Quadro09Panel.label2.text"));
        this.label2.setBorder(new EtchedBorder());
        this.label2.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label2, cc.xywh(3, 3, 3, 1));
        this.anexoHq09C901_label.setText(bundle.getString("Quadro09Panel.anexoHq09C901_label.text"));
        this.anexoHq09C901_label.setHorizontalAlignment(0);
        this.anexoHq09C901_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoHq09C901_label, cc.xy(7, 3));
        this.anexoHq09C901_label2.setText(bundle.getString("Quadro09Panel.anexoHq09C901_label2.text"));
        this.anexoHq09C901_label2.setHorizontalAlignment(0);
        this.anexoHq09C901_label2.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoHq09C901_label2, cc.xy(9, 3));
        this.anexoHq09B1OP1_label.setText(bundle.getString("Quadro09Panel.anexoHq09B1OP1_label.text"));
        this.anexoHq09B1OP1_label.setOpaque(true);
        this.panel2.add((Component)this.anexoHq09B1OP1_label, cc.xy(3, 7));
        this.anexoHq09B1OP1.setHorizontalAlignment(4);
        this.anexoHq09B1OP1.setSelectedIcon(null);
        this.anexoHq09B1OP1.setHorizontalTextPosition(10);
        this.panel2.add((Component)this.anexoHq09B1OP1, cc.xywh(4, 7, 2, 1));
        this.anexoHq09C901_label3.setText(bundle.getString("Quadro09Panel.anexoHq09C901_label3.text"));
        this.anexoHq09C901_label3.setHorizontalAlignment(0);
        this.anexoHq09C901_label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoHq09C901_label3, cc.xy(11, 3));
        this.panel5.setLayout(new FormLayout("0px, center:23dlu", "$glue, $lgap, default, $lgap, $glue"));
        this.panel5.add((Component)this.anexoHq09C901IVA, cc.xy(2, 3));
        this.panel2.add((Component)this.panel5, cc.xywh(11, 7, 1, 3));
        this.panel4.setLayout(new FormLayout("0px, center:21dlu", "$glue, $lgap, default, $lgap, $glue"));
        this.panel4.add((Component)this.anexoHq09C901IRS, cc.xy(2, 3));
        this.panel2.add((Component)this.panel4, cc.xywh(9, 7, 1, 3));
        this.panel3.setLayout(new FormLayout("25dlu, 0px, $lcgap, 43dlu", "$glue, $lgap, fill:default, $lgap, $glue"));
        this.label6.setText(bundle.getString("Quadro09Panel.label6.text"));
        this.label6.setBorder(new EtchedBorder());
        this.label6.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label6, cc.xy(1, 3));
        this.anexoHq09C901.setColumns(9);
        this.panel3.add((Component)this.anexoHq09C901, cc.xy(4, 3));
        this.panel2.add((Component)this.panel3, cc.xywh(7, 7, 1, 3));
        this.anexoHq09B1OP2_label.setText(bundle.getString("Quadro09Panel.anexoHq09B1OP2_label.text"));
        this.anexoHq09B1OP2_label.setOpaque(true);
        this.panel2.add((Component)this.anexoHq09B1OP2_label, cc.xy(3, 9));
        this.anexoHq09B1OP2.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoHq09B1OP2, cc.xywh(4, 9, 2, 1));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.add((Component)this.this2, "Center");
    }
}

