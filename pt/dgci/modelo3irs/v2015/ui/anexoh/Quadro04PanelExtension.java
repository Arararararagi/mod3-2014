/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro04PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q04_T4 = 30;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoHq04T4().getTableHeader().getColumnModel();
        ColumnGroup entidadePagadora = new ColumnGroup("Nif da Entidade Pagadora/Retentora de IRS");
        entidadePagadora.add(cm.getColumn(5));
        entidadePagadora.add(cm.getColumn(6));
        entidadePagadora.add(cm.getColumn(7));
        if (this.getAnexoHq04T4().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoHq04T4().getTableHeader()).addColumnGroup(entidadePagadora);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(entidadePagadora);
        this.getAnexoHq04T4().getColumnModel().getColumn(7).setPreferredWidth(200);
        this.getAnexoHq04T4().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoHq04T4() != null && this.model.getAnexoHq04T4().size() >= 30) {
            return;
        }
        super.addLineAnexoHq04T4_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoHModel anexoH = (AnexoHModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoHModel.class);
            if (anexoH != null) {
                EventList<AnexoHq04T4_Linha> anexoHq04T4Rows = anexoH.getQuadro04().getAnexoHq04T4();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInHq04T4(anexoHq04T4Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInHq04T4(anexoHq04T4Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexHq04T4Elements(anexoHq04T4Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInHq04T4(anexoHq04T4Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInHq04T4(EventList<AnexoHq04T4_Linha> anexoHq04T4Rows, String labelPrefix) {
        if (anexoHq04T4Rows != null) {
            for (AnexoHq04T4_Linha anexoHq04T4_Linha : anexoHq04T4Rows) {
                if (anexoHq04T4_Linha == null || anexoHq04T4_Linha.getTitular() == null || !anexoHq04T4_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoHq04T4_Linha.setTitular(null);
            }
        }
    }

    private void reindexHq04T4Elements(EventList<AnexoHq04T4_Linha> anexoHq04T4Rows, String labelPrefix, int index) {
        if (anexoHq04T4Rows != null) {
            for (AnexoHq04T4_Linha anexoHq04T4_Linha : anexoHq04T4Rows) {
                int currIndex;
                if (anexoHq04T4_Linha == null || anexoHq04T4_Linha.getTitular() == null || !anexoHq04T4_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoHq04T4_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoHq04T4_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

