/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro06Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro06PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro06PanelExtension
extends Quadro06PanelBase
implements IBindablePanel<Quadro06> {
    public static final int MAX_LINES_Q06_T1 = 16;

    public Quadro06PanelExtension(Quadro06 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro06 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro06Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoHq06T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoHq06T1() != null && this.model.getAnexoHq06T1().size() >= 16) {
            return;
        }
        super.addLineAnexoHq06T1_LinhaActionPerformed(e);
    }
}

