/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro05Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro05PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro05PanelExtension
extends Quadro05PanelBase
implements IBindablePanel<Quadro05>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q05_T5 = 16;

    public Quadro05PanelExtension(Quadro05 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro05 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro05Bindings.doBindings(model, this);
        }
        if (!Modelo3IRSv2015Parameters.instance().isFase2()) {
            this.button1.setEnabled(false);
            this.button2.setEnabled(false);
            this.anexoHq05T5.setEnabled(false);
            this.anexoHq05T5Scroll.setEnabled(false);
            this.anexoHq05C1.setEnabled(false);
        }
    }

    @Override
    protected void addLineAnexoHq05T5_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoHq05T5() != null && this.model.getAnexoHq05T5().size() >= 16) {
            return;
        }
        super.addLineAnexoHq05T5_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoHModel anexoH = (AnexoHModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoHModel.class);
            if (anexoH != null) {
                EventList<AnexoHq05T5_Linha> anexoHq05T5Rows = anexoH.getQuadro05().getAnexoHq05T5();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInHq05T5(anexoHq05T5Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInHq05T5(anexoHq05T5Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexHq05T5Elements(anexoHq05T5Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInHq05T5(anexoHq05T5Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInHq05T5(EventList<AnexoHq05T5_Linha> anexoHq05T5Rows, String labelPrefix) {
        if (anexoHq05T5Rows != null) {
            for (AnexoHq05T5_Linha anexoHq05T5_Linha : anexoHq05T5Rows) {
                if (anexoHq05T5_Linha == null || anexoHq05T5_Linha.getTitular() == null || !anexoHq05T5_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoHq05T5_Linha.setTitular(null);
            }
        }
    }

    private void reindexHq05T5Elements(EventList<AnexoHq05T5_Linha> anexoHq05T5Rows, String labelPrefix, int index) {
        if (anexoHq05T5Rows != null) {
            for (AnexoHq05T5_Linha anexoHq05T5_Linha : anexoHq05T5Rows) {
                int currIndex;
                if (anexoHq05T5_Linha == null || anexoHq05T5_Linha.getTitular() == null || !anexoHq05T5_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoHq05T5_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoHq05T5_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

