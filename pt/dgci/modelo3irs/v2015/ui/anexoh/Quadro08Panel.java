/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro08Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel9;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoHq08T814Scroll2;
    protected JTable anexoHq08T1;
    protected JLabel label43;
    protected JPanel panel8;
    protected JLabelTextFieldNumbering label35;
    protected JLabel label14;
    protected JPanel panel5;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoHq08T814Scroll;
    protected JTable anexoHq08T814;
    protected JPanel panel6;
    protected JLabel anexoHq08C814_label;
    protected JEditableComboBox anexoHq08C814;
    protected JPanel panel12;
    protected JLabelTextFieldNumbering label46;
    protected JLabel label47;
    protected JPanel panel7;
    protected JLabel anexoHq08C81501_label;
    protected JLabelTextFieldNumbering anexoHq08C81501_num;
    protected JMoneyTextField anexoHq08C81501;
    protected JLabel anexoHq08C81502_label;
    protected JLabelTextFieldNumbering anexoHq08C81502_num;
    protected JMoneyTextField anexoHq08C81502;
    protected JPanel panel10;
    protected JLabel anexoHq08C81601_label;
    protected JLabelTextFieldNumbering anexoHq08C81601_num;
    protected JLimitedTextField anexoHq08C81601;
    protected JLabel anexoHq08C81602_label;
    protected JLabelTextFieldNumbering anexoHq08C81602_num;
    protected JLimitedTextField anexoHq08C81602;
    protected JPanel panel13;
    protected JLabelTextFieldNumbering label48;
    protected JLabel label49;

    public Quadro08Panel() {
        this.initComponents();
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JScrollPane getAnexoHq08T814Scroll() {
        return this.anexoHq08T814Scroll;
    }

    public JTable getAnexoHq08T814() {
        return this.anexoHq08T814;
    }

    protected void addLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JLabel getAnexoHq08C814_label() {
        return this.anexoHq08C814_label;
    }

    public JEditableComboBox getAnexoHq08C814() {
        return this.anexoHq08C814;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoHq08C81501_label() {
        return this.anexoHq08C81501_label;
    }

    public JLabelTextFieldNumbering getAnexoHq08C81501_num() {
        return this.anexoHq08C81501_num;
    }

    public JMoneyTextField getAnexoHq08C81501() {
        return this.anexoHq08C81501;
    }

    public JLabel getAnexoHq08C81502_label() {
        return this.anexoHq08C81502_label;
    }

    public JLabelTextFieldNumbering getAnexoHq08C81502_num() {
        return this.anexoHq08C81502_num;
    }

    public JMoneyTextField getAnexoHq08C81502() {
        return this.anexoHq08C81502;
    }

    protected void addLineAnexoHq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoHq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JLabel getLabel43() {
        return this.label43;
    }

    public JPanel getPanel12() {
        return this.panel12;
    }

    public JLabelTextFieldNumbering getLabel46() {
        return this.label46;
    }

    public JLabel getLabel47() {
        return this.label47;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JLabelTextFieldNumbering getLabel35() {
        return this.label35;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    protected void addLineAnexoHq08T814_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoHq08T814_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoHq08T814Scroll2() {
        return this.anexoHq08T814Scroll2;
    }

    public JTable getAnexoHq08T1() {
        return this.anexoHq08T1;
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabel getAnexoHq08C81601_label() {
        return this.anexoHq08C81601_label;
    }

    public JLabelTextFieldNumbering getAnexoHq08C81601_num() {
        return this.anexoHq08C81601_num;
    }

    public JLimitedTextField getAnexoHq08C81601() {
        return this.anexoHq08C81601;
    }

    public JLabel getAnexoHq08C81602_label() {
        return this.anexoHq08C81602_label;
    }

    public JLabelTextFieldNumbering getAnexoHq08C81602_num() {
        return this.anexoHq08C81602_num;
    }

    public JLimitedTextField getAnexoHq08C81602() {
        return this.anexoHq08C81602;
    }

    public JPanel getPanel13() {
        return this.panel13;
    }

    public JLabelTextFieldNumbering getLabel48() {
        return this.label48;
    }

    public JLabel getLabel49() {
        return this.label49;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoH");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel9 = new JPanel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoHq08T814Scroll2 = new JScrollPane();
        this.anexoHq08T1 = new JTable();
        this.label43 = new JLabel();
        this.panel8 = new JPanel();
        this.label35 = new JLabelTextFieldNumbering();
        this.label14 = new JLabel();
        this.panel5 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoHq08T814Scroll = new JScrollPane();
        this.anexoHq08T814 = new JTable();
        this.panel6 = new JPanel();
        this.anexoHq08C814_label = new JLabel();
        this.anexoHq08C814 = new JEditableComboBox();
        this.panel12 = new JPanel();
        this.label46 = new JLabelTextFieldNumbering();
        this.label47 = new JLabel();
        this.panel7 = new JPanel();
        this.anexoHq08C81501_label = new JLabel();
        this.anexoHq08C81501_num = new JLabelTextFieldNumbering();
        this.anexoHq08C81501 = new JMoneyTextField();
        this.anexoHq08C81502_label = new JLabel();
        this.anexoHq08C81502_num = new JLabelTextFieldNumbering();
        this.anexoHq08C81502 = new JMoneyTextField();
        this.panel10 = new JPanel();
        this.panel10.setVisible(Modelo3IRSv2015Parameters.instance().isDC());
        this.anexoHq08C81601_label = new JLabel();
        this.anexoHq08C81601_num = new JLabelTextFieldNumbering();
        this.anexoHq08C81601 = new JLimitedTextField();
        this.anexoHq08C81602_label = new JLabel();
        this.anexoHq08C81602_num = new JLabelTextFieldNumbering();
        this.anexoHq08C81602 = new JLimitedTextField();
        this.panel13 = new JPanel();
        this.panel13.setVisible(Modelo3IRSv2015Parameters.instance().isDC());
        this.label48 = new JLabelTextFieldNumbering();
        this.label49 = new JLabel();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro08Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro08Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default", "$rgap, default, $lgap, 0px, 8*(default, $lgap), default"));
        this.panel9.setMinimumSize(null);
        this.panel9.setPreferredSize(null);
        this.panel9.setLayout(new FormLayout("3*(default, $lcgap), 552dlu, $lcgap, 14dlu:grow, $rgap", "default, $lgap, 165dlu, 2*($lgap, default)"));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button3.setText(bundle.getString("Quadro08Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.addLineAnexoHq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro08Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.removeLineAnexoHq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel9.add((Component)this.toolBar2, cc.xy(3, 1));
        this.anexoHq08T814Scroll2.setViewportView(this.anexoHq08T1);
        this.panel9.add((Component)this.anexoHq08T814Scroll2, cc.xywh(3, 3, 5, 1));
        this.this2.add((Component)this.panel9, cc.xy(2, 2));
        this.this2.add((Component)this.label43, cc.xy(2, 5));
        this.panel8.setMinimumSize(null);
        this.panel8.setPreferredSize(null);
        this.panel8.setLayout(new FormLayout("$ugap, $rgap, 20dlu, 2*($lcgap, default), $lcgap, default:grow, $lcgap, default, $rgap", "$ugap, 2*($lgap, default)"));
        this.label35.setText(bundle.getString("Quadro08Panel.label35.text"));
        this.label35.setBorder(new EtchedBorder());
        this.label35.setHorizontalAlignment(0);
        this.panel8.add((Component)this.label35, cc.xy(3, 3));
        this.label14.setText(bundle.getString("Quadro08Panel.label14.text"));
        this.panel8.add((Component)this.label14, cc.xy(5, 3));
        this.this2.add((Component)this.panel8, cc.xy(2, 9));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("3*(default, $lcgap), 551dlu, $lcgap, default:grow, $rgap", "default, $lgap, 165dlu, 2*($lgap, default)"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro08Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.addLineAnexoHq08T814_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro08Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.removeLineAnexoHq08T814_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel5.add((Component)this.toolBar1, cc.xy(3, 1));
        this.anexoHq08T814Scroll.setViewportView(this.anexoHq08T814);
        this.panel5.add((Component)this.anexoHq08T814Scroll, cc.xywh(3, 3, 5, 1));
        this.this2.add((Component)this.panel5, cc.xy(2, 11));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("$ugap, 2*($rgap, default), $rgap", "$ugap, 2*($lgap, default)"));
        this.anexoHq08C814_label.setText(bundle.getString("Quadro08Panel.anexoHq08C814_label.text"));
        this.anexoHq08C814_label.setHorizontalAlignment(4);
        this.panel6.add((Component)this.anexoHq08C814_label, cc.xy(3, 3));
        this.panel6.add((Component)this.anexoHq08C814, cc.xy(5, 3));
        this.this2.add((Component)this.panel6, cc.xy(2, 13));
        this.panel12.setMinimumSize(null);
        this.panel12.setPreferredSize(null);
        this.panel12.setLayout(new FormLayout("$ugap, $rgap, 20dlu, $rgap, default, $rgap, 50dlu, $rgap", "$ugap, 2*($lgap, default)"));
        this.label46.setText(bundle.getString("Quadro08Panel.label46.text"));
        this.label46.setBorder(new EtchedBorder());
        this.label46.setHorizontalAlignment(0);
        this.panel12.add((Component)this.label46, cc.xy(3, 3));
        this.label47.setText(bundle.getString("Quadro08Panel.label47.text"));
        this.panel12.add((Component)this.label47, cc.xy(5, 3));
        this.this2.add((Component)this.panel12, cc.xy(2, 15));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("default:grow, $lcgap, left:[140dlu,default], $ugap, 13dlu, 0px, $lcgap, 75dlu, $lcgap, default:grow", "$ugap, 4*($lgap, default)"));
        this.anexoHq08C81501_label.setText(bundle.getString("Quadro08Panel.anexoHq08C81501_label.text"));
        this.anexoHq08C81501_label.setHorizontalAlignment(4);
        this.anexoHq08C81501_label.setMaximumSize(new Dimension(200, 14));
        this.anexoHq08C81501_label.setMinimumSize(new Dimension(200, 14));
        this.anexoHq08C81501_label.setPreferredSize(new Dimension(200, 14));
        this.panel7.add((Component)this.anexoHq08C81501_label, cc.xy(3, 3));
        this.anexoHq08C81501_num.setText(bundle.getString("Quadro08Panel.anexoHq08C81501_num.text"));
        this.anexoHq08C81501_num.setHorizontalAlignment(0);
        this.anexoHq08C81501_num.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.anexoHq08C81501_num, cc.xy(5, 3));
        this.panel7.add((Component)this.anexoHq08C81501, cc.xy(8, 3));
        this.anexoHq08C81502_label.setText(bundle.getString("Quadro08Panel.anexoHq08C81502_label.text"));
        this.panel7.add((Component)this.anexoHq08C81502_label, cc.xy(3, 5));
        this.anexoHq08C81502_num.setText(bundle.getString("Quadro08Panel.anexoHq08C81502_num.text"));
        this.anexoHq08C81502_num.setHorizontalAlignment(0);
        this.anexoHq08C81502_num.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.anexoHq08C81502_num, cc.xy(5, 5));
        this.panel7.add((Component)this.anexoHq08C81502, cc.xy(8, 5));
        this.this2.add((Component)this.panel7, cc.xy(2, 17));
        this.panel10.setLayout(new FormLayout("default:grow, $lcgap, [140dlu,default], $ugap, 13dlu, $lcgap, 75dlu, $lcgap, default:grow", "3*(default, $lgap), default"));
        this.anexoHq08C81601_label.setText(bundle.getString("Quadro08Panel.anexoHq08C81601_label.text"));
        this.anexoHq08C81601_label.setHorizontalAlignment(4);
        this.anexoHq08C81601_label.setMaximumSize(new Dimension(200, 14));
        this.anexoHq08C81601_label.setMinimumSize(new Dimension(200, 14));
        this.anexoHq08C81601_label.setPreferredSize(new Dimension(200, 14));
        this.panel10.add((Component)this.anexoHq08C81601_label, cc.xy(3, 1));
        this.anexoHq08C81601_num.setText(bundle.getString("Quadro08Panel.anexoHq08C81601_num.text"));
        this.anexoHq08C81601_num.setHorizontalAlignment(0);
        this.anexoHq08C81601_num.setBorder(new EtchedBorder());
        this.panel10.add((Component)this.anexoHq08C81601_num, cc.xy(5, 1));
        this.anexoHq08C81601.setMaxLength(16);
        this.panel10.add((Component)this.anexoHq08C81601, cc.xy(7, 1));
        this.anexoHq08C81602_label.setText(bundle.getString("Quadro08Panel.anexoHq08C81602_label.text"));
        this.anexoHq08C81602_label.setHorizontalAlignment(4);
        this.panel10.add((Component)this.anexoHq08C81602_label, cc.xy(3, 3));
        this.anexoHq08C81602_num.setText(bundle.getString("Quadro08Panel.anexoHq08C81602_num.text"));
        this.anexoHq08C81602_num.setHorizontalAlignment(0);
        this.anexoHq08C81602_num.setBorder(new EtchedBorder());
        this.panel10.add((Component)this.anexoHq08C81602_num, cc.xy(5, 3));
        this.anexoHq08C81602.setMaxLength(15);
        this.panel10.add((Component)this.anexoHq08C81602, cc.xy(7, 3));
        this.this2.add((Component)this.panel10, cc.xy(2, 21));
        this.panel13.setMinimumSize(null);
        this.panel13.setPreferredSize(null);
        this.panel13.setLayout(new FormLayout("$ugap, $rgap, 20dlu, $rgap, default, $rgap, 50dlu, $rgap", "$ugap, 2*($lgap, default)"));
        this.label48.setText(bundle.getString("Quadro08Panel.label48.text"));
        this.label48.setBorder(new EtchedBorder());
        this.label48.setHorizontalAlignment(0);
        this.panel13.add((Component)this.label48, cc.xy(3, 3));
        this.label49.setText(bundle.getString("Quadro08Panel.label49.text"));
        this.panel13.add((Component)this.label49, cc.xy(5, 3));
        this.this2.add((Component)this.panel13, cc.xy(2, 19));
        this.add((Component)this.this2, "Center");
    }

}

