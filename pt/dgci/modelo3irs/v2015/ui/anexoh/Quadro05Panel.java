/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoHq05T5Scroll;
    protected JTable anexoHq05T5;
    protected JPanel panel3;
    protected JLabel anexoHq05C1_label;
    protected JLabel anexoHq05C1_base_label;
    protected JMoneyTextField anexoHq05C1;

    public Quadro05Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoHq05C1_label() {
        return this.anexoHq05C1_label;
    }

    public JMoneyTextField getAnexoHq05C1() {
        return this.anexoHq05C1;
    }

    protected void addLineAnexoHq05T5_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoHq05T5_LinhaActionPerformed(ActionEvent e) {
    }

    protected void button1ActionPerformed(ActionEvent e) {
    }

    public JButton getButton1() {
        return this.button1;
    }

    public JButton getButton2() {
        return this.button2;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JScrollPane getAnexoHq05T5Scroll() {
        return this.anexoHq05T5Scroll;
    }

    public JTable getAnexoHq05T5() {
        return this.anexoHq05T5;
    }

    public JLabel getAnexoHq05C1_base_label() {
        return this.anexoHq05C1_base_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoH");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoHq05T5Scroll = new JScrollPane();
        this.anexoHq05T5 = new JTable();
        this.panel3 = new JPanel();
        this.anexoHq05C1_label = new JLabel();
        this.anexoHq05C1_base_label = new JLabel();
        this.anexoHq05C1 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 111dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro05Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.addLineAnexoHq05T5_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro05Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.removeLineAnexoHq05T5_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel2.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoHq05T5Scroll.setViewportView(this.anexoHq05T5);
        this.panel2.add((Component)this.anexoHq05T5Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, default, $lcgap, 80dlu, $lcgap, default:grow", "$ugap, 3*($lgap, default)"));
        this.anexoHq05C1_label.setText(bundle.getString("Quadro05Panel.anexoHq05C1_label.text"));
        this.anexoHq05C1_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoHq05C1_label, cc.xywh(3, 3, 4, 1));
        this.anexoHq05C1_base_label.setText(bundle.getString("Quadro05Panel.anexoHq05C1_base_label.text"));
        this.panel3.add((Component)this.anexoHq05C1_base_label, cc.xy(3, 5));
        this.anexoHq05C1.setEnabled(false);
        this.anexoHq05C1.setEditable(false);
        this.anexoHq05C1.setColumns(15);
        this.panel3.add((Component)this.anexoHq05C1, cc.xy(5, 5));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.add((Component)this.this2, "Center");
    }

}

