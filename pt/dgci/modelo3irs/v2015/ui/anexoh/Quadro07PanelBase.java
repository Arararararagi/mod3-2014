/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro07Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro07PanelBase
extends Quadro07Panel
implements IBindablePanel<Quadro07> {
    private static final long serialVersionUID = 1;
    protected Quadro07 model;

    @Override
    public abstract void setModel(Quadro07 var1, boolean var2);

    @Override
    public Quadro07 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoHq07T7_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoHq07T7().add(new AnexoHq07T7_Linha());
        }
    }

    @Override
    protected void removeLineAnexoHq07T7_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoHq07T7.getSelectedRow() != -1 ? this.anexoHq07T7.getSelectedRow() : this.anexoHq07T7.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoHq07T7().remove(selectedRow);
            }
        }
    }

    public Quadro07PanelBase(Quadro07 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

