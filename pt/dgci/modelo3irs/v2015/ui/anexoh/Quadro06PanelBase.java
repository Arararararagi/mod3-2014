/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro06Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro06PanelBase
extends Quadro06Panel
implements IBindablePanel<Quadro06> {
    private static final long serialVersionUID = 1;
    protected Quadro06 model;

    @Override
    public abstract void setModel(Quadro06 var1, boolean var2);

    @Override
    public Quadro06 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoHq06T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoHq06T1().add(new AnexoHq06T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoHq06T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoHq06T1.getSelectedRow() != -1 ? this.anexoHq06T1.getSelectedRow() : this.anexoHq06T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoHq06T1().remove(selectedRow);
            }
        }
    }

    public Quadro06PanelBase(Quadro06 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

