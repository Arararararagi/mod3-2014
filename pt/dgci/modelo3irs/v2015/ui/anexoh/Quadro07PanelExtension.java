/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro07Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro07PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro07PanelExtension
extends Quadro07PanelBase
implements IBindablePanel<Quadro07>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q07_T7 = 100;

    public Quadro07PanelExtension(Quadro07 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro07 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro07Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoHq07T7().getTableHeader().getColumnModel();
        ColumnGroup g_EntidadeGestora = new ColumnGroup("Entidade Gestora / Donat\u00e1ria / Senhorio / Locador");
        g_EntidadeGestora.add(cm.getColumn(4));
        g_EntidadeGestora.add(cm.getColumn(5));
        g_EntidadeGestora.add(cm.getColumn(6));
        if (this.getAnexoHq07T7().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoHq07T7().getTableHeader()).addColumnGroup(g_EntidadeGestora);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_EntidadeGestora);
        this.getAnexoHq07T7().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoHq07T7_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoHq07T7() != null && this.model.getAnexoHq07T7().size() >= 100) {
            return;
        }
        super.addLineAnexoHq07T7_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoHModel anexoH = (AnexoHModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoHModel.class);
            if (anexoH != null) {
                EventList<AnexoHq07T7_Linha> anexoHq07T7Rows = anexoH.getQuadro07().getAnexoHq07T7();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInHq07T7(anexoHq07T7Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInHq07T7(anexoHq07T7Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexHq07T7Elements(anexoHq07T7Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInHq07T7(anexoHq07T7Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInHq07T7(EventList<AnexoHq07T7_Linha> anexoHq07T7Rows, String labelPrefix) {
        if (anexoHq07T7Rows != null) {
            for (AnexoHq07T7_Linha anexoHq07T7_Linha : anexoHq07T7Rows) {
                if (anexoHq07T7_Linha == null || anexoHq07T7_Linha.getTitular() == null || !anexoHq07T7_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoHq07T7_Linha.setTitular(null);
            }
        }
    }

    private void reindexHq07T7Elements(EventList<AnexoHq07T7_Linha> anexoHq07T7Rows, String labelPrefix, int index) {
        if (anexoHq07T7Rows != null) {
            for (AnexoHq07T7_Linha anexoHq07T7_Linha : anexoHq07T7Rows) {
                int currIndex;
                if (anexoHq07T7_Linha == null || anexoHq07T7_Linha.getTitular() == null || !anexoHq07T7_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoHq07T7_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoHq07T7_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

