/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro05Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro05PanelBase
extends Quadro05Panel
implements IBindablePanel<Quadro05> {
    private static final long serialVersionUID = 1;
    protected Quadro05 model;

    @Override
    public abstract void setModel(Quadro05 var1, boolean var2);

    @Override
    public Quadro05 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoHq05T5_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoHq05T5().add(new AnexoHq05T5_Linha());
        }
    }

    @Override
    protected void removeLineAnexoHq05T5_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoHq05T5.getSelectedRow() != -1 ? this.anexoHq05T5.getSelectedRow() : this.anexoHq05T5.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoHq05T5().remove(selectedRow);
            }
        }
    }

    public Quadro05PanelBase(Quadro05 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

