/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoh;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoHq07T7Scroll;
    protected JTable anexoHq07T7;
    protected JPanel panel3;
    protected JLabel anexoHq07C1_label;
    protected JLabel anexoHq07C1_base_label;
    protected JMoneyTextField anexoHq07C1;

    public Quadro07Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JTable getAnexoHq07T7() {
        return this.anexoHq07T7;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoHq07C1_label() {
        return this.anexoHq07C1_label;
    }

    public JLabel getAnexoHq07C1_base_label() {
        return this.anexoHq07C1_base_label;
    }

    public JMoneyTextField getAnexoHq07C1() {
        return this.anexoHq07C1;
    }

    public JScrollPane getAnexoHq07T7Scroll() {
        return this.anexoHq07T7Scroll;
    }

    protected void addLineAnexoHq07T7_LinhaActionPerformed(ActionEvent e) {
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    protected void removeLineAnexoHq07T7_LinhaActionPerformed(ActionEvent e) {
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoH");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoHq07T7Scroll = new JScrollPane();
        this.anexoHq07T7 = new JTable();
        this.panel3 = new JPanel();
        this.anexoHq07C1_label = new JLabel();
        this.anexoHq07C1_base_label = new JLabel();
        this.anexoHq07C1 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 4*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 500dlu, $lcgap, default:grow", "$rgap, default, $lgap, 175dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro07Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.addLineAnexoHq07T7_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro07Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.removeLineAnexoHq07T7_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel2.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoHq07T7Scroll.setViewportView(this.anexoHq07T7);
        this.panel2.add((Component)this.anexoHq07T7Scroll, cc.xywh(3, 4, 5, 2));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, default, $lcgap, 80dlu, $lcgap, default:grow", "$ugap, 3*($lgap, default)"));
        this.anexoHq07C1_label.setText(bundle.getString("Quadro07Panel.anexoHq07C1_label.text"));
        this.anexoHq07C1_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoHq07C1_label, cc.xywh(5, 3, 2, 1));
        this.anexoHq07C1_base_label.setText(bundle.getString("Quadro07Panel.anexoHq07C1_base_label.text"));
        this.panel3.add((Component)this.anexoHq07C1_base_label, cc.xy(3, 5));
        this.anexoHq07C1.setEditable(false);
        this.anexoHq07C1.setColumns(15);
        this.panel3.add((Component)this.anexoHq07C1, cc.xy(5, 5));
        this.this2.add((Component)this.panel3, cc.xy(2, 6));
        this.add((Component)this.this2, "Center");
    }

}

