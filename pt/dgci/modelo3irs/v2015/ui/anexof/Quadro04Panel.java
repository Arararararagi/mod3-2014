/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexof;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoFq04T1Scroll;
    protected JTable anexoFq04T1;
    protected JPanel panel4;
    protected JLabel anexoFq04C1_label;
    protected JLabel anexoFq04C2_label;
    protected JLabel anexoFq04C3_label;
    protected JLabel anexoFq04C1_base_label\u00a3anexoFq04C2_base_label\u00a3anexoFq04C3_base_label;
    protected JMoneyTextField anexoFq04C1;
    protected JMoneyTextField anexoFq04C2;
    protected JMoneyTextField anexoFq04C3;

    public Quadro04Panel() {
        this.initComponents();
    }

    protected void addLineAnexoFq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoFq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void addLineAnexoEq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoEq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoFq04T1Scroll() {
        return this.anexoFq04T1Scroll;
    }

    public JTable getAnexoFq04T1() {
        return this.anexoFq04T1;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoFq04C1_label() {
        return this.anexoFq04C1_label;
    }

    public JLabel getAnexoFq04C2_label() {
        return this.anexoFq04C2_label;
    }

    public JLabel getAnexoFq04C1_base_label\u00a3anexoFq04C2_base_label\u00a3anexoFq04C3_base_label() {
        return this.anexoFq04C1_base_label\u00a3anexoFq04C2_base_label\u00a3anexoFq04C3_base_label;
    }

    public JMoneyTextField getAnexoFq04C1() {
        return this.anexoFq04C1;
    }

    public JMoneyTextField getAnexoFq04C2() {
        return this.anexoFq04C2;
    }

    public JLabel getAnexoFq04C3_label() {
        return this.anexoFq04C3_label;
    }

    public JMoneyTextField getAnexoFq04C3() {
        return this.anexoFq04C3;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoF");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoFq04T1Scroll = new JScrollPane();
        this.anexoFq04T1 = new JTable();
        this.panel4 = new JPanel();
        this.anexoFq04C1_label = new JLabel();
        this.anexoFq04C2_label = new JLabel();
        this.anexoFq04C3_label = new JLabel();
        this.anexoFq04C1_base_label\u00a3anexoFq04C2_base_label\u00a3anexoFq04C3_base_label = new JLabel();
        this.anexoFq04C1 = new JMoneyTextField();
        this.anexoFq04C2 = new JMoneyTextField();
        this.anexoFq04C3 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 4*(default, $lgap), default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 630dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:261dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoFq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoFq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoFq04T1Scroll.setViewportView(this.anexoFq04T1);
        this.panel3.add((Component)this.anexoFq04T1Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, default, 3*($lcgap, 80dlu), $lcgap, default:grow", "2*(default, $lgap), default"));
        this.anexoFq04C1_label.setText(bundle.getString("Quadro04Panel.anexoFq04C1_label.text"));
        this.anexoFq04C1_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoFq04C1_label, cc.xywh(5, 1, 2, 1));
        this.anexoFq04C2_label.setText(bundle.getString("Quadro04Panel.anexoFq04C2_label.text"));
        this.anexoFq04C2_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoFq04C2_label, cc.xy(7, 1));
        this.anexoFq04C3_label.setText(bundle.getString("Quadro04Panel.anexoFq04C3_label.text"));
        this.anexoFq04C3_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoFq04C3_label, cc.xy(9, 1));
        this.anexoFq04C1_base_label\u00a3anexoFq04C2_base_label\u00a3anexoFq04C3_base_label.setText(bundle.getString("Quadro04Panel.anexoFq04C1_base_label\u00a3anexoFq04C2_base_label\u00a3anexoFq04C3_base_label.text"));
        this.panel4.add((Component)this.anexoFq04C1_base_label\u00a3anexoFq04C2_base_label\u00a3anexoFq04C3_base_label, cc.xy(3, 3));
        this.anexoFq04C1.setEditable(false);
        this.anexoFq04C1.setColumns(15);
        this.panel4.add((Component)this.anexoFq04C1, cc.xy(5, 3));
        this.anexoFq04C2.setEditable(false);
        this.anexoFq04C2.setColumns(15);
        this.panel4.add((Component)this.anexoFq04C2, cc.xy(7, 3));
        this.anexoFq04C3.setEditable(false);
        this.anexoFq04C3.setColumns(15);
        this.panel4.add((Component)this.anexoFq04C3, cc.xy(9, 3));
        this.this2.add((Component)this.panel4, cc.xy(2, 6));
        this.add((Component)this.this2, "Center");
    }

}

