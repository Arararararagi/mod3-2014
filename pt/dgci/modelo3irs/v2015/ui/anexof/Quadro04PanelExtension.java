/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexof;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexof.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro04PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q04_T1 = 150;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoFq04T1().getTableHeader().getColumnModel();
        ColumnGroup g_IdentificacaoMatricial = new ColumnGroup("Identifica\u00e7\u00e3o Matricial dos Pr\u00e9dios");
        g_IdentificacaoMatricial.add(cm.getColumn(2));
        g_IdentificacaoMatricial.add(cm.getColumn(3));
        g_IdentificacaoMatricial.add(cm.getColumn(4));
        g_IdentificacaoMatricial.add(cm.getColumn(5));
        if (this.getAnexoFq04T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoFq04T1().getTableHeader()).addColumnGroup(g_IdentificacaoMatricial);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_IdentificacaoMatricial);
        this.getAnexoFq04T1().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoFq04T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoFq04T1() != null && this.model.getAnexoFq04T1().size() >= 150) {
            return;
        }
        super.addLineAnexoFq04T1_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoFModel anexoF = (AnexoFModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoFModel.class);
            if (anexoF != null) {
                EventList<AnexoFq04T1_Linha> anexoFq04T1Rows = anexoF.getQuadro04().getAnexoFq04T1();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInFq04T1(anexoFq04T1Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInFq04T1(anexoFq04T1Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexFq04T1Elements(anexoFq04T1Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInFq04T1(anexoFq04T1Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInFq04T1(EventList<AnexoFq04T1_Linha> anexoFq04T1Rows, String labelPrefix) {
        if (anexoFq04T1Rows != null) {
            for (AnexoFq04T1_Linha anexoFq04T1_Linha : anexoFq04T1Rows) {
                if (anexoFq04T1_Linha == null || anexoFq04T1_Linha.getTitular() == null || !anexoFq04T1_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoFq04T1_Linha.setTitular(null);
            }
        }
    }

    private void reindexFq04T1Elements(EventList<AnexoFq04T1_Linha> anexoFq04T1Rows, String labelPrefix, int index) {
        if (anexoFq04T1Rows != null) {
            for (AnexoFq04T1_Linha anexoFq04T1_Linha : anexoFq04T1Rows) {
                int currIndex;
                if (anexoFq04T1_Linha == null || anexoFq04T1_Linha.getTitular() == null || !anexoFq04T1_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoFq04T1_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoFq04T1_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

