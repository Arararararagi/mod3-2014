/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexof;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JLabel label2;
    protected JPanel panel7;
    protected JLabel anexoFq05C1_base_label;
    protected JPanel panel5;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoFq05T1Scroll;
    protected JTable anexoFq05T1;
    protected JPanel panel9;
    protected JLabel label1;
    protected JLabel label3;
    protected JLabel label5;
    protected JPanel panel10;
    protected JLabel label4;
    protected JLabel anexoFq05B1OPSim_base_label;
    protected JLabelTextFieldNumbering label22;
    protected JRadioButton anexoFq05B1OPSim;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton anexoFq05B1OPNao;
    protected JLabel anexoFq05B2OPSim_base_label;
    protected JLabelTextFieldNumbering label24;
    protected JRadioButton anexoFq05B2OPSim;
    protected JLabelTextFieldNumbering label25;
    protected JRadioButton anexoFq05B2OPNao;
    protected JLabel label6;
    protected JLabel anexoFq05B3OPSim_base_label;
    protected JLabelTextFieldNumbering label26;
    protected JRadioButton anexoFq05B3OPSim;
    protected JLabelTextFieldNumbering label27;
    protected JRadioButton anexoFq05B3OPNao;

    public Quadro05Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoFq05C1_base_label() {
        return this.anexoFq05C1_base_label;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabel getAnexoFq05B1OPSim_base_label() {
        return this.anexoFq05B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JRadioButton getAnexoFq05B1OPSim() {
        return this.anexoFq05B1OPSim;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getAnexoFq05B1OPNao() {
        return this.anexoFq05B1OPNao;
    }

    protected void addLineAnexoFq05T1_linhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoHq08T814_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoFq05T1Scroll() {
        return this.anexoFq05T1Scroll;
    }

    public JTable getAnexoFq05T1() {
        return this.anexoFq05T1;
    }

    protected void addLineAnexoFq05T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoFq05T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabel getLabel5() {
        return this.label5;
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabelTextFieldNumbering getLabel24() {
        return this.label24;
    }

    public JRadioButton getAnexoFq05B2OPSim() {
        return this.anexoFq05B2OPSim;
    }

    public JLabelTextFieldNumbering getLabel25() {
        return this.label25;
    }

    public JRadioButton getAnexoFq05B2OPNao() {
        return this.anexoFq05B2OPNao;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    public JLabel getAnexoFq05B2OPSim_base_label() {
        return this.anexoFq05B2OPSim_base_label;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getAnexoFq05B3OPSim_base_label() {
        return this.anexoFq05B3OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel26() {
        return this.label26;
    }

    public JRadioButton getAnexoFq05B3OPSim() {
        return this.anexoFq05B3OPSim;
    }

    public JLabelTextFieldNumbering getLabel27() {
        return this.label27;
    }

    public JRadioButton getAnexoFq05B3OPNao() {
        return this.anexoFq05B3OPNao;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoF");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.label2 = new JLabel();
        this.panel7 = new JPanel();
        this.anexoFq05C1_base_label = new JLabel();
        this.panel5 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoFq05T1Scroll = new JScrollPane();
        this.anexoFq05T1 = new JTable();
        this.panel9 = new JPanel();
        this.label1 = new JLabel();
        this.label3 = new JLabel();
        this.label5 = new JLabel();
        this.panel10 = new JPanel();
        this.label4 = new JLabel();
        this.anexoFq05B1OPSim_base_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoFq05B1OPSim = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.anexoFq05B1OPNao = new JRadioButton();
        this.anexoFq05B2OPSim_base_label = new JLabel();
        this.label24 = new JLabelTextFieldNumbering();
        this.anexoFq05B2OPSim = new JRadioButton();
        this.label25 = new JLabelTextFieldNumbering();
        this.anexoFq05B2OPNao = new JRadioButton();
        this.label6 = new JLabel();
        this.anexoFq05B3OPSim_base_label = new JLabel();
        this.label26 = new JLabelTextFieldNumbering();
        this.anexoFq05B3OPSim = new JRadioButton();
        this.label27 = new JLabelTextFieldNumbering();
        this.anexoFq05B3OPNao = new JRadioButton();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$lcgap, default, $rgap, default:grow", "7*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro05Panel.label2.text"));
        this.label2.setBorder(new EtchedBorder());
        this.label2.setHorizontalAlignment(0);
        this.this2.add((Component)this.label2, cc.xy(2, 3));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("$rgap, default:grow, $rgap, 13dlu, 0px, $lcgap, default, $lcgap, default:grow, 0px, $ugap, 0px", "$rgap, 0px, default, $lgap, default"));
        this.anexoFq05C1_base_label.setText(bundle.getString("Quadro05Panel.anexoFq05C1_base_label.text"));
        this.panel7.add((Component)this.anexoFq05C1_base_label, cc.xywh(2, 3, 8, 1));
        this.this2.add((Component)this.panel7, cc.xy(4, 5));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("default:grow, 3*($lcgap, default), $lcgap, default:grow, $rgap", "2*(default, $lgap), 165dlu, 2*($lgap, default)"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro05Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.addLineAnexoFq05T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro05Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.removeLineAnexoFq05T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel5.add((Component)this.toolBar1, cc.xy(3, 3));
        this.anexoFq05T1Scroll.setViewportView(this.anexoFq05T1);
        this.panel5.add((Component)this.anexoFq05T1Scroll, cc.xywh(3, 5, 5, 1));
        this.this2.add((Component)this.panel5, cc.xy(4, 7));
        this.panel9.setLayout(new FormLayout("20dlu, $lcgap, default, $lcgap, 25dlu, $lcgap, 13dlu, 0px, default, $rgap, default, $lcgap, 13dlu, 0px, default, $lcgap, default:grow, $ugap", "$ugap, 2*($lgap, default)"));
        this.this2.add((Component)this.panel9, cc.xy(4, 9));
        this.label1.setText(bundle.getString("Quadro05Panel.label1.text"));
        this.label1.setBorder(new EtchedBorder());
        this.label1.setHorizontalAlignment(0);
        this.label1.setLabelFor(this.anexoFq05T1);
        this.this2.add((Component)this.label1, cc.xywh(3, 3, 2, 1));
        this.label3.setText(bundle.getString("Quadro05Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.this2.add((Component)this.label3, cc.xy(2, 11));
        this.label5.setText(bundle.getString("Quadro05Panel.label5.text"));
        this.label5.setBorder(new EtchedBorder());
        this.label5.setHorizontalAlignment(0);
        this.this2.add((Component)this.label5, cc.xywh(3, 11, 2, 1));
        this.panel10.setLayout(new FormLayout("135dlu, $lcgap, default, $lcgap, 25dlu, $lcgap, 13dlu, 0px, default, $rgap, default, $lcgap, 13dlu, 0px, default, $lcgap, default:grow, $ugap", "5*(default, $lgap), default"));
        this.label4.setText(bundle.getString("Quadro05Panel.label4.text"));
        this.panel10.add((Component)this.label4, cc.xy(1, 1));
        this.anexoFq05B1OPSim_base_label.setText(bundle.getString("Quadro05Panel.anexoFq05B1OPSim_base_label.text"));
        this.anexoFq05B1OPSim_base_label.setHorizontalAlignment(2);
        this.panel10.add((Component)this.anexoFq05B1OPSim_base_label, cc.xy(3, 3));
        this.label22.setText(bundle.getString("Quadro05Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label22, cc.xy(7, 3));
        this.anexoFq05B1OPSim.setText(bundle.getString("Quadro05Panel.anexoFq05B1OPSim.text"));
        this.panel10.add((Component)this.anexoFq05B1OPSim, cc.xy(9, 3));
        this.label23.setText(bundle.getString("Quadro05Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label23, cc.xy(13, 3));
        this.anexoFq05B1OPNao.setText(bundle.getString("Quadro05Panel.anexoFq05B1OPNao.text"));
        this.panel10.add((Component)this.anexoFq05B1OPNao, cc.xy(15, 3));
        this.anexoFq05B2OPSim_base_label.setText(bundle.getString("Quadro05Panel.anexoFq05B2OPSim_base_label.text"));
        this.anexoFq05B2OPSim_base_label.setHorizontalAlignment(4);
        this.panel10.add((Component)this.anexoFq05B2OPSim_base_label, cc.xy(3, 5));
        this.label24.setText(bundle.getString("Quadro05Panel.label24.text"));
        this.label24.setBorder(new EtchedBorder());
        this.label24.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label24, cc.xy(7, 5));
        this.anexoFq05B2OPSim.setText(bundle.getString("Quadro05Panel.anexoFq05B2OPSim.text"));
        this.panel10.add((Component)this.anexoFq05B2OPSim, cc.xy(9, 5));
        this.label25.setText(bundle.getString("Quadro05Panel.label25.text"));
        this.label25.setBorder(new EtchedBorder());
        this.label25.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label25, cc.xy(13, 5));
        this.anexoFq05B2OPNao.setText(bundle.getString("Quadro05Panel.anexoFq05B2OPNao.text"));
        this.panel10.add((Component)this.anexoFq05B2OPNao, cc.xy(15, 5));
        this.label6.setText(bundle.getString("Quadro05Panel.label6.text"));
        this.panel10.add((Component)this.label6, cc.xy(1, 7));
        this.anexoFq05B3OPSim_base_label.setText(bundle.getString("Quadro05Panel.anexoFq05B3OPSim_base_label.text"));
        this.panel10.add((Component)this.anexoFq05B3OPSim_base_label, cc.xy(3, 9));
        this.label26.setText(bundle.getString("Quadro05Panel.label26.text"));
        this.label26.setBorder(new EtchedBorder());
        this.label26.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label26, cc.xy(7, 9));
        this.anexoFq05B3OPSim.setText(bundle.getString("Quadro05Panel.anexoFq05B3OPSim.text"));
        this.panel10.add((Component)this.anexoFq05B3OPSim, cc.xy(9, 9));
        this.label27.setText(bundle.getString("Quadro05Panel.label27.text"));
        this.label27.setBorder(new EtchedBorder());
        this.label27.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label27, cc.xy(13, 9));
        this.anexoFq05B3OPNao.setText(bundle.getString("Quadro05Panel.anexoFq05B3OPNao.text"));
        this.panel10.add((Component)this.anexoFq05B3OPNao, cc.xy(15, 9));
        this.this2.add((Component)this.panel10, cc.xy(4, 13));
        this.add((Component)this.this2, "Center");
    }

}

