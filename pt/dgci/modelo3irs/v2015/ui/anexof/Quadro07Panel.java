/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexof;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel6;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoFq07T1Scroll;
    protected JTable anexoFq07T1;

    public Quadro07Panel() {
        this.initComponents();
    }

    protected void addLineAnexoFq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoFq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoFq07T1Scroll() {
        return this.anexoFq07T1Scroll;
    }

    public JTable getAnexoFq07T1() {
        return this.anexoFq07T1;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoF");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel6 = new JPanel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoFq07T1Scroll = new JScrollPane();
        this.anexoFq07T1 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 2*(default, $lgap), default"));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 250dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button3.setText(bundle.getString("Quadro07Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.addLineAnexoFq07T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro07Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.removeLineAnexoFq07T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel6.add((Component)this.toolBar2, cc.xy(3, 2));
        this.anexoFq07T1Scroll.setViewportView(this.anexoFq07T1);
        this.panel6.add((Component)this.anexoFq07T1Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel6, cc.xy(2, 4));
        this.add((Component)this.this2, "Center");
    }

}

