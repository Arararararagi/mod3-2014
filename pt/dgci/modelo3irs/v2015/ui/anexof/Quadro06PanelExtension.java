/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexof;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import pt.dgci.modelo3irs.v2015.binding.anexof.Quadro06Bindings;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro06PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro06PanelExtension
extends Quadro06PanelBase
implements IBindablePanel<Quadro06>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q06_T1 = 150;

    public Quadro06PanelExtension(Quadro06 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro06 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro06Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoFq06T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoFq06T1() != null && this.model.getAnexoFq06T1().size() >= 150) {
            return;
        }
        super.addLineAnexoFq06T1_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoFModel anexoF = (AnexoFModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoFModel.class);
            if (anexoF != null) {
                EventList<AnexoFq06T1_Linha> anexoFq06T1Rows = anexoF.getQuadro06().getAnexoFq06T1();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInFq06T1(anexoFq06T1Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInFq06T1(anexoFq06T1Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexFq06T1Elements(anexoFq06T1Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInFq06T1(anexoFq06T1Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInFq06T1(EventList<AnexoFq06T1_Linha> anexoFq06T1Rows, String labelPrefix) {
        if (anexoFq06T1Rows != null) {
            for (AnexoFq06T1_Linha anexoFq06T1_Linha : anexoFq06T1Rows) {
                if (anexoFq06T1_Linha == null || anexoFq06T1_Linha.getTitular() == null || !anexoFq06T1_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoFq06T1_Linha.setTitular(null);
            }
        }
    }

    private void reindexFq06T1Elements(EventList<AnexoFq06T1_Linha> anexoFq06T1Rows, String labelPrefix, int index) {
        if (anexoFq06T1Rows != null) {
            for (AnexoFq06T1_Linha anexoFq06T1_Linha : anexoFq06T1Rows) {
                int currIndex;
                if (anexoFq06T1_Linha == null || anexoFq06T1_Linha.getTitular() == null || !anexoFq06T1_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoFq06T1_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoFq06T1_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

