/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoa;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import pt.dgci.modelo3irs.v2015.binding.anexoa.Quadro05Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoa.Quadro05PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro05PanelExtension
extends Quadro05PanelBase
implements IBindablePanel<Quadro05>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q05_T5 = 16;

    public Quadro05PanelExtension(Quadro05 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro05 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro05Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoAq05T5_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoAq05T5() != null && this.model.getAnexoAq05T5().size() >= 16) {
            return;
        }
        super.addLineAnexoAq05T5_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoAModel anexoA = (AnexoAModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoAModel.class);
            if (anexoA != null) {
                EventList<AnexoAq05T5_Linha> anexoAq05T5Rows = anexoA.getQuadro05().getAnexoAq05T5();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInAq05T5(anexoAq05T5Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInAq05T5(anexoAq05T5Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexAq05T5Elements(anexoAq05T5Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInAq05T5(anexoAq05T5Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInAq05T5(EventList<AnexoAq05T5_Linha> anexoAq05T5Rows, String labelPrefix) {
        if (anexoAq05T5Rows != null) {
            for (AnexoAq05T5_Linha anexoAq05T5_Linha : anexoAq05T5Rows) {
                if (anexoAq05T5_Linha == null || anexoAq05T5_Linha.getTitular() == null || !anexoAq05T5_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoAq05T5_Linha.setTitular(null);
            }
        }
    }

    private void reindexAq05T5Elements(EventList<AnexoAq05T5_Linha> anexoAq05T5Rows, String labelPrefix, int index) {
        if (anexoAq05T5Rows != null) {
            for (AnexoAq05T5_Linha anexoAq05T5_Linha : anexoAq05T5Rows) {
                int currIndex;
                if (anexoAq05T5_Linha == null || anexoAq05T5_Linha.getTitular() == null || !anexoAq05T5_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoAq05T5_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoAq05T5_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

