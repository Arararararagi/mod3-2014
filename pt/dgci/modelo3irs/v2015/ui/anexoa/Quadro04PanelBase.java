/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoa;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoa.Quadro04Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro04PanelBase
extends Quadro04Panel
implements IBindablePanel<Quadro04> {
    private static final long serialVersionUID = 1;
    protected Quadro04 model;

    @Override
    public abstract void setModel(Quadro04 var1, boolean var2);

    @Override
    public Quadro04 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoAq04T4A_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoAq04T4A().add(new AnexoAq04T4A_Linha());
        }
    }

    @Override
    protected void removeLineAnexoAq04T4A_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoAq04T4A.getSelectedRow() != -1 ? this.anexoAq04T4A.getSelectedRow() : this.anexoAq04T4A.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoAq04T4A().remove(selectedRow);
            }
        }
    }

    @Override
    protected void addLineAnexoAq04T4B_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoAq04T4B().add(new AnexoAq04T4B_Linha());
        }
    }

    @Override
    protected void removeLineAnexoAq04T4B_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoAq04T4B.getSelectedRow() != -1 ? this.anexoAq04T4B.getSelectedRow() : this.anexoAq04T4B.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoAq04T4B().remove(selectedRow);
            }
        }
    }

    @Override
    protected void addLineAnexoAq04T4Ba_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoAq04T4Ba().add(new AnexoAq04T4Ba_Linha());
        }
    }

    @Override
    protected void removeLineAnexoAq04T4Ba_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoAq04T4Ba.getSelectedRow() != -1 ? this.anexoAq04T4Ba.getSelectedRow() : this.anexoAq04T4Ba.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoAq04T4Ba().remove(selectedRow);
            }
        }
    }

    public Quadro04PanelBase(Quadro04 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

