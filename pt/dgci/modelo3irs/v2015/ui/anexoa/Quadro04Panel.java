/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoa;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel5;
    protected JLabel label13;
    protected JLabel label3;
    protected JPanel panel2;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoAq04T4AScroll;
    protected JTable anexoAq04T4A;
    protected JPanel panel4;
    protected JLabel anexoAq04C1_label;
    protected JLabel anexoAq04C2_label;
    protected JLabel anexoAq04C3_label;
    protected JLabel anexoAq04C4_label;
    protected JLabel anexoAq04C1_base_label\u00a3anexoAq04C2_base_label\u00a3anexoAq04C3_base_label;
    protected JMoneyTextField anexoAq04C1;
    protected JMoneyTextField anexoAq04C2;
    protected JMoneyTextField anexoAq04C3;
    protected JMoneyTextField anexoAq04C4;
    protected JPanel panel6;
    protected JLabel label14;
    protected JLabel label15;
    protected JPanel panel3;
    protected JToolBar toolBar5;
    protected JAddLineIconableButton button5;
    protected JRemoveLineIconableButton button6;
    protected JScrollPane anexoAq04T4BScroll;
    protected JTable anexoAq04T4B;
    protected JPanel panel8;
    protected JLabel label16;
    protected JLabel label17;
    protected JToolBar toolBar8;
    protected JAddLineIconableButton button7;
    protected JRemoveLineIconableButton button8;
    protected JScrollPane anexoAq04T4BaScroll;
    protected JTable anexoAq04T4Ba;

    public Quadro04Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JScrollPane getAnexoAq04T4BScroll() {
        return this.anexoAq04T4BScroll;
    }

    public JTable getAnexoAq04T4B() {
        return this.anexoAq04T4B;
    }

    public JScrollPane getAnexoAq04T4AScroll() {
        return this.anexoAq04T4AScroll;
    }

    public JTable getAnexoAq04T4A() {
        return this.anexoAq04T4A;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoAq04C1_label() {
        return this.anexoAq04C1_label;
    }

    public JLabel getAnexoAq04C2_label() {
        return this.anexoAq04C2_label;
    }

    public JLabel getAnexoAq04C3_label() {
        return this.anexoAq04C3_label;
    }

    public JLabel getAnexoAq04C1_base_label\u00a3anexoAq04C2_base_label\u00a3anexoAq04C3_base_label() {
        return this.anexoAq04C1_base_label\u00a3anexoAq04C2_base_label\u00a3anexoAq04C3_base_label;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    protected void addLineAnexoAq04T4A_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoAq04T4A_LinhaActionPerformed(ActionEvent e) {
    }

    protected void addLineAnexoAq04T4Aa_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoAq04T4Aa_LinhaActionPerformed(ActionEvent e) {
    }

    public JAddLineIconableButton getButton5() {
        return this.button5;
    }

    public JRemoveLineIconableButton getButton6() {
        return this.button6;
    }

    protected void addLineAnexoAq04T4B_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoAq04T4B_LinhaActionPerformed(ActionEvent e) {
    }

    protected void addLineAnexoAq04T4Ba_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoAq04T4Ba_LinhaActionPerformed(ActionEvent e) {
    }

    public JMoneyTextField getAnexoAq04C1() {
        return this.anexoAq04C1;
    }

    public JMoneyTextField getAnexoAq04C2() {
        return this.anexoAq04C2;
    }

    public JMoneyTextField getAnexoAq04C3() {
        return this.anexoAq04C3;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JToolBar getToolBar5() {
        return this.toolBar5;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    public JLabel getLabel15() {
        return this.label15;
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JLabel getLabel16() {
        return this.label16;
    }

    public JLabel getLabel17() {
        return this.label17;
    }

    public JToolBar getToolBar8() {
        return this.toolBar8;
    }

    public JAddLineIconableButton getButton7() {
        return this.button7;
    }

    public JRemoveLineIconableButton getButton8() {
        return this.button8;
    }

    public JScrollPane getAnexoAq04T4BaScroll() {
        return this.anexoAq04T4BaScroll;
    }

    public JTable getAnexoAq04T4Ba() {
        return this.anexoAq04T4Ba;
    }

    public JLabel getAnexoAq04C4_label() {
        return this.anexoAq04C4_label;
    }

    public JMoneyTextField getAnexoAq04C4() {
        return this.anexoAq04C4;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoA");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel5 = new JPanel();
        this.label13 = new JLabel();
        this.label3 = new JLabel();
        this.panel2 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoAq04T4AScroll = new JScrollPane();
        this.anexoAq04T4A = new JTable();
        this.panel4 = new JPanel();
        this.anexoAq04C1_label = new JLabel();
        this.anexoAq04C2_label = new JLabel();
        this.anexoAq04C3_label = new JLabel();
        this.anexoAq04C4_label = new JLabel();
        this.anexoAq04C1_base_label\u00a3anexoAq04C2_base_label\u00a3anexoAq04C3_base_label = new JLabel();
        this.anexoAq04C1 = new JMoneyTextField();
        this.anexoAq04C2 = new JMoneyTextField();
        this.anexoAq04C3 = new JMoneyTextField();
        this.anexoAq04C4 = new JMoneyTextField();
        this.panel6 = new JPanel();
        this.label14 = new JLabel();
        this.label15 = new JLabel();
        this.panel3 = new JPanel();
        this.toolBar5 = new JToolBar();
        this.button5 = new JAddLineIconableButton();
        this.button6 = new JRemoveLineIconableButton();
        this.anexoAq04T4BScroll = new JScrollPane();
        this.anexoAq04T4B = new JTable();
        this.panel8 = new JPanel();
        this.label16 = new JLabel();
        this.label17 = new JLabel();
        this.toolBar8 = new JToolBar();
        this.button7 = new JAddLineIconableButton();
        this.button8 = new JRemoveLineIconableButton();
        this.anexoAq04T4BaScroll = new JScrollPane();
        this.anexoAq04T4Ba = new JTable();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $lcgap, default", "$lgap, default, $rgap, 6*(default, $lgap), default"));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, 0px, default:grow", "default"));
        this.label13.setText(bundle.getString("Quadro04Panel.label13.text"));
        this.label13.setBorder(new EtchedBorder());
        this.label13.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label13, CC.xy(1, 1));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel5.add((Component)this.label3, CC.xy(3, 1));
        this.this2.add((Component)this.panel5, CC.xy(2, 2));
        this.panel2.setMinimumSize(null);
        this.panel2.setVerifyInputWhenFocusTarget(false);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, 0px, default, 0px, $lcgap, default, $rgap, 500dlu, $lcgap, default:grow", "2*(default, $lgap), 165dlu, 2*($lgap, default)"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoAq04T4A_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoAq04T4A_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel2.add((Component)this.toolBar1, CC.xy(4, 3));
        this.anexoAq04T4AScroll.setViewportView(this.anexoAq04T4A);
        this.panel2.add((Component)this.anexoAq04T4AScroll, CC.xywh(4, 5, 6, 1));
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, default, 4*($lcgap, 80dlu), $rgap", "$ugap, 3*($lgap, default)"));
        this.anexoAq04C1_label.setText(bundle.getString("Quadro04Panel.anexoAq04C1_label.text"));
        this.anexoAq04C1_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoAq04C1_label, CC.xywh(5, 3, 2, 1));
        this.anexoAq04C2_label.setText(bundle.getString("Quadro04Panel.anexoAq04C2_label.text"));
        this.anexoAq04C2_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoAq04C2_label, CC.xy(7, 3));
        this.anexoAq04C3_label.setText(bundle.getString("Quadro04Panel.anexoAq04C3_label.text"));
        this.anexoAq04C3_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoAq04C3_label, CC.xy(9, 3));
        this.anexoAq04C4_label.setText(bundle.getString("Quadro04Panel.anexoAq04C4_label.text"));
        this.anexoAq04C4_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoAq04C4_label, CC.xy(11, 3));
        this.anexoAq04C1_base_label\u00a3anexoAq04C2_base_label\u00a3anexoAq04C3_base_label.setText(bundle.getString("Quadro04Panel.anexoAq04C1_base_label\u00a3anexoAq04C2_base_label\u00a3anexoAq04C3_base_label.text"));
        this.panel4.add((Component)this.anexoAq04C1_base_label\u00a3anexoAq04C2_base_label\u00a3anexoAq04C3_base_label, CC.xy(3, 5));
        this.anexoAq04C1.setEditable(false);
        this.anexoAq04C1.setColumns(15);
        this.panel4.add((Component)this.anexoAq04C1, CC.xy(5, 5));
        this.anexoAq04C2.setEditable(false);
        this.anexoAq04C2.setColumns(15);
        this.panel4.add((Component)this.anexoAq04C2, CC.xy(7, 5));
        this.anexoAq04C3.setEditable(false);
        this.anexoAq04C3.setColumns(15);
        this.panel4.add((Component)this.anexoAq04C3, CC.xy(9, 5));
        this.anexoAq04C4.setEditable(false);
        this.anexoAq04C4.setColumns(15);
        this.panel4.add((Component)this.anexoAq04C4, CC.xy(11, 5));
        this.panel2.add((Component)this.panel4, CC.xywh(2, 7, 8, 1));
        this.this2.add((Component)this.panel2, CC.xy(2, 4));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, 0px, default:grow", "default"));
        this.label14.setText(bundle.getString("Quadro04Panel.label14.text"));
        this.label14.setBorder(new EtchedBorder());
        this.label14.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label14, CC.xy(1, 1));
        this.label15.setText(bundle.getString("Quadro04Panel.label15.text"));
        this.label15.setHorizontalAlignment(0);
        this.label15.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label15, CC.xy(3, 1));
        this.this2.add((Component)this.panel6, CC.xy(2, 10));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, 0px, default, 0px, $lcgap, default, $rgap, 125dlu, $lcgap, default:grow", "2*($lgap, default), $lgap, 165dlu, $lgap, default"));
        this.toolBar5.setFloatable(false);
        this.toolBar5.setRollover(true);
        this.button5.setText(bundle.getString("Quadro04Panel.button5.text"));
        this.button5.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoAq04T4B_LinhaActionPerformed(e);
            }
        });
        this.toolBar5.add(this.button5);
        this.button6.setText(bundle.getString("Quadro04Panel.button6.text"));
        this.button6.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoAq04T4B_LinhaActionPerformed(e);
            }
        });
        this.toolBar5.add(this.button6);
        this.panel3.add((Component)this.toolBar5, CC.xy(4, 4));
        this.anexoAq04T4BScroll.setViewportView(this.anexoAq04T4B);
        this.panel3.add((Component)this.anexoAq04T4BScroll, CC.xywh(4, 6, 6, 1, CC.FILL, CC.FILL));
        this.this2.add((Component)this.panel3, CC.xy(2, 12));
        this.panel8.setMinimumSize(null);
        this.panel8.setPreferredSize(null);
        this.panel8.setLayout(new FormLayout("default:grow, $lcgap, 0px, default, 0px, $lcgap, default, $rgap, 230dlu, $lcgap, default:grow", "7*(default, $lgap), 165dlu"));
        this.label16.setText(bundle.getString("Quadro04Panel.label16.text"));
        this.label16.setHorizontalAlignment(0);
        this.label16.setBorder(new EtchedBorder());
        this.panel8.add((Component)this.label16, CC.xywh(4, 5, 6, 1));
        this.label17.setText(bundle.getString("Quadro04Panel.label17.text"));
        this.panel8.add((Component)this.label17, CC.xywh(4, 9, 6, 1));
        this.toolBar8.setFloatable(false);
        this.toolBar8.setRollover(true);
        this.button7.setText(bundle.getString("Quadro04Panel.button7.text"));
        this.button7.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoAq04T4Ba_LinhaActionPerformed(e);
            }
        });
        this.toolBar8.add(this.button7);
        this.button8.setText(bundle.getString("Quadro04Panel.button8.text"));
        this.button8.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoAq04T4Ba_LinhaActionPerformed(e);
            }
        });
        this.toolBar8.add(this.button8);
        this.panel8.add((Component)this.toolBar8, CC.xy(4, 13));
        this.anexoAq04T4BaScroll.setViewportView(this.anexoAq04T4Ba);
        this.panel8.add((Component)this.anexoAq04T4BaScroll, CC.xywh(4, 15, 6, 1));
        this.this2.add((Component)this.panel8, CC.xy(2, 14));
        this.add((Component)this.this2, "South");
    }

}

