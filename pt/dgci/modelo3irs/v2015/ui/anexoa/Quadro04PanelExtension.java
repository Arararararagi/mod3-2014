/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoa;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoa.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoa.Quadro04PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q04_T4A = 100;
    public static final int MAX_LINES_Q04_T4Aa = 6;
    public static final int MAX_LINES_Q04_T4B = 16;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoAq04T4Ba().getColumnModel();
        ColumnGroup g_EntidadeGestora = new ColumnGroup("Entidade Gestora");
        g_EntidadeGestora.add(cm.getColumn(4));
        g_EntidadeGestora.add(cm.getColumn(5));
        g_EntidadeGestora.add(cm.getColumn(6));
        if (this.getAnexoAq04T4Ba().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoAq04T4Ba().getTableHeader()).addColumnGroup(g_EntidadeGestora);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_EntidadeGestora);
        this.getAnexoAq04T4Ba().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoAq04T4A_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoAq04T4A() != null && this.model.getAnexoAq04T4A().size() >= 100) {
            return;
        }
        super.addLineAnexoAq04T4A_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoAq04T4B_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoAq04T4B() != null && this.model.getAnexoAq04T4B().size() >= 16) {
            return;
        }
        super.addLineAnexoAq04T4B_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoAModel anexoA = (AnexoAModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoAModel.class);
            if (anexoA != null) {
                EventList<AnexoAq04T4A_Linha> anexoAq04T4ARows = anexoA.getQuadro04().getAnexoAq04T4A();
                EventList<AnexoAq04T4B_Linha> anexoAq04T4BRows = anexoA.getQuadro04().getAnexoAq04T4B();
                EventList<AnexoAq04T4Ba_Linha> anexoAq04T4BaRows = anexoA.getQuadro04().getAnexoAq04T4Ba();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInAq04T4A(anexoAq04T4ARows, label);
                        this.removeNifByLabelInAq04T4B(anexoAq04T4BRows, label);
                        this.removeNifByLabelInAq04T4Ba(anexoAq04T4BaRows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInAq04T4A(anexoAq04T4ARows, label + (index + 1));
                        this.removeNifByLabelInAq04T4B(anexoAq04T4BRows, label + (index + 1));
                        this.removeNifByLabelInAq04T4Ba(anexoAq04T4BaRows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexAq04T4AElements(anexoAq04T4ARows, label, index);
                            this.reindexAq04T4BElements(anexoAq04T4BRows, label, index);
                            this.reindexAq04T4BaElements(anexoAq04T4BaRows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInAq04T4A(anexoAq04T4ARows, label);
                    this.removeNifByLabelInAq04T4B(anexoAq04T4BRows, label);
                    this.removeNifByLabelInAq04T4Ba(anexoAq04T4BaRows, label);
                }
            }
        }
    }

    private void removeNifByLabelInAq04T4A(EventList<AnexoAq04T4A_Linha> anexoAq04T4ARows, String labelPrefix) {
        if (anexoAq04T4ARows != null) {
            for (AnexoAq04T4A_Linha anexoAq04T4A_Linha : anexoAq04T4ARows) {
                if (anexoAq04T4A_Linha == null || anexoAq04T4A_Linha.getTitular() == null || !anexoAq04T4A_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoAq04T4A_Linha.setTitular(null);
            }
        }
    }

    private void removeNifByLabelInAq04T4B(EventList<AnexoAq04T4B_Linha> anexoAq04T4BRows, String labelPrefix) {
        if (anexoAq04T4BRows != null) {
            for (AnexoAq04T4B_Linha anexoAq04T4B_Linha : anexoAq04T4BRows) {
                if (anexoAq04T4B_Linha == null || anexoAq04T4B_Linha.getTitular() == null || !anexoAq04T4B_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoAq04T4B_Linha.setTitular(null);
            }
        }
    }

    private void removeNifByLabelInAq04T4Ba(EventList<AnexoAq04T4Ba_Linha> anexoAq04T4BaRows, String labelPrefix) {
        if (anexoAq04T4BaRows != null) {
            for (AnexoAq04T4Ba_Linha anexoAq04T4Ba_Linha : anexoAq04T4BaRows) {
                if (anexoAq04T4Ba_Linha == null || anexoAq04T4Ba_Linha.getTitular() == null || !anexoAq04T4Ba_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoAq04T4Ba_Linha.setTitular(null);
            }
        }
    }

    private void reindexAq04T4AElements(EventList<AnexoAq04T4A_Linha> anexoAq04T4ARows, String labelPrefix, int index) {
        if (anexoAq04T4ARows != null) {
            for (AnexoAq04T4A_Linha anexoAq04T4A_Linha : anexoAq04T4ARows) {
                int currIndex;
                if (anexoAq04T4A_Linha == null || anexoAq04T4A_Linha.getTitular() == null || !anexoAq04T4A_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoAq04T4A_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoAq04T4A_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }

    private void reindexAq04T4BElements(EventList<AnexoAq04T4B_Linha> anexoAq04T4BRows, String labelPrefix, int index) {
        if (anexoAq04T4BRows != null) {
            for (AnexoAq04T4B_Linha anexoAq04T4B_Linha : anexoAq04T4BRows) {
                int currIndex;
                if (anexoAq04T4B_Linha == null || anexoAq04T4B_Linha.getTitular() == null || !anexoAq04T4B_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoAq04T4B_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoAq04T4B_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }

    private void reindexAq04T4BaElements(EventList<AnexoAq04T4Ba_Linha> anexoAq04T4BaRows, String labelPrefix, int index) {
        if (anexoAq04T4BaRows != null) {
            for (AnexoAq04T4Ba_Linha anexoAq04T4Ba_Linha : anexoAq04T4BaRows) {
                int currIndex;
                if (anexoAq04T4Ba_Linha == null || anexoAq04T4Ba_Linha.getTitular() == null || !anexoAq04T4Ba_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoAq04T4Ba_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoAq04T4Ba_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

