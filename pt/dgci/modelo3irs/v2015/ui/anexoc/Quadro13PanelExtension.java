/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro13Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro13PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro13PanelExtension
extends Quadro13PanelBase
implements IBindablePanel<Quadro13> {
    public Quadro13PanelExtension(Quadro13 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro13 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro13Bindings.doBindings(model, this);
        }
    }
}

