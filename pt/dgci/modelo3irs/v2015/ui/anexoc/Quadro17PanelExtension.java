/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro17Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro17PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro17PanelExtension
extends Quadro17PanelBase
implements IBindablePanel<Quadro17> {
    public Quadro17PanelExtension(Quadro17 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro17 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro17Bindings.doBindings(model, this);
        }
    }
}

