/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel anexoCq07C701_label;
    protected JLabelTextFieldNumbering anexoCq07C701_num;
    protected JMoneyTextField anexoCq07C701;

    public Quadro07Panel() {
        this.initComponents();
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabelTextFieldNumbering getAnexoCq07C701_num() {
        return this.anexoCq07C701_num;
    }

    public JMoneyTextField getAnexoCq07C701() {
        return this.anexoCq07C701;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoCq07C701_label() {
        return this.anexoCq07C701_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.anexoCq07C701_label = new JLabel();
        this.anexoCq07C701_num = new JLabelTextFieldNumbering();
        this.anexoCq07C701 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "default, $lgap, default"));
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, 75dlu, $rgap", "$rgap, default, $lgap, default"));
        this.anexoCq07C701_label.setText(bundle.getString("Quadro07Panel.anexoCq07C701_label.text"));
        this.panel4.add((Component)this.anexoCq07C701_label, cc.xy(1, 2));
        this.anexoCq07C701_num.setText(bundle.getString("Quadro07Panel.anexoCq07C701_num.text"));
        this.anexoCq07C701_num.setBorder(new EtchedBorder());
        this.anexoCq07C701_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq07C701_num, cc.xy(3, 2));
        this.panel4.add((Component)this.anexoCq07C701, cc.xy(5, 2));
        this.this2.add((Component)this.panel4, cc.xy(3, 1));
        this.add((Component)this.this2, "Center");
    }
}

