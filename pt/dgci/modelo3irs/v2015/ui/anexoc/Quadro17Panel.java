/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro17Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoCq03C04_label;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField anexoCq17C1701;

    public Quadro17Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoCq03C04_label() {
        return this.anexoCq03C04_label;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getAnexoCq17C1701() {
        return this.anexoCq17C1701;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoCq03C04_label = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoCq17C1701 = new JNIFTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro17Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro17Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("0px, default:grow, $lcgap, default, $ugap, 25dlu, 0px, 43dlu, $lcgap, default:grow", "$rgap, default, $lgap, default"));
        this.anexoCq03C04_label.setText(bundle.getString("Quadro17Panel.anexoCq03C04_label.text"));
        this.panel2.add((Component)this.anexoCq03C04_label, CC.xy(4, 2));
        this.label4.setText(bundle.getString("Quadro17Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(6, 2));
        this.anexoCq17C1701.setColumns(9);
        this.panel2.add((Component)this.anexoCq17C1701, CC.xy(8, 2));
        this.this2.add((Component)this.panel2, CC.xy(3, 1));
        this.add((Component)this.this2, "Center");
    }
}

