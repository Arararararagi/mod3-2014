/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel6;
    protected JLabel anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label;
    protected JLabel anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label;
    protected JLabel anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label;
    protected JLabel anexoCq06C601_base_label\u00a3anexoCq06C606_base_label\u00a3anexoCq06C611_base_label;
    protected JLabelTextFieldNumbering anexoCq06C601_num;
    protected JMoneyTextField anexoCq06C601;
    protected JLabelTextFieldNumbering anexoCq06C606_num;
    protected JMoneyTextField anexoCq06C606;
    protected JLabelTextFieldNumbering anexoCq06C611_num;
    protected JMoneyTextField anexoCq06C611;
    protected JLabel anexoCq06C602_base_label\u00a3anexoCq06C607_base_label\u00a3anexoCq06C612_base_label;
    protected JLabelTextFieldNumbering anexoCq06C602_num;
    protected JMoneyTextField anexoCq06C602;
    protected JLabelTextFieldNumbering anexoCq06C607_num;
    protected JMoneyTextField anexoCq06C607;
    protected JLabelTextFieldNumbering anexoCq06C612_num;
    protected JMoneyTextField anexoCq06C612;
    protected JLabel anexoCq06C603_base_label\u00a3anexoCq06C608_base_label\u00a3anexoCq06C613_base_label;
    protected JLabelTextFieldNumbering anexoCq06C603_num;
    protected JMoneyTextField anexoCq06C603;
    protected JLabelTextFieldNumbering anexoCq06C608_num;
    protected JMoneyTextField anexoCq06C608;
    protected JLabelTextFieldNumbering anexoCq06C613_num;
    protected JMoneyTextField anexoCq06C613;
    protected JLabel anexoCq06C604_base_label\u00a3anexoCq06C609_base_label\u00a3anexoCq06C614_base_label;
    protected JLabelTextFieldNumbering anexoCq06C604_num;
    protected JMoneyTextField anexoCq06C604;
    protected JLabelTextFieldNumbering anexoCq06C609_num;
    protected JMoneyTextField anexoCq06C609;
    protected JLabelTextFieldNumbering anexoCq06C614_num;
    protected JMoneyTextField anexoCq06C614;
    protected JLabel anexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label;
    protected JLabelTextFieldNumbering anexoCq06C605_num;
    protected JMoneyTextField anexoCq06C605;
    protected JLabelTextFieldNumbering anexoCq06C610_num;
    protected JMoneyTextField anexoCq06C610;
    protected JLabelTextFieldNumbering anexoCq06C615_num;
    protected JMoneyTextField anexoCq06C615;
    protected JLabel anexoCq06C616_base_label;
    protected JLabelTextFieldNumbering anexoCq06C616_num;
    protected JMoneyTextField anexoCq06C616;

    public Quadro06Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getAnexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label() {
        return this.anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label;
    }

    public JLabel getAnexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label() {
        return this.anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label;
    }

    public JLabel getAnexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label() {
        return this.anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label;
    }

    public JLabel getAnexoCq06C601_base_label\u00a3anexoCq06C606_base_label\u00a3anexoCq06C611_base_label() {
        return this.anexoCq06C601_base_label\u00a3anexoCq06C606_base_label\u00a3anexoCq06C611_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq06C601_num() {
        return this.anexoCq06C601_num;
    }

    public JMoneyTextField getAnexoCq06C601() {
        return this.anexoCq06C601;
    }

    public JLabelTextFieldNumbering getAnexoCq06C606_num() {
        return this.anexoCq06C606_num;
    }

    public JMoneyTextField getAnexoCq06C606() {
        return this.anexoCq06C606;
    }

    public JLabelTextFieldNumbering getAnexoCq06C611_num() {
        return this.anexoCq06C611_num;
    }

    public JMoneyTextField getAnexoCq06C611() {
        return this.anexoCq06C611;
    }

    public JLabel getAnexoCq06C602_base_label\u00a3anexoCq06C607_base_label\u00a3anexoCq06C612_base_label() {
        return this.anexoCq06C602_base_label\u00a3anexoCq06C607_base_label\u00a3anexoCq06C612_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq06C602_num() {
        return this.anexoCq06C602_num;
    }

    public JMoneyTextField getAnexoCq06C602() {
        return this.anexoCq06C602;
    }

    public JLabelTextFieldNumbering getAnexoCq06C607_num() {
        return this.anexoCq06C607_num;
    }

    public JMoneyTextField getAnexoCq06C607() {
        return this.anexoCq06C607;
    }

    public JLabelTextFieldNumbering getAnexoCq06C612_num() {
        return this.anexoCq06C612_num;
    }

    public JMoneyTextField getAnexoCq06C612() {
        return this.anexoCq06C612;
    }

    public JLabel getAnexoCq06C603_base_label\u00a3anexoCq06C608_base_label\u00a3anexoCq06C613_base_label() {
        return this.anexoCq06C603_base_label\u00a3anexoCq06C608_base_label\u00a3anexoCq06C613_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq06C603_num() {
        return this.anexoCq06C603_num;
    }

    public JMoneyTextField getAnexoCq06C603() {
        return this.anexoCq06C603;
    }

    public JLabelTextFieldNumbering getAnexoCq06C608_num() {
        return this.anexoCq06C608_num;
    }

    public JMoneyTextField getAnexoCq06C608() {
        return this.anexoCq06C608;
    }

    public JLabelTextFieldNumbering getAnexoCq06C613_num() {
        return this.anexoCq06C613_num;
    }

    public JMoneyTextField getAnexoCq06C613() {
        return this.anexoCq06C613;
    }

    public JLabel getAnexoCq06C604_base_label\u00a3anexoCq06C609_base_label\u00a3anexoCq06C614_base_label() {
        return this.anexoCq06C604_base_label\u00a3anexoCq06C609_base_label\u00a3anexoCq06C614_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq06C604_num() {
        return this.anexoCq06C604_num;
    }

    public JMoneyTextField getAnexoCq06C604() {
        return this.anexoCq06C604;
    }

    public JLabelTextFieldNumbering getAnexoCq06C609_num() {
        return this.anexoCq06C609_num;
    }

    public JMoneyTextField getAnexoCq06C609() {
        return this.anexoCq06C609;
    }

    public JLabelTextFieldNumbering getAnexoCq06C614_num() {
        return this.anexoCq06C614_num;
    }

    public JMoneyTextField getAnexoCq06C614() {
        return this.anexoCq06C614;
    }

    public JLabel getAnexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label() {
        return this.anexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq06C605_num() {
        return this.anexoCq06C605_num;
    }

    public JMoneyTextField getAnexoCq06C605() {
        return this.anexoCq06C605;
    }

    public JMoneyTextField getAnexoCq06C610() {
        return this.anexoCq06C610;
    }

    public JMoneyTextField getAnexoCq06C615() {
        return this.anexoCq06C615;
    }

    public JMoneyTextField getAnexoCq06C616() {
        return this.anexoCq06C616;
    }

    public JLabelTextFieldNumbering getAnexoCq06C610_num() {
        return this.anexoCq06C610_num;
    }

    public JLabelTextFieldNumbering getAnexoCq06C615_num() {
        return this.anexoCq06C615_num;
    }

    public JLabelTextFieldNumbering getAnexoCq06C616_num() {
        return this.anexoCq06C616_num;
    }

    public JLabel getAnexoCq06C616_base_label() {
        return this.anexoCq06C616_base_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel6 = new JPanel();
        this.anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label = new JLabel();
        this.anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label = new JLabel();
        this.anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label = new JLabel();
        this.anexoCq06C601_base_label\u00a3anexoCq06C606_base_label\u00a3anexoCq06C611_base_label = new JLabel();
        this.anexoCq06C601_num = new JLabelTextFieldNumbering();
        this.anexoCq06C601 = new JMoneyTextField();
        this.anexoCq06C606_num = new JLabelTextFieldNumbering();
        this.anexoCq06C606 = new JMoneyTextField();
        this.anexoCq06C611_num = new JLabelTextFieldNumbering();
        this.anexoCq06C611 = new JMoneyTextField();
        this.anexoCq06C602_base_label\u00a3anexoCq06C607_base_label\u00a3anexoCq06C612_base_label = new JLabel();
        this.anexoCq06C602_num = new JLabelTextFieldNumbering();
        this.anexoCq06C602 = new JMoneyTextField();
        this.anexoCq06C607_num = new JLabelTextFieldNumbering();
        this.anexoCq06C607 = new JMoneyTextField();
        this.anexoCq06C612_num = new JLabelTextFieldNumbering();
        this.anexoCq06C612 = new JMoneyTextField();
        this.anexoCq06C603_base_label\u00a3anexoCq06C608_base_label\u00a3anexoCq06C613_base_label = new JLabel();
        this.anexoCq06C603_num = new JLabelTextFieldNumbering();
        this.anexoCq06C603 = new JMoneyTextField();
        this.anexoCq06C608_num = new JLabelTextFieldNumbering();
        this.anexoCq06C608 = new JMoneyTextField();
        this.anexoCq06C613_num = new JLabelTextFieldNumbering();
        this.anexoCq06C613 = new JMoneyTextField();
        this.anexoCq06C604_base_label\u00a3anexoCq06C609_base_label\u00a3anexoCq06C614_base_label = new JLabel();
        this.anexoCq06C604_num = new JLabelTextFieldNumbering();
        this.anexoCq06C604 = new JMoneyTextField();
        this.anexoCq06C609_num = new JLabelTextFieldNumbering();
        this.anexoCq06C609 = new JMoneyTextField();
        this.anexoCq06C614_num = new JLabelTextFieldNumbering();
        this.anexoCq06C614 = new JMoneyTextField();
        this.anexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label = new JLabel();
        this.anexoCq06C605_num = new JLabelTextFieldNumbering();
        this.anexoCq06C605 = new JMoneyTextField();
        this.anexoCq06C610_num = new JLabelTextFieldNumbering();
        this.anexoCq06C610 = new JMoneyTextField();
        this.anexoCq06C615_num = new JLabelTextFieldNumbering();
        this.anexoCq06C615 = new JMoneyTextField();
        this.anexoCq06C616_base_label = new JLabel();
        this.anexoCq06C616_num = new JLabelTextFieldNumbering();
        this.anexoCq06C616 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setBorder(LineBorder.createBlackLineBorder());
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "2*(default, $lgap), default"));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("$ugap, 200dlu:grow, $ugap, 25dlu, $rgap, 75dlu, $lcgap, default, $lcgap, 25dlu, $lcgap, 75dlu, $lcgap, default, $lcgap, 25dlu, $lcgap, 75dlu, $rgap", "7*(default, $lgap), default"));
        this.anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label.setText(bundle.getString("Quadro06Panel.anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label.text"));
        this.anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label.setBorder(new EtchedBorder());
        this.anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C601_label\u00a3anexoCq06C602_label\u00a3anexoCq06C603_label\u00a3anexoCq06C604_label\u00a3anexoCq06C605_label, cc.xywh(4, 1, 3, 1));
        this.anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label.setText(bundle.getString("Quadro06Panel.anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label.text"));
        this.anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label.setBorder(new EtchedBorder());
        this.anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C606_label\u00a3anexoCq06C607_label\u00a3anexoCq06C608_label\u00a3anexoCq06C609_label\u00a3anexoCq06C610_label, cc.xywh(10, 1, 3, 1));
        this.anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label.setText(bundle.getString("Quadro06Panel.anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label.text"));
        this.anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label.setBorder(new EtchedBorder());
        this.anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C611_label\u00a3anexoCq06C612_label\u00a3anexoCq06C613_label\u00a3anexoCq06C614_label\u00a3anexoCq06C615_label\u00a3anexoCq06C616_label, cc.xywh(16, 1, 3, 1));
        this.anexoCq06C601_base_label\u00a3anexoCq06C606_base_label\u00a3anexoCq06C611_base_label.setText(bundle.getString("Quadro06Panel.anexoCq06C601_base_label\u00a3anexoCq06C606_base_label\u00a3anexoCq06C611_base_label.text"));
        this.panel6.add((Component)this.anexoCq06C601_base_label\u00a3anexoCq06C606_base_label\u00a3anexoCq06C611_base_label, cc.xy(2, 3));
        this.anexoCq06C601_num.setText(bundle.getString("Quadro06Panel.anexoCq06C601_num.text"));
        this.anexoCq06C601_num.setBorder(new EtchedBorder());
        this.anexoCq06C601_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C601_num, cc.xy(4, 3));
        this.anexoCq06C601.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C601, cc.xy(6, 3));
        this.anexoCq06C606_num.setText(bundle.getString("Quadro06Panel.anexoCq06C606_num.text"));
        this.anexoCq06C606_num.setBorder(new EtchedBorder());
        this.anexoCq06C606_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C606_num, cc.xy(10, 3));
        this.anexoCq06C606.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C606, cc.xy(12, 3));
        this.anexoCq06C611_num.setText(bundle.getString("Quadro06Panel.anexoCq06C611_num.text"));
        this.anexoCq06C611_num.setBorder(new EtchedBorder());
        this.anexoCq06C611_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C611_num, cc.xy(16, 3));
        this.anexoCq06C611.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C611, cc.xy(18, 3));
        this.anexoCq06C602_base_label\u00a3anexoCq06C607_base_label\u00a3anexoCq06C612_base_label.setText(bundle.getString("Quadro06Panel.anexoCq06C602_base_label\u00a3anexoCq06C607_base_label\u00a3anexoCq06C612_base_label.text"));
        this.panel6.add((Component)this.anexoCq06C602_base_label\u00a3anexoCq06C607_base_label\u00a3anexoCq06C612_base_label, cc.xy(2, 5));
        this.anexoCq06C602_num.setText(bundle.getString("Quadro06Panel.anexoCq06C602_num.text"));
        this.anexoCq06C602_num.setBorder(new EtchedBorder());
        this.anexoCq06C602_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C602_num, cc.xy(4, 5));
        this.anexoCq06C602.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C602, cc.xy(6, 5));
        this.anexoCq06C607_num.setText(bundle.getString("Quadro06Panel.anexoCq06C607_num.text"));
        this.anexoCq06C607_num.setBorder(new EtchedBorder());
        this.anexoCq06C607_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C607_num, cc.xy(10, 5));
        this.anexoCq06C607.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C607, cc.xy(12, 5));
        this.anexoCq06C612_num.setText(bundle.getString("Quadro06Panel.anexoCq06C612_num.text"));
        this.anexoCq06C612_num.setBorder(new EtchedBorder());
        this.anexoCq06C612_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C612_num, cc.xy(16, 5));
        this.anexoCq06C612.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C612, cc.xy(18, 5));
        this.anexoCq06C603_base_label\u00a3anexoCq06C608_base_label\u00a3anexoCq06C613_base_label.setText(bundle.getString("Quadro06Panel.anexoCq06C603_base_label\u00a3anexoCq06C608_base_label\u00a3anexoCq06C613_base_label.text"));
        this.panel6.add((Component)this.anexoCq06C603_base_label\u00a3anexoCq06C608_base_label\u00a3anexoCq06C613_base_label, cc.xy(2, 7));
        this.anexoCq06C603_num.setText(bundle.getString("Quadro06Panel.anexoCq06C603_num.text"));
        this.anexoCq06C603_num.setBorder(new EtchedBorder());
        this.anexoCq06C603_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C603_num, cc.xy(4, 7));
        this.anexoCq06C603.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C603, cc.xy(6, 7));
        this.anexoCq06C608_num.setText(bundle.getString("Quadro06Panel.anexoCq06C608_num.text"));
        this.anexoCq06C608_num.setBorder(new EtchedBorder());
        this.anexoCq06C608_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C608_num, cc.xy(10, 7));
        this.anexoCq06C608.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C608, cc.xy(12, 7));
        this.anexoCq06C613_num.setText(bundle.getString("Quadro06Panel.anexoCq06C613_num.text"));
        this.anexoCq06C613_num.setBorder(new EtchedBorder());
        this.anexoCq06C613_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C613_num, cc.xy(16, 7));
        this.anexoCq06C613.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C613, cc.xy(18, 7));
        this.anexoCq06C604_base_label\u00a3anexoCq06C609_base_label\u00a3anexoCq06C614_base_label.setText(bundle.getString("Quadro06Panel.anexoCq06C604_base_label\u00a3anexoCq06C609_base_label\u00a3anexoCq06C614_base_label.text"));
        this.panel6.add((Component)this.anexoCq06C604_base_label\u00a3anexoCq06C609_base_label\u00a3anexoCq06C614_base_label, cc.xy(2, 9));
        this.anexoCq06C604_num.setText(bundle.getString("Quadro06Panel.anexoCq06C604_num.text"));
        this.anexoCq06C604_num.setBorder(new EtchedBorder());
        this.anexoCq06C604_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C604_num, cc.xy(4, 9));
        this.anexoCq06C604.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C604, cc.xy(6, 9));
        this.anexoCq06C609_num.setText(bundle.getString("Quadro06Panel.anexoCq06C609_num.text"));
        this.anexoCq06C609_num.setBorder(new EtchedBorder());
        this.anexoCq06C609_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C609_num, cc.xy(10, 9));
        this.anexoCq06C609.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C609, cc.xy(12, 9));
        this.anexoCq06C614_num.setText(bundle.getString("Quadro06Panel.anexoCq06C614_num.text"));
        this.anexoCq06C614_num.setBorder(new EtchedBorder());
        this.anexoCq06C614_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C614_num, cc.xy(16, 9));
        this.anexoCq06C614.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C614, cc.xy(18, 9));
        this.anexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label.setText(bundle.getString("Quadro06Panel.anexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label.text"));
        this.anexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label.setHorizontalAlignment(4);
        this.panel6.add((Component)this.anexoCq06C605_base_label\u00a3anexoCq06C610_base_label\u00a3anexoCq06C615_base_label, cc.xy(2, 11));
        this.anexoCq06C605_num.setText(bundle.getString("Quadro06Panel.anexoCq06C605_num.text"));
        this.anexoCq06C605_num.setBorder(new EtchedBorder());
        this.anexoCq06C605_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C605_num, cc.xy(4, 11));
        this.anexoCq06C605.setColumns(13);
        this.anexoCq06C605.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C605, cc.xy(6, 11));
        this.anexoCq06C610_num.setText(bundle.getString("Quadro06Panel.anexoCq06C610_num.text"));
        this.anexoCq06C610_num.setBorder(new EtchedBorder());
        this.anexoCq06C610_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C610_num, cc.xy(10, 11));
        this.anexoCq06C610.setColumns(13);
        this.anexoCq06C610.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C610, cc.xy(12, 11));
        this.anexoCq06C615_num.setText(bundle.getString("Quadro06Panel.anexoCq06C615_num.text"));
        this.anexoCq06C615_num.setBorder(new EtchedBorder());
        this.anexoCq06C615_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C615_num, cc.xy(16, 11));
        this.anexoCq06C615.setColumns(13);
        this.anexoCq06C615.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C615, cc.xy(18, 11));
        this.anexoCq06C616_base_label.setText(bundle.getString("Quadro06Panel.anexoCq06C616_base_label.text"));
        this.panel6.add((Component)this.anexoCq06C616_base_label, cc.xywh(4, 13, 9, 1));
        this.anexoCq06C616_num.setText(bundle.getString("Quadro06Panel.anexoCq06C616_num.text"));
        this.anexoCq06C616_num.setBorder(new EtchedBorder());
        this.anexoCq06C616_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq06C616_num, cc.xy(16, 13));
        this.anexoCq06C616.setEditable(false);
        this.panel6.add((Component)this.anexoCq06C616, cc.xy(18, 13));
        this.this2.add((Component)this.panel6, cc.xy(1, 3));
        this.add((Component)this.this2, "Center");
    }
}

