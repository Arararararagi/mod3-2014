/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro04Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro04PanelBase
extends Quadro04Panel
implements IBindablePanel<Quadro04> {
    private static final long serialVersionUID = 1;
    protected Quadro04 model;

    @Override
    public abstract void setModel(Quadro04 var1, boolean var2);

    @Override
    public Quadro04 getModel() {
        return this.model;
    }

    public Quadro04PanelBase(Quadro04 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

