/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro06Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro06PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro06PanelExtension
extends Quadro06PanelBase
implements IBindablePanel<Quadro06> {
    public Quadro06PanelExtension(Quadro06 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro06 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro06Bindings.doBindings(model, this);
        }
    }
}

