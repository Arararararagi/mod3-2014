/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro15Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel label8;
    protected JLabel anexoCq15C1501_label;
    protected JLabelTextFieldNumbering anexoCq15C1501_num;
    protected JMoneyTextField anexoCq15C1501;
    protected JLabel anexoCq15C1502_label;
    protected JLabelTextFieldNumbering anexoCq15C1502_num;
    protected JMoneyTextField anexoCq15C1502;

    public Quadro15Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JLabel getAnexoCq15C1501_label() {
        return this.anexoCq15C1501_label;
    }

    public JLabelTextFieldNumbering getAnexoCq15C1501_num() {
        return this.anexoCq15C1501_num;
    }

    public JMoneyTextField getAnexoCq15C1501() {
        return this.anexoCq15C1501;
    }

    public JLabel getAnexoCq15C1502_label() {
        return this.anexoCq15C1502_label;
    }

    public JLabelTextFieldNumbering getAnexoCq15C1502_num() {
        return this.anexoCq15C1502_num;
    }

    public JMoneyTextField getAnexoCq15C1502() {
        return this.anexoCq15C1502;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.label8 = new JLabel();
        this.anexoCq15C1501_label = new JLabel();
        this.anexoCq15C1501_num = new JLabelTextFieldNumbering();
        this.anexoCq15C1501 = new JMoneyTextField();
        this.anexoCq15C1502_label = new JLabel();
        this.anexoCq15C1502_num = new JLabelTextFieldNumbering();
        this.anexoCq15C1502 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro15Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro15Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "2*(default, $lgap), default"));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("$ugap, default:grow, 0px, $lcgap, default, $rgap, 25dlu, 0px, 75dlu, $lcgap, 30dlu:grow, $lcgap, default, $lcgap, 25dlu, 0px, 75dlu, $lcgap, default:grow", "3*(default, $lgap), default"));
        this.label8.setText(bundle.getString("Quadro15Panel.label8.text"));
        this.label8.setHorizontalAlignment(2);
        this.panel4.add((Component)this.label8, cc.xywh(2, 1, 9, 1));
        this.anexoCq15C1501_label.setText(bundle.getString("Quadro15Panel.anexoCq15C1501_label.text"));
        this.anexoCq15C1501_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoCq15C1501_label, cc.xy(5, 5));
        this.anexoCq15C1501_num.setText(bundle.getString("Quadro15Panel.anexoCq15C1501_num.text"));
        this.anexoCq15C1501_num.setBorder(new EtchedBorder());
        this.anexoCq15C1501_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq15C1501_num, cc.xy(7, 5));
        this.anexoCq15C1501.setColumns(13);
        this.panel4.add((Component)this.anexoCq15C1501, cc.xy(9, 5));
        this.anexoCq15C1502_label.setText(bundle.getString("Quadro15Panel.anexoCq15C1502_label.text"));
        this.anexoCq15C1502_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoCq15C1502_label, cc.xy(13, 5));
        this.anexoCq15C1502_num.setText(bundle.getString("Quadro15Panel.anexoCq15C1502_num.text"));
        this.anexoCq15C1502_num.setBorder(new EtchedBorder());
        this.anexoCq15C1502_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq15C1502_num, cc.xy(15, 5));
        this.anexoCq15C1502.setColumns(13);
        this.panel4.add((Component)this.anexoCq15C1502, cc.xy(17, 5));
        this.this2.add((Component)this.panel4, cc.xy(2, 3));
        this.add((Component)this.this2, "Center");
    }
}

