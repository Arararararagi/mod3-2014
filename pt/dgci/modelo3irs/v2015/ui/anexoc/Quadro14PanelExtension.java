/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro14Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro14PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro14PanelExtension
extends Quadro14PanelBase
implements IBindablePanel<Quadro14> {
    public static final int MAX_LINES_Q04_T1 = 200;

    public Quadro14PanelExtension(Quadro14 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro14 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro14Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoCq14T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoCq14T1() != null && this.model.getAnexoCq14T1().size() >= 200) {
            return;
        }
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            AnexoCq14T1_Linha linha = new AnexoCq14T1_Linha();
            linha.setArt139CIRC("N");
            this.model.getAnexoCq14T1().add(linha);
        }
    }
}

