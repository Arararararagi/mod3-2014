/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro12Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro12PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro12PanelExtension
extends Quadro12PanelBase
implements IBindablePanel<Quadro12> {
    public static final int MAX_LINES_Q12_T1 = 100;

    public Quadro12PanelExtension(Quadro12 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro12 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro12Bindings.doBindings(model, this);
        }
        if (model != null) {
            this.setHeader();
            this.setColumnSizes();
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoCq12T1().getTableHeader().getColumnModel();
        ColumnGroup columnGroup = new ColumnGroup("Subs\u00eddios n\u00e3o destinados \u00e0 explora\u00e7\u00e3o");
        columnGroup.add(cm.getColumn(3));
        columnGroup.add(cm.getColumn(4));
        columnGroup.add(cm.getColumn(5));
        columnGroup.add(cm.getColumn(6));
        columnGroup.add(cm.getColumn(7));
        if (this.getAnexoCq12T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoCq12T1().getTableHeader()).addColumnGroup(columnGroup);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(columnGroup);
        this.getAnexoCq12T1().setTableHeader(header);
    }

    private void setColumnSizes() {
        if (this.getAnexoCq12T1().getColumnCount() >= 4) {
            this.getAnexoCq12T1().getColumnModel().getColumn(2).setMinWidth(130);
            this.getAnexoCq12T1().getColumnModel().getColumn(2).setMaxWidth(205);
            this.getAnexoCq12T1().getColumnModel().getColumn(1).setMinWidth(110);
            this.getAnexoCq12T1().getColumnModel().getColumn(1).setMaxWidth(140);
            this.getAnexoCq12T1().getColumnModel().getColumn(3).setMinWidth(90);
            this.getAnexoCq12T1().getColumnModel().getColumn(3).setMaxWidth(110);
            this.getAnexoCq12T1().getColumnModel().getColumn(4).setMinWidth(90);
            this.getAnexoCq12T1().getColumnModel().getColumn(4).setMaxWidth(110);
            this.getAnexoCq12T1().getColumnModel().getColumn(5).setMinWidth(90);
            this.getAnexoCq12T1().getColumnModel().getColumn(5).setMaxWidth(110);
            this.getAnexoCq12T1().getColumnModel().getColumn(6).setMinWidth(90);
            this.getAnexoCq12T1().getColumnModel().getColumn(6).setMaxWidth(110);
            this.getAnexoCq12T1().getColumnModel().getColumn(7).setMinWidth(90);
            this.getAnexoCq12T1().getColumnModel().getColumn(7).setMaxWidth(110);
        }
    }

    @Override
    protected void addLineAnexoCq12T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoCq12T1() != null && this.model.getAnexoCq12T1().size() >= 100) {
            return;
        }
        super.addLineAnexoCq12T1_LinhaActionPerformed(e);
    }
}

