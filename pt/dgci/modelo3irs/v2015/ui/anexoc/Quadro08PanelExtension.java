/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro08Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro08PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro08PanelExtension
extends Quadro08PanelBase
implements IBindablePanel<Quadro08> {
    public static final int MAX_LINES_Q08_T1 = 2000;

    public Quadro08PanelExtension(Quadro08 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro08 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro08Bindings.doBindings(model, this);
        }
        if (model != null) {
            this.setColumnSizes();
        }
    }

    @Override
    protected void addLineAnexoCq08T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoCq08T1() != null && this.model.getAnexoCq08T1().size() >= 2000) {
            return;
        }
        super.addLineAnexoCq08T1_LinhaActionPerformed(e);
    }

    private void setColumnSizes() {
        if (this.getAnexoCq08T1().getColumnCount() >= 1) {
            this.getAnexoCq08T1().getColumnModel().getColumn(0).setMaxWidth(75);
        }
    }
}

