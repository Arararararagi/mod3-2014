/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro08Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel8;
    protected JPanel panel6;
    protected JLabel anexoCq08C801_label;
    protected JPanel panel7;
    protected JLabel anexoCq08C802_label;
    protected JPanel panel9;
    protected JLabel anexoCq08C803_label;
    protected JPanel panel10;
    protected JLabel anexoCq08C804_label;
    protected JPanel panel1;
    protected JLabel anexoCq08C805_label;
    protected JLabelTextFieldNumbering anexoCq08C801_num;
    protected JMoneyTextField anexoCq08C801;
    protected JLabelTextFieldNumbering anexoCq08C802_num;
    protected JMoneyTextField anexoCq08C802;
    protected JLabelTextFieldNumbering anexoCq08C803_num;
    protected JMoneyTextField anexoCq08C803;
    protected JLabelTextFieldNumbering anexoCq08C804_num;
    protected JMoneyTextField anexoCq08C804;
    protected JLabelTextFieldNumbering anexoCq08C805_num;
    protected JMoneyTextField anexoCq08C805;
    protected JPanel panel3;
    protected JLabel label10;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoCq08T1Scroll;
    protected JTable anexoCq08T1;

    public Quadro08Panel() {
        this.initComponents();
    }

    protected void addLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JMoneyTextField getAnexoCq08C801() {
        return this.anexoCq08C801;
    }

    public JMoneyTextField getAnexoCq08C802() {
        return this.anexoCq08C802;
    }

    public JMoneyTextField getAnexoCq08C803() {
        return this.anexoCq08C803;
    }

    public JMoneyTextField getAnexoCq08C804() {
        return this.anexoCq08C804;
    }

    public JMoneyTextField getAnexoCq08C805() {
        return this.anexoCq08C805;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoCq08T1Scroll() {
        return this.anexoCq08T1Scroll;
    }

    public JTable getAnexoCq08T1() {
        return this.anexoCq08T1;
    }

    protected void addLineAnexoCq08T1_linhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoCq08T1_linhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JLabelTextFieldNumbering getAnexoCq08C801_num() {
        return this.anexoCq08C801_num;
    }

    public JLabelTextFieldNumbering getAnexoCq08C802_num() {
        return this.anexoCq08C802_num;
    }

    public JLabelTextFieldNumbering getAnexoCq08C803_num() {
        return this.anexoCq08C803_num;
    }

    public JLabelTextFieldNumbering getAnexoCq08C804_num() {
        return this.anexoCq08C804_num;
    }

    public JLabelTextFieldNumbering getAnexoCq08C805_num() {
        return this.anexoCq08C805_num;
    }

    protected void addLineAnexoCq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoCq08T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getAnexoCq08C805_label() {
        return this.anexoCq08C805_label;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getAnexoCq08C801_label() {
        return this.anexoCq08C801_label;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoCq08C802_label() {
        return this.anexoCq08C802_label;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabel getAnexoCq08C803_label() {
        return this.anexoCq08C803_label;
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabel getAnexoCq08C804_label() {
        return this.anexoCq08C804_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel8 = new JPanel();
        this.panel6 = new JPanel();
        this.anexoCq08C801_label = new JLabel();
        this.panel7 = new JPanel();
        this.anexoCq08C802_label = new JLabel();
        this.panel9 = new JPanel();
        this.anexoCq08C803_label = new JLabel();
        this.panel10 = new JPanel();
        this.anexoCq08C804_label = new JLabel();
        this.panel1 = new JPanel();
        this.anexoCq08C805_label = new JLabel();
        this.anexoCq08C801_num = new JLabelTextFieldNumbering();
        this.anexoCq08C801 = new JMoneyTextField();
        this.anexoCq08C802_num = new JLabelTextFieldNumbering();
        this.anexoCq08C802 = new JMoneyTextField();
        this.anexoCq08C803_num = new JLabelTextFieldNumbering();
        this.anexoCq08C803 = new JMoneyTextField();
        this.anexoCq08C804_num = new JLabelTextFieldNumbering();
        this.anexoCq08C804 = new JMoneyTextField();
        this.anexoCq08C805_num = new JLabelTextFieldNumbering();
        this.anexoCq08C805 = new JMoneyTextField();
        this.panel3 = new JPanel();
        this.label10 = new JLabel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoCq08T1Scroll = new JScrollPane();
        this.anexoCq08T1 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro08Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro08Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $rgap", "3*(default, $lgap), default"));
        this.panel8.setPreferredSize(null);
        this.panel8.setMinimumSize(null);
        this.panel8.setLayout(new FormLayout("default:grow, $lcgap, 10dlu, 25dlu, 0px, 85dlu, 10dlu, $rgap, 6dlu, $rgap, 25dlu, 0px, 75dlu, $rgap, 6dlu, $rgap, 25dlu, 0px, 75dlu, $rgap, 6dlu, $rgap, 25dlu, 0px, 85dlu, default, 2*($rgap, 6dlu), 25dlu, 0px, 85dlu, 5dlu, default:grow, $rgap", "3*(default, $lgap), default"));
        this.panel6.setBorder(new EtchedBorder());
        this.panel6.setLayout(new FormLayout("124dlu", "28dlu"));
        this.anexoCq08C801_label.setText(bundle.getString("Quadro08Panel.anexoCq08C801_label.text"));
        this.anexoCq08C801_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq08C801_label, cc.xy(1, 1));
        this.panel8.add((Component)this.panel6, cc.xywh(3, 1, 5, 1));
        this.panel7.setBorder(new EtchedBorder());
        this.panel7.setLayout(new FormLayout("$rgap, 94dlu, $rgap", "28dlu"));
        this.anexoCq08C802_label.setText(bundle.getString("Quadro08Panel.anexoCq08C802_label.text"));
        this.anexoCq08C802_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoCq08C802_label, cc.xy(2, 1));
        this.panel8.add((Component)this.panel7, cc.xywh(11, 1, 3, 1));
        this.panel9.setBorder(new EtchedBorder());
        this.panel9.setLayout(new FormLayout("$rgap, 89dlu, $rgap", "28dlu"));
        this.anexoCq08C803_label.setText(bundle.getString("Quadro08Panel.anexoCq08C803_label.text"));
        this.anexoCq08C803_label.setHorizontalAlignment(0);
        this.panel9.add((Component)this.anexoCq08C803_label, cc.xy(2, 1));
        this.panel8.add((Component)this.panel9, cc.xywh(17, 1, 3, 1));
        this.panel10.setBorder(new EtchedBorder());
        this.panel10.setLayout(new FormLayout("101dlu", "28dlu"));
        this.anexoCq08C804_label.setText(bundle.getString("Quadro08Panel.anexoCq08C804_label.text"));
        this.anexoCq08C804_label.setHorizontalAlignment(0);
        this.panel10.add((Component)this.anexoCq08C804_label, cc.xy(1, 1));
        this.panel8.add((Component)this.panel10, cc.xywh(23, 1, 3, 1));
        this.panel1.setBorder(new EtchedBorder());
        this.panel1.setLayout(new FormLayout("112dlu", "28dlu"));
        this.anexoCq08C805_label.setText(bundle.getString("Quadro08Panel.anexoCq08C805_label.text"));
        this.anexoCq08C805_label.setHorizontalAlignment(0);
        this.panel1.add((Component)this.anexoCq08C805_label, cc.xy(1, 1));
        this.panel8.add((Component)this.panel1, cc.xywh(30, 1, 5, 1));
        this.anexoCq08C801_num.setText(bundle.getString("Quadro08Panel.anexoCq08C801_num.text"));
        this.anexoCq08C801_num.setBorder(new EtchedBorder());
        this.anexoCq08C801_num.setHorizontalAlignment(0);
        this.panel8.add((Component)this.anexoCq08C801_num, cc.xy(4, 3));
        this.anexoCq08C801.setColumns(13);
        this.panel8.add((Component)this.anexoCq08C801, cc.xy(6, 3));
        this.anexoCq08C802_num.setText(bundle.getString("Quadro08Panel.anexoCq08C802_num.text"));
        this.anexoCq08C802_num.setBorder(new EtchedBorder());
        this.anexoCq08C802_num.setHorizontalAlignment(0);
        this.panel8.add((Component)this.anexoCq08C802_num, cc.xy(11, 3));
        this.anexoCq08C802.setColumns(13);
        this.panel8.add((Component)this.anexoCq08C802, cc.xy(13, 3));
        this.anexoCq08C803_num.setText(bundle.getString("Quadro08Panel.anexoCq08C803_num.text"));
        this.anexoCq08C803_num.setBorder(new EtchedBorder());
        this.anexoCq08C803_num.setHorizontalAlignment(0);
        this.panel8.add((Component)this.anexoCq08C803_num, cc.xy(17, 3));
        this.anexoCq08C803.setColumns(13);
        this.panel8.add((Component)this.anexoCq08C803, cc.xy(19, 3));
        this.anexoCq08C804_num.setText(bundle.getString("Quadro08Panel.anexoCq08C804_num.text"));
        this.anexoCq08C804_num.setBorder(new EtchedBorder());
        this.anexoCq08C804_num.setHorizontalAlignment(0);
        this.panel8.add((Component)this.anexoCq08C804_num, cc.xy(23, 3));
        this.anexoCq08C804.setColumns(13);
        this.anexoCq08C804.setEditable(false);
        this.panel8.add((Component)this.anexoCq08C804, cc.xy(25, 3));
        this.anexoCq08C805_num.setText(bundle.getString("Quadro08Panel.anexoCq08C805_num.text"));
        this.anexoCq08C805_num.setBorder(new EtchedBorder());
        this.anexoCq08C805_num.setHorizontalAlignment(0);
        this.panel8.add((Component)this.anexoCq08C805_num, cc.xy(31, 3));
        this.anexoCq08C805.setColumns(13);
        this.panel8.add((Component)this.anexoCq08C805, cc.xy(33, 3));
        this.this2.add((Component)this.panel8, cc.xy(2, 3));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 50dlu, $rgap, default:grow", "$ugap, 2*($lgap, default), $lgap, fill:165dlu, $lgap, default"));
        this.label10.setText(bundle.getString("Quadro08Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label10, cc.xywh(1, 3, 9, 1));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro08Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.addLineAnexoCq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro08Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro08Panel.this.removeLineAnexoCq08T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 5));
        this.anexoCq08T1Scroll.setViewportView(this.anexoCq08T1);
        this.panel3.add((Component)this.anexoCq08T1Scroll, cc.xywh(3, 7, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 5));
        this.add((Component)this.this2, "Center");
    }

}

