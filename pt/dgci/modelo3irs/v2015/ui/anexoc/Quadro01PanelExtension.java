/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro01Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro01PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro01PanelExtension
extends Quadro01PanelBase
implements IBindablePanel<Quadro01> {
    public Quadro01PanelExtension(Quadro01 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro01 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro01Bindings.doBindings(model, this);
        }
    }
}

