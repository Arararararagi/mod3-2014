/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro13Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro13PanelBase
extends Quadro13Panel
implements IBindablePanel<Quadro13> {
    private static final long serialVersionUID = 1;
    protected Quadro13 model;

    @Override
    public abstract void setModel(Quadro13 var1, boolean var2);

    @Override
    public Quadro13 getModel() {
        return this.model;
    }

    public Quadro13PanelBase(Quadro13 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

