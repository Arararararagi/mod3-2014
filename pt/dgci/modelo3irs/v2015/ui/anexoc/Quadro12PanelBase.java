/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro12Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro12PanelBase
extends Quadro12Panel
implements IBindablePanel<Quadro12> {
    private static final long serialVersionUID = 1;
    protected Quadro12 model;

    @Override
    public abstract void setModel(Quadro12 var1, boolean var2);

    @Override
    public Quadro12 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoCq12T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoCq12T1().add(new AnexoCq12T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoCq12T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoCq12T1.getSelectedRow() != -1 ? this.anexoCq12T1.getSelectedRow() : this.anexoCq12T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoCq12T1().remove(selectedRow);
            }
        }
    }

    public Quadro12PanelBase(Quadro12 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

