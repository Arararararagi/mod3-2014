/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro08Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro08PanelBase
extends Quadro08Panel
implements IBindablePanel<Quadro08> {
    private static final long serialVersionUID = 1;
    protected Quadro08 model;

    @Override
    public abstract void setModel(Quadro08 var1, boolean var2);

    @Override
    public Quadro08 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoCq08T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoCq08T1().add(new AnexoCq08T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoCq08T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoCq08T1.getSelectedRow() != -1 ? this.anexoCq08T1.getSelectedRow() : this.anexoCq08T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoCq08T1().remove(selectedRow);
            }
        }
    }

    public Quadro08PanelBase(Quadro08 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

