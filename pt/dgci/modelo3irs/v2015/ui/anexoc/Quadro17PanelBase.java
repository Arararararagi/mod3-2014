/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro17Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro17PanelBase
extends Quadro17Panel
implements IBindablePanel<Quadro17> {
    private static final long serialVersionUID = 1;
    protected Quadro17 model;

    @Override
    public abstract void setModel(Quadro17 var1, boolean var2);

    @Override
    public Quadro17 getModel() {
        return this.model;
    }

    public Quadro17PanelBase(Quadro17 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

