/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro15Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro15PanelBase
extends Quadro15Panel
implements IBindablePanel<Quadro15> {
    private static final long serialVersionUID = 1;
    protected Quadro15 model;

    @Override
    public abstract void setModel(Quadro15 var1, boolean var2);

    @Override
    public Quadro15 getModel() {
        return this.model;
    }

    public Quadro15PanelBase(Quadro15 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

