/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro11Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel anexoCq11C1101_label;
    protected JLabelTextFieldNumbering anexoCq11C1101_num;
    protected JMoneyTextField anexoCq11C1101;

    public Quadro11Panel() {
        this.initComponents();
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoCq11C1101_label() {
        return this.anexoCq11C1101_label;
    }

    public JLabelTextFieldNumbering getAnexoCq11C1101_num() {
        return this.anexoCq11C1101_num;
    }

    public JMoneyTextField getAnexoCq11C1101() {
        return this.anexoCq11C1101;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.anexoCq11C1101_label = new JLabel();
        this.anexoCq11C1101_num = new JLabelTextFieldNumbering();
        this.anexoCq11C1101 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro11Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro11Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "default, $lgap, default"));
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, 75dlu, $rgap", "$rgap, default, $lgap, default"));
        this.anexoCq11C1101_label.setText(bundle.getString("Quadro11Panel.anexoCq11C1101_label.text"));
        this.panel4.add((Component)this.anexoCq11C1101_label, cc.xy(1, 2));
        this.anexoCq11C1101_num.setText(bundle.getString("Quadro11Panel.anexoCq11C1101_num.text"));
        this.anexoCq11C1101_num.setBorder(new EtchedBorder());
        this.anexoCq11C1101_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq11C1101_num, cc.xy(3, 2));
        this.panel4.add((Component)this.anexoCq11C1101, cc.xy(5, 2));
        this.this2.add((Component)this.panel4, cc.xy(3, 1));
        this.add((Component)this.this2, "Center");
    }
}

