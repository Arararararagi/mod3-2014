/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro16Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel anexoCq16B1OPSim_base_label;
    protected JLabelTextFieldNumbering label22;
    protected JRadioButton anexoCq16B1OP1;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton anexoCq16B1OP2;
    protected JLabel anexoCq16C3_label;
    protected JLabelTextFieldNumbering label24;
    protected JDateTextField anexoCq16C3;
    protected JPanel panel5;
    protected JLabel anexoCq16B4_label2;
    protected JLabelTextFieldNumbering label29;
    protected JCheckBox anexoCq16B4;

    public Quadro16Panel() {
        this.initComponents();
    }

    protected void addLineAnexoCq14T1_linhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoCq14T1_linhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoCq16B1OPSim_base_label() {
        return this.anexoCq16B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JRadioButton getAnexoCq16B1OP1() {
        return this.anexoCq16B1OP1;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getAnexoCq16B1OP2() {
        return this.anexoCq16B1OP2;
    }

    public JLabel getAnexoCq16C3_label() {
        return this.anexoCq16C3_label;
    }

    public JLabelTextFieldNumbering getLabel24() {
        return this.label24;
    }

    public JDateTextField getAnexoCq16C3() {
        return this.anexoCq16C3;
    }

    public JCheckBox getAnexoCq16B4() {
        return this.anexoCq16B4;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getAnexoCq16B4_label2() {
        return this.anexoCq16B4_label2;
    }

    public JLabelTextFieldNumbering getLabel29() {
        return this.label29;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.anexoCq16B1OPSim_base_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoCq16B1OP1 = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.anexoCq16B1OP2 = new JRadioButton();
        this.anexoCq16C3_label = new JLabel();
        this.label24 = new JLabelTextFieldNumbering();
        this.anexoCq16C3 = new JDateTextField();
        this.panel5 = new JPanel();
        this.anexoCq16B4_label2 = new JLabel();
        this.label29 = new JLabelTextFieldNumbering();
        this.anexoCq16B4 = new JCheckBox();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro16Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro16Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "2*(default, $lgap), default"));
        this.panel4.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 13dlu, 0px, default, $rgap, default, $lcgap, 13dlu, 0px, default, $lcgap, default:grow", "$ugap, 4*($lgap, default)"));
        this.anexoCq16B1OPSim_base_label.setText(bundle.getString("Quadro16Panel.anexoCq16B1OPSim_base_label.text"));
        this.anexoCq16B1OPSim_base_label.setHorizontalAlignment(2);
        this.panel4.add((Component)this.anexoCq16B1OPSim_base_label, cc.xy(3, 3));
        this.label22.setText(bundle.getString("Quadro16Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label22, cc.xy(7, 3));
        this.anexoCq16B1OP1.setText(bundle.getString("Quadro16Panel.anexoCq16B1OP1.text"));
        this.panel4.add((Component)this.anexoCq16B1OP1, cc.xy(9, 3));
        this.label23.setText(bundle.getString("Quadro16Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label23, cc.xy(13, 3));
        this.anexoCq16B1OP2.setText(bundle.getString("Quadro16Panel.anexoCq16B1OP2.text"));
        this.panel4.add((Component)this.anexoCq16B1OP2, cc.xy(15, 3));
        this.anexoCq16C3_label.setText(bundle.getString("Quadro16Panel.anexoCq16C3_label.text"));
        this.anexoCq16C3_label.setHorizontalAlignment(2);
        this.panel4.add((Component)this.anexoCq16C3_label, cc.xy(3, 7));
        this.label24.setText(bundle.getString("Quadro16Panel.label24.text"));
        this.label24.setBorder(new EtchedBorder());
        this.label24.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label24, cc.xy(7, 7));
        this.anexoCq16C3.setColumns(10);
        this.panel4.add((Component)this.anexoCq16C3, cc.xywh(9, 7, 5, 1));
        this.this2.add((Component)this.panel4, cc.xy(2, 1));
        this.panel5.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 13dlu, 0px, default, $rgap, 0px, $lcgap, default:grow", "$ugap, 2*($lgap, default)"));
        this.anexoCq16B4_label2.setText(bundle.getString("Quadro16Panel.anexoCq16B4_label2.text"));
        this.anexoCq16B4_label2.setHorizontalAlignment(4);
        this.panel5.add((Component)this.anexoCq16B4_label2, cc.xy(3, 3));
        this.label29.setText(bundle.getString("Quadro16Panel.label29.text"));
        this.label29.setBorder(new EtchedBorder());
        this.label29.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label29, cc.xy(7, 3));
        this.panel5.add((Component)this.anexoCq16B4, cc.xy(9, 3));
        this.this2.add((Component)this.panel5, cc.xy(2, 3));
        this.add((Component)this.this2, "Center");
    }
}

