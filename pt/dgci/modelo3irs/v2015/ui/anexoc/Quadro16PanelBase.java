/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro16Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro16PanelBase
extends Quadro16Panel
implements IBindablePanel<Quadro16> {
    private static final long serialVersionUID = 1;
    protected Quadro16 model;

    @Override
    public abstract void setModel(Quadro16 var1, boolean var2);

    @Override
    public Quadro16 getModel() {
        return this.model;
    }

    public Quadro16PanelBase(Quadro16 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

