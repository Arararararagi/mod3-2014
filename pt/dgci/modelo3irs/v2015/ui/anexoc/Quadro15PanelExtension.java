/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro15Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro15PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro15PanelExtension
extends Quadro15PanelBase
implements IBindablePanel<Quadro15> {
    public Quadro15PanelExtension(Quadro15 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro15 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro15Bindings.doBindings(model, this);
        }
    }
}

