/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro16Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro16PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro16PanelExtension
extends Quadro16PanelBase
implements IBindablePanel<Quadro16> {
    public Quadro16PanelExtension(Quadro16 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro16 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro16Bindings.doBindings(model, this);
        }
    }
}

