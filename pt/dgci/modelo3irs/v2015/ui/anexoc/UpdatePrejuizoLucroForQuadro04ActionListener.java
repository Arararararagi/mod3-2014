/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;

public class UpdatePrejuizoLucroForQuadro04ActionListener
implements PropertyChangeListener {
    private Quadro04 model;

    public UpdatePrejuizoLucroForQuadro04ActionListener(Quadro04 model) {
        this.model = model;
    }

    @Override
    public void propertyChange(PropertyChangeEvent arg0) {
        long _diff = 0;
        Long _emptyValue = new Long(0);
        if ((_diff+=(this.model.getAnexoCq04C439() == null ? 0 : this.model.getAnexoCq04C439()) - (this.model.getAnexoCq04C458() == null ? 0 : this.model.getAnexoCq04C458())) > 0) {
            this.model.setAnexoCq04C459(_emptyValue);
            this.model.setAnexoCq04C460(Math.abs(_diff));
        } else if (_diff < 0) {
            this.model.setAnexoCq04C459(Math.abs(_diff));
            this.model.setAnexoCq04C460(_emptyValue);
        } else {
            this.model.setAnexoCq04C459(_emptyValue);
            this.model.setAnexoCq04C460(_emptyValue);
        }
    }
}

