/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro13Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JLabel label17;
    protected JLabel label14;
    protected JLabel label16;
    protected JLabel anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label;
    protected JLabel anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label;
    protected JLabel anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label;
    protected JLabel label2;
    protected JLabel label3;
    protected JLabel anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label;
    protected JLabel label37;
    protected JLabelTextFieldNumbering anexoCq13C1301_num;
    protected JMoneyTextField anexoCq13C1301;
    protected JLabelTextFieldNumbering anexoCq13C1302_num;
    protected JMoneyTextField anexoCq13C1302;
    protected JLabel label25;
    protected JLabelTextFieldNumbering anexoCq13C1303_num;
    protected JMoneyTextField anexoCq13C1303;
    protected JLabel label26;
    protected JLabelTextFieldNumbering anexoCq13C1304_num;
    protected JMoneyTextField anexoCq13C1304;
    protected JLabel label35;
    protected JLabel label27;
    protected JLabelTextFieldNumbering anexoCq13C1305_num;
    protected JMoneyTextField anexoCq13C1305;
    protected JLabel label36;
    protected JLabel label28;
    protected JLabelTextFieldNumbering anexoCq13C1306_num;
    protected JMoneyTextField anexoCq13C1306;
    protected JLabel anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label;
    protected JLabel label38;
    protected JLabelTextFieldNumbering anexoCq13C1307_num;
    protected JMoneyTextField anexoCq13C1307;
    protected JLabelTextFieldNumbering anexoCq13C1308_num;
    protected JMoneyTextField anexoCq13C1308;
    protected JLabel label29;
    protected JLabelTextFieldNumbering anexoCq13C1309_num;
    protected JMoneyTextField anexoCq13C1309;
    protected JLabel label30;
    protected JLabelTextFieldNumbering anexoCq13C1310_num;
    protected JMoneyTextField anexoCq13C1310;
    protected JLabel label39;
    protected JLabel label31;
    protected JLabelTextFieldNumbering anexoCq13C1311_num;
    protected JMoneyTextField anexoCq13C1311;
    protected JLabel label40;
    protected JLabel label32;
    protected JLabelTextFieldNumbering anexoCq13C1312_num;
    protected JMoneyTextField anexoCq13C1312;
    protected JLabel anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label;
    protected JLabel label41;
    protected JLabelTextFieldNumbering anexoCq13C1313_num;
    protected JMoneyTextField anexoCq13C1313;
    protected JLabelTextFieldNumbering anexoCq13C1314_num;
    protected JMoneyTextField anexoCq13C1314;
    protected JLabel label33;
    protected JLabelTextFieldNumbering anexoCq13C1315_num;
    protected JMoneyTextField anexoCq13C1315;
    protected JLabel label34;
    protected JLabelTextFieldNumbering anexoCq13C1316_num;
    protected JMoneyTextField anexoCq13C1316;
    protected JLabel label42;
    protected JLabel label44;
    protected JLabelTextFieldNumbering anexoCq13C1317_num;
    protected JMoneyTextField anexoCq13C1317;
    protected JLabel label43;
    protected JLabel label45;
    protected JLabelTextFieldNumbering anexoCq13C1318_num;
    protected JMoneyTextField anexoCq13C1318;

    public Quadro13Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label() {
        return this.anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label;
    }

    public JLabel getLabel16() {
        return this.label16;
    }

    public JLabel getAnexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label() {
        return this.anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label;
    }

    public JLabel getAnexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label() {
        return this.anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label;
    }

    public JLabel getAnexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label() {
        return this.anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabel getLabel17() {
        return this.label17;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1301_num() {
        return this.anexoCq13C1301_num;
    }

    public JMoneyTextField getAnexoCq13C1301() {
        return this.anexoCq13C1301;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1302_num() {
        return this.anexoCq13C1302_num;
    }

    public JMoneyTextField getAnexoCq13C1302() {
        return this.anexoCq13C1302;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1303_num() {
        return this.anexoCq13C1303_num;
    }

    public JMoneyTextField getAnexoCq13C1303() {
        return this.anexoCq13C1303;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1304_num() {
        return this.anexoCq13C1304_num;
    }

    public JMoneyTextField getAnexoCq13C1304() {
        return this.anexoCq13C1304;
    }

    public JLabel getLabel25() {
        return this.label25;
    }

    public JLabel getLabel26() {
        return this.label26;
    }

    public JLabel getLabel27() {
        return this.label27;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1305_num() {
        return this.anexoCq13C1305_num;
    }

    public JMoneyTextField getAnexoCq13C1305() {
        return this.anexoCq13C1305;
    }

    public JLabel getLabel28() {
        return this.label28;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1306_num() {
        return this.anexoCq13C1306_num;
    }

    public JMoneyTextField getAnexoCq13C1306() {
        return this.anexoCq13C1306;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1307_num() {
        return this.anexoCq13C1307_num;
    }

    public JMoneyTextField getAnexoCq13C1307() {
        return this.anexoCq13C1307;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1308_num() {
        return this.anexoCq13C1308_num;
    }

    public JMoneyTextField getAnexoCq13C1308() {
        return this.anexoCq13C1308;
    }

    public JLabel getLabel29() {
        return this.label29;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1309_num() {
        return this.anexoCq13C1309_num;
    }

    public JMoneyTextField getAnexoCq13C1309() {
        return this.anexoCq13C1309;
    }

    public JLabel getLabel30() {
        return this.label30;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1310_num() {
        return this.anexoCq13C1310_num;
    }

    public JMoneyTextField getAnexoCq13C1310() {
        return this.anexoCq13C1310;
    }

    public JLabel getLabel31() {
        return this.label31;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1311_num() {
        return this.anexoCq13C1311_num;
    }

    public JMoneyTextField getAnexoCq13C1311() {
        return this.anexoCq13C1311;
    }

    public JLabel getLabel32() {
        return this.label32;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1312_num() {
        return this.anexoCq13C1312_num;
    }

    public JMoneyTextField getAnexoCq13C1312() {
        return this.anexoCq13C1312;
    }

    public JLabel getLabel37() {
        return this.label37;
    }

    public JLabel getLabel35() {
        return this.label35;
    }

    public JLabel getLabel36() {
        return this.label36;
    }

    public JLabel getLabel38() {
        return this.label38;
    }

    public JLabel getLabel39() {
        return this.label39;
    }

    public JLabel getLabel40() {
        return this.label40;
    }

    public JLabel getAnexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label() {
        return this.anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label;
    }

    public JLabel getAnexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label() {
        return this.anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label;
    }

    public JLabel getLabel41() {
        return this.label41;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1313_num() {
        return this.anexoCq13C1313_num;
    }

    public JMoneyTextField getAnexoCq13C1313() {
        return this.anexoCq13C1313;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1314_num() {
        return this.anexoCq13C1314_num;
    }

    public JMoneyTextField getAnexoCq13C1314() {
        return this.anexoCq13C1314;
    }

    public JLabel getLabel33() {
        return this.label33;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1315_num() {
        return this.anexoCq13C1315_num;
    }

    public JMoneyTextField getAnexoCq13C1315() {
        return this.anexoCq13C1315;
    }

    public JLabel getLabel34() {
        return this.label34;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1316_num() {
        return this.anexoCq13C1316_num;
    }

    public JMoneyTextField getAnexoCq13C1316() {
        return this.anexoCq13C1316;
    }

    public JLabel getLabel42() {
        return this.label42;
    }

    public JLabel getLabel44() {
        return this.label44;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1317_num() {
        return this.anexoCq13C1317_num;
    }

    public JMoneyTextField getAnexoCq13C1317() {
        return this.anexoCq13C1317;
    }

    public JLabel getLabel43() {
        return this.label43;
    }

    public JLabel getLabel45() {
        return this.label45;
    }

    public JLabelTextFieldNumbering getAnexoCq13C1318_num() {
        return this.anexoCq13C1318_num;
    }

    public JMoneyTextField getAnexoCq13C1318() {
        return this.anexoCq13C1318;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.label17 = new JLabel();
        this.label14 = new JLabel();
        this.label16 = new JLabel();
        this.anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label = new JLabel();
        this.anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label = new JLabel();
        this.anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label = new JLabel();
        this.label2 = new JLabel();
        this.label3 = new JLabel();
        this.anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label = new JLabel();
        this.label37 = new JLabel();
        this.anexoCq13C1301_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1301 = new JMoneyTextField();
        this.anexoCq13C1302_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1302 = new JMoneyTextField();
        this.label25 = new JLabel();
        this.anexoCq13C1303_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1303 = new JMoneyTextField();
        this.label26 = new JLabel();
        this.anexoCq13C1304_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1304 = new JMoneyTextField();
        this.label35 = new JLabel();
        this.label27 = new JLabel();
        this.anexoCq13C1305_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1305 = new JMoneyTextField();
        this.label36 = new JLabel();
        this.label28 = new JLabel();
        this.anexoCq13C1306_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1306 = new JMoneyTextField();
        this.anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label = new JLabel();
        this.label38 = new JLabel();
        this.anexoCq13C1307_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1307 = new JMoneyTextField();
        this.anexoCq13C1308_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1308 = new JMoneyTextField();
        this.label29 = new JLabel();
        this.anexoCq13C1309_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1309 = new JMoneyTextField();
        this.label30 = new JLabel();
        this.anexoCq13C1310_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1310 = new JMoneyTextField();
        this.label39 = new JLabel();
        this.label31 = new JLabel();
        this.anexoCq13C1311_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1311 = new JMoneyTextField();
        this.label40 = new JLabel();
        this.label32 = new JLabel();
        this.anexoCq13C1312_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1312 = new JMoneyTextField();
        this.anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label = new JLabel();
        this.label41 = new JLabel();
        this.anexoCq13C1313_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1313 = new JMoneyTextField();
        this.anexoCq13C1314_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1314 = new JMoneyTextField();
        this.label33 = new JLabel();
        this.anexoCq13C1315_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1315 = new JMoneyTextField();
        this.label34 = new JLabel();
        this.anexoCq13C1316_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1316 = new JMoneyTextField();
        this.label42 = new JLabel();
        this.label44 = new JLabel();
        this.anexoCq13C1317_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1317 = new JMoneyTextField();
        this.label43 = new JLabel();
        this.label45 = new JLabel();
        this.anexoCq13C1318_num = new JLabelTextFieldNumbering();
        this.anexoCq13C1318 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro13Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro13Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "default, $lgap, default"));
        this.panel3.setPreferredSize(null);
        this.panel3.setMinimumSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, default, 0px, $lcgap, default, $lcgap, 25dlu, 0px, 75dlu, $lcgap, 25dlu, 0px, 120dlu, $lcgap, default, 0px, $lcgap, 25dlu, 0px, 75dlu, $lcgap, default:grow", "default, 0px, default, $ugap, 3*(default, $lgap), default, $ugap, 3*(default, $lgap), default, 7dlu, 7*(default, $lgap), default"));
        this.label17.setText(bundle.getString("Quadro13Panel.label17.text"));
        this.label17.setHorizontalAlignment(0);
        this.label17.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label17, cc.xywh(3, 1, 1, 3));
        this.label14.setText(bundle.getString("Quadro13Panel.label14.text"));
        this.label14.setHorizontalAlignment(0);
        this.label14.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label14, cc.xywh(6, 1, 9, 1));
        this.label16.setText(bundle.getString("Quadro13Panel.label16.text"));
        this.label16.setHorizontalAlignment(0);
        this.label16.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label16, cc.xy(6, 3));
        this.anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label.setText(bundle.getString("Quadro13Panel.anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label.text"));
        this.anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label.setBorder(new EtchedBorder());
        this.anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1301_base_label\u00a3anexoCq13C1307_base_label, cc.xywh(8, 3, 3, 1));
        this.anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label.setText(bundle.getString("Quadro13Panel.anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label.text"));
        this.anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label.setBorder(new EtchedBorder());
        this.anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1302_base_label\u00a3anexoCq13C1308_base_label, cc.xywh(12, 3, 3, 1));
        this.anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label.setText(bundle.getString("Quadro13Panel.anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label.text"));
        this.anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label.setHorizontalAlignment(0);
        this.anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoCq13C1303_base_label\u00a3anexoCq13C1304_base_label\u00a3anexoCq13C1305_base_label\u00a3anexoCq13C1306_base_label\u00a3anexoCq13C1309_base_label\u00a3anexoCq13C1310_base_label\u00a3anexoCq13C1311_base_label\u00a3anexoCq13C1312_base_label, cc.xywh(16, 1, 6, 1));
        this.label2.setText(bundle.getString("Quadro13Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label2, cc.xy(16, 3));
        this.label3.setText(bundle.getString("Quadro13Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label3, cc.xywh(19, 3, 3, 1));
        this.anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label.setText(bundle.getString("Quadro13Panel.anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label.text"));
        this.anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label.setHorizontalAlignment(2);
        this.anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoCq13C1301_label\u00a3anexoCq13C1302_label\u00a3anexoCq13C1303_label\u00a3anexoCq13C1304_label\u00a3anexoCq13C1305_label\u00a3anexoCq13C1306_label, cc.xywh(3, 5, 1, 7));
        this.label37.setText(bundle.getString("Quadro13Panel.label37.text"));
        this.label37.setBorder(new EtchedBorder());
        this.label37.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label37, cc.xywh(6, 5, 1, 3));
        this.anexoCq13C1301_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1301_num.text"));
        this.anexoCq13C1301_num.setBorder(new EtchedBorder());
        this.anexoCq13C1301_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1301_num, cc.xywh(8, 5, 1, 3));
        this.panel3.add((Component)this.anexoCq13C1301, cc.xywh(10, 5, 1, 3));
        this.anexoCq13C1302_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1302_num.text"));
        this.anexoCq13C1302_num.setBorder(new EtchedBorder());
        this.anexoCq13C1302_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1302_num, cc.xywh(12, 5, 1, 3));
        this.panel3.add((Component)this.anexoCq13C1302, cc.xywh(14, 5, 1, 3));
        this.label25.setText(bundle.getString("Quadro13Panel.label25.text"));
        this.label25.setBorder(new EtchedBorder());
        this.label25.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label25, cc.xy(16, 5));
        this.anexoCq13C1303_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1303_num.text"));
        this.anexoCq13C1303_num.setBorder(new EtchedBorder());
        this.anexoCq13C1303_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1303_num, cc.xy(19, 5));
        this.panel3.add((Component)this.anexoCq13C1303, cc.xy(21, 5));
        this.label26.setText(bundle.getString("Quadro13Panel.label26.text"));
        this.label26.setBorder(new EtchedBorder());
        this.label26.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label26, cc.xy(16, 7));
        this.anexoCq13C1304_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1304_num.text"));
        this.anexoCq13C1304_num.setBorder(new EtchedBorder());
        this.anexoCq13C1304_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1304_num, cc.xy(19, 7));
        this.panel3.add((Component)this.anexoCq13C1304, cc.xy(21, 7));
        this.label35.setText(bundle.getString("Quadro13Panel.label35.text"));
        this.label35.setBorder(new EtchedBorder());
        this.label35.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label35, cc.xy(6, 9));
        this.label27.setText(bundle.getString("Quadro13Panel.label27.text"));
        this.label27.setBorder(new EtchedBorder());
        this.label27.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label27, cc.xy(16, 9));
        this.anexoCq13C1305_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1305_num.text"));
        this.anexoCq13C1305_num.setBorder(new EtchedBorder());
        this.anexoCq13C1305_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1305_num, cc.xy(19, 9));
        this.panel3.add((Component)this.anexoCq13C1305, cc.xy(21, 9));
        this.label36.setText(bundle.getString("Quadro13Panel.label36.text"));
        this.label36.setBorder(new EtchedBorder());
        this.label36.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label36, cc.xy(6, 11));
        this.label28.setText(bundle.getString("Quadro13Panel.label28.text"));
        this.label28.setBorder(new EtchedBorder());
        this.label28.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label28, cc.xy(16, 11));
        this.anexoCq13C1306_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1306_num.text"));
        this.anexoCq13C1306_num.setBorder(new EtchedBorder());
        this.anexoCq13C1306_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1306_num, cc.xy(19, 11));
        this.panel3.add((Component)this.anexoCq13C1306, cc.xy(21, 11));
        this.anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label.setText(bundle.getString("Quadro13Panel.anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label.text"));
        this.anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label.setHorizontalAlignment(0);
        this.anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoCq13C1307_label\u00a3anexoCq13C1308_label\u00a3anexoCq13C1309_label\u00a3anexoCq13C1310_label\u00a3anexoCq13C1311_label\u00a3anexoCq13C1312_label, cc.xywh(3, 13, 1, 7));
        this.label38.setText(bundle.getString("Quadro13Panel.label38.text"));
        this.label38.setBorder(new EtchedBorder());
        this.label38.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label38, cc.xywh(6, 13, 1, 3));
        this.anexoCq13C1307_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1307_num.text"));
        this.anexoCq13C1307_num.setBorder(new EtchedBorder());
        this.anexoCq13C1307_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1307_num, cc.xywh(8, 13, 1, 3));
        this.panel3.add((Component)this.anexoCq13C1307, cc.xywh(10, 13, 1, 3));
        this.anexoCq13C1308_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1308_num.text"));
        this.anexoCq13C1308_num.setBorder(new EtchedBorder());
        this.anexoCq13C1308_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1308_num, cc.xywh(12, 13, 1, 3));
        this.panel3.add((Component)this.anexoCq13C1308, cc.xywh(14, 13, 1, 3));
        this.label29.setText(bundle.getString("Quadro13Panel.label29.text"));
        this.label29.setBorder(new EtchedBorder());
        this.label29.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label29, cc.xy(16, 13));
        this.anexoCq13C1309_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1309_num.text"));
        this.anexoCq13C1309_num.setBorder(new EtchedBorder());
        this.anexoCq13C1309_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1309_num, cc.xy(19, 13));
        this.panel3.add((Component)this.anexoCq13C1309, cc.xy(21, 13));
        this.label30.setText(bundle.getString("Quadro13Panel.label30.text"));
        this.label30.setBorder(new EtchedBorder());
        this.label30.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label30, cc.xy(16, 15));
        this.anexoCq13C1310_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1310_num.text"));
        this.anexoCq13C1310_num.setBorder(new EtchedBorder());
        this.anexoCq13C1310_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1310_num, cc.xy(19, 15));
        this.panel3.add((Component)this.anexoCq13C1310, cc.xy(21, 15));
        this.label39.setText(bundle.getString("Quadro13Panel.label39.text"));
        this.label39.setBorder(new EtchedBorder());
        this.label39.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label39, cc.xy(6, 17));
        this.label31.setText(bundle.getString("Quadro13Panel.label31.text"));
        this.label31.setBorder(new EtchedBorder());
        this.label31.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label31, cc.xy(16, 17));
        this.anexoCq13C1311_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1311_num.text"));
        this.anexoCq13C1311_num.setBorder(new EtchedBorder());
        this.anexoCq13C1311_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1311_num, cc.xy(19, 17));
        this.panel3.add((Component)this.anexoCq13C1311, cc.xy(21, 17));
        this.label40.setText(bundle.getString("Quadro13Panel.label40.text"));
        this.label40.setBorder(new EtchedBorder());
        this.label40.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label40, cc.xy(6, 19));
        this.label32.setText(bundle.getString("Quadro13Panel.label32.text"));
        this.label32.setBorder(new EtchedBorder());
        this.label32.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label32, cc.xy(16, 19));
        this.anexoCq13C1312_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1312_num.text"));
        this.anexoCq13C1312_num.setBorder(new EtchedBorder());
        this.anexoCq13C1312_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1312_num, cc.xy(19, 19));
        this.panel3.add((Component)this.anexoCq13C1312, cc.xy(21, 19));
        this.anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label.setText(bundle.getString("Quadro13Panel.anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label.text"));
        this.anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label.setHorizontalAlignment(2);
        this.anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoCq13C1313_label\u00a3anexoCq13C1314_label\u00a3anexoCq13C1315_label\u00a3anexoCq13C1316_label\u00a3anexoCq13C1317_label\u00a3anexoCq13C1318_label, cc.xywh(3, 21, 1, 7));
        this.label41.setText(bundle.getString("Quadro13Panel.label41.text"));
        this.label41.setBorder(new EtchedBorder());
        this.label41.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label41, cc.xywh(6, 21, 1, 3));
        this.anexoCq13C1313_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1313_num.text"));
        this.anexoCq13C1313_num.setBorder(new EtchedBorder());
        this.anexoCq13C1313_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1313_num, cc.xywh(8, 21, 1, 3));
        this.panel3.add((Component)this.anexoCq13C1313, cc.xywh(10, 21, 1, 3));
        this.anexoCq13C1314_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1314_num.text"));
        this.anexoCq13C1314_num.setBorder(new EtchedBorder());
        this.anexoCq13C1314_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1314_num, cc.xywh(12, 21, 1, 3));
        this.panel3.add((Component)this.anexoCq13C1314, cc.xywh(14, 21, 1, 3));
        this.label33.setText(bundle.getString("Quadro13Panel.label33.text"));
        this.label33.setBorder(new EtchedBorder());
        this.label33.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label33, cc.xy(16, 21));
        this.anexoCq13C1315_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1315_num.text"));
        this.anexoCq13C1315_num.setBorder(new EtchedBorder());
        this.anexoCq13C1315_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1315_num, cc.xy(19, 21));
        this.panel3.add((Component)this.anexoCq13C1315, cc.xy(21, 21));
        this.label34.setText(bundle.getString("Quadro13Panel.label34.text"));
        this.label34.setBorder(new EtchedBorder());
        this.label34.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label34, cc.xy(16, 23));
        this.anexoCq13C1316_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1316_num.text"));
        this.anexoCq13C1316_num.setBorder(new EtchedBorder());
        this.anexoCq13C1316_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1316_num, cc.xy(19, 23));
        this.panel3.add((Component)this.anexoCq13C1316, cc.xy(21, 23));
        this.label42.setText(bundle.getString("Quadro13Panel.label42.text"));
        this.label42.setBorder(new EtchedBorder());
        this.label42.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label42, cc.xy(6, 25));
        this.label44.setText(bundle.getString("Quadro13Panel.label44.text"));
        this.label44.setBorder(new EtchedBorder());
        this.label44.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label44, cc.xy(16, 25));
        this.anexoCq13C1317_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1317_num.text"));
        this.anexoCq13C1317_num.setBorder(new EtchedBorder());
        this.anexoCq13C1317_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1317_num, cc.xy(19, 25));
        this.panel3.add((Component)this.anexoCq13C1317, cc.xy(21, 25));
        this.label43.setText(bundle.getString("Quadro13Panel.label43.text"));
        this.label43.setBorder(new EtchedBorder());
        this.label43.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label43, cc.xy(6, 27));
        this.label45.setText(bundle.getString("Quadro13Panel.label45.text"));
        this.label45.setBorder(new EtchedBorder());
        this.label45.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label45, cc.xy(16, 27));
        this.anexoCq13C1318_num.setText(bundle.getString("Quadro13Panel.anexoCq13C1318_num.text"));
        this.anexoCq13C1318_num.setBorder(new EtchedBorder());
        this.anexoCq13C1318_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq13C1318_num, cc.xy(19, 27));
        this.panel3.add((Component)this.anexoCq13C1318, cc.xy(21, 27));
        this.this2.add((Component)this.panel3, cc.xy(3, 3));
        this.add((Component)this.this2, "Center");
    }
}

