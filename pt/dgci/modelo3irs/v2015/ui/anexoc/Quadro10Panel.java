/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro10Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoCq10C1001_label;
    protected JLabelTextFieldNumbering anexoCq10C1001_num;
    protected JMoneyTextField anexoCq10C1001;
    protected JLabel anexoCq10C1002_label;
    protected JLabelTextFieldNumbering anexoCq10C1002_num;
    protected JMoneyTextField anexoCq10C1002;
    protected JLabel anexoCq10C1006_label;
    protected JLabelTextFieldNumbering anexoCq10C1006_num;
    protected JMoneyTextField anexoCq10C1006;
    protected JLabel anexoCq10C1003_label;
    protected JLabelTextFieldNumbering anexoCq10C1003_num;
    protected JMoneyTextField anexoCq10C1003;
    protected JLabel anexoCq10C1007_label;
    protected JLabelTextFieldNumbering anexoCq10C1007_num;
    protected JMoneyTextField anexoCq10C1007;
    protected JLabel anexoCq10C1004_label;
    protected JLabelTextFieldNumbering anexoCq10C1004_num;
    protected JMoneyTextField anexoCq10C1004;
    protected JLabel anexoCq10C1005_label;
    protected JLabelTextFieldNumbering anexoCq10C1005_num;
    protected JMoneyTextField anexoCq10C1005;
    protected JLabel anexoCq10C1_label;
    protected JMoneyTextField anexoCq10C1;

    public Quadro10Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getAnexoCq10C1001_num() {
        return this.anexoCq10C1001_num;
    }

    public JMoneyTextField getAnexoCq10C1001() {
        return this.anexoCq10C1001;
    }

    public JLabelTextFieldNumbering getAnexoCq10C1002_num() {
        return this.anexoCq10C1002_num;
    }

    public JMoneyTextField getAnexoCq10C1002() {
        return this.anexoCq10C1002;
    }

    public JMoneyTextField getAnexoCq10C1() {
        return this.anexoCq10C1;
    }

    public JMoneyTextField getAnexoCq10C1003() {
        return this.anexoCq10C1003;
    }

    public JMoneyTextField getAnexoCq10C1004() {
        return this.anexoCq10C1004;
    }

    public JMoneyTextField getAnexoCq10C1005() {
        return this.anexoCq10C1005;
    }

    public JLabelTextFieldNumbering getAnexoCq10C1003_num() {
        return this.anexoCq10C1003_num;
    }

    public JLabelTextFieldNumbering getAnexoCq10C1004_num() {
        return this.anexoCq10C1004_num;
    }

    public JLabelTextFieldNumbering getAnexoCq10C1005_num() {
        return this.anexoCq10C1005_num;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoCq10C1001_label() {
        return this.anexoCq10C1001_label;
    }

    public JLabel getAnexoCq10C1002_label() {
        return this.anexoCq10C1002_label;
    }

    public JLabel getAnexoCq10C1003_label() {
        return this.anexoCq10C1003_label;
    }

    public JLabel getAnexoCq10C1004_label() {
        return this.anexoCq10C1004_label;
    }

    public JLabel getAnexoCq10C1005_label() {
        return this.anexoCq10C1005_label;
    }

    public JLabel getAnexoCq10C1_label() {
        return this.anexoCq10C1_label;
    }

    public JLabel getAnexoCq10C1006_label() {
        return this.anexoCq10C1006_label;
    }

    public JLabelTextFieldNumbering getAnexoCq10C1006_num() {
        return this.anexoCq10C1006_num;
    }

    public JMoneyTextField getAnexoCq10C1006() {
        return this.anexoCq10C1006;
    }

    public JLabel getAnexoCq10C1007_label() {
        return this.anexoCq10C1007_label;
    }

    public JLabelTextFieldNumbering getAnexoCq10C1007_num() {
        return this.anexoCq10C1007_num;
    }

    public JMoneyTextField getAnexoCq10C1007() {
        return this.anexoCq10C1007;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoCq10C1001_label = new JLabel();
        this.anexoCq10C1001_num = new JLabelTextFieldNumbering();
        this.anexoCq10C1001 = new JMoneyTextField();
        this.anexoCq10C1002_label = new JLabel();
        this.anexoCq10C1002_num = new JLabelTextFieldNumbering();
        this.anexoCq10C1002 = new JMoneyTextField();
        this.anexoCq10C1006_label = new JLabel();
        this.anexoCq10C1006_num = new JLabelTextFieldNumbering();
        this.anexoCq10C1006 = new JMoneyTextField();
        this.anexoCq10C1003_label = new JLabel();
        this.anexoCq10C1003_num = new JLabelTextFieldNumbering();
        this.anexoCq10C1003 = new JMoneyTextField();
        this.anexoCq10C1007_label = new JLabel();
        this.anexoCq10C1007_num = new JLabelTextFieldNumbering();
        this.anexoCq10C1007 = new JMoneyTextField();
        this.anexoCq10C1004_label = new JLabel();
        this.anexoCq10C1004_num = new JLabelTextFieldNumbering();
        this.anexoCq10C1004 = new JMoneyTextField();
        this.anexoCq10C1005_label = new JLabel();
        this.anexoCq10C1005_num = new JLabelTextFieldNumbering();
        this.anexoCq10C1005 = new JMoneyTextField();
        this.anexoCq10C1_label = new JLabel();
        this.anexoCq10C1 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro10Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro10Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "$rgap, default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("$ugap, $rgap, default:grow, $lcgap, 25dlu, 0px, 75dlu, $rgap", "8*(default, $lgap), default"));
        this.anexoCq10C1001_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1001_label.text"));
        this.panel2.add((Component)this.anexoCq10C1001_label, cc.xy(3, 1));
        this.anexoCq10C1001_num.setText(bundle.getString("Quadro10Panel.anexoCq10C1001_num.text"));
        this.anexoCq10C1001_num.setBorder(new EtchedBorder());
        this.anexoCq10C1001_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq10C1001_num, cc.xy(5, 1));
        this.panel2.add((Component)this.anexoCq10C1001, cc.xy(7, 1));
        this.anexoCq10C1002_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1002_label.text"));
        this.panel2.add((Component)this.anexoCq10C1002_label, cc.xy(3, 3));
        this.anexoCq10C1002_num.setText(bundle.getString("Quadro10Panel.anexoCq10C1002_num.text"));
        this.anexoCq10C1002_num.setBorder(new EtchedBorder());
        this.anexoCq10C1002_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq10C1002_num, cc.xy(5, 3));
        this.panel2.add((Component)this.anexoCq10C1002, cc.xy(7, 3));
        this.anexoCq10C1006_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1006_label.text"));
        this.panel2.add((Component)this.anexoCq10C1006_label, cc.xy(3, 5));
        this.anexoCq10C1006_num.setText(bundle.getString("Quadro10Panel.anexoCq10C1006_num.text"));
        this.anexoCq10C1006_num.setBorder(new EtchedBorder());
        this.anexoCq10C1006_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq10C1006_num, cc.xy(5, 5));
        this.panel2.add((Component)this.anexoCq10C1006, cc.xy(7, 5));
        this.anexoCq10C1003_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1003_label.text"));
        this.panel2.add((Component)this.anexoCq10C1003_label, cc.xy(3, 7));
        this.anexoCq10C1003_num.setText(bundle.getString("Quadro10Panel.anexoCq10C1003_num.text"));
        this.anexoCq10C1003_num.setBorder(new EtchedBorder());
        this.anexoCq10C1003_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq10C1003_num, cc.xy(5, 7));
        this.panel2.add((Component)this.anexoCq10C1003, cc.xy(7, 7));
        this.anexoCq10C1007_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1007_label.text"));
        this.panel2.add((Component)this.anexoCq10C1007_label, cc.xy(3, 9));
        this.anexoCq10C1007_num.setText(bundle.getString("Quadro10Panel.anexoCq10C1007_num.text"));
        this.anexoCq10C1007_num.setBorder(new EtchedBorder());
        this.anexoCq10C1007_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq10C1007_num, cc.xy(5, 9));
        this.panel2.add((Component)this.anexoCq10C1007, cc.xy(7, 9));
        this.anexoCq10C1004_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1004_label.text"));
        this.panel2.add((Component)this.anexoCq10C1004_label, cc.xy(3, 11));
        this.anexoCq10C1004_num.setText(bundle.getString("Quadro10Panel.anexoCq10C1004_num.text"));
        this.anexoCq10C1004_num.setBorder(new EtchedBorder());
        this.anexoCq10C1004_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq10C1004_num, cc.xy(5, 11));
        this.panel2.add((Component)this.anexoCq10C1004, cc.xy(7, 11));
        this.anexoCq10C1005_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1005_label.text"));
        this.panel2.add((Component)this.anexoCq10C1005_label, cc.xy(3, 13));
        this.anexoCq10C1005_num.setText(bundle.getString("Quadro10Panel.anexoCq10C1005_num.text"));
        this.anexoCq10C1005_num.setBorder(new EtchedBorder());
        this.anexoCq10C1005_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq10C1005_num, cc.xy(5, 13));
        this.panel2.add((Component)this.anexoCq10C1005, cc.xy(7, 13));
        this.anexoCq10C1_label.setText(bundle.getString("Quadro10Panel.anexoCq10C1_label.text"));
        this.anexoCq10C1_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoCq10C1_label, cc.xywh(3, 15, 3, 1));
        this.anexoCq10C1.setColumns(13);
        this.anexoCq10C1.setEditable(false);
        this.panel2.add((Component)this.anexoCq10C1, cc.xy(7, 15));
        this.this2.add((Component)this.panel2, cc.xy(1, 2));
        this.add((Component)this.this2, "Center");
    }
}

