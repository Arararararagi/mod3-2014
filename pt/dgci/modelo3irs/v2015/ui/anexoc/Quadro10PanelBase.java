/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro10Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro10PanelBase
extends Quadro10Panel
implements IBindablePanel<Quadro10> {
    private static final long serialVersionUID = 1;
    protected Quadro10 model;

    @Override
    public abstract void setModel(Quadro10 var1, boolean var2);

    @Override
    public Quadro10 getModel() {
        return this.model;
    }

    public Quadro10PanelBase(Quadro10 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

