/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro14Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel anexoCq14B1OPSim_base_label;
    protected JLabelTextFieldNumbering label22;
    protected JRadioButton anexoCq14B1OP1;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton anexoCq14B1OP2;
    protected JLabel q07B1OPSim_base_label2;
    protected JLabel label10;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoCq14T1Scroll;
    protected JTable anexoCq14T1;

    public Quadro14Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoCq14B1OPSim_base_label() {
        return this.anexoCq14B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JRadioButton getAnexoCq14B1OP1() {
        return this.anexoCq14B1OP1;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getAnexoCq14B1OP2() {
        return this.anexoCq14B1OP2;
    }

    public JLabel getQ07B1OPSim_base_label2() {
        return this.q07B1OPSim_base_label2;
    }

    protected void addLineAnexoCq14T1_linhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoCq14T1_linhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoCq14T1Scroll() {
        return this.anexoCq14T1Scroll;
    }

    public JTable getAnexoCq14T1() {
        return this.anexoCq14T1;
    }

    protected void addLineAnexoCq14T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoCq14T1_LinhaActionPerformed(ActionEvent e) {
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.anexoCq14B1OPSim_base_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoCq14B1OP1 = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.anexoCq14B1OP2 = new JRadioButton();
        this.q07B1OPSim_base_label2 = new JLabel();
        this.label10 = new JLabel();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoCq14T1Scroll = new JScrollPane();
        this.anexoCq14T1 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro14Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro14Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $rgap", "3*(default, $lgap), default"));
        this.panel4.setLayout(new FormLayout("2*(default:grow, $lcgap), default, $lcgap, 13dlu, 0px, default, $rgap, default, $lcgap, 13dlu, 0px, default, $lcgap, default:grow", "$ugap, 4*($lgap, default)"));
        this.anexoCq14B1OPSim_base_label.setText(bundle.getString("Quadro14Panel.anexoCq14B1OPSim_base_label.text"));
        this.anexoCq14B1OPSim_base_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoCq14B1OPSim_base_label, cc.xy(3, 3));
        this.label22.setText(bundle.getString("Quadro14Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label22, cc.xy(7, 3));
        this.anexoCq14B1OP1.setText(bundle.getString("Quadro14Panel.anexoCq14B1OP1.text"));
        this.panel4.add((Component)this.anexoCq14B1OP1, cc.xy(9, 3));
        this.label23.setText(bundle.getString("Quadro14Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label23, cc.xy(13, 3));
        this.anexoCq14B1OP2.setText(bundle.getString("Quadro14Panel.anexoCq14B1OP2.text"));
        this.panel4.add((Component)this.anexoCq14B1OP2, cc.xy(15, 3));
        this.q07B1OPSim_base_label2.setText(bundle.getString("Quadro14Panel.q07B1OPSim_base_label2.text"));
        this.q07B1OPSim_base_label2.setHorizontalAlignment(2);
        this.panel4.add((Component)this.q07B1OPSim_base_label2, cc.xywh(1, 7, 3, 1));
        this.this2.add((Component)this.panel4, cc.xy(2, 1));
        this.label10.setText(bundle.getString("Quadro14Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.this2.add((Component)this.label10, cc.xy(2, 3));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 300dlu:grow, $rgap, default:grow", "$rgap, 2*(default, $lgap), fill:174dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro14Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro14Panel.this.addLineAnexoCq14T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro14Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro14Panel.this.removeLineAnexoCq14T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 4));
        this.anexoCq14T1Scroll.setViewportView(this.anexoCq14T1);
        this.panel3.add((Component)this.anexoCq14T1Scroll, cc.xywh(3, 6, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 5));
        this.add((Component)this.this2, "Center");
    }

}

