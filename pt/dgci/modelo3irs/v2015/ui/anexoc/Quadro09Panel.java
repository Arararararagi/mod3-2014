/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro09Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JLabel label2;
    protected JPanel panel2;
    protected JLabel anexoCq09C901_label;
    protected JLabelTextFieldNumbering anexoCq09C901_num;
    protected JNIFTextField anexoCq09C901;
    protected JPanel panel4;
    protected JLabel anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label;
    protected JPanel panel7;
    protected JLabel anexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label;
    protected JPanel panel3;
    protected JLabel anexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label;
    protected JLabelTextFieldNumbering anexoCq09C902_num;
    protected JLimitedTextField anexoCq09C902;
    protected JLabelTextFieldNumbering anexoCq09C908_num;
    protected JMoneyTextField anexoCq09C908;
    protected JLabelTextFieldNumbering anexoCq09C914_num;
    protected JMoneyTextField anexoCq09C914;
    protected JLabelTextFieldNumbering anexoCq09C903_num;
    protected JLimitedTextField anexoCq09C903;
    protected JLabelTextFieldNumbering anexoCq09C909_num;
    protected JMoneyTextField anexoCq09C909;
    protected JLabelTextFieldNumbering anexoCq09C915_num;
    protected JMoneyTextField anexoCq09C915;
    protected JLabelTextFieldNumbering anexoCq09C904_num;
    protected JLimitedTextField anexoCq09C904;
    protected JLabelTextFieldNumbering anexoCq09C910_num;
    protected JMoneyTextField anexoCq09C910;
    protected JLabelTextFieldNumbering anexoCq09C916_num;
    protected JMoneyTextField anexoCq09C916;
    protected JLabelTextFieldNumbering anexoCq09C905_num;
    protected JLimitedTextField anexoCq09C905;
    protected JLabelTextFieldNumbering anexoCq09C911_num;
    protected JMoneyTextField anexoCq09C911;
    protected JLabelTextFieldNumbering anexoCq09C917_num;
    protected JMoneyTextField anexoCq09C917;
    protected JLabelTextFieldNumbering anexoCq09C906_num;
    protected JLimitedTextField anexoCq09C906;
    protected JLabelTextFieldNumbering anexoCq09C912_num;
    protected JMoneyTextField anexoCq09C912;
    protected JLabelTextFieldNumbering anexoCq09C918_num;
    protected JMoneyTextField anexoCq09C918;
    protected JLabelTextFieldNumbering anexoCq09C907_num;
    protected JLimitedTextField anexoCq09C907;
    protected JLabelTextFieldNumbering anexoCq09C913_num;
    protected JMoneyTextField anexoCq09C913;
    protected JLabelTextFieldNumbering anexoCq09C919_num;
    protected JMoneyTextField anexoCq09C919;

    public Quadro09Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoCq09C901_label() {
        return this.anexoCq09C901_label;
    }

    public JLabelTextFieldNumbering getAnexoCq09C901_num() {
        return this.anexoCq09C901_num;
    }

    public JNIFTextField getAnexoCq09C901() {
        return this.anexoCq09C901;
    }

    public JLimitedTextField getAnexoCq09C902() {
        return this.anexoCq09C902;
    }

    public JMoneyTextField getAnexoCq09C908() {
        return this.anexoCq09C908;
    }

    public JMoneyTextField getAnexoCq09C914() {
        return this.anexoCq09C914;
    }

    public JLimitedTextField getAnexoCq09C903() {
        return this.anexoCq09C903;
    }

    public JMoneyTextField getAnexoCq09C909() {
        return this.anexoCq09C909;
    }

    public JMoneyTextField getAnexoCq09C915() {
        return this.anexoCq09C915;
    }

    public JLimitedTextField getAnexoCq09C904() {
        return this.anexoCq09C904;
    }

    public JMoneyTextField getAnexoCq09C910() {
        return this.anexoCq09C910;
    }

    public JMoneyTextField getAnexoCq09C916() {
        return this.anexoCq09C916;
    }

    public JLimitedTextField getAnexoCq09C905() {
        return this.anexoCq09C905;
    }

    public JMoneyTextField getAnexoCq09C911() {
        return this.anexoCq09C911;
    }

    public JMoneyTextField getAnexoCq09C917() {
        return this.anexoCq09C917;
    }

    public JLimitedTextField getAnexoCq09C906() {
        return this.anexoCq09C906;
    }

    public JMoneyTextField getAnexoCq09C912() {
        return this.anexoCq09C912;
    }

    public JMoneyTextField getAnexoCq09C918() {
        return this.anexoCq09C918;
    }

    public JLimitedTextField getAnexoCq09C907() {
        return this.anexoCq09C907;
    }

    public JMoneyTextField getAnexoCq09C913() {
        return this.anexoCq09C913;
    }

    public JMoneyTextField getAnexoCq09C919() {
        return this.anexoCq09C919;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label() {
        return this.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label() {
        return this.anexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label;
    }

    public JLabel getAnexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label() {
        return this.anexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label;
    }

    public JLabelTextFieldNumbering getAnexoCq09C902_num() {
        return this.anexoCq09C902_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C908_num() {
        return this.anexoCq09C908_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C914_num() {
        return this.anexoCq09C914_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C903_num() {
        return this.anexoCq09C903_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C909_num() {
        return this.anexoCq09C909_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C915_num() {
        return this.anexoCq09C915_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C904_num() {
        return this.anexoCq09C904_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C910_num() {
        return this.anexoCq09C910_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C916_num() {
        return this.anexoCq09C916_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C905_num() {
        return this.anexoCq09C905_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C911_num() {
        return this.anexoCq09C911_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C917_num() {
        return this.anexoCq09C917_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C906_num() {
        return this.anexoCq09C906_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C912_num() {
        return this.anexoCq09C912_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C918_num() {
        return this.anexoCq09C918_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C907_num() {
        return this.anexoCq09C907_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C913_num() {
        return this.anexoCq09C913_num;
    }

    public JLabelTextFieldNumbering getAnexoCq09C919_num() {
        return this.anexoCq09C919_num;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.label2 = new JLabel();
        this.panel2 = new JPanel();
        this.anexoCq09C901_label = new JLabel();
        this.anexoCq09C901_num = new JLabelTextFieldNumbering();
        this.anexoCq09C901 = new JNIFTextField();
        this.panel4 = new JPanel();
        this.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label = new JLabel();
        this.panel7 = new JPanel();
        this.anexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label = new JLabel();
        this.panel3 = new JPanel();
        this.anexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label = new JLabel();
        this.anexoCq09C902_num = new JLabelTextFieldNumbering();
        this.anexoCq09C902 = new JLimitedTextField();
        this.anexoCq09C908_num = new JLabelTextFieldNumbering();
        this.anexoCq09C908 = new JMoneyTextField();
        this.anexoCq09C914_num = new JLabelTextFieldNumbering();
        this.anexoCq09C914 = new JMoneyTextField();
        this.anexoCq09C903_num = new JLabelTextFieldNumbering();
        this.anexoCq09C903 = new JLimitedTextField();
        this.anexoCq09C909_num = new JLabelTextFieldNumbering();
        this.anexoCq09C909 = new JMoneyTextField();
        this.anexoCq09C915_num = new JLabelTextFieldNumbering();
        this.anexoCq09C915 = new JMoneyTextField();
        this.anexoCq09C904_num = new JLabelTextFieldNumbering();
        this.anexoCq09C904 = new JLimitedTextField();
        this.anexoCq09C910_num = new JLabelTextFieldNumbering();
        this.anexoCq09C910 = new JMoneyTextField();
        this.anexoCq09C916_num = new JLabelTextFieldNumbering();
        this.anexoCq09C916 = new JMoneyTextField();
        this.anexoCq09C905_num = new JLabelTextFieldNumbering();
        this.anexoCq09C905 = new JLimitedTextField();
        this.anexoCq09C911_num = new JLabelTextFieldNumbering();
        this.anexoCq09C911 = new JMoneyTextField();
        this.anexoCq09C917_num = new JLabelTextFieldNumbering();
        this.anexoCq09C917 = new JMoneyTextField();
        this.anexoCq09C906_num = new JLabelTextFieldNumbering();
        this.anexoCq09C906 = new JLimitedTextField();
        this.anexoCq09C912_num = new JLabelTextFieldNumbering();
        this.anexoCq09C912 = new JMoneyTextField();
        this.anexoCq09C918_num = new JLabelTextFieldNumbering();
        this.anexoCq09C918 = new JMoneyTextField();
        this.anexoCq09C907_num = new JLabelTextFieldNumbering();
        this.anexoCq09C907 = new JLimitedTextField();
        this.anexoCq09C913_num = new JLabelTextFieldNumbering();
        this.anexoCq09C913 = new JMoneyTextField();
        this.anexoCq09C919_num = new JLabelTextFieldNumbering();
        this.anexoCq09C919 = new JMoneyTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setBorder(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro09Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro09Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "$ugap, 2*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro09Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.this2.add((Component)this.label2, CC.xy(1, 2));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, default, $rgap, 25dlu, 0px, 43dlu, $lcgap, default:grow", "$ugap, 2*($lgap, default)"));
        this.anexoCq09C901_label.setText(bundle.getString("Quadro09Panel.anexoCq09C901_label.text"));
        this.panel2.add((Component)this.anexoCq09C901_label, CC.xy(3, 3));
        this.anexoCq09C901_num.setText(bundle.getString("Quadro09Panel.anexoCq09C901_num.text"));
        this.anexoCq09C901_num.setBorder(new EtchedBorder());
        this.anexoCq09C901_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoCq09C901_num, CC.xy(5, 3));
        this.anexoCq09C901.setColumns(9);
        this.panel2.add((Component)this.anexoCq09C901, CC.xy(7, 3));
        this.this2.add((Component)this.panel2, CC.xy(1, 4));
        this.panel4.setPreferredSize(null);
        this.panel4.setMinimumSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, 22dlu, $lcgap, 3dlu, 0px, 25dlu, 0px, 75dlu, 0px, 3dlu, $lcgap, 25dlu, 0px, 75dlu, $lcgap, default:grow, 0px", "$ugap, $lgap, fill:30dlu, 7*($lgap, default)"));
        this.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label.setText(bundle.getString("Quadro09Panel.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label.text"));
        this.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label.setBorder(new EtchedBorder());
        this.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label.setHorizontalAlignment(0);
        this.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label.setHorizontalTextPosition(0);
        this.panel4.add((Component)this.anexoCq09C902_label\u00a3anexoCq09C903_label\u00a3anexoCq09C904_label\u00a3anexoCq09C905_label\u00a3anexoCq09C906_label\u00a3anexoCq09C907_label, CC.xywh(3, 3, 3, 1));
        this.panel7.setBorder(new EtchedBorder());
        this.panel7.setLayout(new FormLayout("105dlu", "$rgap, default"));
        this.anexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label.setText(bundle.getString("Quadro09Panel.anexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label.text"));
        this.anexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoCq09C908_label\u00a3anexoCq09C909_label\u00a3anexoCq09C910_label\u00a3anexoCq09C911_label\u00a3anexoCq09C912_label\u00a3anexoCq09C913_label, CC.xy(1, 2));
        this.panel4.add((Component)this.panel7, CC.xywh(7, 3, 7, 1));
        this.panel3.setBorder(new EtchedBorder());
        this.panel3.setLayout(new FormLayout("99dlu", "$rgap, default"));
        this.anexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label.setText(bundle.getString("Quadro09Panel.anexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label.text"));
        this.anexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoCq09C914_label\u00a3anexoCq09C915_label\u00a3anexoCq09C916_label\u00a3anexoCq09C917_label\u00a3anexoCq09C918_label\u00a3anexoCq09C919_label, CC.xy(1, 2));
        this.panel4.add((Component)this.panel3, CC.xywh(15, 3, 3, 1));
        this.anexoCq09C902_num.setText(bundle.getString("Quadro09Panel.anexoCq09C902_num.text"));
        this.anexoCq09C902_num.setBorder(new EtchedBorder());
        this.anexoCq09C902_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C902_num, CC.xy(3, 5));
        this.anexoCq09C902.setMaxLength(4);
        this.anexoCq09C902.setOnlyNumeric(true);
        this.anexoCq09C902.setColumns(3);
        this.panel4.add((Component)this.anexoCq09C902, CC.xy(5, 5));
        this.anexoCq09C908_num.setText(bundle.getString("Quadro09Panel.anexoCq09C908_num.text"));
        this.anexoCq09C908_num.setBorder(new EtchedBorder());
        this.anexoCq09C908_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C908_num, CC.xy(9, 5));
        this.panel4.add((Component)this.anexoCq09C908, CC.xy(11, 5));
        this.anexoCq09C914_num.setText(bundle.getString("Quadro09Panel.anexoCq09C914_num.text"));
        this.anexoCq09C914_num.setBorder(new EtchedBorder());
        this.anexoCq09C914_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C914_num, CC.xy(15, 5));
        this.panel4.add((Component)this.anexoCq09C914, CC.xy(17, 5));
        this.anexoCq09C903_num.setText(bundle.getString("Quadro09Panel.anexoCq09C903_num.text"));
        this.anexoCq09C903_num.setBorder(new EtchedBorder());
        this.anexoCq09C903_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C903_num, CC.xy(3, 7));
        this.anexoCq09C903.setMaxLength(4);
        this.anexoCq09C903.setOnlyNumeric(true);
        this.anexoCq09C903.setColumns(3);
        this.panel4.add((Component)this.anexoCq09C903, CC.xy(5, 7));
        this.anexoCq09C909_num.setText(bundle.getString("Quadro09Panel.anexoCq09C909_num.text"));
        this.anexoCq09C909_num.setBorder(new EtchedBorder());
        this.anexoCq09C909_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C909_num, CC.xy(9, 7));
        this.panel4.add((Component)this.anexoCq09C909, CC.xy(11, 7));
        this.anexoCq09C915_num.setText(bundle.getString("Quadro09Panel.anexoCq09C915_num.text"));
        this.anexoCq09C915_num.setBorder(new EtchedBorder());
        this.anexoCq09C915_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C915_num, CC.xy(15, 7));
        this.panel4.add((Component)this.anexoCq09C915, CC.xy(17, 7));
        this.anexoCq09C904_num.setText(bundle.getString("Quadro09Panel.anexoCq09C904_num.text"));
        this.anexoCq09C904_num.setBorder(new EtchedBorder());
        this.anexoCq09C904_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C904_num, CC.xy(3, 9));
        this.anexoCq09C904.setMaxLength(4);
        this.anexoCq09C904.setOnlyNumeric(true);
        this.anexoCq09C904.setColumns(3);
        this.panel4.add((Component)this.anexoCq09C904, CC.xy(5, 9));
        this.anexoCq09C910_num.setText(bundle.getString("Quadro09Panel.anexoCq09C910_num.text"));
        this.anexoCq09C910_num.setBorder(new EtchedBorder());
        this.anexoCq09C910_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C910_num, CC.xy(9, 9));
        this.panel4.add((Component)this.anexoCq09C910, CC.xy(11, 9));
        this.anexoCq09C916_num.setText(bundle.getString("Quadro09Panel.anexoCq09C916_num.text"));
        this.anexoCq09C916_num.setBorder(new EtchedBorder());
        this.anexoCq09C916_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C916_num, CC.xy(15, 9));
        this.panel4.add((Component)this.anexoCq09C916, CC.xy(17, 9));
        this.anexoCq09C905_num.setText(bundle.getString("Quadro09Panel.anexoCq09C905_num.text"));
        this.anexoCq09C905_num.setBorder(new EtchedBorder());
        this.anexoCq09C905_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C905_num, CC.xy(3, 11));
        this.anexoCq09C905.setMaxLength(4);
        this.anexoCq09C905.setOnlyNumeric(true);
        this.anexoCq09C905.setColumns(3);
        this.panel4.add((Component)this.anexoCq09C905, CC.xy(5, 11));
        this.anexoCq09C911_num.setText(bundle.getString("Quadro09Panel.anexoCq09C911_num.text"));
        this.anexoCq09C911_num.setBorder(new EtchedBorder());
        this.anexoCq09C911_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C911_num, CC.xy(9, 11));
        this.panel4.add((Component)this.anexoCq09C911, CC.xy(11, 11));
        this.anexoCq09C917_num.setText(bundle.getString("Quadro09Panel.anexoCq09C917_num.text"));
        this.anexoCq09C917_num.setBorder(new EtchedBorder());
        this.anexoCq09C917_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C917_num, CC.xy(15, 11));
        this.panel4.add((Component)this.anexoCq09C917, CC.xy(17, 11));
        this.anexoCq09C906_num.setText(bundle.getString("Quadro09Panel.anexoCq09C906_num.text"));
        this.anexoCq09C906_num.setBorder(new EtchedBorder());
        this.anexoCq09C906_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C906_num, CC.xy(3, 13));
        this.anexoCq09C906.setMaxLength(4);
        this.anexoCq09C906.setOnlyNumeric(true);
        this.anexoCq09C906.setColumns(3);
        this.panel4.add((Component)this.anexoCq09C906, CC.xy(5, 13));
        this.anexoCq09C912_num.setText(bundle.getString("Quadro09Panel.anexoCq09C912_num.text"));
        this.anexoCq09C912_num.setBorder(new EtchedBorder());
        this.anexoCq09C912_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C912_num, CC.xy(9, 13));
        this.panel4.add((Component)this.anexoCq09C912, CC.xy(11, 13));
        this.anexoCq09C918_num.setText(bundle.getString("Quadro09Panel.anexoCq09C918_num.text"));
        this.anexoCq09C918_num.setBorder(new EtchedBorder());
        this.anexoCq09C918_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C918_num, CC.xy(15, 13));
        this.panel4.add((Component)this.anexoCq09C918, CC.xy(17, 13));
        this.anexoCq09C907_num.setText(bundle.getString("Quadro09Panel.anexoCq09C907_num.text"));
        this.anexoCq09C907_num.setBorder(new EtchedBorder());
        this.anexoCq09C907_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C907_num, CC.xy(3, 15));
        this.anexoCq09C907.setMaxLength(4);
        this.anexoCq09C907.setOnlyNumeric(true);
        this.anexoCq09C907.setColumns(3);
        this.panel4.add((Component)this.anexoCq09C907, CC.xy(5, 15));
        this.anexoCq09C913_num.setText(bundle.getString("Quadro09Panel.anexoCq09C913_num.text"));
        this.anexoCq09C913_num.setBorder(new EtchedBorder());
        this.anexoCq09C913_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C913_num, CC.xy(9, 15));
        this.panel4.add((Component)this.anexoCq09C913, CC.xy(11, 15));
        this.anexoCq09C919_num.setText(bundle.getString("Quadro09Panel.anexoCq09C919_num.text"));
        this.anexoCq09C919_num.setBorder(new EtchedBorder());
        this.anexoCq09C919_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoCq09C919_num, CC.xy(15, 15));
        this.panel4.add((Component)this.anexoCq09C919, CC.xy(17, 15));
        this.this2.add((Component)this.panel4, CC.xy(1, 6));
        this.add((Component)this.this2, "Center");
    }
}

