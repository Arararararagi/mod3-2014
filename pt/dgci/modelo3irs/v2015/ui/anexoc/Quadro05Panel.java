/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel6;
    protected JLabel anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label;
    protected JLabel anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label;
    protected JLabel anexoCq05C501_base_label\u00a3anexoCq05C503_base_label;
    protected JLabelTextFieldNumbering anexoCq05C501_num;
    protected JMoneyTextField anexoCq05C501;
    protected JLabelTextFieldNumbering anexoCq05C503_num;
    protected JMoneyTextField anexoCq05C503;
    protected JLabel anexoCq05C502_base_label\u00a3anexoCq05C504_base_label;
    protected JLabelTextFieldNumbering anexoCq05C502_num;
    protected JMoneyTextField anexoCq05C502;
    protected JLabelTextFieldNumbering anexoCq05C504_num;
    protected JMoneyTextField anexoCq05C504;
    protected JLabel anexoCq05C505_base_label\u00a3anexoCq05C506_base_label;
    protected JLabelTextFieldNumbering anexoCq05C505_num;
    protected JMoneyTextField anexoCq05C505;
    protected JLabelTextFieldNumbering anexoCq05C506_num;
    protected JMoneyTextField anexoCq05C506;

    public Quadro05Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getAnexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label() {
        return this.anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label;
    }

    public JLabel getAnexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label() {
        return this.anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label;
    }

    public JLabel getAnexoCq05C501_base_label\u00a3anexoCq05C503_base_label() {
        return this.anexoCq05C501_base_label\u00a3anexoCq05C503_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq05C501_num() {
        return this.anexoCq05C501_num;
    }

    public JMoneyTextField getAnexoCq05C501() {
        return this.anexoCq05C501;
    }

    public JLabelTextFieldNumbering getAnexoCq05C503_num() {
        return this.anexoCq05C503_num;
    }

    public JMoneyTextField getAnexoCq05C503() {
        return this.anexoCq05C503;
    }

    public JLabel getAnexoCq05C502_base_label\u00a3anexoCq05C504_base_label() {
        return this.anexoCq05C502_base_label\u00a3anexoCq05C504_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq05C502_num() {
        return this.anexoCq05C502_num;
    }

    public JMoneyTextField getAnexoCq05C502() {
        return this.anexoCq05C502;
    }

    public JLabelTextFieldNumbering getAnexoCq05C504_num() {
        return this.anexoCq05C504_num;
    }

    public JMoneyTextField getAnexoCq05C504() {
        return this.anexoCq05C504;
    }

    public JLabel getAnexoCq05C505_base_label\u00a3anexoCq05C506_base_label() {
        return this.anexoCq05C505_base_label\u00a3anexoCq05C506_base_label;
    }

    public JLabelTextFieldNumbering getAnexoCq05C505_num() {
        return this.anexoCq05C505_num;
    }

    public JMoneyTextField getAnexoCq05C505() {
        return this.anexoCq05C505;
    }

    public JLabelTextFieldNumbering getAnexoCq05C506_num() {
        return this.anexoCq05C506_num;
    }

    public JMoneyTextField getAnexoCq05C506() {
        return this.anexoCq05C506;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoC");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel6 = new JPanel();
        this.anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label = new JLabel();
        this.anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label = new JLabel();
        this.anexoCq05C501_base_label\u00a3anexoCq05C503_base_label = new JLabel();
        this.anexoCq05C501_num = new JLabelTextFieldNumbering();
        this.anexoCq05C501 = new JMoneyTextField();
        this.anexoCq05C503_num = new JLabelTextFieldNumbering();
        this.anexoCq05C503 = new JMoneyTextField();
        this.anexoCq05C502_base_label\u00a3anexoCq05C504_base_label = new JLabel();
        this.anexoCq05C502_num = new JLabelTextFieldNumbering();
        this.anexoCq05C502 = new JMoneyTextField();
        this.anexoCq05C504_num = new JLabelTextFieldNumbering();
        this.anexoCq05C504 = new JMoneyTextField();
        this.anexoCq05C505_base_label\u00a3anexoCq05C506_base_label = new JLabel();
        this.anexoCq05C505_num = new JLabelTextFieldNumbering();
        this.anexoCq05C505 = new JMoneyTextField();
        this.anexoCq05C506_num = new JLabelTextFieldNumbering();
        this.anexoCq05C506 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setBorder(LineBorder.createBlackLineBorder());
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "2*(default, $lgap), default"));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("$ugap, 200dlu:grow, $ugap, 25dlu, $rgap, 75dlu, $lcgap, default, $lcgap, 25dlu, $lcgap, 75dlu, $rgap", "4*(default, $lgap), default"));
        this.anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label.setText(bundle.getString("Quadro05Panel.anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label.text"));
        this.anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label.setBorder(new EtchedBorder());
        this.anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C501_label\u00a3anexoCq05C502_label\u00a3anexoCq05C505_label, cc.xywh(4, 1, 3, 1));
        this.anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label.setText(bundle.getString("Quadro05Panel.anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label.text"));
        this.anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label.setBorder(new EtchedBorder());
        this.anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C503_label\u00a3anexoCq05C504_label\u00a3anexoCq05C506_label, cc.xywh(10, 1, 3, 1));
        this.anexoCq05C501_base_label\u00a3anexoCq05C503_base_label.setText(bundle.getString("Quadro05Panel.anexoCq05C501_base_label\u00a3anexoCq05C503_base_label.text"));
        this.panel6.add((Component)this.anexoCq05C501_base_label\u00a3anexoCq05C503_base_label, cc.xy(2, 3));
        this.anexoCq05C501_num.setText(bundle.getString("Quadro05Panel.anexoCq05C501_num.text"));
        this.anexoCq05C501_num.setBorder(new EtchedBorder());
        this.anexoCq05C501_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C501_num, cc.xy(4, 3));
        this.anexoCq05C501.setColumns(14);
        this.panel6.add((Component)this.anexoCq05C501, cc.xy(6, 3));
        this.anexoCq05C503_num.setText(bundle.getString("Quadro05Panel.anexoCq05C503_num.text"));
        this.anexoCq05C503_num.setBorder(new EtchedBorder());
        this.anexoCq05C503_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C503_num, cc.xy(10, 3));
        this.anexoCq05C503.setColumns(14);
        this.panel6.add((Component)this.anexoCq05C503, cc.xy(12, 3));
        this.anexoCq05C502_base_label\u00a3anexoCq05C504_base_label.setText(bundle.getString("Quadro05Panel.anexoCq05C502_base_label\u00a3anexoCq05C504_base_label.text"));
        this.panel6.add((Component)this.anexoCq05C502_base_label\u00a3anexoCq05C504_base_label, cc.xy(2, 5));
        this.anexoCq05C502_num.setText(bundle.getString("Quadro05Panel.anexoCq05C502_num.text"));
        this.anexoCq05C502_num.setBorder(new EtchedBorder());
        this.anexoCq05C502_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C502_num, cc.xy(4, 5));
        this.anexoCq05C502.setColumns(14);
        this.panel6.add((Component)this.anexoCq05C502, cc.xy(6, 5));
        this.anexoCq05C504_num.setText(bundle.getString("Quadro05Panel.anexoCq05C504_num.text"));
        this.anexoCq05C504_num.setBorder(new EtchedBorder());
        this.anexoCq05C504_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C504_num, cc.xy(10, 5));
        this.anexoCq05C504.setColumns(14);
        this.panel6.add((Component)this.anexoCq05C504, cc.xy(12, 5));
        this.anexoCq05C505_base_label\u00a3anexoCq05C506_base_label.setText(bundle.getString("Quadro05Panel.anexoCq05C505_base_label\u00a3anexoCq05C506_base_label.text"));
        this.panel6.add((Component)this.anexoCq05C505_base_label\u00a3anexoCq05C506_base_label, cc.xy(2, 7));
        this.anexoCq05C505_num.setText(bundle.getString("Quadro05Panel.anexoCq05C505_num.text"));
        this.anexoCq05C505_num.setBorder(new EtchedBorder());
        this.anexoCq05C505_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C505_num, cc.xy(4, 7));
        this.anexoCq05C505.setColumns(14);
        this.panel6.add((Component)this.anexoCq05C505, cc.xy(6, 7));
        this.anexoCq05C506_num.setText(bundle.getString("Quadro05Panel.anexoCq05C506_num.text"));
        this.anexoCq05C506_num.setBorder(new EtchedBorder());
        this.anexoCq05C506_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoCq05C506_num, cc.xy(10, 7));
        this.anexoCq05C506.setColumns(14);
        this.panel6.add((Component)this.anexoCq05C506, cc.xy(12, 7));
        this.this2.add((Component)this.panel6, cc.xy(1, 3));
        this.add((Component)this.this2, "Center");
    }
}

