/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro11Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro11PanelBase
extends Quadro11Panel
implements IBindablePanel<Quadro11> {
    private static final long serialVersionUID = 1;
    protected Quadro11 model;

    @Override
    public abstract void setModel(Quadro11 var1, boolean var2);

    @Override
    public Quadro11 getModel() {
        return this.model;
    }

    public Quadro11PanelBase(Quadro11 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

