/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoc;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro14Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro14PanelBase
extends Quadro14Panel
implements IBindablePanel<Quadro14> {
    private static final long serialVersionUID = 1;
    protected Quadro14 model;

    @Override
    public abstract void setModel(Quadro14 var1, boolean var2);

    @Override
    public Quadro14 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoCq14T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoCq14T1().add(new AnexoCq14T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoCq14T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoCq14T1.getSelectedRow() != -1 ? this.anexoCq14T1.getSelectedRow() : this.anexoCq14T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoCq14T1().remove(selectedRow);
            }
        }
    }

    public Quadro14PanelBase(Quadro14 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

