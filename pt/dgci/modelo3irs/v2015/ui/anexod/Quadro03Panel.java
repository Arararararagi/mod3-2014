/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro03Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoDq03C04_label;
    protected JLabel label3;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField anexoDq03C04;
    protected JLabel anexoDq03C05_label;
    protected JLabel label6;
    protected JLabelTextFieldNumbering label7;
    protected JNIFTextField anexoDq03C05;
    protected JPanel panel3;
    protected JLabel label12;
    protected JLabel label13;
    protected JLabel anexoDq03C06_label;
    protected JLabelTextFieldNumbering label11;
    protected JNIFTextField anexoDq03C06;

    public Quadro03Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoDq03C04_label() {
        return this.anexoDq03C04_label;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getAnexoDq03C04() {
        return this.anexoDq03C04;
    }

    public JLabel getAnexoDq03C05_label() {
        return this.anexoDq03C05_label;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabelTextFieldNumbering getLabel7() {
        return this.label7;
    }

    public JNIFTextField getAnexoDq03C05() {
        return this.anexoDq03C05;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel12() {
        return this.label12;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JLabel getAnexoDq03C06_label() {
        return this.anexoDq03C06_label;
    }

    public JLabelTextFieldNumbering getLabel11() {
        return this.label11;
    }

    public JNIFTextField getAnexoDq03C06() {
        return this.anexoDq03C06;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoDq03C04_label = new JLabel();
        this.label3 = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoDq03C04 = new JNIFTextField();
        this.anexoDq03C05_label = new JLabel();
        this.label6 = new JLabel();
        this.label7 = new JLabelTextFieldNumbering();
        this.anexoDq03C05 = new JNIFTextField();
        this.panel3 = new JPanel();
        this.label12 = new JLabel();
        this.label13 = new JLabel();
        this.anexoDq03C06_label = new JLabel();
        this.label11 = new JLabelTextFieldNumbering();
        this.anexoDq03C06 = new JNIFTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro03Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro03Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "2*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("0px, default:grow, $lcgap, default, 15dlu, default, $rgap, 13dlu, 0px, 43dlu, $lcgap, default:grow, $lcgap, default, 15dlu, default, $lcgap, 13dlu, 0px, 43dlu, $lcgap, default:grow", "$rgap, default, $lgap, default"));
        this.anexoDq03C04_label.setText(bundle.getString("Quadro03Panel.anexoDq03C04_label.text"));
        this.panel2.add((Component)this.anexoDq03C04_label, CC.xy(4, 2));
        this.label3.setText(bundle.getString("Quadro03Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label3, CC.xy(6, 2));
        this.label4.setText(bundle.getString("Quadro03Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(8, 2));
        this.anexoDq03C04.setColumns(9);
        this.anexoDq03C04.setEditable(false);
        this.panel2.add((Component)this.anexoDq03C04, CC.xy(10, 2));
        this.anexoDq03C05_label.setText(bundle.getString("Quadro03Panel.anexoDq03C05_label.text"));
        this.panel2.add((Component)this.anexoDq03C05_label, CC.xy(14, 2));
        this.label6.setText(bundle.getString("Quadro03Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label6, CC.xy(16, 2));
        this.label7.setText(bundle.getString("Quadro03Panel.label7.text"));
        this.label7.setBorder(new EtchedBorder());
        this.label7.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label7, CC.xy(18, 2));
        this.anexoDq03C05.setColumns(9);
        this.anexoDq03C05.setEditable(false);
        this.panel2.add((Component)this.anexoDq03C05, CC.xy(20, 2));
        this.this2.add((Component)this.panel2, CC.xy(1, 1));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("$rgap, 15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:43dlu, $rgap, 0px, default:grow, $rgap", "3*(default, $lgap), default"));
        this.label12.setText(bundle.getString("Quadro03Panel.label12.text"));
        this.label12.setBorder(new EtchedBorder());
        this.label12.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label12, CC.xy(2, 1));
        this.label13.setText(bundle.getString("Quadro03Panel.label13.text"));
        this.label13.setHorizontalAlignment(0);
        this.label13.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label13, CC.xywh(4, 1, 10, 1));
        this.anexoDq03C06_label.setText(bundle.getString("Quadro03Panel.anexoDq03C06_label.text"));
        this.anexoDq03C06_label.setHorizontalAlignment(4);
        this.panel3.add((Component)this.anexoDq03C06_label, CC.xy(6, 5));
        this.label11.setText(bundle.getString("Quadro03Panel.label11.text"));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label11, CC.xy(8, 5));
        this.anexoDq03C06.setColumns(9);
        this.anexoDq03C06.setEditable(false);
        this.panel3.add((Component)this.anexoDq03C06, CC.xy(10, 5));
        this.this2.add((Component)this.panel3, CC.xy(1, 3));
        this.add((Component)this.this2, "Center");
    }
}

