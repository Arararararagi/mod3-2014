/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel label2;
    protected JPanel panel3;
    protected JLabel anexoDq07C701_label;
    protected JLabelTextFieldNumbering anexoDq07C701_num;
    protected JNIFTextField anexoDq07C701;
    protected JPanel panel5;
    protected JPanel panel6;
    protected JLabel anexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label;
    protected JPanel panel7;
    protected JLabel anexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label;
    protected JPanel panel2;
    protected JLabel anexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label;
    protected JLabelTextFieldNumbering anexoDq07C702_num;
    protected JLimitedTextField anexoDq07C702;
    protected JLabelTextFieldNumbering anexoDq07C708_num;
    protected JMoneyTextField anexoDq07C708;
    protected JLabelTextFieldNumbering anexoDq07C714_num;
    protected JMoneyTextField anexoDq07C714;
    protected JLabelTextFieldNumbering anexoDq07C703_num;
    protected JLimitedTextField anexoDq07C703;
    protected JLabelTextFieldNumbering anexoDq07C709_num;
    protected JMoneyTextField anexoDq07C709;
    protected JLabelTextFieldNumbering anexoDq07C715_num;
    protected JMoneyTextField anexoDq07C715;
    protected JLabelTextFieldNumbering anexoDq07C704_num;
    protected JLimitedTextField anexoDq07C704;
    protected JLabelTextFieldNumbering anexoDq07C710_num;
    protected JMoneyTextField anexoDq07C710;
    protected JLabelTextFieldNumbering anexoDq07C716_num;
    protected JMoneyTextField anexoDq07C716;
    protected JLabelTextFieldNumbering anexoDq07C705_num;
    protected JLimitedTextField anexoDq07C705;
    protected JLabelTextFieldNumbering anexoDq07C711_num;
    protected JMoneyTextField anexoDq07C711;
    protected JLabelTextFieldNumbering anexoDq07C717_num;
    protected JMoneyTextField anexoDq07C717;
    protected JLabelTextFieldNumbering anexoDq07C706_num;
    protected JLimitedTextField anexoDq07C706;
    protected JLabelTextFieldNumbering anexoDq07C712_num;
    protected JMoneyTextField anexoDq07C712;
    protected JLabelTextFieldNumbering anexoDq07C718_num;
    protected JMoneyTextField anexoDq07C718;
    protected JLabelTextFieldNumbering anexoDq07C707_num;
    protected JLimitedTextField anexoDq07C707;
    protected JLabelTextFieldNumbering anexoDq07C713_num;
    protected JMoneyTextField anexoDq07C713;
    protected JLabelTextFieldNumbering anexoDq07C719_num;
    protected JMoneyTextField anexoDq07C719;

    public Quadro07Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoDq07C701_label() {
        return this.anexoDq07C701_label;
    }

    public JLabelTextFieldNumbering getAnexoDq07C701_num() {
        return this.anexoDq07C701_num;
    }

    public JNIFTextField getAnexoDq07C701() {
        return this.anexoDq07C701;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabelTextFieldNumbering getAnexoDq07C702_num() {
        return this.anexoDq07C702_num;
    }

    public JLimitedTextField getAnexoDq07C702() {
        return this.anexoDq07C702;
    }

    public JLabelTextFieldNumbering getAnexoDq07C708_num() {
        return this.anexoDq07C708_num;
    }

    public JMoneyTextField getAnexoDq07C708() {
        return this.anexoDq07C708;
    }

    public JLabelTextFieldNumbering getAnexoDq07C714_num() {
        return this.anexoDq07C714_num;
    }

    public JMoneyTextField getAnexoDq07C714() {
        return this.anexoDq07C714;
    }

    public JLabelTextFieldNumbering getAnexoDq07C703_num() {
        return this.anexoDq07C703_num;
    }

    public JLimitedTextField getAnexoDq07C703() {
        return this.anexoDq07C703;
    }

    public JLabelTextFieldNumbering getAnexoDq07C709_num() {
        return this.anexoDq07C709_num;
    }

    public JMoneyTextField getAnexoDq07C709() {
        return this.anexoDq07C709;
    }

    public JLabelTextFieldNumbering getAnexoDq07C715_num() {
        return this.anexoDq07C715_num;
    }

    public JMoneyTextField getAnexoDq07C715() {
        return this.anexoDq07C715;
    }

    public JLabelTextFieldNumbering getAnexoDq07C704_num() {
        return this.anexoDq07C704_num;
    }

    public JLimitedTextField getAnexoDq07C704() {
        return this.anexoDq07C704;
    }

    public JLabelTextFieldNumbering getAnexoDq07C710_num() {
        return this.anexoDq07C710_num;
    }

    public JMoneyTextField getAnexoDq07C710() {
        return this.anexoDq07C710;
    }

    public JLabelTextFieldNumbering getAnexoDq07C716_num() {
        return this.anexoDq07C716_num;
    }

    public JMoneyTextField getAnexoDq07C716() {
        return this.anexoDq07C716;
    }

    public JLabelTextFieldNumbering getAnexoDq07C705_num() {
        return this.anexoDq07C705_num;
    }

    public JLimitedTextField getAnexoDq07C705() {
        return this.anexoDq07C705;
    }

    public JLabelTextFieldNumbering getAnexoDq07C711_num() {
        return this.anexoDq07C711_num;
    }

    public JMoneyTextField getAnexoDq07C711() {
        return this.anexoDq07C711;
    }

    public JLabelTextFieldNumbering getAnexoDq07C717_num() {
        return this.anexoDq07C717_num;
    }

    public JMoneyTextField getAnexoDq07C717() {
        return this.anexoDq07C717;
    }

    public JLabelTextFieldNumbering getAnexoDq07C706_num() {
        return this.anexoDq07C706_num;
    }

    public JLimitedTextField getAnexoDq07C706() {
        return this.anexoDq07C706;
    }

    public JLabelTextFieldNumbering getAnexoDq07C712_num() {
        return this.anexoDq07C712_num;
    }

    public JMoneyTextField getAnexoDq07C712() {
        return this.anexoDq07C712;
    }

    public JLabelTextFieldNumbering getAnexoDq07C718_num() {
        return this.anexoDq07C718_num;
    }

    public JMoneyTextField getAnexoDq07C718() {
        return this.anexoDq07C718;
    }

    public JLabelTextFieldNumbering getAnexoDq07C707_num() {
        return this.anexoDq07C707_num;
    }

    public JLimitedTextField getAnexoDq07C707() {
        return this.anexoDq07C707;
    }

    public JLabelTextFieldNumbering getAnexoDq07C713_num() {
        return this.anexoDq07C713_num;
    }

    public JMoneyTextField getAnexoDq07C713() {
        return this.anexoDq07C713;
    }

    public JLabelTextFieldNumbering getAnexoDq07C719_num() {
        return this.anexoDq07C719_num;
    }

    public JMoneyTextField getAnexoDq07C719() {
        return this.anexoDq07C719;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getAnexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label() {
        return this.anexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label() {
        return this.anexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label() {
        return this.anexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.label2 = new JLabel();
        this.panel3 = new JPanel();
        this.anexoDq07C701_label = new JLabel();
        this.anexoDq07C701_num = new JLabelTextFieldNumbering();
        this.anexoDq07C701 = new JNIFTextField();
        this.panel5 = new JPanel();
        this.panel6 = new JPanel();
        this.anexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label = new JLabel();
        this.panel7 = new JPanel();
        this.anexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label = new JLabel();
        this.panel2 = new JPanel();
        this.anexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label = new JLabel();
        this.anexoDq07C702_num = new JLabelTextFieldNumbering();
        this.anexoDq07C702 = new JLimitedTextField();
        this.anexoDq07C708_num = new JLabelTextFieldNumbering();
        this.anexoDq07C708 = new JMoneyTextField();
        this.anexoDq07C714_num = new JLabelTextFieldNumbering();
        this.anexoDq07C714 = new JMoneyTextField();
        this.anexoDq07C703_num = new JLabelTextFieldNumbering();
        this.anexoDq07C703 = new JLimitedTextField();
        this.anexoDq07C709_num = new JLabelTextFieldNumbering();
        this.anexoDq07C709 = new JMoneyTextField();
        this.anexoDq07C715_num = new JLabelTextFieldNumbering();
        this.anexoDq07C715 = new JMoneyTextField();
        this.anexoDq07C704_num = new JLabelTextFieldNumbering();
        this.anexoDq07C704 = new JLimitedTextField();
        this.anexoDq07C710_num = new JLabelTextFieldNumbering();
        this.anexoDq07C710 = new JMoneyTextField();
        this.anexoDq07C716_num = new JLabelTextFieldNumbering();
        this.anexoDq07C716 = new JMoneyTextField();
        this.anexoDq07C705_num = new JLabelTextFieldNumbering();
        this.anexoDq07C705 = new JLimitedTextField();
        this.anexoDq07C711_num = new JLabelTextFieldNumbering();
        this.anexoDq07C711 = new JMoneyTextField();
        this.anexoDq07C717_num = new JLabelTextFieldNumbering();
        this.anexoDq07C717 = new JMoneyTextField();
        this.anexoDq07C706_num = new JLabelTextFieldNumbering();
        this.anexoDq07C706 = new JLimitedTextField();
        this.anexoDq07C712_num = new JLabelTextFieldNumbering();
        this.anexoDq07C712 = new JMoneyTextField();
        this.anexoDq07C718_num = new JLabelTextFieldNumbering();
        this.anexoDq07C718 = new JMoneyTextField();
        this.anexoDq07C707_num = new JLabelTextFieldNumbering();
        this.anexoDq07C707 = new JLimitedTextField();
        this.anexoDq07C713_num = new JLabelTextFieldNumbering();
        this.anexoDq07C713 = new JMoneyTextField();
        this.anexoDq07C719_num = new JLabelTextFieldNumbering();
        this.anexoDq07C719 = new JMoneyTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "3*($lgap, default), $ugap, $lgap, default"));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("0px, default:grow, $ugap, 0px, 316dlu, $lcgap, default:grow", "$lgap, default"));
        this.label2.setText(bundle.getString("Quadro07Panel.label2.text"));
        this.panel4.add((Component)this.label2, CC.xy(5, 2));
        this.this2.add((Component)this.panel4, CC.xy(3, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("0px, default:grow, $lcgap, default, $ugap, 25dlu, 0px, 43dlu, $lcgap, default:grow, $lcgap, default", "default, $lgap, default"));
        this.anexoDq07C701_label.setText(bundle.getString("Quadro07Panel.anexoDq07C701_label.text"));
        this.panel3.add((Component)this.anexoDq07C701_label, CC.xy(4, 1));
        this.anexoDq07C701_num.setText(bundle.getString("Quadro07Panel.anexoDq07C701_num.text"));
        this.anexoDq07C701_num.setBorder(new EtchedBorder());
        this.anexoDq07C701_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoDq07C701_num, CC.xy(6, 1));
        this.anexoDq07C701.setColumns(9);
        this.panel3.add((Component)this.anexoDq07C701, CC.xy(8, 1));
        this.this2.add((Component)this.panel3, CC.xy(3, 6));
        this.panel5.setPreferredSize(null);
        this.panel5.setMinimumSize(null);
        this.panel5.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, 22dlu, $lcgap, 3dlu, 0px, 25dlu, 0px, 80dlu, 0px, 3dlu, $lcgap, 25dlu, 0px, 80dlu, $lcgap, default:grow, 0px", "7*(default, $lgap), default"));
        this.panel6.setBorder(new EtchedBorder());
        this.panel6.setLayout(new FormLayout("$rgap, 36dlu, $rgap", "$rgap, 28dlu"));
        this.anexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label.setText(bundle.getString("Quadro07Panel.anexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label.text"));
        this.anexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoDq07C702_label\u00a3anexoDq07C703_label\u00a3anexoDq07C704_label\u00a3anexoDq07C705_label\u00a3anexoDq07C706_label\u00a3anexoDq07C707_label, CC.xy(2, 2));
        this.panel5.add((Component)this.panel6, CC.xywh(3, 1, 3, 1));
        this.panel7.setBorder(new EtchedBorder());
        this.panel7.setLayout(new FormLayout("108dlu", "$rgap, 28dlu"));
        this.anexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label.setText(bundle.getString("Quadro07Panel.anexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label.text"));
        this.anexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoDq07C708_label\u00a3anexoDq07C709_label\u00a3anexoDq07C710_label\u00a3anexoDq07C711_label\u00a3anexoDq07C712_label\u00a3anexoDq07C713_label, CC.xy(1, 2));
        this.panel5.add((Component)this.panel7, CC.xywh(7, 1, 7, 1));
        this.panel2.setBorder(new EtchedBorder());
        this.panel2.setLayout(new FormLayout("99dlu", "$rgap, 28dlu"));
        this.anexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label.setText(bundle.getString("Quadro07Panel.anexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label.text"));
        this.anexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq07C714_label\u00a3anexoDq07C715_label\u00a3anexoDq07C716_label\u00a3anexoDq07C717_label\u00a3anexoDq07C718_label\u00a3anexoDq07C719_label, CC.xy(1, 2));
        this.panel5.add((Component)this.panel2, CC.xywh(15, 1, 3, 1));
        this.anexoDq07C702_num.setText(bundle.getString("Quadro07Panel.anexoDq07C702_num.text"));
        this.anexoDq07C702_num.setBorder(new EtchedBorder());
        this.anexoDq07C702_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C702_num, CC.xy(3, 3));
        this.anexoDq07C702.setMaxLength(4);
        this.anexoDq07C702.setOnlyNumeric(true);
        this.anexoDq07C702.setColumns(3);
        this.panel5.add((Component)this.anexoDq07C702, CC.xy(5, 3));
        this.anexoDq07C708_num.setText(bundle.getString("Quadro07Panel.anexoDq07C708_num.text"));
        this.anexoDq07C708_num.setBorder(new EtchedBorder());
        this.anexoDq07C708_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C708_num, CC.xy(9, 3));
        this.panel5.add((Component)this.anexoDq07C708, CC.xy(11, 3));
        this.anexoDq07C714_num.setText(bundle.getString("Quadro07Panel.anexoDq07C714_num.text"));
        this.anexoDq07C714_num.setBorder(new EtchedBorder());
        this.anexoDq07C714_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C714_num, CC.xy(15, 3));
        this.panel5.add((Component)this.anexoDq07C714, CC.xy(17, 3));
        this.anexoDq07C703_num.setText(bundle.getString("Quadro07Panel.anexoDq07C703_num.text"));
        this.anexoDq07C703_num.setBorder(new EtchedBorder());
        this.anexoDq07C703_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C703_num, CC.xy(3, 5));
        this.anexoDq07C703.setMaxLength(4);
        this.anexoDq07C703.setOnlyNumeric(true);
        this.anexoDq07C703.setColumns(3);
        this.panel5.add((Component)this.anexoDq07C703, CC.xy(5, 5));
        this.anexoDq07C709_num.setText(bundle.getString("Quadro07Panel.anexoDq07C709_num.text"));
        this.anexoDq07C709_num.setBorder(new EtchedBorder());
        this.anexoDq07C709_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C709_num, CC.xy(9, 5));
        this.panel5.add((Component)this.anexoDq07C709, CC.xy(11, 5));
        this.anexoDq07C715_num.setText(bundle.getString("Quadro07Panel.anexoDq07C715_num.text"));
        this.anexoDq07C715_num.setBorder(new EtchedBorder());
        this.anexoDq07C715_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C715_num, CC.xy(15, 5));
        this.panel5.add((Component)this.anexoDq07C715, CC.xy(17, 5));
        this.anexoDq07C704_num.setText(bundle.getString("Quadro07Panel.anexoDq07C704_num.text"));
        this.anexoDq07C704_num.setBorder(new EtchedBorder());
        this.anexoDq07C704_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C704_num, CC.xy(3, 7));
        this.anexoDq07C704.setMaxLength(4);
        this.anexoDq07C704.setOnlyNumeric(true);
        this.anexoDq07C704.setColumns(3);
        this.panel5.add((Component)this.anexoDq07C704, CC.xy(5, 7));
        this.anexoDq07C710_num.setText(bundle.getString("Quadro07Panel.anexoDq07C710_num.text"));
        this.anexoDq07C710_num.setBorder(new EtchedBorder());
        this.anexoDq07C710_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C710_num, CC.xy(9, 7));
        this.panel5.add((Component)this.anexoDq07C710, CC.xy(11, 7));
        this.anexoDq07C716_num.setText(bundle.getString("Quadro07Panel.anexoDq07C716_num.text"));
        this.anexoDq07C716_num.setBorder(new EtchedBorder());
        this.anexoDq07C716_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C716_num, CC.xy(15, 7));
        this.panel5.add((Component)this.anexoDq07C716, CC.xy(17, 7));
        this.anexoDq07C705_num.setText(bundle.getString("Quadro07Panel.anexoDq07C705_num.text"));
        this.anexoDq07C705_num.setBorder(new EtchedBorder());
        this.anexoDq07C705_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C705_num, CC.xy(3, 9));
        this.anexoDq07C705.setMaxLength(4);
        this.anexoDq07C705.setOnlyNumeric(true);
        this.anexoDq07C705.setColumns(3);
        this.panel5.add((Component)this.anexoDq07C705, CC.xy(5, 9));
        this.anexoDq07C711_num.setText(bundle.getString("Quadro07Panel.anexoDq07C711_num.text"));
        this.anexoDq07C711_num.setBorder(new EtchedBorder());
        this.anexoDq07C711_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C711_num, CC.xy(9, 9));
        this.panel5.add((Component)this.anexoDq07C711, CC.xy(11, 9));
        this.anexoDq07C717_num.setText(bundle.getString("Quadro07Panel.anexoDq07C717_num.text"));
        this.anexoDq07C717_num.setBorder(new EtchedBorder());
        this.anexoDq07C717_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C717_num, CC.xy(15, 9));
        this.panel5.add((Component)this.anexoDq07C717, CC.xy(17, 9));
        this.anexoDq07C706_num.setText(bundle.getString("Quadro07Panel.anexoDq07C706_num.text"));
        this.anexoDq07C706_num.setBorder(new EtchedBorder());
        this.anexoDq07C706_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C706_num, CC.xy(3, 11));
        this.anexoDq07C706.setMaxLength(4);
        this.anexoDq07C706.setOnlyNumeric(true);
        this.anexoDq07C706.setColumns(3);
        this.panel5.add((Component)this.anexoDq07C706, CC.xy(5, 11));
        this.anexoDq07C712_num.setText(bundle.getString("Quadro07Panel.anexoDq07C712_num.text"));
        this.anexoDq07C712_num.setBorder(new EtchedBorder());
        this.anexoDq07C712_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C712_num, CC.xy(9, 11));
        this.panel5.add((Component)this.anexoDq07C712, CC.xy(11, 11));
        this.anexoDq07C718_num.setText(bundle.getString("Quadro07Panel.anexoDq07C718_num.text"));
        this.anexoDq07C718_num.setBorder(new EtchedBorder());
        this.anexoDq07C718_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C718_num, CC.xy(15, 11));
        this.panel5.add((Component)this.anexoDq07C718, CC.xy(17, 11));
        this.anexoDq07C707_num.setText(bundle.getString("Quadro07Panel.anexoDq07C707_num.text"));
        this.anexoDq07C707_num.setBorder(new EtchedBorder());
        this.anexoDq07C707_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C707_num, CC.xy(3, 13));
        this.anexoDq07C707.setMaxLength(4);
        this.anexoDq07C707.setOnlyNumeric(true);
        this.anexoDq07C707.setColumns(3);
        this.panel5.add((Component)this.anexoDq07C707, CC.xy(5, 13));
        this.anexoDq07C713_num.setText(bundle.getString("Quadro07Panel.anexoDq07C713_num.text"));
        this.anexoDq07C713_num.setBorder(new EtchedBorder());
        this.anexoDq07C713_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C713_num, CC.xy(9, 13));
        this.panel5.add((Component)this.anexoDq07C713, CC.xy(11, 13));
        this.anexoDq07C719_num.setText(bundle.getString("Quadro07Panel.anexoDq07C719_num.text"));
        this.anexoDq07C719_num.setBorder(new EtchedBorder());
        this.anexoDq07C719_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoDq07C719_num, CC.xy(15, 13));
        this.panel5.add((Component)this.anexoDq07C719, CC.xy(17, 13));
        this.this2.add((Component)this.panel5, CC.xy(3, 9));
        this.add((Component)this.this2, "Center");
    }
}

