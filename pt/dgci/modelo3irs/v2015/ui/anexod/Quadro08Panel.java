/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro08Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoDq08C801_label;
    protected JLabelTextFieldNumbering anexoDq08C801_num;
    protected JMoneyTextField anexoDq08C801;

    public Quadro08Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoDq08C801_label() {
        return this.anexoDq08C801_label;
    }

    public JLabelTextFieldNumbering getAnexoDq08C801_num() {
        return this.anexoDq08C801_num;
    }

    public JMoneyTextField getAnexoDq08C801() {
        return this.anexoDq08C801;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoDq08C801_label = new JLabel();
        this.anexoDq08C801_num = new JLabelTextFieldNumbering();
        this.anexoDq08C801 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro08Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro08Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("0px, 2*(default, $lcgap), default:grow, $rgap, 25dlu, 0px, 75dlu, $lcgap, 0px, default", "$rgap, default, $lgap, default"));
        this.anexoDq08C801_label.setText(bundle.getString("Quadro08Panel.anexoDq08C801_label.text"));
        this.anexoDq08C801_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq08C801_label, cc.xy(4, 2));
        this.anexoDq08C801_num.setText(bundle.getString("Quadro08Panel.anexoDq08C801_num.text"));
        this.anexoDq08C801_num.setBorder(new EtchedBorder());
        this.anexoDq08C801_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq08C801_num, cc.xy(8, 2));
        this.panel2.add((Component)this.anexoDq08C801, cc.xy(10, 2));
        this.this2.add((Component)this.panel2, cc.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

