/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro01Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro01PanelBase
extends Quadro01Panel
implements IBindablePanel<Quadro01> {
    private static final long serialVersionUID = 1;
    protected Quadro01 model;

    @Override
    public abstract void setModel(Quadro01 var1, boolean var2);

    @Override
    public Quadro01 getModel() {
        return this.model;
    }

    public Quadro01PanelBase(Quadro01 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

