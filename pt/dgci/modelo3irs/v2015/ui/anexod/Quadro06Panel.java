/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoDq06C601_label;
    protected JLabelTextFieldNumbering anexoDq06C601_num;
    protected JMoneyTextField anexoDq06C601;
    protected JLabel anexoDq06C602_label;
    protected JLabelTextFieldNumbering anexoDq06C602_num;
    protected JMoneyTextField anexoDq06C602;

    public Quadro06Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoDq06C601_label() {
        return this.anexoDq06C601_label;
    }

    public JLabelTextFieldNumbering getAnexoDq06C601_num() {
        return this.anexoDq06C601_num;
    }

    public JMoneyTextField getAnexoDq06C601() {
        return this.anexoDq06C601;
    }

    public JLabel getAnexoDq06C602_label() {
        return this.anexoDq06C602_label;
    }

    public JLabelTextFieldNumbering getAnexoDq06C602_num() {
        return this.anexoDq06C602_num;
    }

    public JMoneyTextField getAnexoDq06C602() {
        return this.anexoDq06C602;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoDq06C601_label = new JLabel();
        this.anexoDq06C601_num = new JLabelTextFieldNumbering();
        this.anexoDq06C601 = new JMoneyTextField();
        this.anexoDq06C602_label = new JLabel();
        this.anexoDq06C602_num = new JLabelTextFieldNumbering();
        this.anexoDq06C602 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("0px, default:grow, $lcgap, default, $rgap, 25dlu, 0px, 75dlu, $lcgap, default:grow, $lcgap, default, $lcgap, 25dlu, 0px, 75dlu, $lcgap, default:grow", "$rgap, default, $lgap, default"));
        this.anexoDq06C601_label.setText(bundle.getString("Quadro06Panel.anexoDq06C601_label.text"));
        this.anexoDq06C601_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq06C601_label, cc.xy(4, 2));
        this.anexoDq06C601_num.setText(bundle.getString("Quadro06Panel.anexoDq06C601_num.text"));
        this.anexoDq06C601_num.setBorder(new EtchedBorder());
        this.anexoDq06C601_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq06C601_num, cc.xy(6, 2));
        this.panel2.add((Component)this.anexoDq06C601, cc.xy(8, 2));
        this.anexoDq06C602_label.setText(bundle.getString("Quadro06Panel.anexoDq06C602_label.text"));
        this.anexoDq06C602_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq06C602_label, cc.xy(12, 2));
        this.anexoDq06C602_num.setText(bundle.getString("Quadro06Panel.anexoDq06C602_num.text"));
        this.anexoDq06C602_num.setBorder(new EtchedBorder());
        this.anexoDq06C602_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq06C602_num, cc.xy(14, 2));
        this.panel2.add((Component)this.anexoDq06C602, cc.xy(16, 2));
        this.this2.add((Component)this.panel2, cc.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

