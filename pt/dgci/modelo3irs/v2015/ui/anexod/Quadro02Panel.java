/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro02Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JLabel anexoDq02C05_label;
    protected JLabelTextFieldNumbering label3;
    protected JEditableComboBox anexoDq02C03;

    public Quadro02Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoDq02C05_label() {
        return this.anexoDq02C05_label;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JEditableComboBox getAnexoDq02C03() {
        return this.anexoDq02C03;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.anexoDq02C05_label = new JLabel();
        this.label3 = new JLabelTextFieldNumbering();
        this.anexoDq02C03 = new JEditableComboBox();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro02Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro02Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $rgap, left:default, $rgap, 13dlu, 0px, $lcgap, 40dlu, $rgap, default:grow", "$rgap, default, $lgap, default"));
        this.anexoDq02C05_label.setText(bundle.getString("Quadro02Panel.anexoDq02C05_label.text"));
        this.anexoDq02C05_label.setHorizontalAlignment(4);
        this.this2.add((Component)this.anexoDq02C05_label, cc.xy(4, 2));
        this.label3.setText(bundle.getString("Quadro02Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.this2.add((Component)this.label3, cc.xy(6, 2));
        this.anexoDq02C03.setMinimumSize(null);
        this.anexoDq02C03.setPreferredSize(null);
        this.anexoDq02C03.setEditable(false);
        this.anexoDq02C03.setEnabled(false);
        this.this2.add((Component)this.anexoDq02C03, cc.xy(9, 2));
        this.add((Component)this.this2, "Center");
    }
}

