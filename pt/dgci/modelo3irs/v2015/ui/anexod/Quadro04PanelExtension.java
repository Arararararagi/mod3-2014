/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexod.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro04PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04> {
    public static final int MAX_LINES_Q04_T1 = 10;
    public static final int MAX_LINES_Q04_T2 = 10;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeaderT2();
            }
        }
    }

    @Override
    protected void addLineAnexoDq04T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoDq04T1() != null && this.model.getAnexoDq04T1().size() >= 10) {
            return;
        }
        super.addLineAnexoDq04T1_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoDq04T2_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoDq04T2() != null && this.model.getAnexoDq04T2().size() >= 10) {
            return;
        }
        super.addLineAnexoDq04T2_LinhaActionPerformed(e);
    }

    private void setHeaderT2() {
        TableColumnModel cm1 = this.getAnexoDq04T2().getTableHeader().getColumnModel();
        ColumnGroup g_identificacao = new ColumnGroup("Identifica\u00e7\u00e3o do Pa\u00eds");
        g_identificacao.add(cm1.getColumn(1));
        g_identificacao.add(cm1.getColumn(2));
        ColumnGroup g_imposto = new ColumnGroup("Imposto pago no Estrangeiro");
        g_imposto.add(cm1.getColumn(4));
        if (this.getAnexoDq04T2().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoDq04T2().getTableHeader()).addColumnGroup(g_identificacao);
            ((GroupableTableHeader)this.getAnexoDq04T2().getTableHeader()).addColumnGroup(g_imposto);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm1);
        header.addColumnGroup(g_identificacao);
        header.addColumnGroup(g_imposto);
        this.getAnexoDq04T2().setTableHeader(header);
    }
}

