/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro01Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabelTextFieldNumbering label4;
    protected JCheckBox anexoDq01B1;
    protected JLabelTextFieldNumbering label5;
    protected JCheckBox anexoDq01B2;

    public Quadro01Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JCheckBox getAnexoDq01B1() {
        return this.anexoDq01B1;
    }

    public JLabelTextFieldNumbering getLabel5() {
        return this.label5;
    }

    public JCheckBox getAnexoDq01B2() {
        return this.anexoDq01B2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoDq01B1 = new JCheckBox();
        this.label5 = new JLabelTextFieldNumbering();
        this.anexoDq01B2 = new JCheckBox();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro01Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro01Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow, 0px", "default, $lgap, default"));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, 13dlu, 0px, default, $lcgap, default:grow", "$rgap, 2*(default, $lgap), default"));
        this.label4.setText(bundle.getString("Quadro01Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label4, CC.xy(3, 2));
        this.anexoDq01B1.setText(bundle.getString("Quadro01Panel.anexoDq01B1.text"));
        this.panel2.add((Component)this.anexoDq01B1, CC.xy(5, 2));
        this.label5.setText(bundle.getString("Quadro01Panel.label5.text"));
        this.label5.setHorizontalAlignment(0);
        this.label5.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label5, CC.xy(3, 4));
        this.anexoDq01B2.setText(bundle.getString("Quadro01Panel.anexoDq01B2.text"));
        this.panel2.add((Component)this.anexoDq01B2, CC.xy(5, 4));
        this.this2.add((Component)this.panel2, CC.xy(3, 1));
        this.add((Component)this.this2, "Center");
    }
}

