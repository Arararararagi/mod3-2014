/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoDq05C501_base_label\u00a3anexoDq05C505_base_label;
    protected JLabel anexoDq05C502_base_label\u00a3anexoDq05C506_base_label;
    protected JLabel anexoDq05C503_base_label\u00a3anexoDq05C507_base_label;
    protected JLabel anexoDq05C504_base_label\u00a3anexoDq05C508_base_label;
    protected JLabel anexoDq05C501_label\u00a3anexoDq05C502_label\u00a3anexoDq05C503_label\u00a3anexoDq05C504_label;
    protected JLabelTextFieldNumbering anexoDq05C501_num;
    protected JMoneyTextField anexoDq05C501;
    protected JLabelTextFieldNumbering anexoDq05C502_num;
    protected JMoneyTextField anexoDq05C502;
    protected JLabelTextFieldNumbering anexoDq05C503_num;
    protected JMoneyTextField anexoDq05C503;
    protected JLabelTextFieldNumbering anexoDq05C504_num;
    protected JMoneyTextField anexoDq05C504;
    protected JLabel anexoDq05C505_label\u00a3anexoDq05C506_label\u00a3anexoDq05C507_label\u00a3anexoDq05C508_label;
    protected JLabelTextFieldNumbering anexoDq05C505_num;
    protected JMoneyTextField anexoDq05C505;
    protected JLabelTextFieldNumbering anexoDq05C506_num;
    protected JMoneyTextField anexoDq05C506;
    protected JLabelTextFieldNumbering anexoDq05C507_num;
    protected JMoneyTextField anexoDq05C507;
    protected JLabelTextFieldNumbering anexoDq05C508_num;
    protected JMoneyTextField anexoDq05C508;

    public Quadro05Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoDq05C501_base_label\u00a3anexoDq05C505_base_label() {
        return this.anexoDq05C501_base_label\u00a3anexoDq05C505_base_label;
    }

    public JLabelTextFieldNumbering getAnexoDq05C501_num() {
        return this.anexoDq05C501_num;
    }

    public JMoneyTextField getAnexoDq05C501() {
        return this.anexoDq05C501;
    }

    public JLabelTextFieldNumbering getAnexoDq05C505_num() {
        return this.anexoDq05C505_num;
    }

    public JMoneyTextField getAnexoDq05C505() {
        return this.anexoDq05C505;
    }

    public JLabel getAnexoDq05C502_base_label\u00a3anexoDq05C506_base_label() {
        return this.anexoDq05C502_base_label\u00a3anexoDq05C506_base_label;
    }

    public JLabelTextFieldNumbering getAnexoDq05C502_num() {
        return this.anexoDq05C502_num;
    }

    public JMoneyTextField getAnexoDq05C502() {
        return this.anexoDq05C502;
    }

    public JLabelTextFieldNumbering getAnexoDq05C506_num() {
        return this.anexoDq05C506_num;
    }

    public JMoneyTextField getAnexoDq05C506() {
        return this.anexoDq05C506;
    }

    public JLabel getAnexoDq05C503_base_label\u00a3anexoDq05C507_base_label() {
        return this.anexoDq05C503_base_label\u00a3anexoDq05C507_base_label;
    }

    public JLabelTextFieldNumbering getAnexoDq05C503_num() {
        return this.anexoDq05C503_num;
    }

    public JMoneyTextField getAnexoDq05C503() {
        return this.anexoDq05C503;
    }

    public JLabelTextFieldNumbering getAnexoDq05C507_num() {
        return this.anexoDq05C507_num;
    }

    public JMoneyTextField getAnexoDq05C507() {
        return this.anexoDq05C507;
    }

    public JLabel getAnexoDq05C504_base_label\u00a3anexoDq05C508_base_label() {
        return this.anexoDq05C504_base_label\u00a3anexoDq05C508_base_label;
    }

    public JLabelTextFieldNumbering getAnexoDq05C504_num() {
        return this.anexoDq05C504_num;
    }

    public JMoneyTextField getAnexoDq05C504() {
        return this.anexoDq05C504;
    }

    public JLabelTextFieldNumbering getAnexoDq05C508_num() {
        return this.anexoDq05C508_num;
    }

    public JMoneyTextField getAnexoDq05C508() {
        return this.anexoDq05C508;
    }

    public JLabel getAnexoDq05C501_label\u00a3anexoDq05C502_label\u00a3anexoDq05C503_label\u00a3anexoDq05C504_label() {
        return this.anexoDq05C501_label\u00a3anexoDq05C502_label\u00a3anexoDq05C503_label\u00a3anexoDq05C504_label;
    }

    public JLabel getAnexoDq05C505_label\u00a3anexoDq05C506_label\u00a3anexoDq05C507_label\u00a3anexoDq05C508_label() {
        return this.anexoDq05C505_label\u00a3anexoDq05C506_label\u00a3anexoDq05C507_label\u00a3anexoDq05C508_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoDq05C501_base_label\u00a3anexoDq05C505_base_label = new JLabel();
        this.anexoDq05C502_base_label\u00a3anexoDq05C506_base_label = new JLabel();
        this.anexoDq05C503_base_label\u00a3anexoDq05C507_base_label = new JLabel();
        this.anexoDq05C504_base_label\u00a3anexoDq05C508_base_label = new JLabel();
        this.anexoDq05C501_label\u00a3anexoDq05C502_label\u00a3anexoDq05C503_label\u00a3anexoDq05C504_label = new JLabel();
        this.anexoDq05C501_num = new JLabelTextFieldNumbering();
        this.anexoDq05C501 = new JMoneyTextField();
        this.anexoDq05C502_num = new JLabelTextFieldNumbering();
        this.anexoDq05C502 = new JMoneyTextField();
        this.anexoDq05C503_num = new JLabelTextFieldNumbering();
        this.anexoDq05C503 = new JMoneyTextField();
        this.anexoDq05C504_num = new JLabelTextFieldNumbering();
        this.anexoDq05C504 = new JMoneyTextField();
        this.anexoDq05C505_label\u00a3anexoDq05C506_label\u00a3anexoDq05C507_label\u00a3anexoDq05C508_label = new JLabel();
        this.anexoDq05C505_num = new JLabelTextFieldNumbering();
        this.anexoDq05C505 = new JMoneyTextField();
        this.anexoDq05C506_num = new JLabelTextFieldNumbering();
        this.anexoDq05C506 = new JMoneyTextField();
        this.anexoDq05C507_num = new JLabelTextFieldNumbering();
        this.anexoDq05C507 = new JMoneyTextField();
        this.anexoDq05C508_num = new JLabelTextFieldNumbering();
        this.anexoDq05C508 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "$ugap, 2*($lgap, default)"));
        this.panel2.setLayout(new FormLayout("default, $lcgap, default:grow, $lcgap, 25dlu, 0px, 75dlu, 2*($lcgap, default), 0px, 75dlu, $lcgap, default, $lcgap, 0px, 25dlu, 0px, 75dlu, $lcgap, default, $lcgap, 25dlu, 0px, 75dlu, $lcgap, 0px", "default, $glue, 4*(default, $lgap), default"));
        this.anexoDq05C501_base_label\u00a3anexoDq05C505_base_label.setText(bundle.getString("Quadro05Panel.anexoDq05C501_base_label\u00a3anexoDq05C505_base_label.text"));
        this.anexoDq05C501_base_label\u00a3anexoDq05C505_base_label.setBorder(new EtchedBorder());
        this.anexoDq05C501_base_label\u00a3anexoDq05C505_base_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C501_base_label\u00a3anexoDq05C505_base_label, cc.xywh(5, 3, 3, 1));
        this.anexoDq05C502_base_label\u00a3anexoDq05C506_base_label.setText(bundle.getString("Quadro05Panel.anexoDq05C502_base_label\u00a3anexoDq05C506_base_label.text"));
        this.anexoDq05C502_base_label\u00a3anexoDq05C506_base_label.setBorder(new EtchedBorder());
        this.anexoDq05C502_base_label\u00a3anexoDq05C506_base_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C502_base_label\u00a3anexoDq05C506_base_label, cc.xywh(11, 3, 3, 1));
        this.anexoDq05C503_base_label\u00a3anexoDq05C507_base_label.setText(bundle.getString("Quadro05Panel.anexoDq05C503_base_label\u00a3anexoDq05C507_base_label.text"));
        this.anexoDq05C503_base_label\u00a3anexoDq05C507_base_label.setBorder(new EtchedBorder());
        this.anexoDq05C503_base_label\u00a3anexoDq05C507_base_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C503_base_label\u00a3anexoDq05C507_base_label, cc.xywh(18, 3, 3, 1));
        this.anexoDq05C504_base_label\u00a3anexoDq05C508_base_label.setText(bundle.getString("Quadro05Panel.anexoDq05C504_base_label\u00a3anexoDq05C508_base_label.text"));
        this.anexoDq05C504_base_label\u00a3anexoDq05C508_base_label.setBorder(new EtchedBorder());
        this.anexoDq05C504_base_label\u00a3anexoDq05C508_base_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C504_base_label\u00a3anexoDq05C508_base_label, cc.xywh(24, 3, 3, 1));
        this.anexoDq05C501_label\u00a3anexoDq05C502_label\u00a3anexoDq05C503_label\u00a3anexoDq05C504_label.setText(bundle.getString("Quadro05Panel.anexoDq05C501_label\u00a3anexoDq05C502_label\u00a3anexoDq05C503_label\u00a3anexoDq05C504_label.text"));
        this.panel2.add((Component)this.anexoDq05C501_label\u00a3anexoDq05C502_label\u00a3anexoDq05C503_label\u00a3anexoDq05C504_label, cc.xy(1, 5));
        this.anexoDq05C501_num.setText(bundle.getString("Quadro05Panel.anexoDq05C501_num.text"));
        this.anexoDq05C501_num.setBorder(new EtchedBorder());
        this.anexoDq05C501_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C501_num, cc.xy(5, 5));
        this.panel2.add((Component)this.anexoDq05C501, cc.xy(7, 5));
        this.anexoDq05C502_num.setText(bundle.getString("Quadro05Panel.anexoDq05C502_num.text"));
        this.anexoDq05C502_num.setBorder(new EtchedBorder());
        this.anexoDq05C502_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C502_num, cc.xy(11, 5));
        this.panel2.add((Component)this.anexoDq05C502, cc.xy(13, 5));
        this.anexoDq05C503_num.setText(bundle.getString("Quadro05Panel.anexoDq05C503_num.text"));
        this.anexoDq05C503_num.setBorder(new EtchedBorder());
        this.anexoDq05C503_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C503_num, cc.xy(18, 5));
        this.panel2.add((Component)this.anexoDq05C503, cc.xy(20, 5));
        this.anexoDq05C504_num.setText(bundle.getString("Quadro05Panel.anexoDq05C504_num.text"));
        this.anexoDq05C504_num.setBorder(new EtchedBorder());
        this.anexoDq05C504_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C504_num, cc.xy(24, 5));
        this.panel2.add((Component)this.anexoDq05C504, cc.xy(26, 5));
        this.anexoDq05C505_label\u00a3anexoDq05C506_label\u00a3anexoDq05C507_label\u00a3anexoDq05C508_label.setText(bundle.getString("Quadro05Panel.anexoDq05C505_label\u00a3anexoDq05C506_label\u00a3anexoDq05C507_label\u00a3anexoDq05C508_label.text"));
        this.panel2.add((Component)this.anexoDq05C505_label\u00a3anexoDq05C506_label\u00a3anexoDq05C507_label\u00a3anexoDq05C508_label, cc.xy(1, 7));
        this.anexoDq05C505_num.setText(bundle.getString("Quadro05Panel.anexoDq05C505_num.text"));
        this.anexoDq05C505_num.setBorder(new EtchedBorder());
        this.anexoDq05C505_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C505_num, cc.xy(5, 7));
        this.panel2.add((Component)this.anexoDq05C505, cc.xy(7, 7));
        this.anexoDq05C506_num.setText(bundle.getString("Quadro05Panel.anexoDq05C506_num.text"));
        this.anexoDq05C506_num.setBorder(new EtchedBorder());
        this.anexoDq05C506_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C506_num, cc.xy(11, 7));
        this.panel2.add((Component)this.anexoDq05C506, cc.xy(13, 7));
        this.anexoDq05C507_num.setText(bundle.getString("Quadro05Panel.anexoDq05C507_num.text"));
        this.anexoDq05C507_num.setBorder(new EtchedBorder());
        this.anexoDq05C507_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C507_num, cc.xy(18, 7));
        this.panel2.add((Component)this.anexoDq05C507, cc.xy(20, 7));
        this.anexoDq05C508_num.setText(bundle.getString("Quadro05Panel.anexoDq05C508_num.text"));
        this.anexoDq05C508_num.setBorder(new EtchedBorder());
        this.anexoDq05C508_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq05C508_num, cc.xy(24, 7));
        this.panel2.add((Component)this.anexoDq05C508, cc.xy(26, 7));
        this.this2.add((Component)this.panel2, cc.xy(3, 3));
        this.add((Component)this.this2, "Center");
    }
}

