/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label17;
    protected JLabel anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label;
    protected JLabel anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label;
    protected JLabel anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label;
    protected JLabel anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label;
    protected JLabel anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label;
    protected JLabel anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label;
    protected JLabel label1;
    protected JLabel anexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label;
    protected JLabelTextFieldNumbering anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num;
    protected JNIFTextField anexoDq04C401;
    protected JPercentTextField anexoDq04C401a;
    protected JMoneyTextField anexoDq04C401b;
    protected JMoneyTextField anexoDq04C401c;
    protected JMoneyTextField anexoDq04C401d;
    protected JMoneyTextField anexoDq04C401e;
    protected JLabel anexoDq05C502_base_label;
    protected JLabelTextFieldNumbering anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num;
    protected JNIFTextField anexoDq04C402;
    protected JPercentTextField anexoDq04C402a;
    protected JMoneyTextField anexoDq04C402b;
    protected JMoneyTextField anexoDq04C402c;
    protected JMoneyTextField anexoDq04C402d;
    protected JMoneyTextField anexoDq04C402e;
    protected JLabelTextFieldNumbering anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num;
    protected JNIFTextField anexoDq04C403;
    protected JPercentTextField anexoDq04C403a;
    protected JMoneyTextField anexoDq04C403b;
    protected JMoneyTextField anexoDq04C403c;
    protected JMoneyTextField anexoDq04C403d;
    protected JMoneyTextField anexoDq04C403e;
    protected JPanel panel3;
    protected JLabel label20;
    protected JLabel anexoDq04C431_label\u00a3anexoDq04C432_label;
    protected JLabel anexoDq04C431a_label\u00a3anexoDq04C432a_label;
    protected JLabel anexoDq04C431b_label\u00a3anexoDq04C432b_label;
    protected JLabel anexoDq04C431c_label\u00a3anexoDq04C432c_label;
    protected JLabel anexoDq04C431d_label\u00a3anexoDq04C432d_label;
    protected JLabel anexoDq04C431e_label\u00a3anexoDq04C432e_label;
    protected JLabel label27;
    protected JLabel anexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label;
    protected JLabelTextFieldNumbering anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num;
    protected JNIFTextField anexoDq04C431;
    protected JPercentTextField anexoDq04C431a;
    protected JMoneyTextField anexoDq04C431b;
    protected JMoneyTextField anexoDq04C431c;
    protected JMoneyTextField anexoDq04C431d;
    protected JMoneyTextField anexoDq04C431e;
    protected JLabelTextFieldNumbering anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num;
    protected JNIFTextField anexoDq04C432;
    protected JPercentTextField anexoDq04C432a;
    protected JMoneyTextField anexoDq04C432b;
    protected JMoneyTextField anexoDq04C432c;
    protected JMoneyTextField anexoDq04C432d;
    protected JMoneyTextField anexoDq04C432e;
    protected JLabel label39;
    protected JPanel panel5;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoDq04T1Scroll;
    protected JTable anexoDq04T1;
    protected JLabel label40;
    protected JPanel panel1;
    protected JLabel label4;
    protected JLabelTextFieldNumbering anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3;
    protected JLabel label2;
    protected JLabelTextFieldNumbering anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3;
    protected JLabel label3;
    protected JLabel label5;
    protected JLabel anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3;
    protected JLabel anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4;
    protected JPanel panel8;
    protected JPercentTextField anexoDq04C480a;
    protected JPanel panel6;
    protected JMoneyTextField anexoDq04C480b;
    protected JPanel panel9;
    protected JPercentTextField anexoDq04C481a;
    protected JPanel panel7;
    protected JMoneyTextField anexoDq04C481b;
    protected JPanel panel4;
    protected JLabel anexoDq05C502_base_label3;
    protected JLabel anexoDq04C1_label;
    protected JLabel anexoDq04C2_label;
    protected JLabel anexoDq04C3_label;
    protected JLabel anexoDq04C4_label;
    protected JLabel anexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label;
    protected JMoneyTextField anexoDq04C1;
    protected JMoneyTextField anexoDq04C2;
    protected JMoneyTextField anexoDq04C3;
    protected JMoneyTextField anexoDq04C4;
    protected JPanel panel10;
    protected JLabel label6;
    protected JLabel label7;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoDq04T2Scroll;
    protected JTable anexoDq04T2;

    public Quadro04Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel17() {
        return this.label17;
    }

    public JMoneyTextField getAnexoDq04C401b() {
        return this.anexoDq04C401b;
    }

    public JMoneyTextField getAnexoDq04C401c() {
        return this.anexoDq04C401c;
    }

    public JLabel getAnexoDq05C502_base_label() {
        return this.anexoDq05C502_base_label;
    }

    public JMoneyTextField getAnexoDq04C402b() {
        return this.anexoDq04C402b;
    }

    public JMoneyTextField getAnexoDq04C402c() {
        return this.anexoDq04C402c;
    }

    public JMoneyTextField getAnexoDq04C403b() {
        return this.anexoDq04C403b;
    }

    public JMoneyTextField getAnexoDq04C403c() {
        return this.anexoDq04C403c;
    }

    public JLabel getAnexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label() {
        return this.anexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label;
    }

    public JMoneyTextField getAnexoDq04C401d() {
        return this.anexoDq04C401d;
    }

    public JLabelTextFieldNumbering getAnexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num() {
        return this.anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num;
    }

    public JNIFTextField getAnexoDq04C401() {
        return this.anexoDq04C401;
    }

    public JLabelTextFieldNumbering getAnexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num() {
        return this.anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num;
    }

    public JNIFTextField getAnexoDq04C402() {
        return this.anexoDq04C402;
    }

    public JLabelTextFieldNumbering getAnexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num() {
        return this.anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num;
    }

    public JNIFTextField getAnexoDq04C403() {
        return this.anexoDq04C403;
    }

    public JPercentTextField getAnexoDq04C401a() {
        return this.anexoDq04C401a;
    }

    public JPercentTextField getAnexoDq04C402a() {
        return this.anexoDq04C402a;
    }

    public JPercentTextField getAnexoDq04C403a() {
        return this.anexoDq04C403a;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JMoneyTextField getAnexoDq04C401e() {
        return this.anexoDq04C401e;
    }

    public JMoneyTextField getAnexoDq04C402d() {
        return this.anexoDq04C402d;
    }

    public JMoneyTextField getAnexoDq04C402e() {
        return this.anexoDq04C402e;
    }

    public JMoneyTextField getAnexoDq04C403d() {
        return this.anexoDq04C403d;
    }

    public JMoneyTextField getAnexoDq04C403e() {
        return this.anexoDq04C403e;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel20() {
        return this.label20;
    }

    public JLabel getLabel27() {
        return this.label27;
    }

    public JLabelTextFieldNumbering getAnexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num() {
        return this.anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num;
    }

    public JNIFTextField getAnexoDq04C431() {
        return this.anexoDq04C431;
    }

    public JPercentTextField getAnexoDq04C431a() {
        return this.anexoDq04C431a;
    }

    public JMoneyTextField getAnexoDq04C431b() {
        return this.anexoDq04C431b;
    }

    public JMoneyTextField getAnexoDq04C431c() {
        return this.anexoDq04C431c;
    }

    public JMoneyTextField getAnexoDq04C431d() {
        return this.anexoDq04C431d;
    }

    public JMoneyTextField getAnexoDq04C431e() {
        return this.anexoDq04C431e;
    }

    public JLabelTextFieldNumbering getAnexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num() {
        return this.anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num;
    }

    public JNIFTextField getAnexoDq04C432() {
        return this.anexoDq04C432;
    }

    public JPercentTextField getAnexoDq04C432a() {
        return this.anexoDq04C432a;
    }

    public JMoneyTextField getAnexoDq04C432c() {
        return this.anexoDq04C432c;
    }

    public JMoneyTextField getAnexoDq04C432d() {
        return this.anexoDq04C432d;
    }

    public JMoneyTextField getAnexoDq04C432e() {
        return this.anexoDq04C432e;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getLabel39() {
        return this.label39;
    }

    public JLabel getAnexoDq05C502_base_label3() {
        return this.anexoDq05C502_base_label3;
    }

    public JLabel getAnexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label() {
        return this.anexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label;
    }

    public JMoneyTextField getAnexoDq04C1() {
        return this.anexoDq04C1;
    }

    public JMoneyTextField getAnexoDq04C2() {
        return this.anexoDq04C2;
    }

    public JMoneyTextField getAnexoDq04C3() {
        return this.anexoDq04C3;
    }

    public JMoneyTextField getAnexoDq04C4() {
        return this.anexoDq04C4;
    }

    protected void addLineAnexoDq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoCq08T1_linhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoDq04T1Scroll() {
        return this.anexoDq04T1Scroll;
    }

    public JTable getAnexoDq04T1() {
        return this.anexoDq04T1;
    }

    protected void removeLineAnexoDq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JLabel getAnexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label() {
        return this.anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label;
    }

    public JLabel getAnexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label() {
        return this.anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label;
    }

    public JLabel getAnexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label() {
        return this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label;
    }

    public JLabel getAnexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label() {
        return this.anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label;
    }

    public JLabel getAnexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label() {
        return this.anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label;
    }

    public JLabel getAnexoDq04C431_label\u00a3anexoDq04C432_label() {
        return this.anexoDq04C431_label\u00a3anexoDq04C432_label;
    }

    public JLabel getAnexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label() {
        return this.anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label;
    }

    public JLabel getAnexoDq04C431a_label\u00a3anexoDq04C432a_label() {
        return this.anexoDq04C431a_label\u00a3anexoDq04C432a_label;
    }

    public JLabel getAnexoDq04C431b_label\u00a3anexoDq04C432b_label() {
        return this.anexoDq04C431b_label\u00a3anexoDq04C432b_label;
    }

    public JLabel getAnexoDq04C431c_label\u00a3anexoDq04C432c_label() {
        return this.anexoDq04C431c_label\u00a3anexoDq04C432c_label;
    }

    public JLabel getAnexoDq04C431d_label\u00a3anexoDq04C432d_label() {
        return this.anexoDq04C431d_label\u00a3anexoDq04C432d_label;
    }

    public JLabel getAnexoDq04C431e_label\u00a3anexoDq04C432e_label() {
        return this.anexoDq04C431e_label\u00a3anexoDq04C432e_label;
    }

    public JLabel getAnexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label() {
        return this.anexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label;
    }

    public JLabel getAnexoDq04C1_label() {
        return this.anexoDq04C1_label;
    }

    public JLabel getAnexoDq04C2_label() {
        return this.anexoDq04C2_label;
    }

    public JLabel getAnexoDq04C3_label() {
        return this.anexoDq04C3_label;
    }

    public JLabel getAnexoDq04C4_label() {
        return this.anexoDq04C4_label;
    }

    public JMoneyTextField getAnexoDq04C432b() {
        return this.anexoDq04C432b;
    }

    public JLabel getLabel40() {
        return this.label40;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    public JLabelTextFieldNumbering getAnexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3() {
        return this.anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabelTextFieldNumbering getAnexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3() {
        return this.anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPercentTextField getAnexoDq04C480a() {
        return this.anexoDq04C480a;
    }

    public JLabel getLabel5() {
        return this.label5;
    }

    public JPercentTextField getAnexoDq04C481a() {
        return this.anexoDq04C481a;
    }

    public JLabel getAnexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3() {
        return this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3;
    }

    public JLabel getAnexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4() {
        return this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4;
    }

    protected void addLineAnexoDq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoDq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JMoneyTextField getAnexoDq04C480b() {
        return this.anexoDq04C480b;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JMoneyTextField getAnexoDq04C481b() {
        return this.anexoDq04C481b;
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoDq04T2Scroll() {
        return this.anexoDq04T2Scroll;
    }

    public JTable getAnexoDq04T2() {
        return this.anexoDq04T2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoD");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label17 = new JLabel();
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label = new JLabel();
        this.anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label = new JLabel();
        this.anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label = new JLabel();
        this.anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label = new JLabel();
        this.anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label = new JLabel();
        this.anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label = new JLabel();
        this.label1 = new JLabel();
        this.anexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label = new JLabel();
        this.anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num = new JLabelTextFieldNumbering();
        this.anexoDq04C401 = new JNIFTextField();
        this.anexoDq04C401a = new JPercentTextField(2);
        this.anexoDq04C401b = new JMoneyTextField();
        this.anexoDq04C401c = new JMoneyTextField();
        this.anexoDq04C401d = new JMoneyTextField();
        this.anexoDq04C401e = new JMoneyTextField();
        this.anexoDq05C502_base_label = new JLabel();
        this.anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num = new JLabelTextFieldNumbering();
        this.anexoDq04C402 = new JNIFTextField();
        this.anexoDq04C402a = new JPercentTextField(2);
        this.anexoDq04C402b = new JMoneyTextField();
        this.anexoDq04C402c = new JMoneyTextField();
        this.anexoDq04C402d = new JMoneyTextField();
        this.anexoDq04C402e = new JMoneyTextField();
        this.anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num = new JLabelTextFieldNumbering();
        this.anexoDq04C403 = new JNIFTextField();
        this.anexoDq04C403a = new JPercentTextField(2);
        this.anexoDq04C403b = new JMoneyTextField();
        this.anexoDq04C403c = new JMoneyTextField();
        this.anexoDq04C403d = new JMoneyTextField();
        this.anexoDq04C403e = new JMoneyTextField();
        this.panel3 = new JPanel();
        this.label20 = new JLabel();
        this.anexoDq04C431_label\u00a3anexoDq04C432_label = new JLabel();
        this.anexoDq04C431a_label\u00a3anexoDq04C432a_label = new JLabel();
        this.anexoDq04C431b_label\u00a3anexoDq04C432b_label = new JLabel();
        this.anexoDq04C431c_label\u00a3anexoDq04C432c_label = new JLabel();
        this.anexoDq04C431d_label\u00a3anexoDq04C432d_label = new JLabel();
        this.anexoDq04C431e_label\u00a3anexoDq04C432e_label = new JLabel();
        this.label27 = new JLabel();
        this.anexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label = new JLabel();
        this.anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num = new JLabelTextFieldNumbering();
        this.anexoDq04C431 = new JNIFTextField();
        this.anexoDq04C431a = new JPercentTextField(2);
        this.anexoDq04C431b = new JMoneyTextField();
        this.anexoDq04C431c = new JMoneyTextField();
        this.anexoDq04C431d = new JMoneyTextField();
        this.anexoDq04C431e = new JMoneyTextField();
        this.anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num = new JLabelTextFieldNumbering();
        this.anexoDq04C432 = new JNIFTextField();
        this.anexoDq04C432a = new JPercentTextField(2);
        this.anexoDq04C432b = new JMoneyTextField();
        this.anexoDq04C432c = new JMoneyTextField();
        this.anexoDq04C432d = new JMoneyTextField();
        this.anexoDq04C432e = new JMoneyTextField();
        this.label39 = new JLabel();
        this.panel5 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoDq04T1Scroll = new JScrollPane();
        this.anexoDq04T1 = new JTable();
        this.label40 = new JLabel();
        this.panel1 = new JPanel();
        this.label4 = new JLabel();
        this.anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3 = new JLabelTextFieldNumbering();
        this.label2 = new JLabel();
        this.anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3 = new JLabelTextFieldNumbering();
        this.label3 = new JLabel();
        this.label5 = new JLabel();
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3 = new JLabel();
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4 = new JLabel();
        this.panel8 = new JPanel();
        this.anexoDq04C480a = new JPercentTextField(2);
        this.panel6 = new JPanel();
        this.anexoDq04C480b = new JMoneyTextField();
        this.panel9 = new JPanel();
        this.anexoDq04C481a = new JPercentTextField(2);
        this.panel7 = new JPanel();
        this.anexoDq04C481b = new JMoneyTextField();
        this.panel4 = new JPanel();
        this.anexoDq05C502_base_label3 = new JLabel();
        this.anexoDq04C1_label = new JLabel();
        this.anexoDq04C2_label = new JLabel();
        this.anexoDq04C3_label = new JLabel();
        this.anexoDq04C4_label = new JLabel();
        this.anexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label = new JLabel();
        this.anexoDq04C1 = new JMoneyTextField();
        this.anexoDq04C2 = new JMoneyTextField();
        this.anexoDq04C3 = new JMoneyTextField();
        this.anexoDq04C4 = new JMoneyTextField();
        this.panel10 = new JPanel();
        this.label6 = new JLabel();
        this.label7 = new JLabel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoDq04T2Scroll = new JScrollPane();
        this.anexoDq04T2 = new JTable();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "10*($lgap, default)"));
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, 55dlu, $rgap, 11dlu, $lcgap, 36dlu, 0px, $lcgap, 11dlu, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 25dlu, $rgap, 80dlu, $lcgap, default:grow, $rgap", "7*(default, $lgap), default"));
        this.label17.setText(bundle.getString("Quadro04Panel.label17.text"));
        this.label17.setHorizontalAlignment(0);
        this.label17.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label17, CC.xywh(1, 1, 33, 1));
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label.setText(bundle.getString("Quadro04Panel.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label.text"));
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label.setHorizontalAlignment(0);
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label, CC.xywh(3, 5, 3, 1));
        this.anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label.setText(bundle.getString("Quadro04Panel.anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label.text"));
        this.anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label.setHorizontalAlignment(0);
        this.anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoDq04C401a_label\u00a3anexoDq04C402a_label\u00a3anexoDq04C403a_label, CC.xywh(7, 5, 6, 1));
        this.anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label.setText(bundle.getString("Quadro04Panel.anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label.text"));
        this.anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label.setHorizontalAlignment(0);
        this.anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoDq04C401b_label\u00a3anexoDq04C402b_label\u00a3anexoDq04C403b_label, CC.xywh(14, 5, 6, 1));
        this.anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label.setText(bundle.getString("Quadro04Panel.anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label.text"));
        this.anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label.setBorder(new EtchedBorder());
        this.anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq04C401c_label\u00a3anexoDq04C402c_label\u00a3anexoDq04C403c_label, CC.xy(22, 5));
        this.anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label.setText(bundle.getString("Quadro04Panel.anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label.text"));
        this.anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label.setHorizontalAlignment(0);
        this.anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoDq04C401d_label\u00a3anexoDq04C402d_label\u00a3anexoDq04C403d_label, CC.xywh(24, 5, 6, 1));
        this.anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label.setText(bundle.getString("Quadro04Panel.anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label.text"));
        this.anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label.setHorizontalAlignment(0);
        this.anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoDq04C401e_label\u00a3anexoDq04C402e_label\u00a3anexoDq04C403e_label, CC.xy(31, 5));
        this.label1.setText(bundle.getString("Quadro04Panel.label1.text"));
        this.label1.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label1, CC.xywh(3, 7, 3, 1));
        this.anexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label.setText(bundle.getString("Quadro04Panel.anexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label.text"));
        this.anexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq04C401b_base_label\u00a3anexoDq04C402b_base_label\u00a3anexoDq04C403b_base_label, CC.xy(17, 7));
        this.anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num.setText(bundle.getString("Quadro04Panel.anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num.text"));
        this.anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num.setBorder(new EtchedBorder());
        this.anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq04C401_num\u00a3anexoDq04C401a_num\u00a3anexoDq04C401b_num\u00a3anexoDq04C401c_num\u00a3anexoDq04C401d_num\u00a3anexoDq04C401e_num, CC.xy(3, 9));
        this.anexoDq04C401.setColumns(9);
        this.panel2.add((Component)this.anexoDq04C401, CC.xy(5, 9));
        this.anexoDq04C401a.setColumns(5);
        this.panel2.add((Component)this.anexoDq04C401a, CC.xy(9, 9));
        this.panel2.add((Component)this.anexoDq04C401b, CC.xy(17, 9));
        this.panel2.add((Component)this.anexoDq04C401c, CC.xy(22, 9));
        this.panel2.add((Component)this.anexoDq04C401d, CC.xy(27, 9));
        this.panel2.add((Component)this.anexoDq04C401e, CC.xy(31, 9));
        this.panel2.add((Component)this.anexoDq05C502_base_label, CC.xy(1, 11));
        this.anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num.setText(bundle.getString("Quadro04Panel.anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num.text"));
        this.anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num.setBorder(new EtchedBorder());
        this.anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq04C402_num\u00a3anexoDq04C402a_num\u00a3anexoDq04C402b_num\u00a3anexoDq04C402c_num\u00a3anexoDq04C402d_num\u00a3anexoDq04C402e_num, CC.xy(3, 11));
        this.anexoDq04C402.setColumns(9);
        this.panel2.add((Component)this.anexoDq04C402, CC.xy(5, 11));
        this.anexoDq04C402a.setColumns(5);
        this.panel2.add((Component)this.anexoDq04C402a, CC.xy(9, 11));
        this.panel2.add((Component)this.anexoDq04C402b, CC.xy(17, 11));
        this.panel2.add((Component)this.anexoDq04C402c, CC.xy(22, 11));
        this.panel2.add((Component)this.anexoDq04C402d, CC.xy(27, 11));
        this.panel2.add((Component)this.anexoDq04C402e, CC.xy(31, 11));
        this.anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num.setText(bundle.getString("Quadro04Panel.anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num.text"));
        this.anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num.setBorder(new EtchedBorder());
        this.anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoDq04C403_num\u00a3anexoDq04C403a_num\u00a3anexoDq04C403b_num\u00a3anexoDq04C403c_num\u00a3anexoDq04C403d_num\u00a3anexoDq04C403e_num, CC.xy(3, 13));
        this.anexoDq04C403.setColumns(9);
        this.panel2.add((Component)this.anexoDq04C403, CC.xy(5, 13));
        this.anexoDq04C403a.setColumns(5);
        this.panel2.add((Component)this.anexoDq04C403a, CC.xy(9, 13));
        this.panel2.add((Component)this.anexoDq04C403b, CC.xy(17, 13));
        this.panel2.add((Component)this.anexoDq04C403c, CC.xy(22, 13));
        this.panel2.add((Component)this.anexoDq04C403d, CC.xy(27, 13));
        this.panel2.add((Component)this.anexoDq04C403e, CC.xy(31, 13));
        this.this2.add((Component)this.panel2, CC.xy(2, 2));
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, 55dlu, $rgap, 11dlu, $lcgap, 36dlu, 0px, $lcgap, 11dlu, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 25dlu, $rgap, 80dlu, $lcgap, default:grow, $rgap", "8*(default, $lgap), default"));
        this.label20.setText(bundle.getString("Quadro04Panel.label20.text"));
        this.label20.setHorizontalAlignment(0);
        this.label20.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label20, CC.xywh(1, 1, 33, 1));
        this.anexoDq04C431_label\u00a3anexoDq04C432_label.setText(bundle.getString("Quadro04Panel.anexoDq04C431_label\u00a3anexoDq04C432_label.text"));
        this.anexoDq04C431_label\u00a3anexoDq04C432_label.setHorizontalAlignment(0);
        this.anexoDq04C431_label\u00a3anexoDq04C432_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoDq04C431_label\u00a3anexoDq04C432_label, CC.xywh(3, 5, 3, 1));
        this.anexoDq04C431a_label\u00a3anexoDq04C432a_label.setText(bundle.getString("Quadro04Panel.anexoDq04C431a_label\u00a3anexoDq04C432a_label.text"));
        this.anexoDq04C431a_label\u00a3anexoDq04C432a_label.setHorizontalAlignment(0);
        this.anexoDq04C431a_label\u00a3anexoDq04C432a_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoDq04C431a_label\u00a3anexoDq04C432a_label, CC.xywh(7, 5, 6, 1));
        this.anexoDq04C431b_label\u00a3anexoDq04C432b_label.setText(bundle.getString("Quadro04Panel.anexoDq04C431b_label\u00a3anexoDq04C432b_label.text"));
        this.anexoDq04C431b_label\u00a3anexoDq04C432b_label.setHorizontalAlignment(0);
        this.anexoDq04C431b_label\u00a3anexoDq04C432b_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoDq04C431b_label\u00a3anexoDq04C432b_label, CC.xywh(14, 5, 6, 1));
        this.anexoDq04C431c_label\u00a3anexoDq04C432c_label.setText(bundle.getString("Quadro04Panel.anexoDq04C431c_label\u00a3anexoDq04C432c_label.text"));
        this.anexoDq04C431c_label\u00a3anexoDq04C432c_label.setBorder(new EtchedBorder());
        this.anexoDq04C431c_label\u00a3anexoDq04C432c_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoDq04C431c_label\u00a3anexoDq04C432c_label, CC.xy(22, 5));
        this.anexoDq04C431d_label\u00a3anexoDq04C432d_label.setText(bundle.getString("Quadro04Panel.anexoDq04C431d_label\u00a3anexoDq04C432d_label.text"));
        this.anexoDq04C431d_label\u00a3anexoDq04C432d_label.setHorizontalAlignment(0);
        this.anexoDq04C431d_label\u00a3anexoDq04C432d_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoDq04C431d_label\u00a3anexoDq04C432d_label, CC.xywh(24, 5, 6, 1));
        this.anexoDq04C431e_label\u00a3anexoDq04C432e_label.setText(bundle.getString("Quadro04Panel.anexoDq04C431e_label\u00a3anexoDq04C432e_label.text"));
        this.anexoDq04C431e_label\u00a3anexoDq04C432e_label.setHorizontalAlignment(0);
        this.anexoDq04C431e_label\u00a3anexoDq04C432e_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoDq04C431e_label\u00a3anexoDq04C432e_label, CC.xy(31, 5));
        this.label27.setText(bundle.getString("Quadro04Panel.label27.text"));
        this.label27.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label27, CC.xywh(3, 7, 3, 1));
        this.anexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label.setText(bundle.getString("Quadro04Panel.anexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label.text"));
        this.anexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoDq04C431b_base_label\u00a3anexoDq04C432b_base_label, CC.xy(17, 7));
        this.anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num.setText(bundle.getString("Quadro04Panel.anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num.text"));
        this.anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num.setBorder(new EtchedBorder());
        this.anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoDq04C431_num\u00a3anexoDq04C431a_num\u00a3anexoDq04C431b_num\u00a3anexoDq04C431c_num\u00a3anexoDq04C431d_num\u00a3anexoDq04C431e_num, CC.xy(3, 9));
        this.anexoDq04C431.setColumns(9);
        this.panel3.add((Component)this.anexoDq04C431, CC.xy(5, 9));
        this.anexoDq04C431a.setColumns(5);
        this.panel3.add((Component)this.anexoDq04C431a, CC.xy(9, 9));
        this.anexoDq04C431b.setAllowNegative(true);
        this.panel3.add((Component)this.anexoDq04C431b, CC.xy(17, 9));
        this.panel3.add((Component)this.anexoDq04C431c, CC.xy(22, 9));
        this.panel3.add((Component)this.anexoDq04C431d, CC.xy(27, 9));
        this.panel3.add((Component)this.anexoDq04C431e, CC.xy(31, 9));
        this.anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num.setText(bundle.getString("Quadro04Panel.anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num.text"));
        this.anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num.setBorder(new EtchedBorder());
        this.anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoDq04C432_num\u00a3anexoDq04C432a_num\u00a3anexoDq04C432b_num\u00a3anexoDq04C432c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C432e_num, CC.xy(3, 11));
        this.anexoDq04C432.setColumns(9);
        this.panel3.add((Component)this.anexoDq04C432, CC.xy(5, 11));
        this.anexoDq04C432a.setColumns(5);
        this.panel3.add((Component)this.anexoDq04C432a, CC.xy(9, 11));
        this.anexoDq04C432b.setAllowNegative(true);
        this.panel3.add((Component)this.anexoDq04C432b, CC.xy(17, 11));
        this.panel3.add((Component)this.anexoDq04C432c, CC.xy(22, 11));
        this.panel3.add((Component)this.anexoDq04C432d, CC.xy(27, 11));
        this.panel3.add((Component)this.anexoDq04C432e, CC.xy(31, 11));
        this.label39.setText(bundle.getString("Quadro04Panel.label39.text"));
        this.label39.setHorizontalAlignment(0);
        this.label39.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label39, CC.xywh(1, 15, 33, 1));
        this.this2.add((Component)this.panel3, CC.xy(2, 8));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("$ugap, 2*(default, $lcgap), default:grow, $rgap", "$ugap, 2*($lgap, default), $lgap, fill:121dlu, 3*($lgap, default)"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoDq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoDq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel5.add((Component)this.toolBar1, CC.xy(2, 5));
        this.anexoDq04T1Scroll.setViewportView(this.anexoDq04T1);
        this.panel5.add((Component)this.anexoDq04T1Scroll, CC.xywh(2, 7, 5, 1));
        this.label40.setText(bundle.getString("Quadro04Panel.label40.text"));
        this.label40.setHorizontalAlignment(0);
        this.label40.setBorder(new EtchedBorder());
        this.panel5.add((Component)this.label40, CC.xywh(1, 13, 6, 1));
        this.this2.add((Component)this.panel5, CC.xy(2, 10));
        this.panel1.setLayout(new FormLayout("default, $lcgap, 25dlu, $lcgap, 241dlu, $lcgap, 109dlu, $lcgap, 169dlu", "2*(default, $lgap), 23dlu, $lgap, default, $lgap, 22dlu, $lgap, default"));
        this.label4.setText(bundle.getString("Quadro04Panel.label4.text"));
        this.panel1.add((Component)this.label4, CC.xy(9, 3, CC.CENTER, CC.DEFAULT));
        this.anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3.setText(bundle.getString("Quadro04Panel.anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3.text"));
        this.anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3.setBorder(new EtchedBorder());
        this.anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3.setHorizontalAlignment(0);
        this.panel1.add((Component)this.anexoDq04C481_num\u00a3anexoDq04C480a_num\u00a3anexoDq04C480b_num\u00a3anexoDq04C480c_num\u00a3anexoDq04C480d_num\u00a3anexoDq04C480e_num3, CC.xy(3, 5));
        this.label2.setText(bundle.getString("Quadro04Panel.label2.text"));
        this.panel1.add((Component)this.label2, CC.xy(5, 5, CC.DEFAULT, CC.CENTER));
        this.anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3.setText(bundle.getString("Quadro04Panel.anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C481d_num\u00a3anexoDq04C481e_num3.text"));
        this.anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3.setBorder(new EtchedBorder());
        this.anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3.setHorizontalAlignment(0);
        this.panel1.add((Component)this.anexoDq04C481_num\u00a3anexoDq04C481a_num\u00a3anexoDq04C481b_num\u00a3anexoDq04C481c_num\u00a3anexoDq04C432d_num\u00a3anexoDq04C481e_num3, CC.xy(3, 9));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.panel1.add((Component)this.label3, CC.xy(5, 9));
        this.label5.setText(bundle.getString("Quadro04Panel.label5.text"));
        this.panel1.add((Component)this.label5, CC.xy(9, 7, CC.CENTER, CC.DEFAULT));
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3.setText(bundle.getString("Quadro04Panel.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3.text"));
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3.setHorizontalAlignment(0);
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label3, CC.xy(7, 1));
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4.setText(bundle.getString("Quadro04Panel.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4.text"));
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4.setHorizontalAlignment(0);
        this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.anexoDq04C401_label\u00a3anexoDq04C402_label\u00a3anexoDq04C403_label4, CC.xy(9, 1));
        this.panel8.setLayout(new FormLayout("23dlu, $lcgap, 49dlu, $lcgap, 21dlu", "default"));
        this.anexoDq04C480a.setColumns(5);
        this.panel8.add((Component)this.anexoDq04C480a, CC.xy(3, 1));
        this.panel1.add((Component)this.panel8, CC.xy(7, 5));
        this.panel6.setLayout(new FormLayout("36dlu, $lcgap, 91dlu, $lcgap, 21dlu", "default"));
        this.anexoDq04C480b.setAllowNegative(true);
        this.panel6.add((Component)this.anexoDq04C480b, CC.xy(3, 1));
        this.panel1.add((Component)this.panel6, CC.xy(9, 5));
        this.panel9.setLayout(new FormLayout("23dlu, $lcgap, 49dlu, $lcgap, 21dlu", "default"));
        this.anexoDq04C481a.setColumns(5);
        this.panel9.add((Component)this.anexoDq04C481a, CC.xy(3, 1));
        this.panel1.add((Component)this.panel9, CC.xy(7, 9));
        this.panel7.setLayout(new FormLayout("36dlu, $lcgap, 91dlu, $lcgap, 21dlu", "default"));
        this.anexoDq04C481b.setAllowNegative(true);
        this.panel7.add((Component)this.anexoDq04C481b, CC.xy(3, 1));
        this.panel1.add((Component)this.panel7, CC.xy(9, 9));
        this.this2.add((Component)this.panel1, CC.xy(2, 12));
        this.panel4.setLayout(new FormLayout("default:grow, 2*(0px), $lcgap, default, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 25dlu, $rgap, 0px, 80dlu, $lcgap, 24dlu, $rgap, 0px, 95dlu, $lcgap, 24dlu, $rgap, 80dlu, $lcgap, default:grow, $rgap", "2*(default, $lgap), default"));
        this.panel4.add((Component)this.anexoDq05C502_base_label3, CC.xy(1, 1));
        this.anexoDq04C1_label.setText(bundle.getString("Quadro04Panel.anexoDq04C1_label.text"));
        this.anexoDq04C1_label.setHorizontalAlignment(0);
        this.anexoDq04C1_label.setBorder(new EtchedBorder());
        this.panel4.add((Component)this.anexoDq04C1_label, CC.xywh(7, 1, 6, 1));
        this.anexoDq04C2_label.setText(bundle.getString("Quadro04Panel.anexoDq04C2_label.text"));
        this.anexoDq04C2_label.setBorder(new EtchedBorder());
        this.anexoDq04C2_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoDq04C2_label, CC.xy(15, 1));
        this.anexoDq04C3_label.setText(bundle.getString("Quadro04Panel.anexoDq04C3_label.text"));
        this.anexoDq04C3_label.setHorizontalAlignment(0);
        this.anexoDq04C3_label.setBorder(new EtchedBorder());
        this.panel4.add((Component)this.anexoDq04C3_label, CC.xywh(17, 1, 6, 1));
        this.anexoDq04C4_label.setText(bundle.getString("Quadro04Panel.anexoDq04C4_label.text"));
        this.anexoDq04C4_label.setHorizontalAlignment(0);
        this.anexoDq04C4_label.setBorder(new EtchedBorder());
        this.panel4.add((Component)this.anexoDq04C4_label, CC.xy(24, 1));
        this.anexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label.setText(bundle.getString("Quadro04Panel.anexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label.text"));
        this.anexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoDq04C1_base_label\u00a3anexoDq04C2_base_label\u00a3anexoDq04C3_base_label\u00a3anexoDq04C4_base_label, CC.xywh(5, 3, 3, 1));
        this.anexoDq04C1.setEditable(false);
        this.anexoDq04C1.setColumns(15);
        this.anexoDq04C1.setAllowNegative(true);
        this.panel4.add((Component)this.anexoDq04C1, CC.xy(10, 3));
        this.anexoDq04C2.setEditable(false);
        this.anexoDq04C2.setColumns(15);
        this.panel4.add((Component)this.anexoDq04C2, CC.xy(15, 3));
        this.anexoDq04C3.setEditable(false);
        this.anexoDq04C3.setColumns(15);
        this.panel4.add((Component)this.anexoDq04C3, CC.xy(20, 3));
        this.anexoDq04C4.setEditable(false);
        this.anexoDq04C4.setColumns(15);
        this.panel4.add((Component)this.anexoDq04C4, CC.xy(24, 3));
        this.this2.add((Component)this.panel4, CC.xy(2, 16));
        this.panel10.setMinimumSize(null);
        this.panel10.setPreferredSize(null);
        this.panel10.setLayout(new FormLayout("15dlu, default:grow, 2*($lcgap, default), $lcgap, 347dlu:grow, $lcgap, default:grow, $lcgap, $rgap", "2*(default, $lgap), fill:165dlu, $lgap, default"));
        this.label6.setText(bundle.getString("Quadro04Panel.label6.text"));
        this.label6.setBorder(new EtchedBorder());
        this.label6.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label6, CC.xy(1, 1));
        this.label7.setText(bundle.getString("Quadro04Panel.label7.text"));
        this.label7.setBorder(new EtchedBorder());
        this.label7.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label7, CC.xywh(2, 1, 9, 1));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button3.setText(bundle.getString("Quadro04Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoDq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro04Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoDq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel10.add((Component)this.toolBar2, CC.xy(4, 3));
        this.anexoDq04T2Scroll.setViewportView(this.anexoDq04T2);
        this.panel10.add((Component)this.anexoDq04T2Scroll, CC.xywh(4, 5, 5, 1));
        this.this2.add((Component)this.panel10, CC.xywh(1, 18, 2, 1));
        this.add((Component)this.this2, "Center");
    }

}

