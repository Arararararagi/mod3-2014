/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexod;

import pt.dgci.modelo3irs.v2015.binding.anexod.Quadro07Bindings;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro07PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro07PanelExtension
extends Quadro07PanelBase
implements IBindablePanel<Quadro07> {
    public Quadro07PanelExtension(Quadro07 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro07 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro07Bindings.doBindings(model, this);
        }
    }
}

