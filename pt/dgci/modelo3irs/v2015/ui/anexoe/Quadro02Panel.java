/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoe;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro02Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JLabel anexoEq02C01_label;
    protected JLabelTextFieldNumbering label3;
    protected JEditableComboBox anexoEq02C01;

    public Quadro02Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoEq02C01_label() {
        return this.anexoEq02C01_label;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JEditableComboBox getAnexoEq02C01() {
        return this.anexoEq02C01;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoE");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.anexoEq02C01_label = new JLabel();
        this.label3 = new JLabelTextFieldNumbering();
        this.anexoEq02C01 = new JEditableComboBox();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro02Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro02Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $rgap, left:default, $rgap, 13dlu, 0px, $lcgap, 40dlu, $rgap, default:grow", "$rgap, default, $lgap, default"));
        this.anexoEq02C01_label.setText(bundle.getString("Quadro02Panel.anexoEq02C01_label.text"));
        this.anexoEq02C01_label.setHorizontalAlignment(4);
        this.this2.add((Component)this.anexoEq02C01_label, cc.xy(4, 2));
        this.label3.setText(bundle.getString("Quadro02Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.this2.add((Component)this.label3, cc.xy(6, 2));
        this.anexoEq02C01.setMinimumSize(null);
        this.anexoEq02C01.setPreferredSize(null);
        this.anexoEq02C01.setEditable(false);
        this.anexoEq02C01.setEnabled(false);
        this.this2.add((Component)this.anexoEq02C01, cc.xy(9, 2));
        this.add((Component)this.this2, "Center");
    }
}

