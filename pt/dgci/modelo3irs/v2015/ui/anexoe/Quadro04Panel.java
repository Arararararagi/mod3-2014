/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoe;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel5;
    protected JLabel label13;
    protected JLabel label3;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoEq04T1Scroll;
    protected JTable anexoEq04T1;
    protected JPanel panel4;
    protected JLabel anexoEq04C1_label;
    protected JLabel anexoEq04C2_label;
    protected JLabel anexoEq04C1_base_label\u00a3anexoEq04C2_base_label;
    protected JMoneyTextField anexoEq04C1;
    protected JMoneyTextField anexoEq04C2;
    protected JPanel panel8;
    protected JLabel label14;
    protected JLabel label4;
    protected JPanel panel6;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoEq04T2Scroll;
    protected JTable anexoEq04T2;
    protected JPanel panel7;
    protected JLabel anexoEq04C3_label;
    protected JLabel anexoEq04C4_label;
    protected JLabel anexoEq04C3_base_label\u00a3anexoEq04C4_base_label;
    protected JMoneyTextField anexoEq04C3;
    protected JMoneyTextField anexoEq04C4;
    protected JPanel panel9;
    protected JLabel anexoEq04B1OPSim_base_label;
    protected JLabelTextFieldNumbering label22;
    protected JRadioButton anexoEq04B1OP1;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton anexoEq04B1OP2;

    public Quadro04Panel() {
        this.initComponents();
    }

    protected void addLineAnexoEq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoEq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoEq04T1Scroll() {
        return this.anexoEq04T1Scroll;
    }

    public JTable getAnexoEq04T1() {
        return this.anexoEq04T1;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoEq04C1_label() {
        return this.anexoEq04C1_label;
    }

    public JLabel getAnexoEq04C2_label() {
        return this.anexoEq04C2_label;
    }

    public JLabel getAnexoEq04C1_base_label\u00a3anexoEq04C2_base_label() {
        return this.anexoEq04C1_base_label\u00a3anexoEq04C2_base_label;
    }

    public JMoneyTextField getAnexoEq04C1() {
        return this.anexoEq04C1;
    }

    public JMoneyTextField getAnexoEq04C2() {
        return this.anexoEq04C2;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoEq04T2Scroll() {
        return this.anexoEq04T2Scroll;
    }

    public JTable getAnexoEq04T2() {
        return this.anexoEq04T2;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoEq04C3_label() {
        return this.anexoEq04C3_label;
    }

    public JLabel getAnexoEq04C4_label() {
        return this.anexoEq04C4_label;
    }

    public JLabel getAnexoEq04C3_base_label\u00a3anexoEq04C4_base_label() {
        return this.anexoEq04C3_base_label\u00a3anexoEq04C4_base_label;
    }

    public JMoneyTextField getAnexoEq04C3() {
        return this.anexoEq04C3;
    }

    public JMoneyTextField getAnexoEq04C4() {
        return this.anexoEq04C4;
    }

    protected void addLineAnexoEq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoEq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabel getAnexoEq04B1OPSim_base_label() {
        return this.anexoEq04B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JRadioButton getAnexoEq04B1OP1() {
        return this.anexoEq04B1OP1;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getAnexoEq04B1OP2() {
        return this.anexoEq04B1OP2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoE");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel5 = new JPanel();
        this.label13 = new JLabel();
        this.label3 = new JLabel();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoEq04T1Scroll = new JScrollPane();
        this.anexoEq04T1 = new JTable();
        this.panel4 = new JPanel();
        this.anexoEq04C1_label = new JLabel();
        this.anexoEq04C2_label = new JLabel();
        this.anexoEq04C1_base_label\u00a3anexoEq04C2_base_label = new JLabel();
        this.anexoEq04C1 = new JMoneyTextField();
        this.anexoEq04C2 = new JMoneyTextField();
        this.panel8 = new JPanel();
        this.label14 = new JLabel();
        this.label4 = new JLabel();
        this.panel6 = new JPanel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoEq04T2Scroll = new JScrollPane();
        this.anexoEq04T2 = new JTable();
        this.panel7 = new JPanel();
        this.anexoEq04C3_label = new JLabel();
        this.anexoEq04C4_label = new JLabel();
        this.anexoEq04C3_base_label\u00a3anexoEq04C4_base_label = new JLabel();
        this.anexoEq04C3 = new JMoneyTextField();
        this.anexoEq04C4 = new JMoneyTextField();
        this.panel9 = new JPanel();
        this.anexoEq04B1OPSim_base_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoEq04B1OP1 = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.anexoEq04B1OP2 = new JRadioButton();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $lcgap, default", "$rgap, 10*(default, $lgap), default"));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap", "default"));
        this.label13.setText(bundle.getString("Quadro04Panel.label13.text"));
        this.label13.setBorder(new EtchedBorder());
        this.label13.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label13, cc.xy(1, 1));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel5.add((Component)this.label3, cc.xywh(3, 1, 2, 1));
        this.this2.add((Component)this.panel5, cc.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 520dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoEq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoEq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoEq04T1Scroll.setViewportView(this.anexoEq04T1);
        this.panel3.add((Component)this.anexoEq04T1Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, default, 2*($lcgap, 80dlu), $lcgap, default:grow", "2*(default, $lgap), default"));
        this.anexoEq04C1_label.setText(bundle.getString("Quadro04Panel.anexoEq04C1_label.text"));
        this.anexoEq04C1_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoEq04C1_label, cc.xywh(5, 1, 2, 1));
        this.anexoEq04C2_label.setText(bundle.getString("Quadro04Panel.anexoEq04C2_label.text"));
        this.anexoEq04C2_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoEq04C2_label, cc.xy(7, 1));
        this.anexoEq04C1_base_label\u00a3anexoEq04C2_base_label.setText(bundle.getString("Quadro04Panel.anexoEq04C1_base_label\u00a3anexoEq04C2_base_label.text"));
        this.panel4.add((Component)this.anexoEq04C1_base_label\u00a3anexoEq04C2_base_label, cc.xy(3, 3));
        this.anexoEq04C1.setEditable(false);
        this.anexoEq04C1.setColumns(15);
        this.panel4.add((Component)this.anexoEq04C1, cc.xy(5, 3));
        this.anexoEq04C2.setEditable(false);
        this.anexoEq04C2.setColumns(15);
        this.panel4.add((Component)this.anexoEq04C2, cc.xy(7, 3));
        this.this2.add((Component)this.panel4, cc.xy(2, 6));
        this.panel8.setMinimumSize(null);
        this.panel8.setPreferredSize(null);
        this.panel8.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap", "default"));
        this.label14.setText(bundle.getString("Quadro04Panel.label14.text"));
        this.label14.setBorder(new EtchedBorder());
        this.label14.setHorizontalAlignment(0);
        this.panel8.add((Component)this.label14, cc.xy(1, 1));
        this.label4.setText(bundle.getString("Quadro04Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel8.add((Component)this.label4, cc.xywh(3, 1, 2, 1));
        this.this2.add((Component)this.panel8, cc.xy(2, 12));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 520dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:165dlu, $lgap, default"));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button3.setText(bundle.getString("Quadro04Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoEq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro04Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoEq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel6.add((Component)this.toolBar2, cc.xy(3, 2));
        this.anexoEq04T2Scroll.setViewportView(this.anexoEq04T2);
        this.panel6.add((Component)this.anexoEq04T2Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel6, cc.xy(2, 14));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("default:grow, $lcgap, default, 2*($lcgap, 80dlu), $lcgap, default:grow", "2*(default, $lgap), default"));
        this.anexoEq04C3_label.setText(bundle.getString("Quadro04Panel.anexoEq04C3_label.text"));
        this.anexoEq04C3_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoEq04C3_label, cc.xywh(5, 1, 2, 1));
        this.anexoEq04C4_label.setText(bundle.getString("Quadro04Panel.anexoEq04C4_label.text"));
        this.anexoEq04C4_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoEq04C4_label, cc.xy(7, 1));
        this.anexoEq04C3_base_label\u00a3anexoEq04C4_base_label.setText(bundle.getString("Quadro04Panel.anexoEq04C3_base_label\u00a3anexoEq04C4_base_label.text"));
        this.panel7.add((Component)this.anexoEq04C3_base_label\u00a3anexoEq04C4_base_label, cc.xy(3, 3));
        this.anexoEq04C3.setEditable(false);
        this.anexoEq04C3.setColumns(15);
        this.panel7.add((Component)this.anexoEq04C3, cc.xy(5, 3));
        this.anexoEq04C4.setEditable(false);
        this.anexoEq04C4.setColumns(15);
        this.panel7.add((Component)this.anexoEq04C4, cc.xy(7, 3));
        this.this2.add((Component)this.panel7, cc.xy(2, 16));
        this.panel9.setBorder(new EtchedBorder());
        this.panel9.setLayout(new FormLayout("default, $lcgap, 100dlu:grow, $lcgap, default, $lcgap, 13dlu, 0px, default, $rgap, default, $lcgap, 13dlu, 0px, default, $ugap", "$rgap, default, $rgap"));
        this.anexoEq04B1OPSim_base_label.setText(bundle.getString("Quadro04Panel.anexoEq04B1OPSim_base_label.text"));
        this.anexoEq04B1OPSim_base_label.setHorizontalAlignment(2);
        this.panel9.add((Component)this.anexoEq04B1OPSim_base_label, cc.xy(3, 2));
        this.label22.setText(bundle.getString("Quadro04Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel9.add((Component)this.label22, cc.xy(7, 2));
        this.anexoEq04B1OP1.setText(bundle.getString("Quadro04Panel.anexoEq04B1OP1.text"));
        this.panel9.add((Component)this.anexoEq04B1OP1, cc.xy(9, 2));
        this.label23.setText(bundle.getString("Quadro04Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel9.add((Component)this.label23, cc.xy(13, 2));
        this.anexoEq04B1OP2.setText(bundle.getString("Quadro04Panel.anexoEq04B1OP2.text"));
        this.panel9.add((Component)this.anexoEq04B1OP2, cc.xy(15, 2));
        this.this2.add((Component)this.panel9, cc.xy(2, 20));
        this.add((Component)this.this2, "Center");
    }

}

