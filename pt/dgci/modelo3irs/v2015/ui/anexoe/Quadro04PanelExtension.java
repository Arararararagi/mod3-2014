/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoe;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import java.util.Observable;
import java.util.Observer;
import pt.dgci.modelo3irs.v2015.binding.anexoe.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoe.Quadro04PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroObserver;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04>,
RostoQuadroObserver {
    public static final int MAX_LINES_Q04_T1 = 49;
    public static final int MAX_LINES_Q04_T2 = 49;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        rosto.getRostoq07CT1ChangeEvent().addObserver(this);
        rosto.getRostoq07ET1ChangeEvent().addObserver(this);
        rosto.getRostoq03DT1ChangeEvent().addObserver(this);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoEq04T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoEq04T1() != null && this.model.getAnexoEq04T1().size() >= 49) {
            return;
        }
        super.addLineAnexoEq04T1_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoEq04T2_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoEq04T2() != null && this.model.getAnexoEq04T2().size() >= 49) {
            return;
        }
        super.addLineAnexoEq04T2_LinhaActionPerformed(e);
    }

    @Override
    public void update(Observable obs, Object arg) {
        if (obs instanceof RostoQuadroChangeEvent) {
            String label = ((RostoQuadroChangeEvent)obs).getPrefixoTitulares();
            AnexoEModel anexoE = (AnexoEModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(AnexoEModel.class);
            if (anexoE != null) {
                EventList<AnexoEq04T1_Linha> anexoEq04T1Rows = anexoE.getQuadro04().getAnexoEq04T1();
                EventList<AnexoEq04T2_Linha> anexoEq04T2Rows = anexoE.getQuadro04().getAnexoEq04T2();
                if (arg instanceof RostoQuadroChangeState) {
                    int index = ((RostoQuadroChangeState)arg).getRowIndex();
                    if (index == -1) {
                        this.removeNifByLabelInEq04T1(anexoEq04T1Rows, label);
                        this.removeNifByLabelInEq04T2(anexoEq04T2Rows, label);
                    } else {
                        boolean isLastRow = ((RostoQuadroChangeState)arg).isLastRow();
                        RostoQuadroChangeState.OperationEnum operation = ((RostoQuadroChangeState)arg).getOperation();
                        this.removeNifByLabelInEq04T1(anexoEq04T1Rows, label + (index + 1));
                        this.removeNifByLabelInEq04T2(anexoEq04T2Rows, label + (index + 1));
                        if (!isLastRow && RostoQuadroChangeState.OperationEnum.DELETE.equals((Object)operation)) {
                            this.reindexEq04T1Elements(anexoEq04T1Rows, label, index);
                            this.reindexEq04T2Elements(anexoEq04T2Rows, label, index);
                        }
                    }
                } else {
                    this.removeNifByLabelInEq04T1(anexoEq04T1Rows, label);
                    this.removeNifByLabelInEq04T2(anexoEq04T2Rows, label);
                }
            }
        }
    }

    private void removeNifByLabelInEq04T2(EventList<AnexoEq04T2_Linha> anexoEq04T2Rows, String labelPrefix) {
        if (anexoEq04T2Rows != null) {
            for (AnexoEq04T2_Linha anexoEq04T2_Linha : anexoEq04T2Rows) {
                if (anexoEq04T2_Linha == null || anexoEq04T2_Linha.getTitular() == null || !anexoEq04T2_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoEq04T2_Linha.setTitular(null);
            }
        }
    }

    private void removeNifByLabelInEq04T1(EventList<AnexoEq04T1_Linha> anexoEq04T1Rows, String labelPrefix) {
        if (anexoEq04T1Rows != null) {
            for (AnexoEq04T1_Linha anexoEq04T1_Linha : anexoEq04T1Rows) {
                if (anexoEq04T1_Linha == null || anexoEq04T1_Linha.getTitular() == null || !anexoEq04T1_Linha.getTitular().startsWith(labelPrefix)) continue;
                anexoEq04T1_Linha.setTitular(null);
            }
        }
    }

    private void reindexEq04T2Elements(EventList<AnexoEq04T2_Linha> anexoEq04T2Rows, String labelPrefix, int index) {
        if (anexoEq04T2Rows != null) {
            for (AnexoEq04T2_Linha anexoEq04T2_Linha : anexoEq04T2Rows) {
                int currIndex;
                if (anexoEq04T2_Linha == null || anexoEq04T2_Linha.getTitular() == null || !anexoEq04T2_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoEq04T2_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoEq04T2_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }

    private void reindexEq04T1Elements(EventList<AnexoEq04T1_Linha> anexoEq04T1Rows, String labelPrefix, int index) {
        if (anexoEq04T1Rows != null) {
            for (AnexoEq04T1_Linha anexoEq04T1_Linha : anexoEq04T1Rows) {
                int currIndex;
                if (anexoEq04T1_Linha == null || anexoEq04T1_Linha.getTitular() == null || !anexoEq04T1_Linha.getTitular().startsWith(labelPrefix) || (currIndex = Modelo3IRSValidatorUtil.getRowNumberFromTitular(anexoEq04T1_Linha.getTitular(), labelPrefix)) == -1 || currIndex <= index) continue;
                anexoEq04T1_Linha.setTitular(labelPrefix + (currIndex - 1));
            }
        }
    }
}

