/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoi;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JLabel anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label;
    protected JLabel label3;
    protected JLabel anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label;
    protected JLabel label1;
    protected JLabel anexoIq05C501_base_label\u00a3anexoIq05C501a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C501_num\u00a3anexoIq05C501a_num;
    protected JMoneyTextField anexoIq05C501;
    protected JLabel label6;
    protected JMoneyTextField anexoIq05C501a;
    protected JLabel anexoIq05C502_base_label\u00a3anexoIq05C502a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C502_num\u00a3anexoIq05C502a_num;
    protected JMoneyTextField anexoIq05C502;
    protected JLabel label7;
    protected JMoneyTextField anexoIq05C502a;
    protected JLabel anexoIq05C1_base_label\u00a3anexoIq05C1a_base_label;
    protected JMoneyTextField anexoIq05C1;
    protected JMoneyTextField anexoIq05C1a;
    protected JLabel label2;
    protected JLabel anexoIq05C504_base_label\u00a3anexoIq05C504a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C504_num\u00a3anexoIq05C504a_num;
    protected JMoneyTextField anexoIq05C504;
    protected JLabel label8;
    protected JMoneyTextField anexoIq05C504a;
    protected JLabel anexoIq05C505_base_label\u00a3anexoIq05C505a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C505_num\u00a3anexoIq05C505a_num;
    protected JMoneyTextField anexoIq05C505;
    protected JLabel label9;
    protected JMoneyTextField anexoIq05C505a;
    protected JLabel anexoIq05C506_base_label\u00a3anexoIq05C506a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C506_num\u00a3anexoIq05C506a_num;
    protected JMoneyTextField anexoIq05C506;
    protected JLabel label11;
    protected JMoneyTextField anexoIq05C506a;
    protected JLabel anexoIq05C507_base_label\u00a3anexoIq05C507a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C507_num\u00a3anexoIq05C507a_num;
    protected JMoneyTextField anexoIq05C507;
    protected JLabel label12;
    protected JMoneyTextField anexoIq05C507a;
    protected JLabel anexoIq05C508_base_label\u00a3anexoIq05C508a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C508_num\u00a3anexoIq05C508a_num;
    protected JMoneyTextField anexoIq05C508;
    protected JLabel label13;
    protected JMoneyTextField anexoIq05C508a;
    protected JLabel anexoIq05C2_base_label\u00a3anexoIq05C2a_base_label;
    protected JMoneyTextField anexoIq05C2;
    protected JMoneyTextField anexoIq05C2a;
    protected JLabel anexoIq05C503_base_label\u00a3anexoIq05C503a_base_label;
    protected JLabelTextFieldNumbering anexoIq05C503_num;
    protected JMoneyTextField anexoIq05C503;

    public Quadro05Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoIq05C501_base_label\u00a3anexoIq05C501a_base_label() {
        return this.anexoIq05C501_base_label\u00a3anexoIq05C501a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C501_num\u00a3anexoIq05C501a_num() {
        return this.anexoIq05C501_num\u00a3anexoIq05C501a_num;
    }

    public JMoneyTextField getAnexoIq05C501() {
        return this.anexoIq05C501;
    }

    public JLabel getAnexoIq05C502_base_label\u00a3anexoIq05C502a_base_label() {
        return this.anexoIq05C502_base_label\u00a3anexoIq05C502a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C502_num\u00a3anexoIq05C502a_num() {
        return this.anexoIq05C502_num\u00a3anexoIq05C502a_num;
    }

    public JMoneyTextField getAnexoIq05C502() {
        return this.anexoIq05C502;
    }

    public JLabel getAnexoIq05C1_base_label\u00a3anexoIq05C1a_base_label() {
        return this.anexoIq05C1_base_label\u00a3anexoIq05C1a_base_label;
    }

    public JMoneyTextField getAnexoIq05C1() {
        return this.anexoIq05C1;
    }

    public JLabel getAnexoIq05C503_base_label\u00a3anexoIq05C503a_base_label() {
        return this.anexoIq05C503_base_label\u00a3anexoIq05C503a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C503_num() {
        return this.anexoIq05C503_num;
    }

    public JMoneyTextField getAnexoIq05C503() {
        return this.anexoIq05C503;
    }

    public JLabel getAnexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label() {
        return this.anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabel getAnexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label() {
        return this.anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label;
    }

    public JMoneyTextField getAnexoIq05C501a() {
        return this.anexoIq05C501a;
    }

    public JMoneyTextField getAnexoIq05C502a() {
        return this.anexoIq05C502a;
    }

    public JMoneyTextField getAnexoIq05C1a() {
        return this.anexoIq05C1a;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getAnexoIq05C504_base_label\u00a3anexoIq05C504a_base_label() {
        return this.anexoIq05C504_base_label\u00a3anexoIq05C504a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C504_num\u00a3anexoIq05C504a_num() {
        return this.anexoIq05C504_num\u00a3anexoIq05C504a_num;
    }

    public JMoneyTextField getAnexoIq05C504() {
        return this.anexoIq05C504;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JMoneyTextField getAnexoIq05C504a() {
        return this.anexoIq05C504a;
    }

    public JLabel getAnexoIq05C505_base_label\u00a3anexoIq05C505a_base_label() {
        return this.anexoIq05C505_base_label\u00a3anexoIq05C505a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C505_num\u00a3anexoIq05C505a_num() {
        return this.anexoIq05C505_num\u00a3anexoIq05C505a_num;
    }

    public JMoneyTextField getAnexoIq05C505() {
        return this.anexoIq05C505;
    }

    public JLabel getLabel9() {
        return this.label9;
    }

    public JMoneyTextField getAnexoIq05C505a() {
        return this.anexoIq05C505a;
    }

    public JLabel getAnexoIq05C506_base_label\u00a3anexoIq05C506a_base_label() {
        return this.anexoIq05C506_base_label\u00a3anexoIq05C506a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C506_num\u00a3anexoIq05C506a_num() {
        return this.anexoIq05C506_num\u00a3anexoIq05C506a_num;
    }

    public JMoneyTextField getAnexoIq05C506() {
        return this.anexoIq05C506;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JMoneyTextField getAnexoIq05C506a() {
        return this.anexoIq05C506a;
    }

    public JLabel getAnexoIq05C507_base_label\u00a3anexoIq05C507a_base_label() {
        return this.anexoIq05C507_base_label\u00a3anexoIq05C507a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C507_num\u00a3anexoIq05C507a_num() {
        return this.anexoIq05C507_num\u00a3anexoIq05C507a_num;
    }

    public JMoneyTextField getAnexoIq05C507() {
        return this.anexoIq05C507;
    }

    public JLabel getLabel12() {
        return this.label12;
    }

    public JMoneyTextField getAnexoIq05C507a() {
        return this.anexoIq05C507a;
    }

    public JLabel getAnexoIq05C508_base_label\u00a3anexoIq05C508a_base_label() {
        return this.anexoIq05C508_base_label\u00a3anexoIq05C508a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq05C508_num\u00a3anexoIq05C508a_num() {
        return this.anexoIq05C508_num\u00a3anexoIq05C508a_num;
    }

    public JMoneyTextField getAnexoIq05C508() {
        return this.anexoIq05C508;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JMoneyTextField getAnexoIq05C508a() {
        return this.anexoIq05C508a;
    }

    public JLabel getAnexoIq05C2_base_label\u00a3anexoIq05C2a_base_label() {
        return this.anexoIq05C2_base_label\u00a3anexoIq05C2a_base_label;
    }

    public JMoneyTextField getAnexoIq05C2() {
        return this.anexoIq05C2;
    }

    public JMoneyTextField getAnexoIq05C2a() {
        return this.anexoIq05C2a;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoI");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label = new JLabel();
        this.label3 = new JLabel();
        this.anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label = new JLabel();
        this.label1 = new JLabel();
        this.anexoIq05C501_base_label\u00a3anexoIq05C501a_base_label = new JLabel();
        this.anexoIq05C501_num\u00a3anexoIq05C501a_num = new JLabelTextFieldNumbering();
        this.anexoIq05C501 = new JMoneyTextField();
        this.label6 = new JLabel();
        this.anexoIq05C501a = new JMoneyTextField();
        this.anexoIq05C502_base_label\u00a3anexoIq05C502a_base_label = new JLabel();
        this.anexoIq05C502_num\u00a3anexoIq05C502a_num = new JLabelTextFieldNumbering();
        this.anexoIq05C502 = new JMoneyTextField();
        this.label7 = new JLabel();
        this.anexoIq05C502a = new JMoneyTextField();
        this.anexoIq05C1_base_label\u00a3anexoIq05C1a_base_label = new JLabel();
        this.anexoIq05C1 = new JMoneyTextField();
        this.anexoIq05C1a = new JMoneyTextField();
        this.label2 = new JLabel();
        this.anexoIq05C504_base_label\u00a3anexoIq05C504a_base_label = new JLabel();
        this.anexoIq05C504_num\u00a3anexoIq05C504a_num = new JLabelTextFieldNumbering();
        this.anexoIq05C504 = new JMoneyTextField();
        this.label8 = new JLabel();
        this.anexoIq05C504a = new JMoneyTextField();
        this.anexoIq05C505_base_label\u00a3anexoIq05C505a_base_label = new JLabel();
        this.anexoIq05C505_num\u00a3anexoIq05C505a_num = new JLabelTextFieldNumbering();
        this.anexoIq05C505 = new JMoneyTextField();
        this.label9 = new JLabel();
        this.anexoIq05C505a = new JMoneyTextField();
        this.anexoIq05C506_base_label\u00a3anexoIq05C506a_base_label = new JLabel();
        this.anexoIq05C506_num\u00a3anexoIq05C506a_num = new JLabelTextFieldNumbering();
        this.anexoIq05C506 = new JMoneyTextField();
        this.label11 = new JLabel();
        this.anexoIq05C506a = new JMoneyTextField();
        this.anexoIq05C507_base_label\u00a3anexoIq05C507a_base_label = new JLabel();
        this.anexoIq05C507_num\u00a3anexoIq05C507a_num = new JLabelTextFieldNumbering();
        this.anexoIq05C507 = new JMoneyTextField();
        this.label12 = new JLabel();
        this.anexoIq05C507a = new JMoneyTextField();
        this.anexoIq05C508_base_label\u00a3anexoIq05C508a_base_label = new JLabel();
        this.anexoIq05C508_num\u00a3anexoIq05C508a_num = new JLabelTextFieldNumbering();
        this.anexoIq05C508 = new JMoneyTextField();
        this.label13 = new JLabel();
        this.anexoIq05C508a = new JMoneyTextField();
        this.anexoIq05C2_base_label\u00a3anexoIq05C2a_base_label = new JLabel();
        this.anexoIq05C2 = new JMoneyTextField();
        this.anexoIq05C2a = new JMoneyTextField();
        this.anexoIq05C503_base_label\u00a3anexoIq05C503a_base_label = new JLabel();
        this.anexoIq05C503_num = new JLabelTextFieldNumbering();
        this.anexoIq05C503 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default, $rgap, default, $lcgap, default:grow, $lcgap, 25dlu, $rgap, 90dlu, $rgap, default, $lcgap, 90dlu, $lcgap, default", "17*(default, $lgap), default"));
        this.anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label.setText(bundle.getString("Quadro05Panel.anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label.text"));
        this.anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label.setBorder(new EtchedBorder());
        this.anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C501_label\u00a3anexoIq05C502_label\u00a3anexoIq05C1_label\u00a3anexoIq05C503_label, cc.xy(9, 1));
        this.label3.setText(bundle.getString("Quadro05Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label3, cc.xy(11, 1));
        this.anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label.setText(bundle.getString("Quadro05Panel.anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label.text"));
        this.anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label.setBorder(new EtchedBorder());
        this.anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C501a_label\u00a3anexoIq05C502a_label\u00a3anexoIq05C1a_label, cc.xy(13, 1));
        this.label1.setText(bundle.getString("Quadro05Panel.label1.text"));
        this.label1.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label1, cc.xy(3, 3));
        this.anexoIq05C501_base_label\u00a3anexoIq05C501a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C501_base_label\u00a3anexoIq05C501a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C501_base_label\u00a3anexoIq05C501a_base_label, cc.xy(3, 5));
        this.anexoIq05C501_num\u00a3anexoIq05C501a_num.setText(bundle.getString("Quadro05Panel.anexoIq05C501_num\u00a3anexoIq05C501a_num.text"));
        this.anexoIq05C501_num\u00a3anexoIq05C501a_num.setBorder(new EtchedBorder());
        this.anexoIq05C501_num\u00a3anexoIq05C501a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C501_num\u00a3anexoIq05C501a_num, cc.xy(7, 5));
        this.anexoIq05C501.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C501, cc.xy(9, 5));
        this.label6.setText(bundle.getString("Quadro05Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.label6.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label6, cc.xy(11, 5));
        this.anexoIq05C501a.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C501a, cc.xy(13, 5));
        this.anexoIq05C502_base_label\u00a3anexoIq05C502a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C502_base_label\u00a3anexoIq05C502a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C502_base_label\u00a3anexoIq05C502a_base_label, cc.xy(3, 7));
        this.anexoIq05C502_num\u00a3anexoIq05C502a_num.setText(bundle.getString("Quadro05Panel.anexoIq05C502_num\u00a3anexoIq05C502a_num.text"));
        this.anexoIq05C502_num\u00a3anexoIq05C502a_num.setBorder(new EtchedBorder());
        this.anexoIq05C502_num\u00a3anexoIq05C502a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C502_num\u00a3anexoIq05C502a_num, cc.xy(7, 7));
        this.anexoIq05C502.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C502, cc.xy(9, 7));
        this.label7.setText(bundle.getString("Quadro05Panel.label7.text"));
        this.label7.setHorizontalAlignment(0);
        this.label7.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label7, cc.xy(11, 7));
        this.anexoIq05C502a.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C502a, cc.xy(13, 7));
        this.anexoIq05C1_base_label\u00a3anexoIq05C1a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C1_base_label\u00a3anexoIq05C1a_base_label.text"));
        this.anexoIq05C1_base_label\u00a3anexoIq05C1a_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C1_base_label\u00a3anexoIq05C1a_base_label, cc.xy(7, 9));
        this.anexoIq05C1.setColumns(14);
        this.anexoIq05C1.setEditable(false);
        this.panel3.add((Component)this.anexoIq05C1, cc.xy(9, 9));
        this.anexoIq05C1a.setColumns(14);
        this.anexoIq05C1a.setEditable(false);
        this.panel3.add((Component)this.anexoIq05C1a, cc.xy(13, 9));
        this.label2.setText(bundle.getString("Quadro05Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label2, cc.xy(3, 11));
        this.anexoIq05C504_base_label\u00a3anexoIq05C504a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C504_base_label\u00a3anexoIq05C504a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C504_base_label\u00a3anexoIq05C504a_base_label, cc.xy(3, 13));
        this.anexoIq05C504_num\u00a3anexoIq05C504a_num.setText(bundle.getString("Quadro05Panel.anexoIq05C504_num\u00a3anexoIq05C504a_num.text"));
        this.anexoIq05C504_num\u00a3anexoIq05C504a_num.setBorder(new EtchedBorder());
        this.anexoIq05C504_num\u00a3anexoIq05C504a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C504_num\u00a3anexoIq05C504a_num, cc.xy(7, 13));
        this.anexoIq05C504.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C504, cc.xy(9, 13));
        this.label8.setText(bundle.getString("Quadro05Panel.label8.text"));
        this.label8.setHorizontalAlignment(0);
        this.label8.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label8, cc.xy(11, 13));
        this.anexoIq05C504a.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C504a, cc.xy(13, 13));
        this.anexoIq05C505_base_label\u00a3anexoIq05C505a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C505_base_label\u00a3anexoIq05C505a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C505_base_label\u00a3anexoIq05C505a_base_label, cc.xy(3, 15));
        this.anexoIq05C505_num\u00a3anexoIq05C505a_num.setText(bundle.getString("Quadro05Panel.anexoIq05C505_num\u00a3anexoIq05C505a_num.text"));
        this.anexoIq05C505_num\u00a3anexoIq05C505a_num.setBorder(new EtchedBorder());
        this.anexoIq05C505_num\u00a3anexoIq05C505a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C505_num\u00a3anexoIq05C505a_num, cc.xy(7, 15));
        this.anexoIq05C505.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C505, cc.xy(9, 15));
        this.label9.setText(bundle.getString("Quadro05Panel.label9.text"));
        this.label9.setHorizontalAlignment(0);
        this.label9.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label9, cc.xy(11, 15));
        this.anexoIq05C505a.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C505a, cc.xy(13, 15));
        this.anexoIq05C506_base_label\u00a3anexoIq05C506a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C506_base_label\u00a3anexoIq05C506a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C506_base_label\u00a3anexoIq05C506a_base_label, cc.xy(3, 17));
        this.anexoIq05C506_num\u00a3anexoIq05C506a_num.setText(bundle.getString("Quadro05Panel.anexoIq05C506_num\u00a3anexoIq05C506a_num.text"));
        this.anexoIq05C506_num\u00a3anexoIq05C506a_num.setBorder(new EtchedBorder());
        this.anexoIq05C506_num\u00a3anexoIq05C506a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C506_num\u00a3anexoIq05C506a_num, cc.xy(7, 17));
        this.anexoIq05C506.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C506, cc.xy(9, 17));
        this.label11.setText(bundle.getString("Quadro05Panel.label11.text"));
        this.label11.setHorizontalAlignment(0);
        this.label11.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label11, cc.xy(11, 17));
        this.anexoIq05C506a.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C506a, cc.xy(13, 17));
        this.anexoIq05C507_base_label\u00a3anexoIq05C507a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C507_base_label\u00a3anexoIq05C507a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C507_base_label\u00a3anexoIq05C507a_base_label, cc.xy(3, 19));
        this.anexoIq05C507_num\u00a3anexoIq05C507a_num.setBorder(new EtchedBorder());
        this.anexoIq05C507_num\u00a3anexoIq05C507a_num.setHorizontalAlignment(0);
        this.anexoIq05C507_num\u00a3anexoIq05C507a_num.setText(bundle.getString("Quadro05Panel.anexoIq05C507_num\u00a3anexoIq05C507a_num.text"));
        this.panel3.add((Component)this.anexoIq05C507_num\u00a3anexoIq05C507a_num, cc.xy(7, 19));
        this.anexoIq05C507.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C507, cc.xy(9, 19));
        this.label12.setText(bundle.getString("Quadro05Panel.label12.text"));
        this.label12.setHorizontalAlignment(0);
        this.label12.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label12, cc.xy(11, 19));
        this.anexoIq05C507a.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C507a, cc.xy(13, 19));
        this.anexoIq05C508_base_label\u00a3anexoIq05C508a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C508_base_label\u00a3anexoIq05C508a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C508_base_label\u00a3anexoIq05C508a_base_label, cc.xy(3, 21));
        this.anexoIq05C508_num\u00a3anexoIq05C508a_num.setText(bundle.getString("Quadro05Panel.anexoIq05C508_num\u00a3anexoIq05C508a_num.text"));
        this.anexoIq05C508_num\u00a3anexoIq05C508a_num.setBorder(new EtchedBorder());
        this.anexoIq05C508_num\u00a3anexoIq05C508a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C508_num\u00a3anexoIq05C508a_num, cc.xy(7, 21));
        this.anexoIq05C508.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C508, cc.xy(9, 21));
        this.label13.setText(bundle.getString("Quadro05Panel.label13.text"));
        this.label13.setHorizontalAlignment(0);
        this.label13.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label13, cc.xy(11, 21));
        this.anexoIq05C508a.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C508a, cc.xy(13, 21));
        this.anexoIq05C2_base_label\u00a3anexoIq05C2a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C2_base_label\u00a3anexoIq05C2a_base_label.text"));
        this.anexoIq05C2_base_label\u00a3anexoIq05C2a_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C2_base_label\u00a3anexoIq05C2a_base_label, cc.xy(7, 23));
        this.anexoIq05C2.setColumns(14);
        this.anexoIq05C2.setEditable(false);
        this.panel3.add((Component)this.anexoIq05C2, cc.xy(9, 23));
        this.anexoIq05C2a.setColumns(14);
        this.anexoIq05C2a.setEditable(false);
        this.panel3.add((Component)this.anexoIq05C2a, cc.xy(13, 23));
        this.anexoIq05C503_base_label\u00a3anexoIq05C503a_base_label.setText(bundle.getString("Quadro05Panel.anexoIq05C503_base_label\u00a3anexoIq05C503a_base_label.text"));
        this.panel3.add((Component)this.anexoIq05C503_base_label\u00a3anexoIq05C503a_base_label, cc.xy(3, 25));
        this.anexoIq05C503_num.setText(bundle.getString("Quadro05Panel.anexoIq05C503_num.text"));
        this.anexoIq05C503_num.setBorder(new EtchedBorder());
        this.anexoIq05C503_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq05C503_num, cc.xy(7, 25));
        this.anexoIq05C503.setColumns(13);
        this.panel3.add((Component)this.anexoIq05C503, cc.xy(9, 25));
        this.this2.add((Component)this.panel3, cc.xy(1, 3));
        this.add((Component)this.this2, "Center");
    }
}

