/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoi;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoi.Quadro07Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro07PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro07PanelExtension
extends Quadro07PanelBase
implements IBindablePanel<Quadro07> {
    public static final int MAX_LINES_Q07_T1 = 80;

    public Quadro07PanelExtension(Quadro07 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro07 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro07Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoIq07T1().getTableHeader().getColumnModel();
        ColumnGroup g_Rendimento = new ColumnGroup("Rendimento L\u00edquido Imputado");
        g_Rendimento.add(cm.getColumn(4));
        g_Rendimento.add(cm.getColumn(5));
        ColumnGroup g_Deducoes = new ColumnGroup("Dedu\u00e7\u00f5es \u00e0 Coleta");
        g_Deducoes.add(cm.getColumn(6));
        ColumnGroup g_Tributacao = new ColumnGroup("Tributa\u00e7\u00e3o Aut\u00f3noma");
        g_Tributacao.add(cm.getColumn(7));
        if (this.getAnexoIq07T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoIq07T1().getTableHeader()).addColumnGroup(g_Rendimento);
            ((GroupableTableHeader)this.getAnexoIq07T1().getTableHeader()).addColumnGroup(g_Deducoes);
            ((GroupableTableHeader)this.getAnexoIq07T1().getTableHeader()).addColumnGroup(g_Tributacao);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_Rendimento);
        header.addColumnGroup(g_Deducoes);
        header.addColumnGroup(g_Tributacao);
        this.getAnexoIq07T1().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoIq07T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoIq07T1() != null && this.model.getAnexoIq07T1().size() >= 80) {
            return;
        }
        super.addLineAnexoIq07T1_LinhaActionPerformed(e);
    }
}

