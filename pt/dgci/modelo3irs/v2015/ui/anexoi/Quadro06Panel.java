/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoi;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JLabel anexoIq06C601_base_label;
    protected JLabelTextFieldNumbering anexoIq06C601_num;
    protected JMoneyTextField anexoIq06C601;
    protected JLabel anexoIq06C602_base_label;
    protected JLabelTextFieldNumbering anexoIq06C602_num;
    protected JMoneyTextField anexoIq06C602;

    public Quadro06Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoIq06C601_base_label() {
        return this.anexoIq06C601_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq06C601_num() {
        return this.anexoIq06C601_num;
    }

    public JMoneyTextField getAnexoIq06C601() {
        return this.anexoIq06C601;
    }

    public JLabel getAnexoIq06C602_base_label() {
        return this.anexoIq06C602_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq06C602_num() {
        return this.anexoIq06C602_num;
    }

    public JMoneyTextField getAnexoIq06C602() {
        return this.anexoIq06C602;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoI");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.anexoIq06C601_base_label = new JLabel();
        this.anexoIq06C601_num = new JLabelTextFieldNumbering();
        this.anexoIq06C601 = new JMoneyTextField();
        this.anexoIq06C602_base_label = new JLabel();
        this.anexoIq06C602_num = new JLabelTextFieldNumbering();
        this.anexoIq06C602 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, default:grow", "default, $lgap, default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("2*(default, $lcgap), default:grow, $lcgap, 25dlu, 0px, 76dlu, $rgap, default", "2*(default, $lgap), default"));
        this.anexoIq06C601_base_label.setText(bundle.getString("Quadro06Panel.anexoIq06C601_base_label.text"));
        this.panel3.add((Component)this.anexoIq06C601_base_label, cc.xy(3, 3));
        this.anexoIq06C601_num.setText(bundle.getString("Quadro06Panel.anexoIq06C601_num.text"));
        this.anexoIq06C601_num.setBorder(new EtchedBorder());
        this.anexoIq06C601_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq06C601_num, cc.xy(7, 3));
        this.anexoIq06C601.setColumns(14);
        this.panel3.add((Component)this.anexoIq06C601, cc.xy(9, 3));
        this.anexoIq06C602_base_label.setText(bundle.getString("Quadro06Panel.anexoIq06C602_base_label.text"));
        this.panel3.add((Component)this.anexoIq06C602_base_label, cc.xy(3, 5));
        this.anexoIq06C602_num.setText(bundle.getString("Quadro06Panel.anexoIq06C602_num.text"));
        this.anexoIq06C602_num.setBorder(new EtchedBorder());
        this.anexoIq06C602_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq06C602_num, cc.xy(7, 5));
        this.anexoIq06C602.setColumns(14);
        this.panel3.add((Component)this.anexoIq06C602, cc.xy(9, 5));
        this.this2.add((Component)this.panel3, cc.xy(2, 3));
        this.add((Component)this.this2, "Center");
    }
}

