/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoi;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro08Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JLabel anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label;
    protected JLabel label3;
    protected JLabel anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label;
    protected JLabel anexoIq08C801_base_label\u00a3anexoIq08C801a_base_label;
    protected JLabelTextFieldNumbering anexoIq08C801_num\u00a3anexoIq08C801a_num;
    protected JMoneyTextField anexoIq08C801;
    protected JLabel label6;
    protected JMoneyTextField anexoIq08C801a;
    protected JLabel anexoIq08C802_base_label\u00a3anexoIq08C802a_base_label;
    protected JLabelTextFieldNumbering anexoIq08C802_num\u00a3anexoIq08C802a_num;
    protected JMoneyTextField anexoIq08C802;
    protected JLabel label7;
    protected JMoneyTextField anexoIq08C802a;
    protected JLabel anexoIq08C806_base_label\u00a3anexoIq08C806a_base_label;
    protected JLabelTextFieldNumbering anexoIq08C806_num\u00a3anexoIq08C806a_num;
    protected JMoneyTextField anexoIq08C806;
    protected JLabel label10;
    protected JMoneyTextField anexoIq08C806a;
    protected JLabel anexoIq08C803_base_label\u00a3anexoIq08C803a_base_label;
    protected JLabelTextFieldNumbering anexoIq08C803_num\u00a3anexoIq08C803a_num;
    protected JMoneyTextField anexoIq08C803;
    protected JLabel label8;
    protected JMoneyTextField anexoIq08C803a;
    protected JLabel anexoIq08C807_base_label\u00a3anexoIq08C807a_base_label;
    protected JLabelTextFieldNumbering anexoIq08C807_num\u00a3anexoIq08C807a_num;
    protected JMoneyTextField anexoIq08C807;
    protected JLabel label12;
    protected JMoneyTextField anexoIq08C807a;
    protected JLabel anexoIq08C804_base_label\u00a3anexoIq08C804a_base_label;
    protected JLabelTextFieldNumbering anexoIq08C804_num\u00a3anexoIq08C804a_num;
    protected JMoneyTextField anexoIq08C804;
    protected JLabel label9;
    protected JMoneyTextField anexoIq08C804a;
    protected JLabel anexoIq08C805_base_label\u00a3anexoIq08C805a_base_label;
    protected JLabelTextFieldNumbering anexoIq08C805_num\u00a3anexoIq08C805a_num;
    protected JMoneyTextField anexoIq08C805;
    protected JLabel label11;
    protected JMoneyTextField anexoIq08C805a;
    protected JLabel anexoIq08C1_base_label\u00a3anexoIq08C1a_base_label;
    protected JMoneyTextField anexoIq08C1;
    protected JMoneyTextField anexoIq08C1a;

    public Quadro08Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label() {
        return this.anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabel getAnexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label() {
        return this.anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label;
    }

    public JLabel getAnexoIq08C801_base_label\u00a3anexoIq08C801a_base_label() {
        return this.anexoIq08C801_base_label\u00a3anexoIq08C801a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq08C801_num\u00a3anexoIq08C801a_num() {
        return this.anexoIq08C801_num\u00a3anexoIq08C801a_num;
    }

    public JMoneyTextField getAnexoIq08C801() {
        return this.anexoIq08C801;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JMoneyTextField getAnexoIq08C801a() {
        return this.anexoIq08C801a;
    }

    public JLabel getAnexoIq08C802_base_label\u00a3anexoIq08C802a_base_label() {
        return this.anexoIq08C802_base_label\u00a3anexoIq08C802a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq08C802_num\u00a3anexoIq08C802a_num() {
        return this.anexoIq08C802_num\u00a3anexoIq08C802a_num;
    }

    public JMoneyTextField getAnexoIq08C802() {
        return this.anexoIq08C802;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JMoneyTextField getAnexoIq08C802a() {
        return this.anexoIq08C802a;
    }

    public JLabelTextFieldNumbering getAnexoIq08C803_num\u00a3anexoIq08C803a_num() {
        return this.anexoIq08C803_num\u00a3anexoIq08C803a_num;
    }

    public JMoneyTextField getAnexoIq08C803() {
        return this.anexoIq08C803;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JMoneyTextField getAnexoIq08C803a() {
        return this.anexoIq08C803a;
    }

    public JLabelTextFieldNumbering getAnexoIq08C804_num\u00a3anexoIq08C804a_num() {
        return this.anexoIq08C804_num\u00a3anexoIq08C804a_num;
    }

    public JMoneyTextField getAnexoIq08C804() {
        return this.anexoIq08C804;
    }

    public JLabel getLabel9() {
        return this.label9;
    }

    public JMoneyTextField getAnexoIq08C804a() {
        return this.anexoIq08C804a;
    }

    public JLabelTextFieldNumbering getAnexoIq08C805_num\u00a3anexoIq08C805a_num() {
        return this.anexoIq08C805_num\u00a3anexoIq08C805a_num;
    }

    public JMoneyTextField getAnexoIq08C805() {
        return this.anexoIq08C805;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JMoneyTextField getAnexoIq08C805a() {
        return this.anexoIq08C805a;
    }

    public JLabel getAnexoIq08C1_base_label\u00a3anexoIq08C1a_base_label() {
        return this.anexoIq08C1_base_label\u00a3anexoIq08C1a_base_label;
    }

    public JMoneyTextField getAnexoIq08C1() {
        return this.anexoIq08C1;
    }

    public JMoneyTextField getAnexoIq08C1a() {
        return this.anexoIq08C1a;
    }

    public JLabel getAnexoIq08C803_base_label\u00a3anexoIq08C803a_base_label() {
        return this.anexoIq08C803_base_label\u00a3anexoIq08C803a_base_label;
    }

    public JLabel getAnexoIq08C804_base_label\u00a3anexoIq08C804a_base_label() {
        return this.anexoIq08C804_base_label\u00a3anexoIq08C804a_base_label;
    }

    public JLabel getAnexoIq08C805_base_label\u00a3anexoIq08C805a_base_label() {
        return this.anexoIq08C805_base_label\u00a3anexoIq08C805a_base_label;
    }

    public JLabel getAnexoIq08C806_base_label\u00a3anexoIq08C806a_base_label() {
        return this.anexoIq08C806_base_label\u00a3anexoIq08C806a_base_label;
    }

    public JLabelTextFieldNumbering getAnexoIq08C806_num\u00a3anexoIq08C806a_num() {
        return this.anexoIq08C806_num\u00a3anexoIq08C806a_num;
    }

    public JMoneyTextField getAnexoIq08C806() {
        return this.anexoIq08C806;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JMoneyTextField getAnexoIq08C806a() {
        return this.anexoIq08C806a;
    }

    public JLabelTextFieldNumbering getAnexoIq08C807_num\u00a3anexoIq08C807a_num() {
        return this.anexoIq08C807_num\u00a3anexoIq08C807a_num;
    }

    public JMoneyTextField getAnexoIq08C807() {
        return this.anexoIq08C807;
    }

    public JLabel getLabel12() {
        return this.label12;
    }

    public JMoneyTextField getAnexoIq08C807a() {
        return this.anexoIq08C807a;
    }

    public JLabel getAnexoIq08C807_base_label\u00a3anexoIq08C807a_base_label() {
        return this.anexoIq08C807_base_label\u00a3anexoIq08C807a_base_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoI");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label = new JLabel();
        this.label3 = new JLabel();
        this.anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label = new JLabel();
        this.anexoIq08C801_base_label\u00a3anexoIq08C801a_base_label = new JLabel();
        this.anexoIq08C801_num\u00a3anexoIq08C801a_num = new JLabelTextFieldNumbering();
        this.anexoIq08C801 = new JMoneyTextField();
        this.label6 = new JLabel();
        this.anexoIq08C801a = new JMoneyTextField();
        this.anexoIq08C802_base_label\u00a3anexoIq08C802a_base_label = new JLabel();
        this.anexoIq08C802_num\u00a3anexoIq08C802a_num = new JLabelTextFieldNumbering();
        this.anexoIq08C802 = new JMoneyTextField();
        this.label7 = new JLabel();
        this.anexoIq08C802a = new JMoneyTextField();
        this.anexoIq08C806_base_label\u00a3anexoIq08C806a_base_label = new JLabel();
        this.anexoIq08C806_num\u00a3anexoIq08C806a_num = new JLabelTextFieldNumbering();
        this.anexoIq08C806 = new JMoneyTextField();
        this.label10 = new JLabel();
        this.anexoIq08C806a = new JMoneyTextField();
        this.anexoIq08C803_base_label\u00a3anexoIq08C803a_base_label = new JLabel();
        this.anexoIq08C803_num\u00a3anexoIq08C803a_num = new JLabelTextFieldNumbering();
        this.anexoIq08C803 = new JMoneyTextField();
        this.label8 = new JLabel();
        this.anexoIq08C803a = new JMoneyTextField();
        this.anexoIq08C807_base_label\u00a3anexoIq08C807a_base_label = new JLabel();
        this.anexoIq08C807_num\u00a3anexoIq08C807a_num = new JLabelTextFieldNumbering();
        this.anexoIq08C807 = new JMoneyTextField();
        this.label12 = new JLabel();
        this.anexoIq08C807a = new JMoneyTextField();
        this.anexoIq08C804_base_label\u00a3anexoIq08C804a_base_label = new JLabel();
        this.anexoIq08C804_num\u00a3anexoIq08C804a_num = new JLabelTextFieldNumbering();
        this.anexoIq08C804 = new JMoneyTextField();
        this.label9 = new JLabel();
        this.anexoIq08C804a = new JMoneyTextField();
        this.anexoIq08C805_base_label\u00a3anexoIq08C805a_base_label = new JLabel();
        this.anexoIq08C805_num\u00a3anexoIq08C805a_num = new JLabelTextFieldNumbering();
        this.anexoIq08C805 = new JMoneyTextField();
        this.label11 = new JLabel();
        this.anexoIq08C805a = new JMoneyTextField();
        this.anexoIq08C1_base_label\u00a3anexoIq08C1a_base_label = new JLabel();
        this.anexoIq08C1 = new JMoneyTextField();
        this.anexoIq08C1a = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro08Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro08Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, default:grow", "default, $lgap, default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("2*(default, $lcgap), default:grow, $lcgap, 25dlu, $rgap, 75dlu, $rgap, default, $lcgap, 75dlu, $lcgap, default", "10*(default, $lgap), default"));
        this.anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label.setText(bundle.getString("Quadro08Panel.anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label.text"));
        this.anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label.setBorder(new EtchedBorder());
        this.anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C801_label\u00a3anexoIq08C802_label\u00a3anexoIq08C803_label\u00a3anexoIq08C804_label\u00a3anexoIq08C805_label\u00a3anexoIq08C1_label, cc.xy(9, 1));
        this.label3.setText(bundle.getString("Quadro08Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label3, cc.xy(11, 1));
        this.anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label.setText(bundle.getString("Quadro08Panel.anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label.text"));
        this.anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label.setBorder(new EtchedBorder());
        this.anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C801a_label\u00a3anexoIq08C802a_label\u00a3anexoIq08C803a_label\u00a3anexoIq08C804a_label\u00a3anexoIq08C805a_label\u00a3anexoIq08C1a_label, cc.xy(13, 1));
        this.anexoIq08C801_base_label\u00a3anexoIq08C801a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C801_base_label\u00a3anexoIq08C801a_base_label.text"));
        this.panel3.add((Component)this.anexoIq08C801_base_label\u00a3anexoIq08C801a_base_label, cc.xy(3, 3));
        this.anexoIq08C801_num\u00a3anexoIq08C801a_num.setText(bundle.getString("Quadro08Panel.anexoIq08C801_num\u00a3anexoIq08C801a_num.text"));
        this.anexoIq08C801_num\u00a3anexoIq08C801a_num.setBorder(new EtchedBorder());
        this.anexoIq08C801_num\u00a3anexoIq08C801a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C801_num\u00a3anexoIq08C801a_num, cc.xy(7, 3));
        this.panel3.add((Component)this.anexoIq08C801, cc.xy(9, 3));
        this.label6.setText(bundle.getString("Quadro08Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.label6.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label6, cc.xy(11, 3));
        this.panel3.add((Component)this.anexoIq08C801a, cc.xy(13, 3));
        this.anexoIq08C802_base_label\u00a3anexoIq08C802a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C802_base_label\u00a3anexoIq08C802a_base_label.text"));
        this.panel3.add((Component)this.anexoIq08C802_base_label\u00a3anexoIq08C802a_base_label, cc.xy(3, 5));
        this.anexoIq08C802_num\u00a3anexoIq08C802a_num.setText(bundle.getString("Quadro08Panel.anexoIq08C802_num\u00a3anexoIq08C802a_num.text"));
        this.anexoIq08C802_num\u00a3anexoIq08C802a_num.setBorder(new EtchedBorder());
        this.anexoIq08C802_num\u00a3anexoIq08C802a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C802_num\u00a3anexoIq08C802a_num, cc.xy(7, 5));
        this.panel3.add((Component)this.anexoIq08C802, cc.xy(9, 5));
        this.label7.setText(bundle.getString("Quadro08Panel.label7.text"));
        this.label7.setHorizontalAlignment(0);
        this.label7.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label7, cc.xy(11, 5));
        this.panel3.add((Component)this.anexoIq08C802a, cc.xy(13, 5));
        this.anexoIq08C806_base_label\u00a3anexoIq08C806a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C806_base_label\u00a3anexoIq08C806a_base_label.text"));
        this.panel3.add((Component)this.anexoIq08C806_base_label\u00a3anexoIq08C806a_base_label, cc.xy(3, 7));
        this.anexoIq08C806_num\u00a3anexoIq08C806a_num.setText(bundle.getString("Quadro08Panel.anexoIq08C806_num\u00a3anexoIq08C806a_num.text"));
        this.anexoIq08C806_num\u00a3anexoIq08C806a_num.setBorder(new EtchedBorder());
        this.anexoIq08C806_num\u00a3anexoIq08C806a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C806_num\u00a3anexoIq08C806a_num, cc.xy(7, 7));
        this.panel3.add((Component)this.anexoIq08C806, cc.xy(9, 7));
        this.label10.setText(bundle.getString("Quadro08Panel.label10.text"));
        this.label10.setHorizontalAlignment(0);
        this.label10.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label10, cc.xy(11, 7));
        this.panel3.add((Component)this.anexoIq08C806a, cc.xy(13, 7));
        this.anexoIq08C803_base_label\u00a3anexoIq08C803a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C803_base_label\u00a3anexoIq08C803a_base_label.text"));
        this.panel3.add((Component)this.anexoIq08C803_base_label\u00a3anexoIq08C803a_base_label, cc.xy(3, 9));
        this.anexoIq08C803_num\u00a3anexoIq08C803a_num.setText(bundle.getString("Quadro08Panel.anexoIq08C803_num\u00a3anexoIq08C803a_num.text"));
        this.anexoIq08C803_num\u00a3anexoIq08C803a_num.setBorder(new EtchedBorder());
        this.anexoIq08C803_num\u00a3anexoIq08C803a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C803_num\u00a3anexoIq08C803a_num, cc.xy(7, 9));
        this.panel3.add((Component)this.anexoIq08C803, cc.xy(9, 9));
        this.label8.setText(bundle.getString("Quadro08Panel.label8.text"));
        this.label8.setHorizontalAlignment(0);
        this.label8.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label8, cc.xy(11, 9));
        this.panel3.add((Component)this.anexoIq08C803a, cc.xy(13, 9));
        this.anexoIq08C807_base_label\u00a3anexoIq08C807a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C807_base_label\u00a3anexoIq08C807a_base_label.text"));
        this.panel3.add((Component)this.anexoIq08C807_base_label\u00a3anexoIq08C807a_base_label, cc.xy(3, 11));
        this.anexoIq08C807_num\u00a3anexoIq08C807a_num.setText(bundle.getString("Quadro08Panel.anexoIq08C807_num\u00a3anexoIq08C807a_num.text"));
        this.anexoIq08C807_num\u00a3anexoIq08C807a_num.setBorder(new EtchedBorder());
        this.anexoIq08C807_num\u00a3anexoIq08C807a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C807_num\u00a3anexoIq08C807a_num, cc.xy(7, 11));
        this.panel3.add((Component)this.anexoIq08C807, cc.xy(9, 11));
        this.label12.setText(bundle.getString("Quadro08Panel.label12.text"));
        this.label12.setHorizontalAlignment(0);
        this.label12.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label12, cc.xy(11, 11));
        this.panel3.add((Component)this.anexoIq08C807a, cc.xy(13, 11));
        this.anexoIq08C804_base_label\u00a3anexoIq08C804a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C804_base_label\u00a3anexoIq08C804a_base_label.text"));
        this.panel3.add((Component)this.anexoIq08C804_base_label\u00a3anexoIq08C804a_base_label, cc.xy(3, 13));
        this.anexoIq08C804_num\u00a3anexoIq08C804a_num.setText(bundle.getString("Quadro08Panel.anexoIq08C804_num\u00a3anexoIq08C804a_num.text"));
        this.anexoIq08C804_num\u00a3anexoIq08C804a_num.setBorder(new EtchedBorder());
        this.anexoIq08C804_num\u00a3anexoIq08C804a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C804_num\u00a3anexoIq08C804a_num, cc.xy(7, 13));
        this.panel3.add((Component)this.anexoIq08C804, cc.xy(9, 13));
        this.label9.setText(bundle.getString("Quadro08Panel.label9.text"));
        this.label9.setHorizontalAlignment(0);
        this.label9.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label9, cc.xy(11, 13));
        this.panel3.add((Component)this.anexoIq08C804a, cc.xy(13, 13));
        this.anexoIq08C805_base_label\u00a3anexoIq08C805a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C805_base_label\u00a3anexoIq08C805a_base_label.text"));
        this.panel3.add((Component)this.anexoIq08C805_base_label\u00a3anexoIq08C805a_base_label, cc.xy(3, 15));
        this.anexoIq08C805_num\u00a3anexoIq08C805a_num.setText(bundle.getString("Quadro08Panel.anexoIq08C805_num\u00a3anexoIq08C805a_num.text"));
        this.anexoIq08C805_num\u00a3anexoIq08C805a_num.setBorder(new EtchedBorder());
        this.anexoIq08C805_num\u00a3anexoIq08C805a_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoIq08C805_num\u00a3anexoIq08C805a_num, cc.xy(7, 15));
        this.panel3.add((Component)this.anexoIq08C805, cc.xy(9, 15));
        this.label11.setText(bundle.getString("Quadro08Panel.label11.text"));
        this.label11.setHorizontalAlignment(0);
        this.label11.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label11, cc.xy(11, 15));
        this.panel3.add((Component)this.anexoIq08C805a, cc.xy(13, 15));
        this.anexoIq08C1_base_label\u00a3anexoIq08C1a_base_label.setText(bundle.getString("Quadro08Panel.anexoIq08C1_base_label\u00a3anexoIq08C1a_base_label.text"));
        this.anexoIq08C1_base_label\u00a3anexoIq08C1a_base_label.setHorizontalAlignment(4);
        this.panel3.add((Component)this.anexoIq08C1_base_label\u00a3anexoIq08C1a_base_label, cc.xywh(3, 17, 5, 1));
        this.anexoIq08C1.setColumns(13);
        this.anexoIq08C1.setEditable(false);
        this.panel3.add((Component)this.anexoIq08C1, cc.xy(9, 17));
        this.anexoIq08C1a.setEditable(false);
        this.anexoIq08C1a.setColumns(13);
        this.panel3.add((Component)this.anexoIq08C1a, cc.xy(13, 17));
        this.this2.add((Component)this.panel3, cc.xy(2, 3));
        this.add((Component)this.this2, "Center");
    }
}

