/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoi;

import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro08Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro08PanelBase
extends Quadro08Panel
implements IBindablePanel<Quadro08> {
    private static final long serialVersionUID = 1;
    protected Quadro08 model;

    @Override
    public abstract void setModel(Quadro08 var1, boolean var2);

    @Override
    public Quadro08 getModel() {
        return this.model;
    }

    public Quadro08PanelBase(Quadro08 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

