/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoi;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoIq04C04_label;
    protected JLabel label3;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField anexoIq04C04;
    protected JLabel anexoIq04C05_label;
    protected JLabel label6;
    protected JLabelTextFieldNumbering label7;
    protected JNIFTextField anexoIq04C05;
    protected JLabel anexoIq04C06_label;
    protected JLabel label8;
    protected JLabelTextFieldNumbering label9;
    protected JNIFTextField anexoIq04C06;

    public Quadro04Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoIq04C04_label() {
        return this.anexoIq04C04_label;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getAnexoIq04C04() {
        return this.anexoIq04C04;
    }

    public JLabel getAnexoIq04C05_label() {
        return this.anexoIq04C05_label;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabelTextFieldNumbering getLabel7() {
        return this.label7;
    }

    public JNIFTextField getAnexoIq04C05() {
        return this.anexoIq04C05;
    }

    public JLabel getAnexoIq04C06_label() {
        return this.anexoIq04C06_label;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JLabelTextFieldNumbering getLabel9() {
        return this.label9;
    }

    public JNIFTextField getAnexoIq04C06() {
        return this.anexoIq04C06;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoI");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoIq04C04_label = new JLabel();
        this.label3 = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoIq04C04 = new JNIFTextField();
        this.anexoIq04C05_label = new JLabel();
        this.label6 = new JLabel();
        this.label7 = new JLabelTextFieldNumbering();
        this.anexoIq04C05 = new JNIFTextField();
        this.anexoIq04C06_label = new JLabel();
        this.label8 = new JLabel();
        this.label9 = new JLabelTextFieldNumbering();
        this.anexoIq04C06 = new JNIFTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default, 0px, default:grow, $lcgap, default, 15dlu, default, $rgap, 13dlu, 0px, 43dlu, $lcgap, 0px, default:grow", "$ugap, 4*($lgap, default)"));
        this.anexoIq04C04_label.setText(bundle.getString("Quadro04Panel.anexoIq04C04_label.text"));
        this.panel2.add((Component)this.anexoIq04C04_label, CC.xy(5, 3));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label3, CC.xy(7, 3));
        this.label4.setText(bundle.getString("Quadro04Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(9, 3));
        this.anexoIq04C04.setColumns(9);
        this.anexoIq04C04.setEditable(false);
        this.panel2.add((Component)this.anexoIq04C04, CC.xy(11, 3));
        this.anexoIq04C05_label.setText(bundle.getString("Quadro04Panel.anexoIq04C05_label.text"));
        this.panel2.add((Component)this.anexoIq04C05_label, CC.xy(5, 5));
        this.label6.setText(bundle.getString("Quadro04Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label6, CC.xy(7, 5));
        this.label7.setText(bundle.getString("Quadro04Panel.label7.text"));
        this.label7.setBorder(new EtchedBorder());
        this.label7.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label7, CC.xy(9, 5));
        this.anexoIq04C05.setColumns(9);
        this.anexoIq04C05.setEditable(false);
        this.panel2.add((Component)this.anexoIq04C05, CC.xy(11, 5));
        this.anexoIq04C06_label.setText(bundle.getString("Quadro04Panel.anexoIq04C06_label.text"));
        this.panel2.add((Component)this.anexoIq04C06_label, CC.xy(5, 7));
        this.label8.setText(bundle.getString("Quadro04Panel.label8.text"));
        this.label8.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label8, CC.xy(7, 7));
        this.label9.setText(bundle.getString("Quadro04Panel.label9.text"));
        this.label9.setBorder(new EtchedBorder());
        this.label9.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label9, CC.xy(9, 7));
        this.anexoIq04C06.setColumns(9);
        this.panel2.add((Component)this.anexoIq04C06, CC.xy(11, 7));
        this.this2.add((Component)this.panel2, CC.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

