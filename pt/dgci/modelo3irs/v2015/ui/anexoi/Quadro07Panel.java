/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoi;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoIq07T1Scroll;
    protected JTable anexoIq07T1;
    protected JPanel panel5;
    protected JPanel panel1;
    protected JLabel anexoIq07C1_label;
    protected JPanel panel2;
    protected JLabel anexoIq07C2_label;
    protected JPanel panel4;
    protected JLabel anexoIq07C3_label;
    protected JPanel panel6;
    protected JLabel anexoIq07C4_label;
    protected JPanel panel7;
    protected JLabel anexoIq07C5_label;
    protected JLabel anexoIq07C1_base_label\u00a3anexoIq07C2_base_label\u00a3anexoIq07C3_base_label\u00a3anexoIq07C4_base_label\u00a3anexoIq07C5_base_label;
    protected JMoneyTextField anexoIq07C1;
    protected JMoneyTextField anexoIq07C2;
    protected JMoneyTextField anexoIq07C3;
    protected JMoneyTextField anexoIq07C4;
    protected JMoneyTextField anexoIq07C5;

    public Quadro07Panel() {
        this.initComponents();
    }

    protected void addLineAnexoIq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoIq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoIq07T1Scroll() {
        return this.anexoIq07T1Scroll;
    }

    public JTable getAnexoIq07T1() {
        return this.anexoIq07T1;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JMoneyTextField getAnexoIq07C2() {
        return this.anexoIq07C2;
    }

    public JMoneyTextField getAnexoIq07C3() {
        return this.anexoIq07C3;
    }

    public JLabel getAnexoIq07C1_base_label\u00a3anexoIq07C2_base_label\u00a3anexoIq07C3_base_label\u00a3anexoIq07C4_base_label\u00a3anexoIq07C5_base_label() {
        return this.anexoIq07C1_base_label\u00a3anexoIq07C2_base_label\u00a3anexoIq07C3_base_label\u00a3anexoIq07C4_base_label\u00a3anexoIq07C5_base_label;
    }

    public JMoneyTextField getAnexoIq07C4() {
        return this.anexoIq07C4;
    }

    public JMoneyTextField getAnexoIq07C5() {
        return this.anexoIq07C5;
    }

    public JMoneyTextField getAnexoIq07C1() {
        return this.anexoIq07C1;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getAnexoIq07C1_label() {
        return this.anexoIq07C1_label;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoIq07C2_label() {
        return this.anexoIq07C2_label;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoIq07C3_label() {
        return this.anexoIq07C3_label;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getAnexoIq07C4_label() {
        return this.anexoIq07C4_label;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoIq07C5_label() {
        return this.anexoIq07C5_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoI");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoIq07T1Scroll = new JScrollPane();
        this.anexoIq07T1 = new JTable();
        this.panel5 = new JPanel();
        this.panel1 = new JPanel();
        this.anexoIq07C1_label = new JLabel();
        this.panel2 = new JPanel();
        this.anexoIq07C2_label = new JLabel();
        this.panel4 = new JPanel();
        this.anexoIq07C3_label = new JLabel();
        this.panel6 = new JPanel();
        this.anexoIq07C4_label = new JLabel();
        this.panel7 = new JPanel();
        this.anexoIq07C5_label = new JLabel();
        this.anexoIq07C1_base_label\u00a3anexoIq07C2_base_label\u00a3anexoIq07C3_base_label\u00a3anexoIq07C4_base_label\u00a3anexoIq07C5_base_label = new JLabel();
        this.anexoIq07C1 = new JMoneyTextField();
        this.anexoIq07C2 = new JMoneyTextField();
        this.anexoIq07C3 = new JMoneyTextField();
        this.anexoIq07C4 = new JMoneyTextField();
        this.anexoIq07C5 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 3*(default, $lgap), default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 610dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:223dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro07Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.addLineAnexoIq07T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro07Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.removeLineAnexoIq07T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoIq07T1Scroll.setViewportView(this.anexoIq07T1);
        this.panel3.add((Component)this.anexoIq07T1Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.panel5.setPreferredSize(null);
        this.panel5.setMinimumSize(null);
        this.panel5.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, $rgap, 100dlu, $lcgap, 10dlu, $lcgap, 0px, 100dlu, 2*($lcgap, 10dlu), $lcgap, 0px, 100dlu, $lcgap, 10dlu, $lcgap, 100dlu, $lcgap, 6dlu, $lcgap, 103dlu, $lcgap, 6dlu, $lcgap, default:grow, 0px", "$ugap, $lgap, fill:30dlu, 2*($lgap, default)"));
        this.panel1.setBorder(new EtchedBorder());
        this.panel1.setLayout(new FormLayout("$rgap, 93dlu, $rgap", "$rgap, default:grow"));
        this.anexoIq07C1_label.setText(bundle.getString("Quadro07Panel.anexoIq07C1_label.text"));
        this.anexoIq07C1_label.setHorizontalAlignment(0);
        this.panel1.add((Component)this.anexoIq07C1_label, cc.xy(2, 2));
        this.panel5.add((Component)this.panel1, cc.xy(6, 3));
        this.panel2.setBorder(new EtchedBorder());
        this.panel2.setLayout(new FormLayout("$rgap, 119dlu, $rgap", "$rgap, default"));
        this.anexoIq07C2_label.setText(bundle.getString("Quadro07Panel.anexoIq07C2_label.text"));
        this.anexoIq07C2_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoIq07C2_label, cc.xy(2, 2));
        this.panel5.add((Component)this.panel2, cc.xywh(8, 3, 6, 1));
        this.panel4.setBorder(new EtchedBorder());
        this.panel4.setLayout(new FormLayout("$rgap, 113dlu, $rgap", "$rgap, default"));
        this.anexoIq07C3_label.setText(bundle.getString("Quadro07Panel.anexoIq07C3_label.text"));
        this.anexoIq07C3_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoIq07C3_label, cc.xy(2, 2));
        this.panel5.add((Component)this.panel4, cc.xywh(15, 3, 6, 1));
        this.panel6.setBorder(new EtchedBorder());
        this.panel6.setLayout(new FormLayout("$rgap, 93dlu, $rgap", "$rgap, default"));
        this.anexoIq07C4_label.setText(bundle.getString("Quadro07Panel.anexoIq07C4_label.text"));
        this.anexoIq07C4_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoIq07C4_label, cc.xy(2, 2));
        this.panel5.add((Component)this.panel6, cc.xy(22, 3));
        this.panel7.setBorder(new EtchedBorder());
        this.panel7.setLayout(new FormLayout("$rgap, 112dlu, $rgap", "$rgap, default"));
        this.anexoIq07C5_label.setText(bundle.getString("Quadro07Panel.anexoIq07C5_label.text"));
        this.anexoIq07C5_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoIq07C5_label, cc.xy(2, 2));
        this.panel5.add((Component)this.panel7, cc.xywh(24, 3, 5, 1));
        this.anexoIq07C1_base_label\u00a3anexoIq07C2_base_label\u00a3anexoIq07C3_base_label\u00a3anexoIq07C4_base_label\u00a3anexoIq07C5_base_label.setText(bundle.getString("Quadro07Panel.anexoIq07C1_base_label\u00a3anexoIq07C2_base_label\u00a3anexoIq07C3_base_label\u00a3anexoIq07C4_base_label\u00a3anexoIq07C5_base_label.text"));
        this.panel5.add((Component)this.anexoIq07C1_base_label\u00a3anexoIq07C2_base_label\u00a3anexoIq07C3_base_label\u00a3anexoIq07C4_base_label\u00a3anexoIq07C5_base_label, cc.xy(3, 5));
        this.anexoIq07C1.setColumns(15);
        this.anexoIq07C1.setEditable(false);
        this.anexoIq07C1.setAllowNegative(true);
        this.panel5.add((Component)this.anexoIq07C1, cc.xy(6, 5));
        this.anexoIq07C2.setColumns(17);
        this.anexoIq07C2.setEditable(false);
        this.anexoIq07C2.setAllowNegative(true);
        this.panel5.add((Component)this.anexoIq07C2, cc.xy(11, 5));
        this.anexoIq07C3.setColumns(15);
        this.anexoIq07C3.setEditable(false);
        this.anexoIq07C3.setAllowNegative(true);
        this.panel5.add((Component)this.anexoIq07C3, cc.xy(18, 5));
        this.anexoIq07C4.setColumns(15);
        this.anexoIq07C4.setEditable(false);
        this.panel5.add((Component)this.anexoIq07C4, cc.xy(22, 5));
        this.anexoIq07C5.setColumns(15);
        this.anexoIq07C5.setEditable(false);
        this.panel5.add((Component)this.anexoIq07C5, cc.xy(26, 5));
        this.this2.add((Component)this.panel5, cc.xy(2, 6));
        this.add((Component)this.this2, "Center");
    }

}

