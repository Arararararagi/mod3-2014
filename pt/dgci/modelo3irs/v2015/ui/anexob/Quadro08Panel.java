/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro08Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JLabel label2;
    protected JPanel panel2;
    protected JLabel anexoBq08C801_label;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField anexoBq08C801;
    protected JPanel panel3;
    protected JLabel anexoBq08C802_base_label;
    protected JPanel panel7;
    protected JLabel anexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label;
    protected JPanel panel4;
    protected JLabel anexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label;
    protected JLabelTextFieldNumbering label10;
    protected JLimitedTextField anexoBq08C802;
    protected JLabelTextFieldNumbering anexoBq08C808_num;
    protected JMoneyTextField anexoBq08C808;
    protected JLabelTextFieldNumbering anexoBq08C814_num;
    protected JMoneyTextField anexoBq08C814;
    protected JLabelTextFieldNumbering label13;
    protected JLimitedTextField anexoBq08C803;
    protected JLabelTextFieldNumbering anexoBq08C809_num;
    protected JMoneyTextField anexoBq08C809;
    protected JLabelTextFieldNumbering anexoBq08C815_num;
    protected JMoneyTextField anexoBq08C815;
    protected JLabelTextFieldNumbering label16;
    protected JLimitedTextField anexoBq08C804;
    protected JLabelTextFieldNumbering anexoBq08C810_num;
    protected JMoneyTextField anexoBq08C810;
    protected JLabelTextFieldNumbering anexoBq08C816_num;
    protected JMoneyTextField anexoBq08C816;
    protected JLabelTextFieldNumbering label19;
    protected JLimitedTextField anexoBq08C805;
    protected JLabelTextFieldNumbering anexoBq08C811_num;
    protected JMoneyTextField anexoBq08C811;
    protected JLabelTextFieldNumbering anexoBq08C817_num;
    protected JMoneyTextField anexoBq08C817;
    protected JLabelTextFieldNumbering label22;
    protected JLimitedTextField anexoBq08C806;
    protected JLabelTextFieldNumbering anexoBq08C812_num;
    protected JMoneyTextField anexoBq08C812;
    protected JLabelTextFieldNumbering anexoBq08C818_num;
    protected JMoneyTextField anexoBq08C818;
    protected JLabelTextFieldNumbering label25;
    protected JLimitedTextField anexoBq08C807;
    protected JLabelTextFieldNumbering anexoBq08C813_num;
    protected JMoneyTextField anexoBq08C813;
    protected JLabelTextFieldNumbering anexoBq08C819_num;
    protected JMoneyTextField anexoBq08C819;

    public Quadro08Panel() {
        this.initComponents();
    }

    protected void addLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoBq08C802_base_label() {
        return this.anexoBq08C802_base_label;
    }

    public JLabelTextFieldNumbering getLabel10() {
        return this.label10;
    }

    public JLabelTextFieldNumbering getAnexoBq08C808_num() {
        return this.anexoBq08C808_num;
    }

    public JMoneyTextField getAnexoBq08C808() {
        return this.anexoBq08C808;
    }

    public JLabelTextFieldNumbering getAnexoBq08C814_num() {
        return this.anexoBq08C814_num;
    }

    public JMoneyTextField getAnexoBq08C814() {
        return this.anexoBq08C814;
    }

    public JLabelTextFieldNumbering getLabel13() {
        return this.label13;
    }

    public JLabelTextFieldNumbering getAnexoBq08C809_num() {
        return this.anexoBq08C809_num;
    }

    public JMoneyTextField getAnexoBq08C809() {
        return this.anexoBq08C809;
    }

    public JLabelTextFieldNumbering getAnexoBq08C815_num() {
        return this.anexoBq08C815_num;
    }

    public JMoneyTextField getAnexoBq08C815() {
        return this.anexoBq08C815;
    }

    public JLabelTextFieldNumbering getLabel16() {
        return this.label16;
    }

    public JLabelTextFieldNumbering getAnexoBq08C810_num() {
        return this.anexoBq08C810_num;
    }

    public JMoneyTextField getAnexoBq08C810() {
        return this.anexoBq08C810;
    }

    public JLabelTextFieldNumbering getAnexoBq08C816_num() {
        return this.anexoBq08C816_num;
    }

    public JMoneyTextField getAnexoBq08C816() {
        return this.anexoBq08C816;
    }

    public JLabelTextFieldNumbering getLabel19() {
        return this.label19;
    }

    public JLabelTextFieldNumbering getAnexoBq08C811_num() {
        return this.anexoBq08C811_num;
    }

    public JMoneyTextField getAnexoBq08C811() {
        return this.anexoBq08C811;
    }

    public JLabelTextFieldNumbering getAnexoBq08C817_num() {
        return this.anexoBq08C817_num;
    }

    public JMoneyTextField getAnexoBq08C817() {
        return this.anexoBq08C817;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JLabelTextFieldNumbering getAnexoBq08C812_num() {
        return this.anexoBq08C812_num;
    }

    public JMoneyTextField getAnexoBq08C812() {
        return this.anexoBq08C812;
    }

    public JLabelTextFieldNumbering getAnexoBq08C818_num() {
        return this.anexoBq08C818_num;
    }

    public JMoneyTextField getAnexoBq08C818() {
        return this.anexoBq08C818;
    }

    public JLabelTextFieldNumbering getLabel25() {
        return this.label25;
    }

    public JLabelTextFieldNumbering getAnexoBq08C813_num() {
        return this.anexoBq08C813_num;
    }

    public JMoneyTextField getAnexoBq08C813() {
        return this.anexoBq08C813;
    }

    public JLabelTextFieldNumbering getAnexoBq08C819_num() {
        return this.anexoBq08C819_num;
    }

    public JMoneyTextField getAnexoBq08C819() {
        return this.anexoBq08C819;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoBq08C801_label() {
        return this.anexoBq08C801_label;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getAnexoBq08C801() {
        return this.anexoBq08C801;
    }

    public JLimitedTextField getAnexoBq08C802() {
        return this.anexoBq08C802;
    }

    public JLimitedTextField getAnexoBq08C803() {
        return this.anexoBq08C803;
    }

    public JLimitedTextField getAnexoBq08C804() {
        return this.anexoBq08C804;
    }

    public JLimitedTextField getAnexoBq08C805() {
        return this.anexoBq08C805;
    }

    public JLimitedTextField getAnexoBq08C806() {
        return this.anexoBq08C806;
    }

    public JLimitedTextField getAnexoBq08C807() {
        return this.anexoBq08C807;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label() {
        return this.anexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label;
    }

    public JLabel getAnexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label() {
        return this.anexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.label2 = new JLabel();
        this.panel2 = new JPanel();
        this.anexoBq08C801_label = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoBq08C801 = new JNIFTextField();
        this.panel3 = new JPanel();
        this.anexoBq08C802_base_label = new JLabel();
        this.panel7 = new JPanel();
        this.anexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label = new JLabel();
        this.panel4 = new JPanel();
        this.anexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label = new JLabel();
        this.label10 = new JLabelTextFieldNumbering();
        this.anexoBq08C802 = new JLimitedTextField();
        this.anexoBq08C808_num = new JLabelTextFieldNumbering();
        this.anexoBq08C808 = new JMoneyTextField();
        this.anexoBq08C814_num = new JLabelTextFieldNumbering();
        this.anexoBq08C814 = new JMoneyTextField();
        this.label13 = new JLabelTextFieldNumbering();
        this.anexoBq08C803 = new JLimitedTextField();
        this.anexoBq08C809_num = new JLabelTextFieldNumbering();
        this.anexoBq08C809 = new JMoneyTextField();
        this.anexoBq08C815_num = new JLabelTextFieldNumbering();
        this.anexoBq08C815 = new JMoneyTextField();
        this.label16 = new JLabelTextFieldNumbering();
        this.anexoBq08C804 = new JLimitedTextField();
        this.anexoBq08C810_num = new JLabelTextFieldNumbering();
        this.anexoBq08C810 = new JMoneyTextField();
        this.anexoBq08C816_num = new JLabelTextFieldNumbering();
        this.anexoBq08C816 = new JMoneyTextField();
        this.label19 = new JLabelTextFieldNumbering();
        this.anexoBq08C805 = new JLimitedTextField();
        this.anexoBq08C811_num = new JLabelTextFieldNumbering();
        this.anexoBq08C811 = new JMoneyTextField();
        this.anexoBq08C817_num = new JLabelTextFieldNumbering();
        this.anexoBq08C817 = new JMoneyTextField();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoBq08C806 = new JLimitedTextField();
        this.anexoBq08C812_num = new JLabelTextFieldNumbering();
        this.anexoBq08C812 = new JMoneyTextField();
        this.anexoBq08C818_num = new JLabelTextFieldNumbering();
        this.anexoBq08C818 = new JMoneyTextField();
        this.label25 = new JLabelTextFieldNumbering();
        this.anexoBq08C807 = new JLimitedTextField();
        this.anexoBq08C813_num = new JLabelTextFieldNumbering();
        this.anexoBq08C813 = new JMoneyTextField();
        this.anexoBq08C819_num = new JLabelTextFieldNumbering();
        this.anexoBq08C819 = new JMoneyTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro08Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro08Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "4*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro08Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.this2.add((Component)this.label2, CC.xy(1, 3));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, default, $rgap, 25dlu, 0px, 43dlu, $lcgap, default:grow", "$ugap, 3*($lgap, default)"));
        this.anexoBq08C801_label.setText(bundle.getString("Quadro08Panel.anexoBq08C801_label.text"));
        this.panel2.add((Component)this.anexoBq08C801_label, CC.xy(3, 5));
        this.label4.setText(bundle.getString("Quadro08Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(5, 5));
        this.anexoBq08C801.setColumns(9);
        this.panel2.add((Component)this.anexoBq08C801, CC.xy(7, 5));
        this.this2.add((Component)this.panel2, CC.xy(1, 5));
        this.panel3.setPreferredSize(null);
        this.panel3.setMinimumSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, 25dlu, 0px, 22dlu, $lcgap, 3dlu, 0px, 25dlu, 0px, 87dlu, 0px, 3dlu, $lcgap, 25dlu, 0px, 72dlu, $lcgap, default:grow, 0px", "$ugap, $lgap, fill:30dlu, 7*($lgap, default)"));
        this.anexoBq08C802_base_label.setText(bundle.getString("Quadro08Panel.anexoBq08C802_base_label.text"));
        this.anexoBq08C802_base_label.setBorder(new EtchedBorder());
        this.anexoBq08C802_base_label.setHorizontalAlignment(0);
        this.anexoBq08C802_base_label.setHorizontalTextPosition(0);
        this.panel3.add((Component)this.anexoBq08C802_base_label, CC.xywh(3, 3, 3, 1));
        this.panel7.setBorder(new EtchedBorder());
        this.panel7.setLayout(new FormLayout("112dlu", "$rgap, default"));
        this.anexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label.setText(bundle.getString("Quadro08Panel.anexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label.text"));
        this.anexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq08C808_label\u00a3anexoBq08C809_label\u00a3anexoBq08C810_label\u00a3anexoBq08C811_label\u00a3anexoBq08C812_label\u00a3anexoBq08C813_label, CC.xy(1, 2));
        this.panel3.add((Component)this.panel7, CC.xywh(9, 3, 3, 1));
        this.panel4.setBorder(new EtchedBorder());
        this.panel4.setLayout(new FormLayout("92dlu", "$rgap, default"));
        this.anexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label.setText(bundle.getString("Quadro08Panel.anexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label.text"));
        this.anexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq08C814_label\u00a3anexoBq08C815_label\u00a3anexoBq08C816_label\u00a3anexoBq08C817_label\u00a3anexoBq08C818_label\u00a3anexoBq08C819_label, CC.xy(1, 2));
        this.panel3.add((Component)this.panel4, CC.xywh(15, 3, 3, 1));
        this.label10.setText(bundle.getString("Quadro08Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label10, CC.xy(3, 5));
        this.anexoBq08C802.setMaxLength(4);
        this.anexoBq08C802.setOnlyNumeric(true);
        this.anexoBq08C802.setColumns(3);
        this.panel3.add((Component)this.anexoBq08C802, CC.xy(5, 5));
        this.anexoBq08C808_num.setText(bundle.getString("Quadro08Panel.anexoBq08C808_num.text"));
        this.anexoBq08C808_num.setBorder(new EtchedBorder());
        this.anexoBq08C808_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C808_num, CC.xy(9, 5));
        this.panel3.add((Component)this.anexoBq08C808, CC.xy(11, 5));
        this.anexoBq08C814_num.setText(bundle.getString("Quadro08Panel.anexoBq08C814_num.text"));
        this.anexoBq08C814_num.setBorder(new EtchedBorder());
        this.anexoBq08C814_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C814_num, CC.xy(15, 5));
        this.panel3.add((Component)this.anexoBq08C814, CC.xy(17, 5));
        this.label13.setText(bundle.getString("Quadro08Panel.label13.text"));
        this.label13.setBorder(new EtchedBorder());
        this.label13.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label13, CC.xy(3, 7));
        this.anexoBq08C803.setMaxLength(4);
        this.anexoBq08C803.setOnlyNumeric(true);
        this.anexoBq08C803.setColumns(3);
        this.panel3.add((Component)this.anexoBq08C803, CC.xy(5, 7));
        this.anexoBq08C809_num.setText(bundle.getString("Quadro08Panel.anexoBq08C809_num.text"));
        this.anexoBq08C809_num.setBorder(new EtchedBorder());
        this.anexoBq08C809_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C809_num, CC.xy(9, 7));
        this.panel3.add((Component)this.anexoBq08C809, CC.xy(11, 7));
        this.anexoBq08C815_num.setText(bundle.getString("Quadro08Panel.anexoBq08C815_num.text"));
        this.anexoBq08C815_num.setBorder(new EtchedBorder());
        this.anexoBq08C815_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C815_num, CC.xy(15, 7));
        this.panel3.add((Component)this.anexoBq08C815, CC.xy(17, 7));
        this.label16.setText(bundle.getString("Quadro08Panel.label16.text"));
        this.label16.setBorder(new EtchedBorder());
        this.label16.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label16, CC.xy(3, 9));
        this.anexoBq08C804.setMaxLength(4);
        this.anexoBq08C804.setOnlyNumeric(true);
        this.anexoBq08C804.setColumns(3);
        this.panel3.add((Component)this.anexoBq08C804, CC.xy(5, 9));
        this.anexoBq08C810_num.setText(bundle.getString("Quadro08Panel.anexoBq08C810_num.text"));
        this.anexoBq08C810_num.setBorder(new EtchedBorder());
        this.anexoBq08C810_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C810_num, CC.xy(9, 9));
        this.panel3.add((Component)this.anexoBq08C810, CC.xy(11, 9));
        this.anexoBq08C816_num.setText(bundle.getString("Quadro08Panel.anexoBq08C816_num.text"));
        this.anexoBq08C816_num.setBorder(new EtchedBorder());
        this.anexoBq08C816_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C816_num, CC.xy(15, 9));
        this.panel3.add((Component)this.anexoBq08C816, CC.xy(17, 9));
        this.label19.setText(bundle.getString("Quadro08Panel.label19.text"));
        this.label19.setBorder(new EtchedBorder());
        this.label19.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label19, CC.xy(3, 11));
        this.anexoBq08C805.setMaxLength(4);
        this.anexoBq08C805.setOnlyNumeric(true);
        this.anexoBq08C805.setColumns(3);
        this.panel3.add((Component)this.anexoBq08C805, CC.xy(5, 11));
        this.anexoBq08C811_num.setText(bundle.getString("Quadro08Panel.anexoBq08C811_num.text"));
        this.anexoBq08C811_num.setBorder(new EtchedBorder());
        this.anexoBq08C811_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C811_num, CC.xy(9, 11));
        this.panel3.add((Component)this.anexoBq08C811, CC.xy(11, 11));
        this.anexoBq08C817_num.setText(bundle.getString("Quadro08Panel.anexoBq08C817_num.text"));
        this.anexoBq08C817_num.setBorder(new EtchedBorder());
        this.anexoBq08C817_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C817_num, CC.xy(15, 11));
        this.panel3.add((Component)this.anexoBq08C817, CC.xy(17, 11));
        this.label22.setText(bundle.getString("Quadro08Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label22, CC.xy(3, 13));
        this.anexoBq08C806.setMaxLength(4);
        this.anexoBq08C806.setOnlyNumeric(true);
        this.anexoBq08C806.setColumns(3);
        this.panel3.add((Component)this.anexoBq08C806, CC.xy(5, 13));
        this.anexoBq08C812_num.setText(bundle.getString("Quadro08Panel.anexoBq08C812_num.text"));
        this.anexoBq08C812_num.setBorder(new EtchedBorder());
        this.anexoBq08C812_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C812_num, CC.xy(9, 13));
        this.panel3.add((Component)this.anexoBq08C812, CC.xy(11, 13));
        this.anexoBq08C818_num.setText(bundle.getString("Quadro08Panel.anexoBq08C818_num.text"));
        this.anexoBq08C818_num.setBorder(new EtchedBorder());
        this.anexoBq08C818_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C818_num, CC.xy(15, 13));
        this.panel3.add((Component)this.anexoBq08C818, CC.xy(17, 13));
        this.label25.setText(bundle.getString("Quadro08Panel.label25.text"));
        this.label25.setBorder(new EtchedBorder());
        this.label25.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label25, CC.xy(3, 15));
        this.anexoBq08C807.setMaxLength(4);
        this.anexoBq08C807.setOnlyNumeric(true);
        this.anexoBq08C807.setColumns(3);
        this.panel3.add((Component)this.anexoBq08C807, CC.xy(5, 15));
        this.anexoBq08C813_num.setText(bundle.getString("Quadro08Panel.anexoBq08C813_num.text"));
        this.anexoBq08C813_num.setBorder(new EtchedBorder());
        this.anexoBq08C813_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C813_num, CC.xy(9, 15));
        this.panel3.add((Component)this.anexoBq08C813, CC.xy(11, 15));
        this.anexoBq08C819_num.setText(bundle.getString("Quadro08Panel.anexoBq08C819_num.text"));
        this.anexoBq08C819_num.setBorder(new EtchedBorder());
        this.anexoBq08C819_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq08C819_num, CC.xy(15, 15));
        this.panel3.add((Component)this.anexoBq08C819, CC.xy(17, 15));
        this.this2.add((Component)this.panel3, CC.xy(1, 7));
        this.add((Component)this.this2, "Center");
    }
}

