/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro07Bindings;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro07PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro07PanelExtension
extends Quadro07PanelBase
implements IBindablePanel<Quadro07> {
    public static final int MAX_LINES_Q07_T1 = 2000;

    public Quadro07PanelExtension(Quadro07 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro07 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro07Bindings.doBindings(model, this);
        }
        if (model != null) {
            this.setColumnSizes();
        }
    }

    @Override
    protected void addLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoBq07T1() != null && this.model.getAnexoBq07T1().size() >= 2000) {
            return;
        }
        super.addLineAnexoBq07T1_LinhaActionPerformed(e);
    }

    private void setColumnSizes() {
        if (this.getAnexoBq07T1().getColumnCount() >= 1) {
            this.getAnexoBq07T1().getColumnModel().getColumn(0).setMaxWidth(75);
        }
    }
}

