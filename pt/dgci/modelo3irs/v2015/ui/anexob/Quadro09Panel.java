/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro09Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JPanel panel3;
    protected JLabel anexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label;
    protected JPanel panel2;
    protected JLabel anexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label;
    protected JLabel anexoBq09C901_base_label\u00a3anexoBq09C910_base_label;
    protected JLabelTextFieldNumbering anexoBq09C901_num;
    protected JMoneyTextField anexoBq09C901;
    protected JLabelTextFieldNumbering anexoBq09C910_num;
    protected JMoneyTextField anexoBq09C910;
    protected JLabel anexoBq09C902_base_label\u00a3anexoBq09C911_base_label;
    protected JLabelTextFieldNumbering anexoBq09C902_num;
    protected JMoneyTextField anexoBq09C902;
    protected JLabelTextFieldNumbering anexoBq09C911_num;
    protected JMoneyTextField anexoBq09C911;
    protected JLabel anexoBq09C903_base_label\u00a3anexoBq09C912_base_label;
    protected JLabelTextFieldNumbering anexoBq09C903_num;
    protected JMoneyTextField anexoBq09C903;
    protected JLabelTextFieldNumbering anexoBq09C912_num;
    protected JMoneyTextField anexoBq09C912;
    protected JLabel anexoBq09C904_base_label\u00a3anexoBq09C913_base_label;
    protected JLabelTextFieldNumbering anexoBq09C904_num;
    protected JMoneyTextField anexoBq09C904;
    protected JLabelTextFieldNumbering anexoBq09C913_num;
    protected JMoneyTextField anexoBq09C913;
    protected JLabel anexoBq09C905_base_label\u00a3anexoBq09C914_base_label;
    protected JLabelTextFieldNumbering anexoBq09C905_num;
    protected JMoneyTextField anexoBq09C905;
    protected JLabelTextFieldNumbering anexoBq09C914_num;
    protected JMoneyTextField anexoBq09C914;
    protected JLabel anexoBq09C906_base_label\u00a3anexoBq09C915_base_label;
    protected JLabelTextFieldNumbering anexoBq09C906_num;
    protected JMoneyTextField anexoBq09C906;
    protected JLabelTextFieldNumbering anexoBq09C915_num;
    protected JMoneyTextField anexoBq09C915;
    protected JLabel anexoBq09C907_base_label\u00a3anexoBq09C916_base_label;
    protected JLabelTextFieldNumbering anexoBq09C907_num;
    protected JMoneyTextField anexoBq09C907;
    protected JLabelTextFieldNumbering anexoBq09C916_num;
    protected JMoneyTextField anexoBq09C916;
    protected JLabel anexoBq09C908_base_label\u00a3anexoBq09C916_base_label;
    protected JLabelTextFieldNumbering anexoBq09C908_num;
    protected JMoneyTextField anexoBq09C908;
    protected JLabelTextFieldNumbering anexoBq09C917_num;
    protected JMoneyTextField anexoBq09C917;
    protected JLabel anexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label;
    protected JMoneyTextField anexoBq09C1a;
    protected JMoneyTextField anexoBq09C1b;
    protected JPanel panel7;
    protected JLabel label30;
    protected JLabel label31;

    public Quadro09Panel() {
        this.initComponents();
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabelTextFieldNumbering getAnexoBq09C901_num() {
        return this.anexoBq09C901_num;
    }

    public JMoneyTextField getAnexoBq09C901() {
        return this.anexoBq09C901;
    }

    public JLabelTextFieldNumbering getAnexoBq09C910_num() {
        return this.anexoBq09C910_num;
    }

    public JMoneyTextField getAnexoBq09C910() {
        return this.anexoBq09C910;
    }

    public JLabelTextFieldNumbering getAnexoBq09C902_num() {
        return this.anexoBq09C902_num;
    }

    public JMoneyTextField getAnexoBq09C902() {
        return this.anexoBq09C902;
    }

    public JLabelTextFieldNumbering getAnexoBq09C911_num() {
        return this.anexoBq09C911_num;
    }

    public JMoneyTextField getAnexoBq09C911() {
        return this.anexoBq09C911;
    }

    public JLabelTextFieldNumbering getAnexoBq09C903_num() {
        return this.anexoBq09C903_num;
    }

    public JMoneyTextField getAnexoBq09C903() {
        return this.anexoBq09C903;
    }

    public JLabelTextFieldNumbering getAnexoBq09C912_num() {
        return this.anexoBq09C912_num;
    }

    public JMoneyTextField getAnexoBq09C912() {
        return this.anexoBq09C912;
    }

    public JLabelTextFieldNumbering getAnexoBq09C904_num() {
        return this.anexoBq09C904_num;
    }

    public JMoneyTextField getAnexoBq09C904() {
        return this.anexoBq09C904;
    }

    public JLabelTextFieldNumbering getAnexoBq09C913_num() {
        return this.anexoBq09C913_num;
    }

    public JMoneyTextField getAnexoBq09C913() {
        return this.anexoBq09C913;
    }

    public JLabelTextFieldNumbering getAnexoBq09C905_num() {
        return this.anexoBq09C905_num;
    }

    public JMoneyTextField getAnexoBq09C905() {
        return this.anexoBq09C905;
    }

    public JLabelTextFieldNumbering getAnexoBq09C914_num() {
        return this.anexoBq09C914_num;
    }

    public JMoneyTextField getAnexoBq09C914() {
        return this.anexoBq09C914;
    }

    public JLabelTextFieldNumbering getAnexoBq09C906_num() {
        return this.anexoBq09C906_num;
    }

    public JMoneyTextField getAnexoBq09C906() {
        return this.anexoBq09C906;
    }

    public JLabelTextFieldNumbering getAnexoBq09C915_num() {
        return this.anexoBq09C915_num;
    }

    public JMoneyTextField getAnexoBq09C915() {
        return this.anexoBq09C915;
    }

    public JLabelTextFieldNumbering getAnexoBq09C907_num() {
        return this.anexoBq09C907_num;
    }

    public JMoneyTextField getAnexoBq09C907() {
        return this.anexoBq09C907;
    }

    public JLabelTextFieldNumbering getAnexoBq09C916_num() {
        return this.anexoBq09C916_num;
    }

    public JMoneyTextField getAnexoBq09C916() {
        return this.anexoBq09C916;
    }

    public JLabelTextFieldNumbering getAnexoBq09C908_num() {
        return this.anexoBq09C908_num;
    }

    public JMoneyTextField getAnexoBq09C908() {
        return this.anexoBq09C908;
    }

    public JLabelTextFieldNumbering getAnexoBq09C917_num() {
        return this.anexoBq09C917_num;
    }

    public JMoneyTextField getAnexoBq09C917() {
        return this.anexoBq09C917;
    }

    public JMoneyTextField getAnexoBq09C1a() {
        return this.anexoBq09C1a;
    }

    public JMoneyTextField getAnexoBq09C1b() {
        return this.anexoBq09C1b;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getLabel30() {
        return this.label30;
    }

    public JLabel getLabel31() {
        return this.label31;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label() {
        return this.anexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label;
    }

    public JLabel getAnexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label() {
        return this.anexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label;
    }

    public JLabel getAnexoBq09C901_base_label\u00a3anexoBq09C910_base_label() {
        return this.anexoBq09C901_base_label\u00a3anexoBq09C910_base_label;
    }

    public JLabel getAnexoBq09C902_base_label\u00a3anexoBq09C911_base_label() {
        return this.anexoBq09C902_base_label\u00a3anexoBq09C911_base_label;
    }

    public JLabel getAnexoBq09C903_base_label\u00a3anexoBq09C912_base_label() {
        return this.anexoBq09C903_base_label\u00a3anexoBq09C912_base_label;
    }

    public JLabel getAnexoBq09C904_base_label\u00a3anexoBq09C913_base_label() {
        return this.anexoBq09C904_base_label\u00a3anexoBq09C913_base_label;
    }

    public JLabel getAnexoBq09C905_base_label\u00a3anexoBq09C914_base_label() {
        return this.anexoBq09C905_base_label\u00a3anexoBq09C914_base_label;
    }

    public JLabel getAnexoBq09C906_base_label\u00a3anexoBq09C915_base_label() {
        return this.anexoBq09C906_base_label\u00a3anexoBq09C915_base_label;
    }

    public JLabel getAnexoBq09C907_base_label\u00a3anexoBq09C916_base_label() {
        return this.anexoBq09C907_base_label\u00a3anexoBq09C916_base_label;
    }

    public JLabel getAnexoBq09C908_base_label\u00a3anexoBq09C916_base_label() {
        return this.anexoBq09C908_base_label\u00a3anexoBq09C916_base_label;
    }

    public JLabel getAnexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label() {
        return this.anexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.panel3 = new JPanel();
        this.anexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label = new JLabel();
        this.panel2 = new JPanel();
        this.anexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label = new JLabel();
        this.anexoBq09C901_base_label\u00a3anexoBq09C910_base_label = new JLabel();
        this.anexoBq09C901_num = new JLabelTextFieldNumbering();
        this.anexoBq09C901 = new JMoneyTextField();
        this.anexoBq09C910_num = new JLabelTextFieldNumbering();
        this.anexoBq09C910 = new JMoneyTextField();
        this.anexoBq09C902_base_label\u00a3anexoBq09C911_base_label = new JLabel();
        this.anexoBq09C902_num = new JLabelTextFieldNumbering();
        this.anexoBq09C902 = new JMoneyTextField();
        this.anexoBq09C911_num = new JLabelTextFieldNumbering();
        this.anexoBq09C911 = new JMoneyTextField();
        this.anexoBq09C903_base_label\u00a3anexoBq09C912_base_label = new JLabel();
        this.anexoBq09C903_num = new JLabelTextFieldNumbering();
        this.anexoBq09C903 = new JMoneyTextField();
        this.anexoBq09C912_num = new JLabelTextFieldNumbering();
        this.anexoBq09C912 = new JMoneyTextField();
        this.anexoBq09C904_base_label\u00a3anexoBq09C913_base_label = new JLabel();
        this.anexoBq09C904_num = new JLabelTextFieldNumbering();
        this.anexoBq09C904 = new JMoneyTextField();
        this.anexoBq09C913_num = new JLabelTextFieldNumbering();
        this.anexoBq09C913 = new JMoneyTextField();
        this.anexoBq09C905_base_label\u00a3anexoBq09C914_base_label = new JLabel();
        this.anexoBq09C905_num = new JLabelTextFieldNumbering();
        this.anexoBq09C905 = new JMoneyTextField();
        this.anexoBq09C914_num = new JLabelTextFieldNumbering();
        this.anexoBq09C914 = new JMoneyTextField();
        this.anexoBq09C906_base_label\u00a3anexoBq09C915_base_label = new JLabel();
        this.anexoBq09C906_num = new JLabelTextFieldNumbering();
        this.anexoBq09C906 = new JMoneyTextField();
        this.anexoBq09C915_num = new JLabelTextFieldNumbering();
        this.anexoBq09C915 = new JMoneyTextField();
        this.anexoBq09C907_base_label\u00a3anexoBq09C916_base_label = new JLabel();
        this.anexoBq09C907_num = new JLabelTextFieldNumbering();
        this.anexoBq09C907 = new JMoneyTextField();
        this.anexoBq09C916_num = new JLabelTextFieldNumbering();
        this.anexoBq09C916 = new JMoneyTextField();
        this.anexoBq09C908_base_label\u00a3anexoBq09C916_base_label = new JLabel();
        this.anexoBq09C908_num = new JLabelTextFieldNumbering();
        this.anexoBq09C908 = new JMoneyTextField();
        this.anexoBq09C917_num = new JLabelTextFieldNumbering();
        this.anexoBq09C917 = new JMoneyTextField();
        this.anexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label = new JLabel();
        this.anexoBq09C1a = new JMoneyTextField();
        this.anexoBq09C1b = new JMoneyTextField();
        this.panel7 = new JPanel();
        this.label30 = new JLabel();
        this.label31 = new JLabel();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro09Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro09Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "default, $lgap, default"));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default, $lcgap, default:grow, 0px, $ugap, 25dlu, 0px, 87dlu, $ugap, 25dlu, 0px, 75dlu, $rgap, 0px", "default, $lgap, fill:default, 10*($lgap, default)"));
        this.panel3.setBorder(new EtchedBorder());
        this.panel3.setLayout(new FormLayout("$rgap, 105dlu, $rgap", "$rgap, default, $rgap"));
        this.anexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label.setText(bundle.getString("Quadro09Panel.anexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label.text"));
        this.anexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.anexoBq09C901_label\u00a3anexoBq09C902_label\u00a3anexoBq09C903_label\u00a3anexoBq09C904_label\u00a3anexoBq09C905_label\u00a3anexoBq09C906_label\u00a3anexoBq09C907_label\u00a3anexoBq09C908_label\u00a3anexoBq09C1a_label, cc.xy(2, 2));
        this.panel4.add((Component)this.panel3, cc.xywh(6, 3, 3, 1));
        this.panel2.setBorder(new EtchedBorder());
        this.panel2.setLayout(new FormLayout("102dlu, $rgap", "$rgap, default, $rgap"));
        this.anexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label.setText(bundle.getString("Quadro09Panel.anexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label.text"));
        this.anexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq09C910_label\u00a3anexoBq09C911_label\u00a3anexoBq09C912_label\u00a3anexoBq09C913_label\u00a3anexoBq09C914_label\u00a3anexoBq09C915_label\u00a3anexoBq09C916_label\u00a3anexoBq09C917_label\u00a3anexoBq09C1b_label, cc.xy(1, 2));
        this.panel4.add((Component)this.panel2, cc.xywh(10, 3, 3, 1));
        this.anexoBq09C901_base_label\u00a3anexoBq09C910_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C901_base_label\u00a3anexoBq09C910_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C901_base_label\u00a3anexoBq09C910_base_label, cc.xy(1, 5));
        this.anexoBq09C901_num.setText(bundle.getString("Quadro09Panel.anexoBq09C901_num.text"));
        this.anexoBq09C901_num.setBorder(new EtchedBorder());
        this.anexoBq09C901_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C901_num, cc.xy(6, 5));
        this.panel4.add((Component)this.anexoBq09C901, cc.xy(8, 5));
        this.anexoBq09C910_num.setText(bundle.getString("Quadro09Panel.anexoBq09C910_num.text"));
        this.anexoBq09C910_num.setBorder(new EtchedBorder());
        this.anexoBq09C910_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C910_num, cc.xy(10, 5));
        this.panel4.add((Component)this.anexoBq09C910, cc.xy(12, 5));
        this.anexoBq09C902_base_label\u00a3anexoBq09C911_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C902_base_label\u00a3anexoBq09C911_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C902_base_label\u00a3anexoBq09C911_base_label, cc.xy(1, 7));
        this.anexoBq09C902_num.setText(bundle.getString("Quadro09Panel.anexoBq09C902_num.text"));
        this.anexoBq09C902_num.setBorder(new EtchedBorder());
        this.anexoBq09C902_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C902_num, cc.xy(6, 7));
        this.panel4.add((Component)this.anexoBq09C902, cc.xy(8, 7));
        this.anexoBq09C911_num.setText(bundle.getString("Quadro09Panel.anexoBq09C911_num.text"));
        this.anexoBq09C911_num.setBorder(new EtchedBorder());
        this.anexoBq09C911_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C911_num, cc.xy(10, 7));
        this.panel4.add((Component)this.anexoBq09C911, cc.xy(12, 7));
        this.anexoBq09C903_base_label\u00a3anexoBq09C912_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C903_base_label\u00a3anexoBq09C912_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C903_base_label\u00a3anexoBq09C912_base_label, cc.xy(1, 9));
        this.anexoBq09C903_num.setText(bundle.getString("Quadro09Panel.anexoBq09C903_num.text"));
        this.anexoBq09C903_num.setBorder(new EtchedBorder());
        this.anexoBq09C903_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C903_num, cc.xy(6, 9));
        this.panel4.add((Component)this.anexoBq09C903, cc.xy(8, 9));
        this.anexoBq09C912_num.setText(bundle.getString("Quadro09Panel.anexoBq09C912_num.text"));
        this.anexoBq09C912_num.setBorder(new EtchedBorder());
        this.anexoBq09C912_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C912_num, cc.xy(10, 9));
        this.panel4.add((Component)this.anexoBq09C912, cc.xy(12, 9));
        this.anexoBq09C904_base_label\u00a3anexoBq09C913_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C904_base_label\u00a3anexoBq09C913_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C904_base_label\u00a3anexoBq09C913_base_label, cc.xy(1, 11));
        this.anexoBq09C904_num.setText(bundle.getString("Quadro09Panel.anexoBq09C904_num.text"));
        this.anexoBq09C904_num.setBorder(new EtchedBorder());
        this.anexoBq09C904_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C904_num, cc.xy(6, 11));
        this.panel4.add((Component)this.anexoBq09C904, cc.xy(8, 11));
        this.anexoBq09C913_num.setText(bundle.getString("Quadro09Panel.anexoBq09C913_num.text"));
        this.anexoBq09C913_num.setBorder(new EtchedBorder());
        this.anexoBq09C913_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C913_num, cc.xy(10, 11));
        this.panel4.add((Component)this.anexoBq09C913, cc.xy(12, 11));
        this.anexoBq09C905_base_label\u00a3anexoBq09C914_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C905_base_label\u00a3anexoBq09C914_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C905_base_label\u00a3anexoBq09C914_base_label, cc.xy(1, 13));
        this.anexoBq09C905_num.setText(bundle.getString("Quadro09Panel.anexoBq09C905_num.text"));
        this.anexoBq09C905_num.setBorder(new EtchedBorder());
        this.anexoBq09C905_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C905_num, cc.xy(6, 13));
        this.panel4.add((Component)this.anexoBq09C905, cc.xy(8, 13));
        this.anexoBq09C914_num.setText(bundle.getString("Quadro09Panel.anexoBq09C914_num.text"));
        this.anexoBq09C914_num.setBorder(new EtchedBorder());
        this.anexoBq09C914_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C914_num, cc.xy(10, 13));
        this.panel4.add((Component)this.anexoBq09C914, cc.xy(12, 13));
        this.anexoBq09C906_base_label\u00a3anexoBq09C915_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C906_base_label\u00a3anexoBq09C915_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C906_base_label\u00a3anexoBq09C915_base_label, cc.xy(1, 15));
        this.anexoBq09C906_num.setText(bundle.getString("Quadro09Panel.anexoBq09C906_num.text"));
        this.anexoBq09C906_num.setBorder(new EtchedBorder());
        this.anexoBq09C906_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C906_num, cc.xy(6, 15));
        this.panel4.add((Component)this.anexoBq09C906, cc.xy(8, 15));
        this.anexoBq09C915_num.setText(bundle.getString("Quadro09Panel.anexoBq09C915_num.text"));
        this.anexoBq09C915_num.setBorder(new EtchedBorder());
        this.anexoBq09C915_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C915_num, cc.xy(10, 15));
        this.panel4.add((Component)this.anexoBq09C915, cc.xy(12, 15));
        this.anexoBq09C907_base_label\u00a3anexoBq09C916_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C907_base_label\u00a3anexoBq09C916_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C907_base_label\u00a3anexoBq09C916_base_label, cc.xy(1, 17));
        this.anexoBq09C907_num.setText(bundle.getString("Quadro09Panel.anexoBq09C907_num.text"));
        this.anexoBq09C907_num.setBorder(new EtchedBorder());
        this.anexoBq09C907_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C907_num, cc.xy(6, 17));
        this.panel4.add((Component)this.anexoBq09C907, cc.xy(8, 17));
        this.anexoBq09C916_num.setText(bundle.getString("Quadro09Panel.anexoBq09C916_num.text"));
        this.anexoBq09C916_num.setBorder(new EtchedBorder());
        this.anexoBq09C916_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C916_num, cc.xy(10, 17));
        this.panel4.add((Component)this.anexoBq09C916, cc.xy(12, 17));
        this.anexoBq09C908_base_label\u00a3anexoBq09C916_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C908_base_label\u00a3anexoBq09C916_base_label.text"));
        this.panel4.add((Component)this.anexoBq09C908_base_label\u00a3anexoBq09C916_base_label, cc.xy(1, 19));
        this.anexoBq09C908_num.setText(bundle.getString("Quadro09Panel.anexoBq09C908_num.text"));
        this.anexoBq09C908_num.setBorder(new EtchedBorder());
        this.anexoBq09C908_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C908_num, cc.xy(6, 19));
        this.panel4.add((Component)this.anexoBq09C908, cc.xy(8, 19));
        this.anexoBq09C917_num.setText(bundle.getString("Quadro09Panel.anexoBq09C917_num.text"));
        this.anexoBq09C917_num.setBorder(new EtchedBorder());
        this.anexoBq09C917_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C917_num, cc.xy(10, 19));
        this.panel4.add((Component)this.anexoBq09C917, cc.xy(12, 19));
        this.anexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label.setText(bundle.getString("Quadro09Panel.anexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label.text"));
        this.anexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq09C1a_base_label\u00a3anexoBq09C1b_base_label, cc.xywh(5, 21, 2, 1));
        this.anexoBq09C1a.setColumns(15);
        this.anexoBq09C1a.setEditable(false);
        this.panel4.add((Component)this.anexoBq09C1a, cc.xy(8, 21));
        this.anexoBq09C1b.setColumns(15);
        this.anexoBq09C1b.setEditable(false);
        this.panel4.add((Component)this.anexoBq09C1b, cc.xy(12, 21));
        this.this2.add((Component)this.panel4, cc.xy(3, 1));
        this.add((Component)this.this2, "Center");
        this.panel7.setBorder(new EtchedBorder());
        this.panel7.setLayout(new FormLayout("106dlu", "$rgap, default, $lgap, default, $rgap"));
        this.label30.setText(bundle.getString("Quadro09Panel.label30.text"));
        this.label30.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label30, cc.xy(1, 2));
        this.label31.setText(bundle.getString("Quadro09Panel.label31.text"));
        this.label31.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label31, cc.xy(1, 4));
    }
}

