/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro03Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoBq03C06_label;
    protected JLabel label3;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField anexoBq03C06;
    protected JLabel anexoBq03C07_label;
    protected JLabel label6;
    protected JLabelTextFieldNumbering label7;
    protected JNIFTextField anexoBq03C07;
    protected JPanel panel3;
    protected JLabel label12;
    protected JLabel label13;
    protected JLabel anexoBq03B1OPSim_base_label;
    protected JLabelTextFieldNumbering label22;
    protected JRadioButton anexoBq03B1OP1;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton anexoBq03B1OP2;
    protected JPanel panel4;
    protected JLabel anexoBq03C08_label;
    protected JLabelTextFieldNumbering label11;
    protected JNIFTextField anexoBq03C08;
    protected JLabel anexoBq03C09_label;
    protected JLabelTextFieldNumbering label20;
    protected JNIFTextField anexoBq03C09;
    protected JPanel panel6;
    protected JLabel anexoBq03C10_label;
    protected JLabelTextFieldNumbering label32;
    protected JLimitedTextField anexoBq03C10;
    protected JPanel panel5;
    protected JLabel anexoBq03C11_label;
    protected JLabelTextFieldNumbering label28;
    protected JLimitedTextField anexoBq03C11;
    protected JLabel anexoBq03C12_label;
    protected JLabelTextFieldNumbering label30;
    protected JLimitedTextField anexoBq03C12;
    protected JLabel label10;
    protected JLabel label14;
    protected JPanel panel7;
    protected JLabel label15;
    protected JLabel label16;
    protected JLabel anexoBq03B13OPSim_base_label;
    protected JLabelTextFieldNumbering label24;
    protected JRadioButton anexoBq03B13OP13;
    protected JLabelTextFieldNumbering label25;
    protected JRadioButton anexoBq03B13OP14;

    public Quadro03Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoBq03C06_label() {
        return this.anexoBq03C06_label;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getAnexoBq03C06() {
        return this.anexoBq03C06;
    }

    public JLabel getAnexoBq03C07_label() {
        return this.anexoBq03C07_label;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabelTextFieldNumbering getLabel7() {
        return this.label7;
    }

    public JNIFTextField getAnexoBq03C07() {
        return this.anexoBq03C07;
    }

    public JLabel getAnexoBq03C08_label() {
        return this.anexoBq03C08_label;
    }

    public JLabelTextFieldNumbering getLabel11() {
        return this.label11;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel12() {
        return this.label12;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JLabel getAnexoBq03B1OPSim_base_label() {
        return this.anexoBq03B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JRadioButton getAnexoBq03B1OP1() {
        return this.anexoBq03B1OP1;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getAnexoBq03B1OP2() {
        return this.anexoBq03B1OP2;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JNIFTextField getAnexoBq03C08() {
        return this.anexoBq03C08;
    }

    public JLabel getAnexoBq03C09_label() {
        return this.anexoBq03C09_label;
    }

    public JLabelTextFieldNumbering getLabel20() {
        return this.label20;
    }

    public JNIFTextField getAnexoBq03C09() {
        return this.anexoBq03C09;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getAnexoBq03C11_label() {
        return this.anexoBq03C11_label;
    }

    public JLabelTextFieldNumbering getLabel28() {
        return this.label28;
    }

    public JLabel getAnexoBq03C12_label() {
        return this.anexoBq03C12_label;
    }

    public JLabelTextFieldNumbering getLabel30() {
        return this.label30;
    }

    public JLimitedTextField getAnexoBq03C11() {
        return this.anexoBq03C11;
    }

    public JLimitedTextField getAnexoBq03C12() {
        return this.anexoBq03C12;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getAnexoBq03C10_label() {
        return this.anexoBq03C10_label;
    }

    public JLabelTextFieldNumbering getLabel32() {
        return this.label32;
    }

    public JLimitedTextField getAnexoBq03C10() {
        return this.anexoBq03C10;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getLabel15() {
        return this.label15;
    }

    public JLabel getLabel16() {
        return this.label16;
    }

    public JLabel getAnexoBq03B13OPSim_base_label() {
        return this.anexoBq03B13OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel24() {
        return this.label24;
    }

    public JRadioButton getAnexoBq03B13OP13() {
        return this.anexoBq03B13OP13;
    }

    public JLabelTextFieldNumbering getLabel25() {
        return this.label25;
    }

    public JRadioButton getAnexoBq03B13OP14() {
        return this.anexoBq03B13OP14;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoBq03C06_label = new JLabel();
        this.label3 = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoBq03C06 = new JNIFTextField();
        this.anexoBq03C07_label = new JLabel();
        this.label6 = new JLabel();
        this.label7 = new JLabelTextFieldNumbering();
        this.anexoBq03C07 = new JNIFTextField();
        this.panel3 = new JPanel();
        this.label12 = new JLabel();
        this.label13 = new JLabel();
        this.anexoBq03B1OPSim_base_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoBq03B1OP1 = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.anexoBq03B1OP2 = new JRadioButton();
        this.panel4 = new JPanel();
        this.anexoBq03C08_label = new JLabel();
        this.label11 = new JLabelTextFieldNumbering();
        this.anexoBq03C08 = new JNIFTextField();
        this.anexoBq03C09_label = new JLabel();
        this.label20 = new JLabelTextFieldNumbering();
        this.anexoBq03C09 = new JNIFTextField();
        this.panel6 = new JPanel();
        this.anexoBq03C10_label = new JLabel();
        this.label32 = new JLabelTextFieldNumbering();
        this.anexoBq03C10 = new JLimitedTextField();
        this.panel5 = new JPanel();
        this.anexoBq03C11_label = new JLabel();
        this.label28 = new JLabelTextFieldNumbering();
        this.anexoBq03C11 = new JLimitedTextField();
        this.anexoBq03C12_label = new JLabel();
        this.label30 = new JLabelTextFieldNumbering();
        this.anexoBq03C12 = new JLimitedTextField();
        this.label10 = new JLabel();
        this.label14 = new JLabel();
        this.panel7 = new JPanel();
        this.label15 = new JLabel();
        this.label16 = new JLabel();
        this.anexoBq03B13OPSim_base_label = new JLabel();
        this.label24 = new JLabelTextFieldNumbering();
        this.anexoBq03B13OP13 = new JRadioButton();
        this.label25 = new JLabelTextFieldNumbering();
        this.anexoBq03B13OP14 = new JRadioButton();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro03Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro03Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "10*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("0px, default:grow, $lcgap, default, 15dlu, default, $rgap, 13dlu, 0px, 43dlu, $lcgap, default:grow, $lcgap, default, 0px, 15dlu, 0px, default, $lcgap, 13dlu, 43dlu, $rgap, default:grow", "$ugap, 2*($lgap, default)"));
        this.anexoBq03C06_label.setText(bundle.getString("Quadro03Panel.anexoBq03C06_label.text"));
        this.panel2.add((Component)this.anexoBq03C06_label, CC.xy(4, 3));
        this.label3.setText(bundle.getString("Quadro03Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label3, CC.xy(6, 3));
        this.label4.setText(bundle.getString("Quadro03Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(8, 3));
        this.anexoBq03C06.setColumns(9);
        this.anexoBq03C06.setEditable(false);
        this.panel2.add((Component)this.anexoBq03C06, CC.xy(10, 3));
        this.anexoBq03C07_label.setText(bundle.getString("Quadro03Panel.anexoBq03C07_label.text"));
        this.panel2.add((Component)this.anexoBq03C07_label, CC.xy(14, 3));
        this.label6.setText(bundle.getString("Quadro03Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label6, CC.xy(18, 3));
        this.label7.setText(bundle.getString("Quadro03Panel.label7.text"));
        this.label7.setBorder(new EtchedBorder());
        this.label7.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label7, CC.xy(20, 3));
        this.anexoBq03C07.setColumns(9);
        this.anexoBq03C07.setEditable(false);
        this.panel2.add((Component)this.anexoBq03C07, CC.xy(21, 3));
        this.this2.add((Component)this.panel2, CC.xy(2, 1));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:default, $rgap, default, $lcgap, 13dlu, 0px, left:default, $lcgap, default:grow, $rgap", "3*(default, $lgap), default"));
        this.label12.setText(bundle.getString("Quadro03Panel.label12.text"));
        this.label12.setBorder(new EtchedBorder());
        this.label12.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label12, CC.xy(1, 1));
        this.label13.setText(bundle.getString("Quadro03Panel.label13.text"));
        this.label13.setHorizontalAlignment(0);
        this.label13.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label13, CC.xywh(3, 1, 15, 1));
        this.anexoBq03B1OPSim_base_label.setText(bundle.getString("Quadro03Panel.anexoBq03B1OPSim_base_label.text"));
        this.panel3.add((Component)this.anexoBq03B1OPSim_base_label, CC.xy(5, 5));
        this.label22.setText(bundle.getString("Quadro03Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label22, CC.xy(7, 5));
        this.anexoBq03B1OP1.setText(bundle.getString("Quadro03Panel.anexoBq03B1OP1.text"));
        this.panel3.add((Component)this.anexoBq03B1OP1, CC.xy(9, 5));
        this.label23.setText(bundle.getString("Quadro03Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label23, CC.xy(13, 5));
        this.anexoBq03B1OP2.setText(bundle.getString("Quadro03Panel.anexoBq03B1OP2.text"));
        this.panel3.add((Component)this.anexoBq03B1OP2, CC.xy(15, 5));
        this.this2.add((Component)this.panel3, CC.xy(2, 7));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, 0px, $lcgap, default, $rgap, 13dlu, 0px, 43dlu, $lcgap, default:grow, 2*($lcgap, default), 0px, 43dlu, $lcgap, default:grow", "2*(default, $lgap), default"));
        this.anexoBq03C08_label.setText(bundle.getString("Quadro03Panel.anexoBq03C08_label.text"));
        this.anexoBq03C08_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoBq03C08_label, CC.xy(4, 3));
        this.label11.setText(bundle.getString("Quadro03Panel.label11.text"));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label11, CC.xy(6, 3));
        this.anexoBq03C08.setColumns(9);
        this.anexoBq03C08.setEditable(false);
        this.panel4.add((Component)this.anexoBq03C08, CC.xy(8, 3));
        this.anexoBq03C09_label.setText(bundle.getString("Quadro03Panel.anexoBq03C09_label.text"));
        this.anexoBq03C09_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoBq03C09_label, CC.xy(12, 3));
        this.label20.setText(bundle.getString("Quadro03Panel.label20.text"));
        this.label20.setBorder(new EtchedBorder());
        this.label20.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label20, CC.xy(14, 3));
        this.anexoBq03C09.setColumns(9);
        this.anexoBq03C09.setEditable(false);
        this.panel4.add((Component)this.anexoBq03C09, CC.xy(16, 3));
        this.this2.add((Component)this.panel4, CC.xy(2, 9));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("default:grow, 0px, $lcgap, default, $rgap, 13dlu, 0px, 24dlu, $lcgap, 0px, default:grow", "$ugap, $lgap, default, 0px, default"));
        this.anexoBq03C10_label.setText(bundle.getString("Quadro03Panel.anexoBq03C10_label.text"));
        this.anexoBq03C10_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq03C10_label, CC.xy(4, 3));
        this.label32.setText(bundle.getString("Quadro03Panel.label32.text"));
        this.label32.setBorder(new EtchedBorder());
        this.label32.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label32, CC.xy(6, 3));
        this.anexoBq03C10.setOnlyNumeric(true);
        this.anexoBq03C10.setMaxLength(4);
        this.anexoBq03C10.setColumns(5);
        this.anexoBq03C10.setHorizontalAlignment(4);
        this.panel6.add((Component)this.anexoBq03C10, CC.xy(8, 3));
        this.this2.add((Component)this.panel6, CC.xy(2, 11));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, $rgap, 0px, default, $rgap, 13dlu, 0px, default, $lcgap, 30dlu:grow, $lcgap, default, $lcgap, 13dlu, 0px, default, $lcgap, 10dlu", "default, 0px, default"));
        this.anexoBq03C11_label.setText(bundle.getString("Quadro03Panel.anexoBq03C11_label.text"));
        this.anexoBq03C11_label.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoBq03C11_label, CC.xy(4, 1));
        this.label28.setText(bundle.getString("Quadro03Panel.label28.text"));
        this.label28.setBorder(new EtchedBorder());
        this.label28.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label28, CC.xy(6, 1));
        this.anexoBq03C11.setOnlyNumeric(true);
        this.anexoBq03C11.setMaxLength(5);
        this.anexoBq03C11.setColumns(6);
        this.anexoBq03C11.setHorizontalAlignment(4);
        this.panel5.add((Component)this.anexoBq03C11, CC.xy(8, 1));
        this.anexoBq03C12_label.setText(bundle.getString("Quadro03Panel.anexoBq03C12_label.text"));
        this.anexoBq03C12_label.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoBq03C12_label, CC.xy(12, 1));
        this.label30.setText(bundle.getString("Quadro03Panel.label30.text"));
        this.label30.setBorder(new EtchedBorder());
        this.label30.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label30, CC.xy(14, 1));
        this.anexoBq03C12.setOnlyNumeric(true);
        this.anexoBq03C12.setMaxLength(5);
        this.anexoBq03C12.setColumns(6);
        this.anexoBq03C12.setHorizontalAlignment(4);
        this.panel5.add((Component)this.anexoBq03C12, CC.xy(16, 1));
        this.panel5.add((Component)this.label10, CC.xy(4, 3));
        this.panel5.add((Component)this.label14, CC.xy(12, 3));
        this.this2.add((Component)this.panel5, CC.xy(2, 15));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:default, $rgap, default, $lcgap, 13dlu, 0px, default, $lcgap, default:grow, $rgap", "3*(default, $lgap), default"));
        this.label15.setText(bundle.getString("Quadro03Panel.label15.text"));
        this.label15.setBorder(new EtchedBorder());
        this.label15.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label15, CC.xy(1, 1));
        this.label16.setText(bundle.getString("Quadro03Panel.label16.text"));
        this.label16.setHorizontalAlignment(0);
        this.label16.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.label16, CC.xywh(3, 1, 15, 1));
        this.anexoBq03B13OPSim_base_label.setText(bundle.getString("Quadro03Panel.anexoBq03B13OPSim_base_label.text"));
        this.panel7.add((Component)this.anexoBq03B13OPSim_base_label, CC.xy(5, 5));
        this.label24.setText(bundle.getString("Quadro03Panel.label24.text"));
        this.label24.setBorder(new EtchedBorder());
        this.label24.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label24, CC.xy(7, 5));
        this.anexoBq03B13OP13.setText(bundle.getString("Quadro03Panel.anexoBq03B13OP13.text"));
        this.panel7.add((Component)this.anexoBq03B13OP13, CC.xy(9, 5));
        this.label25.setText(bundle.getString("Quadro03Panel.label25.text"));
        this.label25.setBorder(new EtchedBorder());
        this.label25.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label25, CC.xy(13, 5));
        this.anexoBq03B13OP14.setText(bundle.getString("Quadro03Panel.anexoBq03B13OP14.text"));
        this.panel7.add((Component)this.anexoBq03B13OP14, CC.xy(15, 5));
        this.this2.add((Component)this.panel7, CC.xy(2, 21));
        this.add((Component)this.this2, "Center");
    }
}

