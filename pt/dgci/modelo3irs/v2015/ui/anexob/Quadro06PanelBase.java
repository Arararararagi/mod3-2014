/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro06Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro06PanelBase
extends Quadro06Panel
implements IBindablePanel<Quadro06> {
    private static final long serialVersionUID = 1;
    protected Quadro06 model;

    @Override
    public abstract void setModel(Quadro06 var1, boolean var2);

    @Override
    public Quadro06 getModel() {
        return this.model;
    }

    public Quadro06PanelBase(Quadro06 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

