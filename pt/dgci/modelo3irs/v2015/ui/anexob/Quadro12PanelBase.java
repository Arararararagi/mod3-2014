/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro12Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro12PanelBase
extends Quadro12Panel
implements IBindablePanel<Quadro12> {
    private static final long serialVersionUID = 1;
    protected Quadro12 model;

    @Override
    public abstract void setModel(Quadro12 var1, boolean var2);

    @Override
    public Quadro12 getModel() {
        return this.model;
    }

    public Quadro12PanelBase(Quadro12 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

