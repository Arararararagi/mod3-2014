/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro11Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoBq11C1101_label\u00a3anexoBq11C1102_label;
    protected JLabel anexoBq11C1103_label\u00a3anexoBq11C1104_label;
    protected JLabel anexoBq11C1105_label\u00a3anexoBq11C1106_label;
    protected JLabel anexoBq11C1101_base_label\u00a3anexoBq11C1103_base_label\u00a3anexoBq11C1105_base_label;
    protected JLabelTextFieldNumbering anexoBq11C1101_num;
    protected JMoneyTextField anexoBq11C1101;
    protected JLabelTextFieldNumbering anexoBq11C1103_num;
    protected JMoneyTextField anexoBq11C1103;
    protected JLabelTextFieldNumbering anexoBq11C1105_num;
    protected JMoneyTextField anexoBq11C1105;
    protected JLabel anexoBq11C1102_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1106_base_label;
    protected JLabelTextFieldNumbering anexoBq11C1102_num;
    protected JMoneyTextField anexoBq11C1102;
    protected JLabelTextFieldNumbering anexoBq11C1104_num;
    protected JMoneyTextField anexoBq11C1104;
    protected JLabelTextFieldNumbering anexoBq11C1106_num;
    protected JMoneyTextField anexoBq11C1106;
    protected JLabel anexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label;
    protected JLabelTextFieldNumbering anexoBq11C1107_num;
    protected JMoneyTextField anexoBq11C1107;
    protected JLabelTextFieldNumbering anexoBq11C1108_num;
    protected JMoneyTextField anexoBq11C1108;
    protected JLabelTextFieldNumbering anexoBq11C1109_num;
    protected JMoneyTextField anexoBq11C1109;

    public Quadro11Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1101_num() {
        return this.anexoBq11C1101_num;
    }

    public JMoneyTextField getAnexoBq11C1101() {
        return this.anexoBq11C1101;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1103_num() {
        return this.anexoBq11C1103_num;
    }

    public JMoneyTextField getAnexoBq11C1103() {
        return this.anexoBq11C1103;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1105_num() {
        return this.anexoBq11C1105_num;
    }

    public JMoneyTextField getAnexoBq11C1105() {
        return this.anexoBq11C1105;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1102_num() {
        return this.anexoBq11C1102_num;
    }

    public JMoneyTextField getAnexoBq11C1102() {
        return this.anexoBq11C1102;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1104_num() {
        return this.anexoBq11C1104_num;
    }

    public JMoneyTextField getAnexoBq11C1104() {
        return this.anexoBq11C1104;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1106_num() {
        return this.anexoBq11C1106_num;
    }

    public JMoneyTextField getAnexoBq11C1106() {
        return this.anexoBq11C1106;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoBq11C1101_label\u00a3anexoBq11C1102_label() {
        return this.anexoBq11C1101_label\u00a3anexoBq11C1102_label;
    }

    public JLabel getAnexoBq11C1103_label\u00a3anexoBq11C1104_label() {
        return this.anexoBq11C1103_label\u00a3anexoBq11C1104_label;
    }

    public JLabel getAnexoBq11C1105_label\u00a3anexoBq11C1106_label() {
        return this.anexoBq11C1105_label\u00a3anexoBq11C1106_label;
    }

    public JLabel getAnexoBq11C1101_base_label\u00a3anexoBq11C1103_base_label\u00a3anexoBq11C1105_base_label() {
        return this.anexoBq11C1101_base_label\u00a3anexoBq11C1103_base_label\u00a3anexoBq11C1105_base_label;
    }

    public JLabel getAnexoBq11C1102_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1106_base_label() {
        return this.anexoBq11C1102_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1106_base_label;
    }

    public JLabel getAnexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label() {
        return this.anexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1107_num() {
        return this.anexoBq11C1107_num;
    }

    public JMoneyTextField getAnexoBq11C1107() {
        return this.anexoBq11C1107;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1108_num() {
        return this.anexoBq11C1108_num;
    }

    public JMoneyTextField getAnexoBq11C1108() {
        return this.anexoBq11C1108;
    }

    public JLabelTextFieldNumbering getAnexoBq11C1109_num() {
        return this.anexoBq11C1109_num;
    }

    public JMoneyTextField getAnexoBq11C1109() {
        return this.anexoBq11C1109;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoBq11C1101_label\u00a3anexoBq11C1102_label = new JLabel();
        this.anexoBq11C1103_label\u00a3anexoBq11C1104_label = new JLabel();
        this.anexoBq11C1105_label\u00a3anexoBq11C1106_label = new JLabel();
        this.anexoBq11C1101_base_label\u00a3anexoBq11C1103_base_label\u00a3anexoBq11C1105_base_label = new JLabel();
        this.anexoBq11C1101_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1101 = new JMoneyTextField();
        this.anexoBq11C1103_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1103 = new JMoneyTextField();
        this.anexoBq11C1105_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1105 = new JMoneyTextField();
        this.anexoBq11C1102_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1106_base_label = new JLabel();
        this.anexoBq11C1102_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1102 = new JMoneyTextField();
        this.anexoBq11C1104_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1104 = new JMoneyTextField();
        this.anexoBq11C1106_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1106 = new JMoneyTextField();
        this.anexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label = new JLabel();
        this.anexoBq11C1107_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1107 = new JMoneyTextField();
        this.anexoBq11C1108_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1108 = new JMoneyTextField();
        this.anexoBq11C1109_num = new JLabelTextFieldNumbering();
        this.anexoBq11C1109 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro11Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro11Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "2*(default, $lgap), default"));
        this.panel2.setLayout(new FormLayout("$ugap, $lcgap, default:grow, $lcgap, 25dlu, 0px, 85dlu, 2*($lcgap, default), 0px, 85dlu, 2*($lcgap, default), 0px, 85dlu, $rgap", "4*(default, $lgap), default"));
        this.anexoBq11C1101_label\u00a3anexoBq11C1102_label.setText(bundle.getString("Quadro11Panel.anexoBq11C1101_label\u00a3anexoBq11C1102_label.text"));
        this.anexoBq11C1101_label\u00a3anexoBq11C1102_label.setBorder(new EtchedBorder());
        this.anexoBq11C1101_label\u00a3anexoBq11C1102_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1101_label\u00a3anexoBq11C1102_label, cc.xywh(5, 1, 3, 1));
        this.anexoBq11C1103_label\u00a3anexoBq11C1104_label.setText(bundle.getString("Quadro11Panel.anexoBq11C1103_label\u00a3anexoBq11C1104_label.text"));
        this.anexoBq11C1103_label\u00a3anexoBq11C1104_label.setBorder(new EtchedBorder());
        this.anexoBq11C1103_label\u00a3anexoBq11C1104_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1103_label\u00a3anexoBq11C1104_label, cc.xywh(11, 1, 3, 1));
        this.anexoBq11C1105_label\u00a3anexoBq11C1106_label.setText(bundle.getString("Quadro11Panel.anexoBq11C1105_label\u00a3anexoBq11C1106_label.text"));
        this.anexoBq11C1105_label\u00a3anexoBq11C1106_label.setBorder(new EtchedBorder());
        this.anexoBq11C1105_label\u00a3anexoBq11C1106_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1105_label\u00a3anexoBq11C1106_label, cc.xywh(17, 1, 3, 1));
        this.anexoBq11C1101_base_label\u00a3anexoBq11C1103_base_label\u00a3anexoBq11C1105_base_label.setText(bundle.getString("Quadro11Panel.anexoBq11C1101_base_label\u00a3anexoBq11C1103_base_label\u00a3anexoBq11C1105_base_label.text"));
        this.panel2.add((Component)this.anexoBq11C1101_base_label\u00a3anexoBq11C1103_base_label\u00a3anexoBq11C1105_base_label, cc.xy(3, 3));
        this.anexoBq11C1101_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1101_num.text"));
        this.anexoBq11C1101_num.setBorder(new EtchedBorder());
        this.anexoBq11C1101_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1101_num, cc.xy(5, 3));
        this.anexoBq11C1101.setColumns(13);
        this.panel2.add((Component)this.anexoBq11C1101, cc.xy(7, 3));
        this.anexoBq11C1103_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1103_num.text"));
        this.anexoBq11C1103_num.setBorder(new EtchedBorder());
        this.anexoBq11C1103_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1103_num, cc.xy(11, 3));
        this.panel2.add((Component)this.anexoBq11C1103, cc.xy(13, 3));
        this.anexoBq11C1105_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1105_num.text"));
        this.anexoBq11C1105_num.setBorder(new EtchedBorder());
        this.anexoBq11C1105_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1105_num, cc.xy(17, 3));
        this.panel2.add((Component)this.anexoBq11C1105, cc.xy(19, 3));
        this.anexoBq11C1102_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1106_base_label.setText(bundle.getString("Quadro11Panel.anexoBq11C1102_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1106_base_label.text"));
        this.panel2.add((Component)this.anexoBq11C1102_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1106_base_label, cc.xy(3, 5));
        this.anexoBq11C1102_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1102_num.text"));
        this.anexoBq11C1102_num.setBorder(new EtchedBorder());
        this.anexoBq11C1102_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1102_num, cc.xy(5, 5));
        this.anexoBq11C1102.setColumns(13);
        this.panel2.add((Component)this.anexoBq11C1102, cc.xy(7, 5));
        this.anexoBq11C1104_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1104_num.text"));
        this.anexoBq11C1104_num.setBorder(new EtchedBorder());
        this.anexoBq11C1104_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1104_num, cc.xy(11, 5));
        this.panel2.add((Component)this.anexoBq11C1104, cc.xy(13, 5));
        this.anexoBq11C1106_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1106_num.text"));
        this.anexoBq11C1106_num.setBorder(new EtchedBorder());
        this.anexoBq11C1106_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq11C1106_num, cc.xy(17, 5));
        this.panel2.add((Component)this.anexoBq11C1106, cc.xy(19, 5));
        this.anexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label.setText(bundle.getString("Quadro11Panel.anexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label.text"));
        this.anexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoBq11C1103_base_label\u00a3anexoBq11C1104_base_label\u00a3anexoBq11C1107_base_label, cc.xy(3, 7));
        this.anexoBq11C1107_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1107_num.text"));
        this.anexoBq11C1107_num.setHorizontalAlignment(0);
        this.anexoBq11C1107_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoBq11C1107_num, cc.xy(5, 7));
        this.anexoBq11C1107.setEditable(false);
        this.anexoBq11C1107.setColumns(15);
        this.panel2.add((Component)this.anexoBq11C1107, cc.xy(7, 7));
        this.anexoBq11C1108_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1108_num.text"));
        this.anexoBq11C1108_num.setHorizontalAlignment(0);
        this.anexoBq11C1108_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoBq11C1108_num, cc.xy(11, 7));
        this.anexoBq11C1108.setEditable(false);
        this.anexoBq11C1108.setColumns(15);
        this.panel2.add((Component)this.anexoBq11C1108, cc.xy(13, 7));
        this.anexoBq11C1109_num.setText(bundle.getString("Quadro11Panel.anexoBq11C1109_num.text"));
        this.anexoBq11C1109_num.setHorizontalAlignment(0);
        this.anexoBq11C1109_num.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoBq11C1109_num, cc.xy(17, 7));
        this.anexoBq11C1109.setEditable(false);
        this.anexoBq11C1109.setColumns(15);
        this.panel2.add((Component)this.anexoBq11C1109, cc.xy(19, 7));
        this.this2.add((Component)this.panel2, cc.xy(1, 3));
        this.add((Component)this.this2, "Center");
    }
}

