/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel anexoBq05C501_label;
    protected JLabelTextFieldNumbering anexoBq05C501_num;
    protected JMoneyTextField anexoBq05C501;

    public Quadro05Panel() {
        this.initComponents();
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoBq05C501_label() {
        return this.anexoBq05C501_label;
    }

    public JLabelTextFieldNumbering getAnexoBq05C501_num() {
        return this.anexoBq05C501_num;
    }

    public JMoneyTextField getAnexoBq05C501() {
        return this.anexoBq05C501;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.anexoBq05C501_label = new JLabel();
        this.anexoBq05C501_num = new JLabelTextFieldNumbering();
        this.anexoBq05C501 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel4.setLayout(new FormLayout("$ugap, $rgap, left:10dlu:grow, $lcgap, 25dlu, 0px, 70dlu, $rgap", "$rgap, default, $lgap, default"));
        this.anexoBq05C501_label.setText(bundle.getString("Quadro05Panel.anexoBq05C501_label.text"));
        this.panel4.add((Component)this.anexoBq05C501_label, cc.xy(3, 2));
        this.anexoBq05C501_num.setText(bundle.getString("Quadro05Panel.anexoBq05C501_num.text"));
        this.anexoBq05C501_num.setBorder(new EtchedBorder());
        this.anexoBq05C501_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq05C501_num, cc.xy(5, 2));
        this.panel4.add((Component)this.anexoBq05C501, cc.xy(7, 2));
        this.this2.add((Component)this.panel4, cc.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

