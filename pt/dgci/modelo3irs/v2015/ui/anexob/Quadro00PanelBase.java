/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.Quadro00;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro00Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro00PanelBase
extends Quadro00Panel
implements IBindablePanel<Quadro00> {
    private static final long serialVersionUID = 1;
    protected Quadro00 model;

    @Override
    public abstract void setModel(Quadro00 var1, boolean var2);

    @Override
    public Quadro00 getModel() {
        return this.model;
    }

    public Quadro00PanelBase(Quadro00 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

