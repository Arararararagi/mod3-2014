/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoBq07C701_label;
    protected JLabel anexoBq07C702_label;
    protected JLabel anexoBq07C703_label;
    protected JLabel anexoBq07C704_label;
    protected JLabelTextFieldNumbering anexoBq07C701_num;
    protected JMoneyTextField anexoBq07C701;
    protected JLabelTextFieldNumbering anexoBq07C702_num;
    protected JMoneyTextField anexoBq07C702;
    protected JLabelTextFieldNumbering anexoBq07C703_num;
    protected JMoneyTextField anexoBq07C703;
    protected JLabelTextFieldNumbering anexoBq07C704_num;
    protected JMoneyTextField anexoBq07C704;
    protected JPanel panel3;
    protected JLabel label10;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoBq07T1Scroll;
    protected JTable anexoBq07T1;

    public Quadro07Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getAnexoBq07C701_num() {
        return this.anexoBq07C701_num;
    }

    public JMoneyTextField getAnexoBq07C701() {
        return this.anexoBq07C701;
    }

    public JLabelTextFieldNumbering getAnexoBq07C702_num() {
        return this.anexoBq07C702_num;
    }

    public JMoneyTextField getAnexoBq07C702() {
        return this.anexoBq07C702;
    }

    public JLabelTextFieldNumbering getAnexoBq07C703_num() {
        return this.anexoBq07C703_num;
    }

    public JMoneyTextField getAnexoBq07C703() {
        return this.anexoBq07C703;
    }

    public JLabelTextFieldNumbering getAnexoBq07C704_num() {
        return this.anexoBq07C704_num;
    }

    public JMoneyTextField getAnexoBq07C704() {
        return this.anexoBq07C704;
    }

    public JLabel getAnexoBq07C701_label() {
        return this.anexoBq07C701_label;
    }

    public JLabel getAnexoBq07C702_label() {
        return this.anexoBq07C702_label;
    }

    public JLabel getAnexoBq07C703_label() {
        return this.anexoBq07C703_label;
    }

    public JLabel getAnexoBq07C704_label() {
        return this.anexoBq07C704_label;
    }

    protected void addLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoBq07T1Scroll() {
        return this.anexoBq07T1Scroll;
    }

    public JTable getAnexoBq07T1() {
        return this.anexoBq07T1;
    }

    protected void removeLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoBq07C701_label = new JLabel();
        this.anexoBq07C702_label = new JLabel();
        this.anexoBq07C703_label = new JLabel();
        this.anexoBq07C704_label = new JLabel();
        this.anexoBq07C701_num = new JLabelTextFieldNumbering();
        this.anexoBq07C701 = new JMoneyTextField();
        this.anexoBq07C702_num = new JLabelTextFieldNumbering();
        this.anexoBq07C702 = new JMoneyTextField();
        this.anexoBq07C703_num = new JLabelTextFieldNumbering();
        this.anexoBq07C703 = new JMoneyTextField();
        this.anexoBq07C704_num = new JLabelTextFieldNumbering();
        this.anexoBq07C704 = new JMoneyTextField();
        this.panel3 = new JPanel();
        this.label10 = new JLabel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoBq07T1Scroll = new JScrollPane();
        this.anexoBq07T1 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "3*(default, $lgap), default"));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, 5dlu, $lcgap, 25dlu, 0px, 95dlu, $lcgap, 5dlu, $ugap, default, $ugap, 25dlu, 0px, 75dlu, $ugap, default, $ugap, 25dlu, 0px, 75dlu, $ugap, default, $ugap, 25dlu, 0px, 75dlu, $lcgap, default:grow, $rgap", "$rgap, 3*(default, $lgap), default"));
        this.anexoBq07C701_label.setText(bundle.getString("Quadro07Panel.anexoBq07C701_label.text"));
        this.anexoBq07C701_label.setHorizontalAlignment(0);
        this.anexoBq07C701_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoBq07C701_label, cc.xywh(3, 2, 5, 1));
        this.anexoBq07C702_label.setText(bundle.getString("Quadro07Panel.anexoBq07C702_label.text"));
        this.anexoBq07C702_label.setHorizontalAlignment(0);
        this.anexoBq07C702_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoBq07C702_label, cc.xywh(13, 2, 3, 1));
        this.anexoBq07C703_label.setText(bundle.getString("Quadro07Panel.anexoBq07C703_label.text"));
        this.anexoBq07C703_label.setHorizontalAlignment(0);
        this.anexoBq07C703_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoBq07C703_label, cc.xywh(19, 2, 3, 1));
        this.anexoBq07C704_label.setText(bundle.getString("Quadro07Panel.anexoBq07C704_label.text"));
        this.anexoBq07C704_label.setHorizontalAlignment(0);
        this.anexoBq07C704_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.anexoBq07C704_label, cc.xywh(25, 2, 3, 1));
        this.anexoBq07C701_num.setText(bundle.getString("Quadro07Panel.anexoBq07C701_num.text"));
        this.anexoBq07C701_num.setBorder(new EtchedBorder());
        this.anexoBq07C701_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq07C701_num, cc.xy(5, 4));
        this.anexoBq07C701.setColumns(13);
        this.panel2.add((Component)this.anexoBq07C701, cc.xy(7, 4));
        this.anexoBq07C702_num.setText(bundle.getString("Quadro07Panel.anexoBq07C702_num.text"));
        this.anexoBq07C702_num.setBorder(new EtchedBorder());
        this.anexoBq07C702_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq07C702_num, cc.xy(13, 4));
        this.anexoBq07C702.setColumns(13);
        this.panel2.add((Component)this.anexoBq07C702, cc.xy(15, 4));
        this.anexoBq07C703_num.setText(bundle.getString("Quadro07Panel.anexoBq07C703_num.text"));
        this.anexoBq07C703_num.setBorder(new EtchedBorder());
        this.anexoBq07C703_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq07C703_num, cc.xy(19, 4));
        this.anexoBq07C703.setColumns(13);
        this.panel2.add((Component)this.anexoBq07C703, cc.xy(21, 4));
        this.anexoBq07C704_num.setText(bundle.getString("Quadro07Panel.anexoBq07C704_num.text"));
        this.anexoBq07C704_num.setBorder(new EtchedBorder());
        this.anexoBq07C704_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq07C704_num, cc.xy(25, 4));
        this.anexoBq07C704.setColumns(13);
        this.anexoBq07C704.setEditable(false);
        this.panel2.add((Component)this.anexoBq07C704, cc.xy(27, 4));
        this.this2.add((Component)this.panel2, cc.xy(2, 3));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 50dlu, $lcgap, default:grow, $rgap", "2*(default, $lgap), fill:165dlu, $lgap, default"));
        this.label10.setText(bundle.getString("Quadro07Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label10, cc.xywh(1, 1, 9, 1));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro07Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.addLineAnexoBq07T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro07Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.removeLineAnexoBq07T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 3));
        this.anexoBq07T1Scroll.setViewportView(this.anexoBq07T1);
        this.panel3.add((Component)this.anexoBq07T1Scroll, cc.xywh(3, 5, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 7));
        this.add((Component)this.this2, "Center");
    }

}

