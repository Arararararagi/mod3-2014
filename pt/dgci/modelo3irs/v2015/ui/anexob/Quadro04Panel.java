/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label2;
    protected JLabel label3;
    protected JPanel panel6;
    protected JLabel label6;
    protected JLabel anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label;
    protected JLabel anexoBq04C401_base_label;
    protected JLabelTextFieldNumbering anexoBq04C401_num;
    protected JMoneyTextField anexoBq04C401;
    protected JLabel anexoBq04C402_base_label;
    protected JLabelTextFieldNumbering anexoBq04C402_num;
    protected JMoneyTextField anexoBq04C402;
    protected JLabel anexoBq04C403_base_label;
    protected JLabelTextFieldNumbering anexoBq04C403_num;
    protected JMoneyTextField anexoBq04C403;
    protected JLabel anexoBq04C440_base_label;
    protected JLabelTextFieldNumbering anexoBq04C440_num;
    protected JMoneyTextField anexoBq04C440;
    protected JLabel anexoBq04C441_base_label;
    protected JLabelTextFieldNumbering anexoBq04C441_num;
    protected JMoneyTextField anexoBq04C441;
    protected JLabel anexoBq04C404_base_label;
    protected JLabelTextFieldNumbering anexoBq04C404_num;
    protected JMoneyTextField anexoBq04C404;
    protected JLabel anexoBq04C442_base_label;
    protected JLabelTextFieldNumbering anexoBq04C442_num;
    protected JMoneyTextField anexoBq04C442;
    protected JLabel anexoBq04C405_base_label;
    protected JLabelTextFieldNumbering anexoBq04C405_num;
    protected JMoneyTextField anexoBq04C405;
    protected JLabel anexoBq04C420_base_label;
    protected JLabelTextFieldNumbering anexoBq04C420_num;
    protected JMoneyTextField anexoBq04C420;
    protected JLabel anexoBq04C421_base_label;
    protected JLabelTextFieldNumbering anexoBq04C421_num;
    protected JMoneyTextField anexoBq04C421;
    protected JLabel anexoBq04C422_base_label;
    protected JLabelTextFieldNumbering anexoBq04C422_num;
    protected JMoneyTextField anexoBq04C422;
    protected JLabel anexoBq04C423_base_label;
    protected JLabelTextFieldNumbering anexoBq04C423_num;
    protected JMoneyTextField anexoBq04C423;
    protected JLabel anexoBq04C424_base_label;
    protected JLabelTextFieldNumbering anexoBq04C424_num;
    protected JMoneyTextField anexoBq04C424;
    protected JLabel anexoBq04C425_base_label;
    protected JLabelTextFieldNumbering anexoBq04C425_num;
    protected JMoneyTextField anexoBq04C425;
    protected JLabel anexoBq04C443_base_label;
    protected JLabelTextFieldNumbering anexoBq04C443_num;
    protected JMoneyTextField anexoBq04C443;
    protected JLabel anexoBq04C1_base_label;
    protected JMoneyTextField anexoBq04C1;
    protected JLabel label7;
    protected JLabel anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label;
    protected JLabel anexoBq04C406_base_label;
    protected JLabelTextFieldNumbering anexoBq04C406_num;
    protected JMoneyTextField anexoBq04C406;
    protected JLabel anexoBq04C407_base_label;
    protected JLabelTextFieldNumbering anexoBq04C407_num;
    protected JMoneyTextField anexoBq04C407;
    protected JLabel anexoBq04C2_base_label;
    protected JMoneyTextField anexoBq04C2;
    protected JPanel panel3;
    protected JLabel label8;
    protected JLabel label9;
    protected JPanel panel7;
    protected JLabel label18;
    protected JLabel anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label;
    protected JLabel anexoBq04C409_base_label;
    protected JLabelTextFieldNumbering anexoBq04C409_num;
    protected JMoneyTextField anexoBq04C409;
    protected JLabel anexoBq04C410_base_label;
    protected JLabelTextFieldNumbering anexoBq04C410_num;
    protected JMoneyTextField anexoBq04C410;
    protected JLabel anexoBq04C444_base_label;
    protected JLabelTextFieldNumbering anexoBq04C444_num;
    protected JMoneyTextField anexoBq04C444;
    protected JLabel anexoBq04C445_base_label;
    protected JLabelTextFieldNumbering anexoBq04C445_num;
    protected JMoneyTextField anexoBq04C445;
    protected JLabel anexoBq04C411_base_label;
    protected JLabelTextFieldNumbering anexoBq04C411_num;
    protected JMoneyTextField anexoBq04C411;
    protected JLabel anexoBq04C426_base_label;
    protected JLabelTextFieldNumbering anexoBq04C426_num;
    protected JMoneyTextField anexoBq04C426;
    protected JLabel anexoBq04C446_base_label;
    protected JLabelTextFieldNumbering anexoBq04C446_num;
    protected JMoneyTextField anexoBq04C446;
    protected JLabel anexoBq04C3_base_label;
    protected JMoneyTextField anexoBq04C3;
    protected JLabel label21;
    protected JLabel anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label;
    protected JLabel anexoBq04C413_base_label;
    protected JLabelTextFieldNumbering anexoBq04C413_num;
    protected JMoneyTextField anexoBq04C413;
    protected JLabel anexoBq04C414_base_label;
    protected JLabelTextFieldNumbering anexoBq04C414_num;
    protected JMoneyTextField anexoBq04C414;
    protected JLabel anexoBq04C4_base_label;
    protected JMoneyTextField anexoBq04C4;
    protected JPanel panel5;
    protected JLabel label20;
    protected JLabel label22;
    protected JLabel anexoBq04B1OPSim_base_label;
    protected JLabelTextFieldNumbering label50;
    protected JRadioButton anexoBq04B1OP1;
    protected JLabelTextFieldNumbering label51;
    protected JRadioButton anexoBq04B1OP2;
    protected JLabel anexoBq04B2OPSim_base_label;
    protected JLabelTextFieldNumbering label53;
    protected JRadioButton anexoBq04B2OP3;
    protected JLabelTextFieldNumbering label54;
    protected JRadioButton anexoBq04B2OP4;
    protected JPanel panel8;
    protected JLabel label55;
    protected JLabel label56;
    protected JLabel anexoBq04B3OPSim_base_label;
    protected JLabelTextFieldNumbering label59;
    protected JRadioButton anexoBq04B3OP1;
    protected JLabelTextFieldNumbering label68;
    protected JRadioButton anexoBq04B3OP2;
    protected JLabel label69;
    protected JPanel panel4;
    protected JLabel label10;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoBq04T1Scroll;
    protected JTable anexoBq04T1;
    protected JPanel panel9;
    protected JLabel label57;
    protected JLabel label11;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button7;
    protected JRemoveLineIconableButton button8;
    protected JScrollPane anexoBq04T2Scroll;
    protected JTable anexoBq04T2;
    protected JPanel panel10;
    protected JLabel label58;
    protected JLabel label12;
    protected JToolBar toolBar3;
    protected JAddLineIconableButton button5;
    protected JRemoveLineIconableButton button6;
    protected JScrollPane anexoBq04T3Scroll;
    protected JTable anexoBq04T3;

    public Quadro04Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getAnexoBq04C401_base_label() {
        return this.anexoBq04C401_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C401_num() {
        return this.anexoBq04C401_num;
    }

    public JMoneyTextField getAnexoBq04C401() {
        return this.anexoBq04C401;
    }

    public JLabel getAnexoBq04C402_base_label() {
        return this.anexoBq04C402_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C402_num() {
        return this.anexoBq04C402_num;
    }

    public JMoneyTextField getAnexoBq04C402() {
        return this.anexoBq04C402;
    }

    public JLabel getAnexoBq04C403_base_label() {
        return this.anexoBq04C403_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C403_num() {
        return this.anexoBq04C403_num;
    }

    public JMoneyTextField getAnexoBq04C403() {
        return this.anexoBq04C403;
    }

    public JLabel getAnexoBq04C404_base_label() {
        return this.anexoBq04C404_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C404_num() {
        return this.anexoBq04C404_num;
    }

    public JLabel getAnexoBq04C405_base_label() {
        return this.anexoBq04C405_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C405_num() {
        return this.anexoBq04C405_num;
    }

    public JLabel getAnexoBq04C420_base_label() {
        return this.anexoBq04C420_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C420_num() {
        return this.anexoBq04C420_num;
    }

    public JMoneyTextField getAnexoBq04C420() {
        return this.anexoBq04C420;
    }

    public JLabel getAnexoBq04C421_base_label() {
        return this.anexoBq04C421_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C421_num() {
        return this.anexoBq04C421_num;
    }

    public JLabel getAnexoBq04C422_base_label() {
        return this.anexoBq04C422_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C422_num() {
        return this.anexoBq04C422_num;
    }

    public JLabel getAnexoBq04C1_base_label() {
        return this.anexoBq04C1_base_label;
    }

    public JMoneyTextField getAnexoBq04C1() {
        return this.anexoBq04C1;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JLabel getLabel9() {
        return this.label9;
    }

    public JLabel getLabel18() {
        return this.label18;
    }

    public JLabel getAnexoBq04C411_base_label() {
        return this.anexoBq04C411_base_label;
    }

    public JMoneyTextField getAnexoBq04C404() {
        return this.anexoBq04C404;
    }

    public JMoneyTextField getAnexoBq04C405() {
        return this.anexoBq04C405;
    }

    public JMoneyTextField getAnexoBq04C421() {
        return this.anexoBq04C421;
    }

    public JMoneyTextField getAnexoBq04C422() {
        return this.anexoBq04C422;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JLabel getAnexoBq04C406_base_label() {
        return this.anexoBq04C406_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C406_num() {
        return this.anexoBq04C406_num;
    }

    public JMoneyTextField getAnexoBq04C406() {
        return this.anexoBq04C406;
    }

    public JLabel getAnexoBq04C407_base_label() {
        return this.anexoBq04C407_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C407_num() {
        return this.anexoBq04C407_num;
    }

    public JMoneyTextField getAnexoBq04C407() {
        return this.anexoBq04C407;
    }

    public JLabel getAnexoBq04C2_base_label() {
        return this.anexoBq04C2_base_label;
    }

    public JMoneyTextField getAnexoBq04C2() {
        return this.anexoBq04C2;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getAnexoBq04C409_base_label() {
        return this.anexoBq04C409_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C409_num() {
        return this.anexoBq04C409_num;
    }

    public JMoneyTextField getAnexoBq04C409() {
        return this.anexoBq04C409;
    }

    public JLabel getAnexoBq04C410_base_label() {
        return this.anexoBq04C410_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C410_num() {
        return this.anexoBq04C410_num;
    }

    public JMoneyTextField getAnexoBq04C410() {
        return this.anexoBq04C410;
    }

    public JMoneyTextField getAnexoBq04C411() {
        return this.anexoBq04C411;
    }

    public JLabelTextFieldNumbering getAnexoBq04C411_num() {
        return this.anexoBq04C411_num;
    }

    public JLabel getAnexoBq04C3_base_label() {
        return this.anexoBq04C3_base_label;
    }

    public JMoneyTextField getAnexoBq04C3() {
        return this.anexoBq04C3;
    }

    public JLabel getLabel21() {
        return this.label21;
    }

    public JLabel getAnexoBq04C413_base_label() {
        return this.anexoBq04C413_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C413_num() {
        return this.anexoBq04C413_num;
    }

    public JMoneyTextField getAnexoBq04C413() {
        return this.anexoBq04C413;
    }

    public JLabel getAnexoBq04C414_base_label() {
        return this.anexoBq04C414_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C414_num() {
        return this.anexoBq04C414_num;
    }

    public JMoneyTextField getAnexoBq04C414() {
        return this.anexoBq04C414;
    }

    public JLabel getAnexoBq04C4_base_label() {
        return this.anexoBq04C4_base_label;
    }

    public JMoneyTextField getAnexoBq04C4() {
        return this.anexoBq04C4;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getLabel20() {
        return this.label20;
    }

    public JLabel getLabel22() {
        return this.label22;
    }

    public JLabel getAnexoBq04B1OPSim_base_label() {
        return this.anexoBq04B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel50() {
        return this.label50;
    }

    public JLabelTextFieldNumbering getLabel51() {
        return this.label51;
    }

    public JLabel getAnexoBq04B2OPSim_base_label() {
        return this.anexoBq04B2OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel53() {
        return this.label53;
    }

    public JLabelTextFieldNumbering getLabel54() {
        return this.label54;
    }

    public JRadioButton getAnexoBq04B1OP1() {
        return this.anexoBq04B1OP1;
    }

    public JRadioButton getAnexoBq04B1OP2() {
        return this.anexoBq04B1OP2;
    }

    public JRadioButton getAnexoBq04B2OP3() {
        return this.anexoBq04B2OP3;
    }

    public JRadioButton getAnexoBq04B2OP4() {
        return this.anexoBq04B2OP4;
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JLabel getLabel55() {
        return this.label55;
    }

    public JLabel getLabel56() {
        return this.label56;
    }

    public JLabel getAnexoBq04B3OPSim_base_label() {
        return this.anexoBq04B3OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel59() {
        return this.label59;
    }

    public JRadioButton getAnexoBq04B3OP1() {
        return this.anexoBq04B3OP1;
    }

    public JLabelTextFieldNumbering getLabel68() {
        return this.label68;
    }

    public JRadioButton getAnexoBq04B3OP2() {
        return this.anexoBq04B3OP2;
    }

    public JLabel getLabel69() {
        return this.label69;
    }

    protected void addLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoHq04T4_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoBq04T1Scroll() {
        return this.anexoBq04T1Scroll;
    }

    public JTable getAnexoBq04T1() {
        return this.anexoBq04T1;
    }

    protected void button1ActionPerformed(ActionEvent e) {
    }

    protected void addLineAnexoBq04T3_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoBq04T3_LinhaActionPerformed(ActionEvent e) {
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label() {
        return this.anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label;
    }

    public JLabel getAnexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label() {
        return this.anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label;
    }

    public JLabel getAnexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label() {
        return this.anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label;
    }

    public JLabel getAnexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label() {
        return this.anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JScrollPane getAnexoBq04T2Scroll() {
        return this.anexoBq04T2Scroll;
    }

    public JTable getAnexoBq04T2() {
        return this.anexoBq04T2;
    }

    protected void addLineAnexoBq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoBq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void addLineAnexoBq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoBq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    public JLabel getAnexoBq04C423_base_label() {
        return this.anexoBq04C423_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C423_num() {
        return this.anexoBq04C423_num;
    }

    public JMoneyTextField getAnexoBq04C423() {
        return this.anexoBq04C423;
    }

    public JLabel getAnexoBq04C424_base_label() {
        return this.anexoBq04C424_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C424_num() {
        return this.anexoBq04C424_num;
    }

    public JMoneyTextField getAnexoBq04C424() {
        return this.anexoBq04C424;
    }

    public JLabel getAnexoBq04C425_base_label() {
        return this.anexoBq04C425_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C425_num() {
        return this.anexoBq04C425_num;
    }

    public JMoneyTextField getAnexoBq04C425() {
        return this.anexoBq04C425;
    }

    public JLabel getAnexoBq04C426_base_label() {
        return this.anexoBq04C426_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C426_num() {
        return this.anexoBq04C426_num;
    }

    public JMoneyTextField getAnexoBq04C426() {
        return this.anexoBq04C426;
    }

    public JToolBar getToolBar3() {
        return this.toolBar3;
    }

    public JAddLineIconableButton getButton5() {
        return this.button5;
    }

    public JRemoveLineIconableButton getButton6() {
        return this.button6;
    }

    public JScrollPane getAnexoBq04T3Scroll() {
        return this.anexoBq04T3Scroll;
    }

    public JTable getAnexoBq04T3() {
        return this.anexoBq04T3;
    }

    public JAddLineIconableButton getButton7() {
        return this.button7;
    }

    public JRemoveLineIconableButton getButton8() {
        return this.button8;
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JLabel getLabel57() {
        return this.label57;
    }

    public JLabel getLabel58() {
        return this.label58;
    }

    public JLabel getLabel12() {
        return this.label12;
    }

    public JLabel getAnexoBq04C440_base_label() {
        return this.anexoBq04C440_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C440_num() {
        return this.anexoBq04C440_num;
    }

    public JMoneyTextField getAnexoBq04C440() {
        return this.anexoBq04C440;
    }

    public JLabel getAnexoBq04C441_base_label() {
        return this.anexoBq04C441_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C441_num() {
        return this.anexoBq04C441_num;
    }

    public JMoneyTextField getAnexoBq04C441() {
        return this.anexoBq04C441;
    }

    public JLabel getAnexoBq04C442_base_label() {
        return this.anexoBq04C442_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C442_num() {
        return this.anexoBq04C442_num;
    }

    public JMoneyTextField getAnexoBq04C442() {
        return this.anexoBq04C442;
    }

    public JLabel getAnexoBq04C443_base_label() {
        return this.anexoBq04C443_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C443_num() {
        return this.anexoBq04C443_num;
    }

    public JMoneyTextField getAnexoBq04C443() {
        return this.anexoBq04C443;
    }

    public JLabel getAnexoBq04C444_base_label() {
        return this.anexoBq04C444_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C444_num() {
        return this.anexoBq04C444_num;
    }

    public JMoneyTextField getAnexoBq04C444() {
        return this.anexoBq04C444;
    }

    public JLabel getAnexoBq04C445_base_label() {
        return this.anexoBq04C445_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C445_num() {
        return this.anexoBq04C445_num;
    }

    public JMoneyTextField getAnexoBq04C445() {
        return this.anexoBq04C445;
    }

    public JLabel getAnexoBq04C446_base_label() {
        return this.anexoBq04C446_base_label;
    }

    public JLabelTextFieldNumbering getAnexoBq04C446_num() {
        return this.anexoBq04C446_num;
    }

    public JMoneyTextField getAnexoBq04C446() {
        return this.anexoBq04C446;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label2 = new JLabel();
        this.label3 = new JLabel();
        this.panel6 = new JPanel();
        this.label6 = new JLabel();
        this.anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label = new JLabel();
        this.anexoBq04C401_base_label = new JLabel();
        this.anexoBq04C401_num = new JLabelTextFieldNumbering();
        this.anexoBq04C401 = new JMoneyTextField();
        this.anexoBq04C402_base_label = new JLabel();
        this.anexoBq04C402_num = new JLabelTextFieldNumbering();
        this.anexoBq04C402 = new JMoneyTextField();
        this.anexoBq04C403_base_label = new JLabel();
        this.anexoBq04C403_num = new JLabelTextFieldNumbering();
        this.anexoBq04C403 = new JMoneyTextField();
        this.anexoBq04C440_base_label = new JLabel();
        this.anexoBq04C440_num = new JLabelTextFieldNumbering();
        this.anexoBq04C440 = new JMoneyTextField();
        this.anexoBq04C441_base_label = new JLabel();
        this.anexoBq04C441_num = new JLabelTextFieldNumbering();
        this.anexoBq04C441 = new JMoneyTextField();
        this.anexoBq04C404_base_label = new JLabel();
        this.anexoBq04C404_num = new JLabelTextFieldNumbering();
        this.anexoBq04C404 = new JMoneyTextField();
        this.anexoBq04C442_base_label = new JLabel();
        this.anexoBq04C442_num = new JLabelTextFieldNumbering();
        this.anexoBq04C442 = new JMoneyTextField();
        this.anexoBq04C405_base_label = new JLabel();
        this.anexoBq04C405_num = new JLabelTextFieldNumbering();
        this.anexoBq04C405 = new JMoneyTextField();
        this.anexoBq04C420_base_label = new JLabel();
        this.anexoBq04C420_num = new JLabelTextFieldNumbering();
        this.anexoBq04C420 = new JMoneyTextField();
        this.anexoBq04C421_base_label = new JLabel();
        this.anexoBq04C421_num = new JLabelTextFieldNumbering();
        this.anexoBq04C421 = new JMoneyTextField();
        this.anexoBq04C422_base_label = new JLabel();
        this.anexoBq04C422_num = new JLabelTextFieldNumbering();
        this.anexoBq04C422 = new JMoneyTextField();
        this.anexoBq04C423_base_label = new JLabel();
        this.anexoBq04C423_num = new JLabelTextFieldNumbering();
        this.anexoBq04C423 = new JMoneyTextField();
        this.anexoBq04C424_base_label = new JLabel();
        this.anexoBq04C424_num = new JLabelTextFieldNumbering();
        this.anexoBq04C424 = new JMoneyTextField();
        this.anexoBq04C425_base_label = new JLabel();
        this.anexoBq04C425_num = new JLabelTextFieldNumbering();
        this.anexoBq04C425 = new JMoneyTextField();
        this.anexoBq04C443_base_label = new JLabel();
        this.anexoBq04C443_num = new JLabelTextFieldNumbering();
        this.anexoBq04C443 = new JMoneyTextField();
        this.anexoBq04C1_base_label = new JLabel();
        this.anexoBq04C1 = new JMoneyTextField();
        this.label7 = new JLabel();
        this.anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label = new JLabel();
        this.anexoBq04C406_base_label = new JLabel();
        this.anexoBq04C406_num = new JLabelTextFieldNumbering();
        this.anexoBq04C406 = new JMoneyTextField();
        this.anexoBq04C407_base_label = new JLabel();
        this.anexoBq04C407_num = new JLabelTextFieldNumbering();
        this.anexoBq04C407 = new JMoneyTextField();
        this.anexoBq04C2_base_label = new JLabel();
        this.anexoBq04C2 = new JMoneyTextField();
        this.panel3 = new JPanel();
        this.label8 = new JLabel();
        this.label9 = new JLabel();
        this.panel7 = new JPanel();
        this.label18 = new JLabel();
        this.anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label = new JLabel();
        this.anexoBq04C409_base_label = new JLabel();
        this.anexoBq04C409_num = new JLabelTextFieldNumbering();
        this.anexoBq04C409 = new JMoneyTextField();
        this.anexoBq04C410_base_label = new JLabel();
        this.anexoBq04C410_num = new JLabelTextFieldNumbering();
        this.anexoBq04C410 = new JMoneyTextField();
        this.anexoBq04C444_base_label = new JLabel();
        this.anexoBq04C444_num = new JLabelTextFieldNumbering();
        this.anexoBq04C444 = new JMoneyTextField();
        this.anexoBq04C445_base_label = new JLabel();
        this.anexoBq04C445_num = new JLabelTextFieldNumbering();
        this.anexoBq04C445 = new JMoneyTextField();
        this.anexoBq04C411_base_label = new JLabel();
        this.anexoBq04C411_num = new JLabelTextFieldNumbering();
        this.anexoBq04C411 = new JMoneyTextField();
        this.anexoBq04C426_base_label = new JLabel();
        this.anexoBq04C426_num = new JLabelTextFieldNumbering();
        this.anexoBq04C426 = new JMoneyTextField();
        this.anexoBq04C446_base_label = new JLabel();
        this.anexoBq04C446_num = new JLabelTextFieldNumbering();
        this.anexoBq04C446 = new JMoneyTextField();
        this.anexoBq04C3_base_label = new JLabel();
        this.anexoBq04C3 = new JMoneyTextField();
        this.label21 = new JLabel();
        this.anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label = new JLabel();
        this.anexoBq04C413_base_label = new JLabel();
        this.anexoBq04C413_num = new JLabelTextFieldNumbering();
        this.anexoBq04C413 = new JMoneyTextField();
        this.anexoBq04C414_base_label = new JLabel();
        this.anexoBq04C414_num = new JLabelTextFieldNumbering();
        this.anexoBq04C414 = new JMoneyTextField();
        this.anexoBq04C4_base_label = new JLabel();
        this.anexoBq04C4 = new JMoneyTextField();
        this.panel5 = new JPanel();
        this.label20 = new JLabel();
        this.label22 = new JLabel();
        this.anexoBq04B1OPSim_base_label = new JLabel();
        this.label50 = new JLabelTextFieldNumbering();
        this.anexoBq04B1OP1 = new JRadioButton();
        this.label51 = new JLabelTextFieldNumbering();
        this.anexoBq04B1OP2 = new JRadioButton();
        this.anexoBq04B2OPSim_base_label = new JLabel();
        this.label53 = new JLabelTextFieldNumbering();
        this.anexoBq04B2OP3 = new JRadioButton();
        this.label54 = new JLabelTextFieldNumbering();
        this.anexoBq04B2OP4 = new JRadioButton();
        this.panel8 = new JPanel();
        this.label55 = new JLabel();
        this.label56 = new JLabel();
        this.anexoBq04B3OPSim_base_label = new JLabel();
        this.label59 = new JLabelTextFieldNumbering();
        this.anexoBq04B3OP1 = new JRadioButton();
        this.label68 = new JLabelTextFieldNumbering();
        this.anexoBq04B3OP2 = new JRadioButton();
        this.label69 = new JLabel();
        this.panel4 = new JPanel();
        this.label10 = new JLabel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoBq04T1Scroll = new JScrollPane();
        this.anexoBq04T1 = new JTable();
        this.panel9 = new JPanel();
        this.label57 = new JLabel();
        this.label11 = new JLabel();
        this.toolBar2 = new JToolBar();
        this.button7 = new JAddLineIconableButton();
        this.button8 = new JRemoveLineIconableButton();
        this.anexoBq04T2Scroll = new JScrollPane();
        this.anexoBq04T2 = new JTable();
        this.panel10 = new JPanel();
        this.label58 = new JLabel();
        this.label12 = new JLabel();
        this.toolBar3 = new JToolBar();
        this.button5 = new JAddLineIconableButton();
        this.button6 = new JRemoveLineIconableButton();
        this.anexoBq04T3Scroll = new JScrollPane();
        this.anexoBq04T3 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $lcgap, default", "11*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:default, $rgap, default, $lcgap, 13dlu, 0px, default:grow, $rgap", "$ugap, 2*($lgap, default)"));
        this.label2.setText(bundle.getString("Quadro04Panel.label2.text"));
        this.label2.setBorder(new EtchedBorder());
        this.label2.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label2, cc.xy(1, 3));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label3, cc.xywh(3, 3, 13, 1));
        this.this2.add((Component)this.panel2, cc.xy(2, 1));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("$ugap, 200dlu:grow, $lcgap, 25dlu, $rgap, 75dlu, $lcgap, $rgap", "22*(default, $lgap), default"));
        this.label6.setBorder(new EtchedBorder());
        this.label6.setText(bundle.getString("Quadro04Panel.label6.text"));
        this.panel6.add((Component)this.label6, cc.xy(2, 1));
        this.anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label.setText(bundle.getString("Quadro04Panel.anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label.text"));
        this.anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label.setBorder(new EtchedBorder());
        this.anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C401_label\u00a3anexoBq04C402_label\u00a3anexoBq04C403_label\u00a3anexoBq04C404_label\u00a3anexoBq04C405_label\u00a3anexoBq04C420_label\u00a3anexoBq04C421_label\u00a3anexoBq04C422_label\u00a3anexoBq04C1_label, cc.xywh(4, 1, 3, 1));
        this.anexoBq04C401_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C401_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C401_base_label, cc.xy(2, 3));
        this.anexoBq04C401_num.setText(bundle.getString("Quadro04Panel.anexoBq04C401_num.text"));
        this.anexoBq04C401_num.setBorder(new EtchedBorder());
        this.anexoBq04C401_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C401_num, cc.xy(4, 3));
        this.panel6.add((Component)this.anexoBq04C401, cc.xy(6, 3));
        this.anexoBq04C402_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C402_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C402_base_label, cc.xy(2, 5));
        this.anexoBq04C402_num.setText(bundle.getString("Quadro04Panel.anexoBq04C402_num.text"));
        this.anexoBq04C402_num.setBorder(new EtchedBorder());
        this.anexoBq04C402_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C402_num, cc.xy(4, 5));
        this.panel6.add((Component)this.anexoBq04C402, cc.xy(6, 5));
        this.anexoBq04C403_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C403_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C403_base_label, cc.xy(2, 7));
        this.anexoBq04C403_num.setText(bundle.getString("Quadro04Panel.anexoBq04C403_num.text"));
        this.anexoBq04C403_num.setBorder(new EtchedBorder());
        this.anexoBq04C403_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C403_num, cc.xy(4, 7));
        this.panel6.add((Component)this.anexoBq04C403, cc.xy(6, 7));
        this.anexoBq04C440_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C440_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C440_base_label, cc.xy(2, 9));
        this.anexoBq04C440_num.setText(bundle.getString("Quadro04Panel.anexoBq04C440_num.text"));
        this.anexoBq04C440_num.setBorder(new EtchedBorder());
        this.anexoBq04C440_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C440_num, cc.xy(4, 9));
        this.panel6.add((Component)this.anexoBq04C440, cc.xy(6, 9));
        this.anexoBq04C441_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C441_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C441_base_label, cc.xy(2, 11));
        this.anexoBq04C441_num.setText(bundle.getString("Quadro04Panel.anexoBq04C441_num.text"));
        this.anexoBq04C441_num.setBorder(new EtchedBorder());
        this.anexoBq04C441_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C441_num, cc.xy(4, 11));
        this.panel6.add((Component)this.anexoBq04C441, cc.xy(6, 11));
        this.anexoBq04C404_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C404_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C404_base_label, cc.xy(2, 13));
        this.anexoBq04C404_num.setText(bundle.getString("Quadro04Panel.anexoBq04C404_num.text"));
        this.anexoBq04C404_num.setBorder(new EtchedBorder());
        this.anexoBq04C404_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C404_num, cc.xy(4, 13));
        this.panel6.add((Component)this.anexoBq04C404, cc.xy(6, 13));
        this.anexoBq04C442_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C442_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C442_base_label, cc.xy(2, 15));
        this.anexoBq04C442_num.setText(bundle.getString("Quadro04Panel.anexoBq04C442_num.text"));
        this.anexoBq04C442_num.setBorder(new EtchedBorder());
        this.anexoBq04C442_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C442_num, cc.xy(4, 15));
        this.panel6.add((Component)this.anexoBq04C442, cc.xy(6, 15));
        this.anexoBq04C405_base_label.setToolTipText(bundle.getString("Quadro04Panel.anexoBq04C405_base_label.toolTipText"));
        this.anexoBq04C405_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C405_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C405_base_label, cc.xy(2, 17));
        this.anexoBq04C405_num.setText(bundle.getString("Quadro04Panel.anexoBq04C405_num.text"));
        this.anexoBq04C405_num.setBorder(new EtchedBorder());
        this.anexoBq04C405_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C405_num, cc.xy(4, 17));
        this.panel6.add((Component)this.anexoBq04C405, cc.xy(6, 17));
        this.anexoBq04C420_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C420_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C420_base_label, cc.xy(2, 19));
        this.anexoBq04C420_num.setText(bundle.getString("Quadro04Panel.anexoBq04C420_num.text"));
        this.anexoBq04C420_num.setBorder(new EtchedBorder());
        this.anexoBq04C420_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C420_num, cc.xy(4, 19));
        this.panel6.add((Component)this.anexoBq04C420, cc.xy(6, 19));
        this.anexoBq04C421_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C421_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C421_base_label, cc.xy(2, 21));
        this.anexoBq04C421_num.setText(bundle.getString("Quadro04Panel.anexoBq04C421_num.text"));
        this.anexoBq04C421_num.setBorder(new EtchedBorder());
        this.anexoBq04C421_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C421_num, cc.xy(4, 21));
        this.panel6.add((Component)this.anexoBq04C421, cc.xy(6, 21));
        this.anexoBq04C422_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C422_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C422_base_label, cc.xy(2, 23));
        this.anexoBq04C422_num.setText(bundle.getString("Quadro04Panel.anexoBq04C422_num.text"));
        this.anexoBq04C422_num.setBorder(new EtchedBorder());
        this.anexoBq04C422_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C422_num, cc.xy(4, 23));
        this.panel6.add((Component)this.anexoBq04C422, cc.xy(6, 23));
        this.anexoBq04C423_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C423_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C423_base_label, cc.xy(2, 25));
        this.anexoBq04C423_num.setText(bundle.getString("Quadro04Panel.anexoBq04C423_num.text"));
        this.anexoBq04C423_num.setHorizontalAlignment(0);
        this.anexoBq04C423_num.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.anexoBq04C423_num, cc.xy(4, 25));
        this.panel6.add((Component)this.anexoBq04C423, cc.xy(6, 25));
        this.anexoBq04C424_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C424_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C424_base_label, cc.xy(2, 27));
        this.anexoBq04C424_num.setText(bundle.getString("Quadro04Panel.anexoBq04C424_num.text"));
        this.anexoBq04C424_num.setHorizontalAlignment(0);
        this.anexoBq04C424_num.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.anexoBq04C424_num, cc.xy(4, 27));
        this.panel6.add((Component)this.anexoBq04C424, cc.xy(6, 27));
        this.anexoBq04C425_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C425_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C425_base_label, cc.xy(2, 29));
        this.anexoBq04C425_num.setText(bundle.getString("Quadro04Panel.anexoBq04C425_num.text"));
        this.anexoBq04C425_num.setHorizontalAlignment(0);
        this.anexoBq04C425_num.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.anexoBq04C425_num, cc.xy(4, 29));
        this.panel6.add((Component)this.anexoBq04C425, cc.xy(6, 29));
        this.anexoBq04C443_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C443_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C443_base_label, cc.xy(2, 31));
        this.anexoBq04C443_num.setText(bundle.getString("Quadro04Panel.anexoBq04C443_num.text"));
        this.anexoBq04C443_num.setHorizontalAlignment(0);
        this.anexoBq04C443_num.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.anexoBq04C443_num, cc.xy(4, 31));
        this.panel6.add((Component)this.anexoBq04C443, cc.xy(6, 31));
        this.anexoBq04C1_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C1_base_label.text"));
        this.anexoBq04C1_base_label.setHorizontalAlignment(4);
        this.panel6.add((Component)this.anexoBq04C1_base_label, cc.xy(4, 33));
        this.anexoBq04C1.setEditable(false);
        this.anexoBq04C1.setColumns(15);
        this.panel6.add((Component)this.anexoBq04C1, cc.xy(6, 33));
        this.label7.setBorder(new EtchedBorder());
        this.label7.setText(bundle.getString("Quadro04Panel.label7.text"));
        this.label7.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label7, cc.xy(2, 37));
        this.anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label.setText(bundle.getString("Quadro04Panel.anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label.text"));
        this.anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label.setBorder(new EtchedBorder());
        this.anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C406_label\u00a3anexoBq04C407_labell\u00a3anexoBq04C2_label, cc.xywh(4, 37, 3, 1));
        this.anexoBq04C406_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C406_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C406_base_label, cc.xy(2, 39));
        this.anexoBq04C406_num.setText(bundle.getString("Quadro04Panel.anexoBq04C406_num.text"));
        this.anexoBq04C406_num.setBorder(new EtchedBorder());
        this.anexoBq04C406_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C406_num, cc.xy(4, 39));
        this.panel6.add((Component)this.anexoBq04C406, cc.xy(6, 39));
        this.anexoBq04C407_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C407_base_label.text"));
        this.panel6.add((Component)this.anexoBq04C407_base_label, cc.xy(2, 41));
        this.anexoBq04C407_num.setText(bundle.getString("Quadro04Panel.anexoBq04C407_num.text"));
        this.anexoBq04C407_num.setBorder(new EtchedBorder());
        this.anexoBq04C407_num.setHorizontalAlignment(0);
        this.panel6.add((Component)this.anexoBq04C407_num, cc.xy(4, 41));
        this.panel6.add((Component)this.anexoBq04C407, cc.xy(6, 41));
        this.anexoBq04C2_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C2_base_label.text"));
        this.anexoBq04C2_base_label.setHorizontalAlignment(4);
        this.panel6.add((Component)this.anexoBq04C2_base_label, cc.xy(4, 43));
        this.anexoBq04C2.setEditable(false);
        this.anexoBq04C2.setColumns(15);
        this.panel6.add((Component)this.anexoBq04C2, cc.xy(6, 43));
        this.this2.add((Component)this.panel6, cc.xy(2, 3));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:default, $rgap, default, $lcgap, 13dlu, 0px, default:grow, $rgap", "default, $lgap, default"));
        this.label8.setText(bundle.getString("Quadro04Panel.label8.text"));
        this.label8.setBorder(new EtchedBorder());
        this.label8.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label8, cc.xy(1, 1));
        this.label9.setText(bundle.getString("Quadro04Panel.label9.text"));
        this.label9.setHorizontalAlignment(0);
        this.label9.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label9, cc.xywh(3, 1, 13, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 5));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("$ugap, default, $ugap, default:grow, $lcgap, 25dlu, $rgap, 75dlu, $lcgap, $rgap", "14*(default, $lgap), default"));
        this.label18.setBorder(new EtchedBorder());
        this.label18.setText(bundle.getString("Quadro04Panel.label18.text"));
        this.panel7.add((Component)this.label18, cc.xywh(2, 1, 3, 1));
        this.anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label.setText(bundle.getString("Quadro04Panel.anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label.text"));
        this.anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label.setBorder(new EtchedBorder());
        this.anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C409_label\u00a3anexoBq04C410_label\u00a3anexoBq04C411_labell\u00a3anexoBq04C3_label, cc.xywh(6, 1, 3, 1));
        this.anexoBq04C409_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C409_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C409_base_label, cc.xy(2, 3));
        this.anexoBq04C409_num.setText(bundle.getString("Quadro04Panel.anexoBq04C409_num.text"));
        this.anexoBq04C409_num.setBorder(new EtchedBorder());
        this.anexoBq04C409_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C409_num, cc.xy(6, 3));
        this.panel7.add((Component)this.anexoBq04C409, cc.xy(8, 3));
        this.anexoBq04C410_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C410_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C410_base_label, cc.xy(2, 5));
        this.anexoBq04C410_num.setText(bundle.getString("Quadro04Panel.anexoBq04C410_num.text"));
        this.anexoBq04C410_num.setBorder(new EtchedBorder());
        this.anexoBq04C410_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C410_num, cc.xy(6, 5));
        this.panel7.add((Component)this.anexoBq04C410, cc.xy(8, 5));
        this.anexoBq04C444_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C444_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C444_base_label, cc.xy(2, 7));
        this.anexoBq04C444_num.setText(bundle.getString("Quadro04Panel.anexoBq04C444_num.text"));
        this.anexoBq04C444_num.setBorder(new EtchedBorder());
        this.anexoBq04C444_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C444_num, cc.xy(6, 7));
        this.panel7.add((Component)this.anexoBq04C444, cc.xy(8, 7));
        this.anexoBq04C445_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C445_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C445_base_label, cc.xywh(2, 9, 3, 1));
        this.anexoBq04C445_num.setText(bundle.getString("Quadro04Panel.anexoBq04C445_num.text"));
        this.anexoBq04C445_num.setBorder(new EtchedBorder());
        this.anexoBq04C445_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C445_num, cc.xy(6, 9));
        this.panel7.add((Component)this.anexoBq04C445, cc.xy(8, 9));
        this.anexoBq04C411_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C411_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C411_base_label, cc.xy(2, 11));
        this.anexoBq04C411_num.setText(bundle.getString("Quadro04Panel.anexoBq04C411_num.text"));
        this.anexoBq04C411_num.setBorder(new EtchedBorder());
        this.anexoBq04C411_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C411_num, cc.xy(6, 11));
        this.panel7.add((Component)this.anexoBq04C411, cc.xy(8, 11));
        this.anexoBq04C426_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C426_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C426_base_label, cc.xy(2, 13));
        this.anexoBq04C426_num.setText(bundle.getString("Quadro04Panel.anexoBq04C426_num.text"));
        this.anexoBq04C426_num.setBorder(new EtchedBorder());
        this.anexoBq04C426_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C426_num, cc.xy(6, 13));
        this.panel7.add((Component)this.anexoBq04C426, cc.xy(8, 13));
        this.anexoBq04C446_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C446_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C446_base_label, cc.xy(2, 15));
        this.anexoBq04C446_num.setText(bundle.getString("Quadro04Panel.anexoBq04C446_num.text"));
        this.anexoBq04C446_num.setBorder(new EtchedBorder());
        this.anexoBq04C446_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C446_num, cc.xy(6, 15));
        this.panel7.add((Component)this.anexoBq04C446, cc.xy(8, 15));
        this.anexoBq04C3_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C3_base_label.text"));
        this.anexoBq04C3_base_label.setHorizontalAlignment(4);
        this.panel7.add((Component)this.anexoBq04C3_base_label, cc.xy(6, 17));
        this.anexoBq04C3.setEditable(false);
        this.anexoBq04C3.setColumns(15);
        this.panel7.add((Component)this.anexoBq04C3, cc.xy(8, 17));
        this.label21.setBorder(new EtchedBorder());
        this.label21.setText(bundle.getString("Quadro04Panel.label21.text"));
        this.label21.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label21, cc.xywh(2, 21, 3, 1));
        this.anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label.setText(bundle.getString("Quadro04Panel.anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label.text"));
        this.anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label.setBorder(new EtchedBorder());
        this.anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C413_label\u00a3anexoBq04C414_labell\u00a3anexoBq04C4_label, cc.xywh(6, 21, 3, 1));
        this.anexoBq04C413_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C413_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C413_base_label, cc.xy(2, 23));
        this.anexoBq04C413_num.setText(bundle.getString("Quadro04Panel.anexoBq04C413_num.text"));
        this.anexoBq04C413_num.setBorder(new EtchedBorder());
        this.anexoBq04C413_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C413_num, cc.xy(6, 23));
        this.panel7.add((Component)this.anexoBq04C413, cc.xy(8, 23));
        this.anexoBq04C414_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C414_base_label.text"));
        this.panel7.add((Component)this.anexoBq04C414_base_label, cc.xy(2, 25));
        this.anexoBq04C414_num.setText(bundle.getString("Quadro04Panel.anexoBq04C414_num.text"));
        this.anexoBq04C414_num.setBorder(new EtchedBorder());
        this.anexoBq04C414_num.setHorizontalAlignment(0);
        this.panel7.add((Component)this.anexoBq04C414_num, cc.xy(6, 25));
        this.panel7.add((Component)this.anexoBq04C414, cc.xy(8, 25));
        this.anexoBq04C4_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04C4_base_label.text"));
        this.anexoBq04C4_base_label.setHorizontalAlignment(4);
        this.panel7.add((Component)this.anexoBq04C4_base_label, cc.xy(6, 27));
        this.anexoBq04C4.setEditable(false);
        this.anexoBq04C4.setColumns(15);
        this.panel7.add((Component)this.anexoBq04C4, cc.xy(8, 27));
        this.this2.add((Component)this.panel7, cc.xy(2, 7));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:default, $rgap, default, $lcgap, 13dlu, 0px, left:default, $rgap, default:grow, $rgap", "4*(default, $lgap), default"));
        this.label20.setText(bundle.getString("Quadro04Panel.label20.text"));
        this.label20.setBorder(new EtchedBorder());
        this.label20.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label20, cc.xy(1, 1));
        this.label22.setText(bundle.getString("Quadro04Panel.label22.text"));
        this.label22.setHorizontalAlignment(0);
        this.label22.setBorder(new EtchedBorder());
        this.panel5.add((Component)this.label22, cc.xywh(3, 1, 15, 1));
        this.anexoBq04B1OPSim_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04B1OPSim_base_label.text"));
        this.panel5.add((Component)this.anexoBq04B1OPSim_base_label, cc.xy(5, 5));
        this.label50.setText(bundle.getString("Quadro04Panel.label50.text"));
        this.label50.setBorder(new EtchedBorder());
        this.label50.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label50, cc.xy(7, 5));
        this.anexoBq04B1OP1.setText(bundle.getString("Quadro04Panel.anexoBq04B1OP1.text"));
        this.panel5.add((Component)this.anexoBq04B1OP1, cc.xy(9, 5));
        this.label51.setText(bundle.getString("Quadro04Panel.label51.text"));
        this.label51.setBorder(new EtchedBorder());
        this.label51.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label51, cc.xy(13, 5));
        this.anexoBq04B1OP2.setText(bundle.getString("Quadro04Panel.anexoBq04B1OP2.text"));
        this.panel5.add((Component)this.anexoBq04B1OP2, cc.xy(15, 5));
        this.anexoBq04B2OPSim_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04B2OPSim_base_label.text"));
        this.panel5.add((Component)this.anexoBq04B2OPSim_base_label, cc.xy(5, 7));
        this.label53.setText(bundle.getString("Quadro04Panel.label53.text"));
        this.label53.setBorder(new EtchedBorder());
        this.label53.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label53, cc.xy(7, 7));
        this.anexoBq04B2OP3.setText(bundle.getString("Quadro04Panel.anexoBq04B2OP3.text"));
        this.panel5.add((Component)this.anexoBq04B2OP3, cc.xy(9, 7));
        this.label54.setText(bundle.getString("Quadro04Panel.label54.text"));
        this.label54.setBorder(new EtchedBorder());
        this.label54.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label54, cc.xy(13, 7));
        this.anexoBq04B2OP4.setText(bundle.getString("Quadro04Panel.anexoBq04B2OP4.text"));
        this.panel5.add((Component)this.anexoBq04B2OP4, cc.xy(15, 7));
        this.this2.add((Component)this.panel5, cc.xy(2, 9));
        this.panel8.setMinimumSize(null);
        this.panel8.setPreferredSize(null);
        this.panel8.setLayout(new FormLayout("15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:default, $rgap, default, $lcgap, 13dlu, 0px, left:default, $lcgap, default:grow, $rgap", "3*(default, $lgap), default"));
        this.label55.setText(bundle.getString("Quadro04Panel.label55.text"));
        this.label55.setBorder(new EtchedBorder());
        this.label55.setHorizontalAlignment(0);
        this.panel8.add((Component)this.label55, cc.xy(1, 1));
        this.label56.setText(bundle.getString("Quadro04Panel.label56.text"));
        this.label56.setHorizontalAlignment(0);
        this.label56.setBorder(new EtchedBorder());
        this.panel8.add((Component)this.label56, cc.xywh(3, 1, 15, 1));
        this.anexoBq04B3OPSim_base_label.setText(bundle.getString("Quadro04Panel.anexoBq04B3OPSim_base_label.text"));
        this.panel8.add((Component)this.anexoBq04B3OPSim_base_label, cc.xy(5, 5));
        this.label59.setText(bundle.getString("Quadro04Panel.label59.text"));
        this.label59.setBorder(new EtchedBorder());
        this.label59.setHorizontalAlignment(0);
        this.panel8.add((Component)this.label59, cc.xy(7, 5));
        this.anexoBq04B3OP1.setText(bundle.getString("Quadro04Panel.anexoBq04B3OP1.text"));
        this.panel8.add((Component)this.anexoBq04B3OP1, cc.xy(9, 5));
        this.label68.setText(bundle.getString("Quadro04Panel.label68.text"));
        this.label68.setBorder(new EtchedBorder());
        this.label68.setHorizontalAlignment(0);
        this.panel8.add((Component)this.label68, cc.xy(13, 5));
        this.anexoBq04B3OP2.setText(bundle.getString("Quadro04Panel.anexoBq04B3OP2.text"));
        this.panel8.add((Component)this.anexoBq04B3OP2, cc.xy(15, 5));
        this.label69.setText(bundle.getString("Quadro04Panel.label69.text"));
        this.label69.setHorizontalAlignment(0);
        this.panel8.add((Component)this.label69, cc.xy(3, 7));
        this.this2.add((Component)this.panel8, cc.xy(2, 13));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("$ugap, 2*(default, $lcgap), default:grow, $lcgap, $rgap", "2*(default, $lgap), fill:165dlu, $lgap, default"));
        this.label10.setText(bundle.getString("Quadro04Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label10, cc.xywh(2, 1, 5, 1));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoBq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoBq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel4.add((Component)this.toolBar1, cc.xy(2, 3));
        this.anexoBq04T1Scroll.setViewportView(this.anexoBq04T1);
        this.panel4.add((Component)this.anexoBq04T1Scroll, cc.xywh(2, 5, 5, 1));
        this.this2.add((Component)this.panel4, cc.xy(2, 17));
        this.panel9.setMinimumSize(null);
        this.panel9.setPreferredSize(null);
        this.panel9.setLayout(new FormLayout("15dlu, default:grow, $lcgap, 219dlu, $lcgap, default:grow, $lcgap, $rgap", "2*(default, $lgap), fill:165dlu, $lgap, default"));
        this.panel9.setVisible(Modelo3IRSv2015Parameters.instance().isDC());
        this.label57.setText(bundle.getString("Quadro04Panel.label57.text"));
        this.label57.setBorder(new EtchedBorder());
        this.label57.setHorizontalAlignment(0);
        this.panel9.add((Component)this.label57, cc.xy(1, 1));
        this.label11.setText(bundle.getString("Quadro04Panel.label11.text"));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.panel9.add((Component)this.label11, cc.xywh(2, 1, 5, 1));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button7.setText(bundle.getString("Quadro04Panel.button7.text"));
        this.button7.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoBq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button7);
        this.button8.setText(bundle.getString("Quadro04Panel.button8.text"));
        this.button8.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoBq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button8);
        this.panel9.add((Component)this.toolBar2, cc.xy(4, 3));
        this.anexoBq04T2Scroll.setVisible(Modelo3IRSv2015Parameters.instance().isDC());
        this.anexoBq04T2.setEnabled(false);
        this.anexoBq04T2Scroll.setViewportView(this.anexoBq04T2);
        this.panel9.add((Component)this.anexoBq04T2Scroll, cc.xy(4, 5));
        this.this2.add((Component)this.panel9, cc.xy(2, 21));
        this.panel10.setMinimumSize(null);
        this.panel10.setPreferredSize(null);
        this.panel10.setLayout(new FormLayout("15dlu, 21dlu:grow, $lcgap, 540dlu, $lcgap, default:grow, $lcgap, $rgap", "3*(default, $lgap), fill:165dlu, $lgap, default"));
        this.label58.setText(bundle.getString("Quadro04Panel.label58.text"));
        this.label58.setBorder(new EtchedBorder());
        this.label58.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label58, cc.xy(1, 1));
        this.label12.setText(bundle.getString("Quadro04Panel.label12.text"));
        this.label12.setBorder(new EtchedBorder());
        this.label12.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label12, cc.xywh(2, 1, 5, 1));
        this.toolBar3.setFloatable(false);
        this.toolBar3.setRollover(true);
        this.button5.setText(bundle.getString("Quadro04Panel.button5.text"));
        this.button5.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoBq04T3_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button5);
        this.button6.setText(bundle.getString("Quadro04Panel.button6.text"));
        this.button6.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoBq04T3_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button6);
        this.panel10.add((Component)this.toolBar3, cc.xy(4, 3));
        this.anexoBq04T3Scroll.setViewportView(this.anexoBq04T3);
        this.panel10.add((Component)this.anexoBq04T3Scroll, cc.xywh(4, 5, 1, 3));
        this.this2.add((Component)this.panel10, cc.xywh(2, 19, 2, 1));
        this.add((Component)this.this2, "Center");
    }

}

