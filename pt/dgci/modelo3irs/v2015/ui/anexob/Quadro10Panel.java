/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro10Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoBq10C1001_label;
    protected JLabelTextFieldNumbering anexoBq10C1001_num;
    protected JMoneyTextField anexoBq10C1001;
    protected JLabel anexoBq10C1002_label;
    protected JLabelTextFieldNumbering anexoBq10C1002_num;
    protected JMoneyTextField anexoBq10C1002;
    protected JLabel anexoBq10C1_label;
    protected JMoneyTextField anexoBq10C1;

    public Quadro10Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoBq10C1001_label() {
        return this.anexoBq10C1001_label;
    }

    public JLabelTextFieldNumbering getAnexoBq10C1001_num() {
        return this.anexoBq10C1001_num;
    }

    public JMoneyTextField getAnexoBq10C1001() {
        return this.anexoBq10C1001;
    }

    public JLabel getAnexoBq10C1002_label() {
        return this.anexoBq10C1002_label;
    }

    public JLabelTextFieldNumbering getAnexoBq10C1002_num() {
        return this.anexoBq10C1002_num;
    }

    public JMoneyTextField getAnexoBq10C1002() {
        return this.anexoBq10C1002;
    }

    public JLabel getAnexoBq10C1_label() {
        return this.anexoBq10C1_label;
    }

    public JMoneyTextField getAnexoBq10C1() {
        return this.anexoBq10C1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoBq10C1001_label = new JLabel();
        this.anexoBq10C1001_num = new JLabelTextFieldNumbering();
        this.anexoBq10C1001 = new JMoneyTextField();
        this.anexoBq10C1002_label = new JLabel();
        this.anexoBq10C1002_num = new JLabelTextFieldNumbering();
        this.anexoBq10C1002 = new JMoneyTextField();
        this.anexoBq10C1_label = new JLabel();
        this.anexoBq10C1 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro10Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro10Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $ugap, 25dlu, 0px, 75dlu, $rgap", "$rgap, 3*(default, $lgap), default"));
        this.anexoBq10C1001_label.setText(bundle.getString("Quadro10Panel.anexoBq10C1001_label.text"));
        this.panel2.add((Component)this.anexoBq10C1001_label, cc.xy(1, 2));
        this.anexoBq10C1001_num.setText(bundle.getString("Quadro10Panel.anexoBq10C1001_num.text"));
        this.anexoBq10C1001_num.setBorder(new EtchedBorder());
        this.anexoBq10C1001_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq10C1001_num, cc.xy(3, 2));
        this.panel2.add((Component)this.anexoBq10C1001, cc.xy(5, 2));
        this.anexoBq10C1002_label.setText(bundle.getString("Quadro10Panel.anexoBq10C1002_label.text"));
        this.panel2.add((Component)this.anexoBq10C1002_label, cc.xy(1, 4));
        this.anexoBq10C1002_num.setText(bundle.getString("Quadro10Panel.anexoBq10C1002_num.text"));
        this.anexoBq10C1002_num.setBorder(new EtchedBorder());
        this.anexoBq10C1002_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq10C1002_num, cc.xy(3, 4));
        this.panel2.add((Component)this.anexoBq10C1002, cc.xy(5, 4));
        this.anexoBq10C1_label.setText(bundle.getString("Quadro10Panel.anexoBq10C1_label.text"));
        this.anexoBq10C1_label.setHorizontalAlignment(0);
        this.panel2.add((Component)this.anexoBq10C1_label, cc.xywh(2, 6, 2, 1));
        this.anexoBq10C1.setColumns(13);
        this.anexoBq10C1.setEditable(false);
        this.panel2.add((Component)this.anexoBq10C1, cc.xy(5, 6));
        this.this2.add((Component)this.panel2, cc.xy(3, 1));
        this.add((Component)this.this2, "Center");
    }
}

