/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro04Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro04PanelBase
extends Quadro04Panel
implements IBindablePanel<Quadro04> {
    private static final long serialVersionUID = 1;
    protected Quadro04 model;

    @Override
    public abstract void setModel(Quadro04 var1, boolean var2);

    @Override
    public Quadro04 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoBq04T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoBq04T1().add(new AnexoBq04T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoBq04T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoBq04T1.getSelectedRow() != -1 ? this.anexoBq04T1.getSelectedRow() : this.anexoBq04T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoBq04T1().remove(selectedRow);
            }
        }
    }

    @Override
    protected void addLineAnexoBq04T3_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoBq04T3().add(new AnexoBq04T3_Linha());
        }
    }

    @Override
    protected void removeLineAnexoBq04T3_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoBq04T3.getSelectedRow() != -1 ? this.anexoBq04T3.getSelectedRow() : this.anexoBq04T3.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoBq04T3().remove(selectedRow);
            }
        }
    }

    public Quadro04PanelBase(Quadro04 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

