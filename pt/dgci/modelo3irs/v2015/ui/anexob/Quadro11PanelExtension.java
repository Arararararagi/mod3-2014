/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro11Bindings;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro11PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro11PanelExtension
extends Quadro11PanelBase
implements IBindablePanel<Quadro11> {
    public Quadro11PanelExtension(Quadro11 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro11 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro11Bindings.doBindings(model, this);
        }
    }
}

