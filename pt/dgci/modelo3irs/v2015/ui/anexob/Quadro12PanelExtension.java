/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro12Bindings;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro12PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro12PanelExtension
extends Quadro12PanelBase
implements IBindablePanel<Quadro12> {
    public Quadro12PanelExtension(Quadro12 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro12 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro12Bindings.doBindings(model, this);
        }
    }
}

