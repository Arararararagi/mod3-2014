/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro12Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoBq12B1OPSim_base_label;
    protected JLabelTextFieldNumbering label3;
    protected JRadioButton anexoBq12B1OP1;
    protected JLabelTextFieldNumbering label4;
    protected JRadioButton anexoBq12B1OP2;
    protected JLabel anexoBq12C3_label;
    protected JLabelTextFieldNumbering label6;
    protected JDateTextField anexoBq12C3;
    protected JLabel anexoBq12B4_label;
    protected JLabelTextFieldNumbering label8;
    protected JCheckBox anexoBq12B4;

    public Quadro12Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoBq12B1OPSim_base_label() {
        return this.anexoBq12B1OPSim_base_label;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JRadioButton getAnexoBq12B1OP1() {
        return this.anexoBq12B1OP1;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JRadioButton getAnexoBq12B1OP2() {
        return this.anexoBq12B1OP2;
    }

    public JLabel getAnexoBq12C3_label() {
        return this.anexoBq12C3_label;
    }

    public JLabelTextFieldNumbering getLabel6() {
        return this.label6;
    }

    public JDateTextField getAnexoBq12C3() {
        return this.anexoBq12C3;
    }

    public JLabel getAnexoBq12B4_label() {
        return this.anexoBq12B4_label;
    }

    public JLabelTextFieldNumbering getLabel8() {
        return this.label8;
    }

    public JCheckBox getAnexoBq12B4() {
        return this.anexoBq12B4;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoBq12B1OPSim_base_label = new JLabel();
        this.label3 = new JLabelTextFieldNumbering();
        this.anexoBq12B1OP1 = new JRadioButton();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoBq12B1OP2 = new JRadioButton();
        this.anexoBq12C3_label = new JLabel();
        this.label6 = new JLabelTextFieldNumbering();
        this.anexoBq12C3 = new JDateTextField();
        this.anexoBq12B4_label = new JLabel();
        this.label8 = new JLabelTextFieldNumbering();
        this.anexoBq12B4 = new JCheckBox();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro12Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro12Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("15dlu, 0px, default:grow, $rgap, default, $ugap, 13dlu, 0px, left:43dlu, $rgap, default, $ugap, 13dlu, 0px, left:default, $lcgap, default:grow", "$ugap, 7*($lgap, default)"));
        this.anexoBq12B1OPSim_base_label.setText(bundle.getString("Quadro12Panel.anexoBq12B1OPSim_base_label.text"));
        this.panel2.add((Component)this.anexoBq12B1OPSim_base_label, CC.xy(5, 5));
        this.label3.setText(bundle.getString("Quadro12Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label3, CC.xy(7, 5));
        this.anexoBq12B1OP1.setText(bundle.getString("Quadro12Panel.anexoBq12B1OP1.text"));
        this.panel2.add((Component)this.anexoBq12B1OP1, CC.xy(9, 5));
        this.label4.setText(bundle.getString("Quadro12Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(13, 5));
        this.anexoBq12B1OP2.setText(bundle.getString("Quadro12Panel.anexoBq12B1OP2.text"));
        this.panel2.add((Component)this.anexoBq12B1OP2, CC.xy(15, 5));
        this.anexoBq12C3_label.setText(bundle.getString("Quadro12Panel.anexoBq12C3_label.text"));
        this.panel2.add((Component)this.anexoBq12C3_label, CC.xy(5, 7));
        this.label6.setText(bundle.getString("Quadro12Panel.label6.text"));
        this.label6.setBorder(new EtchedBorder());
        this.label6.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label6, CC.xy(7, 7));
        this.anexoBq12C3.setColumns(10);
        this.panel2.add((Component)this.anexoBq12C3, CC.xy(9, 7));
        this.anexoBq12B4_label.setText(bundle.getString("Quadro12Panel.anexoBq12B4_label.text"));
        this.anexoBq12B4_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoBq12B4_label, CC.xywh(1, 13, 11, 1));
        this.label8.setText(bundle.getString("Quadro12Panel.label8.text"));
        this.label8.setBorder(new EtchedBorder());
        this.label8.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label8, CC.xy(13, 13));
        this.panel2.add((Component)this.anexoBq12B4, CC.xy(15, 13));
        this.this2.add((Component)this.panel2, CC.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

