/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel anexoBq06C601_label;
    protected JLabelTextFieldNumbering anexoBq06C601_num;
    protected JMoneyTextField anexoBq06C601;
    protected JLabel anexoBq06C602_label;
    protected JLabelTextFieldNumbering anexoBq06C602_num;
    protected JMoneyTextField anexoBq06C602;
    protected JLabel anexoBq06C1_label;
    protected JMoneyTextField anexoBq06C1;

    public Quadro06Panel() {
        this.initComponents();
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabelTextFieldNumbering getAnexoBq06C601_num() {
        return this.anexoBq06C601_num;
    }

    public JMoneyTextField getAnexoBq06C601() {
        return this.anexoBq06C601;
    }

    public JLabelTextFieldNumbering getAnexoBq06C602_num() {
        return this.anexoBq06C602_num;
    }

    public JMoneyTextField getAnexoBq06C602() {
        return this.anexoBq06C602;
    }

    public JMoneyTextField getAnexoBq06C1() {
        return this.anexoBq06C1;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoBq06C601_label() {
        return this.anexoBq06C601_label;
    }

    public JLabel getAnexoBq06C602_label() {
        return this.anexoBq06C602_label;
    }

    public JLabel getAnexoBq06C1_label() {
        return this.anexoBq06C1_label;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.anexoBq06C601_label = new JLabel();
        this.anexoBq06C601_num = new JLabelTextFieldNumbering();
        this.anexoBq06C601 = new JMoneyTextField();
        this.anexoBq06C602_label = new JLabel();
        this.anexoBq06C602_num = new JLabelTextFieldNumbering();
        this.anexoBq06C602 = new JMoneyTextField();
        this.anexoBq06C1_label = new JLabel();
        this.anexoBq06C1 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow", "default, $lgap, default"));
        this.panel4.setLayout(new FormLayout("default, $lcgap, default:grow, $lcgap, 25dlu, 0px, 75dlu, $rgap", "$rgap, 3*(default, $lgap), default"));
        this.anexoBq06C601_label.setText(bundle.getString("Quadro06Panel.anexoBq06C601_label.text"));
        this.panel4.add((Component)this.anexoBq06C601_label, cc.xy(1, 2));
        this.anexoBq06C601_num.setText(bundle.getString("Quadro06Panel.anexoBq06C601_num.text"));
        this.anexoBq06C601_num.setBorder(new EtchedBorder());
        this.anexoBq06C601_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq06C601_num, cc.xy(5, 2));
        this.panel4.add((Component)this.anexoBq06C601, cc.xy(7, 2));
        this.anexoBq06C602_label.setText(bundle.getString("Quadro06Panel.anexoBq06C602_label.text"));
        this.panel4.add((Component)this.anexoBq06C602_label, cc.xy(1, 4));
        this.anexoBq06C602_num.setText(bundle.getString("Quadro06Panel.anexoBq06C602_num.text"));
        this.anexoBq06C602_num.setBorder(new EtchedBorder());
        this.anexoBq06C602_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq06C602_num, cc.xy(5, 4));
        this.panel4.add((Component)this.anexoBq06C602, cc.xy(7, 4));
        this.anexoBq06C1_label.setText(bundle.getString("Quadro06Panel.anexoBq06C1_label.text"));
        this.anexoBq06C1_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoBq06C1_label, cc.xy(5, 6));
        this.anexoBq06C1.setColumns(15);
        this.anexoBq06C1.setEditable(false);
        this.panel4.add((Component)this.anexoBq06C1, cc.xy(7, 6));
        this.this2.add((Component)this.panel4, cc.xy(3, 1));
        this.add((Component)this.this2, "Center");
    }
}

