/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro07Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro07PanelBase
extends Quadro07Panel
implements IBindablePanel<Quadro07> {
    private static final long serialVersionUID = 1;
    protected Quadro07 model;

    @Override
    public abstract void setModel(Quadro07 var1, boolean var2);

    @Override
    public Quadro07 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoBq07T1().add(new AnexoBq07T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoBq07T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoBq07T1.getSelectedRow() != -1 ? this.anexoBq07T1.getSelectedRow() : this.anexoBq07T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoBq07T1().remove(selectedRow);
            }
        }
    }

    public Quadro07PanelBase(Quadro07 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

