/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;

public class Quadro00Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JLabel label4;
    protected JLabel label5;
    protected JLabel label6;

    public Quadro00Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    public JLabel getLabel5() {
        return this.label5;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.label4 = new JLabel();
        this.label5 = new JLabel();
        this.label6 = new JLabel();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro00Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro00Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$lcgap, 20dlu, $rgap, 300dlu:grow, 0px, $lcgap, 20dlu", "$ugap, 2*($lgap, default), $lgap, 10dlu, $lgap, default, $lgap, 10dlu, 2*($lgap, default)"));
        this.label4.setText(bundle.getString("Quadro00Panel.label4.text"));
        this.this2.add((Component)this.label4, cc.xy(4, 5));
        this.label5.setText(bundle.getString("Quadro00Panel.label5.text"));
        this.this2.add((Component)this.label5, cc.xy(4, 9));
        this.label6.setText(bundle.getString("Quadro00Panel.label6.text"));
        this.this2.add((Component)this.label6, cc.xy(4, 13));
        this.add((Component)this.this2, "Center");
    }
}

