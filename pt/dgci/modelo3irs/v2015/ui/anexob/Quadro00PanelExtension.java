/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro00Bindings;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro00;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro00PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro00PanelExtension
extends Quadro00PanelBase
implements IBindablePanel<Quadro00> {
    public Quadro00PanelExtension(Quadro00 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro00 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro00Bindings.doBindings(model, this);
        }
    }
}

