/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro08Bindings;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro08PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro08PanelExtension
extends Quadro08PanelBase
implements IBindablePanel<Quadro08> {
    public Quadro08PanelExtension(Quadro08 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro08 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro08Bindings.doBindings(model, this);
        }
    }
}

