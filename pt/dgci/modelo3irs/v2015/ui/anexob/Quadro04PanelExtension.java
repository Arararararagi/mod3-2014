/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro04PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04> {
    public static final int MAX_LINES_Q04_T1 = 200;
    public static final int MAX_LINES_Q04_T3 = 100;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
        }
        if (model != null) {
            this.setHeader();
            this.setColumnSizes();
        }
    }

    @Override
    protected void addLineAnexoBq04T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoBq04T1() != null && this.model.getAnexoBq04T1().size() >= 200) {
            return;
        }
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            AnexoBq04T1_Linha linha = new AnexoBq04T1_Linha();
            linha.setArt139CIRC("N");
            this.model.getAnexoBq04T1().add(linha);
        }
    }

    @Override
    protected void addLineAnexoBq04T3_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoBq04T3() != null && this.model.getAnexoBq04T3().size() >= 100) {
            return;
        }
        super.addLineAnexoBq04T3_LinhaActionPerformed(e);
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoBq04T3().getTableHeader().getColumnModel();
        ColumnGroup columnGroup = new ColumnGroup("Subs\u00eddios n\u00e3o destinados \u00e0 explora\u00e7\u00e3o");
        columnGroup.add(cm.getColumn(3));
        columnGroup.add(cm.getColumn(4));
        columnGroup.add(cm.getColumn(5));
        columnGroup.add(cm.getColumn(6));
        columnGroup.add(cm.getColumn(7));
        if (this.getAnexoBq04T3().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoBq04T3().getTableHeader()).addColumnGroup(columnGroup);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(columnGroup);
        this.getAnexoBq04T3().setTableHeader(header);
    }

    private void setColumnSizes() {
        if (this.getAnexoBq04T3().getColumnCount() >= 4) {
            this.getAnexoBq04T3().getColumnModel().getColumn(2).setMinWidth(130);
            this.getAnexoBq04T3().getColumnModel().getColumn(2).setMaxWidth(205);
            this.getAnexoBq04T3().getColumnModel().getColumn(1).setMinWidth(110);
            this.getAnexoBq04T3().getColumnModel().getColumn(1).setMaxWidth(140);
            this.getAnexoBq04T3().getColumnModel().getColumn(3).setMinWidth(90);
            this.getAnexoBq04T3().getColumnModel().getColumn(3).setMaxWidth(110);
            this.getAnexoBq04T3().getColumnModel().getColumn(4).setMinWidth(90);
            this.getAnexoBq04T3().getColumnModel().getColumn(4).setMaxWidth(110);
            this.getAnexoBq04T3().getColumnModel().getColumn(5).setMinWidth(90);
            this.getAnexoBq04T3().getColumnModel().getColumn(5).setMaxWidth(110);
            this.getAnexoBq04T3().getColumnModel().getColumn(6).setMinWidth(90);
            this.getAnexoBq04T3().getColumnModel().getColumn(6).setMaxWidth(110);
            this.getAnexoBq04T3().getColumnModel().getColumn(7).setMinWidth(90);
            this.getAnexoBq04T3().getColumnModel().getColumn(7).setMaxWidth(110);
        }
    }
}

