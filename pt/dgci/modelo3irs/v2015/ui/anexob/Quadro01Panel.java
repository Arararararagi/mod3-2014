/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexob;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro01Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabelTextFieldNumbering label2;
    protected JRadioButton anexoBq01B1OP1;
    protected JLabelTextFieldNumbering label3;
    protected JRadioButton anexoBq01B1OP2;
    protected JLabelTextFieldNumbering label4;
    protected JCheckBox anexoBq01B3;
    protected JLabelTextFieldNumbering label5;
    protected JCheckBox anexoBq01B4;

    public Quadro01Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getLabel2() {
        return this.label2;
    }

    public JRadioButton getAnexoBq01B1OP1() {
        return this.anexoBq01B1OP1;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JRadioButton getAnexoBq01B1OP2() {
        return this.anexoBq01B1OP2;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JCheckBox getAnexoBq01B3() {
        return this.anexoBq01B3;
    }

    public JLabelTextFieldNumbering getLabel5() {
        return this.label5;
    }

    public JCheckBox getAnexoBq01B4() {
        return this.anexoBq01B4;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoB");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label2 = new JLabelTextFieldNumbering();
        this.anexoBq01B1OP1 = new JRadioButton();
        this.label3 = new JLabelTextFieldNumbering();
        this.anexoBq01B1OP2 = new JRadioButton();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoBq01B3 = new JCheckBox();
        this.label5 = new JLabelTextFieldNumbering();
        this.anexoBq01B4 = new JCheckBox();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro01Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro01Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow, 0px", "$ugap, 2*($lgap, default)"));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, 13dlu, 0px, default, $lcgap, default:grow", "6*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro01Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label2, cc.xy(3, 1));
        this.anexoBq01B1OP1.setText(bundle.getString("Quadro01Panel.anexoBq01B1OP1.text"));
        this.panel2.add((Component)this.anexoBq01B1OP1, cc.xy(5, 1));
        this.label3.setText(bundle.getString("Quadro01Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label3, cc.xy(3, 3));
        this.anexoBq01B1OP2.setText(bundle.getString("Quadro01Panel.anexoBq01B1OP2.text"));
        this.panel2.add((Component)this.anexoBq01B1OP2, cc.xy(5, 3));
        this.label4.setText(bundle.getString("Quadro01Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label4, cc.xy(3, 9));
        this.anexoBq01B3.setText(bundle.getString("Quadro01Panel.anexoBq01B3.text"));
        this.panel2.add((Component)this.anexoBq01B3, cc.xy(5, 9));
        this.label5.setText(bundle.getString("Quadro01Panel.label5.text"));
        this.label5.setHorizontalAlignment(0);
        this.label5.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label5, cc.xy(3, 11));
        this.anexoBq01B4.setText(bundle.getString("Quadro01Panel.anexoBq01B4.text"));
        this.panel2.add((Component)this.anexoBq01B4, cc.xy(5, 11));
        this.this2.add((Component)this.panel2, cc.xy(3, 3));
        this.add((Component)this.this2, "Center");
    }
}

