/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIBTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro07Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label2;
    protected JLabel label3;
    protected JLabel label1;
    protected JLabel label5;
    protected JLabel label6;
    protected JLabel q07C1_label;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField q07C1;
    protected JPercentTextField q07C1a;
    protected JCheckBox q07C1b;
    protected JPanel panel3;
    protected JLabel label8;
    protected JLabel label9;
    protected JPanel panel4;
    protected JLabel q07C01_base_label;
    protected JLabel q07C01a_base_label;
    protected JLabel q07C02_base_label;
    protected JLabel q07C02a_base_label;
    protected JLabelTextFieldNumbering label14;
    protected JNIFTextField q07C01;
    protected JPercentTextField q07C01a;
    protected JLabelTextFieldNumbering label15;
    protected JNIFTextField q07C02;
    protected JPercentTextField q07C02a;
    protected JLabelTextFieldNumbering label16;
    protected JNIFTextField q07C03;
    protected JPercentTextField q07C03a;
    protected JLabelTextFieldNumbering label17;
    protected JNIFTextField q07C04;
    protected JPercentTextField q07C04a;
    protected JPanel panel6;
    protected JLabel label20;
    protected JLabel label21;
    protected JPanel panel12;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane rostoq7ET1Scroll2;
    protected JTable rostoq07ET1;
    protected JPanel panel9;
    protected JLabel label10;
    protected JLabel label11;
    protected JPanel panel11;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane rostoq7CT1Scroll;
    protected JTable rostoq07CT1;
    protected JPanel panel5;
    protected JLabel label18;
    protected JLabel label19;
    protected JLabel q07D01_label;
    protected JNIBTextField q07D01;
    protected JCheckBox q07D01a;

    public Quadro07Panel() {
        this.initComponents();
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JLabel getLabel9() {
        return this.label9;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabelTextFieldNumbering getLabel14() {
        return this.label14;
    }

    public JNIFTextField getQ07C01() {
        return this.q07C01;
    }

    public JLabelTextFieldNumbering getLabel15() {
        return this.label15;
    }

    public JNIFTextField getQ07C02() {
        return this.q07C02;
    }

    public JLabelTextFieldNumbering getLabel16() {
        return this.label16;
    }

    public JNIFTextField getQ07C03() {
        return this.q07C03;
    }

    public JLabelTextFieldNumbering getLabel17() {
        return this.label17;
    }

    public JNIFTextField getQ07C04() {
        return this.q07C04;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getLabel18() {
        return this.label18;
    }

    public JLabel getLabel19() {
        return this.label19;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getQ07C1() {
        return this.q07C1;
    }

    public JLabel getQ07C01_base_label() {
        return this.q07C01_base_label;
    }

    public JLabel getQ07C01a_base_label() {
        return this.q07C01a_base_label;
    }

    public JLabel getQ07C02_base_label() {
        return this.q07C02_base_label;
    }

    public JPercentTextField getQ07C01a() {
        return this.q07C01a;
    }

    public JPercentTextField getQ07C03a() {
        return this.q07C03a;
    }

    public JPercentTextField getQ07C04a() {
        return this.q07C04a;
    }

    public JLabel getQ07C02a_base_label() {
        return this.q07C02a_base_label;
    }

    public JPercentTextField getQ07C02a() {
        return this.q07C02a;
    }

    public JLabel getQ07D01_label() {
        return this.q07D01_label;
    }

    public JCheckBox getQ07D01a() {
        return this.q07D01a;
    }

    public JNIBTextField getQ07D01() {
        return this.q07D01;
    }

    public JLabel getQ07C1_label() {
        return this.q07C1_label;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JLabel getLabel5() {
        return this.label5;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JPercentTextField getQ07C1a() {
        return this.q07C1a;
    }

    public JCheckBox getQ07C1b() {
        return this.q07C1b;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JPanel getPanel11() {
        return this.panel11;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JScrollPane getRostoq7CT1Scroll() {
        return this.rostoq7CT1Scroll;
    }

    public JTable getRostoq07CT1() {
        return this.rostoq07CT1;
    }

    protected void addLineRostoq07ET1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineRostoq07ET1_LinhaActionPerformed(ActionEvent e) {
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel20() {
        return this.label20;
    }

    public JLabel getLabel21() {
        return this.label21;
    }

    public JPanel getPanel12() {
        return this.panel12;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getRostoq7ET1Scroll2() {
        return this.rostoq7ET1Scroll2;
    }

    public JTable getRostoq07ET1() {
        return this.rostoq07ET1;
    }

    protected void addLineRostoq07CT1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineRostoq07CT1_LinhaActionPerformed(ActionEvent e) {
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.Rosto");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label2 = new JLabel();
        this.label3 = new JLabel();
        this.label1 = new JLabel();
        this.label5 = new JLabel();
        this.label6 = new JLabel();
        this.q07C1_label = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.q07C1 = new JNIFTextField();
        this.q07C1a = new JPercentTextField();
        this.q07C1b = new JCheckBox();
        this.panel3 = new JPanel();
        this.label8 = new JLabel();
        this.label9 = new JLabel();
        this.panel4 = new JPanel();
        this.q07C01_base_label = new JLabel();
        this.q07C01a_base_label = new JLabel();
        this.q07C02_base_label = new JLabel();
        this.q07C02a_base_label = new JLabel();
        this.label14 = new JLabelTextFieldNumbering();
        this.q07C01 = new JNIFTextField();
        this.q07C01a = new JPercentTextField();
        this.label15 = new JLabelTextFieldNumbering();
        this.q07C02 = new JNIFTextField();
        this.q07C02a = new JPercentTextField();
        this.label16 = new JLabelTextFieldNumbering();
        this.q07C03 = new JNIFTextField();
        this.q07C03a = new JPercentTextField();
        this.label17 = new JLabelTextFieldNumbering();
        this.q07C04 = new JNIFTextField();
        this.q07C04a = new JPercentTextField();
        this.panel6 = new JPanel();
        this.label20 = new JLabel();
        this.label21 = new JLabel();
        this.panel12 = new JPanel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.rostoq7ET1Scroll2 = new JScrollPane();
        this.rostoq07ET1 = new JTable();
        this.panel9 = new JPanel();
        this.label10 = new JLabel();
        this.label11 = new JLabel();
        this.panel11 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.rostoq7CT1Scroll = new JScrollPane();
        this.rostoq07CT1 = new JTable();
        this.panel5 = new JPanel();
        this.label18 = new JLabel();
        this.label19 = new JLabel();
        this.q07D01_label = new JLabel();
        this.q07D01 = new JNIBTextField();
        this.q07D01a = new JCheckBox();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro07Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro07Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, 757dlu", "$rgap, 8*(default, $lgap), 14dlu, 4*($lgap, default)"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("15dlu, 13dlu, 0px, [203dlu,default], $lcgap, 13dlu, $rgap, 46dlu, $lcgap, default, $ugap, 13dlu, 2*(0px), left:46dlu, $rgap, center:41dlu, $lcgap, center:39dlu, 0px, 272dlu:grow", "$ugap, 7*($lgap, default)"));
        this.label2.setText(bundle.getString("Quadro07Panel.label2.text"));
        this.label2.setBorder(new EtchedBorder());
        this.label2.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label2, CC.xy(1, 3));
        this.label3.setText(bundle.getString("Quadro07Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label3, CC.xywh(2, 3, 20, 1));
        this.label1.setText(bundle.getString("Quadro07Panel.label1.text"));
        this.label1.setBorder(new EtchedBorder());
        this.label1.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label1, CC.xywh(17, 7, 3, 1));
        this.label5.setText(bundle.getString("Quadro07Panel.label5.text"));
        this.label5.setBorder(new EtchedBorder());
        this.label5.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label5, CC.xywh(17, 9, 2, 1));
        this.label6.setText(bundle.getString("Quadro07Panel.label6.text"));
        this.label6.setBorder(new EtchedBorder());
        this.label6.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label6, CC.xy(19, 9));
        this.q07C1_label.setText(bundle.getString("Quadro07Panel.q07C1_label.text"));
        this.q07C1_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.q07C1_label, CC.xy(4, 11));
        this.label4.setText(bundle.getString("Quadro07Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(6, 11));
        this.q07C1.setColumns(9);
        this.panel2.add((Component)this.q07C1, CC.xy(8, 11));
        this.panel2.add((Component)this.q07C1a, CC.xywh(17, 11, 2, 1));
        this.panel2.add((Component)this.q07C1b, CC.xy(19, 11));
        this.this2.add((Component)this.panel2, CC.xy(2, 2, CC.FILL, CC.DEFAULT));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("15dlu, 0px, default, $lcgap, default, $rgap, default, $lcgap, 607dlu:grow", "$ugap, $lgap, default"));
        this.label8.setText(bundle.getString("Quadro07Panel.label8.text"));
        this.label8.setBorder(new EtchedBorder());
        this.label8.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label8, CC.xy(1, 3));
        this.label9.setText(bundle.getString("Quadro07Panel.label9.text"));
        this.label9.setHorizontalAlignment(0);
        this.label9.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label9, CC.xywh(3, 3, 7, 1));
        this.this2.add((Component)this.panel3, CC.xy(2, 6, CC.FILL, CC.DEFAULT));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("119dlu, $lcgap, default, 0px, $lcgap, 43dlu, 0px, $rgap, center:default, $lcgap, 151dlu, default, 0px, $lcgap, 43dlu, 0px, $lcgap, default, $lcgap, left:default:grow", "$ugap, 4*($lgap, default)"));
        this.q07C01_base_label.setText(bundle.getString("Quadro07Panel.q07C01_base_label.text"));
        this.q07C01_base_label.setHorizontalAlignment(0);
        this.q07C01_base_label.setBorder(new EtchedBorder());
        this.panel4.add((Component)this.q07C01_base_label, CC.xywh(3, 3, 4, 1));
        this.q07C01a_base_label.setText(bundle.getString("Quadro07Panel.q07C01a_base_label.text"));
        this.q07C01a_base_label.setBorder(new EtchedBorder());
        this.q07C01a_base_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.q07C01a_base_label, CC.xy(9, 3));
        this.q07C02_base_label.setText(bundle.getString("Quadro07Panel.q07C02_base_label.text"));
        this.q07C02_base_label.setHorizontalAlignment(0);
        this.q07C02_base_label.setBorder(new EtchedBorder());
        this.panel4.add((Component)this.q07C02_base_label, CC.xywh(12, 3, 4, 1));
        this.q07C02a_base_label.setText(bundle.getString("Quadro07Panel.q07C02a_base_label.text"));
        this.q07C02a_base_label.setHorizontalAlignment(0);
        this.q07C02a_base_label.setBorder(new EtchedBorder());
        this.panel4.add((Component)this.q07C02a_base_label, CC.xy(18, 3));
        this.label14.setText(bundle.getString("Quadro07Panel.label14.text"));
        this.label14.setBorder(new EtchedBorder());
        this.label14.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label14, CC.xy(3, 5));
        this.q07C01.setColumns(9);
        this.panel4.add((Component)this.q07C01, CC.xy(6, 5));
        this.q07C01a.setColumns(5);
        this.panel4.add((Component)this.q07C01a, CC.xy(9, 5));
        this.label15.setText(bundle.getString("Quadro07Panel.label15.text"));
        this.label15.setBorder(new EtchedBorder());
        this.label15.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label15, CC.xy(12, 5));
        this.q07C02.setColumns(9);
        this.panel4.add((Component)this.q07C02, CC.xy(15, 5));
        this.q07C02a.setColumns(5);
        this.panel4.add((Component)this.q07C02a, CC.xy(18, 5, CC.CENTER, CC.DEFAULT));
        this.label16.setText(bundle.getString("Quadro07Panel.label16.text"));
        this.label16.setBorder(new EtchedBorder());
        this.label16.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label16, CC.xy(3, 7));
        this.q07C03.setColumns(9);
        this.panel4.add((Component)this.q07C03, CC.xy(6, 7));
        this.q07C03a.setColumns(5);
        this.panel4.add((Component)this.q07C03a, CC.xy(9, 7));
        this.label17.setText(bundle.getString("Quadro07Panel.label17.text"));
        this.label17.setBorder(new EtchedBorder());
        this.label17.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label17, CC.xy(12, 7));
        this.q07C04.setColumns(9);
        this.panel4.add((Component)this.q07C04, CC.xy(15, 7));
        this.q07C04a.setColumns(5);
        this.panel4.add((Component)this.q07C04a, CC.xy(18, 7, CC.CENTER, CC.DEFAULT));
        this.this2.add((Component)this.panel4, CC.xy(2, 8));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, 0px, default, $lcgap, default, $rgap, default, $lcgap, 607dlu:grow", "default"));
        this.label20.setText(bundle.getString("Quadro07Panel.label20.text"));
        this.label20.setBorder(new EtchedBorder());
        this.label20.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label20, CC.xy(1, 1));
        this.label21.setText(bundle.getString("Quadro07Panel.label21.text"));
        this.label21.setHorizontalAlignment(0);
        this.label21.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label21, CC.xywh(3, 1, 7, 1, CC.FILL, CC.DEFAULT));
        this.this2.add((Component)this.panel6, CC.xy(2, 12, CC.FILL, CC.DEFAULT));
        this.panel12.setLayout(new FormLayout("default, $lcgap, 243dlu:grow, $lcgap, min, $lcgap, default:grow, $lcgap, default, $lcgap, 167dlu, $lcgap, default", "default, $lgap, 90dlu, 2*($lgap, default)"));
        this.toolBar2.setRollover(true);
        this.toolBar2.setFloatable(false);
        this.button3.setText(bundle.getString("Quadro07Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.addLineRostoq07ET1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro07Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.removeLineRostoq07ET1_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel12.add((Component)this.toolBar2, CC.xy(5, 1));
        this.rostoq7ET1Scroll2.setViewportView(this.rostoq07ET1);
        this.panel12.add((Component)this.rostoq7ET1Scroll2, CC.xywh(5, 3, 1, 2));
        this.this2.add((Component)this.panel12, CC.xy(2, 14));
        this.panel9.setLayout(new FormLayout("15dlu, 93dlu, default, left:555dlu:grow", "default, $lgap, default"));
        this.label10.setText(bundle.getString("Quadro07Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.panel9.add((Component)this.label10, CC.xy(1, 1));
        this.label11.setText(bundle.getString("Quadro07Panel.label11.text"));
        this.label11.setHorizontalAlignment(0);
        this.label11.setBorder(new EtchedBorder());
        this.panel9.add((Component)this.label11, CC.xywh(2, 1, 3, 1));
        this.this2.add((Component)this.panel9, CC.xy(2, 18, CC.FILL, CC.DEFAULT));
        this.panel11.setLayout(new FormLayout("default, $lcgap, 243dlu:grow, $lcgap, min, $lcgap, default:grow, $lcgap, default, $lcgap, 167dlu, $lcgap, default", "default, $lgap, 90dlu, 2*($lgap, default)"));
        this.toolBar1.setRollover(true);
        this.toolBar1.setFloatable(false);
        this.button1.setText(bundle.getString("Quadro07Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.addLineRostoq07CT1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro07Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro07Panel.this.removeLineRostoq07CT1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel11.add((Component)this.toolBar1, CC.xy(5, 1));
        this.rostoq7CT1Scroll.setViewportView(this.rostoq07CT1);
        this.panel11.add((Component)this.rostoq7CT1Scroll, CC.xywh(5, 3, 1, 2));
        this.this2.add((Component)this.panel11, CC.xy(2, 20));
        this.panel5.setFocusTraversalPolicyProvider(true);
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, 0px, 217dlu, $lcgap, center:149dlu, $lcgap, default, $lcgap, 103dlu, $rgap, 133dlu:grow", "$ugap, 5*($lgap, default)"));
        this.label18.setText(bundle.getString("Quadro07Panel.label18.text"));
        this.label18.setBorder(new EtchedBorder());
        this.label18.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label18, CC.xy(1, 3));
        this.label19.setText(bundle.getString("Quadro07Panel.label19.text"));
        this.label19.setHorizontalAlignment(0);
        this.label19.setBorder(new EtchedBorder());
        this.panel5.add((Component)this.label19, CC.xywh(3, 3, 9, 1, CC.FILL, CC.DEFAULT));
        this.q07D01_label.setText(bundle.getString("Quadro07Panel.q07D01_label.text"));
        this.panel5.add((Component)this.q07D01_label, CC.xywh(3, 7, 3, 1));
        this.panel5.add((Component)this.q07D01, CC.xy(9, 7));
        this.q07D01a.setText(bundle.getString("Quadro07Panel.q07D01a.text"));
        this.panel5.add((Component)this.q07D01a, CC.xy(11, 7));
        this.this2.add((Component)this.panel5, CC.xy(2, 24, CC.FILL, CC.DEFAULT));
        this.add((Component)this.this2, "Center");
    }

}

