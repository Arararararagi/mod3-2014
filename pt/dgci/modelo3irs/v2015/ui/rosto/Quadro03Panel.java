/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro03Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label2;
    protected JLabel label3;
    protected JLabel q03C01_label;
    protected JTextField q03C01;
    protected JLabel q03C02_label;
    protected JTextField q03C02;
    protected JPanel panel3;
    protected JLabel label6;
    protected JLabel label7;
    protected JLabel q03C03a_base_label;
    protected JLabel q03B03_base_label;
    protected JLabel q03C03_label;
    protected JLabelTextFieldNumbering label11;
    protected JNIFTextField q03C03;
    protected JPercentTextField q03C03a;
    protected JCheckBox q03B03;
    protected JLabel q03C04_label;
    protected JLabelTextFieldNumbering label13;
    protected JNIFTextField q03C04;
    protected JPercentTextField q03C04a;
    protected JCheckBox q03B04;
    protected JPanel panel4;
    protected JLabel label14;
    protected JLabel label15;
    protected JPanel panel5;
    protected JLabel q03CD1_label;
    protected JLabelTextFieldNumbering label18;
    protected JNIFTextField q03CD1;
    protected JLabel q03CD2_label;
    protected JLabelTextFieldNumbering label22;
    protected JNIFTextField q03CD2;
    protected JLabel q03CD3_label;
    protected JLabelTextFieldNumbering label26;
    protected JNIFTextField q03CD3;
    protected JLabel q03CD4_label;
    protected JLabelTextFieldNumbering label30;
    protected JNIFTextField q03CD4;
    protected JLabel q03CD5_label;
    protected JLabelTextFieldNumbering label34;
    protected JNIFTextField q03CD5;
    protected JLabel q03CD6_label;
    protected JLabelTextFieldNumbering label38;
    protected JNIFTextField q03CD6;
    protected JLabel q03CD7_label;
    protected JLabelTextFieldNumbering label42;
    protected JNIFTextField q03CD7;
    protected JLabel q03CD8_label;
    protected JLabelTextFieldNumbering label20;
    protected JNIFTextField q03CD8;
    protected JLabel q03CD9_label;
    protected JLabelTextFieldNumbering label24;
    protected JNIFTextField q03CD9;
    protected JLabel q03CD10_label;
    protected JLabelTextFieldNumbering label28;
    protected JNIFTextField q03CD10;
    protected JLabel q03CD11_label;
    protected JLabelTextFieldNumbering label32;
    protected JNIFTextField q03CD11;
    protected JLabel q03CD12_label;
    protected JLabelTextFieldNumbering label36;
    protected JNIFTextField q03CD12;
    protected JLabel q03CD13_label;
    protected JLabelTextFieldNumbering label40;
    protected JNIFTextField q03CD13;
    protected JLabel q03CD14_label;
    protected JLabelTextFieldNumbering label44;
    protected JNIFTextField q03CD14;
    protected JPanel panel6;
    protected JLabel label45;
    protected JLabel label46;
    protected JPanel panel7;
    protected JLabel label48;
    protected JLabel label49;
    protected JLabel q03CDD1a_base_label;
    protected JLabel q03CDD1_label;
    protected JLabelTextFieldNumbering label53;
    protected JNIFTextField q03CDD1;
    protected JPercentTextField q03CDD1a;
    protected JLabel q03CDD2_label;
    protected JLabelTextFieldNumbering label55;
    protected JNIFTextField q03CDD2;
    protected JPercentTextField q03CDD2a;
    protected JLabel q03CDD3_label;
    protected JLabelTextFieldNumbering label57;
    protected JNIFTextField q03CDD3;
    protected JPercentTextField q03CDD3a;
    protected JLabel q03CDD4_label;
    protected JLabelTextFieldNumbering label59;
    protected JNIFTextField q03CDD4;
    protected JPercentTextField q03CDD4a;
    protected JLabel q03CDD5_label;
    protected JLabelTextFieldNumbering label61;
    protected JNIFTextField q03CDD5;
    protected JPercentTextField q03CDD5a;
    protected JLabel q03CDD6_label;
    protected JLabelTextFieldNumbering label63;
    protected JNIFTextField q03CDD6;
    protected JPercentTextField q03CDD6a;
    protected JLabel q03CDD7_label;
    protected JLabelTextFieldNumbering label65;
    protected JNIFTextField q03CDD7;
    protected JPercentTextField q03CDD7a;
    protected JLabel q03CDD8_label;
    protected JLabelTextFieldNumbering label67;
    protected JNIFTextField q03CDD8;
    protected JPercentTextField q03CDD8a;
    protected JPanel panel10;
    protected JLabel label47;
    protected JLabel label52;
    protected JPanel panel11;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane rostoq03DT1Scroll;
    protected JTable rostoq03DT1;

    public Quadro03Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabel getQ03C01_label() {
        return this.q03C01_label;
    }

    public JTextField getQ03C01() {
        return this.q03C01;
    }

    public JLabel getQ03C02_label() {
        return this.q03C02_label;
    }

    public JTextField getQ03C02() {
        return this.q03C02;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JLabel getQ03C03a_base_label() {
        return this.q03C03a_base_label;
    }

    public JLabel getQ03B03_base_label() {
        return this.q03B03_base_label;
    }

    public JLabel getQ03C03_label() {
        return this.q03C03_label;
    }

    public JLabelTextFieldNumbering getLabel11() {
        return this.label11;
    }

    public JNIFTextField getQ03C03() {
        return this.q03C03;
    }

    public JPercentTextField getQ03C03a() {
        return this.q03C03a;
    }

    public JCheckBox getQ03B03() {
        return this.q03B03;
    }

    public JLabel getQ03C04_label() {
        return this.q03C04_label;
    }

    public JLabelTextFieldNumbering getLabel13() {
        return this.label13;
    }

    public JNIFTextField getQ03C04() {
        return this.q03C04;
    }

    public JPercentTextField getQ03C04a() {
        return this.q03C04a;
    }

    public JCheckBox getQ03B04() {
        return this.q03B04;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    public JLabel getLabel15() {
        return this.label15;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getQ03CD1_label() {
        return this.q03CD1_label;
    }

    public JLabelTextFieldNumbering getLabel18() {
        return this.label18;
    }

    public JNIFTextField getQ03CD1() {
        return this.q03CD1;
    }

    public JLabel getQ03CD8_label() {
        return this.q03CD8_label;
    }

    public JLabelTextFieldNumbering getLabel20() {
        return this.label20;
    }

    public JNIFTextField getQ03CD8() {
        return this.q03CD8;
    }

    public JLabel getQ03CD2_label() {
        return this.q03CD2_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JNIFTextField getQ03CD2() {
        return this.q03CD2;
    }

    public JLabel getQ03CD9_label() {
        return this.q03CD9_label;
    }

    public JLabelTextFieldNumbering getLabel24() {
        return this.label24;
    }

    public JNIFTextField getQ03CD9() {
        return this.q03CD9;
    }

    public JLabel getQ03CD3_label() {
        return this.q03CD3_label;
    }

    public JLabelTextFieldNumbering getLabel26() {
        return this.label26;
    }

    public JNIFTextField getQ03CD3() {
        return this.q03CD3;
    }

    public JLabel getQ03CD10_label() {
        return this.q03CD10_label;
    }

    public JLabelTextFieldNumbering getLabel28() {
        return this.label28;
    }

    public JNIFTextField getQ03CD10() {
        return this.q03CD10;
    }

    public JLabel getQ03CD4_label() {
        return this.q03CD4_label;
    }

    public JLabelTextFieldNumbering getLabel30() {
        return this.label30;
    }

    public JNIFTextField getQ03CD4() {
        return this.q03CD4;
    }

    public JLabel getQ03CD11_label() {
        return this.q03CD11_label;
    }

    public JLabelTextFieldNumbering getLabel32() {
        return this.label32;
    }

    public JNIFTextField getQ03CD11() {
        return this.q03CD11;
    }

    public JLabel getQ03CD5_label() {
        return this.q03CD5_label;
    }

    public JLabelTextFieldNumbering getLabel34() {
        return this.label34;
    }

    public JNIFTextField getQ03CD5() {
        return this.q03CD5;
    }

    public JLabel getQ03CD12_label() {
        return this.q03CD12_label;
    }

    public JLabelTextFieldNumbering getLabel36() {
        return this.label36;
    }

    public JNIFTextField getQ03CD12() {
        return this.q03CD12;
    }

    public JLabel getQ03CD6_label() {
        return this.q03CD6_label;
    }

    public JLabelTextFieldNumbering getLabel38() {
        return this.label38;
    }

    public JNIFTextField getQ03CD6() {
        return this.q03CD6;
    }

    public JLabel getQ03CD13_label() {
        return this.q03CD13_label;
    }

    public JLabelTextFieldNumbering getLabel40() {
        return this.label40;
    }

    public JNIFTextField getQ03CD13() {
        return this.q03CD13;
    }

    public JLabel getQ03CD7_label() {
        return this.q03CD7_label;
    }

    public JLabelTextFieldNumbering getLabel42() {
        return this.label42;
    }

    public JNIFTextField getQ03CD7() {
        return this.q03CD7;
    }

    public JLabel getQ03CD14_label() {
        return this.q03CD14_label;
    }

    public JLabelTextFieldNumbering getLabel44() {
        return this.label44;
    }

    public JNIFTextField getQ03CD14() {
        return this.q03CD14;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel45() {
        return this.label45;
    }

    public JLabel getLabel46() {
        return this.label46;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getLabel48() {
        return this.label48;
    }

    public JLabel getLabel49() {
        return this.label49;
    }

    public JLabel getQ03CDD1a_base_label() {
        return this.q03CDD1a_base_label;
    }

    public JLabel getQ03CDD1_label() {
        return this.q03CDD1_label;
    }

    public JLabelTextFieldNumbering getLabel53() {
        return this.label53;
    }

    public JNIFTextField getQ03CDD1() {
        return this.q03CDD1;
    }

    public JPercentTextField getQ03CDD1a() {
        return this.q03CDD1a;
    }

    public JLabel getQ03CDD2_label() {
        return this.q03CDD2_label;
    }

    public JLabelTextFieldNumbering getLabel55() {
        return this.label55;
    }

    public JNIFTextField getQ03CDD2() {
        return this.q03CDD2;
    }

    public JPercentTextField getQ03CDD2a() {
        return this.q03CDD2a;
    }

    public JLabel getQ03CDD3_label() {
        return this.q03CDD3_label;
    }

    public JLabelTextFieldNumbering getLabel57() {
        return this.label57;
    }

    public JNIFTextField getQ03CDD3() {
        return this.q03CDD3;
    }

    public JPercentTextField getQ03CDD3a() {
        return this.q03CDD3a;
    }

    public JLabel getQ03CDD4_label() {
        return this.q03CDD4_label;
    }

    public JLabelTextFieldNumbering getLabel59() {
        return this.label59;
    }

    public JNIFTextField getQ03CDD4() {
        return this.q03CDD4;
    }

    public JPercentTextField getQ03CDD4a() {
        return this.q03CDD4a;
    }

    public JLabel getQ03CDD5_label() {
        return this.q03CDD5_label;
    }

    public JLabelTextFieldNumbering getLabel61() {
        return this.label61;
    }

    public JNIFTextField getQ03CDD5() {
        return this.q03CDD5;
    }

    public JPercentTextField getQ03CDD5a() {
        return this.q03CDD5a;
    }

    public JLabel getQ03CDD6_label() {
        return this.q03CDD6_label;
    }

    public JLabelTextFieldNumbering getLabel63() {
        return this.label63;
    }

    public JNIFTextField getQ03CDD6() {
        return this.q03CDD6;
    }

    public JPercentTextField getQ03CDD6a() {
        return this.q03CDD6a;
    }

    public JLabel getQ03CDD7_label() {
        return this.q03CDD7_label;
    }

    public JLabelTextFieldNumbering getLabel65() {
        return this.label65;
    }

    public JNIFTextField getQ03CDD7() {
        return this.q03CDD7;
    }

    public JPercentTextField getQ03CDD7a() {
        return this.q03CDD7a;
    }

    public JLabel getQ03CDD8_label() {
        return this.q03CDD8_label;
    }

    public JLabelTextFieldNumbering getLabel67() {
        return this.label67;
    }

    public JNIFTextField getQ03CDD8() {
        return this.q03CDD8;
    }

    public JPercentTextField getQ03CDD8a() {
        return this.q03CDD8a;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getPanel10() {
        return this.panel10;
    }

    public JLabel getLabel47() {
        return this.label47;
    }

    public JLabel getLabel52() {
        return this.label52;
    }

    protected void addLineRostoq03DT1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineRostoq03DT1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel11() {
        return this.panel11;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getRostoq03DT1Scroll() {
        return this.rostoq03DT1Scroll;
    }

    public JTable getRostoq03DT1() {
        return this.rostoq03DT1;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.Rosto");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label2 = new JLabel();
        this.label3 = new JLabel();
        this.q03C01_label = new JLabel();
        this.q03C01 = new JTextField();
        this.q03C02_label = new JLabel();
        this.q03C02 = new JTextField();
        this.panel3 = new JPanel();
        this.label6 = new JLabel();
        this.label7 = new JLabel();
        this.q03C03a_base_label = new JLabel();
        this.q03B03_base_label = new JLabel();
        this.q03C03_label = new JLabel();
        this.label11 = new JLabelTextFieldNumbering();
        this.q03C03 = new JNIFTextField();
        this.q03C03a = new JPercentTextField();
        this.q03B03 = new JCheckBox();
        this.q03C04_label = new JLabel();
        this.label13 = new JLabelTextFieldNumbering();
        this.q03C04 = new JNIFTextField();
        this.q03C04a = new JPercentTextField();
        this.q03B04 = new JCheckBox();
        this.panel4 = new JPanel();
        this.label14 = new JLabel();
        this.label15 = new JLabel();
        this.panel5 = new JPanel();
        this.q03CD1_label = new JLabel();
        this.label18 = new JLabelTextFieldNumbering();
        this.q03CD1 = new JNIFTextField();
        this.q03CD2_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.q03CD2 = new JNIFTextField();
        this.q03CD3_label = new JLabel();
        this.label26 = new JLabelTextFieldNumbering();
        this.q03CD3 = new JNIFTextField();
        this.q03CD4_label = new JLabel();
        this.label30 = new JLabelTextFieldNumbering();
        this.q03CD4 = new JNIFTextField();
        this.q03CD5_label = new JLabel();
        this.label34 = new JLabelTextFieldNumbering();
        this.q03CD5 = new JNIFTextField();
        this.q03CD6_label = new JLabel();
        this.label38 = new JLabelTextFieldNumbering();
        this.q03CD6 = new JNIFTextField();
        this.q03CD7_label = new JLabel();
        this.label42 = new JLabelTextFieldNumbering();
        this.q03CD7 = new JNIFTextField();
        this.q03CD8_label = new JLabel();
        this.label20 = new JLabelTextFieldNumbering();
        this.q03CD8 = new JNIFTextField();
        this.q03CD9_label = new JLabel();
        this.label24 = new JLabelTextFieldNumbering();
        this.q03CD9 = new JNIFTextField();
        this.q03CD10_label = new JLabel();
        this.label28 = new JLabelTextFieldNumbering();
        this.q03CD10 = new JNIFTextField();
        this.q03CD11_label = new JLabel();
        this.label32 = new JLabelTextFieldNumbering();
        this.q03CD11 = new JNIFTextField();
        this.q03CD12_label = new JLabel();
        this.label36 = new JLabelTextFieldNumbering();
        this.q03CD12 = new JNIFTextField();
        this.q03CD13_label = new JLabel();
        this.label40 = new JLabelTextFieldNumbering();
        this.q03CD13 = new JNIFTextField();
        this.q03CD14_label = new JLabel();
        this.label44 = new JLabelTextFieldNumbering();
        this.q03CD14 = new JNIFTextField();
        this.panel6 = new JPanel();
        this.label45 = new JLabel();
        this.label46 = new JLabel();
        this.panel7 = new JPanel();
        this.label48 = new JLabel();
        this.label49 = new JLabel();
        this.q03CDD1a_base_label = new JLabel();
        this.q03CDD1_label = new JLabel();
        this.label53 = new JLabelTextFieldNumbering();
        this.q03CDD1 = new JNIFTextField();
        this.q03CDD1a = new JPercentTextField();
        this.q03CDD2_label = new JLabel();
        this.label55 = new JLabelTextFieldNumbering();
        this.q03CDD2 = new JNIFTextField();
        this.q03CDD2a = new JPercentTextField();
        this.q03CDD3_label = new JLabel();
        this.label57 = new JLabelTextFieldNumbering();
        this.q03CDD3 = new JNIFTextField();
        this.q03CDD3a = new JPercentTextField();
        this.q03CDD4_label = new JLabel();
        this.label59 = new JLabelTextFieldNumbering();
        this.q03CDD4 = new JNIFTextField();
        this.q03CDD4a = new JPercentTextField();
        this.q03CDD5_label = new JLabel();
        this.label61 = new JLabelTextFieldNumbering();
        this.q03CDD5 = new JNIFTextField();
        this.q03CDD5a = new JPercentTextField();
        this.q03CDD6_label = new JLabel();
        this.label63 = new JLabelTextFieldNumbering();
        this.q03CDD6 = new JNIFTextField();
        this.q03CDD6a = new JPercentTextField();
        this.q03CDD7_label = new JLabel();
        this.label65 = new JLabelTextFieldNumbering();
        this.q03CDD7 = new JNIFTextField();
        this.q03CDD7a = new JPercentTextField();
        this.q03CDD8_label = new JLabel();
        this.label67 = new JLabelTextFieldNumbering();
        this.q03CDD8 = new JNIFTextField();
        this.q03CDD8a = new JPercentTextField();
        this.panel10 = new JPanel();
        this.label47 = new JLabel();
        this.label52 = new JLabel();
        this.panel11 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.rostoq03DT1Scroll = new JScrollPane();
        this.rostoq03DT1 = new JTable();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro03Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro03Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $lcgap, default", "$rgap, 14*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("$rgap, 10dlu, 0px, default:grow, $rgap", "6*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro03Panel.label2.text"));
        this.label2.setBorder(new EtchedBorder());
        this.label2.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label2, CC.xywh(1, 1, 2, 1));
        this.label3.setText(bundle.getString("Quadro03Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label3, CC.xywh(4, 1, 2, 1));
        this.q03C01_label.setText(bundle.getString("Quadro03Panel.q03C01_label.text"));
        this.panel2.add((Component)this.q03C01_label, CC.xywh(2, 3, 3, 1));
        this.q03C01.setEditable(false);
        this.panel2.add((Component)this.q03C01, CC.xywh(2, 5, 3, 1));
        this.q03C02_label.setText(bundle.getString("Quadro03Panel.q03C02_label.text"));
        this.panel2.add((Component)this.q03C02_label, CC.xywh(2, 9, 3, 1));
        this.q03C02.setEditable(false);
        this.panel2.add((Component)this.q03C02, CC.xywh(2, 11, 3, 1));
        this.this2.add((Component)this.panel2, CC.xy(2, 2));
        this.panel3.setPreferredSize(null);
        this.panel3.setMinimumSize(null);
        this.panel3.setLayout(new FormLayout("$rgap, default:grow, $rgap, default, $rgap, 13dlu, 0px, $lcgap, 43dlu, $lcgap, default:grow, 0px, $ugap, center:80dlu, 0px, default, $lcgap, 21dlu, $lcgap, center:default, $rgap", "$ugap, $lgap, default, 0px, 4*(default, $lgap), default"));
        this.label6.setText(bundle.getString("Quadro03Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.label6.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label6, CC.xywh(2, 3, 12, 3));
        this.label7.setText(bundle.getString("Quadro03Panel.label7.text"));
        this.label7.setHorizontalAlignment(0);
        this.label7.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label7, CC.xywh(14, 3, 7, 1));
        this.q03C03a_base_label.setText(bundle.getString("Quadro03Panel.q03C03a_base_label.text"));
        this.q03C03a_base_label.setBorder(new EtchedBorder());
        this.q03C03a_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.q03C03a_base_label, CC.xy(14, 5, CC.FILL, CC.DEFAULT));
        this.q03B03_base_label.setText(bundle.getString("Quadro03Panel.q03B03_base_label.text"));
        this.q03B03_base_label.setBorder(new EtchedBorder());
        this.q03B03_base_label.setHorizontalAlignment(0);
        this.panel3.add((Component)this.q03B03_base_label, CC.xywh(16, 5, 5, 1));
        this.q03C03_label.setText(bundle.getString("Quadro03Panel.q03C03_label.text"));
        this.panel3.add((Component)this.q03C03_label, CC.xy(4, 9));
        this.label11.setText(bundle.getString("Quadro03Panel.label11.text"));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label11, CC.xy(6, 9));
        this.q03C03.setColumns(9);
        this.panel3.add((Component)this.q03C03, CC.xy(9, 9));
        this.q03C03a.setColumns(5);
        this.panel3.add((Component)this.q03C03a, CC.xy(14, 9));
        this.q03B03.setHorizontalAlignment(0);
        this.panel3.add((Component)this.q03B03, CC.xy(18, 9));
        this.q03C04_label.setText(bundle.getString("Quadro03Panel.q03C04_label.text"));
        this.panel3.add((Component)this.q03C04_label, CC.xy(4, 11));
        this.label13.setText(bundle.getString("Quadro03Panel.label13.text"));
        this.label13.setBorder(new EtchedBorder());
        this.label13.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label13, CC.xy(6, 11));
        this.q03C04.setColumns(9);
        this.panel3.add((Component)this.q03C04, CC.xy(9, 11));
        this.q03C04a.setColumns(5);
        this.panel3.add((Component)this.q03C04a, CC.xy(14, 11));
        this.q03B04.setHorizontalAlignment(0);
        this.panel3.add((Component)this.q03B04, CC.xy(18, 11));
        this.this2.add((Component)this.panel3, CC.xy(2, 4));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("$rgap, 10dlu, 0px, default:grow, $lcgap, default, $rgap, default, $lcgap, default:grow", "default"));
        this.label14.setText(bundle.getString("Quadro03Panel.label14.text"));
        this.label14.setBorder(new EtchedBorder());
        this.label14.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label14, CC.xywh(1, 1, 2, 1));
        this.label15.setText(bundle.getString("Quadro03Panel.label15.text"));
        this.label15.setHorizontalAlignment(0);
        this.label15.setBorder(new EtchedBorder());
        this.panel4.add((Component)this.label15, CC.xywh(4, 1, 7, 1));
        this.this2.add((Component)this.panel4, CC.xy(2, 8));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("default:grow, $lcgap, default, $rgap, 13dlu, 0px, $lcgap, 43dlu, $lcgap, 15dlu, $lcgap, default, $rgap, 13dlu, 0px, $lcgap, 43dlu, $lcgap, default:grow", "$ugap, 7*($lgap, default)"));
        this.q03CD1_label.setText(bundle.getString("Quadro03Panel.q03CD1_label.text"));
        this.panel5.add((Component)this.q03CD1_label, CC.xy(3, 3));
        this.label18.setText(bundle.getString("Quadro03Panel.label18.text"));
        this.label18.setBorder(new EtchedBorder());
        this.label18.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label18, CC.xy(5, 3));
        this.q03CD1.setColumns(9);
        this.panel5.add((Component)this.q03CD1, CC.xy(8, 3));
        this.q03CD2_label.setText(bundle.getString("Quadro03Panel.q03CD2_label.text"));
        this.panel5.add((Component)this.q03CD2_label, CC.xy(12, 3));
        this.label22.setText(bundle.getString("Quadro03Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label22, CC.xy(14, 3));
        this.q03CD2.setColumns(9);
        this.panel5.add((Component)this.q03CD2, CC.xy(17, 3));
        this.q03CD3_label.setText(bundle.getString("Quadro03Panel.q03CD3_label.text"));
        this.panel5.add((Component)this.q03CD3_label, CC.xy(3, 5));
        this.label26.setText(bundle.getString("Quadro03Panel.label26.text"));
        this.label26.setBorder(new EtchedBorder());
        this.label26.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label26, CC.xy(5, 5));
        this.q03CD3.setColumns(9);
        this.panel5.add((Component)this.q03CD3, CC.xy(8, 5));
        this.q03CD4_label.setText(bundle.getString("Quadro03Panel.q03CD4_label.text"));
        this.panel5.add((Component)this.q03CD4_label, CC.xy(12, 5));
        this.label30.setText(bundle.getString("Quadro03Panel.label30.text"));
        this.label30.setBorder(new EtchedBorder());
        this.label30.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label30, CC.xy(14, 5));
        this.q03CD4.setColumns(9);
        this.panel5.add((Component)this.q03CD4, CC.xy(17, 5));
        this.q03CD5_label.setText(bundle.getString("Quadro03Panel.q03CD5_label.text"));
        this.panel5.add((Component)this.q03CD5_label, CC.xy(3, 7));
        this.label34.setText(bundle.getString("Quadro03Panel.label34.text"));
        this.label34.setBorder(new EtchedBorder());
        this.label34.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label34, CC.xy(5, 7));
        this.q03CD5.setColumns(9);
        this.panel5.add((Component)this.q03CD5, CC.xy(8, 7));
        this.q03CD6_label.setText(bundle.getString("Quadro03Panel.q03CD6_label.text"));
        this.panel5.add((Component)this.q03CD6_label, CC.xy(12, 7));
        this.label38.setText(bundle.getString("Quadro03Panel.label38.text"));
        this.label38.setBorder(new EtchedBorder());
        this.label38.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label38, CC.xy(14, 7));
        this.q03CD6.setColumns(9);
        this.panel5.add((Component)this.q03CD6, CC.xy(17, 7));
        this.q03CD7_label.setText(bundle.getString("Quadro03Panel.q03CD7_label.text"));
        this.panel5.add((Component)this.q03CD7_label, CC.xy(3, 9));
        this.label42.setText(bundle.getString("Quadro03Panel.label42.text"));
        this.label42.setBorder(new EtchedBorder());
        this.label42.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label42, CC.xy(5, 9));
        this.q03CD7.setColumns(9);
        this.panel5.add((Component)this.q03CD7, CC.xy(8, 9));
        this.q03CD8_label.setText(bundle.getString("Quadro03Panel.q03CD8_label.text"));
        this.panel5.add((Component)this.q03CD8_label, CC.xy(12, 9));
        this.label20.setText(bundle.getString("Quadro03Panel.label20.text"));
        this.label20.setBorder(new EtchedBorder());
        this.label20.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label20, CC.xy(14, 9));
        this.q03CD8.setColumns(9);
        this.panel5.add((Component)this.q03CD8, CC.xy(17, 9));
        this.q03CD9_label.setText(bundle.getString("Quadro03Panel.q03CD9_label.text"));
        this.panel5.add((Component)this.q03CD9_label, CC.xy(3, 11));
        this.label24.setText(bundle.getString("Quadro03Panel.label24.text"));
        this.label24.setBorder(new EtchedBorder());
        this.label24.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label24, CC.xy(5, 11));
        this.q03CD9.setColumns(9);
        this.panel5.add((Component)this.q03CD9, CC.xy(8, 11));
        this.q03CD10_label.setText(bundle.getString("Quadro03Panel.q03CD10_label.text"));
        this.panel5.add((Component)this.q03CD10_label, CC.xy(12, 11));
        this.label28.setText(bundle.getString("Quadro03Panel.label28.text"));
        this.label28.setBorder(new EtchedBorder());
        this.label28.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label28, CC.xy(14, 11));
        this.q03CD10.setColumns(9);
        this.panel5.add((Component)this.q03CD10, CC.xy(17, 11));
        this.q03CD11_label.setText(bundle.getString("Quadro03Panel.q03CD11_label.text"));
        this.panel5.add((Component)this.q03CD11_label, CC.xy(3, 13));
        this.label32.setText(bundle.getString("Quadro03Panel.label32.text"));
        this.label32.setBorder(new EtchedBorder());
        this.label32.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label32, CC.xy(5, 13));
        this.q03CD11.setColumns(9);
        this.panel5.add((Component)this.q03CD11, CC.xy(8, 13));
        this.q03CD12_label.setText(bundle.getString("Quadro03Panel.q03CD12_label.text"));
        this.panel5.add((Component)this.q03CD12_label, CC.xy(12, 13));
        this.label36.setText(bundle.getString("Quadro03Panel.label36.text"));
        this.label36.setBorder(new EtchedBorder());
        this.label36.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label36, CC.xy(14, 13));
        this.q03CD12.setColumns(9);
        this.panel5.add((Component)this.q03CD12, CC.xy(17, 13));
        this.q03CD13_label.setText(bundle.getString("Quadro03Panel.q03CD13_label.text"));
        this.panel5.add((Component)this.q03CD13_label, CC.xy(3, 15));
        this.label40.setText(bundle.getString("Quadro03Panel.label40.text"));
        this.label40.setBorder(new EtchedBorder());
        this.label40.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label40, CC.xy(5, 15));
        this.q03CD13.setColumns(9);
        this.panel5.add((Component)this.q03CD13, CC.xy(8, 15));
        this.q03CD14_label.setText(bundle.getString("Quadro03Panel.q03CD14_label.text"));
        this.panel5.add((Component)this.q03CD14_label, CC.xy(12, 15));
        this.label44.setText(bundle.getString("Quadro03Panel.label44.text"));
        this.label44.setBorder(new EtchedBorder());
        this.label44.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label44, CC.xy(14, 15));
        this.q03CD14.setColumns(9);
        this.panel5.add((Component)this.q03CD14, CC.xy(17, 15));
        this.this2.add((Component)this.panel5, CC.xy(2, 12));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("$rgap, 10dlu, 0px, default:grow, $lcgap, default, $rgap, default, $lcgap, default:grow", "default"));
        this.label45.setText(bundle.getString("Quadro03Panel.label45.text"));
        this.label45.setBorder(new EtchedBorder());
        this.label45.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label45, CC.xywh(1, 1, 2, 1));
        this.label46.setText(bundle.getString("Quadro03Panel.label46.text"));
        this.label46.setHorizontalAlignment(0);
        this.label46.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label46, CC.xywh(4, 1, 7, 1));
        this.this2.add((Component)this.panel6, CC.xy(2, 16));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("$rgap, default:grow, $rgap, default, $rgap, 13dlu, 0px, $lcgap, 43dlu, $lcgap, default:grow, 0px, $ugap, center:80dlu, 0px, $lcgap, center:default, $rgap", "$ugap, $lgap, default, 0px, 9*(default, $lgap), default"));
        this.label48.setHorizontalAlignment(0);
        this.label48.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.label48, CC.xywh(2, 3, 10, 3));
        this.label49.setText(bundle.getString("Quadro03Panel.label49.text"));
        this.label49.setHorizontalAlignment(0);
        this.label49.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.label49, CC.xy(14, 3, CC.FILL, CC.DEFAULT));
        this.q03CDD1a_base_label.setText(bundle.getString("Quadro03Panel.q03CDD1a_base_label.text"));
        this.q03CDD1a_base_label.setBorder(new EtchedBorder());
        this.q03CDD1a_base_label.setHorizontalAlignment(0);
        this.panel7.add((Component)this.q03CDD1a_base_label, CC.xy(14, 5, CC.FILL, CC.DEFAULT));
        this.q03CDD1_label.setText(bundle.getString("Quadro03Panel.q03CDD1_label.text"));
        this.panel7.add((Component)this.q03CDD1_label, CC.xy(4, 9));
        this.label53.setText(bundle.getString("Quadro03Panel.label53.text"));
        this.label53.setBorder(new EtchedBorder());
        this.label53.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label53, CC.xy(6, 9));
        this.q03CDD1.setColumns(9);
        this.panel7.add((Component)this.q03CDD1, CC.xy(9, 9));
        this.q03CDD1a.setColumns(5);
        this.panel7.add((Component)this.q03CDD1a, CC.xy(14, 9));
        this.q03CDD2_label.setText(bundle.getString("Quadro03Panel.q03CDD2_label.text"));
        this.panel7.add((Component)this.q03CDD2_label, CC.xy(4, 11));
        this.label55.setText(bundle.getString("Quadro03Panel.label55.text"));
        this.label55.setBorder(new EtchedBorder());
        this.label55.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label55, CC.xy(6, 11));
        this.q03CDD2.setColumns(9);
        this.panel7.add((Component)this.q03CDD2, CC.xy(9, 11));
        this.q03CDD2a.setColumns(5);
        this.panel7.add((Component)this.q03CDD2a, CC.xy(14, 11));
        this.q03CDD3_label.setText(bundle.getString("Quadro03Panel.q03CDD3_label.text"));
        this.panel7.add((Component)this.q03CDD3_label, CC.xy(4, 13));
        this.label57.setText(bundle.getString("Quadro03Panel.label57.text"));
        this.label57.setBorder(new EtchedBorder());
        this.label57.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label57, CC.xy(6, 13));
        this.q03CDD3.setColumns(9);
        this.panel7.add((Component)this.q03CDD3, CC.xy(9, 13));
        this.q03CDD3a.setColumns(5);
        this.panel7.add((Component)this.q03CDD3a, CC.xy(14, 13));
        this.q03CDD4_label.setText(bundle.getString("Quadro03Panel.q03CDD4_label.text"));
        this.panel7.add((Component)this.q03CDD4_label, CC.xy(4, 15));
        this.label59.setText(bundle.getString("Quadro03Panel.label59.text"));
        this.label59.setBorder(new EtchedBorder());
        this.label59.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label59, CC.xy(6, 15));
        this.q03CDD4.setColumns(9);
        this.panel7.add((Component)this.q03CDD4, CC.xy(9, 15));
        this.q03CDD4a.setColumns(5);
        this.panel7.add((Component)this.q03CDD4a, CC.xy(14, 15));
        this.q03CDD5_label.setText(bundle.getString("Quadro03Panel.q03CDD5_label.text"));
        this.panel7.add((Component)this.q03CDD5_label, CC.xy(4, 17));
        this.label61.setText(bundle.getString("Quadro03Panel.label61.text"));
        this.label61.setBorder(new EtchedBorder());
        this.label61.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label61, CC.xy(6, 17));
        this.q03CDD5.setColumns(9);
        this.panel7.add((Component)this.q03CDD5, CC.xy(9, 17));
        this.q03CDD5a.setColumns(5);
        this.panel7.add((Component)this.q03CDD5a, CC.xy(14, 17));
        this.q03CDD6_label.setText(bundle.getString("Quadro03Panel.q03CDD6_label.text"));
        this.panel7.add((Component)this.q03CDD6_label, CC.xy(4, 19));
        this.label63.setText(bundle.getString("Quadro03Panel.label63.text"));
        this.label63.setBorder(new EtchedBorder());
        this.label63.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label63, CC.xy(6, 19));
        this.q03CDD6.setColumns(9);
        this.panel7.add((Component)this.q03CDD6, CC.xy(9, 19));
        this.q03CDD6a.setColumns(5);
        this.panel7.add((Component)this.q03CDD6a, CC.xy(14, 19));
        this.q03CDD7_label.setText(bundle.getString("Quadro03Panel.q03CDD7_label.text"));
        this.panel7.add((Component)this.q03CDD7_label, CC.xy(4, 21));
        this.label65.setText(bundle.getString("Quadro03Panel.label65.text"));
        this.label65.setBorder(new EtchedBorder());
        this.label65.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label65, CC.xy(6, 21));
        this.q03CDD7.setColumns(9);
        this.panel7.add((Component)this.q03CDD7, CC.xy(9, 21));
        this.q03CDD7a.setColumns(5);
        this.panel7.add((Component)this.q03CDD7a, CC.xy(14, 21));
        this.q03CDD8_label.setText(bundle.getString("Quadro03Panel.q03CDD8_label.text"));
        this.panel7.add((Component)this.q03CDD8_label, CC.xy(4, 23));
        this.label67.setText(bundle.getString("Quadro03Panel.label67.text"));
        this.label67.setRequestFocusEnabled(false);
        this.label67.setBorder(new EtchedBorder());
        this.label67.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label67, CC.xy(6, 23));
        this.q03CDD8.setColumns(9);
        this.panel7.add((Component)this.q03CDD8, CC.xy(9, 23));
        this.q03CDD8a.setColumns(5);
        this.panel7.add((Component)this.q03CDD8a, CC.xy(14, 23));
        this.this2.add((Component)this.panel7, CC.xy(2, 20));
        this.panel10.setMinimumSize(null);
        this.panel10.setPreferredSize(null);
        this.panel10.setLayout(new FormLayout("$rgap, 10dlu, 0px, default:grow", "default"));
        this.label47.setText(bundle.getString("Quadro03Panel.label47.text"));
        this.label47.setBorder(new EtchedBorder());
        this.label47.setHorizontalAlignment(0);
        this.panel10.add((Component)this.label47, CC.xywh(1, 1, 2, 1));
        this.label52.setText(bundle.getString("Quadro03Panel.label52.text"));
        this.label52.setHorizontalAlignment(0);
        this.label52.setBorder(new EtchedBorder());
        this.panel10.add((Component)this.label52, CC.xy(4, 1));
        this.this2.add((Component)this.panel10, CC.xywh(2, 24, 2, 1));
        this.panel11.setLayout(new FormLayout("default, $lcgap, 10dlu:grow, $lcgap, min:grow, $lcgap, 10dlu:grow, $lcgap, default", "default, $lgap, 90dlu, 2*($lgap, default)"));
        this.toolBar1.setRollover(true);
        this.toolBar1.setFloatable(false);
        this.button1.setText(bundle.getString("Quadro03Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro03Panel.this.addLineRostoq03DT1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro03Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro03Panel.this.removeLineRostoq03DT1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel11.add((Component)this.toolBar1, CC.xy(5, 1));
        this.rostoq03DT1Scroll.setViewportView(this.rostoq03DT1);
        this.panel11.add((Component)this.rostoq03DT1Scroll, CC.xywh(5, 3, 1, 2));
        this.this2.add((Component)this.panel11, CC.xy(2, 28));
        this.add((Component)this.this2, "Center");
    }

}

