/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;

public class QuadroInicioPanel
extends JPanel {
    protected QuadroTitlePanel quadroTitlePanel1;
    protected JPanel panel1;
    protected JPanel superPanel;

    public QuadroInicioPanel() {
        this.initComponents();
    }

    public JPanel getSuperPanel() {
        return this.superPanel;
    }

    public QuadroTitlePanel getQuadroTitlePanel1() {
        return this.quadroTitlePanel1;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    private void initComponents() {
        this.quadroTitlePanel1 = new QuadroTitlePanel();
        this.panel1 = new JPanel();
        this.superPanel = new JPanel();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.quadroTitlePanel1.setTitle("Aten\u00e7\u00e3o");
        this.quadroTitlePanel1.setNumber("In\u00edcio");
        this.add((Component)this.quadroTitlePanel1, "North");
        this.panel1.setBorder(new LineBorder(Color.black));
        this.panel1.setLayout(new FormLayout("$glue", "default"));
        this.superPanel.setMaximumSize(new Dimension(1024, 1024));
        this.superPanel.setMinimumSize(new Dimension(800, 600));
        this.superPanel.setLayout(new BorderLayout());
        this.panel1.add((Component)this.superPanel, cc.xy(1, 1));
        this.add((Component)this.panel1, "Center");
    }
}

