/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel label2;
    protected JLabel label3;
    protected JLabelTextFieldNumbering label4;
    protected JRadioButton q05B1OP1;
    protected JLabelTextFieldNumbering label6;
    protected JRadioButton q05B1OP2;
    protected JLabelTextFieldNumbering label8;
    protected JRadioButton q05B1OP3;
    protected JLabel label10;
    protected JLabel label11;
    protected JLabelTextFieldNumbering label12;
    protected JRadioButton q05B1OP4;
    protected JLabel q05C5_label;
    protected JLabelTextFieldNumbering label15;
    protected JNIFTextField q05C5;
    protected JLabel q05C5_1_label;
    protected JEditableComboBox q05C5_1;
    protected JPanel panel3;
    protected JLabel label16;
    protected JLabelTextFieldNumbering label17;
    protected JRadioButton q05B2OP6;
    protected JSeparator separator3;
    protected JLabelTextFieldNumbering label18;
    protected JRadioButton q05B2OP7;
    protected JLabel label20;
    protected JLabel label29;
    protected JLabelTextFieldNumbering label19;
    protected JRadioButton q05B3OP8;
    protected JSeparator separator4;
    protected JLabelTextFieldNumbering label21;
    protected JRadioButton q05B3OP9;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton q05B4OP10;
    protected JLabelTextFieldNumbering label24;
    protected JRadioButton q05B4OP11;
    protected JSeparator separator2;
    protected JLabel q05C12_label;
    protected JLabelTextFieldNumbering q05C12_num;
    protected JPanel panel5;
    protected JMoneyTextField q05C12;
    protected JLabel q05C13_label;
    protected JLabelTextFieldNumbering label28;
    protected JPanel panel4;
    protected JEditableComboBox q05C13;

    public Quadro05Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getLabel2() {
        return this.label2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JRadioButton getQ05B1OP1() {
        return this.q05B1OP1;
    }

    public JLabelTextFieldNumbering getLabel6() {
        return this.label6;
    }

    public JRadioButton getQ05B1OP2() {
        return this.q05B1OP2;
    }

    public JLabelTextFieldNumbering getLabel8() {
        return this.label8;
    }

    public JRadioButton getQ05B1OP3() {
        return this.q05B1OP3;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JLabelTextFieldNumbering getLabel12() {
        return this.label12;
    }

    public JRadioButton getQ05B1OP4() {
        return this.q05B1OP4;
    }

    public JLabel getQ05C5_label() {
        return this.q05C5_label;
    }

    public JLabelTextFieldNumbering getLabel15() {
        return this.label15;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel16() {
        return this.label16;
    }

    public JLabelTextFieldNumbering getLabel17() {
        return this.label17;
    }

    public JRadioButton getQ05B2OP6() {
        return this.q05B2OP6;
    }

    public JSeparator getSeparator3() {
        return this.separator3;
    }

    public JLabelTextFieldNumbering getLabel18() {
        return this.label18;
    }

    public JRadioButton getQ05B2OP7() {
        return this.q05B2OP7;
    }

    public JLabel getLabel20() {
        return this.label20;
    }

    public JLabel getLabel29() {
        return this.label29;
    }

    public JLabelTextFieldNumbering getLabel19() {
        return this.label19;
    }

    public JRadioButton getQ05B3OP8() {
        return this.q05B3OP8;
    }

    public JSeparator getSeparator4() {
        return this.separator4;
    }

    public JLabelTextFieldNumbering getLabel21() {
        return this.label21;
    }

    public JRadioButton getQ05B3OP9() {
        return this.q05B3OP9;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getQ05B4OP10() {
        return this.q05B4OP10;
    }

    public JLabelTextFieldNumbering getLabel24() {
        return this.label24;
    }

    public JRadioButton getQ05B4OP11() {
        return this.q05B4OP11;
    }

    public JSeparator getSeparator2() {
        return this.separator2;
    }

    public JLabel getQ05C12_label() {
        return this.q05C12_label;
    }

    public JLabelTextFieldNumbering getQ05C12_num() {
        return this.q05C12_num;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getQ05C13_label() {
        return this.q05C13_label;
    }

    public JLabelTextFieldNumbering getLabel28() {
        return this.label28;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JEditableComboBox getQ05C13() {
        return this.q05C13;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    private void createUIComponents() {
    }

    public JLabel getQ05C5_1_label() {
        return this.q05C5_1_label;
    }

    public JEditableComboBox getQ05C5_1() {
        return this.q05C5_1;
    }

    public JNIFTextField getQ05C5() {
        return this.q05C5;
    }

    public JMoneyTextField getQ05C12() {
        return this.q05C12;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.Rosto");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label2 = new JLabel();
        this.label3 = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.q05B1OP1 = new JRadioButton();
        this.label6 = new JLabelTextFieldNumbering();
        this.q05B1OP2 = new JRadioButton();
        this.label8 = new JLabelTextFieldNumbering();
        this.q05B1OP3 = new JRadioButton();
        this.label10 = new JLabel();
        this.label11 = new JLabel();
        this.label12 = new JLabelTextFieldNumbering();
        this.q05B1OP4 = new JRadioButton();
        this.q05C5_label = new JLabel();
        this.label15 = new JLabelTextFieldNumbering();
        this.q05C5 = new JNIFTextField();
        this.q05C5_1_label = new JLabel();
        this.q05C5_1 = new JEditableComboBox();
        this.panel3 = new JPanel();
        this.label16 = new JLabel();
        this.label17 = new JLabelTextFieldNumbering();
        this.q05B2OP6 = new JRadioButton();
        this.separator3 = new JSeparator();
        this.label18 = new JLabelTextFieldNumbering();
        this.q05B2OP7 = new JRadioButton();
        this.label20 = new JLabel();
        this.label29 = new JLabel();
        this.label19 = new JLabelTextFieldNumbering();
        this.q05B3OP8 = new JRadioButton();
        this.separator4 = new JSeparator();
        this.label21 = new JLabelTextFieldNumbering();
        this.q05B3OP9 = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.q05B4OP10 = new JRadioButton();
        this.label24 = new JLabelTextFieldNumbering();
        this.q05B4OP11 = new JRadioButton();
        this.separator2 = new JSeparator();
        this.q05C12_label = new JLabel();
        this.q05C12_num = new JLabelTextFieldNumbering();
        this.panel5 = new JPanel();
        this.q05C12 = new JMoneyTextField();
        this.q05C13_label = new JLabel();
        this.label28 = new JLabelTextFieldNumbering();
        this.panel4 = new JPanel();
        this.q05C13 = new JEditableComboBox();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, 0px", "$rgap, default, $lgap, default"));
        this.panel2.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap, 13dlu, 0px, 13dlu, $lcgap, 20dlu, $lcgap, 13dlu, 0px, 13dlu, $lcgap, 20dlu, $lcgap, 15dlu, 0px, $rgap, 13dlu, 2*(0px), 131dlu, $lcgap, 15dlu, 0px, 97dlu, $lcgap, 13dlu, 0px, $lcgap, 43dlu, $lcgap, 53dlu, $rgap, default:grow, $ugap", "12*(default, $lgap), default, fill:0px, 7dlu, $glue, 3*(default, $lgap), default, 7dlu, 9*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro05Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label2, cc.xy(1, 1));
        this.label3.setText(bundle.getString("Quadro05Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label3, cc.xywh(3, 1, 35, 1));
        this.label4.setText(bundle.getString("Quadro05Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, cc.xy(20, 3));
        this.q05B1OP1.setText(bundle.getString("Quadro05Panel.q05B1OP1.text"));
        this.panel2.add((Component)this.q05B1OP1, cc.xy(23, 3));
        this.label6.setText(bundle.getString("Quadro05Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.label6.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label6, cc.xy(20, 5));
        this.q05B1OP2.setText(bundle.getString("Quadro05Panel.q05B1OP2.text"));
        this.panel2.add((Component)this.q05B1OP2, cc.xy(23, 5));
        this.label8.setText(bundle.getString("Quadro05Panel.label8.text"));
        this.label8.setHorizontalAlignment(0);
        this.label8.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label8, cc.xy(20, 7));
        this.q05B1OP3.setText(bundle.getString("Quadro05Panel.q05B1OP3.text"));
        this.panel2.add((Component)this.q05B1OP3, cc.xy(23, 7));
        this.label10.setText(bundle.getString("Quadro05Panel.label10.text"));
        this.label10.setHorizontalAlignment(0);
        this.label10.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label10, cc.xy(1, 11));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.label11.setText(bundle.getString("Quadro05Panel.label11.text"));
        this.panel2.add((Component)this.label11, cc.xywh(3, 11, 35, 1));
        this.label12.setText(bundle.getString("Quadro05Panel.label12.text"));
        this.label12.setHorizontalAlignment(0);
        this.label12.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label12, cc.xy(20, 13));
        this.q05B1OP4.setText(bundle.getString("Quadro05Panel.q05B1OP4.text"));
        this.panel2.add((Component)this.q05B1OP4, cc.xy(23, 13));
        this.q05C5_label.setText(bundle.getString("Quadro05Panel.q05C5_label.text"));
        this.q05C5_label.setHorizontalAlignment(4);
        this.q05C5_label.setHorizontalTextPosition(4);
        this.panel2.add((Component)this.q05C5_label, cc.xywh(25, 13, 3, 1));
        this.label15.setText(bundle.getString("Quadro05Panel.label15.text"));
        this.label15.setBorder(new EtchedBorder());
        this.label15.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label15, cc.xy(29, 13));
        this.q05C5.setColumns(9);
        this.panel2.add((Component)this.q05C5, cc.xy(32, 13));
        this.q05C5_1_label.setText(bundle.getString("Quadro05Panel.q05C5_1_label.text_2"));
        this.panel2.add((Component)this.q05C5_1_label, cc.xywh(25, 15, 3, 1, CellConstraints.RIGHT, CellConstraints.DEFAULT));
        this.panel2.add((Component)this.q05C5_1, cc.xywh(32, 15, 4, 1));
        this.panel3.setLayout(new FormLayout("default", "default"));
        this.panel2.add((Component)this.panel3, cc.xy(23, 17));
        this.label16.setText(bundle.getString("Quadro05Panel.label16.text"));
        this.panel2.add((Component)this.label16, cc.xywh(3, 21, 21, 1));
        this.label17.setText(bundle.getString("Quadro05Panel.label17.text"));
        this.label17.setBorder(new EtchedBorder());
        this.label17.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label17, cc.xy(5, 25));
        this.q05B2OP6.setText(bundle.getString("Quadro05Panel.q05B2OP6.text"));
        this.panel2.add((Component)this.q05B2OP6, cc.xywh(7, 25, 17, 1));
        this.panel2.add((Component)this.separator3, cc.xywh(5, 27, 32, 1));
        this.label18.setText(bundle.getString("Quadro05Panel.label18.text"));
        this.label18.setBorder(new EtchedBorder());
        this.label18.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label18, cc.xy(5, 29));
        this.q05B2OP7.setText(bundle.getString("Quadro05Panel.q05B2OP7.text"));
        this.panel2.add((Component)this.q05B2OP7, cc.xywh(7, 29, 17, 1));
        this.panel2.add((Component)this.label20, cc.xywh(23, 31, 13, 1));
        this.panel2.add((Component)this.label29, cc.xywh(17, 33, 9, 1));
        this.label19.setText(bundle.getString("Quadro05Panel.label19.text"));
        this.label19.setBorder(new EtchedBorder());
        this.label19.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label19, cc.xy(11, 35));
        this.q05B3OP8.setText(bundle.getString("Quadro05Panel.q05B3OP8.text"));
        this.panel2.add((Component)this.q05B3OP8, cc.xywh(13, 35, 15, 1));
        this.panel2.add((Component)this.separator4, cc.xywh(11, 36, 26, 1));
        this.label21.setText(bundle.getString("Quadro05Panel.label21.text"));
        this.label21.setBorder(new EtchedBorder());
        this.label21.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label21, cc.xy(11, 37));
        this.q05B3OP9.setText(bundle.getString("Quadro05Panel.q05B3OP9.text"));
        this.panel2.add((Component)this.q05B3OP9, cc.xywh(13, 37, 20, 1));
        this.label23.setText(bundle.getString("Quadro05Panel.label23.text"));
        this.label23.setHorizontalAlignment(0);
        this.label23.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label23, cc.xy(17, 43));
        this.q05B4OP10.setText(bundle.getString("Quadro05Panel.q05B4OP10.text"));
        this.panel2.add((Component)this.q05B4OP10, cc.xywh(19, 43, 5, 1));
        this.label24.setText(bundle.getString("Quadro05Panel.label24.text"));
        this.label24.setBorder(new EtchedBorder());
        this.label24.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label24, cc.xy(25, 43));
        this.q05B4OP11.setText(bundle.getString("Quadro05Panel.q05B4OP11.text"));
        this.q05B4OP11.setHorizontalAlignment(2);
        this.panel2.add((Component)this.q05B4OP11, cc.xywh(27, 43, 6, 1));
        this.panel2.add((Component)this.separator2, cc.xywh(11, 47, 26, 1));
        this.q05C12_label.setText(bundle.getString("Quadro05Panel.q05C12_label.text"));
        this.q05C12_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.q05C12_label, cc.xywh(3, 53, 15, 1));
        this.q05C12_num.setText(bundle.getString("Quadro05Panel.q05C12_num.text"));
        this.q05C12_num.setBorder(new EtchedBorder());
        this.q05C12_num.setHorizontalAlignment(0);
        this.panel2.add((Component)this.q05C12_num, cc.xy(20, 53));
        this.panel5.setLayout(new FormLayout("$lcgap, 85dlu", "default"));
        this.q05C12.setColumns(15);
        this.panel5.add((Component)this.q05C12, cc.xy(2, 1));
        this.panel2.add((Component)this.panel5, cc.xy(23, 53));
        this.q05C13_label.setText(bundle.getString("Quadro05Panel.q05C13_label.text"));
        this.q05C13_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.q05C13_label, cc.xywh(3, 55, 15, 1));
        this.label28.setText(bundle.getString("Quadro05Panel.label28.text"));
        this.label28.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label28, cc.xy(20, 55));
        this.panel4.setLayout(new FormLayout("$lcgap, 112dlu", "default"));
        this.panel4.add((Component)this.q05C13, cc.xy(2, 1));
        this.panel2.add((Component)this.panel4, cc.xy(23, 55));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.add((Component)this.this2, "Center");
    }
}

