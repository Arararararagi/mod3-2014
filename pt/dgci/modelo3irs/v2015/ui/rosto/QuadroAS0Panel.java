/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.net.URL;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class QuadroAS0Panel
extends JPanel {
    private static final long serialVersionUID = -3386580642950918822L;
    protected JPanel panel2;
    protected JLabel label3;
    protected JPanel panel1;
    protected JLabel label1;
    protected JRadioButton qAS0B1OP1;
    protected JRadioButton qAS0B1OP2;
    protected JRadioButton qAS0B1OP3;
    protected JRadioButton qAS0B1OP4;

    public QuadroAS0Panel() {
        this.initComponents();
        ButtonGroup opcaoGrupo = new ButtonGroup();
        opcaoGrupo.add(this.qAS0B1OP1);
        opcaoGrupo.add(this.qAS0B1OP2);
        opcaoGrupo.add(this.qAS0B1OP3);
        opcaoGrupo.add(this.qAS0B1OP4);
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JRadioButton getQAS0B1OP1() {
        return this.qAS0B1OP1;
    }

    public JRadioButton getQAS0B1OP2() {
        return this.qAS0B1OP2;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JRadioButton getQAS0B1OP3() {
        return this.qAS0B1OP3;
    }

    public JRadioButton getQAS0B1OP4() {
        return this.qAS0B1OP4;
    }

    private void initComponents() {
        this.panel2 = new JPanel();
        this.label3 = new JLabel();
        this.panel1 = new JPanel();
        this.label1 = new JLabel();
        this.qAS0B1OP1 = new JRadioButton();
        this.qAS0B1OP2 = new JRadioButton();
        this.qAS0B1OP3 = new JRadioButton();
        this.qAS0B1OP4 = new JRadioButton();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(new Dimension(540, 140));
        this.setLayout(new FormLayout("default, $rgap, default:grow, 0px", "top:default"));
        this.panel2.setLayout(new FormLayout("default, $lcgap, $rgap", "fill:default"));
        this.label3.setIcon(new ImageIcon(this.getClass().getResource("/icons/icon.gif")));
        this.label3.setAlignmentY(-20.0f);
        this.panel2.add((Component)this.label3, cc.xy(1, 1));
        this.add((Component)this.panel2, cc.xy(1, 1));
        this.panel1.setPreferredSize(null);
        this.panel1.setMinimumSize(null);
        this.panel1.setLayout(new FormLayout("320dlu:grow", "default, $lgap, $rgap, 4*($lgap, default), 2*($rgap, $lgap), default, $lgap, default"));
        this.label1.setText("<html>Para efectuar o preenchimento da sua declara\u00e7\u00e3o de forma simples e r\u00e1pida, pode optar por uma das op\u00e7\u00f5es:</html>");
        this.panel1.add((Component)this.label1, cc.xy(1, 1));
        this.qAS0B1OP1.setText("<html>Obten\u00e7\u00e3o de uma declara\u00e7\u00e3o pr\u00e9-preenchida.</html>");
        this.panel1.add((Component)this.qAS0B1OP1, cc.xy(1, 5));
        this.qAS0B1OP2.setText("<html>Leitura de uma declara\u00e7\u00e3o previamente gravada num ficheiro (ficheiro).</html>");
        this.panel1.add((Component)this.qAS0B1OP2, cc.xy(1, 7));
        this.qAS0B1OP3.setText("<html>Obten\u00e7\u00e3o da \u00faltima declara\u00e7\u00e3o submetida (requer acesso \u00e0 internet).</html>");
        this.panel1.add((Component)this.qAS0B1OP3, cc.xy(1, 9));
        this.qAS0B1OP4.setText("<html>Cria\u00e7\u00e3o de uma declara\u00e7\u00e3o vazia.</html>");
        this.panel1.add((Component)this.qAS0B1OP4, cc.xy(1, 11));
        this.add((Component)this.panel1, cc.xy(3, 1));
    }
}

