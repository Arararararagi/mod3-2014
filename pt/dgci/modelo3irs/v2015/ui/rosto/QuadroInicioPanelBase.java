/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroInicioPanel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class QuadroInicioPanelBase
extends QuadroInicioPanel
implements IBindablePanel<QuadroInicio> {
    private static final long serialVersionUID = 1;
    protected QuadroInicio model;

    @Override
    public abstract void setModel(QuadroInicio var1, boolean var2);

    @Override
    public QuadroInicio getModel() {
        return this.model;
    }

    public QuadroInicioPanelBase(QuadroInicio model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

