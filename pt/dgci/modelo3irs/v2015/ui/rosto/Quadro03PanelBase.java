/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro03Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro03PanelBase
extends Quadro03Panel
implements IBindablePanel<Quadro03> {
    private static final long serialVersionUID = 1;
    protected Quadro03 model;

    @Override
    public abstract void setModel(Quadro03 var1, boolean var2);

    @Override
    public Quadro03 getModel() {
        return this.model;
    }

    @Override
    protected void addLineRostoq03DT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getRostoq03DT1().add(new Rostoq03DT1_Linha());
        }
    }

    @Override
    protected void removeLineRostoq03DT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.rostoq03DT1.getSelectedRow() != -1 ? this.rostoq03DT1.getSelectedRow() : this.rostoq03DT1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getRostoq03DT1().remove(selectedRow);
            }
        }
    }

    public Quadro03PanelBase(Quadro03 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

