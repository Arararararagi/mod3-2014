/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.dgci.modelo3irs.v2015.binding.rosto.QuadroInicioBindings;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroInicioPanelBase;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.Util;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class QuadroInicioPanelExtension
extends QuadroInicioPanelBase
implements IBindablePanel<QuadroInicio> {
    private static final long serialVersionUID = 3289064144989696901L;
    private Dimension initialTextLbSize;

    public QuadroInicioPanelExtension(QuadroInicio model, boolean skipBinding) {
        Component msgPane;
        super(model, skipBinding);
        JPanel main = new JPanel();
        main.setLayout(new BorderLayout());
        try {
            msgPane = new Quadro0Pane();
        }
        catch (IOException e) {
            msgPane = new JLabel();
        }
        this.initialTextLbSize = msgPane.getPreferredSize();
        main.add((Component)new JLabel("           "), "East");
        main.add(msgPane, "Center");
        main.add((Component)new JLabel("           "), "West");
        main.setBorder(BorderFactory.createEmptyBorder());
        main.setSize(this.superPanel.getSize());
        super.getSuperPanel().add(main);
    }

    @Override
    public void setModel(QuadroInicio model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            QuadroInicioBindings.doBindings(model, this);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension((int)super.getPreferredSize().getWidth(), (int)super.getPreferredSize().getHeight() + this.quadroTitlePanel1.getHeight());
    }

    class Quadro0Pane
    extends JEditorPane
    implements HyperlinkListener {
        private static final String ACTION_PROTOCOL = "action://";
        private static final String AJUDA_PREENCHIMENTO_ACTION = "ajuda";
        private static final String AJUDA_TEMAS_ACTION = "temas";

        public Quadro0Pane() throws IOException {
            this.setEditable(false);
            StyleSheet css = new StyleSheet();
            css.importStyleSheet(Util.class.getResource("/ajuda/Rosto/ajudaPF.css"));
            HTMLEditorKit edtKit = new HTMLEditorKit();
            edtKit.setStyleSheet(css);
            this.setEditorKit(edtKit);
            this.setDocument(new HTMLDocument(css));
            this.setText(Util.getHtmlText(this, "/ajuda/Rosto/alerta_Rosto.htm"));
            this.setBackground(UIManager.getColor("ScrollBar.background"));
            this.addHyperlinkListener(this);
        }

        @Override
        public void hyperlinkUpdate(HyperlinkEvent e) {
            String desc;
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED && (desc = e.getDescription()).startsWith("action://")) {
                if ("ajuda".equalsIgnoreCase(desc.substring("action://".length()))) {
                    this.simulateKeyPressed(112);
                } else if ("temas".equalsIgnoreCase(desc.substring("action://".length()))) {
                    this.simulateKeyPressed(113);
                }
            }
        }

        private void simulateKeyPressed(int keyCode) {
            EventQueue q = this.getToolkit().getSystemEventQueue();
            q.postEvent(new KeyEvent(this, 401, System.currentTimeMillis(), 0, keyCode, (char)keyCode));
        }
    }

}

