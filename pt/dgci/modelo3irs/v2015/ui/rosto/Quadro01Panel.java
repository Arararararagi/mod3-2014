/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;

public class Quadro01Panel
extends JPanel {
    protected JPanel this2;
    protected JLabel q01C01_label;
    protected JLabelTextFieldNumbering label3;
    protected JLimitedTextField q01C01;
    protected QuadroTitlePanel titlePanel;

    public Quadro01Panel() {
        this.initComponents();
    }

    public JLabel getQ01C01_label() {
        return this.q01C01_label;
    }

    public JTextField getQ01C01() {
        return this.q01C01;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.Rosto");
        this.this2 = new JPanel();
        this.q01C01_label = new JLabel();
        this.label3 = new JLabelTextFieldNumbering();
        this.q01C01 = new JLimitedTextField();
        this.titlePanel = new QuadroTitlePanel();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $rgap, left:default, $rgap, 13dlu, 0px, $lcgap, default, $rgap, default:grow", "$rgap, default, $lgap, default"));
        this.q01C01_label.setText(bundle.getString("Quadro01Panel.q01C01_label.text"));
        this.q01C01_label.setHorizontalAlignment(4);
        this.this2.add((Component)this.q01C01_label, cc.xy(4, 2));
        this.label3.setText(bundle.getString("Quadro01Panel.label3.text"));
        this.label3.setBorder(new EtchedBorder());
        this.label3.setHorizontalAlignment(0);
        this.this2.add((Component)this.label3, cc.xy(6, 2));
        this.q01C01.setMaxLength(4);
        this.q01C01.setOnlyNumeric(true);
        this.q01C01.setColumns(4);
        this.q01C01.setHorizontalAlignment(4);
        this.this2.add((Component)this.q01C01, cc.xy(9, 2));
        this.add((Component)this.this2, "Center");
        this.titlePanel.setNumber(bundle.getString("Quadro01Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro01Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
    }
}

