/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro07Bindings;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro07PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro07PanelExtension
extends Quadro07PanelBase
implements IBindablePanel<Quadro07> {
    public Quadro07PanelExtension(Quadro07 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro07 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro07Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void removeLineRostoq07ET1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            boolean isLastRow = false;
            int lastRow = this.rostoq07ET1.getRowCount() - 1;
            int selectedRow = this.rostoq07ET1.getSelectedRow();
            if (selectedRow == -1) {
                selectedRow = lastRow;
                isLastRow = true;
            }
            if (selectedRow != -1) {
                Rostoq07ET1_Linha linha = this.model.getRostoq07ET1().get(selectedRow);
                this.model.getRostoq07ET1().remove(selectedRow);
                if (!(isLastRow && linha.getNIF() == null)) {
                    RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
                    rosto.getRostoq07ET1ChangeEvent().fireDeleteRow(selectedRow, linha.getNIF() == null, isLastRow);
                }
            }
        }
    }

    @Override
    protected void removeLineRostoq07CT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            boolean isLastRow = false;
            int lastRow = this.rostoq07CT1.getRowCount() - 1;
            int selectedRow = this.rostoq07CT1.getSelectedRow();
            if (selectedRow == -1) {
                selectedRow = lastRow;
                isLastRow = true;
            }
            if (selectedRow != -1) {
                Rostoq07CT1_Linha linha = this.model.getRostoq07CT1().get(selectedRow);
                this.model.getRostoq07CT1().remove(selectedRow);
                if (!(isLastRow && linha.getNIF() == null)) {
                    RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
                    rosto.getRostoq07CT1ChangeEvent().fireDeleteRow(selectedRow, linha.getNIF() == null, isLastRow);
                }
            }
        }
    }
}

