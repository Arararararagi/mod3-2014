/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro04PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04> {
    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
        }
    }
}

