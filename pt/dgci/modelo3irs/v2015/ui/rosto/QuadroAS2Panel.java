/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.util.Session;

public class QuadroAS2Panel
extends JPanel {
    protected JPanel panel1;
    protected JLabel label1;
    protected JLabel label3;
    protected JPanel panel3;
    protected JPanel panel4;
    protected JComboBox qAS1C1;
    protected JLabel label4;
    protected JNIFTextField qAS1C2;
    protected JLabel label5;
    protected JPasswordField qAS1C3;
    protected JLabel label6;
    protected JNIFTextField qAS1C4;
    protected JLabel label7;
    protected JPasswordField qAS1C5;

    public QuadroAS2Panel() {
        this.initComponents();
        if (!Session.isApplet()) {
            this.addCatalogAnos();
        } else if (Session.getApplet().getParameter("anoExercicio") == null) {
            this.addCatalogAnos();
        } else {
            this.qAS1C1.addItem(Session.getApplet().getParameter("anoExercicio"));
            this.qAS1C1.setEnabled(false);
        }
    }

    private void addCatalogAnos() {
        this.qAS1C1.addItem("");
        this.qAS1C1.addItem("2014");
        if (Modelo3IRSv2015Parameters.instance().isFase2()) {
            this.qAS1C1.addItem("2013");
            this.qAS1C1.addItem("2012");
            this.qAS1C1.addItem("2011");
            this.qAS1C1.addItem("2010");
            this.qAS1C1.addItem("2009");
            this.qAS1C1.addItem("2008");
            this.qAS1C1.addItem("2007");
            this.qAS1C1.addItem("2006");
            this.qAS1C1.addItem("2005");
            this.qAS1C1.addItem("2004");
            this.qAS1C1.addItem("2003");
        }
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JComboBox getQAS1C1() {
        return this.qAS1C1;
    }

    public JLabel getLabel4() {
        return this.label4;
    }

    public JNIFTextField getQAS1C2() {
        return this.qAS1C2;
    }

    public JLabel getLabel5() {
        return this.label5;
    }

    public JPasswordField getQAS1C3() {
        return this.qAS1C3;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JNIFTextField getQAS1C4() {
        return this.qAS1C4;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JPasswordField getQAS1C5() {
        return this.qAS1C5;
    }

    private void initComponents() {
        this.panel1 = new JPanel();
        this.label1 = new JLabel();
        this.label3 = new JLabel();
        this.panel3 = new JPanel();
        this.panel4 = new JPanel();
        this.qAS1C1 = new JComboBox();
        this.label4 = new JLabel();
        this.qAS1C2 = new JNIFTextField();
        this.label5 = new JLabel();
        this.qAS1C3 = new JPasswordField();
        this.label6 = new JLabel();
        this.qAS1C4 = new JNIFTextField();
        this.label7 = new JLabel();
        this.qAS1C5 = new JPasswordField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(new Dimension(300, 150));
        this.setRequestFocusEnabled(false);
        this.setLayout(new FormLayout("default:grow, 0px", "default"));
        this.panel1.setPreferredSize(null);
        this.panel1.setMinimumSize(null);
        this.panel1.setLayout(new FormLayout("20dlu, $lcgap, default, $lcgap, 50dlu:grow, $lcgap, default", "6*(default, $lgap)"));
        this.label1.setText("<html>Por favor, indique:</html>");
        this.panel1.add((Component)this.label1, cc.xy(3, 1));
        this.label3.setText("Ano de Exerc\u00edcio");
        this.panel1.add((Component)this.label3, cc.xy(3, 3));
        this.panel3.setPreferredSize(null);
        this.panel3.setMinimumSize(null);
        this.panel3.setLayout(new FormLayout("default:grow", "default"));
        this.panel4.setLayout(new FormLayout("default", "default"));
        this.qAS1C1.setPreferredSize(null);
        this.panel4.add((Component)this.qAS1C1, cc.xy(1, 1));
        this.panel3.add((Component)this.panel4, cc.xy(1, 1));
        this.panel1.add((Component)this.panel3, cc.xy(5, 3));
        this.label4.setText("NIF do Sujeito Passivo A:");
        this.panel1.add((Component)this.label4, cc.xy(3, 5));
        this.qAS1C2.setColumns(7);
        this.qAS1C2.setPreferredSize(new Dimension(30, 20));
        this.qAS1C2.setHorizontalAlignment(2);
        this.panel1.add((Component)this.qAS1C2, cc.xy(5, 5));
        this.label5.setText("Senha do Sujeito Passivo A:");
        this.panel1.add((Component)this.label5, cc.xy(3, 7));
        this.qAS1C3.setColumns(16);
        this.panel1.add((Component)this.qAS1C3, cc.xy(5, 7));
        this.label6.setText("NIF do Sujeito Passivo B:");
        this.panel1.add((Component)this.label6, cc.xy(3, 9));
        this.qAS1C4.setColumns(7);
        this.qAS1C4.setHorizontalAlignment(2);
        this.panel1.add((Component)this.qAS1C4, cc.xy(5, 9));
        this.label7.setText("Senha do Sujeito Passivo B:");
        this.panel1.add((Component)this.label7, cc.xy(3, 11));
        this.qAS1C5.setColumns(16);
        this.panel1.add((Component)this.qAS1C5, cc.xy(5, 11));
        this.add((Component)this.panel1, cc.xy(1, 1));
    }
}

