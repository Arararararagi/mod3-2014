/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.actions.PrePreenchimentoAction;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS0Dialog;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS1Panel;
import pt.opensoft.field.NifValidator;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.util.ArrayUtil;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.StringUtil;

public class QuadroAS1Dialog
extends JDialog {
    private static final long serialVersionUID = -136300494198152213L;
    private QuadroAS0Dialog quadroAS0;
    private static final int MIN_PASSWORD_SIZE = 8;
    private static final int MAX_PASSWORD_SIZE = 16;
    private static final String IRS_VALID_YEARS_FASE1 = "2014";
    private static final String IRS_VALID_YEARS_FASE2 = "2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003";
    private static final String[] IRS_VALIDATION_YEARS = Modelo3IRSv2015Parameters.instance().isFase2() ? ArrayUtil.toStringArray("2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003") : ArrayUtil.toStringArray("2014");
    private String anoExercicio;
    private String nifA;
    private String passA;
    private String nifB;
    private String passB;
    private QuadroAS1Panel quadroAS1Panel1;
    private JPanel panel2;
    private JButton qAS1B1;
    private JButton qAS1B2;
    private JButton qAS1B3;

    public QuadroAS1Dialog(Frame owner, QuadroAS0Dialog parent) {
        this(owner);
        this.quadroAS0 = parent;
    }

    public QuadroAS1Dialog(Frame owner) {
        super(owner);
        this.initComponents();
        this.keyListener();
    }

    public QuadroAS1Dialog(Dialog owner) {
        super(owner);
        this.initComponents();
    }

    public String getAno() {
        return this.anoExercicio;
    }

    public String getNifA() {
        return this.nifA;
    }

    public String getPassA() {
        return this.passA;
    }

    public String getNifB() {
        return this.nifB;
    }

    public String getPassB() {
        return this.passB;
    }

    private void qAS1B1ActionPerformed(ActionEvent e) {
        this.quadroAS0.setVisible(true);
        this.quadroAS0.setModal(true);
        this.setVisible(false);
    }

    private void qAS1B3ActionPerformed(ActionEvent e) {
        if (this.quadroAS0 != null) {
            this.quadroAS0.dispose();
        }
        this.dispose();
    }

    private void qAS1B2ActionPerformed(ActionEvent e) {
        this.anoExercicio = this.quadroAS1Panel1.getQAS1C1().getSelectedItem().toString();
        this.nifA = this.quadroAS1Panel1.getQAS1C2().getText();
        this.passA = new String(this.quadroAS1Panel1.getQAS1C3().getPassword());
        this.nifB = this.quadroAS1Panel1.getQAS1C4().getText();
        this.passB = new String(this.quadroAS1Panel1.getQAS1C5().getPassword());
        if (this.validateData(this.anoExercicio, this.nifA, this.nifB, this.passA, this.passB)) {
            if (!(StringUtil.isEmpty(this.passB) || this.passB.trim().length() <= 16)) {
                this.passB = this.passB.substring(0, 16);
            }
            if (!(StringUtil.isEmpty(this.passA) || this.passA.trim().length() <= 16)) {
                this.passA = this.passA.substring(0, 16);
            }
            PrePreenchimentoAction prePreenchimentoAction = (PrePreenchimentoAction)ActionsTaxClientManager.getAction("PrePreenchimento");
            prePreenchimentoAction.setDialog(this);
            ActionsTaxClientManager.getAction("PrePreenchimento").actionPerformed(e);
            this.setVisible(false);
        }
    }

    public QuadroAS1Panel getQuadroAS1Panel1() {
        return this.quadroAS1Panel1;
    }

    private boolean validateData(String ano, String nifA, String nifB, String passwordA, String passwordB) {
        if (StringUtil.isEmpty(ano)) {
            DialogFactory.instance().showErrorDialog("Por favor indique o ano do exerc\u00edcio.");
            return false;
        }
        try {
            Long.parseLong(ano);
        }
        catch (NumberFormatException nfe) {
            DialogFactory.instance().showErrorDialog("O ano do exerc\u00edcio indicado \u00e9 inv\u00e1lido.");
            return false;
        }
        if (!ListUtil.toList(IRS_VALIDATION_YEARS).contains(ano)) {
            DialogFactory.instance().showErrorDialog("O ano do exerc\u00edcio indicado \u00e9 inv\u00e1lido.");
            return false;
        }
        if (StringUtil.isEmpty(nifA)) {
            DialogFactory.instance().showErrorDialog("Por favor indique o NIF do sujeito passivo A.");
            return false;
        }
        if (!NifValidator.validate(nifA)) {
            DialogFactory.instance().showErrorDialog("O NIF do sujeito passivo A \u00e9 inv\u00e1lido.");
            return false;
        }
        if (!NifValidator.isSingular(nifA)) {
            DialogFactory.instance().showErrorDialog("O NIF do sujeito passivo A \u00e9 inv\u00e1lido. Sujeito Passivo A colectivo.");
            return false;
        }
        if (StringUtil.isEmpty(passwordA)) {
            DialogFactory.instance().showErrorDialog("Por favor indique a senha do sujeito passivo A.");
            return false;
        }
        if (passwordA.trim().length() < 8) {
            DialogFactory.instance().showErrorDialog("A senha do sujeito passivo A \u00e9 inv\u00e1lida.");
            return false;
        }
        if (!StringUtil.isEmpty(nifB)) {
            if (!NifValidator.validate(nifB)) {
                DialogFactory.instance().showErrorDialog("O NIF do sujeito passivo B \u00e9 inv\u00e1lido.");
                return false;
            }
            if (!NifValidator.isSingular(nifB)) {
                DialogFactory.instance().showErrorDialog("O NIF do sujeito passivo B \u00e9 inv\u00e1lido. Sujeito Passivo B colectivo.");
                return false;
            }
            if (nifA.trim().equals(nifB.trim())) {
                DialogFactory.instance().showErrorDialog("O NIF do sujeito passivo A n\u00e3o pode ser igual ao NIF do sujeito passivo B.");
                return false;
            }
            if (StringUtil.isEmpty(passwordB)) {
                DialogFactory.instance().showErrorDialog("Por favor indique a senha do sujeito passivo B.");
                return false;
            }
            if (passwordB.trim().length() < 8) {
                DialogFactory.instance().showErrorDialog("A senha do sujeito passivo B \u00e9 inv\u00e1lida.");
                return false;
            }
        } else if (!StringUtil.isEmpty(passwordB)) {
            DialogFactory.instance().showErrorDialog("Senha do sujeito passivo B preenchida e NIF do sujeito passivo B n\u00e3o preenchido.");
            return false;
        }
        return true;
    }

    private void keyListener() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher(){

            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
                ActionEvent actionEvent = new ActionEvent(this, 1001, KeyEvent.getKeyText(e.getKeyCode()));
                if (e.getKeyCode() == 27) {
                    QuadroAS1Dialog.this.qAS1B3ActionPerformed(actionEvent);
                } else if (e.getKeyCode() == 10 && QuadroAS1Dialog.this.isActive() && e.getID() == 401) {
                    QuadroAS1Dialog.this.qAS1B2ActionPerformed(actionEvent);
                }
                boolean discardEvent = false;
                return discardEvent;
            }
        });
    }

    private void initComponents() {
        this.quadroAS1Panel1 = new QuadroAS1Panel();
        this.panel2 = new JPanel();
        this.qAS1B1 = new JButton();
        this.qAS1B2 = new JButton();
        this.qAS1B3 = new JButton();
        CellConstraints cc = new CellConstraints();
        this.setTitle("Assistente de preenchimento");
        this.setResizable(false);
        this.setModal(true);
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new FormLayout("$ugap, $lcgap, default, $lcgap, $ugap", "$ugap, default, $lgap, default, $ugap"));
        contentPane.add((Component)this.quadroAS1Panel1, cc.xy(3, 2));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("$ugap, $rgap, default:grow, $rgap, 3*(default, $lcgap), default:grow", "default"));
        this.qAS1B1.setText("\u00ab Anterior");
        this.qAS1B1.setPreferredSize(new Dimension(120, 22));
        this.qAS1B1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                QuadroAS1Dialog.this.qAS1B1ActionPerformed(e);
                QuadroAS1Dialog.this.qAS1B1ActionPerformed(e);
            }
        });
        this.panel2.add((Component)this.qAS1B1, cc.xy(5, 1));
        this.qAS1B2.setText("Continuar \u00bb");
        this.qAS1B2.setPreferredSize(new Dimension(120, 22));
        this.qAS1B2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                QuadroAS1Dialog.this.qAS1B2ActionPerformed(e);
            }
        });
        this.panel2.add((Component)this.qAS1B2, cc.xy(7, 1));
        this.qAS1B3.setText("Cancelar");
        this.qAS1B3.setMaximumSize(new Dimension(89, 23));
        this.qAS1B3.setMinimumSize(new Dimension(89, 23));
        this.qAS1B3.setPreferredSize(new Dimension(120, 22));
        this.qAS1B3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                QuadroAS1Dialog.this.qAS1B3ActionPerformed(e);
            }
        });
        this.panel2.add((Component)this.qAS1B3, cc.xy(9, 1));
        contentPane.add((Component)this.panel2, cc.xy(3, 4));
        this.pack();
        this.setLocationRelativeTo(this.getOwner());
    }

}

