/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro03Bindings;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro03PanelBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public class Quadro03PanelExtension
extends Quadro03PanelBase
implements IBindablePanel<Quadro03> {
    public Quadro03PanelExtension(Quadro03 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro03 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro03Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
                this.setColumnSizes();
            }
        }
        this.disableReadOnlyFields();
    }

    private void setHeader() {
        TableColumnModel cm = this.getRostoq03DT1().getTableHeader().getColumnModel();
        ColumnGroup g_Dependentes = new ColumnGroup("Identifica\u00e7\u00e3o dos Dependentes");
        g_Dependentes.add(cm.getColumn(1));
        ColumnGroup g_Deficientes = new ColumnGroup("Deficientes");
        g_Deficientes.add(cm.getColumn(2));
        ColumnGroup g_OutroProgenitor = new ColumnGroup("Identifica\u00e7\u00e3o do Outro Progenitor");
        g_OutroProgenitor.add(cm.getColumn(3));
        if (this.getRostoq03DT1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getRostoq03DT1().getTableHeader()).addColumnGroup(g_Dependentes);
            ((GroupableTableHeader)this.getRostoq03DT1().getTableHeader()).addColumnGroup(g_Deficientes);
            ((GroupableTableHeader)this.getRostoq03DT1().getTableHeader()).addColumnGroup(g_OutroProgenitor);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_Dependentes);
        header.addColumnGroup(g_Deficientes);
        header.addColumnGroup(g_OutroProgenitor);
        this.getRostoq03DT1().setTableHeader(header);
    }

    private void setColumnSizes() {
        if (this.getRostoq03DT1().getColumnCount() >= 2) {
            this.getRostoq03DT1().getColumnModel().getColumn(2).setMaxWidth(100);
        }
    }

    protected void disableReadOnlyFields() {
        if (Session.isApplet()) {
            this.getQ03C01().setEnabled(false);
            this.getQ03C02().setEnabled(false);
            this.getQ03C03().setEnabled(false);
            this.getQ03C04().setEnabled(false);
        }
    }

    @Override
    protected void removeLineRostoq03DT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            boolean isLastRow = false;
            int lastRow = this.rostoq03DT1.getRowCount() - 1;
            int selectedRow = this.rostoq03DT1.getSelectedRow();
            if (selectedRow == -1) {
                selectedRow = lastRow;
                isLastRow = true;
            }
            if (selectedRow != -1) {
                Rostoq03DT1_Linha linha = this.model.getRostoq03DT1().get(selectedRow);
                this.model.getRostoq03DT1().remove(selectedRow);
                if (!(isLastRow && linha.getNIF() == null)) {
                    RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
                    rosto.getRostoq03DT1ChangeEvent().fireDeleteRow(selectedRow, linha.getNIF() == null, isLastRow);
                }
            }
        }
    }
}

