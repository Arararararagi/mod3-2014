/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro09Panel
extends JPanel {
    protected JPanel this2;
    protected JPanel panel5;
    protected JPanel panel2;
    protected JLabelTextFieldNumbering q09B1OP1_label;
    protected JRadioButton q09B1OP1;
    protected JLabelTextFieldNumbering q09B1OP2_label;
    protected JRadioButton q09B1OP2;
    protected JPanel panel3;
    protected JLabel q09C05_label;
    protected JLabel q09C05_label2;
    protected JLabelTextFieldNumbering q09C03_num;
    protected JDateTextField q09C03;
    protected JPanel this3;
    protected JPanel panel1;
    protected JLabel label1;
    protected JPanel panel4;
    protected JLabel q09C1_label;
    protected JLabelTextFieldNumbering label14;
    protected JDateTextField q09C1;
    protected JLabel label3;
    protected JLabel q09C2_label;
    protected JLabelTextFieldNumbering label15;
    protected JDateTextField q09C2;
    protected JLabel label5;
    protected JLabel q09C3_label;
    protected JLabelTextFieldNumbering label16;
    protected JDateTextField q09C3;
    protected JLabel label7;
    protected JPanel panel6;
    protected JLabelTextFieldNumbering label17;
    protected JRadioButton q09B2OP4;
    protected JLabelTextFieldNumbering label22;
    protected JRadioButton q09B2OP9;
    protected JLabelTextFieldNumbering label18;
    protected JRadioButton q09B2OP5;
    protected JLabelTextFieldNumbering label23;
    protected JRadioButton q09B2OP10;
    protected JLabelTextFieldNumbering label19;
    protected JRadioButton q09B2OP6;
    protected JLabelTextFieldNumbering label24;
    protected JRadioButton q09B2OP11;
    protected JLabelTextFieldNumbering label20;
    protected JRadioButton q09B2OP7;
    protected JLabelTextFieldNumbering label25;
    protected JRadioButton q09B2OP12;
    protected JLabelTextFieldNumbering label21;
    protected JRadioButton q09B2OP8;
    protected JLabelTextFieldNumbering label26;
    protected JRadioButton q09B2OP13;
    protected QuadroTitlePanel titlePanel;

    public Quadro09Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getQ09B1OP1_label() {
        return this.q09B1OP1_label;
    }

    public JLabelTextFieldNumbering getQ09B1OP2_label() {
        return this.q09B1OP2_label;
    }

    public JRadioButton getQ09B1OP1() {
        return this.q09B1OP1;
    }

    public JRadioButton getQ09B1OP2() {
        return this.q09B1OP2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getQ09C05_label() {
        return this.q09C05_label;
    }

    public JLabel getQ09C05_label2() {
        return this.q09C05_label2;
    }

    public JLabelTextFieldNumbering getQ09C03_num() {
        return this.q09C03_num;
    }

    public JDateTextField getQ09C03() {
        return this.q09C03;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis3() {
        return this.this3;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getQ09C1_label() {
        return this.q09C1_label;
    }

    public JLabelTextFieldNumbering getLabel14() {
        return this.label14;
    }

    public JDateTextField getQ09C1() {
        return this.q09C1;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabel getQ09C2_label() {
        return this.q09C2_label;
    }

    public JLabelTextFieldNumbering getLabel15() {
        return this.label15;
    }

    public JDateTextField getQ09C2() {
        return this.q09C2;
    }

    public JLabel getLabel5() {
        return this.label5;
    }

    public JLabel getQ09C3_label() {
        return this.q09C3_label;
    }

    public JLabelTextFieldNumbering getLabel16() {
        return this.label16;
    }

    public JDateTextField getQ09C3() {
        return this.q09C3;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabelTextFieldNumbering getLabel17() {
        return this.label17;
    }

    public JRadioButton getQ09B2OP4() {
        return this.q09B2OP4;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JRadioButton getQ09B2OP9() {
        return this.q09B2OP9;
    }

    public JLabelTextFieldNumbering getLabel18() {
        return this.label18;
    }

    public JRadioButton getQ09B2OP5() {
        return this.q09B2OP5;
    }

    public JLabelTextFieldNumbering getLabel23() {
        return this.label23;
    }

    public JRadioButton getQ09B2OP10() {
        return this.q09B2OP10;
    }

    public JLabelTextFieldNumbering getLabel19() {
        return this.label19;
    }

    public JRadioButton getQ09B2OP6() {
        return this.q09B2OP6;
    }

    public JLabelTextFieldNumbering getLabel24() {
        return this.label24;
    }

    public JRadioButton getQ09B2OP11() {
        return this.q09B2OP11;
    }

    public JLabelTextFieldNumbering getLabel20() {
        return this.label20;
    }

    public JRadioButton getQ09B2OP7() {
        return this.q09B2OP7;
    }

    public JLabelTextFieldNumbering getLabel25() {
        return this.label25;
    }

    public JRadioButton getQ09B2OP12() {
        return this.q09B2OP12;
    }

    public JLabelTextFieldNumbering getLabel21() {
        return this.label21;
    }

    public JRadioButton getQ09B2OP8() {
        return this.q09B2OP8;
    }

    public JLabelTextFieldNumbering getLabel26() {
        return this.label26;
    }

    public JRadioButton getQ09B2OP13() {
        return this.q09B2OP13;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.Rosto");
        this.this2 = new JPanel();
        this.panel5 = new JPanel();
        this.panel5.setVisible(Modelo3IRSv2015Parameters.instance().isNET());
        this.panel2 = new JPanel();
        this.q09B1OP1_label = new JLabelTextFieldNumbering();
        this.q09B1OP1 = new JRadioButton();
        this.q09B1OP2_label = new JLabelTextFieldNumbering();
        this.q09B1OP2 = new JRadioButton();
        this.panel3 = new JPanel();
        this.q09C05_label = new JLabel();
        this.q09C05_label2 = new JLabel();
        this.q09C03_num = new JLabelTextFieldNumbering();
        this.q09C03 = new JDateTextField();
        this.this3 = new JPanel();
        this.panel1 = new JPanel();
        this.panel1.setVisible(Modelo3IRSv2015Parameters.instance().isDC());
        this.label1 = new JLabel();
        this.panel4 = new JPanel();
        this.q09C1_label = new JLabel();
        this.label14 = new JLabelTextFieldNumbering();
        this.q09C1 = new JDateTextField();
        this.label3 = new JLabel();
        this.q09C2_label = new JLabel();
        this.label15 = new JLabelTextFieldNumbering();
        this.q09C2 = new JDateTextField();
        this.label5 = new JLabel();
        this.q09C3_label = new JLabel();
        this.label16 = new JLabelTextFieldNumbering();
        this.q09C3 = new JDateTextField();
        this.label7 = new JLabel();
        this.panel6 = new JPanel();
        this.label17 = new JLabelTextFieldNumbering();
        this.q09B2OP4 = new JRadioButton();
        this.label22 = new JLabelTextFieldNumbering();
        this.q09B2OP9 = new JRadioButton();
        this.label18 = new JLabelTextFieldNumbering();
        this.q09B2OP5 = new JRadioButton();
        this.label23 = new JLabelTextFieldNumbering();
        this.q09B2OP10 = new JRadioButton();
        this.label19 = new JLabelTextFieldNumbering();
        this.q09B2OP6 = new JRadioButton();
        this.label24 = new JLabelTextFieldNumbering();
        this.q09B2OP11 = new JRadioButton();
        this.label20 = new JLabelTextFieldNumbering();
        this.q09B2OP7 = new JRadioButton();
        this.label25 = new JLabelTextFieldNumbering();
        this.q09B2OP12 = new JRadioButton();
        this.label21 = new JLabelTextFieldNumbering();
        this.q09B2OP8 = new JRadioButton();
        this.label26 = new JLabelTextFieldNumbering();
        this.q09B2OP13 = new JRadioButton();
        this.titlePanel = new QuadroTitlePanel();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow, 0px", "2*($lgap, default)"));
        this.panel5.setLayout(new FormLayout("15dlu, default:grow", "default, $lgap, default"));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, 13dlu, 0px, default:grow, $rgap", "$rgap, 2*(default, $lgap), default"));
        this.q09B1OP1_label.setText(bundle.getString("Quadro09Panel.q09B1OP1_label.text"));
        this.q09B1OP1_label.setHorizontalAlignment(0);
        this.q09B1OP1_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.q09B1OP1_label, cc.xy(3, 2));
        this.q09B1OP1.setText(bundle.getString("Quadro09Panel.q09B1OP1.text"));
        this.panel2.add((Component)this.q09B1OP1, cc.xy(5, 2));
        this.q09B1OP2_label.setText(bundle.getString("Quadro09Panel.q09B1OP2_label.text"));
        this.q09B1OP2_label.setHorizontalAlignment(0);
        this.q09B1OP2_label.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.q09B1OP2_label, cc.xy(3, 4));
        this.q09B1OP2.setText(bundle.getString("Quadro09Panel.q09B1OP2.text"));
        this.panel2.add((Component)this.q09B1OP2, cc.xy(5, 4));
        this.panel5.add((Component)this.panel2, cc.xy(2, 1));
        this.panel3.setPreferredSize(null);
        this.panel3.setMinimumSize(null);
        this.panel3.setLayout(new FormLayout("default, $lcgap, 0px, 16dlu, $lcgap, default, $lcgap, 13dlu, 0px, $lcgap, 45dlu, $rgap, 10dlu:grow", "$ugap, 2*($lgap, default)"));
        this.q09C05_label.setText(bundle.getString("Quadro09Panel.q09C05_label.text"));
        this.q09C05_label.setHorizontalAlignment(4);
        this.q09C05_label.setHorizontalTextPosition(4);
        this.q09C05_label.setPreferredSize(null);
        this.q09C05_label.setMaximumSize(null);
        this.panel3.add((Component)this.q09C05_label, cc.xy(1, 3));
        this.q09C05_label2.setText(bundle.getString("Quadro09Panel.q09C05_label2.text"));
        this.q09C05_label2.setHorizontalAlignment(4);
        this.panel3.add((Component)this.q09C05_label2, cc.xy(6, 3));
        this.q09C03_num.setText(bundle.getString("Quadro09Panel.q09C03_num.text"));
        this.q09C03_num.setBorder(new EtchedBorder());
        this.q09C03_num.setHorizontalAlignment(0);
        this.panel3.add((Component)this.q09C03_num, cc.xy(8, 3));
        this.q09C03.setColumns(10);
        this.panel3.add((Component)this.q09C03, cc.xy(11, 3));
        this.panel5.add((Component)this.panel3, cc.xy(2, 3));
        this.this2.add((Component)this.panel5, cc.xy(3, 2));
        this.this3.setMinimumSize(null);
        this.this3.setPreferredSize(null);
        this.this3.setBorder(null);
        this.this3.setLayout(new FormLayout("$rgap, default:grow", "$rgap, default, $lgap, default"));
        this.panel1.setLayout(new FormLayout("default:grow, $lcgap, default, $lcgap, default:grow", "2*(default, $lgap), $ugap, $lgap, default"));
        this.label1.setText(bundle.getString("Quadro09Panel.label1.text"));
        this.panel1.add((Component)this.label1, cc.xywh(1, 1, 1, 1, CellConstraints.DEFAULT, CellConstraints.TOP));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("0px, $rgap, default, $ugap, 13dlu, 2*(0px), 49dlu, $rgap, default, 0px", "3*($lgap, default)"));
        this.q09C1_label.setText(bundle.getString("Quadro09Panel.q09C1_label.text"));
        this.panel4.add((Component)this.q09C1_label, cc.xy(3, 2));
        this.label14.setText(bundle.getString("Quadro09Panel.label14.text"));
        this.label14.setBorder(new EtchedBorder());
        this.label14.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label14, cc.xy(5, 2));
        this.q09C1.setColumns(10);
        this.panel4.add((Component)this.q09C1, cc.xy(8, 2));
        this.label3.setText(bundle.getString("Quadro09Panel.label3.text"));
        this.panel4.add((Component)this.label3, cc.xy(10, 2));
        this.q09C2_label.setText(bundle.getString("Quadro09Panel.q09C2_label.text"));
        this.panel4.add((Component)this.q09C2_label, cc.xy(3, 4));
        this.label15.setText(bundle.getString("Quadro09Panel.label15.text"));
        this.label15.setBorder(new EtchedBorder());
        this.label15.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label15, cc.xy(5, 4));
        this.q09C2.setColumns(10);
        this.panel4.add((Component)this.q09C2, cc.xy(8, 4));
        this.label5.setText(bundle.getString("Quadro09Panel.label5.text"));
        this.panel4.add((Component)this.label5, cc.xy(10, 4));
        this.q09C3_label.setText(bundle.getString("Quadro09Panel.q09C3_label.text"));
        this.panel4.add((Component)this.q09C3_label, cc.xy(3, 6));
        this.label16.setText(bundle.getString("Quadro09Panel.label16.text"));
        this.label16.setBorder(new EtchedBorder());
        this.label16.setHorizontalAlignment(0);
        this.panel4.add((Component)this.label16, cc.xy(5, 6));
        this.q09C3.setColumns(10);
        this.panel4.add((Component)this.q09C3, cc.xy(8, 6));
        this.label7.setText(bundle.getString("Quadro09Panel.label7.text"));
        this.panel4.add((Component)this.label7, cc.xy(10, 6));
        this.panel1.add((Component)this.panel4, cc.xy(3, 3));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("0px, $lcgap, 13dlu, default, $lcgap, 13dlu, default, $ugap, 3*(0px)", "6*($lgap, default)"));
        this.label17.setText(bundle.getString("Quadro09Panel.label17.text"));
        this.label17.setBorder(new EtchedBorder());
        this.label17.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label17, cc.xy(3, 2));
        this.q09B2OP4.setText(bundle.getString("Quadro09Panel.q09B2OP4.text"));
        this.panel6.add((Component)this.q09B2OP4, cc.xy(4, 2));
        this.label22.setText(bundle.getString("Quadro09Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label22, cc.xy(6, 2));
        this.q09B2OP9.setText(bundle.getString("Quadro09Panel.q09B2OP9.text"));
        this.panel6.add((Component)this.q09B2OP9, cc.xy(7, 2));
        this.label18.setText(bundle.getString("Quadro09Panel.label18.text"));
        this.label18.setBorder(new EtchedBorder());
        this.label18.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label18, cc.xy(3, 4));
        this.q09B2OP5.setText(bundle.getString("Quadro09Panel.q09B2OP5.text"));
        this.panel6.add((Component)this.q09B2OP5, cc.xy(4, 4));
        this.label23.setText(bundle.getString("Quadro09Panel.label23.text"));
        this.label23.setBorder(new EtchedBorder());
        this.label23.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label23, cc.xy(6, 4));
        this.q09B2OP10.setText(bundle.getString("Quadro09Panel.q09B2OP10.text"));
        this.panel6.add((Component)this.q09B2OP10, cc.xy(7, 4));
        this.label19.setText(bundle.getString("Quadro09Panel.label19.text"));
        this.label19.setBorder(new EtchedBorder());
        this.label19.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label19, cc.xy(3, 6));
        this.q09B2OP6.setText(bundle.getString("Quadro09Panel.q09B2OP6.text"));
        this.panel6.add((Component)this.q09B2OP6, cc.xy(4, 6));
        this.label24.setText(bundle.getString("Quadro09Panel.label24.text"));
        this.label24.setBorder(new EtchedBorder());
        this.label24.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label24, cc.xy(6, 6));
        this.q09B2OP11.setText(bundle.getString("Quadro09Panel.q09B2OP11.text"));
        this.panel6.add((Component)this.q09B2OP11, cc.xy(7, 6));
        this.label20.setText(bundle.getString("Quadro09Panel.label20.text"));
        this.label20.setBorder(new EtchedBorder());
        this.label20.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label20, cc.xy(3, 8));
        this.q09B2OP7.setText(bundle.getString("Quadro09Panel.q09B2OP7.text"));
        this.panel6.add((Component)this.q09B2OP7, cc.xy(4, 8));
        this.label25.setText(bundle.getString("Quadro09Panel.label25.text"));
        this.label25.setBorder(new EtchedBorder());
        this.label25.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label25, cc.xy(6, 8));
        this.q09B2OP12.setText(bundle.getString("Quadro09Panel.q09B2OP12.text"));
        this.panel6.add((Component)this.q09B2OP12, cc.xy(7, 8));
        this.label21.setText(bundle.getString("Quadro09Panel.label21.text"));
        this.label21.setBorder(new EtchedBorder());
        this.label21.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label21, cc.xy(3, 10));
        this.q09B2OP8.setText(bundle.getString("Quadro09Panel.q09B2OP8.text"));
        this.panel6.add((Component)this.q09B2OP8, cc.xy(4, 10));
        this.label26.setText(bundle.getString("Quadro09Panel.label26.text"));
        this.label26.setBorder(new EtchedBorder());
        this.label26.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label26, cc.xy(6, 10));
        this.q09B2OP13.setText(bundle.getString("Quadro09Panel.q09B2OP13.text"));
        this.panel6.add((Component)this.q09B2OP13, cc.xy(7, 10));
        this.panel1.add((Component)this.panel6, cc.xy(3, 7));
        this.this3.add((Component)this.panel1, cc.xy(2, 2));
        this.this2.add((Component)this.this3, cc.xy(3, 4));
        this.add((Component)this.this2, "Center");
        this.titlePanel.setNumber(bundle.getString("Quadro09Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro09Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
    }
}

