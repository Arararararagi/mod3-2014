/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.actions.file.Modelo3IRSv2015NewFileAction;
import pt.dgci.modelo3irs.v2015.listeners.AutomaticPasswordFillingDocumentListener;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS0Panel;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS1Dialog;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS1Panel;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS2Dialog;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS2Panel;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedOpenFileAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class QuadroAS0Dialog
extends JDialog {
    private QuadroAS1Dialog quadroAS1;
    private QuadroAS2Dialog quadroAS2;
    private QuadroAS0Panel quadroAS0Panel1;
    private JPanel panel1;
    private JButton qAS0B2;
    private JButton qAS0B3;

    public QuadroAS0Dialog(Frame owner) {
        super(owner);
        this.initComponents();
        this.keyListener();
        this.setDescriptions();
    }

    public QuadroAS0Dialog(Dialog owner) {
        super(owner);
        this.initComponents();
        this.keyListener();
        this.setDescriptions();
    }

    private void setDescriptions() {
        if (Session.isApplet() || Modelo3IRSv2015Parameters.instance().isAppletOffline()) {
            this.quadroAS0Panel1.getQAS0B1OP1().setText("<html>Obten\u00e7\u00e3o de uma declara\u00e7\u00e3o pr\u00e9-preenchida.</html>");
            this.quadroAS0Panel1.getQAS0B1OP3().setText("<html>Obten\u00e7\u00e3o da \u00faltima declara\u00e7\u00e3o submetida.</html>");
        } else {
            this.quadroAS0Panel1.getQAS0B1OP1().setText("<html>Obten\u00e7\u00e3o de uma declara\u00e7\u00e3o pr\u00e9-preenchida (requer acesso \u00e0 internet).</html>");
            this.quadroAS0Panel1.getQAS0B1OP3().setText("<html>Obten\u00e7\u00e3o da \u00faltima declara\u00e7\u00e3o submetida (requer acesso \u00e0 internet).</html>");
        }
    }

    private void qAS0B3ActionPerformed(ActionEvent e) {
        if (this.quadroAS1 != null) {
            this.quadroAS1.dispose();
        }
        this.dispose();
    }

    private void qAS0B3OP4ActionPerformed(ActionEvent e) {
        DeclaracaoModel currentDeclaracao = Session.getCurrentDeclaracao().getDeclaracaoModel();
        currentDeclaracao.resetModel();
        Modelo3IRSv2015NewFileAction t = new Modelo3IRSv2015NewFileAction();
        t.getDeclaracaoInicializada();
        if (this.quadroAS1 != null) {
            this.quadroAS1.dispose();
        }
        this.dispose();
    }

    private void qAS0B2ActionPerformed(ActionEvent e) {
        if (this.quadroAS0Panel1.getQAS0B1OP1().isSelected()) {
            if (this.quadroAS1 == null) {
                this.quadroAS1 = new QuadroAS1Dialog((Frame)this.getOwner(), this);
                if (Modelo3IRSv2015Parameters.instance().isAutomaticPasswordFillingEnabled()) {
                    this.quadroAS1.getQuadroAS1Panel1().getQAS1C2().getDocument().addDocumentListener(new AutomaticPasswordFillingDocumentListener(this.quadroAS1.getQuadroAS1Panel1().getQAS1C3()));
                    this.quadroAS1.getQuadroAS1Panel1().getQAS1C4().getDocument().addDocumentListener(new AutomaticPasswordFillingDocumentListener(this.quadroAS1.getQuadroAS1Panel1().getQAS1C5()));
                }
            }
            this.setModal(false);
            this.setVisible(false);
            this.quadroAS1.setVisible(true);
        } else if (this.quadroAS0Panel1.getQAS0B1OP2().isSelected()) {
            TaxClientRevampedOpenFileAction openFileAction = (TaxClientRevampedOpenFileAction)ActionsTaxClientManager.getAction("Abrir");
            this.qAS0B3ActionPerformed(e);
            openFileAction.actionPerformed(e, false);
        } else if (this.quadroAS0Panel1.getQAS0B1OP3().isSelected()) {
            if (this.quadroAS2 == null) {
                this.quadroAS2 = new QuadroAS2Dialog((Frame)this.getOwner(), this);
                if (Modelo3IRSv2015Parameters.instance().isAutomaticPasswordFillingEnabled()) {
                    this.quadroAS2.getQuadroAS2Panel1().getQAS1C2().getDocument().addDocumentListener(new AutomaticPasswordFillingDocumentListener(this.quadroAS2.getQuadroAS2Panel1().getQAS1C3()));
                    this.quadroAS2.getQuadroAS2Panel1().getQAS1C4().getDocument().addDocumentListener(new AutomaticPasswordFillingDocumentListener(this.quadroAS2.getQuadroAS2Panel1().getQAS1C5()));
                }
            }
            this.setModal(false);
            this.setVisible(false);
            this.quadroAS2.setVisible(true);
        } else if (this.quadroAS0Panel1.getQAS0B1OP4().isSelected()) {
            this.qAS0B3OP4ActionPerformed(e);
        } else {
            DialogFactory.instance().showErrorDialog("Por favor seleccione uma das op\u00e7\u00f5es ou cancele no bot\u00e3o Cancelar.");
        }
    }

    private void initComponents() {
        this.quadroAS0Panel1 = new QuadroAS0Panel();
        this.panel1 = new JPanel();
        this.qAS0B2 = new JButton();
        this.qAS0B3 = new JButton();
        CellConstraints cc = new CellConstraints();
        this.setTitle("Assistente de preenchimento");
        this.setMinimumSize(new Dimension(16, 50));
        this.setResizable(false);
        this.setModal(true);
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new FormLayout("$rgap, $lcgap, default, 2*($lcgap)", "$lgap, $rgap, 101dlu, $lgap, $rgap, $lgap, default, $lgap, $rgap"));
        contentPane.add((Component)this.quadroAS0Panel1, cc.xywh(3, 3, 1, 2));
        this.panel1.setLayout(new FormLayout("default:grow, $lcgap, default, $ugap, default, $lcgap, default:grow", "default"));
        this.qAS0B2.setText("Continuar \u00bb");
        this.qAS0B2.setPreferredSize(new Dimension(120, 22));
        this.qAS0B2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                QuadroAS0Dialog.this.qAS0B2ActionPerformed(e);
            }
        });
        this.panel1.add((Component)this.qAS0B2, cc.xy(3, 1));
        this.qAS0B3.setText("Cancelar");
        this.qAS0B3.setPreferredSize(new Dimension(120, 22));
        this.qAS0B3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                QuadroAS0Dialog.this.qAS0B3ActionPerformed(e);
            }
        });
        this.panel1.add((Component)this.qAS0B3, cc.xy(5, 1));
        contentPane.add((Component)this.panel1, cc.xy(3, 7));
        this.pack();
        this.setLocationRelativeTo(this.getOwner());
    }

    private void keyListener() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher(){

            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
                ActionEvent actionEvent = new ActionEvent(this, 1001, KeyEvent.getKeyText(e.getKeyCode()));
                if (e.getKeyCode() == 27) {
                    QuadroAS0Dialog.this.qAS0B3ActionPerformed(actionEvent);
                } else if (e.getKeyCode() == 10 && QuadroAS0Dialog.this.isActive() && e.getID() == 401) {
                    QuadroAS0Dialog.this.qAS0B2ActionPerformed(actionEvent);
                }
                boolean discardEvent = false;
                return discardEvent;
            }
        });
    }

}

