/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabelTextFieldNumbering label2;
    protected JRadioButton q06B1OP1;
    protected JLabelTextFieldNumbering label3;
    protected JRadioButton q06B1OP2;
    protected JLabelTextFieldNumbering label4;
    protected JRadioButton q06B1OP3;
    protected JLabelTextFieldNumbering label5;
    protected JRadioButton q06B1OP4;

    public Quadro06Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getLabel2() {
        return this.label2;
    }

    public JRadioButton getQ06B1OP1() {
        return this.q06B1OP1;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JRadioButton getQ06B1OP2() {
        return this.q06B1OP2;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JRadioButton getQ06B1OP3() {
        return this.q06B1OP3;
    }

    public JLabelTextFieldNumbering getLabel5() {
        return this.label5;
    }

    public JRadioButton getQ06B1OP4() {
        return this.q06B1OP4;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.Rosto");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.label2 = new JLabelTextFieldNumbering();
        this.q06B1OP1 = new JRadioButton();
        this.label3 = new JLabelTextFieldNumbering();
        this.q06B1OP2 = new JRadioButton();
        this.label4 = new JLabelTextFieldNumbering();
        this.q06B1OP3 = new JRadioButton();
        this.label5 = new JLabelTextFieldNumbering();
        this.q06B1OP4 = new JRadioButton();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, 0px", "$rgap, default, $lgap, default"));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $rgap, 13dlu, 0px, default, $rgap, default:grow", "4*(default, $lgap), default"));
        this.label2.setText(bundle.getString("Quadro06Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label2, cc.xy(3, 1));
        this.q06B1OP1.setText(bundle.getString("Quadro06Panel.q06B1OP1.text"));
        this.panel2.add((Component)this.q06B1OP1, cc.xy(5, 1));
        this.label3.setText(bundle.getString("Quadro06Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label3, cc.xy(3, 3));
        this.q06B1OP2.setText(bundle.getString("Quadro06Panel.q06B1OP2.text"));
        this.panel2.add((Component)this.q06B1OP2, cc.xy(5, 3));
        this.label4.setText(bundle.getString("Quadro06Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label4, cc.xy(3, 5));
        this.q06B1OP3.setText(bundle.getString("Quadro06Panel.q06B1OP3.text"));
        this.panel2.add((Component)this.q06B1OP3, cc.xy(5, 5));
        this.label5.setText(bundle.getString("Quadro06Panel.label5.text"));
        this.label5.setHorizontalAlignment(0);
        this.label5.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label5, cc.xy(3, 7));
        this.q06B1OP4.setText(bundle.getString("Quadro06Panel.q06B1OP4.text"));
        this.panel2.add((Component)this.q06B1OP4, cc.xy(5, 7));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.add((Component)this.this2, "Center");
    }
}

