/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JLimitedTextField;

public class Quadro04Panel
extends JPanel {
    protected JPanel this2;
    protected JPanel panel5;
    protected JPanel panel2;
    protected JLabelTextFieldNumbering label2;
    protected JRadioButton q04B1OP1;
    protected JLabelTextFieldNumbering label3;
    protected JRadioButton q04B1OP2;
    protected JPanel panel1;
    protected JPanel panel6;
    protected JLabelTextFieldNumbering label6;
    protected JRadioButton q04B3OP3;
    protected JLabelTextFieldNumbering label8;
    protected JRadioButton q04B3OP5;
    protected JLabelTextFieldNumbering label9;
    protected JRadioButton q04B3OP6;
    protected JLabelTextFieldNumbering label12;
    protected JRadioButton q04B3OP7;
    protected JLabelTextFieldNumbering label13;
    protected JRadioButton q04B3OP8;
    protected JLabelTextFieldNumbering label20;
    protected JRadioButton q04B3OP9;
    protected JLabel label10;
    protected JLabel label11;
    protected JPanel panel4;
    protected JLabel codSF_label;
    protected JLimitedTextField codSF;
    protected JLabel codDirFinancas_label;
    protected JLimitedTextField codDirFinancas;
    protected JLabel numLote_label;
    protected JLimitedTextField numLote;
    protected JLabel numDecl_label;
    protected JLimitedTextField numDecl;
    protected JLabel label15;
    protected JPanel panel9;
    protected JPanel panel7;
    protected JLabel dataRecepcao_label;
    protected JDateTextField dataRecepcao;
    protected JPanel panel8;
    protected JLabelTextFieldNumbering label16;
    protected JRadioButton q04B4OP2;
    protected JLabelTextFieldNumbering label17;
    protected JRadioButton q04B4OP3;
    protected JLabelTextFieldNumbering label18;
    protected JRadioButton q04B4OP4;
    protected QuadroTitlePanel titlePanel;

    public Quadro04Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getLabel2() {
        return this.label2;
    }

    public JRadioButton getQ04B1OP1() {
        return this.q04B1OP1;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JRadioButton getQ04B1OP2() {
        return this.q04B1OP2;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getCodSF_label() {
        return this.codSF_label;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabelTextFieldNumbering getLabel6() {
        return this.label6;
    }

    public JRadioButton getQ04B3OP3() {
        return this.q04B3OP3;
    }

    public JLabelTextFieldNumbering getLabel8() {
        return this.label8;
    }

    public JRadioButton getQ04B3OP5() {
        return this.q04B3OP5;
    }

    public JLabelTextFieldNumbering getLabel9() {
        return this.label9;
    }

    public JRadioButton getQ04B3OP6() {
        return this.q04B3OP6;
    }

    public JLabelTextFieldNumbering getLabel12() {
        return this.label12;
    }

    public JRadioButton getQ04B3OP7() {
        return this.q04B3OP7;
    }

    public JLabelTextFieldNumbering getLabel13() {
        return this.label13;
    }

    public JRadioButton getQ04B3OP8() {
        return this.q04B3OP8;
    }

    public JLabel getCodDirFinancas_label() {
        return this.codDirFinancas_label;
    }

    public JLimitedTextField getCodDirFinancas() {
        return this.codDirFinancas;
    }

    public JLabel getNumLote_label() {
        return this.numLote_label;
    }

    public JLimitedTextField getNumLote() {
        return this.numLote;
    }

    public JLabel getNumDecl_label() {
        return this.numDecl_label;
    }

    public JLimitedTextField getNumDecl() {
        return this.numDecl;
    }

    public JLimitedTextField getCodSF() {
        return this.codSF;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getLabel15() {
        return this.label15;
    }

    public JPanel getPanel8() {
        return this.panel8;
    }

    public JLabelTextFieldNumbering getLabel16() {
        return this.label16;
    }

    public JRadioButton getQ04B4OP2() {
        return this.q04B4OP2;
    }

    public JLabelTextFieldNumbering getLabel17() {
        return this.label17;
    }

    public JRadioButton getQ04B4OP3() {
        return this.q04B4OP3;
    }

    public JLabelTextFieldNumbering getLabel18() {
        return this.label18;
    }

    public JRadioButton getQ04B4OP4() {
        return this.q04B4OP4;
    }

    public JLabel getDataRecepcao_label() {
        return this.dataRecepcao_label;
    }

    public JDateTextField getDataRecepcao() {
        return this.dataRecepcao;
    }

    public JPanel getPanel9() {
        return this.panel9;
    }

    public JLabelTextFieldNumbering getLabel20() {
        return this.label20;
    }

    public JRadioButton getQ04B3OP9() {
        return this.q04B3OP9;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.Rosto");
        this.this2 = new JPanel();
        this.panel5 = new JPanel();
        this.panel5.setVisible(Modelo3IRSv2015Parameters.instance().isNET());
        this.panel2 = new JPanel();
        this.label2 = new JLabelTextFieldNumbering();
        this.q04B1OP1 = new JRadioButton();
        this.label3 = new JLabelTextFieldNumbering();
        this.q04B1OP2 = new JRadioButton();
        this.panel1 = new JPanel();
        this.panel1.setVisible(Modelo3IRSv2015Parameters.instance().isDC());
        this.panel6 = new JPanel();
        this.label6 = new JLabelTextFieldNumbering();
        this.q04B3OP3 = new JRadioButton();
        this.label8 = new JLabelTextFieldNumbering();
        this.q04B3OP5 = new JRadioButton();
        this.label9 = new JLabelTextFieldNumbering();
        this.q04B3OP6 = new JRadioButton();
        this.label12 = new JLabelTextFieldNumbering();
        this.q04B3OP7 = new JRadioButton();
        this.label13 = new JLabelTextFieldNumbering();
        this.q04B3OP8 = new JRadioButton();
        this.label20 = new JLabelTextFieldNumbering();
        this.q04B3OP9 = new JRadioButton();
        this.label10 = new JLabel();
        this.label11 = new JLabel();
        this.panel4 = new JPanel();
        this.codSF_label = new JLabel();
        this.codSF = new JLimitedTextField();
        this.codDirFinancas_label = new JLabel();
        this.codDirFinancas = new JLimitedTextField();
        this.numLote_label = new JLabel();
        this.numLote = new JLimitedTextField();
        this.numDecl_label = new JLabel();
        this.numDecl = new JLimitedTextField();
        this.label15 = new JLabel();
        this.panel9 = new JPanel();
        this.panel7 = new JPanel();
        this.dataRecepcao_label = new JLabel();
        this.dataRecepcao = new JDateTextField();
        this.panel8 = new JPanel();
        this.label16 = new JLabelTextFieldNumbering();
        this.q04B4OP2 = new JRadioButton();
        this.label17 = new JLabelTextFieldNumbering();
        this.q04B4OP3 = new JRadioButton();
        this.label18 = new JLabelTextFieldNumbering();
        this.q04B4OP4 = new JRadioButton();
        this.titlePanel = new QuadroTitlePanel();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow, 0px", "3*($lgap, default)"));
        this.panel5.setLayout(new FormLayout("15dlu, default:grow", "default"));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $lcgap, 13dlu, 0px, default:grow, $rgap", "$rgap, default, $lgap, default"));
        this.label2.setText(bundle.getString("Quadro04Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label2, CC.xy(3, 2));
        this.q04B1OP1.setText(bundle.getString("Quadro04Panel.q04B1OP1.text"));
        this.panel2.add((Component)this.q04B1OP1, CC.xy(5, 2));
        this.label3.setText(bundle.getString("Quadro04Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label3, CC.xy(3, 4));
        this.q04B1OP2.setText(bundle.getString("Quadro04Panel.q04B1OP2.text"));
        this.panel2.add((Component)this.q04B1OP2, CC.xy(5, 4));
        this.panel5.add((Component)this.panel2, CC.xy(2, 1));
        this.this2.add((Component)this.panel5, CC.xy(3, 2));
        this.panel1.setLayout(new FormLayout("15dlu, default:grow", "4*(default, $lgap), default"));
        this.panel6.setPreferredSize(null);
        this.panel6.setMinimumSize(null);
        this.panel6.setLayout(new FormLayout("default:grow, $lcgap, 13dlu, 0px, default:grow, $rgap", "$rgap, 5*(default, $lgap), default"));
        this.label6.setText(bundle.getString("Quadro04Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.label6.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label6, CC.xy(3, 2));
        this.q04B3OP3.setText(bundle.getString("Quadro04Panel.q04B3OP3.text"));
        this.panel6.add((Component)this.q04B3OP3, CC.xy(5, 2));
        this.label8.setText(bundle.getString("Quadro04Panel.label8.text"));
        this.label8.setHorizontalAlignment(0);
        this.label8.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label8, CC.xy(3, 4));
        this.q04B3OP5.setText(bundle.getString("Quadro04Panel.q04B3OP5.text"));
        this.panel6.add((Component)this.q04B3OP5, CC.xy(5, 4));
        this.label9.setText(bundle.getString("Quadro04Panel.label9.text"));
        this.label9.setHorizontalAlignment(0);
        this.label9.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label9, CC.xy(3, 6));
        this.q04B3OP6.setText(bundle.getString("Quadro04Panel.q04B3OP6.text"));
        this.panel6.add((Component)this.q04B3OP6, CC.xy(5, 6));
        this.label12.setText(bundle.getString("Quadro04Panel.label12.text"));
        this.label12.setHorizontalAlignment(0);
        this.label12.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label12, CC.xy(3, 8));
        this.q04B3OP7.setText(bundle.getString("Quadro04Panel.q04B3OP7.text"));
        this.panel6.add((Component)this.q04B3OP7, CC.xy(5, 8));
        this.label13.setText(bundle.getString("Quadro04Panel.label13.text"));
        this.label13.setHorizontalAlignment(0);
        this.label13.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label13, CC.xy(3, 10));
        this.q04B3OP8.setText(bundle.getString("Quadro04Panel.q04B3OP8.text"));
        this.panel6.add((Component)this.q04B3OP8, CC.xy(5, 10));
        this.label20.setText(bundle.getString("Quadro04Panel.label20.text"));
        this.label20.setHorizontalAlignment(0);
        this.label20.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label20, CC.xy(3, 12));
        this.q04B3OP9.setText(bundle.getString("Quadro04Panel.q04B3OP9.text"));
        this.panel6.add((Component)this.q04B3OP9, CC.xy(5, 12));
        this.panel1.add((Component)this.panel6, CC.xy(2, 1));
        this.label10.setText(bundle.getString("Quadro04Panel.label10.text"));
        this.label10.setHorizontalAlignment(0);
        this.label10.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.label10, CC.xy(1, 3));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.label11.setText(bundle.getString("Quadro04Panel.label11.text"));
        this.panel1.add((Component)this.label11, CC.xy(2, 3));
        this.panel4.setPreferredSize(null);
        this.panel4.setMinimumSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, 0px, $lcgap, default, 0px, $lcgap, 22dlu, $rgap, 10dlu:grow", "$ugap, 5*($lgap, default)"));
        this.codSF_label.setText(bundle.getString("Quadro04Panel.codSF_label.text"));
        this.codSF_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.codSF_label, CC.xy(4, 3));
        this.codSF.setMaxLength(4);
        this.codSF.setColumns(4);
        this.codSF.setHorizontalAlignment(4);
        this.panel4.add((Component)this.codSF, CC.xy(7, 3));
        this.codDirFinancas_label.setText(bundle.getString("Quadro04Panel.codDirFinancas_label.text"));
        this.codDirFinancas_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.codDirFinancas_label, CC.xy(4, 5));
        this.codDirFinancas.setMaxLength(2);
        this.codDirFinancas.setHorizontalAlignment(4);
        this.codDirFinancas.setColumns(5);
        this.panel4.add((Component)this.codDirFinancas, CC.xy(7, 5));
        this.numLote_label.setText(bundle.getString("Quadro04Panel.numLote_label.text"));
        this.numLote_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.numLote_label, CC.xy(4, 7));
        this.numLote.setHorizontalAlignment(4);
        this.numLote.setMaxLength(5);
        this.panel4.add((Component)this.numLote, CC.xy(7, 7));
        this.numDecl_label.setText(bundle.getString("Quadro04Panel.numDecl_label.text"));
        this.numDecl_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.numDecl_label, CC.xy(4, 9));
        this.numDecl.setHorizontalAlignment(4);
        this.numDecl.setMaxLength(2);
        this.panel4.add((Component)this.numDecl, CC.xy(7, 9));
        this.panel1.add((Component)this.panel4, CC.xy(2, 5));
        this.label15.setBorder(new EtchedBorder());
        this.label15.setHorizontalAlignment(0);
        this.label15.setText(bundle.getString("Quadro04Panel.label15.text"));
        this.panel1.add((Component)this.label15, CC.xywh(1, 7, 2, 1));
        this.panel9.setLayout(new FormLayout("default:grow, $lcgap, default, $lcgap, default:grow", "default, $lgap, default"));
        this.panel7.setPreferredSize(null);
        this.panel7.setMinimumSize(null);
        this.panel7.setLayout(new FormLayout("0px, default, $lcgap, 0px, 49dlu", "default"));
        this.dataRecepcao_label.setText(bundle.getString("Quadro04Panel.dataRecepcao_label.text"));
        this.dataRecepcao_label.setHorizontalAlignment(4);
        this.panel7.add((Component)this.dataRecepcao_label, CC.xy(2, 1));
        this.dataRecepcao.setColumns(10);
        this.panel7.add((Component)this.dataRecepcao, CC.xy(5, 1));
        this.panel9.add((Component)this.panel7, CC.xy(3, 1));
        this.panel8.setPreferredSize(null);
        this.panel8.setMinimumSize(null);
        this.panel8.setLayout(new FormLayout("13dlu, 0px, default:grow, $rgap", "$rgap, 3*(default, $lgap), default"));
        this.label16.setText(bundle.getString("Quadro04Panel.label16.text"));
        this.label16.setHorizontalAlignment(0);
        this.label16.setBorder(new EtchedBorder());
        this.panel8.add((Component)this.label16, CC.xy(1, 2));
        this.q04B4OP2.setText(bundle.getString("Quadro04Panel.q04B4OP2.text"));
        this.panel8.add((Component)this.q04B4OP2, CC.xy(3, 2));
        this.label17.setText(bundle.getString("Quadro04Panel.label17.text"));
        this.label17.setHorizontalAlignment(0);
        this.label17.setBorder(new EtchedBorder());
        this.panel8.add((Component)this.label17, CC.xy(1, 4));
        this.q04B4OP3.setText(bundle.getString("Quadro04Panel.q04B4OP3.text"));
        this.panel8.add((Component)this.q04B4OP3, CC.xy(3, 4));
        this.label18.setText(bundle.getString("Quadro04Panel.label18.text"));
        this.label18.setHorizontalAlignment(0);
        this.label18.setBorder(new EtchedBorder());
        this.panel8.add((Component)this.label18, CC.xy(1, 6));
        this.q04B4OP4.setText(bundle.getString("Quadro04Panel.q04B4OP4.text"));
        this.panel8.add((Component)this.q04B4OP4, CC.xy(3, 6));
        this.panel9.add((Component)this.panel8, CC.xy(3, 3));
        this.panel1.add((Component)this.panel9, CC.xy(2, 9));
        this.this2.add((Component)this.panel1, CC.xy(3, 4));
        this.add((Component)this.this2, "Center");
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
    }
}

