/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.rosto;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro07Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro07PanelBase
extends Quadro07Panel
implements IBindablePanel<Quadro07> {
    private static final long serialVersionUID = 1;
    protected Quadro07 model;

    @Override
    public abstract void setModel(Quadro07 var1, boolean var2);

    @Override
    public Quadro07 getModel() {
        return this.model;
    }

    @Override
    protected void addLineRostoq07ET1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getRostoq07ET1().add(new Rostoq07ET1_Linha());
        }
    }

    @Override
    protected void removeLineRostoq07ET1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.rostoq07ET1.getSelectedRow() != -1 ? this.rostoq07ET1.getSelectedRow() : this.rostoq07ET1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getRostoq07ET1().remove(selectedRow);
            }
        }
    }

    @Override
    protected void addLineRostoq07CT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getRostoq07CT1().add(new Rostoq07CT1_Linha());
        }
    }

    @Override
    protected void removeLineRostoq07CT1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.rostoq07CT1.getSelectedRow() != -1 ? this.rostoq07CT1.getSelectedRow() : this.rostoq07CT1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getRostoq07CT1().remove(selectedRow);
            }
        }
    }

    public Quadro07PanelBase(Quadro07 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

