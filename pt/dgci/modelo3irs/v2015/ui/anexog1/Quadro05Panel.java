/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog1;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel3;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoG1q05T1Scroll;
    protected JTable anexoG1q05T1;
    protected JPanel panel4;
    protected JLabel anexoG1q05C1_label;
    protected JLabel anexoG1q05C2_label;
    protected JLabel anexoG1q05C1_base_label\u00a3anexoG1q05C2_base_label;
    protected JMoneyTextField anexoG1q05C1;
    protected JMoneyTextField anexoG1q05C2;

    public Quadro05Panel() {
        this.initComponents();
    }

    protected void addLineAnexoG1q05T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoG1q05T1_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoG1q05T1Scroll() {
        return this.anexoG1q05T1Scroll;
    }

    public JTable getAnexoG1q05T1() {
        return this.anexoG1q05T1;
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JLabel getAnexoG1q05C1_label() {
        return this.anexoG1q05C1_label;
    }

    public JLabel getAnexoG1q05C2_label() {
        return this.anexoG1q05C2_label;
    }

    public JLabel getAnexoG1q05C1_base_label\u00a3anexoG1q05C2_base_label() {
        return this.anexoG1q05C1_base_label\u00a3anexoG1q05C2_base_label;
    }

    public JMoneyTextField getAnexoG1q05C1() {
        return this.anexoG1q05C1;
    }

    public JMoneyTextField getAnexoG1q05C2() {
        return this.anexoG1q05C2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoG1");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel3 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoG1q05T1Scroll = new JScrollPane();
        this.anexoG1q05T1 = new JTable();
        this.panel4 = new JPanel();
        this.anexoG1q05C1_label = new JLabel();
        this.anexoG1q05C2_label = new JLabel();
        this.anexoG1q05C1_base_label\u00a3anexoG1q05C2_base_label = new JLabel();
        this.anexoG1q05C1 = new JMoneyTextField();
        this.anexoG1q05C2 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 3*(default, $lgap), default"));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $lcgap, 575dlu, $rgap, default:grow", "$rgap, default, $lgap, fill:255dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro05Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.addLineAnexoG1q05T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro05Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro05Panel.this.removeLineAnexoG1q05T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel3.add((Component)this.toolBar1, cc.xy(3, 2));
        this.anexoG1q05T1Scroll.setViewportView(this.anexoG1q05T1);
        this.panel3.add((Component)this.anexoG1q05T1Scroll, cc.xywh(3, 4, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, $lcgap, default, 2*($lcgap, 75dlu), $lcgap, default:grow", "2*(default, $lgap), default"));
        this.anexoG1q05C1_label.setText(bundle.getString("Quadro05Panel.anexoG1q05C1_label.text"));
        this.anexoG1q05C1_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoG1q05C1_label, cc.xywh(5, 1, 2, 1));
        this.anexoG1q05C2_label.setText(bundle.getString("Quadro05Panel.anexoG1q05C2_label.text"));
        this.anexoG1q05C2_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoG1q05C2_label, cc.xy(7, 1));
        this.anexoG1q05C1_base_label\u00a3anexoG1q05C2_base_label.setText(bundle.getString("Quadro05Panel.anexoG1q05C1_base_label\u00a3anexoG1q05C2_base_label.text"));
        this.panel4.add((Component)this.anexoG1q05C1_base_label\u00a3anexoG1q05C2_base_label, cc.xy(3, 3));
        this.anexoG1q05C1.setEditable(false);
        this.anexoG1q05C1.setColumns(15);
        this.panel4.add((Component)this.anexoG1q05C1, cc.xy(5, 3));
        this.anexoG1q05C2.setEditable(false);
        this.anexoG1q05C2.setColumns(15);
        this.panel4.add((Component)this.anexoG1q05C2, cc.xy(7, 3));
        this.this2.add((Component)this.panel4, cc.xy(2, 6));
        this.add((Component)this.this2, "Center");
    }

}

