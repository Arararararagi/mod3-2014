/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog1;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog1.Quadro05Bindings;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro05PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro05PanelExtension
extends Quadro05PanelBase
implements IBindablePanel<Quadro05> {
    public static final int MAX_LINES_Q05_T1 = 40;

    public Quadro05PanelExtension(Quadro05 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro05 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro05Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoG1q05T1().getTableHeader().getColumnModel();
        ColumnGroup g_IdMatricial = new ColumnGroup("Identifica\u00e7\u00e3o Matricial");
        g_IdMatricial.add(cm.getColumn(1));
        g_IdMatricial.add(cm.getColumn(2));
        g_IdMatricial.add(cm.getColumn(3));
        g_IdMatricial.add(cm.getColumn(4));
        ColumnGroup g_Aquisicao = new ColumnGroup("Data de Aquisi\u00e7\u00e3o");
        g_Aquisicao.add(cm.getColumn(6));
        g_Aquisicao.add(cm.getColumn(7));
        g_Aquisicao.add(cm.getColumn(8));
        ColumnGroup g_Valor = new ColumnGroup("Valor");
        g_Valor.add(cm.getColumn(9));
        g_Valor.add(cm.getColumn(10));
        if (this.getAnexoG1q05T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoG1q05T1().getTableHeader()).addColumnGroup(g_IdMatricial);
            ((GroupableTableHeader)this.getAnexoG1q05T1().getTableHeader()).addColumnGroup(g_Aquisicao);
            ((GroupableTableHeader)this.getAnexoG1q05T1().getTableHeader()).addColumnGroup(g_Valor);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_IdMatricial);
        header.addColumnGroup(g_Aquisicao);
        header.addColumnGroup(g_Valor);
        this.getAnexoG1q05T1().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoG1q05T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoG1q05T1() != null && this.model.getAnexoG1q05T1().size() >= 40) {
            return;
        }
        super.addLineAnexoG1q05T1_LinhaActionPerformed(e);
    }
}

