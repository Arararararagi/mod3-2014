/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog1;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog1.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro04PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04> {
    public static final int MAX_LINES_Q04_T1 = 12;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoG1q04T1().getTableHeader().getColumnModel();
        ColumnGroup g_Realizacao = new ColumnGroup("Realiza\u00e7\u00e3o");
        g_Realizacao.add(cm.getColumn(1));
        g_Realizacao.add(cm.getColumn(2));
        ColumnGroup g_Aquisicao = new ColumnGroup("Aquisi\u00e7\u00e3o");
        g_Aquisicao.add(cm.getColumn(3));
        g_Aquisicao.add(cm.getColumn(4));
        g_Aquisicao.add(cm.getColumn(5));
        if (this.getAnexoG1q04T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoG1q04T1().getTableHeader()).addColumnGroup(g_Realizacao);
            ((GroupableTableHeader)this.getAnexoG1q04T1().getTableHeader()).addColumnGroup(g_Aquisicao);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_Realizacao);
        header.addColumnGroup(g_Aquisicao);
        this.getAnexoG1q04T1().setTableHeader(header);
    }

    @Override
    protected void addLineAnexoG1q04T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoG1q04T1() != null && this.model.getAnexoG1q04T1().size() >= 12) {
            return;
        }
        super.addLineAnexoG1q04T1_LinhaActionPerformed(e);
    }
}

