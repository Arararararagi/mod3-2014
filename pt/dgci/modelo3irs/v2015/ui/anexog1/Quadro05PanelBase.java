/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexog1;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro05Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.util.Session;

public abstract class Quadro05PanelBase
extends Quadro05Panel
implements IBindablePanel<Quadro05> {
    private static final long serialVersionUID = 1;
    protected Quadro05 model;

    @Override
    public abstract void setModel(Quadro05 var1, boolean var2);

    @Override
    public Quadro05 getModel() {
        return this.model;
    }

    @Override
    protected void addLineAnexoG1q05T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            this.model.getAnexoG1q05T1().add(new AnexoG1q05T1_Linha());
        }
    }

    @Override
    protected void removeLineAnexoG1q05T1_LinhaActionPerformed(ActionEvent e) {
        if (((Boolean)Session.isEditable().getValue()).booleanValue()) {
            int selectedRow;
            int n = selectedRow = this.anexoG1q05T1.getSelectedRow() != -1 ? this.anexoG1q05T1.getSelectedRow() : this.anexoG1q05T1.getRowCount() - 1;
            if (selectedRow != -1) {
                this.model.getAnexoG1q05T1().remove(selectedRow);
            }
        }
    }

    public Quadro05PanelBase(Quadro05 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

