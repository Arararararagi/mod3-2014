/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import pt.dgci.modelo3irs.v2015.binding.anexoss.Quadro03Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro03PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro03PanelExtension
extends Quadro03PanelBase
implements IBindablePanel<Quadro03> {
    public Quadro03PanelExtension(Quadro03 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro03 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro03Bindings.doBindings(model, this);
        }
    }
}

