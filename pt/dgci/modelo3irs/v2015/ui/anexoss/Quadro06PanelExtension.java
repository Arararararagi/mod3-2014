/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoss.Quadro06Bindings;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro06PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro06PanelExtension
extends Quadro06PanelBase
implements IBindablePanel<Quadro06> {
    public Quadro06PanelExtension(Quadro06 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro06 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro06Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
                this.setColumnSizes();
            }
        }
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoSSq06T1().getTableHeader().getColumnModel();
        ColumnGroup columnGroup = new ColumnGroup("N.\u00ba de Identifica\u00e7\u00e3o do adquirente do servi\u00e7o");
        columnGroup.add(cm.getColumn(1));
        columnGroup.add(cm.getColumn(2));
        columnGroup.add(cm.getColumn(3));
        if (this.getAnexoSSq06T1().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoSSq06T1().getTableHeader()).addColumnGroup(columnGroup);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(columnGroup);
        this.getAnexoSSq06T1().setTableHeader(header);
    }

    private void setColumnSizes() {
        if (this.getAnexoSSq06T1().getColumnCount() >= 4) {
            this.getAnexoSSq06T1().getColumnModel().getColumn(2).setMaxWidth(170);
            this.getAnexoSSq06T1().getColumnModel().getColumn(2).setMinWidth(170);
            this.getAnexoSSq06T1().getColumnModel().getColumn(2).setMaxWidth(130);
            this.getAnexoSSq06T1().getColumnModel().getColumn(2).setMinWidth(130);
        }
    }
}

