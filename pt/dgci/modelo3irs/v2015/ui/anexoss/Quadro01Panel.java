/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro01Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel1;
    protected JLabelTextFieldNumbering label1;
    protected JCheckBox anexoSSq01B1;
    protected JLabelTextFieldNumbering label2;
    protected JCheckBox anexoSSq01B2;
    protected JLabelTextFieldNumbering label3;
    protected JCheckBox anexoSSq01B3;

    public Quadro01Panel() {
        this.initComponents();
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JLabelTextFieldNumbering getLabel1() {
        return this.label1;
    }

    public JCheckBox getAnexoSSq01B1() {
        return this.anexoSSq01B1;
    }

    public JLabelTextFieldNumbering getLabel2() {
        return this.label2;
    }

    public JCheckBox getAnexoSSq01B2() {
        return this.anexoSSq01B2;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JCheckBox getAnexoSSq01B3() {
        return this.anexoSSq01B3;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoSS");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel1 = new JPanel();
        this.label1 = new JLabelTextFieldNumbering();
        this.anexoSSq01B1 = new JCheckBox();
        this.label2 = new JLabelTextFieldNumbering();
        this.anexoSSq01B2 = new JCheckBox();
        this.label3 = new JLabelTextFieldNumbering();
        this.anexoSSq01B3 = new JCheckBox();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro01Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro01Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$ugap, $rgap, default:grow, 0px", "$ugap, 2*($lgap, default)"));
        this.panel1.setPreferredSize(null);
        this.panel1.setMinimumSize(null);
        this.panel1.setLayout(new FormLayout("default:grow, $lcgap, 13dlu, 0px, default, $lcgap, default:grow", "3*(default, $lgap), default"));
        this.label1.setText(bundle.getString("Quadro01Panel.label1.text"));
        this.label1.setHorizontalAlignment(0);
        this.label1.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.label1, CC.xy(3, 1));
        this.anexoSSq01B1.setText(bundle.getString("Quadro01Panel.anexoSSq01B1.text"));
        this.panel1.add((Component)this.anexoSSq01B1, CC.xy(5, 1));
        this.label2.setText(bundle.getString("Quadro01Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.label2, CC.xy(3, 3));
        this.anexoSSq01B2.setText(bundle.getString("Quadro01Panel.anexoSSq01B2.text"));
        this.panel1.add((Component)this.anexoSSq01B2, CC.xy(5, 3));
        this.label3.setText(bundle.getString("Quadro01Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel1.add((Component)this.label3, CC.xy(3, 5));
        this.anexoSSq01B3.setText(bundle.getString("Quadro01Panel.anexoSSq01B3.text"));
        this.panel1.add((Component)this.anexoSSq01B3, CC.xy(5, 5));
        this.this2.add((Component)this.panel1, CC.xy(3, 3));
        this.add((Component)this.this2, "Center");
    }
}

