/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.base.filters.LimitedSizeDocumentFilter;
import pt.opensoft.swing.base.filters.NumericDocumentFilter;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro03Panel
extends JPanel {
    private static final int ID_SS_SIZE = 11;
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoSSq03C05_label;
    protected JLabelTextFieldNumbering label11;
    protected JTextField anexoSSq03C05;
    protected JPanel panel3;
    protected JLabel anexoSSq03C06_label;
    protected JLabelTextFieldNumbering label18;
    protected JNIFTextField anexoSSq03C06;
    protected JLabel anexoSSq03C07_label;
    protected JLabelTextFieldNumbering label26;
    protected JTextField anexoSSq03C07;
    protected JPanel panel1;
    protected JLabel anexoSSq03C08_label;
    protected JLabelTextFieldNumbering label22;
    protected JCheckBox anexoSSq03C08;

    public Quadro03Panel() {
        this.initComponents();
        ((AbstractDocument)this.anexoSSq03C07.getDocument()).setDocumentFilter(new NumericDocumentFilter(new LimitedSizeDocumentFilter(11)));
        this.anexoSSq03C07.setHorizontalAlignment(4);
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    protected void addLineRostoq03DT1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineRostoq03DT1_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JLabel getAnexoSSq03C05_label() {
        return this.anexoSSq03C05_label;
    }

    public JLabelTextFieldNumbering getLabel11() {
        return this.label11;
    }

    public JTextField getAnexoSSq03C05() {
        return this.anexoSSq03C05;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getAnexoSSq03C06_label() {
        return this.anexoSSq03C06_label;
    }

    public JLabelTextFieldNumbering getLabel18() {
        return this.label18;
    }

    public JLabelTextFieldNumbering getLabel26() {
        return this.label26;
    }

    public JTextField getAnexoSSq03C07() {
        return this.anexoSSq03C07;
    }

    public JLabel getAnexoSSq03C08_label() {
        return this.anexoSSq03C08_label;
    }

    public JLabelTextFieldNumbering getLabel22() {
        return this.label22;
    }

    public JCheckBox getAnexoSSq03C08() {
        return this.anexoSSq03C08;
    }

    public JNIFTextField getAnexoSSq03C06() {
        return this.anexoSSq03C06;
    }

    public JLabel getAnexoSSq03C07_label() {
        return this.anexoSSq03C07_label;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoSS");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoSSq03C05_label = new JLabel();
        this.label11 = new JLabelTextFieldNumbering();
        this.anexoSSq03C05 = new JTextField();
        this.panel3 = new JPanel();
        this.anexoSSq03C06_label = new JLabel();
        this.label18 = new JLabelTextFieldNumbering();
        this.anexoSSq03C06 = new JNIFTextField();
        this.anexoSSq03C07_label = new JLabel();
        this.label26 = new JLabelTextFieldNumbering();
        this.anexoSSq03C07 = new JTextField();
        this.panel1 = new JPanel();
        this.anexoSSq03C08_label = new JLabel();
        this.label22 = new JLabelTextFieldNumbering();
        this.anexoSSq03C08 = new JCheckBox();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro03Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro03Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $lcgap, default", "$rgap, 3*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("$rgap, 2*(default, $lcgap), 13dlu, 2*($lcgap, default:grow), $rgap", "13dlu, 3*($lgap, default)"));
        this.anexoSSq03C05_label.setText(bundle.getString("Quadro03Panel.anexoSSq03C05_label.text"));
        this.panel2.add((Component)this.anexoSSq03C05_label, CC.xy(4, 5));
        this.label11.setText(bundle.getString("Quadro03Panel.label11.text"));
        this.label11.setBorder(new EtchedBorder());
        this.label11.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label11, CC.xy(6, 5));
        this.anexoSSq03C05.setEditable(false);
        this.panel2.add((Component)this.anexoSSq03C05, CC.xywh(8, 5, 3, 1));
        this.this2.add((Component)this.panel2, CC.xy(2, 2));
        this.panel3.setPreferredSize(null);
        this.panel3.setMinimumSize(null);
        this.panel3.setLayout(new FormLayout("$rgap, default:grow, $rgap, default, $rgap, 13dlu, $lcgap, 43dlu, 3dlu, 50dlu, 3dlu, default, 0px, $lcgap, 13dlu, $lcgap, 52dlu, $lcgap, default:grow, $rgap", "$ugap, $lgap, default, 0px, $lgap, default"));
        this.anexoSSq03C06_label.setText(bundle.getString("Quadro03Panel.anexoSSq03C06_label.text"));
        this.panel3.add((Component)this.anexoSSq03C06_label, CC.xy(4, 3));
        this.label18.setText(bundle.getString("Quadro03Panel.label18.text"));
        this.label18.setBorder(new EtchedBorder());
        this.label18.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label18, CC.xy(6, 3));
        this.anexoSSq03C06.setColumns(9);
        this.anexoSSq03C06.setEditable(false);
        this.panel3.add((Component)this.anexoSSq03C06, CC.xy(8, 3));
        this.anexoSSq03C07_label.setText(bundle.getString("Quadro03Panel.anexoSSq03C07_label.text"));
        this.panel3.add((Component)this.anexoSSq03C07_label, CC.xy(12, 3, CC.RIGHT, CC.DEFAULT));
        this.label26.setText(bundle.getString("Quadro03Panel.label26.text"));
        this.label26.setBorder(new EtchedBorder());
        this.label26.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label26, CC.xy(15, 3));
        this.anexoSSq03C07.setColumns(11);
        this.panel3.add((Component)this.anexoSSq03C07, CC.xy(17, 3));
        this.this2.add((Component)this.panel3, CC.xy(2, 4));
        this.panel1.setLayout(new FormLayout("3dlu, default:grow, 3*($lcgap, default), $lcgap, default:grow", "2*(default, $lgap), default"));
        this.anexoSSq03C08_label.setText(bundle.getString("Quadro03Panel.anexoSSq03C08_label.text"));
        this.panel1.add((Component)this.anexoSSq03C08_label, CC.xy(4, 3, CC.FILL, CC.DEFAULT));
        this.label22.setText(bundle.getString("Quadro03Panel.label22.text"));
        this.label22.setBorder(new EtchedBorder());
        this.label22.setHorizontalAlignment(0);
        this.panel1.add((Component)this.label22, CC.xy(6, 3));
        this.panel1.add((Component)this.anexoSSq03C08, CC.xy(8, 3));
        this.this2.add((Component)this.panel1, CC.xy(2, 6));
        this.add((Component)this.this2, "Center");
    }
}

