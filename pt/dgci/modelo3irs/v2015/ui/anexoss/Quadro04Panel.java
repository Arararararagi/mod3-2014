/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel4;
    protected JLabel label6;
    protected JLabel anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label;
    protected JLabel anexoSSq04C401_base_label;
    protected JLabelTextFieldNumbering anexoSSq04C401_num;
    protected JMoneyTextField anexoSSq04C401;
    protected JLabel anexoSSq04C402_base_label;
    protected JLabelTextFieldNumbering anexoSSq04C402_num;
    protected JMoneyTextField anexoSSq04C402;
    protected JLabel anexoSSq04C403_base_label;
    protected JLabelTextFieldNumbering anexoSSq04C403_num;
    protected JMoneyTextField anexoSSq04C403;
    protected JLabel anexoSSq04C404_base_label;
    protected JLabelTextFieldNumbering anexoSSq04C404_num;
    protected JMoneyTextField anexoSSq04C404;
    protected JLabel anexoSSq04C405_base_label;
    protected JLabelTextFieldNumbering anexoSSq04C405_num;
    protected JMoneyTextField anexoSSq04C405;
    protected JLabel anexoSSq04C406_base_label;
    protected JLabelTextFieldNumbering anexoSSq04C406_num;
    protected JMoneyTextField anexoSSq04C406;
    protected JLabel anexoSSq04C407_base_label;
    protected JLabelTextFieldNumbering anexoSSq04C407_num;
    protected JMoneyTextField anexoSSq04C407;
    protected JLabel anexoSSq04C1_base_label;
    protected JMoneyTextField anexoSSq04C1;

    public Quadro04Panel() {
        this.initComponents();
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoSSq04C401_base_label() {
        return this.anexoSSq04C401_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq04C401_num() {
        return this.anexoSSq04C401_num;
    }

    public JMoneyTextField getAnexoSSq04C401() {
        return this.anexoSSq04C401;
    }

    public JLabel getAnexoSSq04C402_base_label() {
        return this.anexoSSq04C402_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq04C402_num() {
        return this.anexoSSq04C402_num;
    }

    public JMoneyTextField getAnexoSSq04C402() {
        return this.anexoSSq04C402;
    }

    public JLabel getAnexoSSq04C403_base_label() {
        return this.anexoSSq04C403_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq04C403_num() {
        return this.anexoSSq04C403_num;
    }

    public JMoneyTextField getAnexoSSq04C403() {
        return this.anexoSSq04C403;
    }

    public JLabel getAnexoSSq04C404_base_label() {
        return this.anexoSSq04C404_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq04C404_num() {
        return this.anexoSSq04C404_num;
    }

    public JMoneyTextField getAnexoSSq04C404() {
        return this.anexoSSq04C404;
    }

    public JLabel getAnexoSSq04C405_base_label() {
        return this.anexoSSq04C405_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq04C405_num() {
        return this.anexoSSq04C405_num;
    }

    public JMoneyTextField getAnexoSSq04C405() {
        return this.anexoSSq04C405;
    }

    public JLabel getAnexoSSq04C406_base_label() {
        return this.anexoSSq04C406_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq04C406_num() {
        return this.anexoSSq04C406_num;
    }

    public JMoneyTextField getAnexoSSq04C406() {
        return this.anexoSSq04C406;
    }

    public JLabel getAnexoSSq04C1_base_label() {
        return this.anexoSSq04C1_base_label;
    }

    public JMoneyTextField getAnexoSSq04C1() {
        return this.anexoSSq04C1;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getAnexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label() {
        return this.anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label;
    }

    public JLabel getAnexoSSq04C407_base_label() {
        return this.anexoSSq04C407_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq04C407_num() {
        return this.anexoSSq04C407_num;
    }

    public JMoneyTextField getAnexoSSq04C407() {
        return this.anexoSSq04C407;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoSS");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel4 = new JPanel();
        this.label6 = new JLabel();
        this.anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label = new JLabel();
        this.anexoSSq04C401_base_label = new JLabel();
        this.anexoSSq04C401_num = new JLabelTextFieldNumbering();
        this.anexoSSq04C401 = new JMoneyTextField();
        this.anexoSSq04C402_base_label = new JLabel();
        this.anexoSSq04C402_num = new JLabelTextFieldNumbering();
        this.anexoSSq04C402 = new JMoneyTextField();
        this.anexoSSq04C403_base_label = new JLabel();
        this.anexoSSq04C403_num = new JLabelTextFieldNumbering();
        this.anexoSSq04C403 = new JMoneyTextField();
        this.anexoSSq04C404_base_label = new JLabel();
        this.anexoSSq04C404_num = new JLabelTextFieldNumbering();
        this.anexoSSq04C404 = new JMoneyTextField();
        this.anexoSSq04C405_base_label = new JLabel();
        this.anexoSSq04C405_num = new JLabelTextFieldNumbering();
        this.anexoSSq04C405 = new JMoneyTextField();
        this.anexoSSq04C406_base_label = new JLabel();
        this.anexoSSq04C406_num = new JLabelTextFieldNumbering();
        this.anexoSSq04C406 = new JMoneyTextField();
        this.anexoSSq04C407_base_label = new JLabel();
        this.anexoSSq04C407_num = new JLabelTextFieldNumbering();
        this.anexoSSq04C407 = new JMoneyTextField();
        this.anexoSSq04C1_base_label = new JLabel();
        this.anexoSSq04C1 = new JMoneyTextField();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel4.setLayout(new FormLayout("$ugap, $rgap, 10dlu:grow, $lcgap, 25dlu, 0px, 70dlu, $rgap", "$rgap, 9*(default, $lgap), default"));
        this.label6.setBorder(new EtchedBorder());
        this.label6.setText(bundle.getString("Quadro04Panel.label6.text"));
        this.panel4.add((Component)this.label6, cc.xy(3, 2));
        this.anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label.text"));
        this.anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label.setBorder(new EtchedBorder());
        this.anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C401_label\u00a3anexoSSq04C402_label\u00a3anexoSSq04C403_label\u00a3anexoSSq04C404_label\u00a3anexoSSq04C405_label\u00a3anexoSSq04C406_label, cc.xywh(5, 2, 3, 1));
        this.anexoSSq04C401_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C401_base_label.text"));
        this.panel4.add((Component)this.anexoSSq04C401_base_label, cc.xy(3, 4));
        this.anexoSSq04C401_num.setText(bundle.getString("Quadro04Panel.anexoSSq04C401_num.text"));
        this.anexoSSq04C401_num.setBorder(new EtchedBorder());
        this.anexoSSq04C401_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C401_num, cc.xy(5, 4));
        this.panel4.add((Component)this.anexoSSq04C401, cc.xy(7, 4));
        this.anexoSSq04C402_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C402_base_label.text"));
        this.panel4.add((Component)this.anexoSSq04C402_base_label, cc.xy(3, 6));
        this.anexoSSq04C402_num.setText(bundle.getString("Quadro04Panel.anexoSSq04C402_num.text"));
        this.anexoSSq04C402_num.setBorder(new EtchedBorder());
        this.anexoSSq04C402_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C402_num, cc.xy(5, 6));
        this.panel4.add((Component)this.anexoSSq04C402, cc.xy(7, 6));
        this.anexoSSq04C403_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C403_base_label.text"));
        this.panel4.add((Component)this.anexoSSq04C403_base_label, cc.xy(3, 8));
        this.anexoSSq04C403_num.setText(bundle.getString("Quadro04Panel.anexoSSq04C403_num.text"));
        this.anexoSSq04C403_num.setBorder(new EtchedBorder());
        this.anexoSSq04C403_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C403_num, cc.xy(5, 8));
        this.panel4.add((Component)this.anexoSSq04C403, cc.xy(7, 8));
        this.anexoSSq04C404_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C404_base_label.text"));
        this.panel4.add((Component)this.anexoSSq04C404_base_label, cc.xy(3, 10));
        this.anexoSSq04C404_num.setText(bundle.getString("Quadro04Panel.anexoSSq04C404_num.text"));
        this.anexoSSq04C404_num.setBorder(new EtchedBorder());
        this.anexoSSq04C404_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C404_num, cc.xy(5, 10));
        this.panel4.add((Component)this.anexoSSq04C404, cc.xy(7, 10));
        this.anexoSSq04C405_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C405_base_label.text"));
        this.panel4.add((Component)this.anexoSSq04C405_base_label, cc.xy(3, 12));
        this.anexoSSq04C405_num.setText(bundle.getString("Quadro04Panel.anexoSSq04C405_num.text"));
        this.anexoSSq04C405_num.setBorder(new EtchedBorder());
        this.anexoSSq04C405_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C405_num, cc.xy(5, 12));
        this.panel4.add((Component)this.anexoSSq04C405, cc.xy(7, 12));
        this.anexoSSq04C406_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C406_base_label.text"));
        this.panel4.add((Component)this.anexoSSq04C406_base_label, cc.xy(3, 14));
        this.anexoSSq04C406_num.setText(bundle.getString("Quadro04Panel.anexoSSq04C406_num.text"));
        this.anexoSSq04C406_num.setBorder(new EtchedBorder());
        this.anexoSSq04C406_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C406_num, cc.xy(5, 14));
        this.panel4.add((Component)this.anexoSSq04C406, cc.xy(7, 14));
        this.anexoSSq04C407_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C407_base_label.text"));
        this.panel4.add((Component)this.anexoSSq04C407_base_label, cc.xy(3, 16));
        this.anexoSSq04C407_num.setText(bundle.getString("Quadro04Panel.anexoSSq04C407_num.text"));
        this.anexoSSq04C407_num.setBorder(new EtchedBorder());
        this.anexoSSq04C407_num.setHorizontalAlignment(0);
        this.panel4.add((Component)this.anexoSSq04C407_num, cc.xy(5, 16));
        this.panel4.add((Component)this.anexoSSq04C407, cc.xy(7, 16));
        this.anexoSSq04C1_base_label.setText(bundle.getString("Quadro04Panel.anexoSSq04C1_base_label.text"));
        this.anexoSSq04C1_base_label.setHorizontalAlignment(4);
        this.panel4.add((Component)this.anexoSSq04C1_base_label, cc.xy(5, 18));
        this.anexoSSq04C1.setEditable(false);
        this.anexoSSq04C1.setColumns(15);
        this.panel4.add((Component)this.anexoSSq04C1, cc.xy(7, 18));
        this.this2.add((Component)this.panel4, cc.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

