/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JMoneyTextField;

public class Quadro05Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel5;
    protected JLabel label6;
    protected JLabel anexoSSq05C501_label\u00a3anexoSSq05C502_labell;
    protected JLabel anexoSSq05C501_base_label;
    protected JLabelTextFieldNumbering anexoSSq05C501_num;
    protected JMoneyTextField anexoSSq05C501;
    protected JLabel anexoSSq05C502_base_label;
    protected JLabelTextFieldNumbering anexoSSq05C502_num;
    protected JMoneyTextField anexoSSq05C502;
    protected JLabel anexoSSq05C1_base_label;
    protected JMoneyTextField anexoSSq05C1;

    public Quadro05Panel() {
        this.initComponents();
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoSSq05C501_base_label() {
        return this.anexoSSq05C501_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq05C501_num() {
        return this.anexoSSq05C501_num;
    }

    public JMoneyTextField getAnexoSSq05C501() {
        return this.anexoSSq05C501;
    }

    public JLabel getAnexoSSq05C502_base_label() {
        return this.anexoSSq05C502_base_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq05C502_num() {
        return this.anexoSSq05C502_num;
    }

    public JMoneyTextField getAnexoSSq05C502() {
        return this.anexoSSq05C502;
    }

    public JLabel getAnexoSSq05C1_base_label() {
        return this.anexoSSq05C1_base_label;
    }

    public JMoneyTextField getAnexoSSq05C1() {
        return this.anexoSSq05C1;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getAnexoSSq05C501_label\u00a3anexoSSq05C502_labell() {
        return this.anexoSSq05C501_label\u00a3anexoSSq05C502_labell;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoSS");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel5 = new JPanel();
        this.label6 = new JLabel();
        this.anexoSSq05C501_label\u00a3anexoSSq05C502_labell = new JLabel();
        this.anexoSSq05C501_base_label = new JLabel();
        this.anexoSSq05C501_num = new JLabelTextFieldNumbering();
        this.anexoSSq05C501 = new JMoneyTextField();
        this.anexoSSq05C502_base_label = new JLabel();
        this.anexoSSq05C502_num = new JLabelTextFieldNumbering();
        this.anexoSSq05C502 = new JMoneyTextField();
        this.anexoSSq05C1_base_label = new JLabel();
        this.anexoSSq05C1 = new JMoneyTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro05Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro05Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("default:grow", "default, $lgap, default"));
        this.panel5.setLayout(new FormLayout("$ugap, $rgap, 10dlu:grow, $lcgap, 25dlu, 0px, 70dlu, $rgap", "$rgap, 4*(default, $lgap), default"));
        this.label6.setBorder(new EtchedBorder());
        this.label6.setText(bundle.getString("Quadro05Panel.label6.text"));
        this.panel5.add((Component)this.label6, CC.xy(3, 2));
        this.anexoSSq05C501_label\u00a3anexoSSq05C502_labell.setText(bundle.getString("Quadro05Panel.anexoSSq05C501_label\u00a3anexoSSq05C502_labell.text"));
        this.anexoSSq05C501_label\u00a3anexoSSq05C502_labell.setBorder(new EtchedBorder());
        this.anexoSSq05C501_label\u00a3anexoSSq05C502_labell.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoSSq05C501_label\u00a3anexoSSq05C502_labell, CC.xywh(5, 2, 3, 1));
        this.anexoSSq05C501_base_label.setText(bundle.getString("Quadro05Panel.anexoSSq05C501_base_label.text"));
        this.panel5.add((Component)this.anexoSSq05C501_base_label, CC.xy(3, 4));
        this.anexoSSq05C501_num.setText(bundle.getString("Quadro05Panel.anexoSSq05C501_num.text"));
        this.anexoSSq05C501_num.setBorder(new EtchedBorder());
        this.anexoSSq05C501_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoSSq05C501_num, CC.xy(5, 4));
        this.panel5.add((Component)this.anexoSSq05C501, CC.xy(7, 4));
        this.anexoSSq05C502_base_label.setText(bundle.getString("Quadro05Panel.anexoSSq05C502_base_label.text"));
        this.panel5.add((Component)this.anexoSSq05C502_base_label, CC.xy(3, 6));
        this.anexoSSq05C502_num.setText(bundle.getString("Quadro05Panel.anexoSSq05C502_num.text"));
        this.anexoSSq05C502_num.setBorder(new EtchedBorder());
        this.anexoSSq05C502_num.setHorizontalAlignment(0);
        this.panel5.add((Component)this.anexoSSq05C502_num, CC.xy(5, 6));
        this.panel5.add((Component)this.anexoSSq05C502, CC.xy(7, 6));
        this.anexoSSq05C1_base_label.setText(bundle.getString("Quadro05Panel.anexoSSq05C1_base_label.text"));
        this.anexoSSq05C1_base_label.setHorizontalAlignment(4);
        this.panel5.add((Component)this.anexoSSq05C1_base_label, CC.xy(5, 8));
        this.anexoSSq05C1.setEditable(false);
        this.anexoSSq05C1.setColumns(15);
        this.panel5.add((Component)this.anexoSSq05C1, CC.xy(7, 8));
        this.this2.add((Component)this.panel5, CC.xy(1, 1));
        this.add((Component)this.this2, "Center");
    }
}

