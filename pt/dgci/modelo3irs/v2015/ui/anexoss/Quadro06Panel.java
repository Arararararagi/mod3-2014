/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoSSq06B1_label;
    protected JLabelTextFieldNumbering label50;
    protected JRadioButton anexoSSq06B1Sim;
    protected JLabelTextFieldNumbering label51;
    protected JRadioButton anexoSSq06B1Nao;
    protected JLabel anexoSSq06B1_label2;
    protected JPanel panel1;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoSSq06T1Scroll;
    protected JTable anexoSSq06T1;

    public Quadro06Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoSSq06B1_label() {
        return this.anexoSSq06B1_label;
    }

    public JLabelTextFieldNumbering getLabel50() {
        return this.label50;
    }

    public JRadioButton getAnexoSSq06B1Sim() {
        return this.anexoSSq06B1Sim;
    }

    public JLabelTextFieldNumbering getLabel51() {
        return this.label51;
    }

    public JRadioButton getAnexoSSq06B1Nao() {
        return this.anexoSSq06B1Nao;
    }

    protected void addLineAnexoBq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoBq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JTable getAnexoSSq06T1() {
        return this.anexoSSq06T1;
    }

    public JButton getButton1() {
        return this.button1;
    }

    public JButton getButton2() {
        return this.button2;
    }

    protected void addLineAnexoSSq06T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoSSq06T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JScrollPane getAnexoSSq06T1Scroll() {
        return this.anexoSSq06T1Scroll;
    }

    public JLabel getAnexoSSq06B1_label2() {
        return this.anexoSSq06B1_label2;
    }

    public JPanel getPanel1() {
        return this.panel1;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoSS");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoSSq06B1_label = new JLabel();
        this.label50 = new JLabelTextFieldNumbering();
        this.anexoSSq06B1Sim = new JRadioButton();
        this.label51 = new JLabelTextFieldNumbering();
        this.anexoSSq06B1Nao = new JRadioButton();
        this.anexoSSq06B1_label2 = new JLabel();
        this.panel1 = new JPanel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoSSq06T1Scroll = new JScrollPane();
        this.anexoSSq06T1 = new JTable();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("3dlu, left:default:grow, 0px, 3dlu", "default, $lgap, default"));
        this.panel2.setLayout(new FormLayout("3*(default, $lcgap), 10dlu, $lcgap, default, $lcgap, 10dlu, $lcgap, default, $lcgap, default:grow, $lcgap, default", "3*(default, $lgap), default"));
        this.anexoSSq06B1_label.setText(bundle.getString("Quadro06Panel.anexoSSq06B1_label.text"));
        this.anexoSSq06B1_label.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoSSq06B1_label, CC.xy(3, 3, CC.LEFT, CC.DEFAULT));
        this.label50.setText(bundle.getString("Quadro06Panel.label50.text"));
        this.label50.setBorder(new EtchedBorder());
        this.label50.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label50, CC.xy(7, 3));
        this.anexoSSq06B1Sim.setText(bundle.getString("Quadro06Panel.anexoSSq06B1Sim.text"));
        this.panel2.add((Component)this.anexoSSq06B1Sim, CC.xy(9, 3));
        this.label51.setText(bundle.getString("Quadro06Panel.label51.text"));
        this.label51.setBorder(new EtchedBorder());
        this.label51.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label51, CC.xy(11, 3));
        this.anexoSSq06B1Nao.setText(bundle.getString("Quadro06Panel.anexoSSq06B1Nao.text"));
        this.panel2.add((Component)this.anexoSSq06B1Nao, CC.xy(13, 3));
        this.anexoSSq06B1_label2.setText(bundle.getString("Quadro06Panel.anexoSSq06B1_label2.text"));
        this.anexoSSq06B1_label2.setHorizontalAlignment(4);
        this.panel2.add((Component)this.anexoSSq06B1_label2, CC.xy(3, 5, CC.LEFT, CC.DEFAULT));
        this.this2.add((Component)this.panel2, CC.xywh(2, 1, 3, 1));
        this.panel1.setLayout(new FormLayout("default:grow, $lcgap, 600px, $lcgap, default:grow", "default, $lgap, 95dlu, $lgap, default"));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro06Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro06Panel.this.addLineAnexoSSq06T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro06Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro06Panel.this.removeLineAnexoSSq06T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel1.add((Component)this.toolBar1, CC.xy(3, 1));
        this.anexoSSq06T1Scroll.setViewportView(this.anexoSSq06T1);
        this.panel1.add((Component)this.anexoSSq06T1Scroll, CC.xy(3, 3));
        this.this2.add((Component)this.panel1, CC.xywh(2, 3, 3, 1));
        this.add((Component)this.this2, "Center");
    }

}

