/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro02Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JLabel anexoSSq02C04_label;
    protected JLabelTextFieldNumbering anexoSSq02C04_num;
    protected JEditableComboBox anexoSSq02C04;

    public Quadro02Panel() {
        this.initComponents();
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JLabel getAnexoSSq02C04_label() {
        return this.anexoSSq02C04_label;
    }

    public JLabelTextFieldNumbering getAnexoSSq02C04_num() {
        return this.anexoSSq02C04_num;
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JEditableComboBox getAnexoSSq02C04() {
        return this.anexoSSq02C04;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoSS");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.anexoSSq02C04_label = new JLabel();
        this.anexoSSq02C04_num = new JLabelTextFieldNumbering();
        this.anexoSSq02C04 = new JEditableComboBox();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setNumber(bundle.getString("Quadro02Panel.titlePanel.number"));
        this.titlePanel.setTitle(bundle.getString("Quadro02Panel.titlePanel.title"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow, $rgap, left:default, $rgap, 13dlu, 0px, $lcgap, 40dlu, $rgap, default:grow", "$rgap, default, $lgap, default"));
        this.anexoSSq02C04_label.setText(bundle.getString("Quadro02Panel.anexoSSq02C04_label.text"));
        this.anexoSSq02C04_label.setHorizontalAlignment(4);
        this.this2.add((Component)this.anexoSSq02C04_label, CC.xy(4, 2));
        this.anexoSSq02C04_num.setText(bundle.getString("Quadro02Panel.anexoSSq02C04_num.text"));
        this.anexoSSq02C04_num.setBorder(new EtchedBorder());
        this.anexoSSq02C04_num.setHorizontalAlignment(0);
        this.this2.add((Component)this.anexoSSq02C04_num, CC.xy(6, 2));
        this.anexoSSq02C04.setMinimumSize(null);
        this.anexoSSq02C04.setPreferredSize(null);
        this.anexoSSq02C04.setEditable(false);
        this.anexoSSq02C04.setEnabled(false);
        this.this2.add((Component)this.anexoSSq02C04, CC.xy(9, 2));
        this.add((Component)this.this2, "Center");
    }
}

