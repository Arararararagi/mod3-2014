/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexoss;

import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro03Panel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public abstract class Quadro03PanelBase
extends Quadro03Panel
implements IBindablePanel<Quadro03> {
    private static final long serialVersionUID = 1;
    protected Quadro03 model;

    @Override
    public abstract void setModel(Quadro03 var1, boolean var2);

    @Override
    public Quadro03 getModel() {
        return this.model;
    }

    public Quadro03PanelBase(Quadro03 model, boolean skipBinding) {
        this.setModel(model, skipBinding);
    }
}

