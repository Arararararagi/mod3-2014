/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexol;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;
import pt.opensoft.swing.components.JNIFTextField;

public class Quadro03Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JLabel anexoLq03C02_label;
    protected JLabel label3;
    protected JLabelTextFieldNumbering label4;
    protected JNIFTextField anexoLq03C02;
    protected JLabel anexoLq03C03_label;
    protected JLabel label6;
    protected JLabelTextFieldNumbering label7;
    protected JNIFTextField anexoLq03C03;
    protected JPanel panel3;
    protected JLabel label18;
    protected JLabel anexoLq03C04_label;
    protected JLabel label20;
    protected JLabelTextFieldNumbering label21;
    protected JNIFTextField anexoLq03C04;

    public Quadro03Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabel getAnexoLq03C02_label() {
        return this.anexoLq03C02_label;
    }

    public JLabel getLabel3() {
        return this.label3;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JNIFTextField getAnexoLq03C02() {
        return this.anexoLq03C02;
    }

    public JLabel getAnexoLq03C03_label() {
        return this.anexoLq03C03_label;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabelTextFieldNumbering getLabel7() {
        return this.label7;
    }

    public JNIFTextField getAnexoLq03C03() {
        return this.anexoLq03C03;
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JLabel getLabel18() {
        return this.label18;
    }

    public JLabel getAnexoLq03C04_label() {
        return this.anexoLq03C04_label;
    }

    public JLabel getLabel20() {
        return this.label20;
    }

    public JLabelTextFieldNumbering getLabel21() {
        return this.label21;
    }

    public JNIFTextField getAnexoLq03C04() {
        return this.anexoLq03C04;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoL");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.anexoLq03C02_label = new JLabel();
        this.label3 = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.anexoLq03C02 = new JNIFTextField();
        this.anexoLq03C03_label = new JLabel();
        this.label6 = new JLabel();
        this.label7 = new JLabelTextFieldNumbering();
        this.anexoLq03C03 = new JNIFTextField();
        this.panel3 = new JPanel();
        this.label18 = new JLabel();
        this.anexoLq03C04_label = new JLabel();
        this.label20 = new JLabel();
        this.label21 = new JLabelTextFieldNumbering();
        this.anexoLq03C04 = new JNIFTextField();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro03Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro03Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, default, $lgap, default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("$rgap, 10dlu, 0px, default:grow, $lcgap, default, 15dlu, default, $rgap, 13dlu, 0px, $lcgap, 43dlu, $lcgap, default:grow, $lcgap, default, 15dlu, default, $lcgap, default, 0px, $lcgap, 43dlu, $lcgap, default:grow", "$ugap, 2*($lgap, default)"));
        this.anexoLq03C02_label.setText(bundle.getString("Quadro03Panel.anexoLq03C02_label.text"));
        this.panel2.add((Component)this.anexoLq03C02_label, CC.xy(6, 3));
        this.label3.setText(bundle.getString("Quadro03Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label3, CC.xy(8, 3));
        this.label4.setText(bundle.getString("Quadro03Panel.label4.text"));
        this.label4.setBorder(new EtchedBorder());
        this.label4.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label4, CC.xy(10, 3));
        this.anexoLq03C02.setColumns(9);
        this.anexoLq03C02.setEditable(false);
        this.panel2.add((Component)this.anexoLq03C02, CC.xy(13, 3));
        this.anexoLq03C03_label.setText(bundle.getString("Quadro03Panel.anexoLq03C03_label.text"));
        this.panel2.add((Component)this.anexoLq03C03_label, CC.xy(17, 3));
        this.label6.setText(bundle.getString("Quadro03Panel.label6.text"));
        this.label6.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label6, CC.xy(19, 3));
        this.label7.setText(bundle.getString("Quadro03Panel.label7.text"));
        this.label7.setBorder(new EtchedBorder());
        this.label7.setHorizontalAlignment(0);
        this.panel2.add((Component)this.label7, CC.xy(21, 3));
        this.anexoLq03C03.setColumns(9);
        this.anexoLq03C03.setEditable(false);
        this.panel2.add((Component)this.anexoLq03C03, CC.xy(24, 3));
        this.this2.add((Component)this.panel2, CC.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, $lcgap, 12dlu, 0px, 40dlu, $lcgap, default, $rgap, 13dlu, 0px, $lcgap, 43dlu, $lcgap, 40dlu, $lcgap, 0px, default:grow", "$ugap, 3*($lgap, default)"));
        this.label18.setText(bundle.getString("Quadro03Panel.label18.text"));
        this.label18.setHorizontalAlignment(0);
        this.label18.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.label18, CC.xy(3, 3));
        this.anexoLq03C04_label.setText(bundle.getString("Quadro03Panel.anexoLq03C04_label.text"));
        this.anexoLq03C04_label.setHorizontalAlignment(0);
        this.anexoLq03C04_label.setBorder(new EtchedBorder());
        this.panel3.add((Component)this.anexoLq03C04_label, CC.xywh(5, 3, 10, 1));
        this.label20.setText(bundle.getString("Quadro03Panel.label20.text"));
        this.label20.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label20, CC.xy(7, 7));
        this.label21.setText(bundle.getString("Quadro03Panel.label21.text"));
        this.label21.setBorder(new EtchedBorder());
        this.label21.setHorizontalAlignment(0);
        this.panel3.add((Component)this.label21, CC.xy(9, 7));
        this.anexoLq03C04.setColumns(9);
        this.anexoLq03C04.setEditable(false);
        this.panel3.add((Component)this.anexoLq03C04, CC.xy(12, 7));
        this.this2.add((Component)this.panel3, CC.xy(2, 4));
        this.add((Component)this.this2, "Center");
    }
}

