/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexol;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JLabelTextFieldNumbering;

public class Quadro06Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JPanel panel6;
    protected JLabel label10;
    protected JLabel label11;
    protected JLabelTextFieldNumbering label2;
    protected JRadioButton q06B1OP1;
    protected JLabelTextFieldNumbering label3;
    protected JRadioButton q06B1OP2;
    protected JPanel panel7;
    protected JLabel label12;
    protected JLabel label13;
    protected JLabel label1;
    protected JLabel label6;
    protected JLabel label7;
    protected JLabel label8;
    protected JLabel label9;
    protected JLabel label14;
    protected JLabelTextFieldNumbering label4;
    protected JRadioButton q06B2OP3;
    protected JLabelTextFieldNumbering label5;
    protected JRadioButton q06B2OP4;

    public Quadro06Panel() {
        this.initComponents();
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JLabelTextFieldNumbering getLabel2() {
        return this.label2;
    }

    public JRadioButton getQ06B1OP1() {
        return this.q06B1OP1;
    }

    public JLabelTextFieldNumbering getLabel3() {
        return this.label3;
    }

    public JRadioButton getQ06B1OP2() {
        return this.q06B1OP2;
    }

    public JLabelTextFieldNumbering getLabel4() {
        return this.label4;
    }

    public JRadioButton getQ06B2OP3() {
        return this.q06B2OP3;
    }

    public JLabelTextFieldNumbering getLabel5() {
        return this.label5;
    }

    public JRadioButton getQ06B2OP4() {
        return this.q06B2OP4;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getLabel12() {
        return this.label12;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    public JLabel getLabel1() {
        return this.label1;
    }

    public JLabel getLabel6() {
        return this.label6;
    }

    public JLabel getLabel7() {
        return this.label7;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JLabel getLabel9() {
        return this.label9;
    }

    public JLabel getLabel14() {
        return this.label14;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoL");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.panel6 = new JPanel();
        this.label10 = new JLabel();
        this.label11 = new JLabel();
        this.label2 = new JLabelTextFieldNumbering();
        this.q06B1OP1 = new JRadioButton();
        this.label3 = new JLabelTextFieldNumbering();
        this.q06B1OP2 = new JRadioButton();
        this.panel7 = new JPanel();
        this.label12 = new JLabel();
        this.label13 = new JLabel();
        this.label1 = new JLabel();
        this.label6 = new JLabel();
        this.label7 = new JLabel();
        this.label8 = new JLabel();
        this.label9 = new JLabel();
        this.label14 = new JLabel();
        this.label4 = new JLabelTextFieldNumbering();
        this.q06B2OP3 = new JRadioButton();
        this.label5 = new JLabelTextFieldNumbering();
        this.q06B2OP4 = new JRadioButton();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro06Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro06Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, 550dlu:grow, 0px", "$rgap, default, $lgap, default"));
        this.panel2.setPreferredSize(null);
        this.panel2.setMinimumSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, $rgap, 13dlu, 0px, default:grow, $rgap, default:grow", "14*(default, $lgap), default"));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap, default, $rgap, default, $lcgap, default:grow, $rgap", "default"));
        this.label10.setText(bundle.getString("Quadro06Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label10, cc.xy(1, 1));
        this.label11.setText(bundle.getString("Quadro06Panel.label11.text"));
        this.label11.setHorizontalAlignment(0);
        this.label11.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label11, cc.xywh(3, 1, 7, 1));
        this.panel2.add((Component)this.panel6, cc.xywh(1, 1, 7, 1));
        this.label2.setText(bundle.getString("Quadro06Panel.label2.text"));
        this.label2.setHorizontalAlignment(0);
        this.label2.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label2, cc.xy(3, 3));
        this.q06B1OP1.setText(bundle.getString("Quadro06Panel.q06B1OP1.text"));
        this.panel2.add((Component)this.q06B1OP1, cc.xy(5, 3));
        this.label3.setText(bundle.getString("Quadro06Panel.label3.text"));
        this.label3.setHorizontalAlignment(0);
        this.label3.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label3, cc.xy(3, 5));
        this.q06B1OP2.setText(bundle.getString("Quadro06Panel.q06B1OP2.text"));
        this.q06B1OP2.setMaximumSize(new Dimension(317, 23));
        this.q06B1OP2.setMinimumSize(new Dimension(317, 23));
        this.q06B1OP2.setPreferredSize(new Dimension(800, 23));
        this.panel2.add((Component)this.q06B1OP2, cc.xy(5, 5));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap, default, $rgap, default, $lcgap, default:grow, $rgap", "default"));
        this.label12.setText(bundle.getString("Quadro06Panel.label12.text"));
        this.label12.setBorder(new EtchedBorder());
        this.label12.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label12, cc.xy(1, 1));
        this.label13.setText(bundle.getString("Quadro06Panel.label13.text"));
        this.label13.setHorizontalAlignment(0);
        this.label13.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.label13, cc.xywh(3, 1, 7, 1));
        this.panel2.add((Component)this.panel7, cc.xywh(1, 11, 7, 1));
        this.label1.setText("Relativamente aos rendimentos auferidos de:");
        this.panel2.add((Component)this.label1, cc.xywh(1, 13, 7, 1, CellConstraints.LEFT, CellConstraints.DEFAULT));
        this.label6.setText("- Categoria A tributados no estrangeiro;");
        this.panel2.add((Component)this.label6, cc.xy(1, 15));
        this.label7.setText("- Categoria B respeitantes a atividades de elevado valor acrescentado que possam ser tributados no estrangeiro;");
        this.panel2.add((Component)this.label7, cc.xywh(1, 17, 5, 1));
        this.label8.setText("- Categoria E, F ou G que possam ser tributados no estrangeiro;");
        this.panel2.add((Component)this.label8, cc.xy(1, 19));
        this.label9.setText("- Caregoria H tributados no estrangeiro ou n\u00e3o obtidos no territ\u00f3rio portugu\u00eas,");
        this.panel2.add((Component)this.label9, cc.xy(1, 21));
        this.label14.setText("Indique o m\u00e9todo que pretende:");
        this.panel2.add((Component)this.label14, cc.xy(1, 23));
        this.label4.setText(bundle.getString("Quadro06Panel.label4.text"));
        this.label4.setHorizontalAlignment(0);
        this.label4.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label4, cc.xy(3, 25));
        this.q06B2OP3.setText(bundle.getString("Quadro06Panel.q06B2OP3.text"));
        this.panel2.add((Component)this.q06B2OP3, cc.xy(5, 25));
        this.label5.setText(bundle.getString("Quadro06Panel.label5.text"));
        this.label5.setHorizontalAlignment(0);
        this.label5.setBorder(new EtchedBorder());
        this.panel2.add((Component)this.label5, cc.xy(3, 27));
        this.q06B2OP4.setText(bundle.getString("Quadro06Panel.q06B2OP4.text"));
        this.panel2.add((Component)this.q06B2OP4, cc.xy(5, 27));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.add((Component)this.this2, "Center");
    }
}

