/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexol;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import pt.dgci.modelo3irs.v2015.binding.anexol.Quadro05Bindings;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro05PanelBase;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro05PanelExtension
extends Quadro05PanelBase
implements IBindablePanel<Quadro05> {
    public static final int MAX_LINES_Q05_T1 = 100;

    public Quadro05PanelExtension(Quadro05 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro05 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro05Bindings.doBindings(model, this);
        }
    }

    @Override
    protected void addLineAnexoLq05T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoLq05T1() != null && this.model.getAnexoLq05T1().size() >= 100) {
            return;
        }
        super.addLineAnexoLq05T1_LinhaActionPerformed(e);
    }
}

