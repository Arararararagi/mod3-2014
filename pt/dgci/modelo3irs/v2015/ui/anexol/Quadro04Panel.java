/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexol;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import pt.opensoft.swing.QuadroTitlePanel;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JRemoveLineIconableButton;

public class Quadro04Panel
extends JPanel {
    protected QuadroTitlePanel titlePanel;
    protected JPanel this2;
    protected JPanel panel2;
    protected JPanel panel5;
    protected JLabel label8;
    protected JLabel label9;
    protected JToolBar toolBar1;
    protected JAddLineIconableButton button1;
    protected JRemoveLineIconableButton button2;
    protected JScrollPane anexoJq04T1Scroll;
    protected JTable anexoLq04T1;
    protected JPanel panel3;
    protected JPanel panel6;
    protected JLabel label10;
    protected JLabel label11;
    protected JToolBar toolBar2;
    protected JAddLineIconableButton button3;
    protected JRemoveLineIconableButton button4;
    protected JScrollPane anexoJq04T2Scroll;
    protected JTable anexoLq04T2;
    protected JPanel panel4;
    protected JPanel panel7;
    protected JLabel label12;
    protected JLabel label13;
    protected JToolBar toolBar3;
    protected JAddLineIconableButton button5;
    protected JRemoveLineIconableButton button6;
    protected JScrollPane anexoJq04T3Scroll;
    protected JTable anexoLq04T3;

    public Quadro04Panel() {
        this.initComponents();
    }

    protected void addLineAnexoLq04T4_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoLq04T4_LinhaActionPerformed(ActionEvent e) {
    }

    public QuadroTitlePanel getTitlePanel() {
        return this.titlePanel;
    }

    public JPanel getThis2() {
        return this.this2;
    }

    public JPanel getPanel2() {
        return this.panel2;
    }

    public JToolBar getToolBar1() {
        return this.toolBar1;
    }

    public JAddLineIconableButton getButton1() {
        return this.button1;
    }

    public JRemoveLineIconableButton getButton2() {
        return this.button2;
    }

    public JScrollPane getAnexoJq04T1Scroll() {
        return this.anexoJq04T1Scroll;
    }

    public JTable getAnexoLq04T1() {
        return this.anexoLq04T1;
    }

    protected void addLineAnexoLq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoLq04T1_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel3() {
        return this.panel3;
    }

    public JToolBar getToolBar2() {
        return this.toolBar2;
    }

    public JAddLineIconableButton getButton3() {
        return this.button3;
    }

    public JRemoveLineIconableButton getButton4() {
        return this.button4;
    }

    public JScrollPane getAnexoJq04T2Scroll() {
        return this.anexoJq04T2Scroll;
    }

    public JTable getAnexoLq04T2() {
        return this.anexoLq04T2;
    }

    protected void addLineAnexoLq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoLq04T2_LinhaActionPerformed(ActionEvent e) {
    }

    protected void addLineAnexoLq04T3_LinhaActionPerformed(ActionEvent e) {
    }

    protected void removeLineAnexoLq04T3_LinhaActionPerformed(ActionEvent e) {
    }

    public JPanel getPanel4() {
        return this.panel4;
    }

    public JToolBar getToolBar3() {
        return this.toolBar3;
    }

    public JAddLineIconableButton getButton5() {
        return this.button5;
    }

    public JRemoveLineIconableButton getButton6() {
        return this.button6;
    }

    public JScrollPane getAnexoJq04T3Scroll() {
        return this.anexoJq04T3Scroll;
    }

    public JTable getAnexoLq04T3() {
        return this.anexoLq04T3;
    }

    public JPanel getPanel5() {
        return this.panel5;
    }

    public JLabel getLabel8() {
        return this.label8;
    }

    public JLabel getLabel9() {
        return this.label9;
    }

    public JPanel getPanel6() {
        return this.panel6;
    }

    public JLabel getLabel10() {
        return this.label10;
    }

    public JLabel getLabel11() {
        return this.label11;
    }

    public JPanel getPanel7() {
        return this.panel7;
    }

    public JLabel getLabel12() {
        return this.label12;
    }

    public JLabel getLabel13() {
        return this.label13;
    }

    private void initComponents() {
        ResourceBundle bundle = ResourceBundle.getBundle("pt.dgci.modelo3irs.v2015.gui.AnexoL");
        this.titlePanel = new QuadroTitlePanel();
        this.this2 = new JPanel();
        this.panel2 = new JPanel();
        this.panel5 = new JPanel();
        this.label8 = new JLabel();
        this.label9 = new JLabel();
        this.toolBar1 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button2 = new JRemoveLineIconableButton();
        this.anexoJq04T1Scroll = new JScrollPane();
        this.anexoLq04T1 = new JTable();
        this.panel3 = new JPanel();
        this.panel6 = new JPanel();
        this.label10 = new JLabel();
        this.label11 = new JLabel();
        this.toolBar2 = new JToolBar();
        this.button3 = new JAddLineIconableButton();
        this.button4 = new JRemoveLineIconableButton();
        this.anexoJq04T2Scroll = new JScrollPane();
        this.anexoLq04T2 = new JTable();
        this.panel4 = new JPanel();
        this.panel7 = new JPanel();
        this.label12 = new JLabel();
        this.label13 = new JLabel();
        this.toolBar3 = new JToolBar();
        this.button5 = new JAddLineIconableButton();
        this.button6 = new JRemoveLineIconableButton();
        this.anexoJq04T3Scroll = new JScrollPane();
        this.anexoLq04T3 = new JTable();
        CellConstraints cc = new CellConstraints();
        this.setMinimumSize(null);
        this.setPreferredSize(null);
        this.setLayout(new BorderLayout());
        this.titlePanel.setTitle(bundle.getString("Quadro04Panel.titlePanel.title"));
        this.titlePanel.setNumber(bundle.getString("Quadro04Panel.titlePanel.number"));
        this.add((Component)this.titlePanel, "North");
        this.this2.setMinimumSize(null);
        this.this2.setPreferredSize(null);
        this.this2.setBorder(LineBorder.createBlackLineBorder());
        this.this2.setLayout(new FormLayout("$rgap, default:grow", "$rgap, 4*(default, $lgap), default"));
        this.panel2.setMinimumSize(null);
        this.panel2.setPreferredSize(null);
        this.panel2.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $rgap, 300dlu, $lcgap, default:grow", "$ugap, 2*($lgap, default), $lgap, 165dlu, $lgap, default"));
        this.panel5.setMinimumSize(null);
        this.panel5.setPreferredSize(null);
        this.panel5.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap, default, $rgap, default, $lcgap, default:grow, $rgap", "default"));
        this.label8.setText(bundle.getString("Quadro04Panel.label8.text"));
        this.label8.setBorder(new EtchedBorder());
        this.label8.setHorizontalAlignment(0);
        this.panel5.add((Component)this.label8, cc.xy(1, 1));
        this.label9.setText(bundle.getString("Quadro04Panel.label9.text"));
        this.label9.setHorizontalAlignment(0);
        this.label9.setBorder(new EtchedBorder());
        this.panel5.add((Component)this.label9, cc.xywh(3, 1, 7, 1));
        this.panel2.add((Component)this.panel5, cc.xywh(1, 3, 9, 1));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button1.setText(bundle.getString("Quadro04Panel.button1.text"));
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoLq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button1);
        this.button2.setText(bundle.getString("Quadro04Panel.button2.text"));
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoLq04T1_LinhaActionPerformed(e);
            }
        });
        this.toolBar1.add(this.button2);
        this.panel2.add((Component)this.toolBar1, cc.xy(3, 5));
        this.anexoJq04T1Scroll.setViewportView(this.anexoLq04T1);
        this.panel2.add((Component)this.anexoJq04T1Scroll, cc.xywh(3, 7, 5, 1));
        this.this2.add((Component)this.panel2, cc.xy(2, 2));
        this.panel3.setMinimumSize(null);
        this.panel3.setPreferredSize(null);
        this.panel3.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $rgap, 300dlu, $lcgap, default:grow", "$ugap, 2*($lgap, default), $lgap, 165dlu, $lgap, default"));
        this.panel6.setMinimumSize(null);
        this.panel6.setPreferredSize(null);
        this.panel6.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap, default, $rgap, default, $lcgap, default:grow, $rgap", "default"));
        this.label10.setText(bundle.getString("Quadro04Panel.label10.text"));
        this.label10.setBorder(new EtchedBorder());
        this.label10.setHorizontalAlignment(0);
        this.panel6.add((Component)this.label10, cc.xy(1, 1));
        this.label11.setText(bundle.getString("Quadro04Panel.label11.text"));
        this.label11.setHorizontalAlignment(0);
        this.label11.setBorder(new EtchedBorder());
        this.panel6.add((Component)this.label11, cc.xywh(3, 1, 7, 1));
        this.panel3.add((Component)this.panel6, cc.xywh(1, 3, 9, 1));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button3.setText(bundle.getString("Quadro04Panel.button3.text"));
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoLq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button3);
        this.button4.setText(bundle.getString("Quadro04Panel.button4.text"));
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoLq04T2_LinhaActionPerformed(e);
            }
        });
        this.toolBar2.add(this.button4);
        this.panel3.add((Component)this.toolBar2, cc.xy(3, 5));
        this.anexoJq04T2Scroll.setViewportView(this.anexoLq04T2);
        this.panel3.add((Component)this.anexoJq04T2Scroll, cc.xywh(3, 7, 5, 1));
        this.this2.add((Component)this.panel3, cc.xy(2, 4));
        this.panel4.setMinimumSize(null);
        this.panel4.setPreferredSize(null);
        this.panel4.setLayout(new FormLayout("default:grow, 2*($lcgap, default), $rgap, 300dlu, $lcgap, default:grow", "$ugap, 2*($lgap, default), $lgap, 165dlu, $lgap, default"));
        this.panel7.setMinimumSize(null);
        this.panel7.setPreferredSize(null);
        this.panel7.setLayout(new FormLayout("15dlu, 0px, default:grow, $lcgap, default, $rgap, default, $lcgap, default:grow, $rgap", "default"));
        this.label12.setText(bundle.getString("Quadro04Panel.label12.text"));
        this.label12.setBorder(new EtchedBorder());
        this.label12.setHorizontalAlignment(0);
        this.panel7.add((Component)this.label12, cc.xy(1, 1));
        this.label13.setText(bundle.getString("Quadro04Panel.label13.text"));
        this.label13.setHorizontalAlignment(0);
        this.label13.setBorder(new EtchedBorder());
        this.panel7.add((Component)this.label13, cc.xywh(3, 1, 7, 1));
        this.panel4.add((Component)this.panel7, cc.xywh(1, 3, 9, 1));
        this.toolBar3.setFloatable(false);
        this.toolBar3.setRollover(true);
        this.button5.setText(bundle.getString("Quadro04Panel.button5.text"));
        this.button5.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.addLineAnexoLq04T3_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button5);
        this.button6.setText(bundle.getString("Quadro04Panel.button6.text"));
        this.button6.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Quadro04Panel.this.removeLineAnexoLq04T3_LinhaActionPerformed(e);
            }
        });
        this.toolBar3.add(this.button6);
        this.panel4.add((Component)this.toolBar3, cc.xy(3, 5));
        this.anexoJq04T3Scroll.setViewportView(this.anexoLq04T3);
        this.panel4.add((Component)this.anexoJq04T3Scroll, cc.xywh(3, 7, 5, 1));
        this.this2.add((Component)this.panel4, cc.xy(2, 6));
        this.add((Component)this.this2, "Center");
    }

}

