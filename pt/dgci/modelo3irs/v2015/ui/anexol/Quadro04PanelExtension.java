/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.ui.anexol;

import ca.odell.glazedlists.EventList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexol.Quadro04Bindings;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro04PanelBase;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeader;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;

public class Quadro04PanelExtension
extends Quadro04PanelBase
implements IBindablePanel<Quadro04> {
    public static final int MAX_LINES_Q04_T1 = 100;
    public static final int MAX_LINES_Q04_T2 = 100;
    public static final int MAX_LINES_Q04_T3 = 100;

    public Quadro04PanelExtension(Quadro04 model, boolean skipBinding) {
        super(model, skipBinding);
    }

    @Override
    public void setModel(Quadro04 model, boolean skipBinding) {
        this.model = model;
        if (!skipBinding) {
            Quadro04Bindings.doBindings(model, this);
            if (model != null) {
                this.setHeader();
            }
        }
    }

    @Override
    protected void addLineAnexoLq04T1_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoLq04T1() != null && this.model.getAnexoLq04T1().size() >= 100) {
            return;
        }
        super.addLineAnexoLq04T1_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoLq04T2_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoLq04T2() != null && this.model.getAnexoLq04T2().size() >= 100) {
            return;
        }
        super.addLineAnexoLq04T2_LinhaActionPerformed(e);
    }

    @Override
    protected void addLineAnexoLq04T3_LinhaActionPerformed(ActionEvent e) {
        if (this.model.getAnexoLq04T3() != null && this.model.getAnexoLq04T3().size() >= 100) {
            return;
        }
        super.addLineAnexoLq04T3_LinhaActionPerformed(e);
    }

    private void setHeader() {
        TableColumnModel cm = this.getAnexoLq04T3().getTableHeader().getColumnModel();
        ColumnGroup g_Resultado = new ColumnGroup("Resultado");
        g_Resultado.add(cm.getColumn(3));
        g_Resultado.add(cm.getColumn(4));
        if (this.getAnexoLq04T3().getTableHeader() instanceof GroupableTableHeader) {
            ((GroupableTableHeader)this.getAnexoLq04T3().getTableHeader()).addColumnGroup(g_Resultado);
            return;
        }
        GroupableTableHeader header = new GroupableTableHeader(cm);
        header.addColumnGroup(g_Resultado);
        this.getAnexoLq04T3().setTableHeader(header);
    }
}

