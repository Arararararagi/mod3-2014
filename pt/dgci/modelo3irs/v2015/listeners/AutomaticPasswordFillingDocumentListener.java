/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.listeners;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class AutomaticPasswordFillingDocumentListener
implements DocumentListener {
    private JTextField passTextField;

    public AutomaticPasswordFillingDocumentListener(JTextField passTextField) {
        this.passTextField = passTextField;
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.updatePassword(e);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.updatePassword(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.updatePassword(e);
    }

    private void updatePassword(DocumentEvent e) {
        try {
            String password = e.getDocument().getLength() >= 3 ? "000000000" + e.getDocument().getText(0, 3) : null;
            this.passTextField.setText(password);
        }
        catch (BadLocationException e1) {
            this.passTextField.setText(null);
        }
    }
}

