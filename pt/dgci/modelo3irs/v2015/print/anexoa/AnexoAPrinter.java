/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexoa;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro05;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.util.Date;

public class AnexoAPrinter {
    AnexoAModel anexoA;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoAPrinter(AnexoAModel anexoA, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoA = anexoA;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo A");
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoA.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoAq02C01"), quadro2.getAnexoAq02C01() == null ? "" : quadro2.getAnexoAq02C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.anexoA.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoAq03C02"), quadro3.getAnexoAq03C02() == null ? "" : quadro3.getAnexoAq03C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoAq03C03"), quadro3.getAnexoAq03C03() == null ? "" : quadro3.getAnexoAq03C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.anexoA.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label13.text") + ": " + this.labelManager.getString("Quadro04Panel.label3.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoAq04T4A_LinhaAdapterFormat tableAdapter = new AnexoAq04T4A_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoAq04T4A_Linha> list = quadro4.getAnexoAq04T4A();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoAq04T4A_Linha linha = list.get(i2);
            String[] campos = new String[9];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = linha.getCodRendimentos() == null ? "" : linha.getCodRendimentos().toString();
            campos[2] = linha.getTitular() == null ? "" : linha.getTitular();
            campos[3] = linha.getRendimentos() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimentos());
            campos[4] = linha.getRetencoes() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRetencoes());
            campos[5] = linha.getContribuicoes() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getContribuicoes());
            campos[6] = linha.getRetSobretaxa() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRetSobretaxa());
            campos[7] = linha.getDataContratoPreReforma() == null ? "" : linha.getDataContratoPreReforma().formatDate();
            campos[8] = linha.getDataPrimeiroPagamento() == null ? "" : linha.getDataPrimeiroPagamento().formatDate();
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas, new float[]{5.0f, 10.5f, 12.5f, 7.0f, 12.5f, 10.5f, 12.5f, 12.5f, 12.5f, 12.5f});
        this.render.addNewLine();
        this.render.startTable(new float[]{4.0f, 2.0f, 2.0f, 2.0f, 2.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoAq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoAq04C2"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoAq04C3"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoAq04C4"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoAq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(quadro4.getAnexoAq04C1() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoAq04C1()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(quadro4.getAnexoAq04C2() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoAq04C2()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(quadro4.getAnexoAq04C3() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoAq04C3()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(quadro4.getAnexoAq04C4() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoAq04C4()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label14.text") + ": " + this.labelManager.getString("Quadro04Panel.label15.text"));
        this.render.addNewLine();
        ArrayList<String> headers3 = new ArrayList<String>();
        AnexoAq04T4B_LinhaAdapterFormat tableAdapter3 = new AnexoAq04T4B_LinhaAdapterFormat();
        for (int i3 = 1; i3 <= tableAdapter3.getColumnCount(); ++i3) {
            String colName = tableAdapter3.getColumnName(i3);
            if (colName == null) continue;
            headers3.add(colName);
        }
        EventList<AnexoAq04T4B_Linha> list3 = quadro4.getAnexoAq04T4B();
        ArrayList<String[]> linhas3 = new ArrayList<String[]>();
        for (int i4 = 0; i4 < list3.size(); ++i4) {
            AnexoAq04T4B_Linha linha = list3.get(i4);
            String[] campos = new String[3];
            campos[0] = linha.getCodDespesa() == null ? "" : linha.getCodDespesa().toString();
            campos[1] = linha.getTitular() == null ? "" : linha.getTitular().toString();
            campos[2] = linha.getValor() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getValor());
            linhas3.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers3, linhas3);
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printTexto(this.render, this.labelManager.getString("Quadro04Panel.label16.text"), 1, 5, 15);
        this.render.addNewLine();
        ArrayList<String> headers4 = new ArrayList<String>();
        AnexoAq04T4Ba_LinhaAdapterFormat tableAdapter4 = new AnexoAq04T4Ba_LinhaAdapterFormat();
        for (int i5 = 1; i5 <= tableAdapter4.getColumnCount(); ++i5) {
            String colName = tableAdapter4.getColumnName(i5);
            if (colName == null) continue;
            headers4.add(colName);
        }
        EventList<AnexoAq04T4Ba_Linha> list4 = quadro4.getAnexoAq04T4Ba();
        ArrayList<String[]> linhas4 = new ArrayList<String[]>();
        for (int i6 = 0; i6 < list4.size(); ++i6) {
            AnexoAq04T4Ba_Linha linha = list4.get(i6);
            String[] campos = new String[6];
            campos[0] = linha.getProfissaoCodigo() == null ? "" : String.valueOf(linha.getProfissaoCodigo());
            campos[1] = linha.getTitular() == null ? "" : linha.getTitular().toString();
            campos[2] = linha.getValor() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getValor());
            campos[3] = linha.getNifNipcPortugues() == null ? "" : linha.getNifNipcPortugues().toString();
            campos[4] = linha.getPais() == null ? "" : linha.getPais().toString();
            campos[5] = linha.getNumeroFiscalUE() == null ? "" : linha.getNumeroFiscalUE().toString();
            linhas4.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers4);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers4.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers4.get(1));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers4.get(2));
        this.render.endCell();
        this.render.startCell(3, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Entidade Gestora");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers4.get(3));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers4.get(4));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers4.get(5));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas4, headers4.size());
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro5 = this.anexoA.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoAq05T5_LinhaAdapterFormat tableAdapter = new AnexoAq05T5_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoAq05T5_Linha> list = quadro5.getAnexoAq05T5();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoAq05T5_Linha linha = list.get(i2);
            String[] campos = new String[5];
            campos[0] = linha.getNif() == null ? "" : linha.getNif().toString();
            campos[1] = linha.getCodRendimentos() == null ? "" : linha.getCodRendimentos().toString();
            campos[2] = linha.getTitular() == null ? "" : linha.getTitular();
            campos[3] = linha.getRendimentos() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimentos());
            campos[4] = linha.getNanos() == null ? "" : linha.getNanos().toString();
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
    }
}

