/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexof;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro07;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;

public class AnexoFPrinter {
    AnexoFModel anexoF;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoFPrinter(AnexoFModel anexoF, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoF = anexoF;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo F");
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
        this.renderQuadro7();
        this.render.addNewLine();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoF.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoFq02C01"), quadro2.getAnexoFq02C01() == null ? "" : quadro2.getAnexoFq02C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.anexoF.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoFq03C02"), quadro3.getAnexoFq03C02() == null ? "" : quadro3.getAnexoFq03C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoFq03C03"), quadro3.getAnexoFq03C03() == null ? "" : quadro3.getAnexoFq03C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.anexoF.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoFq04T1_LinhaAdapterFormat tableAdapter = new AnexoFq04T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoFq04T1_Linha> list = quadro4.getAnexoFq04T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoFq04T1_Linha linha = list.get(i2);
            String[] campos = new String[11];
            campos[0] = linha.getNLinha() == null ? "" : linha.getNLinha().toString();
            campos[1] = linha.getFreguesia() == null ? "" : linha.getFreguesia().toString();
            campos[2] = linha.getTipoPredio() == null ? "" : linha.getTipoPredio();
            campos[3] = linha.getArtigo() == null ? "" : linha.getArtigo().toString();
            campos[4] = linha.getFraccao() == null ? "" : linha.getFraccao().toString();
            campos[5] = linha.getTitular() == null ? "" : linha.getTitular();
            campos[6] = linha.getQuotaParte() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(linha.getQuotaParte(), 2);
            campos[7] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRendas());
            campos[8] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRetencoes());
            campos[9] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[10] = Modelo3IRSPrinterUtilities.convertEuro(linha.getDespesas());
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(new float[]{0.5f, 1.0f, 1.2f, 0.7f, 0.8f, 1.0f, 1.0f, 1.1f, 1.0f, 1.0f, 1.0f, 1.2f});
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(4, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Identifica\u00e7\u00e3o Matricial dos Pr\u00e9dios");
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(5));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(6));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(7));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(8));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(9));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(10));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(4));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{2.0f, 2.0f, 2.0f, 2.0f});
        this.render.emptyCell(1, 0);
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoFq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoFq04C2"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoFq04C3"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoFq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoFq04C1()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoFq04C2()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoFq04C3()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro5 = this.anexoF.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro05Panel.label2.text") + ": " + this.labelManager.getString("Quadro05Panel.label1.text"));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro05Panel.anexoFq05C1_base_label.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoFq05T1_LinhaAdapterFormat tableAdapter = new AnexoFq05T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoFq05T1_Linha> list = quadro5.getAnexoFq05T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoFq05T1_Linha linha = list.get(i2);
            String[] campos = new String[1];
            campos[0] = linha.getCamposQuadro4() == null ? "" : linha.getCamposQuadro4().toString();
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
        this.render.addNewLine();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro05Panel.label3.text") + ": " + this.labelManager.getString("Quadro05Panel.label5.text"));
        this.render.addNewLine();
        Boolean q05B1OP = quadro5.getAnexoFq05B1OP() == null ? null : Boolean.valueOf(quadro5.getAnexoFq05B1OP().equals("S"));
        this.render.startTable(new float[]{10.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0);
        this.render.field(this.labelManager.getString("Quadro05Panel.label4.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 0, 5, 0);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B1OPSim.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q05B1OP != null && q05B1OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B1OPNao.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q05B1OP == null || q05B1OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        Boolean q05B2OP = quadro5.getAnexoFq05B2OP() == null ? null : Boolean.valueOf(quadro5.getAnexoFq05B2OP().equals("S"));
        this.render.startTable(new float[]{10.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B2OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B2OPSim.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q05B2OP != null && q05B2OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B2OPNao.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q05B2OP == null || q05B2OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        Boolean q05B3OP = quadro5.getAnexoFq05B3OP() == null ? null : Boolean.valueOf(quadro5.getAnexoFq05B3OP().equals("S"));
        this.render.startTable(new float[]{10.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0);
        this.render.field(this.labelManager.getString("Quadro05Panel.label6.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 0, 5, 0);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B3OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B3OPSim.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q05B3OP != null && q05B3OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.anexoFq05B3OPNao.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q05B3OP == null || q05B3OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro6 = this.anexoF.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoFq06T1_LinhaAdapterFormat tableAdapter = new AnexoFq06T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoFq06T1_Linha> list = quadro6.getAnexoFq06T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoFq06T1_Linha linha = list.get(i2);
            String[] campos = new String[6];
            campos[0] = linha.getTitular() == null ? "" : linha.getTitular().toString();
            campos[1] = linha.getRendaRecebida() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendaRecebida()).toString();
            campos[2] = linha.getRetencoes() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRetencoes()).toString();
            campos[3] = linha.getNifSublocatario() == null ? "" : linha.getNifSublocatario().toString();
            campos[4] = linha.getRendaPaga() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendaPaga()).toString();
            campos[5] = linha.getNifSenhorio() == null ? "" : linha.getNifSenhorio().toString();
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(4));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(5));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
    }

    private void renderQuadro7() throws DocumentException {
        Quadro07 quadro7 = this.anexoF.getQuadro07();
        String quadroName = "Quadro07";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoFq07T1_LinhaAdapterFormat tableAdapter = new AnexoFq07T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoFq07T1_Linha> list = quadro7.getAnexoFq07T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoFq07T1_Linha linha = list.get(i2);
            String[] campos = new String[3];
            campos[0] = linha.getCampoQ4() == null ? "" : linha.getCampoQ4().toString();
            campos[1] = linha.getRendimento() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimento()).toString();
            campos[2] = linha.getNanos() == null ? "" : linha.getNanos().toString();
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
    }
}

