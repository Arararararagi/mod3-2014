/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexoe;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;

public class AnexoEPrinter {
    AnexoEModel anexoE;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoEPrinter(AnexoEModel anexoE, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoE = anexoE;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo E");
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoE.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoEq02C01"), quadro2.getAnexoEq02C01() == null ? "" : quadro2.getAnexoEq02C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.anexoE.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoEq03C02"), quadro3.getAnexoEq03C02() == null ? "" : quadro3.getAnexoEq03C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoEq03C03"), quadro3.getAnexoEq03C03() == null ? "" : quadro3.getAnexoEq03C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.anexoE.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label13.text") + ": " + this.labelManager.getString("Quadro04Panel.label3.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoEq04T1_LinhaAdapterFormat tableAdapter = new AnexoEq04T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoEq04T1_Linha> list = quadro4.getAnexoEq04T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoEq04T1_Linha linha = list.get(i2);
            String[] campos = new String[5];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = linha.getCodRendimentos() == null ? "" : linha.getCodRendimentos();
            campos[2] = linha.getTitular() == null ? "" : linha.getTitular();
            campos[3] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimentos());
            campos[4] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRetencoes());
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 1.0f, 2.0f, 2.0f, 1.0f});
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoEq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoEq04C2"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(2, 1, 2, 5, 0);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoEq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoEq04C1()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoEq04C2()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label14.text") + ": " + this.labelManager.getString("Quadro04Panel.label4.text"));
        this.render.addNewLine();
        ArrayList<String> headers2 = new ArrayList<String>();
        AnexoEq04T2_LinhaAdapterFormat tableAdapter2 = new AnexoEq04T2_LinhaAdapterFormat();
        for (int i3 = 1; i3 <= tableAdapter2.getColumnCount(); ++i3) {
            String colName = tableAdapter2.getColumnName(i3);
            if (colName == null) continue;
            headers2.add(colName);
        }
        EventList<AnexoEq04T2_Linha> list2 = quadro4.getAnexoEq04T2();
        ArrayList<String[]> linhas2 = new ArrayList<String[]>();
        for (int i4 = 0; i4 < list2.size(); ++i4) {
            AnexoEq04T2_Linha linha = list2.get(i4);
            String[] campos = new String[5];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = linha.getCodRendimentos() == null ? "" : linha.getCodRendimentos();
            campos[2] = linha.getTitular() == null ? "" : linha.getTitular();
            campos[3] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimentos());
            campos[4] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRetencoes());
            linhas2.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers2, linhas2);
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 1.0f, 2.0f, 2.0f, 1.0f});
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoEq04C3"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoEq04C4"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(2, 1, 2, 5, 0);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoEq04C3"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoEq04C3()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoEq04C4()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        Boolean q04B1OP = quadro4.getAnexoEq04B1() == null ? null : Boolean.valueOf(quadro4.getAnexoEq04B1().equals("1"));
        this.render.startTable(new float[]{10.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoEq04B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoEq04B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q04B1OP != null && q04B1OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoEq04B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q04B1OP == null || q04B1OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
    }
}

