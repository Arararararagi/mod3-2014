/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexod;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro08;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.taxclient.model.FormKey;

public class AnexoDPrinter {
    AnexoDModel anexoD;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoDPrinter(AnexoDModel anexoD, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoD = anexoD;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo D (" + this.anexoD.getFormKey().getSubId() + ")");
        this.render.addNewLine();
        this.renderQuadro1();
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
        this.renderQuadro7();
        this.render.addNewLine();
        this.renderQuadro8();
        this.render.addNewLine();
    }

    private void renderQuadro1() throws DocumentException {
        Quadro01 quadro01 = this.anexoD.getQuadro01();
        String quadroName = "Quadro01";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        float[] cols = new float[]{5.0f, 1.0f, 15.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label4.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (quadro01.isAnexoDq01B1Selected()) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoDq01B1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label5.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (quadro01.isAnexoDq01B2Selected()) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoDq01B2.text"));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoD.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoDq02C03"), quadro2.getAnexoDq02C03() == null ? "" : quadro2.getAnexoDq02C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.anexoD.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoDq03C04"), quadro3.getAnexoDq03C04() == null ? "" : quadro3.getAnexoDq03C04().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoDq03C05"), quadro3.getAnexoDq03C05() == null ? "" : quadro3.getAnexoDq03C05().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label12.text") + ": " + this.labelManager.getString("Quadro03Panel.label13.text"));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoDq03C06"), quadro3.getAnexoDq03C06() == null ? "" : quadro3.getAnexoDq03C06().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.anexoD.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label17.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 2.0f, 2.0f, 2.0f, 2.0f, 2.0f, 2.0f});
        String[][] lines1 = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoDq04C401"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C401a"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C401b"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C401c"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C401d"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C401e")}, {this.labelManager.getCampoNumber(quadroName, "anexoDq04C401"), Modelo3IRSPrinterUtilities.convertLong(quadro4.getAnexoDq04C401()), Modelo3IRSPrinterUtilities.convertPercent(quadro4.getAnexoDq04C401a(), 2), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C401b()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C401c()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C401d()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C401e())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq04C402"), Modelo3IRSPrinterUtilities.convertLong(quadro4.getAnexoDq04C402()), Modelo3IRSPrinterUtilities.convertPercent(quadro4.getAnexoDq04C402a(), 2), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C402b()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C402c()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C402d()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C402e())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq04C403"), Modelo3IRSPrinterUtilities.convertLong(quadro4.getAnexoDq04C403()), Modelo3IRSPrinterUtilities.convertPercent(quadro4.getAnexoDq04C403a(), 2), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C403b()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C403c()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C403d()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C403e())}};
        int[][] horizAligns1 = new int[][]{{1, 1, 1, 1, 1, 1}, {1, 2, 2, 2, 2, 2, 2}, {1, 2, 2, 2, 2, 2, 2}, {1, 2, 2, 2, 2, 2, 2}};
        int[][] colSpans1 = new int[][]{{2, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines1, horizAligns1, colSpans1);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label20.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 2.0f, 2.0f, 2.0f, 2.0f, 2.0f, 2.0f});
        String[][] lines2 = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoDq04C431"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C431a"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C431b"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C431c"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C431d"), this.labelManager.getCampoLabel(quadroName, "anexoDq04C431e")}, {this.labelManager.getCampoNumber(quadroName, "anexoDq04C431"), Modelo3IRSPrinterUtilities.convertLong(quadro4.getAnexoDq04C431()), Modelo3IRSPrinterUtilities.convertPercent(quadro4.getAnexoDq04C431a(), 2), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C431b()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C431c()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C431d()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C431e())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq04C432"), Modelo3IRSPrinterUtilities.convertLong(quadro4.getAnexoDq04C432()), Modelo3IRSPrinterUtilities.convertPercent(quadro4.getAnexoDq04C432a(), 2), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C432b()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C432c()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C432d()), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C432e())}};
        int[][] horizAligns2 = new int[][]{{1, 1, 1, 1, 1, 1}, {1, 2, 2, 2, 2, 2, 2}, {1, 2, 2, 2, 2, 2, 2}};
        int[][] colSpans2 = new int[][]{{2, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines2, horizAligns2, colSpans2);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label39.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoDq04T1_LinhaAdapterFormat tableAdapter = new AnexoDq04T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoDq04T1_Linha> list = quadro4.getAnexoDq04T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoDq04T1_Linha linha = list.get(i2);
            String[] campos = new String[4];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = Modelo3IRSPrinterUtilities.convertPercent(linha.getImputacao(), 2);
            campos[2] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimentos());
            campos[3] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRetencao());
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
        this.render.addNewLine();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label40.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{0.5f, 4.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(2, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq04C431"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq04C431a"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq04C431b"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field("Natureza");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field("Valor");
        this.render.endCell();
        String[][] lines3 = new String[][]{{this.labelManager.getCampoNumber(quadroName, "anexoDq04C480b"), this.labelManager.getString("Quadro04Panel.label2.text"), Modelo3IRSPrinterUtilities.convertPercent(quadro4.getAnexoDq04C480a(), 2), this.labelManager.getString("Quadro04Panel.label4.text"), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C480b())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq04C481b"), this.labelManager.getString("Quadro04Panel.label3.text"), Modelo3IRSPrinterUtilities.convertPercent(quadro4.getAnexoDq04C481a(), 2), this.labelManager.getString("Quadro04Panel.label5.text"), Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C481b())}};
        int[][] horizAligns3 = new int[][]{{1, 2, 2, 1, 2}, {1, 2, 2, 1, 2}};
        int[][] colSpans3 = new int[][]{{1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines3, horizAligns3, colSpans3);
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{2.0f, 2.0f, 2.0f, 2.0f, 2.0f});
        this.render.emptyCell(1, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq04C2"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq04C3"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq04C4"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoDq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C1()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C2()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C3()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoDq04C4()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label6.text") + ": " + this.labelManager.getString("Quadro04Panel.label7.text"));
        this.render.addNewLine();
        ArrayList<String> headers2 = new ArrayList<String>();
        AnexoDq04T2_LinhaAdapterFormat tableAdapter2 = new AnexoDq04T2_LinhaAdapterFormat();
        for (int i3 = 1; i3 <= tableAdapter2.getColumnCount(); ++i3) {
            String colName = tableAdapter2.getColumnName(i3);
            if (colName == null) continue;
            headers2.add(colName);
        }
        EventList<AnexoDq04T2_Linha> list2 = quadro4.getAnexoDq04T2();
        ArrayList<String[]> linhas2 = new ArrayList<String[]>();
        for (int i4 = 0; i4 < list2.size(); ++i4) {
            AnexoDq04T2_Linha linha = list2.get(i4);
            String[] campos = new String[4];
            campos[0] = linha.getCampoQ4() == null ? "" : linha.getCampoQ4().toString();
            campos[1] = linha.getCodigoDoPais() == null ? "" : linha.getCodigoDoPais().toString();
            campos[2] = linha.getMontanteDoRendimento() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getMontanteDoRendimento());
            campos[3] = linha.getValor() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getValor());
            linhas2.add(campos);
        }
        float[] colsSize2 = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers2);
        this.render.startTable(colsSize2);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Identifica\u00e7\u00e3o do Pa\u00eds");
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers2.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Imposto Pago no Estrangeiro");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers2.get(0));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers2.get(1));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers2.get(3));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas2, headers2.size());
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro5 = this.anexoD.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{3.0f, 0.5f, 1.0f, 0.5f, 1.0f, 0.5f, 1.0f, 0.5f, 1.0f});
        String[][] lines = new String[][]{{"###EMPTY_CELL###", this.labelManager.getCampoBaseLabel(quadroName, "anexoDq05C501"), this.labelManager.getCampoBaseLabel(quadroName, "anexoDq05C502"), this.labelManager.getCampoBaseLabel(quadroName, "anexoDq05C503"), this.labelManager.getCampoBaseLabel(quadroName, "anexoDq05C504")}, {this.labelManager.getCampoLabel(quadroName, "anexoDq05C501"), this.labelManager.getCampoNumber(quadroName, "anexoDq05C501"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C501()), this.labelManager.getCampoNumber(quadroName, "anexoDq05C502"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C502()), this.labelManager.getCampoNumber(quadroName, "anexoDq05C503"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C503()), this.labelManager.getCampoNumber(quadroName, "anexoDq05C504"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C504())}, {this.labelManager.getCampoLabel(quadroName, "anexoDq05C505"), this.labelManager.getCampoNumber(quadroName, "anexoDq05C505"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C505()), this.labelManager.getCampoNumber(quadroName, "anexoDq05C506"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C506()), this.labelManager.getCampoNumber(quadroName, "anexoDq05C507"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C507()), this.labelManager.getCampoNumber(quadroName, "anexoDq05C508"), Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoDq05C508())}};
        int[][] horizAligns = new int[][]{{-1, 1, 1, 1, 1}, {0, 1, 2, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{1, 2, 2, 2, 2}, {1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro6 = this.anexoD.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{3.0f, 3.0f, 4.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq06C601"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro6.getAnexoDq06C601()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq06C602"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro6.getAnexoDq06C602()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro7() throws DocumentException {
        Quadro07 quadro07 = this.anexoD.getQuadro07();
        String quadroName = "Quadro07";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro07Panel.label2.text"));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoDq07C701"), quadro07.getAnexoDq07C701() == null ? "" : quadro07.getAnexoDq07C701().toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 2.0f, 1.0f, 2.0f, 1.0f, 2.0f});
        String[][] lines = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoDq07C702"), this.labelManager.getCampoLabel(quadroName, "anexoDq07C708"), this.labelManager.getCampoLabel(quadroName, "anexoDq07C714")}, {this.labelManager.getCampoNumber(quadroName, "anexoDq07C702"), Modelo3IRSPrinterUtilities.convertLong(quadro07.getAnexoDq07C702()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C708"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C708()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C714"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C714())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq07C703"), Modelo3IRSPrinterUtilities.convertLong(quadro07.getAnexoDq07C703()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C709"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C709()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C715"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C715())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq07C704"), Modelo3IRSPrinterUtilities.convertLong(quadro07.getAnexoDq07C704()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C710"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C710()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C716"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C716())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq07C705"), Modelo3IRSPrinterUtilities.convertLong(quadro07.getAnexoDq07C705()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C711"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C711()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C717"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C717())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq07C706"), Modelo3IRSPrinterUtilities.convertLong(quadro07.getAnexoDq07C706()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C712"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C712()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C718"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C718())}, {this.labelManager.getCampoNumber(quadroName, "anexoDq07C707"), Modelo3IRSPrinterUtilities.convertLong(quadro07.getAnexoDq07C707()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C713"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C713()), this.labelManager.getCampoNumber(quadroName, "anexoDq07C719"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoDq07C719())}};
        int[][] horizAligns = new int[][]{{1, 1, 1}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{2, 2, 2}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
    }

    private void renderQuadro8() throws DocumentException {
        Quadro08 quadro8 = this.anexoD.getQuadro08();
        String quadroName = "Quadro08";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{3.0f, 3.0f, 4.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoDq08C801"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro8.getAnexoDq08C801()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }
}

