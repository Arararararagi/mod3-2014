/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.rosto;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_Rosto;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_RostoQ5B_cp13;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro01;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.pdf.DynamicPDFRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.util.Date;
import pt.opensoft.util.ListMap;

public class RostoPrinter {
    RostoModel rosto;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;
    private static final String PREFIX_DG = "DG";
    private static final String PREFIX_AS = "AC";
    private static final String PREFIX_AF = "AF";

    public RostoPrinter(RostoModel rosto, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.rosto = rosto;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Rosto");
        this.render.addNewLine();
        this.renderQuadro1();
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
        this.renderQuadro7();
        this.render.addNewLine();
        this.renderQuadro9();
    }

    private void renderQuadro1() throws DocumentException {
        Quadro01 quadro1 = this.rosto.getQuadro01();
        String quadroName = "Quadro01";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q01C01"), quadro1.getQ01C01() == null ? "" : quadro1.getQ01C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.rosto.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q02C02"), quadro2.getQ02C02() == null ? "" : quadro2.getQ02C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.rosto.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label2.text") + ": " + this.labelManager.getString("Quadro03Panel.label3.text"));
        this.render.startTable(Modelo3IRSPrinterUtilities.LabelLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03C01"), quadro3.getQ03C01() == null ? "" : quadro3.getQ03C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.startTable(Modelo3IRSPrinterUtilities.LabelLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03C02"), quadro3.getQ03C02() == null ? "" : quadro3.getQ03C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 3.0f, 3.0f, 1.0f, 2.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.label6.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.q03C03a_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.q03B03_base_label.text"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03C03"), quadro3.getQ03C03() == null ? "" : quadro3.getQ03C03().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(quadro3.getQ03C03a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03C03a()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.printerUtils.convertBoolean(quadro3.getQ03B03()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03C04"), quadro3.getQ03C04() == null ? "" : quadro3.getQ03C04().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(quadro3.getQ03C04a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03C04a()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.printerUtils.convertBoolean(quadro3.getQ03B04()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label14.text") + ": " + this.labelManager.getString("Quadro03Panel.label15.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 3.5f, 3.5f, 2.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD1"), quadro3.getQ03CD1() == null ? "" : quadro3.getQ03CD1().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD8"), quadro3.getQ03CD8() == null ? "" : quadro3.getQ03CD8().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD2"), quadro3.getQ03CD2() == null ? "" : quadro3.getQ03CD2().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD9"), quadro3.getQ03CD9() == null ? "" : quadro3.getQ03CD9().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD3"), quadro3.getQ03CD3() == null ? "" : quadro3.getQ03CD3().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD10"), quadro3.getQ03CD10() == null ? "" : quadro3.getQ03CD10().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD4"), quadro3.getQ03CD4() == null ? "" : quadro3.getQ03CD4().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD11"), quadro3.getQ03CD11() == null ? "" : quadro3.getQ03CD11().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD5"), quadro3.getQ03CD5() == null ? "" : quadro3.getQ03CD5().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD12"), quadro3.getQ03CD12() == null ? "" : quadro3.getQ03CD12().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD6"), quadro3.getQ03CD6() == null ? "" : quadro3.getQ03CD6().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD13"), quadro3.getQ03CD13() == null ? "" : quadro3.getQ03CD13().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD7"), quadro3.getQ03CD7() == null ? "" : quadro3.getQ03CD7().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CD14"), quadro3.getQ03CD14() == null ? "" : quadro3.getQ03CD14().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label45.text") + ": " + this.labelManager.getString("Quadro03Panel.label46.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 3.5f, 3.5f, 2.0f});
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD1a"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD1"), quadro3.getQ03CDD1() == null ? "" : quadro3.getQ03CDD1().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD1a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD1a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD2"), quadro3.getQ03CDD2() == null ? "" : quadro3.getQ03CDD2().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD2a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD2a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD3"), quadro3.getQ03CDD3() == null ? "" : quadro3.getQ03CDD3().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD3a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD3a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD4"), quadro3.getQ03CDD4() == null ? "" : quadro3.getQ03CDD4().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD4a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD4a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD5"), quadro3.getQ03CDD5() == null ? "" : quadro3.getQ03CDD5().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD5a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD5a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD6"), quadro3.getQ03CDD6() == null ? "" : quadro3.getQ03CDD6().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD6a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD6a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD7"), quadro3.getQ03CDD7() == null ? "" : quadro3.getQ03CDD7().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD7a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD7a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q03CDD8"), quadro3.getQ03CDD8() == null ? "" : quadro3.getQ03CDD8().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro3.getQ03CDD8a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro3.getQ03CDD8a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label47.text") + ": " + this.labelManager.getString("Quadro03Panel.label52.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        Rostoq03DT1_LinhaAdapterFormat tableAdapter = new Rostoq03DT1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<Rostoq03DT1_Linha> list = quadro3.getRostoq03DT1();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            Rostoq03DT1_Linha linha = list.get(i2);
            String[] campos = new String[3];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = linha.getDeficienteGrau() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(linha.getDeficienteGrau());
            campos[2] = linha.getNifProgenitor() == null ? "" : linha.getNifProgenitor().toString();
            linhas.add(campos);
        }
        this.render.startTable(new float[]{1.0f, 3.0f, 1.2f, 3.0f, 2.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Identifica\u00e7\u00e3o dos Dependentes");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Deficientes");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Identifica\u00e7\u00e3o do Outro Progenitor");
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headers.get(2));
        this.render.endCell();
        this.render.emptyCell();
        Modelo3IRSPrinterUtilities.printTabelaContentWithPaddingAndPrefix(this.render, linhas, headers.size(), 0, 1, "DG");
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.rosto.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        String value1 = quadro4.getQ04B1() == null ? "" : quadro4.getQ04B1().toString();
        float[] cols = new float[]{5.0f, 1.0f, 15.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("1")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.q04B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.label3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("2")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.q04B1OP2.text"));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro5 = this.rosto.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro05Panel.label2.text") + ": " + this.labelManager.getString("Quadro05Panel.label3.text"));
        String value1 = quadro5.getQ05B1() == null ? "" : quadro5.getQ05B1().toString();
        String value2 = quadro5.getQ05B2() == null ? "" : quadro5.getQ05B2().toString();
        String value3 = quadro5.getQ05B3() == null ? "" : quadro5.getQ05B3().toString();
        String value4 = quadro5.getQ05B4() == null ? "" : quadro5.getQ05B4().toString();
        float[] cols = new float[]{1.0f, 1.0f, 19.0f};
        float[] cols_tab1 = new float[]{2.0f, 1.0f, 18.0f};
        float[] cols_tab2 = new float[]{3.0f, 1.0f, 17.0f};
        float[] cols_tab3 = new float[]{4.0f, 1.0f, 16.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label4.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("1")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label6.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("2")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label8.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("3")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B1OP3.text"));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro05Panel.label10.text") + ": " + this.labelManager.getString("Quadro05Panel.label11.text"));
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label12.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("4")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B1OP4.text"));
        this.render.endCell();
        this.render.endTable();
        this.render.startTable(Modelo3IRSPrinterUtilities.LabelLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q05C5"), quadro5.getQ05C5() == null ? "" : quadro5.getQ05C5().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_Rosto.class.getSimpleName());
        this.render.field(this.labelManager.getString("Quadro05Panel.q05C5_1_label.text_2"), quadro5.getQ05C5_1() == null ? "" : paisesCatalog.get(quadro5.getQ05C5_1()).toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        Modelo3IRSPrinterUtilities.printTexto(this.render, this.labelManager.getString("Quadro05Panel.label16.text"));
        this.render.addNewLine();
        this.render.startTable(cols_tab1);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label17.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value2.equals("6")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B2OP6.text"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell(2, 1);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label18.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value2.equals("7")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B2OP7.text"));
        this.render.endCell();
        this.render.endTable();
        this.render.startTable(cols_tab2);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label19.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value3.equals("8")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B3OP8.text"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell(2, 1);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label21.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value3.equals("9")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B3OP9.text"));
        this.render.endCell();
        this.render.endTable();
        this.render.startTable(cols_tab3);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label23.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value4.equals("10")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B4OP10.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.label24.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value4.equals("11")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro05Panel.q05B4OP11.text"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell(2, 1);
        this.render.endTable();
        this.render.startTable(new float[]{0.0f, 12.0f, 9.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q05C12"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(quadro5.getQ05C12() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro5.getQ05C12()));
        this.render.endCell();
        ListMap paisesCatalogcp13 = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_RostoQ5B_cp13.class.getSimpleName());
        this.render.emptyCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q05C13"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(quadro5.getQ05C13() == null ? "" : paisesCatalogcp13.get(quadro5.getQ05C13()).toString());
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro6 = this.rosto.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        String value1 = quadro6.getQ06B1() == null ? "" : quadro6.getQ06B1().toString();
        float[] cols = new float[]{5.0f, 1.0f, 15.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("1")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("2")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label4.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("3")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B1OP3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label5.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("4")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B1OP4.text"));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro7() throws DocumentException {
        Quadro07 quadro7 = this.rosto.getQuadro07();
        String quadroName = "Quadro07";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro07Panel.label2.text") + ": " + this.labelManager.getString("Quadro07Panel.label3.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{3.0f, 2.0f, 1.0f, 1.0f, 1.0f});
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.label1.text"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.label5.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.label6.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q07C1") + ":", DynamicPDFRenderer.boldText9);
        this.render.endCell();
        this.render.startCell(1, 1, 4, 5, 0, true);
        this.render.field(quadro7.getQ07C1() == null ? "" : quadro7.getQ07C1().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(quadro7.getQ07C1a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro7.getQ07C1a()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.printerUtils.convertBoolean(quadro7.getQ07C1b()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro07Panel.label8.text") + ": " + this.labelManager.getString("Quadro07Panel.label9.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 1.75f, 1.5f, 0.75f, 1.75f, 1.5f, 1.75f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q07C01"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q07C01a"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q07C01"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q07C01a"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.label14.text"), quadro7.getQ07C01() == null ? "" : quadro7.getQ07C01().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro7.getQ07C01a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro7.getQ07C01a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.label15.text"), quadro7.getQ07C02() == null ? "" : quadro7.getQ07C02().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro7.getQ07C02a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro7.getQ07C02a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.label16.text"), quadro7.getQ07C03() == null ? "" : quadro7.getQ07C03().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro7.getQ07C03a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro7.getQ07C03a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.label17.text"), quadro7.getQ07C04() == null ? "" : quadro7.getQ07C04().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro7.getQ07C04a() == null ? "" : Modelo3IRSPrinterUtilities.convertPercent(quadro7.getQ07C04a()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro07Panel.label20.text") + ": " + this.labelManager.getString("Quadro07Panel.label21.text"));
        ArrayList<String> headers7ET1 = new ArrayList<String>();
        Rostoq07ET1_LinhaAdapterFormat rostoq07CE1ta = new Rostoq07ET1_LinhaAdapterFormat();
        for (int i = 1; i <= rostoq07CE1ta.getColumnCount(); ++i) {
            String colName = rostoq07CE1ta.getColumnName(i);
            if (colName == null) continue;
            headers7ET1.add(colName);
        }
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        EventList<Rostoq07ET1_Linha> rostoq07ET1_Linha = quadro7.getRostoq07ET1();
        for (Rostoq07ET1_Linha linhaQ7ET1 : rostoq07ET1_Linha) {
            String[] campos = new String[1];
            campos[0] = linhaQ7ET1.getNIF() == null ? "" : String.valueOf(linhaQ7ET1.getNIF());
            linhas.add(campos);
        }
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printTabelaWithPrefix(this.render, headers7ET1, linhas, new float[]{1.0f, 3.0f}, 4.0f, 4.0f, "AC");
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro07Panel.label10.text") + ": " + this.labelManager.getString("Quadro07Panel.label11.text"));
        ArrayList<String> headersQ7CT1 = new ArrayList<String>();
        Rostoq07CT1_LinhaAdapterFormat rostoq07CT1ta = new Rostoq07CT1_LinhaAdapterFormat();
        for (int i2 = 1; i2 <= rostoq07CT1ta.getColumnCount(); ++i2) {
            String colName = rostoq07CT1ta.getColumnName(i2);
            if (colName == null) continue;
            headersQ7CT1.add(colName);
        }
        ArrayList<String[]> linhas07CT1 = new ArrayList<String[]>();
        EventList<Rostoq07CT1_Linha> rostoq07CT1_Linha = quadro7.getRostoq07CT1();
        for (Rostoq07CT1_Linha linhaQ7CT1 : rostoq07CT1_Linha) {
            String[] campos = new String[1];
            campos[0] = linhaQ7CT1.getNIF() == null ? "" : String.valueOf(linhaQ7CT1.getNIF());
            linhas07CT1.add(campos);
        }
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printTabelaWithPrefix(this.render, headersQ7CT1, linhas07CT1, new float[]{1.0f, 3.0f}, 4.0f, 4.0f, "AF");
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro07Panel.label18.text") + ": " + this.labelManager.getString("Quadro07Panel.label19.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.6f, 2.4f});
        this.render.startCell(1, 1, 0, 6, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "q07D01"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 0, 6, 0, true);
        this.render.field(quadro7.getQ07D01() == null ? "" : quadro7.getQ07D01().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.q07D01a.text"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.printerUtils.convertBoolean(quadro7.getQ07D01a()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro9() throws DocumentException {
        Quadro09 quadro9 = this.rosto.getQuadro09();
        String quadroName = "Quadro09";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        String value1 = quadro9.getQ09B1() == null ? "" : quadro9.getQ09B1().toString();
        float[] cols = new float[]{5.0f, 1.0f, 15.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.q09B1OP1_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("1")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.q09B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.q09B1OP2_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("2")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.q09B1OP2.text"));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{5.0f, 2.0f, 5.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.q09C05_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.q09C05_label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(quadro9.getQ09C03() == null ? "" : quadro9.getQ09C03().format());
        this.render.endCell();
        this.render.endTable();
    }
}

