/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexoh;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxH;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxH_Q04;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro10;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.util.ListMap;

public class AnexoHPrinter {
    AnexoHModel anexoH;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoHPrinter(AnexoHModel anexoH, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoH = anexoH;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo H");
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
        this.renderQuadro7();
        this.render.addNewLine();
        this.renderQuadro8();
        this.render.addNewLine();
        this.renderQuadro9();
        this.render.addNewLine();
        this.renderQuadro10();
        this.render.addNewLine();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoH.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoHq02C01"), quadro2.getAnexoHq02C01() == null ? "" : quadro2.getAnexoHq02C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.anexoH.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoHq03C02"), quadro3.getAnexoHq03C02() == null ? "" : quadro3.getAnexoHq03C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoHq03C03"), quadro3.getAnexoHq03C03() == null ? "" : quadro3.getAnexoHq03C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.anexoH.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoHq04T4_LinhaAdapterFormat tableAdapter = new AnexoHq04T4_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoHq04T4_Linha> list = quadro4.getAnexoHq04T4();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoHq04T4_Linha linha = list.get(i2);
            String[] campos = new String[7];
            campos[0] = linha.getCodRendimentos() == null ? "" : linha.getCodRendimentos().toString();
            campos[1] = linha.getTitular() == null ? "" : linha.getTitular().toString();
            campos[2] = linha.getRendimentosIliquidos() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimentosIliquidos());
            campos[3] = linha.getRentencaoIRS() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRentencaoIRS());
            campos[4] = linha.getNifPortugues() == null ? "" : linha.getNifPortugues().toString();
            ListMap catalogPais = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxH_Q04.class.getSimpleName());
            campos[5] = linha.getPais() == null ? "" : catalogPais.get(linha.getPais()).toString();
            campos[6] = linha.getNumeroFiscalUE() == null ? "" : String.valueOf(linha.getNumeroFiscalUE());
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 1.0f, 2.0f, 2.0f, 1.0f});
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq04C2"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 2, 5, 0);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(quadro4.getAnexoHq04C1() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoHq04C1()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15);
        this.render.field(quadro4.getAnexoHq04C2() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoHq04C2()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro5 = this.anexoH.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoHq05T5_LinhaAdapterFormat tableAdapter = new AnexoHq05T5_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoHq05T5_Linha> list = quadro5.getAnexoHq05T5();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoHq05T5_Linha linha = list.get(i2);
            String[] campos = new String[2];
            campos[0] = linha.getTitular() == null ? "" : linha.getTitular().toString();
            campos[1] = linha.getMontanteRendimento() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getMontanteRendimento());
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
        this.render.addNewLine();
        this.render.startTable(new float[]{3.0f, 3.0f, 3.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq05C1"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq05C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro5.getAnexoHq05C1() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoHq05C1()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro6 = this.anexoH.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        this.render.emptyCell(0);
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, false);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq06C601"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, false);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq06C601"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, false);
        this.render.field(quadro6.getAnexoHq06C601() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro6.getAnexoHq06C601()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, false);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq06C602"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, false);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq06C602"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, false);
        this.render.field(quadro6.getAnexoHq06C602() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro6.getAnexoHq06C602()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, false);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq06C603"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, false);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq06C603"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, false);
        this.render.field(quadro6.getAnexoHq06C603() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro6.getAnexoHq06C603()));
        this.render.endCell();
        this.render.startCell(2, 1, 2, 5, 0, false);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq06C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, false);
        this.render.field(quadro6.getAnexoHq06C1() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro6.getAnexoHq06C1()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoHq06T1_LinhaAdapterFormat tableAdapter = new AnexoHq06T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoHq06T1_Linha> list = quadro6.getAnexoHq06T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoHq06T1_Linha linha = list.get(i2);
            String[] campos = new String[2];
            campos[0] = linha.getNifBeneficiarios() == null ? "" : linha.getNifBeneficiarios().toString();
            campos[1] = linha.getValor() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getValor());
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
    }

    private void renderQuadro7() throws DocumentException {
        Quadro07 quadro7 = this.anexoH.getQuadro07();
        String quadroName = "Quadro07";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoHq07T7_LinhaAdapterFormat tableAdapter = new AnexoHq07T7_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoHq07T7_Linha> list = quadro7.getAnexoHq07T7();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoHq07T7_Linha linha = list.get(i2);
            String[] campos = new String[6];
            campos[0] = linha.getCodBeneficio() == null ? "" : linha.getCodBeneficio().toString();
            campos[1] = linha.getTitular() == null ? "" : linha.getTitular().toString();
            campos[2] = linha.getImportanciaAplicada() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getImportanciaAplicada());
            campos[3] = linha.getNifNipcPortugues() == null ? "" : linha.getNifNipcPortugues().toString();
            campos[4] = linha.getPais() == null ? "" : linha.getPais().toString();
            campos[5] = linha.getNumeroFiscalUE() == null ? "" : linha.getNumeroFiscalUE().toString();
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(3, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Entidade Gestora / Donat\u00e1ria / Senhorio / Locador");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(4));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(5));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{3.0f, 3.0f, 3.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq07C1"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq07C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro7.getAnexoHq07C1() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro7.getAnexoHq07C1()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro8() throws DocumentException {
        Quadro08 quadro8 = this.anexoH.getQuadro08();
        String quadroName = "Quadro08";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headersHq08T1 = new ArrayList<String>();
        ArrayList<String[]> linhasHq08T1 = new ArrayList<String[]>();
        AnexoHq08T1_LinhaAdapterFormat tableAdapterHq08T1 = new AnexoHq08T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapterHq08T1.getColumnCount(); ++i) {
            String colName = tableAdapterHq08T1.getColumnName(i);
            if (colName == null) continue;
            headersHq08T1.add(colName);
        }
        EventList<AnexoHq08T1_Linha> listHq08T1 = quadro8.getAnexoHq08T1();
        for (int i2 = 0; i2 < listHq08T1.size(); ++i2) {
            AnexoHq08T1_Linha linha = listHq08T1.get(i2);
            String[] campos = new String[4];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = linha.getAnexoHq08C801() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getAnexoHq08C801());
            campos[2] = linha.getAnexoHq08C802() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getAnexoHq08C802());
            campos[3] = linha.getAnexoHq08C803() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getAnexoHq08C803());
            linhasHq08T1.add(campos);
        }
        this.render.startTable(new float[]{1.0f, 1.0f, 2.0f, 2.0f, 2.0f, 2.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headersHq08T1.get(0));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headersHq08T1.get(1));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headersHq08T1.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headersHq08T1.get(3));
        this.render.endCell();
        this.render.emptyCell();
        Modelo3IRSPrinterUtilities.printTabelaContentWithPadding(this.render, linhasHq08T1, headersHq08T1.size(), 0, 1);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printTexto(this.render, this.labelManager.getString("Quadro08Panel.label35.text") + ": " + this.labelManager.getString("Quadro08Panel.label14.text"), 0, 5, 15);
        this.render.addNewLine();
        ArrayList<String> headersHq08T814 = new ArrayList<String>();
        AnexoHq08T814_LinhaAdapterFormat tableAdapterHq08T814 = new AnexoHq08T814_LinhaAdapterFormat();
        for (int i3 = 1; i3 <= tableAdapterHq08T814.getColumnCount(); ++i3) {
            String colName = tableAdapterHq08T814.getColumnName(i3);
            if (colName == null) continue;
            headersHq08T814.add(colName);
        }
        EventList<AnexoHq08T814_Linha> listHq08T814 = quadro8.getAnexoHq08T814();
        ArrayList<String[]> linhasHq08T814 = new ArrayList<String[]>();
        for (int i4 = 0; i4 < listHq08T814.size(); ++i4) {
            AnexoHq08T814_Linha linha = listHq08T814.get(i4);
            String[] campos = new String[9];
            campos[0] = linha.getCodigo() == null ? "" : linha.getCodigo().toString();
            campos[1] = linha.getFreguesia() == null ? "" : linha.getFreguesia().toString();
            campos[2] = linha.getTipoPredio() == null ? "" : linha.getTipoPredio().toString();
            campos[3] = linha.getArtigo() == null ? "" : linha.getArtigo().toString();
            campos[4] = linha.getFraccao() == null ? "" : linha.getFraccao().toString();
            campos[5] = linha.getTitular() == null ? "" : linha.getTitular().toString();
            campos[6] = linha.getHabitacaoPermanenteArrendada() == null ? "" : linha.getHabitacaoPermanenteArrendada().toString();
            campos[7] = linha.getNIFArrendatario() == null ? "" : linha.getNIFArrendatario().toString();
            campos[8] = linha.getClassificacaoA() == null ? "" : linha.getClassificacaoA().toString();
            linhasHq08T814.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headersHq08T814, linhasHq08T814, new float[]{0.5f, 0.7f, 0.9f, 0.5f, 0.9f, 0.9f, 0.6f, 1.0f, 1.0f, 1.0f});
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 3.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoHq08C814"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxH.class.getSimpleName());
        this.render.field(quadro8.getAnexoHq08C814() == null ? "" : paisesCatalog.get(quadro8.getAnexoHq08C814()).toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printTexto(this.render, this.labelManager.getString("Quadro08Panel.label46.text") + ": " + this.labelManager.getString("Quadro08Panel.label47.text"), 0, 5, 15);
        this.render.addNewLine();
        this.render.startTable(new float[]{5.0f, 3.0f, 2.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoHq08C81501"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(quadro8.getAnexoHq08C81501() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro8.getAnexoHq08C81501()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoHq08C81502"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(quadro8.getAnexoHq08C81502() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro8.getAnexoHq08C81502()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro9() throws DocumentException {
        Quadro09 quadro9 = this.anexoH.getQuadro09();
        String quadroName = "Quadro09";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro09Panel.label2.text"));
        this.render.addNewLine();
        String value1 = quadro9.getAnexoHq09B1() == null ? "" : quadro9.getAnexoHq09B1().toString();
        this.render.startTable(new float[]{9.0f, 0.5f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.anexoHq09B1OP1_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (value1.equals("1")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(2, 0);
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.anexoHq09B1OP2_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (value1.equals("2")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoHq09C901"), quadro9.getAnexoHq09C901() == null ? "" : quadro9.getAnexoHq09C901().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.startTable(new float[]{9.0f, 0.5f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.anexoHq09C901_label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.printerUtils.convertBoolean(quadro9.getAnexoHq09C901IRS()));
        this.render.endCell();
        this.render.startCell(2, 0);
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro09Panel.anexoHq09C901_label3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.printerUtils.convertBoolean(quadro9.getAnexoHq09C901IVA()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
    }

    private void renderQuadro10() throws DocumentException {
        Quadro10 quadro10 = this.anexoH.getQuadro10();
        String quadroName = "Quadro10";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{6.4f, 0.6f, 1.5f, 1.5f});
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq10C1001"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoHq10C1001a"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1001"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1001"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1001() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1001()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1001a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1001a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1002"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1002"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1002() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1002()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1002a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1002a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1003"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1003"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1003() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1003()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1003a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1003a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1004"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1004"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1004() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1004()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1004a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1004a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1005"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1005"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1005() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1005()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1005a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1005a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1006"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1006"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1006() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1006()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1006a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1006a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1007"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1007"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1007() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1007()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1007a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1007a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1008"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1008"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1008() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1008()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1008a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1008a()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1009"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoHq10C1009"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1009() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1009()));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(2, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoHq10C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro10.getAnexoHq10C1a() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoHq10C1a()));
        this.render.endCell();
        this.render.endTable();
    }
}

