/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexob;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro10;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.Date;

public class AnexoBPrinter {
    AnexoBModel anexoB;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoBPrinter(AnexoBModel anexoB, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoB = anexoB;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo B (" + this.anexoB.getFormKey().getSubId() + ")");
        this.render.addNewLine();
        this.renderQuadro1();
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
        this.renderQuadro7();
        this.render.addNewLine();
        this.renderQuadro8();
        this.render.addNewLine();
        this.renderQuadro9();
        this.render.addNewLine();
        this.renderQuadro10();
        this.render.addNewLine();
        this.renderQuadro11();
        this.render.addNewLine();
        this.renderQuadro12();
        this.render.addNewLine();
    }

    private void renderQuadro1() throws DocumentException {
        Quadro01 quadro01 = this.anexoB.getQuadro01();
        String quadroName = "Quadro01";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        String value1 = quadro01.getAnexoBq01B1() == null ? "" : quadro01.getAnexoBq01B1();
        boolean value3 = quadro01.getAnexoBq01B3() == null ? false : quadro01.getAnexoBq01B3();
        boolean value4 = quadro01.getAnexoBq01B4() == null ? false : quadro01.getAnexoBq01B4();
        float[] cols = new float[]{5.0f, 1.0f, 15.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("1")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoBq01B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1.equals("2")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoBq01B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label4.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value3) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoBq01B3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label5.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value4) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoBq01B4.text"));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoB.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq02C05"), quadro2.getAnexoBq02C05() == null ? "" : quadro2.getAnexoBq02C05().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro03 = this.anexoB.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq03C06"), quadro03.getAnexoBq03C06() == null ? "" : quadro03.getAnexoBq03C06().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq03C07"), quadro03.getAnexoBq03C07() == null ? "" : quadro03.getAnexoBq03C07().toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label12.text") + ": " + this.labelManager.getString("Quadro03Panel.label13.text"));
        Boolean value1 = quadro03.getAnexoBq03B1() == null ? null : Boolean.valueOf(quadro03.getAnexoBq03B1().equals("1"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (value1 != null && value1.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(value1 == null || value1.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03C08_label.text"), quadro03.getAnexoBq03C08() == null ? "" : quadro03.getAnexoBq03C08().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03C09_label.text"), quadro03.getAnexoBq03C09() == null ? "" : quadro03.getAnexoBq03C09().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03C10_label.text"), quadro03.getAnexoBq03C10() == null ? "" : quadro03.getAnexoBq03C10().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03C11_label.text"), quadro03.getAnexoBq03C11() == null ? "" : quadro03.getAnexoBq03C11().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03C12_label.text"), quadro03.getAnexoBq03C12() == null ? "" : quadro03.getAnexoBq03C12().toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label15.text") + ": " + this.labelManager.getString("Quadro03Panel.label16.text"));
        Boolean value2 = quadro03.getAnexoBq03B13() == null ? null : Boolean.valueOf(quadro03.getAnexoBq03B13().equals("13"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03B13OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03B13OP13.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (value2 != null && value2.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoBq03B13OP14.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(value2 == null || value2.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro04 = this.anexoB.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label2.text") + ": " + this.labelManager.getString("Quadro04Panel.label3.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.5f, 0.75f, 2.0f});
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C401()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C402"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C402"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C402()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C403"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C403"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C403()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C440"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C440"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C440()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C441"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C441"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C441()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C404"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C404"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C404()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C442"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C442"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C442()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C405"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C405"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C405()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C420"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C420"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C420()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C421"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C421"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C421()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C422"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C422"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C422()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C423"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C423"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C423()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C424"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C424"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C424()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C425"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C425"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C425()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C443"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C443"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C443()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C1()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{7.5f, 0.75f, 2.0f});
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.label7.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq04C406"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C406"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C406"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C406()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C407"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C407"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C407()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C2"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C2()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label8.text") + ": " + this.labelManager.getString("Quadro04Panel.label9.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.5f, 0.75f, 2.0f});
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq04C409"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C409"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C409"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C409()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C410"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C410"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C410()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C444"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C444"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C444()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C445"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C445"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C445()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C411"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C411"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C411()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C426"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C426"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C426()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C446"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C446"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C446()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C3"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C3()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{7.5f, 0.75f, 2.0f});
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.label21.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq04C413"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C413"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C413"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C413()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C414"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq04C414"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C414()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq04C4"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoBq04C4()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label20.text") + ": " + this.labelManager.getString("Quadro04Panel.label22.text"));
        this.render.addNewLine();
        Boolean q04B1OP = quadro04.getAnexoBq04B1() == null ? null : Boolean.valueOf(quadro04.getAnexoBq04B1().equals("1"));
        Boolean q04B2OP = quadro04.getAnexoBq04B2() == null ? null : Boolean.valueOf(quadro04.getAnexoBq04B2().equals("3"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q04B1OP != null && q04B1OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q04B1OP == null || q04B1OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B2OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B2OP3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q04B2OP != null && q04B2OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B2OP4.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q04B2OP == null || q04B2OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label55.text") + ": " + this.labelManager.getString("Quadro04Panel.label56.text"));
        this.render.addNewLine();
        Boolean q04B3OP = quadro04.getAnexoBq04B3() == null ? null : Boolean.valueOf(quadro04.getAnexoBq04B3().equals("1"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B3OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B3OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q04B3OP != null && q04B3OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro04Panel.anexoBq04B3OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q04B3OP == null || q04B3OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label10.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoBq04T1_LinhaAdapterFormat tableAdapter = new AnexoBq04T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoBq04T1_Linha> list = quadro04.getAnexoBq04T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoBq04T1_Linha linha = list.get(i2);
            String[] campos = new String[8];
            campos[0] = linha.getFreguesia() == null ? "" : linha.getFreguesia().toString();
            campos[1] = linha.getTipoPredio() == null ? "" : linha.getTipoPredio();
            campos[2] = linha.getArtigo() == null ? "" : linha.getArtigo().toString();
            campos[3] = linha.getFraccao() == null ? "" : linha.getFraccao();
            campos[4] = linha.getValorVenda() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getValorVenda());
            campos[5] = linha.getCampoQ4() == null ? "" : linha.getCampoQ4().toString();
            campos[6] = linha.getValorDefinitivo() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getValorDefinitivo());
            campos[7] = linha.getArt139CIRC() == null ? "" : linha.getArt139CIRC();
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label58.text") + ": " + this.labelManager.getString("Quadro04Panel.label12.text"));
        this.render.addNewLine();
        ArrayList<String> headersT2 = new ArrayList<String>();
        AnexoBq04T3_LinhaAdapterFormat tableAdapterT2 = new AnexoBq04T3_LinhaAdapterFormat();
        for (int i3 = 1; i3 <= tableAdapterT2.getColumnCount(); ++i3) {
            String colName = tableAdapterT2.getColumnName(i3);
            if (colName == null) continue;
            headersT2.add(colName);
        }
        EventList<AnexoBq04T3_Linha> listT3 = quadro04.getAnexoBq04T3();
        ArrayList<String[]> linhasT2 = new ArrayList<String[]>();
        for (int i4 = 0; i4 < listT3.size(); ++i4) {
            AnexoBq04T3_Linha linha = listT3.get(i4);
            String[] campos = new String[7];
            campos[0] = linha.getNIPCdasEntidades() == null ? "" : linha.getNIPCdasEntidades().toString();
            campos[1] = linha.getSubsidioDetinadoaExploracao() == null ? "" : linha.getSubsidioDetinadoaExploracao().toString();
            campos[2] = linha.getSubsidiosNaoDestinadosExploracaoN() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN().toString();
            campos[3] = linha.getSubsidiosNaoDestinadosExploracaoN1() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN1().toString();
            campos[4] = linha.getSubsidiosNaoDestinadosExploracaoN2() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN2().toString();
            campos[5] = linha.getSubsidiosNaoDestinadosExploracaoN3() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN3().toString();
            campos[6] = linha.getSubsidiosNaoDestinadosExploracaoN4() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN4().toString();
            linhasT2.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headersT2);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersT2.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersT2.get(1));
        this.render.endCell();
        this.render.startCell(5, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Subs\u00eddios n\u00e3o destinados \u00e0 explora\u00e7\u00e3o");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersT2.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersT2.get(3));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersT2.get(4));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersT2.get(5));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersT2.get(6));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhasT2, headersT2.size());
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro05 = this.anexoB.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq05C501"), quadro05.getAnexoBq05C501() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoBq05C501()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro06 = this.anexoB.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 3.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq06C601"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(quadro06.getAnexoBq06C601() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoBq06C601()));
        this.render.endCell();
        this.render.emptyCell(2, 2);
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq06C602"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(quadro06.getAnexoBq06C601() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoBq06C602()));
        this.render.endCell();
        this.render.emptyCell(2, 2);
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq06C1"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(quadro06.getAnexoBq06C1() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoBq06C1()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro7() throws DocumentException {
        Quadro07 quadro07 = this.anexoB.getQuadro07();
        String quadroName = "Quadro07";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 2.0f, 1.0f, 2.0f, 1.0f, 2.0f, 1.0f, 2.0f});
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.anexoBq07C701_label.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.anexoBq07C702_label.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.anexoBq07C703_label.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro07Panel.anexoBq07C704_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq07C701"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoBq07C701()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq07C702"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoBq07C702()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq07C703"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoBq07C703()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq07C704"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoBq07C704()));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro07Panel.label10.text"));
        this.render.addNewLine();
        AnexoBq07T1_LinhaAdapterFormat tableAdapter = new AnexoBq07T1_LinhaAdapterFormat();
        ArrayList<String> headers = new ArrayList<String>();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoBq07T1_Linha> list = quadro07.getAnexoBq07T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoBq07T1_Linha linha = list.get(i2);
            String[] campos = new String[2];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = Modelo3IRSPrinterUtilities.convertEuro(linha.getValor());
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
    }

    private void renderQuadro8() throws DocumentException {
        Quadro08 quadro08 = this.anexoB.getQuadro08();
        String quadroName = "Quadro08";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro08Panel.label2.text"));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq08C801"), quadro08.getAnexoBq08C801() == null ? "" : quadro08.getAnexoBq08C801().toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 2.0f, 1.0f, 2.0f, 1.0f, 2.0f});
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro08Panel.anexoBq08C802_base_label.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq08C808"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoBq08C814"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro08Panel.label10.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro08.getAnexoBq08C802() == null ? "" : quadro08.getAnexoBq08C802().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C808"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C808()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C814"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C814()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro08Panel.label13.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro08.getAnexoBq08C803() == null ? "" : quadro08.getAnexoBq08C803().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C809"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C809()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C815"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C815()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro08Panel.label16.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro08.getAnexoBq08C804() == null ? "" : quadro08.getAnexoBq08C804().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C810"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C810()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C816"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C816()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro08Panel.label19.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro08.getAnexoBq08C805() == null ? "" : quadro08.getAnexoBq08C805().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C811"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C811()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C817"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C817()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro08Panel.label22.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro08.getAnexoBq08C806() == null ? "" : quadro08.getAnexoBq08C806().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C812"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C812()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C818"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C818()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro08Panel.label25.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(quadro08.getAnexoBq08C807() == null ? "" : quadro08.getAnexoBq08C807().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C813"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C813()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoBq08C819"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoBq08C819()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro9() throws DocumentException {
        Quadro09 quadro09 = this.anexoB.getQuadro09();
        String quadroName = "Quadro09";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.emptyCell(1, 0);
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq09C901"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq09C910"));
        this.render.endCell();
        String[][] lines = new String[][]{{this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C901"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C901"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C901()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C910"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C910())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C902"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C902"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C902()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C911"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C911())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C903"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C903"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C903()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C912"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C912())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C904"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C904"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C904()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C913"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C913())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C905"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C905"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C905()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C914"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C914())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C906"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C906"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C906()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C915"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C915())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C907"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C907"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C907()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C916"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C916())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C908"), this.labelManager.getCampoNumber(quadroName, "anexoBq09C908"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C908()), this.labelManager.getCampoNumber(quadroName, "anexoBq09C917"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C917())}};
        int[][] horizAligns = new int[][]{{0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.emptyCell(1, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoBq09C1a"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C1a()));
        this.render.endCell();
        this.render.emptyCell(1, 0);
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoBq09C1b()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro10() throws DocumentException {
        Quadro10 quadro10 = this.anexoB.getQuadro10();
        String quadroName = "Quadro10";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        String[][] lines = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoBq10C1001"), this.labelManager.getCampoNumber(quadroName, "anexoBq10C1001"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoBq10C1001())}, {this.labelManager.getCampoLabel(quadroName, "anexoBq10C1002"), this.labelManager.getCampoNumber(quadroName, "anexoBq10C1002"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoBq10C1002())}};
        int[][] horizAligns = new int[][]{{0, 1, 2}, {0, 1, 2}};
        int[][] colSpans = new int[][]{{1, 1, 1}, {1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.emptyCell(1, 0);
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro10Panel.anexoBq10C1_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoBq10C1()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro11() throws DocumentException {
        Quadro11 quadro11 = this.anexoB.getQuadro11();
        String quadroName = "Quadro11";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{4.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.emptyCell(1, 0);
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq11C1101"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq11C1103"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoBq11C1105"));
        this.render.endCell();
        String[][] lines = new String[][]{{this.labelManager.getCampoBaseLabel(quadroName, "anexoBq11C1101"), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1101"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1101()), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1103"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1103()), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1105"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1105())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq11C1102"), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1102"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1102()), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1104"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1104()), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1106"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1106())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoBq11C1107"), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1107"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1107()), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1108"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1108()), this.labelManager.getCampoNumber(quadroName, "anexoBq11C1109"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoBq11C1109())}};
        int[][] horizAligns = new int[][]{{0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
    }

    private void renderQuadro12() throws DocumentException {
        Quadro12 quadro12 = this.anexoB.getQuadro12();
        String quadroName = "Quadro12";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Boolean q12B1OP = quadro12.getAnexoBq12B1() == null ? null : Boolean.valueOf(quadro12.getAnexoBq12B1().equals("1"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro12Panel.anexoBq12B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro12Panel.anexoBq12B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q12B1OP != null && q12B1OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro12Panel.anexoBq12B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q12B1OP == null || q12B1OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{5.0f, 3.0f, 2.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro12Panel.anexoBq12C3_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(quadro12.getAnexoBq12C3() == null ? "" : quadro12.getAnexoBq12C3().format());
        this.render.endCell();
        this.render.emptyCell(1, 0);
        this.render.endTable();
        this.render.addNewLine();
        Boolean q12B4 = quadro12.getAnexoBq12B4();
        this.render.startTable(new float[]{6.0f, 0.5f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro12Panel.anexoBq12B4_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q12B4 != null && q12B4.booleanValue()) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.endTable();
    }
}

