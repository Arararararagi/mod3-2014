/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.util;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import pt.opensoft.util.SimpleParameters;
import pt.opensoft.util.StringUtil;

public class LabelParameters
extends SimpleParameters {
    private static final String SEPARATOR = "\u00a3";

    @Override
    public void read(InputStream in) {
        super.read(in);
        HashMap extraEntries = new HashMap();
        Set allValues = this.values.entrySet();
        for (Map.Entry entry : allValues) {
            String key = entry.getKey().toString();
            if (!key.contains((CharSequence)"\u00a3")) continue;
            String quadroStr = key.substring(0, key.indexOf("."));
            String camposStr = key.substring(key.indexOf(".") + 1, key.lastIndexOf("."));
            String propertyStr = key.substring(key.lastIndexOf(".") + 1);
            String[] campos = camposStr.split("\u00a3");
            Object value = entry.getValue();
            for (String campoStr : campos) {
                extraEntries.put(quadroStr + "." + campoStr + "." + propertyStr, value);
            }
        }
        this.values.putAll(extraEntries);
    }

    @Override
    public String getString(String name, String defaultValue) {
        String resultString = super.getString(name, defaultValue);
        if (!StringUtil.isEmpty(resultString) && resultString.length() > "<html>".length() && resultString.substring(0, "<html>".length()).toLowerCase().equals("<html>")) {
            resultString = resultString.replaceAll("<.*?>", "");
        }
        return resultString;
    }

    public String getNumForQuadro(String quadro) {
        String key = quadro + "Panel.titlePanel.number";
        String result = this.getString(key);
        return result;
    }

    public String getTitleForQuadro(String quadro) {
        String key = quadro + "Panel.titlePanel.title";
        String result = this.getString(key);
        return result;
    }

    public String getLabelFor(String quadro, String campo) {
        String key = quadro + "Panel." + LabelParameters.firstToLower(campo) + "_label.text";
        String result = this.getString(key);
        return result;
    }

    public String getBaseLabelFor(String quadro, String campo) {
        String key = quadro + "Panel." + LabelParameters.firstToLower(campo) + "_base_label.text";
        String result = this.getString(key);
        return result;
    }

    public String getSimpleLabelFor(String quadro, String campo) {
        String key = quadro + "Panel." + LabelParameters.firstToLower(campo) + ".text";
        String result = this.getString(key);
        return result;
    }

    public String getNumForCampo(String quadro, String campo) {
        String key = quadro + "Panel." + LabelParameters.firstToLower(campo) + "_num.text";
        String result = this.getString(key);
        return result;
    }

    private static final String firstToLower(String str) {
        return new String(new StringBuilder().append("").append(str.charAt(0)).toString()).toLowerCase() + str.substring(1);
    }
}

