/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.util;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import pt.opensoft.field.EuroField;
import pt.opensoft.pdf.DynamicPDFRenderer;
import pt.opensoft.swing.Util;

public class Modelo3IRSPrinterUtilities {
    public static final String EMPTY_CELL = "###EMPTY_CELL###";
    private static final float SCALE_VALUE = 70.0f;
    public static float[] QuadroLayout = new float[]{0.0f, 8.0f, 0.0f};
    public static float[] LabelLayout = new float[]{0.0f, 7.0f, 0.0f};
    public static Color anexoColor = new Color(230, 230, 230);
    public static Color quadroColor = new Color(180, 180, 180);
    public static Color tableColor = new Color(130, 150, 130);
    public Image radioButton;
    public Image radioButtonSel;
    public Image checkBox;
    public Image checkBoxSel;

    public static void printAnexoTitle(DynamicPDFRenderer render, String name) throws DocumentException {
        render.startTable();
        render.startCell(1, 1, 1, 5, 15, true);
        render.setCellBGColor(anexoColor);
        render.field(name);
        render.endCell();
        render.endTable();
    }

    public static void printQuadroTitle(DynamicPDFRenderer render, String number, String name) throws DocumentException {
        render.startTable(QuadroLayout);
        render.emptyCell();
        render.startCell(1, 1, 0, 5, 15, true);
        render.setCellBGColor(quadroColor);
        render.field(number, name);
        render.endCell();
        render.emptyCell();
        render.endTable();
    }

    public static void printSubQuadro(DynamicPDFRenderer render, String name) throws DocumentException {
        render.startTable(LabelLayout);
        render.emptyCell();
        render.startCell(1, 1, 0, 5, 15, true);
        render.field(name);
        render.endCell();
        render.emptyCell();
        render.endTable();
    }

    public static void printTexto(DynamicPDFRenderer render, String name, int horizAlign, int vertAling, int border) throws DocumentException {
        render.startTable(LabelLayout);
        render.emptyCell();
        render.startCell(1, 1, horizAlign, vertAling, border, true);
        render.field(name);
        render.endCell();
        render.emptyCell();
        render.endTable();
    }

    public static void printTexto(DynamicPDFRenderer render, String name) throws DocumentException {
        render.startTable(LabelLayout);
        render.emptyCell();
        render.startCell(1, 1, 0, 5, 0, true);
        render.field(name);
        render.endCell();
        render.emptyCell();
        render.endTable();
    }

    public static float[] getTabelaColsArraySize(List<String> headers) {
        float[] cols = new float[headers.size() + 1];
        cols[0] = 0.5f;
        float colsize = 9.5f / (float)headers.size();
        for (int i = 1; i <= headers.size(); ++i) {
            cols[i] = colsize;
        }
        return cols;
    }

    public static void printTabela(DynamicPDFRenderer render, List<String> headers, List<String[]> values) throws DocumentException {
        float[] cols = new float[headers.size() + 1];
        cols[0] = 0.5f;
        float colsize = 9.5f / (float)headers.size();
        for (int i = 1; i <= headers.size(); ++i) {
            cols[i] = colsize;
        }
        Modelo3IRSPrinterUtilities.printTabela(render, headers, values, cols);
    }

    public static void printTabela(DynamicPDFRenderer render, List<String> headers, List<String[]> values, float[] cols) throws DocumentException {
        render.startTable(cols);
        render.startCell(1, 1, 1, 5, 15, true);
        render.setCellBGColor(tableColor);
        render.endCell();
        for (int i = 0; i < headers.size(); ++i) {
            render.startCell(1, 1, 1, 5, 15, true);
            render.setCellBGColor(tableColor);
            render.field(headers.get(i));
            render.endCell();
        }
        Modelo3IRSPrinterUtilities.printTabelaContent(render, values, headers.size());
        render.endTable();
    }

    public static void printTabela(DynamicPDFRenderer render, List<String> headers, List<String[]> values, float[] cols, float marginLeft, float marginRight) throws DocumentException {
        Modelo3IRSPrinterUtilities.printTabela(render, headers, values, cols, marginLeft, marginRight, "");
    }

    public static void printTabelaWithPrefix(DynamicPDFRenderer render, List<String> headers, List<String[]> values, float[] cols, float marginLeft, float marginRight, String prefix) throws DocumentException {
        Modelo3IRSPrinterUtilities.printTabela(render, headers, values, cols, marginLeft, marginRight, prefix);
    }

    private static void printTabela(DynamicPDFRenderer render, List<String> headers, List<String[]> values, float[] cols, float marginLeft, float marginRight, String prefix) throws DocumentException {
        int i;
        float[] newCols = new float[cols.length + 2];
        newCols[0] = marginLeft;
        for (i = 0; i < cols.length; ++i) {
            newCols[i + 1] = cols[i];
        }
        newCols[newCols.length - 1] = marginRight;
        render.startTable(newCols);
        render.emptyCell();
        render.startCell(1, 1, 1, 5, 15, true);
        render.setCellBGColor(tableColor);
        render.endCell();
        for (i = 0; i < headers.size(); ++i) {
            render.startCell(1, 1, 1, 5, 15, true);
            render.setCellBGColor(tableColor);
            render.field(headers.get(i));
            render.endCell();
        }
        render.emptyCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(render, values, headers.size(), true, prefix);
        render.endTable();
    }

    public static void printTabelaContent(DynamicPDFRenderer render, List<String[]> values, int colNum) throws DocumentException {
        Modelo3IRSPrinterUtilities.printTabelaContent(render, values, null, colNum, null);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static void printTabelaContent(DynamicPDFRenderer render, List<String[]> values, Set<Integer> indexesToCheckboxes, int colNum, Modelo3IRSPrinterUtilities printerUtils) throws DocumentException {
        if (values == null || values.isEmpty()) {
            render.startCell(1, 1, 1, 5, 15, true);
            render.setCellBGColor(tableColor);
            render.field("1");
            render.endCell();
            for (int j = 0; j < colNum; ++j) {
                render.startCell(1, 1, 2, 5, 15, true);
                render.field("");
                render.endCell();
            }
            return;
        }
        for (int i = 0; i < values.size(); ++i) {
            render.startCell(1, 1, 1, 5, 15, true);
            render.setCellBGColor(tableColor);
            render.field("" + (i + 1) + "");
            render.endCell();
            String[] campos = values.get(i);
            for (int j = 0; j < campos.length; ++j) {
                if (indexesToCheckboxes != null && !indexesToCheckboxes.isEmpty() && printerUtils != null && indexesToCheckboxes.contains(j)) {
                    render.startCell(1, 1, 1, 5, 15, true);
                    render.field(printerUtils.convertBoolean(campos[j].equals("S")));
                    render.endCell();
                    continue;
                }
                render.startCell(1, 1, 2, 5, 15, true);
                render.field(campos[j] == null ? "" : campos[j]);
                render.endCell();
            }
        }
    }

    public static void printTabelaContentWithPadding(DynamicPDFRenderer render, List<String[]> values, int colNum, int paddingLeft, int paddingRight) throws DocumentException {
        Modelo3IRSPrinterUtilities.printTabelaContentWithPadding(render, values, colNum, paddingLeft, paddingRight, "");
    }

    public static void printTabelaContentWithPaddingAndPrefix(DynamicPDFRenderer render, List<String[]> values, int colNum, int paddingLeft, int paddingRight, String prefix) throws DocumentException {
        Modelo3IRSPrinterUtilities.printTabelaContentWithPadding(render, values, colNum, paddingLeft, paddingRight, prefix);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static void printTabelaContentWithPadding(DynamicPDFRenderer render, List<String[]> values, int colNum, int paddingLeft, int paddingRight, String prefix) throws DocumentException {
        if (values == null || values.isEmpty()) {
            int k;
            if (paddingLeft > 0) {
                for (k = 0; k < paddingLeft; ++k) {
                    render.emptyCell();
                }
            }
            render.startCell(1, 1, 1, 5, 15, true);
            render.setCellBGColor(tableColor);
            render.field(prefix + "1");
            render.endCell();
            for (int j = 0; j < colNum; ++j) {
                render.startCell(1, 1, 2, 5, 15, true);
                render.field("");
                render.endCell();
            }
            if (paddingRight <= 0) return;
            for (k = 0; k < paddingRight; ++k) {
                render.emptyCell();
            }
            return;
        }
        for (int i = 0; i < values.size(); ++i) {
            if (paddingLeft > 0) {
                for (int k = 0; k < paddingLeft; ++k) {
                    render.emptyCell();
                }
            }
            render.startCell(1, 1, 1, 5, 15, true);
            render.setCellBGColor(tableColor);
            render.field(prefix + (i + 1) + "");
            render.endCell();
            String[] campos = values.get(i);
            for (int j = 0; j < campos.length; ++j) {
                render.startCell(1, 1, 2, 5, 15, true);
                render.field(campos[j] == null ? "" : campos[j]);
                render.endCell();
            }
            if (paddingRight <= 0) continue;
            for (int k = 0; k < paddingRight; ++k) {
                render.emptyCell();
            }
        }
    }

    private static void printTabelaContent(DynamicPDFRenderer render, List<String[]> values, int colNum, boolean margin, String prefix) throws DocumentException {
        if (values.size() == 0) {
            render.emptyCell();
            render.startCell(1, 1, 1, 5, 15, true);
            render.setCellBGColor(tableColor);
            render.field(prefix + 1 + "");
            render.endCell();
            for (int j = 0; j < colNum; ++j) {
                render.startCell(1, 1, 2, 5, 15, true);
                render.field("");
                render.endCell();
            }
            render.emptyCell();
        } else {
            for (int i = 0; i < values.size(); ++i) {
                render.emptyCell();
                render.startCell(1, 1, 1, 5, 15, true);
                render.setCellBGColor(tableColor);
                render.field(prefix + (i + 1) + "");
                render.endCell();
                String[] campos = values.get(i);
                for (int j = 0; j < campos.length; ++j) {
                    render.startCell(1, 1, 2, 5, 15, true);
                    render.field(campos[j] == null ? "" : campos[j]);
                    render.endCell();
                }
                render.emptyCell();
            }
        }
    }

    public static void renderLines(DynamicPDFRenderer render, String[][] lines, int[][] horizAligns, int[][] colSpans) throws BadElementException {
        for (int linhaIdx = 0; linhaIdx < lines.length; ++linhaIdx) {
            String[] linha = lines[linhaIdx];
            for (int colunaIdx = 0; colunaIdx < linha.length; ++colunaIdx) {
                if ("###EMPTY_CELL###".equals(lines[linhaIdx][colunaIdx])) {
                    render.emptyCell(colSpans[linhaIdx][colunaIdx], 0);
                    continue;
                }
                render.startCell(colSpans[linhaIdx][colunaIdx], 1, horizAligns[linhaIdx][colunaIdx], 5, 15, true);
                render.field(lines[linhaIdx][colunaIdx]);
                render.endCell();
            }
        }
    }

    public Image convertBoolean(Boolean bool) {
        if (bool == null) {
            return this.checkBox;
        }
        if (bool.booleanValue()) {
            return this.checkBoxSel;
        }
        return this.checkBox;
    }

    public static String convertLong(Long longVal) {
        return longVal == null ? "" : longVal.toString();
    }

    public static String convertPercent(Long longVal) {
        if (longVal == null) {
            return "";
        }
        return longVal + "%";
    }

    public static String convertPercent(Long longVal, int precision) {
        if (longVal == null) {
            return "";
        }
        EuroField percentFormat = new EuroField("euroFormat");
        percentFormat.setValue(longVal);
        percentFormat.setPrecision(precision);
        return percentFormat.format() + "%";
    }

    public static String convertEuro(Long value) {
        EuroField euroFormat = new EuroField("euroFormat");
        if (value == null) {
            return "";
        }
        euroFormat.setValue(value);
        euroFormat.setPrecision(2);
        return euroFormat.format() + "";
    }

    public void initImages() throws MalformedURLException, IOException, BadElementException {
        this.radioButton = Image.getInstance(Util.class.getResource("/icons/radiobutton.png"));
        this.radioButton.scalePercent(70.0f);
        this.radioButtonSel = Image.getInstance(Util.class.getResource("/icons/radiobutton_sel.png"));
        this.radioButtonSel.scalePercent(70.0f);
        this.checkBoxSel = Image.getInstance(Util.class.getResource("/icons/checkbox_sel.png"));
        this.checkBoxSel.scalePercent(70.0f);
        this.checkBox = Image.getInstance(Util.class.getResource("/icons/checkbox.png"));
        this.checkBox.scalePercent(70.0f);
    }
}

