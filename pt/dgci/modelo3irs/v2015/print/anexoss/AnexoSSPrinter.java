/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexoss;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxSS;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;

public class AnexoSSPrinter {
    AnexoSSModel anexoSS;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoSSPrinter(AnexoSSModel anexoSS, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoSS = anexoSS;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo SS (" + this.anexoSS.getFormKey().getSubId() + ")");
        this.render.addNewLine();
        this.renderQuadro1();
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
    }

    private void renderQuadro1() throws DocumentException {
        Quadro01 quadro01 = this.anexoSS.getQuadro01();
        String quadroName = "Quadro01";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        boolean value1 = quadro01.getAnexoSSq01B1() == null ? false : quadro01.getAnexoSSq01B1();
        boolean value2 = quadro01.getAnexoSSq01B2() == null ? false : quadro01.getAnexoSSq01B2();
        boolean value3 = quadro01.getAnexoSSq01B3() == null ? false : quadro01.getAnexoSSq01B3();
        float[] cols = new float[]{5.0f, 1.0f, 15.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value1) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoSSq01B1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value2) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoSSq01B2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value3) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoSSq01B3.text"));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoSS.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoSSq02C04"), quadro2.getAnexoSSq02C04() == null ? "" : quadro2.getAnexoSSq02C04().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro03 = this.anexoSS.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.LabelLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoSSq03C05"), quadro03.getAnexoSSq03C05() == null ? "" : quadro03.getAnexoSSq03C05().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.LabelLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoSSq03C06"), quadro03.getAnexoSSq03C06() == null ? "" : quadro03.getAnexoSSq03C06().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.startTable(Modelo3IRSPrinterUtilities.LabelLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoSSq03C07"), quadro03.getAnexoSSq03C07() == null ? "" : quadro03.getAnexoSSq03C07().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        boolean q03C08Value = quadro03.getAnexoSSq03C08() != null && quadro03.getAnexoSSq03C08() != false;
        this.render.startTable(new float[]{0.0f, 7.0f, 1.0f, 0.0f});
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoSSq03C08"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(q03C08Value ? this.printerUtils.checkBoxSel : this.printerUtils.checkBox);
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro04 = this.anexoSS.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.5f, 0.75f, 2.0f});
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoSSq04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C401()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C402"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq04C402"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C402()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C403"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq04C403"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C403()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C404"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq04C404"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C404()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C405"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq04C405"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C405()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C406"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq04C406"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C406()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C407"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq04C407"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C407()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq04C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoSSq04C1()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro05 = this.anexoSS.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.5f, 0.75f, 2.0f});
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoSSq05C501"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq05C501"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq05C501"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoSSq05C501()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq05C502"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoSSq05C502"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoSSq05C502()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoSSq05C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoSSq05C1()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro06 = this.anexoSS.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        boolean q06B1Value = "S".equals(quadro06.getAnexoSSq06B1());
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoSSq06B1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getSimpleLabel(quadroName, "anexoSSq06B1Sim"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(q06B1Value ? this.printerUtils.radioButtonSel : this.printerUtils.radioButton);
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getSimpleLabel(quadroName, "anexoSSq06B1Nao"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(quadro06.getAnexoSSq06B1() != null && !q06B1Value ? this.printerUtils.radioButtonSel : this.printerUtils.radioButton);
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        AnexoSSq06T1_LinhaAdapterFormat tableAdapter = new AnexoSSq06T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxSS.class.getSimpleName());
        EventList<AnexoSSq06T1_Linha> list = quadro06.getAnexoSSq06T1();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoSSq06T1_Linha linha = list.get(i2);
            String[] campos = new String[4];
            campos[0] = linha.getNifPortugues() == null ? "" : linha.getNifPortugues().toString();
            campos[1] = linha.getPais() == null ? "" : paisesCatalog.get(linha.getPais()).toString();
            campos[2] = linha.getNFiscalEstrangeiro() == null ? "" : linha.getNFiscalEstrangeiro().toString();
            campos[3] = linha.getValor() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getValor());
            linhas.add(campos);
        }
        this.render.startTable(new float[]{1.0f, 2.0f, 2.0f, 2.0f, 2.0f});
        this.render.emptyCell();
        this.render.startCell(3, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("N.\u00ba de Identifica\u00e7\u00e3o do adquirente do servi\u00e7o");
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headers.get(3));
        this.render.endCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field((String)headers.get(2));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContentWithPadding(this.render, linhas, headers.size(), 0, 0);
        this.render.endTable();
    }
}

