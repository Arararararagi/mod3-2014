/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexol;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro06;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.pdf.DynamicPDFRenderer;
import pt.opensoft.taxclient.model.FormKey;

public class AnexoLPrinter {
    AnexoLModel anexoL;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoLPrinter(AnexoLModel anexoL, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoL = anexoL;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo L (" + this.anexoL.getFormKey().getSubId() + ")");
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoL.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoLq02C01"), quadro2.getAnexoLq02C01() == null ? "" : quadro2.getAnexoLq02C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.anexoL.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoLq03C02"), quadro3.getAnexoLq03C02() == null ? "" : quadro3.getAnexoLq03C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoLq03C03"), quadro3.getAnexoLq03C03() == null ? "" : quadro3.getAnexoLq03C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label18.text") + ": " + this.labelManager.getCampo(quadroName, "anexoLq03C04"));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.label3.text"), quadro3.getAnexoLq03C04() == null ? "" : quadro3.getAnexoLq03C04().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.anexoL.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label8.text") + ": " + this.labelManager.getString("Quadro04Panel.label9.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoLq04T1_LinhaAdapterFormat tableAdapter = new AnexoLq04T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoLq04T1_Linha> list = quadro4.getAnexoLq04T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoLq04T1_Linha linha = list.get(i2);
            String[] campos = new String[4];
            campos[0] = linha.getEntidade() == null ? "" : linha.getEntidade().toString();
            campos[1] = linha.getCodRendAnexoA() == null ? "" : linha.getCodRendAnexoA().toString();
            campos[2] = linha.getCodActividade() == null ? "" : linha.getCodActividade().toString();
            campos[3] = linha.getRendimento() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimento()).toString();
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label10.text") + ": " + this.labelManager.getString("Quadro04Panel.label11.text"));
        this.render.addNewLine();
        ArrayList<String> headersb = new ArrayList<String>();
        AnexoLq04T2_LinhaAdapterFormat tableAdapterb = new AnexoLq04T2_LinhaAdapterFormat();
        for (int i3 = 1; i3 <= tableAdapterb.getColumnCount(); ++i3) {
            String colName = tableAdapterb.getColumnName(i3);
            if (colName == null) continue;
            headersb.add(colName);
        }
        EventList<AnexoLq04T2_Linha> listb = quadro4.getAnexoLq04T2();
        ArrayList<String[]> linhasb = new ArrayList<String[]>();
        for (int i4 = 0; i4 < listb.size(); ++i4) {
            AnexoLq04T2_Linha linhab = listb.get(i4);
            String[] campos = new String[4];
            campos[0] = linhab.getEntidade() == null ? "" : linhab.getEntidade().toString();
            campos[1] = linhab.getCampoQ4AnexoB() == null ? "" : linhab.getCampoQ4AnexoB().toString();
            campos[2] = linhab.getCodActividade() == null ? "" : linhab.getCodActividade().toString();
            campos[3] = linhab.getRendimento() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linhab.getRendimento()).toString();
            linhasb.add(campos);
        }
        float[] colsSizeb = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headersb);
        this.render.startTable(colsSizeb);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersb.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersb.get(1));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersb.get(2));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersb.get(3));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhasb, headersb.size());
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label12.text") + ": " + this.labelManager.getString("Quadro04Panel.label13.text"));
        this.render.addNewLine();
        ArrayList<String> headersC = new ArrayList<String>();
        AnexoLq04T3_LinhaAdapterFormat tableAdapterC = new AnexoLq04T3_LinhaAdapterFormat();
        for (int i5 = 1; i5 <= tableAdapterC.getColumnCount(); ++i5) {
            String colName = tableAdapterC.getColumnName(i5);
            if (colName == null) continue;
            headersC.add(colName);
        }
        EventList<AnexoLq04T3_Linha> listC = quadro4.getAnexoLq04T3();
        ArrayList<String[]> linhasC = new ArrayList<String[]>();
        for (int i6 = 0; i6 < listC.size(); ++i6) {
            AnexoLq04T3_Linha linhaC = listC.get(i6);
            String[] campos = new String[4];
            campos[0] = linhaC.getEntidade() == null ? "" : linhaC.getEntidade().toString();
            campos[1] = linhaC.getCodActividade() == null ? "" : linhaC.getCodActividade().toString();
            campos[2] = linhaC.getLucro() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linhaC.getLucro()).toString();
            campos[3] = linhaC.getPrejuizo() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linhaC.getPrejuizo()).toString();
            linhasC.add(campos);
        }
        float[] colsSizeC = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headersC);
        this.render.startTable(colsSizeC);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersC.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("C\u00f3digo Atividade");
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Resultado");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersC.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headersC.get(3));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhasC, headersC.size());
        this.render.endTable();
        this.render.addNewLine();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro5 = this.anexoL.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoLq05T1_LinhaAdapterFormat tableAdapter = new AnexoLq05T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoLq05T1_Linha> list = quadro5.getAnexoLq05T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        HashSet<Integer> indexesToCheckboxes = new HashSet<Integer>();
        indexesToCheckboxes.add(6);
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoLq05T1_Linha linha = list.get(i2);
            String[] campos = new String[7];
            campos[0] = linha.getCampoNLinhaQ6() == null ? "" : linha.getCampoNLinhaQ6().toString();
            campos[1] = linha.getCodActividade() == null ? "" : linha.getCodActividade().toString();
            campos[2] = linha.getCatAouB() == null ? "" : linha.getCatAouB().toString();
            campos[3] = linha.getPais() == null ? "" : linha.getPais().toString();
            campos[4] = linha.getRendimento() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getRendimento()).toString();
            String string = campos[5] = linha.getImpostoPagoNoEstrangeiro() == null ? "" : Modelo3IRSPrinterUtilities.convertEuro(linha.getImpostoPagoNoEstrangeiro()).toString();
            campos[6] = linha.getSemImpostoPagoNoEstrangeiro() == null ? "N" : (linha.getSemImpostoPagoNoEstrangeiro() != false ? "S" : "N");
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(4));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(5));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(6));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent((DynamicPDFRenderer)this.render, linhas, indexesToCheckboxes, headers.size(), this.printerUtils);
        this.render.endTable();
        this.render.addNewLine();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro6 = this.anexoL.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro06Panel.label10.text") + ": " + this.labelManager.getString("Quadro06Panel.label11.text"));
        this.render.addNewLine();
        String value6A12 = quadro6.getQ06B1() == null ? "" : quadro6.getQ06B1().toString();
        float[] cols = new float[]{4.0f, 1.0f, 1.0f, 10.0f};
        this.render.startTable(cols);
        this.render.emptyCell();
        this.render.startCell(1, 2, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label2.text"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 0, true);
        if (value6A12.equals("1")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 2, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B1OP1.text"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label3.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value6A12.equals("2")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 2, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B1OP2.text"));
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro06Panel.label12.text") + ": " + this.labelManager.getString("Quadro06Panel.label13.text"));
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printTexto(this.render, "Relativamente aos Rendimentos Tributados no Estrangeiro (Categorias A e H) ou que possam ser (Categorias B, E, F e G), indique o M\u00e9todo que Pretende:", 0, 1, 0);
        String value6A34 = quadro6.getQ06B2() == null ? "" : quadro6.getQ06B2().toString();
        float[] cols34 = new float[]{4.0f, 1.0f, 1.0f, 10.0f};
        this.render.startTable(cols34);
        this.render.emptyCell();
        this.render.startCell(1, 2, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label4.text"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 0, true);
        if (value6A34.equals("3")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 2, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B2OP3.text"));
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.label5.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (value6A34.equals("4")) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 2, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro06Panel.q06B2OP4.text"));
        this.render.endCell();
        this.render.endTable();
    }
}

