/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexog1;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;

public class AnexoG1Printer {
    AnexoG1Model anexoG1;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoG1Printer(AnexoG1Model anexoG1, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoG1 = anexoG1;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo G1");
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoG1.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoG1q02C01"), quadro2.getAnexoG1q02C01() == null ? "" : quadro2.getAnexoG1q02C01().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro3 = this.anexoG1.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoG1q03C02"), quadro3.getAnexoG1q03C02() == null ? "" : quadro3.getAnexoG1q03C02().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoG1q03C03"), quadro3.getAnexoG1q03C03() == null ? "" : quadro3.getAnexoG1q03C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro4 = this.anexoG1.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoG1q04T1_LinhaAdapterFormat tableAdapter = new AnexoG1q04T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoG1q04T1_Linha> list = quadro4.getAnexoG1q04T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoG1q04T1_Linha linha = list.get(i2);
            String[] campos = new String[5];
            campos[0] = linha.getMesRealizacao() == null ? "" : linha.getMesRealizacao().toString();
            campos[1] = Modelo3IRSPrinterUtilities.convertEuro(linha.getValorRealizacao());
            campos[2] = linha.getAnoAquisicao() == null ? "" : linha.getAnoAquisicao().toString();
            campos[3] = linha.getMesAquisicao() == null ? "" : linha.getMesAquisicao().toString();
            campos[4] = Modelo3IRSPrinterUtilities.convertEuro(linha.getValorAquisicao());
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Realiza\u00e7\u00e3o");
        this.render.endCell();
        this.render.startCell(3, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Aquisi\u00e7\u00e3o");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(4));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{2.0f, 2.0f, 2.0f, 2.0f});
        this.render.emptyCell(2, 0);
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoG1q04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoG1q04C401a"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoG1q04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoG1q04C401"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoG1q04C401()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro4.getAnexoG1q04C401a()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro5 = this.anexoG1.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoG1q05T1_LinhaAdapterFormat tableAdapter = new AnexoG1q05T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoG1q05T1_Linha> list = quadro5.getAnexoG1q05T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoG1q05T1_Linha linha = list.get(i2);
            String[] campos = new String[10];
            campos[0] = linha.getFreguesia() == null ? "" : linha.getFreguesia().toString();
            campos[1] = linha.getTipoPredio() == null ? "" : linha.getTipoPredio();
            campos[2] = linha.getArtigo() == null ? "" : linha.getArtigo().toString();
            campos[3] = linha.getFraccao() == null ? "" : linha.getFraccao().toString();
            campos[4] = linha.getCodigo() == null ? "" : linha.getCodigo().toString();
            campos[5] = linha.getAno() == null ? "" : linha.getAno().toString();
            campos[6] = linha.getMes() == null ? "" : linha.getMes().toString();
            campos[7] = linha.getDia() == null ? "" : linha.getDia().toString();
            campos[8] = Modelo3IRSPrinterUtilities.convertEuro(linha.getRealizacao());
            campos[9] = Modelo3IRSPrinterUtilities.convertEuro(linha.getAquisicao());
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(4, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Identifica\u00e7\u00e3o Matricial");
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(4));
        this.render.endCell();
        this.render.startCell(3, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Data de Aquisi\u00e7\u00e3o");
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Valor");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(5));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(6));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(7));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(8));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(9));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{2.0f, 2.0f, 2.0f});
        this.render.emptyCell(1, 0);
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoG1q05C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoG1q05C2"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoG1q05C1"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoG1q05C1()));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro5.getAnexoG1q05C2()));
        this.render.endCell();
        this.render.endTable();
    }
}

