/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print.anexoc;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import java.awt.Color;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.dgci.modelo3irs.v2015.print.Modelo3IRSPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.Date;

public class AnexoCPrinter {
    AnexoCModel anexoC;
    PrintLabelManagerIRS labelManager;
    Modelo3IRSPrinter render;
    Modelo3IRSPrinterUtilities printerUtils;

    public AnexoCPrinter(AnexoCModel anexoC, PrintLabelManagerIRS labelManager, Modelo3IRSPrinter render, Modelo3IRSPrinterUtilities printerUtils) {
        this.anexoC = anexoC;
        this.labelManager = labelManager;
        this.render = render;
        this.printerUtils = printerUtils;
    }

    public void render() throws DocumentException {
        Modelo3IRSPrinterUtilities.printAnexoTitle(this.render, "Anexo C (" + this.anexoC.getFormKey().getSubId() + ")");
        this.render.addNewLine();
        this.renderQuadro1();
        this.render.addNewLine();
        this.renderQuadro2();
        this.render.addNewLine();
        this.renderQuadro3();
        this.render.addNewLine();
        this.renderQuadro4();
        this.render.addNewLine();
        this.renderQuadro5();
        this.render.addNewLine();
        this.renderQuadro6();
        this.render.addNewLine();
        this.renderQuadro7();
        this.render.addNewLine();
        this.renderQuadro8();
        this.render.addNewLine();
        this.renderQuadro9();
        this.render.addNewLine();
        this.renderQuadro10();
        this.render.addNewLine();
        this.renderQuadro11();
        this.render.addNewLine();
        this.renderQuadro12();
        this.render.addNewLine();
        this.renderQuadro13();
        this.render.addNewLine();
        this.renderQuadro14();
        this.render.addNewLine();
        this.renderQuadro15();
        this.render.addNewLine();
        this.renderQuadro16();
        this.render.addNewLine();
        this.renderQuadro17();
        this.render.addNewLine();
    }

    private void renderQuadro1() throws DocumentException {
        Quadro01 quadro01 = this.anexoC.getQuadro01();
        String quadroName = "Quadro01";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        float[] cols = new float[]{5.0f, 1.0f, 15.0f};
        this.render.startTable(cols);
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label4.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (quadro01.isAnexoCq01B1Selected()) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoCq01B1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.label5.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 0, true);
        if (quadro01.isAnexoCq01B2Selected()) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro01Panel.anexoCq01B2.text"));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro2() throws DocumentException {
        Quadro02 quadro2 = this.anexoC.getQuadro02();
        String quadroName = "Quadro02";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.startTable(Modelo3IRSPrinterUtilities.QuadroLayout);
        this.render.emptyCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoCq02C03"), quadro2.getAnexoCq02C03() == null ? "" : quadro2.getAnexoCq02C03().toString());
        this.render.endCell();
        this.render.emptyCell();
        this.render.endTable();
    }

    private void renderQuadro3() throws DocumentException {
        Quadro03 quadro03 = this.anexoC.getQuadro03();
        String quadroName = "Quadro03";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoCq03C04"), quadro03.getAnexoCq03C04() == null ? "" : quadro03.getAnexoCq03C04().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoCq03C05"), quadro03.getAnexoCq03C05() == null ? "" : quadro03.getAnexoCq03C05().toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro03Panel.label12.text") + ": " + this.labelManager.getString("Quadro03Panel.label13.text"));
        Boolean value1 = quadro03.getAnexoCq03B1() == null ? null : Boolean.valueOf(quadro03.getAnexoCq03B1().equals("1"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (value1 != null && value1.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(value1 == null || value1.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03C06_label.text"), quadro03.getAnexoCq03C06() == null ? "" : quadro03.getAnexoCq03C06().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03C07_label.text"), quadro03.getAnexoCq03C07() == null ? "" : quadro03.getAnexoCq03C07().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03C08_label.text"), quadro03.getAnexoCq03C08() == null ? "" : quadro03.getAnexoCq03C08().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03C09_label.text"), quadro03.getAnexoCq03C09() == null ? "" : quadro03.getAnexoCq03C09().toString());
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03C10_label.text"), quadro03.getAnexoCq03C10() == null ? "" : quadro03.getAnexoCq03C10().toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printTexto(this.render, "", 1, 5, 1);
        Boolean value2 = quadro03.getAnexoCq03B2() == null ? null : Boolean.valueOf(quadro03.getAnexoCq03B2().equals("11"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03B2OPSim_base_label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03B2OP11.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (value2 != null && value2.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro03Panel.anexoCq03B2OP12.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(value2 == null || value2.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro4() throws DocumentException {
        Quadro04 quadro04 = this.anexoC.getQuadro04();
        String quadroName = "Quadro04";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        String[][] lines1 = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq04C401"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C401"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C401())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C402"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C402"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C402())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C403"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C403"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C403())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C404"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C404"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C404())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C405"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C405"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C405())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C406"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C406"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C406())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C407"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C407"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C407())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C408"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C408"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C408())}};
        int[][] horizAligns1 = new int[][]{{0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {2, 1, 2}};
        int[][] colSpans1 = new int[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines1, horizAligns1, colSpans1);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label4.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        String[][] lines2 = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq04C409"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C409"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C409())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C410"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C410"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C410())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C464"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C464"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C464())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C411"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C411"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C411())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C416"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C416"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C416())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C419"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C419"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C419())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C412"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C412"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C412())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C420"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C420"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C420())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C465"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C465"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C465())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C422"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C422"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C422())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C421"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C421"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C421())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C423"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C423"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C423())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C424"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C424"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C424())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C425"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C425"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C425())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C426"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C426"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C426())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C427"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C427"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C427())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C466"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C466"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C466())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C435"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C435"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C435())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C413"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C413"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C413())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C414"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C414"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C414())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C415"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C415"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C415())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C417"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C417"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C417())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C418"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C418"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C418())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C428"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C428"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C428())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C429"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C429"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C429())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C430"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C430"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C430())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C431"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C431"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C431())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C432"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C432"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C432())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C433"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C433"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C433())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C434"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C434"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C434())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C467"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C467"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C467())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C468"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C468"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C468())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C436"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C436"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C436())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C437"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C437"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C437())}, {"", this.labelManager.getCampoNumber(quadroName, "anexoCq04C438"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C438())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C439"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C439"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C439())}};
        int[][] horizAligns2 = new int[][]{{0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {2, 1, 2}};
        int[][] colSpans2 = new int[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines2, horizAligns2, colSpans2);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label5.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        String[][] lines3 = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq04C440"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C440"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C440())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C441"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C441"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C441())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C442"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C442"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C442())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C469"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C469"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C469())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C443"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C443"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C443())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C444"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C444"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C444())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C445"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C445"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C445())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C470"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C470"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C470())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C446"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C446"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C446())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C447"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C447"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C447())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C471"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C471"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C471())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C448"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C448"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C448())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C449"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C449"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C449())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C450"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C450"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C450())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C472"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C472"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C472())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C451"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C451"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C451())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C473"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C473"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C473())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C453"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C453"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C453())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C454"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C454"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C454())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C455"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C455"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C455())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C456"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C456"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C456())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C462"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C462"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C462())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C463"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C463"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C463())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C452"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C452"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C452())}, {"", this.labelManager.getCampoNumber(quadroName, "anexoCq04C457"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C457())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C458"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C458"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C458())}};
        int[][] horizAligns3 = new int[][]{{0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {2, 1, 2}};
        int[][] colSpans3 = new int[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines3, horizAligns3, colSpans3);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, "");
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        String[][] lines4 = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq04C459"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C459"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C459())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq04C460"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C460"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C460())}};
        int[][] horizAligns4 = new int[][]{{0, 1, 2}, {0, 1, 2}};
        int[][] colSpans4 = new int[][]{{1, 1, 1}, {1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines4, horizAligns4, colSpans4);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro04Panel.label2.text") + ": " + this.labelManager.getString("Quadro04Panel.label3.text"));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        String[][] lines5 = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq04C461"), this.labelManager.getCampoNumber(quadroName, "anexoCq04C461"), Modelo3IRSPrinterUtilities.convertEuro(quadro04.getAnexoCq04C461())}};
        int[][] horizAligns5 = new int[][]{{0, 1, 2}};
        int[][] colSpans5 = new int[][]{{1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines5, horizAligns5, colSpans5);
        this.render.endTable();
    }

    private void renderQuadro5() throws DocumentException {
        Quadro05 quadro05 = this.anexoC.getQuadro05();
        String quadroName = "Quadro05";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{5.0f, 1.0f, 2.0f, 1.0f, 2.0f});
        String[][] lines = new String[][]{{"###EMPTY_CELL###", this.labelManager.getCampoLabel(quadroName, "anexoCq05C501"), this.labelManager.getCampoLabel(quadroName, "anexoCq05C503")}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq05C501"), this.labelManager.getCampoNumber(quadroName, "anexoCq05C501"), Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoCq05C501()), this.labelManager.getCampoNumber(quadroName, "anexoCq05C503"), Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoCq05C503())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq05C502"), this.labelManager.getCampoNumber(quadroName, "anexoCq05C502"), Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoCq05C502()), this.labelManager.getCampoNumber(quadroName, "anexoCq05C504"), Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoCq05C504())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq05C505"), this.labelManager.getCampoNumber(quadroName, "anexoCq05C505"), Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoCq05C505()), this.labelManager.getCampoNumber(quadroName, "anexoCq05C506"), Modelo3IRSPrinterUtilities.convertEuro(quadro05.getAnexoCq05C506())}};
        int[][] horizAligns = new int[][]{{-1, 1, 1}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}, {0, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{1, 2, 2}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
    }

    private void renderQuadro6() throws DocumentException {
        Quadro06 quadro06 = this.anexoC.getQuadro06();
        String quadroName = "Quadro06";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{5.0f, 1.0f, 2.0f, 1.0f, 2.0f, 1.0f, 2.0f});
        String[][] lines = new String[][]{{"###EMPTY_CELL###", this.labelManager.getCampoLabel(quadroName, "anexoCq06C601"), this.labelManager.getCampoLabel(quadroName, "anexoCq06C606"), this.labelManager.getCampoLabel(quadroName, "anexoCq06C611")}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq06C601"), this.labelManager.getCampoNumber(quadroName, "anexoCq06C601"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C601()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C606"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C606()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C611"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C611())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq06C602"), this.labelManager.getCampoNumber(quadroName, "anexoCq06C602"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C602()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C607"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C607()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C612"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C612())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq06C603"), this.labelManager.getCampoNumber(quadroName, "anexoCq06C603"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C603()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C608"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C608()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C613"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C613())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq06C604"), this.labelManager.getCampoNumber(quadroName, "anexoCq06C604"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C604()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C609"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C609()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C614"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C614())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq06C605"), this.labelManager.getCampoNumber(quadroName, "anexoCq06C605"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C605()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C610"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C610()), this.labelManager.getCampoNumber(quadroName, "anexoCq06C615"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C615())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq06C616"), this.labelManager.getCampoNumber(quadroName, "anexoCq06C616"), Modelo3IRSPrinterUtilities.convertEuro(quadro06.getAnexoCq06C616())}};
        int[][] horizAligns = new int[][]{{-1, 1, 1, 1}, {0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}, {2, 1, 2, 1, 2, 1, 2}, {2, 1, 2}};
        int[][] colSpans = new int[][]{{1, 2, 2, 2}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {5, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
    }

    private void renderQuadro7() throws DocumentException {
        Quadro07 quadro07 = this.anexoC.getQuadro07();
        String quadroName = "Quadro07";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoCq07C701"), Modelo3IRSPrinterUtilities.convertEuro(quadro07.getAnexoCq07C701()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro8() throws DocumentException {
        Quadro08 quadro08 = this.anexoC.getQuadro08();
        String quadroName = "Quadro08";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 3.0f, 1.0f, 3.0f, 1.0f, 3.0f, 1.0f, 3.0f, 1.0f, 3.0f});
        String[][] lines = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq08C801"), this.labelManager.getCampoLabel(quadroName, "anexoCq08C802"), this.labelManager.getCampoLabel(quadroName, "anexoCq08C803"), this.labelManager.getCampoLabel(quadroName, "anexoCq08C804"), this.labelManager.getCampoLabel(quadroName, "anexoCq08C805")}, {this.labelManager.getCampoNumber(quadroName, "anexoCq08C801"), Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoCq08C801()), this.labelManager.getCampoNumber(quadroName, "anexoCq08C802"), Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoCq08C802()), this.labelManager.getCampoNumber(quadroName, "anexoCq08C803"), Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoCq08C803()), this.labelManager.getCampoNumber(quadroName, "anexoCq08C804"), Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoCq08C804()), this.labelManager.getCampoNumber(quadroName, "anexoCq08C805"), Modelo3IRSPrinterUtilities.convertEuro(quadro08.getAnexoCq08C805())}};
        int[][] horizAligns = new int[][]{{1, 1, 1, 1, 1}, {1, 2, 1, 2, 1, 2, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{2, 2, 2, 2, 2}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro08Panel.label10.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoCq08T1_LinhaAdapterFormat tableAdapter = new AnexoCq08T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoCq08T1_Linha> list = quadro08.getAnexoCq08T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoCq08T1_Linha linha = list.get(i2);
            String[] campos = new String[2];
            campos[0] = linha.getNIF() == null ? "" : linha.getNIF().toString();
            campos[1] = Modelo3IRSPrinterUtilities.convertEuro(linha.getValor());
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
    }

    private void renderQuadro9() throws DocumentException {
        Quadro09 quadro09 = this.anexoC.getQuadro09();
        String quadroName = "Quadro09";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampo(quadroName, "anexoCq09C901"), quadro09.getAnexoCq09C901() == null ? "" : quadro09.getAnexoCq09C901().toString());
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{1.0f, 2.0f, 1.0f, 2.0f, 1.0f, 2.0f});
        String[][] lines = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq09C902"), this.labelManager.getCampoLabel(quadroName, "anexoCq09C908"), this.labelManager.getCampoLabel(quadroName, "anexoCq09C914")}, {this.labelManager.getCampoNumber(quadroName, "anexoCq09C902"), Modelo3IRSPrinterUtilities.convertLong(quadro09.getAnexoCq09C902()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C908"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C908()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C914"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C914())}, {this.labelManager.getCampoNumber(quadroName, "anexoCq09C903"), Modelo3IRSPrinterUtilities.convertLong(quadro09.getAnexoCq09C903()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C909"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C909()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C915"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C915())}, {this.labelManager.getCampoNumber(quadroName, "anexoCq09C904"), Modelo3IRSPrinterUtilities.convertLong(quadro09.getAnexoCq09C904()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C910"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C910()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C916"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C916())}, {this.labelManager.getCampoNumber(quadroName, "anexoCq09C905"), Modelo3IRSPrinterUtilities.convertLong(quadro09.getAnexoCq09C905()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C911"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C911()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C917"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C917())}, {this.labelManager.getCampoNumber(quadroName, "anexoCq09C906"), Modelo3IRSPrinterUtilities.convertLong(quadro09.getAnexoCq09C906()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C912"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C912()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C918"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C918())}, {this.labelManager.getCampoNumber(quadroName, "anexoCq09C907"), Modelo3IRSPrinterUtilities.convertLong(quadro09.getAnexoCq09C907()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C913"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C913()), this.labelManager.getCampoNumber(quadroName, "anexoCq09C919"), Modelo3IRSPrinterUtilities.convertEuro(quadro09.getAnexoCq09C919())}};
        int[][] horizAligns = new int[][]{{1, 1, 1}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}, {1, 2, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{2, 2, 2}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
    }

    private void renderQuadro10() throws DocumentException {
        Quadro10 quadro10 = this.anexoC.getQuadro10();
        String quadroName = "Quadro10";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{7.0f, 1.0f, 2.0f});
        String[][] lines = new String[][]{{this.labelManager.getCampoLabel(quadroName, "anexoCq10C1001"), this.labelManager.getCampoNumber(quadroName, "anexoCq10C1001"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1001())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq10C1002"), this.labelManager.getCampoNumber(quadroName, "anexoCq10C1002"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1002())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq10C1006").replace((CharSequence)"&lt;", (CharSequence)"<"), this.labelManager.getCampoNumber(quadroName, "anexoCq10C1006"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1006())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq10C1003"), this.labelManager.getCampoNumber(quadroName, "anexoCq10C1003"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1003())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq10C1007"), this.labelManager.getCampoNumber(quadroName, "anexoCq10C1007"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1007())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq10C1004"), this.labelManager.getCampoNumber(quadroName, "anexoCq10C1004"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1004())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq10C1005"), this.labelManager.getCampoNumber(quadroName, "anexoCq10C1005"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1005())}, {this.labelManager.getCampoLabel(quadroName, "anexoCq10C1"), Modelo3IRSPrinterUtilities.convertEuro(quadro10.getAnexoCq10C1())}};
        int[][] horizAligns = new int[][]{{0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {0, 1, 2}, {2, 2}};
        int[][] colSpans = new int[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {2, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
    }

    private void renderQuadro11() throws DocumentException {
        Quadro11 quadro11 = this.anexoC.getQuadro11();
        String quadroName = "Quadro11";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoCq11C1101"), Modelo3IRSPrinterUtilities.convertEuro(quadro11.getAnexoCq11C1101()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro12() throws DocumentException {
        Quadro12 quadro12 = this.anexoC.getQuadro12();
        String quadroName = "Quadro12";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{4.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        String[][] lines = new String[][]{{"###EMPTY_CELL###", this.labelManager.getCampoLabel(quadroName, "anexoCq12C1201"), this.labelManager.getCampoLabel(quadroName, "anexoCq12C1203"), this.labelManager.getCampoLabel(quadroName, "anexoCq12C1205")}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq12C1201"), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1201"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1201()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1203"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1203()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1205"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1205())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq12C1210"), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1210"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1210()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1211"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1211()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1212"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1212())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq12C1202"), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1202"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1202()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1204"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1204()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1206"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1206())}, {this.labelManager.getCampoBaseLabel(quadroName, "anexoCq12C1207"), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1207"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1207()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1208"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1208()), this.labelManager.getCampoNumber(quadroName, "anexoCq12C1209"), Modelo3IRSPrinterUtilities.convertEuro(quadro12.getAnexoCq12C1209())}};
        int[][] horizAligns = new int[][]{{-1, 1, 1, 1}, {0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}, {0, 1, 2, 1, 2, 1, 2}};
        int[][] colSpans = new int[][]{{1, 2, 2, 2}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 1, 1}};
        Modelo3IRSPrinterUtilities.renderLines(this.render, lines, horizAligns, colSpans);
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro12Panel.label1.text") + ": " + this.labelManager.getString("Quadro12Panel.label2.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoCq12T1_LinhaAdapterFormat tableAdapter = new AnexoCq12T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoCq12T1_Linha> list = quadro12.getAnexoCq12T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoCq12T1_Linha linha = list.get(i2);
            String[] campos = new String[7];
            campos[0] = linha.getNIPCdasEntidades() == null ? "" : linha.getNIPCdasEntidades().toString();
            campos[1] = linha.getSubsidioDetinadoaExploracao() == null ? "" : linha.getSubsidioDetinadoaExploracao().toString();
            campos[2] = linha.getSubsidiosNaoDestinadosExploracaoN() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN().toString();
            campos[3] = linha.getSubsidiosNaoDestinadosExploracaoN1() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN1().toString();
            campos[4] = linha.getSubsidiosNaoDestinadosExploracaoN2() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN2().toString();
            campos[5] = linha.getSubsidiosNaoDestinadosExploracaoN3() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN3().toString();
            campos[6] = linha.getSubsidiosNaoDestinadosExploracaoN4() == null ? "" : linha.getSubsidiosNaoDestinadosExploracaoN4().toString();
            linhas.add(campos);
        }
        float[] colsSize = Modelo3IRSPrinterUtilities.getTabelaColsArraySize(headers);
        this.render.startTable(colsSize);
        this.render.startCell(1, 2, 15);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(0));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(1));
        this.render.endCell();
        this.render.startCell(5, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field("Subs\u00eddios n\u00e3o destinados \u00e0 explora\u00e7\u00e3o");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(2));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(3));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(4));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(5));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.setCellBGColor(Modelo3IRSPrinterUtilities.tableColor);
        this.render.field(headers.get(6));
        this.render.endCell();
        Modelo3IRSPrinterUtilities.printTabelaContent(this.render, linhas, headers.size());
        this.render.endTable();
    }

    private void renderQuadro13() throws DocumentException {
        Quadro13 quadro13 = this.anexoC.getQuadro13();
        String quadroName = "Quadro13";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{4.0f, 4.0f, 1.0f, 4.0f, 1.0f, 4.0f, 1.0f, 1.0f, 2.0f});
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label17.text"));
        this.render.endCell();
        this.render.startCell(5, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label14.text"));
        this.render.endCell();
        this.render.startCell(3, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoCq13C1303"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label16.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoCq13C1301"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoBaseLabel(quadroName, "anexoCq13C1302"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label2.text"));
        this.render.endCell();
        this.render.startCell(2, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label3.text"));
        this.render.endCell();
        this.render.startCell(1, 4, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoCq13C1301"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label25.text"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1301"));
        this.render.endCell();
        this.render.startCell(1, 2, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1301()));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1302"));
        this.render.endCell();
        this.render.startCell(1, 2, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1302()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label25.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1303"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1303()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label26.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1304"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1304()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label35.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label27.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1305"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1305()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label36.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label28.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1306"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1306()));
        this.render.endCell();
        this.render.startCell(1, 4, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoCq13C1307"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label38.text"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1307"));
        this.render.endCell();
        this.render.startCell(1, 2, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1307()));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1308"));
        this.render.endCell();
        this.render.startCell(1, 2, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1308()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label29.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1309"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1309()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label30.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1310"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1310()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label39.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label31.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1311"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1311()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label40.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label32.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1312"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1312()));
        this.render.endCell();
        this.render.startCell(1, 4, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoLabel(quadroName, "anexoCq13C1313"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label41.text"));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1313"));
        this.render.endCell();
        this.render.startCell(1, 2, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1313()));
        this.render.endCell();
        this.render.startCell(1, 2, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1314"));
        this.render.endCell();
        this.render.startCell(1, 2, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1314()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label33.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1315"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1315()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label34.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1316"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1316()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label42.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label44.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1317"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1317()));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label43.text"));
        this.render.endCell();
        this.render.emptyCell(4, 0);
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro13Panel.label45.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq13C1318"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro13.getAnexoCq13C1318()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro14() throws DocumentException {
        Quadro14 quadro14 = this.anexoC.getQuadro14();
        String quadroName = "Quadro14";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Boolean value1 = quadro14.getAnexoCq14B1() == null ? null : Boolean.valueOf(quadro14.getAnexoCq14B1().equals("1"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro14Panel.anexoCq14B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro14Panel.anexoCq14B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (value1 != null && value1.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro14Panel.anexoCq14B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(value1 == null || value1.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        Modelo3IRSPrinterUtilities.printSubQuadro(this.render, this.labelManager.getString("Quadro14Panel.label10.text"));
        this.render.addNewLine();
        ArrayList<String> headers = new ArrayList<String>();
        AnexoCq14T1_LinhaAdapterFormat tableAdapter = new AnexoCq14T1_LinhaAdapterFormat();
        for (int i = 1; i <= tableAdapter.getColumnCount(); ++i) {
            String colName = tableAdapter.getColumnName(i);
            if (colName == null) continue;
            headers.add(colName);
        }
        EventList<AnexoCq14T1_Linha> list = quadro14.getAnexoCq14T1();
        ArrayList<String[]> linhas = new ArrayList<String[]>();
        for (int i2 = 0; i2 < list.size(); ++i2) {
            AnexoCq14T1_Linha linha = list.get(i2);
            String[] campos = new String[7];
            campos[0] = linha.getFreguesia() == null ? "" : linha.getFreguesia().toString();
            campos[1] = linha.getTipoPredio() == null ? "" : linha.getTipoPredio();
            campos[2] = linha.getArtigo() == null ? "" : linha.getArtigo().toString();
            campos[3] = linha.getFraccao() == null ? "" : linha.getFraccao();
            campos[4] = Modelo3IRSPrinterUtilities.convertEuro(linha.getValorVenda());
            campos[5] = Modelo3IRSPrinterUtilities.convertEuro(linha.getValorDefinitivo());
            campos[6] = linha.getArt139CIRC() == null ? "" : linha.getArt139CIRC();
            linhas.add(campos);
        }
        Modelo3IRSPrinterUtilities.printTabela(this.render, headers, linhas);
    }

    private void renderQuadro15() throws DocumentException {
        Quadro15 quadro15 = this.anexoC.getQuadro15();
        String quadroName = "Quadro15";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{4.0f, 1.0f, 4.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro15Panel.anexoCq15C1501_label.text"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq15C1501"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro15.getAnexoCq15C1501()));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro15Panel.anexoCq15C1502_label.text"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getCampoNumber(quadroName, "anexoCq15C1502"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(Modelo3IRSPrinterUtilities.convertEuro(quadro15.getAnexoCq15C1502()));
        this.render.endCell();
        this.render.endTable();
    }

    private void renderQuadro16() throws DocumentException {
        Quadro16 quadro16 = this.anexoC.getQuadro16();
        String quadroName = "Quadro16";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        Boolean q16B1OP = quadro16.getAnexoCq16B1() == null ? null : Boolean.valueOf(quadro16.getAnexoCq16B1().equals("1"));
        this.render.startTable(new float[]{5.0f, 1.0f, 1.0f, 1.0f, 1.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro16Panel.anexoCq16B1OPSim_base_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro16Panel.anexoCq16B1OP1.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q16B1OP != null && q16B1OP.booleanValue()) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro16Panel.anexoCq16B1OP2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (!(q16B1OP == null || q16B1OP.booleanValue())) {
            this.render.field(this.printerUtils.radioButtonSel);
        } else {
            this.render.field(this.printerUtils.radioButton);
        }
        this.render.endCell();
        this.render.endTable();
        this.render.addNewLine();
        this.render.startTable(new float[]{5.0f, 3.0f, 2.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro16Panel.anexoCq16C3_label.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(quadro16.getAnexoCq16C3() == null ? "" : quadro16.getAnexoCq16C3().format());
        this.render.endCell();
        this.render.emptyCell(1, 0);
        this.render.endTable();
        this.render.addNewLine();
        Boolean q16B4 = quadro16.getAnexoCq16B4();
        this.render.startTable(new float[]{5.0f, 3.0f, 2.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro16Panel.anexoCq16B4_label2.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 0, 5, 0, true);
        if (q16B4 != null && q16B4.booleanValue()) {
            this.render.field(this.printerUtils.checkBoxSel);
        } else {
            this.render.field(this.printerUtils.checkBox);
        }
        this.render.endCell();
        this.render.emptyCell(1, 0);
        this.render.endTable();
    }

    private void renderQuadro17() throws DocumentException {
        Quadro17 quadro17 = this.anexoC.getQuadro17();
        String quadroName = "Quadro17";
        Modelo3IRSPrinterUtilities.printQuadroTitle(this.render, this.labelManager.getQuadroNumber(quadroName), this.labelManager.getQuadroTitle(quadroName));
        this.render.addNewLine();
        this.render.startTable(new float[]{4.0f, 1.0f, 4.0f});
        this.render.startCell(1, 1, 0, 5, 0, true);
        this.render.field(this.labelManager.getString("Quadro17Panel.anexoCq03C04_label.text"), "");
        this.render.endCell();
        this.render.startCell(1, 1, 1, 5, 15, true);
        this.render.field(this.labelManager.getString("Quadro17Panel.label4.text"));
        this.render.endCell();
        this.render.startCell(1, 1, 2, 5, 15, true);
        this.render.field(quadro17.getAnexoCq17C1701() == null ? "" : quadro17.getAnexoCq17C1701().toString());
        this.render.endCell();
        this.render.endTable();
    }
}

