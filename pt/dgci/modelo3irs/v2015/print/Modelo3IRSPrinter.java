/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.print;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.List;
import pt.dgci.modelo3irs.v2015.gui.PrintLabelManagerIRS;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.print.anexoa.AnexoAPrinter;
import pt.dgci.modelo3irs.v2015.print.anexob.AnexoBPrinter;
import pt.dgci.modelo3irs.v2015.print.anexoc.AnexoCPrinter;
import pt.dgci.modelo3irs.v2015.print.anexod.AnexoDPrinter;
import pt.dgci.modelo3irs.v2015.print.anexoe.AnexoEPrinter;
import pt.dgci.modelo3irs.v2015.print.anexof.AnexoFPrinter;
import pt.dgci.modelo3irs.v2015.print.anexog.AnexoGPrinter;
import pt.dgci.modelo3irs.v2015.print.anexog1.AnexoG1Printer;
import pt.dgci.modelo3irs.v2015.print.anexoh.AnexoHPrinter;
import pt.dgci.modelo3irs.v2015.print.anexoi.AnexoIPrinter;
import pt.dgci.modelo3irs.v2015.print.anexoj.AnexoJPrinter;
import pt.dgci.modelo3irs.v2015.print.anexol.AnexoLPrinter;
import pt.dgci.modelo3irs.v2015.print.anexoss.AnexoSSPrinter;
import pt.dgci.modelo3irs.v2015.print.rosto.RostoPrinter;
import pt.dgci.modelo3irs.v2015.print.util.Modelo3IRSPrinterUtilities;
import pt.opensoft.pdf.DynamicPDFRenderer;

public class Modelo3IRSPrinter
extends DynamicPDFRenderer {
    private Modelo3IRSv2015Model model;

    public Modelo3IRSPrinter(OutputStream stream, Modelo3IRSv2015Model model) throws FileNotFoundException, DocumentException {
        super(stream);
        this.model = model;
    }

    public void render() throws IOException, DocumentException {
        this.openDocument();
        this.buildDocument();
        this.closeDocument();
    }

    private void buildDocument() throws DocumentException, MalformedURLException, IOException {
        AnexoG1Model anexoG1;
        List<AnexoJModel> anexoJList;
        AnexoFModel anexoF;
        List<AnexoBModel> anexoBList;
        List<AnexoCModel> anexoCList;
        AnexoHModel anexoH;
        AnexoEModel anexoE;
        List<AnexoSSModel> anexoSSList;
        AnexoGModel anexoG;
        List<AnexoIModel> anexoIList;
        List<AnexoDModel> anexoDList;
        List<AnexoLModel> anexoLList;
        this.startTable();
        this.startCell(1, 1, 1, 5, 0, true);
        this.field("Esta Listagem n\u00e3o serve como Comprovativo");
        this.endCell();
        this.startCell(1, 1, 1, 5, 0, true);
        this.field("Declara\u00e7\u00e3o de IRS");
        this.endCell();
        this.endTable();
        this.addNewLine();
        PrintLabelManagerIRS labelManager = new PrintLabelManagerIRS();
        Modelo3IRSPrinterUtilities printerUtils = new Modelo3IRSPrinterUtilities();
        printerUtils.initImages();
        labelManager.getResource("Rosto");
        RostoModel rosto = this.model.getRosto();
        RostoPrinter rostoPrinter = new RostoPrinter(rosto, labelManager, this, printerUtils);
        rostoPrinter.render();
        this.newPage();
        AnexoAModel anexoA = this.model.getAnexoA();
        if (anexoA != null) {
            labelManager.getResource("AnexoA");
            AnexoAPrinter anexoAPrinter = new AnexoAPrinter(anexoA, labelManager, this, printerUtils);
            anexoAPrinter.render();
            this.newPage();
        }
        if ((anexoH = this.model.getAnexoH()) != null) {
            labelManager.getResource("AnexoH");
            AnexoHPrinter anexoHPrinter = new AnexoHPrinter(anexoH, labelManager, this, printerUtils);
            anexoHPrinter.render();
            this.newPage();
        }
        if ((anexoJList = this.model.getAnexoJ()) != null && anexoJList.size() > 0) {
            for (int i = 0; i < anexoJList.size(); ++i) {
                AnexoJModel anexoJ = anexoJList.get(i);
                labelManager.getResource("AnexoJ");
                AnexoJPrinter anexoJPrinter = new AnexoJPrinter(anexoJ, labelManager, this, printerUtils);
                anexoJPrinter.render();
                this.newPage();
            }
        }
        if ((anexoBList = this.model.getAnexoB()) != null && anexoBList.size() > 0) {
            for (int i = 0; i < anexoBList.size(); ++i) {
                AnexoBModel anexoB = anexoBList.get(i);
                labelManager.getResource("AnexoB");
                AnexoBPrinter anexoBPrinter = new AnexoBPrinter(anexoB, labelManager, this, printerUtils);
                anexoBPrinter.render();
                this.newPage();
            }
        }
        if ((anexoCList = this.model.getAnexoC()) != null && anexoCList.size() > 0) {
            for (int i = 0; i < anexoCList.size(); ++i) {
                AnexoCModel anexoC = anexoCList.get(i);
                labelManager.getResource("AnexoC");
                AnexoCPrinter anexoCPrinter = new AnexoCPrinter(anexoC, labelManager, this, printerUtils);
                anexoCPrinter.render();
                this.newPage();
            }
        }
        if ((anexoDList = this.model.getAnexoD()) != null && anexoDList.size() > 0) {
            for (int i = 0; i < anexoDList.size(); ++i) {
                AnexoDModel anexoD = anexoDList.get(i);
                labelManager.getResource("AnexoD");
                AnexoDPrinter anexoDPrinter = new AnexoDPrinter(anexoD, labelManager, this, printerUtils);
                anexoDPrinter.render();
                this.newPage();
            }
        }
        if ((anexoE = this.model.getAnexoE()) != null) {
            labelManager.getResource("AnexoE");
            AnexoEPrinter anexoEPrinter = new AnexoEPrinter(anexoE, labelManager, this, printerUtils);
            anexoEPrinter.render();
            this.newPage();
        }
        if ((anexoF = this.model.getAnexoF()) != null) {
            labelManager.getResource("AnexoF");
            AnexoFPrinter anexoFPrinter = new AnexoFPrinter(anexoF, labelManager, this, printerUtils);
            anexoFPrinter.render();
            this.newPage();
        }
        if ((anexoG = this.model.getAnexoG()) != null) {
            labelManager.getResource("AnexoG");
            AnexoGPrinter anexoGPrinter = new AnexoGPrinter(anexoG, labelManager, this, printerUtils);
            anexoGPrinter.render();
            this.newPage();
        }
        if ((anexoG1 = this.model.getAnexoG1()) != null) {
            labelManager.getResource("AnexoG1");
            AnexoG1Printer anexoG1Printer = new AnexoG1Printer(anexoG1, labelManager, this, printerUtils);
            anexoG1Printer.render();
            this.newPage();
        }
        if ((anexoIList = this.model.getAnexoI()) != null && anexoIList.size() > 0) {
            for (int i = 0; i < anexoIList.size(); ++i) {
                AnexoIModel anexoI = anexoIList.get(i);
                labelManager.getResource("AnexoI");
                AnexoIPrinter anexoIPrinter = new AnexoIPrinter(anexoI, labelManager, this, printerUtils);
                anexoIPrinter.render();
                this.newPage();
            }
        }
        if ((anexoLList = this.model.getAnexoL()) != null && anexoLList.size() > 0) {
            for (int i = 0; i < anexoLList.size(); ++i) {
                AnexoLModel anexoL = anexoLList.get(i);
                labelManager.getResource("AnexoL");
                AnexoLPrinter anexoLPrinter = new AnexoLPrinter(anexoL, labelManager, this, printerUtils);
                anexoLPrinter.render();
                this.newPage();
            }
        }
        if ((anexoSSList = this.model.getAnexoSS()) != null && anexoSSList.size() > 0) {
            for (int i = 0; i < anexoSSList.size(); ++i) {
                AnexoSSModel anexoSS = anexoSSList.get(i);
                labelManager.getResource("AnexoSS");
                AnexoSSPrinter anexoSSPrinter = new AnexoSSPrinter(anexoSS, labelManager, this, printerUtils);
                anexoSSPrinter.render();
                this.newPage();
            }
        }
    }

    public void addNewLine() throws BadElementException, DocumentException {
        this.newLine();
        this.emptySpace();
    }
}

