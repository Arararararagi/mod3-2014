/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.flatfile;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.flatfile.ObtainFlatFileFields;
import pt.opensoft.io.ResourceLoader;
import pt.opensoft.io.ResourceNotFoundException;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class AnexoXMLWrite
extends DefaultHandler {
    private static final String CAMPO = "field";
    private static final String MAPPING = "mapping";
    private static final String RANGEVALIDATOR = "range-validator";
    private static final String RADIOMAPPING = "radio-mapping";
    private static final String LIMITS = "limits";
    private static final String LINK = "link";
    private static final String ANEXO = "anexo";
    private static final String FILE = "file";
    private static final String INCLUDE = "include";
    protected Contexto contexto;
    protected ContextoTabela contextoTabela;
    protected ContextoInclude contextoInclude;
    protected AnexoModel anexo;
    private DeclaracaoModel declaracaoModel;
    private StringWriter anexoFlatFile;
    private ValidationResult validationResult;
    private String dataRecepcao;
    private String indicadorReembolso;
    private String nifHashTabela;
    protected String filename;
    protected SAXParser parserForAnexo;
    protected SAXParser parserForTabelas;

    public AnexoXMLWrite(SAXParser parserForAnexo, SAXParser parserForTabelas, DeclaracaoModel declaracaoModel, AnexoModel anexo, String dataRecepcao, String indicadorReembolso, String nifHashTabela) {
        this(parserForAnexo, parserForTabelas, declaracaoModel, anexo, anexo.getFormKey().getId(), dataRecepcao, indicadorReembolso, nifHashTabela);
    }

    public AnexoXMLWrite(SAXParser parserForAnexo, SAXParser parserForTabelas, DeclaracaoModel declaracaoModel, AnexoModel anexo, String dataRecepcao, String indicadorReembolso) {
        this(parserForAnexo, parserForTabelas, declaracaoModel, anexo, anexo.getFormKey().getId(), dataRecepcao, indicadorReembolso, "");
    }

    public AnexoXMLWrite(SAXParser parserForAnexo, SAXParser parserForTabelas, DeclaracaoModel declaracaoModel, String fileName, String dataRecepcao, String indicadorReembolso) {
        this(parserForAnexo, parserForTabelas, declaracaoModel, null, fileName, dataRecepcao, indicadorReembolso, "");
    }

    private AnexoXMLWrite(SAXParser parserForAnexo, SAXParser parserForTabelas, DeclaracaoModel declaracaoModel, AnexoModel anexo, String fileName, String dataRecepcao, String indicadorReembolso, String nifHashTabela) {
        this.declaracaoModel = declaracaoModel;
        this.anexo = anexo;
        this.anexoFlatFile = new StringWriter();
        this.validationResult = new ValidationResult();
        this.dataRecepcao = dataRecepcao;
        this.indicadorReembolso = indicadorReembolso;
        this.nifHashTabela = nifHashTabela;
        this.parserForAnexo = parserForAnexo;
        this.parserForTabelas = parserForTabelas;
        this.filename = fileName;
    }

    public void loadDefinitionsFromFile() {
        this.parserForAnexo.reset();
        this.read(this.parserForAnexo, this.filename);
    }

    protected void read(SAXParser parser, String xmlFileName) {
        this.read(parser, xmlFileName, null);
    }

    private void read(SAXParser parser, String xmlFileName, String xmlFilePath) {
        String resourceName = (xmlFilePath == null ? new StringBuilder().append(this.getDescriptorsFolder()).append(xmlFileName).toString() : new StringBuilder().append(xmlFilePath).append(xmlFileName).toString()) + (!xmlFileName.endsWith(".xml") ? ".xml" : "");
        try {
            SimpleLog.log("File: " + resourceName);
            InputStream stream = ResourceLoader.getResourceStream(resourceName);
            parser.parse(stream, (DefaultHandler)this);
        }
        catch (ResourceNotFoundException e) {
            throw new RuntimeException("Could not load file: " + resourceName, e);
        }
        catch (SAXException se) {
            throw new RuntimeException(se);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected String getDescriptorsFolder() {
        return Modelo3IRSv2015Parameters.instance().getFlatfileFolder();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("field")) {
            Integer length = attributes.getValue("length") != null ? Integer.valueOf(attributes.getValue("length")) : null;
            boolean signed = Boolean.parseBoolean(attributes.getValue("signed"));
            this.contexto = new Contexto(attributes.getValue("name"), attributes.getValue("description"), attributes.getValue("type"), length, attributes.getValue("emptyValue"), attributes.getValue("format"), attributes.getValue("constant"), signed, this.anexo);
        } else if (qName.equals("mapping")) {
            this.contexto.quadro = attributes.getValue("quadro");
            this.contexto.field = attributes.getValue("field");
        } else if (qName.equals("range-validator")) {
            this.contexto.values = attributes.getValue("values").split(",");
        } else if (qName.equals("radio-mapping")) {
            this.contexto.addRadioMapping(attributes.getValue("value"), attributes.getValue("target"));
        } else if (qName.equals("limits")) {
            this.contexto.min = attributes.getValue("min") != null ? Long.valueOf(Long.parseLong(attributes.getValue("min"))) : null;
            this.contexto.max = attributes.getValue("max") != null ? Long.valueOf(Long.parseLong(attributes.getValue("max"))) : null;
        } else if (qName.equals("link")) {
            this.contexto.link = attributes.getValue("value");
        } else if (qName.equals("anexo")) {
            if (attributes.getValue("name") != null) {
                List<FormKey> listAnexos = this.declaracaoModel.getAllAnexosByType(attributes.getValue("name"));
                FormKey key = listAnexos.get(0);
                this.contexto.anexo = this.declaracaoModel.getAnexo(key);
            }
        } else if (qName.equals("file")) {
            this.contexto.tabelaFile = attributes.getValue("name");
        } else if (qName.equals("include")) {
            this.contextoInclude = new ContextoInclude(attributes.getValue("file"), attributes.getValue("path"));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        try {
            if (qName.equals("field") && this.contexto != null) {
                this.preHandle();
                if (this.contexto.constant != null) {
                    this.handleConstant();
                } else if (this.contexto.name.startsWith("numAnexos")) {
                    this.handleNumAnexos();
                } else if (this.contexto.name.equals("dataRecepcao")) {
                    this.handleDataRecepcao();
                } else if (this.contexto.name.equals("indicadorReembolso")) {
                    this.handleIndicadorReembolso();
                } else if (this.contexto.name.equals("nifHashTabela")) {
                    this.handleNifHashTabela();
                } else if (this.contexto.name.equals("RostonifHashT1_Nif")) {
                    this.handleNifHash_Nif();
                } else if (this.contexto.name.equals("RostonifHashT1_Hash")) {
                    this.handleNifHash_Hash();
                } else if (this.contexto.name.startsWith("numOcorrencias")) {
                    this.handleNumOcorrencias();
                } else if (this.contexto.name.startsWith("tabela")) {
                    this.handleTabela();
                } else {
                    this.handleCampo();
                }
            } else if (qName.equals("include") && this.contextoInclude != null) {
                this.handleInclude();
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void preHandle() {
    }

    protected void handleConstant() {
        this.anexoFlatFile.append((CharSequence)this.contexto.constant);
    }

    protected void handleNumAnexos() {
        String anexoStr = "Anexo" + this.contexto.name.substring(9);
        int countAnexos = this.declaracaoModel.countAnexosByType(anexoStr);
        if (this.contexto.min != null && (long)countAnexos < this.contexto.min || this.contexto.max != null && (long)countAnexos > this.contexto.max) {
            this.validationResult.add(new DeclValidationMessage("998", "", null, new String[]{anexoStr}));
        } else {
            this.anexoFlatFile.append((CharSequence)String.valueOf(countAnexos));
        }
    }

    protected void handleDataRecepcao() {
        if (StringUtil.isEmpty(this.dataRecepcao)) {
            this.anexoFlatFile.append((CharSequence)this.contexto.emptyValue);
        } else {
            this.anexoFlatFile.append((CharSequence)this.dataRecepcao);
        }
    }

    protected void handleIndicadorReembolso() {
        if (StringUtil.isEmpty(this.indicadorReembolso)) {
            this.anexoFlatFile.append((CharSequence)this.contexto.emptyValue);
        } else {
            boolean valid = false;
            for (int k = 0; k < this.contexto.values.length; ++k) {
                if (!this.contexto.values[k].equals(this.indicadorReembolso)) continue;
                valid = true;
            }
            if (valid) {
                this.anexoFlatFile.append((CharSequence)this.indicadorReembolso);
            } else {
                throw new RuntimeException("Indicador de reembolso inv\u00e1lido.");
            }
        }
    }

    protected void handleNumOcorrencias() throws Exception {
        String fieldAsFlatFileString = ObtainFlatFileFields.obtainTableSize(this.contexto.anexo, this.contexto.quadro, this.contexto.field, this.contexto.length, this.contexto.type);
        String link = this.getLink();
        if (this.validateLimits(Long.valueOf(fieldAsFlatFileString), link)) {
            this.anexoFlatFile.append((CharSequence)fieldAsFlatFileString);
        }
    }

    protected void handleTabela() throws Exception {
        List tableLines = (List)ObtainFlatFileFields.obtainSimpleField(this.contexto.anexo, this.contexto.quadro, this.contexto.field, this.contexto.length, this.contexto.type, this.contexto.emptyValue, this.contexto.format);
        if (tableLines != null) {
            String xmlFileName = StringUtil.isEmpty(this.contexto.tabelaFile) ? this.contexto.field : this.contexto.tabelaFile;
            this.contextoTabela = new ContextoTabela(this.contexto.field, xmlFileName, tableLines, 0);
            for (int k = 0; k < this.contextoTabela.lines.size(); ++k) {
                this.contextoTabela.currentLineNumber = k;
                this.parserForTabelas.reset();
                this.read(this.parserForTabelas, this.contextoTabela.xmlFileName);
            }
        }
        this.contexto = null;
        this.contextoTabela = null;
    }

    protected void handleNifHashTabela() {
        if (!StringUtil.isEmpty(this.nifHashTabela)) {
            int countAllAnexos = this.declaracaoModel.getAnexos().size();
            if (this.nifHashTabela.length() != countAllAnexos * 49) {
                throw new RuntimeException("Conjunto de Nif-Hash's passados inv\u00e1lido. S\u00f3 \u00e9 poss\u00edvel converter para flatfile se todos os anexos tiverem NIF associado.");
            }
            if (countAllAnexos > 0) {
                String xmlFileName = this.contexto.field;
                this.contextoTabela = new ContextoTabela(this.contexto.field, xmlFileName, null, 0);
                for (int k = 0; k < countAllAnexos; ++k) {
                    this.contextoTabela.currentLineNumber = k;
                    this.parserForTabelas.reset();
                    this.read(this.parserForTabelas, this.contextoTabela.xmlFileName);
                }
            }
        }
        this.contexto = null;
        this.contextoTabela = null;
    }

    protected void handleNifHash_Nif() {
        if (!StringUtil.isEmpty(this.nifHashTabela)) {
            String nif = this.nifHashTabela.substring(this.contextoTabela.currentLineNumber * 49, this.contextoTabela.currentLineNumber * 49 + 9);
            this.anexoFlatFile.append((CharSequence)nif);
        }
    }

    protected void handleNifHash_Hash() {
        if (!StringUtil.isEmpty(this.nifHashTabela)) {
            String hash = this.nifHashTabela.substring(this.contextoTabela.currentLineNumber * 49 + 9, this.contextoTabela.currentLineNumber * 49 + 9 + 40);
            this.anexoFlatFile.append((CharSequence)hash);
        }
    }

    protected void handleInclude() throws Exception {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser parserForInclude = saxParserFactory.newSAXParser();
        this.read(parserForInclude, this.contextoInclude.fileName, this.contextoInclude.filePath);
        this.contexto = null;
        this.contextoInclude = null;
    }

    protected void handleCampo() throws Exception {
        Object fieldAsObject = null;
        fieldAsObject = this.contextoTabela == null ? ObtainFlatFileFields.obtainSimpleField(this.contexto.anexo, this.contexto.quadro, this.contexto.field, this.contexto.length, this.contexto.type, this.contexto.emptyValue, this.contexto.format) : ObtainFlatFileFields.obtainFieldInLine(this.contextoTabela.getCurrentLine(), this.contexto.name);
        String link = this.getLink();
        String fieldAsFlatFileString = ObtainFlatFileFields.getValorAsFlatFileString(fieldAsObject, this.contexto.length, this.contexto.type, this.contexto.emptyValue, this.contexto.format, this.contexto.signed);
        if (fieldAsFlatFileString.length() != this.contexto.length.intValue()) {
            this.validationResult.add(new DeclValidationMessage("999", link, new String[]{link}));
            return;
        }
        if (!(this.validateType(fieldAsObject, link) && this.validateSigned(fieldAsObject, fieldAsFlatFileString, link) && this.validateRangevalidator(fieldAsFlatFileString, link) && this.validateLimits(fieldAsObject, link))) {
            return;
        }
        if (this.checkRadioMapping(fieldAsFlatFileString)) {
            this.anexoFlatFile.append((CharSequence)((String)this.contexto.radiomapping.get(fieldAsFlatFileString)));
        } else {
            this.anexoFlatFile.append((CharSequence)fieldAsFlatFileString);
        }
    }

    protected boolean validateType(Object fieldAsObject, String link) {
        if (fieldAsObject != null && this.contexto.type.equals("N") && !StringUtil.isSignedNumeric(fieldAsObject.toString())) {
            this.validationResult.add(new DeclValidationMessage("999", link, new String[]{link}));
            return false;
        }
        return true;
    }

    protected boolean validateSigned(Object fieldAsObject, String fieldAsFlatFileString, String link) {
        if (fieldAsObject != null && !this.contexto.signed && this.contexto.type.equals("N") && Long.valueOf(fieldAsObject.toString()) < 0) {
            this.validationResult.add(new DeclValidationMessage("999", link, new String[]{link}));
            return false;
        }
        return true;
    }

    protected boolean validateRangevalidator(String value, String link) {
        if (this.contexto.values != null) {
            for (int k = 0; k < this.contexto.values.length; ++k) {
                if (!this.contexto.values[k].equals(value)) continue;
                return true;
            }
            this.validationResult.add(new DeclValidationMessage("999", link, new String[]{link}));
            return false;
        }
        return true;
    }

    protected boolean validateLimits(Object objectCampo, String link) {
        if (this.contexto.min != null || this.contexto.max != null) {
            long value;
            long l = value = objectCampo != null ? Long.parseLong(objectCampo.toString()) : 0;
            if (this.contexto.min != null && value < this.contexto.min || this.contexto.max != null && value > this.contexto.max) {
                this.validationResult.add(new DeclValidationMessage("999", link, new String[]{link}));
                return false;
            }
        }
        return true;
    }

    protected boolean checkRadioMapping(String value) {
        return this.contexto.radiomapping != null && !this.contexto.radiomapping.isEmpty() && this.contexto.radiomapping.containsKey(value);
    }

    public String getAnexoFlatFileVersion() {
        return this.anexoFlatFile.getBuffer().toString();
    }

    public ValidationResult getValidationResult() {
        return this.validationResult;
    }

    private String getLink() {
        String anexoIdSubId;
        String link = "";
        String campo = this.contextoTabela == null ? ".f" + this.contexto.field.substring(0, 1).toLowerCase() + this.contexto.field.substring(1) : ".t" + this.contextoTabela.name.substring(0, 1).toLowerCase() + this.contextoTabela.name.substring(1) + ".l" + (this.contextoTabela.currentLineNumber + 1) + ".c" + this.contexto.field;
        String string = anexoIdSubId = this.contexto.anexo.getFormKey().getSubId() == null ? this.contexto.anexo.getFormKey().getId() : this.contexto.anexo.getFormKey().getId() + "|" + this.contexto.anexo.getFormKey().getSubId();
        if (this.contexto.link == null) {
            link = "a" + anexoIdSubId + ".qQuadro" + this.contexto.quadro + campo;
        } else {
            link = this.contexto.link;
            if (link.contains((CharSequence)"subId") && this.contexto.anexo.getFormKey().getSubId() != null) {
                link = link.replaceFirst("subId", this.contexto.anexo.getFormKey().getSubId());
            }
        }
        return link;
    }

    public String getFilename() {
        return this.filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    protected class Contexto {
        protected String name;
        protected String description;
        protected String type;
        protected Integer length;
        protected String emptyValue;
        protected String format;
        protected String constant;
        protected boolean signed;
        protected String quadro;
        protected String field;
        protected String[] values;
        protected ListHashMap radiomapping;
        protected Long min;
        protected Long max;
        protected String link;
        protected AnexoModel anexo;
        protected String tabelaFile;

        public Contexto(String name, String description, String type, Integer length, String emptyValue, String format, String constant, boolean signed, AnexoModel anexo) {
            this.name = name;
            this.description = description;
            this.type = type;
            this.length = length;
            this.emptyValue = emptyValue;
            this.format = format;
            this.constant = constant;
            this.signed = signed;
            this.anexo = anexo;
        }

        public void addRadioMapping(String value, String target) {
            if (this.radiomapping == null) {
                this.radiomapping = new ListHashMap();
                this.radiomapping.put(value, target);
            } else {
                this.radiomapping.put(value, target);
            }
        }

        public String getName() {
            return this.name;
        }

        public String getDescription() {
            return this.description;
        }

        public String getType() {
            return this.type;
        }

        public Integer getLength() {
            return this.length;
        }

        public String getEmptyValue() {
            return this.emptyValue;
        }

        public String getFormat() {
            return this.format;
        }

        public String getConstant() {
            return this.constant;
        }

        public boolean isSigned() {
            return this.signed;
        }

        public String getQuadro() {
            return this.quadro;
        }

        public String getField() {
            return this.field;
        }

        public String[] getValues() {
            return this.values;
        }

        public ListHashMap getRadiomapping() {
            return this.radiomapping;
        }

        public Long getMin() {
            return this.min;
        }

        public Long getMax() {
            return this.max;
        }

        public String getLink() {
            return this.link;
        }

        public AnexoModel getAnexo() {
            return this.anexo;
        }

        public String getTabelaFile() {
            return this.tabelaFile;
        }

        public String toString() {
            return "name=" + this.name + ";type=" + this.type + ";length=" + this.length + ";quadro=" + this.quadro + ";field=" + this.field + ";anexo=" + (this.anexo != null ? this.anexo.getFormKey() : null);
        }
    }

    private class ContextoInclude {
        private String fileName;
        private String filePath;

        public ContextoInclude(String name, String filePath) {
            this.fileName = name;
            this.filePath = filePath;
        }
    }

    private class ContextoTabela {
        private String name;
        private int currentLineNumber;
        private String xmlFileName;
        private List<Model> lines;

        public ContextoTabela(String name, String xmlFileName, List<Model> lines, int currentLineNumber) {
            this.name = name;
            this.xmlFileName = xmlFileName;
            this.lines = lines;
            this.currentLineNumber = currentLineNumber;
        }

        public Model getCurrentLine() {
            return this.lines.get(this.currentLineNumber);
        }
    }

}

