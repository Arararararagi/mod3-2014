/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.flatfile;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import pt.opensoft.io.VariableSizeWriter;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.util.Date;
import pt.opensoft.util.StringUtil;

public class ObtainFlatFileFields {
    public static final String NUMERIC = "N";
    public static final String CHARACTER = "C";
    public static final String DATE = "D";

    public static Object obtainSimpleField(Integer tamanho, String tipo, String constant) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException {
        return ObtainFlatFileFields.obtainSimpleField(null, null, null, tamanho, tipo, null, null, constant);
    }

    public static Object obtainSimpleField(AnexoModel anexo, String quadro, String campo, Integer tamanho, String tipo) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException {
        return ObtainFlatFileFields.obtainSimpleField(anexo, quadro, campo, tamanho, tipo, null, null, null);
    }

    public static Object obtainSimpleField(AnexoModel anexo, String quadro, String campo, Integer tamanho, String tipo, String emptyValue) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException {
        return ObtainFlatFileFields.obtainSimpleField(anexo, quadro, campo, tamanho, tipo, emptyValue, null, null);
    }

    public static Object obtainSimpleField(AnexoModel anexo, String quadro, String campo, Integer tamanho, String tipo, String emptyValue, String format) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException {
        return ObtainFlatFileFields.obtainSimpleField(anexo, quadro, campo, tamanho, tipo, emptyValue, format, null);
    }

    public static Object obtainSimpleField(AnexoModel anexo, String quadro, String campo, Integer tamanho, String tipo, String emptyValue, String format, String constant) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException {
        if (!StringUtil.isEmpty(constant)) {
            return constant;
        }
        Method metodoQuadro = anexo.getClass().getMethod("getQuadro" + quadro, new Class[0]);
        Object objectoQuadro = metodoQuadro.invoke(anexo, new Object[0]);
        Method metodoCampo = objectoQuadro.getClass().getMethod("get" + campo, new Class[0]);
        Object objectCampo = metodoCampo.invoke(objectoQuadro, new Object[0]);
        return objectCampo;
    }

    public static String obtainTableSize(AnexoModel anexo, String quadro, String campo, Integer tamanho, String tipo) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException {
        Method metodoQuadro = anexo.getClass().getMethod("getQuadro" + quadro, new Class[0]);
        Object objectoQuadro = metodoQuadro.invoke(anexo, new Object[0]);
        Method metodoCampo = objectoQuadro.getClass().getMethod("get" + campo, new Class[0]);
        Object objectCampo = metodoCampo.invoke(objectoQuadro, new Object[0]);
        Method metodoSize = objectCampo.getClass().getMethod("size", new Class[0]);
        Object objectSize = metodoSize.invoke(objectCampo, new Object[0]);
        String valorCampo = objectSize != null ? objectSize.toString() : "0";
        StringWriter sWriter = new StringWriter();
        VariableSizeWriter varWriter = new VariableSizeWriter((Writer)sWriter, Integer.MAX_VALUE);
        varWriter.writeNumeric(valorCampo, (int)tamanho);
        return sWriter.getBuffer().toString();
    }

    public static Object obtainFieldInLine(Object tablePosition, String campo) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Method getCampo = tablePosition.getClass().getMethod("get" + campo, new Class[0]);
        return getCampo.invoke(tablePosition, new Object[0]);
    }

    public static String getValorAsFlatFileString(Object objectCampo, Integer tamanho, String tipo, String emptyValue, String format, boolean signed) throws IOException {
        String valorAsFlatFileString = "";
        if (objectCampo != null) {
            if (objectCampo instanceof Boolean) {
                valorAsFlatFileString = Boolean.parseBoolean(objectCampo.toString()) ? "S" : "N";
            } else {
                if (tipo.equals("D")) {
                    return ((Date)objectCampo).format(format);
                }
                valorAsFlatFileString = objectCampo.toString();
            }
        } else if (emptyValue != null) {
            valorAsFlatFileString = emptyValue;
        }
        if (valorAsFlatFileString.length() < tamanho) {
            StringWriter sWriter = new StringWriter();
            VariableSizeWriter varWriter = new VariableSizeWriter((Writer)sWriter, Integer.MAX_VALUE);
            if (tipo.equals("C")) {
                varWriter.writeString(valorAsFlatFileString, tamanho);
                return sWriter.getBuffer().toString();
            }
            if (tipo.equals("N")) {
                varWriter.writeNumeric(valorAsFlatFileString, (int)tamanho);
                valorAsFlatFileString = sWriter.getBuffer().toString();
            }
        }
        if (signed) {
            valorAsFlatFileString = ObtainFlatFileFields.getSignedString(objectCampo, valorAsFlatFileString);
        }
        return valorAsFlatFileString;
    }

    private static String getSignedString(Object fieldAsObject, String fieldAsFlatFileString) {
        if (fieldAsObject != null && Long.valueOf(fieldAsObject.toString()) < 0) {
            fieldAsFlatFileString = fieldAsFlatFileString.replace((CharSequence)"-", (CharSequence)"");
            fieldAsFlatFileString = "-" + fieldAsFlatFileString;
            return fieldAsFlatFileString;
        }
        fieldAsFlatFileString = "+" + fieldAsFlatFileString.substring(1);
        return fieldAsFlatFileString;
    }
}

