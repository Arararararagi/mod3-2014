/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.flatfile;

import com.jgoodies.validation.ValidationResult;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.flatfile.AnexoXMLWrite;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Timer;

public class WriteDeclarationToFlatFile {
    private StringWriter sflatfile = new StringWriter();
    private ValidationResult validationResult = new ValidationResult();
    private StringWriter nifHashPorAnexoSW;
    private Map<String, String> nifHashPorAnexoImpressoId = new HashMap<String, String>();
    private DeclaracaoModel declaracaoModel;
    private String dataRecepcao;
    private String indicadorReembolso;
    private String nifHashPorAnexoStr;
    private boolean jaCalculadoNifHashPorAnexo;
    private SAXParser parserForAnexos;
    private SAXParser parserForTabelas;
    private long initSaxParsersTimes;
    private long flatFileConversionTimes;
    private long hashCalculationTimes;
    private static final String ROSTO_IMPRESSOS_ID = "A0";
    private static final String ANEXOA_IMPRESSOS_ID = "A1";
    private static final String ANEXOB_IMPRESSOS_ID = "B0";
    private static final String ANEXOC_IMPRESSOS_ID = "C0";
    private static final String ANEXOD_IMPRESSOS_ID = "D0";
    private static final String ANEXOE_IMPRESSOS_ID = "E0";
    private static final String ANEXOF_IMPRESSOS_ID = "F0";
    private static final String ANEXOG_IMPRESSOS_ID = "G0";
    private static final String ANEXOG1_IMPRESSOS_ID = "G1";
    private static final String ANEXOH_IMPRESSOS_ID = "H1";
    private static final String ANEXOI_IMPRESSOS_ID = "I0";
    private static final String ANEXOJ_IMPRESSOS_ID = "J0";
    private static final String ANEXOL_IMPRESSOS_ID = "L0";
    private static final String ANEXOSS_IMPRESSOS_ID = "S0";
    private String rostoNifHash = "";
    private String anexoANifHash = "";
    private List<String> anexosBNifHash = new ArrayList<String>();
    private List<String> anexosCNifHash = new ArrayList<String>();
    private List<String> anexosDNifHash = new ArrayList<String>();
    private String anexoENifHash = "";
    private String anexoFNifHash = "";
    private String anexoGNifHash = "";
    private String anexoG1NifHash = "";
    private String anexoHNifHash = "";
    private List<String> anexosINifHash = new ArrayList<String>();
    private List<String> anexosJNifHash = new ArrayList<String>();
    private List<String> anexosLNifHash = new ArrayList<String>();
    private List<String> anexosSSNifHash = new ArrayList<String>();

    public WriteDeclarationToFlatFile(SAXParser parser, DeclaracaoModel declaracaoModel, String dataRecepcao, String indicadorReembolso, String nifHashPorAnexoStr) {
        this.declaracaoModel = declaracaoModel;
        this.dataRecepcao = dataRecepcao;
        this.indicadorReembolso = indicadorReembolso;
        this.nifHashPorAnexoStr = nifHashPorAnexoStr;
        boolean bl = this.jaCalculadoNifHashPorAnexo = !StringUtil.isEmpty(nifHashPorAnexoStr);
        if (!this.jaCalculadoNifHashPorAnexo) {
            this.nifHashPorAnexoSW = new StringWriter();
        }
        try {
            Timer timerForInitSaxParsers = new Timer();
            timerForInitSaxParsers.start();
            try {
                this.initSAXParsers(parser);
            }
            finally {
                this.initSaxParsersTimes = timerForInitSaxParsers.ellapsed();
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public WriteDeclarationToFlatFile(DeclaracaoModel declaracaoModel, String dataRecepcao, String indicadorReembolso, String nifHashPorAnexoStr) {
        this.declaracaoModel = declaracaoModel;
        this.dataRecepcao = dataRecepcao;
        this.indicadorReembolso = indicadorReembolso;
        this.nifHashPorAnexoStr = nifHashPorAnexoStr;
        boolean bl = this.jaCalculadoNifHashPorAnexo = !StringUtil.isEmpty(nifHashPorAnexoStr);
        if (!this.jaCalculadoNifHashPorAnexo) {
            this.nifHashPorAnexoSW = new StringWriter();
        }
        try {
            Timer timerForInitSaxParsers = new Timer();
            timerForInitSaxParsers.start();
            try {
                this.initSAXParsers();
            }
            finally {
                this.initSaxParsersTimes = timerForInitSaxParsers.ellapsed();
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean process() {
        FormKey key;
        String anexoflatfile;
        String nif;
        String hash;
        String nif2;
        String nif3;
        String hash2;
        String anexoflatfile2;
        String anexoflatfile3;
        List<FormKey> listAnexos;
        String hash3;
        Timer timerForFlatFileConversion = new Timer();
        Timer timerForHashCalculation = new Timer();
        AnexoModel anexo = null;
        AnexoXMLWrite xmlWrite = null;
        StringWriter anexosflatfile = new StringWriter();
        anexo = this.declaracaoModel.getAnexo(RostoModel.class);
        if (anexo != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            String anexoflatfileWithouAnexosNifHashs = xmlWrite.getAnexoFlatFileVersion();
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
            if (!this.jaCalculadoNifHashPorAnexo) {
                timerForHashCalculation.start();
                nif = String.valueOf(((RostoModel)anexo).getQuadro03().getQ03C03());
                hash2 = this.toSha1(anexoflatfileWithouAnexosNifHashs);
                this.rostoNifHash = nif + hash2;
                this.nifHashPorAnexoImpressoId.put("A0", nif + hash2);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if ((anexo = this.declaracaoModel.getAnexo(AnexoAModel.class)) != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            anexoflatfile = xmlWrite.getAnexoFlatFileVersion();
            anexosflatfile.append((CharSequence)"&AnexoA=").append((CharSequence)anexoflatfile);
            this.validationResult.addAllFrom(xmlWrite.getValidationResult());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
            if (!this.jaCalculadoNifHashPorAnexo) {
                timerForHashCalculation.start();
                nif = String.valueOf(((AnexoAModel)anexo).getQuadro03().getAnexoAq03C02());
                hash2 = this.toSha1(anexoflatfile);
                this.anexoANifHash = nif + hash2;
                this.nifHashPorAnexoImpressoId.put("A1", nif + hash2);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if ((anexo = this.declaracaoModel.getAnexo(AnexoHModel.class)) != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            anexoflatfile = xmlWrite.getAnexoFlatFileVersion();
            anexosflatfile.append((CharSequence)"&AnexoH=").append((CharSequence)anexoflatfile);
            this.validationResult.addAllFrom(xmlWrite.getValidationResult());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
            if (!this.jaCalculadoNifHashPorAnexo) {
                timerForHashCalculation.start();
                nif = String.valueOf(((AnexoHModel)anexo).getQuadro03().getAnexoHq03C02());
                hash2 = this.toSha1(anexoflatfile);
                this.anexoHNifHash = nif + hash2;
                this.nifHashPorAnexoImpressoId.put("H1", nif + hash2);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (!(listAnexos = this.declaracaoModel.getAllAnexosByType("AnexoJ")).isEmpty()) {
            for (int k = 0; k < listAnexos.size(); ++k) {
                timerForFlatFileConversion.start();
                key = listAnexos.get(k);
                anexo = this.declaracaoModel.getAnexo(key);
                xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
                xmlWrite.loadDefinitionsFromFile();
                anexoflatfile3 = xmlWrite.getAnexoFlatFileVersion();
                anexosflatfile.append((CharSequence)"&AnexoJ=").append((CharSequence)anexoflatfile3);
                this.validationResult.addAllFrom(xmlWrite.getValidationResult());
                this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
                if (this.jaCalculadoNifHashPorAnexo) continue;
                timerForHashCalculation.start();
                nif3 = String.valueOf(((AnexoJModel)anexo).getQuadro03().getAnexoJq03C04());
                hash = this.toSha1(anexoflatfile3);
                this.anexosJNifHash.add(nif3 + hash);
                this.nifHashPorAnexoImpressoId.put("J0" + k, nif3 + hash);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (!(listAnexos = this.declaracaoModel.getAllAnexosByType("AnexoB")).isEmpty()) {
            for (int k = 0; k < listAnexos.size(); ++k) {
                timerForFlatFileConversion.start();
                key = listAnexos.get(k);
                anexo = this.declaracaoModel.getAnexo(key);
                xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
                xmlWrite.loadDefinitionsFromFile();
                anexoflatfile3 = xmlWrite.getAnexoFlatFileVersion();
                anexosflatfile.append((CharSequence)"&AnexoB=").append((CharSequence)anexoflatfile3);
                this.validationResult.addAllFrom(xmlWrite.getValidationResult());
                this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
                if (this.jaCalculadoNifHashPorAnexo) continue;
                timerForHashCalculation.start();
                nif3 = String.valueOf(((AnexoBModel)anexo).getQuadro03().hasAnexoBq03C08() ? ((AnexoBModel)anexo).getQuadro03().getAnexoBq03C08() : ((AnexoBModel)anexo).getQuadro03().getAnexoBq03C09());
                hash = this.toSha1(anexoflatfile3);
                this.anexosBNifHash.add(nif3 + hash);
                this.nifHashPorAnexoImpressoId.put("B0" + k, nif3 + hash);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (!(listAnexos = this.declaracaoModel.getAllAnexosByType("AnexoC")).isEmpty()) {
            for (int k = 0; k < listAnexos.size(); ++k) {
                timerForFlatFileConversion.start();
                key = listAnexos.get(k);
                anexo = this.declaracaoModel.getAnexo(key);
                xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
                xmlWrite.loadDefinitionsFromFile();
                anexoflatfile3 = xmlWrite.getAnexoFlatFileVersion();
                anexosflatfile.append((CharSequence)"&AnexoC=").append((CharSequence)anexoflatfile3);
                this.validationResult.addAllFrom(xmlWrite.getValidationResult());
                this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
                if (this.jaCalculadoNifHashPorAnexo) continue;
                timerForHashCalculation.start();
                nif3 = String.valueOf(((AnexoCModel)anexo).getQuadro03().getNifTitular());
                hash = this.toSha1(anexoflatfile3);
                this.anexosCNifHash.add(nif3 + hash);
                this.nifHashPorAnexoImpressoId.put("C0" + k, nif3 + hash);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (!(listAnexos = this.declaracaoModel.getAllAnexosByType("AnexoD")).isEmpty()) {
            for (int k = 0; k < listAnexos.size(); ++k) {
                timerForFlatFileConversion.start();
                key = listAnexos.get(k);
                anexo = this.declaracaoModel.getAnexo(key);
                xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
                xmlWrite.loadDefinitionsFromFile();
                anexoflatfile3 = xmlWrite.getAnexoFlatFileVersion();
                anexosflatfile.append((CharSequence)"&AnexoD=").append((CharSequence)anexoflatfile3);
                this.validationResult.addAllFrom(xmlWrite.getValidationResult());
                this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
                if (this.jaCalculadoNifHashPorAnexo) continue;
                timerForHashCalculation.start();
                nif3 = String.valueOf(((AnexoDModel)anexo).getQuadro03().getAnexoDq03C06());
                hash = this.toSha1(anexoflatfile3);
                this.anexosDNifHash.add(nif3 + hash);
                this.nifHashPorAnexoImpressoId.put("D0" + k, nif3 + hash);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if ((anexo = this.declaracaoModel.getAnexo(AnexoEModel.class)) != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            anexoflatfile2 = xmlWrite.getAnexoFlatFileVersion();
            anexosflatfile.append((CharSequence)"&AnexoE=").append((CharSequence)anexoflatfile2);
            this.validationResult.addAllFrom(xmlWrite.getValidationResult());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
            if (!this.jaCalculadoNifHashPorAnexo) {
                timerForHashCalculation.start();
                nif2 = String.valueOf(((AnexoEModel)anexo).getQuadro03().getAnexoEq03C02());
                hash3 = this.toSha1(anexoflatfile2);
                this.anexoENifHash = nif2 + hash3;
                this.nifHashPorAnexoImpressoId.put("E0", nif2 + hash3);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if ((anexo = this.declaracaoModel.getAnexo(AnexoFModel.class)) != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            anexoflatfile2 = xmlWrite.getAnexoFlatFileVersion();
            anexosflatfile.append((CharSequence)"&AnexoF=").append((CharSequence)anexoflatfile2);
            this.validationResult.addAllFrom(xmlWrite.getValidationResult());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
            if (!this.jaCalculadoNifHashPorAnexo) {
                timerForHashCalculation.start();
                nif2 = String.valueOf(((AnexoFModel)anexo).getQuadro03().getAnexoFq03C02());
                hash3 = this.toSha1(anexoflatfile2);
                this.anexoFNifHash = nif2 + hash3;
                this.nifHashPorAnexoImpressoId.put("F0", nif2 + hash3);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if ((anexo = this.declaracaoModel.getAnexo(AnexoGModel.class)) != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            anexoflatfile2 = xmlWrite.getAnexoFlatFileVersion();
            anexosflatfile.append((CharSequence)"&AnexoG=").append((CharSequence)anexoflatfile2);
            this.validationResult.addAllFrom(xmlWrite.getValidationResult());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
            if (!this.jaCalculadoNifHashPorAnexo) {
                timerForHashCalculation.start();
                nif2 = String.valueOf(((AnexoGModel)anexo).getQuadro03().getAnexoGq03C02());
                hash3 = this.toSha1(anexoflatfile2);
                this.anexoGNifHash = nif2 + hash3;
                this.nifHashPorAnexoImpressoId.put("G0", nif2 + hash3);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if ((anexo = this.declaracaoModel.getAnexo(AnexoG1Model.class)) != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            anexoflatfile2 = xmlWrite.getAnexoFlatFileVersion();
            anexosflatfile.append((CharSequence)"&AnexoG1=").append((CharSequence)anexoflatfile2);
            this.validationResult.addAllFrom(xmlWrite.getValidationResult());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
            if (!this.jaCalculadoNifHashPorAnexo) {
                timerForHashCalculation.start();
                nif2 = String.valueOf(((AnexoG1Model)anexo).getQuadro03().getAnexoG1q03C02());
                hash3 = this.toSha1(anexoflatfile2);
                this.anexoG1NifHash = nif2 + hash3;
                this.nifHashPorAnexoImpressoId.put("G1", nif2 + hash3);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (!(listAnexos = this.declaracaoModel.getAllAnexosByType("AnexoI")).isEmpty()) {
            for (int k = 0; k < listAnexos.size(); ++k) {
                timerForFlatFileConversion.start();
                key = listAnexos.get(k);
                anexo = this.declaracaoModel.getAnexo(key);
                xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
                xmlWrite.loadDefinitionsFromFile();
                anexoflatfile3 = xmlWrite.getAnexoFlatFileVersion();
                anexosflatfile.append((CharSequence)"&AnexoI=").append((CharSequence)anexoflatfile3);
                this.validationResult.addAllFrom(xmlWrite.getValidationResult());
                this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
                if (this.jaCalculadoNifHashPorAnexo) continue;
                timerForHashCalculation.start();
                nif3 = String.valueOf(((AnexoIModel)anexo).getQuadro04().getAnexoIq04C05());
                hash = this.toSha1(anexoflatfile3);
                this.anexosINifHash.add(nif3 + hash);
                this.nifHashPorAnexoImpressoId.put("I0" + k, nif3 + hash);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (!(listAnexos = this.declaracaoModel.getAllAnexosByType("AnexoL")).isEmpty()) {
            for (int k = 0; k < listAnexos.size(); ++k) {
                timerForFlatFileConversion.start();
                key = listAnexos.get(k);
                anexo = this.declaracaoModel.getAnexo(key);
                xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
                xmlWrite.loadDefinitionsFromFile();
                anexoflatfile3 = xmlWrite.getAnexoFlatFileVersion();
                anexosflatfile.append((CharSequence)"&AnexoL=").append((CharSequence)anexoflatfile3);
                this.validationResult.addAllFrom(xmlWrite.getValidationResult());
                this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
                if (this.jaCalculadoNifHashPorAnexo) continue;
                timerForHashCalculation.start();
                nif3 = String.valueOf(((AnexoLModel)anexo).getQuadro03().getAnexoLq03C04());
                hash = this.toSha1(anexoflatfile3);
                this.anexosLNifHash.add(nif3 + hash);
                this.nifHashPorAnexoImpressoId.put("L0" + k, nif3 + hash);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (!(listAnexos = this.declaracaoModel.getAllAnexosByType("AnexoSS")).isEmpty()) {
            for (int k = 0; k < listAnexos.size(); ++k) {
                timerForFlatFileConversion.start();
                key = listAnexos.get(k);
                anexo = this.declaracaoModel.getAnexo(key);
                xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso);
                xmlWrite.loadDefinitionsFromFile();
                anexoflatfile3 = xmlWrite.getAnexoFlatFileVersion();
                this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
                if (this.jaCalculadoNifHashPorAnexo) continue;
                timerForHashCalculation.start();
                nif3 = String.valueOf(((AnexoSSModel)anexo).getQuadro03().getAnexoSSq03C06());
                hash = this.toSha1(anexoflatfile3);
                this.anexosSSNifHash.add(nif3 + hash);
                this.nifHashPorAnexoImpressoId.put("S0" + k, nif3 + hash);
                this.hashCalculationTimes+=timerForHashCalculation.ellapsed();
            }
        }
        if (Modelo3IRSv2015Parameters.instance().isDC()) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, "GFFModelo3", this.dataRecepcao, this.indicadorReembolso);
            xmlWrite.loadDefinitionsFromFile();
            anexosflatfile.append((CharSequence)"&GFFModelo3=").append((CharSequence)xmlWrite.getAnexoFlatFileVersion());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
        }
        if ((anexo = this.declaracaoModel.getAnexo(RostoModel.class)) != null) {
            timerForFlatFileConversion.start();
            xmlWrite = new AnexoXMLWrite(this.parserForAnexos, this.parserForTabelas, this.declaracaoModel, anexo, this.dataRecepcao, this.indicadorReembolso, !this.jaCalculadoNifHashPorAnexo ? this.appendAnexosNifHash() : this.nifHashPorAnexoStr);
            xmlWrite.loadDefinitionsFromFile();
            this.sflatfile.append((CharSequence)"Rosto=").append((CharSequence)xmlWrite.getAnexoFlatFileVersion()).append((CharSequence)anexosflatfile.getBuffer().toString());
            this.validationResult.addAllFrom(xmlWrite.getValidationResult());
            this.flatFileConversionTimes+=timerForFlatFileConversion.ellapsed();
        }
        return !this.validationResult.hasErrors();
    }

    private String appendAnexosNifHash() {
        this.nifHashPorAnexoSW.append((CharSequence)this.rostoNifHash);
        this.nifHashPorAnexoSW.append((CharSequence)this.anexoANifHash);
        for (String anexoBNifHash : this.anexosBNifHash) {
            this.nifHashPorAnexoSW.append((CharSequence)anexoBNifHash);
        }
        for (String anexoCNifHash : this.anexosCNifHash) {
            this.nifHashPorAnexoSW.append((CharSequence)anexoCNifHash);
        }
        for (String anexoDNifHash : this.anexosDNifHash) {
            this.nifHashPorAnexoSW.append((CharSequence)anexoDNifHash);
        }
        this.nifHashPorAnexoSW.append((CharSequence)this.anexoENifHash);
        this.nifHashPorAnexoSW.append((CharSequence)this.anexoFNifHash);
        this.nifHashPorAnexoSW.append((CharSequence)this.anexoGNifHash);
        this.nifHashPorAnexoSW.append((CharSequence)this.anexoG1NifHash);
        this.nifHashPorAnexoSW.append((CharSequence)this.anexoHNifHash);
        for (String anexoINifHash : this.anexosINifHash) {
            this.nifHashPorAnexoSW.append((CharSequence)anexoINifHash);
        }
        for (String anexoJNifHash : this.anexosJNifHash) {
            this.nifHashPorAnexoSW.append((CharSequence)anexoJNifHash);
        }
        for (String anexoLNifHash : this.anexosLNifHash) {
            this.nifHashPorAnexoSW.append((CharSequence)anexoLNifHash);
        }
        for (String anexoSSNifHash : this.anexosSSNifHash) {
            this.nifHashPorAnexoSW.append((CharSequence)anexoSSNifHash);
        }
        return this.nifHashPorAnexoSW.getBuffer().toString();
    }

    private void initSAXParsers() throws ParserConfigurationException, SAXException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        this.parserForAnexos = saxParserFactory.newSAXParser();
        this.parserForTabelas = saxParserFactory.newSAXParser();
    }

    private void initSAXParsers(SAXParser saxParserFromPool) throws ParserConfigurationException, SAXException {
        if (saxParserFromPool == null) {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            this.parserForAnexos = saxParserFactory.newSAXParser();
            this.parserForTabelas = saxParserFactory.newSAXParser();
        } else {
            this.parserForAnexos = saxParserFromPool;
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            this.parserForTabelas = saxParserFactory.newSAXParser();
        }
    }

    public String getFlatFile() {
        if (this.validationResult.hasErrors()) {
            return null;
        }
        return this.sflatfile.getBuffer().toString();
    }

    public ValidationResult getValidationResult() {
        return this.validationResult;
    }

    public Map<String, String> getNifHashPorAnexo() {
        return this.nifHashPorAnexoImpressoId;
    }

    public long getInitSaxParsersTimes() {
        return this.initSaxParsersTimes;
    }

    public long getHashCalculationTimes() {
        return this.hashCalculationTimes;
    }

    public long getFlatFileConversionTimes() {
        return this.flatFileConversionTimes;
    }

    private String toSha1(String anexo) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
        }
        catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException(nsae);
        }
        byte[] anexoAsByte = StringUtil.toByteArray(anexo);
        byte[] anexoDigested = md.digest(anexoAsByte);
        return StringUtil.bytes2HexString(anexoDigested);
    }
}

