/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro10PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro10BindingsBase {
    protected static BeanAdapter<Quadro10> beanModel = null;
    protected static Quadro10PanelExtension panelExtension = null;

    public static void doBindings(Quadro10 model, Quadro10PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro10>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoHq10C1001(), beanModel, "anexoHq10C1001", true);
        BindingsProxy.bind(panel.getAnexoHq10C1001a(), beanModel, "anexoHq10C1001a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1002(), beanModel, "anexoHq10C1002", true);
        BindingsProxy.bind(panel.getAnexoHq10C1002a(), beanModel, "anexoHq10C1002a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1003(), beanModel, "anexoHq10C1003", true);
        BindingsProxy.bind(panel.getAnexoHq10C1003a(), beanModel, "anexoHq10C1003a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1004(), beanModel, "anexoHq10C1004", true);
        BindingsProxy.bind(panel.getAnexoHq10C1004a(), beanModel, "anexoHq10C1004a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1005(), beanModel, "anexoHq10C1005", true);
        BindingsProxy.bind(panel.getAnexoHq10C1005a(), beanModel, "anexoHq10C1005a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1006(), beanModel, "anexoHq10C1006", true);
        BindingsProxy.bind(panel.getAnexoHq10C1006a(), beanModel, "anexoHq10C1006a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1007(), beanModel, "anexoHq10C1007", true);
        BindingsProxy.bind(panel.getAnexoHq10C1007a(), beanModel, "anexoHq10C1007a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1008(), beanModel, "anexoHq10C1008", true);
        BindingsProxy.bind(panel.getAnexoHq10C1008a(), beanModel, "anexoHq10C1008a", true);
        BindingsProxy.bind(panel.getAnexoHq10C1009(), beanModel, "anexoHq10C1009", true);
        BindingsProxy.bind(panel.getAnexoHq10C1(), beanModel, "anexoHq10C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1001", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1002", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1003", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1004", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1005", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1006", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1007", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1008", beanModel.getBean(), "anexoHq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1009", beanModel.getBean(), "anexoHq10C1Formula");
        BindingsProxy.bind(panel.getAnexoHq10C1a(), beanModel, "anexoHq10C1a", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1001a", beanModel.getBean(), "anexoHq10C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1002a", beanModel.getBean(), "anexoHq10C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1003a", beanModel.getBean(), "anexoHq10C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1004a", beanModel.getBean(), "anexoHq10C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1005a", beanModel.getBean(), "anexoHq10C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1006a", beanModel.getBean(), "anexoHq10C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1007a", beanModel.getBean(), "anexoHq10C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq10C1008a", beanModel.getBean(), "anexoHq10C1aFormula");
        Quadro10BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro10BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro10 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro10PanelExtension panel) {
        panel.getAnexoHq10C1001().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1001a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1002().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1002a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1003().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1003a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1004().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1004a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1005().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1005a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1006().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1006a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1007().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1007a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1008().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1008a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1009().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq10C1a().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro10PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1001(), "anexoHq10C1001");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1001a(), "anexoHq10C1001a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1002(), "anexoHq10C1002");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1002a(), "anexoHq10C1002a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1003(), "anexoHq10C1003");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1003a(), "anexoHq10C1003a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1004(), "anexoHq10C1004");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1004a(), "anexoHq10C1004a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1005(), "anexoHq10C1005");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1005a(), "anexoHq10C1005a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1006(), "anexoHq10C1006");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1006a(), "anexoHq10C1006a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1007(), "anexoHq10C1007");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1007a(), "anexoHq10C1007a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1008(), "anexoHq10C1008");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1008a(), "anexoHq10C1008a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1009(), "anexoHq10C1009");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1(), "anexoHq10C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq10C1a(), "anexoHq10C1a");
    }
}

