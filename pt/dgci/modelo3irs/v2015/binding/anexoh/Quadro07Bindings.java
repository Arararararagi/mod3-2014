/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro07BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComDepAfiAscSPCEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro07PanelExtension;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro07Bindings
extends Quadro07BindingsBase {
    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        Quadro07BindingsBase.doBindings(model, panel);
        Quadro07Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro07PanelExtension panel, Quadro07 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoHq07T7().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoHq07T7().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoHq07T7().getColumnModel().getColumn(2).setCellEditor(new TitularesComDepAfiAscSPCEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoHq07T7().getColumnModel().getColumn(4).setCellEditor(new NifCellEditor());
        panel.getAnexoHq07T7().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoHq07T7().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoHq07T7().getColumnModel().getColumn(6).setCellEditor(new LimitedTextCellEditor(13, 12, false));
    }
}

