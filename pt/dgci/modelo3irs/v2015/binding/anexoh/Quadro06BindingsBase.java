/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro06PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoHq06C601(), beanModel, "anexoHq06C601", true);
        BindingsProxy.bind(panel.getAnexoHq06C602(), beanModel, "anexoHq06C602", true);
        BindingsProxy.bind(panel.getAnexoHq06C603(), beanModel, "anexoHq06C603", true);
        BindingsProxy.bind(panel.getAnexoHq06C1(), beanModel, "anexoHq06C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq06C601", beanModel.getBean(), "anexoHq06C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq06C602", beanModel.getBean(), "anexoHq06C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoHq06C603", beanModel.getBean(), "anexoHq06C1Formula");
        ObservableElementList.Connector tableConnectorAnexoHq06T1_Linha = GlazedLists.beanConnector(AnexoHq06T1_Linha.class);
        ObservableElementList<AnexoHq06T1_Linha> tableElementListAnexoHq06T1_Linha = new ObservableElementList<AnexoHq06T1_Linha>(beanModel.getBean().getAnexoHq06T1(), tableConnectorAnexoHq06T1_Linha);
        panel.getAnexoHq06T1().setModel(new EventTableModel<AnexoHq06T1_Linha>(tableElementListAnexoHq06T1_Linha, new AnexoHq06T1_LinhaAdapterFormat()));
        panel.getAnexoHq06T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoHq06T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoHq06T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoHq06T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getAnexoHq06C601().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq06C602().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq06C603().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq06C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq06T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq06C601(), "anexoHq06C601");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq06C602(), "anexoHq06C602");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq06C603(), "anexoHq06C603");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq06C1(), "anexoHq06C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq06T1(), "anexoHq06T1");
    }
}

