/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro06PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoHq06T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoHq06T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoHq06T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getAnexoHq06T1().getColumnModel().getColumn(2).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoHq06T1().getColumnModel().getColumn(2).setCellRenderer(new JMoneyCellRenderer());
        Quadro02 quadro02Rosto = ((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).getRosto().getQuadro02();
        RostoQ02C02PropertyChangeListener rostoQ02C02PropertyChangeListener = new RostoQ02C02PropertyChangeListener(panel, model);
        quadro02Rosto.addPropertyChangeListener("q02C02", rostoQ02C02PropertyChangeListener);
        rostoQ02C02PropertyChangeListener.propertyChange(new PropertyChangeEvent(quadro02Rosto, "q02C02", quadro02Rosto.getQ02C02(), quadro02Rosto.getQ02C02()));
    }

    private static final class RostoQ02C02PropertyChangeListener
    implements PropertyChangeListener {
        private Quadro06PanelExtension panel;
        private Quadro06 model;

        private RostoQ02C02PropertyChangeListener(Quadro06PanelExtension panel, Quadro06 model) {
            this.panel = panel;
            this.model = model;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Long rostoQ02C02 = (Long)evt.getNewValue();
            if (rostoQ02C02 != null && rostoQ02C02 == 2001) {
                this.panel.getAnexoHq06C602().setEnabled(true);
                this.panel.getAnexoHq06C603().setEnabled(true);
            } else {
                this.model.setAnexoHq06C602(null);
                this.panel.getAnexoHq06C602().setEnabled(false);
                this.model.setAnexoHq06C603(null);
                this.panel.getAnexoHq06C603().setEnabled(false);
            }
        }
    }

}

