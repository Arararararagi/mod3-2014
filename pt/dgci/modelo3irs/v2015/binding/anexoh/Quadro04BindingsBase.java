/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoHq04T4_Linha = GlazedLists.beanConnector(AnexoHq04T4_Linha.class);
        ObservableElementList<AnexoHq04T4_Linha> tableElementListAnexoHq04T4_Linha = new ObservableElementList<AnexoHq04T4_Linha>(beanModel.getBean().getAnexoHq04T4(), tableConnectorAnexoHq04T4_Linha);
        panel.getAnexoHq04T4().setModel(new EventTableModel<AnexoHq04T4_Linha>(tableElementListAnexoHq04T4_Linha, new AnexoHq04T4_LinhaAdapterFormat()));
        panel.getAnexoHq04T4().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoHq04T4().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoHq04C1(), beanModel, "anexoHq04C1", true);
        beanModel.getBean().getAnexoHq04T4().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoHq04C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoHq04C2(), beanModel, "anexoHq04C2", true);
        beanModel.getBean().getAnexoHq04T4().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoHq04C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        panel.getAnexoHq04T4().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoHq04T4().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq04T4(), "Cat_M3V2015_AnexoHQ4", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq04T4(), "Cat_M3V2015_RostoTitularesComDepEFalecidos", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq04T4(), "Cat_M3V2015_Pais_AnxH_Q04", 6);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoHq04T4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq04C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq04C2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq04T4(), "anexoHq04T4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq04C1(), "anexoHq04C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq04C2(), "anexoHq04C2");
    }

}

