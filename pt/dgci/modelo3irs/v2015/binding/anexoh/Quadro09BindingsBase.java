/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro09PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro09BindingsBase {
    protected static BeanAdapter<Quadro09> beanModel = null;
    protected static Quadro09PanelExtension panelExtension = null;

    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro09>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getAnexoHq09B1OP1(), beanModel, "anexoHq09B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoHq09B1OP2(), beanModel, "anexoHq09B1", "2");
        BindingsProxy.bind(panel.getAnexoHq09C901(), beanModel, "anexoHq09C901", true);
        BindingsProxy.bind(panel.getAnexoHq09C901IRS(), beanModel, "anexoHq09C901IRS", true);
        BindingsProxy.bind(panel.getAnexoHq09C901IVA(), beanModel, "anexoHq09C901IVA", true);
        Quadro09BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro09BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro09 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro09PanelExtension panel) {
        panel.getAnexoHq09B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq09B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq09C901().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq09C901IRS().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq09C901IVA().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro09PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq09B1OP1(), "anexoHq09B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq09B1OP2(), "anexoHq09B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq09C901(), "anexoHq09C901");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq09C901IRS(), "anexoHq09C901IRS");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq09C901IVA(), "anexoHq09C901IVA");
    }
}

