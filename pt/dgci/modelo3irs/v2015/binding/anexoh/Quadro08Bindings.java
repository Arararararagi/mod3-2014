/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro08BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComDepAfiAscEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComDepSPCEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro08PanelExtension;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro08Bindings
extends Quadro08BindingsBase {
    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        Quadro08BindingsBase.doBindings(model, panel);
        Quadro08Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro08PanelExtension panel, Quadro08 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoHq08T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoHq08T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoHq08T1().getColumnModel().getColumn(1).setCellEditor(new TitularesComDepAfiAscEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoHq08T1().getColumnModel().getColumn(2).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoHq08T1().getColumnModel().getColumn(2).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoHq08T1().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoHq08T1().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoHq08T1().getColumnModel().getColumn(4).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoHq08T1().getColumnModel().getColumn(4).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoHq08T814().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoHq08T814().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoHq08T814().getColumnModel().getColumn(6).setCellEditor(new TitularesComDepSPCEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoHq08T814().getColumnModel().getColumn(8).setCellEditor(new NifCellEditor());
        panel.getAnexoHq08T814().getColumnModel().getColumn(2).setCellEditor(new LimitedTextCellEditor(8, 6, false));
        panel.getAnexoHq08T814().getColumnModel().getColumn(4).setCellEditor(new NumericCellEditor(5, 0));
        panel.getAnexoHq08T814().getColumnModel().getColumn(4).setCellRenderer(new NumericCellRenderer(5, 0));
        panel.getAnexoHq08T814().getColumnModel().getColumn(5).setCellEditor(new LimitedTextCellEditor(8, 7, false));
    }
}

