/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro08PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro08BindingsBase {
    protected static BeanAdapter<Quadro08> beanModel = null;
    protected static Quadro08PanelExtension panelExtension = null;

    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro08>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoHq08T1_Linha = GlazedLists.beanConnector(AnexoHq08T1_Linha.class);
        ObservableElementList<AnexoHq08T1_Linha> tableElementListAnexoHq08T1_Linha = new ObservableElementList<AnexoHq08T1_Linha>(beanModel.getBean().getAnexoHq08T1(), tableConnectorAnexoHq08T1_Linha);
        panel.getAnexoHq08T1().setModel(new EventTableModel<AnexoHq08T1_Linha>(tableElementListAnexoHq08T1_Linha, new AnexoHq08T1_LinhaAdapterFormat()));
        panel.getAnexoHq08T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoHq08T1().getTableHeader().setReorderingAllowed(false);
        ObservableElementList.Connector tableConnectorAnexoHq08T814_Linha = GlazedLists.beanConnector(AnexoHq08T814_Linha.class);
        ObservableElementList<AnexoHq08T814_Linha> tableElementListAnexoHq08T814_Linha = new ObservableElementList<AnexoHq08T814_Linha>(beanModel.getBean().getAnexoHq08T814(), tableConnectorAnexoHq08T814_Linha);
        panel.getAnexoHq08T814().setModel(new EventTableModel<AnexoHq08T814_Linha>(tableElementListAnexoHq08T814_Linha, new AnexoHq08T814_LinhaAdapterFormat()));
        panel.getAnexoHq08T814().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoHq08T814().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoHq08C814(), beanModel, "anexoHq08C814", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_Pais_AnxH"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_Pais_AnxH"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoHq08C81501(), beanModel, "anexoHq08C81501", true);
        BindingsProxy.bind(panel.getAnexoHq08C81502(), beanModel, "anexoHq08C81502", true);
        panel.getAnexoHq08T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoHq08T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq08T1(), "Cat_M3V2015_RostoTitularesComDepAfiAscEFalecidos", 1);
        panel.getAnexoHq08T814().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoHq08T814().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq08T814(), "Cat_M3V2015_BeneficiosAnexoHQ8", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq08T814(), "Cat_M3V2015_TipoPredio_UrbanoOmisso", 3);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq08T814(), "Cat_M3V2015_RostoTitularesComDepSPCEFalecidos", 6);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq08T814(), "Cat_M3V2015_PermanenteArrendada", 7);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq08T814(), "Cat_M3V2015_SimNao", 9);
        Quadro08BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro08BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro08 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro08PanelExtension panel) {
        panel.getAnexoHq08T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq08T814().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq08C814().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq08C81501().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq08C81502().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro08PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq08T1(), "anexoHq08T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq08T814(), "anexoHq08T814");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq08C814(), "anexoHq08C814");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq08C81501(), "anexoHq08C81501");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq08C81502(), "anexoHq08C81502");
    }
}

