/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoh.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComDepEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro05PanelExtension;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoHq05T5().getColumnModel().getColumn(1).setCellEditor(new TitularesComDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoHq05T5().getColumnModel().getColumn(2).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoHq05T5().getColumnModel().getColumn(2).setCellRenderer(new JMoneyCellRenderer());
    }
}

