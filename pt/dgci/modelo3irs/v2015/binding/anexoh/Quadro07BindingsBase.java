/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoh;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoh.Quadro07PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoHq07T7_Linha = GlazedLists.beanConnector(AnexoHq07T7_Linha.class);
        ObservableElementList<AnexoHq07T7_Linha> tableElementListAnexoHq07T7_Linha = new ObservableElementList<AnexoHq07T7_Linha>(beanModel.getBean().getAnexoHq07T7(), tableConnectorAnexoHq07T7_Linha);
        panel.getAnexoHq07T7().setModel(new EventTableModel<AnexoHq07T7_Linha>(tableElementListAnexoHq07T7_Linha, new AnexoHq07T7_LinhaAdapterFormat()));
        panel.getAnexoHq07T7().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoHq07T7().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoHq07C1(), beanModel, "anexoHq07C1", true);
        beanModel.getBean().getAnexoHq07T7().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro07BindingsBase.beanModel.getBean().setAnexoHq07C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        panel.getAnexoHq07T7().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoHq07T7().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq07T7(), "Cat_M3V2015_Beneficios", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq07T7(), "Cat_M3V2015_RostoTitularesComDepAfiAscSPCEFalecidos", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoHq07T7(), "Cat_M3V2015_Pais_AnxH_Q04", 5);
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getAnexoHq07T7().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoHq07C1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq07T7(), "anexoHq07T7");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoHq07C1(), "anexoHq07C1");
    }

}

