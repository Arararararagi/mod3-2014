/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro07PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoDq07C701(), beanModel, "anexoDq07C701", true);
        BindingsProxy.bind(panel.getAnexoDq07C702(), beanModel, "anexoDq07C702", true);
        BindingsProxy.bind(panel.getAnexoDq07C703(), beanModel, "anexoDq07C703", true);
        BindingsProxy.bind(panel.getAnexoDq07C704(), beanModel, "anexoDq07C704", true);
        BindingsProxy.bind(panel.getAnexoDq07C705(), beanModel, "anexoDq07C705", true);
        BindingsProxy.bind(panel.getAnexoDq07C706(), beanModel, "anexoDq07C706", true);
        BindingsProxy.bind(panel.getAnexoDq07C707(), beanModel, "anexoDq07C707", true);
        BindingsProxy.bind(panel.getAnexoDq07C708(), beanModel, "anexoDq07C708", true);
        BindingsProxy.bind(panel.getAnexoDq07C709(), beanModel, "anexoDq07C709", true);
        BindingsProxy.bind(panel.getAnexoDq07C710(), beanModel, "anexoDq07C710", true);
        BindingsProxy.bind(panel.getAnexoDq07C711(), beanModel, "anexoDq07C711", true);
        BindingsProxy.bind(panel.getAnexoDq07C712(), beanModel, "anexoDq07C712", true);
        BindingsProxy.bind(panel.getAnexoDq07C713(), beanModel, "anexoDq07C713", true);
        BindingsProxy.bind(panel.getAnexoDq07C714(), beanModel, "anexoDq07C714", true);
        BindingsProxy.bind(panel.getAnexoDq07C715(), beanModel, "anexoDq07C715", true);
        BindingsProxy.bind(panel.getAnexoDq07C716(), beanModel, "anexoDq07C716", true);
        BindingsProxy.bind(panel.getAnexoDq07C717(), beanModel, "anexoDq07C717", true);
        BindingsProxy.bind(panel.getAnexoDq07C718(), beanModel, "anexoDq07C718", true);
        BindingsProxy.bind(panel.getAnexoDq07C719(), beanModel, "anexoDq07C719", true);
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getAnexoDq07C701().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C702().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C703().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C704().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C705().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C706().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C707().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C708().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C709().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C710().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C711().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C712().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C713().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C714().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C715().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C716().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C717().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C718().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq07C719().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C701(), "anexoDq07C701");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C702(), "anexoDq07C702");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C703(), "anexoDq07C703");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C704(), "anexoDq07C704");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C705(), "anexoDq07C705");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C706(), "anexoDq07C706");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C707(), "anexoDq07C707");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C708(), "anexoDq07C708");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C709(), "anexoDq07C709");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C710(), "anexoDq07C710");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C711(), "anexoDq07C711");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C712(), "anexoDq07C712");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C713(), "anexoDq07C713");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C714(), "anexoDq07C714");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C715(), "anexoDq07C715");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C716(), "anexoDq07C716");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C717(), "anexoDq07C717");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C718(), "anexoDq07C718");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq07C719(), "anexoDq07C719");
    }
}

