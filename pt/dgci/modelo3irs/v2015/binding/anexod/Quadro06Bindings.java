/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import pt.dgci.modelo3irs.v2015.binding.anexod.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro06PanelExtension;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
    }
}

