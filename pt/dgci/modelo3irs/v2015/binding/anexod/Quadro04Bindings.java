/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexod.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro04PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.JPercentCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.JPercentCellRenderer;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoDq04T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoDq04T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoDq04T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getAnexoDq04T1().getColumnModel().getColumn(2).setCellEditor(new JPercentCellEditor(2));
        panel.getAnexoDq04T1().getColumnModel().getColumn(2).setCellRenderer(new JPercentCellRenderer(2));
        JMoneyCellEditor moneyCellEditor = new JMoneyCellEditor();
        moneyCellEditor.allowNegative(true);
        ((JMoneyTextField)moneyCellEditor.getComponent()).setColumns(12);
        panel.getAnexoDq04T1().getColumnModel().getColumn(3).setCellEditor(moneyCellEditor);
        JMoneyCellRenderer moneyCellRenderer = new JMoneyCellRenderer();
        moneyCellRenderer.allowNegative(true);
        moneyCellRenderer.setColumns(12);
        panel.getAnexoDq04T1().getColumnModel().getColumn(3).setCellRenderer(moneyCellRenderer);
        panel.getAnexoDq04T1().getColumnModel().getColumn(4).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoDq04T1().getColumnModel().getColumn(4).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoDq04T2().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoDq04T2().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoDq04T2().getColumnModel().getColumn(4).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoDq04T2().getColumnModel().getColumn(4).setCellRenderer(new JMoneyCellRenderer());
    }
}

