/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro05PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoDq05C501(), beanModel, "anexoDq05C501", true);
        BindingsProxy.bind(panel.getAnexoDq05C502(), beanModel, "anexoDq05C502", true);
        BindingsProxy.bind(panel.getAnexoDq05C503(), beanModel, "anexoDq05C503", true);
        BindingsProxy.bind(panel.getAnexoDq05C504(), beanModel, "anexoDq05C504", true);
        BindingsProxy.bind(panel.getAnexoDq05C505(), beanModel, "anexoDq05C505", true);
        BindingsProxy.bind(panel.getAnexoDq05C506(), beanModel, "anexoDq05C506", true);
        BindingsProxy.bind(panel.getAnexoDq05C507(), beanModel, "anexoDq05C507", true);
        BindingsProxy.bind(panel.getAnexoDq05C508(), beanModel, "anexoDq05C508", true);
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoDq05C501().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq05C502().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq05C503().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq05C504().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq05C505().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq05C506().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq05C507().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq05C508().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C501(), "anexoDq05C501");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C502(), "anexoDq05C502");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C503(), "anexoDq05C503");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C504(), "anexoDq05C504");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C505(), "anexoDq05C505");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C506(), "anexoDq05C506");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C507(), "anexoDq05C507");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq05C508(), "anexoDq05C508");
    }
}

