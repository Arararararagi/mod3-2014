/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoDq04C401(), beanModel, "anexoDq04C401", true);
        BindingsProxy.bind(panel.getAnexoDq04C402(), beanModel, "anexoDq04C402", true);
        BindingsProxy.bind(panel.getAnexoDq04C403(), beanModel, "anexoDq04C403", true);
        BindingsProxy.bind(panel.getAnexoDq04C431(), beanModel, "anexoDq04C431", true);
        BindingsProxy.bind(panel.getAnexoDq04C432(), beanModel, "anexoDq04C432", true);
        BindingsProxy.bind(panel.getAnexoDq04C401a(), beanModel, "anexoDq04C401a", true);
        BindingsProxy.bind(panel.getAnexoDq04C402a(), beanModel, "anexoDq04C402a", true);
        BindingsProxy.bind(panel.getAnexoDq04C403a(), beanModel, "anexoDq04C403a", true);
        BindingsProxy.bind(panel.getAnexoDq04C431a(), beanModel, "anexoDq04C431a", true);
        BindingsProxy.bind(panel.getAnexoDq04C432a(), beanModel, "anexoDq04C432a", true);
        BindingsProxy.bind(panel.getAnexoDq04C480a(), beanModel, "anexoDq04C480a", true);
        BindingsProxy.bind(panel.getAnexoDq04C481a(), beanModel, "anexoDq04C481a", true);
        BindingsProxy.bind(panel.getAnexoDq04C401b(), beanModel, "anexoDq04C401b", true);
        BindingsProxy.bind(panel.getAnexoDq04C402b(), beanModel, "anexoDq04C402b", true);
        BindingsProxy.bind(panel.getAnexoDq04C403b(), beanModel, "anexoDq04C403b", true);
        BindingsProxy.bind(panel.getAnexoDq04C431b(), beanModel, "anexoDq04C431b", true);
        BindingsProxy.bind(panel.getAnexoDq04C432b(), beanModel, "anexoDq04C432b", true);
        BindingsProxy.bind(panel.getAnexoDq04C480b(), beanModel, "anexoDq04C480b", true);
        BindingsProxy.bind(panel.getAnexoDq04C481b(), beanModel, "anexoDq04C481b", true);
        BindingsProxy.bind(panel.getAnexoDq04C401c(), beanModel, "anexoDq04C401c", true);
        BindingsProxy.bind(panel.getAnexoDq04C402c(), beanModel, "anexoDq04C402c", true);
        BindingsProxy.bind(panel.getAnexoDq04C403c(), beanModel, "anexoDq04C403c", true);
        BindingsProxy.bind(panel.getAnexoDq04C431c(), beanModel, "anexoDq04C431c", true);
        BindingsProxy.bind(panel.getAnexoDq04C432c(), beanModel, "anexoDq04C432c", true);
        BindingsProxy.bind(panel.getAnexoDq04C401d(), beanModel, "anexoDq04C401d", true);
        BindingsProxy.bind(panel.getAnexoDq04C402d(), beanModel, "anexoDq04C402d", true);
        BindingsProxy.bind(panel.getAnexoDq04C403d(), beanModel, "anexoDq04C403d", true);
        BindingsProxy.bind(panel.getAnexoDq04C431d(), beanModel, "anexoDq04C431d", true);
        BindingsProxy.bind(panel.getAnexoDq04C432d(), beanModel, "anexoDq04C432d", true);
        BindingsProxy.bind(panel.getAnexoDq04C3(), beanModel, "anexoDq04C3", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C401d", beanModel.getBean(), "anexoDq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C402d", beanModel.getBean(), "anexoDq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C403d", beanModel.getBean(), "anexoDq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C431d", beanModel.getBean(), "anexoDq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C432d", beanModel.getBean(), "anexoDq04C3Formula");
        BindingsProxy.bind(panel.getAnexoDq04C401e(), beanModel, "anexoDq04C401e", true);
        BindingsProxy.bind(panel.getAnexoDq04C402e(), beanModel, "anexoDq04C402e", true);
        BindingsProxy.bind(panel.getAnexoDq04C403e(), beanModel, "anexoDq04C403e", true);
        BindingsProxy.bind(panel.getAnexoDq04C431e(), beanModel, "anexoDq04C431e", true);
        BindingsProxy.bind(panel.getAnexoDq04C432e(), beanModel, "anexoDq04C432e", true);
        BindingsProxy.bind(panel.getAnexoDq04C4(), beanModel, "anexoDq04C4", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C401e", beanModel.getBean(), "anexoDq04C4Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C402e", beanModel.getBean(), "anexoDq04C4Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C403e", beanModel.getBean(), "anexoDq04C4Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C431e", beanModel.getBean(), "anexoDq04C4Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C432e", beanModel.getBean(), "anexoDq04C4Formula");
        ObservableElementList.Connector tableConnectorAnexoDq04T1_Linha = GlazedLists.beanConnector(AnexoDq04T1_Linha.class);
        ObservableElementList<AnexoDq04T1_Linha> tableElementListAnexoDq04T1_Linha = new ObservableElementList<AnexoDq04T1_Linha>(beanModel.getBean().getAnexoDq04T1(), tableConnectorAnexoDq04T1_Linha);
        panel.getAnexoDq04T1().setModel(new EventTableModel<AnexoDq04T1_Linha>(tableElementListAnexoDq04T1_Linha, new AnexoDq04T1_LinhaAdapterFormat()));
        panel.getAnexoDq04T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoDq04T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoDq04C1(), beanModel, "anexoDq04C1", true);
        beanModel.getBean().getAnexoDq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoDq04C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C401b", beanModel.getBean(), "anexoDq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C402b", beanModel.getBean(), "anexoDq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C403b", beanModel.getBean(), "anexoDq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C431b", beanModel.getBean(), "anexoDq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C432b", beanModel.getBean(), "anexoDq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C480b", beanModel.getBean(), "anexoDq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C481b", beanModel.getBean(), "anexoDq04C1Formula");
        BindingsProxy.bind(panel.getAnexoDq04C2(), beanModel, "anexoDq04C2", true);
        beanModel.getBean().getAnexoDq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoDq04C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C401c", beanModel.getBean(), "anexoDq04C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C402c", beanModel.getBean(), "anexoDq04C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C403c", beanModel.getBean(), "anexoDq04C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C431c", beanModel.getBean(), "anexoDq04C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoDq04C432c", beanModel.getBean(), "anexoDq04C2Formula");
        ObservableElementList.Connector tableConnectorAnexoDq04T2_Linha = GlazedLists.beanConnector(AnexoDq04T2_Linha.class);
        ObservableElementList<AnexoDq04T2_Linha> tableElementListAnexoDq04T2_Linha = new ObservableElementList<AnexoDq04T2_Linha>(beanModel.getBean().getAnexoDq04T2(), tableConnectorAnexoDq04T2_Linha);
        panel.getAnexoDq04T2().setModel(new EventTableModel<AnexoDq04T2_Linha>(tableElementListAnexoDq04T2_Linha, new AnexoDq04T2_LinhaAdapterFormat()));
        panel.getAnexoDq04T2().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoDq04T2().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoDq04T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoDq04T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        panel.getAnexoDq04T2().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoDq04T2().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoDq04T2(), "Cat_M3V2015_AnexoDQ4", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoDq04T2(), "Cat_M3V2015_Pais_AnxD", 2);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoDq04C401().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C402().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C403().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C431().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C432().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C401a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C402a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C403a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C431a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C432a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C480a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C481a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C401b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C402b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C403b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C431b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C432b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C480b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C481b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C401c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C402c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C403c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C431c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C432c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C401d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C402d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C403d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C431d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C432d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C401e().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C402e().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C403e().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C431e().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C432e().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq04T2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C401(), "anexoDq04C401");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C402(), "anexoDq04C402");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C403(), "anexoDq04C403");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C431(), "anexoDq04C431");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C432(), "anexoDq04C432");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C401a(), "anexoDq04C401a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C402a(), "anexoDq04C402a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C403a(), "anexoDq04C403a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C431a(), "anexoDq04C431a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C432a(), "anexoDq04C432a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C480a(), "anexoDq04C480a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C481a(), "anexoDq04C481a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C401b(), "anexoDq04C401b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C402b(), "anexoDq04C402b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C403b(), "anexoDq04C403b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C431b(), "anexoDq04C431b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C432b(), "anexoDq04C432b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C480b(), "anexoDq04C480b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C481b(), "anexoDq04C481b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C401c(), "anexoDq04C401c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C402c(), "anexoDq04C402c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C403c(), "anexoDq04C403c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C431c(), "anexoDq04C431c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C432c(), "anexoDq04C432c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C401d(), "anexoDq04C401d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C402d(), "anexoDq04C402d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C403d(), "anexoDq04C403d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C431d(), "anexoDq04C431d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C432d(), "anexoDq04C432d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C3(), "anexoDq04C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C401e(), "anexoDq04C401e");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C402e(), "anexoDq04C402e");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C403e(), "anexoDq04C403e");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C431e(), "anexoDq04C431e");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C432e(), "anexoDq04C432e");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C4(), "anexoDq04C4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04T1(), "anexoDq04T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C1(), "anexoDq04C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04C2(), "anexoDq04C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq04T2(), "anexoDq04T2");
    }

}

