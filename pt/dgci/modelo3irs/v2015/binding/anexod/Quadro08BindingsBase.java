/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro08PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro08BindingsBase {
    protected static BeanAdapter<Quadro08> beanModel = null;
    protected static Quadro08PanelExtension panelExtension = null;

    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro08>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoDq08C801(), beanModel, "anexoDq08C801", true);
        Quadro08BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro08BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro08 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro08PanelExtension panel) {
        panel.getAnexoDq08C801().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro08PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq08C801(), "anexoDq08C801");
    }
}

