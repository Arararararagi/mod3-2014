/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro06PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoDq06C601(), beanModel, "anexoDq06C601", true);
        BindingsProxy.bind(panel.getAnexoDq06C602(), beanModel, "anexoDq06C602", true);
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getAnexoDq06C601().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq06C602().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq06C601(), "anexoDq06C601");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq06C602(), "anexoDq06C602");
    }
}

