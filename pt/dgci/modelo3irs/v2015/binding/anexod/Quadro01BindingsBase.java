/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexod;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.anexod.Quadro01PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro01BindingsBase {
    protected static BeanAdapter<Quadro01> beanModel = null;
    protected static Quadro01PanelExtension panelExtension = null;

    public static void doBindings(Quadro01 model, Quadro01PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro01>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoDq01B1(), beanModel, "anexoDq01B1", true);
        BindingsProxy.bind(panel.getAnexoDq01B2(), beanModel, "anexoDq01B2", true);
        Quadro01BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro01BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro01 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro01PanelExtension panel) {
        panel.getAnexoDq01B1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoDq01B2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro01PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq01B1(), "anexoDq01B1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoDq01B2(), "anexoDq01B2");
    }
}

