/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.list.SelectionInList;
import java.beans.PropertyChangeListener;
import javax.swing.JComboBox;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro07BindingsBase;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosPropertyChangeListener;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro07PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro07Bindings
extends Quadro07BindingsBase {
    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        Quadro07BindingsBase.doBindings(model, panel);
        Quadro07Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro07PanelExtension panel, Quadro07 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        SelectionInList<ICatalogItem> selectionInListC701b = BindingsProxy.bindAndReturn(panel.getAnexoGq07C701b(), beanModel, "anexoGq07C701b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC702b = BindingsProxy.bindAndReturn(panel.getAnexoGq07C702b(), beanModel, "anexoGq07C702b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq07C701b(), selectionInListC701b));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq07C702b(), selectionInListC702b));
    }
}

