/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.list.SelectionInList;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro10BindingsBase;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosPropertyChangeListener;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro10;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro10PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro10Bindings
extends Quadro10BindingsBase {
    public static void doBindings(Quadro10 model, Quadro10PanelExtension panel) {
        Quadro10BindingsBase.doBindings(model, panel);
        Quadro10Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro10PanelExtension panel, Quadro10 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        SelectionInList<ICatalogItem> selectionInListC1001a = BindingsProxy.bindAndReturn(panel.getAnexoGq10C1001a(), beanModel, "anexoGq10C1001a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC1002a = BindingsProxy.bindAndReturn(panel.getAnexoGq10C1002a(), beanModel, "anexoGq10C1002a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq10C1001a(), selectionInListC1001a));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq10C1002a(), selectionInListC1002a));
        panel.getAnexoGq10T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoGq10T1().getColumnModel().getColumn(2).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoGq10T1().getColumnModel().getColumn(2).setCellRenderer(jMoneyCellRenderer);
    }
}

