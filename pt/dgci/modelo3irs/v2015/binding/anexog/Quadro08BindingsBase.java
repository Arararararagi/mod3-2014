/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro08PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro08BindingsBase {
    protected static BeanAdapter<Quadro08> beanModel = null;
    protected static Quadro08PanelExtension panelExtension = null;

    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro08>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoGq08T1_Linha = GlazedLists.beanConnector(AnexoGq08T1_Linha.class);
        ObservableElementList<AnexoGq08T1_Linha> tableElementListAnexoGq08T1_Linha = new ObservableElementList<AnexoGq08T1_Linha>(beanModel.getBean().getAnexoGq08T1(), tableConnectorAnexoGq08T1_Linha);
        panel.getAnexoGq08T1().setModel(new EventTableModel<AnexoGq08T1_Linha>(tableElementListAnexoGq08T1_Linha, new AnexoGq08T1_LinhaAdapterFormat()));
        panel.getAnexoGq08T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoGq08T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoGq08C1(), beanModel, "anexoGq08C1", true);
        beanModel.getBean().getAnexoGq08T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro08BindingsBase.beanModel.getBean().setAnexoGq08C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoGq08C2(), beanModel, "anexoGq08C2", true);
        beanModel.getBean().getAnexoGq08T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro08BindingsBase.beanModel.getBean().setAnexoGq08C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoGq08C3(), beanModel, "anexoGq08C3", true);
        beanModel.getBean().getAnexoGq08T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro08BindingsBase.beanModel.getBean().setAnexoGq08C3Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        ObservableElementList.Connector tableConnectorAnexoGq08T2_Linha = GlazedLists.beanConnector(AnexoGq08T2_Linha.class);
        ObservableElementList<AnexoGq08T2_Linha> tableElementListAnexoGq08T2_Linha = new ObservableElementList<AnexoGq08T2_Linha>(beanModel.getBean().getAnexoGq08T2(), tableConnectorAnexoGq08T2_Linha);
        panel.getAnexoGq08T2().setModel(new EventTableModel<AnexoGq08T2_Linha>(tableElementListAnexoGq08T2_Linha, new AnexoGq08T2_LinhaAdapterFormat()));
        panel.getAnexoGq08T2().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoGq08T2().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoGq08T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoGq08T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq08T1(), "Cat_M3V2015_AnexoGQ8", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq08T1(), "Cat_M3V2015_RostoTitularesComCDepEFalecidos", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq08T1(), "Cat_M3V2015_AnexoGQ8CodEncargos", 4);
        panel.getAnexoGq08T2().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoGq08T2().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq08T2(), "Cat_M3V2015_AnexoGQ8", 1);
        Quadro08BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro08BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro08 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro08PanelExtension panel) {
        panel.getAnexoGq08T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq08C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq08C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq08C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq08T2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro08PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq08T1(), "anexoGq08T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq08C1(), "anexoGq08C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq08C2(), "anexoGq08C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq08C3(), "anexoGq08C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq08T2(), "anexoGq08T2");
    }

}

