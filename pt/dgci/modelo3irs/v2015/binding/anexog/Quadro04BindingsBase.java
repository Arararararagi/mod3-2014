/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoGq04T1_Linha = GlazedLists.beanConnector(AnexoGq04T1_Linha.class);
        ObservableElementList<AnexoGq04T1_Linha> tableElementListAnexoGq04T1_Linha = new ObservableElementList<AnexoGq04T1_Linha>(beanModel.getBean().getAnexoGq04T1(), tableConnectorAnexoGq04T1_Linha);
        panel.getAnexoGq04T1().setModel(new EventTableModel<AnexoGq04T1_Linha>(tableElementListAnexoGq04T1_Linha, new AnexoGq04T1_LinhaAdapterFormat()));
        panel.getAnexoGq04T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoGq04T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoGq04C1(), beanModel, "anexoGq04C1", true);
        beanModel.getBean().getAnexoGq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoGq04C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoGq04C2(), beanModel, "anexoGq04C2", true);
        beanModel.getBean().getAnexoGq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoGq04C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoGq04C3(), beanModel, "anexoGq04C3", true);
        beanModel.getBean().getAnexoGq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoGq04C3Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        ObservableElementList.Connector tableConnectorAnexoGq04AT1_Linha = GlazedLists.beanConnector(AnexoGq04AT1_Linha.class);
        ObservableElementList<AnexoGq04AT1_Linha> tableElementListAnexoGq04AT1_Linha = new ObservableElementList<AnexoGq04AT1_Linha>(beanModel.getBean().getAnexoGq04AT1(), tableConnectorAnexoGq04AT1_Linha);
        panel.getAnexoGq04AT1().setModel(new EventTableModel<AnexoGq04AT1_Linha>(tableElementListAnexoGq04AT1_Linha, new AnexoGq04AT1_LinhaAdapterFormat()));
        panel.getAnexoGq04AT1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoGq04AT1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bindChoice(panel.getAnexoGq04B1OPSim(), beanModel, "anexoGq04B1OP", "S");
        BindingsProxy.bindChoice(panel.getAnexoGq04B1OPNao(), beanModel, "anexoGq04B1OP", "N");
        ObservableElementList.Connector tableConnectorAnexoGq04T2_Linha = GlazedLists.beanConnector(AnexoGq04T2_Linha.class);
        ObservableElementList<AnexoGq04T2_Linha> tableElementListAnexoGq04T2_Linha = new ObservableElementList<AnexoGq04T2_Linha>(beanModel.getBean().getAnexoGq04T2(), tableConnectorAnexoGq04T2_Linha);
        panel.getAnexoGq04T2().setModel(new EventTableModel<AnexoGq04T2_Linha>(tableElementListAnexoGq04T2_Linha, new AnexoGq04T2_LinhaAdapterFormat()));
        panel.getAnexoGq04T2().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoGq04T2().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoGq04T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoGq04T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq04T1(), "Cat_M3V2015_AnexoGQ4", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq04T1(), "Cat_M3V2015_RostoTitularesComCDepEFalecidos", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq04T1(), "Cat_M3V2015_TipoPredio", 11);
        panel.getAnexoGq04AT1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoGq04AT1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq04AT1(), "Cat_M3V2015_AnexoGQ4", 1);
        panel.getAnexoGq04T2().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoGq04T2().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq04T2(), "Cat_M3V2015_RostoTitularesComCDepEFalecidos", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq04T2(), "Cat_M3V2015_AnexoGQ4BensMoveisImoveis", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoGq04T2(), "Cat_M3V2015_TipoPredio", 10);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoGq04T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04AT1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04B1OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04B1OPNao().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04T2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04T1(), "anexoGq04T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04C1(), "anexoGq04C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04C2(), "anexoGq04C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04C3(), "anexoGq04C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04AT1(), "anexoGq04AT1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04B1OPSim(), "anexoGq04B1OPSim");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04B1OPNao(), "anexoGq04B1OPNao");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq04T2(), "anexoGq04T2");
    }

}

