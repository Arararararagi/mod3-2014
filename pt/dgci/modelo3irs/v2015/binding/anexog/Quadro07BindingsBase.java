/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro07PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoGq07C701b(), beanModel, "anexoGq07C701b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq07C701c(), beanModel, "anexoGq07C701c", true);
        BindingsProxy.bind(panel.getAnexoGq07C701d(), beanModel, "anexoGq07C701d", true);
        BindingsProxy.bind(panel.getAnexoGq07C702b(), beanModel, "anexoGq07C702b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq07C702c(), beanModel, "anexoGq07C702c", true);
        BindingsProxy.bind(panel.getAnexoGq07C702d(), beanModel, "anexoGq07C702d", true);
        BindingsProxy.bind(panel.getAnexoGq07C1c(), beanModel, "anexoGq07C1c", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq07C701c", beanModel.getBean(), "anexoGq07C1cFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq07C702c", beanModel.getBean(), "anexoGq07C1cFormula");
        BindingsProxy.bind(panel.getAnexoGq07C1d(), beanModel, "anexoGq07C1d", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq07C701d", beanModel.getBean(), "anexoGq07C1dFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq07C702d", beanModel.getBean(), "anexoGq07C1dFormula");
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getAnexoGq07C701b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq07C701c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq07C701d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq07C702b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq07C702c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq07C702d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq07C1c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq07C1d().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C701b(), "anexoGq07C701b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C701c(), "anexoGq07C701c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C701d(), "anexoGq07C701d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C702b(), "anexoGq07C702b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C702c(), "anexoGq07C702c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C702d(), "anexoGq07C702d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C1c(), "anexoGq07C1c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq07C1d(), "anexoGq07C1d");
    }
}

