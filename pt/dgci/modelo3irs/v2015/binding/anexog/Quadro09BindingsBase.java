/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro09PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro09BindingsBase {
    protected static BeanAdapter<Quadro09> beanModel = null;
    protected static Quadro09PanelExtension panelExtension = null;

    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro09>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoGq09C901a(), beanModel, "anexoGq09C901a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq09C901b(), beanModel, "anexoGq09C901b", true);
        BindingsProxy.bind(panel.getAnexoGq09C902a(), beanModel, "anexoGq09C902a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq09C902b(), beanModel, "anexoGq09C902b", true);
        BindingsProxy.bind(panel.getAnexoGq09C903a(), beanModel, "anexoGq09C903a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq09C903b(), beanModel, "anexoGq09C903b", true);
        BindingsProxy.bind(panel.getAnexoGq09C904a(), beanModel, "anexoGq09C904a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq09C904b(), beanModel, "anexoGq09C904b", true);
        BindingsProxy.bind(panel.getAnexoGq09C905a(), beanModel, "anexoGq09C905a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq09C905b(), beanModel, "anexoGq09C905b", true);
        BindingsProxy.bind(panel.getAnexoGq09C1b(), beanModel, "anexoGq09C1b", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq09C901b", beanModel.getBean(), "anexoGq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq09C902b", beanModel.getBean(), "anexoGq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq09C903b", beanModel.getBean(), "anexoGq09C1bFormula");
        BindingsProxy.bindChoice(panel.getAnexoGq09B1OP1(), beanModel, "anexoGq09B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoGq09B1OP2(), beanModel, "anexoGq09B1", "2");
        Quadro09BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro09BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro09 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro09PanelExtension panel) {
        panel.getAnexoGq09C901a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C901b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C902a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C902b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C903a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C903b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C904a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C904b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C905a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C905b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09C1b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09B1OP2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro09PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C901a(), "anexoGq09C901a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C901b(), "anexoGq09C901b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C902a(), "anexoGq09C902a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C902b(), "anexoGq09C902b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C903a(), "anexoGq09C903a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C903b(), "anexoGq09C903b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C904a(), "anexoGq09C904a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C904b(), "anexoGq09C904b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C905a(), "anexoGq09C905a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C905b(), "anexoGq09C905b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09C1b(), "anexoGq09C1b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09B1OP1(), "anexoGq09B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq09B1OP2(), "anexoGq09B1OP2");
    }
}

