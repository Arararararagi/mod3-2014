/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro10PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro10BindingsBase {
    protected static BeanAdapter<Quadro10> beanModel = null;
    protected static Quadro10PanelExtension panelExtension = null;

    public static void doBindings(Quadro10 model, Quadro10PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro10>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoGq10C1001a(), beanModel, "anexoGq10C1001a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq10C1001b(), beanModel, "anexoGq10C1001b", true);
        BindingsProxy.bind(panel.getAnexoGq10C1001c(), beanModel, "anexoGq10C1001c", true);
        BindingsProxy.bind(panel.getAnexoGq10C1002a(), beanModel, "anexoGq10C1002a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq10C1002b(), beanModel, "anexoGq10C1002b", true);
        BindingsProxy.bind(panel.getAnexoGq10C1002c(), beanModel, "anexoGq10C1002c", true);
        BindingsProxy.bind(panel.getAnexoGq10C1b(), beanModel, "anexoGq10C1b", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq10C1001b", beanModel.getBean(), "anexoGq10C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq10C1002b", beanModel.getBean(), "anexoGq10C1bFormula");
        BindingsProxy.bind(panel.getAnexoGq10C1c(), beanModel, "anexoGq10C1c", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq10C1001c", beanModel.getBean(), "anexoGq10C1cFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq10C1002c", beanModel.getBean(), "anexoGq10C1cFormula");
        ObservableElementList.Connector tableConnectorAnexoGq10T1_Linha = GlazedLists.beanConnector(AnexoGq10T1_Linha.class);
        ObservableElementList<AnexoGq10T1_Linha> tableElementListAnexoGq10T1_Linha = new ObservableElementList<AnexoGq10T1_Linha>(beanModel.getBean().getAnexoGq10T1(), tableConnectorAnexoGq10T1_Linha);
        panel.getAnexoGq10T1().setModel(new EventTableModel<AnexoGq10T1_Linha>(tableElementListAnexoGq10T1_Linha, new AnexoGq10T1_LinhaAdapterFormat()));
        panel.getAnexoGq10T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoGq10T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoGq10T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoGq10T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro10BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro10BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro10 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro10PanelExtension panel) {
        panel.getAnexoGq10C1001a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10C1001b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10C1001c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10C1002a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10C1002b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10C1002c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10C1b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10C1c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq10T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro10PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1001a(), "anexoGq10C1001a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1001b(), "anexoGq10C1001b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1001c(), "anexoGq10C1001c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1002a(), "anexoGq10C1002a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1002b(), "anexoGq10C1002b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1002c(), "anexoGq10C1002c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1b(), "anexoGq10C1b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10C1c(), "anexoGq10C1c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq10T1(), "anexoGq10T1");
    }
}

