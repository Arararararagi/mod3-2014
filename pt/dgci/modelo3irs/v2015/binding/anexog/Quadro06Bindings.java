/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.list.SelectionInList;
import java.beans.PropertyChangeListener;
import javax.swing.JComboBox;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosPropertyChangeListener;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro06PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        SelectionInList<ICatalogItem> selectionInListC601b = BindingsProxy.bindAndReturn(panel.getAnexoGq06C601b(), beanModel, "anexoGq06C601b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC602b = BindingsProxy.bindAndReturn(panel.getAnexoGq06C602b(), beanModel, "anexoGq06C602b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq06C601b(), selectionInListC601b));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq06C602b(), selectionInListC602b));
    }
}

