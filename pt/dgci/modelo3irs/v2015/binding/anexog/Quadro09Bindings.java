/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.list.SelectionInList;
import java.beans.PropertyChangeListener;
import javax.swing.ButtonModel;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro09BindingsBase;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosPropertyChangeListener;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro09PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro09Bindings
extends Quadro09BindingsBase {
    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        Quadro09BindingsBase.doBindings(model, panel);
        Quadro09Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro09PanelExtension panel, Quadro09 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        SelectionInList<ICatalogItem> selectionInListC901a = BindingsProxy.bindAndReturn(panel.getAnexoGq09C901a(), beanModel, "anexoGq09C901a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC902a = BindingsProxy.bindAndReturn(panel.getAnexoGq09C902a(), beanModel, "anexoGq09C902a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC903a = BindingsProxy.bindAndReturn(panel.getAnexoGq09C903a(), beanModel, "anexoGq09C903a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC904a = BindingsProxy.bindAndReturn(panel.getAnexoGq09C904a(), beanModel, "anexoGq09C904a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC905a = BindingsProxy.bindAndReturn(panel.getAnexoGq09C905a(), beanModel, "anexoGq09C905a", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq09C901a(), selectionInListC901a));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq09C902a(), selectionInListC902a));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq09C903a(), selectionInListC903a));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq09C904a(), selectionInListC904a));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq09C905a(), selectionInListC905a));
        panel.getAnexoGq09C904a().setEditable(false);
        panel.getAnexoGq09C904a().setEnabled(false);
        panel.getAnexoGq09C905a().setEditable(false);
        panel.getAnexoGq09C905a().setEnabled(false);
        panel.getAnexoGq09B1OP1().setModel(new RadioButtonCustomAdapter(model, "anexoGq09B1", "1"));
        panel.getAnexoGq09B1OP2().setModel(new RadioButtonCustomAdapter(model, "anexoGq09B1", "2"));
        Quadro09Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro09 model, Quadro09PanelExtension panel) {
        panel.getAnexoGq09B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq09B1OP2().setEnabled(Session.isEditable().booleanValue());
    }
}

