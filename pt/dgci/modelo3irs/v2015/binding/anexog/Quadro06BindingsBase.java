/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro06PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoGq06C601b(), beanModel, "anexoGq06C601b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq06C601c(), beanModel, "anexoGq06C601c", true);
        BindingsProxy.bind(panel.getAnexoGq06C601d(), beanModel, "anexoGq06C601d", true);
        BindingsProxy.bind(panel.getAnexoGq06C601e(), beanModel, "anexoGq06C601e", true);
        BindingsProxy.bind(panel.getAnexoGq06C602b(), beanModel, "anexoGq06C602b", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq06C602c(), beanModel, "anexoGq06C602c", true);
        BindingsProxy.bind(panel.getAnexoGq06C602d(), beanModel, "anexoGq06C602d", true);
        BindingsProxy.bind(panel.getAnexoGq06C602e(), beanModel, "anexoGq06C602e", true);
        BindingsProxy.bind(panel.getAnexoGq06C1c(), beanModel, "anexoGq06C1c", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq06C601c", beanModel.getBean(), "anexoGq06C1cFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq06C602c", beanModel.getBean(), "anexoGq06C1cFormula");
        BindingsProxy.bind(panel.getAnexoGq06C1d(), beanModel, "anexoGq06C1d", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq06C601d", beanModel.getBean(), "anexoGq06C1dFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq06C602d", beanModel.getBean(), "anexoGq06C1dFormula");
        BindingsProxy.bind(panel.getAnexoGq06C1e(), beanModel, "anexoGq06C1e", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq06C601e", beanModel.getBean(), "anexoGq06C1eFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoGq06C602e", beanModel.getBean(), "anexoGq06C1eFormula");
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getAnexoGq06C601b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C601c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C601d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C601e().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C602b().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C602c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C602d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C602e().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C1c().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C1d().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq06C1e().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C601b(), "anexoGq06C601b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C601c(), "anexoGq06C601c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C601d(), "anexoGq06C601d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C601e(), "anexoGq06C601e");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C602b(), "anexoGq06C602b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C602c(), "anexoGq06C602c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C602d(), "anexoGq06C602d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C602e(), "anexoGq06C602e");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C1c(), "anexoGq06C1c");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C1d(), "anexoGq06C1d");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq06C1e(), "anexoGq06C1e");
    }
}

