/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.list.SelectionInList;
import java.beans.PropertyChangeListener;
import javax.swing.JComboBox;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosPropertyChangeListener;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro05PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        SelectionInList<ICatalogItem> selectionInListC1 = BindingsProxy.bindAndReturn(panel.getAnexoGq05AC1(), beanModel, "anexoGq05AC1", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        SelectionInList<ICatalogItem> selectionInListC7 = BindingsProxy.bindAndReturn(panel.getAnexoGq05AC7(), beanModel, "anexoGq05AC7", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq05AC1(), selectionInListC1));
        rosto.addTitularesPropertyChangeListener(new TitularesComCDepEFalecidosPropertyChangeListener(rosto, panel.getAnexoGq05AC7(), selectionInListC7));
        Quadro05Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro05 model, Quadro05PanelExtension panel) {
    }
}

