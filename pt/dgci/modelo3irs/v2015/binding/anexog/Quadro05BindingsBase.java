/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro05PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoGq05C501(), beanModel, "anexoGq05C501", true);
        BindingsProxy.bind(panel.getAnexoGq05C502(), beanModel, "anexoGq05C502", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_AnexoGQ4"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_AnexoGQ4"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05C503(), beanModel, "anexoGq05C503", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_AnexoGQ4"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_AnexoGQ4"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05C504(), beanModel, "anexoGq05C504", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_AnexoGQ4"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_AnexoGQ4"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05C505(), beanModel, "anexoGq05C505", true);
        BindingsProxy.bind(panel.getAnexoGq05C506(), beanModel, "anexoGq05C506", true);
        BindingsProxy.bind(panel.getAnexoGq05C507(), beanModel, "anexoGq05C507", true);
        BindingsProxy.bind(panel.getAnexoGq05C508(), beanModel, "anexoGq05C508", true);
        BindingsProxy.bind(panel.getAnexoGq05C509(), beanModel, "anexoGq05C509", true);
        BindingsProxy.bind(panel.getAnexoGq05C510(), beanModel, "anexoGq05C510", true);
        BindingsProxy.bind(panel.getAnexoGq05C511(), beanModel, "anexoGq05C511", true);
        BindingsProxy.bind(panel.getAnexoGq05C521(), beanModel, "anexoGq05C521", true);
        BindingsProxy.bind(panel.getAnexoGq05C522(), beanModel, "anexoGq05C522", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_AnexoGQ4"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_AnexoGQ4"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05C523(), beanModel, "anexoGq05C523", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_AnexoGQ4"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_AnexoGQ4"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05C524(), beanModel, "anexoGq05C524", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_AnexoGQ4"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_AnexoGQ4"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05C525(), beanModel, "anexoGq05C525", true);
        BindingsProxy.bind(panel.getAnexoGq05C526(), beanModel, "anexoGq05C526", true);
        BindingsProxy.bind(panel.getAnexoGq05C527(), beanModel, "anexoGq05C527", true);
        BindingsProxy.bind(panel.getAnexoGq05C528(), beanModel, "anexoGq05C528", true);
        BindingsProxy.bind(panel.getAnexoGq05C529(), beanModel, "anexoGq05C529", true);
        BindingsProxy.bind(panel.getAnexoGq05C530(), beanModel, "anexoGq05C530", true);
        BindingsProxy.bind(panel.getAnexoGq05C531(), beanModel, "anexoGq05C531", true);
        BindingsProxy.bind(panel.getAnexoGq05AC1(), beanModel, "anexoGq05AC1", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05AC2(), beanModel, "anexoGq05AC2", true);
        BindingsProxy.bind(panel.getAnexoGq05AC3(), beanModel, "anexoGq05AC3", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_TipoPredio"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_TipoPredio"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05AC4(), beanModel, "anexoGq05AC4", true);
        BindingsProxy.bind(panel.getAnexoGq05AC5(), beanModel, "anexoGq05AC5", true);
        BindingsProxy.bind(panel.getAnexoGq05AC6(), beanModel, "anexoGq05AC6", true);
        BindingsProxy.bind(panel.getAnexoGq05AC7(), beanModel, "anexoGq05AC7", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_RostoTitularesComCDepEFalecidos"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05AC8(), beanModel, "anexoGq05AC8", true);
        BindingsProxy.bind(panel.getAnexoGq05AC9(), beanModel, "anexoGq05AC9", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_TipoPredio"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_TipoPredio"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bind(panel.getAnexoGq05AC10(), beanModel, "anexoGq05AC10", true);
        BindingsProxy.bind(panel.getAnexoGq05AC11(), beanModel, "anexoGq05AC11", true);
        BindingsProxy.bind(panel.getAnexoGq05AC12(), beanModel, "anexoGq05AC12", true);
        BindingsProxy.bind(panel.getAnexoGq05AC13(), beanModel, "anexoGq05AC13", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_Pais_AnxG"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_Pais_AnxG"), Session.isComboBoxEditable().booleanValue());
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoGq05C501().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C502().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C503().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C504().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C505().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C506().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C507().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C508().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C509().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C510().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C511().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C521().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C522().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C523().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C524().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C525().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C526().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C527().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C528().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C529().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C530().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05C531().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC5().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC6().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC7().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC8().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC9().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC10().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC11().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC12().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq05AC13().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C501(), "anexoGq05C501");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C502(), "anexoGq05C502");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C503(), "anexoGq05C503");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C504(), "anexoGq05C504");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C505(), "anexoGq05C505");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C506(), "anexoGq05C506");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C507(), "anexoGq05C507");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C508(), "anexoGq05C508");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C509(), "anexoGq05C509");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C510(), "anexoGq05C510");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C511(), "anexoGq05C511");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C521(), "anexoGq05C521");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C522(), "anexoGq05C522");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C523(), "anexoGq05C523");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C524(), "anexoGq05C524");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C525(), "anexoGq05C525");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C526(), "anexoGq05C526");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C527(), "anexoGq05C527");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C528(), "anexoGq05C528");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C529(), "anexoGq05C529");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C530(), "anexoGq05C530");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05C531(), "anexoGq05C531");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC1(), "anexoGq05AC1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC2(), "anexoGq05AC2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC3(), "anexoGq05AC3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC4(), "anexoGq05AC4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC5(), "anexoGq05AC5");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC6(), "anexoGq05AC6");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC7(), "anexoGq05AC7");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC8(), "anexoGq05AC8");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC9(), "anexoGq05AC9");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC10(), "anexoGq05AC10");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC11(), "anexoGq05AC11");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC12(), "anexoGq05AC12");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoGq05AC13(), "anexoGq05AC13");
    }
}

