/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro08BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro08PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro08Bindings
extends Quadro08BindingsBase {
    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        Quadro08BindingsBase.doBindings(model, panel);
        Quadro08Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro08PanelExtension panel, Quadro08 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoGq08T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoGq08T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoGq08T1().getColumnModel().getColumn(2).setCellEditor(new TitularesComCDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoGq08T1().getColumnModel().getColumn(3).setCellEditor(new NifCellEditor());
        panel.getAnexoGq08T1().getColumnModel().getColumn(5).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoGq08T1().getColumnModel().getColumn(5).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoGq08T1().getColumnModel().getColumn(6).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoGq08T1().getColumnModel().getColumn(6).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoGq08T1().getColumnModel().getColumn(7).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoGq08T1().getColumnModel().getColumn(7).setCellRenderer(jMoneyCellRenderer);
        panel.getAnexoGq08T1().getColumnModel().getColumn(8).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoGq08T1().getColumnModel().getColumn(8).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoGq08T1().getColumnModel().getColumn(9).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoGq08T1().getColumnModel().getColumn(9).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor1 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor1.getComponent()).setColumns(13);
        panel.getAnexoGq08T1().getColumnModel().getColumn(10).setCellEditor(jMoneyCellEditor1);
        JMoneyCellRenderer jMoneyCellRenderer1 = new JMoneyCellRenderer();
        jMoneyCellRenderer1.setColumns(13);
        panel.getAnexoGq08T1().getColumnModel().getColumn(10).setCellRenderer(jMoneyCellRenderer1);
        JMoneyCellEditor jMoneyCellEditor2 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor2.getComponent()).setColumns(13);
        panel.getAnexoGq08T1().getColumnModel().getColumn(11).setCellEditor(jMoneyCellEditor2);
        JMoneyCellRenderer jMoneyCellRenderer2 = new JMoneyCellRenderer();
        jMoneyCellRenderer2.setColumns(13);
        panel.getAnexoGq08T1().getColumnModel().getColumn(11).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoGq08T2().getColumnModel().getColumn(2).setCellEditor(new NifCellEditor());
    }
}

