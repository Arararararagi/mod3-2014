/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro03BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro03PanelExtension;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Quadro03Bindings
extends Quadro03BindingsBase {
    public static void doBindings(pt.dgci.modelo3irs.v2015.model.anexog.Quadro03 model, Quadro03PanelExtension panel) {
        Quadro03BindingsBase.doBindings(model, panel);
        Quadro03Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro03PanelExtension panel, pt.dgci.modelo3irs.v2015.model.anexog.Quadro03 model) {
        if (model == null) {
            return;
        }
        Quadro03Bindings.initBindingConnectors(model);
    }

    private static void initBindingConnectors(pt.dgci.modelo3irs.v2015.model.anexog.Quadro03 model) {
        if (!Modelo3IRSv2015Parameters.instance().isDpapelIRS()) {
            Quadro03 quadro3Rosto = ((RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class)).getQuadro03();
            UnidireccionalPropertyConnector nifAConnection = new UnidireccionalPropertyConnector(quadro3Rosto, "q03C03", model, "anexoGq03C02");
            nifAConnection.updateProperty2();
            UnidireccionalPropertyConnector nifBConnection = new UnidireccionalPropertyConnector(quadro3Rosto, "q03C04", model, "anexoGq03C03");
            nifBConnection.updateProperty2();
        }
    }
}

