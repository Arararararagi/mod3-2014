/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog;

import java.awt.Component;
import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexog.Quadro04PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.JPercentCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.JPercentCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoGq04T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoGq04T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoGq04T1().getColumnModel().getColumn(2).setCellEditor(new TitularesComCDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoGq04T1().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(4).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(4).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoGq04T1().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoGq04T1().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer);
        panel.getAnexoGq04T1().getColumnModel().getColumn(6).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(6).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(7).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(7).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor2 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor2.getComponent()).setColumns(13);
        panel.getAnexoGq04T1().getColumnModel().getColumn(8).setCellEditor(jMoneyCellEditor2);
        JMoneyCellRenderer jMoneyCellRenderer2 = new JMoneyCellRenderer();
        jMoneyCellRenderer2.setColumns(13);
        panel.getAnexoGq04T1().getColumnModel().getColumn(8).setCellRenderer(jMoneyCellRenderer2);
        JMoneyCellEditor jMoneyCellEditor3 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor3.getComponent()).setColumns(13);
        panel.getAnexoGq04T1().getColumnModel().getColumn(9).setCellEditor(jMoneyCellEditor3);
        JMoneyCellRenderer jMoneyCellRenderer3 = new JMoneyCellRenderer();
        jMoneyCellRenderer3.setColumns(13);
        panel.getAnexoGq04T1().getColumnModel().getColumn(9).setCellRenderer(jMoneyCellRenderer3);
        panel.getAnexoGq04T1().getColumnModel().getColumn(10).setCellEditor(new LimitedTextCellEditor(8, 6, false));
        panel.getAnexoGq04T1().getColumnModel().getColumn(12).setCellEditor(new NumericCellEditor(5, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(12).setCellRenderer(new NumericCellRenderer(5, 0));
        panel.getAnexoGq04T1().getColumnModel().getColumn(13).setCellEditor(new LimitedTextCellEditor(8, 7, false));
        panel.getAnexoGq04T1().getColumnModel().getColumn(14).setCellEditor(new JPercentCellEditor(2));
        panel.getAnexoGq04T1().getColumnModel().getColumn(14).setCellRenderer(new JPercentCellRenderer(2));
        panel.getAnexoGq04T2().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoGq04T2().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoGq04T2().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoGq04T2().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoGq04T2().getColumnModel().getColumn(1).setCellEditor(new TitularesComCDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoGq04T2().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(4).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(4).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor4 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor4.getComponent()).setColumns(13);
        panel.getAnexoGq04T2().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor4);
        JMoneyCellRenderer jMoneyCellRenderer4 = new JMoneyCellRenderer();
        jMoneyCellRenderer4.setColumns(13);
        panel.getAnexoGq04T2().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer4);
        panel.getAnexoGq04T2().getColumnModel().getColumn(6).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(6).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(7).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(7).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor5 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor5.getComponent()).setColumns(13);
        panel.getAnexoGq04T2().getColumnModel().getColumn(8).setCellEditor(jMoneyCellEditor5);
        JMoneyCellRenderer jMoneyCellRenderer5 = new JMoneyCellRenderer();
        jMoneyCellRenderer5.setColumns(13);
        panel.getAnexoGq04T2().getColumnModel().getColumn(8).setCellRenderer(jMoneyCellRenderer5);
        panel.getAnexoGq04T2().getColumnModel().getColumn(9).setCellEditor(new LimitedTextCellEditor(8, 6, false));
        panel.getAnexoGq04T2().getColumnModel().getColumn(11).setCellEditor(new NumericCellEditor(5, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(11).setCellRenderer(new NumericCellRenderer(5, 0));
        panel.getAnexoGq04T2().getColumnModel().getColumn(12).setCellEditor(new LimitedTextCellEditor(8, 7, false));
        panel.getAnexoGq04T2().getColumnModel().getColumn(13).setCellEditor(new JPercentCellEditor(2));
        panel.getAnexoGq04T2().getColumnModel().getColumn(13).setCellRenderer(new JPercentCellRenderer(2));
        panel.getAnexoGq04B1OPSim().setModel(new RadioButtonCustomAdapter(model, "anexoGq04B1OP", "S"));
        panel.getAnexoGq04B1OPNao().setModel(new RadioButtonCustomAdapter(model, "anexoGq04B1OP", "N"));
        Quadro04Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro04 model, Quadro04PanelExtension panel) {
        panel.getAnexoGq04B1OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoGq04B1OPNao().setEnabled(Session.isEditable().booleanValue());
    }
}

