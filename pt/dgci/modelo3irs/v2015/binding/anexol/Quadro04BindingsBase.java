/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexol;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoLq04T1_Linha = GlazedLists.beanConnector(AnexoLq04T1_Linha.class);
        ObservableElementList<AnexoLq04T1_Linha> tableElementListAnexoLq04T1_Linha = new ObservableElementList<AnexoLq04T1_Linha>(beanModel.getBean().getAnexoLq04T1(), tableConnectorAnexoLq04T1_Linha);
        panel.getAnexoLq04T1().setModel(new EventTableModel<AnexoLq04T1_Linha>(tableElementListAnexoLq04T1_Linha, new AnexoLq04T1_LinhaAdapterFormat()));
        panel.getAnexoLq04T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoLq04T1().getTableHeader().setReorderingAllowed(false);
        ObservableElementList.Connector tableConnectorAnexoLq04T2_Linha = GlazedLists.beanConnector(AnexoLq04T2_Linha.class);
        ObservableElementList<AnexoLq04T2_Linha> tableElementListAnexoLq04T2_Linha = new ObservableElementList<AnexoLq04T2_Linha>(beanModel.getBean().getAnexoLq04T2(), tableConnectorAnexoLq04T2_Linha);
        panel.getAnexoLq04T2().setModel(new EventTableModel<AnexoLq04T2_Linha>(tableElementListAnexoLq04T2_Linha, new AnexoLq04T2_LinhaAdapterFormat()));
        panel.getAnexoLq04T2().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoLq04T2().getTableHeader().setReorderingAllowed(false);
        ObservableElementList.Connector tableConnectorAnexoLq04T3_Linha = GlazedLists.beanConnector(AnexoLq04T3_Linha.class);
        ObservableElementList<AnexoLq04T3_Linha> tableElementListAnexoLq04T3_Linha = new ObservableElementList<AnexoLq04T3_Linha>(beanModel.getBean().getAnexoLq04T3(), tableConnectorAnexoLq04T3_Linha);
        panel.getAnexoLq04T3().setModel(new EventTableModel<AnexoLq04T3_Linha>(tableElementListAnexoLq04T3_Linha, new AnexoLq04T3_LinhaAdapterFormat()));
        panel.getAnexoLq04T3().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoLq04T3().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoLq04T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoLq04T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoLq04T1(), "Cat_M3V2015_AnexoLCodRendAnexoA", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoLq04T1(), "Cat_M3V2015_CodActividade", 3);
        panel.getAnexoLq04T2().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoLq04T2().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoLq04T2(), "Cat_M3V2015_AnexoLCampoQ4AnexoB", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoLq04T2(), "Cat_M3V2015_CodActividade", 3);
        panel.getAnexoLq04T3().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoLq04T3().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoLq04T3(), "Cat_M3V2015_CodActividade", 2);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoLq04T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoLq04T2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoLq04T3().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoLq04T1(), "anexoLq04T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoLq04T2(), "anexoLq04T2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoLq04T3(), "anexoLq04T3");
    }
}

