/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexol;

import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.binding.anexol.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro06PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
        panel.getQ06B1OP1().setModel(new RadioButtonCustomAdapter(model, "q06B1", "1"));
        panel.getQ06B1OP2().setModel(new RadioButtonCustomAdapter(model, "q06B1", "2"));
        panel.getQ06B2OP3().setModel(new RadioButtonCustomAdapter(model, "q06B2", "3"));
        panel.getQ06B2OP4().setModel(new RadioButtonCustomAdapter(model, "q06B2", "4"));
    }
}

