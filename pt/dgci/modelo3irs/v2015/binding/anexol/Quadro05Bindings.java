/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexol;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexol.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro05PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model != null) {
            JMoneyCellEditor rendimentoCellEditor = new JMoneyCellEditor();
            ((JMoneyTextField)rendimentoCellEditor.getComponent()).setColumns(13);
            panel.getAnexoLq05T1().getColumnModel().getColumn(5).setCellEditor(rendimentoCellEditor);
            JMoneyCellRenderer rendimentoQ4ACellRenderer = new JMoneyCellRenderer();
            rendimentoQ4ACellRenderer.setColumns(13);
            panel.getAnexoLq05T1().getColumnModel().getColumn(5).setCellRenderer(rendimentoQ4ACellRenderer);
            JMoneyCellEditor impostoPagoEstrangeiroCellEditor = new JMoneyCellEditor();
            ((JMoneyTextField)impostoPagoEstrangeiroCellEditor.getComponent()).setColumns(13);
            panel.getAnexoLq05T1().getColumnModel().getColumn(6).setCellEditor(impostoPagoEstrangeiroCellEditor);
            JMoneyCellRenderer impostoPagoEstrangeiroCellRenderer = new JMoneyCellRenderer();
            impostoPagoEstrangeiroCellRenderer.setColumns(13);
            panel.getAnexoLq05T1().getColumnModel().getColumn(6).setCellRenderer(impostoPagoEstrangeiroCellRenderer);
        }
    }
}

