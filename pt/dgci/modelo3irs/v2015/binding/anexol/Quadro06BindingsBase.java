/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexol;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro06PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getQ06B1OP1(), beanModel, "q06B1", "1");
        BindingsProxy.bindChoice(panel.getQ06B1OP2(), beanModel, "q06B1", "2");
        BindingsProxy.bindChoice(panel.getQ06B2OP3(), beanModel, "q06B2", "3");
        BindingsProxy.bindChoice(panel.getQ06B2OP4(), beanModel, "q06B2", "4");
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getQ06B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ06B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getQ06B2OP3().setEnabled(Session.isEditable().booleanValue());
        panel.getQ06B2OP4().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ06B1OP1(), "q06B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ06B1OP2(), "q06B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ06B2OP3(), "q06B2OP3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ06B2OP4(), "q06B2OP4");
    }
}

