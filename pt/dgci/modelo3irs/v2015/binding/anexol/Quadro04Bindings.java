/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexol;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexol.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro04PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoLq04T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoLq04T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoLq04T2().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoLq04T2().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoLq04T3().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoLq04T3().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoLq04T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        JMoneyCellEditor rendimentoQ4ACellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)rendimentoQ4ACellEditor.getComponent()).setColumns(13);
        panel.getAnexoLq04T1().getColumnModel().getColumn(4).setCellEditor(rendimentoQ4ACellEditor);
        JMoneyCellRenderer rendimentoQ4ACellRenderer = new JMoneyCellRenderer();
        rendimentoQ4ACellRenderer.setColumns(13);
        panel.getAnexoLq04T1().getColumnModel().getColumn(4).setCellRenderer(rendimentoQ4ACellRenderer);
        panel.getAnexoLq04T2().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        JMoneyCellEditor rendimentoQ4BCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)rendimentoQ4BCellEditor.getComponent()).setColumns(13);
        panel.getAnexoLq04T2().getColumnModel().getColumn(4).setCellEditor(rendimentoQ4BCellEditor);
        JMoneyCellRenderer rendimentoQ4BCellRenderer = new JMoneyCellRenderer();
        rendimentoQ4BCellRenderer.setColumns(13);
        panel.getAnexoLq04T2().getColumnModel().getColumn(4).setCellRenderer(rendimentoQ4BCellRenderer);
        panel.getAnexoLq04T3().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        JMoneyCellEditor lucroCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)lucroCellEditor.getComponent()).setColumns(13);
        panel.getAnexoLq04T3().getColumnModel().getColumn(3).setCellEditor(lucroCellEditor);
        JMoneyCellRenderer lucroCellRenderer = new JMoneyCellRenderer();
        lucroCellRenderer.setColumns(13);
        panel.getAnexoLq04T3().getColumnModel().getColumn(3).setCellRenderer(lucroCellRenderer);
        JMoneyCellEditor prejuizoCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)prejuizoCellEditor.getComponent()).setColumns(13);
        panel.getAnexoLq04T3().getColumnModel().getColumn(4).setCellEditor(prejuizoCellEditor);
        JMoneyCellRenderer prejuizoCellRenderer = new JMoneyCellRenderer();
        prejuizoCellRenderer.setColumns(13);
        panel.getAnexoLq04T3().getColumnModel().getColumn(4).setCellRenderer(prejuizoCellRenderer);
    }
}

