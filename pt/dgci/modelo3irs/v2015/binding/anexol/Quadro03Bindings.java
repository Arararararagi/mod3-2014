/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexol;

import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.binding.anexol.Quadro03BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexol.Quadro03PanelExtension;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Quadro03Bindings
extends Quadro03BindingsBase {
    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        Quadro03BindingsBase.doBindings(model, panel);
        Quadro03Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro03PanelExtension panel, Quadro03 model) {
        if (model == null) {
            return;
        }
        Quadro03Bindings.initBindingConnectors(model);
    }

    private static void initBindingConnectors(Quadro03 model) {
        if (!Modelo3IRSv2015Parameters.instance().isDpapelIRS()) {
            pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 quadro3Rosto = ((RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class)).getQuadro03();
            UnidireccionalPropertyConnector nifAConnection = new UnidireccionalPropertyConnector(quadro3Rosto, "q03C03", model, "anexoLq03C02");
            nifAConnection.updateProperty2();
            UnidireccionalPropertyConnector nifBConnection = new UnidireccionalPropertyConnector(quadro3Rosto, "q03C04", model, "anexoLq03C03");
            nifBConnection.updateProperty2();
        }
    }
}

