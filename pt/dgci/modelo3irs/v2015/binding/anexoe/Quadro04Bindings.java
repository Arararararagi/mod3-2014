/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoe;

import java.awt.Component;
import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoe.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComDepEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoe.Quadro04PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoEq04T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoEq04T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoEq04T1().getColumnModel().getColumn(3).setCellEditor(new TitularesComDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoEq04T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoEq04T1().getColumnModel().getColumn(4).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoEq04T1().getColumnModel().getColumn(4).setCellRenderer(jMoneyCellRenderer);
        JMoneyCellEditor jMoneyCellEditor2 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor2.getComponent()).setColumns(13);
        panel.getAnexoEq04T1().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor2);
        JMoneyCellRenderer jMoneyCellRenderer2 = new JMoneyCellRenderer();
        jMoneyCellRenderer2.setColumns(13);
        panel.getAnexoEq04T1().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoEq04T2().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoEq04T2().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoEq04T2().getColumnModel().getColumn(3).setCellEditor(new TitularesComDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoEq04T2().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        JMoneyCellEditor jMoneyCellEditor3 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor3.getComponent()).setColumns(13);
        panel.getAnexoEq04T2().getColumnModel().getColumn(4).setCellEditor(jMoneyCellEditor3);
        JMoneyCellRenderer jMoneyCellRenderer3 = new JMoneyCellRenderer();
        jMoneyCellRenderer3.setColumns(13);
        panel.getAnexoEq04T2().getColumnModel().getColumn(4).setCellRenderer(jMoneyCellRenderer3);
        JMoneyCellEditor jMoneyCellEditor4 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor4.getComponent()).setColumns(13);
        panel.getAnexoEq04T2().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor4);
        JMoneyCellRenderer jMoneyCellRenderer4 = new JMoneyCellRenderer();
        jMoneyCellRenderer4.setColumns(13);
        panel.getAnexoEq04T2().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer4);
        panel.getAnexoEq04B1OP2().setModel(new RadioButtonCustomAdapter(model, "anexoEq04B1", "2"));
        panel.getAnexoEq04B1OP1().setModel(new RadioButtonCustomAdapter(model, "anexoEq04B1", "1"));
        Quadro04Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro04 model, Quadro04PanelExtension panel) {
        panel.getAnexoEq04B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04B1OP2().setEnabled(Session.isEditable().booleanValue());
    }
}

