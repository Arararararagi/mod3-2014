/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoe;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoe.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoEq04T1_Linha = GlazedLists.beanConnector(AnexoEq04T1_Linha.class);
        ObservableElementList<AnexoEq04T1_Linha> tableElementListAnexoEq04T1_Linha = new ObservableElementList<AnexoEq04T1_Linha>(beanModel.getBean().getAnexoEq04T1(), tableConnectorAnexoEq04T1_Linha);
        panel.getAnexoEq04T1().setModel(new EventTableModel<AnexoEq04T1_Linha>(tableElementListAnexoEq04T1_Linha, new AnexoEq04T1_LinhaAdapterFormat()));
        panel.getAnexoEq04T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoEq04T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoEq04C1(), beanModel, "anexoEq04C1", true);
        beanModel.getBean().getAnexoEq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoEq04C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoEq04C2(), beanModel, "anexoEq04C2", true);
        beanModel.getBean().getAnexoEq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoEq04C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        ObservableElementList.Connector tableConnectorAnexoEq04T2_Linha = GlazedLists.beanConnector(AnexoEq04T2_Linha.class);
        ObservableElementList<AnexoEq04T2_Linha> tableElementListAnexoEq04T2_Linha = new ObservableElementList<AnexoEq04T2_Linha>(beanModel.getBean().getAnexoEq04T2(), tableConnectorAnexoEq04T2_Linha);
        panel.getAnexoEq04T2().setModel(new EventTableModel<AnexoEq04T2_Linha>(tableElementListAnexoEq04T2_Linha, new AnexoEq04T2_LinhaAdapterFormat()));
        panel.getAnexoEq04T2().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoEq04T2().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoEq04C3(), beanModel, "anexoEq04C3", true);
        beanModel.getBean().getAnexoEq04T2().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoEq04C3Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoEq04C4(), beanModel, "anexoEq04C4", true);
        beanModel.getBean().getAnexoEq04T2().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoEq04C4Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bindChoice(panel.getAnexoEq04B1OP1(), beanModel, "anexoEq04B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoEq04B1OP2(), beanModel, "anexoEq04B1", "2");
        panel.getAnexoEq04T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoEq04T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoEq04T1(), "Cat_M3V2015_AnexoEQ4A", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoEq04T1(), "Cat_M3V2015_RostoTitularesComDepEFalecidos", 3);
        panel.getAnexoEq04T2().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoEq04T2().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoEq04T2(), "Cat_M3V2015_AnexoEQ4B", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoEq04T2(), "Cat_M3V2015_RostoTitularesComDepEFalecidos", 3);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoEq04T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04T2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04C4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoEq04B1OP2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04T1(), "anexoEq04T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04C1(), "anexoEq04C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04C2(), "anexoEq04C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04T2(), "anexoEq04T2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04C3(), "anexoEq04C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04C4(), "anexoEq04C4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04B1OP1(), "anexoEq04B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoEq04B1OP2(), "anexoEq04B1OP2");
    }

}

