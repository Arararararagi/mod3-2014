/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoe;

import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.binding.anexoe.Quadro02BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoe.Quadro02PanelExtension;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Quadro02Bindings
extends Quadro02BindingsBase {
    public static void doBindings(Quadro02 model, Quadro02PanelExtension panel) {
        Quadro02BindingsBase.doBindings(model, panel);
        Quadro02Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro02PanelExtension panel, Quadro02 model) {
        if (model == null) {
            return;
        }
        Quadro02Bindings.initBindingConnectors(model);
    }

    private static void initBindingConnectors(Quadro02 model) {
        if (!Modelo3IRSv2015Parameters.instance().isDpapelIRS()) {
            pt.dgci.modelo3irs.v2015.model.rosto.Quadro02 quadro3Rosto = ((RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class)).getQuadro02();
            UnidireccionalPropertyConnector anoConnection = new UnidireccionalPropertyConnector(quadro3Rosto, "q02C02", model, "anexoEq02C01");
            anoConnection.updateProperty2();
        }
    }
}

