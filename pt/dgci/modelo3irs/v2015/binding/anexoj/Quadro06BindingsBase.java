/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro06PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoJq06T1_Linha = GlazedLists.beanConnector(AnexoJq06T1_Linha.class);
        ObservableElementList<AnexoJq06T1_Linha> tableElementListAnexoJq06T1_Linha = new ObservableElementList<AnexoJq06T1_Linha>(beanModel.getBean().getAnexoJq06T1(), tableConnectorAnexoJq06T1_Linha);
        panel.getAnexoJq06T1().setModel(new EventTableModel<AnexoJq06T1_Linha>(tableElementListAnexoJq06T1_Linha, new AnexoJq06T1_LinhaAdapterFormat()));
        panel.getAnexoJq06T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoJq06T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoJq06C1(), beanModel, "anexoJq06C1", true);
        beanModel.getBean().getAnexoJq06T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro06BindingsBase.beanModel.getBean().setAnexoJq06C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoJq06C2(), beanModel, "anexoJq06C2", true);
        beanModel.getBean().getAnexoJq06T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro06BindingsBase.beanModel.getBean().setAnexoJq06C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoJq06C3(), beanModel, "anexoJq06C3", true);
        beanModel.getBean().getAnexoJq06T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro06BindingsBase.beanModel.getBean().setAnexoJq06C3Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        panel.getAnexoJq06T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoJq06T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoJq06T1(), "Cat_M3V2015_AnexoJQ6NLinha", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoJq06T1(), "Cat_M3V2015_AnexoJQ4", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoJq06T1(), "Cat_M3V2015_SimNao", 3);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoJq06T1(), "Cat_M3V2015_Pais_AnxJ", 4);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoJq06T1(), "Cat_M3V2015_Pais_AnxJ", 7);
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getAnexoJq06T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq06C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq06C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq06C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq06C4OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq06C4OP2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq06T1(), "anexoJq06T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq06C1(), "anexoJq06C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq06C2(), "anexoJq06C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq06C3(), "anexoJq06C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq06C4OP1(), "anexoJq06C4OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq06C4OP2(), "anexoJq06C4OP2");
    }

}

