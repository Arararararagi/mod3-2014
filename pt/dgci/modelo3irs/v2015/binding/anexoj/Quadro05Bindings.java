/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro05PanelExtension;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoJq05T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoJq05T1().getColumnModel().getColumn(1).setCellEditor(new LimitedTextCellEditor(35, 34, false));
        panel.getAnexoJq05T1().getColumnModel().getColumn(2).setCellEditor(new LimitedTextCellEditor(12, 11, false));
        panel.getAnexoJq05T2().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoJq05T2().getColumnModel().getColumn(1).setCellEditor(new LimitedTextCellEditor(35, 34, false));
    }
}

