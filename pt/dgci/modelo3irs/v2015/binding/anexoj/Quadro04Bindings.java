/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro04PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;
import pt.opensoft.taxclient.util.Session;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoJq04T1().getColumnModel().getColumn(1).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(1).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(2).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(2).setCellRenderer(new NumericCellRenderer(2, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T1().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04T1().getColumnModel().getColumn(4).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(4).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(5).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(5).setCellRenderer(new NumericCellRenderer(2, 0));
        panel.getAnexoJq04T1().getColumnModel().getColumn(6).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T1().getColumnModel().getColumn(6).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04T1().getColumnModel().getColumn(7).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T1().getColumnModel().getColumn(7).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04T1().getColumnModel().getColumn(8).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T1().getColumnModel().getColumn(8).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04T2().getColumnModel().getColumn(2).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(2).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(2, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(4).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T2().getColumnModel().getColumn(4).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04T2().getColumnModel().getColumn(5).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(5).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(6).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(6).setCellRenderer(new NumericCellRenderer(2, 0));
        panel.getAnexoJq04T2().getColumnModel().getColumn(7).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T2().getColumnModel().getColumn(7).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04T2().getColumnModel().getColumn(8).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T2().getColumnModel().getColumn(8).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04T2().getColumnModel().getColumn(9).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq04T2().getColumnModel().getColumn(9).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq04B1Sim().setModel(new RadioButtonCustomAdapter(model, "anexoJq04B1", Quadro04.ANEXOJQ04B1SIM_VALUE));
        panel.getAnexoJq04B1Nao().setModel(new RadioButtonCustomAdapter(model, "anexoJq04B1", Quadro04.ANEXOJQ04B1NAO_VALUE));
        Quadro04Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro04 model, Quadro04PanelExtension panel) {
        panel.getAnexoJq04B1Sim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq04B1Nao().setEnabled(Session.isEditable().booleanValue());
    }
}

