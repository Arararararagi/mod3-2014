/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro05PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoJq05T1_Linha = GlazedLists.beanConnector(AnexoJq05T1_Linha.class);
        ObservableElementList<AnexoJq05T1_Linha> tableElementListAnexoJq05T1_Linha = new ObservableElementList<AnexoJq05T1_Linha>(beanModel.getBean().getAnexoJq05T1(), tableConnectorAnexoJq05T1_Linha);
        panel.getAnexoJq05T1().setModel(new EventTableModel<AnexoJq05T1_Linha>(tableElementListAnexoJq05T1_Linha, new AnexoJq05T1_LinhaAdapterFormat()));
        panel.getAnexoJq05T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoJq05T1().getTableHeader().setReorderingAllowed(false);
        ObservableElementList.Connector tableConnectorAnexoJq05T2_Linha = GlazedLists.beanConnector(AnexoJq05T2_Linha.class);
        ObservableElementList<AnexoJq05T2_Linha> tableElementListAnexoJq05T2_Linha = new ObservableElementList<AnexoJq05T2_Linha>(beanModel.getBean().getAnexoJq05T2(), tableConnectorAnexoJq05T2_Linha);
        panel.getAnexoJq05T2().setModel(new EventTableModel<AnexoJq05T2_Linha>(tableElementListAnexoJq05T2_Linha, new AnexoJq05T2_LinhaAdapterFormat()));
        panel.getAnexoJq05T2().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoJq05T2().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoJq05T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoJq05T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        panel.getAnexoJq05T2().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoJq05T2().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoJq05T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq05T2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq05T1(), "anexoJq05T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq05T2(), "anexoJq05T2");
    }
}

