/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro07BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro07PanelExtension;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;

public class Quadro07Bindings
extends Quadro07BindingsBase {
    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        Quadro07BindingsBase.doBindings(model, panel);
        Quadro07Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro07PanelExtension panel, Quadro07 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoJq07T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoJq07T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoJq07T1().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(1, 0));
        panel.getAnexoJq07T1().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(1, 0));
        panel.getAnexoJq07T1().getColumnModel().getColumn(2).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq07T1().getColumnModel().getColumn(2).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq07T1().getColumnModel().getColumn(2).setMinWidth(150);
        panel.getAnexoJq07T1().getColumnModel().getColumn(2).setMaxWidth(150);
        panel.getAnexoJq07T1().getColumnModel().getColumn(3).setMinWidth(100);
        panel.getAnexoJq07T1().getColumnModel().getColumn(3).setMaxWidth(100);
    }
}

