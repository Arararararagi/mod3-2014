/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro07PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoJq07T1_Linha = GlazedLists.beanConnector(AnexoJq07T1_Linha.class);
        ObservableElementList<AnexoJq07T1_Linha> tableElementListAnexoJq07T1_Linha = new ObservableElementList<AnexoJq07T1_Linha>(beanModel.getBean().getAnexoJq07T1(), tableConnectorAnexoJq07T1_Linha);
        panel.getAnexoJq07T1().setModel(new EventTableModel<AnexoJq07T1_Linha>(tableElementListAnexoJq07T1_Linha, new AnexoJq07T1_LinhaAdapterFormat()));
        panel.getAnexoJq07T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoJq07T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoJq07T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoJq07T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoJq07T1(), "Cat_M3V2015_AnexoJQ7LinhaQ6", 1);
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getAnexoJq07T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoJq07T1(), "anexoJq07T1");
    }
}

