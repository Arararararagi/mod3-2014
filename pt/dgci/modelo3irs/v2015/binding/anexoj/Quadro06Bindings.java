/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import com.jgoodies.binding.beans.BeanAdapter;
import java.awt.Component;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro06PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.util.Session;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    private static final String SISTEMA_SOCIAL_PAIS_ORIGEM = "1";
    private static final String OUTRAS_ENTIDADES = "2";
    private static final String AMBOS = "3";

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
        JMoneyCellEditor moneyCellEditor = new JMoneyCellEditor();
        moneyCellEditor.allowNegative(true);
        ((JMoneyTextField)moneyCellEditor.getComponent()).setColumns(12);
        panel.getAnexoJq06T1().getColumnModel().getColumn(5).setCellEditor(moneyCellEditor);
        JMoneyCellRenderer moneyCellRenderer = new JMoneyCellRenderer();
        moneyCellRenderer.allowNegative(true);
        moneyCellRenderer.setColumns(12);
        panel.getAnexoJq06T1().getColumnModel().getColumn(5).setCellRenderer(moneyCellRenderer);
        panel.getAnexoJq06T1().getColumnModel().getColumn(6).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq06T1().getColumnModel().getColumn(6).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq06T1().getColumnModel().getColumn(8).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq06T1().getColumnModel().getColumn(8).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq06C4OP1().setSelected("1".equals(model.getAnexoJq06C4()) || "3".equals(model.getAnexoJq06C4()));
        panel.getAnexoJq06C4OP2().setSelected("2".equals(model.getAnexoJq06C4()) || "3".equals(model.getAnexoJq06C4()));
        Quadro06Bindings quadro06Bindings = new Quadro06Bindings();
        quadro06Bindings.getClass();
        panel.getAnexoJq06C4OP1().addChangeListener(quadro06Bindings.new CustomChangeListener("1", "2", panel.getAnexoJq06C4OP2()));
        Quadro06Bindings quadro06Bindings2 = new Quadro06Bindings();
        quadro06Bindings2.getClass();
        panel.getAnexoJq06C4OP2().addChangeListener(quadro06Bindings2.new CustomChangeListener("2", "1", panel.getAnexoJq06C4OP1()));
        Quadro06Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro06 model, Quadro06PanelExtension panel) {
        panel.getAnexoJq06C4OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoJq06C4OP2().setEnabled(Session.isEditable().booleanValue());
    }

    class CustomChangeListener
    implements ChangeListener {
        private String selectedValue;
        private String deselectedValue;
        private JRadioButton otherButton;
        private boolean wasSelected;

        public CustomChangeListener(String selectedValue, String deselectedValue, JRadioButton otherButton) {
            this.selectedValue = selectedValue;
            this.deselectedValue = deselectedValue;
            this.otherButton = otherButton;
            this.wasSelected = false;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (e.getSource() instanceof JRadioButton && ((JRadioButton)e.getSource()).isSelected() && !this.wasSelected) {
                if (this.otherButton.isSelected()) {
                    Quadro06BindingsBase.beanModel.getValueModel("anexoJq06C4").setValue("3");
                } else {
                    Quadro06BindingsBase.beanModel.getValueModel("anexoJq06C4").setValue(this.selectedValue);
                }
                this.wasSelected = true;
            }
            if (e.getSource() instanceof JRadioButton && !((JRadioButton)e.getSource()).isSelected() && this.wasSelected) {
                if (this.otherButton.isSelected()) {
                    Quadro06BindingsBase.beanModel.getValueModel("anexoJq06C4").setValue(this.deselectedValue);
                } else {
                    Quadro06BindingsBase.beanModel.getValueModel("anexoJq06C4").setValue(null);
                }
                this.wasSelected = false;
            }
        }
    }

}

