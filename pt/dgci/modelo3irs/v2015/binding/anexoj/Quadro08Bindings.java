/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoj;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoj.Quadro08BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoj.Quadro08PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;

public class Quadro08Bindings
extends Quadro08BindingsBase {
    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        Quadro08BindingsBase.doBindings(model, panel);
        Quadro08Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro08PanelExtension panel, Quadro08 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoJq08T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoJq08T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoJq08T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getAnexoJq08T1().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq08T1().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoJq08T1().getColumnModel().getColumn(4).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoJq08T1().getColumnModel().getColumn(4).setCellRenderer(new JMoneyCellRenderer());
        JMoneyCellEditor moneyCellEditorSobretaxa = new JMoneyCellEditor();
        ((JMoneyTextField)moneyCellEditorSobretaxa.getComponent()).setColumns(9);
        JMoneyCellRenderer moneyCellRendererSobretaxa = new JMoneyCellRenderer();
        moneyCellRendererSobretaxa.setColumns(9);
        panel.getAnexoJq08T1().getColumnModel().getColumn(5).setCellEditor(moneyCellEditorSobretaxa);
        panel.getAnexoJq08T1().getColumnModel().getColumn(5).setCellRenderer(moneyCellRendererSobretaxa);
    }
}

