/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoi;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro05PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoIq05C501(), beanModel, "anexoIq05C501", true);
        BindingsProxy.bind(panel.getAnexoIq05C501a(), beanModel, "anexoIq05C501a", true);
        BindingsProxy.bind(panel.getAnexoIq05C502(), beanModel, "anexoIq05C502", true);
        BindingsProxy.bind(panel.getAnexoIq05C502a(), beanModel, "anexoIq05C502a", true);
        BindingsProxy.bind(panel.getAnexoIq05C1(), beanModel, "anexoIq05C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C501", beanModel.getBean(), "anexoIq05C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C502", beanModel.getBean(), "anexoIq05C1Formula");
        BindingsProxy.bind(panel.getAnexoIq05C1a(), beanModel, "anexoIq05C1a", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C501a", beanModel.getBean(), "anexoIq05C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C502a", beanModel.getBean(), "anexoIq05C1aFormula");
        BindingsProxy.bind(panel.getAnexoIq05C504(), beanModel, "anexoIq05C504", true);
        BindingsProxy.bind(panel.getAnexoIq05C504a(), beanModel, "anexoIq05C504a", true);
        BindingsProxy.bind(panel.getAnexoIq05C505(), beanModel, "anexoIq05C505", true);
        BindingsProxy.bind(panel.getAnexoIq05C505a(), beanModel, "anexoIq05C505a", true);
        BindingsProxy.bind(panel.getAnexoIq05C506(), beanModel, "anexoIq05C506", true);
        BindingsProxy.bind(panel.getAnexoIq05C506a(), beanModel, "anexoIq05C506a", true);
        BindingsProxy.bind(panel.getAnexoIq05C507(), beanModel, "anexoIq05C507", true);
        BindingsProxy.bind(panel.getAnexoIq05C507a(), beanModel, "anexoIq05C507a", true);
        BindingsProxy.bind(panel.getAnexoIq05C508(), beanModel, "anexoIq05C508", true);
        BindingsProxy.bind(panel.getAnexoIq05C508a(), beanModel, "anexoIq05C508a", true);
        BindingsProxy.bind(panel.getAnexoIq05C2(), beanModel, "anexoIq05C2", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C504", beanModel.getBean(), "anexoIq05C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C505", beanModel.getBean(), "anexoIq05C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C506", beanModel.getBean(), "anexoIq05C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C507", beanModel.getBean(), "anexoIq05C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C508", beanModel.getBean(), "anexoIq05C2Formula");
        BindingsProxy.bind(panel.getAnexoIq05C2a(), beanModel, "anexoIq05C2a", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C504a", beanModel.getBean(), "anexoIq05C2aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C505a", beanModel.getBean(), "anexoIq05C2aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C506a", beanModel.getBean(), "anexoIq05C2aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C507a", beanModel.getBean(), "anexoIq05C2aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq05C508a", beanModel.getBean(), "anexoIq05C2aFormula");
        BindingsProxy.bind(panel.getAnexoIq05C503(), beanModel, "anexoIq05C503", true);
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoIq05C501().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C501a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C502().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C502a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C1a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C504().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C504a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C505().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C505a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C506().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C506a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C507().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C507a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C508().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C508a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C2a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq05C503().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C501(), "anexoIq05C501");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C501a(), "anexoIq05C501a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C502(), "anexoIq05C502");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C502a(), "anexoIq05C502a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C1(), "anexoIq05C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C1a(), "anexoIq05C1a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C504(), "anexoIq05C504");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C504a(), "anexoIq05C504a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C505(), "anexoIq05C505");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C505a(), "anexoIq05C505a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C506(), "anexoIq05C506");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C506a(), "anexoIq05C506a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C507(), "anexoIq05C507");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C507a(), "anexoIq05C507a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C508(), "anexoIq05C508");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C508a(), "anexoIq05C508a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C2(), "anexoIq05C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C2a(), "anexoIq05C2a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq05C503(), "anexoIq05C503");
    }
}

