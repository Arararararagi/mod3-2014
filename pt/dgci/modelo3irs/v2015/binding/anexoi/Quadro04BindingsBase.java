/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoi;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro04PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoIq04C04(), beanModel, "anexoIq04C04", true);
        BindingsProxy.bind(panel.getAnexoIq04C05(), beanModel, "anexoIq04C05", true);
        BindingsProxy.bind(panel.getAnexoIq04C06(), beanModel, "anexoIq04C06", true);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoIq04C04().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq04C05().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq04C06().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq04C04(), "anexoIq04C04");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq04C05(), "anexoIq04C05");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq04C06(), "anexoIq04C06");
    }
}

