/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoi;

import pt.dgci.modelo3irs.v2015.binding.anexoi.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro04PanelExtension;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        Quadro04Bindings.doExtraBindingsForEnabled(panel, model);
    }

    public static void doExtraBindingsForEnabled(Quadro04PanelExtension panel, Quadro04 model) {
        panel.getAnexoIq04C04().setEditable(!model.getAnexoPai().getFormKey().getSubId().equals(String.valueOf(model.getAnexoIq04C04())));
        panel.getAnexoIq04C05().setEditable(!model.getAnexoPai().getFormKey().getSubId().equals(String.valueOf(model.getAnexoIq04C05())));
    }
}

