/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoi;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro07PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoIq07T1_Linha = GlazedLists.beanConnector(AnexoIq07T1_Linha.class);
        ObservableElementList<AnexoIq07T1_Linha> tableElementListAnexoIq07T1_Linha = new ObservableElementList<AnexoIq07T1_Linha>(beanModel.getBean().getAnexoIq07T1(), tableConnectorAnexoIq07T1_Linha);
        panel.getAnexoIq07T1().setModel(new EventTableModel<AnexoIq07T1_Linha>(tableElementListAnexoIq07T1_Linha, new AnexoIq07T1_LinhaAdapterFormat()));
        panel.getAnexoIq07T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoIq07T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoIq07C1(), beanModel, "anexoIq07C1", true);
        beanModel.getBean().getAnexoIq07T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro07BindingsBase.beanModel.getBean().setAnexoIq07C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoIq07C2(), beanModel, "anexoIq07C2", true);
        beanModel.getBean().getAnexoIq07T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro07BindingsBase.beanModel.getBean().setAnexoIq07C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoIq07C3(), beanModel, "anexoIq07C3", true);
        beanModel.getBean().getAnexoIq07T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro07BindingsBase.beanModel.getBean().setAnexoIq07C3Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoIq07C4(), beanModel, "anexoIq07C4", true);
        beanModel.getBean().getAnexoIq07T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro07BindingsBase.beanModel.getBean().setAnexoIq07C4Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoIq07C5(), beanModel, "anexoIq07C5", true);
        beanModel.getBean().getAnexoIq07T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro07BindingsBase.beanModel.getBean().setAnexoIq07C5Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        panel.getAnexoIq07T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoIq07T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getAnexoIq07T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq07C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq07C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq07C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq07C4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq07C5().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq07T1(), "anexoIq07T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq07C1(), "anexoIq07C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq07C2(), "anexoIq07C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq07C3(), "anexoIq07C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq07C4(), "anexoIq07C4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq07C5(), "anexoIq07C5");
    }

}

