/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoi;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoi.Quadro07BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro07PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.JPercentCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.JPercentCellRenderer;

public class Quadro07Bindings
extends Quadro07BindingsBase {
    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        Quadro07BindingsBase.doBindings(model, panel);
        Quadro07Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro07PanelExtension panel, Quadro07 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoIq07T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoIq07T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoIq07T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getAnexoIq07T1().getColumnModel().getColumn(2).setCellEditor(new JPercentCellEditor(2));
        panel.getAnexoIq07T1().getColumnModel().getColumn(2).setCellRenderer(new JPercentCellRenderer(2));
        JMoneyCellEditor moneyCellEditor = new JMoneyCellEditor();
        moneyCellEditor.allowNegative(true);
        ((JMoneyTextField)moneyCellEditor.getComponent()).setColumns(12);
        panel.getAnexoIq07T1().getColumnModel().getColumn(3).setCellEditor(moneyCellEditor);
        JMoneyCellRenderer moneyCellRenderer = new JMoneyCellRenderer();
        moneyCellRenderer.allowNegative(true);
        moneyCellRenderer.setColumns(12);
        panel.getAnexoIq07T1().getColumnModel().getColumn(3).setCellRenderer(moneyCellRenderer);
        JMoneyCellEditor moneyCellEditor1 = new JMoneyCellEditor();
        moneyCellEditor1.allowNegative(true);
        ((JMoneyTextField)moneyCellEditor1.getComponent()).setColumns(12);
        panel.getAnexoIq07T1().getColumnModel().getColumn(4).setCellEditor(moneyCellEditor1);
        JMoneyCellRenderer moneyCellRenderer1 = new JMoneyCellRenderer();
        moneyCellRenderer1.allowNegative(true);
        moneyCellRenderer1.setColumns(12);
        panel.getAnexoIq07T1().getColumnModel().getColumn(4).setCellRenderer(moneyCellRenderer1);
        JMoneyCellEditor moneyCellEditor2 = new JMoneyCellEditor();
        moneyCellEditor2.allowNegative(true);
        ((JMoneyTextField)moneyCellEditor2.getComponent()).setColumns(12);
        panel.getAnexoIq07T1().getColumnModel().getColumn(5).setCellEditor(moneyCellEditor2);
        JMoneyCellRenderer moneyCellRenderer2 = new JMoneyCellRenderer();
        moneyCellRenderer2.allowNegative(true);
        moneyCellRenderer2.setColumns(12);
        panel.getAnexoIq07T1().getColumnModel().getColumn(5).setCellRenderer(moneyCellRenderer2);
        panel.getAnexoIq07T1().getColumnModel().getColumn(6).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoIq07T1().getColumnModel().getColumn(6).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoIq07T1().getColumnModel().getColumn(7).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoIq07T1().getColumnModel().getColumn(7).setCellRenderer(new JMoneyCellRenderer());
    }
}

