/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoi;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoi.Quadro08PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro08BindingsBase {
    protected static BeanAdapter<Quadro08> beanModel = null;
    protected static Quadro08PanelExtension panelExtension = null;

    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro08>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoIq08C801(), beanModel, "anexoIq08C801", true);
        BindingsProxy.bind(panel.getAnexoIq08C801a(), beanModel, "anexoIq08C801a", true);
        BindingsProxy.bind(panel.getAnexoIq08C802(), beanModel, "anexoIq08C802", true);
        BindingsProxy.bind(panel.getAnexoIq08C802a(), beanModel, "anexoIq08C802a", true);
        BindingsProxy.bind(panel.getAnexoIq08C806(), beanModel, "anexoIq08C806", true);
        BindingsProxy.bind(panel.getAnexoIq08C806a(), beanModel, "anexoIq08C806a", true);
        BindingsProxy.bind(panel.getAnexoIq08C803(), beanModel, "anexoIq08C803", true);
        BindingsProxy.bind(panel.getAnexoIq08C803a(), beanModel, "anexoIq08C803a", true);
        BindingsProxy.bind(panel.getAnexoIq08C807(), beanModel, "anexoIq08C807", true);
        BindingsProxy.bind(panel.getAnexoIq08C807a(), beanModel, "anexoIq08C807a", true);
        BindingsProxy.bind(panel.getAnexoIq08C804(), beanModel, "anexoIq08C804", true);
        BindingsProxy.bind(panel.getAnexoIq08C804a(), beanModel, "anexoIq08C804a", true);
        BindingsProxy.bind(panel.getAnexoIq08C805(), beanModel, "anexoIq08C805", true);
        BindingsProxy.bind(panel.getAnexoIq08C805a(), beanModel, "anexoIq08C805a", true);
        BindingsProxy.bind(panel.getAnexoIq08C1(), beanModel, "anexoIq08C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C801", beanModel.getBean(), "anexoIq08C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C802", beanModel.getBean(), "anexoIq08C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C803", beanModel.getBean(), "anexoIq08C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C804", beanModel.getBean(), "anexoIq08C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C805", beanModel.getBean(), "anexoIq08C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C806", beanModel.getBean(), "anexoIq08C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C807", beanModel.getBean(), "anexoIq08C1Formula");
        BindingsProxy.bind(panel.getAnexoIq08C1a(), beanModel, "anexoIq08C1a", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C801a", beanModel.getBean(), "anexoIq08C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C802a", beanModel.getBean(), "anexoIq08C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C803a", beanModel.getBean(), "anexoIq08C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C804a", beanModel.getBean(), "anexoIq08C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C805a", beanModel.getBean(), "anexoIq08C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C806a", beanModel.getBean(), "anexoIq08C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoIq08C807a", beanModel.getBean(), "anexoIq08C1aFormula");
        Quadro08BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro08BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro08 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro08PanelExtension panel) {
        panel.getAnexoIq08C801().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C801a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C802().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C802a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C806().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C806a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C803().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C803a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C807().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C807a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C804().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C804a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C805().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C805a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoIq08C1a().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro08PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C801(), "anexoIq08C801");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C801a(), "anexoIq08C801a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C802(), "anexoIq08C802");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C802a(), "anexoIq08C802a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C806(), "anexoIq08C806");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C806a(), "anexoIq08C806a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C803(), "anexoIq08C803");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C803a(), "anexoIq08C803a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C807(), "anexoIq08C807");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C807a(), "anexoIq08C807a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C804(), "anexoIq08C804");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C804a(), "anexoIq08C804a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C805(), "anexoIq08C805");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C805a(), "anexoIq08C805a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C1(), "anexoIq08C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoIq08C1a(), "anexoIq08C1a");
    }
}

