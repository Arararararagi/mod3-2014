/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro03PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro03BindingsBase {
    protected static BeanAdapter<Quadro03> beanModel = null;
    protected static Quadro03PanelExtension panelExtension = null;

    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro03>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoSSq03C05(), beanModel, "anexoSSq03C05", true);
        BindingsProxy.bind(panel.getAnexoSSq03C06(), beanModel, "anexoSSq03C06", true);
        BindingsProxy.bind(panel.getAnexoSSq03C07(), beanModel, "anexoSSq03C07", true);
        BindingsProxy.bind(panel.getAnexoSSq03C08(), beanModel, "anexoSSq03C08", true);
        Quadro03BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro03BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro03 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro03PanelExtension panel) {
        panel.getAnexoSSq03C05().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq03C06().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq03C07().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq03C08().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro03PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq03C05(), "anexoSSq03C05");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq03C06(), "anexoSSq03C06");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq03C07(), "anexoSSq03C07");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq03C08(), "anexoSSq03C08");
    }
}

