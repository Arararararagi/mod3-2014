/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import pt.dgci.modelo3irs.v2015.binding.anexoss.Quadro03BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro03PanelExtension;

public class Quadro03Bindings
extends Quadro03BindingsBase {
    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        Quadro03BindingsBase.doBindings(model, panel);
        Quadro03Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro03PanelExtension panel, Quadro03 model) {
        if (model == null) {
            return;
        }
    }
}

