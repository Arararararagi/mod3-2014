/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import pt.dgci.modelo3irs.v2015.binding.anexoss.Quadro01BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro01PanelExtension;

public class Quadro01Bindings
extends Quadro01BindingsBase {
    public static void doBindings(Quadro01 model, Quadro01PanelExtension panel) {
        Quadro01BindingsBase.doBindings(model, panel);
        Quadro01Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro01PanelExtension panel, Quadro01 model) {
    }
}

