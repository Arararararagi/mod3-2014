/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import java.awt.Component;
import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoss.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro06PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoSSq06B1Sim().setModel(new RadioButtonCustomAdapter(model, "anexoSSq06B1", "S"));
        panel.getAnexoSSq06B1Nao().setModel(new RadioButtonCustomAdapter(model, "anexoSSq06B1", "N"));
        panel.getAnexoSSq06T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoSSq06T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoSSq06T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        LimitedTextCellEditor nifEstrangeiroEditor = new LimitedTextCellEditor(13, 12, false);
        ((JTextField)nifEstrangeiroEditor.getComponent()).setHorizontalAlignment(4);
        panel.getAnexoSSq06T1().getColumnModel().getColumn(3).setCellEditor(nifEstrangeiroEditor);
        DefaultPFCellRenderer nifEstrangeiroRenderer = new DefaultPFCellRenderer();
        nifEstrangeiroRenderer.setHorizontalAlignment(4);
        panel.getAnexoSSq06T1().getColumnModel().getColumn(3).setCellRenderer(nifEstrangeiroRenderer);
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoSSq06T1().getColumnModel().getColumn(4).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoSSq06T1().getColumnModel().getColumn(4).setCellRenderer(jMoneyCellRenderer);
    }
}

