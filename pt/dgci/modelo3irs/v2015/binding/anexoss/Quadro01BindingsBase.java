/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro01PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro01BindingsBase {
    protected static BeanAdapter<Quadro01> beanModel = null;
    protected static Quadro01PanelExtension panelExtension = null;

    public static void doBindings(Quadro01 model, Quadro01PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro01>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoSSq01B1(), beanModel, "anexoSSq01B1", true);
        BindingsProxy.bind(panel.getAnexoSSq01B2(), beanModel, "anexoSSq01B2", true);
        BindingsProxy.bind(panel.getAnexoSSq01B3(), beanModel, "anexoSSq01B3", true);
        Quadro01BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro01BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro01 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro01PanelExtension panel) {
        panel.getAnexoSSq01B1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq01B2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq01B3().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro01PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq01B1(), "anexoSSq01B1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq01B2(), "anexoSSq01B2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq01B3(), "anexoSSq01B3");
    }
}

