/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro04PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoSSq04C401(), beanModel, "anexoSSq04C401", true);
        BindingsProxy.bind(panel.getAnexoSSq04C402(), beanModel, "anexoSSq04C402", true);
        BindingsProxy.bind(panel.getAnexoSSq04C403(), beanModel, "anexoSSq04C403", true);
        BindingsProxy.bind(panel.getAnexoSSq04C404(), beanModel, "anexoSSq04C404", true);
        BindingsProxy.bind(panel.getAnexoSSq04C405(), beanModel, "anexoSSq04C405", true);
        BindingsProxy.bind(panel.getAnexoSSq04C406(), beanModel, "anexoSSq04C406", true);
        BindingsProxy.bind(panel.getAnexoSSq04C407(), beanModel, "anexoSSq04C407", true);
        BindingsProxy.bind(panel.getAnexoSSq04C1(), beanModel, "anexoSSq04C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq04C401", beanModel.getBean(), "anexoSSq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq04C402", beanModel.getBean(), "anexoSSq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq04C403", beanModel.getBean(), "anexoSSq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq04C404", beanModel.getBean(), "anexoSSq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq04C405", beanModel.getBean(), "anexoSSq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq04C406", beanModel.getBean(), "anexoSSq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq04C407", beanModel.getBean(), "anexoSSq04C1Formula");
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoSSq04C401().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq04C402().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq04C403().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq04C404().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq04C405().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq04C406().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq04C407().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq04C1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C401(), "anexoSSq04C401");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C402(), "anexoSSq04C402");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C403(), "anexoSSq04C403");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C404(), "anexoSSq04C404");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C405(), "anexoSSq04C405");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C406(), "anexoSSq04C406");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C407(), "anexoSSq04C407");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq04C1(), "anexoSSq04C1");
    }
}

