/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import pt.dgci.modelo3irs.v2015.binding.anexoss.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro04PanelExtension;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
    }
}

