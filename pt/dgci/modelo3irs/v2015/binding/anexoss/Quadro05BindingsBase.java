/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoss;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoss.Quadro05PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoSSq05C501(), beanModel, "anexoSSq05C501", true);
        BindingsProxy.bind(panel.getAnexoSSq05C502(), beanModel, "anexoSSq05C502", true);
        BindingsProxy.bind(panel.getAnexoSSq05C1(), beanModel, "anexoSSq05C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq05C501", beanModel.getBean(), "anexoSSq05C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoSSq05C502", beanModel.getBean(), "anexoSSq05C1Formula");
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoSSq05C501().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq05C502().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoSSq05C1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq05C501(), "anexoSSq05C501");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq05C502(), "anexoSSq05C502");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoSSq05C1(), "anexoSSq05C1");
    }
}

