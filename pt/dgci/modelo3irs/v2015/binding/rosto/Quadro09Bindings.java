/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro09BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro09PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.taxclient.util.Session;

public class Quadro09Bindings
extends Quadro09BindingsBase {
    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        Quadro09BindingsBase.doBindings(model, panel);
        Quadro09Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro09PanelExtension panel, Quadro09 model) {
        if (model == null) {
            return;
        }
        panel.getQ09B1OP1().setModel(new RadioButtonCustomAdapter(model, "q09B1", "1"));
        panel.getQ09B1OP2().setModel(new RadioButtonCustomAdapter(model, "q09B1", "2"));
        Quadro09Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro09 model, Quadro09PanelExtension panel) {
        panel.getQ09B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ09B1OP2().setEnabled(Session.isEditable().booleanValue());
    }
}

