/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro07PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JNIBTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getQ07C1(), beanModel, "q07C1", true);
        BindingsProxy.bind(panel.getQ07C1a(), beanModel, "q07C1a", true);
        BindingsProxy.bind(panel.getQ07C1b(), beanModel, "q07C1b", true);
        BindingsProxy.bind(panel.getQ07C01(), beanModel, "q07C01", true);
        BindingsProxy.bind(panel.getQ07C01a(), beanModel, "q07C01a", true);
        BindingsProxy.bind(panel.getQ07C02(), beanModel, "q07C02", true);
        BindingsProxy.bind(panel.getQ07C02a(), beanModel, "q07C02a", true);
        BindingsProxy.bind(panel.getQ07C03(), beanModel, "q07C03", true);
        BindingsProxy.bind(panel.getQ07C03a(), beanModel, "q07C03a", true);
        BindingsProxy.bind(panel.getQ07C04(), beanModel, "q07C04", true);
        BindingsProxy.bind(panel.getQ07C04a(), beanModel, "q07C04a", true);
        ObservableElementList.Connector tableConnectorRostoq07ET1_Linha = GlazedLists.beanConnector(Rostoq07ET1_Linha.class);
        ObservableElementList<Rostoq07ET1_Linha> tableElementListRostoq07ET1_Linha = new ObservableElementList<Rostoq07ET1_Linha>(beanModel.getBean().getRostoq07ET1(), tableConnectorRostoq07ET1_Linha);
        panel.getRostoq07ET1().setModel(new EventTableModel<Rostoq07ET1_Linha>(tableElementListRostoq07ET1_Linha, new Rostoq07ET1_LinhaAdapterFormat()));
        panel.getRostoq07ET1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getRostoq07ET1().getTableHeader().setReorderingAllowed(false);
        ObservableElementList.Connector tableConnectorRostoq07CT1_Linha = GlazedLists.beanConnector(Rostoq07CT1_Linha.class);
        ObservableElementList<Rostoq07CT1_Linha> tableElementListRostoq07CT1_Linha = new ObservableElementList<Rostoq07CT1_Linha>(beanModel.getBean().getRostoq07CT1(), tableConnectorRostoq07CT1_Linha);
        panel.getRostoq07CT1().setModel(new EventTableModel<Rostoq07CT1_Linha>(tableElementListRostoq07CT1_Linha, new Rostoq07CT1_LinhaAdapterFormat()));
        panel.getRostoq07CT1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getRostoq07CT1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getQ07D01(), beanModel, "q07D01", true);
        BindingsProxy.bind(panel.getQ07D01a(), beanModel, "q07D01a", true);
        panel.getRostoq07ET1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getRostoq07ET1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        panel.getRostoq07CT1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getRostoq07CT1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getQ07C1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C1a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C1b().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C01().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C01a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C02().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C02a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C03().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C03a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C04().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07C04a().setEnabled(Session.isEditable().booleanValue());
        panel.getRostoq07ET1().setEnabled(Session.isEditable().booleanValue());
        panel.getRostoq07CT1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07D01().setEnabled(Session.isEditable().booleanValue());
        panel.getQ07D01a().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C1(), "q07C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C1a(), "q07C1a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C1b(), "q07C1b");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C01(), "q07C01");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C01a(), "q07C01a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C02(), "q07C02");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C02a(), "q07C02a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C03(), "q07C03");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C03a(), "q07C03a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C04(), "q07C04");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07C04a(), "q07C04a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getRostoq07ET1(), "rostoq07ET1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getRostoq07CT1(), "rostoq07CT1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07D01(), "q07D01");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ07D01a(), "q07D01a");
    }
}

