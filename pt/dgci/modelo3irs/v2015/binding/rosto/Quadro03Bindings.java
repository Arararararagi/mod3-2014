/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro03BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro03PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.editor.JPercentCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JPercentCellRenderer;
import pt.opensoft.swing.renderer.RowIndexTableCellRendererWithPrefix;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.util.ListMap;

public class Quadro03Bindings
extends Quadro03BindingsBase {
    private static final String PREFIX_DG = "DG";
    private static final int TABLE_INDEX_COLUMN_WIDTH = 20;

    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        Quadro03BindingsBase.doBindings(model, panel);
        Quadro03Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro03PanelExtension panel, final Quadro03 model) {
        if (model == null) {
            return;
        }
        PropertyChangeListener lister = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent arg0) {
                List<Cat_M3V2015_RostoTitulares> cats = model.getTodosTitularesAsCatalog();
                ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
                if (listMap != null) {
                    for (Cat_M3V2015_RostoTitulares Cat_M3V2015_RostoTitulares : cats) {
                        listMap.put(Cat_M3V2015_RostoTitulares.getValue(), Cat_M3V2015_RostoTitulares);
                    }
                }
            }
        };
        panel.getRostoq03DT1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getRostoq03DT1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getRostoq03DT1().getColumnModel().getColumn(2).setCellEditor(new JPercentCellEditor());
        panel.getRostoq03DT1().getColumnModel().getColumn(2).setCellRenderer(new JPercentCellRenderer());
        panel.getRostoq03DT1().getColumnModel().getColumn(3).setCellEditor(new NifCellEditor());
        model.addPropertyChangeListener("q03C03", lister);
        model.addPropertyChangeListener("q03C04", lister);
        model.addPropertyChangeListener("q03CD1", lister);
        model.addPropertyChangeListener("q03CD2", lister);
        model.addPropertyChangeListener("q03CD3", lister);
        model.addPropertyChangeListener("q03CD4", lister);
        model.addPropertyChangeListener("q03CD5", lister);
        model.addPropertyChangeListener("q03CD6", lister);
        model.addPropertyChangeListener("q03CD7", lister);
        model.addPropertyChangeListener("q03CD8", lister);
        model.addPropertyChangeListener("q03CD9", lister);
        model.addPropertyChangeListener("q03CD10", lister);
        model.addPropertyChangeListener("q03CD11", lister);
        model.addPropertyChangeListener("q03CD12", lister);
        model.addPropertyChangeListener("q03CD13", lister);
        model.addPropertyChangeListener("q03CD14", lister);
        model.addPropertyChangeListener("q03CDD1", lister);
        model.addPropertyChangeListener("q03CDD2", lister);
        model.addPropertyChangeListener("q03CDD3", lister);
        model.addPropertyChangeListener("q03CDD4", lister);
        model.addPropertyChangeListener("q03CDD5", lister);
        model.addPropertyChangeListener("q03CDD6", lister);
        model.addPropertyChangeListener("q03CDD7", lister);
        model.addPropertyChangeListener("q03CDD8", lister);
        panel.getRostoq03DT1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH + 20);
        panel.getRostoq03DT1().getColumnModel().getColumn(0).setMinWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH + 20);
        panel.getRostoq03DT1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRendererWithPrefix("DG"));
    }

}

