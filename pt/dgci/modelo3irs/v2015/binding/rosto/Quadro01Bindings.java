/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro01BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro01PanelExtension;

public class Quadro01Bindings
extends Quadro01BindingsBase {
    public static void doBindings(Quadro01 model, Quadro01PanelExtension panel) {
        Quadro01BindingsBase.doBindings(model, panel);
        Quadro01Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro01PanelExtension panel, Quadro01 model) {
        if (model == null) {
            return;
        }
    }
}

