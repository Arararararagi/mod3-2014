/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro02BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro02PanelExtension;

public class Quadro02Bindings
extends Quadro02BindingsBase {
    public static void doBindings(Quadro02 model, Quadro02PanelExtension panel) {
        Quadro02BindingsBase.doBindings(model, panel);
        Quadro02Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro02PanelExtension panel, Quadro02 model) {
        if (model == null) {
            return;
        }
    }
}

