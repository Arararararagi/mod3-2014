/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import pt.dgci.modelo3irs.v2015.binding.rosto.QuadroInicioBindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroInicioPanelExtension;

public class QuadroInicioBindings
extends QuadroInicioBindingsBase {
    public static void doBindings(QuadroInicio model, QuadroInicioPanelExtension panel) {
        QuadroInicioBindingsBase.doBindings(model, panel);
        QuadroInicioBindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(QuadroInicioPanelExtension panel, QuadroInicio model) {
        if (model == null) {
            return;
        }
    }
}

