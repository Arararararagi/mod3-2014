/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTextField;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro01PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro01BindingsBase {
    protected static BeanAdapter<Quadro01> beanModel = null;
    protected static Quadro01PanelExtension panelExtension = null;

    public static void doBindings(Quadro01 model, Quadro01PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro01>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getQ01C01(), beanModel, "q01C01", true);
        Quadro01BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro01BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro01 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro01PanelExtension panel) {
        panel.getQ01C01().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro01PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ01C01(), "q01C01");
    }
}

