/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro05PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getQ05B1OP1(), beanModel, "q05B1", "1");
        BindingsProxy.bindChoice(panel.getQ05B1OP2(), beanModel, "q05B1", "2");
        BindingsProxy.bindChoice(panel.getQ05B1OP3(), beanModel, "q05B1", "3");
        BindingsProxy.bindChoice(panel.getQ05B1OP4(), beanModel, "q05B1", "4");
        BindingsProxy.bind(panel.getQ05C5(), beanModel, "q05C5", true);
        BindingsProxy.bind(panel.getQ05C5_1(), beanModel, "q05C5_1", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_Pais_Rosto"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_Pais_Rosto"), Session.isComboBoxEditable().booleanValue());
        BindingsProxy.bindChoice(panel.getQ05B2OP6(), beanModel, "q05B2", "6");
        BindingsProxy.bindChoice(panel.getQ05B2OP7(), beanModel, "q05B2", "7");
        BindingsProxy.bindChoice(panel.getQ05B3OP8(), beanModel, "q05B3", "8");
        BindingsProxy.bindChoice(panel.getQ05B3OP9(), beanModel, "q05B3", "9");
        BindingsProxy.bindChoice(panel.getQ05B4OP10(), beanModel, "q05B4", "10");
        BindingsProxy.bindChoice(panel.getQ05B4OP11(), beanModel, "q05B4", "11");
        BindingsProxy.bind(panel.getQ05C12(), beanModel, "q05C12", true);
        BindingsProxy.bind(panel.getQ05C13(), beanModel, "q05C13", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_Pais_RostoQ5B_cp13"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_Pais_RostoQ5B_cp13"), Session.isComboBoxEditable().booleanValue());
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getQ05B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B1OP3().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B1OP4().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05C5().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05C5_1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B2OP6().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B2OP7().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B3OP8().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B3OP9().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B4OP10().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B4OP11().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05C12().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05C13().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B1OP1(), "q05B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B1OP2(), "q05B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B1OP3(), "q05B1OP3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B1OP4(), "q05B1OP4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05C5(), "q05C5");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05C5_1(), "q05C5_1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B2OP6(), "q05B2OP6");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B2OP7(), "q05B2OP7");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B3OP8(), "q05B3OP8");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B3OP9(), "q05B3OP9");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B4OP10(), "q05B4OP10");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05B4OP11(), "q05B4OP11");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05C12(), "q05C12");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ05C13(), "q05C13");
    }
}

