/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroInicioPanelExtension;

public class QuadroInicioBindingsBase {
    protected static BeanAdapter<QuadroInicio> beanModel = null;
    protected static QuadroInicioPanelExtension panelExtension = null;

    public static void doBindings(QuadroInicio model, QuadroInicioPanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<QuadroInicio>(model, true);
        panelExtension = panel;
        QuadroInicioBindingsBase.doBindingsForOverwriteBehavior(panel);
        QuadroInicioBindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(QuadroInicio model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(QuadroInicioPanelExtension panel) {
    }

    public static void doBindingsForOverwriteBehavior(QuadroInicioPanelExtension panel) {
    }
}

