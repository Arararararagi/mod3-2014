/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro09PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro09BindingsBase {
    protected static BeanAdapter<Quadro09> beanModel = null;
    protected static Quadro09PanelExtension panelExtension = null;

    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro09>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getQ09B1OP1(), beanModel, "q09B1", "1");
        BindingsProxy.bindChoice(panel.getQ09B1OP2(), beanModel, "q09B1", "2");
        BindingsProxy.bind(panel.getQ09C03(), beanModel, "q09C03", true);
        Quadro09BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro09BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro09 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro09PanelExtension panel) {
        panel.getQ09B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ09B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getQ09C03().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro09PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ09B1OP1(), "q09B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ09B1OP2(), "q09B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ09C03(), "q09C03");
    }
}

