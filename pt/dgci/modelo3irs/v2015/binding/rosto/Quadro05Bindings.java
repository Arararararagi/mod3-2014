/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro05PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.taxclient.util.Session;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model == null) {
            return;
        }
        panel.getQ05B2OP6().setModel(new RadioButtonCustomAdapter(model, "q05B2", "6"));
        panel.getQ05B2OP7().setModel(new RadioButtonCustomAdapter(model, "q05B2", "7"));
        panel.getQ05B3OP8().setModel(new RadioButtonCustomAdapter(model, "q05B3", "8"));
        panel.getQ05B3OP9().setModel(new RadioButtonCustomAdapter(model, "q05B3", "9"));
        panel.getQ05B4OP10().setModel(new RadioButtonCustomAdapter(model, "q05B4", "10"));
        panel.getQ05B4OP11().setModel(new RadioButtonCustomAdapter(model, "q05B4", "11"));
        Quadro05Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro05 model, Quadro05PanelExtension panel) {
        panel.getQ05B2OP6().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B2OP7().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B3OP8().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B3OP9().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B4OP10().setEnabled(Session.isEditable().booleanValue());
        panel.getQ05B4OP11().setEnabled(Session.isEditable().booleanValue());
    }
}

