/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro04PanelExtension;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
    }
}

