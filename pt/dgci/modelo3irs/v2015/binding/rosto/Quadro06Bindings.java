/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro06PanelExtension;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
    }
}

