/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.rosto.Quadro07BindingsBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro07PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.RowIndexTableCellRendererWithPrefix;

public class Quadro07Bindings
extends Quadro07BindingsBase {
    private static final String PREFIX_AS = "AC";
    private static final String PREFIX_AF = "AF";
    private static final int TABLE_INDEX_COLUMN_WIDTH = 20;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        Quadro07BindingsBase.doBindings(model, panel);
        Quadro07Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro07PanelExtension panel, Quadro07 model) {
        if (model == null) {
            return;
        }
        panel.getRostoq07CT1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getRostoq07ET1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getRostoq07CT1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getRostoq07ET1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getRostoq07ET1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH + 20);
        panel.getRostoq07ET1().getColumnModel().getColumn(0).setMinWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH + 20);
        panel.getRostoq07ET1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRendererWithPrefix("AC"));
        panel.getRostoq07CT1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH + 20);
        panel.getRostoq07CT1().getColumnModel().getColumn(0).setMinWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH + 20);
        panel.getRostoq07CT1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRendererWithPrefix("AF"));
    }

    public static void doExtraBindingsForEnabled(Quadro07 model, Quadro07PanelExtension panel) {
    }
}

