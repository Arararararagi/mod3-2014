/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.rosto;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro03PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro03BindingsBase {
    protected static BeanAdapter<Quadro03> beanModel = null;
    protected static Quadro03PanelExtension panelExtension = null;

    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro03>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getQ03C01(), beanModel, "q03C01", true);
        BindingsProxy.bind(panel.getQ03C02(), beanModel, "q03C02", true);
        BindingsProxy.bind(panel.getQ03C03(), beanModel, "q03C03", true);
        BindingsProxy.bind(panel.getQ03C03a(), beanModel, "q03C03a", true);
        BindingsProxy.bind(panel.getQ03B03(), beanModel, "q03B03", true);
        BindingsProxy.bind(panel.getQ03C04(), beanModel, "q03C04", true);
        BindingsProxy.bind(panel.getQ03C04a(), beanModel, "q03C04a", true);
        BindingsProxy.bind(panel.getQ03B04(), beanModel, "q03B04", true);
        BindingsProxy.bind(panel.getQ03CD1(), beanModel, "q03CD1", true);
        BindingsProxy.bind(panel.getQ03CD2(), beanModel, "q03CD2", true);
        BindingsProxy.bind(panel.getQ03CD3(), beanModel, "q03CD3", true);
        BindingsProxy.bind(panel.getQ03CD4(), beanModel, "q03CD4", true);
        BindingsProxy.bind(panel.getQ03CD5(), beanModel, "q03CD5", true);
        BindingsProxy.bind(panel.getQ03CD6(), beanModel, "q03CD6", true);
        BindingsProxy.bind(panel.getQ03CD7(), beanModel, "q03CD7", true);
        BindingsProxy.bind(panel.getQ03CD8(), beanModel, "q03CD8", true);
        BindingsProxy.bind(panel.getQ03CD9(), beanModel, "q03CD9", true);
        BindingsProxy.bind(panel.getQ03CD10(), beanModel, "q03CD10", true);
        BindingsProxy.bind(panel.getQ03CD11(), beanModel, "q03CD11", true);
        BindingsProxy.bind(panel.getQ03CD12(), beanModel, "q03CD12", true);
        BindingsProxy.bind(panel.getQ03CD13(), beanModel, "q03CD13", true);
        BindingsProxy.bind(panel.getQ03CD14(), beanModel, "q03CD14", true);
        BindingsProxy.bind(panel.getQ03CDD1(), beanModel, "q03CDD1", true);
        BindingsProxy.bind(panel.getQ03CDD1a(), beanModel, "q03CDD1a", true);
        BindingsProxy.bind(panel.getQ03CDD2(), beanModel, "q03CDD2", true);
        BindingsProxy.bind(panel.getQ03CDD2a(), beanModel, "q03CDD2a", true);
        BindingsProxy.bind(panel.getQ03CDD3(), beanModel, "q03CDD3", true);
        BindingsProxy.bind(panel.getQ03CDD3a(), beanModel, "q03CDD3a", true);
        BindingsProxy.bind(panel.getQ03CDD4(), beanModel, "q03CDD4", true);
        BindingsProxy.bind(panel.getQ03CDD4a(), beanModel, "q03CDD4a", true);
        BindingsProxy.bind(panel.getQ03CDD5(), beanModel, "q03CDD5", true);
        BindingsProxy.bind(panel.getQ03CDD5a(), beanModel, "q03CDD5a", true);
        BindingsProxy.bind(panel.getQ03CDD6(), beanModel, "q03CDD6", true);
        BindingsProxy.bind(panel.getQ03CDD6a(), beanModel, "q03CDD6a", true);
        BindingsProxy.bind(panel.getQ03CDD7(), beanModel, "q03CDD7", true);
        BindingsProxy.bind(panel.getQ03CDD7a(), beanModel, "q03CDD7a", true);
        BindingsProxy.bind(panel.getQ03CDD8(), beanModel, "q03CDD8", true);
        BindingsProxy.bind(panel.getQ03CDD8a(), beanModel, "q03CDD8a", true);
        ObservableElementList.Connector tableConnectorRostoq03DT1_Linha = GlazedLists.beanConnector(Rostoq03DT1_Linha.class);
        ObservableElementList<Rostoq03DT1_Linha> tableElementListRostoq03DT1_Linha = new ObservableElementList<Rostoq03DT1_Linha>(beanModel.getBean().getRostoq03DT1(), tableConnectorRostoq03DT1_Linha);
        panel.getRostoq03DT1().setModel(new EventTableModel<Rostoq03DT1_Linha>(tableElementListRostoq03DT1_Linha, new Rostoq03DT1_LinhaAdapterFormat()));
        panel.getRostoq03DT1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getRostoq03DT1().getTableHeader().setReorderingAllowed(false);
        panel.getRostoq03DT1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getRostoq03DT1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro03BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro03BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro03 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro03PanelExtension panel) {
        panel.getQ03C01().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03C02().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03C03().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03C03a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03B03().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03C04().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03C04a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03B04().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD2().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD3().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD4().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD5().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD6().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD7().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD8().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD9().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD10().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD11().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD12().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD13().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CD14().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD1().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD1a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD2().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD2a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD3().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD3a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD4().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD4a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD5().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD5a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD6().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD6a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD7().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD7a().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD8().setEnabled(Session.isEditable().booleanValue());
        panel.getQ03CDD8a().setEnabled(Session.isEditable().booleanValue());
        panel.getRostoq03DT1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro03PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03C01(), "q03C01");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03C02(), "q03C02");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03C03(), "q03C03");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03C03a(), "q03C03a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03B03(), "q03B03");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03C04(), "q03C04");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03C04a(), "q03C04a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03B04(), "q03B04");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD1(), "q03CD1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD2(), "q03CD2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD3(), "q03CD3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD4(), "q03CD4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD5(), "q03CD5");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD6(), "q03CD6");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD7(), "q03CD7");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD8(), "q03CD8");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD9(), "q03CD9");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD10(), "q03CD10");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD11(), "q03CD11");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD12(), "q03CD12");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD13(), "q03CD13");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CD14(), "q03CD14");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD1(), "q03CDD1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD1a(), "q03CDD1a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD2(), "q03CDD2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD2a(), "q03CDD2a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD3(), "q03CDD3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD3a(), "q03CDD3a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD4(), "q03CDD4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD4a(), "q03CDD4a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD5(), "q03CDD5");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD5a(), "q03CDD5a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD6(), "q03CDD6");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD6a(), "q03CDD6a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD7(), "q03CDD7");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD7a(), "q03CDD7a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD8(), "q03CDD8");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getQ03CDD8a(), "q03CDD8a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getRostoq03DT1(), "rostoq03DT1");
    }
}

