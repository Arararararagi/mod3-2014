/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoa;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoa.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.DateCellEditor;
import pt.opensoft.swing.renderer.DateCellRenderer;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoAq04T4A_Linha = GlazedLists.beanConnector(AnexoAq04T4A_Linha.class);
        ObservableElementList<AnexoAq04T4A_Linha> tableElementListAnexoAq04T4A_Linha = new ObservableElementList<AnexoAq04T4A_Linha>(beanModel.getBean().getAnexoAq04T4A(), tableConnectorAnexoAq04T4A_Linha);
        panel.getAnexoAq04T4A().setModel(new EventTableModel<AnexoAq04T4A_Linha>(tableElementListAnexoAq04T4A_Linha, new AnexoAq04T4A_LinhaAdapterFormat()));
        panel.getAnexoAq04T4A().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoAq04T4A().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoAq04C1(), beanModel, "anexoAq04C1", true);
        beanModel.getBean().getAnexoAq04T4A().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoAq04C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoAq04C2(), beanModel, "anexoAq04C2", true);
        beanModel.getBean().getAnexoAq04T4A().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoAq04C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoAq04C3(), beanModel, "anexoAq04C3", true);
        beanModel.getBean().getAnexoAq04T4A().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoAq04C3Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoAq04C4(), beanModel, "anexoAq04C4", true);
        beanModel.getBean().getAnexoAq04T4A().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoAq04C4Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        ObservableElementList.Connector tableConnectorAnexoAq04T4B_Linha = GlazedLists.beanConnector(AnexoAq04T4B_Linha.class);
        ObservableElementList<AnexoAq04T4B_Linha> tableElementListAnexoAq04T4B_Linha = new ObservableElementList<AnexoAq04T4B_Linha>(beanModel.getBean().getAnexoAq04T4B(), tableConnectorAnexoAq04T4B_Linha);
        panel.getAnexoAq04T4B().setModel(new EventTableModel<AnexoAq04T4B_Linha>(tableElementListAnexoAq04T4B_Linha, new AnexoAq04T4B_LinhaAdapterFormat()));
        panel.getAnexoAq04T4B().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoAq04T4B().getTableHeader().setReorderingAllowed(false);
        ObservableElementList.Connector tableConnectorAnexoAq04T4Ba_Linha = GlazedLists.beanConnector(AnexoAq04T4Ba_Linha.class);
        ObservableElementList<AnexoAq04T4Ba_Linha> tableElementListAnexoAq04T4Ba_Linha = new ObservableElementList<AnexoAq04T4Ba_Linha>(beanModel.getBean().getAnexoAq04T4Ba(), tableConnectorAnexoAq04T4Ba_Linha);
        panel.getAnexoAq04T4Ba().setModel(new EventTableModel<AnexoAq04T4Ba_Linha>(tableElementListAnexoAq04T4Ba_Linha, new AnexoAq04T4Ba_LinhaAdapterFormat()));
        panel.getAnexoAq04T4Ba().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoAq04T4Ba().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoAq04T4A().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq04T4A(), "Cat_M3V2015_AnexoAQ4A", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq04T4A(), "Cat_M3V2015_RostoTitularesComDepEFalecidos", 3);
        panel.getAnexoAq04T4A().getColumnModel().getColumn(8).setCellEditor(new DateCellEditor());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(8).setCellRenderer(new DateCellRenderer());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(9).setCellEditor(new DateCellEditor());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(9).setCellRenderer(new DateCellRenderer());
        panel.getAnexoAq04T4B().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoAq04T4B().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq04T4B(), "Cat_M3V2015_Despesas", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq04T4B(), "Cat_M3V2015_RostoTitularesComDepEFalecidos", 2);
        panel.getAnexoAq04T4Ba().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoAq04T4Ba().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq04T4Ba(), "Cat_M3V2015_AnexoAQ4BA", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq04T4Ba(), "Cat_M3V2015_RostoTitularesComDepEFalecidos", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq04T4Ba(), "Cat_M3V2015_Pais_Rosto", 5);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoAq04T4A().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoAq04C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoAq04C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoAq04C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoAq04C4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoAq04T4B().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoAq04T4Ba().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq04T4A(), "anexoAq04T4A");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq04C1(), "anexoAq04C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq04C2(), "anexoAq04C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq04C3(), "anexoAq04C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq04C4(), "anexoAq04C4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq04T4B(), "anexoAq04T4B");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq04T4Ba(), "anexoAq04T4Ba");
    }

}

