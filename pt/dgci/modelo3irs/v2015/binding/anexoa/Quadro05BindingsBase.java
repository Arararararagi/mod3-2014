/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoa;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoa.Quadro05PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoAq05T5_Linha = GlazedLists.beanConnector(AnexoAq05T5_Linha.class);
        ObservableElementList<AnexoAq05T5_Linha> tableElementListAnexoAq05T5_Linha = new ObservableElementList<AnexoAq05T5_Linha>(beanModel.getBean().getAnexoAq05T5(), tableConnectorAnexoAq05T5_Linha);
        panel.getAnexoAq05T5().setModel(new EventTableModel<AnexoAq05T5_Linha>(tableElementListAnexoAq05T5_Linha, new AnexoAq05T5_LinhaAdapterFormat()));
        panel.getAnexoAq05T5().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoAq05T5().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoAq05T5().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoAq05T5().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq05T5(), "Cat_M3V2015_AnexoAQ5", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoAq05T5(), "Cat_M3V2015_RostoTitularesComDepEFalecidos", 3);
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoAq05T5().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoAq05T5(), "anexoAq05T5");
    }
}

