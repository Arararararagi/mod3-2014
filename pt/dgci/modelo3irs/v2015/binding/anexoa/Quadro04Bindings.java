/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoa;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoa.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComDepEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexoa.Quadro04PanelExtension;
import pt.opensoft.swing.editor.JDateTextFieldCellEditor;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoAq04T4A().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoAq04T4A().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoAq04T4A().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(3).setCellEditor(new TitularesComDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoAq04T4A().getColumnModel().getColumn(4).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(4).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(5).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(5).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(6).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(6).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(7).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(7).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(8).setCellEditor(new JDateTextFieldCellEditor());
        panel.getAnexoAq04T4A().getColumnModel().getColumn(9).setCellEditor(new JDateTextFieldCellEditor());
        panel.getAnexoAq04T4B().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoAq04T4B().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoAq04T4B().getColumnModel().getColumn(2).setCellEditor(new TitularesComDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoAq04T4B().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoAq04T4B().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoAq04T4Ba().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoAq04T4Ba().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoAq04T4Ba().getColumnModel().getColumn(2).setCellEditor(new TitularesComDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoAq04T4Ba().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoAq04T4Ba().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoAq04T4Ba().getColumnModel().getColumn(4).setCellEditor(new NifCellEditor());
        panel.getAnexoAq04T4Ba().getColumnModel().getColumn(6).setCellEditor(new LimitedTextCellEditor(13, 12, false));
        panel.getAnexoAq04T4A().getColumnModel().getColumn(0).setMaxWidth(45);
        panel.getAnexoAq04T4A().getColumnModel().getColumn(0).setWidth(45);
    }
}

