/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoFq04T1_Linha = GlazedLists.beanConnector(AnexoFq04T1_Linha.class);
        ObservableElementList<AnexoFq04T1_Linha> tableElementListAnexoFq04T1_Linha = new ObservableElementList<AnexoFq04T1_Linha>(beanModel.getBean().getAnexoFq04T1(), tableConnectorAnexoFq04T1_Linha);
        panel.getAnexoFq04T1().setModel(new EventTableModel<AnexoFq04T1_Linha>(tableElementListAnexoFq04T1_Linha, new AnexoFq04T1_LinhaAdapterFormat()));
        panel.getAnexoFq04T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoFq04T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoFq04C1(), beanModel, "anexoFq04C1", true);
        beanModel.getBean().getAnexoFq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoFq04C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoFq04C2(), beanModel, "anexoFq04C2", true);
        beanModel.getBean().getAnexoFq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoFq04C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoFq04C3(), beanModel, "anexoFq04C3", true);
        beanModel.getBean().getAnexoFq04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoFq04C3Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        panel.getAnexoFq04T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoFq04T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoFq04T1(), "Cat_M3V2015_AnexoFQ4", 1);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoFq04T1(), "Cat_M3V2015_TipoPredio", 3);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoFq04T1(), "Cat_M3V2015_RostoTitularesComCDepEFalecidos", 6);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoFq04T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq04C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq04C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq04C3().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq04T1(), "anexoFq04T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq04C1(), "anexoFq04C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq04C2(), "anexoFq04C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq04C3(), "anexoFq04C3");
    }

}

