/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro05PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoFq05T1_Linha = GlazedLists.beanConnector(AnexoFq05T1_Linha.class);
        ObservableElementList<AnexoFq05T1_Linha> tableElementListAnexoFq05T1_Linha = new ObservableElementList<AnexoFq05T1_Linha>(beanModel.getBean().getAnexoFq05T1(), tableConnectorAnexoFq05T1_Linha);
        panel.getAnexoFq05T1().setModel(new EventTableModel<AnexoFq05T1_Linha>(tableElementListAnexoFq05T1_Linha, new AnexoFq05T1_LinhaAdapterFormat()));
        panel.getAnexoFq05T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoFq05T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bindChoice(panel.getAnexoFq05B1OPSim(), beanModel, "anexoFq05B1OP", "S");
        BindingsProxy.bindChoice(panel.getAnexoFq05B1OPNao(), beanModel, "anexoFq05B1OP", "N");
        BindingsProxy.bindChoice(panel.getAnexoFq05B2OPSim(), beanModel, "anexoFq05B2OP", "S");
        BindingsProxy.bindChoice(panel.getAnexoFq05B2OPNao(), beanModel, "anexoFq05B2OP", "N");
        BindingsProxy.bindChoice(panel.getAnexoFq05B3OPSim(), beanModel, "anexoFq05B3OP", "S");
        BindingsProxy.bindChoice(panel.getAnexoFq05B3OPNao(), beanModel, "anexoFq05B3OP", "N");
        panel.getAnexoFq05T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoFq05T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoFq05T1(), "Cat_M3V2015_AnexoFQ4", 1);
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoFq05T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B1OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B1OPNao().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B2OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B2OPNao().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B3OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B3OPNao().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq05T1(), "anexoFq05T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq05B1OPSim(), "anexoFq05B1OPSim");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq05B1OPNao(), "anexoFq05B1OPNao");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq05B2OPSim(), "anexoFq05B2OPSim");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq05B2OPNao(), "anexoFq05B2OPNao");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq05B3OPSim(), "anexoFq05B3OPSim");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq05B3OPNao(), "anexoFq05B3OPNao");
    }
}

