/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexof.Quadro07BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro07PanelExtension;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;

public class Quadro07Bindings
extends Quadro07BindingsBase {
    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        Quadro07BindingsBase.doBindings(model, panel);
        Quadro07Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro07PanelExtension panel, Quadro07 model) {
        if (model != null) {
            panel.getAnexoFq07T1().getColumnModel().getColumn(2).setCellEditor(new JMoneyCellEditor());
            panel.getAnexoFq07T1().getColumnModel().getColumn(2).setCellRenderer(new JMoneyCellRenderer());
            panel.getAnexoFq07T1().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(1, 0));
            panel.getAnexoFq07T1().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(1, 0));
        }
    }
}

