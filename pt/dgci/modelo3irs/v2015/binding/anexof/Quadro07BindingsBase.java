/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro07PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoFq07T1_Linha = GlazedLists.beanConnector(AnexoFq07T1_Linha.class);
        ObservableElementList<AnexoFq07T1_Linha> tableElementListAnexoFq07T1_Linha = new ObservableElementList<AnexoFq07T1_Linha>(beanModel.getBean().getAnexoFq07T1(), tableConnectorAnexoFq07T1_Linha);
        panel.getAnexoFq07T1().setModel(new EventTableModel<AnexoFq07T1_Linha>(tableElementListAnexoFq07T1_Linha, new AnexoFq07T1_LinhaAdapterFormat()));
        panel.getAnexoFq07T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoFq07T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoFq07T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoFq07T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoFq07T1(), "Cat_M3V2015_AnexoFQ4", 1);
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getAnexoFq07T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq07T1(), "anexoFq07T1");
    }
}

