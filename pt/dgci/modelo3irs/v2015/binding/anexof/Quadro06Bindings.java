/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexof.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro06PanelExtension;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoFq06T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoFq06T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoFq06T1().getColumnModel().getColumn(1).setCellEditor(new TitularesComCDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoFq06T1().getColumnModel().getColumn(2).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoFq06T1().getColumnModel().getColumn(2).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoFq06T1().getColumnModel().getColumn(3).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoFq06T1().getColumnModel().getColumn(3).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoFq06T1().getColumnModel().getColumn(4).setCellEditor(new NifCellEditor());
        panel.getAnexoFq06T1().getColumnModel().getColumn(5).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoFq06T1().getColumnModel().getColumn(5).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoFq06T1().getColumnModel().getColumn(6).setCellEditor(new NifCellEditor());
    }
}

