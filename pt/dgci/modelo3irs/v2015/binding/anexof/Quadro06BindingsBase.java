/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro06PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoFq06T1_Linha = GlazedLists.beanConnector(AnexoFq06T1_Linha.class);
        ObservableElementList<AnexoFq06T1_Linha> tableElementListAnexoFq06T1_Linha = new ObservableElementList<AnexoFq06T1_Linha>(beanModel.getBean().getAnexoFq06T1(), tableConnectorAnexoFq06T1_Linha);
        panel.getAnexoFq06T1().setModel(new EventTableModel<AnexoFq06T1_Linha>(tableElementListAnexoFq06T1_Linha, new AnexoFq06T1_LinhaAdapterFormat()));
        panel.getAnexoFq06T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoFq06T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoFq06T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoFq06T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoFq06T1(), "Cat_M3V2015_RostoTitularesComCDepEFalecidos", 1);
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getAnexoFq06T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoFq06T1(), "anexoFq06T1");
    }
}

