/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.binding.anexof.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro05PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.taxclient.util.Session;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoFq05B1OPNao().setModel(new RadioButtonCustomAdapter(model, "anexoFq05B1OP", "N"));
        panel.getAnexoFq05B1OPSim().setModel(new RadioButtonCustomAdapter(model, "anexoFq05B1OP", "S"));
        panel.getAnexoFq05B2OPNao().setModel(new RadioButtonCustomAdapter(model, "anexoFq05B2OP", "N"));
        panel.getAnexoFq05B2OPSim().setModel(new RadioButtonCustomAdapter(model, "anexoFq05B2OP", "S"));
        panel.getAnexoFq05B3OPNao().setModel(new RadioButtonCustomAdapter(model, "anexoFq05B3OP", "N"));
        panel.getAnexoFq05B3OPSim().setModel(new RadioButtonCustomAdapter(model, "anexoFq05B3OP", "S"));
        Quadro05Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro05 model, Quadro05PanelExtension panel) {
        panel.getAnexoFq05B1OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B1OPNao().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B2OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B2OPNao().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B3OPSim().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoFq05B3OPNao().setEnabled(Session.isEditable().booleanValue());
    }
}

