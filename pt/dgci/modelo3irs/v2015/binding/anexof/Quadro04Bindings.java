/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexof;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexof.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComCDepEFalecidosComboCellEditor;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexof.Quadro04PanelExtension;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.JPercentCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.JPercentCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        RostoModel rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
        ListMap listMap = CatalogManager.getInstance().getCatalog(Cat_M3V2015_RostoTitulares.class.getSimpleName());
        panel.getAnexoFq04T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoFq04T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoFq04T1().getColumnModel().getColumn(2).setCellEditor(new LimitedTextCellEditor(8, 6, false));
        panel.getAnexoFq04T1().getColumnModel().getColumn(4).setCellEditor(new NumericCellEditor(5, 0));
        panel.getAnexoFq04T1().getColumnModel().getColumn(4).setCellRenderer(new NumericCellRenderer(5, 0));
        panel.getAnexoFq04T1().getColumnModel().getColumn(5).setCellEditor(new LimitedTextCellEditor(8, 7, false));
        panel.getAnexoFq04T1().getColumnModel().getColumn(6).setCellEditor(new TitularesComCDepEFalecidosComboCellEditor(rosto, (Cat_M3V2015_RostoTitulares)listMap.get("A")));
        panel.getAnexoFq04T1().getColumnModel().getColumn(7).setCellEditor(new JPercentCellEditor(2));
        panel.getAnexoFq04T1().getColumnModel().getColumn(7).setCellRenderer(new JPercentCellRenderer(2));
        panel.getAnexoFq04T1().getColumnModel().getColumn(8).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoFq04T1().getColumnModel().getColumn(8).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoFq04T1().getColumnModel().getColumn(9).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoFq04T1().getColumnModel().getColumn(9).setCellRenderer(new JMoneyCellRenderer());
        panel.getAnexoFq04T1().getColumnModel().getColumn(10).setCellEditor(new NifCellEditor());
        panel.getAnexoFq04T1().getColumnModel().getColumn(11).setCellEditor(new JMoneyCellEditor());
        panel.getAnexoFq04T1().getColumnModel().getColumn(11).setCellRenderer(new JMoneyCellRenderer());
    }
}

