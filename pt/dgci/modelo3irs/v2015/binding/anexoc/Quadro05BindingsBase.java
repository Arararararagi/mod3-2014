/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro05PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq05C501(), beanModel, "anexoCq05C501", true);
        BindingsProxy.bind(panel.getAnexoCq05C502(), beanModel, "anexoCq05C502", true);
        BindingsProxy.bind(panel.getAnexoCq05C503(), beanModel, "anexoCq05C503", true);
        BindingsProxy.bind(panel.getAnexoCq05C504(), beanModel, "anexoCq05C504", true);
        BindingsProxy.bind(panel.getAnexoCq05C505(), beanModel, "anexoCq05C505", true);
        BindingsProxy.bind(panel.getAnexoCq05C506(), beanModel, "anexoCq05C506", true);
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoCq05C501().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq05C502().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq05C503().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq05C504().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq05C505().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq05C506().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq05C501(), "anexoCq05C501");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq05C502(), "anexoCq05C502");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq05C503(), "anexoCq05C503");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq05C504(), "anexoCq05C504");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq05C505(), "anexoCq05C505");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq05C506(), "anexoCq05C506");
    }
}

