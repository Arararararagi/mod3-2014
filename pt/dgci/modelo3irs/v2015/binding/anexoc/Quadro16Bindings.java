/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro16BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro16PanelExtension;

public class Quadro16Bindings
extends Quadro16BindingsBase {
    public static void doBindings(Quadro16 model, Quadro16PanelExtension panel) {
        Quadro16BindingsBase.doBindings(model, panel);
        Quadro16Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro16PanelExtension panel, Quadro16 model) {
        if (model == null) {
            return;
        }
    }
}

