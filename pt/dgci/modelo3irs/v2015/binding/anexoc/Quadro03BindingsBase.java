/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro03PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro03BindingsBase {
    protected static BeanAdapter<Quadro03> beanModel = null;
    protected static Quadro03PanelExtension panelExtension = null;

    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro03>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq03C04(), beanModel, "anexoCq03C04", true);
        BindingsProxy.bind(panel.getAnexoCq03C05(), beanModel, "anexoCq03C05", true);
        BindingsProxy.bindChoice(panel.getAnexoCq03B1OP1(), beanModel, "anexoCq03B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoCq03B1OP2(), beanModel, "anexoCq03B1", "2");
        BindingsProxy.bind(panel.getAnexoCq03C06(), beanModel, "anexoCq03C06", true);
        BindingsProxy.bind(panel.getAnexoCq03C07(), beanModel, "anexoCq03C07", true);
        BindingsProxy.bind(panel.getAnexoCq03C08(), beanModel, "anexoCq03C08", true);
        BindingsProxy.bind(panel.getAnexoCq03C09(), beanModel, "anexoCq03C09", true);
        BindingsProxy.bind(panel.getAnexoCq03C10(), beanModel, "anexoCq03C10", true);
        BindingsProxy.bindChoice(panel.getAnexoCq03B2OP11(), beanModel, "anexoCq03B2", "11");
        BindingsProxy.bindChoice(panel.getAnexoCq03B2OP12(), beanModel, "anexoCq03B2", "12");
        Quadro03BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro03BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro03 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro03PanelExtension panel) {
        panel.getAnexoCq03C04().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03C05().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03C06().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03C07().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03C08().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03C09().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03C10().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03B2OP11().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq03B2OP12().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro03PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03C04(), "anexoCq03C04");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03C05(), "anexoCq03C05");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03B1OP1(), "anexoCq03B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03B1OP2(), "anexoCq03B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03C06(), "anexoCq03C06");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03C07(), "anexoCq03C07");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03C08(), "anexoCq03C08");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03C09(), "anexoCq03C09");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03C10(), "anexoCq03C10");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03B2OP11(), "anexoCq03B2OP11");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq03B2OP12(), "anexoCq03B2OP12");
    }
}

