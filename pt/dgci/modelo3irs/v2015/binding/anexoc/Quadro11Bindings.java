/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro11BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro11PanelExtension;

public class Quadro11Bindings
extends Quadro11BindingsBase {
    public static void doBindings(Quadro11 model, Quadro11PanelExtension panel) {
        Quadro11BindingsBase.doBindings(model, panel);
        Quadro11Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro11PanelExtension panel, Quadro11 model) {
        if (model == null) {
            return;
        }
    }
}

