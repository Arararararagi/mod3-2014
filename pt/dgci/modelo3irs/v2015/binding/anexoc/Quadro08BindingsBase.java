/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro08PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro08BindingsBase {
    protected static BeanAdapter<Quadro08> beanModel = null;
    protected static Quadro08PanelExtension panelExtension = null;

    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro08>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq08C801(), beanModel, "anexoCq08C801", true);
        BindingsProxy.bind(panel.getAnexoCq08C802(), beanModel, "anexoCq08C802", true);
        BindingsProxy.bind(panel.getAnexoCq08C803(), beanModel, "anexoCq08C803", true);
        BindingsProxy.bind(panel.getAnexoCq08C804(), beanModel, "anexoCq08C804", true);
        BindingsProxy.bind(panel.getAnexoCq08C805(), beanModel, "anexoCq08C805", true);
        ObservableElementList.Connector tableConnectorAnexoCq08T1_Linha = GlazedLists.beanConnector(AnexoCq08T1_Linha.class);
        ObservableElementList<AnexoCq08T1_Linha> tableElementListAnexoCq08T1_Linha = new ObservableElementList<AnexoCq08T1_Linha>(beanModel.getBean().getAnexoCq08T1(), tableConnectorAnexoCq08T1_Linha);
        panel.getAnexoCq08T1().setModel(new EventTableModel<AnexoCq08T1_Linha>(tableElementListAnexoCq08T1_Linha, new AnexoCq08T1_LinhaAdapterFormat()));
        panel.getAnexoCq08T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoCq08T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoCq08T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoCq08T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro08BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro08BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro08 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro08PanelExtension panel) {
        panel.getAnexoCq08C801().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq08C802().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq08C803().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq08C804().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq08C805().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq08T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro08PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq08C801(), "anexoCq08C801");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq08C802(), "anexoCq08C802");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq08C803(), "anexoCq08C803");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq08C804(), "anexoCq08C804");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq08C805(), "anexoCq08C805");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq08T1(), "anexoCq08T1");
    }
}

