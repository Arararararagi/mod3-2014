/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro15PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro15BindingsBase {
    protected static BeanAdapter<Quadro15> beanModel = null;
    protected static Quadro15PanelExtension panelExtension = null;

    public static void doBindings(Quadro15 model, Quadro15PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro15>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq15C1501(), beanModel, "anexoCq15C1501", true);
        BindingsProxy.bind(panel.getAnexoCq15C1502(), beanModel, "anexoCq15C1502", true);
        Quadro15BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro15BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro15 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro15PanelExtension panel) {
        panel.getAnexoCq15C1501().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq15C1502().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro15PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq15C1501(), "anexoCq15C1501");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq15C1502(), "anexoCq15C1502");
    }
}

