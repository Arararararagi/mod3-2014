/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro11PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro11BindingsBase {
    protected static BeanAdapter<Quadro11> beanModel = null;
    protected static Quadro11PanelExtension panelExtension = null;

    public static void doBindings(Quadro11 model, Quadro11PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro11>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq11C1101(), beanModel, "anexoCq11C1101", true);
        Quadro11BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro11BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro11 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro11PanelExtension panel) {
        panel.getAnexoCq11C1101().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro11PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq11C1101(), "anexoCq11C1101");
    }
}

