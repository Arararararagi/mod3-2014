/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro10PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro10BindingsBase {
    protected static BeanAdapter<Quadro10> beanModel = null;
    protected static Quadro10PanelExtension panelExtension = null;

    public static void doBindings(Quadro10 model, Quadro10PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro10>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq10C1001(), beanModel, "anexoCq10C1001", true);
        BindingsProxy.bind(panel.getAnexoCq10C1002(), beanModel, "anexoCq10C1002", true);
        BindingsProxy.bind(panel.getAnexoCq10C1006(), beanModel, "anexoCq10C1006", true);
        BindingsProxy.bind(panel.getAnexoCq10C1003(), beanModel, "anexoCq10C1003", true);
        BindingsProxy.bind(panel.getAnexoCq10C1007(), beanModel, "anexoCq10C1007", true);
        BindingsProxy.bind(panel.getAnexoCq10C1004(), beanModel, "anexoCq10C1004", true);
        BindingsProxy.bind(panel.getAnexoCq10C1005(), beanModel, "anexoCq10C1005", true);
        BindingsProxy.bind(panel.getAnexoCq10C1(), beanModel, "anexoCq10C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq10C1001", beanModel.getBean(), "anexoCq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq10C1002", beanModel.getBean(), "anexoCq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq10C1003", beanModel.getBean(), "anexoCq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq10C1004", beanModel.getBean(), "anexoCq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq10C1005", beanModel.getBean(), "anexoCq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq10C1006", beanModel.getBean(), "anexoCq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq10C1007", beanModel.getBean(), "anexoCq10C1Formula");
        Quadro10BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro10BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro10 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro10PanelExtension panel) {
        panel.getAnexoCq10C1001().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq10C1002().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq10C1006().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq10C1003().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq10C1007().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq10C1004().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq10C1005().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq10C1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro10PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1001(), "anexoCq10C1001");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1002(), "anexoCq10C1002");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1006(), "anexoCq10C1006");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1003(), "anexoCq10C1003");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1007(), "anexoCq10C1007");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1004(), "anexoCq10C1004");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1005(), "anexoCq10C1005");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq10C1(), "anexoCq10C1");
    }
}

