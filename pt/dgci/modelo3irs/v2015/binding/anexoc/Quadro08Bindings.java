/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro08BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro08PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;

public class Quadro08Bindings
extends Quadro08BindingsBase {
    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        Quadro08BindingsBase.doBindings(model, panel);
        Quadro08Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro08PanelExtension panel, Quadro08 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoCq08T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getAnexoCq08T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoCq08T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoCq08T1().getColumnModel().getColumn(2).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoCq08T1().getColumnModel().getColumn(2).setCellRenderer(jMoneyCellRenderer);
    }
}

