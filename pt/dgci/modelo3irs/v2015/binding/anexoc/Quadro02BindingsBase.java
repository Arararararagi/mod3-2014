/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro02;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro02PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListMap;

public class Quadro02BindingsBase {
    protected static BeanAdapter<Quadro02> beanModel = null;
    protected static Quadro02PanelExtension panelExtension = null;

    public static void doBindings(Quadro02 model, Quadro02PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro02>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq02C03(), beanModel, "anexoCq02C03", CatalogManager.getInstance().getCatalogForUI("Cat_M3V2015_Anos"), CatalogManager.getInstance().getCatalogInvalidItem("Cat_M3V2015_Anos"), Session.isComboBoxEditable().booleanValue());
        Quadro02BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro02BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro02 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro02PanelExtension panel) {
        panel.getAnexoCq02C03().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro02PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq02C03(), "anexoCq02C03");
    }
}

