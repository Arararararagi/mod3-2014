/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro14BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro14PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;

public class Quadro14Bindings
extends Quadro14BindingsBase {
    public static void doBindings(Quadro14 model, Quadro14PanelExtension panel) {
        Quadro14BindingsBase.doBindings(model, panel);
        Quadro14Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro14PanelExtension panel, Quadro14 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoCq14T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoCq14T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoCq14T1().getColumnModel().getColumn(1).setCellEditor(new LimitedTextCellEditor(8, 6, false));
        panel.getAnexoCq14T1().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(5, 0));
        panel.getAnexoCq14T1().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(5, 0));
        panel.getAnexoCq14T1().getColumnModel().getColumn(4).setCellEditor(new LimitedTextCellEditor(8, 7, false));
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoCq14T1().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoCq14T1().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer);
        JMoneyCellEditor jMoneyCellEditor2 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor2.getComponent()).setColumns(13);
        panel.getAnexoCq14T1().getColumnModel().getColumn(6).setCellEditor(jMoneyCellEditor2);
        JMoneyCellRenderer jMoneyCellRenderer2 = new JMoneyCellRenderer();
        jMoneyCellRenderer2.setColumns(13);
        panel.getAnexoCq14T1().getColumnModel().getColumn(6).setCellRenderer(jMoneyCellRenderer2);
    }
}

