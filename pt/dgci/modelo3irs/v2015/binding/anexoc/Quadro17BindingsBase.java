/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro17PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro17BindingsBase {
    protected static BeanAdapter<Quadro17> beanModel = null;
    protected static Quadro17PanelExtension panelExtension = null;

    public static void doBindings(Quadro17 model, Quadro17PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro17>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq17C1701(), beanModel, "anexoCq17C1701", true);
        Quadro17BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro17BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro17 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro17PanelExtension panel) {
        panel.getAnexoCq17C1701().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro17PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq17C1701(), "anexoCq17C1701");
    }
}

