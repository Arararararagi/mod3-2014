/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro15BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro15PanelExtension;

public class Quadro15Bindings
extends Quadro15BindingsBase {
    public static void doBindings(Quadro15 model, Quadro15PanelExtension panel) {
        Quadro15BindingsBase.doBindings(model, panel);
        Quadro15Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro15PanelExtension panel, Quadro15 model) {
        if (model == null) {
            return;
        }
    }
}

