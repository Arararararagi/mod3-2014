/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro16PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro16BindingsBase {
    protected static BeanAdapter<Quadro16> beanModel = null;
    protected static Quadro16PanelExtension panelExtension = null;

    public static void doBindings(Quadro16 model, Quadro16PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro16>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getAnexoCq16B1OP1(), beanModel, "anexoCq16B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoCq16B1OP2(), beanModel, "anexoCq16B1", "2");
        BindingsProxy.bind(panel.getAnexoCq16C3(), beanModel, "anexoCq16C3", true);
        BindingsProxy.bind(panel.getAnexoCq16B4(), beanModel, "anexoCq16B4", true);
        Quadro16BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro16BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro16 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro16PanelExtension panel) {
        panel.getAnexoCq16B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq16B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq16C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq16B4().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro16PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq16B1OP1(), "anexoCq16B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq16B1OP2(), "anexoCq16B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq16C3(), "anexoCq16C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq16B4(), "anexoCq16B4");
    }
}

