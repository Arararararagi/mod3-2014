/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro12BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro12PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;

public class Quadro12Bindings
extends Quadro12BindingsBase {
    public static void doBindings(Quadro12 model, Quadro12PanelExtension panel) {
        Quadro12BindingsBase.doBindings(model, panel);
        Quadro12Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro12PanelExtension panel, Quadro12 model) {
        if (model == null) {
            return;
        }
        JMoneyCellEditor jMoneyCellEditor2 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor2.getComponent()).setColumns(13);
        JMoneyCellRenderer jMoneyCellRenderer2 = new JMoneyCellRenderer();
        jMoneyCellRenderer2.setColumns(13);
        panel.getAnexoCq12T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoCq12T1().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getAnexoCq12T1().getColumnModel().getColumn(2).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(2).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(3).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(3).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(4).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(4).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(6).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(6).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(7).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoCq12T1().getColumnModel().getColumn(7).setCellRenderer(jMoneyCellRenderer2);
    }
}

