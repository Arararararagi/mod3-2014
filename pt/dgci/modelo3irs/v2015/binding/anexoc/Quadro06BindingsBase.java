/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro06PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq06C601(), beanModel, "anexoCq06C601", true);
        BindingsProxy.bind(panel.getAnexoCq06C602(), beanModel, "anexoCq06C602", true);
        BindingsProxy.bind(panel.getAnexoCq06C603(), beanModel, "anexoCq06C603", true);
        BindingsProxy.bind(panel.getAnexoCq06C604(), beanModel, "anexoCq06C604", true);
        BindingsProxy.bind(panel.getAnexoCq06C605(), beanModel, "anexoCq06C605", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C601", beanModel.getBean(), "anexoCq06C605Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C602", beanModel.getBean(), "anexoCq06C605Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C603", beanModel.getBean(), "anexoCq06C605Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C604", beanModel.getBean(), "anexoCq06C605Formula");
        BindingsProxy.bind(panel.getAnexoCq06C606(), beanModel, "anexoCq06C606", true);
        BindingsProxy.bind(panel.getAnexoCq06C607(), beanModel, "anexoCq06C607", true);
        BindingsProxy.bind(panel.getAnexoCq06C608(), beanModel, "anexoCq06C608", true);
        BindingsProxy.bind(panel.getAnexoCq06C609(), beanModel, "anexoCq06C609", true);
        BindingsProxy.bind(panel.getAnexoCq06C610(), beanModel, "anexoCq06C610", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C606", beanModel.getBean(), "anexoCq06C610Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C607", beanModel.getBean(), "anexoCq06C610Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C608", beanModel.getBean(), "anexoCq06C610Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C609", beanModel.getBean(), "anexoCq06C610Formula");
        BindingsProxy.bind(panel.getAnexoCq06C611(), beanModel, "anexoCq06C611", true);
        BindingsProxy.bind(panel.getAnexoCq06C612(), beanModel, "anexoCq06C612", true);
        BindingsProxy.bind(panel.getAnexoCq06C613(), beanModel, "anexoCq06C613", true);
        BindingsProxy.bind(panel.getAnexoCq06C614(), beanModel, "anexoCq06C614", true);
        BindingsProxy.bind(panel.getAnexoCq06C615(), beanModel, "anexoCq06C615", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C611", beanModel.getBean(), "anexoCq06C615Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C612", beanModel.getBean(), "anexoCq06C615Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C613", beanModel.getBean(), "anexoCq06C615Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq06C614", beanModel.getBean(), "anexoCq06C615Formula");
        BindingsProxy.bind(panel.getAnexoCq06C616(), beanModel, "anexoCq06C616", true);
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getAnexoCq06C601().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C602().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C603().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C604().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C605().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C606().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C607().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C608().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C609().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C610().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C611().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C612().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C613().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C614().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C615().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq06C616().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C601(), "anexoCq06C601");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C602(), "anexoCq06C602");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C603(), "anexoCq06C603");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C604(), "anexoCq06C604");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C605(), "anexoCq06C605");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C606(), "anexoCq06C606");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C607(), "anexoCq06C607");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C608(), "anexoCq06C608");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C609(), "anexoCq06C609");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C610(), "anexoCq06C610");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C611(), "anexoCq06C611");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C612(), "anexoCq06C612");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C613(), "anexoCq06C613");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C614(), "anexoCq06C614");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C615(), "anexoCq06C615");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq06C616(), "anexoCq06C616");
    }
}

