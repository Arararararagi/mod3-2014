/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro14PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro14BindingsBase {
    protected static BeanAdapter<Quadro14> beanModel = null;
    protected static Quadro14PanelExtension panelExtension = null;

    public static void doBindings(Quadro14 model, Quadro14PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro14>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getAnexoCq14B1OP1(), beanModel, "anexoCq14B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoCq14B1OP2(), beanModel, "anexoCq14B1", "2");
        ObservableElementList.Connector tableConnectorAnexoCq14T1_Linha = GlazedLists.beanConnector(AnexoCq14T1_Linha.class);
        ObservableElementList<AnexoCq14T1_Linha> tableElementListAnexoCq14T1_Linha = new ObservableElementList<AnexoCq14T1_Linha>(beanModel.getBean().getAnexoCq14T1(), tableConnectorAnexoCq14T1_Linha);
        panel.getAnexoCq14T1().setModel(new EventTableModel<AnexoCq14T1_Linha>(tableElementListAnexoCq14T1_Linha, new AnexoCq14T1_LinhaAdapterFormat()));
        panel.getAnexoCq14T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoCq14T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoCq14T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoCq14T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoCq14T1(), "Cat_M3V2015_TipoPredio", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoCq14T1(), "Cat_M3V2015_SimNao", 7);
        Quadro14BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro14BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro14 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro14PanelExtension panel) {
        panel.getAnexoCq14B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq14B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq14T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro14PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq14B1OP1(), "anexoCq14B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq14B1OP2(), "anexoCq14B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq14T1(), "anexoCq14T1");
    }
}

