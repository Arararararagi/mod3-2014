/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro12PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro12BindingsBase {
    protected static BeanAdapter<Quadro12> beanModel = null;
    protected static Quadro12PanelExtension panelExtension = null;

    public static void doBindings(Quadro12 model, Quadro12PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro12>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq12C1201(), beanModel, "anexoCq12C1201", true);
        BindingsProxy.bind(panel.getAnexoCq12C1210(), beanModel, "anexoCq12C1210", true);
        BindingsProxy.bind(panel.getAnexoCq12C1202(), beanModel, "anexoCq12C1202", true);
        BindingsProxy.bind(panel.getAnexoCq12C1203(), beanModel, "anexoCq12C1203", true);
        BindingsProxy.bind(panel.getAnexoCq12C1211(), beanModel, "anexoCq12C1211", true);
        BindingsProxy.bind(panel.getAnexoCq12C1204(), beanModel, "anexoCq12C1204", true);
        BindingsProxy.bind(panel.getAnexoCq12C1205(), beanModel, "anexoCq12C1205", true);
        BindingsProxy.bind(panel.getAnexoCq12C1212(), beanModel, "anexoCq12C1212", true);
        BindingsProxy.bind(panel.getAnexoCq12C1206(), beanModel, "anexoCq12C1206", true);
        BindingsProxy.bind(panel.getAnexoCq12C1207(), beanModel, "anexoCq12C1207", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1201", beanModel.getBean(), "anexoCq12C1207Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1210", beanModel.getBean(), "anexoCq12C1207Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1202", beanModel.getBean(), "anexoCq12C1207Formula");
        BindingsProxy.bind(panel.getAnexoCq12C1208(), beanModel, "anexoCq12C1208", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1203", beanModel.getBean(), "anexoCq12C1208Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1211", beanModel.getBean(), "anexoCq12C1208Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1204", beanModel.getBean(), "anexoCq12C1208Formula");
        BindingsProxy.bind(panel.getAnexoCq12C1209(), beanModel, "anexoCq12C1209", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1205", beanModel.getBean(), "anexoCq12C1209Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1212", beanModel.getBean(), "anexoCq12C1209Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoCq12C1206", beanModel.getBean(), "anexoCq12C1209Formula");
        ObservableElementList.Connector tableConnectorAnexoCq12T1_Linha = GlazedLists.beanConnector(AnexoCq12T1_Linha.class);
        ObservableElementList<AnexoCq12T1_Linha> tableElementListAnexoCq12T1_Linha = new ObservableElementList<AnexoCq12T1_Linha>(beanModel.getBean().getAnexoCq12T1(), tableConnectorAnexoCq12T1_Linha);
        panel.getAnexoCq12T1().setModel(new EventTableModel<AnexoCq12T1_Linha>(tableElementListAnexoCq12T1_Linha, new AnexoCq12T1_LinhaAdapterFormat()));
        panel.getAnexoCq12T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoCq12T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoCq12T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoCq12T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro12BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro12BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro12 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro12PanelExtension panel) {
        panel.getAnexoCq12C1201().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1210().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1202().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1203().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1211().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1204().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1205().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1212().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1206().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1207().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1208().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12C1209().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq12T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro12PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1201(), "anexoCq12C1201");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1210(), "anexoCq12C1210");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1202(), "anexoCq12C1202");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1203(), "anexoCq12C1203");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1211(), "anexoCq12C1211");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1204(), "anexoCq12C1204");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1205(), "anexoCq12C1205");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1212(), "anexoCq12C1212");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1206(), "anexoCq12C1206");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1207(), "anexoCq12C1207");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1208(), "anexoCq12C1208");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12C1209(), "anexoCq12C1209");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq12T1(), "anexoCq12T1");
    }
}

