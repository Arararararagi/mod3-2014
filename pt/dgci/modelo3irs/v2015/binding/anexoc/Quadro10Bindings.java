/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro10BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro10PanelExtension;

public class Quadro10Bindings
extends Quadro10BindingsBase {
    public static void doBindings(Quadro10 model, Quadro10PanelExtension panel) {
        Quadro10BindingsBase.doBindings(model, panel);
        Quadro10Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro10PanelExtension panel, Quadro10 model) {
        if (model == null) {
            return;
        }
    }
}

