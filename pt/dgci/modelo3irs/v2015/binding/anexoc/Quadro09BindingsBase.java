/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro09PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro09BindingsBase {
    protected static BeanAdapter<Quadro09> beanModel = null;
    protected static Quadro09PanelExtension panelExtension = null;

    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro09>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq09C901(), beanModel, "anexoCq09C901", true);
        BindingsProxy.bind(panel.getAnexoCq09C902(), beanModel, "anexoCq09C902", true);
        BindingsProxy.bind(panel.getAnexoCq09C903(), beanModel, "anexoCq09C903", true);
        BindingsProxy.bind(panel.getAnexoCq09C904(), beanModel, "anexoCq09C904", true);
        BindingsProxy.bind(panel.getAnexoCq09C905(), beanModel, "anexoCq09C905", true);
        BindingsProxy.bind(panel.getAnexoCq09C906(), beanModel, "anexoCq09C906", true);
        BindingsProxy.bind(panel.getAnexoCq09C907(), beanModel, "anexoCq09C907", true);
        BindingsProxy.bind(panel.getAnexoCq09C908(), beanModel, "anexoCq09C908", true);
        BindingsProxy.bind(panel.getAnexoCq09C909(), beanModel, "anexoCq09C909", true);
        BindingsProxy.bind(panel.getAnexoCq09C910(), beanModel, "anexoCq09C910", true);
        BindingsProxy.bind(panel.getAnexoCq09C911(), beanModel, "anexoCq09C911", true);
        BindingsProxy.bind(panel.getAnexoCq09C912(), beanModel, "anexoCq09C912", true);
        BindingsProxy.bind(panel.getAnexoCq09C913(), beanModel, "anexoCq09C913", true);
        BindingsProxy.bind(panel.getAnexoCq09C914(), beanModel, "anexoCq09C914", true);
        BindingsProxy.bind(panel.getAnexoCq09C915(), beanModel, "anexoCq09C915", true);
        BindingsProxy.bind(panel.getAnexoCq09C916(), beanModel, "anexoCq09C916", true);
        BindingsProxy.bind(panel.getAnexoCq09C917(), beanModel, "anexoCq09C917", true);
        BindingsProxy.bind(panel.getAnexoCq09C918(), beanModel, "anexoCq09C918", true);
        BindingsProxy.bind(panel.getAnexoCq09C919(), beanModel, "anexoCq09C919", true);
        Quadro09BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro09BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro09 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro09PanelExtension panel) {
        panel.getAnexoCq09C901().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C902().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C903().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C904().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C905().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C906().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C907().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C908().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C909().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C910().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C911().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C912().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C913().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C914().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C915().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C916().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C917().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C918().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq09C919().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro09PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C901(), "anexoCq09C901");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C902(), "anexoCq09C902");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C903(), "anexoCq09C903");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C904(), "anexoCq09C904");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C905(), "anexoCq09C905");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C906(), "anexoCq09C906");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C907(), "anexoCq09C907");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C908(), "anexoCq09C908");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C909(), "anexoCq09C909");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C910(), "anexoCq09C910");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C911(), "anexoCq09C911");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C912(), "anexoCq09C912");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C913(), "anexoCq09C913");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C914(), "anexoCq09C914");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C915(), "anexoCq09C915");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C916(), "anexoCq09C916");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C917(), "anexoCq09C917");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C918(), "anexoCq09C918");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq09C919(), "anexoCq09C919");
    }
}

