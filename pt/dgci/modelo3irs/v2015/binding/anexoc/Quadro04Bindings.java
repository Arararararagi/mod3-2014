/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import java.beans.PropertyChangeListener;
import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro04PanelExtension;
import pt.dgci.modelo3irs.v2015.ui.anexoc.UpdatePrejuizoLucroForQuadro04ActionListener;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        model.addPropertyChangeListener("anexoCq04C439", new UpdatePrejuizoLucroForQuadro04ActionListener(model));
        model.addPropertyChangeListener("anexoCq04C458", new UpdatePrejuizoLucroForQuadro04ActionListener(model));
    }
}

