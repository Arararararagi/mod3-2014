/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro13PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro13BindingsBase {
    protected static BeanAdapter<Quadro13> beanModel = null;
    protected static Quadro13PanelExtension panelExtension = null;

    public static void doBindings(Quadro13 model, Quadro13PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro13>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoCq13C1301(), beanModel, "anexoCq13C1301", true);
        BindingsProxy.bind(panel.getAnexoCq13C1302(), beanModel, "anexoCq13C1302", true);
        BindingsProxy.bind(panel.getAnexoCq13C1303(), beanModel, "anexoCq13C1303", true);
        BindingsProxy.bind(panel.getAnexoCq13C1304(), beanModel, "anexoCq13C1304", true);
        BindingsProxy.bind(panel.getAnexoCq13C1305(), beanModel, "anexoCq13C1305", true);
        BindingsProxy.bind(panel.getAnexoCq13C1306(), beanModel, "anexoCq13C1306", true);
        BindingsProxy.bind(panel.getAnexoCq13C1307(), beanModel, "anexoCq13C1307", true);
        BindingsProxy.bind(panel.getAnexoCq13C1308(), beanModel, "anexoCq13C1308", true);
        BindingsProxy.bind(panel.getAnexoCq13C1309(), beanModel, "anexoCq13C1309", true);
        BindingsProxy.bind(panel.getAnexoCq13C1310(), beanModel, "anexoCq13C1310", true);
        BindingsProxy.bind(panel.getAnexoCq13C1311(), beanModel, "anexoCq13C1311", true);
        BindingsProxy.bind(panel.getAnexoCq13C1312(), beanModel, "anexoCq13C1312", true);
        BindingsProxy.bind(panel.getAnexoCq13C1313(), beanModel, "anexoCq13C1313", true);
        BindingsProxy.bind(panel.getAnexoCq13C1314(), beanModel, "anexoCq13C1314", true);
        BindingsProxy.bind(panel.getAnexoCq13C1315(), beanModel, "anexoCq13C1315", true);
        BindingsProxy.bind(panel.getAnexoCq13C1316(), beanModel, "anexoCq13C1316", true);
        BindingsProxy.bind(panel.getAnexoCq13C1317(), beanModel, "anexoCq13C1317", true);
        BindingsProxy.bind(panel.getAnexoCq13C1318(), beanModel, "anexoCq13C1318", true);
        Quadro13BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro13BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro13 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro13PanelExtension panel) {
        panel.getAnexoCq13C1301().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1302().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1303().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1304().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1305().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1306().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1307().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1308().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1309().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1310().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1311().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1312().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1313().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1314().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1315().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1316().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1317().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoCq13C1318().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro13PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1301(), "anexoCq13C1301");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1302(), "anexoCq13C1302");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1303(), "anexoCq13C1303");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1304(), "anexoCq13C1304");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1305(), "anexoCq13C1305");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1306(), "anexoCq13C1306");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1307(), "anexoCq13C1307");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1308(), "anexoCq13C1308");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1309(), "anexoCq13C1309");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1310(), "anexoCq13C1310");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1311(), "anexoCq13C1311");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1312(), "anexoCq13C1312");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1313(), "anexoCq13C1313");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1314(), "anexoCq13C1314");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1315(), "anexoCq13C1315");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1316(), "anexoCq13C1316");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1317(), "anexoCq13C1317");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoCq13C1318(), "anexoCq13C1318");
    }
}

