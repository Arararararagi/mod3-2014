/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro07BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro07PanelExtension;

public class Quadro07Bindings
extends Quadro07BindingsBase {
    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        Quadro07BindingsBase.doBindings(model, panel);
        Quadro07Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro07PanelExtension panel, Quadro07 model) {
        if (model == null) {
            return;
        }
    }
}

