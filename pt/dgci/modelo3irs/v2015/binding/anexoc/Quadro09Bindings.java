/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro09BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro09PanelExtension;

public class Quadro09Bindings
extends Quadro09BindingsBase {
    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        Quadro09BindingsBase.doBindings(model, panel);
        Quadro09Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro09PanelExtension panel, Quadro09 model) {
        if (model == null) {
            return;
        }
    }
}

