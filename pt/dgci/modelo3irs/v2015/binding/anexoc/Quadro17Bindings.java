/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro17BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro17PanelExtension;

public class Quadro17Bindings
extends Quadro17BindingsBase {
    public static void doBindings(Quadro17 model, Quadro17PanelExtension panel) {
        Quadro17BindingsBase.doBindings(model, panel);
        Quadro17Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro17PanelExtension panel, Quadro17 model) {
        if (model == null) {
            return;
        }
    }
}

