/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexoc;

import pt.dgci.modelo3irs.v2015.binding.anexoc.Quadro13BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.ui.anexoc.Quadro13PanelExtension;

public class Quadro13Bindings
extends Quadro13BindingsBase {
    public static void doBindings(Quadro13 model, Quadro13PanelExtension panel) {
        Quadro13BindingsBase.doBindings(model, panel);
        Quadro13Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro13PanelExtension panel, Quadro13 model) {
        if (model == null) {
            return;
        }
    }
}

