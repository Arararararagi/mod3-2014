/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog1;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro05PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro05BindingsBase {
    protected static BeanAdapter<Quadro05> beanModel = null;
    protected static Quadro05PanelExtension panelExtension = null;

    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro05>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoG1q05T1_Linha = GlazedLists.beanConnector(AnexoG1q05T1_Linha.class);
        ObservableElementList<AnexoG1q05T1_Linha> tableElementListAnexoG1q05T1_Linha = new ObservableElementList<AnexoG1q05T1_Linha>(beanModel.getBean().getAnexoG1q05T1(), tableConnectorAnexoG1q05T1_Linha);
        panel.getAnexoG1q05T1().setModel(new EventTableModel<AnexoG1q05T1_Linha>(tableElementListAnexoG1q05T1_Linha, new AnexoG1q05T1_LinhaAdapterFormat()));
        panel.getAnexoG1q05T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoG1q05T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoG1q05C1(), beanModel, "anexoG1q05C1", true);
        beanModel.getBean().getAnexoG1q05T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro05BindingsBase.beanModel.getBean().setAnexoG1q05C1Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoG1q05C2(), beanModel, "anexoG1q05C2", true);
        beanModel.getBean().getAnexoG1q05T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro05BindingsBase.beanModel.getBean().setAnexoG1q05C2Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        panel.getAnexoG1q05T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoG1q05T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoG1q05T1(), "Cat_M3V2015_TipoPredio", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoG1q05T1(), "Cat_M3V2015_AnexoG1Q5", 5);
        Quadro05BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro05BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro05 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro05PanelExtension panel) {
        panel.getAnexoG1q05T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoG1q05C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoG1q05C2().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro05PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q05T1(), "anexoG1q05T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q05C1(), "anexoG1q05C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q05C2(), "anexoG1q05C2");
    }

}

