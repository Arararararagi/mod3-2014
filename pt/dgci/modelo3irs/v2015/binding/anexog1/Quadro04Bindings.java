/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog1;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog1.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro04PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoG1q04T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoG1q04T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoG1q04T1().getColumnModel().getColumn(1).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoG1q04T1().getColumnModel().getColumn(1).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoG1q04T1().getColumnModel().getColumn(2).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoG1q04T1().getColumnModel().getColumn(2).setCellRenderer(jMoneyCellRenderer);
        panel.getAnexoG1q04T1().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoG1q04T1().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoG1q04T1().getColumnModel().getColumn(4).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoG1q04T1().getColumnModel().getColumn(4).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor1 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor1.getComponent()).setColumns(13);
        panel.getAnexoG1q04T1().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor1);
        JMoneyCellRenderer jMoneyCellRenderer1 = new JMoneyCellRenderer();
        jMoneyCellRenderer1.setColumns(13);
        panel.getAnexoG1q04T1().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer1);
    }
}

