/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog1;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro03PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro03BindingsBase {
    protected static BeanAdapter<Quadro03> beanModel = null;
    protected static Quadro03PanelExtension panelExtension = null;

    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro03>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoG1q03C02(), beanModel, "anexoG1q03C02", true);
        BindingsProxy.bind(panel.getAnexoG1q03C03(), beanModel, "anexoG1q03C03", true);
        Quadro03BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro03BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro03 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro03PanelExtension panel) {
        panel.getAnexoG1q03C02().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoG1q03C03().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro03PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q03C02(), "anexoG1q03C02");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q03C03(), "anexoG1q03C03");
    }
}

