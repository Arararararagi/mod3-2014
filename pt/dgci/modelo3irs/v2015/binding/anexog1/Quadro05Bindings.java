/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog1;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexog1.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro05PanelExtension;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoG1q05T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoG1q05T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoG1q05T1().getColumnModel().getColumn(1).setCellEditor(new LimitedTextCellEditor(8, 6, false));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(5, 0));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(5, 0));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(4).setCellEditor(new LimitedTextCellEditor(8, 7, false));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(6).setCellEditor(new NumericCellEditor(4, 0));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(6).setCellRenderer(new NumericCellRenderer(4, 0));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(7).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(7).setCellRenderer(new NumericCellRenderer(2, 0));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(8).setCellEditor(new NumericCellEditor(2, 0));
        panel.getAnexoG1q05T1().getColumnModel().getColumn(8).setCellRenderer(new NumericCellRenderer(2, 0));
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoG1q05T1().getColumnModel().getColumn(9).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoG1q05T1().getColumnModel().getColumn(9).setCellRenderer(jMoneyCellRenderer);
        JMoneyCellEditor jMoneyCellEditor1 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor1.getComponent()).setColumns(13);
        panel.getAnexoG1q05T1().getColumnModel().getColumn(10).setCellEditor(jMoneyCellEditor1);
        JMoneyCellRenderer jMoneyCellRenderer1 = new JMoneyCellRenderer();
        jMoneyCellRenderer1.setColumns(13);
        panel.getAnexoG1q05T1().getColumnModel().getColumn(10).setCellRenderer(jMoneyCellRenderer1);
    }
}

