/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexog1;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexog1.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        ObservableElementList.Connector tableConnectorAnexoG1q04T1_Linha = GlazedLists.beanConnector(AnexoG1q04T1_Linha.class);
        ObservableElementList<AnexoG1q04T1_Linha> tableElementListAnexoG1q04T1_Linha = new ObservableElementList<AnexoG1q04T1_Linha>(beanModel.getBean().getAnexoG1q04T1(), tableConnectorAnexoG1q04T1_Linha);
        panel.getAnexoG1q04T1().setModel(new EventTableModel<AnexoG1q04T1_Linha>(tableElementListAnexoG1q04T1_Linha, new AnexoG1q04T1_LinhaAdapterFormat()));
        panel.getAnexoG1q04T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoG1q04T1().getTableHeader().setReorderingAllowed(false);
        BindingsProxy.bind(panel.getAnexoG1q04C401(), beanModel, "anexoG1q04C401", true);
        beanModel.getBean().getAnexoG1q04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoG1q04C401Formula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        BindingsProxy.bind(panel.getAnexoG1q04C401a(), beanModel, "anexoG1q04C401a", true);
        beanModel.getBean().getAnexoG1q04T1().addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    Quadro04BindingsBase.beanModel.getBean().setAnexoG1q04C401aFormula(null);
                }
                catch (Exception e) {
                    SimpleLog.log(e.getMessage(), e);
                }
            }
        });
        panel.getAnexoG1q04T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoG1q04T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoG1q04T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoG1q04C401().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoG1q04C401a().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q04T1(), "anexoG1q04T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q04C401(), "anexoG1q04C401");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoG1q04C401a(), "anexoG1q04C401a");
    }

}

