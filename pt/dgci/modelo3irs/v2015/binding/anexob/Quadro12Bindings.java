/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import java.beans.PropertyChangeListener;
import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.binding.anexob.EnableOrDisableCheckBoxAnexoBListener;
import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro12BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro12PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.taxclient.util.Session;

public class Quadro12Bindings
extends Quadro12BindingsBase {
    public static void doBindings(Quadro12 model, Quadro12PanelExtension panel) {
        Quadro12BindingsBase.doBindings(model, panel);
        Quadro12Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro12PanelExtension panel, Quadro12 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoBq12B1OP1().setModel(new RadioButtonCustomAdapter(model, "anexoBq12B1", "1"));
        panel.getAnexoBq12B1OP2().setModel(new RadioButtonCustomAdapter(model, "anexoBq12B1", "2"));
        EnableOrDisableCheckBoxAnexoBListener listener = new EnableOrDisableCheckBoxAnexoBListener(model.getAnexoPai().getQuadro11(), model, panel);
        model.getAnexoPai().getQuadro11().addPropertyChangeListener("anexoBq11C1101", listener);
        model.getAnexoPai().getQuadro11().addPropertyChangeListener("anexoBq11C1102", listener);
        Quadro12Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro12 model, Quadro12PanelExtension panel) {
        panel.getAnexoBq12B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq12B1OP2().setEnabled(Session.isEditable().booleanValue());
    }
}

