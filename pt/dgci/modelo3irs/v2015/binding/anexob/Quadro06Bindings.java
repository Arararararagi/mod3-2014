/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro06BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro06PanelExtension;

public class Quadro06Bindings
extends Quadro06BindingsBase {
    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        Quadro06BindingsBase.doBindings(model, panel);
        Quadro06Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro06PanelExtension panel, Quadro06 model) {
        if (model == null) {
            return;
        }
    }
}

