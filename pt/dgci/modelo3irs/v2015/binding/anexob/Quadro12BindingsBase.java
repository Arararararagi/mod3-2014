/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro12PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro12BindingsBase {
    protected static BeanAdapter<Quadro12> beanModel = null;
    protected static Quadro12PanelExtension panelExtension = null;

    public static void doBindings(Quadro12 model, Quadro12PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro12>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getAnexoBq12B1OP1(), beanModel, "anexoBq12B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoBq12B1OP2(), beanModel, "anexoBq12B1", "2");
        BindingsProxy.bind(panel.getAnexoBq12C3(), beanModel, "anexoBq12C3", true);
        BindingsProxy.bind(panel.getAnexoBq12B4(), beanModel, "anexoBq12B4", true);
        Quadro12BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro12BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro12 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro12PanelExtension panel) {
        panel.getAnexoBq12B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq12B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq12C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq12B4().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro12PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq12B1OP1(), "anexoBq12B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq12B1OP2(), "anexoBq12B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq12C3(), "anexoBq12C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq12B4(), "anexoBq12B4");
    }
}

