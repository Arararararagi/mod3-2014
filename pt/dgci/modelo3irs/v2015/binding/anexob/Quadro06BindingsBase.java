/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro06;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro06PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro06BindingsBase {
    protected static BeanAdapter<Quadro06> beanModel = null;
    protected static Quadro06PanelExtension panelExtension = null;

    public static void doBindings(Quadro06 model, Quadro06PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro06>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq06C601(), beanModel, "anexoBq06C601", true);
        BindingsProxy.bind(panel.getAnexoBq06C602(), beanModel, "anexoBq06C602", true);
        BindingsProxy.bind(panel.getAnexoBq06C1(), beanModel, "anexoBq06C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq06C601", beanModel.getBean(), "anexoBq06C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq06C602", beanModel.getBean(), "anexoBq06C1Formula");
        Quadro06BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro06BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro06 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro06PanelExtension panel) {
        panel.getAnexoBq06C601().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq06C602().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq06C1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro06PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq06C601(), "anexoBq06C601");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq06C602(), "anexoBq06C602");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq06C1(), "anexoBq06C1");
    }
}

