/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro10;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro10PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro10BindingsBase {
    protected static BeanAdapter<Quadro10> beanModel = null;
    protected static Quadro10PanelExtension panelExtension = null;

    public static void doBindings(Quadro10 model, Quadro10PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro10>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq10C1001(), beanModel, "anexoBq10C1001", true);
        BindingsProxy.bind(panel.getAnexoBq10C1002(), beanModel, "anexoBq10C1002", true);
        BindingsProxy.bind(panel.getAnexoBq10C1(), beanModel, "anexoBq10C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq10C1001", beanModel.getBean(), "anexoBq10C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq10C1002", beanModel.getBean(), "anexoBq10C1Formula");
        Quadro10BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro10BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro10 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro10PanelExtension panel) {
        panel.getAnexoBq10C1001().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq10C1002().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq10C1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro10PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq10C1001(), "anexoBq10C1001");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq10C1002(), "anexoBq10C1002");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq10C1(), "anexoBq10C1");
    }
}

