/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro05BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro05;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro05PanelExtension;

public class Quadro05Bindings
extends Quadro05BindingsBase {
    public static void doBindings(Quadro05 model, Quadro05PanelExtension panel) {
        Quadro05BindingsBase.doBindings(model, panel);
        Quadro05Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro05PanelExtension panel, Quadro05 model) {
        if (model == null) {
            return;
        }
    }
}

