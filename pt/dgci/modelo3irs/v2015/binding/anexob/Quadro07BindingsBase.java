/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro07;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro07PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro07BindingsBase {
    protected static BeanAdapter<Quadro07> beanModel = null;
    protected static Quadro07PanelExtension panelExtension = null;

    public static void doBindings(Quadro07 model, Quadro07PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro07>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq07C701(), beanModel, "anexoBq07C701", true);
        BindingsProxy.bind(panel.getAnexoBq07C702(), beanModel, "anexoBq07C702", true);
        BindingsProxy.bind(panel.getAnexoBq07C703(), beanModel, "anexoBq07C703", true);
        BindingsProxy.bind(panel.getAnexoBq07C704(), beanModel, "anexoBq07C704", true);
        ObservableElementList.Connector tableConnectorAnexoBq07T1_Linha = GlazedLists.beanConnector(AnexoBq07T1_Linha.class);
        ObservableElementList<AnexoBq07T1_Linha> tableElementListAnexoBq07T1_Linha = new ObservableElementList<AnexoBq07T1_Linha>(beanModel.getBean().getAnexoBq07T1(), tableConnectorAnexoBq07T1_Linha);
        panel.getAnexoBq07T1().setModel(new EventTableModel<AnexoBq07T1_Linha>(tableElementListAnexoBq07T1_Linha, new AnexoBq07T1_LinhaAdapterFormat()));
        panel.getAnexoBq07T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoBq07T1().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoBq07T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoBq07T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro07BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro07BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro07 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro07PanelExtension panel) {
        panel.getAnexoBq07C701().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq07C702().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq07C703().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq07C704().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq07T1().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro07PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq07C701(), "anexoBq07C701");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq07C702(), "anexoBq07C702");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq07C703(), "anexoBq07C703");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq07C704(), "anexoBq07C704");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq07T1(), "anexoBq07T1");
    }
}

