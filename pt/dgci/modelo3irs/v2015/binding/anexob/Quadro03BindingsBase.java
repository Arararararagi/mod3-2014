/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro03PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro03BindingsBase {
    protected static BeanAdapter<Quadro03> beanModel = null;
    protected static Quadro03PanelExtension panelExtension = null;

    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro03>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq03C06(), beanModel, "anexoBq03C06", true);
        BindingsProxy.bind(panel.getAnexoBq03C07(), beanModel, "anexoBq03C07", true);
        BindingsProxy.bindChoice(panel.getAnexoBq03B1OP1(), beanModel, "anexoBq03B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoBq03B1OP2(), beanModel, "anexoBq03B1", "2");
        BindingsProxy.bind(panel.getAnexoBq03C08(), beanModel, "anexoBq03C08", true);
        BindingsProxy.bind(panel.getAnexoBq03C09(), beanModel, "anexoBq03C09", true);
        BindingsProxy.bind(panel.getAnexoBq03C10(), beanModel, "anexoBq03C10", true);
        BindingsProxy.bind(panel.getAnexoBq03C11(), beanModel, "anexoBq03C11", true);
        BindingsProxy.bind(panel.getAnexoBq03C12(), beanModel, "anexoBq03C12", true);
        BindingsProxy.bindChoice(panel.getAnexoBq03B13OP13(), beanModel, "anexoBq03B13", "13");
        BindingsProxy.bindChoice(panel.getAnexoBq03B13OP14(), beanModel, "anexoBq03B13", "14");
        Quadro03BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro03BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro03 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro03PanelExtension panel) {
        panel.getAnexoBq03C06().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03C07().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03C08().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03C09().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03C10().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03C11().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03C12().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03B13OP13().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq03B13OP14().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro03PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03C06(), "anexoBq03C06");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03C07(), "anexoBq03C07");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03B1OP1(), "anexoBq03B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03B1OP2(), "anexoBq03B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03C08(), "anexoBq03C08");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03C09(), "anexoBq03C09");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03C10(), "anexoBq03C10");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03C11(), "anexoBq03C11");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03C12(), "anexoBq03C12");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03B13OP13(), "anexoBq03B13OP13");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq03B13OP14(), "anexoBq03B13OP14");
    }
}

