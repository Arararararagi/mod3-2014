/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro00;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro00PanelExtension;

public class Quadro00BindingsBase {
    protected static BeanAdapter<Quadro00> beanModel = null;
    protected static Quadro00PanelExtension panelExtension = null;

    public static void doBindings(Quadro00 model, Quadro00PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro00>(model, true);
        panelExtension = panel;
        Quadro00BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro00BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro00 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro00PanelExtension panel) {
    }

    public static void doBindingsForOverwriteBehavior(Quadro00PanelExtension panel) {
    }
}

