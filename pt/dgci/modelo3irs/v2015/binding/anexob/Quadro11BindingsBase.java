/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro11PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro11BindingsBase {
    protected static BeanAdapter<Quadro11> beanModel = null;
    protected static Quadro11PanelExtension panelExtension = null;

    public static void doBindings(Quadro11 model, Quadro11PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro11>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq11C1101(), beanModel, "anexoBq11C1101", true);
        BindingsProxy.bind(panel.getAnexoBq11C1102(), beanModel, "anexoBq11C1102", true);
        BindingsProxy.bind(panel.getAnexoBq11C1103(), beanModel, "anexoBq11C1103", true);
        BindingsProxy.bind(panel.getAnexoBq11C1104(), beanModel, "anexoBq11C1104", true);
        BindingsProxy.bind(panel.getAnexoBq11C1105(), beanModel, "anexoBq11C1105", true);
        BindingsProxy.bind(panel.getAnexoBq11C1106(), beanModel, "anexoBq11C1106", true);
        BindingsProxy.bind(panel.getAnexoBq11C1107(), beanModel, "anexoBq11C1107", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq11C1101", beanModel.getBean(), "anexoBq11C1107Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq11C1102", beanModel.getBean(), "anexoBq11C1107Formula");
        BindingsProxy.bind(panel.getAnexoBq11C1108(), beanModel, "anexoBq11C1108", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq11C1103", beanModel.getBean(), "anexoBq11C1108Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq11C1104", beanModel.getBean(), "anexoBq11C1108Formula");
        BindingsProxy.bind(panel.getAnexoBq11C1109(), beanModel, "anexoBq11C1109", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq11C1105", beanModel.getBean(), "anexoBq11C1109Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq11C1106", beanModel.getBean(), "anexoBq11C1109Formula");
        Quadro11BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro11BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro11 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro11PanelExtension panel) {
        panel.getAnexoBq11C1101().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1102().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1103().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1104().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1105().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1106().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1107().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1108().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq11C1109().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro11PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1101(), "anexoBq11C1101");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1102(), "anexoBq11C1102");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1103(), "anexoBq11C1103");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1104(), "anexoBq11C1104");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1105(), "anexoBq11C1105");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1106(), "anexoBq11C1106");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1107(), "anexoBq11C1107");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1108(), "anexoBq11C1108");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq11C1109(), "anexoBq11C1109");
    }
}

