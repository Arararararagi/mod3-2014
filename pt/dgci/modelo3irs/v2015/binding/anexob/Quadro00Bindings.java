/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro00BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro00;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro00PanelExtension;

public class Quadro00Bindings
extends Quadro00BindingsBase {
    public static void doBindings(Quadro00 model, Quadro00PanelExtension panel) {
        Quadro00BindingsBase.doBindings(model, panel);
        Quadro00Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro00PanelExtension panel, Quadro00 model) {
        if (model == null) {
            return;
        }
    }
}

