/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro08BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro08PanelExtension;

public class Quadro08Bindings
extends Quadro08BindingsBase {
    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        Quadro08BindingsBase.doBindings(model, panel);
        Quadro08Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro08PanelExtension panel, Quadro08 model) {
        if (model == null) {
            return;
        }
    }
}

