/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro01PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro01BindingsBase {
    protected static BeanAdapter<Quadro01> beanModel = null;
    protected static Quadro01PanelExtension panelExtension = null;

    public static void doBindings(Quadro01 model, Quadro01PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro01>(model, true);
        panelExtension = panel;
        BindingsProxy.bindChoice(panel.getAnexoBq01B1OP1(), beanModel, "anexoBq01B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoBq01B1OP2(), beanModel, "anexoBq01B1", "2");
        BindingsProxy.bind(panel.getAnexoBq01B3(), beanModel, "anexoBq01B3", true);
        BindingsProxy.bind(panel.getAnexoBq01B4(), beanModel, "anexoBq01B4", true);
        Quadro01BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro01BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro01 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro01PanelExtension panel) {
        panel.getAnexoBq01B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq01B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq01B3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq01B4().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro01PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq01B1OP1(), "anexoBq01B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq01B1OP2(), "anexoBq01B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq01B3(), "anexoBq01B3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq01B4(), "anexoBq01B4");
    }
}

