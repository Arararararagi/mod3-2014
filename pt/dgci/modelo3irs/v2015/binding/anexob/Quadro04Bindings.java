/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import javax.swing.ButtonModel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.dgci.modelo3irs.v2015.binding.anexob.EnableorDisableRadiosForQuadro01ActionListener;
import pt.dgci.modelo3irs.v2015.binding.anexob.EnableorDisableRadiosForQuadro04ActionListener;
import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro04BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro04PanelExtension;
import pt.opensoft.swing.bindingutil.RadioButtonCustomAdapter;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.editor.LimitedTextCellEditor;
import pt.opensoft.swing.editor.NifCellEditor;
import pt.opensoft.swing.editor.NumericCellEditor;
import pt.opensoft.swing.renderer.DefaultPFCellRenderer;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.NumericCellRenderer;
import pt.opensoft.taxclient.util.Session;

public class Quadro04Bindings
extends Quadro04BindingsBase {
    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        Quadro04BindingsBase.doBindings(model, panel);
        Quadro04Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro04PanelExtension panel, Quadro04 model) {
        if (model == null) {
            return;
        }
        panel.getAnexoBq04T1().setDefaultRenderer(String.class, new DefaultPFCellRenderer());
        panel.getAnexoBq04T1().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoBq04T1().getColumnModel().getColumn(1).setCellEditor(new LimitedTextCellEditor(8, 6, false));
        panel.getAnexoBq04T1().getColumnModel().getColumn(3).setCellEditor(new NumericCellEditor(5, 0));
        panel.getAnexoBq04T1().getColumnModel().getColumn(3).setCellRenderer(new NumericCellRenderer(5, 0));
        panel.getAnexoBq04T1().getColumnModel().getColumn(4).setCellEditor(new LimitedTextCellEditor(8, 7, false));
        JMoneyCellEditor jMoneyCellEditor = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor.getComponent()).setColumns(13);
        panel.getAnexoBq04T1().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor);
        JMoneyCellRenderer jMoneyCellRenderer = new JMoneyCellRenderer();
        jMoneyCellRenderer.setColumns(13);
        panel.getAnexoBq04T1().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer);
        JMoneyCellEditor jMoneyCellEditor2 = new JMoneyCellEditor();
        ((JMoneyTextField)jMoneyCellEditor2.getComponent()).setColumns(13);
        panel.getAnexoBq04T1().getColumnModel().getColumn(7).setCellEditor(jMoneyCellEditor2);
        JMoneyCellRenderer jMoneyCellRenderer2 = new JMoneyCellRenderer();
        jMoneyCellRenderer2.setColumns(13);
        panel.getAnexoBq04T1().getColumnModel().getColumn(7).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoBq04T3().setDefaultRenderer(Long.class, new DefaultPFCellRenderer());
        panel.getAnexoBq04T3().getColumnModel().getColumn(1).setCellEditor(new NifCellEditor());
        panel.getAnexoBq04T3().getColumnModel().getColumn(2).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(2).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(3).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(3).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(4).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(4).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(5).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(5).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(6).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(6).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(7).setCellEditor(jMoneyCellEditor2);
        panel.getAnexoBq04T3().getColumnModel().getColumn(7).setCellRenderer(jMoneyCellRenderer2);
        panel.getAnexoBq04B1OP1().setModel(new RadioButtonCustomAdapter(model, "anexoBq04B1", "1"));
        panel.getAnexoBq04B1OP2().setModel(new RadioButtonCustomAdapter(model, "anexoBq04B1", "2"));
        panel.getAnexoBq04B2OP3().setModel(new RadioButtonCustomAdapter(model, "anexoBq04B2", "3"));
        panel.getAnexoBq04B2OP4().setModel(new RadioButtonCustomAdapter(model, "anexoBq04B2", "4"));
        model.addPropertyChangeListener("anexoBq04B1", new EnableorDisableRadiosForQuadro04ActionListener(panel, model));
        model.getAnexoPai().getQuadro01().addPropertyChangeListener("anexoBq01B1", new EnableorDisableRadiosForQuadro01ActionListener(panel, model));
        Quadro04Bindings.doExtraBindingsForEnabled(model, panel);
    }

    public static void doExtraBindingsForEnabled(Quadro04 model, Quadro04PanelExtension panel) {
        panel.getAnexoBq04B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B2OP3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B2OP4().setEnabled(Session.isEditable().booleanValue());
    }
}

