/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro08PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.components.JLimitedTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro08BindingsBase {
    protected static BeanAdapter<Quadro08> beanModel = null;
    protected static Quadro08PanelExtension panelExtension = null;

    public static void doBindings(Quadro08 model, Quadro08PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro08>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq08C801(), beanModel, "anexoBq08C801", true);
        BindingsProxy.bind(panel.getAnexoBq08C802(), beanModel, "anexoBq08C802", true);
        BindingsProxy.bind(panel.getAnexoBq08C803(), beanModel, "anexoBq08C803", true);
        BindingsProxy.bind(panel.getAnexoBq08C804(), beanModel, "anexoBq08C804", true);
        BindingsProxy.bind(panel.getAnexoBq08C805(), beanModel, "anexoBq08C805", true);
        BindingsProxy.bind(panel.getAnexoBq08C806(), beanModel, "anexoBq08C806", true);
        BindingsProxy.bind(panel.getAnexoBq08C807(), beanModel, "anexoBq08C807", true);
        BindingsProxy.bind(panel.getAnexoBq08C808(), beanModel, "anexoBq08C808", true);
        BindingsProxy.bind(panel.getAnexoBq08C809(), beanModel, "anexoBq08C809", true);
        BindingsProxy.bind(panel.getAnexoBq08C810(), beanModel, "anexoBq08C810", true);
        BindingsProxy.bind(panel.getAnexoBq08C811(), beanModel, "anexoBq08C811", true);
        BindingsProxy.bind(panel.getAnexoBq08C812(), beanModel, "anexoBq08C812", true);
        BindingsProxy.bind(panel.getAnexoBq08C813(), beanModel, "anexoBq08C813", true);
        BindingsProxy.bind(panel.getAnexoBq08C814(), beanModel, "anexoBq08C814", true);
        BindingsProxy.bind(panel.getAnexoBq08C815(), beanModel, "anexoBq08C815", true);
        BindingsProxy.bind(panel.getAnexoBq08C816(), beanModel, "anexoBq08C816", true);
        BindingsProxy.bind(panel.getAnexoBq08C817(), beanModel, "anexoBq08C817", true);
        BindingsProxy.bind(panel.getAnexoBq08C818(), beanModel, "anexoBq08C818", true);
        BindingsProxy.bind(panel.getAnexoBq08C819(), beanModel, "anexoBq08C819", true);
        Quadro08BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro08BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro08 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro08PanelExtension panel) {
        panel.getAnexoBq08C801().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C802().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C803().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C804().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C805().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C806().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C807().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C808().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C809().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C810().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C811().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C812().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C813().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C814().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C815().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C816().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C817().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C818().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq08C819().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro08PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C801(), "anexoBq08C801");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C802(), "anexoBq08C802");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C803(), "anexoBq08C803");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C804(), "anexoBq08C804");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C805(), "anexoBq08C805");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C806(), "anexoBq08C806");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C807(), "anexoBq08C807");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C808(), "anexoBq08C808");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C809(), "anexoBq08C809");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C810(), "anexoBq08C810");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C811(), "anexoBq08C811");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C812(), "anexoBq08C812");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C813(), "anexoBq08C813");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C814(), "anexoBq08C814");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C815(), "anexoBq08C815");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C816(), "anexoBq08C816");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C817(), "anexoBq08C817");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C818(), "anexoBq08C818");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq08C819(), "anexoBq08C819");
    }
}

