/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JRadioButton;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro04PanelExtension;

public class EnableorDisableRadiosForQuadro01ActionListener
implements PropertyChangeListener {
    private Quadro04PanelExtension panel;
    private Quadro04 model;

    public EnableorDisableRadiosForQuadro01ActionListener(Quadro04PanelExtension panel, Quadro04 model) {
        this.panel = panel;
        this.model = model;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("2".equals(evt.getNewValue())) {
            if (this.panel.getAnexoBq04B1OP1().isSelected()) {
                this.panel.getAnexoBq04B1OP1().doClick();
            }
            if (this.panel.getAnexoBq04B1OP2().isSelected()) {
                this.panel.getAnexoBq04B1OP2().doClick();
            }
            if (this.panel.getAnexoBq04B2OP3().isSelected()) {
                this.panel.getAnexoBq04B2OP3().doClick();
            }
            if (this.panel.getAnexoBq04B2OP4().isSelected()) {
                this.panel.getAnexoBq04B2OP4().doClick();
            }
            this.panel.getAnexoBq04B1OP1().setEnabled(false);
            this.panel.getAnexoBq04B1OP2().setEnabled(false);
            this.panel.getAnexoBq04B2OP3().setEnabled(false);
            this.panel.getAnexoBq04B2OP4().setEnabled(false);
            this.model.setAnexoBq04B1(null);
            this.model.setAnexoBq04B2(null);
        } else if ("2".equals(evt.getOldValue())) {
            this.panel.getAnexoBq04B1OP1().setEnabled(true);
            this.panel.getAnexoBq04B1OP2().setEnabled(true);
            this.panel.getAnexoBq04B2OP3().setEnabled(true);
            this.panel.getAnexoBq04B2OP4().setEnabled(true);
        }
    }
}

