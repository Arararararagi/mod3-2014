/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import com.jgoodies.binding.beans.BeanAdapter;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro09;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro09PanelExtension;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro09BindingsBase {
    protected static BeanAdapter<Quadro09> beanModel = null;
    protected static Quadro09PanelExtension panelExtension = null;

    public static void doBindings(Quadro09 model, Quadro09PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro09>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq09C901(), beanModel, "anexoBq09C901", true);
        BindingsProxy.bind(panel.getAnexoBq09C902(), beanModel, "anexoBq09C902", true);
        BindingsProxy.bind(panel.getAnexoBq09C903(), beanModel, "anexoBq09C903", true);
        BindingsProxy.bind(panel.getAnexoBq09C904(), beanModel, "anexoBq09C904", true);
        BindingsProxy.bind(panel.getAnexoBq09C905(), beanModel, "anexoBq09C905", true);
        BindingsProxy.bind(panel.getAnexoBq09C906(), beanModel, "anexoBq09C906", true);
        BindingsProxy.bind(panel.getAnexoBq09C907(), beanModel, "anexoBq09C907", true);
        BindingsProxy.bind(panel.getAnexoBq09C908(), beanModel, "anexoBq09C908", true);
        BindingsProxy.bind(panel.getAnexoBq09C1a(), beanModel, "anexoBq09C1a", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C901", beanModel.getBean(), "anexoBq09C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C902", beanModel.getBean(), "anexoBq09C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C903", beanModel.getBean(), "anexoBq09C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C904", beanModel.getBean(), "anexoBq09C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C905", beanModel.getBean(), "anexoBq09C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C906", beanModel.getBean(), "anexoBq09C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C907", beanModel.getBean(), "anexoBq09C1aFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C908", beanModel.getBean(), "anexoBq09C1aFormula");
        BindingsProxy.bind(panel.getAnexoBq09C910(), beanModel, "anexoBq09C910", true);
        BindingsProxy.bind(panel.getAnexoBq09C911(), beanModel, "anexoBq09C911", true);
        BindingsProxy.bind(panel.getAnexoBq09C912(), beanModel, "anexoBq09C912", true);
        BindingsProxy.bind(panel.getAnexoBq09C913(), beanModel, "anexoBq09C913", true);
        BindingsProxy.bind(panel.getAnexoBq09C914(), beanModel, "anexoBq09C914", true);
        BindingsProxy.bind(panel.getAnexoBq09C915(), beanModel, "anexoBq09C915", true);
        BindingsProxy.bind(panel.getAnexoBq09C916(), beanModel, "anexoBq09C916", true);
        BindingsProxy.bind(panel.getAnexoBq09C917(), beanModel, "anexoBq09C917", true);
        BindingsProxy.bind(panel.getAnexoBq09C1b(), beanModel, "anexoBq09C1b", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C910", beanModel.getBean(), "anexoBq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C911", beanModel.getBean(), "anexoBq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C912", beanModel.getBean(), "anexoBq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C913", beanModel.getBean(), "anexoBq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C914", beanModel.getBean(), "anexoBq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C915", beanModel.getBean(), "anexoBq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C916", beanModel.getBean(), "anexoBq09C1bFormula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq09C917", beanModel.getBean(), "anexoBq09C1bFormula");
        Quadro09BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro09BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro09 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro09PanelExtension panel) {
        panel.getAnexoBq09C901().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C902().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C903().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C904().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C905().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C906().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C907().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C908().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C1a().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C910().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C911().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C912().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C913().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C914().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C915().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C916().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C917().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq09C1b().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro09PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C901(), "anexoBq09C901");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C902(), "anexoBq09C902");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C903(), "anexoBq09C903");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C904(), "anexoBq09C904");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C905(), "anexoBq09C905");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C906(), "anexoBq09C906");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C907(), "anexoBq09C907");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C908(), "anexoBq09C908");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C1a(), "anexoBq09C1a");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C910(), "anexoBq09C910");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C911(), "anexoBq09C911");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C912(), "anexoBq09C912");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C913(), "anexoBq09C913");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C914(), "anexoBq09C914");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C915(), "anexoBq09C915");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C916(), "anexoBq09C916");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C917(), "anexoBq09C917");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq09C1b(), "anexoBq09C1b");
    }
}

