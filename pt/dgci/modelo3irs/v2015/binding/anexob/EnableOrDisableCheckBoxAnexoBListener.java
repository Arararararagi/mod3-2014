/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JCheckBox;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro12PanelExtension;

public class EnableOrDisableCheckBoxAnexoBListener
implements PropertyChangeListener {
    private Quadro11 quadro11;
    private Quadro12 model;
    private Quadro12PanelExtension panel;

    public EnableOrDisableCheckBoxAnexoBListener(Quadro11 quadro11, Quadro12 model, Quadro12PanelExtension panel) {
        this.quadro11 = quadro11;
        this.model = model;
        this.panel = panel;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.quadro11.getAnexoBq11C1101() != null && this.quadro11.getAnexoBq11C1101() > 0 || this.quadro11.getAnexoBq11C1102() != null && this.quadro11.getAnexoBq11C1102() > 0) {
            this.panel.getAnexoBq12B4().setSelected(false);
            this.panel.getAnexoBq12B4().setEnabled(false);
            this.model.setAnexoBq12B4(null);
        } else {
            this.panel.getAnexoBq12B4().setEnabled(true);
        }
    }
}

