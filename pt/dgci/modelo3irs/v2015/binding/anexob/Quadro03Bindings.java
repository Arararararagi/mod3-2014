/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.binding.anexob.Quadro03BindingsBase;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro03PanelExtension;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Quadro03Bindings
extends Quadro03BindingsBase {
    public static void doBindings(Quadro03 model, Quadro03PanelExtension panel) {
        Quadro03BindingsBase.doBindings(model, panel);
        Quadro03Bindings.doExtraBindings(panel, model);
    }

    public static void doExtraBindings(Quadro03PanelExtension panel, Quadro03 model) {
        if (model == null) {
            return;
        }
        Quadro03Bindings.initBindingConnectors(model);
        if (model.getAnexoBq03B13() == null) {
            model.setAnexoBq03B13("14");
        }
    }

    private static void initBindingConnectors(Quadro03 model) {
        if (!Modelo3IRSv2015Parameters.instance().isDpapelIRS()) {
            pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 quadro3Rosto = ((RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class)).getQuadro03();
            UnidireccionalPropertyConnector nifAConnection = new UnidireccionalPropertyConnector(quadro3Rosto, "q03C03", model, "anexoBq03C06");
            nifAConnection.updateProperty2();
            UnidireccionalPropertyConnector nifBConnection = new UnidireccionalPropertyConnector(quadro3Rosto, "q03C04", model, "anexoBq03C07");
            nifBConnection.updateProperty2();
        }
    }
}

