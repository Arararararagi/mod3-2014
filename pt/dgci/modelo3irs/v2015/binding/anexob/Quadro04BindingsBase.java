/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.binding.anexob;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.binding.beans.BeanAdapter;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_LinhaAdapterFormat;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.ui.anexob.Quadro04PanelExtension;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.binding.BindingsProxy;
import pt.opensoft.swing.binding.UnidireccionalPropertyConnector;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.util.Session;

public class Quadro04BindingsBase {
    protected static BeanAdapter<Quadro04> beanModel = null;
    protected static Quadro04PanelExtension panelExtension = null;

    public static void doBindings(Quadro04 model, Quadro04PanelExtension panel) {
        if (model == null) {
            if (beanModel == null) {
                return;
            }
            beanModel.release();
            beanModel.setBean(null);
            return;
        }
        if (panel == panelExtension && beanModel.getBean() != model) {
            beanModel.release();
            beanModel.setBean(null);
        }
        beanModel = new BeanAdapter<Quadro04>(model, true);
        panelExtension = panel;
        BindingsProxy.bind(panel.getAnexoBq04C401(), beanModel, "anexoBq04C401", true);
        BindingsProxy.bind(panel.getAnexoBq04C402(), beanModel, "anexoBq04C402", true);
        BindingsProxy.bind(panel.getAnexoBq04C403(), beanModel, "anexoBq04C403", true);
        BindingsProxy.bind(panel.getAnexoBq04C440(), beanModel, "anexoBq04C440", true);
        BindingsProxy.bind(panel.getAnexoBq04C441(), beanModel, "anexoBq04C441", true);
        BindingsProxy.bind(panel.getAnexoBq04C404(), beanModel, "anexoBq04C404", true);
        BindingsProxy.bind(panel.getAnexoBq04C442(), beanModel, "anexoBq04C442", true);
        BindingsProxy.bind(panel.getAnexoBq04C405(), beanModel, "anexoBq04C405", true);
        BindingsProxy.bind(panel.getAnexoBq04C420(), beanModel, "anexoBq04C420", true);
        BindingsProxy.bind(panel.getAnexoBq04C421(), beanModel, "anexoBq04C421", true);
        BindingsProxy.bind(panel.getAnexoBq04C422(), beanModel, "anexoBq04C422", true);
        BindingsProxy.bind(panel.getAnexoBq04C423(), beanModel, "anexoBq04C423", true);
        BindingsProxy.bind(panel.getAnexoBq04C424(), beanModel, "anexoBq04C424", true);
        BindingsProxy.bind(panel.getAnexoBq04C425(), beanModel, "anexoBq04C425", true);
        BindingsProxy.bind(panel.getAnexoBq04C443(), beanModel, "anexoBq04C443", true);
        BindingsProxy.bind(panel.getAnexoBq04C1(), beanModel, "anexoBq04C1", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C401", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C402", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C403", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C404", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C405", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C420", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C421", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C422", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C423", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C424", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C425", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C440", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C441", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C442", beanModel.getBean(), "anexoBq04C1Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C443", beanModel.getBean(), "anexoBq04C1Formula");
        BindingsProxy.bind(panel.getAnexoBq04C406(), beanModel, "anexoBq04C406", true);
        BindingsProxy.bind(panel.getAnexoBq04C407(), beanModel, "anexoBq04C407", true);
        BindingsProxy.bind(panel.getAnexoBq04C2(), beanModel, "anexoBq04C2", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C406", beanModel.getBean(), "anexoBq04C2Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C407", beanModel.getBean(), "anexoBq04C2Formula");
        BindingsProxy.bind(panel.getAnexoBq04C409(), beanModel, "anexoBq04C409", true);
        BindingsProxy.bind(panel.getAnexoBq04C410(), beanModel, "anexoBq04C410", true);
        BindingsProxy.bind(panel.getAnexoBq04C444(), beanModel, "anexoBq04C444", true);
        BindingsProxy.bind(panel.getAnexoBq04C445(), beanModel, "anexoBq04C445", true);
        BindingsProxy.bind(panel.getAnexoBq04C411(), beanModel, "anexoBq04C411", true);
        BindingsProxy.bind(panel.getAnexoBq04C426(), beanModel, "anexoBq04C426", true);
        BindingsProxy.bind(panel.getAnexoBq04C446(), beanModel, "anexoBq04C446", true);
        BindingsProxy.bind(panel.getAnexoBq04C3(), beanModel, "anexoBq04C3", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C409", beanModel.getBean(), "anexoBq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C410", beanModel.getBean(), "anexoBq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C411", beanModel.getBean(), "anexoBq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C426", beanModel.getBean(), "anexoBq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C444", beanModel.getBean(), "anexoBq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C445", beanModel.getBean(), "anexoBq04C3Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C446", beanModel.getBean(), "anexoBq04C3Formula");
        BindingsProxy.bind(panel.getAnexoBq04C413(), beanModel, "anexoBq04C413", true);
        BindingsProxy.bind(panel.getAnexoBq04C414(), beanModel, "anexoBq04C414", true);
        BindingsProxy.bind(panel.getAnexoBq04C4(), beanModel, "anexoBq04C4", true);
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C413", beanModel.getBean(), "anexoBq04C4Formula");
        UnidireccionalPropertyConnector.connect(beanModel.getBean(), "anexoBq04C414", beanModel.getBean(), "anexoBq04C4Formula");
        BindingsProxy.bindChoice(panel.getAnexoBq04B1OP1(), beanModel, "anexoBq04B1", "1");
        BindingsProxy.bindChoice(panel.getAnexoBq04B1OP2(), beanModel, "anexoBq04B1", "2");
        BindingsProxy.bindChoice(panel.getAnexoBq04B2OP3(), beanModel, "anexoBq04B2", "3");
        BindingsProxy.bindChoice(panel.getAnexoBq04B2OP4(), beanModel, "anexoBq04B2", "4");
        BindingsProxy.bindChoice(panel.getAnexoBq04B3OP1(), beanModel, "anexoBq04B3", "1");
        BindingsProxy.bindChoice(panel.getAnexoBq04B3OP2(), beanModel, "anexoBq04B3", "2");
        ObservableElementList.Connector tableConnectorAnexoBq04T1_Linha = GlazedLists.beanConnector(AnexoBq04T1_Linha.class);
        ObservableElementList<AnexoBq04T1_Linha> tableElementListAnexoBq04T1_Linha = new ObservableElementList<AnexoBq04T1_Linha>(beanModel.getBean().getAnexoBq04T1(), tableConnectorAnexoBq04T1_Linha);
        panel.getAnexoBq04T1().setModel(new EventTableModel<AnexoBq04T1_Linha>(tableElementListAnexoBq04T1_Linha, new AnexoBq04T1_LinhaAdapterFormat()));
        panel.getAnexoBq04T1().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoBq04T1().getTableHeader().setReorderingAllowed(false);
        ObservableElementList.Connector tableConnectorAnexoBq04T3_Linha = GlazedLists.beanConnector(AnexoBq04T3_Linha.class);
        ObservableElementList<AnexoBq04T3_Linha> tableElementListAnexoBq04T3_Linha = new ObservableElementList<AnexoBq04T3_Linha>(beanModel.getBean().getAnexoBq04T3(), tableConnectorAnexoBq04T3_Linha);
        panel.getAnexoBq04T3().setModel(new EventTableModel<AnexoBq04T3_Linha>(tableElementListAnexoBq04T3_Linha, new AnexoBq04T3_LinhaAdapterFormat()));
        panel.getAnexoBq04T3().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        panel.getAnexoBq04T3().getTableHeader().setReorderingAllowed(false);
        panel.getAnexoBq04T1().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoBq04T1().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoBq04T1(), "Cat_M3V2015_TipoPredio", 2);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoBq04T1(), "Cat_M3V2015_AnexoBQ4", 6);
        TableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel.getAnexoBq04T1(), "Cat_M3V2015_SimNao", 8);
        panel.getAnexoBq04T3().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        panel.getAnexoBq04T3().getColumnModel().getColumn(0).setMaxWidth(GUIParameters.TABLE_INDEX_COLUMN_WIDTH);
        Quadro04BindingsBase.doBindingsForOverwriteBehavior(panel);
        Quadro04BindingsBase.doBindingsForEnabled(panel);
    }

    public static void rebind(Quadro04 model) {
        if (beanModel != null) {
            beanModel.setBean(null);
        }
        beanModel = null;
        panelExtension.setModel(model, false);
    }

    public static void doBindingsForEnabled(Quadro04PanelExtension panel) {
        panel.getAnexoBq04C401().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C402().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C403().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C440().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C441().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C404().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C442().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C405().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C420().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C421().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C422().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C423().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C424().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C425().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C443().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C406().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C407().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C409().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C410().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C444().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C445().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C411().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C426().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C446().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C413().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C414().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04C4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B1OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B1OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B2OP3().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B2OP4().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B3OP1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04B3OP2().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04T1().setEnabled(Session.isEditable().booleanValue());
        panel.getAnexoBq04T3().setEnabled(Session.isEditable().booleanValue());
    }

    public static void doBindingsForOverwriteBehavior(Quadro04PanelExtension panel) {
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C401(), "anexoBq04C401");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C402(), "anexoBq04C402");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C403(), "anexoBq04C403");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C440(), "anexoBq04C440");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C441(), "anexoBq04C441");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C404(), "anexoBq04C404");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C442(), "anexoBq04C442");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C405(), "anexoBq04C405");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C420(), "anexoBq04C420");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C421(), "anexoBq04C421");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C422(), "anexoBq04C422");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C423(), "anexoBq04C423");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C424(), "anexoBq04C424");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C425(), "anexoBq04C425");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C443(), "anexoBq04C443");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C1(), "anexoBq04C1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C406(), "anexoBq04C406");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C407(), "anexoBq04C407");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C2(), "anexoBq04C2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C409(), "anexoBq04C409");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C410(), "anexoBq04C410");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C444(), "anexoBq04C444");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C445(), "anexoBq04C445");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C411(), "anexoBq04C411");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C426(), "anexoBq04C426");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C446(), "anexoBq04C446");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C3(), "anexoBq04C3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C413(), "anexoBq04C413");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C414(), "anexoBq04C414");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04C4(), "anexoBq04C4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04B1OP1(), "anexoBq04B1OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04B1OP2(), "anexoBq04B1OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04B2OP3(), "anexoBq04B2OP3");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04B2OP4(), "anexoBq04B2OP4");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04B3OP1(), "anexoBq04B3OP1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04B3OP2(), "anexoBq04B3OP2");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04T1(), "anexoBq04T1");
        OverwriteBehaviorManager.applyOverwriteBehavior(panel.getAnexoBq04T3(), "anexoBq04T3");
    }
}

