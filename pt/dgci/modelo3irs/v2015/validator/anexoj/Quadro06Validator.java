/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_AnexoJQ6NLinha;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxJ;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final int LINE_PAD = 1;

    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = null;
        if (rostoModel != null && rostoModel.getQuadro02() != null) {
            anoExercicio = rostoModel.getQuadro02().getQ02C02();
        }
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            long totalPaisAgentePagadorPreenchido;
            long totalPaisAgentePagador;
            long totalImpostoEstrangeiroQuadro04;
            long totalImpostoEstrangeiro;
            long totalImpostoEstrangeiroPreenchido;
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro06 quadro06Model = anexoJModel.getQuadro06();
            if (quadro06Model.getAnexoJq06T1().size() > 63) {
                result.add(new DeclValidationMessage("J188", anexoJModel.getLink("aAnexoJ.qQuadro06"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro06")}));
            }
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            TreeSet<Long> visitedCodes = new TreeSet<Long>();
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
                String instalacaoFixa = anexoJq06T1Linha.getInstalacaoFixa();
                Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
                Long montanteRendimento = anexoJq06T1Linha.getMontanteRendimento();
                Long paisDaFonteValor = anexoJq06T1Linha.getPaisDaFonteValor();
                Long paisAgentePagadorCodPais = anexoJq06T1Linha.getPaisAgentePagadorCodPais();
                Long paisAgentePagadorValor = anexoJq06T1Linha.getPaisAgentePagadorValor();
                Long nLinha = anexoJq06T1Linha.getNLinha();
                String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1.l" + (linha + 1));
                String campoQ4Link = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.CAMPOQ4.getIndex();
                String instalacaoFixaLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.INSTALACAOFIXA.getIndex();
                String identificacaoPaisCodPaisLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.IDENTIFICACAOPAISCODPAIS.getIndex();
                String montanteRendimentoLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.MONTANTERENDIMENTO.getIndex();
                String paisDaFonteValorLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.PAISDAFONTEVALOR.getIndex();
                String paisAgentePagadorCodPaisLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.PAISAGENTEPAGADORCODPAIS.getIndex();
                String paisAgentePagadorValorLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.PAISAGENTEPAGADORVALOR.getIndex();
                if (campoQ4 != null && identificacaoPaisCodPais != null) {
                    Tuple processedTuple = new Tuple(new Long[]{campoQ4, identificacaoPaisCodPais});
                    if (processedTuples.contains(processedTuple)) {
                        result.add(new DeclValidationMessage("J143", lineLink, new String[]{lineLink}));
                    } else {
                        processedTuples.add(processedTuple);
                    }
                }
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(campoQ4) && Modelo3IRSValidatorUtil.isEmptyOrZero(identificacaoPaisCodPais) && Modelo3IRSValidatorUtil.isEmptyOrZero(paisAgentePagadorCodPais) && Modelo3IRSValidatorUtil.isEmptyOrZero(montanteRendimento) && Modelo3IRSValidatorUtil.isEmptyOrZero(paisDaFonteValor) && Modelo3IRSValidatorUtil.isEmptyOrZero(paisAgentePagadorValor) && Modelo3IRSValidatorUtil.isEmptyOrZero(nLinha) && (StringUtil.isEmpty(instalacaoFixa) || instalacaoFixa.equals("N"))) {
                    result.add(new DeclValidationMessage("J144", lineLink, new String[]{lineLink}));
                }
                this.validateJ231(result, anexoJq06T1Linha, anexoJModel, linha + 1);
                this.validateJ232(result, anexoJq06T1Linha, visitedCodes, anexoJModel, linha + 1);
                this.validateJ233(result, anexoJq06T1Linha, visitedCodes, anexoJModel, linha + 1);
                this.validateJ145(result, anexoJModel, campoQ4, campoQ4Link);
                if (campoQ4 == null && (anexoJq06T1Linha.getNLinha() != null || identificacaoPaisCodPais != null || montanteRendimento != null)) {
                    result.add(new DeclValidationMessage("J216", campoQ4Link, new String[]{campoQ4Link}));
                }
                this.validateJ248(result, anexoJq06T1Linha, anoExercicio, anexoJModel, linha + 1);
                if (!(instalacaoFixa == null || StringUtil.in(instalacaoFixa, new String[]{" ", "S", "N"}))) {
                    result.add(new DeclValidationMessage("J147", instalacaoFixaLink, new String[]{instalacaoFixaLink}));
                }
                this.validateJ148(result, anexoJq06T1Linha, anexoJModel, linha);
                this.validateJ149(result, anexoJq06T1Linha, linha, anexoJModel);
                ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxJ.class.getSimpleName());
                if (!(identificacaoPaisCodPais == null || paisesCatalog.containsKey(identificacaoPaisCodPais))) {
                    result.add(new DeclValidationMessage("J150", identificacaoPaisCodPaisLink, new String[]{identificacaoPaisCodPaisLink}));
                }
                this.validateJ294(result, montanteRendimento, campoQ4, montanteRendimentoLink, campoQ4Link);
                if (campoQ4 != null && campoQ4 != 418 && paisAgentePagadorCodPais != null) {
                    result.add(new DeclValidationMessage("J154", paisAgentePagadorCodPaisLink, new String[]{paisAgentePagadorCodPaisLink}));
                }
                if (paisAgentePagadorValor != null && paisAgentePagadorValor > 0 && paisAgentePagadorCodPais == null) {
                    result.add(new DeclValidationMessage("J155", paisAgentePagadorCodPaisLink, new String[]{paisAgentePagadorCodPaisLink}));
                }
                if (paisAgentePagadorValor != null && (campoQ4 != null && campoQ4 != 418 || campoQ4 == null)) {
                    result.add(new DeclValidationMessage("J156", paisAgentePagadorValorLink, new String[]{paisAgentePagadorValorLink}));
                }
                if (paisAgentePagadorCodPais != null && paisAgentePagadorValor == null) {
                    result.add(new DeclValidationMessage("J157", paisAgentePagadorValorLink, new String[]{paisAgentePagadorValorLink}));
                }
                if (paisDaFonteValor == null || paisDaFonteValor <= 0 || paisDaFonteValor < (montanteRendimento != null ? montanteRendimento : 0)) continue;
                result.add(new DeclValidationMessage("J158", paisDaFonteValorLink, new String[]{paisDaFonteValorLink, montanteRendimentoLink}));
            }
            this.validateJ250(result, anexoJModel);
            long totalMontanteRendimentos = quadro06Model.getAnexoJq06C1() != null ? quadro06Model.getAnexoJq06C1() : 0;
            long totalMontanteRendimentosPreenchido = quadro06Model.getTotalMontanteRendimentoPreenchido();
            if (totalMontanteRendimentos != totalMontanteRendimentosPreenchido) {
                result.add(new DeclValidationMessage("J159", anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C1"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C1")}));
            }
            if ((totalImpostoEstrangeiro = quadro06Model.getAnexoJq06C2() != null ? quadro06Model.getAnexoJq06C2() : 0) != (totalImpostoEstrangeiroPreenchido = quadro06Model.getTotalImpostoEstrangeiroPreenchido())) {
                result.add(new DeclValidationMessage("J160", anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C2"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C2")}));
            }
            if ((totalPaisAgentePagador = quadro06Model.getAnexoJq06C3() != null ? quadro06Model.getAnexoJq06C3() : 0) != (totalPaisAgentePagadorPreenchido = quadro06Model.getTotalPaisAgentePagadorPreenchido())) {
                result.add(new DeclValidationMessage("J161", anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C3"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C3")}));
            }
            long l = totalImpostoEstrangeiroQuadro04 = anexoJModel.getQuadro04().getAnexoJq04C1c() != null ? anexoJModel.getQuadro04().getAnexoJq04C1c() : 0;
            if (totalImpostoEstrangeiroQuadro04 != totalImpostoEstrangeiro + totalPaisAgentePagador) {
                result.add(new DeclValidationMessage("J163", anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C1"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C2"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1c")}));
            }
            this.validateJ165(result, anexoJModel);
            this.validateJ304(result, anexoJModel);
        }
        return result;
    }

    protected void validateJ303(ValidationResult result, AnexoJModel anexoJModel) {
        boolean isNotQ04C416Preenchido;
        Long pensoesQ04C416 = anexoJModel.getQuadro04().getAnexoJq04C416b();
        boolean hasPensoesBrasil = false;
        for (AnexoJq06T1_Linha linha : anexoJModel.getQuadro06().getAnexoJq06T1()) {
            if (!new Long(76).equals(linha.getIdentificacaoPaisCodPais())) continue;
            hasPensoesBrasil = true;
            break;
        }
        String anexoJq06C4 = anexoJModel.getQuadro06().getAnexoJq06C4();
        boolean bl = isNotQ04C416Preenchido = pensoesQ04C416 == null || pensoesQ04C416 == 0;
        if (pensoesQ04C416 != null && pensoesQ04C416 > 0 && anexoJq06C4 == null && hasPensoesBrasil || isNotQ04C416Preenchido && anexoJq06C4 != null) {
            result.add(new DeclValidationMessage("J303", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416b"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416b"), anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C4OP1"), anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C4OP2")}));
        }
    }

    protected void validateJ304(ValidationResult result, AnexoJModel anexoJModel) {
        String anexoJq06C4 = anexoJModel.getQuadro06().getAnexoJq06C4();
        String opcao1 = "1";
        String opcao2 = "2";
        String opcao3 = "3";
        if (!(anexoJq06C4 == null || anexoJq06C4.equals(opcao1) || anexoJq06C4.equals(opcao2) || anexoJq06C4.equals(opcao3))) {
            result.add(new DeclValidationMessage("J304", anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C4OP1"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro06.fanexoJq06C4OP1")}));
        }
    }

    protected void validateJ148(ValidationResult result, AnexoJq06T1_Linha anexoJq06T1Linha, AnexoJModel anexoJModel, int linha) {
        String instalacaoFixa = anexoJq06T1Linha.getInstalacaoFixa();
        Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
        String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1.l" + (linha + 1));
        String campoQ4Link = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.CAMPOQ4.getIndex();
        String instalacaoFixaLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.INSTALACAOFIXA.getIndex();
        if (instalacaoFixa != null && instalacaoFixa.equals("S") && (campoQ4 == null || !Modelo3IRSValidatorUtil.in(campoQ4, new long[]{403, 404, 405, 406, 426}))) {
            result.add(new DeclValidationMessage("J148", instalacaoFixaLink, new String[]{instalacaoFixaLink, campoQ4Link}));
        }
    }

    protected void validateJ149(ValidationResult result, AnexoJq06T1_Linha anexoJq06T1Linha, int linha, AnexoJModel anexoJModel) {
        String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1.l" + (linha + 1));
        String instalacaoFixa = anexoJq06T1Linha.getInstalacaoFixa();
        Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
        String instalacaoFixaLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.INSTALACAOFIXA.getIndex();
        if (instalacaoFixa == null && campoQ4 != null && Modelo3IRSValidatorUtil.in(campoQ4, new long[]{403, 404, 405, 406, 426})) {
            result.add(new DeclValidationMessage("J149", instalacaoFixaLink, new String[]{instalacaoFixaLink}));
        }
    }

    protected void validateJ250(ValidationResult result, AnexoJModel anexoJModel) {
        if (!(anexoJModel == null || anexoJModel.getQuadro06().getAnexoJq06T1() == null || anexoJModel.getQuadro06().getAnexoJq06T1().isEmpty())) {
            EventList<AnexoJq06T1_Linha> tabelaQuadro6 = anexoJModel.getQuadro06().getAnexoJq06T1();
            for (int i = 0; i < tabelaQuadro6.size(); ++i) {
                AnexoJq06T1_Linha linha = tabelaQuadro6.get(i);
                if (linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || anexoJModel.getQuadro04().isMontanteRendimentoCampoPreenchido(linha.getCampoQ4())) continue;
                result.add(new DeclValidationMessage("J250", anexoJModel.getLink(AnexoJq06T1_Linha.getLink(i + 1, AnexoJq06T1_LinhaBase.Property.CAMPOQ4)), new String[]{anexoJModel.getLink(AnexoJq06T1_Linha.getLink(i + 1, AnexoJq06T1_LinhaBase.Property.CAMPOQ4)), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C" + linha.getCampoQ4() + "b")}));
            }
        }
    }

    protected void validateJ165(ValidationResult result, AnexoJModel anexoJModel) {
        if (anexoJModel != null && anexoJModel.getQuadro04().getDistinctNumCodigosNatureza() != (long)anexoJModel.getQuadro06().getDistinctNumCodigosNatureza()) {
            result.add(new DeclValidationMessage("J165", anexoJModel.getLink("aAnexoJ.qQuadro06")));
        }
    }

    protected void validateJ145(ValidationResult result, AnexoJModel anexoJModel, Long campoQ4, String campoQ4Link) {
        if (!(campoQ4 == null || anexoJModel.getQuadro04().hasLinhaPreenchida(campoQ4))) {
            result.add(new DeclValidationMessage("J145", campoQ4Link, new String[]{campoQ4Link}));
        }
    }

    protected void validateJ248(ValidationResult result, AnexoJq06T1_Linha anexoJq06T1Linha, Long anoExercicio, AnexoJModel anexoJModel, int numLinha) {
        Long campoQuadro4ToValdiate = 418;
        long[] codIdentificacaoPaisToValdiate = new long[]{56, 40, 442, 20, 438, 492, 674, 756, 530, 944, 945, 796, 92, 833, 531, 534};
        long[] codIdentificacaoPaisToValdiate2 = new long[]{944, 92, 833, 796};
        long ano2009 = 2009;
        long ano2011 = 2011;
        long ano2012 = 2012;
        long codigoBelgica = 56;
        long codigoAntilhasHolandesas = 530;
        if (anexoJq06T1Linha != null && anexoJq06T1Linha.getIdentificacaoPaisCodPais() != null && campoQuadro4ToValdiate.equals(anexoJq06T1Linha.getCampoQ4()) && (!Modelo3IRSValidatorUtil.in(anexoJq06T1Linha.getIdentificacaoPaisCodPais(), codIdentificacaoPaisToValdiate) || anexoJq06T1Linha.getIdentificacaoPaisCodPais().equals(codigoBelgica) && anoExercicio != null && anoExercicio > ano2009 || Modelo3IRSValidatorUtil.in(anexoJq06T1Linha.getIdentificacaoPaisCodPais(), codIdentificacaoPaisToValdiate2) && anoExercicio != null && anoExercicio > ano2011 || anexoJq06T1Linha.getIdentificacaoPaisCodPais().equals(codigoAntilhasHolandesas) && anoExercicio != null && anoExercicio > ano2012)) {
            String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1.l" + numLinha);
            String linkPais = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.IDENTIFICACAOPAISCODPAIS.getIndex();
            result.add(new DeclValidationMessage("J248", lineLink, new String[]{linkPais}));
        }
    }

    protected void validateJ215(ValidationResult result, AnexoJModel anexoJModel, AnexoJq06T1_Linha anexoJq06T1Linha, int numLinha) {
        Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
        Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
        Long montanteRendimento = anexoJq06T1Linha.getMontanteRendimento();
        String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1.l" + numLinha);
        String campoQ4Link = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.CAMPOQ4.getIndex();
        String identificacaoPaisCodPaisLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.IDENTIFICACAOPAISCODPAIS.getIndex();
        String montanteRendimentoLink = lineLink + ".c" + AnexoJq06T1_LinhaBase.Property.MONTANTERENDIMENTO.getIndex();
        if (campoQ4 != null && (anexoJq06T1Linha.getNLinha() == null || identificacaoPaisCodPais == null || montanteRendimento == null || montanteRendimento == 0)) {
            String nLinhaLink = anexoJModel.getLink(AnexoJq06T1_Linha.getLink(numLinha, AnexoJq06T1_LinhaBase.Property.NLINHA));
            result.add(new DeclValidationMessage("J215", campoQ4Link, new String[]{campoQ4Link, nLinhaLink, identificacaoPaisCodPaisLink, montanteRendimentoLink}));
        }
    }

    protected void validateJ233(ValidationResult result, AnexoJq06T1_Linha current, SortedSet<Long> visitedCodes, AnexoJModel anexoJModel, int numLinha) {
        if (current.getNLinha() != null && visitedCodes.last() > current.getNLinha()) {
            String nLinhaLink = anexoJModel.getLink(AnexoJq06T1_Linha.getLink(numLinha, AnexoJq06T1_LinhaBase.Property.NLINHA));
            result.add(new DeclValidationMessage("J233", nLinhaLink, new String[]{nLinhaLink}));
        }
    }

    public void validateJ231(ValidationResult result, AnexoJq06T1_Linha current, AnexoJModel anexoJModel, int numLinha) {
        ListMap codigos = CatalogManager.getInstance().getCatalog(Cat_M3V2015_AnexoJQ6NLinha.class.getSimpleName());
        if (current.getNLinha() != null && codigos.get(current.getNLinha()) == null) {
            String nLinhaLink = anexoJModel.getLink(AnexoJq06T1_Linha.getLink(numLinha, AnexoJq06T1_LinhaBase.Property.NLINHA));
            result.add(new DeclValidationMessage("J231", nLinhaLink, new String[]{nLinhaLink}));
        }
    }

    public void validateJ232(ValidationResult result, AnexoJq06T1_Linha current, Set<Long> visitedCodes, AnexoJModel anexoJModel, int numLinha) {
        if (!(current.getNLinha() == null || visitedCodes.add(current.getNLinha()))) {
            String nLinhaLink = anexoJModel.getLink(AnexoJq06T1_Linha.getLink(numLinha, AnexoJq06T1_LinhaBase.Property.NLINHA));
            result.add(new DeclValidationMessage("J232", nLinhaLink, new String[]{nLinhaLink}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro06 quadro06Model = anexoJModel.getQuadro06();
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                this.validateJ215(result, anexoJModel, anexoJq06T1Linha, linha + 1);
            }
            this.validateJ303(result, anexoJModel);
        }
        return result;
    }

    protected void validateJ294(ValidationResult result, Long montanteRendimento, Long campoQ4, String linkMontanteRendimento, String linkCampoQ4) {
        long[] codigosQ4ToValidate = new long[]{401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 426};
        if (montanteRendimento != null && montanteRendimento < 0 && campoQ4 != null && Modelo3IRSValidatorUtil.in(campoQ4, codigosQ4ToValidate)) {
            result.add(new DeclValidationMessage("J294", linkMontanteRendimento, new String[]{linkCampoQ4, linkMontanteRendimento}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

