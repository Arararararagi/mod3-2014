/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoj.Quadro04Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            this.validateJ052(result, rostoModel, anexoJModel);
            this.validateJ256(result, rostoModel, anexoJModel);
        }
        return result;
    }

    protected void validateJ052(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel) {
        Long periodo = rostoModel.getQuadro02().getQ02C02();
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro06 quadro06Model = anexoJModel.getQuadro06();
        Long rendimento = quadro04Model.getAnexoJq04C407b() != null ? quadro04Model.getAnexoJq04C407b() : 0;
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d() != null ? quadro04Model.getAnexoJq04C407d() : 0;
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && periodo != null && periodo >= 2006 && periodo < 2010) {
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
                Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
                long[] codIdentificacaoPaisToValdiate = new long[]{276, 40, 56, 100, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 380, 428, 440, 442, 470, 528, 616, 203, 826, 642, 752};
                if (campoQ4 == null || campoQ4 != 407 || identificacaoPaisCodPais == null || Modelo3IRSValidatorUtil.in(identificacaoPaisCodPais, codIdentificacaoPaisToValdiate) || (double)impostoPortugal.longValue() <= (double)rendimento.longValue() * 0.205) continue;
                result.add(new DeclValidationMessage("J052", impostoPortugalLink, new String[]{impostoPortugalLink}));
            }
        }
    }

    protected void validateJ256(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel) {
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro06 quadro06Model = anexoJModel.getQuadro06();
        Long rendimento = quadro04Model.getAnexoJq04C407b() != null ? quadro04Model.getAnexoJq04C407b() : 0;
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d() != null ? quadro04Model.getAnexoJq04C407d() : 0;
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && anoExercicio != null && anoExercicio >= 2009 && anoExercicio < 2012 && impostoPortugal > 0) {
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
                Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
                long[] codIdentificacaoPaisToValdiate = new long[]{276, 40, 56, 100, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 380, 428, 440, 442, 470, 528, 616, 203, 826, 642, 752};
                if (campoQ4 == null || campoQ4 != 407 || identificacaoPaisCodPais == null || Modelo3IRSValidatorUtil.in(identificacaoPaisCodPais, codIdentificacaoPaisToValdiate) || (double)impostoPortugal.longValue() <= (double)rendimento.longValue() * 0.215) continue;
                result.add(new DeclValidationMessage("J256", impostoPortugalLink, new String[]{impostoPortugalLink}));
            }
        }
    }
}

