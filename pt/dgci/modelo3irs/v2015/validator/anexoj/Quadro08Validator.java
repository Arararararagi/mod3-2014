/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq08T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.Tuple;

public abstract class Quadro08Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro08Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro08 quadro08Model = anexoJModel.getQuadro08();
            if (quadro08Model.getAnexoJq08T1().size() > 30) {
                result.add(new DeclValidationMessage("J173", anexoJModel.getLink("aAnexoJ.qQuadro08"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro08")}));
            }
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            for (int linha = 0; linha < quadro08Model.getAnexoJq08T1().size(); ++linha) {
                AnexoJq08T1_Linha anexoJq08T1Linha = quadro08Model.getAnexoJq08T1().get(linha);
                Long campoQ4 = anexoJq08T1Linha.getCampoQ4();
                Long nifEntidadeRetentora = anexoJq08T1Linha.getNifEntidadeRetentora();
                String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro08.tanexoJq08T1.l" + (linha + 1));
                String campoQ4Link = lineLink + ".c" + AnexoJq08T1_LinhaBase.Property.CAMPOQ4.getIndex();
                String nifEntidadeRetentoraLink = lineLink + ".c" + AnexoJq08T1_LinhaBase.Property.NIFENTIDADERETENTORA.getIndex();
                if (campoQ4 != null && nifEntidadeRetentora != null) {
                    Tuple processedTuple = new Tuple(new Long[]{campoQ4, nifEntidadeRetentora});
                    if (processedTuples.contains(processedTuple)) {
                        result.add(new DeclValidationMessage("J174", lineLink, new String[]{campoQ4Link, nifEntidadeRetentoraLink}));
                    } else {
                        processedTuples.add(processedTuple);
                    }
                }
                this.validateJ175(result, anexoJq08T1Linha, anexoJModel, linha + 1);
                if (!(nifEntidadeRetentora == null || Modelo3IRSValidatorUtil.isNIFValid(nifEntidadeRetentora))) {
                    result.add(new DeclValidationMessage("J183", nifEntidadeRetentoraLink, new String[]{nifEntidadeRetentoraLink}));
                }
                if (!(nifEntidadeRetentora == null || Modelo3IRSValidatorUtil.startsWith(String.valueOf(nifEntidadeRetentora), "1", "2", "3", "5", "6", "9"))) {
                    result.add(new DeclValidationMessage("J184", nifEntidadeRetentoraLink, new String[]{nifEntidadeRetentoraLink}));
                }
                if (nifEntidadeRetentora != null && (rostoModel.getQuadro03().isSujeitoPassivoA(nifEntidadeRetentora) || rostoModel.getQuadro03().isSujeitoPassivoB(nifEntidadeRetentora) || rostoModel.getQuadro03().existsInDependentes(nifEntidadeRetentora) || rostoModel.getQuadro07().isConjugeFalecido(nifEntidadeRetentora) || rostoModel.getQuadro07().existsInAscendentes(nifEntidadeRetentora))) {
                    result.add(new DeclValidationMessage("J185", nifEntidadeRetentoraLink, new String[]{nifEntidadeRetentoraLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.fq07C1"}));
                }
                this.validateJ179(result, campoQ4, campoQ4Link);
                this.validateJ230(result, anexoJq08T1Linha, rostoModel.getQuadro02().getQ02C02(), linha + 1, anexoJModel);
                this.validateJ234(result, anexoJq08T1Linha, rostoModel.getQuadro02().getQ02C02(), linha + 1, anexoJModel);
            }
            this.validateJ186(result, anexoJModel, quadro08Model);
        }
        return result;
    }

    protected void validateJ179(ValidationResult result, Long campoQ4, String campoQ4Link) {
        if (!(campoQ4 == null || Modelo3IRSValidatorUtil.in(campoQ4, new long[]{401, 403, 404, 405, 406, 407, 408, 410, 418, 422, 424, 425, 426}))) {
            result.add(new DeclValidationMessage("J179", campoQ4Link, new String[]{campoQ4Link}));
        }
    }

    protected void validateJ175(ValidationResult result, AnexoJq08T1_Linha anexoJq08T1Linha, AnexoJModel anexoJModel, int numLinha) {
        Long retencoesIRS = anexoJq08T1Linha.getRetencoesIRS();
        Long campoQ4 = anexoJq08T1Linha.getCampoQ4();
        Long nifEntidadeRetentora = anexoJq08T1Linha.getNifEntidadeRetentora();
        Long rendimentos = anexoJq08T1Linha.getRendimentos();
        Long retencaoSobretaxa = anexoJq08T1Linha.getRetencaoSobretaxa();
        String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro08.tanexoJq08T1.l" + numLinha);
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(retencoesIRS) && Modelo3IRSValidatorUtil.isEmptyOrZero(campoQ4) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencaoSobretaxa) && Modelo3IRSValidatorUtil.isEmptyOrZero(nifEntidadeRetentora)) {
            result.add(new DeclValidationMessage("J175", lineLink, new String[]{lineLink}));
        }
    }

    public void validateJ230(ValidationResult result, AnexoJq08T1_Linha current, Long anoRendimentos, int numLinha, AnexoJModel anexoJmodel) {
        if (current.getRetencaoSobretaxa() != null && current.getRetencaoSobretaxa() > 0 && anoRendimentos != null && anoRendimentos != 2011 && anoRendimentos != 2013 && anoRendimentos != 2014) {
            String sobretaxaLinkWithoutSubId = AnexoJq08T1_Linha.getLink(numLinha, AnexoJq08T1_LinhaBase.Property.RETENCAOSOBRETAXA);
            String sobretaxaLink = anexoJmodel.getLink(sobretaxaLinkWithoutSubId);
            result.add(new DeclValidationMessage("J230", sobretaxaLink, new String[]{sobretaxaLink, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    public void validateJ234(ValidationResult result, AnexoJq08T1_Linha current, Long anoRendimentos, int numLinha, AnexoJModel anexoJmodel) {
        if (anoRendimentos != null && (anoRendimentos == 2011 || anoRendimentos > 2012) && current.getRetencaoSobretaxa() != null && current.getRetencaoSobretaxa() > 0 && (current.getRendimentos() == null || current.getRendimentos() != null && current.getRendimentos() < current.getRetencaoSobretaxa())) {
            String sobretaxaLinkWithoutSubId = AnexoJq08T1_Linha.getLink(numLinha, AnexoJq08T1_LinhaBase.Property.RETENCAOSOBRETAXA);
            String rendimentosLinkWithoutSubId = AnexoJq08T1_Linha.getLink(numLinha, AnexoJq08T1_LinhaBase.Property.RENDIMENTOS);
            String sobretaxaLink = anexoJmodel.getLink(sobretaxaLinkWithoutSubId);
            String rendimentosLink = anexoJmodel.getLink(rendimentosLinkWithoutSubId);
            result.add(new DeclValidationMessage("J234", sobretaxaLink, new String[]{sobretaxaLink, rendimentosLink}));
        }
    }

    protected void validateJ186(ValidationResult result, AnexoJModel anexoJModel, Quadro08 quadro08Model) {
        Set<Long> distinctCodigosCamposQ4 = quadro08Model.getDistinctCodigosCamposQ4();
        for (Long codigoCampoQ4 : distinctCodigosCamposQ4) {
            long totalRetencaoQuadro04 = anexoJModel.getQuadro04().getRetencaoByCodigo(codigoCampoQ4);
            long totalRetencaoQuadro08 = quadro08Model.getTotalRetencoesIRSPreenchidoByCodigoCampoQ4(codigoCampoQ4);
            if (codigoCampoQ4 == 423 || totalRetencaoQuadro08 <= totalRetencaoQuadro04) continue;
            result.add(new DeclValidationMessage("J186", anexoJModel.getLink("aAnexoJ.qQuadro08"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro08"), anexoJModel.getLink("aAnexoJ.qQuadro04")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro04 quadro04Model = anexoJModel.getQuadro04();
            Quadro08 quadro08Model = anexoJModel.getQuadro08();
            for (int linha = 0; linha < quadro08Model.getAnexoJq08T1().size(); ++linha) {
                AnexoJq08T1_Linha anexoJq08T1Linha = quadro08Model.getAnexoJq08T1().get(linha);
                this.validateJ190(result, anexoJModel, anexoJq08T1Linha, linha + 1);
                this.validateJ192(result, anexoJModel, anexoJq08T1Linha, linha + 1);
            }
            Set<Long> distinctCodigosCamposQ4 = quadro08Model.getDistinctCodigosCamposQ4();
            this.validateJ177(result, anexoJModel, quadro04Model, quadro08Model, distinctCodigosCamposQ4);
            this.validateJ229(result, anexoJModel, quadro04Model, quadro08Model, distinctCodigosCamposQ4);
        }
        return result;
    }

    protected void validateJ192(ValidationResult result, AnexoJModel anexoJModel, AnexoJq08T1_Linha anexoJq08T1Linha, int numLinha) {
        String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro08.tanexoJq08T1.l" + numLinha);
        String campoQ4Link = lineLink + ".c" + AnexoJq08T1_LinhaBase.Property.CAMPOQ4.getIndex();
        String nifEntidadeRetentoraLink = lineLink + ".c" + AnexoJq08T1_LinhaBase.Property.NIFENTIDADERETENTORA.getIndex();
        String rendimentosLink = lineLink + ".c" + AnexoJq08T1_LinhaBase.Property.RENDIMENTOS.getIndex();
        Long campoQ4 = anexoJq08T1Linha.getCampoQ4();
        Long nifEntidadeRetentora = anexoJq08T1Linha.getNifEntidadeRetentora();
        Long rendimentos = anexoJq08T1Linha.getRendimentos();
        if (campoQ4 != null && (nifEntidadeRetentora == null || Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos))) {
            result.add(new DeclValidationMessage("J192", campoQ4Link, new String[]{campoQ4Link, rendimentosLink, nifEntidadeRetentoraLink}));
        }
    }

    protected void validateJ190(ValidationResult result, AnexoJModel anexoJModel, AnexoJq08T1_Linha anexoJq08T1Linha, int numLinha) {
        Long retencoesIRS = anexoJq08T1Linha.getRetencoesIRS();
        Long campoQ4 = anexoJq08T1Linha.getCampoQ4();
        Long nifEntidadeRetentora = anexoJq08T1Linha.getNifEntidadeRetentora();
        Long rendimentos = anexoJq08T1Linha.getRendimentos();
        String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro08.tanexoJq08T1.l" + numLinha);
        String campoQ4Link = lineLink + ".c" + AnexoJq08T1_LinhaBase.Property.CAMPOQ4.getIndex();
        if (campoQ4 == null && (retencoesIRS != null || nifEntidadeRetentora != null || rendimentos != null && rendimentos > 0)) {
            result.add(new DeclValidationMessage("J190", campoQ4Link, new String[]{campoQ4Link}));
        }
    }

    protected void validateJ177(ValidationResult result, AnexoJModel anexoJModel, Quadro04 quadro04Model, Quadro08 quadro08Model, Set<Long> distinctCodigosCamposQ4) {
        for (Long codigoCampoQ4 : distinctCodigosCamposQ4) {
            long totalRetencoesQuadro04 = quadro04Model.getRetencaoByCodigo(codigoCampoQ4);
            long totalRetencoesQuadro08 = quadro08Model.getTotalRetencoesIRSPreenchidoByCodigoCampoQ4(codigoCampoQ4);
            if (codigoCampoQ4 == 423 || totalRetencoesQuadro04 == totalRetencoesQuadro08) continue;
            result.add(new DeclValidationMessage("J177", anexoJModel.getLink("aAnexoJ.qQuadro08"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04"), anexoJModel.getLink("aAnexoJ.qQuadro08")}));
        }
    }

    protected void validateJ229(ValidationResult result, AnexoJModel anexoJModel, Quadro04 quadro04Model, Quadro08 quadro08Model, Set<Long> distinctCodigosCamposQ4) {
        for (Long codigoCampoQ4 : distinctCodigosCamposQ4) {
            long totalRendimentosQuadro04 = quadro04Model.getRendimentosByCodigo(codigoCampoQ4);
            long totalRendimentosQuadro08 = quadro08Model.getTotalRendimentosIRSPreenchidoByCodigoCampoQ4(codigoCampoQ4);
            if (codigoCampoQ4 == 423 || totalRendimentosQuadro08 <= totalRendimentosQuadro04) continue;
            result.add(new DeclValidationMessage("J229", anexoJModel.getLink("aAnexoJ.qQuadro08"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro08"), anexoJModel.getLink("aAnexoJ.qQuadro04")}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

