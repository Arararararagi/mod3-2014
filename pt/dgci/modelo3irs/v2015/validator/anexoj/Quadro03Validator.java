/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro03 quadro03Model = anexoJModel.getQuadro03();
            if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoJq03C02(), rostoModel.getQuadro03().getQ03C03())) {
                result.add(new DeclValidationMessage("J003", anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C02"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C02"), "aRosto.qQuadro03.fq03C03"}));
            }
            if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoJq03C03(), rostoModel.getQuadro03().getQ03C04())) {
                result.add(new DeclValidationMessage("J004", anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C03"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C03"), "aRosto.qQuadro03.fq03C04"}));
            }
            Long nifTitularRendimento = quadro03Model.getAnexoJq03C04();
            this.validateJ005(result, rostoModel, anexoJModel, nifTitularRendimento);
            if (nifTitularRendimento != null) continue;
            result.add(new DeclValidationMessage("J006", anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04")}));
        }
        return result;
    }

    protected void validateJ005(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel, Long nifTitularRendimento) {
        if (!(nifTitularRendimento == null || rostoModel.getQuadro03().isSujeitoPassivoA(nifTitularRendimento) || rostoModel.getQuadro03().isSujeitoPassivoB(nifTitularRendimento) || rostoModel.getQuadro03().existsInDependentes(nifTitularRendimento) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nifTitularRendimento) || rostoModel.getQuadro07().isConjugeFalecido(nifTitularRendimento))) {
            result.add(new DeclValidationMessage("J005", anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04"), "aRosto.qQuadro03"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

