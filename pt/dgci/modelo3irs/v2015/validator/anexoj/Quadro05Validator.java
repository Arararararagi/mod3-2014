/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro05;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro05 quadro05Model = anexoJModel.getQuadro05();
            if (quadro05Model.getAnexoJq05T1().size() > 50) {
                result.add(new DeclValidationMessage("J139", anexoJModel.getLink("aAnexoJ.qQuadro05"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro05")}));
            }
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            for (int linha = 0; linha < quadro05Model.getAnexoJq05T1().size(); ++linha) {
                boolean isBicZeros;
                AnexoJq05T1_Linha anexoJq05T1Linha = quadro05Model.getAnexoJq05T1().get(linha);
                String iban = anexoJq05T1Linha.getIban();
                String bic = anexoJq05T1Linha.getBic();
                String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro05.tanexoJq05T1.l" + (linha + 1));
                if (iban != null && bic != null) {
                    Tuple processedTuple = new Tuple(new String[]{iban, bic});
                    if (processedTuples.contains(processedTuple)) {
                        result.add(new DeclValidationMessage("J140", lineLink, new String[]{lineLink}));
                    } else {
                        processedTuples.add(processedTuple);
                    }
                }
                boolean isIbanZeros = !StringUtil.isEmpty(iban) && StringUtil.isNumeric(iban) && Modelo3IRSValidatorUtil.isZeroString(iban);
                boolean bl = isBicZeros = !StringUtil.isEmpty(bic) && StringUtil.isNumeric(bic) && Long.valueOf(bic) == 0;
                if ((!StringUtil.isEmpty(iban) || !StringUtil.isEmpty(bic)) && (!isIbanZeros || !isBicZeros)) continue;
                result.add(new DeclValidationMessage("J141", lineLink, new String[]{lineLink}));
            }
            this.validateAnexoJq05T2(model, anexoJModel, quadro05Model, result, quadro05Model.getAnexoJq05T2());
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro05 quadro05Model = anexoJModel.getQuadro05();
            for (int linha = 0; linha < quadro05Model.getAnexoJq05T1().size(); ++linha) {
                AnexoJq05T1_Linha anexoJq05T1Linha = quadro05Model.getAnexoJq05T1().get(linha);
                this.validateJ142(result, anexoJq05T1Linha, anexoJModel, linha + 1);
            }
        }
        return result;
    }

    protected void validateJ142(ValidationResult result, AnexoJq05T1_Linha anexoJq05T1Linha, AnexoJModel anexoJModel, int numLinha) {
        String iban = anexoJq05T1Linha.getIban();
        String bic = anexoJq05T1Linha.getBic();
        String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro05.tanexoJq05T1.l" + numLinha);
        String ibanLink = lineLink + ".c" + AnexoJq05T1_LinhaBase.Property.IBAN.getIndex();
        String bicLink = lineLink + ".c" + AnexoJq05T1_LinhaBase.Property.BIC.getIndex();
        if (iban != null && bic == null || iban == null && bic != null) {
            result.add(new DeclValidationMessage("J142", lineLink, new String[]{ibanLink, bicLink}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private void validateAnexoJq05T2(DeclaracaoModel model, AnexoJModel anexoJModel, Quadro05 quadro05Model, ValidationResult result, EventList<AnexoJq05T2_Linha> anexoJq05T2) {
        ArrayList<Tuple> processedNlinhasQ5 = new ArrayList<Tuple>();
        this.validateJ281(anexoJModel, result, anexoJq05T2);
        this.validateJ282(anexoJModel, result, anexoJq05T2, processedNlinhasQ5);
        for (int i = 0; i < anexoJq05T2.size(); ++i) {
            AnexoJq05T2_Linha linha = anexoJq05T2.get(i);
            String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro05.tanexoJq05T2.l" + (i + 1));
            this.validateJ283(result, linha, lineLink);
        }
    }

    protected void validateJ281(AnexoJModel anexoJModel, ValidationResult result, EventList<AnexoJq05T2_Linha> anexoJq05T2) {
        int maxLinhas = 50;
        if (anexoJq05T2.size() > maxLinhas) {
            result.add(new DeclValidationMessage("J281", anexoJModel.getLink("aAnexoJ.qQuadro05.tanexoJq05T2"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro05.tanexoJq05T2")}));
        }
    }

    protected void validateJ282(AnexoJModel anexoJModel, ValidationResult result, EventList<AnexoJq05T2_Linha> anexoJq05T2, List<Tuple> processedNlinhasQ5) {
        for (int i = 0; i < anexoJq05T2.size(); ++i) {
            AnexoJq05T2_Linha linha = anexoJq05T2.get(i);
            String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro05.tanexoJq05T2.l" + (i + 1));
            String campoNLinhaQ5 = linha.getOutrosNumIdentificacao();
            String linkOutrosNumIdentificacao = lineLink + ".c" + AnexoJq05T2_LinhaBase.Property.OUTROSNUMIDENTIFICACAO.getIndex();
            if (campoNLinhaQ5 == null) continue;
            Tuple processedCampoNLinhaQ5 = new Tuple(new String[]{campoNLinhaQ5});
            if (processedNlinhasQ5.contains(processedCampoNLinhaQ5)) {
                result.add(new DeclValidationMessage("J282", linkOutrosNumIdentificacao, new String[]{linkOutrosNumIdentificacao}));
                continue;
            }
            processedNlinhasQ5.add(processedCampoNLinhaQ5);
        }
    }

    protected void validateJ283(ValidationResult result, AnexoJq05T2_Linha linha, String lineLink) {
        if (StringUtil.isEmpty(linha.getOutrosNumIdentificacao()) || linha.getOutrosNumIdentificacao().equals("0")) {
            result.add(new DeclValidationMessage("J283", "aAnexoJ.qQuadro05.tanexoJq05T2", new String[]{lineLink}));
        }
    }
}

