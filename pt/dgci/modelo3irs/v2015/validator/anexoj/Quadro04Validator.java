/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxJ;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Year;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            this.validateTrabalhoDependente(result, anexoJModel);
            this.validateRemuneracoesPublicas(result, anexoJModel);
            this.validateTrabalhoIndependente(model, result, anexoJModel);
            this.validateComerciais(result, model, anexoJModel);
            this.validateAgriculas(result, model, anexoJModel);
            this.validatePropriedadeIntelectual(result, anexoJModel);
            this.validateParticipacoesSociais(result, anexoJModel);
            this.validateJurosOuCreditos(result, anexoJModel);
            this.validateRoyalties(result, anexoJModel);
            this.validateRendimentosMobiliarios(result, anexoJModel);
            this.validateRendimentosDeCapitais(result, anexoJModel);
            this.validateOutrasAplicacoes(result, anexoJModel);
            this.validatePrediais(result, anexoJModel);
            this.validateOutrosIncrementosPatrimoniais(result, anexoJModel);
            this.validatePensoes(result, anexoJModel);
            this.validatePensoesPublicas(result, anexoJModel);
            this.validateRendimentoDirectivaPoupanca(result, anexoJModel);
            this.validateRendimentoDirectivaPoupancaRestantesPaises(result, anexoJModel);
            this.validateOutrosRendimentosReferidos(result, anexoJModel);
            this.validateRendimentosCapitaisReferidos(result, anexoJModel);
            this.validateRendimentosPropriedadeIntelectual(result, anexoJModel);
            this.validateRendimentoDirectivaPoupancaPaisesNaoAbrangidos(result, rostoModel, anexoJModel);
            this.validateOutrosRendimentosReferidosN5Art72(result, rostoModel, anexoJModel);
            this.validateRendasTemporariasOuVitalicias(result, anexoJModel);
            this.validateDividendosOuLucrosNaoSujeitosRetencaoPortugal(result, anexoJModel);
            this.validateInstrumentosFinanceirosDerivadosWarrantsCertificados(result, anexoJModel);
            this.validateJ298(result, anexoJModel, Modelo3IRSv2015Parameters.instance().isFase2());
            this.validateJ299(result, anexoJModel, Modelo3IRSv2015Parameters.instance().isFase2());
            this.validateJ300(result, anexoJModel, Modelo3IRSv2015Parameters.instance().isFase2());
            this.validateJ301(result, anexoJModel, Modelo3IRSv2015Parameters.instance().isFase2());
            this.validateJ302(result, anexoJModel, Modelo3IRSv2015Parameters.instance().isFase2());
            Quadro04 quadro04Model = anexoJModel.getQuadro04();
            AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(anexoJModel.getAnexoJTitular());
            AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(anexoJModel.getAnexoJTitular());
            this.validateJ199(result, anexoBModel, anexoCModel, anexoJModel);
            this.validateJ297(result, anexoJModel);
            this.validateJ119(result, anexoJModel);
            this.validateJ120(result, anexoJModel);
            this.validateJ121(result, anexoJModel);
            this.validateJ122(result, anexoJModel);
            this.validateJ123(result, anexoJModel);
            this.validateJ191(result, anexoJModel);
            this.validateJ237(result, anexoBModel, anexoJModel);
            Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
            this.validateJ126(result, anexoHModel, anexoJModel, rostoModel, Modelo3IRSv2015Parameters.instance().isFase2());
            this.validateJ127(result, anexoHModel, anexoJModel, rostoModel, Modelo3IRSv2015Parameters.instance().isFase2());
            this.validateJ131(result, anexoJModel, quadro04Model);
            if (Modelo3IRSv2015Parameters.instance().isFase2() && quadro04Model.isAnexoJq04B1NaoSelected() && anoExercicio != null && Modelo3IRSValidatorUtil.in(anoExercicio, new long[]{2002, 2003, 2004, 2005})) {
                result.add(new DeclValidationMessage("J132", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim"), "aRosto.qQuadro02.fq02C02"}));
            }
            if (!(quadro04Model.getAnexoJq04B1() == null || quadro04Model.getAnexoJq04B1().equals(Quadro04.ANEXOJQ04B1NAO_VALUE) || quadro04Model.getAnexoJq04B1().equals(Quadro04.ANEXOJQ04B1SIM_VALUE))) {
                result.add(new DeclValidationMessage("J220", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim")}));
            }
            this.validateAnexoJq04T1(model, rostoModel, anexoJModel, quadro04Model, result, quadro04Model.getAnexoJq04T1());
            this.validateAnexoJq04T2(model, rostoModel, anexoJModel, quadro04Model, result, quadro04Model.getAnexoJq04T2());
        }
        return result;
    }

    protected void validateJ122(ValidationResult result, AnexoJModel anexoJModel) {
        if (anexoJModel != null) {
            long totalRendimentosQuadro6;
            long totalRendimentosQuadro4 = anexoJModel.getQuadro04().getAnexoJq04C1b() != null ? anexoJModel.getQuadro04().getAnexoJq04C1b() : 0;
            long l = totalRendimentosQuadro6 = anexoJModel.getQuadro06().getAnexoJq06C1() != null ? anexoJModel.getQuadro06().getAnexoJq06C1() : 0;
            if (totalRendimentosQuadro4 != totalRendimentosQuadro6) {
                result.add(new DeclValidationMessage("J122", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1b"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04"), anexoJModel.getLink("aAnexoJ.qQuadro06")}));
            }
        }
    }

    protected void validateJ123(ValidationResult result, AnexoJModel anexoJModel) {
        if (anexoJModel != null) {
            long totalImpostoEstrangeiro;
            long l = totalImpostoEstrangeiro = anexoJModel.getQuadro04().getAnexoJq04C1c() != null ? anexoJModel.getQuadro04().getAnexoJq04C1c() : 0;
            if (totalImpostoEstrangeiro > 0 && anexoJModel.getQuadro06().isEmpty()) {
                result.add(new DeclValidationMessage("J123", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1c"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1")}));
            }
        }
    }

    protected void validateJ191(ValidationResult result, AnexoJModel anexoJModel) {
        if (anexoJModel != null) {
            long totalImpostoPortugalQuadro04;
            long l = totalImpostoPortugalQuadro04 = anexoJModel.getQuadro04().getAnexoJq04C1d() != null ? anexoJModel.getQuadro04().getAnexoJq04C1d() : 0;
            if (totalImpostoPortugalQuadro04 > 0 && anexoJModel.getQuadro08().isEmpty()) {
                result.add(new DeclValidationMessage("J191", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1d"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro08")}));
            }
        }
    }

    protected void validateJ126(ValidationResult result, AnexoHModel anexoHModel, AnexoJModel anexoJModel, RostoModel rostoModel, boolean isFase2) {
        if (anexoHModel == null && rostoModel != null && anexoJModel != null && rostoModel.getQuadro02().getQ02C02() != null && isFase2) {
            long anoExercicio = rostoModel.getQuadro02().getQ02C02();
            long valAnxH = 0;
            long valAnxJ = anexoJModel.getQuadro04().getAnexoJq04C421() != null ? anexoJModel.getQuadro04().getAnexoJq04C421() : 0;
            long soma = valAnxH + valAnxJ;
            long ano2001 = 2001;
            long ano2004 = 2004;
            long ano2011 = 2011;
            long ano2012 = 2012;
            long ano2013 = 2013;
            long limite30mil = 3000000;
            long limite20mil = 2000000;
            long limite10mil = 1000000;
            if (soma > 3000000 && (anoExercicio < 2001 || anoExercicio > 2004 && anoExercicio < 2012) || soma > 2000000 && anoExercicio > 2011 && anoExercicio < 2013 || soma > 1000000 && anoExercicio > 2012) {
                result.add(new DeclValidationMessage("J126", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421")}));
            }
        }
    }

    protected void validateJ127(ValidationResult result, AnexoHModel anexoHModel, AnexoJModel anexoJModel, RostoModel rostoModel, boolean isFase2) {
        if (anexoHModel != null && rostoModel != null && anexoJModel != null && rostoModel.getQuadro02().getQ02C02() != null && isFase2) {
            long anoExercicio = rostoModel.getQuadro02().getQ02C02();
            long valAnxH = anexoHModel.getQuadro05().getAnexoHq05C1() != null ? anexoHModel.getQuadro05().getAnexoHq05C1() : 0;
            long valAnxJ = anexoJModel.getQuadro04().getAnexoJq04C421() != null ? anexoJModel.getQuadro04().getAnexoJq04C421() : 0;
            long soma = valAnxH + valAnxJ;
            long ano2001 = 2001;
            long ano2004 = 2004;
            long ano2011 = 2011;
            long ano2012 = 2012;
            long ano2013 = 2013;
            long limite30mil = 3000000;
            long limite20mil = 2000000;
            long limite10mil = 1000000;
            if (soma > 3000000 && (anoExercicio < 2001 || anoExercicio > 2004 && anoExercicio < 2012) || soma > 2000000 && anoExercicio > 2011 && anoExercicio < 2013 || soma > 1000000 && anoExercicio > 2012) {
                result.add(new DeclValidationMessage("J127", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421"), new String[]{"aAnexoH.qQuadro05.fanexoHq05C1", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421")}));
            }
        }
    }

    protected void validateJ120(ValidationResult result, AnexoJModel anexoJModel) {
        long somaImpostoEstrangeiro;
        long totalImpostoEstrangeiroPreenchido = anexoJModel.getQuadro04().getTotalImpostoEstrangeiroPreenchido();
        long l = somaImpostoEstrangeiro = anexoJModel.getQuadro04().getAnexoJq04C1c() != null ? anexoJModel.getQuadro04().getAnexoJq04C1c() : 0;
        if (totalImpostoEstrangeiroPreenchido != somaImpostoEstrangeiro) {
            result.add(new DeclValidationMessage("J120", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1c"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1c")}));
        }
    }

    protected void validateJ119(ValidationResult result, AnexoJModel anexoJModel) {
        long somaRendimento;
        long totalRendimentoPreenchido = anexoJModel.getQuadro04().getTotalRendimentoPreenchido();
        long l = somaRendimento = anexoJModel.getQuadro04().getAnexoJq04C1b() != null ? anexoJModel.getQuadro04().getAnexoJq04C1b() : 0;
        if (totalRendimentoPreenchido != somaRendimento) {
            result.add(new DeclValidationMessage("J119", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1b"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1b")}));
        }
    }

    protected void validateJ121(ValidationResult result, AnexoJModel anexoJModel) {
        long somaImpostoPortugal;
        long totalImpostoPortugalPreenchido = anexoJModel.getQuadro04().getTotalImpostoPortugalPreenchido();
        long l = somaImpostoPortugal = anexoJModel.getQuadro04().getAnexoJq04C1d() != null ? anexoJModel.getQuadro04().getAnexoJq04C1d() : 0;
        if (totalImpostoPortugalPreenchido != somaImpostoPortugal) {
            result.add(new DeclValidationMessage("J121", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1d"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C1d")}));
        }
    }

    protected void validateRendimentosDeCapitais(ValidationResult result, AnexoJModel anexoJModel) {
        this.validateJ245(result, anexoJModel);
        this.validateJ251(result, anexoJModel);
        this.validateJ246(result, anexoJModel);
        this.validateJ252(result, anexoJModel);
        this.validateJ247(result, anexoJModel);
        this.validateJ249(result, anexoJModel);
        this.validateJ257(result, anexoJModel);
    }

    protected void validateJ245(ValidationResult result, AnexoJModel anexoJModel) {
        Long ano = anexoJModel.getQuadro02().getAnexoJq02C01();
        Long value = anexoJModel.getQuadro04().getAnexoJq04C424b();
        if (value != null && value > 0 && ano != null && ano < 2012) {
            result.add(new DeclValidationMessage("J245", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424b"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424b")}));
        }
    }

    protected void validateJ251(ValidationResult result, AnexoJModel anexoJModel) {
        if (!(anexoJModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoJModel.getQuadro04().getAnexoJq04C424b()) || Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J251", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424b"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424b")}));
        }
    }

    protected void validateJ246(ValidationResult result, AnexoJModel anexoJModel) {
        Long ano = anexoJModel.getQuadro02().getAnexoJq02C01();
        Long value = anexoJModel.getQuadro04().getAnexoJq04C424c();
        if (value != null && value > 0 && ano != null && ano < 2012) {
            result.add(new DeclValidationMessage("J246", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424c"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424c")}));
        }
    }

    protected void validateJ252(ValidationResult result, AnexoJModel anexoJModel) {
        if (!(anexoJModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoJModel.getQuadro04().getAnexoJq04C424c()) || Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J252", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424c"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424c")}));
        }
    }

    protected void validateJ247(ValidationResult result, AnexoJModel anexoJModel) {
        Long ano = anexoJModel.getQuadro02().getAnexoJq02C01();
        Long value = anexoJModel.getQuadro04().getAnexoJq04C424d();
        if (value != null && value > 0 && ano != null && ano < 2012) {
            result.add(new DeclValidationMessage("J247", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424d"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424d")}));
        }
    }

    protected void validateJ249(ValidationResult result, AnexoJModel anexoJModel) {
        if (!(anexoJModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoJModel.getQuadro04().getAnexoJq04C424d()) || Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J249", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424d"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424d")}));
        }
    }

    protected void validateJ257(ValidationResult result, AnexoJModel anexoJModel) {
        double J257multiplicador = 0.355;
        if (!(anexoJModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoJModel.getQuadro04().getAnexoJq04C424d()) || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoJModel.getQuadro04().getAnexoJq04C424b()) || (double)anexoJModel.getQuadro04().getAnexoJq04C424d().longValue() <= (double)anexoJModel.getQuadro04().getAnexoJq04C424b().longValue() * 0.355 || !Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J257", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424d"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424d")}));
        }
    }

    protected void validateJ128(ValidationResult result, AnexoJModel anexoJModel) {
        if (!(anexoJModel == null || Modelo3IRSv2015Parameters.instance().isFase2() || anexoJModel.getQuadro04().getAnexoJq04B1() == null)) {
            result.add(new DeclValidationMessage("J128", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim")}));
        }
    }

    protected void validateJ125(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || quadro04Model.getAnexoJq04C421() == null || quadro04Model.getAnexoJq04C421() == 0)) {
            result.add(new DeclValidationMessage("J125", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421")));
        }
    }

    private void validateTrabalhoDependente(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long segurancaSocial = quadro04Model.getAnexoJq04C401a();
        String segurancaSocialLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401a");
        Long rendimento = quadro04Model.getAnexoJq04C401b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C401c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C401d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (segurancaSocial != null && segurancaSocial > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J009", segurancaSocialLink, new String[]{segurancaSocialLink, rendimentoLink}));
        }
        if (segurancaSocial != null && segurancaSocial > 0 && rendimento != null && rendimento > 0 && segurancaSocial > rendimento) {
            result.add(new DeclValidationMessage("J010", segurancaSocialLink, new String[]{segurancaSocialLink, rendimentoLink}));
        }
        if (impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J012", rendimentoLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
    }

    private void validateRemuneracoesPublicas(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long segurancaSocial = quadro04Model.getAnexoJq04C402a();
        String segurancaSocialLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C402a");
        Long rendimento = quadro04Model.getAnexoJq04C402b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C402b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C402c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C402c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C402d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C402d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (segurancaSocial != null && segurancaSocial > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J014", segurancaSocialLink, new String[]{segurancaSocialLink, rendimentoLink}));
        }
        if (segurancaSocial != null && segurancaSocial > 0 && rendimento != null && rendimento > 0 && segurancaSocial > rendimento) {
            result.add(new DeclValidationMessage("J015", segurancaSocialLink, new String[]{segurancaSocialLink, rendimentoLink}));
        }
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J017", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    private void validateTrabalhoIndependente(DeclaracaoModel model, ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C403b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C403c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C403d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J020", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J022", impostoPortugalLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
        Quadro04Validator.validateJ242(model, anexoJModel, result);
    }

    protected void validateJ021(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C403d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal == 0)) {
            result.add(new DeclValidationMessage("J021", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ019(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C403c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J019", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ018(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C403b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J018", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateComerciais(ValidationResult result, DeclaracaoModel model, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C404b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C404c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404c");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        Quadro04Validator.validateJ243(model, anexoJModel, result);
    }

    protected void validateJ025(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C404c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J025", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void valiidateJ024(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C404b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J024", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateAgriculas(ValidationResult result, DeclaracaoModel model, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C405b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C405c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C405d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J035", impostoPortugalLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
        Quadro04Validator.validateJ244(model, anexoJModel, result);
    }

    protected void validateJ034(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C405d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal == 0)) {
            result.add(new DeclValidationMessage("J034", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ032(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C405c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J032", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ031(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C405b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J031", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validatePropriedadeIntelectual(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C406b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C406c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C406d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (Modelo3IRSv2015Parameters.instance().isFase2() && (quadro04Model.getAnexoJq04C421() != null ? quadro04Model.getAnexoJq04C421() : 0) > (rendimento != null ? rendimento : 0)) {
            result.add(new DeclValidationMessage("J040", rendimentoLink, new String[]{rendimentoLink, anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421")}));
        }
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J045", impostoPortugalLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
    }

    protected void validateJ298(ValidationResult result, AnexoJModel anexoJModel, boolean isFase2) {
        Quadro04 quadro04 = anexoJModel.getQuadro04();
        Long rendimentos = quadro04.getAnexoJq04C426b();
        String linkRendimentos = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b");
        if (!(isFase2 || rendimentos == null || rendimentos == 0)) {
            result.add(new DeclValidationMessage("J298", linkRendimentos, new String[]{linkRendimentos}));
        }
    }

    protected void validateJ299(ValidationResult result, AnexoJModel anexoJModel, boolean isFase2) {
        Quadro04 quadro04 = anexoJModel.getQuadro04();
        Long impostoEstrangeiro = quadro04.getAnexoJq04C426c();
        String linkImpostoEstrangeiro = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426c");
        if (!(isFase2 || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J299", linkImpostoEstrangeiro, new String[]{linkImpostoEstrangeiro}));
        }
    }

    protected void validateJ300(ValidationResult result, AnexoJModel anexoJModel, boolean isFase2) {
        Quadro04 quadro04 = anexoJModel.getQuadro04();
        Long impostoRetido = quadro04.getAnexoJq04C426d();
        String linkImpostoRetido = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426d");
        if (!(isFase2 || impostoRetido == null || impostoRetido == 0)) {
            result.add(new DeclValidationMessage("J300", linkImpostoRetido, new String[]{linkImpostoRetido}));
        }
    }

    protected void validateJ301(ValidationResult result, AnexoJModel anexoJModel, boolean isFase2) {
        Quadro04 quadro04 = anexoJModel.getQuadro04();
        if (isFase2 && quadro04.getAnexoJq04C426d() != null && quadro04.getAnexoJq04C426b() != null && quadro04.getAnexoJq04C426d() > quadro04.getAnexoJq04C426b()) {
            result.add(new DeclValidationMessage("J301", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426d"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426d"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b")}));
        }
    }

    protected void validateJ302(ValidationResult result, AnexoJModel anexoJModel, boolean isFase2) {
        Quadro04 quadro04 = anexoJModel.getQuadro04();
        if (isFase2 && quadro04.getAnexoJq04C426d() != null && quadro04.getAnexoJq04C426b() != null && (double)quadro04.getAnexoJq04C426d().longValue() > 0.285 * (double)quadro04.getAnexoJq04C426b().longValue()) {
            result.add(new DeclValidationMessage("J302", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426d"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426d")}));
        }
    }

    protected void validateJ044(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C406d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal == 0)) {
            result.add(new DeclValidationMessage("J044", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ041(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C406c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J041", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ038(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C406b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J038", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateParticipacoesSociais(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C407b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C407c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J051", impostoPortugalLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
    }

    protected void validateJ050(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal == 0)) {
            result.add(new DeclValidationMessage("J050", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ048(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C407c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J048", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ047(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C407b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J047", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateJurosOuCreditos(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C408b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C408c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C408d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J059", impostoPortugalLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
    }

    protected void validateJ057(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C408d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal == 0)) {
            result.add(new DeclValidationMessage("J057", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ055(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C408c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J055", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ054(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C408b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J054", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateRoyalties(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C409b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C409c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C409d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J064", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ062(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C409c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J062", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ061(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C409b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J061", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateRendimentosMobiliarios(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C410b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C410c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C410d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J073", impostoPortugalLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
    }

    protected void validateJ072(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C410d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal == 0)) {
            result.add(new DeclValidationMessage("J072", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ069(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C410c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J069", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ067(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C410b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J067", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    protected void validateJ068(ValidationResult result, AnexoJModel anexoJModel, RostoModel rostoModel) {
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C410b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410b");
        if (anoExercicio != null && (anoExercicio == 2001 || anoExercicio == 2002) && rendimento != null && rendimento != 0) {
            result.add(new DeclValidationMessage("J068", rendimentoLink, new String[]{rendimentoLink, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    private void validateOutrasAplicacoes(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C411b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C411c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C411d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J078", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ076(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C411c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J076", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ075(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C411b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J075", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validatePrediais(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C412b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C412c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C412d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J084", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ082(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C412c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J082", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ081(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C412b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J081", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateOutrosIncrementosPatrimoniais(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C415b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C415c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C415d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J099", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ097(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C415c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J097", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ096(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C415b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J096", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validatePensoes(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C416b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C416c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C416d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        Long segSocial = quadro04Model.getAnexoJq04C416a();
        String segSocialLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416a");
        if (segSocial != null && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J100", segSocialLink, new String[]{segSocialLink, rendimentoLink}));
        }
        if (segSocial != null && rendimento != null && segSocial > rendimento && rendimento > 0) {
            result.add(new DeclValidationMessage("J101", segSocialLink, new String[]{segSocialLink, rendimentoLink}));
        }
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J103", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
        segSocial = quadro04Model.getAnexoJq04C417a();
        segSocialLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417a");
        Long rendimento417 = quadro04Model.getAnexoJq04C417b();
        String rendimento417Link = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417b");
        if (segSocial != null && (rendimento417 == null || rendimento417 == 0)) {
            result.add(new DeclValidationMessage("J104", segSocialLink, new String[]{segSocialLink, rendimento417Link}));
        }
        if (segSocial != null && rendimento417 != null && segSocial > rendimento417 && rendimento417 > 0) {
            result.add(new DeclValidationMessage("J105", segSocialLink, new String[]{segSocialLink, rendimento417Link}));
        }
    }

    private void validatePensoesPublicas(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C417b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C417c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C417d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J107", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    private void validateRendimentoDirectivaPoupanca(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C418b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C418c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418c");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
    }

    private void validateRendimentoDirectivaPoupancaRestantesPaises(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C422b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C422c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422c");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
    }

    private void validateOutrosRendimentosReferidos(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C423b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C423c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423c");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
    }

    private void validateRendimentosCapitaisReferidos(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C424b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C424c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424c");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
    }

    private void validateRendimentosPropriedadeIntelectual(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C426b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C426c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426c");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
    }

    protected void validateJ111(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C418d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal <= 0)) {
            result.add(new DeclValidationMessage("J111", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ109(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C418c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J109", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ108(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C418b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J108", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateRendimentoDirectivaPoupancaPaisesNaoAbrangidos(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C422b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C422c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C422d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422d");
        if (rendimento != null && rendimento > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
            result.add(new DeclValidationMessage("J201", rendimentoLink, new String[]{rendimentoLink, anexoJModel.getLink("aAnexoJ.qQuadro02")}));
        }
        if (impostoEstrangeiro != null && impostoEstrangeiro > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
            result.add(new DeclValidationMessage("J204", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink, anexoJModel.getLink("aRosto.qQuadro02")}));
        }
        if (impostoPortugal != null && impostoPortugal > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
            result.add(new DeclValidationMessage("J207", impostoPortugalLink, new String[]{impostoPortugalLink, anexoJModel.getLink("aRosto.qQuadro02")}));
        }
    }

    protected void validateJ205(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoPortugal = quadro04Model.getAnexoJq04C422d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal <= 0)) {
            result.add(new DeclValidationMessage("J205", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ202(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C422c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422c");
        if (!(Modelo3IRSv2015Parameters.instance.isFase2() || impostoEstrangeiro == null || impostoEstrangeiro <= 0)) {
            result.add(new DeclValidationMessage("J202", rendimentoLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ200(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C422b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento <= 0)) {
            result.add(new DeclValidationMessage("J200", rendimentoLink, new String[]{rendimentoLink, anexoJModel.getLink("aAnexoJ.qQuadro04")}));
        }
    }

    private void validateOutrosRendimentosReferidosN5Art72(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C423b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C423c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C423d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423d");
        if (rendimento != null && rendimento > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
            result.add(new DeclValidationMessage("J209", rendimentoLink, new String[]{rendimentoLink, anexoJModel.getLink("aRosto.qQuadro02")}));
        }
        if (impostoEstrangeiro != null && impostoEstrangeiro > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
            result.add(new DeclValidationMessage("J212", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink, anexoJModel.getLink("aRosto.qQuadro02")}));
        }
        this.validateJ213(result, impostoPortugal, impostoPortugalLink);
    }

    protected void validateJ213(ValidationResult result, Long impostoPortugal, String impostoPortugalLink) {
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J213", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ210(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C423c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423c");
        if (!(Modelo3IRSv2015Parameters.instance.isFase2() || impostoEstrangeiro == null || impostoEstrangeiro <= 0)) {
            result.add(new DeclValidationMessage("J210", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ208(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C423b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento <= 0)) {
            result.add(new DeclValidationMessage("J208", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    private void validateRendasTemporariasOuVitalicias(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C419b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C419b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C419c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C419c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C419d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C419d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J114", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    private void validateDividendosOuLucrosNaoSujeitosRetencaoPortugal(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C420b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C420c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420c");
        Long impostoPortugal = quadro04Model.getAnexoJq04C420d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420d");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        if (impostoPortugal != null && impostoPortugal > 0) {
            result.add(new DeclValidationMessage("J118", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    private void validateInstrumentosFinanceirosDerivadosWarrantsCertificados(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C425b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C425c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425c");
        this.validateJ007(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
    }

    protected void validateJ116(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C420c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420c");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoEstrangeiro == null || impostoEstrangeiro == 0)) {
            result.add(new DeclValidationMessage("J116", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ115(ValidationResult result, Quadro04 quadro04Model, AnexoJModel anexoJModel) {
        Long rendimento = quadro04Model.getAnexoJq04C420b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420b");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J115", rendimentoLink, new String[]{rendimentoLink}));
        }
    }

    protected void validateJ007(ValidationResult result, Long rendimento, String rendimentoLink, Long impostoEstrangeiro, String impostoEstrangeiroLink) {
        if (impostoEstrangeiro != null && impostoEstrangeiro > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J007", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink, rendimentoLink}));
        }
    }

    protected void validateJ008(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C401b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C401c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C402b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C402b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C402c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C402c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C403b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C403c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C404b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C404c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C405b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C405c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C406b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C406c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C407b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C407c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C408b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C408c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C409b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C409c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C410b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C410c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C411b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C411c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C412b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C412c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C415b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C415c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C416b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C416c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C417b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C417c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C418b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C418c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C419b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C419b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C419c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C419c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C420b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C420c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C422b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C422c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C423b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C423c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C424b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C424c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C424c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C425b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C425c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
        rendimento = quadro04Model.getAnexoJq04C426b();
        rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b");
        impostoEstrangeiro = quadro04Model.getAnexoJq04C426c();
        impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426c");
        this.checkJ008(result, rendimento, rendimentoLink, impostoEstrangeiro, impostoEstrangeiroLink);
    }

    protected void checkJ008(ValidationResult result, Long rendimento, String rendimentoLink, Long impostoEstrangeiro, String impostoEstrangeiroLink) {
        if (impostoEstrangeiro != null && impostoEstrangeiro > 0 && rendimento != null && rendimento > 0 && impostoEstrangeiro > rendimento) {
            result.add(new DeclValidationMessage("J008", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink, rendimentoLink}));
        }
    }

    protected void validateJ297(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04 = anexoJModel.getQuadro04();
        Quadro02 quadro02 = anexoJModel.getQuadro02();
        if ((quadro04.getAnexoJq04C426b() != null && quadro04.getAnexoJq04C426b() > 0 || quadro04.getAnexoJq04C426c() != null && quadro04.getAnexoJq04C426c() > 0 || quadro04.getAnexoJq04C426d() != null && quadro04.getAnexoJq04C426d() > 0) && quadro02.getAnexoJq02C01() != null && quadro02.getAnexoJq02C01() < 2014) {
            result.add(new DeclValidationMessage("J297", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426c"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426d")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro04 quadro04Model = anexoJModel.getQuadro04();
            this.validateJ129(result, rostoModel, anexoJModel, quadro04Model);
            this.validateJ011(result, anexoJModel);
        }
        return result;
    }

    protected void validateJ129(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel, Quadro04 quadro04Model) {
        if (Modelo3IRSv2015Parameters.instance().isFase2() && quadro04Model.getAnexoJq04B1() == null && (quadro04Model.getAnexoJq04C407b() != null && quadro04Model.getAnexoJq04C407b() > 0 || quadro04Model.getAnexoJq04C408b() != null && quadro04Model.getAnexoJq04C408b() > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() != 2007 || quadro04Model.getAnexoJq04C410b() != null && quadro04Model.getAnexoJq04C410b() > 0 || quadro04Model.getAnexoJq04C418b() != null && quadro04Model.getAnexoJq04C418b() > 0 || quadro04Model.getAnexoJq04C420b() != null && quadro04Model.getAnexoJq04C420b() > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() != 2006 && rostoModel.getQuadro02().getQ02C02() != 2007 || quadro04Model.getAnexoJq04C422b() != null && quadro04Model.getAnexoJq04C422b() > 0 || quadro04Model.getAnexoJq04C423b() != null && quadro04Model.getAnexoJq04C423b() > 0 || quadro04Model.getAnexoJq04T2().size() > 0 || quadro04Model.getAnexoJq04C412b() != null && quadro04Model.getAnexoJq04C412b() > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() > 2012)) {
            result.add(new DeclValidationMessage("J129", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim")}));
        }
    }

    protected void validateJ011(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C401b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C401c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J011", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ131(ValidationResult result, AnexoJModel anexoJModel, Quadro04 quadro04Model) {
        if (Modelo3IRSv2015Parameters.instance().isFase2() && quadro04Model.getAnexoJq04B1() != null && (quadro04Model.getAnexoJq04C407b() == null || quadro04Model.getAnexoJq04C407b() == 0) && (quadro04Model.getAnexoJq04C408b() == null || quadro04Model.getAnexoJq04C408b() == 0) && (quadro04Model.getAnexoJq04C410b() == null || quadro04Model.getAnexoJq04C410b() == 0) && (quadro04Model.getAnexoJq04C418b() == null || quadro04Model.getAnexoJq04C418b() == 0) && (quadro04Model.getAnexoJq04C420b() == null || quadro04Model.getAnexoJq04C420b() == 0) && (quadro04Model.getAnexoJq04C422b() == null || quadro04Model.getAnexoJq04C422b() == 0) && (quadro04Model.getAnexoJq04C423b() == null || quadro04Model.getAnexoJq04C423b() == 0) && (quadro04Model.getAnexoJq04C412b() == null || quadro04Model.getAnexoJq04C412b() == 0) && quadro04Model.getAnexoJq04T2().size() == 0) {
            result.add(new DeclValidationMessage("J131", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04B1Sim"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423b")}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(anexoJModel.getAnexoJTitular());
            Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
            this.validateJ008(result, anexoJModel);
            this.validateJ013(result, anexoJModel);
            this.validateJ016(result, anexoJModel);
            this.validateJ102(result, anexoJModel);
            this.validateJ106(result, anexoJModel);
            this.validateJ113(result, anexoJModel);
            this.validateJ023(result, anexoJModel);
            this.validateJ026(result, anexoJModel);
            this.validateJ027(result, anexoJModel);
            this.validateJ028(result, anexoJModel);
            this.validateJ029(result, anexoJModel);
            this.validateJ033(result, anexoJModel);
            this.validateJ036(result, anexoJModel);
            this.validateJ043(result, anexoJModel);
            this.validateJ046(result, anexoJModel);
            this.validateJ049(result, anexoJModel);
            this.validateJ238(result, anexoCModel, anexoJModel);
            if (anoExercicio != null && Modelo3IRSValidatorUtil.in(anoExercicio, new long[]{2002, 2003, 2004, 2005})) {
                this.validateJ053(result, anexoJModel);
            }
            this.validateJ255(result, rostoModel, anexoJModel);
            this.validateJ223(result, rostoModel, anexoJModel);
            this.validateJ253(result, anexoJModel, anoExercicio);
            this.validateJ254(result, anexoJModel, anoExercicio);
            this.validateJ117(result, anexoJModel);
            this.validateJ110(result, anexoJModel);
            this.validateJ112(result, anexoJModel);
            this.validateJ203(result, anexoJModel);
            this.validateJ206(result, anexoJModel);
            this.validateJ056(result, anexoJModel);
            this.validateJ060(result, anexoJModel);
            this.validateJ211(result, anexoJModel);
            this.validateJ068(result, anexoJModel, rostoModel);
            if (anoExercicio != null && (anoExercicio == 2001 || anoExercicio == 2002)) {
                this.validateJ070(result, anexoJModel);
            } else {
                this.validateJ071(result, anexoJModel);
            }
            this.validateJ074(result, anexoJModel);
            this.validateJ063(result, anexoJModel);
            this.validateJ077(result, anexoJModel);
            this.validateJ083(result, anexoJModel);
            this.validateJ286(result, anexoJModel);
            this.validateJ287(result, anexoJModel);
            this.validateJ289(result, anexoJModel);
            this.validateJ288(result, anexoJModel);
            this.validateJ098(result, anexoJModel);
            Quadro04 quadro04 = anexoJModel.getQuadro04();
            this.validateJ018(result, quadro04, anexoJModel);
            this.validateJ019(result, quadro04, anexoJModel);
            this.validateJ021(result, quadro04, anexoJModel);
            this.valiidateJ024(result, quadro04, anexoJModel);
            this.validateJ025(result, quadro04, anexoJModel);
            this.validateJ031(result, quadro04, anexoJModel);
            this.validateJ032(result, quadro04, anexoJModel);
            this.validateJ034(result, quadro04, anexoJModel);
            this.validateJ038(result, quadro04, anexoJModel);
            this.validateJ041(result, quadro04, anexoJModel);
            this.validateJ044(result, quadro04, anexoJModel);
            this.validateJ047(result, quadro04, anexoJModel);
            this.validateJ048(result, quadro04, anexoJModel);
            this.validateJ050(result, quadro04, anexoJModel);
            this.validateJ115(result, quadro04, anexoJModel);
            this.validateJ116(result, quadro04, anexoJModel);
            this.validateJ108(result, quadro04, anexoJModel);
            this.validateJ109(result, quadro04, anexoJModel);
            this.validateJ111(result, quadro04, anexoJModel);
            this.validateJ200(result, quadro04, anexoJModel);
            this.validateJ202(result, quadro04, anexoJModel);
            this.validateJ205(result, quadro04, anexoJModel);
            this.validateJ054(result, quadro04, anexoJModel);
            this.validateJ055(result, quadro04, anexoJModel);
            this.validateJ057(result, quadro04, anexoJModel);
            this.validateJ208(result, quadro04, anexoJModel);
            this.validateJ210(result, quadro04, anexoJModel);
            this.validateJ067(result, quadro04, anexoJModel);
            this.validateJ069(result, quadro04, anexoJModel);
            this.validateJ072(result, quadro04, anexoJModel);
            this.validateJ061(result, quadro04, anexoJModel);
            this.validateJ062(result, quadro04, anexoJModel);
            this.validateJ075(result, quadro04, anexoJModel);
            this.validateJ076(result, quadro04, anexoJModel);
            this.validateJ081(result, quadro04, anexoJModel);
            this.validateJ082(result, quadro04, anexoJModel);
            this.validateJ096(result, quadro04, anexoJModel);
            this.validateJ097(result, quadro04, anexoJModel);
            this.validateJ125(result, quadro04, anexoJModel);
            this.validateJ128(result, anexoJModel);
            AnexoLModel anexoLModel = ((Modelo3IRSv2015Model)model).getAnexoLByTitular(anexoJModel.getAnexoJTitular());
            this.validateJ241(result, anexoJModel, anexoLModel);
        }
        return result;
    }

    protected void validateJ013(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C401b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C401d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C401d");
        if (impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && impostoPortugal > Math.round(0.465 * (double)rendimento.longValue())) {
            result.add(new DeclValidationMessage("J013", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ016(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C402b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C402c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C402c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J016", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ102(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C416b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C416c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C416c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J102", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ106(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C417b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C417c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C417c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J106", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ113(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C419b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C419c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C419c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J113", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ023(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C403b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C403d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && (double)impostoPortugal.longValue() > 0.265 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J023", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ026(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C404b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C404c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J026", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ027(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long impostoPortugal = quadro04Model.getAnexoJq04C404d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404d");
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || impostoPortugal == null || impostoPortugal == 0)) {
            result.add(new DeclValidationMessage("J027", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ028(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C404b();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b");
        Long impostoPortugal = quadro04Model.getAnexoJq04C404d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && (rendimento == null || rendimento == 0)) {
            result.add(new DeclValidationMessage("J028", impostoPortugalLink, new String[]{rendimentoLink, impostoPortugalLink}));
        }
    }

    protected void validateJ029(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C404b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C404d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && (double)impostoPortugal.longValue() > 0.265 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J029", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ033(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C405b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C405c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J033", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ036(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C405b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C405d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && (double)impostoPortugal.longValue() > 0.265 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J036", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ043(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C406b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C406c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J043", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ046(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C406b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C406d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && (double)impostoPortugal.longValue() > 0.17 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J046", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ049(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C407b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C407c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J049", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ053(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C407b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && (float)impostoPortugal.longValue() > (rendimento != null ? 0.155f * (float)rendimento.longValue() : 0.0f)) {
            result.add(new DeclValidationMessage("J053", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ255(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel) {
        Long periodo = rostoModel.getQuadro02().getQ02C02();
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro06 quadro06Model = anexoJModel.getQuadro06();
        Long rendimento = quadro04Model.getAnexoJq04C407b() != null ? quadro04Model.getAnexoJq04C407b() : 0;
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d() != null ? quadro04Model.getAnexoJq04C407d() : 0;
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal > 0 && periodo != null && periodo >= 2006 && periodo < 2010) {
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
                Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
                long[] codIdentificacaoPaisToValdiate = new long[]{276, 40, 56, 100, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 380, 428, 440, 442, 470, 528, 616, 203, 826, 642, 752};
                if (campoQ4 == null || campoQ4 != 407 || identificacaoPaisCodPais == null || !Modelo3IRSValidatorUtil.in(identificacaoPaisCodPais, codIdentificacaoPaisToValdiate) || (double)impostoPortugal.longValue() <= (double)rendimento.longValue() * 0.405) continue;
                result.add(new DeclValidationMessage("J255", impostoPortugalLink, new String[]{impostoPortugalLink}));
            }
        }
    }

    protected void validateJ223(ValidationResult result, RostoModel rostoModel, AnexoJModel anexoJModel) {
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro06 quadro06Model = anexoJModel.getQuadro06();
        Long rendimento = quadro04Model.getAnexoJq04C407b() != null ? quadro04Model.getAnexoJq04C407b() : 0;
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d() != null ? quadro04Model.getAnexoJq04C407d() : 0;
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && anoExercicio != null && anoExercicio >= 2009 && anoExercicio < 2012 && impostoPortugal > 0) {
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
                Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
                long[] codIdentificacaoPaisToValdiate = new long[]{276, 40, 56, 100, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 380, 428, 440, 442, 470, 528, 616, 203, 826, 642, 752};
                if (campoQ4 == null || campoQ4 != 407 || identificacaoPaisCodPais == null || !Modelo3IRSValidatorUtil.in(identificacaoPaisCodPais, codIdentificacaoPaisToValdiate) || (double)impostoPortugal.longValue() <= (double)rendimento.longValue() * 0.435) continue;
                result.add(new DeclValidationMessage("J223", impostoPortugalLink, new String[]{impostoPortugalLink}));
            }
        }
    }

    protected void validateJ253(ValidationResult result, AnexoJModel anexoJModel, Long anoExercicio) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro06 quadro06Model = anexoJModel.getQuadro06();
        Long rendimento = quadro04Model.getAnexoJq04C407b() != null ? quadro04Model.getAnexoJq04C407b() : 0;
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d() != null ? quadro04Model.getAnexoJq04C407d() : 0;
        if (anoExercicio != null && (anoExercicio > 2011 && anoExercicio < 2013 && impostoPortugal > 0 && (double)impostoPortugal.longValue() > (double)rendimento.longValue() * 0.266 || anoExercicio > 2012 && impostoPortugal > 0 && (double)impostoPortugal.longValue() > (double)rendimento.longValue() * 0.285)) {
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
                Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
                long[] codIdentificacaoPaisToValdiate = new long[]{276, 40, 56, 100, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 380, 428, 440, 442, 470, 528, 616, 203, 826, 642, 752};
                if (campoQ4 == null || campoQ4 != 407 || identificacaoPaisCodPais == null || Modelo3IRSValidatorUtil.in(identificacaoPaisCodPais, codIdentificacaoPaisToValdiate)) continue;
                String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
                result.add(new DeclValidationMessage("J253", impostoPortugalLink, new String[]{impostoPortugalLink}));
            }
        }
    }

    protected void validateJ254(ValidationResult result, AnexoJModel anexoJModel, Long anoExercicio) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro06 quadro06Model = anexoJModel.getQuadro06();
        Long rendimento = quadro04Model.getAnexoJq04C407b() != null ? quadro04Model.getAnexoJq04C407b() : 0;
        Long impostoPortugal = quadro04Model.getAnexoJq04C407d() != null ? quadro04Model.getAnexoJq04C407d() : 0;
        if (anoExercicio != null && anoExercicio > 2011 && impostoPortugal > 0 && (double)impostoPortugal.longValue() > (double)rendimento.longValue() * 0.57) {
            for (int linha = 0; linha < quadro06Model.getAnexoJq06T1().size(); ++linha) {
                AnexoJq06T1_Linha anexoJq06T1Linha = quadro06Model.getAnexoJq06T1().get(linha);
                Long campoQ4 = anexoJq06T1Linha.getCampoQ4();
                Long identificacaoPaisCodPais = anexoJq06T1Linha.getIdentificacaoPaisCodPais();
                long[] codIdentificacaoPaisToValdiate = new long[]{276, 40, 56, 100, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 380, 428, 440, 442, 470, 528, 616, 203, 826, 642, 752};
                if (campoQ4 == null || campoQ4 != 407 || identificacaoPaisCodPais == null || !Modelo3IRSValidatorUtil.in(identificacaoPaisCodPais, codIdentificacaoPaisToValdiate)) continue;
                String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407d");
                result.add(new DeclValidationMessage("J254", impostoPortugalLink, new String[]{impostoPortugalLink}));
            }
        }
    }

    protected void validateJ117(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C420b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C420c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C420c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J117", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ110(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C418b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C418c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J110", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ112(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C418b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C418d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C418d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && impostoPortugal > Math.round(0.285 * (double)rendimento.longValue())) {
            result.add(new DeclValidationMessage("J112", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ203(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C422b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C422c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue() && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("J203", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ206(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C422b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C422d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C422d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && rendimento != null && impostoPortugal > Math.round(0.285 * (double)rendimento.longValue())) {
            result.add(new DeclValidationMessage("J206", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ056(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C408b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C408c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J056", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ060(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C408b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C408d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C408d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && impostoPortugal > Math.round(0.285 * (double)rendimento.longValue())) {
            result.add(new DeclValidationMessage("J060", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ211(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C423b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C423c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C423c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue() && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("J211", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ071(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C410b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C410c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J071", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ070(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410b");
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C410c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410c");
        if (impostoEstrangeiro != null && impostoEstrangeiro != 0) {
            result.add(new DeclValidationMessage("J070", rendimentoLink, new String[]{"aRosto.qQuadro02.fq02C02", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C407c"), impostoEstrangeiroLink}));
        }
    }

    protected void validateJ074(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C410b();
        Long impostoPortugal = quadro04Model.getAnexoJq04C410d();
        String impostoPortugalLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C410d");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoPortugal != null && impostoPortugal > 0 && rendimento != null && rendimento > 0 && impostoPortugal > Math.round(0.285 * (double)rendimento.longValue())) {
            result.add(new DeclValidationMessage("J074", impostoPortugalLink, new String[]{impostoPortugalLink}));
        }
    }

    protected void validateJ063(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C409b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C409c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C409c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J063", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ077(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C411b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C411c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C411c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J077", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ083(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C412b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C412c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C412c");
        if (Modelo3IRSv2015Parameters.instance().isFase2() && impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J083", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ286(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        String rendimentoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425b");
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04Model.getAnexoJq04C425b()) || Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J286", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425b"), new String[]{rendimentoLink}));
        }
    }

    protected void validateJ287(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        String impostoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425c");
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04Model.getAnexoJq04C425c()) || Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J287", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425c"), new String[]{impostoLink}));
        }
    }

    protected void validateJ289(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        String impostoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425c");
        if (quadro04Model != null && quadro04Model.getAnexoJq04C425c() != null && quadro04Model.getAnexoJq04C425b() != null && (double)quadro04Model.getAnexoJq04C425c().longValue() > 0.5 * (double)quadro04Model.getAnexoJq04C425b().longValue() && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("J289", impostoLink, new String[]{impostoLink}));
        }
    }

    protected void validateJ288(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        String impostoRetidoLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425d");
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04Model.getAnexoJq04C425d())) {
            result.add(new DeclValidationMessage("J288", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C425d"), new String[]{impostoRetidoLink}));
        }
    }

    protected void validateJ098(ValidationResult result, AnexoJModel anexoJModel) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Long rendimento = quadro04Model.getAnexoJq04C415b();
        Long impostoEstrangeiro = quadro04Model.getAnexoJq04C415c();
        String impostoEstrangeiroLink = anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C415c");
        if (impostoEstrangeiro != null && rendimento != null && (double)impostoEstrangeiro.longValue() > 0.5 * (double)rendimento.longValue()) {
            result.add(new DeclValidationMessage("J098", impostoEstrangeiroLink, new String[]{impostoEstrangeiroLink}));
        }
    }

    protected void validateJ199(ValidationResult result, AnexoBModel anexoBModel, AnexoCModel anexoCModel, AnexoJModel anexoJModel) {
        boolean anoMaior2013 = anexoJModel.getQuadro02().getAnexoJq02C01() != null && anexoJModel.getQuadro02().getAnexoJq02C01() > 2013;
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        if ((quadro04Model.getAnexoJq04C403b() != null && quadro04Model.getAnexoJq04C403b() > 0 || quadro04Model.getAnexoJq04C404b() != null && quadro04Model.getAnexoJq04C404b() > 0 || quadro04Model.getAnexoJq04C405b() != null && quadro04Model.getAnexoJq04C405b() > 0 || quadro04Model.getAnexoJq04C406b() != null && quadro04Model.getAnexoJq04C406b() > 0 || quadro04Model.getAnexoJq04C426b() != null && quadro04Model.getAnexoJq04C426b() > 0 && anoMaior2013) && anexoBModel == null && anexoCModel == null) {
            result.add(new DeclValidationMessage("J199", anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b")}));
        }
    }

    protected void validateJ237(ValidationResult result, AnexoBModel anexoBModel, AnexoJModel anexoJModel) {
        if (!(anexoBModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoJModel.getQuadro03().getAnexoJq03C04()) || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoBModel.getQuadro03().getAnexoBq03C08()) || anexoJModel.getQuadro03().getAnexoJq03C04().longValue() != anexoBModel.getQuadro03().getAnexoBq03C08().longValue())) {
            long c1102;
            Quadro11 quadro11AnxB = anexoBModel.getQuadro11();
            Quadro04 quadro04AnxJ = anexoJModel.getQuadro04();
            long c403 = quadro04AnxJ.getAnexoJq04C403b() != null ? quadro04AnxJ.getAnexoJq04C403b() : 0;
            long c404 = quadro04AnxJ.getAnexoJq04C404b() != null ? quadro04AnxJ.getAnexoJq04C404b() : 0;
            long c405 = quadro04AnxJ.getAnexoJq04C405b() != null ? quadro04AnxJ.getAnexoJq04C405b() : 0;
            long c406 = quadro04AnxJ.getAnexoJq04C406b() != null ? quadro04AnxJ.getAnexoJq04C406b() : 0;
            long c421 = quadro04AnxJ.getAnexoJq04C421() != null ? quadro04AnxJ.getAnexoJq04C421() : 0;
            long c1101 = quadro11AnxB.getAnexoBq11C1101() != null ? quadro11AnxB.getAnexoBq11C1101() : 0;
            long l = c1102 = quadro11AnxB.getAnexoBq11C1102() != null ? quadro11AnxB.getAnexoBq11C1102() : 0;
            if (c403 + c404 + c405 + c406 + c421 > c1101 + c1102) {
                result.add(new DeclValidationMessage("J237", anexoJModel.getLink("aAnexoJ.qQuadro04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421"), anexoBModel.getLink("aAnexoB.qQuadro11")}));
            }
        }
    }

    protected void validateJ238(ValidationResult result, AnexoCModel anexoCModel, AnexoJModel anexoJModel) {
        if (!(anexoCModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoJModel.getQuadro03().getAnexoJq03C04()) || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro03().getAnexoCq03C06()) || anexoJModel.getQuadro03().getAnexoJq03C04().longValue() != anexoCModel.getQuadro03().getAnexoCq03C06().longValue())) {
            long c1202;
            Quadro12 quadro11AnxC = anexoCModel.getQuadro12();
            Quadro04 quadro04AnxJ = anexoJModel.getQuadro04();
            long c403 = quadro04AnxJ.getAnexoJq04C403b() != null ? quadro04AnxJ.getAnexoJq04C403b() : 0;
            long c404 = quadro04AnxJ.getAnexoJq04C404b() != null ? quadro04AnxJ.getAnexoJq04C404b() : 0;
            long c405 = quadro04AnxJ.getAnexoJq04C405b() != null ? quadro04AnxJ.getAnexoJq04C405b() : 0;
            long c406 = quadro04AnxJ.getAnexoJq04C406b() != null ? quadro04AnxJ.getAnexoJq04C406b() : 0;
            long c421 = quadro04AnxJ.getAnexoJq04C421() != null ? quadro04AnxJ.getAnexoJq04C421() : 0;
            long c1201 = quadro11AnxC.getAnexoCq12C1201() != null ? quadro11AnxC.getAnexoCq12C1201() : 0;
            long l = c1202 = quadro11AnxC.getAnexoCq12C1202() != null ? quadro11AnxC.getAnexoCq12C1202() : 0;
            if (c403 + c404 + c405 + c406 + c421 > c1201 + c1202) {
                result.add(new DeclValidationMessage("J238", anexoJModel.getLink("aAnexoJ.qQuadro04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C421"), anexoCModel.getLink("aAnexoC.qQuadro12")}));
            }
        }
    }

    protected void validateJ241(ValidationResult result, AnexoJModel anexoJModel, AnexoLModel anexoLModel) {
        boolean sameTitulares;
        if (anexoJModel == null || anexoLModel == null) {
            return;
        }
        long totalRendimentos = anexoJModel.getQuadro04().getTotalRendimentoPreenchido();
        Long titularAnexoJ = anexoJModel.getAnexoJTitular();
        Long titularAnexoL = anexoLModel.getAnexoLTitular();
        boolean bl = sameTitulares = titularAnexoJ != null && titularAnexoL != null && titularAnexoJ.equals(titularAnexoL);
        if (totalRendimentos > 0 && sameTitulares && StringUtil.isEmpty(anexoLModel.getQuadro06().getQ06B2())) {
            result.add(new DeclValidationMessage("J241", anexoJModel.getLink("aAnexoJ.qQuadro04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04"), anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP3"), anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP4")}));
        }
    }

    public static void validateJ242(DeclaracaoModel model, AnexoJModel anexoJModel, ValidationResult result) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro03 quadro03Model = anexoJModel.getQuadro03();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long titular = quadro03Model.getAnexoJq03C04();
        if (titular != null) {
            Long rendimento = quadro04Model.getAnexoJq04C403b();
            Long ano = rostoModel.getQuadro02().getQ02C02();
            AnexoBModel anexoB = (AnexoBModel)model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(titular)));
            AnexoCModel anexoC = (AnexoCModel)model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(titular)));
            if (!(rendimento == null || rendimento <= 0 || ano == null || ano <= 2010 || (anexoB == null || anexoB.getQuadro01().isAnexoBq01B3Selected()) && (anexoC == null || anexoC.getQuadro01().isAnexoCq01B1Selected()))) {
                result.add(new DeclValidationMessage("J242", anexoJModel.getLink("aAnexoJ.qQuadro04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b")}));
            }
        }
    }

    public static void validateJ243(DeclaracaoModel model, AnexoJModel anexoJModel, ValidationResult result) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro03 quadro03Model = anexoJModel.getQuadro03();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long titular = quadro03Model.getAnexoJq03C04();
        if (titular != null) {
            Long rendimento = quadro04Model.getAnexoJq04C404b();
            Long ano = rostoModel.getQuadro02().getQ02C02();
            AnexoBModel anexoB = (AnexoBModel)model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(titular)));
            AnexoCModel anexoC = (AnexoCModel)model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(titular)));
            if (!(rendimento == null || rendimento <= 0 || ano == null || ano <= 2010 || (anexoB == null || anexoB.getQuadro01().isAnexoBq01B3Selected()) && (anexoC == null || anexoC.getQuadro01().isAnexoCq01B1Selected()))) {
                result.add(new DeclValidationMessage("J243", anexoJModel.getLink("aAnexoJ.qQuadro04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b")}));
            }
        }
    }

    public static void validateJ244(DeclaracaoModel model, AnexoJModel anexoJModel, ValidationResult result) {
        Quadro04 quadro04Model = anexoJModel.getQuadro04();
        Quadro03 quadro03Model = anexoJModel.getQuadro03();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long titular = quadro03Model.getAnexoJq03C04();
        if (titular != null) {
            Long rendimento = quadro04Model.getAnexoJq04C405b();
            Long ano = rostoModel.getQuadro02().getQ02C02();
            AnexoBModel anexoB = (AnexoBModel)model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(titular)));
            AnexoCModel anexoC = (AnexoCModel)model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(titular)));
            if (!(rendimento == null || rendimento <= 0 || ano == null || ano <= 2010 || (anexoB == null || anexoB.getQuadro01().isAnexoBq01B4Selected()) && (anexoC == null || anexoC.getQuadro01().isAnexoCq01B2Selected()))) {
                result.add(new DeclValidationMessage("J244", anexoJModel.getLink("aAnexoJ.qQuadro04"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b")}));
            }
        }
    }

    private void validateAnexoJq04T1(DeclaracaoModel model, RostoModel rostoModel, AnexoJModel anexoJModel, Quadro04 quadro04Model, ValidationResult result, EventList<AnexoJq04T1_Linha> anexoJq04T1) {
        this.validateJ284(result, anexoJModel);
        this.validateJ306(anexoJq04T1, result, anexoJModel);
        for (int i = 0; i < anexoJq04T1.size(); ++i) {
            AnexoJq04T1_Linha linha = anexoJq04T1.get(i);
            String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro04.tanexoJq04T1.l" + (i + 1));
            String linkAnoRealizacao = lineLink + ".c" + AnexoJq04T1_LinhaBase.Property.ANOREALIZACAO.getIndex();
            String linkMesRealizacao = lineLink + ".c" + AnexoJq04T1_LinhaBase.Property.MESREALIZACAO.getIndex();
            String linkValorRealizacao = lineLink + ".c" + AnexoJq04T1_LinhaBase.Property.VALORREALIZACAO.getIndex();
            String linkAnoAquisicao = lineLink + ".c" + AnexoJq04T1_LinhaBase.Property.ANOAQUISICAO.getIndex();
            String linkMesAquisicao = lineLink + ".c" + AnexoJq04T1_LinhaBase.Property.MESAQUISICAO.getIndex();
            String linkValorAquisicao = lineLink + ".c" + AnexoJq04T1_LinhaBase.Property.VALORAQUISICAO.getIndex();
            String linkCodigoDoPais = lineLink + ".c" + AnexoJq04T1_LinhaBase.Property.CODIGODOPAIS.getIndex();
            this.validateJ258(linha, lineLink, result);
            this.validateJ259(linha, lineLink, result, linkMesRealizacao, linkValorRealizacao, linkAnoAquisicao, linkMesAquisicao, linkValorAquisicao, linkCodigoDoPais);
            this.validateJ260(linha, lineLink, result, linkAnoRealizacao);
            this.validateJ261(linha, lineLink, result, linkAnoAquisicao, linkMesAquisicao, linkAnoRealizacao, linkMesRealizacao);
            this.validateJ262(linha, lineLink, rostoModel, result, linkAnoRealizacao);
            this.validateJ263(linha, lineLink, result, linkAnoRealizacao);
            this.validateJ264(linha, lineLink, result, linkMesRealizacao);
            this.validateJ265(linha, lineLink, result, linkAnoAquisicao);
            this.validateJ266(linha, lineLink, result, linkAnoAquisicao);
            this.validateJ267(linha, lineLink, result, linkMesAquisicao);
            this.validateJ268(linha, lineLink, result, linkCodigoDoPais);
        }
    }

    protected void validateJ258(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result) {
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDespesasEncargos()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getImpostoPagoNoEstrangeiro()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCodigoDoPais())) {
            result.add(new DeclValidationMessage("J258", lineLink, new String[]{lineLink}));
        }
    }

    protected void validateJ259(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkMesRealizacao, String linkValorRealizacao, String linkAnoAquisicao, String linkMesAquisicao, String linkValorAquisicao, String linkCodigoDoPais) {
        if (linha.getAnoRealizacao() != null && Modelo3IRSValidatorUtil.isValidYear(linha.getAnoRealizacao()) && (linha.getMesRealizacao() == null || linha.getValorRealizacao() == null || linha.getAnoAquisicao() == null || linha.getMesAquisicao() == null || linha.getValorAquisicao() == null || linha.getCodigoDoPais() == null)) {
            result.add(new DeclValidationMessage("J259", lineLink, new String[]{linkMesRealizacao, linkValorRealizacao, linkAnoAquisicao, linkMesAquisicao, linkValorAquisicao, linkCodigoDoPais}));
        }
    }

    protected void validateJ260(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkAnoRealizacao) {
        if ((linha.getMesRealizacao() != null || linha.getValorRealizacao() != null || linha.getAnoAquisicao() != null || linha.getMesAquisicao() != null || linha.getValorAquisicao() != null || linha.getDespesasEncargos() != null || linha.getImpostoPagoNoEstrangeiro() != null || linha.getCodigoDoPais() != null) && linha.getAnoRealizacao() == null) {
            result.add(new DeclValidationMessage("J260", lineLink, new String[]{linkAnoRealizacao}));
        }
    }

    protected void validateJ261(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkAnoAquisicao, String linkMesAquisicao, String linkAnoRealizacao, String linkMesRealizacao) {
        Long anoRealizacao = linha.getAnoRealizacao();
        Long mesRealizacao = linha.getMesRealizacao();
        Long anoAquisicao = linha.getAnoAquisicao();
        Long mesAquisicao = linha.getMesAquisicao();
        boolean hasAnoRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoRealizacao);
        boolean hasMesRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesRealizacao);
        boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao);
        boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao);
        if ((hasAnoAquisicao ? anoAquisicao : 0) > (hasAnoRealizacao ? anoRealizacao : 0) || (hasAnoAquisicao ? anoAquisicao : 0) == (hasAnoRealizacao ? anoRealizacao : 0) && (hasMesAquisicao ? mesAquisicao : 0) > (hasMesRealizacao ? mesRealizacao : 0)) {
            result.add(new DeclValidationMessage("J261", lineLink, new String[]{linkAnoAquisicao, linkMesAquisicao, linkAnoRealizacao, linkMesRealizacao}));
        }
    }

    protected void validateJ262(AnexoJq04T1_Linha linha, String lineLink, RostoModel rostoModel, ValidationResult result, String linkAnoRealizacao) {
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) || !Modelo3IRSValidatorUtil.isValidYear(linha.getAnoRealizacao()) || Modelo3IRSValidatorUtil.equals(anoExercicio, linha.getAnoRealizacao()))) {
            result.add(new DeclValidationMessage("J262", lineLink, new String[]{linkAnoRealizacao}));
        }
    }

    protected void validateJ284(ValidationResult result, AnexoJModel anexoJmodel) {
        Quadro04 quadro04 = anexoJmodel.getQuadro04();
        if (!(quadro04.getAnexoJq04T1().size() <= 0 || Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J284", anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T1"), new String[]{anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T1")}));
        }
    }

    protected void validateJ285(ValidationResult result, AnexoJModel anexoJmodel) {
        Quadro04 quadro04 = anexoJmodel.getQuadro04();
        if (!(quadro04.getAnexoJq04T2().size() <= 0 || Modelo3IRSv2015Parameters.instance().isFase2())) {
            result.add(new DeclValidationMessage("J285", anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T2"), new String[]{anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T2")}));
        }
    }

    protected void validateJ305(EventList<AnexoJq04T2_Linha> eventList, ValidationResult result, AnexoJModel anexoJmodel) {
        if (eventList.size() > 240) {
            result.add(new DeclValidationMessage("J305", anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T2"), new String[]{anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T2")}));
        }
    }

    protected void validateJ263(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkAnoRealizacao) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) || Modelo3IRSValidatorUtil.isValidYear(linha.getAnoRealizacao()))) {
            result.add(new DeclValidationMessage("J263", lineLink, new String[]{linkAnoRealizacao}));
        }
    }

    protected void validateJ264(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkMesRealizacao) {
        if (linha.getMesRealizacao() != null && (linha.getMesRealizacao() < 1 || linha.getMesRealizacao() > 12)) {
            result.add(new DeclValidationMessage("J264", lineLink, new String[]{linkMesRealizacao}));
        }
    }

    protected void validateJ265(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkAnoAquisicao) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) && Year.isYear(String.valueOf(linha.getAnoAquisicao())) && linha.getAnoAquisicao() < 1900) {
            result.add(new DeclValidationMessage("J265", lineLink, new String[]{linkAnoAquisicao}));
        }
    }

    protected void validateJ266(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkAnoAquisicao) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) || Modelo3IRSValidatorUtil.isValidYear(linha.getAnoAquisicao()))) {
            result.add(new DeclValidationMessage("J266", lineLink, new String[]{linkAnoAquisicao}));
        }
    }

    protected void validateJ267(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkMesAquisicao) {
        if (linha.getMesAquisicao() != null && (linha.getMesAquisicao() < 1 || linha.getMesAquisicao() > 12)) {
            result.add(new DeclValidationMessage("J267", lineLink, new String[]{linkMesAquisicao}));
        }
    }

    protected void validateJ268(AnexoJq04T1_Linha linha, String lineLink, ValidationResult result, String linkCodigoDoPais) {
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxJ.class.getSimpleName());
        if (!(linha.getCodigoDoPais() == null || paisesCatalog.containsKey(linha.getCodigoDoPais()))) {
            result.add(new DeclValidationMessage("J268", lineLink, new String[]{linkCodigoDoPais}));
        }
    }

    protected void validateJ306(EventList<AnexoJq04T1_Linha> eventList, ValidationResult result, AnexoJModel anexoJmodel) {
        if (eventList.size() > 240) {
            result.add(new DeclValidationMessage("J306", anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T1"), new String[]{anexoJmodel.getLink("aAnexoJ.qQuadro04.tanexoJq04T1")}));
        }
    }

    private void validateAnexoJq04T2(DeclaracaoModel model, RostoModel rostoModel, AnexoJModel anexoJModel, Quadro04 quadro04Model, ValidationResult result, EventList<AnexoJq04T2_Linha> anexoJq04T2) {
        this.validateJ285(result, anexoJModel);
        this.validateJ305(anexoJq04T2, result, anexoJModel);
        for (int i = 0; i < anexoJq04T2.size(); ++i) {
            AnexoJq04T2_Linha linha = anexoJq04T2.get(i);
            String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro04.tanexoJq04T2.l" + (i + 1));
            String linkCodigos = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.CODIGOS.getIndex();
            String linkAnoRealizacao = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.ANOREALIZACAO.getIndex();
            String linkMesRealizacao = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.MESREALIZACAO.getIndex();
            String linkValorRealizacao = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.VALORREALIZACAO.getIndex();
            String linkAnoAquisicao = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.ANOAQUISICAO.getIndex();
            String linkMesAquisicao = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.MESAQUISICAO.getIndex();
            String linkValorAquisicao = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.VALORAQUISICAO.getIndex();
            String linkCodigoDoPais = lineLink + ".c" + AnexoJq04T2_LinhaBase.Property.CODIGODOPAIS.getIndex();
            this.validateJ269(linha, lineLink, result);
            this.validateJ270(linha, lineLink, result, linkAnoRealizacao, linkMesRealizacao, linkValorRealizacao, linkAnoAquisicao, linkMesAquisicao, linkValorAquisicao, linkCodigoDoPais);
            this.validateJ271(linha, lineLink, result, linkCodigos);
            this.validateJ272(linha, lineLink, result, linkAnoAquisicao, linkMesAquisicao, linkAnoRealizacao, linkMesRealizacao);
            this.validateJ273(linha, lineLink, result, linkCodigos);
            this.validateJ274(linha, lineLink, rostoModel, result, linkAnoRealizacao);
            this.validateJ275(linha, lineLink, result, linkAnoRealizacao);
            this.validateJ276(linha, lineLink, result, linkMesRealizacao);
            this.validateJ277(linha, lineLink, result, linkAnoAquisicao);
            this.validateJ278(linha, lineLink, result, linkAnoAquisicao);
            this.validateJ279(linha, lineLink, result, linkMesAquisicao);
            this.validateJ280(linha, lineLink, result, linkCodigoDoPais);
        }
    }

    protected void validateJ269(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result) {
        if (linha.getCodigos() == null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDespesasEncargos()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getImpostoPagoNoEstrangeiro()) && linha.getCodigoDoPais() == null) {
            result.add(new DeclValidationMessage("J269", lineLink, new String[]{lineLink}));
        }
    }

    protected void validateJ270(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkAnoRealizacao, String linkMesRealizacao, String linkValorRealizacao, String linkAnoAquisicao, String linkMesAquisicao, String linkValorAquisicao, String linkCodigoDoPais) {
        if (linha.getCodigos() != null && (linha.getAnoRealizacao() == null || linha.getMesRealizacao() == null || linha.getValorRealizacao() == null || linha.getAnoAquisicao() == null || linha.getMesAquisicao() == null || linha.getValorAquisicao() == null || linha.getCodigoDoPais() == null)) {
            result.add(new DeclValidationMessage("J270", lineLink, new String[]{linkAnoRealizacao, linkMesRealizacao, linkValorRealizacao, linkAnoAquisicao, linkMesAquisicao, linkValorAquisicao, linkCodigoDoPais}));
        }
    }

    protected void validateJ271(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkCodigos) {
        if ((linha.getAnoRealizacao() != null || linha.getMesRealizacao() != null || linha.getValorRealizacao() != null || linha.getAnoAquisicao() != null || linha.getMesAquisicao() != null || linha.getValorAquisicao() != null || linha.getDespesasEncargos() != null || linha.getImpostoPagoNoEstrangeiro() != null || linha.getCodigoDoPais() != null) && linha.getCodigos() == null) {
            result.add(new DeclValidationMessage("J271", lineLink, new String[]{linkCodigos}));
        }
    }

    protected void validateJ272(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkAnoAquisicao, String linkMesAquisicao, String linkAnoRealizacao, String linkMesRealizacao) {
        Long anoRealizacao = linha.getAnoRealizacao();
        Long mesRealizacao = linha.getMesRealizacao();
        Long anoAquisicao = linha.getAnoAquisicao();
        Long mesAquisicao = linha.getMesAquisicao();
        boolean hasAnoRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoRealizacao);
        boolean hasMesRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesRealizacao);
        boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao);
        boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao);
        if ((hasAnoAquisicao ? anoAquisicao : 0) > (hasAnoRealizacao ? anoRealizacao : 0) || (hasAnoAquisicao ? anoAquisicao : 0) == (hasAnoRealizacao ? anoRealizacao : 0) && (hasMesAquisicao ? mesAquisicao : 0) > (hasMesRealizacao ? mesRealizacao : 0)) {
            result.add(new DeclValidationMessage("J272", lineLink, new String[]{linkAnoAquisicao, linkMesAquisicao, linkAnoRealizacao, linkMesRealizacao}));
        }
    }

    protected void validateJ273(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkCodigos) {
        HashSet<Long> supportedCodes = new HashSet<Long>(Arrays.asList(1, 2, 3, 4));
        if (!(linha.getCodigos() == null || supportedCodes.contains(linha.getCodigos()))) {
            result.add(new DeclValidationMessage("J273", lineLink, new String[]{linkCodigos}));
        }
    }

    protected void validateJ274(AnexoJq04T2_Linha linha, String lineLink, RostoModel rostoModel, ValidationResult result, String linkAnoRealizacao) {
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) || !Modelo3IRSValidatorUtil.isValidYear(linha.getAnoRealizacao()) || Modelo3IRSValidatorUtil.equals(anoExercicio, linha.getAnoRealizacao()))) {
            result.add(new DeclValidationMessage("J274", lineLink, new String[]{linkAnoRealizacao}));
        }
    }

    protected void validateJ275(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkAnoRealizacao) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) || Modelo3IRSValidatorUtil.isValidYear(linha.getAnoRealizacao()))) {
            result.add(new DeclValidationMessage("J275", lineLink, new String[]{linkAnoRealizacao}));
        }
    }

    protected void validateJ276(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkMesRealizacao) {
        if (linha.getMesRealizacao() != null && (linha.getMesRealizacao() < 1 || linha.getMesRealizacao() > 12)) {
            result.add(new DeclValidationMessage("J276", lineLink, new String[]{linkMesRealizacao}));
        }
    }

    protected void validateJ277(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkAnoAquisicao) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) && Year.isYear(String.valueOf(linha.getAnoAquisicao())) && linha.getAnoAquisicao() < 1900) {
            result.add(new DeclValidationMessage("J277", lineLink, new String[]{linkAnoAquisicao}));
        }
    }

    protected void validateJ278(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkAnoAquisicao) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) || Modelo3IRSValidatorUtil.isValidYear(linha.getAnoAquisicao()))) {
            result.add(new DeclValidationMessage("J278", lineLink, new String[]{linkAnoAquisicao}));
        }
    }

    protected void validateJ279(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkMesAquisicao) {
        if (linha.getMesAquisicao() != null && (linha.getMesAquisicao() < 1 || linha.getMesAquisicao() > 12)) {
            result.add(new DeclValidationMessage("J279", lineLink, new String[]{linkMesAquisicao}));
        }
    }

    protected void validateJ280(AnexoJq04T2_Linha linha, String lineLink, ValidationResult result, String linkCodigoDoPais) {
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxJ.class.getSimpleName());
        if (!(linha.getCodigoDoPais() == null || paisesCatalog.containsKey(linha.getCodigoDoPais()))) {
            result.add(new DeclValidationMessage("J280", lineLink, new String[]{linkCodigoDoPais}));
        }
    }
}

