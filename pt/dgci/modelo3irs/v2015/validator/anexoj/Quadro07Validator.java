/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.Tuple;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro07 quadro07Model = anexoJModel.getQuadro07();
            Quadro06 quadro06Model = anexoJModel.getQuadro06();
            if (quadro07Model.getAnexoJq07T1().size() > 10) {
                result.add(new DeclValidationMessage("J166", anexoJModel.getLink("aAnexoJ.qQuadro07"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro07")}));
            }
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            ArrayList<Tuple> processedNlinhasQ6 = new ArrayList<Tuple>();
            for (int linha = 0; linha < quadro07Model.getAnexoJq07T1().size(); ++linha) {
                AnexoJq07T1_Linha anexoJq07T1Linha = quadro07Model.getAnexoJq07T1().get(linha);
                Long campoNLinhaQ6 = anexoJq07T1Linha.getCampoNLinhaQ6();
                Long rendimento = anexoJq07T1Linha.getRendimento();
                Long numAnos = anexoJq07T1Linha.getNanos();
                String lineLink = anexoJModel.getLink("aAnexoJ.qQuadro07.tanexoJq07T1.l" + (linha + 1));
                String campoNLinhaQ6Link = lineLink + ".c" + AnexoJq07T1_LinhaBase.Property.CAMPONLINHAQ6.getIndex();
                String rendimentoLink = lineLink + ".c" + AnexoJq07T1_LinhaBase.Property.RENDIMENTO.getIndex();
                if (campoNLinhaQ6 != null) {
                    Tuple processedCampoNLinhaQ6 = new Tuple(new Long[]{campoNLinhaQ6});
                    if (processedNlinhasQ6.contains(processedCampoNLinhaQ6)) {
                        result.add(new DeclValidationMessage("J167", lineLink, new String[]{lineLink}));
                    } else {
                        processedNlinhasQ6.add(processedCampoNLinhaQ6);
                    }
                    if (rendimento != null && numAnos != null) {
                        Tuple processedTuple = new Tuple(new Long[]{campoNLinhaQ6, rendimento, numAnos});
                        if (processedTuples.contains(processedTuple) && !processedNlinhasQ6.contains(processedCampoNLinhaQ6)) {
                            result.add(new DeclValidationMessage("J167", lineLink, new String[]{lineLink}));
                        } else {
                            processedTuples.add(processedTuple);
                        }
                    }
                }
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(campoNLinhaQ6) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento) && Modelo3IRSValidatorUtil.isEmptyOrZero(numAnos)) {
                    result.add(new DeclValidationMessage("J168", lineLink, new String[]{lineLink}));
                }
                this.validateJ169(result, anexoJq07T1Linha, anexoJModel, linha + 1);
                this.validateJ170(result, anexoJq07T1Linha, anexoJModel, linha + 1);
                if (campoNLinhaQ6 == null && (rendimento != null || numAnos != null)) {
                    result.add(new DeclValidationMessage("J171", campoNLinhaQ6Link, new String[]{campoNLinhaQ6Link}));
                }
                Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
                if (rendimento != null && rendimento > 0 && anoExercicio != null && anoExercicio < 2006) {
                    result.add(new DeclValidationMessage("J172", rendimentoLink, new String[]{rendimentoLink, "aRosto.qQuadro02.fq02C02"}));
                }
                this.validateJ228(result, anexoJModel, quadro06Model, anexoJq07T1Linha, linha + 1);
                this.validateJ239(result, anexoJModel, quadro06Model, anexoJq07T1Linha, linha + 1);
            }
        }
        return result;
    }

    protected void validateJ170(ValidationResult result, AnexoJq07T1_Linha current, AnexoJModel anexoJModel, int numLinha) {
        long[] codigos = new long[]{601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630};
        Long rendimento = current.getRendimento();
        Long numAnos = current.getNanos();
        if (current.getCampoNLinhaQ6() != null && Modelo3IRSValidatorUtil.in(current.getCampoNLinhaQ6(), codigos) && (rendimento == null || rendimento == 0 || numAnos == null || numAnos == 0)) {
            String campoNLinhaQ6Link = anexoJModel.getLink(AnexoJq07T1_Linha.getLink(numLinha, AnexoJq07T1_LinhaBase.Property.CAMPONLINHAQ6));
            String rendimentoLink = anexoJModel.getLink(AnexoJq07T1_Linha.getLink(numLinha, AnexoJq07T1_LinhaBase.Property.RENDIMENTO));
            String numAnosLink = anexoJModel.getLink(AnexoJq07T1_Linha.getLink(numLinha, AnexoJq07T1_LinhaBase.Property.NANOS));
            result.add(new DeclValidationMessage("J170", campoNLinhaQ6Link, new String[]{campoNLinhaQ6Link, rendimentoLink, numAnosLink}));
        }
    }

    protected void validateJ169(ValidationResult result, AnexoJq07T1_Linha current, AnexoJModel anexoJmodel, int numLinha) {
        long[] codigos = new long[]{601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663};
        if (!(current.getCampoNLinhaQ6() == null || Modelo3IRSValidatorUtil.in(current.getCampoNLinhaQ6(), codigos))) {
            String campoNLinhaQ6Link = anexoJmodel.getLink(AnexoJq07T1_Linha.getLink(numLinha, AnexoJq07T1_LinhaBase.Property.CAMPONLINHAQ6));
            result.add(new DeclValidationMessage("J169", campoNLinhaQ6Link, new String[]{campoNLinhaQ6Link}));
        }
    }

    public void validateJ228(ValidationResult result, AnexoJModel model, Quadro06 quadro06Model, AnexoJq07T1_Linha current, int numLinha) {
        EventList<AnexoJq06T1_Linha> tabelaQuadro06 = quadro06Model.getAnexoJq06T1();
        if (current.getCampoNLinhaQ6() != null && this.hasOcorrenciaQ6codigosIrregulares(tabelaQuadro06, current)) {
            result.add(new DeclValidationMessage("J228", model.getLink("aAnexoJ.qQuadro07"), new String[]{model.getLink("aAnexoJ.qQuadro07")}));
        }
    }

    protected boolean hasOcorrenciaQ6(EventList<AnexoJq06T1_Linha> linhas, AnexoJq07T1_Linha current) {
        for (AnexoJq06T1_Linha linha : linhas) {
            Long nLinha = linha.getNLinha();
            if (nLinha == null || !nLinha.equals(current.getCampoNLinhaQ6()) || linha.getCampoQ4() == null || !Modelo3IRSValidatorUtil.in(linha.getCampoQ4(), new long[]{401, 402, 412, 416, 417})) continue;
            return true;
        }
        return false;
    }

    protected boolean hasOcorrenciaQ6codigosIrregulares(EventList<AnexoJq06T1_Linha> linhas, AnexoJq07T1_Linha current) {
        for (AnexoJq06T1_Linha linha : linhas) {
            Long nLinha = linha.getNLinha();
            if (nLinha == null || !nLinha.equals(current.getCampoNLinhaQ6()) || linha.getCampoQ4() == null || Modelo3IRSValidatorUtil.in(linha.getCampoQ4(), new long[]{401, 402, 412, 416, 417})) continue;
            return true;
        }
        return false;
    }

    protected AnexoJq06T1_Linha getOcorrenciaQ6Codigos(EventList<AnexoJq06T1_Linha> linhas, AnexoJq07T1_Linha current, long[] codigos) {
        for (AnexoJq06T1_Linha linha : linhas) {
            Long nLinha = linha.getNLinha();
            if (nLinha == null || current == null || !nLinha.equals(current.getCampoNLinhaQ6()) || linha.getCampoQ4() == null || !Modelo3IRSValidatorUtil.in(linha.getCampoQ4(), codigos)) continue;
            return linha;
        }
        return null;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            Quadro06 quadro06Model = anexoJModel.getQuadro06();
            Quadro07 quadro07Model = anexoJModel.getQuadro07();
            for (int linha = 0; linha < quadro07Model.getAnexoJq07T1().size(); ++linha) {
                AnexoJq07T1_Linha anexoJq07T1Linha = quadro07Model.getAnexoJq07T1().get(linha);
                this.validateJ214(result, anexoJModel, quadro06Model, anexoJq07T1Linha, linha + 1);
            }
        }
        return result;
    }

    protected void validateJ214(ValidationResult result, AnexoJModel anexoJModel, Quadro06 quadro06Model, AnexoJq07T1_Linha current, int numLinha) {
        EventList<AnexoJq06T1_Linha> tabelaQuadro06 = quadro06Model.getAnexoJq06T1();
        if (!(current.getCampoNLinhaQ6() == null || this.hasOcorrenciaQ6(tabelaQuadro06, current))) {
            result.add(new DeclValidationMessage("J214", anexoJModel.getLink("aAnexoJ.qQuadro07"), new String[]{anexoJModel.getLink("aAnexoJ.qQuadro07"), anexoJModel.getLink("aAnexoJ.qQuadro06")}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public void validateJ239(ValidationResult result, AnexoJModel model, Quadro06 quadro06Model, AnexoJq07T1_Linha current, int numLinha) {
        long rendimentoDeclaradoQ7;
        EventList<AnexoJq06T1_Linha> tabelaQuadro06 = quadro06Model.getAnexoJq06T1();
        AnexoJq06T1_Linha linhaQ6 = this.getOcorrenciaQ6Codigos(tabelaQuadro06, current, new long[]{401, 402, 412, 416, 417});
        long totalRendimentos = linhaQ6 != null && linhaQ6.getMontanteRendimento() != null ? linhaQ6.getMontanteRendimento() : 0;
        long l = rendimentoDeclaradoQ7 = current.getRendimento() != null ? current.getRendimento() : 0;
        if (current.getCampoNLinhaQ6() != null && linhaQ6 != null && rendimentoDeclaradoQ7 > totalRendimentos) {
            result.add(new DeclValidationMessage("J239", model.getLink("aAnexoJ.qQuadro07"), new String[]{model.getLink("aAnexoJ.qQuadro07"), model.getLink("aAnexoJ.qQuadro06")}));
        }
    }
}

