/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoj;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro03;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class AnexoJValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoJValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            this.validateJ001(result, anexoJModel);
        }
        this.validateJ002(result, model);
        return result;
    }

    protected void validateJ001(ValidationResult result, AnexoJModel anexoJModel) {
        if (anexoJModel != null && anexoJModel.isEmpty()) {
            result.add(new DeclValidationMessage("J001", anexoJModel.getLink("aAnexoJ"), new String[]{anexoJModel.getLink("aAnexoJ")}));
        }
    }

    protected void validateJ002(ValidationResult result, DeclaracaoModel model) {
        ArrayList<FormKey> processedKeys = new ArrayList<FormKey>();
        HashMap<Long, String> processedNifs = new HashMap<Long, String>();
        List<FormKey> keys = model.getAllAnexosByType("AnexoJ");
        for (FormKey key : keys) {
            AnexoJModel otherModel;
            AnexoJModel anexoJModel = (AnexoJModel)model.getAnexo(key);
            if (anexoJModel == null) continue;
            if (processedKeys.contains(key)) {
                otherModel = (AnexoJModel)model.getAnexo(new FormKey(AnexoJModel.class, key.getSubId()));
                if (otherModel == null) {
                    otherModel = anexoJModel;
                }
                result.add(new DeclValidationMessage("J002", anexoJModel.getLink("aAnexoJ"), new String[]{otherModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04"), anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04")}));
            } else {
                processedKeys.add(key);
            }
            if (anexoJModel.getQuadro03().getAnexoJq03C04() == null) continue;
            if (processedNifs.get(anexoJModel.getQuadro03().getAnexoJq03C04()) != null) {
                otherModel = (AnexoJModel)model.getAnexo(new FormKey(AnexoJModel.class, (String)processedNifs.get(anexoJModel.getQuadro03().getAnexoJq03C04())));
                if (otherModel == null) {
                    otherModel = anexoJModel;
                }
                result.add(new DeclValidationMessage("J002", anexoJModel.getLink("aAnexoJ"), new String[]{otherModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04"), anexoJModel.getLink("aAnexoJ.qQuadro03.fanexoJq03C04")}));
                continue;
            }
            processedNifs.put(anexoJModel.getQuadro03().getAnexoJq03C04(), key.getSubId());
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

