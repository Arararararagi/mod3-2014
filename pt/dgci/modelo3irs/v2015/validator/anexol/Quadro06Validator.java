/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexol;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro06;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.StringUtil;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        return new ValidationResult();
    }

    private boolean existsAnexoJForTitular(DeclaracaoModel model, String nif) {
        return ((Modelo3IRSv2015Model)model).getAnexoJByTitular(nif) != null;
    }

    private boolean existsAnexoJForTitular(DeclaracaoModel model, Long nif) {
        return ((Modelo3IRSv2015Model)model).getAnexoJByTitular(nif) != null;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        return new ValidationResult();
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoL");
        for (FormKey key : keys) {
            AnexoLModel anexoLModel = (AnexoLModel)model.getAnexo(key);
            Quadro04 quadro04Model = anexoLModel.getQuadro04();
            Quadro05 quadro05Model = anexoLModel.getQuadro05();
            Quadro06 quadro06Model = anexoLModel.getQuadro06();
            if (!(quadro06Model.getQ06B1() == null || StringUtil.in(quadro06Model.getQ06B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("L062", anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP1"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP1")}));
                result.add(new DeclValidationMessage("L063", anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP2"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP2")}));
            }
            if ((quadro06Model.isQ06B1OP1Selected() || quadro06Model.isQ06B1OP2Selected()) && quadro04Model.isEmpty() && quadro05Model.isEmpty()) {
                result.add(new DeclValidationMessage("L051", anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP1"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP1"), anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP2"), anexoLModel.getLink("aAnexoL.qQuadro04"), anexoLModel.getLink("aAnexoL.qQuadro05")}));
            }
            if (!(quadro06Model.getQ06B2() == null || StringUtil.in(quadro06Model.getQ06B2(), new String[]{"3", "4"}))) {
                result.add(new DeclValidationMessage("L064", anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP3"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP3")}));
                result.add(new DeclValidationMessage("L065", anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP4"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP4")}));
            }
            if (!quadro06Model.isQ06B2OP3Selected() && !quadro06Model.isQ06B2OP4Selected() || this.existsAnexoJForTitular(model, anexoLModel.getAnexoLTitular())) continue;
            result.add(new DeclValidationMessage("L052", anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP3"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP3"), anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP4")}));
        }
        return result;
    }
}

