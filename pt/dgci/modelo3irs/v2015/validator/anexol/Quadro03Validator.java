/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexol;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoL");
        for (FormKey key : keys) {
            Long nifTitularRendimento;
            AnexoLModel anexoLModel = (AnexoLModel)model.getAnexo(key);
            Quadro03 quadro03Model = anexoLModel.getQuadro03();
            if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoLq03C02(), rostoModel.getQuadro03().getQ03C03())) {
                result.add(new DeclValidationMessage("L003", anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C02"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C02"), "aRosto.qQuadro03.fq03C03"}));
            }
            if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoLq03C03(), rostoModel.getQuadro03().getQ03C04())) {
                result.add(new DeclValidationMessage("L004", anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C03"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C03"), "aRosto.qQuadro03.fq03C04"}));
            }
            if (!((nifTitularRendimento = quadro03Model.getAnexoLq03C04()) == null || rostoModel.getQuadro03().isSujeitoPassivoA(nifTitularRendimento) || rostoModel.getQuadro03().isSujeitoPassivoB(nifTitularRendimento) || rostoModel.getQuadro07().isConjugeFalecido(nifTitularRendimento))) {
                result.add(new DeclValidationMessage("L005", anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C04"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C02"), anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C03")}));
            }
            if (nifTitularRendimento != null) continue;
            result.add(new DeclValidationMessage("L006", anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C04"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C04")}));
        }
        return result;
    }
}

