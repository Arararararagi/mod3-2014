/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexol.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexol.AnexoLValidator;
import pt.dgci.modelo3irs.v2015.validator.anexol.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexol.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexol.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexol.net.Quadro06NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoLNETValidator
extends AnexoLValidator {
    public AnexoLNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        return result;
    }
}

