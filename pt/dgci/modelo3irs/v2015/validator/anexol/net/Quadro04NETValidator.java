/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexol.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexol.Quadro04Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoL");
        for (FormKey key : keys) {
            AnexoLModel anexoLModel = (AnexoLModel)model.getAnexo(key);
            Quadro04 quadro04Model = anexoLModel.getQuadro04();
            AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(anexoLModel.getAnexoLTitular());
            AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(anexoLModel.getAnexoLTitular());
            result.addAllFrom(this.validaT1NET(rostoModel, anexoLModel, quadro04Model, anexoAModel));
            result.addAllFrom(this.validaT2NET(rostoModel, anexoLModel, quadro04Model, anexoBModel));
            result.addAllFrom(this.validaT3NET(rostoModel, anexoLModel, quadro04Model, anexoCModel));
        }
        return result;
    }

    protected ValidationResult validaT1NET(RostoModel rostoModel, AnexoLModel anexoLModel, Quadro04 quadro04AnexoLModel, AnexoAModel anexoAModel) {
        return new ValidationResult();
    }

    protected ValidationResult validaT2NET(RostoModel rostoModel, AnexoLModel anexoLModel, Quadro04 quadro04AnexoLModel, AnexoBModel anexoBModel) {
        ValidationResult result = new ValidationResult();
        for (int linha = 0; linha < quadro04AnexoLModel.getAnexoLq04T2().size(); ++linha) {
            AnexoLq04T2_Linha anexoLq04T2Linha = quadro04AnexoLModel.getAnexoLq04T2().get(linha);
            String linkCurrentLine = "aAnexoL.qQuadro04.tanexoLq04T2.l" + (linha + 1);
            String linkCampoQ4AnexoB = linkCurrentLine + ".c" + AnexoLq04T2_LinhaBase.Property.CAMPOQ4ANEXOB.getIndex();
            String linkCodActividade = linkCurrentLine + ".c" + AnexoLq04T2_LinhaBase.Property.CODACTIVIDADE.getIndex();
            String linkRendimento = linkCurrentLine + ".c" + AnexoLq04T2_LinhaBase.Property.RENDIMENTO.getIndex();
            Long campoQ4AnexoB = anexoLq04T2Linha.getCampoQ4AnexoB();
            Long codActividade = anexoLq04T2Linha.getCodActividade();
            Long rendimento = anexoLq04T2Linha.getRendimento();
            if (campoQ4AnexoB == null || codActividade != null && !Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento)) continue;
            result.add(new DeclValidationMessage("L022", anexoLModel.getLink(linkCampoQ4AnexoB), new String[]{anexoLModel.getLink(linkCampoQ4AnexoB), anexoLModel.getLink(linkCodActividade), anexoLModel.getLink(linkRendimento)}));
        }
        return result;
    }

    protected ValidationResult validaT3NET(RostoModel rostoModel, AnexoLModel anexoLModel, Quadro04 quadro04AnexoLModel, AnexoCModel anexoCModel) {
        return new ValidationResult();
    }
}

