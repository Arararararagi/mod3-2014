/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexol;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxL;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro05;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.Tuple;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateL069(ValidationResult result, AnexoLModel anexoLModel, AnexoJModel anexoJModel, AnexoLq05T1_Linha current, int numLinha) {
        Long campoNLinhaQ6 = current.getCampoNLinhaQ6();
        Long pais = current.getPais();
        Quadro06 quadro06 = null;
        if (anexoJModel != null) {
            quadro06 = anexoJModel.getQuadro06();
        }
        if (campoNLinhaQ6 != null && pais != null && (anexoJModel == null || quadro06.existCampoQ6IdentificacaoPaisCodC4Invalido(campoNLinhaQ6, pais))) {
            String quadro05Link = anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1");
            result.add(new DeclValidationMessage("L069", quadro05Link, new String[]{quadro05Link, quadro05Link}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoL");
        for (FormKey key : keys) {
            AnexoLModel anexoLModel = (AnexoLModel)model.getAnexo(key);
            Quadro05 quadro05Model = anexoLModel.getQuadro05();
            AnexoJModel anexoJModel = ((Modelo3IRSv2015Model)model).getAnexoJByTitular(anexoLModel.getAnexoLTitular());
            HashSet<Long> distinctCampoQ6AnexoJ = new HashSet<Long>();
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            this.validateL086(quadro05Model, result, anexoLModel);
            for (int linha = 0; linha < quadro05Model.getAnexoLq05T1().size(); ++linha) {
                AnexoLq05T1_Linha anexoLq05T1Linha = quadro05Model.getAnexoLq05T1().get(linha);
                String linkCurrentLine = "aAnexoL.qQuadro05.tanexoLq05T1.l" + (linha + 1);
                String linkCampoQ6 = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.CAMPONLINHAQ6.getIndex();
                String linkCodActividade = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.CODACTIVIDADE.getIndex();
                String linkCategoria = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.CATAOUB.getIndex();
                String linkPais = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.PAIS.getIndex();
                String linkRendimento = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.RENDIMENTO.getIndex();
                String linkImpostoPagoNoEstrangeiro = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.IMPOSTOPAGONOESTRANGEIRO.getIndex();
                String linkSemImpostoPagoNoEstrangeiro = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.SEMIMPOSTOPAGONOESTRANGEIRO.getIndex();
                Long campoLinhaQ6 = anexoLq05T1Linha.getCampoNLinhaQ6();
                Long codActividade = anexoLq05T1Linha.getCodActividade();
                String categoria = anexoLq05T1Linha.getCatAouB();
                Long pais = anexoLq05T1Linha.getPais();
                Long rendimento = anexoLq05T1Linha.getRendimento();
                Long impostoPagoNoEstrangeiro = anexoLq05T1Linha.getImpostoPagoNoEstrangeiro();
                Boolean semImpostoPagoNoEstrangeiro = anexoLq05T1Linha.getSemImpostoPagoNoEstrangeiro();
                if (campoLinhaQ6 != null) {
                    distinctCampoQ6AnexoJ.add(campoLinhaQ6);
                }
                if (campoLinhaQ6 != null && codActividade != null && pais != null) {
                    Tuple tuple = new Tuple(new Object[]{campoLinhaQ6, codActividade, pais});
                    if (processedTuples.contains(tuple)) {
                        result.add(new DeclValidationMessage("L040", anexoLModel.getLink(linkCurrentLine), new String[]{anexoLModel.getLink(linkCurrentLine)}));
                    } else {
                        processedTuples.add(tuple);
                    }
                }
                this.validateL041(result, campoLinhaQ6, codActividade, categoria, pais, rendimento, impostoPagoNoEstrangeiro, semImpostoPagoNoEstrangeiro, anexoLModel, linkCurrentLine);
                if (campoLinhaQ6 != null && (campoLinhaQ6 < 601 || campoLinhaQ6 > 630)) {
                    result.add(new DeclValidationMessage("L042", anexoLModel.getLink(linkCampoQ6), new String[]{anexoLModel.getLink(linkCampoQ6)}));
                }
                this.validateL043(result, anexoLModel, campoLinhaQ6, codActividade, pais, categoria, rendimento, linkCampoQ6, linkCodActividade, linkCategoria, linkPais, linkRendimento);
                this.validateL044(result, anexoLModel, campoLinhaQ6, codActividade, pais, rendimento, impostoPagoNoEstrangeiro, semImpostoPagoNoEstrangeiro, categoria, linkCampoQ6);
                this.validateL084(result, anexoLModel, categoria, linkCategoria);
                this.validateL089(result, anexoLModel, anexoLq05T1Linha, linha, Modelo3IRSv2015Parameters.instance().isFase2());
                this.validateL081(result, anexoLModel, semImpostoPagoNoEstrangeiro, linkSemImpostoPagoNoEstrangeiro);
                this.validateL082(result, anexoLModel, anexoLq05T1Linha, linkImpostoPagoNoEstrangeiro, linkSemImpostoPagoNoEstrangeiro);
                this.validateL083(result, anexoLModel, anexoLq05T1Linha, linkImpostoPagoNoEstrangeiro, linkSemImpostoPagoNoEstrangeiro);
                if (!(codActividade == null || Modelo3IRSValidatorUtil.in(codActividade, new long[]{101, 102, 103, 201, 202, 203, 204, 205, 301, 302, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 501, 601, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 801, 802, 999}))) {
                    result.add(new DeclValidationMessage("L045", anexoLModel.getLink(linkCodActividade), new String[]{anexoLModel.getLink(linkCodActividade)}));
                }
                ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxL.class.getSimpleName());
                if (!(pais == null || paisesCatalog.containsKey(pais))) {
                    result.add(new DeclValidationMessage("L046", anexoLModel.getLink(linkPais), new String[]{anexoLModel.getLink(linkPais)}));
                }
                this.validateL047(anexoLq05T1Linha, anexoJModel, anexoLModel, result, linha);
                this.validateL069(result, anexoLModel, anexoJModel, anexoLq05T1Linha, linha + 1);
                this.validateL079(result, anexoLq05T1Linha, anexoJModel, anexoLModel.getLink(linkImpostoPagoNoEstrangeiro));
            }
            if (!(quadro05Model.getAnexoLq05T1().isEmpty() || anexoLModel.getQuadro06().isQ06B2OP3Selected() || anexoLModel.getQuadro06().isQ06B2OP4Selected())) {
                result.add(new DeclValidationMessage("L048", anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP3"), anexoLModel.getLink("aAnexoL.qQuadro06.fq06B2OP4")}));
            }
            for (Long campoQ6AnexoJ : distinctCampoQ6AnexoJ) {
                String linkAnexoJQ6T1 = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1") : "aAnexoJ.qQuadro06.tanexoJq06T1";
                this.validateL049(result, anexoLModel, anexoJModel, campoQ6AnexoJ);
                this.validateL050(result, anexoLModel, anexoJModel, campoQ6AnexoJ);
            }
        }
        return result;
    }

    protected void validateL049(ValidationResult result, AnexoLModel anexoLModel, AnexoJModel anexoJModel, Long campoQ6AnexoJ) {
        long totalRendimentoAnexoJByCodigo;
        Quadro05 quadro05Model = anexoLModel.getQuadro05();
        long totalRendimentoAnexoLByCodigo = quadro05Model.getTotalRendimentoByCampoQ6AnexoJ(campoQ6AnexoJ);
        long l = totalRendimentoAnexoJByCodigo = anexoJModel != null ? anexoJModel.getQuadro06().getTotalMontanteRendimentoPreenchidoByCampoQ6(campoQ6AnexoJ) : 0;
        if (totalRendimentoAnexoLByCodigo > totalRendimentoAnexoJByCodigo) {
            String linkAnexoJQ6T1 = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1") : "aAnexoJ.qQuadro06.tanexoJq06T1";
            result.add(new DeclValidationMessage("L049", anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), linkAnexoJQ6T1}));
        }
    }

    protected void validateL050(ValidationResult result, AnexoLModel anexoLModel, AnexoJModel anexoJModel, long campoQ6AnexoJ) {
        long totalImpostoAnexoJByCodigo;
        Quadro05 quadro05Model = anexoLModel.getQuadro05();
        long totalImpostoAnexoLByCodigo = quadro05Model.getTotalImpostoByCampoQ4AnexoJ(campoQ6AnexoJ);
        long l = totalImpostoAnexoJByCodigo = anexoJModel != null ? anexoJModel.getQuadro06().getTotalImpostoByCampoQ6(campoQ6AnexoJ) : 0;
        if (totalImpostoAnexoLByCodigo > totalImpostoAnexoJByCodigo) {
            String linkAnexoJQ6T1 = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro06.tanexoJq06T1") : "aAnexoJ.qQuadro06.tanexoJq06T1";
            result.add(new DeclValidationMessage("L050", anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), linkAnexoJQ6T1}));
        }
    }

    protected void validateL047(AnexoLq05T1_Linha anexoLq05T1Linha, AnexoJModel anexoJModel, AnexoLModel anexoLModel, ValidationResult result, int linha) {
        String linkCurrentLine = "aAnexoL.qQuadro05.tanexoLq05T1.l" + (linha + 1);
        String linkPais = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.PAIS.getIndex();
        String linkCampoQ6 = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.CAMPONLINHAQ6.getIndex();
        Long campoLinhaQ6 = anexoLq05T1Linha.getCampoNLinhaQ6();
        Long pais = anexoLq05T1Linha.getPais();
        if (!(campoLinhaQ6 == null || pais == null || anexoJModel != null && anexoJModel.getQuadro06().existCampoQ6IdentificacaoPais(campoLinhaQ6, pais))) {
            String linkAnexoJQ6 = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro06") : "aAnexoJ.qQuadro06";
            result.add(new DeclValidationMessage("L047", anexoLModel.getLink(linkCampoQ6), new String[]{anexoLModel.getLink(linkCampoQ6), anexoLModel.getLink(linkPais), linkAnexoJQ6}));
        }
    }

    protected void validateL086(Quadro05 quadro5, ValidationResult result, AnexoLModel anexoLmodel) {
        if (quadro5.getAnexoLq05T1().size() > 100) {
            result.add(new DeclValidationMessage("L086", anexoLmodel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), new String[]{anexoLmodel.getLink("aAnexoL.qQuadro05.tanexoLq05T1")}));
        }
    }

    protected void validateL079(ValidationResult result, AnexoLq05T1_Linha anexoLq05T1Linha, AnexoJModel anexoJModel, String linkImpostoPagoAoEstado) {
        if (!(anexoLq05T1Linha == null || anexoJModel == null || anexoJModel.getQuadro06() == null || anexoJModel.getQuadro06().isEmpty())) {
            for (AnexoJq06T1_Linha anexoJq06T1_Linha : anexoJModel.getQuadro06().getAnexoJq06T1()) {
                if (anexoLq05T1Linha.getRendimento() == null || !anexoLq05T1Linha.getRendimento().equals(anexoJq06T1_Linha.getMontanteRendimento()) || anexoLq05T1Linha.getCampoNLinhaQ6() == null || !anexoLq05T1Linha.getCampoNLinhaQ6().equals(anexoJq06T1_Linha.getNLinha()) || anexoJq06T1_Linha.getCampoQ4() == null || !Modelo3IRSValidatorUtil.in(anexoJq06T1_Linha.getCampoQ4(), new long[]{401, 402, 403, 406, 426}) || this.sum(anexoJq06T1_Linha.getPaisDaFonteValor(), anexoJq06T1_Linha.getPaisAgentePagadorValor()) <= 0 || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoLq05T1Linha.getImpostoPagoNoEstrangeiro())) continue;
                result.add(new DeclValidationMessage("L079", linkImpostoPagoAoEstado, new String[]{linkImpostoPagoAoEstado}));
                break;
            }
        }
    }

    protected void validateL041(ValidationResult result, Long campoLinhaQ6, Long codActividade, String categoria, Long pais, Long rendimento, Long impostoPagoAoEstado, Boolean semImpostoPagoNoEstrangeiro, AnexoLModel anexoLModel, String linkCurrentLine) {
        if (campoLinhaQ6 == null && codActividade == null && pais == null && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento) && Modelo3IRSValidatorUtil.isEmptyOrZero(impostoPagoAoEstado) && (semImpostoPagoNoEstrangeiro == null || !semImpostoPagoNoEstrangeiro.booleanValue()) && categoria == null) {
            result.add(new DeclValidationMessage("L041", anexoLModel.getLink(linkCurrentLine), new String[]{anexoLModel.getLink(linkCurrentLine)}));
        }
    }

    protected void validateL043(ValidationResult result, AnexoLModel anexoLModel, Long campoLinhaQ6, Long codActividade, Long pais, String categoria, Long rendimento, String linkCampoQ6, String linkCodActividade, String linkCategoria, String linkPais, String linkRendimento) {
        if (campoLinhaQ6 != null && (codActividade == null || pais == null || categoria == null || Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento))) {
            result.add(new DeclValidationMessage("L043", anexoLModel.getLink(linkCampoQ6), new String[]{anexoLModel.getLink(linkCampoQ6), anexoLModel.getLink(linkCodActividade), anexoLModel.getLink(linkCategoria), anexoLModel.getLink(linkPais), anexoLModel.getLink(linkRendimento)}));
        }
    }

    protected void validateL044(ValidationResult result, AnexoLModel anexoLModel, Long campoLinhaQ6, Long codActividade, Long pais, Long rendimento, Long impostoPagoNoEstrangeiro, Boolean semImpostoPagoNoEstrangeiro, String categoria, String linkCampoQ6) {
        if (!(campoLinhaQ6 != null || codActividade == null && pais == null && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento) && Modelo3IRSValidatorUtil.isEmptyOrZero(impostoPagoNoEstrangeiro) && (semImpostoPagoNoEstrangeiro == null || !semImpostoPagoNoEstrangeiro.booleanValue()) && categoria == null)) {
            result.add(new DeclValidationMessage("L044", anexoLModel.getLink(linkCampoQ6), new String[]{anexoLModel.getLink(linkCampoQ6)}));
        }
    }

    protected void validateL084(ValidationResult result, AnexoLModel anexoLModel, String categoria, String linkCategoria) {
        HashSet<String> supportedCodes = new HashSet<String>(Arrays.asList("A", "B"));
        if (!(categoria == null || supportedCodes.contains(categoria))) {
            result.add(new DeclValidationMessage("L084", anexoLModel.getLink(linkCategoria), new String[]{anexoLModel.getLink(linkCategoria)}));
        }
    }

    protected void validateL089(ValidationResult result, AnexoLModel anexoLModel, AnexoLq05T1_Linha anexoLq05T1Linha, int numLinha, boolean isFase2) {
        if (!isFase2 && anexoLq05T1Linha.getCatAouB() != null && anexoLq05T1Linha.getCatAouB().equals("B")) {
            String linkCurrentLine = "aAnexoL.qQuadro05.tanexoLq05T1.l" + (numLinha + 1);
            String linkCategoria = linkCurrentLine + ".c" + AnexoLq05T1_LinhaBase.Property.CATAOUB.getIndex();
            result.add(new DeclValidationMessage("L089", anexoLModel.getLink(linkCategoria), new String[]{anexoLModel.getLink(linkCategoria)}));
        }
    }

    protected void validateL081(ValidationResult result, AnexoLModel anexoLModel, Boolean semImpostoPagoNoEstrangeiro, String linkSemImpostoPagoNoEstrangeiro) {
        if (!(semImpostoPagoNoEstrangeiro == null || semImpostoPagoNoEstrangeiro.equals(Boolean.TRUE) || semImpostoPagoNoEstrangeiro.equals(Boolean.FALSE))) {
            result.add(new DeclValidationMessage("B081", anexoLModel.getLink(linkSemImpostoPagoNoEstrangeiro), new String[]{anexoLModel.getLink(linkSemImpostoPagoNoEstrangeiro)}));
        }
    }

    protected void validateL082(ValidationResult result, AnexoLModel anexoLModel, AnexoLq05T1_Linha anexoLq05T1_Linha, String linkImpostoPagoNoEstrangeiro, String linkSemImpostoPagoNoEstrangeiro) {
        if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(anexoLq05T1_Linha.getImpostoPagoNoEstrangeiro()) || anexoLq05T1_Linha.getSemImpostoPagoNoEstrangeiro() != null && anexoLq05T1_Linha.getSemImpostoPagoNoEstrangeiro().booleanValue() || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoLq05T1_Linha.getRendimento()))) {
            result.add(new DeclValidationMessage("L082", anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), new String[]{anexoLModel.getLink(linkImpostoPagoNoEstrangeiro), anexoLModel.getLink(linkSemImpostoPagoNoEstrangeiro)}));
        }
    }

    protected void validateL083(ValidationResult result, AnexoLModel anexoLModel, AnexoLq05T1_Linha anexoLq05T1_Linha, String linkImpostoPagoNoEstrangeiro, String linkSemImpostoPagoNoEstrangeiro) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(anexoLq05T1_Linha.getImpostoPagoNoEstrangeiro()) && anexoLq05T1_Linha.getSemImpostoPagoNoEstrangeiro() != null && anexoLq05T1_Linha.getSemImpostoPagoNoEstrangeiro().booleanValue()) {
            result.add(new DeclValidationMessage("L083", anexoLModel.getLink("aAnexoL.qQuadro05.tanexoLq05T1"), new String[]{anexoLModel.getLink(linkImpostoPagoNoEstrangeiro), anexoLModel.getLink(linkSemImpostoPagoNoEstrangeiro)}));
        }
    }

    private long sum(Long val1, Long val2) {
        long soma = 0;
        if (val1 != null) {
            soma+=val1.longValue();
        }
        if (val2 != null) {
            soma+=val2.longValue();
        }
        return soma;
    }
}

