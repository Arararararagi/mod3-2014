/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexol;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_AnexoLCodRendAnexoA;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_CodActividade;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        return new ValidationResult();
    }

    protected ValidationResult validaT1(RostoModel rostoModel, AnexoLModel anexoLModel, pt.dgci.modelo3irs.v2015.model.anexol.Quadro04 quadro04AnexoLModel, AnexoAModel anexoAModel) {
        ValidationResult result = new ValidationResult();
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        this.validateL087(quadro04AnexoLModel, result, anexoLModel);
        for (int linha = 0; linha < quadro04AnexoLModel.getAnexoLq04T1().size(); ++linha) {
            String titularAnexoL;
            List<AnexoAq04T4A_Linha> linhasAnexoAq04T4A;
            AnexoLq04T1_Linha anexoLq04T1Linha = quadro04AnexoLModel.getAnexoLq04T1().get(linha);
            String linkCurrentLine = "aAnexoL.qQuadro04.tanexoLq04T1.l" + (linha + 1);
            String linkEntidade = linkCurrentLine + ".c" + AnexoLq04T1_LinhaBase.Property.ENTIDADE.getIndex();
            String linkCodRendAnexoA = linkCurrentLine + ".c3";
            String linkCodActividade = linkCurrentLine + ".c4";
            String linkRendimento = linkCurrentLine + ".c5";
            Long entidade = anexoLq04T1Linha.getEntidade();
            Long codRendAnexoA = anexoLq04T1Linha.getCodRendAnexoA();
            Long codActividade = anexoLq04T1Linha.getCodActividade();
            Long rendimento = anexoLq04T1Linha.getRendimento();
            if (entidade != null && codRendAnexoA != null && codActividade != null) {
                Tuple tuple = new Tuple(new Object[]{entidade, codRendAnexoA, codActividade});
                if (processedTuples.contains(tuple)) {
                    result.add(new DeclValidationMessage("L007", anexoLModel.getLink(linkCurrentLine), new String[]{anexoLModel.getLink(linkEntidade), anexoLModel.getLink(linkCodRendAnexoA), anexoLModel.getLink(linkCodActividade)}));
                } else {
                    processedTuples.add(tuple);
                }
            }
            if (entidade == null && codRendAnexoA == null && codActividade == null && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento)) {
                result.add(new DeclValidationMessage("L008", anexoLModel.getLink(linkCurrentLine), new String[]{anexoLModel.getLink(linkCurrentLine)}));
            }
            if (!(entidade == null || Modelo3IRSValidatorUtil.startsWith(entidade, "1", "2", "3", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("L009", anexoLModel.getLink(linkEntidade), new String[]{anexoLModel.getLink(linkEntidade)}));
            }
            if (!(entidade == null || Modelo3IRSValidatorUtil.isNIFValid(entidade))) {
                result.add(new DeclValidationMessage("L010", anexoLModel.getLink(linkEntidade), new String[]{anexoLModel.getLink(linkEntidade)}));
            }
            this.validateL011(result, rostoModel, anexoLModel, linkEntidade, entidade);
            if (codRendAnexoA != null && (codActividade == null || rendimento == null) || rendimento != null && rendimento == 0) {
                result.add(new DeclValidationMessage("L012", anexoLModel.getLink(linkCodRendAnexoA), new String[]{anexoLModel.getLink(linkCodRendAnexoA), anexoLModel.getLink(linkCodActividade), anexoLModel.getLink(linkRendimento)}));
            }
            if (!(codRendAnexoA != null || entidade == null && codActividade == null && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento))) {
                result.add(new DeclValidationMessage("L013", anexoLModel.getLink(linkCodRendAnexoA), new String[]{anexoLModel.getLink(linkCodRendAnexoA)}));
            }
            ListMap codRendAnexoACatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_AnexoLCodRendAnexoA.class.getSimpleName());
            if (!(codRendAnexoA == null || codRendAnexoACatalog.containsKey(codRendAnexoA))) {
                result.add(new DeclValidationMessage("L014", anexoLModel.getLink(linkCodRendAnexoA), new String[]{anexoLModel.getLink(linkCodRendAnexoA)}));
            }
            ListMap codActividadeCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_CodActividade.class.getSimpleName());
            if (!(codActividade == null || codActividadeCatalog.containsKey(codActividade))) {
                result.add(new DeclValidationMessage("L015", anexoLModel.getLink(linkCodActividade), new String[]{anexoLModel.getLink(linkCodActividade)}));
            }
            if (rendimento != null && anexoLModel.getQuadro03().getAnexoLq03C04() != null && entidade != null && (titularAnexoL = rostoModel.getTitularByNIF(anexoLModel.getQuadro03().getAnexoLq03C04())) != null) {
                long rendimentoAnexoA;
                long l = rendimentoAnexoA = anexoAModel != null ? anexoAModel.getQuadro04().getTotalRendimentosQ4T4AByEntidadeTitular(entidade, titularAnexoL) : 0;
                if (rendimento > rendimentoAnexoA) {
                    result.add(new DeclValidationMessage("L016", anexoLModel.getLink(linkRendimento), new String[]{anexoLModel.getLink(linkRendimento), "aAnexoA.qQuadro04"}));
                }
            }
            if (codRendAnexoA == null || (titularAnexoL = rostoModel.getTitularByNIF(anexoLModel.getQuadro03().getAnexoLq03C04())) == null) continue;
            List<AnexoAq04T4A_Linha> list = linhasAnexoAq04T4A = anexoAModel != null ? anexoAModel.getQuadro04().getLinhasQ4T4A(entidade, codRendAnexoA, titularAnexoL) : null;
            if (!ListUtil.isEmpty(linhasAnexoAq04T4A)) continue;
            result.add(new DeclValidationMessage("L017", anexoLModel.getLink(linkCodRendAnexoA), new String[]{anexoLModel.getLink(linkCodRendAnexoA), "aAnexoA.qQuadro04"}));
        }
        String titularAnexoL = rostoModel.getTitularByNIF(anexoLModel.getQuadro03().getAnexoLq03C04());
        if (titularAnexoL != null) {
            Set<Tuple> distinctEntidadeCodRendimento = quadro04AnexoLModel.getDistinctEntidadeCodRendimento();
            for (Tuple entidadeCodRendimento : distinctEntidadeCodRendimento) {
                long totalRendimentosAnxA;
                Long entidade = (Long)entidadeCodRendimento.getPosition(0);
                Long codRendimento = (Long)entidadeCodRendimento.getPosition(1);
                long l = totalRendimentosAnxA = anexoAModel != null ? anexoAModel.getQuadro04().getTotalRendimentosQ4T4AByEntidadeCodRendimentosTitular(entidade, codRendimento, titularAnexoL) : 0;
                long totalRendimentosAnxL = quadro04AnexoLModel.getTotalRendimentosQ4T4AByEntidadeCodRendimentos(entidade, codRendimento);
                if (totalRendimentosAnxL <= totalRendimentosAnxA) continue;
                result.add(new DeclValidationMessage("L018", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T1"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T1"), "aAnexoA.qQuadro04"}));
            }
        }
        return result;
    }

    protected void validateL087(pt.dgci.modelo3irs.v2015.model.anexol.Quadro04 quadro4, ValidationResult result, AnexoLModel anexoLmodel) {
        if (quadro4.getAnexoLq04T1().size() > 100) {
            result.add(new DeclValidationMessage("L087", anexoLmodel.getLink("aAnexoL.qQuadro04.tanexoLq04T1"), new String[]{anexoLmodel.getLink("aAnexoL.qQuadro04.tanexoLq04T1")}));
        }
    }

    protected void validateL011(ValidationResult result, RostoModel rostoModel, AnexoLModel anexoLModel, String linkEntidade, Long entidade) {
        Quadro03 quadro03RostoModel = rostoModel.getQuadro03();
        Quadro07 quadro07RostoModel = rostoModel.getQuadro07();
        if (entidade != null && (quadro03RostoModel.isSujeitoPassivoA(entidade) || quadro03RostoModel.isSujeitoPassivoB(entidade) || quadro03RostoModel.existsInDependentes(entidade) || quadro07RostoModel.isConjugeFalecido(entidade) || quadro07RostoModel.existsInAscendentes(entidade) || quadro07RostoModel.existsInAfilhadosCivisEmComunhao(entidade) || quadro03RostoModel.existsInDependentesEmGuardaConjunta(entidade) || quadro03RostoModel.existsInDependentesEmGuardaConjuntaOutro(entidade) || quadro07RostoModel.existsInAscendentesEColaterais(entidade))) {
            result.add(new DeclValidationMessage("L011", anexoLModel.getLink(linkEntidade), new String[]{anexoLModel.getLink(linkEntidade), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected ValidationResult validaT2(RostoModel rostoModel, AnexoLModel anexoLModel, pt.dgci.modelo3irs.v2015.model.anexol.Quadro04 quadro04AnexoLModel, AnexoBModel anexoBModel) {
        String linkAnexoBq04C404;
        String linkAnexoBq04C403;
        String linkAnexoBq04C420;
        ValidationResult result = new ValidationResult();
        long totalRendimentoCampoAnxB404 = 0;
        long totalRendimentoCampoAnxB420 = 0;
        long totalRendimentoCampoAnxB440 = 0;
        long totalRendimentoActividade999 = 0;
        long totalRendimentoActividadeSem999CampoAnxB403 = 0;
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        this.validateL085(quadro04AnexoLModel, result, anexoLModel);
        for (int linha = 0; linha < quadro04AnexoLModel.getAnexoLq04T2().size(); ++linha) {
            AnexoLq04T2_Linha anexoLq04T2Linha = quadro04AnexoLModel.getAnexoLq04T2().get(linha);
            String linkCurrentLine = "aAnexoL.qQuadro04.tanexoLq04T2.l" + (linha + 1);
            String linkCampoQ4AnexoB = linkCurrentLine + ".c" + AnexoLq04T2_LinhaBase.Property.CAMPOQ4ANEXOB.getIndex();
            String linkCodActividade = linkCurrentLine + ".c" + AnexoLq04T2_LinhaBase.Property.CODACTIVIDADE.getIndex();
            Long campoQ4AnexoB = anexoLq04T2Linha.getCampoQ4AnexoB();
            Long codActividade = anexoLq04T2Linha.getCodActividade();
            Long rendimento = anexoLq04T2Linha.getRendimento();
            Long nifEntidadePagadora = anexoLq04T2Linha.getEntidade();
            if (rendimento != null) {
                if (campoQ4AnexoB != null) {
                    if (campoQ4AnexoB == 404) {
                        totalRendimentoCampoAnxB404+=rendimento.longValue();
                    } else if (campoQ4AnexoB == 420) {
                        totalRendimentoCampoAnxB420+=rendimento.longValue();
                    } else if (campoQ4AnexoB == 440) {
                        totalRendimentoCampoAnxB440+=rendimento.longValue();
                    }
                }
                if (codActividade != null) {
                    if (codActividade == 999) {
                        totalRendimentoActividade999+=rendimento.longValue();
                    } else if (campoQ4AnexoB != null && campoQ4AnexoB == 403 && codActividade != 999) {
                        totalRendimentoActividadeSem999CampoAnxB403+=rendimento.longValue();
                    }
                }
            }
            Quadro04Validator.validateL019(processedTuples, anexoLModel, anexoLq04T2Linha, linha + 1, result);
            if (campoQ4AnexoB == null && codActividade == null && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento)) {
                result.add(new DeclValidationMessage("L020", anexoLModel.getLink(linkCurrentLine), new String[]{anexoLModel.getLink(linkCurrentLine)}));
            }
            this.validateL075(result, anexoLq04T2Linha, anexoLModel, linha + 1);
            this.validateL076(result, anexoLq04T2Linha, anexoLModel, linha + 1);
            this.validateL077(result, anexoLq04T2Linha, linha + 1, rostoModel.getQuadro07(), rostoModel.getQuadro05(), rostoModel.getQuadro03(), anexoLModel, rostoModel);
            this.validateL021(result, anexoLModel, campoQ4AnexoB, linha);
            this.validateL090(result, anexoLModel, campoQ4AnexoB, linha);
            if (!(campoQ4AnexoB != null || codActividade == null && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento) && nifEntidadePagadora == null)) {
                result.add(new DeclValidationMessage("L023", anexoLModel.getLink(linkCampoQ4AnexoB), new String[]{anexoLModel.getLink(linkCampoQ4AnexoB)}));
            }
            if (codActividade == null || Modelo3IRSValidatorUtil.in(codActividade, new long[]{101, 102, 103, 201, 202, 203, 204, 205, 301, 302, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 501, 601, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 801, 802, 999})) continue;
            result.add(new DeclValidationMessage("L024", anexoLModel.getLink(linkCodActividade), new String[]{anexoLModel.getLink(linkCodActividade)}));
        }
        long anexoBq04C403Value = anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04C403() != null ? anexoBModel.getQuadro04().getAnexoBq04C403() : 0;
        String string = linkAnexoBq04C403 = anexoBModel != null ? anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C403") : "aAnexoB.qQuadro04.fanexoBq04C403";
        if (totalRendimentoActividadeSem999CampoAnxB403 > anexoBq04C403Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L025", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), linkAnexoBq04C403}));
        }
        long anexoBq04C404Value = anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04C404() != null ? anexoBModel.getQuadro04().getAnexoBq04C404() : 0;
        String string2 = linkAnexoBq04C404 = anexoBModel != null ? anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C404") : "aAnexoB.qQuadro04.fanexoBq04C404";
        if (totalRendimentoCampoAnxB404 > anexoBq04C404Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L026", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), linkAnexoBq04C404}));
        }
        this.validateL091(result, anexoLModel, anexoBModel, totalRendimentoCampoAnxB440);
        long anexoBq04C420Value = anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04C420() != null ? anexoBModel.getQuadro04().getAnexoBq04C420() : 0;
        String string3 = linkAnexoBq04C420 = anexoBModel != null ? anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C420") : "aAnexoB.qQuadro04.fanexoBq04C420";
        if (totalRendimentoCampoAnxB420 > anexoBq04C420Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L027", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), linkAnexoBq04C420}));
        }
        if (totalRendimentoActividade999 > anexoBq04C404Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L028", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), linkAnexoBq04C404}));
        }
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || quadro04AnexoLModel.getAnexoLq04T2().isEmpty())) {
            result.add(new DeclValidationMessage("L053", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2")}));
        }
        if (!(quadro04AnexoLModel.getAnexoLq04T1().isEmpty() && quadro04AnexoLModel.getAnexoLq04T2().isEmpty() && quadro04AnexoLModel.getAnexoLq04T3().isEmpty() && anexoLModel.getQuadro05().getAnexoLq05T1().isEmpty() || anexoLModel.getQuadro06().isQ06B1OP1Selected() || anexoLModel.getQuadro06().isQ06B1OP2Selected())) {
            result.add(new DeclValidationMessage("L029", anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP1"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP1"), anexoLModel.getLink("aAnexoL.qQuadro06.fq06B1OP2")}));
        }
        return result;
    }

    protected void validateL021(ValidationResult result, AnexoLModel anexoLModel, Long campoQ4AnexoB, int linha) {
        String linkCurrentLine = "aAnexoL.qQuadro04.tanexoLq04T2.l" + (linha + 1);
        String linkCampoQ4AnexoB = linkCurrentLine + ".c" + AnexoLq04T2_LinhaBase.Property.CAMPOQ4ANEXOB.getIndex();
        if (!(campoQ4AnexoB == null || Modelo3IRSValidatorUtil.in(campoQ4AnexoB, new long[]{403, 404, 420, 440}))) {
            result.add(new DeclValidationMessage("L021", anexoLModel.getLink(linkCampoQ4AnexoB), new String[]{anexoLModel.getLink(linkCampoQ4AnexoB)}));
        }
    }

    private void validateL091(ValidationResult result, AnexoLModel anexoLModel, AnexoBModel anexoBModel, long totalRendimentoCampoAnxB440) {
        String linkAnexoBq04C440;
        long anexoBq04C440Value = anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04C440() != null ? anexoBModel.getQuadro04().getAnexoBq04C440() : 0;
        String string = linkAnexoBq04C440 = anexoBModel != null ? anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C440") : "aAnexoB.qQuadro04.fanexoBq04C440";
        if (totalRendimentoCampoAnxB440 > anexoBq04C440Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L091", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), linkAnexoBq04C440}));
        }
    }

    protected void validateL090(ValidationResult result, AnexoLModel anexoLModel, Long campoQ4AnexoB, int linha) {
        Long ano = anexoLModel.getQuadro02().getAnexoLq02C01();
        if (ano != null && campoQ4AnexoB != null && (campoQ4AnexoB == 440 && ano < 2014 || campoQ4AnexoB == 403 && ano > 2013)) {
            String linkCurrentLine = "aAnexoL.qQuadro04.tanexoLq04T2.l" + (linha + 1);
            String linkCampoQ4AnexoB = linkCurrentLine + ".c" + AnexoLq04T2_LinhaBase.Property.CAMPOQ4ANEXOB.getIndex();
            result.add(new DeclValidationMessage("L090", anexoLModel.getLink(linkCampoQ4AnexoB), new String[]{anexoLModel.getLink(linkCampoQ4AnexoB), anexoLModel.getLink("aAnexoL.qQuadro02.fanexoLq02C01")}));
        }
    }

    protected void validateL085(pt.dgci.modelo3irs.v2015.model.anexol.Quadro04 quadro4, ValidationResult result, AnexoLModel anexoLmodel) {
        if (quadro4.getAnexoLq04T2().size() > 100) {
            result.add(new DeclValidationMessage("L085", anexoLmodel.getLink("aAnexoL.qQuadro04.tanexoLq04T2"), new String[]{anexoLmodel.getLink("aAnexoL.qQuadro04.tanexoLq04T2")}));
        }
    }

    public static void validateL019(List<Tuple> processedTuples, AnexoLModel anexoLModel, AnexoLq04T2_Linha anexoLq04T2Linha, int numLinha, ValidationResult result) {
        Long campoQ4AnexoB = anexoLq04T2Linha.getCampoQ4AnexoB();
        Long codActividade = anexoLq04T2Linha.getCodActividade();
        Long nifEntidadePagadora = anexoLq04T2Linha.getEntidade();
        if (campoQ4AnexoB != null && codActividade != null && nifEntidadePagadora != null) {
            Tuple tuple = new Tuple(new Object[]{campoQ4AnexoB, codActividade, nifEntidadePagadora});
            if (processedTuples.contains(tuple)) {
                result.add(new DeclValidationMessage("L019", anexoLModel.getLink(AnexoLq04T2_Linha.getLink(numLinha, AnexoLq04T2_LinhaBase.Property.CAMPOQ4ANEXOB)), new String[]{anexoLModel.getLink(AnexoLq04T2_Linha.getLink(numLinha, AnexoLq04T2_LinhaBase.Property.CAMPOQ4ANEXOB))}));
            } else {
                processedTuples.add(tuple);
            }
        }
    }

    protected ValidationResult validaT3(RostoModel rostoModel, AnexoLModel anexoLModel, pt.dgci.modelo3irs.v2015.model.anexol.Quadro04 quadro04AnexoLModel, AnexoCModel anexoCModel) {
        String linkAnexoCq04C459;
        String linkAnexoCq04C461;
        String linkAnexoCq04C460;
        ValidationResult result = new ValidationResult();
        long totalLucro999 = 0;
        long totalPrejuizo999 = 0;
        long totalLucroSem999 = 0;
        long totalPrejuizoSem999 = 0;
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        this.validateL088(quadro04AnexoLModel, result, anexoLModel);
        for (int linha = 0; linha < quadro04AnexoLModel.getAnexoLq04T3().size(); ++linha) {
            AnexoLq04T3_Linha anexoLq04T3Linha = quadro04AnexoLModel.getAnexoLq04T3().get(linha);
            String linkCurrentLine = "aAnexoL.qQuadro04.tanexoLq04T3.l" + (linha + 1);
            String linkCodActividade = linkCurrentLine + ".c" + AnexoLq04T3_LinhaBase.Property.CODACTIVIDADE.getIndex();
            String linkLucro = linkCurrentLine + ".c" + AnexoLq04T3_LinhaBase.Property.LUCRO.getIndex();
            String linkPrejuizo = linkCurrentLine + ".c" + AnexoLq04T3_LinhaBase.Property.PREJUIZO.getIndex();
            String linkNifEntidadePagadora = linkCurrentLine + ".c" + AnexoLq04T3_LinhaBase.Property.ENTIDADE.getIndex();
            Long codActividade = anexoLq04T3Linha.getCodActividade();
            Long lucro = anexoLq04T3Linha.getLucro();
            Long prejuizo = anexoLq04T3Linha.getPrejuizo();
            Long nifEntidadePagadora = anexoLq04T3Linha.getEntidade();
            if (codActividade != null && codActividade == 999) {
                if (lucro != null) {
                    totalLucro999+=lucro.longValue();
                }
                if (prejuizo != null) {
                    totalPrejuizo999+=prejuizo.longValue();
                }
            }
            if (codActividade != null && lucro != null && codActividade != 999) {
                totalLucroSem999+=lucro.longValue();
            }
            if (codActividade != null && prejuizo != null && codActividade != 999) {
                totalPrejuizoSem999+=prejuizo.longValue();
            }
            Quadro04Validator.validateL030(processedTuples, anexoLModel, anexoLq04T3Linha, linha + 1, result);
            if (codActividade == null && lucro == null && Modelo3IRSValidatorUtil.isEmptyOrZero(prejuizo)) {
                result.add(new DeclValidationMessage("L031", anexoLModel.getLink(linkCurrentLine), new String[]{anexoLModel.getLink(linkCurrentLine)}));
            }
            this.validateL071(result, anexoLq04T3Linha, anexoLModel, linha + 1);
            this.validateL072(result, anexoLq04T3Linha, anexoLModel, linha + 1);
            this.validateL073(result, anexoLq04T3Linha, linha + 1, rostoModel.getQuadro07(), rostoModel.getQuadro05(), rostoModel.getQuadro03(), anexoLModel, rostoModel);
            if (!(codActividade == null || Modelo3IRSValidatorUtil.in(codActividade, new long[]{101, 102, 103, 201, 202, 203, 204, 205, 301, 302, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 501, 601, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 801, 802, 999}))) {
                result.add(new DeclValidationMessage("L032", anexoLModel.getLink(linkCodActividade), new String[]{anexoLModel.getLink(linkCodActividade)}));
            }
            if (codActividade != null && lucro == null && Modelo3IRSValidatorUtil.isEmptyOrZero(prejuizo)) {
                result.add(new DeclValidationMessage("L033", anexoLModel.getLink(linkCodActividade), new String[]{anexoLModel.getLink(linkCodActividade), anexoLModel.getLink(linkLucro), anexoLModel.getLink(linkPrejuizo)}));
            }
            if (!(codActividade != null || lucro == null && Modelo3IRSValidatorUtil.isEmptyOrZero(prejuizo) && nifEntidadePagadora == null)) {
                result.add(new DeclValidationMessage("L034", anexoLModel.getLink(linkCodActividade), new String[]{anexoLModel.getLink(linkCodActividade)}));
            }
            if (lucro == null || lucro < 0 || prejuizo == null || prejuizo <= 0 || !Modelo3IRSv2015Parameters.instance().isFase2()) continue;
            result.add(new DeclValidationMessage("L035", anexoLModel.getLink(linkLucro), new String[]{anexoLModel.getLink(linkLucro), anexoLModel.getLink(linkPrejuizo)}));
        }
        long anexoCq04C460Value = anexoCModel != null && anexoCModel.getQuadro04().getAnexoCq04C460() != null ? anexoCModel.getQuadro04().getAnexoCq04C460() : 0;
        String string = linkAnexoCq04C460 = anexoCModel != null ? anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C460") : "aAnexoC.qQuadro04.fanexoCq04C460";
        if (totalLucroSem999 > 0 && totalLucroSem999 > anexoCq04C460Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L036", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), linkAnexoCq04C460}));
        }
        long anexoCq04C459Value = anexoCModel != null && anexoCModel.getQuadro04().getAnexoCq04C459() != null ? anexoCModel.getQuadro04().getAnexoCq04C459() : 0;
        String string2 = linkAnexoCq04C459 = anexoCModel != null ? anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C459") : "aAnexoC.qQuadro04.fanexoCq04C459";
        if (totalPrejuizoSem999 > 0 && totalPrejuizoSem999 > anexoCq04C459Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L037", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), linkAnexoCq04C459}));
        }
        long anexoCq04C461Value = anexoCModel != null && anexoCModel.getQuadro04().getAnexoCq04C461() != null ? anexoCModel.getQuadro04().getAnexoCq04C461() : 0;
        String string3 = linkAnexoCq04C461 = anexoCModel != null ? anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C461") : "aAnexoC.qQuadro04.fanexoCq04C461";
        if (totalLucro999 > anexoCq04C461Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L038", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), linkAnexoCq04C461}));
        }
        if (totalPrejuizo999 > anexoCq04C461Value && Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("L039", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), linkAnexoCq04C461}));
        }
        if (!(Modelo3IRSv2015Parameters.instance().isFase2() || quadro04AnexoLModel.getAnexoLq04T3().isEmpty())) {
            result.add(new DeclValidationMessage("L057", anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro04.tanexoLq04T3")}));
        }
        return result;
    }

    protected void validateL088(pt.dgci.modelo3irs.v2015.model.anexol.Quadro04 quadro4, ValidationResult result, AnexoLModel anexoLmodel) {
        if (quadro4.getAnexoLq04T3().size() > 100) {
            result.add(new DeclValidationMessage("L088", anexoLmodel.getLink("aAnexoL.qQuadro04.tanexoLq04T3"), new String[]{anexoLmodel.getLink("aAnexoL.qQuadro04.tanexoLq04T3")}));
        }
    }

    public static void validateL030(List<Tuple> processedTuples, AnexoLModel anexoLModel, AnexoLq04T3_Linha anexoLq04T3Linha, int numLinha, ValidationResult result) {
        Long codActividade = anexoLq04T3Linha.getCodActividade();
        Long nifEntidadePagadora = anexoLq04T3Linha.getEntidade();
        if (codActividade != null && nifEntidadePagadora != null) {
            Tuple tuple = new Tuple(new Object[]{codActividade, nifEntidadePagadora});
            if (processedTuples.contains(tuple)) {
                result.add(new DeclValidationMessage("L030", anexoLModel.getLink(AnexoLq04T3_Linha.getLink(numLinha, AnexoLq04T3_LinhaBase.Property.CODACTIVIDADE)), new String[]{anexoLModel.getLink(AnexoLq04T3_Linha.getLink(numLinha, AnexoLq04T3_LinhaBase.Property.CODACTIVIDADE))}));
            } else {
                processedTuples.add(tuple);
            }
        }
    }

    public void validateL075(ValidationResult result, AnexoLq04T2_Linha current, AnexoLModel anexoLModel, int numLinha) {
        if (!(current.getEntidade() == null || Modelo3IRSValidatorUtil.startsWith(current.getEntidade(), "1", "2", "3", "5", "6", "7", "9"))) {
            String linkNif = anexoLModel.getLink(AnexoLq04T2_Linha.getLink(numLinha, AnexoLq04T2_LinhaBase.Property.ENTIDADE));
            result.add(new DeclValidationMessage("L075", linkNif, new String[]{linkNif}));
        }
    }

    public void validateL071(ValidationResult result, AnexoLq04T3_Linha current, AnexoLModel anexoLModel, int numLinha) {
        if (!(current.getEntidade() == null || Modelo3IRSValidatorUtil.startsWith(current.getEntidade(), "1", "2", "3", "5", "6", "7", "9"))) {
            String linkNif = anexoLModel.getLink(AnexoLq04T3_Linha.getLink(numLinha, AnexoLq04T3_LinhaBase.Property.ENTIDADE));
            result.add(new DeclValidationMessage("L071", linkNif, new String[]{linkNif}));
        }
    }

    public void validateL076(ValidationResult result, AnexoLq04T2_Linha current, AnexoLModel anexoLModel, int numLinha) {
        if (!(current.getEntidade() == null || NifValidator.validate(current.getEntidade()))) {
            String linkNif = anexoLModel.getLink(AnexoLq04T2_Linha.getLink(numLinha, AnexoLq04T2_LinhaBase.Property.ENTIDADE));
            result.add(new DeclValidationMessage("L076", linkNif, new String[]{linkNif}));
        }
    }

    public void validateL072(ValidationResult result, AnexoLq04T3_Linha current, AnexoLModel anexoLModel, int numLinha) {
        if (!(current.getEntidade() == null || NifValidator.validate(current.getEntidade()))) {
            String linkNif = anexoLModel.getLink(AnexoLq04T3_Linha.getLink(numLinha, AnexoLq04T3_LinhaBase.Property.ENTIDADE));
            result.add(new DeclValidationMessage("L072", linkNif, new String[]{linkNif}));
        }
    }

    public void validateL077(ValidationResult result, AnexoLq04T2_Linha current, int numLinha, Quadro07 quadro07model, Quadro05 quadro05model, Quadro03 quadro03model, AnexoLModel anexoLModel, RostoModel rostoModel) {
        Long nif = current.getEntidade();
        if (nif != null) {
            String[] links;
            String linkNif = anexoLModel.getLink(AnexoLq04T2_Linha.getLink(numLinha, AnexoLq04T2_LinhaBase.Property.ENTIDADE));
            if (nif.equals(quadro03model.getQ03C03()) || nif.equals(quadro03model.getQ03C04()) || nif.equals(quadro07model.getQ07C1())) {
                String[] links2 = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                result.add(new DeclValidationMessage("L077", linkNif, links2));
            }
            Long[] dependentes = new Long[]{quadro03model.getQ03CD1(), quadro03model.getQ03CD2(), quadro03model.getQ03CD3(), quadro03model.getQ03CD4(), quadro03model.getQ03CD5(), quadro03model.getQ03CD6(), quadro03model.getQ03CD7(), quadro03model.getQ03CD8(), quadro03model.getQ03CD9(), quadro03model.getQ03CD10(), quadro03model.getQ03CD11(), quadro03model.getQ03CD12(), quadro03model.getQ03CD13(), quadro03model.getQ03CD14()};
            Long[] dependentesDeficientes = new Long[]{quadro03model.getQ03CDD1(), quadro03model.getQ03CDD2(), quadro03model.getQ03CDD3(), quadro03model.getQ03CDD4(), quadro03model.getQ03CDD5(), quadro03model.getQ03CDD6(), quadro03model.getQ03CDD7(), quadro03model.getQ03CDD8()};
            int campo = 1;
            for (Long nifDependente : dependentes) {
                if (nifDependente != null && nif.equals(nifDependente)) {
                    links = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD" + campo, "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                    result.add(new DeclValidationMessage("L077", linkNif, links));
                }
                ++campo;
            }
            campo = 1;
            for (Long nifDependenteDef : dependentesDeficientes) {
                if (nifDependenteDef != null && nif.equals(nifDependenteDef)) {
                    links = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CDD" + campo, "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                    result.add(new DeclValidationMessage("L077", linkNif, links));
                }
                ++campo;
            }
            Long[] ascendentes = new Long[]{quadro07model.getQ07C01(), quadro07model.getQ07C02(), quadro07model.getQ07C03(), quadro07model.getQ07C04()};
            campo = 1;
            for (Long ascendente : ascendentes) {
                if (ascendente != null && nif.equals(ascendente)) {
                    String[] links3 = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C0" + campo, "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                    result.add(new DeclValidationMessage("L077", linkNif, links3));
                }
                ++campo;
            }
            campo = 1;
            for (Rostoq07CT1_Linha afilhado : rostoModel.getQuadro07().getRostoq07CT1()) {
                if (afilhado.getNIF() != null && afilhado.getNIF().equals(nif)) {
                    String[] links4 = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", Rostoq07CT1_Linha.getlink(campo, Rostoq07CT1_LinhaBase.Property.NIF)};
                    result.add(new DeclValidationMessage("L077", linkNif, links4));
                }
                ++campo;
            }
            if (rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nif) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nif)) {
                result.add(new DeclValidationMessage("L077", linkNif, new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", Rostoq07CT1_Linha.getlink(campo, Rostoq07CT1_LinhaBase.Property.NIF)}));
            }
        }
    }

    public void validateL073(ValidationResult result, AnexoLq04T3_Linha current, int numLinha, Quadro07 quadro07model, Quadro05 quadro05model, Quadro03 quadro03model, AnexoLModel anexoLModel, RostoModel rostoModel) {
        Long nif = current.getEntidade();
        if (nif != null) {
            String[] links;
            String linkNif = anexoLModel.getLink(AnexoLq04T3_Linha.getLink(numLinha, AnexoLq04T3_LinhaBase.Property.ENTIDADE));
            if (nif.equals(quadro03model.getQ03C03()) || nif.equals(quadro03model.getQ03C04()) || nif.equals(quadro07model.getQ07C1())) {
                String[] links2 = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                result.add(new DeclValidationMessage("L073", linkNif, links2));
            }
            Long[] dependentes = new Long[]{quadro03model.getQ03CD1(), quadro03model.getQ03CD2(), quadro03model.getQ03CD3(), quadro03model.getQ03CD4(), quadro03model.getQ03CD5(), quadro03model.getQ03CD6(), quadro03model.getQ03CD7(), quadro03model.getQ03CD8(), quadro03model.getQ03CD9(), quadro03model.getQ03CD10(), quadro03model.getQ03CD11(), quadro03model.getQ03CD12(), quadro03model.getQ03CD13(), quadro03model.getQ03CD14()};
            Long[] dependentesDeficientes = new Long[]{quadro03model.getQ03CDD1(), quadro03model.getQ03CDD2(), quadro03model.getQ03CDD3(), quadro03model.getQ03CDD4(), quadro03model.getQ03CDD5(), quadro03model.getQ03CDD6(), quadro03model.getQ03CDD7(), quadro03model.getQ03CDD8()};
            int campo = 1;
            for (Long nifDependente : dependentes) {
                if (nifDependente != null && nif.equals(nifDependente)) {
                    links = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD" + campo, "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                    result.add(new DeclValidationMessage("L073", linkNif, links));
                }
                ++campo;
            }
            campo = 1;
            for (Long nifDependenteDef : dependentesDeficientes) {
                if (nifDependenteDef != null && nif.equals(nifDependenteDef)) {
                    links = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CDD" + campo, "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                    result.add(new DeclValidationMessage("L073", linkNif, links));
                }
                ++campo;
            }
            Long[] ascendentes = new Long[]{quadro07model.getQ07C01(), quadro07model.getQ07C02(), quadro07model.getQ07C03(), quadro07model.getQ07C04()};
            campo = 1;
            for (Long ascendente : ascendentes) {
                if (ascendente != null && nif.equals(ascendente)) {
                    String[] links3 = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C0" + campo, "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1"};
                    result.add(new DeclValidationMessage("L073", linkNif, links3));
                }
                ++campo;
            }
            campo = 1;
            for (Rostoq07CT1_Linha afilhado : rostoModel.getQuadro07().getRostoq07CT1()) {
                if (afilhado.getNIF() != null && afilhado.getNIF().equals(nif)) {
                    String[] links4 = new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", Rostoq07CT1_Linha.getlink(campo, Rostoq07CT1_LinhaBase.Property.NIF)};
                    result.add(new DeclValidationMessage("L073", linkNif, links4));
                }
                ++campo;
            }
            if (rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nif) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nif)) {
                result.add(new DeclValidationMessage("L073", linkNif, new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", Rostoq07CT1_Linha.getlink(campo, Rostoq07CT1_LinhaBase.Property.NIF)}));
            }
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        return new ValidationResult();
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoL");
        for (FormKey key : keys) {
            AnexoLModel anexoLModel = (AnexoLModel)model.getAnexo(key);
            pt.dgci.modelo3irs.v2015.model.anexol.Quadro04 quadro04Model = anexoLModel.getQuadro04();
            AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(anexoLModel.getAnexoLTitular());
            AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(anexoLModel.getAnexoLTitular());
            result.addAllFrom(this.validaT1(rostoModel, anexoLModel, quadro04Model, anexoAModel));
            result.addAllFrom(this.validaT2(rostoModel, anexoLModel, quadro04Model, anexoBModel));
            result.addAllFrom(this.validaT3(rostoModel, anexoLModel, quadro04Model, anexoCModel));
        }
        return result;
    }
}

