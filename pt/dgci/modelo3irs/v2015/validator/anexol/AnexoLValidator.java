/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexol;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class AnexoLValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoLValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoL");
        for (FormKey key : keys) {
            AnexoLModel anexoLModel = (AnexoLModel)model.getAnexo(key);
            if (anexoLModel.isEmpty()) {
                result.add(new DeclValidationMessage("L001", anexoLModel.getLink("aAnexoL"), new String[]{anexoLModel.getLink("aAnexoL")}));
            }
            this.validateL068(result, rostoModel, anexoLModel);
            this.validateL080(result, rostoModel, anexoLModel);
        }
        this.validateL002(result, model);
        return result;
    }

    protected void validateL080(ValidationResult result, RostoModel rostoModel, AnexoLModel anexoLModel) {
        long ano2009 = 2009;
        if (anexoLModel != null && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() > 2009 && rostoModel.getQuadro05().isQ05B1OP4Selected()) {
            result.add(new DeclValidationMessage("L080", anexoLModel.getLink("aAnexoL"), new String[]{"aRosto.qQuadro05.fq05B1OP4"}));
        }
    }

    protected void validateL002(ValidationResult result, DeclaracaoModel model) {
        ArrayList<FormKey> processedKeys = new ArrayList<FormKey>();
        HashMap<Long, String> processedNifs = new HashMap<Long, String>();
        List<FormKey> keys = model.getAllAnexosByType("AnexoL");
        for (FormKey key : keys) {
            AnexoLModel otherModel;
            AnexoLModel anexoLModel = (AnexoLModel)model.getAnexo(key);
            if (processedKeys.contains(key)) {
                otherModel = (AnexoLModel)model.getAnexo(new FormKey(AnexoLModel.class, key.getSubId()));
                if (otherModel == null) {
                    otherModel = anexoLModel;
                }
                result.add(new DeclValidationMessage("L002", anexoLModel.getLink("aAnexoL"), new String[]{otherModel.getLink("aAnexoL.qQuadro03.fanexoLq03C04"), anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C04")}));
            } else {
                processedKeys.add(key);
            }
            if (anexoLModel.getQuadro03().getAnexoLq03C04() == null) continue;
            if (processedNifs.get(anexoLModel.getQuadro03().getAnexoLq03C04()) != null) {
                otherModel = (AnexoLModel)model.getAnexo(new FormKey(AnexoLModel.class, (String)processedNifs.get(anexoLModel.getQuadro03().getAnexoLq03C04())));
                if (otherModel == null) {
                    otherModel = anexoLModel;
                }
                result.add(new DeclValidationMessage("L002", anexoLModel.getLink("aAnexoL"), new String[]{otherModel.getLink("aAnexoL.qQuadro03.fanexoLq03C04"), anexoLModel.getLink("aAnexoL.qQuadro03.fanexoLq03C04")}));
                continue;
            }
            processedNifs.put(anexoLModel.getQuadro03().getAnexoLq03C04(), key.getSubId());
        }
    }

    protected void validateL068(ValidationResult result, RostoModel rostoModel, AnexoLModel anexoLModel) {
        if (!(anexoLModel.isEmpty() || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() >= 2009)) {
            result.add(new DeclValidationMessage("L068", anexoLModel.getLink("aAnexoL"), new String[]{anexoLModel.getLink("aAnexoL.qQuadro02.fanexoLq02C01")}));
        }
    }
}

