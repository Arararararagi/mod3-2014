/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog1;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Freguesia;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.Month;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;
import pt.opensoft.util.Year;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    protected boolean isAnoValido;

    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        List<FormKey> keys = model.getAllAnexosByType("AnexoG1");
        for (FormKey key : keys) {
            long anexoG1q05C1Value;
            long anexoG1q05C2Value;
            AnexoG1Model anexoG1Model = (AnexoG1Model)model.getAnexo(key);
            if (anexoG1Model == null) {
                return result;
            }
            Quadro05 q05Model = anexoG1Model.getQuadro05();
            if (!(Modelo3IRSv2015Parameters.instance.isFase2() || q05Model.isEmpty())) {
                result.add(new DeclValidationMessage("G1013", "aAnexoG1.qQuadro05", new String[]{"aAnexoG1.qQuadro05"}));
            }
            if (q05Model.getAnexoG1q05T1().size() > 40) {
                result.add(new DeclValidationMessage("G1014", "aAnexoG1.qQuadro05.tanexoG1q05T1", new String[]{"aAnexoG1.qQuadro05.tanexoG1q05T1"}));
            }
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            HashMap<Tuple, Long> tupleCodigoMap = new HashMap<Tuple, Long>();
            long somaControloRealizacao = 0;
            long somaControloAquisicao = 0;
            for (int i = 0; i < q05Model.getAnexoG1q05T1().size(); ++i) {
                AnexoG1q05T1_Linha line = q05Model.getAnexoG1q05T1().get(i);
                String freguesia = line.getFreguesia() != null ? line.getFreguesia() : "";
                String tipo = line.getTipoPredio() != null ? line.getTipoPredio() : "";
                long artigo = line.getArtigo() != null ? line.getArtigo() : 0;
                String fraccao = line.getFraccao() != null ? line.getFraccao() : "";
                long codigo = line.getCodigo() != null ? line.getCodigo() : 0;
                long anoAquisicao = line.getAno() != null ? line.getAno() : 0;
                long mesAquisicao = line.getMes() != null ? line.getMes() : 0;
                long diaAquisicao = line.getDia() != null ? line.getDia() : 0;
                long valorRealizacao = line.getRealizacao() != null ? line.getRealizacao() : 0;
                long valorAquisicao = line.getAquisicao() != null ? line.getAquisicao() : 0;
                String lineLink = "aAnexoG1.qQuadro05.tanexoG1q05T1.l" + (i + 1);
                String freguesiaLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.FREGUESIA.getIndex();
                String tipoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.TIPOPREDIO.getIndex();
                String artigoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.ARTIGO.getIndex();
                String fraccaoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.FRACCAO.getIndex();
                String anoAquisicaoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.ANO.getIndex();
                String mesAquisicaoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.MES.getIndex();
                String diaAquisicaoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.DIA.getIndex();
                String codigoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.CODIGO.getIndex();
                String valorRealizacaoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.REALIZACAO.getIndex();
                String valorAquisicaoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.AQUISICAO.getIndex();
                Tuple tuple = new Tuple(new Object[]{freguesia, tipo, artigo, fraccao, codigo, anoAquisicao, mesAquisicao, diaAquisicao});
                if (processedTuples.contains(tuple)) {
                    result.add(new DeclValidationMessage("G1015", lineLink, new String[]{lineLink}));
                } else {
                    processedTuples.add(tuple);
                }
                if (StringUtil.isEmpty(freguesia) && StringUtil.isEmpty(tipo) && Modelo3IRSValidatorUtil.isEmptyOrZero(artigo) && StringUtil.isEmpty(fraccao) && Modelo3IRSValidatorUtil.isEmptyOrZero(codigo) && Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao) && Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao) && Modelo3IRSValidatorUtil.isEmptyOrZero(diaAquisicao) && Modelo3IRSValidatorUtil.isEmptyOrZero(valorRealizacao) && Modelo3IRSValidatorUtil.isEmptyOrZero(valorAquisicao)) {
                    result.add(new DeclValidationMessage("G1016", lineLink, new String[]{lineLink}));
                }
                if (!StringUtil.isEmpty(line.getFreguesia()) && (StringUtil.isEmpty(tipo) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getCodigo()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getAno()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getMes()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getDia()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getRealizacao()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getAquisicao()))) {
                    result.add(new DeclValidationMessage("G1017", lineLink, new String[]{freguesiaLink, tipoLink, codigoLink, anoAquisicaoLink, mesAquisicaoLink, diaAquisicaoLink, valorRealizacaoLink, valorAquisicaoLink}));
                }
                if (!(!StringUtil.isEmpty(line.getFreguesia()) || StringUtil.isEmpty(tipo) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getCodigo()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getAno()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getMes()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getDia()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getAquisicao()))) {
                    result.add(new DeclValidationMessage("G1018", lineLink, new String[]{freguesiaLink}));
                }
                ListMap freguesiasCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
                if (!(StringUtil.isEmpty(line.getFreguesia()) || freguesiasCatalog.containsKey(line.getFreguesia()))) {
                    result.add(new DeclValidationMessage("G1019", freguesiaLink, new String[]{freguesiaLink}));
                }
                if (!(StringUtil.isEmpty(line.getTipoPredio()) || StringUtil.in(line.getTipoPredio(), new String[]{"U", "R", "O"}))) {
                    result.add(new DeclValidationMessage("G1020", tipoLink, new String[]{tipoLink}));
                }
                if (!StringUtil.isEmpty(line.getTipoPredio()) && StringUtil.in(line.getTipoPredio(), new String[]{"U", "R"}) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getArtigo())) {
                    result.add(new DeclValidationMessage("G1021", tipoLink, new String[]{tipoLink, artigoLink}));
                }
                if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(line.getArtigo()) || !StringUtil.isEmpty(line.getTipoPredio()) && (StringUtil.isEmpty(line.getTipoPredio()) || StringUtil.in(line.getTipoPredio(), new String[]{"U", "R"})))) {
                    result.add(new DeclValidationMessage("G1022", tipoLink, new String[]{artigoLink, tipoLink}));
                }
                if (!(StringUtil.isEmpty(fraccao) || Pattern.matches("[a-zA-Z0-9 ]*", (CharSequence)fraccao))) {
                    result.add(new DeclValidationMessage("G1023", fraccaoLink, new String[]{fraccaoLink}));
                }
                if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(line.getCodigo()) || line.getCodigo() >= 1 && line.getCodigo() <= 2)) {
                    result.add(new DeclValidationMessage("G1024", codigoLink, new String[]{codigoLink}));
                }
                if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(codigo) || codigo != 2 || Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio) || anoExercicio >= 2009)) {
                    result.add(new DeclValidationMessage("G1032", "aAnexoG1.qQuadro05", new String[]{codigoLink, "aRosto.qQuadro02.fq02C02"}));
                }
                if (!(StringUtil.isEmpty(line.getFraccao()) || !StringUtil.isEmpty(line.getTipoPredio()) && (StringUtil.isEmpty(line.getTipoPredio()) || StringUtil.in(line.getTipoPredio(), new String[]{"U", "R"})))) {
                    result.add(new DeclValidationMessage("G1028", fraccaoLink, new String[]{fraccaoLink, tipoLink}));
                }
                this.isAnoValido = true;
                if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao) || Quadro05Validator.isValidYear((short)anoAquisicao) && anoAquisicao <= 1988 || codigo != 1)) {
                    result.add(new DeclValidationMessage("G1025", anoAquisicaoLink, new String[]{anoAquisicaoLink}));
                    this.isAnoValido = false;
                }
                boolean isMesValido = true;
                if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao) || Month.isMonth(String.valueOf(mesAquisicao)))) {
                    result.add(new DeclValidationMessage("G1026", mesAquisicaoLink, new String[]{mesAquisicaoLink}));
                    isMesValido = false;
                }
                boolean isDiaValido = true;
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(line.getDia())) {
                    if (line.getDia() < 1 || line.getDia() > 31) {
                        isDiaValido = false;
                    } else if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(line.getAno()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getMes()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getDia()) || !this.isAnoValido || !isMesValido)) {
                        try {
                            DateTime.getFormater("yyyy-MM-dd", false).parse(StringUtil.padZeros(line.getAno(), 4) + "-" + StringUtil.padZeros(line.getMes(), 2) + "-" + StringUtil.padZeros(line.getDia(), 2));
                        }
                        catch (ParseException e) {
                            isDiaValido = false;
                        }
                    }
                    if (!isDiaValido) {
                        result.add(new DeclValidationMessage("G1027", diaAquisicaoLink, new String[]{diaAquisicaoLink}));
                    }
                }
                if (tupleCodigoMap.containsKey(tuple = new Tuple(new Object[]{freguesia, tipo, artigo, fraccao})) && ((Long)tupleCodigoMap.get(tuple) == 1 && codigo == 2 || (Long)tupleCodigoMap.get(tuple) == 2 && codigo == 1)) {
                    result.add(new DeclValidationMessage("G1029", lineLink, new String[]{lineLink}));
                } else {
                    tupleCodigoMap.put(tuple, codigo);
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(line.getRealizacao())) {
                    somaControloRealizacao+=line.getRealizacao().longValue();
                }
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(line.getAquisicao())) continue;
                somaControloAquisicao+=line.getAquisicao().longValue();
            }
            long l = anexoG1q05C1Value = q05Model.getAnexoG1q05C1() != null ? q05Model.getAnexoG1q05C1() : 0;
            if (anexoG1q05C1Value != somaControloRealizacao) {
                result.add(new DeclValidationMessage("YG1001", "aAnexoG1.qQuadro05.fanexoG1q05C1", new String[]{"aAnexoG1.qQuadro05.fanexoG1q05C1"}));
            }
            if ((anexoG1q05C2Value = q05Model.getAnexoG1q05C2() != null ? q05Model.getAnexoG1q05C2() : 0) == somaControloAquisicao) continue;
            result.add(new DeclValidationMessage("YG1002", "aAnexoG1.qQuadro05.fanexoG1q05C2", new String[]{"aAnexoG1.qQuadro05.fanexoG1q05C2"}));
        }
        return result;
    }

    public static boolean isValidYear(short year) {
        return Year.isYear(year) && year > 1900;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoG1");
        for (FormKey key : keys) {
            AnexoG1Model anexoG1Model = (AnexoG1Model)model.getAnexo(key);
            if (anexoG1Model == null) {
                return result;
            }
            Quadro05 q05Model = anexoG1Model.getQuadro05();
            for (int i = 0; i < q05Model.getAnexoG1q05T1().size(); ++i) {
                AnexoG1q05T1_Linha line = q05Model.getAnexoG1q05T1().get(i);
                long codigo = line.getCodigo() != null ? line.getCodigo() : 0;
                long anoAquisicao = line.getAno() != null ? line.getAno() : 0;
                String lineLink = "aAnexoG1.qQuadro05.tanexoG1q05T1.l" + (i + 1);
                String anoAquisicaoLink = lineLink + ".c" + AnexoG1q05T1_LinhaBase.Property.ANO.getIndex();
                this.validateG1031(result, codigo, anoAquisicao, anoAquisicaoLink);
            }
        }
        return result;
    }

    protected void validateG1031(ValidationResult result, long codigo, long anoAquisicao, String anoAquisicaoLink) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao) || Quadro05Validator.isValidYear((short)anoAquisicao) || codigo != 2)) {
            result.add(new DeclValidationMessage("G1031", anoAquisicaoLink, new String[]{anoAquisicaoLink}));
            this.isAnoValido = false;
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

