/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog1;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.Date;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.DateTime;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoG1");
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoG1Model anexoG1Model = (AnexoG1Model)model.getAnexo(key);
            if (anexoG1Model == null) {
                return result;
            }
            Quadro04 q04Model = anexoG1Model.getQuadro04();
            if (!(Modelo3IRSv2015Parameters.instance.isFase2() || q04Model.isEmpty())) {
                result.add(new DeclValidationMessage("G1003", "aAnexoG1.qQuadro04", new String[]{"aAnexoG1.qQuadro04"}));
            }
            if (q04Model.getAnexoG1q04T1().size() > 12) {
                result.add(new DeclValidationMessage("G1030", "aAnexoG1.qQuadro04.tanexoG1q04T1", new String[]{"aAnexoG1.qQuadro04.tanexoG1q04T1"}));
            }
            long anoExercicio = rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0;
            long somaRealizacao = 0;
            long somaAquisicao = 0;
            String somaRealizacaoLink = "aAnexoG1.qQuadro04.fanexoG1q04C401";
            String somaAquisicaoLink = "aAnexoG1.qQuadro04.fanexoG1q04C401a";
            for (int i = 0; i < q04Model.getAnexoG1q04T1().size(); ++i) {
                AnexoG1q04T1_Linha line = q04Model.getAnexoG1q04T1().get(i);
                long mesRealizacao = line.getMesRealizacao() != null ? line.getMesRealizacao() : 0;
                long anoAquisicao = line.getAnoAquisicao() != null ? line.getAnoAquisicao() : 0;
                long mesAquisicao = line.getMesAquisicao() != null ? line.getMesAquisicao() : 0;
                long valorRealizacao = line.getValorRealizacao() != null ? line.getValorRealizacao() : 0;
                long valorAquisicao = line.getValorAquisicao() != null ? line.getValorAquisicao() : 0;
                boolean mesReaValido = true;
                boolean mesAquiValido = true;
                String lineLink = "aAnexoG1.qQuadro04.tanexoG1q04T1.l" + (i + 1);
                String mesRealizacaoLink = lineLink + ".c" + AnexoG1q04T1_LinhaBase.Property.MESREALIZACAO.getIndex();
                String mesAquisicaoLink = lineLink + ".c" + AnexoG1q04T1_LinhaBase.Property.MESAQUISICAO.getIndex();
                String anoAquisicaoLink = lineLink + ".c" + AnexoG1q04T1_LinhaBase.Property.ANOAQUISICAO.getIndex();
                String valorRealizacaoLink = lineLink + ".c" + AnexoG1q04T1_LinhaBase.Property.VALORREALIZACAO.getIndex();
                String valorAquisicaoLink = lineLink + ".c" + AnexoG1q04T1_LinhaBase.Property.VALORAQUISICAO.getIndex();
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(mesRealizacao) && Modelo3IRSValidatorUtil.isEmptyOrZero(valorRealizacao) && Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao) && Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao) && Modelo3IRSValidatorUtil.isEmptyOrZero(valorAquisicao)) {
                    result.add(new DeclValidationMessage("G1004", lineLink, new String[]{lineLink}));
                }
                if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(line.getMesRealizacao()) || line.getMesRealizacao() >= 1 && line.getMesRealizacao() <= 12)) {
                    result.add(new DeclValidationMessage("G1006", "aAnexoG1.qQuadro04.tanexoG1q04T1", new String[]{mesRealizacaoLink}));
                    mesReaValido = false;
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(line.getMesRealizacao()) && mesReaValido && (Modelo3IRSValidatorUtil.isEmptyOrZero(valorRealizacao) || Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao) || Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao) || Modelo3IRSValidatorUtil.isEmptyOrZero(valorAquisicao))) {
                    result.add(new DeclValidationMessage("G1007", lineLink, new String[]{mesRealizacaoLink, valorRealizacaoLink, anoAquisicaoLink, mesAquisicaoLink, valorAquisicaoLink}));
                }
                if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(line.getMesRealizacao()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getValorRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getAnoAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getMesAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(line.getValorAquisicao()))) {
                    result.add(new DeclValidationMessage("G1008", "aAnexoG1.qQuadro04.tanexoG1q04T1", new String[]{mesRealizacaoLink}));
                    mesReaValido = false;
                }
                if (line.getMesAquisicao() != null && (line.getMesAquisicao() < 1 || line.getMesAquisicao() > 12)) {
                    result.add(new DeclValidationMessage("G1010", "aAnexoG1.qQuadro04.tanexoG1q04T1", new String[]{mesAquisicaoLink}));
                    mesAquiValido = false;
                }
                if (line.getAnoAquisicao() != null && line.getAnoAquisicao() == 0) {
                    result.add(new DeclValidationMessage("G1009", "aAnexoG1.qQuadro04.tanexoG1q04T1", new String[]{anoAquisicaoLink}));
                }
                this.validateG1040(result, line, i + 1, rostoModel);
                if (mesReaValido && mesAquiValido && anoExercicio != 0 && anoAquisicao != 0 && mesAquisicao != 0) {
                    DateTime dataRealizacao = new DateTime();
                    dataRealizacao.setDay(1);
                    dataRealizacao.setMonth((short)mesRealizacao - 1);
                    dataRealizacao.setYear((short)anoExercicio);
                    if (dataRealizacao != null) {
                        dataRealizacao.addHours(0);
                    }
                    DateTime dataAquisicao = new DateTime();
                    dataAquisicao.setDay(1);
                    dataAquisicao.setMonth((short)mesAquisicao - 1);
                    dataAquisicao.setYear((short)anoAquisicao);
                    dataAquisicao.addYears(1);
                    if (dataAquisicao != null) {
                        dataAquisicao.addHours(0);
                    }
                    if (dataRealizacao.before(dataAquisicao)) {
                        result.add(new DeclValidationMessage("G1005", "aAnexoG1.qQuadro04.tanexoG1q04T1", new String[]{anoAquisicaoLink, mesRealizacaoLink}));
                    }
                }
                somaRealizacao+=!Modelo3IRSValidatorUtil.isEmptyOrZero(line.getValorRealizacao()) ? line.getValorRealizacao() : 0;
                somaAquisicao+=!Modelo3IRSValidatorUtil.isEmptyOrZero(line.getValorAquisicao()) ? line.getValorAquisicao() : 0;
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(q04Model.getAnexoG1q04C401()) || somaRealizacao == q04Model.getAnexoG1q04C401())) {
                result.add(new DeclValidationMessage("G1011", "aAnexoG1.qQuadro04.fanexoG1q04C401", new String[]{somaRealizacaoLink}));
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(q04Model.getAnexoG1q04C401a()) || somaAquisicao == q04Model.getAnexoG1q04C401a()) continue;
            result.add(new DeclValidationMessage("G1012", "aAnexoG1.qQuadro04.fanexoG1q04C401a", new String[]{somaAquisicaoLink}));
        }
        return result;
    }

    protected void validateG1040(ValidationResult result_input, AnexoG1q04T1_Linha line_input, int numLinha_input, RostoModel rostoModel_input) {
        if (line_input.getAnoAquisicao() != null && (line_input.getAnoAquisicao() < 1900 || line_input.getAnoAquisicao() > 1988 && rostoModel_input.getQuadro02().getQ02C02() != null && rostoModel_input.getQuadro02().getQ02C02() > 2010 && line_input.getMesRealizacao() != null && line_input.getMesRealizacao() > 7)) {
            String lineLink = "aAnexoG1.qQuadro04.tanexoG1q04T1.l" + numLinha_input;
            String anoAquisicaoLink = lineLink + ".c" + AnexoG1q04T1_LinhaBase.Property.ANOAQUISICAO.getIndex();
            result_input.add(new DeclValidationMessage("G1040", "aAnexoG1.qQuadro04.tanexoG1q04T1", new String[]{anoAquisicaoLink}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

