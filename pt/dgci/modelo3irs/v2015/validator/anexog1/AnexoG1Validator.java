/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog1;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class AnexoG1Validator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoG1Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoG1Model anexoG1Model = (AnexoG1Model)model.getAnexo(AnexoG1Model.class);
        if (anexoG1Model.isEmpty()) {
            result.add(new DeclValidationMessage("G1000", "aAnexoG1", new String[]{"aAnexoG1"}));
        }
        if (!Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("G1037", "aAnexoG1", new String[]{"aAnexoG1"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

