/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog1.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog1.AnexoG1Validator;
import pt.dgci.modelo3irs.v2015.validator.anexog1.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog1.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog1.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog1.net.Quadro05NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoG1NETValidator
extends AnexoG1Validator {
    public AnexoG1NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        return result;
    }
}

