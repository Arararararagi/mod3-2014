/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_AnexoFQ4;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.ListMap;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro07 quadro07Model = anexoFModel.getQuadro07();
        Quadro04 quadro04Model = anexoFModel.getQuadro04();
        if (quadro07Model.getAnexoFq07T1().size() > 150) {
            result.add(new DeclValidationMessage("F072", "aAnexoF.qQuadro07", new String[]{"aAnexoF.qQuadro07.tanexoFq07T1"}));
        }
        this.validateAnexoFq07T1(model, quadro04Model, quadro07Model, rostoModel, result);
        return result;
    }

    private void validateAnexoFq07T1(DeclaracaoModel model, Quadro04 quadro04Model, Quadro07 quadro07Model, RostoModel rostoModel, ValidationResult result) {
        EventList<AnexoFq07T1_Linha> linhas = quadro07Model.getAnexoFq07T1();
        ArrayList<AnexoFq07T1_Linha> processedLines = new ArrayList<AnexoFq07T1_Linha>();
        ArrayList<AnexoFq07T1_Linha> processedLinesForCampoQ4 = new ArrayList<AnexoFq07T1_Linha>();
        int i = 0;
        for (AnexoFq07T1_Linha linha : linhas) {
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimento()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNanos()) || !this.repeatedLine(processedLines, linha))) {
                result.add(new DeclValidationMessage("F073", "aAnexoF.qQuadro07", new String[]{Quadro07Validator.getLinkForLinha(i)}));
            }
            if (linha.isEmpty()) {
                result.add(new DeclValidationMessage("F074", "aAnexoF.qQuadro07", new String[]{Quadro07Validator.getLinkForLinha(i)}));
            }
            ListMap catAnexoFQ4 = CatalogManager.getInstance().getCatalog(Cat_M3V2015_AnexoFQ4.class.getSimpleName());
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || catAnexoFQ4.containsKey(linha.getCampoQ4()))) {
                result.add(new DeclValidationMessage("F075", "aAnexoF.qQuadro07", new String[]{Quadro07Validator.getLinkForCampoQ4(i)}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) && this.isCampoQ4Duplicated(processedLinesForCampoQ4, linha)) {
                result.add(new DeclValidationMessage("F085", "aAnexoF.qQuadro07", new String[]{Quadro07Validator.getLinkForCampoQ4(i)}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) && (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimento()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNanos()))) {
                result.add(new DeclValidationMessage("F076", "aAnexoF.qQuadro07", new String[]{Quadro07Validator.getLinkForCampoQ4(i), Quadro07Validator.getLinkForRendimento(i), Quadro07Validator.getLinkForCampoNanos(i)}));
            }
            if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimento()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNanos()))) {
                result.add(new DeclValidationMessage("F077", "aAnexoF.qQuadro07", new String[]{Quadro07Validator.getLinkForCampoQ4(i)}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimento()) || linha.getRendimento() <= quadro04Model.getRendasByNLinha(linha.getCampoQ4()))) {
                result.add(new DeclValidationMessage("F078", "aAnexoF.qQuadro07", new String[]{"aAnexoF.qQuadro07", "aAnexoF.qQuadro04"}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || quadro04Model.existsNLinha(linha.getCampoQ4()))) {
                result.add(new DeclValidationMessage("F079", "aAnexoF.qQuadro07", new String[]{"aAnexoF.qQuadro07", "aAnexoF.qQuadro04"}));
            }
            ++i;
        }
    }

    private boolean repeatedLine(ArrayList<AnexoFq07T1_Linha> processedLines, AnexoFq07T1_Linha linha) {
        for (AnexoFq07T1_Linha processedLine : processedLines) {
            if (processedLine.getCampoQ4() == null || processedLine.getRendimento() == null || processedLine.getNanos() == null || !processedLine.getCampoQ4().equals(linha.getCampoQ4()) || !processedLine.getRendimento().equals(linha.getRendimento()) || !processedLine.getNanos().equals(linha.getNanos())) continue;
            return true;
        }
        processedLines.add(linha);
        return false;
    }

    private boolean isCampoQ4Duplicated(ArrayList<AnexoFq07T1_Linha> processedLines, AnexoFq07T1_Linha linha) {
        for (AnexoFq07T1_Linha processedLine : processedLines) {
            if (processedLine.getCampoQ4() == null || !processedLine.getCampoQ4().equals(linha.getCampoQ4())) continue;
            return true;
        }
        processedLines.add(linha);
        return false;
    }

    public static String getLinkForLinha(int linhaPos) {
        return "aAnexoF.qQuadro07.tanexoFq07T1.l" + (linhaPos + 1);
    }

    public static String getLinkForCampoQ4(int linhaPos) {
        return Quadro07Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq07T1_LinhaBase.Property.CAMPOQ4.getIndex();
    }

    public static String getLinkForRendimento(int linhaPos) {
        return Quadro07Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq07T1_LinhaBase.Property.RENDIMENTO.getIndex();
    }

    public static String getLinkForCampoNanos(int linhaPos) {
        return Quadro07Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq07T1_LinhaBase.Property.NANOS.getIndex();
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

