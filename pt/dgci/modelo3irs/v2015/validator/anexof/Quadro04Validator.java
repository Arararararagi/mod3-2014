/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Freguesia;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String F016 = "F016";

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        Quadro04 quadro04Model = anexoFModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        int max_lines = 150;
        if (quadro04Model.getAnexoFq04T1().size() > max_lines) {
            result.add(new DeclValidationMessage("F004", "aAnexoF.qQuadro04", new String[]{"aAnexoF.qQuadro04"}));
        }
        this.validateAnexoFq04T1(model, anexoFModel, quadro04Model, rostoModel, result);
        return result;
    }

    private void validateAnexoFq04T1(DeclaracaoModel model, AnexoFModel anexoFModel, Quadro04 quadro04Model, RostoModel rostoModel, ValidationResult result) {
        long anexoFq04C3Value;
        long anexoFq04C2Value;
        long anexoFq04C1Value;
        EventList<AnexoFq04T1_Linha> linhas = quadro04Model.getAnexoFq04T1();
        ArrayList<AnexoFq04T1_Linha> processedLines = new ArrayList<AnexoFq04T1_Linha>();
        ArrayList<Long> processedNLinhas = new ArrayList<Long>();
        int i = 0;
        long somaControloRendas = 0;
        long somaControloRetencoes = 0;
        long somaControloDespesas = 0;
        for (AnexoFq04T1_Linha linha : linhas) {
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendas())) {
                somaControloRendas+=linha.getRendas().longValue();
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes())) {
                somaControloRetencoes+=linha.getRetencoes().longValue();
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDespesas())) {
                somaControloDespesas+=linha.getDespesas().longValue();
            }
            if (!linha.isEmpty()) {
                if (Quadro04Validator.repeatedLine(processedLines, linha)) {
                    result.add(new DeclValidationMessage("F006", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForLinha(i)}));
                } else {
                    processedLines.add(linha);
                }
            } else if (linha.isEmpty()) {
                result.add(new DeclValidationMessage("F007", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForLinha(i)}));
            }
            ListMap fregCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
            if (!(linha.getFreguesia() == null || fregCatalog.containsKey(linha.getFreguesia()))) {
                result.add(new DeclValidationMessage("F010", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForFreguesia(i)}));
            }
            if (!StringUtil.isEmpty(linha.getTipoPredio()) && StringUtil.in(linha.getTipoPredio(), new String[]{"R", "U"}) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo())) {
                result.add(new DeclValidationMessage("F013", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForTipo(i), Quadro04Validator.getLinkForArtigo(i)}));
            }
            if (!(StringUtil.isEmpty(linha.getTipoPredio()) || StringUtil.in(linha.getTipoPredio(), new String[]{"U", "R", "O"}))) {
                result.add(new DeclValidationMessage("F015", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForTipo(i)}));
            }
            this.validateF016(result, i, linha);
            if (!(StringUtil.isEmpty(linha.getFraccao()) || Pattern.matches("[a-zA-Z0-9 ]*", (CharSequence)linha.getFraccao()))) {
                result.add(new DeclValidationMessage("F018", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForFraccao(i)}));
            }
            if (!(StringUtil.isEmpty(linha.getFraccao()) || StringUtil.in(linha.getTipoPredio(), new String[]{"R", "U"}))) {
                result.add(new DeclValidationMessage("F054", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForFraccao(i), Quadro04Validator.getLinkForTipo(i)}));
            }
            this.validateF019(result, linha.getTitular(), Quadro04Validator.getLinkForTitular(i));
            this.validateF020(result, rostoModel, linha.getTitular(), Quadro04Validator.getLinkForTitular(i));
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getQuotaParte()) || linha.getQuotaParte() <= 10000)) {
                result.add(new DeclValidationMessage("F022", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForQuotaParte(i)}));
            }
            if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.isNIFValid(linha.getNIF()))) {
                result.add(new DeclValidationMessage("F027", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForEntidade(i)}));
            }
            if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.startsWith(linha.getNIF(), "1", "2", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("F028", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForEntidade(i)}));
            }
            if (!(linha.getNIF() == null || StringUtil.isEmpty(linha.getTitular()))) {
                boolean isEqual = false;
                if (linha.getTitular().equals("C")) {
                    Long nifA = rostoModel.getNIFTitular("A");
                    Long nifB = rostoModel.getNIFTitular("A");
                    isEqual = nifA != null && nifA.equals(linha.getNIF()) || nifB != null && nifB.equals(linha.getNIF());
                } else {
                    Long nif = rostoModel.getNIFTitular(linha.getTitular());
                    boolean bl = isEqual = nif != null && nif.equals(linha.getNIF());
                }
                if (isEqual) {
                    result.add(new DeclValidationMessage("F029", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForTitular(i), Quadro04Validator.getLinkForEntidade(i)}));
                }
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) && this.isNLinhaForaDeOrdem(processedLines, linha.getNLinha())) {
                result.add(new DeclValidationMessage("F057", Quadro04Validator.getLinkForNLinha(i), new String[]{Quadro04Validator.getLinkForNLinha(i)}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha())) {
                if (processedNLinhas.contains(linha.getNLinha())) {
                    result.add(new DeclValidationMessage("F058", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForNLinha(i)}));
                } else {
                    processedNLinhas.add(linha.getNLinha());
                }
            }
            processedLines.add(linha);
            ++i;
        }
        this.validateF099(result, anexoFModel);
        this.validateF100(result, anexoFModel);
        long l = anexoFq04C1Value = quadro04Model.getAnexoFq04C1() != null ? quadro04Model.getAnexoFq04C1() : 0;
        if (anexoFq04C1Value != somaControloRendas) {
            result.add(new DeclValidationMessage("YF001", "aAnexoF.qQuadro04", new String[]{"aAnexoF.qQuadro04.fanexoFq04C1"}));
        }
        long l2 = anexoFq04C2Value = quadro04Model.getAnexoFq04C2() != null ? quadro04Model.getAnexoFq04C2() : 0;
        if (anexoFq04C2Value != somaControloRetencoes) {
            result.add(new DeclValidationMessage("YF002", "aAnexoF.qQuadro04", new String[]{"aAnexoF.qQuadro04.fanexoFq04C2"}));
        }
        long l3 = anexoFq04C3Value = quadro04Model.getAnexoFq04C3() != null ? quadro04Model.getAnexoFq04C3() : 0;
        if (anexoFq04C3Value != somaControloDespesas) {
            result.add(new DeclValidationMessage("YF003", "aAnexoF.qQuadro04", new String[]{"aAnexoF.qQuadro04.fanexoFq04C3"}));
        }
    }

    protected void validateF099(ValidationResult result, AnexoFModel anexoFModel) {
        HashMap<String, Long> calculatePercentagem = new HashMap<String, Long>();
        for (AnexoFq04T1_Linha linha : anexoFModel.getQuadro04().getAnexoFq04T1()) {
            if (linha.getFreguesia() == null) continue;
            if (!calculatePercentagem.containsKey(this.convertIdentificacaoMatricialAndNIFToString(linha))) {
                calculatePercentagem.put(this.convertIdentificacaoMatricialAndNIFToString(linha), linha.getQuotaParte() != null ? linha.getQuotaParte() : 0);
                continue;
            }
            Long value = (Long)calculatePercentagem.get(this.convertIdentificacaoMatricialAndNIFToString(linha));
            value = value + (linha.getQuotaParte() != null ? linha.getQuotaParte() : 0);
            calculatePercentagem.put(this.convertIdentificacaoMatricialAndNIFToString(linha), value);
        }
        int i = 0;
        for (AnexoFq04T1_Linha linha2 : anexoFModel.getQuadro04().getAnexoFq04T1()) {
            if (linha2.getFreguesia() != null && calculatePercentagem.containsKey(this.convertIdentificacaoMatricialAndNIFToString(linha2)) && (Long)calculatePercentagem.get(this.convertIdentificacaoMatricialAndNIFToString(linha2)) > 10000) {
                calculatePercentagem.remove(this.convertIdentificacaoMatricialAndNIFToString(linha2));
                result.add(new DeclValidationMessage("F099", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForQuotaParte(i)}));
            }
            ++i;
        }
    }

    private String convertIdentificacaoMatricialAndNIFToString(AnexoFq04T1_Linha linha) {
        String freguesia = linha.getFreguesia() != null ? linha.getFreguesia() : " ";
        String artigo = linha.getArtigo() != null ? linha.getArtigo().toString() : " ";
        String fraccao = linha.getFraccao() != null ? linha.getFraccao() : " ";
        String nifArrendatario = linha.getNIF() != null ? String.valueOf(linha.getNIF()) : " ";
        return freguesia + artigo + fraccao + nifArrendatario;
    }

    protected void validateF100(ValidationResult result, AnexoFModel anexoFModel) {
        HashMap<String, String> calculateTitulares = new HashMap<String, String>();
        for (AnexoFq04T1_Linha linha : anexoFModel.getQuadro04().getAnexoFq04T1()) {
            if (linha.getFreguesia() == null || linha.getTitular() == null) continue;
            if (!calculateTitulares.containsKey(this.convertIdentificacaoMatricialToString(linha))) {
                calculateTitulares.put(this.convertIdentificacaoMatricialToString(linha), linha.getTitular());
                continue;
            }
            String value = (String)calculateTitulares.get(this.convertIdentificacaoMatricialToString(linha));
            value = value + linha.getTitular();
            calculateTitulares.put(this.convertIdentificacaoMatricialToString(linha), value);
        }
        for (AnexoFq04T1_Linha linha2 : anexoFModel.getQuadro04().getAnexoFq04T1()) {
            String titular = linha2.getTitular();
            String outrosTitulares = (String)calculateTitulares.get(this.convertIdentificacaoMatricialToString(linha2));
            if (linha2.getFreguesia() == null || linha2.getTitular() == null || !calculateTitulares.containsKey(this.convertIdentificacaoMatricialToString(linha2)) || !outrosTitulares.contains((CharSequence)"A") && !outrosTitulares.contains((CharSequence)"B") || !titular.equals("C")) continue;
            calculateTitulares.remove(this.convertIdentificacaoMatricialToString(linha2));
            result.add(new DeclValidationMessage("F100", "aAnexoF.qQuadro04", new String[0]));
        }
    }

    private String convertIdentificacaoMatricialToString(AnexoFq04T1_Linha linha) {
        String freguesia = linha.getFreguesia() != null ? linha.getFreguesia() : " ";
        String artigo = linha.getArtigo() != null ? linha.getArtigo().toString() : " ";
        String fraccao = linha.getFraccao() != null ? linha.getFraccao() : " ";
        return freguesia + artigo + fraccao;
    }

    protected void validateF024(ValidationResult result, RostoModel rostoModel, int i, AnexoFq04T1_Linha linha) {
        long renda;
        long retencoes = linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes()) ? 0 : linha.getRetencoes();
        long l = renda = linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendas()) ? 0 : linha.getRendas();
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes()) || rostoModel == null || rostoModel.getQuadro02() == null || rostoModel.getQuadro02().getQ02C02() == null || ((double)retencoes <= 0.19 * (double)renda || rostoModel.getQuadro02().getQ02C02() >= 2013) && ((double)retencoes <= 0.26 * (double)renda || rostoModel.getQuadro02().getQ02C02() <= 2012))) {
            result.add(new DeclValidationMessage("F024", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForRetencoes(i)}));
        }
    }

    protected void validateF019(ValidationResult result, String titular, String linkForTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("F019", linkForTitular, new String[]{linkForTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateF020(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("F020", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    protected void validateF016(ValidationResult result, int i, AnexoFq04T1_Linha linha) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) || linha.getTipoPredio() == null || StringUtil.in(linha.getTipoPredio(), new String[]{"R", "U"}))) {
            result.add(new DeclValidationMessage("F016", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForTipo(i), Quadro04Validator.getLinkForArtigo(i)}));
        }
    }

    private boolean isNLinhaForaDeOrdem(ArrayList<AnexoFq04T1_Linha> processedLines, Long nLinha) {
        for (AnexoFq04T1_Linha linha : processedLines) {
            if (linha.getNLinha() == null || linha.getNLinha() <= nLinha) continue;
            return true;
        }
        return false;
    }

    private static String getLinkForLinha(int linhaPos) {
        return "aAnexoF.qQuadro04.tanexoFq04T1.l" + (linhaPos + 1);
    }

    private static String getLinkForNLinha(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.NLINHA.getIndex();
    }

    private static String getLinkForFreguesia(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.FREGUESIA.getIndex();
    }

    private static String getLinkForTipo(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.TIPOPREDIO.getIndex();
    }

    private static String getLinkForArtigo(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.ARTIGO.getIndex();
    }

    private static String getLinkForFraccao(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.FRACCAO.getIndex();
    }

    private static String getLinkForTitular(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.TITULAR.getIndex();
    }

    private static String getLinkForNIF(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.NIF.getIndex();
    }

    private static String getLinkForQuotaParte(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.QUOTAPARTE.getIndex();
    }

    private static String getLinkForRetencoes(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.RETENCOES.getIndex();
    }

    private static String getLinkForEntidade(int linhaPos) {
        return Quadro04Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq04T1_LinhaBase.Property.NIF.getIndex();
    }

    private static boolean repeatedLine(ArrayList<AnexoFq04T1_Linha> processedLines, AnexoFq04T1_Linha linha) {
        for (AnexoFq04T1_Linha line : processedLines) {
            if (line.isEmpty() || !Modelo3IRSValidatorUtil.equals(line.getFreguesia(), linha.getFreguesia()) || Modelo3IRSValidatorUtil.equals(line.getTipoPredio(), "O") || !Modelo3IRSValidatorUtil.equals(line.getTipoPredio(), linha.getTipoPredio()) || !Modelo3IRSValidatorUtil.equals(line.getArtigo(), linha.getArtigo()) || !Modelo3IRSValidatorUtil.equals(line.getFraccao(), linha.getFraccao()) || !Modelo3IRSValidatorUtil.equals(line.getTitular(), linha.getTitular()) || !Modelo3IRSValidatorUtil.equals(line.getNIF(), linha.getNIF())) continue;
            return true;
        }
        return false;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        if (anexoFModel == null) {
            return result;
        }
        Quadro04 quadro04Model = anexoFModel.getQuadro04();
        EventList<AnexoFq04T1_Linha> linhas = quadro04Model.getAnexoFq04T1();
        int i = 0;
        for (AnexoFq04T1_Linha linha : linhas) {
            this.validateF011(result, i, linha);
            this.validateF012(result, i, linha);
            this.validateF087(result, rostoModel, linha, AnexoFq04T1_Linha.getLink(i + 1, AnexoFq04T1_LinhaBase.Property.NIF));
            ++i;
        }
        return result;
    }

    protected void validateF087(ValidationResult result, RostoModel rostoModel, AnexoFq04T1_Linha anexoFq04T1_Linha, String linkNIF) {
        Long nif = anexoFq04T1_Linha.getNIF();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && (rostoModel.getQuadro03().isSujeitoPassivoA(nif) || rostoModel.getQuadro03().isSujeitoPassivoB(nif) || rostoModel.getQuadro03().existsInDependentes(nif) || rostoModel.getQuadro07().isConjugeFalecido(nif) || rostoModel.getQuadro07().existsInAscendentes(nif) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nif) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nif))) {
            result.add(new DeclValidationMessage("F087", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected void validateF012(ValidationResult result, int numLinha, AnexoFq04T1_Linha linha) {
        if (!(!StringUtil.isEmpty(linha.getFreguesia()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) && StringUtil.isEmpty(linha.getTipoPredio()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) && StringUtil.isEmpty(linha.getFraccao()) && StringUtil.isEmpty(linha.getTitular()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getQuotaParte()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) && linha.getRendas() == null && linha.getRetencoes() == null && linha.getDespesas() == null)) {
            result.add(new DeclValidationMessage("F012", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForFreguesia(numLinha)}));
        }
    }

    protected void validateF011(ValidationResult result, int numLinha, AnexoFq04T1_Linha linha) {
        if (!StringUtil.isEmpty(linha.getFreguesia()) && (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || StringUtil.isEmpty(linha.getTipoPredio()) || StringUtil.isEmpty(linha.getTitular()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getQuotaParte())) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes())) {
            result.add(new DeclValidationMessage("F011", "aAnexoF.qQuadro04", new String[]{Quadro04Validator.getLinkForFreguesia(numLinha), Quadro04Validator.getLinkForNLinha(numLinha), Quadro04Validator.getLinkForTipo(numLinha), Quadro04Validator.getLinkForTitular(numLinha), Quadro04Validator.getLinkForNIF(numLinha), Quadro04Validator.getLinkForQuotaParte(numLinha)}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        Quadro04 quadro04Model = anexoFModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        EventList<AnexoFq04T1_Linha> linhas = quadro04Model.getAnexoFq04T1();
        int i = 0;
        for (AnexoFq04T1_Linha linha : linhas) {
            this.validateF024(result, rostoModel, i, linha);
        }
        return result;
    }
}

