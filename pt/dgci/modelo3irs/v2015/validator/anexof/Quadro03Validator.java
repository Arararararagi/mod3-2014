/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        Quadro03 quadro03Model = anexoFModel.getQuadro03();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoFq03C02(), rostoModel.getQuadro03().getQ03C03())) {
            result.add(new DeclValidationMessage("F002", "aAnexoF.qQuadro03", new String[]{"aAnexoF.qQuadro03.fanexoFq03C02", "aRosto.qQuadro03.fq03C03"}));
        }
        if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoFq03C03(), rostoModel.getQuadro03().getQ03C04())) {
            result.add(new DeclValidationMessage("F003", "aAnexoF.qQuadro03", new String[]{"aAnexoF.qQuadro03.fanexoFq03C03", "aRosto.qQuadro03.fq03C04"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

