/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class AnexoFValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoFValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        if (anexoFModel.isEmpty()) {
            result.add(new DeclValidationMessage("F001", "aAnexoF", new String[]{"aAnexoF"}));
        }
        if (!Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("F088", "aAnexoF", new String[]{"aAnexoF"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

