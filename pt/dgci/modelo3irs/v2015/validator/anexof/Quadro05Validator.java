/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final long ANO_2013 = 2013;

    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        Quadro05 quadro05Model = anexoFModel.getQuadro05();
        if (quadro05Model.getAnexoFq05T1().size() > 20) {
            result.add(new DeclValidationMessage("F005", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.tanexoFq05T1"}));
        }
        List<Long> quadro04NlinhaList = anexoFModel.getQuadro04().getNlinhaList();
        this.validateAnexoFq05T1(model, quadro05Model, result, quadro04NlinhaList, rostoModel);
        this.validateF096(result, anexoFModel);
        if (!(quadro05Model.getAnexoFq05B1OP() == null || quadro05Model.getAnexoFq05B1OP().equals("S") || quadro05Model.getAnexoFq05B1OP().equals("N"))) {
            result.add(new DeclValidationMessage("F030", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.fanexoFq05B1OPSim"}));
        }
        this.validateF031(result, anexoFModel);
        this.validateF033(result, anexoFModel);
        if (!(quadro05Model.isEmpty() && StringUtil.isEmpty(quadro05Model.getAnexoFq05B1OP()) || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() >= 2009)) {
            result.add(new DeclValidationMessage("F059", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05"}));
        }
        this.validateF091(result, anexoFModel, rostoModel);
        this.validateF092(result, anexoFModel, rostoModel);
        this.validateF094(result, anexoFModel, rostoModel);
        List<Long> quadro05NlinhaList = anexoFModel.getQuadro05().getNlinhaList();
        this.validateF097(result, anexoFModel);
        this.validateF098(result, anexoFModel);
        this.validateF093(anexoFModel, result, quadro05NlinhaList, rostoModel);
        return result;
    }

    protected void validateF031(ValidationResult result, AnexoFModel anexoFModel) {
        Quadro05 quadro05Model = anexoFModel.getQuadro05();
        Long ano = anexoFModel.getQuadro02().getAnexoFq02C01();
        if (quadro05Model.getAnexoFq05B1OP() != null && (quadro05Model.getAnexoFq05T1().isEmpty() || quadro05Model.isEmpty()) && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("F031", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.fanexoFq05B1OPSim", "aAnexoF.qQuadro05.fanexoFq05B1OPNao", "aAnexoF.qQuadro05.tanexoFq05T1", "aAnexoF.qQuadro04"}));
        }
    }

    protected void validateF033(ValidationResult result, AnexoFModel anexoFModel) {
        Quadro05 quadro05Model = anexoFModel.getQuadro05();
        Long ano = anexoFModel.getQuadro02().getAnexoFq02C01();
        if (!(quadro05Model.getAnexoFq05T1().isEmpty() || quadro05Model.isEmpty() || quadro05Model.getAnexoFq05B1OP() != null || ano == null || ano <= 2008 || ano >= 2014)) {
            result.add(new DeclValidationMessage("F033", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.fanexoFq05B1OPSim", "aAnexoF.qQuadro05.fanexoFq05B1OPNao"}));
        }
    }

    protected void validateF096(ValidationResult result, AnexoFModel anexoFModel) {
        Long ano = anexoFModel.getQuadro02().getAnexoFq02C01();
        if ((anexoFModel.getQuadro05().getAnexoFq05B1OP() != null || anexoFModel.getQuadro05().getAnexoFq05B2OP() != null) && ano != null && ano > 2013) {
            result.add(new DeclValidationMessage("F096", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.fanexoFq05B1OPSim", "aAnexoF.qQuadro05.fanexoFq05B1OPNao", "aAnexoF.qQuadro05.fanexoFq05B2OPSim", "aAnexoF.qQuadro05.fanexoFq05B2OPNao", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateF091(ValidationResult result, AnexoFModel anexoFModel, RostoModel rostoModel) {
        if (anexoFModel.getQuadro05().getAnexoFq05B2OP() != null && (anexoFModel.getQuadro05().getAnexoFq05B2OP().equals("S") || anexoFModel.getQuadro05().getAnexoFq05B2OP().equals("N")) && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2013) {
            result.add(new DeclValidationMessage("F091", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.fanexoFq05B2OPSim", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateF092(ValidationResult result, AnexoFModel anexoFModel, RostoModel rostoModel) {
        if (anexoFModel.getQuadro05().getAnexoFq05B2OP() == null && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() == 2013 && anexoFModel.getQuadro06().getAnexoFq06T1().size() > 0) {
            result.add(new DeclValidationMessage("F092", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro06", "aAnexoF.qQuadro05.fanexoFq05B2OPSim"}));
        }
    }

    protected void validateF094(ValidationResult result, AnexoFModel anexoFModel, RostoModel rostoModel) {
        if (anexoFModel != null && anexoFModel.getQuadro05() != null && anexoFModel.getQuadro05().getAnexoFq05B2OP() != null && rostoModel != null && rostoModel.getQuadro02() != null && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() == 2013) {
            EventList<AnexoFq04T1_Linha> linhas = anexoFModel.getQuadro04().getAnexoFq04T1();
            List<Long> quadro05NlinhaList = anexoFModel.getQuadro05().getNlinhaList();
            boolean todasOcorrenciasNoQ5A = linhas != null && linhas.size() > 0;
            for (AnexoFq04T1_Linha linha : linhas) {
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || quadro05NlinhaList.contains(linha.getNLinha())) continue;
                todasOcorrenciasNoQ5A = false;
                break;
            }
            if (todasOcorrenciasNoQ5A && anexoFModel.getQuadro06() != null && (anexoFModel.getQuadro06().getAnexoFq06T1() == null || anexoFModel.getQuadro06().getAnexoFq06T1().size() == 0)) {
                result.add(new DeclValidationMessage("F094", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.fanexoFq05B2OPSim", "aAnexoF.qQuadro05.fanexoFq05B2OPNao", "aAnexoF.qQuadro04.tanexoFq04T1", "aAnexoF.qQuadro05.tanexoFq05T1", "aAnexoF.qQuadro06.tanexoFq06T1"}));
            }
        }
    }

    protected void validateF093(AnexoFModel anexoFModel, ValidationResult result, List<Long> quadro05NlinhaList, RostoModel rostoModel) {
        Quadro04 quadro04Model = anexoFModel.getQuadro04();
        EventList<AnexoFq04T1_Linha> linhas = quadro04Model.getAnexoFq04T1();
        if (anexoFModel.getQuadro05().getAnexoFq05B2OP() == null && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() == 2013 && anexoFModel.getQuadro04().getAnexoFq04T1().size() > 0) {
            for (AnexoFq04T1_Linha linha : linhas) {
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || quadro05NlinhaList.contains(linha.getNLinha())) continue;
                result.add(new DeclValidationMessage("F093", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro04", "aAnexoF.qQuadro05.fanexoFq05B2OPSim"}));
            }
        }
    }

    protected void validateF097(ValidationResult result, AnexoFModel anexoFModel) {
        Long ano = anexoFModel.getQuadro02().getAnexoFq02C01();
        if (anexoFModel.getQuadro05().getAnexoFq05B3OP() != null && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("F097", "aAnexoF.qQuadro05", new String[]{"aAnexoF.qQuadro05.fanexoFq05B3OPSim", "aAnexoF.qQuadro05.fanexoFq05B3OPNao", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateF098(ValidationResult result, AnexoFModel anexoFModel) {
        Long ano = anexoFModel.getQuadro02().getAnexoFq02C01();
        boolean condition1 = false;
        boolean condition2 = false;
        for (AnexoFq06T1_Linha linha2 : anexoFModel.getQuadro06().getAnexoFq06T1()) {
            if (linha2.isEmpty()) continue;
            condition1 = true;
            break;
        }
        for (AnexoFq04T1_Linha linha : anexoFModel.getQuadro04().getAnexoFq04T1()) {
            if (linha.isEmpty()) continue;
            condition2 = true;
            break;
        }
        if ((condition1 || condition2) && ano != null && ano > 2013 && anexoFModel.getQuadro05().getAnexoFq05B3OP() == null) {
            result.add(new DeclValidationMessage("F098", "aAnexoF.qQuadro04.tanexoFq04T1", new String[]{"aAnexoF.qQuadro04.tanexoFq04T1", "aAnexoF.qQuadro06.tanexoFq06T1", "aAnexoF.qQuadro05.fanexoFq05B3OPSim", "aAnexoF.qQuadro05.fanexoFq05B3OPNao"}));
        }
    }

    private void validateAnexoFq05T1(DeclaracaoModel model, Quadro05 quadro05Model, ValidationResult result, List<Long> quadro04NlinhaList, RostoModel rostoModel) {
        EventList<AnexoFq05T1_Linha> linhas = quadro05Model.getAnexoFq05T1();
        ArrayList<Long> processedLines = new ArrayList<Long>();
        int i = 0;
        for (AnexoFq05T1_Linha linha : linhas) {
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCamposQuadro4()) || quadro04NlinhaList.contains(linha.getCamposQuadro4()) || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() <= 2008)) {
                result.add(new DeclValidationMessage("F032", "aAnexoF.qQuadro05", new String[]{this.getLinkForQ4(i), "aAnexoF.qQuadro04"}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCamposQuadro4())) {
                if (processedLines.contains(linha.getCamposQuadro4())) {
                    result.add(new DeclValidationMessage("F055", "aAnexoF.qQuadro05", new String[]{this.getLinkForLinha(i)}));
                } else {
                    processedLines.add(linha.getCamposQuadro4());
                }
            }
            if (linha.isEmpty()) {
                result.add(new DeclValidationMessage("F056", "aAnexoF.qQuadro05", new String[]{this.getLinkForLinha(i)}));
            }
            ++i;
        }
    }

    private String getLinkForLinha(int linhaPos) {
        return "aAnexoF.qQuadro05.tanexoFq05T1.l" + (linhaPos + 1);
    }

    private String getLinkForQ4(int linhaPos) {
        return this.getLinkForLinha(linhaPos) + ".c" + AnexoFq05T1_LinhaBase.Property.CAMPOSQUADRO4.getIndex();
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

