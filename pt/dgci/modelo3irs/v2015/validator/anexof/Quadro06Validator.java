/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro06 quadro06Model = anexoFModel.getQuadro06();
        if (quadro06Model.getAnexoFq06T1().size() > 150) {
            result.add(new DeclValidationMessage("F037", "aAnexoF.qQuadro06", new String[]{"aAnexoF.qQuadro06.tanexoFq06T1"}));
        }
        this.validateAnexoFq06T1(model, quadro06Model, rostoModel, result);
        return result;
    }

    private void validateAnexoFq06T1(DeclaracaoModel model, Quadro06 quadro06Model, RostoModel rostoModel, ValidationResult result) {
        EventList<AnexoFq06T1_Linha> linhas = quadro06Model.getAnexoFq06T1();
        ArrayList<AnexoFq06T1_Linha> processedLines = new ArrayList<AnexoFq06T1_Linha>();
        int i = 0;
        for (AnexoFq06T1_Linha linha : linhas) {
            long rendaPaga_034;
            this.validateF036(result, rostoModel, i, linha);
            if (!(StringUtil.isEmpty(linha.getTitular()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNifSublocatario()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNifSenhorio()) || !this.repeatedLine(processedLines, linha))) {
                result.add(new DeclValidationMessage("F038", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForLinha(i)}));
            }
            if (linha.isEmpty()) {
                result.add(new DeclValidationMessage("F039", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForLinha(i)}));
            }
            this.validateF042(result, linha.getTitular(), Quadro06Validator.getLinkForTitular(i));
            this.validateF043(result, rostoModel, linha.getTitular(), Quadro06Validator.getLinkForTitular(i));
            if (!StringUtil.isEmpty(linha.getTitular()) && StringUtil.in(linha.getTitular(), new String[]{"B", "C"}) && Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro03().getQ03C04())) {
                result.add(new DeclValidationMessage("F044", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForTitular(i), "aRosto.qQuadro03.fq03C04"}));
            }
            if (!(linha.getNifSenhorio() == null || Modelo3IRSValidatorUtil.startsWith(linha.getNifSenhorio(), "1", "2", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("F046", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForNifSenhorio(i)}));
            }
            if (!(linha.getNifSenhorio() == null || Modelo3IRSValidatorUtil.isNIFValid(linha.getNifSenhorio()))) {
                result.add(new DeclValidationMessage("F047", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForNifSenhorio(i)}));
            }
            this.validateF048(result, rostoModel, linha, Quadro06Validator.getLinkForNifSenhorio(i));
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNifSublocatario()) || Modelo3IRSValidatorUtil.startsWith(linha.getNifSublocatario(), "1", "2", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("F069", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForNifSublocatorio(i)}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNifSublocatario()) || Modelo3IRSValidatorUtil.isNIFValid(linha.getNifSublocatario()))) {
                result.add(new DeclValidationMessage("F070", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForNifSublocatorio(i)}));
            }
            this.validateF071(result, rostoModel, linha, Quadro06Validator.getLinkForNifSublocatorio(i));
            this.validateF095(result, linha, i);
            long rendaRecebida_034 = !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendaRecebida()) ? linha.getRendaRecebida() : 0;
            long l = rendaPaga_034 = !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendaPaga()) ? linha.getRendaPaga() : 0;
            if (rendaRecebida_034 < rendaPaga_034) {
                result.add(new DeclValidationMessage("F034", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForRendaPagaSenhorio(i), Quadro06Validator.getLinkForRendaRecebida(i)}));
            }
            ++i;
        }
    }

    protected void validateF036(ValidationResult result, RostoModel rostoModel, int i, AnexoFq06T1_Linha linha) {
        long rendaRecebida;
        long l = rendaRecebida = linha != null && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendaRecebida()) ? linha.getRendaRecebida() : 0;
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes()) || rostoModel == null || rostoModel.getQuadro02() == null || rostoModel.getQuadro02().getQ02C02() == null || ((double)linha.getRetencoes().longValue() <= 0.19 * (double)rendaRecebida || rostoModel.getQuadro02().getQ02C02() >= 2013) && ((double)linha.getRetencoes().longValue() <= 0.26 * (double)rendaRecebida || rostoModel.getQuadro02().getQ02C02() != 2013))) {
            result.add(new DeclValidationMessage("F036", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForRetencao(i)}));
        }
    }

    protected void validateF042(ValidationResult result, String titular, String linkForTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("F042", linkForTitular, new String[]{linkForTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateF043(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("F043", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    protected void validateF048(ValidationResult result, RostoModel rostoModel, AnexoFq06T1_Linha anexoFq06T1_Linha, String linkNIF) {
        Long nif = anexoFq06T1_Linha.getNifSenhorio();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && (rostoModel.getQuadro03().isSujeitoPassivoA(nif) || rostoModel.getQuadro03().isSujeitoPassivoB(nif) || rostoModel.getQuadro03().existsInDependentes(nif) || rostoModel.getQuadro07().isConjugeFalecido(nif) || rostoModel.getQuadro07().existsInAscendentes(nif) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nif) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nif))) {
            result.add(new DeclValidationMessage("F048", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro07.fq07C01"}));
        }
    }

    protected void validateF071(ValidationResult result, RostoModel rostoModel, AnexoFq06T1_Linha anexoFq06T1_Linha, String linkNIF) {
        Long nif = anexoFq06T1_Linha.getNifSublocatario();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && (rostoModel.getQuadro03().isSujeitoPassivoA(nif) || rostoModel.getQuadro03().isSujeitoPassivoB(nif) || rostoModel.getQuadro03().existsInDependentes(nif) || rostoModel.getQuadro07().isConjugeFalecido(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nif))) {
            result.add(new DeclValidationMessage("F071", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateF095(ValidationResult result, AnexoFq06T1_Linha anexoFq06T1_Linha, int pos) {
        if (anexoFq06T1_Linha.getRendaPaga() != null && anexoFq06T1_Linha.getNifSenhorio() == null || anexoFq06T1_Linha.getRendaPaga() == null && anexoFq06T1_Linha.getNifSenhorio() != null) {
            result.add(new DeclValidationMessage("F095", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForRendaPagaSenhorio(pos), Quadro06Validator.getLinkForNifSenhorio(pos)}));
        }
    }

    private boolean repeatedLine(ArrayList<AnexoFq06T1_Linha> processedLines, AnexoFq06T1_Linha linha) {
        for (AnexoFq06T1_Linha processedLine : processedLines) {
            if (processedLine.getTitular() == null || processedLine.getNifSublocatario() == null || processedLine.getNifSenhorio() == null || !processedLine.getTitular().equals(linha.getTitular()) || !processedLine.getNifSublocatario().equals(linha.getNifSublocatario()) || !processedLine.getNifSenhorio().equals(linha.getNifSenhorio())) continue;
            return true;
        }
        processedLines.add(linha);
        return false;
    }

    public static String getLinkForLinha(int linhaPos) {
        return "aAnexoF.qQuadro06.tanexoFq06T1.l" + (linhaPos + 1);
    }

    public static String getLinkForTitular(int linhaPos) {
        return Quadro06Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq06T1_LinhaBase.Property.TITULAR.getIndex();
    }

    public static String getLinkForRendaRecebida(int linhaPos) {
        return Quadro06Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq06T1_LinhaBase.Property.RENDARECEBIDA.getIndex();
    }

    public static String getLinkForRendaPagaSenhorio(int linhaPos) {
        return Quadro06Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq06T1_LinhaBase.Property.RENDAPAGA.getIndex();
    }

    public static String getLinkForRetencao(int linhaPos) {
        return Quadro06Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq06T1_LinhaBase.Property.RETENCOES.getIndex();
    }

    public static String getLinkForNifSublocatorio(int linhaPos) {
        return Quadro06Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq06T1_LinhaBase.Property.NIFSUBLOCATARIO.getIndex();
    }

    public static String getLinkForNifSenhorio(int linhaPos) {
        return Quadro06Validator.getLinkForLinha(linhaPos) + ".c" + AnexoFq06T1_LinhaBase.Property.NIFSENHORIO.getIndex();
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoFModel anexoFModel = (AnexoFModel)model.getAnexo(AnexoFModel.class);
        if (anexoFModel == null) {
            return result;
        }
        Quadro06 quadro06Model = anexoFModel.getQuadro06();
        EventList<AnexoFq06T1_Linha> linhas = quadro06Model.getAnexoFq06T1();
        int i = 0;
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (AnexoFq06T1_Linha linha : linhas) {
            this.validateF040(result, rostoModel, linha, i);
            this.validateF041(result, linha, i);
            ++i;
        }
        return result;
    }

    protected void validateF040(ValidationResult result, RostoModel rostoModel, AnexoFq06T1_Linha linha, int numLinha) {
        if (!StringUtil.isEmpty(linha.getTitular()) && (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendaRecebida()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNifSublocatario()) && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() > 2009 && Modelo3IRSValidatorUtil.isGreaterThanZero(linha.getRetencoes()))) {
            result.add(new DeclValidationMessage("F040", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForTitular(numLinha), Quadro06Validator.getLinkForRendaRecebida(numLinha), Quadro06Validator.getLinkForNifSublocatorio(numLinha)}));
        }
    }

    protected void validateF041(ValidationResult result, AnexoFq06T1_Linha linha, int numLinha) {
        if (!(!StringUtil.isEmpty(linha.getTitular()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendaRecebida()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendaPaga()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNifSublocatario()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNifSenhorio()))) {
            result.add(new DeclValidationMessage("F041", "aAnexoF.qQuadro06", new String[]{Quadro06Validator.getLinkForTitular(numLinha)}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

