/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.Quadro07Validator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro07NetValidator
extends Quadro07Validator {
    public Quadro07NetValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        return null;
    }
}

