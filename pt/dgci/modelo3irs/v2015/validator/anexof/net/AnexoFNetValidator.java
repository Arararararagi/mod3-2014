/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.AnexoFValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.net.Quadro03NetValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.net.Quadro04NetValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.net.Quadro05NetValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.net.Quadro06NetValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.net.Quadro07NetValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoFNetValidator
extends AnexoFValidator {
    public AnexoFNetValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NetValidator().validate(model));
        result.addAllFrom(new Quadro04NetValidator().validate(model));
        result.addAllFrom(new Quadro05NetValidator().validate(model));
        result.addAllFrom(new Quadro06NetValidator().validate(model));
        result.addAllFrom(new Quadro07NetValidator().validate(model));
        return result;
    }
}

