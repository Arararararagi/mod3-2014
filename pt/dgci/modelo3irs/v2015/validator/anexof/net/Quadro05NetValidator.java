/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexof.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.Quadro05Validator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro05NetValidator
extends Quadro05Validator {
    public Quadro05NetValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        return null;
    }
}

