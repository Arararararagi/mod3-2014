/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateStep1Q03NifA(ValidationResult result, Long rostoNifA, Long anexoINifA, String anexoINifALink, String rostoNifBLink) {
        if (!Modelo3IRSValidatorUtil.equals(anexoINifA, rostoNifA)) {
            result.add(new DeclValidationMessage("I003", anexoINifALink, new String[]{anexoINifALink, rostoNifBLink}));
        }
    }

    protected void validateStep1Q03NifB(ValidationResult result, Long rostoNifB, Long sujPassivoNifB, String sujPassivoNifBLink, String rostoNifBLink) {
        if (!Modelo3IRSValidatorUtil.equals(sujPassivoNifB, rostoNifB)) {
            result.add(new DeclValidationMessage("I004", sujPassivoNifBLink, new String[]{sujPassivoNifBLink, rostoNifBLink}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQ03Model = rostoModel.getQuadro03();
        Long rostoNifA = rostoQ03Model.getQ03C03();
        Long rostoNifB = rostoQ03Model.getQ03C04();
        String rostoNifALink = "aRosto.qQuadro03.fq03C03";
        String rostoNifBLink = "aRosto.qQuadro03.fq03C04";
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key : keys) {
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key);
            Quadro03 anexoIQ03 = anexoIModel.getQuadro03();
            Long anexoINifA = anexoIQ03.getAnexoIq03C02();
            Long anexoINifB = anexoIQ03.getAnexoIq03C03();
            String anexoINifALink = anexoIModel.getLink("aAnexoI.qQuadro03.fanexoIq03C02");
            String anexoINifBLink = anexoIModel.getLink("aAnexoI.qQuadro03.fanexoIq03C03");
            this.validateStep1Q03NifA(result, rostoNifA, anexoINifA, anexoINifALink, rostoNifALink);
            this.validateStep1Q03NifB(result, rostoNifB, anexoINifB, anexoINifBLink, rostoNifBLink);
        }
        return result;
    }
}

