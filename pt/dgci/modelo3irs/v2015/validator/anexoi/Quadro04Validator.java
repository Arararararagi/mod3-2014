/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private void validateStep1AnexoIQ04(ValidationResult result, RostoModel rostoModel, AnexoBModel anexoB, AnexoCModel anexoC, AnexoIModel anexoIModel, Quadro04 q04Model, String stringNifTitular) {
        pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQ03Model = rostoModel.getQuadro03();
        Long nifAutorHeranca = q04Model.getAnexoIq04C04();
        Long nipcHerancaIndivisa = q04Model.getAnexoIq04C05();
        Long nifCabecaDeCasal = q04Model.getAnexoIq04C06();
        String nifAutorHerancaLink = anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C04");
        String nipcHerancaIndivisaLink = anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C05");
        String nifCabecaDeCasalLink = anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C06");
        this.validateStep1NifAutorHeranca(result, nifAutorHeranca, nifAutorHerancaLink, rostoModel);
        this.validateStep1NifHerancaIndivisa(result, nipcHerancaIndivisa, nipcHerancaIndivisaLink);
        if (stringNifTitular != null) {
            this.validateStep1NifCabecaDeCasal(result, rostoQ03Model, anexoB, anexoC, nifCabecaDeCasal, nifAutorHeranca, nipcHerancaIndivisa, nifCabecaDeCasalLink, nifAutorHerancaLink, nipcHerancaIndivisaLink);
            this.validateStep1AutorHerancaENipcHerancaIndivisa(result, anexoIModel, anexoB, anexoC, nifAutorHeranca, nipcHerancaIndivisa, nifAutorHerancaLink, nipcHerancaIndivisaLink);
        }
    }

    private void validateStep1NifAutorHeranca(ValidationResult result, Long nifAutorHeranca, String nifAutorHerancaLink, RostoModel rostoModel) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nifAutorHeranca)) {
            if (!Modelo3IRSValidatorUtil.startsWith(nifAutorHeranca, "1", "2")) {
                result.add(new DeclValidationMessage("I005", nifAutorHerancaLink, new String[]{nifAutorHerancaLink}));
            }
            if (!Modelo3IRSValidatorUtil.isNIFValid(nifAutorHeranca)) {
                result.add(new DeclValidationMessage("I006", nifAutorHerancaLink, new String[]{nifAutorHerancaLink}));
            }
            this.validateI086(result, nifAutorHeranca, rostoModel, nifAutorHerancaLink);
        }
    }

    protected void validateI086(ValidationResult result, Long nifAutorHeranca, RostoModel rostoModel, String nifAutorHerancaLink) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nifAutorHeranca) && (rostoModel.getQuadro03().isSujeitoPassivoA(nifAutorHeranca) || rostoModel.getQuadro03().isSujeitoPassivoB(nifAutorHeranca) || rostoModel.getQuadro07().existsInAscendentes(nifAutorHeranca)) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nifAutorHeranca) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nifAutorHeranca) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nifAutorHeranca) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nifAutorHeranca)) {
            result.add(new DeclValidationMessage("I086", nifAutorHerancaLink, new String[]{nifAutorHerancaLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro03.trostoq03DT1"}));
        }
    }

    private void validateStep1NifHerancaIndivisa(ValidationResult result, Long nipcHerancaIndivisa, String nipcHerancaIndivisaLink) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nipcHerancaIndivisa)) {
            if (!Modelo3IRSValidatorUtil.startsWith(nipcHerancaIndivisa, "7", "9")) {
                result.add(new DeclValidationMessage("I007", nipcHerancaIndivisaLink, new String[]{nipcHerancaIndivisaLink}));
            }
            if (!Modelo3IRSValidatorUtil.isNIFValid(nipcHerancaIndivisa)) {
                result.add(new DeclValidationMessage("I008", nipcHerancaIndivisaLink, new String[]{nipcHerancaIndivisaLink}));
            }
        }
        if (nipcHerancaIndivisa == null) {
            result.add(new DeclValidationMessage("I097", nipcHerancaIndivisaLink, new String[]{nipcHerancaIndivisaLink}));
        }
    }

    private void validateStep1NifCabecaDeCasal(ValidationResult result, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQ03Model, AnexoBModel anexoB, AnexoCModel anexoC, Long nifCabecaDeCasal, Long autorHeranca, Long herancaIndivisa, String nifCabecaDeCasalLink, String autorHerancaLink, String herancaIndivisaLink) {
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(nifCabecaDeCasal)) {
            result.add(new DeclValidationMessage("I011", nifCabecaDeCasalLink, new String[]{nifCabecaDeCasalLink}));
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nifCabecaDeCasal)) {
            if (!Modelo3IRSValidatorUtil.startsWith(nifCabecaDeCasal, "1", "2", "5", "6", "7", "9")) {
                result.add(new DeclValidationMessage("I009", nifCabecaDeCasalLink, new String[]{nifCabecaDeCasalLink}));
            }
            if (!Modelo3IRSValidatorUtil.isNIFValid(nifCabecaDeCasal)) {
                result.add(new DeclValidationMessage("I010", nifCabecaDeCasalLink, new String[]{nifCabecaDeCasalLink}));
            }
            if (!(rostoQ03Model.isSujeitoPassivoA(nifCabecaDeCasal) || rostoQ03Model.isSujeitoPassivoB(nifCabecaDeCasal) || anexoB != null && anexoB.getQuadro03().isAnexoBq03B1OPSim() || anexoC != null && (anexoC.getQuadro03().getAnexoCq03B1() == null || !anexoC.getQuadro03().getAnexoCq03B1().equals("2")))) {
                result.add(new DeclValidationMessage("I012", nifCabecaDeCasalLink, new String[]{nifCabecaDeCasalLink}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nifCabecaDeCasal) && (Modelo3IRSValidatorUtil.equals(nifCabecaDeCasal, autorHeranca) || Modelo3IRSValidatorUtil.equals(nifCabecaDeCasal, herancaIndivisa))) {
                result.add(new DeclValidationMessage("I083", nifCabecaDeCasalLink, new String[]{autorHerancaLink, herancaIndivisaLink, nifCabecaDeCasalLink}));
            }
        }
    }

    private void validateStep1AutorHerancaENipcHerancaIndivisa(ValidationResult result, AnexoIModel anexoIModel, AnexoBModel anexoB, AnexoCModel anexoC, Long nifAutorHeranca, Long nipcHerancaIndivisa, String nifAutorHerancaLink, String nipcHerancaIndivisaLink) {
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(nifAutorHeranca) && Modelo3IRSValidatorUtil.isEmptyOrZero(nipcHerancaIndivisa)) {
            result.add(new DeclValidationMessage("I014", nipcHerancaIndivisaLink, new String[]{nifAutorHerancaLink, nipcHerancaIndivisaLink}));
            return;
        }
        if (anexoB == null && anexoC == null) {
            result.add(new DeclValidationMessage("I015", nipcHerancaIndivisaLink, new String[]{nipcHerancaIndivisaLink}));
        }
        if (anexoB != null) {
            String anexoBQ03F8 = anexoB.getQuadro03().getAnexoBq03B1();
            String anexoBQ03F8Link = anexoB.getLink("aAnexoB.qQuadro03.fanexoBq03C08");
            if (anexoBQ03F8 == null || anexoBQ03F8.equals("2")) {
                result.add(new DeclValidationMessage("I016", nipcHerancaIndivisaLink, new String[]{nipcHerancaIndivisaLink, anexoBQ03F8Link}));
            }
        }
    }

    protected void validateI017(ValidationResult result, AnexoIModel anexoIModel, AnexoCModel anexoC, String nipcHerancaIndivisaLink) {
        if (anexoC != null) {
            String anexoCQ03F6 = anexoC.getQuadro03().getAnexoCq03B1();
            String anexoCQ03F6Link = anexoC.getLink(anexoIModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"));
            if (anexoCQ03F6 == null || anexoCQ03F6.equals("2")) {
                result.add(new DeclValidationMessage("I017", nipcHerancaIndivisaLink, new String[]{nipcHerancaIndivisaLink, anexoCQ03F6Link}));
            }
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        return new ValidationResult();
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key2 : keys) {
            AnexoBModel anexoB = null;
            AnexoCModel anexoC = null;
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key2);
            Quadro04 q04Model = anexoIModel.getQuadro04();
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            String stringNifTitular = anexoIModel.getStringNifTitular();
            if (stringNifTitular != null) {
                anexoB = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(stringNifTitular);
                anexoC = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(stringNifTitular);
            }
            this.validateStep1AnexoIQ04(result, rostoModel, anexoB, anexoC, anexoIModel, q04Model, stringNifTitular);
            this.validateI110(result, anexoIModel, rostoModel);
        }
        for (FormKey key2 : keys) {
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key2);
            Long titular = anexoIModel.getNifTitular();
            AnexoCModel anexoC = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(titular);
            String nipcHerancaIndivisaLink = anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C05");
            this.validateI017(result, anexoIModel, anexoC, nipcHerancaIndivisaLink);
        }
        return result;
    }

    public void validateI110(ValidationResult result, AnexoIModel anexoI, RostoModel rosto) {
        if (anexoI != null) {
            long falecido;
            long q4C04 = anexoI.getQuadro04().getAnexoIq04C04() != null ? anexoI.getQuadro04().getAnexoIq04C04() : 0;
            long l = falecido = rosto.getQuadro07().getQ07C1() != null ? rosto.getQuadro07().getQ07C1() : 0;
            if (!(q4C04 <= 0 || falecido <= 0 || q4C04 != falecido || anexoI.getQuadro07().existNifInQ7C70i1(q4C04))) {
                result.add(new DeclValidationMessage("I110", anexoI.getLink("aAnexoI.qQuadro04.fanexoIq04C04"), new String[]{anexoI.getLink("aAnexoI.qQuadro07")}));
            }
        }
    }
}

