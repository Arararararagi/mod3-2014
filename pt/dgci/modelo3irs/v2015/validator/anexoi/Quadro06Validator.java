/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    protected static final String ANEXO_I = "AnexoI";

    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private void validateStep1AnexoIQ06(ValidationResult result, DeclaracaoModel model, AnexoCModel anexoC, AnexoIModel anexoI, Quadro06 anexoIQ06, String stringNifTitular) {
        Long prejuizo = anexoIQ06.getAnexoIq06C601();
        Long lucro = anexoIQ06.getAnexoIq06C602();
        long somaRendComerciais = 0;
        long somaRendAgricolas = 0;
        for (AnexoIq07T1_Linha linha : anexoI.getQuadro07().getAnexoIq07T1()) {
            if (linha.getRendimentosComerciais() != null) {
                somaRendComerciais+=linha.getRendimentosComerciais().longValue();
            }
            if (linha.getRendimentosAgricolas() == null) continue;
            somaRendAgricolas+=linha.getRendimentosAgricolas().longValue();
        }
        String prejuizoLink = anexoI.getLink("aAnexoI.qQuadro06.fanexoIq06C601");
        String lucroLink = anexoI.getLink("aAnexoI.qQuadro06.fanexoIq06C602");
        String q07ComerciaisLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C2");
        String q07AgricolasLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C3");
        if (stringNifTitular != null) {
            this.validateStep1AnexoIQ06Prejuizo(result, anexoC, prejuizo, somaRendComerciais, somaRendAgricolas, prejuizoLink, q07ComerciaisLink, q07AgricolasLink);
            this.validateStep1AnexoIQ06Lucro(result, anexoC, prejuizo, lucro, somaRendComerciais, somaRendAgricolas, prejuizoLink, lucroLink, q07ComerciaisLink, q07AgricolasLink);
        }
    }

    private void validateStep1AnexoIQ06Prejuizo(ValidationResult result, AnexoCModel anexoC, Long prejuizo, long somaRendComerciais, long somaRendAgricolas, String prejuizoLink, String q07ComerciaisLink, String q07AgricolasLink) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(prejuizo) || anexoC != null)) {
            result.add(new DeclValidationMessage("I032", prejuizoLink, new String[]{prejuizoLink}));
        }
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(prejuizo) || anexoC == null)) {
            Long anexoCPrejuizoApurado = anexoC.getQuadro04().getAnexoCq04C459();
            String anexoCPrejuizoApuradoLink = anexoC.getLink("aAnexoC.qQuadro04.fanexoCq04C459");
            if (anexoCPrejuizoApurado != null && prejuizo.longValue() != anexoCPrejuizoApurado.longValue()) {
                result.add(new DeclValidationMessage("I033", prejuizoLink, new String[]{prejuizoLink, anexoCPrejuizoApuradoLink}));
            }
            if (prejuizo * -1 < somaRendComerciais + somaRendAgricolas - 100 || prejuizo * -1 > somaRendComerciais + somaRendAgricolas + 100) {
                result.add(new DeclValidationMessage("I034", prejuizoLink, new String[]{prejuizoLink, q07ComerciaisLink, q07AgricolasLink}));
            }
        }
    }

    private void validateStep1AnexoIQ06Lucro(ValidationResult result, AnexoCModel anexoC, Long prejuizo, Long lucro, long somaRendComerciais, long somaRendAgricolas, String prejuizoLink, String lucroLink, String q07ComerciaisLink, String q07AgricolasLink) {
        if (lucro != null) {
            if (anexoC == null) {
                result.add(new DeclValidationMessage("I035", lucroLink, new String[]{lucroLink}));
            }
            if (anexoC != null) {
                Long lucroApurado = anexoC.getQuadro04().getAnexoCq04C460();
                String lucroApuradoLink = anexoC.getLink("aAnexoC.qQuadro04.fanexoCq04C460");
                if (lucroApurado != null && lucro.longValue() != lucroApurado.longValue()) {
                    result.add(new DeclValidationMessage("I036", lucroLink, new String[]{lucroLink, lucroApuradoLink}));
                }
                if (lucro > 0 && lucro < somaRendComerciais + somaRendAgricolas - 100 || lucro > somaRendComerciais + somaRendAgricolas + 100) {
                    result.add(new DeclValidationMessage("I037", lucroLink, new String[]{lucroLink, q07ComerciaisLink, q07AgricolasLink}));
                }
                if (prejuizo != null && lucro != null) {
                    result.add(new DeclValidationMessage("I038", lucroLink, new String[]{lucroLink, prejuizoLink}));
                }
            }
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key : keys) {
            AnexoCModel anexoC = null;
            AnexoIModel anexoI = (AnexoIModel)model.getAnexo(key);
            String stringNifTitular = anexoI.getStringNifTitular();
            Quadro06 anexoIQ06 = anexoI.getQuadro06();
            if (stringNifTitular != null) {
                anexoC = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(stringNifTitular);
            }
            this.validateStep1AnexoIQ06(result, model, anexoC, anexoI, anexoIQ06, stringNifTitular);
        }
        return result;
    }
}

