/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.EuroField;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private void validateStep1AnexoIQ05(ValidationResult result, RostoModel rosto, AnexoBModel anexoB, AnexoIModel anexoI, Quadro05 anexoIQ05, String stringNifTitular) {
        Quadro06 anexoIQ06 = anexoI.getQuadro06();
        Quadro07 anexoIQ07 = anexoI.getQuadro07();
        Long rostof202_anoExercicio = rosto.getQuadro02().getQ02C02();
        Long f501_iliquido = anexoIQ05.getAnexoIq05C501();
        Long f501_liquido = anexoIQ05.getAnexoIq05C501a();
        Long f502_iliquido = anexoIQ05.getAnexoIq05C502();
        Long f502_liquido = anexoIQ05.getAnexoIq05C502a();
        Long f503_herdeiros = anexoIQ05.getAnexoIq05C503();
        Long f05_somaIliquido = anexoIQ05.getAnexoIq05C1();
        Long f05_somaLiquido = anexoIQ05.getAnexoIq05C1a();
        Long q06Prejuizo = anexoIQ06.getAnexoIq06C601();
        Long q06Lucro = anexoIQ06.getAnexoIq06C602();
        EventList<AnexoIq07T1_Linha> anexoIQ07Linhas = anexoIQ07.getAnexoIq07T1();
        long anoExercicio = !Modelo3IRSValidatorUtil.isEmptyOrZero(rostof202_anoExercicio) ? rostof202_anoExercicio : 0;
        long iliquido01 = !Modelo3IRSValidatorUtil.isEmptyOrZero(f501_iliquido) ? f501_iliquido : 0;
        long liquido01 = !Modelo3IRSValidatorUtil.isEmptyOrZero(f501_liquido) ? f501_liquido : 0;
        long iliquido02 = !Modelo3IRSValidatorUtil.isEmptyOrZero(f502_iliquido) ? f502_iliquido : 0;
        long liquido02 = !Modelo3IRSValidatorUtil.isEmptyOrZero(f502_liquido) ? f502_liquido : 0;
        long herdeiros = !Modelo3IRSValidatorUtil.isEmptyOrZero(f503_herdeiros) ? f503_herdeiros : 0;
        long somaIliquido = !Modelo3IRSValidatorUtil.isEmptyOrZero(f05_somaIliquido) ? f05_somaIliquido : 0;
        long somaLiquido = !Modelo3IRSValidatorUtil.isEmptyOrZero(f05_somaLiquido) ? f05_somaLiquido : 0;
        String anoExercicioLink = "aRosto.qQuadro02.fq02C02";
        String iliquidos01Link = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C501");
        String liquidos01Link = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C501a");
        String iliquidosLink02 = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C502");
        String liquidosLink02 = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C502a");
        String somaIliquidoLink = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C1");
        String somaLiquidoLink = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C1a");
        String herdeirosLink = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C503");
        String q06PrejuizoLink = anexoI.getLink("aAnexoI.qQuadro06.fanexoIq06C602");
        String q07ComerciaisLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C2");
        String q07AgricolasLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C3");
        String nifTitularLink = anexoI.getNifTitularLink();
        String fieldLink = !Modelo3IRSValidatorUtil.isEmptyOrZero(f501_iliquido) ? iliquidos01Link : liquidos01Link;
        if (stringNifTitular != null) {
            if (!(anexoB != null || anexoIQ05.isEmpty())) {
                result.add(new DeclValidationMessage("I018", anexoI.getLink("aAnexoI.qQuadro05"), new String[]{fieldLink, nifTitularLink}));
            }
            this.validateStep1AnexoIQ05RendimentoLiquidoVendasMercadoriasEProdutos(result, iliquido01, liquido01, iliquidos01Link, liquidos01Link);
            this.validateStep1AnexoIQ05RendimentoLiquidoOutrasPrestacoesDeServicos(result, anoExercicio, iliquido02, liquido02, iliquidosLink02, liquidosLink02, anoExercicioLink);
            this.validateStep1AnexoIQ05SomaRendimentoIliquido(result, iliquido01, iliquido02, somaIliquido, somaIliquidoLink);
            this.validateStep1AnexoIQ05SomaRendimentoLiquido(result, anexoB, anexoIQ07Linhas, anoExercicio, liquido01, liquido02, somaIliquido, somaLiquido, q06Prejuizo, q06Lucro, somaIliquidoLink, somaLiquidoLink, q06PrejuizoLink, q07ComerciaisLink, q07AgricolasLink);
            this.validateStep1AnexoIQ05ValorImputadoAosHerdeiros(result, somaLiquido, herdeiros, anoExercicio, somaLiquidoLink, herdeirosLink);
        }
    }

    private void validateStep1AnexoIQ05RendimentoLiquidoVendasMercadoriasEProdutos(ValidationResult result, long iliquido, long liquido, String rendIliquidosLink, String rendLiquidosLink) {
        if (liquido < (long)((int)(0.195 * (double)iliquido)) || (double)liquido > 0.205 * (double)iliquido) {
            result.add(new DeclValidationMessage("I023", rendLiquidosLink, new String[]{rendLiquidosLink, rendIliquidosLink}));
        }
    }

    private void validateStep1AnexoIQ05RendimentoLiquidoOutrasPrestacoesDeServicos(ValidationResult result, long anoExercicio, long iliquido, long liquido, String rendIliquidosLink, String rendLiquidosLink, String anoExercicioLink) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio)) {
            if (anoExercicio < 2007 && (liquido < (long)((int)(0.645 * (double)iliquido)) || (double)liquido > 0.655 * (double)iliquido)) {
                result.add(new DeclValidationMessage("I024", rendLiquidosLink, new String[]{rendLiquidosLink, rendIliquidosLink, anoExercicioLink}));
            }
            this.validateI025(result, anoExercicio, iliquido, liquido, rendIliquidosLink, rendLiquidosLink, anoExercicioLink);
            this.validateI117(result, anoExercicio, iliquido, liquido, rendIliquidosLink, rendLiquidosLink);
        }
    }

    protected void validateI025(ValidationResult result, long anoExercicio, double iliquido, double liquido, String rendIliquidosLink, String rendLiquidosLink, String anoExercicioLink) {
        if (anoExercicio >= 2007 && anoExercicio < 2013 && (liquido < (double)((int)(0.695 * iliquido)) || liquido > 0.705 * iliquido)) {
            result.add(new DeclValidationMessage("I025", rendLiquidosLink, new String[]{rendLiquidosLink, rendIliquidosLink, anoExercicioLink}));
        }
    }

    protected void validateI117(ValidationResult result, long anoExercicio, double iliquido, double liquido, String rendIliquidosLink, String rendLiquidosLink) {
        if (anoExercicio > 2012 && (liquido < (double)((int)(0.745 * iliquido)) || liquido > 0.755 * iliquido)) {
            result.add(new DeclValidationMessage("I117", rendLiquidosLink, new String[]{rendLiquidosLink, rendIliquidosLink}));
        }
    }

    private void validateStep1AnexoIQ05SomaRendimentoIliquido(ValidationResult result, long iliquido01, long iliquido02, long somaIliquido, String somaIliquidoLink) {
        if (somaIliquido != iliquido01 + iliquido02) {
            result.add(new DeclValidationMessage("I026", somaIliquidoLink, new String[]{somaIliquidoLink}));
        }
    }

    private long getValorMinimoRegimeSimplificadoRounded(long exercicio) {
        long retValue;
        switch ((short)exercicio) {
            case 2002: {
                retValue = 244000;
                break;
            }
            case 2003: {
                retValue = 313000;
                break;
            }
            case 2004: {
                retValue = 313000;
                break;
            }
            case 2005: {
                retValue = 313000;
                break;
            }
            case 2006: {
                retValue = 270130;
                break;
            }
            case 2007: {
                retValue = 282100;
                break;
            }
            case 2008: {
                retValue = 298200;
                break;
            }
            case 2009: {
                retValue = 315000;
                break;
            }
            default: {
                retValue = 315000;
            }
        }
        return retValue;
    }

    private long getValorMinimoRegimeSimplificado(long exercicio) {
        long retValue;
        switch ((short)exercicio) {
            case 2002: {
                retValue = 244000;
                break;
            }
            case 2003: {
                retValue = 312500;
                break;
            }
            case 2004: {
                retValue = 312500;
                break;
            }
            case 2005: {
                retValue = 312500;
                break;
            }
            case 2006: {
                retValue = 270130;
                break;
            }
            case 2007: {
                retValue = 282100;
                break;
            }
            case 2008: {
                retValue = 298200;
                break;
            }
            case 2009: {
                retValue = 315000;
                break;
            }
            default: {
                retValue = 315000;
            }
        }
        return retValue;
    }

    private void validateStep1AnexoIQ05SomaRendimentoLiquido(ValidationResult result, AnexoBModel anexoB, EventList<AnexoIq07T1_Linha> anexoIQ07Linhas, Long anoExercicio, Long liquido01, Long liquido02, Long somaIliquido, Long somaLiquido, Long q06Prejuizo, Long q06Lucro, String somaIliquidoLink, String somaLiquidoLink, String q06PrejuizoLink, String q07ComerciaisLink, String q07AgricolasLink) {
        long f70i4 = 0;
        long f70i5 = 0;
        for (AnexoIq07T1_Linha linha : anexoIQ07Linhas) {
            if (linha.getRendimentosComerciais() != null) {
                f70i4+=linha.getRendimentosComerciais().longValue();
            }
            if (linha.getRendimentosAgricolas() == null) continue;
            f70i5+=linha.getRendimentosAgricolas().longValue();
        }
        if (somaLiquido != liquido01 + liquido02) {
            result.add(new DeclValidationMessage("I027", somaLiquidoLink, new String[]{somaLiquidoLink}));
        }
        if (anoExercicio != null && anoExercicio < 2010) {
            long somatorioF70i4_5 = f70i4 + f70i5;
            if (somaLiquido > this.getValorMinimoRegimeSimplificadoRounded(anoExercicio) && anexoB != null && somatorioF70i4_5 + 1 > this.getValorMinimoRegimeSimplificado(anoExercicio) && (somaLiquido < somatorioF70i4_5 - 1 || somaLiquido > somatorioF70i4_5 + 1)) {
                result.add(new DeclValidationMessage("I028", somaLiquidoLink, new String[]{somaLiquidoLink, q07ComerciaisLink, q07AgricolasLink}));
            }
        }
        long somaQ6 = (!Modelo3IRSValidatorUtil.isEmptyOrZero(q06Prejuizo) ? q06Prejuizo : 0) + (!Modelo3IRSValidatorUtil.isEmptyOrZero(q06Lucro) ? q06Lucro : 0);
        if (somaLiquido + somaIliquido != 0 && somaQ6 != 0) {
            result.add(new DeclValidationMessage("I029", somaIliquidoLink, new String[]{somaIliquidoLink, q06PrejuizoLink}));
        }
    }

    private void validateStep1AnexoIQ05ValorImputadoAosHerdeiros(ValidationResult result, long soma_rend_liquidos, long herdeiros, Long anoExercicio, String somaLiquidoLink, String herdeirosLink) {
        long valMinRegSimplificado = this.getValorMinimoRegimeSimplificado(anoExercicio);
        if (anoExercicio != null && anoExercicio < 2010 && soma_rend_liquidos >= valMinRegSimplificado && herdeiros != soma_rend_liquidos) {
            result.add(new DeclValidationMessage("I030", herdeirosLink, new String[]{herdeirosLink, somaLiquidoLink}, new String[]{new EuroField("", valMinRegSimplificado).format(), String.valueOf(anoExercicio)}));
        }
        if (anoExercicio != null && anoExercicio < 2010 && Modelo3IRSValidatorUtil.isGreaterThanZero(anoExercicio) && soma_rend_liquidos > 0 && soma_rend_liquidos < valMinRegSimplificado && herdeiros < valMinRegSimplificado) {
            result.add(new DeclValidationMessage("I031", herdeirosLink, new String[]{herdeirosLink}, new String[]{new EuroField("", valMinRegSimplificado).format(), String.valueOf(anoExercicio)}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateI118(ValidationResult result, AnexoIModel anexoIModel) {
        Long C501 = anexoIModel.getQuadro05().getAnexoIq05C501();
        Long C501a = anexoIModel.getQuadro05().getAnexoIq05C501a();
        Long C502 = anexoIModel.getQuadro05().getAnexoIq05C502();
        Long C502a = anexoIModel.getQuadro05().getAnexoIq05C502a();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if ((C501 != null && C501 > 0 || C501a != null && C501a > 0 || C502 != null && C502 > 0 || C502a != null && C502a > 0) && ano != null && ano > 2013) {
            result.add(new DeclValidationMessage("I118", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C501"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C501"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C501a"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C502"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C502a"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateI119(ValidationResult result, AnexoIModel anexoIModel) {
        Long C504 = anexoIModel.getQuadro05().getAnexoIq05C504();
        Long C504a = anexoIModel.getQuadro05().getAnexoIq05C504a();
        Long C505 = anexoIModel.getQuadro05().getAnexoIq05C505();
        Long C505a = anexoIModel.getQuadro05().getAnexoIq05C505a();
        Long C506 = anexoIModel.getQuadro05().getAnexoIq05C506();
        Long C506a = anexoIModel.getQuadro05().getAnexoIq05C506a();
        Long C507 = anexoIModel.getQuadro05().getAnexoIq05C507();
        Long C507a = anexoIModel.getQuadro05().getAnexoIq05C507a();
        Long C508 = anexoIModel.getQuadro05().getAnexoIq05C508();
        Long C508a = anexoIModel.getQuadro05().getAnexoIq05C508a();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if ((C504 != null && C504 > 0 || C504a != null && C504a > 0 || C505 != null && C505 > 0 || C505a != null && C505a > 0 || C506 != null && C506 > 0 || C506a != null && C506a > 0 || C507 != null && C507 > 0 || C507a != null && C507a > 0 || C508 != null && C508 > 0 || C508a != null && C508a > 0) && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("I119", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C501"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C504"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C505"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C506"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C507"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C508"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateI120(ValidationResult result, AnexoIModel anexoIModel, AnexoBModel anexoBModel) {
        boolean condicao2;
        if (anexoBModel == null) {
            return;
        }
        long margem = 500000;
        Long C504 = anexoIModel.getQuadro05().getAnexoIq05C504();
        Long C423 = anexoBModel.getQuadro04().getAnexoBq04C423();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        Long NIPCI = anexoIModel.getQuadro04().getAnexoIq04C05();
        Long NIPCB = anexoBModel.getQuadro03().getAnexoBq03C09();
        boolean condicao1 = C504 != null && C423 != null && C504 != this.somaCamposValidacaoI120(anexoBModel) + C423 && C423 >= margem;
        boolean bl = condicao2 = C504 != null && C423 != null && C504.longValue() != this.somaCamposValidacaoI120(anexoBModel) && C423 < margem;
        if (ano != null && NIPCB != null && NIPCI != null && (condicao1 || condicao2) && ano > 2013 && NIPCB.equals(NIPCI)) {
            result.add(new DeclValidationMessage("I120", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C504"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C504"), anexoBModel.getLink("aAnexoB.qQuadro04")}));
        }
    }

    private long somaCamposValidacaoI120(AnexoBModel anexoBModel) {
        long soma = 0;
        if (anexoBModel.getQuadro04().getAnexoBq04C401() != null) {
            soma+=anexoBModel.getQuadro04().getAnexoBq04C401().longValue();
        }
        if (anexoBModel.getQuadro04().getAnexoBq04C402() != null) {
            soma+=anexoBModel.getQuadro04().getAnexoBq04C402().longValue();
        }
        if (anexoBModel.getQuadro04().getAnexoBq04C409() != null) {
            soma+=anexoBModel.getQuadro04().getAnexoBq04C409().longValue();
        }
        return soma;
    }

    protected void validateI121(ValidationResult result, AnexoIModel anexoIModel) {
        boolean condicao3;
        Long C504a = anexoIModel.getQuadro05().getAnexoIq05C504a();
        Long C504 = anexoIModel.getQuadro05().getAnexoIq05C504();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        boolean condicao1 = C504a != null && C504a > 0 && (C504 == null || C504 != null && C504 == 0);
        boolean condicao2 = (C504a == null || C504a != null && C504a == 0) && C504 != null && C504 > 0;
        boolean bl = condicao3 = C504 != null && C504a != null && ano != null && ((double)C504a.longValue() < 0.145 * (double)C504.longValue() || (double)C504a.longValue() > 0.155 * (double)C504.longValue()) && ano > 2013;
        if (condicao1 || condicao2 || condicao3) {
            result.add(new DeclValidationMessage("I121", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C504a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C504a"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C504")}));
        }
    }

    protected void validateI122(ValidationResult result, AnexoIModel anexoIModel, AnexoBModel anexoBModel) {
        if (anexoBModel != null) {
            Long C505 = anexoIModel.getQuadro05().getAnexoIq05C505();
            Long C405 = anexoBModel.getQuadro04().getAnexoBq04C405() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C405();
            Long C440 = anexoBModel.getQuadro04().getAnexoBq04C440() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C440();
            Long C444 = anexoBModel.getQuadro04().getAnexoBq04C444() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C444();
            long somaCamposB = C405 + C440 + C444;
            Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
            Long NIPCAnexoI = anexoIModel.getQuadro04().getAnexoIq04C05();
            Long NIPCAnexoB = anexoBModel.getQuadro03().getAnexoBq03C09();
            if (C505 != null && !C505.equals(somaCamposB) && ano != null && ano > 2013 && NIPCAnexoB.equals(NIPCAnexoI)) {
                result.add(new DeclValidationMessage("I122", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C505"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C505"), anexoIModel.getLink("aAnexoI.qQuadro05"), anexoBModel.getLink("aAnexoB.qQuadro04")}));
            }
        }
    }

    protected void validateI123(ValidationResult result, AnexoIModel anexoIModel) {
        boolean condicao3;
        Long C505a = anexoIModel.getQuadro05().getAnexoIq05C505a();
        Long C505 = anexoIModel.getQuadro05().getAnexoIq05C505();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        boolean condicao1 = C505a != null && C505a > 0 && (C505 == null || C505 != null && C505 == 0);
        boolean condicao2 = (C505a == null || C505a != null && C505a == 0) && C505 != null && C505 > 0;
        boolean bl = condicao3 = C505 != null && C505a != null && ano != null && ((double)C505a.longValue() < 0.745 * (double)C505.longValue() || (double)C505a.longValue() > 0.755 * (double)C505.longValue()) && ano > 2013;
        if (condicao1 || condicao2 || condicao3) {
            result.add(new DeclValidationMessage("I123", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C505a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C505a"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C505")}));
        }
    }

    protected void validateI124(ValidationResult result, AnexoIModel anexoIModel, AnexoBModel anexoBModel) {
        if (anexoBModel != null) {
            Long C506 = anexoIModel.getQuadro05().getAnexoIq05C506();
            Long C404 = anexoBModel.getQuadro04().getAnexoBq04C404() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C404();
            Long C421 = anexoBModel.getQuadro04().getAnexoBq04C421() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C421();
            Long C422 = anexoBModel.getQuadro04().getAnexoBq04C422() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C422();
            Long C441 = anexoBModel.getQuadro04().getAnexoBq04C441() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C441();
            Long C442 = anexoBModel.getQuadro04().getAnexoBq04C442() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C442();
            Long C445 = anexoBModel.getQuadro04().getAnexoBq04C445() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C445();
            long somaCamposB = C404 + C421 + C422 + C441 + C442 + C445;
            Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
            Long NIPCAnexoI = anexoIModel.getQuadro04().getAnexoIq04C05();
            Long NIPCAnexoB = anexoBModel.getQuadro03().getAnexoBq03C09();
            if (C506 != null && !C506.equals(somaCamposB) && ano != null && ano > 2013 && NIPCAnexoB.equals(NIPCAnexoI)) {
                result.add(new DeclValidationMessage("I124", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C506"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C506"), anexoIModel.getLink("aAnexoI.qQuadro05"), anexoBModel.getLink("aAnexoB.qQuadro04")}));
            }
        }
    }

    protected void validateI125(ValidationResult result, AnexoIModel anexoIModel) {
        boolean condicao3;
        Long C506a = anexoIModel.getQuadro05().getAnexoIq05C506a();
        Long C506 = anexoIModel.getQuadro05().getAnexoIq05C506();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        boolean condicao1 = C506a != null && C506a > 0 && (C506 == null || C506 != null && C506 == 0);
        boolean condicao2 = (C506a == null || C506a != null && C506a == 0) && C506 != null && C506 > 0;
        boolean bl = condicao3 = C506 != null && C506a != null && ano != null && ((double)C506a.longValue() < 0.945 * (double)C506.longValue() || (double)C506a.longValue() > 0.955 * (double)C506.longValue()) && ano > 2013;
        if (condicao1 || condicao2 || condicao3) {
            result.add(new DeclValidationMessage("I125", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C506a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C506a"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C506")}));
        }
    }

    protected void validateI126(ValidationResult result, AnexoIModel anexoIModel, AnexoBModel anexoBModel) {
        if (anexoBModel != null) {
            Long C507 = anexoIModel.getQuadro05().getAnexoIq05C507();
            Long C425 = anexoBModel.getQuadro04().getAnexoBq04C425() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C425();
            Long C426 = anexoBModel.getQuadro04().getAnexoBq04C426() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C426();
            long somaCamposB = C425 + C426;
            Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
            Long NIPCAnexoI = anexoIModel.getQuadro04().getAnexoIq04C05();
            Long NIPCAnexoB = anexoBModel.getQuadro03().getAnexoBq03C09();
            if (C507 != null && !C507.equals(somaCamposB) && ano != null && ano > 2013 && NIPCAnexoB.equals(NIPCAnexoI)) {
                result.add(new DeclValidationMessage("I126", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C507"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C507"), anexoIModel.getLink("aAnexoI.qQuadro05"), anexoBModel.getLink("aAnexoB.qQuadro04")}));
            }
        }
    }

    protected void validateI127(ValidationResult result, AnexoIModel anexoIModel) {
        boolean condicao3;
        Long C507a = anexoIModel.getQuadro05().getAnexoIq05C507a();
        Long C507 = anexoIModel.getQuadro05().getAnexoIq05C507();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        boolean condicao1 = C507a != null && C507a > 0 && (C507 == null || C507 != null && C507 == 0);
        boolean condicao2 = (C507a == null || C507a != null && C507a == 0) && C507 != null && C507 > 0;
        boolean bl = condicao3 = C507 != null && C507a != null && ano != null && ((double)C507a.longValue() < 0.295 * (double)C507.longValue() || (double)C507a.longValue() > 0.305 * (double)C507.longValue()) && ano > 2013;
        if (condicao1 || condicao2 || condicao3) {
            result.add(new DeclValidationMessage("I127", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C507a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C507a"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C507")}));
        }
    }

    protected void validateI128(ValidationResult result, AnexoIModel anexoIModel, AnexoBModel anexoBModel) {
        if (anexoBModel != null) {
            Long C508 = anexoIModel.getQuadro05().getAnexoIq05C508();
            Long somaCamposB = this.calculateSomaCamposB(anexoBModel);
            Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
            Long NIPCAnexoI = anexoIModel.getQuadro04().getAnexoIq04C05();
            Long NIPCAnexoB = anexoBModel.getQuadro03().getAnexoBq03C09();
            if (C508 != null && !C508.equals(somaCamposB) && ano != null && ano > 2013 && NIPCAnexoB.equals(NIPCAnexoI)) {
                result.add(new DeclValidationMessage("I128", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C508"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C508"), anexoIModel.getLink("aAnexoI.qQuadro05"), anexoBModel.getLink("aAnexoB.qQuadro04")}));
            }
        }
    }

    protected void validateI129(ValidationResult result, AnexoIModel anexoIModel) {
        Long C508a = anexoIModel.getQuadro05().getAnexoIq05C508a();
        Long C508 = anexoIModel.getQuadro05().getAnexoIq05C508();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if (C508 != null && C508a != null && ano != null && ((double)C508a.longValue() < 0.095 * (double)C508.longValue() || (double)C508a.longValue() > 0.105 * (double)C508.longValue()) && ano > 2013) {
            result.add(new DeclValidationMessage("I129", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C508a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C508a"), anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C508")}));
        }
    }

    private long calculateSomaCamposB(AnexoBModel anexoBModel) {
        long soma = 0;
        if (anexoBModel.getQuadro04().getAnexoBq04C424() != null) {
            soma+=anexoBModel.getQuadro04().getAnexoBq04C424().longValue();
        }
        if (anexoBModel.getQuadro04().getAnexoBq04C443() != null) {
            soma+=anexoBModel.getQuadro04().getAnexoBq04C443().longValue();
        }
        if (anexoBModel.getQuadro04().getAnexoBq04C411() != null) {
            soma+=anexoBModel.getQuadro04().getAnexoBq04C411().longValue();
        }
        if (anexoBModel.getQuadro04().getAnexoBq04C446() != null) {
            soma+=anexoBModel.getQuadro04().getAnexoBq04C446().longValue();
        }
        return soma;
    }

    protected void validateI130(ValidationResult result, AnexoIModel anexoIModel) {
        long soma_4;
        long l = soma_4 = anexoIModel.getQuadro05().getAnexoIq05C2() == null ? 0 : anexoIModel.getQuadro05().getAnexoIq05C2();
        if (soma_4 != this.calculateSomaC1(anexoIModel)) {
            result.add(new DeclValidationMessage("I130", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C2"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C2")}));
        }
    }

    protected void validateI131(ValidationResult result, AnexoIModel anexoIModel) {
        long soma_4;
        long l = soma_4 = anexoIModel.getQuadro05().getAnexoIq05C2a() == null ? 0 : anexoIModel.getQuadro05().getAnexoIq05C2a();
        if (soma_4 != this.calculateSomaC2(anexoIModel)) {
            result.add(new DeclValidationMessage("I131", anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C2a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C2a")}));
        }
    }

    private long calculateSomaC1(AnexoIModel anexoIModel) {
        long soma = 0;
        if (anexoIModel.getQuadro05().getAnexoIq05C504() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C504().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C505() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C505().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C506() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C506().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C507() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C507().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C508() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C508().longValue();
        }
        return soma;
    }

    private long calculateSomaC2(AnexoIModel anexoIModel) {
        long soma = 0;
        if (anexoIModel.getQuadro05().getAnexoIq05C504a() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C504a().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C505a() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C505a().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C506a() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C506a().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C507a() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C507a().longValue();
        }
        if (anexoIModel.getQuadro05().getAnexoIq05C508a() != null) {
            soma+=anexoIModel.getQuadro05().getAnexoIq05C508a().longValue();
        }
        return soma;
    }

    protected void validateI019(ValidationResult result, AnexoIModel anexoIModel, Quadro04 anexoBQ04, String anexoIQ05C501Link, String anexoBQ04C1Link) {
        boolean condicao2;
        if (anexoIModel == null) {
            return;
        }
        long soma = 0;
        long f501_iliquido = 0;
        if (anexoIModel.getQuadro05().getAnexoIq05C501() != null) {
            f501_iliquido = anexoIModel.getQuadro05().getAnexoIq05C501();
        }
        Long anexoBQ04C401 = anexoBQ04.getAnexoBq04C401();
        Long anexoBQ04C402 = anexoBQ04.getAnexoBq04C402();
        Long anexoBQ04C409 = anexoBQ04.getAnexoBq04C409();
        Long anexoBQ04C411 = anexoBQ04.getAnexoBq04C411();
        Long anexoBQ04C423 = anexoBQ04.getAnexoBq04C423();
        Long anexoBQ04C424 = anexoBQ04.getAnexoBq04C424();
        if (anexoBQ04C401 != null) {
            soma+=anexoBQ04C401.longValue();
        }
        if (anexoBQ04C402 != null) {
            soma+=anexoBQ04C402.longValue();
        }
        if (anexoBQ04C409 != null) {
            soma+=anexoBQ04C409.longValue();
        }
        if (anexoBQ04C411 != null) {
            soma+=anexoBQ04C411.longValue();
        }
        if (anexoBQ04C424 != null) {
            soma+=anexoBQ04C424.longValue();
        }
        long maximo423 = 500000;
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        boolean condicao1 = anexoBQ04C423 != null && anexoBQ04C423 >= 500000 && f501_iliquido != soma + anexoBQ04C423;
        boolean bl = condicao2 = (anexoIModel.getQuadro05().getAnexoIq05C501() == null || f501_iliquido >= 0 && anexoBQ04C423 != null && anexoBQ04C423 <= 500000) && f501_iliquido != soma;
        if (ano != null && (condicao1 || condicao2) && ano < 2014) {
            result.add(new DeclValidationMessage("I019", anexoIQ05C501Link, new String[]{anexoIQ05C501Link, anexoBQ04C1Link}));
        }
    }

    protected void validateI021(ValidationResult result, AnexoIModel anexoIModel, Quadro04 anexoBQ04, String anexoIQ05C502Link, String anexoBQ04C3Link) {
        if (anexoIModel == null) {
            return;
        }
        long soma = 0;
        Long anexoBQ04C403 = anexoBQ04.getAnexoBq04C403();
        Long anexoBQ04C404 = anexoBQ04.getAnexoBq04C404();
        Long anexoBQ04C405 = anexoBQ04.getAnexoBq04C405();
        Long anexoBQ04C410 = anexoBQ04.getAnexoBq04C410();
        Long anexoBQ04C421 = anexoBQ04.getAnexoBq04C421();
        Long anexoBQ04C422 = anexoBQ04.getAnexoBq04C422();
        if (anexoBQ04C403 != null) {
            soma+=anexoBQ04C403.longValue();
        }
        if (anexoBQ04C404 != null) {
            soma+=anexoBQ04C404.longValue();
        }
        if (anexoBQ04C405 != null) {
            soma+=anexoBQ04C405.longValue();
        }
        if (anexoBQ04C410 != null) {
            soma+=anexoBQ04C410.longValue();
        }
        if (anexoBQ04C421 != null) {
            soma+=anexoBQ04C421.longValue();
        }
        if (anexoBQ04C422 != null) {
            soma+=anexoBQ04C422.longValue();
        }
        long f502_iliquido = 0;
        if (anexoIModel.getQuadro05().getAnexoIq05C502() != null) {
            f502_iliquido = anexoIModel.getQuadro05().getAnexoIq05C502();
        }
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if (f502_iliquido != soma && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("I021", anexoIQ05C502Link, new String[]{anexoIQ05C502Link, anexoBQ04C3Link}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        RostoModel rosto = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoBModel anexoB = null;
            AnexoIModel anexoI = (AnexoIModel)model.getAnexo(key);
            String stringNifTitular = anexoI.getStringNifTitular();
            if (stringNifTitular != null) {
                anexoB = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(stringNifTitular);
            }
            Quadro05 q05Model = anexoI.getQuadro05();
            this.validateStep1AnexoIQ05(result, rosto, anexoB, anexoI, q05Model, stringNifTitular);
            this.validateI118(result, anexoI);
            this.validateI119(result, anexoI);
            this.validateI120(result, anexoI, anexoB);
            this.validateI121(result, anexoI);
            this.validateI122(result, anexoI, anexoB);
            this.validateI123(result, anexoI);
            this.validateI124(result, anexoI, anexoB);
            this.validateI125(result, anexoI);
            this.validateI126(result, anexoI, anexoB);
            this.validateI127(result, anexoI);
            this.validateI128(result, anexoI, anexoB);
            this.validateI129(result, anexoI);
            this.validateI130(result, anexoI);
            this.validateI131(result, anexoI);
        }
        return result;
    }
}

