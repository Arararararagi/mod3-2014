/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class AnexoIValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoIValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateI002(ValidationResult result, DeclaracaoModel model) {
        ArrayList<String> processedKeys = new ArrayList<String>();
        HashMap<Long, String> processedNifs = new HashMap<Long, String>();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key : keys) {
            AnexoIModel otherModel;
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key);
            if (processedKeys.contains(key.getSubId())) {
                otherModel = (AnexoIModel)model.getAnexo(new FormKey(AnexoIModel.class, key.getSubId()));
                if (otherModel == null) {
                    otherModel = anexoIModel;
                }
                result.add(new DeclValidationMessage("I002", anexoIModel.getLink("aAnexoI"), new String[]{otherModel.getLink("aAnexoI.qQuadro04.fanexoIq04C05"), anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C05")}));
            } else {
                processedKeys.add(key.getSubId());
            }
            if (anexoIModel.getQuadro04().getAnexoIq04C05() == null) continue;
            if (processedNifs.get(anexoIModel.getQuadro04().getAnexoIq04C05()) != null) {
                otherModel = (AnexoIModel)model.getAnexo(new FormKey(AnexoIModel.class, (String)processedNifs.get(anexoIModel.getQuadro04().getAnexoIq04C05())));
                if (otherModel == null) {
                    otherModel = anexoIModel;
                }
                result.add(new DeclValidationMessage("I002", anexoIModel.getLink("aAnexoI"), new String[]{otherModel.getLink("aAnexoI.qQuadro04.fanexoIq04C05"), anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C05")}));
                continue;
            }
            processedNifs.put(anexoIModel.getQuadro04().getAnexoIq04C05(), key.getSubId());
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        String anexoILink = null;
        for (FormKey key : keys) {
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key);
            anexoILink = anexoIModel.getLink("aAnexoI");
            this.validateI095(result, anexoILink, anexoIModel);
            if (Modelo3IRSv2015Parameters.instance().isFase2()) continue;
            result.add(new DeclValidationMessage("I096", anexoIModel.getLink("aAnexoI"), new String[]{anexoIModel.getLink("aAnexoI")}));
        }
        this.validateI002(result, model);
        return result;
    }

    protected void validateI095(ValidationResult result, String anexoILink, AnexoIModel anexoIModel) {
        if (anexoIModel.isEmpty()) {
            result.add(new DeclValidationMessage("I095", anexoILink, new String[]{anexoILink}));
        }
    }
}

