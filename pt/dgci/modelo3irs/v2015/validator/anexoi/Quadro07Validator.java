/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.EuroField;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String I046 = "I046";
    public static final String I102 = "I102";
    protected static final String ANEXO_I = "AnexoI";

    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private void validateStep1AnexoIQ07(ValidationResult result, DeclaracaoModel model, RostoModel rosto, AnexoBModel anexoBTitular, AnexoCModel anexoCTitular, AnexoDModel anexoDTitular, AnexoIModel anexoI, Quadro07 anexoITitularQ07, Long nifTitular) {
        Long q04AutorHeranca = anexoI.getQuadro04().getAnexoIq04C04();
        long herdeiros = anexoI.getQuadro05().getAnexoIq05C503() != null ? anexoI.getQuadro05().getAnexoIq05C503() : 0;
        long somaLiquido = anexoI.getQuadro05().getAnexoIq05C1a() != null ? anexoI.getQuadro05().getAnexoIq05C1a() : 0;
        Long f601 = anexoI.getQuadro06().getAnexoIq06C601();
        Long f602 = anexoI.getQuadro06().getAnexoIq06C602();
        long q06Prejuizo = f601 != null ? f601 : 0;
        long q06Lucro = f602 != null ? f602 : 0;
        Long somaComerciais = anexoITitularQ07.getAnexoIq07C2();
        Long somaAgriculas = anexoITitularQ07.getAnexoIq07C3();
        Long somaRetencoes = anexoI.getQuadro07().getAnexoIq07C4();
        EventList<AnexoIq07T1_Linha> linhasQ07 = anexoITitularQ07.getAnexoIq07T1();
        long nLinhasQ07 = linhasQ07.size();
        int maxLines = 80;
        short nLinhaAnexoIQ07 = 0;
        Iterator<AnexoIq07T1_Linha> itLinhasQ07 = linhasQ07.iterator();
        HashMap<String, String> nifsUnicos = new HashMap<String, String>(80);
        String anexoIQ05HerdeirosLink = anexoI.getLink("aAnexoI.qQuadro05.fanexoIq05C503");
        String anexoIQ07Link = anexoI.getLink("aAnexoI.qQuadro07.tanexoIq07T1");
        String somaComerciaisLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C2");
        String somaAgriculasLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C3");
        String somaRetencoesLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C4");
        String anexoBQ07C704Link = null;
        if (anexoBTitular != null) {
            anexoBQ07C704Link = anexoBTitular.getLink("aAnexoB.qQuadro07.fanexoBq07C702");
        }
        String anexoCQ08C804Link = null;
        if (anexoCTitular != null) {
            anexoCQ08C804Link = anexoCTitular.getLink("aAnexoC.qQuadro08.fanexoCq08C804");
        }
        if (nLinhasQ07 > 80) {
            result.add(new DeclValidationMessage("I039", anexoIQ07Link, new String[]{anexoIQ07Link}));
        }
        if (nLinhasQ07 == 0 && (q06Prejuizo != 0 || q06Lucro != 0)) {
            result.add(new DeclValidationMessage("I040", anexoIQ07Link, new String[]{anexoIQ07Link}));
        }
        long totalRespeitanteAoFalecidoQ7 = this.getTotalRespeitanteAoFalecido(anexoI);
        while (itLinhasQ07.hasNext()) {
            nLinhaAnexoIQ07 = (short)(nLinhaAnexoIQ07 + 1);
            this.validateStep1AnexoIQ07Linha(result, model, rosto, anexoBTitular, anexoCTitular, anexoDTitular, anexoI, anexoITitularQ07, nifTitular, nLinhaAnexoIQ07, itLinhasQ07.next(), nifsUnicos, somaLiquido, q04AutorHeranca, totalRespeitanteAoFalecidoQ7);
        }
        Quadro07 quadro07 = anexoI.getQuadro07();
        EventList<AnexoIq07T1_Linha> anexoIQ07Linhas = anexoI.getQuadro07().getAnexoIq07T1();
        long nLinhasParticipacao0 = 0;
        int somatorio = 0;
        int somaControloRendBruto = 0;
        int somaControloComerciais = 0;
        int somaControloAgriculas = 0;
        int somaControloRetencoes = 0;
        int somaControloValorImposto = 0;
        int nLinha = 1;
        Long nifFalecido = rosto.getQuadro07().getQ07C1();
        Long nifA = rosto.getQuadro03().getQ03C03();
        AnexoIq07T1_Linha contitular = null;
        AnexoIq07T1_Linha falecido = null;
        Integer numLinha = null;
        if (!anexoIQ07Linhas.isEmpty()) {
            long anexoIq07C2Value;
            long anexoIq07C1Value;
            long anexoIq07C5Value;
            long anexoIq07C4Value;
            long anexoIq07C3Value;
            for (AnexoIq07T1_Linha linha : anexoIQ07Linhas) {
                if (linha.getParticipacao() != null) {
                    somatorio = (int)((long)somatorio + linha.getParticipacao());
                }
                Long nifContitular = linha.getContitulares();
                if (linha.getParticipacao() == null || linha.getParticipacao() == 0 && Modelo3IRSValidatorUtil.equals(nifContitular, q04AutorHeranca)) {
                    ++nLinhasParticipacao0;
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentoBruto())) {
                    somaControloRendBruto = (int)((long)somaControloRendBruto + linha.getRendimentoBruto());
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentosComerciais())) {
                    somaControloComerciais = (int)((long)somaControloComerciais + linha.getRendimentosComerciais());
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentosAgricolas())) {
                    somaControloAgriculas = (int)((long)somaControloAgriculas + linha.getRendimentosAgricolas());
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes())) {
                    somaControloRetencoes = (int)((long)somaControloRetencoes + linha.getRetencoes());
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorImposto())) {
                    somaControloValorImposto = (int)((long)somaControloValorImposto + linha.getValorImposto());
                }
                if (linha.getContitulares() != null && nifA != null && nifA.equals(linha.getContitulares())) {
                    contitular = linha;
                    numLinha = nLinha;
                }
                if (linha.getContitulares() != null && nifFalecido != null && linha.getContitulares().equals(nifFalecido)) {
                    falecido = linha;
                }
                ++nLinha;
            }
            long l = anexoIq07C1Value = quadro07.getAnexoIq07C1() != null ? quadro07.getAnexoIq07C1() : 0;
            if (anexoIq07C1Value != (long)somaControloRendBruto) {
                result.add(new DeclValidationMessage("YI001", anexoI.getLink("aAnexoI.qQuadro07"), new String[]{anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C1")}));
            }
            long l2 = anexoIq07C2Value = quadro07.getAnexoIq07C2() != null ? quadro07.getAnexoIq07C2() : 0;
            if (anexoIq07C2Value != (long)somaControloComerciais) {
                result.add(new DeclValidationMessage("YI002", anexoI.getLink("aAnexoI.qQuadro07"), new String[]{anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C2")}));
            }
            long l3 = anexoIq07C3Value = quadro07.getAnexoIq07C3() != null ? quadro07.getAnexoIq07C3() : 0;
            if (anexoIq07C3Value != (long)somaControloAgriculas) {
                result.add(new DeclValidationMessage("YI003", anexoI.getLink("aAnexoI.qQuadro07"), new String[]{anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C3")}));
            }
            long l4 = anexoIq07C4Value = quadro07.getAnexoIq07C4() != null ? quadro07.getAnexoIq07C4() : 0;
            if (anexoIq07C4Value != (long)somaControloRetencoes) {
                result.add(new DeclValidationMessage("YI004", anexoI.getLink("aAnexoI.qQuadro07"), new String[]{anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C4")}));
            }
            long l5 = anexoIq07C5Value = quadro07.getAnexoIq07C5() != null ? quadro07.getAnexoIq07C5() : 0;
            if (anexoIq07C5Value != (long)somaControloValorImposto) {
                result.add(new DeclValidationMessage("YI005", anexoI.getLink("aAnexoI.qQuadro07"), new String[]{anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C5")}));
            }
            String participacaoQ07L0Link = anexoI.getLink("aAnexoI.qQuadro07.tanexoIq07T1.l1.c3");
            if (somatorio < 10000 || somatorio > 10000) {
                result.add(new DeclValidationMessage("I052", participacaoQ07L0Link, new String[]{participacaoQ07L0Link}));
            }
            if (anexoIQ07Linhas.size() == 1 && anexoIQ07Linhas.get(0).getParticipacao() != null && anexoIQ07Linhas.get(0).getParticipacao() == 10000) {
                result.add(new DeclValidationMessage("I053", participacaoQ07L0Link, new String[]{participacaoQ07L0Link}));
            }
        }
        if (nLinhasParticipacao0 > 1) {
            result.add(new DeclValidationMessage("I084", anexoIQ07Link));
        }
        this.validateStep1AnexoIQ07SomaAgriculas(result, anexoBTitular, anexoCTitular, anexoI, somaComerciais, somaAgriculas, herdeiros, somaComerciaisLink, somaAgriculasLink, anexoIQ05HerdeirosLink);
        this.validateStep1AnexoIQ07SomaRetencoes(result, anexoBTitular, anexoCTitular, somaRetencoes, somaRetencoesLink, anexoBQ07C704Link, anexoCQ08C804Link);
        Long nifCabecaCasal = anexoI.getQuadro04().getAnexoIq04C06();
        AnexoDModel anexoDModel = nifCabecaCasal != null ? ((Modelo3IRSv2015Model)model).getAnexoDByTitular(nifCabecaCasal) : null;
        this.validateI100(result, contitular, falecido, rosto, anexoI, anexoDModel, numLinha);
        this.validateI103(result, contitular, falecido, rosto, anexoI, anexoDModel, numLinha);
        this.validateI106(result, contitular, falecido, rosto, anexoI, anexoDModel, numLinha);
    }

    private long getTotalRespeitanteAoFalecido(AnexoIModel anexoI) {
        if (anexoI != null) {
            for (AnexoIq07T1_Linha linha : anexoI.getQuadro07().getAnexoIq07T1()) {
                if (linha.getParticipacao() == null || linha.getParticipacao() != 0) continue;
                long rendimentoBruto = linha.getRendimentosAgricolas() != null ? Math.abs(linha.getRendimentosAgricolas()) : 0;
                long rendimentocomercial = linha.getRendimentosComerciais() != null ? Math.abs(linha.getRendimentosComerciais()) : 0;
                return rendimentoBruto + rendimentocomercial;
            }
        }
        return 0;
    }

    protected void validateI100(ValidationResult result, AnexoIq07T1_Linha contitular, AnexoIq07T1_Linha falecido, RostoModel rosto, AnexoIModel anexoIModel, AnexoDModel anexoDModel, Integer numLinha) {
        long rendimentosAgricolasContitular = contitular != null && contitular.getRendimentosAgricolas() != null ? contitular.getRendimentosAgricolas() : 0;
        long rendimentosComerciaisContitular = contitular != null && contitular.getRendimentosComerciais() != null ? contitular.getRendimentosComerciais() : 0;
        long rendimentosAgricolasFalecido = falecido != null && falecido.getRendimentosAgricolas() != null ? falecido.getRendimentosAgricolas() : 0;
        long rendimentosComerciaisFalecido = falecido != null && falecido.getRendimentosComerciais() != null ? falecido.getRendimentosComerciais() : 0;
        long totalRendimentos = rendimentosAgricolasContitular + rendimentosComerciaisContitular + rendimentosAgricolasFalecido + rendimentosComerciaisFalecido;
        if (falecido != null && contitular != null && anexoDModel == null && totalRendimentos > 0) {
            String linkNifContitular = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha));
            String linkAnexoDModel = anexoDModel != null ? anexoDModel.getLink("aAnexoD") : "aAnexoD";
            result.add(new DeclValidationMessage("I100", linkNifContitular, new String[]{linkAnexoDModel, linkNifContitular}));
        }
    }

    private void validateStep1AnexoIQ07Linha(ValidationResult result, DeclaracaoModel model, RostoModel rosto, AnexoBModel anexoBTitular, AnexoCModel anexoCTitular, AnexoDModel anexoDTitular, AnexoIModel anexoITitular, Quadro07 anexoIQ07, Long nifTitular, short nLinha, AnexoIq07T1_Linha linha, Map<String, String> nifsUnicos, long anexoIQ05somaLiquido, Long q04AutorDaHeranca, long totalRespeitanteAoFalecidoQ07) {
        Quadro02 rostoQ02 = rosto.getQuadro02();
        pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQ03 = rosto.getQuadro03();
        pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQ07 = rosto.getQuadro07();
        Quadro06 anexoIQ06 = anexoITitular.getQuadro06();
        Long anoExercicio = rostoQ02.getQ02C02();
        Long nifContitular = linha.getContitulares();
        Long participacao = linha.getParticipacao();
        Long rendBruto = linha.getRendimentoBruto();
        Long rendComerciais = linha.getRendimentosComerciais();
        Long percentParticipacao = linha.getParticipacao();
        Long rendAgricolas = linha.getRendimentosAgricolas();
        Long retencoes = linha.getRetencoes();
        Long valorImposto = linha.getValorImposto();
        AnexoDModel anexoDContitular = null;
        String anexoDContitularQ04PercentImputacaoLink = null;
        String anexoDContitularLink = null;
        String anexoDContitularQ04F471Link = "aAnexoD.qQuadro04.tanexoDq04T1";
        String anexoDContitularQ04C481Link = "aAnexoD.qQuadro04.tanexoDq04T1";
        anexoDContitularQ04PercentImputacaoLink = "aAnexoD.qQuadro04.tanexoDq04T1";
        if (nifContitular != null && (anexoDContitular = ((Modelo3IRSv2015Model)model).getAnexoDByTitular(nifContitular)) != null) {
            anexoDContitularLink = anexoDContitular.getLink("aAnexoD");
            anexoDContitularQ04F471Link = anexoDContitularQ04F471Link + (!anexoDContitular.getQuadro04().getAnexoDq04T1().isEmpty() ? ".l1.c4" : "");
            anexoDContitularQ04C481Link = anexoDContitularQ04C481Link + (!anexoDContitular.getQuadro04().getAnexoDq04T1().isEmpty() ? ".l1.c5" : "");
            anexoDContitularQ04F471Link = anexoDContitular.getLink(anexoDContitularQ04F471Link);
            anexoDContitularQ04C481Link = anexoDContitular.getLink(anexoDContitularQ04C481Link);
            anexoDContitularQ04PercentImputacaoLink = anexoDContitular.getLink(anexoDContitularQ04PercentImputacaoLink);
        }
        String anexoILink = anexoITitular.getLink("aAnexoI");
        String anexoIQ06LucroLink = anexoITitular.getLink("aAnexoI.qQuadro06.fanexoIq06C602");
        String anexoIQ07LinhaLink = "aAnexoI.qQuadro07.tanexoIq07T1.l" + nLinha;
        String nifContitularLink = anexoITitular.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.CONTITULARES.getIndex());
        String participacaoLink = anexoITitular.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.PARTICIPACAO.getIndex());
        String rendBrutoLink = anexoITitular.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.RENDIMENTOBRUTO.getIndex());
        String rendComerciaisLink = anexoITitular.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.RENDIMENTOSCOMERCIAIS.getIndex());
        String rendAgricolasLink = anexoITitular.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.RENDIMENTOSAGRICOLAS.getIndex());
        if (nifContitular != null) {
            String nifContitularStr = String.valueOf(nifContitular);
            if (nifsUnicos.containsKey(nifContitularStr)) {
                result.add(new DeclValidationMessage("I041", nifContitularLink, new String[]{nifContitularLink}));
            } else {
                nifsUnicos.put(nifContitularStr, null);
            }
        }
        this.validateStep1AnexoIQ07LinhaContitular(result, rosto, anexoITitular, anexoDContitular, rostoQ03, rostoQ07, nifContitular, participacao, rendBruto, rendComerciais, rendAgricolas, retencoes, valorImposto, nifContitularLink, participacaoLink, rendBrutoLink, anexoDContitularLink, anexoILink, linha, totalRespeitanteAoFalecidoQ07, nLinha);
        this.validateStep1AnexoIQ07LinhaParticipacao(result, anexoITitular, participacao, participacaoLink, nifContitular, q04AutorDaHeranca, anexoDContitular, nifTitular, percentParticipacao, anexoDContitularQ04PercentImputacaoLink);
        this.validateStep1AnexoIQ07LinhaRendComerciais(result, rosto, anexoBTitular, anexoCTitular, anexoDContitular, anexoITitular, nifContitular, nifTitular, rendComerciais, rendAgricolas, rendComerciaisLink, rendAgricolasLink, anexoIQ06LucroLink, anexoDContitularQ04F471Link, percentParticipacao);
        this.validateStep1AnexoIQ07LinhaRendAgricolas(result, rosto, anexoIQ06, anexoBTitular, anexoCTitular, anexoDContitular, anexoITitular, nifContitular, nifTitular, anoExercicio, rendComerciais, rendAgricolas, anexoIQ05somaLiquido, rendComerciaisLink, rendAgricolasLink, anexoIQ06LucroLink, anexoDContitularQ04F471Link);
    }

    protected void validateI042(ValidationResult result, AnexoIq07T1_Linha current, AnexoIModel anexoI, int numLinha) {
        Long nifContitular = current.getContitulares();
        Long participacao = current.getParticipacao();
        Long rendComerciais = current.getRendimentosComerciais();
        Long rendAgricolas = current.getRendimentosAgricolas();
        Long retencoes = current.getRetencoes();
        Long valorImposto = current.getValorImposto();
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(nifContitular) && Modelo3IRSValidatorUtil.isEmptyOrZero(participacao) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendComerciais) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendAgricolas) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencoes) && Modelo3IRSValidatorUtil.isEmptyOrZero(valorImposto)) {
            String nifContitularLink = anexoI.getLink(AnexoIq07T1_Linha.getLink(numLinha, AnexoIq07T1_LinhaBase.Property.CONTITULARES));
            result.add(new DeclValidationMessage("I042", nifContitularLink, new String[]{nifContitularLink}));
        }
    }

    private void validateStep1AnexoIQ07LinhaContitular(ValidationResult result, RostoModel rosto, AnexoIModel anexoITitular, AnexoDModel anexoDContitular, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQ03, pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQ07, Long nifContitular, Long participacao, Long rendBruto, Long rendComerciais, Long rendAgricolas, Long retencoes, Long valorImposto, String nifContitularLink, String participacaoLink, String rendBrutoLink, String anexoDContitularLink, String anexoILink, AnexoIq07T1_Linha current, long totalRespeitanteAoFalecido, int numLinha) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nifContitular)) {
            if (!Modelo3IRSValidatorUtil.startsWith(nifContitular, "1", "2")) {
                result.add(new DeclValidationMessage("I043", nifContitularLink, new String[]{nifContitularLink}));
            }
            if (!Modelo3IRSValidatorUtil.isNIFValid(nifContitular)) {
                result.add(new DeclValidationMessage("I045", nifContitularLink, new String[]{nifContitularLink}));
            }
            this.validateI102(result, nifContitular, participacao, rosto, nifContitularLink);
            this.validateI046(result, anexoITitular, anexoDContitular, rostoQ03, rostoQ07, nifContitular, rendComerciais, rendAgricolas, nifContitularLink, rosto);
            this.validateI101(result, current, anexoDContitular, anexoITitular, rostoQ03, rostoQ07, numLinha);
            this.validateI104(result, current, anexoDContitular, anexoITitular, rostoQ03, rostoQ07, numLinha);
            this.validateI105(result, current, anexoDContitular, anexoITitular, rostoQ03, rostoQ07, numLinha);
            this.validateI107(result, current, anexoDContitular, anexoITitular, rostoQ03, rostoQ07, numLinha);
            this.validateI108(result, current, anexoDContitular, anexoITitular, rostoQ03, rostoQ07, numLinha);
            this.validateI109(result, rosto, anexoITitular, current, numLinha);
            this.validateI111(result, current, totalRespeitanteAoFalecido, anexoITitular, numLinha);
            this.validateI112(result, current, totalRespeitanteAoFalecido, anexoITitular, numLinha);
        }
        if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(nifContitular) || Modelo3IRSValidatorUtil.isEmptyOrZero(participacao) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendComerciais) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendAgricolas) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencoes) && Modelo3IRSValidatorUtil.isEmptyOrZero(valorImposto))) {
            result.add(new DeclValidationMessage("I044", nifContitularLink, new String[]{nifContitularLink}));
        }
    }

    public void validateI101(ValidationResult result, AnexoIq07T1_Linha current, AnexoDModel anexoD, AnexoIModel anexoIModel, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQuadro03, pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQuadro07, int linha) {
        long rendimentosAgricolas;
        long rendimentosComerciais = current.getRendimentosComerciais() != null ? current.getRendimentosComerciais() : 0;
        long l = rendimentosAgricolas = current.getRendimentosAgricolas() != null ? current.getRendimentosAgricolas() : 0;
        if (current.getContitulares() != null && rostoQuadro03.existsInDependentes(current.getContitulares()) && anexoD == null && rendimentosAgricolas + rendimentosComerciais > 0 && rostoQuadro07.getQ07C1() != null) {
            String linkAnexoD = anexoD != null ? anexoD.getLink("aAnexoD") : "aAnexoD";
            String linkAnexoI = anexoIModel.getLink("aAnexoI");
            String linkContitulares = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(linha));
            result.add(new DeclValidationMessage("I101", linkContitulares, new String[]{linkAnexoD, linkAnexoI}));
        }
    }

    protected void validateI046(ValidationResult result, AnexoIModel anexoITitular, AnexoDModel anexoDContitular, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQ03, pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQ07, Long nifContitular, Long rendComerciais, Long rendAgricolas, String nifContitularLink, RostoModel rosto) {
        long rendComercPlusRendAgric = 0;
        rendComercPlusRendAgric+=rendComerciais == null ? 0 : rendComerciais;
        if (nifContitular != null && (rostoQ03.isSujeitoPassivoA(nifContitular) || rostoQ03.isSujeitoPassivoB(nifContitular) || rostoQ03.existsInDependentes(nifContitular) || rostoQ07.isConjugeFalecido(nifContitular)) && anexoDContitular == null && (rendComercPlusRendAgric+=rendAgricolas == null ? 0 : rendAgricolas) > 0 && rostoQ07.getQ07C1() == null) {
            result.add(new DeclValidationMessage("I046", nifContitularLink, new String[]{anexoITitular.getLink("aAnexoI.qQuadro07")}));
        }
    }

    protected void validateI102(ValidationResult result, Long nifContitular, Long participacao, RostoModel rosto, String nifContitularLink) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(nifContitular) || !Modelo3IRSValidatorUtil.equals(nifContitular, rosto.getQuadro07().getQ07C1()) || Modelo3IRSValidatorUtil.isEmptyOrZero(participacao))) {
            result.add(new DeclValidationMessage("I102", nifContitularLink, new String[]{nifContitularLink}));
        }
    }

    private void validateStep1AnexoIQ07LinhaParticipacao(ValidationResult result, AnexoIModel anexoI, Long participacao, String participacaoLink, Long nifContitular, Long q04AutorDaHeranca, AnexoDModel anexoDContitular, Long nifTitular, Long percentParticipacao, String anexoDContitularQ04PercentImputacaoLink) {
        if (nifContitular != null && nifTitular != null && anexoDContitular != null && percentParticipacao != null) {
            long percentImputadaAnexoD = anexoDContitular.getQuadro04().getPercentImputacao(nifTitular);
            if (percentParticipacao != percentImputadaAnexoD) {
                result.add(new DeclValidationMessage("I098", participacaoLink, new String[]{participacaoLink, anexoDContitularQ04PercentImputacaoLink}));
            }
        }
        if (participacao != null && (participacao < 0 || participacao > 10000)) {
            result.add(new DeclValidationMessage("I047", participacaoLink, new String[]{participacaoLink}));
        }
        if (!(participacao == null || participacao != 0 || Modelo3IRSValidatorUtil.equals(nifContitular, q04AutorDaHeranca))) {
            result.add(new DeclValidationMessage("I085", anexoI.getLink("aAnexoI.qQuadro04"), new String[]{anexoI.getLink("aAnexoI.qQuadro04.fanexoIq04C04")}));
        }
    }

    public static long getValorCampo(AnexoDModel anexoD, int campo) {
        if (anexoD != null) {
            Quadro04 anexoDQ04 = anexoD.getQuadro04();
            switch (campo) {
                case 471: {
                    long sumF471 = 0;
                    for (AnexoDq04T1_Linha linha : anexoDQ04.getAnexoDq04T1()) {
                        sumF471+=linha.getRendimentos() != null ? linha.getRendimentos() : 0;
                    }
                    return sumF471;
                }
                case 481: {
                    long sumF481 = 0;
                    for (AnexoDq04T1_Linha linha : anexoDQ04.getAnexoDq04T1()) {
                        sumF481+=linha.getRetencao() != null ? linha.getRetencao() : 0;
                    }
                    return sumF481;
                }
            }
        }
        return 0;
    }

    private void validateStep1AnexoIQ07LinhaRendComerciais(ValidationResult result, RostoModel rosto, AnexoBModel anexoB, AnexoCModel anexoC, AnexoDModel anexoDContitular, AnexoIModel anexoI, Long nifContitular, Long nifTitular, Long rendComerciais, Long rendAgricolas, String rendComerciaisLink, String rendAgricolasLink, String anexoIQ06LucroLink, String anexoDQ04F471Link, Long percentParticipacao) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rendComerciais) || anexoI.getQuadro05().isEmpty() || anexoB != null && anexoB.getQuadro01().getAnexoBq01B3() != null && anexoB.getQuadro01().getAnexoBq01B3().booleanValue())) {
            result.add(new DeclValidationMessage("I064", rendComerciaisLink, new String[]{rendComerciaisLink}));
        }
    }

    protected void validateI065(ValidationResult result, AnexoIq07T1_Linha linha, AnexoIModel anexoI, AnexoCModel anexoC, int numLinha) {
        Long rendComerciais = linha.getRendimentosComerciais();
        String rendComerciaisLink = anexoI.getLink(AnexoIq07T1_Linha.getLink(numLinha, AnexoIq07T1_LinhaBase.Property.RENDIMENTOSCOMERCIAIS));
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rendComerciais) || anexoI.getQuadro06().isEmpty() || anexoC != null && anexoC.getQuadro01().getAnexoCq01B1() != null && anexoC.getQuadro01().getAnexoCq01B1().booleanValue())) {
            result.add(new DeclValidationMessage("I065", rendComerciaisLink, new String[]{rendComerciaisLink}));
        }
    }

    private long getValorMinimoRegimeSimplificado(short exercicio) {
        long retValue;
        switch (exercicio) {
            case 2001: {
                retValue = 233936;
                break;
            }
            case 2002: {
                retValue = 243607;
                break;
            }
            case 2003: {
                retValue = 312500;
                break;
            }
            case 2004: {
                retValue = 312500;
                break;
            }
            case 2005: {
                retValue = 312500;
                break;
            }
            case 2006: {
                retValue = 270130;
                break;
            }
            case 2007: {
                retValue = 282100;
                break;
            }
            case 2008: {
                retValue = 298200;
                break;
            }
            default: {
                retValue = 315000;
            }
        }
        return retValue;
    }

    private void validateStep1AnexoIQ07LinhaRendAgricolas(ValidationResult result, RostoModel rosto, Quadro06 anexoIQ06, AnexoBModel anexoB, AnexoCModel anexoC, AnexoDModel anexoDContitular, AnexoIModel anexoI, Long nifContitular, Long nifTitular, Long anoExercicio, Long rendComerciais, Long rendAgricolas, long anexoIQ05somaLiquido, String rendComerciaisLink, String rendAgricolasLink, String anexoIQ06LucroLink, String anexoDQ04F471Link) {
        long maxSumByExercicio = 0;
        if (anoExercicio != null) {
            maxSumByExercicio = this.getValorMinimoRegimeSimplificado(anoExercicio.shortValue());
        }
        long f614_agricolas = 0;
        long f614_comerciais = 0;
        for (AnexoIq07T1_Linha linha : anexoI.getQuadro07().getAnexoIq07T1()) {
            if (linha.getRendimentosComerciais() != null) {
                f614_comerciais+=linha.getRendimentosComerciais().longValue();
            }
            if (linha.getRendimentosAgricolas() == null) continue;
            f614_agricolas+=linha.getRendimentosAgricolas().longValue();
        }
        if (rendComerciais == null && rendAgricolas == null) {
            result.add(new DeclValidationMessage("I059", rendAgricolasLink, new String[]{rendComerciaisLink, rendAgricolasLink}));
        }
        if (anoExercicio != null && anoExercicio < 2010 && anexoIQ05somaLiquido > 0 && anexoB != null && f614_comerciais + f614_agricolas + 1 < maxSumByExercicio) {
            result.add(new DeclValidationMessage("I060", rendAgricolasLink, new String[]{rendAgricolasLink}, new String[]{new EuroField("euro", maxSumByExercicio).format()}));
        }
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rendAgricolas) || anexoI.getQuadro05().isEmpty() || anexoB != null && anexoB.getQuadro01().getAnexoBq01B4() != null && anexoB.getQuadro01().getAnexoBq01B4().booleanValue())) {
            result.add(new DeclValidationMessage("I066", rendAgricolasLink, new String[]{rendAgricolasLink}));
        }
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rendAgricolas) || anexoI.getQuadro06().isEmpty() || anexoC != null && anexoC.getQuadro01().getAnexoCq01B2() != null && anexoC.getQuadro01().getAnexoCq01B2().booleanValue())) {
            result.add(new DeclValidationMessage("I067", rendAgricolasLink, new String[]{rendAgricolasLink}));
        }
    }

    private void validateStep1AnexoIQ07SomaAgriculas(ValidationResult result, AnexoBModel anexoB, AnexoCModel anexoC, AnexoIModel anexoI, Long somaComerciais, Long somaAgricolas, Long herdeiros, String somaComerciaisLink, String somaAgricolasLink, String herdeirosLink) {
        boolean condicao1;
        long somaValorImputado = 0;
        somaValorImputado+=somaComerciais != null ? somaComerciais : 0;
        int margem = 100;
        boolean bl = condicao1 = Math.abs(herdeiros - (somaValorImputado+=somaAgricolas != null ? somaAgricolas : 0)) > (long)margem;
        if (condicao1 && herdeiros > 0) {
            result.add(new DeclValidationMessage("I068", somaComerciaisLink, new String[]{somaComerciaisLink, herdeirosLink}));
        }
    }

    private void validateStep1AnexoIQ07SomaRetencoes(ValidationResult result, AnexoBModel anexoB, AnexoCModel anexoC, Long somaRetencoes, String retencoesLink, String anexoBQ07C704Link, String anexoCQ08C804Link) {
        if (anexoB != null) {
            long f704_3;
            long l = f704_3 = anexoB.getQuadro07().getAnexoBq07C702() != null ? anexoB.getQuadro07().getAnexoBq07C702() : 0;
            if (somaRetencoes != null && Math.abs(somaRetencoes - f704_3) > 100) {
                result.add(new DeclValidationMessage("I069", retencoesLink, new String[]{retencoesLink, anexoBQ07C704Link}));
            }
        }
    }

    protected void validateI070(ValidationResult result, AnexoCModel anexoC, Long somaRetencoes, String retencoesLink, String anexoCQ08C804Link) {
        if (anexoC != null) {
            long f804_3;
            long l = f804_3 = anexoC.getQuadro08().getAnexoCq08C802() != null ? anexoC.getQuadro08().getAnexoCq08C802() : 0;
            if (somaRetencoes != null && Math.abs(somaRetencoes - f804_3) > 100) {
                result.add(new DeclValidationMessage("I070", retencoesLink, new String[]{retencoesLink, anexoCQ08C804Link}));
            }
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateI054(ValidationResult result, AnexoBModel anexoB, String rendBrutoLink, long somaAnexoIQ07RendBrutos) {
        long somaAnexoBC_1101_e_C_1102 = 0;
        if (anexoB.getQuadro11().getAnexoBq11C1101() != null) {
            somaAnexoBC_1101_e_C_1102+=anexoB.getQuadro11().getAnexoBq11C1101().longValue();
        }
        if (anexoB.getQuadro11().getAnexoBq11C1102() != null) {
            somaAnexoBC_1101_e_C_1102+=anexoB.getQuadro11().getAnexoBq11C1102().longValue();
        }
        String anexoBC1101Link = anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1101");
        String anexoBC1102Link = anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1102");
        if (somaAnexoIQ07RendBrutos != somaAnexoBC_1101_e_C_1102) {
            result.add(new DeclValidationMessage("I054", rendBrutoLink, new String[]{rendBrutoLink, anexoBC1101Link, anexoBC1102Link}));
        }
    }

    protected void validateI082(ValidationResult result, AnexoIModel anexoI, AnexoIq07T1_Linha linha, long nLinha) {
        Long nifContitular = linha.getContitulares();
        Long participacao = linha.getParticipacao();
        Long rendBruto = linha.getRendimentoBruto();
        Long rendComerciais = linha.getRendimentosComerciais();
        Long rendAgricolas = linha.getRendimentosAgricolas();
        Long retencoes = linha.getRetencoes();
        Long valorImposto = linha.getValorImposto();
        String anexoIQ07LinhaLink = "aAnexoI.qQuadro07.tanexoIq07T1.l" + nLinha;
        String nifContitularLink = anexoI.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.CONTITULARES.getIndex());
        if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(nifContitular) || participacao == null && Modelo3IRSValidatorUtil.isEmptyOrZero(rendBruto) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendComerciais) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendAgricolas) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencoes) && Modelo3IRSValidatorUtil.isEmptyOrZero(valorImposto))) {
            result.add(new DeclValidationMessage("I082", nifContitularLink, new String[]{nifContitularLink}));
        }
    }

    protected void validateI081(ValidationResult result, AnexoIModel anexoI, AnexoIq07T1_Linha linha, long nLinha) {
        Long nifContitular = linha.getContitulares();
        Long participacao = linha.getParticipacao();
        String anexoIQ07LinhaLink = "aAnexoI.qQuadro07.tanexoIq07T1.l" + nLinha;
        String nifContitularLink = anexoI.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.CONTITULARES.getIndex());
        String participacaoLink = anexoI.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.PARTICIPACAO.getIndex());
        String rendBrutoLink = anexoI.getLink(anexoIQ07LinhaLink + ".c" + AnexoIq07T1_LinhaBase.Property.RENDIMENTOBRUTO.getIndex());
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(nifContitular) || participacao != null)) {
            result.add(new DeclValidationMessage("I081", nifContitularLink, new String[]{nifContitularLink, participacaoLink, rendBrutoLink}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        String stringNifTitular;
        AnexoIModel anexoITitular;
        ValidationResult result = new ValidationResult();
        RostoModel rosto = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key2 : keys) {
            anexoITitular = (AnexoIModel)model.getAnexo(key2);
            stringNifTitular = anexoITitular.getStringNifTitular();
            Long nifTitular = anexoITitular.getNifTitular();
            AnexoBModel anexoBTitular = null;
            AnexoCModel anexoCTitular = null;
            AnexoDModel anexoDTitular = null;
            if (stringNifTitular != null) {
                anexoBTitular = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(stringNifTitular);
                anexoCTitular = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(stringNifTitular);
                anexoDTitular = ((Modelo3IRSv2015Model)model).getAnexoDByTitular(stringNifTitular);
            }
            Quadro07 anexoITitularQ07 = anexoITitular.getQuadro07();
            this.validateStep1AnexoIQ07(result, model, rosto, anexoBTitular, anexoCTitular, anexoDTitular, anexoITitular, anexoITitularQ07, nifTitular);
            this.validateI113(result, rosto, anexoITitular);
            this.validateI114(result, anexoITitular);
        }
        for (FormKey key2 : keys) {
            anexoITitular = (AnexoIModel)model.getAnexo(key2);
            stringNifTitular = anexoITitular.getStringNifTitular();
            AnexoCModel anexoCTitular = null;
            AnexoDModel anexoD = null;
            if (stringNifTitular != null) {
                anexoCTitular = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(stringNifTitular);
                anexoD = ((Modelo3IRSv2015Model)model).getAnexoDByTitular(stringNifTitular);
            }
            Long somaRetencoes = anexoITitular.getQuadro07().getAnexoIq07C4();
            String somaRetencoesLink = anexoITitular.getLink("aAnexoI.qQuadro07.fanexoIq07C4");
            String anexoCQ08C804Link = "";
            if (anexoCTitular != null) {
                anexoCQ08C804Link = anexoCTitular.getLink("aAnexoC.qQuadro08.fanexoCq08C804");
            }
            this.validateI070(result, anexoCTitular, somaRetencoes, somaRetencoesLink, anexoCQ08C804Link);
            EventList<AnexoIq07T1_Linha> linhasQ07 = anexoITitular.getQuadro07().getAnexoIq07T1();
            int numLinha = 1;
            for (AnexoIq07T1_Linha linha : linhasQ07) {
                this.validateI042(result, linha, anexoITitular, numLinha);
                this.validateI065(result, linha, anexoITitular, anexoCTitular, numLinha);
                this.validateI115(result, linha, anexoD, anexoITitular, rosto, numLinha);
                this.validateI116(result, linha, anexoD, anexoITitular, rosto, numLinha);
                ++numLinha;
            }
        }
        return result;
    }

    public void validateI103(ValidationResult result, AnexoIq07T1_Linha contitular, AnexoIq07T1_Linha falecido, RostoModel rosto, AnexoIModel anexoIModel, AnexoDModel anexoD, Integer numLinha) {
        long rendimentosEntidadeImputadora;
        long rendimentosAgricolasContitular = contitular != null && contitular.getRendimentosAgricolas() != null ? contitular.getRendimentosAgricolas() : 0;
        long rendimentosComerciaisContitular = contitular != null && contitular.getRendimentosComerciais() != null ? contitular.getRendimentosComerciais() : 0;
        long rendimentosAgricolasFalecido = falecido != null && falecido.getRendimentosAgricolas() != null ? falecido.getRendimentosAgricolas() : 0;
        long rendimentosComerciaisFalecido = falecido != null && falecido.getRendimentosComerciais() != null ? falecido.getRendimentosComerciais() : 0;
        long totalRendimentos = rendimentosAgricolasContitular + rendimentosComerciaisContitular + rendimentosAgricolasFalecido + rendimentosComerciaisFalecido;
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoD != null ? anexoD.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long l = rendimentosEntidadeImputadora = entidadeImputadora != null && entidadeImputadora.getRendimentos() != null ? entidadeImputadora.getRendimentos() : 0;
        if (falecido != null && contitular != null && (anexoD == null || entidadeImputadora != null && Math.abs(rendimentosEntidadeImputadora - totalRendimentos) > 100)) {
            String linkNifContitular = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha));
            String linkAnexoDModel = anexoD != null ? anexoD.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            result.add(new DeclValidationMessage("I103", linkNifContitular, new String[]{linkNifContitular, linkAnexoDModel}));
        }
    }

    public void validateI104(ValidationResult result, AnexoIq07T1_Linha current, AnexoDModel anexoD, AnexoIModel anexoIModel, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQuadro03, pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQuadro07, Integer linha) {
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoD != null ? anexoD.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long rendimentosEntidadeImputadora = entidadeImputadora != null && entidadeImputadora.getRendimentos() != null ? entidadeImputadora.getRendimentos() : 0;
        long rendimentosComerciais = current.getRendimentosComerciais() != null ? current.getRendimentosComerciais() : 0;
        long rendimentosAgricolas = current.getRendimentosAgricolas() != null ? current.getRendimentosAgricolas() : 0;
        long totalRendimentos = rendimentosAgricolas + rendimentosComerciais;
        if (current.getContitulares() != null && rostoQuadro03.existsInDependentes(current.getContitulares()) && rostoQuadro07.getQ07C1() != null && (anexoD == null || anexoD != null && (entidadeImputadora == null || entidadeImputadora != null && Math.abs(rendimentosEntidadeImputadora - totalRendimentos) > 100))) {
            String linkAnexoD = anexoD != null ? anexoD.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            String linkAnexoI = anexoIModel.getLink("aAnexoI");
            String linkContitulares = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(linha));
            result.add(new DeclValidationMessage("I104", linkContitulares, new String[]{linkAnexoI, linkAnexoD}));
        }
    }

    protected void validateI115(ValidationResult result, AnexoIq07T1_Linha current, AnexoDModel anexoDModel, AnexoIModel anexoIModel, RostoModel rosto, int linha) {
        boolean notExistsAnexoD = false;
        if (!(anexoDModel != null && anexoIModel != null && (anexoDModel == null || anexoIModel == null || anexoDModel.getQuadro03() == null || anexoIModel.getQuadro07() == null || anexoIModel.getQuadro07().existNifInQ7C70i1(anexoDModel.getQuadro03().getAnexoDq03C06())))) {
            notExistsAnexoD = true;
        }
        long rendimentosComerciais = current.getRendimentosComerciais() != null ? current.getRendimentosComerciais() : 0;
        long rendimentosAgricolas = current.getRendimentosAgricolas() != null ? current.getRendimentosAgricolas() : 0;
        long totalRendimentos = rendimentosAgricolas + rendimentosComerciais;
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoDModel != null ? anexoDModel.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long rendimentosEntidadeImputadora = 0;
        if (entidadeImputadora != null && entidadeImputadora.getRendimentos() != null) {
            rendimentosEntidadeImputadora = entidadeImputadora.getRendimentos();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(current.getContitulares()) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(current.getContitulares()) && notExistsAnexoD && rosto.getQuadro07().getQ07C1() != null && (entidadeImputadora == null || entidadeImputadora != null && Math.abs(rendimentosEntidadeImputadora - totalRendimentos) > 100)) {
            String linkAnexoD = anexoDModel != null ? anexoDModel.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            String linkAnexoI = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(linha, AnexoIq07T1_LinhaBase.Property.CONTITULARES));
            result.add(new DeclValidationMessage("I115", anexoIModel.getLink("aAnexoI.qQuadro07.tanexoIq07T1"), new String[]{linkAnexoI, linkAnexoD}));
        }
    }

    protected void validateI116(ValidationResult result, AnexoIq07T1_Linha current, AnexoDModel anexoDModel, AnexoIModel anexoIModel, RostoModel rosto, int linha) {
        boolean notExistsAnexoD = false;
        if (!(anexoDModel != null && anexoIModel != null && (anexoDModel == null || anexoIModel == null || anexoDModel.getQuadro03() == null || anexoIModel.getQuadro07() == null || anexoIModel.getQuadro07().existNifInQ7C70i1(anexoDModel.getQuadro03().getAnexoDq03C06())))) {
            notExistsAnexoD = true;
        }
        long totalRetencao = current.getRetencoes() != null ? current.getRetencoes() : 0;
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoDModel != null ? anexoDModel.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long rentencaoEntidadeImputadora = 0;
        if (entidadeImputadora != null && entidadeImputadora.getRetencao() != null) {
            rentencaoEntidadeImputadora = entidadeImputadora.getRetencao();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(current.getContitulares()) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(current.getContitulares()) && notExistsAnexoD && rosto.getQuadro07().getQ07C1() != null && (entidadeImputadora == null || entidadeImputadora != null && Math.abs(rentencaoEntidadeImputadora - totalRetencao) > 100)) {
            String linkAnexoD = anexoDModel != null ? anexoDModel.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            String linkAnexoI = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(linha, AnexoIq07T1_LinhaBase.Property.CONTITULARES));
            result.add(new DeclValidationMessage("I116", anexoIModel.getLink("aAnexoI.qQuadro07.tanexoIq07T1"), new String[]{linkAnexoI, linkAnexoD}));
        }
    }

    public void validateI105(ValidationResult result, AnexoIq07T1_Linha current, AnexoDModel anexoD, AnexoIModel anexoIModel, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQuadro03, pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQuadro07, Integer numLinha) {
        boolean existsInDepOrSujPass;
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoD != null ? anexoD.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long rendimentosEntidadeImputadora = entidadeImputadora != null && entidadeImputadora.getRendimentos() != null ? entidadeImputadora.getRendimentos() : 0;
        long rendimentosComerciais = current.getRendimentosComerciais() != null ? current.getRendimentosComerciais() : 0;
        long rendimentosAgricolas = current.getRendimentosAgricolas() != null ? current.getRendimentosAgricolas() : 0;
        long totalRendimentos = rendimentosAgricolas + rendimentosComerciais;
        Long contitular = current.getContitulares();
        boolean bl = existsInDepOrSujPass = contitular != null && (rostoQuadro03.existsInDependentes(contitular) || rostoQuadro03.isSujeitoPassivoA(contitular) || rostoQuadro03.isSujeitoPassivoB(contitular) || rostoQuadro03.existsInDependentesEmGuardaConjunta(contitular));
        if (rostoQuadro07.getQ07C1() == null && existsInDepOrSujPass && (anexoD == null || anexoD != null && (entidadeImputadora == null || entidadeImputadora != null && Math.abs(rendimentosEntidadeImputadora - totalRendimentos) > 100))) {
            String linkAnexoD = anexoD != null ? anexoD.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            String linkAnexoI = anexoIModel.getLink("aAnexoI");
            String linkContitulares = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha));
            result.add(new DeclValidationMessage("I105", linkContitulares, new String[]{linkAnexoI, linkAnexoD}));
        }
    }

    public void validateI106(ValidationResult result, AnexoIq07T1_Linha contitular, AnexoIq07T1_Linha falecido, RostoModel rosto, AnexoIModel anexoIModel, AnexoDModel anexoD, Integer numLinha) {
        long rentencaoEntidadeImputadora;
        long retencoesContitular = contitular != null && contitular.getRetencoes() != null ? contitular.getRetencoes() : 0;
        long retencoesFalecido = falecido != null && falecido.getRetencoes() != null ? falecido.getRetencoes() : 0;
        long totalRetencoes = retencoesContitular + retencoesFalecido;
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoD != null ? anexoD.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long l = rentencaoEntidadeImputadora = entidadeImputadora != null && entidadeImputadora.getRetencao() != null ? entidadeImputadora.getRetencao() : 0;
        if (falecido != null && contitular != null && (anexoD == null || entidadeImputadora != null && Math.abs(rentencaoEntidadeImputadora - totalRetencoes) > 100)) {
            String linkNifContitular = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha));
            String linkAnexoDModel = anexoD != null ? anexoD.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            result.add(new DeclValidationMessage("I106", linkNifContitular, new String[]{linkNifContitular, linkAnexoDModel}));
        }
    }

    public void validateI107(ValidationResult result, AnexoIq07T1_Linha current, AnexoDModel anexoD, AnexoIModel anexoIModel, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQuadro03, pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQuadro07, Integer numLinha) {
        long rentencaoEntidadeImputadora;
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoD != null ? anexoD.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long totalRetencao = current.getRetencoes() != null ? current.getRetencoes() : 0;
        long l = rentencaoEntidadeImputadora = entidadeImputadora != null && entidadeImputadora.getRetencao() != null ? entidadeImputadora.getRetencao() : 0;
        if (current.getContitulares() != null && rostoQuadro03.existsInDependentes(current.getContitulares()) && rostoQuadro07.getQ07C1() != null && (anexoD == null || anexoD != null && (entidadeImputadora == null || entidadeImputadora != null && Math.abs(rentencaoEntidadeImputadora - totalRetencao) > 100))) {
            String linkAnexoD = anexoD != null ? anexoD.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            String linkAnexoI = anexoIModel.getLink("aAnexoI");
            String linkContitulares = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha));
            result.add(new DeclValidationMessage("I107", linkContitulares, new String[]{linkAnexoI, linkAnexoD}));
        }
    }

    public void validateI108(ValidationResult result, AnexoIq07T1_Linha current, AnexoDModel anexoD, AnexoIModel anexoIModel, pt.dgci.modelo3irs.v2015.model.rosto.Quadro03 rostoQuadro03, pt.dgci.modelo3irs.v2015.model.rosto.Quadro07 rostoQuadro07, Integer numLinha) {
        boolean existsInDepOrSujPass;
        Long nifAnexoIQ4 = anexoIModel.getQuadro04().getAnexoIq04C05();
        AnexoDq04T1_Linha entidadeImputadora = anexoD != null ? anexoD.getQuadro04().getLinhaByEntidadeImputadora(nifAnexoIQ4) : null;
        long totalRetencao = current.getRetencoes() != null ? current.getRetencoes() : 0;
        long rentencaoEntidadeImputadora = entidadeImputadora != null && entidadeImputadora.getRetencao() != null ? entidadeImputadora.getRetencao() : 0;
        Long contitular = current.getContitulares();
        boolean bl = existsInDepOrSujPass = contitular != null && (rostoQuadro03.existsInDependentes(contitular) || rostoQuadro03.isSujeitoPassivoA(contitular) || rostoQuadro03.isSujeitoPassivoB(contitular) || rostoQuadro03.existsInDependentesEmGuardaConjunta(contitular));
        if (rostoQuadro07.getQ07C1() == null && existsInDepOrSujPass && (anexoD == null || anexoD != null && (entidadeImputadora == null || entidadeImputadora != null && Math.abs(rentencaoEntidadeImputadora - totalRetencao) > 100))) {
            String linkAnexoD = anexoD != null ? anexoD.getLink("aAnexoD.qQuadro04") : "aAnexoD.qQuadro04";
            String linkAnexoI = anexoIModel.getLink("aAnexoI");
            String linkContitulares = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha));
            result.add(new DeclValidationMessage("I108", linkContitulares, new String[]{linkAnexoI, linkAnexoD}));
        }
    }

    public void validateI109(ValidationResult result, RostoModel rosto, AnexoIModel anexoI, AnexoIq07T1_Linha current, int numLinha) {
        if (!(current.getContitulares() == null || current.getParticipacao() == null || current.getParticipacao() != 0 || current.getContitulares().equals(rosto.getQuadro07().getQ07C1()))) {
            String contitularLink = anexoI.getLink(AnexoIq07T1_Linha.getLink(numLinha, AnexoIq07T1_LinhaBase.Property.CONTITULARES));
            result.add(new DeclValidationMessage("I109", contitularLink, new String[]{contitularLink, "aRosto.qQuadro07.fq07C1"}));
        }
    }

    public void validateI111(ValidationResult result, AnexoIq07T1_Linha current, long totalRespeitanteAoFalecido, AnexoIModel anexoIModel, int numLinha) {
        if (anexoIModel != null) {
            long nif = current.getContitulares() != null ? current.getContitulares() : 0;
            long _participacao = current.getParticipacao() != null ? current.getParticipacao() : 0;
            BigDecimal participacao = new BigDecimal(String.valueOf(_participacao)).movePointLeft(4);
            long soma3 = anexoIModel.getQuadro05().getAnexoIq05C1a() != null ? anexoIModel.getQuadro05().getAnexoIq05C1a() : 0;
            BigDecimal parcela1 = new BigDecimal(soma3 - totalRespeitanteAoFalecido).multiply(participacao).setScale(0, RoundingMode.HALF_EVEN);
            long rendimentoComercial = current.getRendimentosComerciais() != null ? current.getRendimentosComerciais() : 0;
            long rendimentoAgricola = current.getRendimentosAgricolas() != null ? current.getRendimentosAgricolas() : 0;
            BigDecimal parcela2 = new BigDecimal(rendimentoAgricola + rendimentoComercial);
            String participacaoLink = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha, AnexoIq07T1_LinhaBase.Property.PARTICIPACAO));
            String rendimentoLink = anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C1a");
            int margem = 100;
            if (nif > 0 && _participacao > 0 && soma3 > 0 && parcela1.subtract(parcela2).longValue() > (long)margem) {
                result.add(new DeclValidationMessage("I111", participacaoLink, new String[]{rendimentoLink, participacaoLink}));
            }
        }
    }

    public void validateI112(ValidationResult result, AnexoIq07T1_Linha current, long totalRespeitanteAoFalecido, AnexoIModel anexoIModel, int numLinha) {
        if (anexoIModel != null) {
            long nif = current.getContitulares() != null ? current.getContitulares() : 0;
            long _participacao = current.getParticipacao() != null ? current.getParticipacao() : 0;
            BigDecimal participacao = new BigDecimal(String.valueOf(_participacao)).movePointLeft(4);
            long prejuizo = anexoIModel.getQuadro06().getAnexoIq06C601() != null ? anexoIModel.getQuadro06().getAnexoIq06C601() : 0;
            long lucro = anexoIModel.getQuadro06().getAnexoIq06C602() != null ? anexoIModel.getQuadro06().getAnexoIq06C602() : 0;
            BigDecimal parcelaPrejuizo = new BigDecimal(prejuizo - totalRespeitanteAoFalecido).multiply(participacao).setScale(0, RoundingMode.HALF_EVEN);
            BigDecimal parcelaLucro = new BigDecimal(lucro - totalRespeitanteAoFalecido).multiply(participacao).setScale(0, RoundingMode.HALF_EVEN);
            long rendimentoComercial = current.getRendimentosComerciais() != null ? Math.abs(current.getRendimentosComerciais()) : 0;
            long rendimentoAgricola = current.getRendimentosAgricolas() != null ? Math.abs(current.getRendimentosAgricolas()) : 0;
            BigDecimal parcela2 = new BigDecimal(rendimentoAgricola + rendimentoComercial);
            String participacaoLink = anexoIModel.getLink(AnexoIq07T1_Linha.getLink(numLinha, AnexoIq07T1_LinhaBase.Property.PARTICIPACAO));
            String rendimentoLink = anexoIModel.getLink("aAnexoI.qQuadro06");
            int margem = 100;
            if (nif > 0 && _participacao > 0 && (prejuizo > 0 && parcelaPrejuizo.subtract(parcela2).longValue() > (long)margem || lucro > 0 && parcelaLucro.subtract(parcela2).longValue() > (long)margem)) {
                result.add(new DeclValidationMessage("I112", participacaoLink, new String[]{rendimentoLink, participacaoLink}));
            }
        }
    }

    public void validateI113(ValidationResult result, RostoModel rostoModel, AnexoIModel anexoIModel) {
        if (anexoIModel != null) {
            long soma3 = anexoIModel.getQuadro05().getAnexoIq05C1a() != null ? anexoIModel.getQuadro05().getAnexoIq05C1a() : 0;
            long totalRendimentoComercial = anexoIModel.getQuadro07().getAnexoIq07C2() != null ? anexoIModel.getQuadro07().getAnexoIq07C2() : 0;
            long totalRendimentoAgricola = anexoIModel.getQuadro07().getAnexoIq07C3() != null ? anexoIModel.getQuadro07().getAnexoIq07C3() : 0;
            int margem = 100;
            boolean condicao2 = Math.abs(totalRendimentoAgricola + totalRendimentoComercial - soma3) > (long)margem;
            Long periodo = rostoModel.getQuadro02().getQ02C02();
            if (soma3 > 0 && condicao2 && periodo != null && periodo > 2009) {
                result.add(new DeclValidationMessage("I113", anexoIModel.getLink("aAnexoI.qQuadro07"), new String[0]));
            }
        }
    }

    public void validateI114(ValidationResult result, AnexoIModel anexoIModel) {
        if (anexoIModel != null) {
            boolean condicao2_lowerBond;
            long prejuizo = anexoIModel.getQuadro06().getAnexoIq06C601() != null ? anexoIModel.getQuadro06().getAnexoIq06C601() : 0;
            long lucro = anexoIModel.getQuadro06().getAnexoIq06C602() != null ? anexoIModel.getQuadro06().getAnexoIq06C602() : 0;
            long totalRendimentoComercial = anexoIModel.getQuadro07().getAnexoIq07C2() != null ? anexoIModel.getQuadro07().getAnexoIq07C2() : 0;
            long totalRendimentoAgricola = anexoIModel.getQuadro07().getAnexoIq07C3() != null ? anexoIModel.getQuadro07().getAnexoIq07C3() : 0;
            int margem = 100;
            boolean condicao1_upperBond = prejuizo > 0 && Math.abs(totalRendimentoAgricola + totalRendimentoComercial) + (long)margem < prejuizo;
            boolean condicao1_lowerBond = prejuizo > 0 && Math.abs(totalRendimentoAgricola + totalRendimentoComercial) - (long)margem > prejuizo;
            boolean condicao2_upperBond = lucro > 0 && Math.abs(totalRendimentoAgricola + totalRendimentoComercial) + (long)margem < lucro;
            boolean bl = condicao2_lowerBond = lucro > 0 && Math.abs(totalRendimentoAgricola + totalRendimentoComercial) - (long)margem > lucro;
            if (condicao1_upperBond || condicao1_lowerBond || condicao2_upperBond || condicao2_lowerBond) {
                result.add(new DeclValidationMessage("I114", anexoIModel.getLink("aAnexoI.qQuadro07"), new String[0]));
            }
        }
    }
}

