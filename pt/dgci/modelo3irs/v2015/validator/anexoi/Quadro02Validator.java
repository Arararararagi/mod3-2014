/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro02Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String I099 = "I099";

    public Quadro02Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateI099(ValidationResult result, RostoModel rosto, Quadro02 quadro02) {
        if (!Modelo3IRSValidatorUtil.equals(quadro02.getAnexoIq02C01(), rosto.getQuadro02().getQ02C02())) {
            result.add(new DeclValidationMessage("I099", "aAnexoI.qQuadro02.fanexoIq02C01", new String[]{"aAnexoI.qQuadro02.fanexoIq02C01", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key : keys) {
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key);
            Quadro02 quadro02AnexoIModel = (Quadro02)anexoIModel.getQuadro(Quadro02.class.getSimpleName());
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            this.validateI099(result, rostoModel, quadro02AnexoIModel);
        }
        return result;
    }
}

