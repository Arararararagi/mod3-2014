/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.AnexoIValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.Quadro06NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.Quadro07NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.Quadro08NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoINETValidator
extends AnexoIValidator {
    public AnexoINETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        result.addAllFrom(new Quadro07NETValidator().validate(model));
        result.addAllFrom(new Quadro08NETValidator().validate(model));
        return result;
    }
}

