/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.Quadro07Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro07NETValidator
extends Quadro07Validator {
    public Quadro07NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key : keys) {
            AnexoBModel anexoB = null;
            AnexoCModel anexoC = null;
            AnexoIModel anexoI = (AnexoIModel)model.getAnexo(key);
            Quadro07 anexoIQ07 = anexoI.getQuadro07();
            String stringNifTitular = anexoI.getStringNifTitular();
            if (stringNifTitular != null) {
                anexoC = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(stringNifTitular);
                anexoB = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(stringNifTitular);
            }
            String rendBrutoLink = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C1");
            EventList<AnexoIq07T1_Linha> linhasQ07 = anexoIQ07.getAnexoIq07T1();
            int nLinha = 0;
            long somaAnexoIQ07RendBrutos = 0;
            for (AnexoIq07T1_Linha linha : linhasQ07) {
                this.validateI081(result, anexoI, linha, ++nLinha);
                this.validateI082(result, anexoI, linha, nLinha);
                if (linha.getRendimentoBruto() == null) continue;
                somaAnexoIQ07RendBrutos+=linha.getRendimentoBruto().longValue();
            }
            if (stringNifTitular == null) continue;
            if (anexoC != null) {
                this.validateI056(result, anexoC, rendBrutoLink, somaAnexoIQ07RendBrutos);
            }
            if (anexoB == null) continue;
            this.validateI054(result, anexoB, rendBrutoLink, somaAnexoIQ07RendBrutos);
        }
        return result;
    }

    protected void validateI056(ValidationResult result, AnexoCModel anexoC, String rendBrutoLink, long somaAnexoIQ07RendBrutos) {
        long somaAnexoCC_1201_e_C_1202_e_C1210 = 0;
        if (anexoC.getQuadro12().getAnexoCq12C1201() != null) {
            somaAnexoCC_1201_e_C_1202_e_C1210+=anexoC.getQuadro12().getAnexoCq12C1201().longValue();
        }
        if (anexoC.getQuadro12().getAnexoCq12C1202() != null) {
            somaAnexoCC_1201_e_C_1202_e_C1210+=anexoC.getQuadro12().getAnexoCq12C1202().longValue();
        }
        if (anexoC.getQuadro12().getAnexoCq12C1210() != null) {
            somaAnexoCC_1201_e_C_1202_e_C1210+=anexoC.getQuadro12().getAnexoCq12C1210().longValue();
        }
        String anexoCC1201Link = anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1201");
        String anexoCC1202Link = anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1202");
        String anexoCC1210Link = anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1210");
        if (somaAnexoIQ07RendBrutos != somaAnexoCC_1201_e_C_1202_e_C1210) {
            result.add(new DeclValidationMessage("I056", rendBrutoLink, new String[]{rendBrutoLink, anexoCC1201Link, anexoCC1202Link, anexoCC1210Link}));
        }
    }
}

