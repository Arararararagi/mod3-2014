/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.Quadro08Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro08NETValidator
extends Quadro08Validator {
    public Quadro08NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        RostoModel rosto = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoIModel anexoI = (AnexoIModel)model.getAnexo(key);
            this.validateI079(result, rosto, anexoI);
        }
        return result;
    }

    private void validateI079(ValidationResult result, RostoModel rosto, AnexoIModel anexoI) {
        long somaValorImpostoImputar = this.calculateSomaQ7ValorImpostoImputar(anexoI);
        long soma_2 = anexoI.getQuadro08().getAnexoIq08C1a() != null ? anexoI.getQuadro08().getAnexoIq08C1a() : 0;
        String somaImpostoLink = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C1a");
        String anexoIQ07SomaImpostoAImputar = anexoI.getLink("aAnexoI.qQuadro07.fanexoIq07C5");
        if (Math.abs(soma_2 - somaValorImpostoImputar) > 500) {
            result.add(new DeclValidationMessage("I079", somaImpostoLink, new String[]{somaImpostoLink, anexoIQ07SomaImpostoAImputar}));
        }
    }

    private long calculateSomaQ7ValorImpostoImputar(AnexoIModel anexoI) {
        long soma = 0;
        for (AnexoIq07T1_Linha linha : anexoI.getQuadro07().getAnexoIq07T1()) {
            if (linha.getValorImposto() == null) continue;
            soma+=linha.getValorImposto().longValue();
        }
        return soma;
    }
}

