/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi.net;

import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro05;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.Quadro05Validator;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro05NETValidator
extends Quadro05Validator {
    public Quadro05NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key : keys) {
            AnexoBModel anexoB = null;
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key);
            Quadro05 anexoIQ04 = anexoIModel.getQuadro05();
            String stringNifTitular = anexoIModel.getStringNifTitular();
            if (stringNifTitular != null) {
                anexoB = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(stringNifTitular);
            }
            this.validateStep2AnexoIQ05(result, anexoB, anexoIModel, anexoIQ04);
        }
        return result;
    }

    private void validateStep2AnexoIQ05(ValidationResult result, AnexoBModel anexoB, AnexoIModel anexoIModel, Quadro05 q05Model) {
        if (anexoB != null) {
            Quadro04 anexoBQ04 = anexoB.getQuadro04();
            String anexoIQ05C501Link = anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C501");
            String anexoIQ05C502Link = anexoIModel.getLink("aAnexoI.qQuadro05.fanexoIq05C502");
            String anexoBQ04C1Link = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C1");
            String anexoBQ04C3Link = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C3");
            this.validateI019(result, anexoIModel, anexoBQ04, anexoIQ05C501Link, anexoBQ04C1Link);
            this.validateI021(result, anexoIModel, anexoBQ04, anexoIQ05C502Link, anexoBQ04C3Link);
        }
    }
}

