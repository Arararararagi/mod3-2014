/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi.net;

import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.Quadro04Validator;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        for (FormKey key : keys) {
            AnexoIModel anexoIModel = (AnexoIModel)model.getAnexo(key);
            Quadro04 q04Model = anexoIModel.getQuadro04();
            this.validateStep2AnexoIQ04(result, anexoIModel, q04Model);
        }
        return result;
    }

    private void validateStep2AnexoIQ04(ValidationResult result, AnexoIModel anexoIModel, Quadro04 q04Model) {
        Long autorHeranca = q04Model.getAnexoIq04C04();
        Long herancaIndivisa = q04Model.getAnexoIq04C05();
        Long nifCabecaDeCasal = q04Model.getAnexoIq04C06();
        String autorHerancaLink = anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C04");
        String herancaIndivisaLink = anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C05");
        String nifCabecaDeCasalLink = anexoIModel.getLink("aAnexoI.qQuadro04.fanexoIq04C06");
        this.validateStep2AnexoIQ04CabecaDeCasal(result, nifCabecaDeCasal, autorHeranca, herancaIndivisa, nifCabecaDeCasalLink, autorHerancaLink, herancaIndivisaLink);
    }

    private void validateStep2AnexoIQ04CabecaDeCasal(ValidationResult result, Long nifCabecaDeCasal, Long autorHeranca, Long herancaIndivisa, String nifCabecaDeCasalLink, String autorHerancaLink, String herancaIndivisaLink) {
    }
}

