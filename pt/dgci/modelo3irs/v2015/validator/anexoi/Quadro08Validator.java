/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoi;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro08Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro08Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateStep1AnexoIQ08(ValidationResult result, RostoModel rosto, AnexoIModel anexoI) {
        long folgaDiferenca = 100;
        Long anoExercicio = rosto.getQuadro02().getQ02C02();
        long f801_1 = anexoI.getQuadro08().getAnexoIq08C801() != null ? anexoI.getQuadro08().getAnexoIq08C801() : 0;
        long f802_1 = anexoI.getQuadro08().getAnexoIq08C802() != null ? anexoI.getQuadro08().getAnexoIq08C802() : 0;
        long f803_1 = anexoI.getQuadro08().getAnexoIq08C803() != null ? anexoI.getQuadro08().getAnexoIq08C803() : 0;
        long f804_1 = anexoI.getQuadro08().getAnexoIq08C804() != null ? anexoI.getQuadro08().getAnexoIq08C804() : 0;
        long f805_1 = anexoI.getQuadro08().getAnexoIq08C805() != null ? anexoI.getQuadro08().getAnexoIq08C805() : 0;
        long f806_1 = anexoI.getQuadro08().getAnexoIq08C806() != null ? anexoI.getQuadro08().getAnexoIq08C806() : 0;
        long f807_1 = anexoI.getQuadro08().getAnexoIq08C807() != null ? anexoI.getQuadro08().getAnexoIq08C807() : 0;
        long f801_2 = anexoI.getQuadro08().getAnexoIq08C801a() != null ? anexoI.getQuadro08().getAnexoIq08C801a() : 0;
        long f802_2 = anexoI.getQuadro08().getAnexoIq08C802a() != null ? anexoI.getQuadro08().getAnexoIq08C802a() : 0;
        long f803_2 = anexoI.getQuadro08().getAnexoIq08C803a() != null ? anexoI.getQuadro08().getAnexoIq08C803a() : 0;
        long f804_2 = anexoI.getQuadro08().getAnexoIq08C804a() != null ? anexoI.getQuadro08().getAnexoIq08C804a() : 0;
        long f805_2 = anexoI.getQuadro08().getAnexoIq08C805a() != null ? anexoI.getQuadro08().getAnexoIq08C805a() : 0;
        long f806_2 = anexoI.getQuadro08().getAnexoIq08C806a() != null ? anexoI.getQuadro08().getAnexoIq08C806a() : 0;
        long f807_2 = anexoI.getQuadro08().getAnexoIq08C807a() != null ? anexoI.getQuadro08().getAnexoIq08C807a() : 0;
        long soma_1 = anexoI.getQuadro08().getAnexoIq08C1() != null ? anexoI.getQuadro08().getAnexoIq08C1() : 0;
        long soma_2 = anexoI.getQuadro08().getAnexoIq08C1a() != null ? anexoI.getQuadro08().getAnexoIq08C1a() : 0;
        String f801_1Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C801");
        String f802_1Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C802");
        String f803_1Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C803");
        String f804_1Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C804");
        String f805_1Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C805");
        String f801_2Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C801a");
        String f802_2Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C802a");
        String f803_2Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C803a");
        String f804_2Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C804a");
        String f805_2Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C805a");
        String soma_1Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C1");
        String soma_2Link = anexoI.getLink("aAnexoI.qQuadro08.fanexoIq08C1a");
        this.validateStep1AnexoIQ08DespesasConfidenciaisImposto(result, f801_1, f801_2, 100, f801_1Link, f801_2Link);
        this.validateStep1AnexoIQ08DespesasRepresentacaoImposto(result, f802_1, f802_2, anoExercicio, 100, f802_1Link, f802_2Link);
        this.validateStep1AnexoIQ08EncargosImpostos(result, f803_1, f803_2, 100, f803_1Link, f803_2Link);
        this.validateStep1AnexoIQ08ImportanciasPagasImpostos(result, f804_1, f804_2, 100, f804_1Link, f804_2Link);
        this.validateStep1AnexoIQ08AjudasDeCustoImposto(result, f805_1, f805_2, 100, f805_1Link, f805_2Link);
        this.validateStep1AnexoIQ08TotalValor(result, anexoI, soma_1, f801_1, f802_1, f803_1, f804_1, f805_1, f806_1, f807_1, soma_1Link);
        this.validateStep1AnexoIQ08TotalImposto(result, anexoI, soma_2, f801_2, f802_2, f803_2, f804_2, f805_2, f806_2, f807_2, soma_2Link);
    }

    private void validateStep1AnexoIQ08DespesasConfidenciaisImposto(ValidationResult result, long f801_1, long f801_2, long folgaDiferenca, String f801_1Link, String f801_2Link) {
        if (Math.abs((double)f801_2 - 0.5 * (double)f801_1) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I071", f801_2Link, new String[]{f801_2Link, f801_1Link}));
        }
    }

    private void validateStep1AnexoIQ08DespesasRepresentacaoImposto(ValidationResult result, long f802_1, long f802_2, Long anoExercicio, long folgaDiferenca, String f802_1Link, String f802_2Link) {
        if (Modelo3IRSValidatorUtil.isValidYear(anoExercicio) && anoExercicio >= 2008 && Math.abs((double)f802_2 - 0.1 * (double)f802_1) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I072", f802_2Link, new String[]{f802_2Link, f802_1Link}));
        }
        if (Modelo3IRSValidatorUtil.isValidYear(anoExercicio) && anoExercicio < 2008 && Math.abs((double)f802_2 - 0.05 * (double)f802_1) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I073", f802_2Link, new String[]{f802_2Link, f802_1Link}));
        }
    }

    private void validateStep1AnexoIQ08EncargosImpostos(ValidationResult result, long f803_1, long f803_2, long folgaDiferenca, String f803_1Link, String f803_2Link) {
        if (Math.abs((double)f803_2 - 0.05 * (double)f803_1) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I074", f803_2Link, new String[]{f803_2Link, f803_1Link}));
        }
    }

    protected void validateI132(ValidationResult result, AnexoIModel anexoIModel) {
        long folgaDiferenca = 100;
        Long C806a = anexoIModel.getQuadro08().getAnexoIq08C806a();
        long C806 = anexoIModel.getQuadro08().getAnexoIq08C806() != null ? anexoIModel.getQuadro08().getAnexoIq08C806() : 0;
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if (C806a != null && ano != null && ano > 2013 && Math.abs((double)C806a.longValue() - 0.1 * (double)C806) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I132", anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C806a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C806a"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C806")}));
        }
    }

    protected void validateI133(ValidationResult result, AnexoIModel anexoIModel) {
        long folgaDiferenca = 100;
        Long C807a = anexoIModel.getQuadro08().getAnexoIq08C807a();
        long C807 = anexoIModel.getQuadro08().getAnexoIq08C807() != null ? anexoIModel.getQuadro08().getAnexoIq08C807() : 0;
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if (C807a != null && ano != null && ano > 2013 && Math.abs((double)C807a.longValue() - 0.2 * (double)C807) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I133", anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C807a"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C807a"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C807")}));
        }
    }

    private void validateStep1AnexoIQ08ImportanciasPagasImpostos(ValidationResult result, long f804_1, long f804_2, long folgaDiferenca, String f804_1Link, String f804_2Link) {
        if (Math.abs((double)f804_2 - 0.35 * (double)f804_1) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I075", f804_2Link, new String[]{f804_2Link, f804_1Link}));
        }
    }

    private void validateStep1AnexoIQ08AjudasDeCustoImposto(ValidationResult result, long f805_1, long f805_2, long folgaDiferenca, String f805_1Link, String f805_2Link) {
        if (Math.abs((double)f805_2 - 0.05 * (double)f805_1) > (double)folgaDiferenca) {
            result.add(new DeclValidationMessage("I076", f805_2Link, new String[]{f805_2Link, f805_1Link}));
        }
    }

    protected void validateStep1AnexoIQ08TotalValor(ValidationResult result, AnexoIModel anexoI, long soma_1, long f801_1, long f802_1, long f803_1, long f804_1, long f805_1, long f806_1, long f807_1, String soma_1Link) {
        if (soma_1 != f801_1 + f802_1 + f803_1 + f804_1 + f805_1 + f806_1 + f807_1) {
            result.add(new DeclValidationMessage("I077", soma_1Link, new String[]{soma_1Link}));
        }
    }

    protected void validateStep1AnexoIQ08TotalImposto(ValidationResult result, AnexoIModel anexoI, long soma_2, long f801_2, long f802_2, long f803_2, long f804_2, long f805_2, long f806_2, long f807_2, String soma_2Link) {
        if (Math.abs(soma_2 - (f801_2 + f802_2 + f803_2 + f804_2 + f805_2 + f806_2 + f807_2)) > 500) {
            result.add(new DeclValidationMessage("I078", soma_2Link, new String[]{soma_2Link}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoI");
        RostoModel rosto = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoIModel anexoI = (AnexoIModel)model.getAnexo(key);
            this.validateStep1AnexoIQ08(result, rosto, anexoI);
            this.validateI132(result, anexoI);
            this.validateI133(result, anexoI);
            this.validateI134(result, anexoI);
            this.validateI135(result, anexoI);
        }
        return result;
    }

    protected void validateI134(ValidationResult result, AnexoIModel anexoIModel) {
        Long C802 = anexoIModel.getQuadro08().getAnexoIq08C802();
        Long C802a = anexoIModel.getQuadro08().getAnexoIq08C802a();
        Long C803 = anexoIModel.getQuadro08().getAnexoIq08C803();
        Long C803a = anexoIModel.getQuadro08().getAnexoIq08C803a();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if ((C802 != null && C802 > 0 || C802a != null && C802a > 0 || C803 != null && C803 > 0 || C803a != null && C803a > 0) && ano != null && ano > 2013) {
            result.add(new DeclValidationMessage("I134", anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C802"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C802"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C802a"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C803"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C803a"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateI135(ValidationResult result, AnexoIModel anexoIModel) {
        Long C806 = anexoIModel.getQuadro08().getAnexoIq08C806();
        Long C806a = anexoIModel.getQuadro08().getAnexoIq08C806a();
        Long C807 = anexoIModel.getQuadro08().getAnexoIq08C807();
        Long C807a = anexoIModel.getQuadro08().getAnexoIq08C807a();
        Long ano = anexoIModel.getQuadro02().getAnexoIq02C01();
        if ((C806 != null && C806 > 0 || C806a != null && C806a > 0 || C807 != null && C807 > 0 || C807a != null && C807a > 0) && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("I135", anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C806"), new String[]{anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C806"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C806a"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C807"), anexoIModel.getLink("aAnexoI.qQuadro08.fanexoIq08C807a"), "aRosto.qQuadro02.fq02C02"}));
        }
    }
}

