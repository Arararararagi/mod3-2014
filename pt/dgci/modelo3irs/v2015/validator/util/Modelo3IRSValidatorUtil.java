/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.util;

import pt.opensoft.field.NifValidator;
import pt.opensoft.util.CharUtil;
import pt.opensoft.util.Year;

public class Modelo3IRSValidatorUtil {
    public static /* varargs */ boolean startsWith(long nif, String ... values) {
        return Modelo3IRSValidatorUtil.startsWith(String.valueOf(nif), values);
    }

    public static /* varargs */ boolean startsWith(String nif, String ... values) {
        for (String nifStartingChars : values) {
            if (!nif.startsWith(nifStartingChars)) continue;
            return true;
        }
        return false;
    }

    public static boolean isNIFValid(long nif) {
        return nif != 123456789 && NifValidator.validate(nif);
    }

    public static boolean equals(Long num1, Long num2) {
        return num1 == null && num2 == null || num1 != null && num2 != null && num1.equals(num2);
    }

    public static boolean equals(String string1, String string2) {
        return string1 == null && string2 == null || string1 != null && string2 != null && string1.equals(string2);
    }

    public static boolean in(long value, long[] values) {
        if (values == null || values.length == 0) {
            return false;
        }
        for (int i = 0; i < values.length; ++i) {
            long valueInArray = values[i];
            if (value != valueInArray) continue;
            return true;
        }
        return false;
    }

    public static boolean in(String value, String[] values) {
        if (values == null || values.length == 0) {
            return false;
        }
        for (int i = 0; i < values.length; ++i) {
            String valueInArray = values[i];
            if ((value != null || valueInArray != null) && (value == null || valueInArray == null || !value.equals(valueInArray))) continue;
            return true;
        }
        return false;
    }

    public static boolean isEmptyOrZero(Long value) {
        return value == null || value == 0;
    }

    public static boolean isGreaterThanZero(Long value) {
        return !Modelo3IRSValidatorUtil.isEmptyOrZero(value) && value > 0;
    }

    public static boolean isAlphaNumericOrSpace(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isLetterOrDigit(ch) || ch == ' ') continue;
            return false;
        }
        return true;
    }

    public static boolean isValidYear(Long year) {
        return year != null && Year.isYear(String.valueOf(year)) && year > 1900;
    }

    public static long getMaxFromValues(Long num1, Long num2) {
        if (num1 == null && num2 == null) {
            return 0;
        }
        if (num1 == null) {
            return num2;
        }
        if (num2 == null) {
            return num1;
        }
        return Math.max(num1, num2);
    }

    public static long getMaxFromValuesIgnoreZero(Long num1, Long num2) {
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(num1) && Modelo3IRSValidatorUtil.isEmptyOrZero(num2)) {
            return 0;
        }
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(num1)) {
            return num2;
        }
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(num2)) {
            return num1;
        }
        return Math.max(num1, num2);
    }

    public static /* varargs */ Long sum(Long ... values) {
        Long result = null;
        for (Long value : values) {
            if (value == null) continue;
            result = result == null ? value : Long.valueOf(result + value);
        }
        return result;
    }

    public static /* varargs */ long sumValue(Long ... values) {
        Long result = Modelo3IRSValidatorUtil.sum(values);
        if (result == null) {
            return 0;
        }
        return result;
    }

    public static boolean isZeroString(String value) {
        for (int i = 0; i < value.length(); ++i) {
            if (value.charAt(i) == '0') continue;
            return false;
        }
        return true;
    }

    public static int getRowNumberFromTitular(String titular, String labelPrefix) {
        String value;
        int rowNumber = -1;
        if (titular != null && labelPrefix != null && titular.trim().length() >= labelPrefix.length() && (value = titular.trim().substring(labelPrefix.length())) != null && value.length() > 0) {
            rowNumber = Integer.parseInt(value);
        }
        return rowNumber;
    }
}

