/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoA;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    protected Quadro03Validator(AmbitosValidator.AmbitoIRS ambitoIRS) {
        super(ambitoIRS);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        Quadro03 quadro03AnexoAModel = (Quadro03)anexoAModel.getQuadro(Quadro03.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (!Modelo3IRSValidatorUtil.equals(quadro03AnexoAModel.getAnexoAq03C02(), rostoModel.getQuadro03().getQ03C03())) {
            result.add(new DeclValidationMessage("A002", "aAnexoA.qQuadro03.fanexoAq03C02", new String[]{"aAnexoA.qQuadro03.fanexoAq03C02", "aRosto.qQuadro03.fq03C03"}));
        }
        if (!Modelo3IRSValidatorUtil.equals(quadro03AnexoAModel.getAnexoAq03C03(), rostoModel.getQuadro03().getQ03C04())) {
            result.add(new DeclValidationMessage("A004", "aAnexoA.qQuadro03.fanexoAq03C03", new String[]{"aAnexoA.qQuadro03.fanexoAq03C03", "aRosto.qQuadro03.fq03C04"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

