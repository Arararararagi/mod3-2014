/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoA.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoA.Quadro04Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        Quadro04 quadro04AnexoAModel = (Quadro04)anexoAModel.getQuadro(Quadro04.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        result.addAllFrom(this.validaNETT4A(rostoModel, quadro04AnexoAModel));
        return result;
    }

    protected ValidationResult validaNETT4A(RostoModel rostoModel, Quadro04 quadro04AnexoAModel) {
        ValidationResult result = new ValidationResult();
        for (int linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = quadro04AnexoAModel.getAnexoAq04T4A().get(linha);
            this.validateA105(result, anexoAq04T4ALinha, linha + 1, rostoModel);
        }
        return result;
    }

    protected void validateA105(ValidationResult result, AnexoAq04T4A_Linha linha, int numLinha, RostoModel rosto) {
        boolean condicao2;
        Long codRendimentos = linha.getCodRendimentos();
        Long retencoes = linha.getRetencoes();
        Long sobretaxa = linha.getRetSobretaxa();
        Long nif = linha.getNIF();
        long ano2011 = 2011;
        long ano2012 = 2012;
        boolean condicao1 = retencoes != null && retencoes > 0 && nif == null;
        boolean bl = condicao2 = nif == null && sobretaxa != null && sobretaxa > 0 && rosto.getQuadro02().getQ02C02() != null && (rosto.getQuadro02().getQ02C02() == ano2011 || rosto.getQuadro02().getQ02C02() > ano2012);
        if (codRendimentos != null && codRendimentos == 401 && (condicao1 || condicao2)) {
            String codRendimentosLink = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS);
            String linkNif = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.NIF);
            result.add(new DeclValidationMessage("A105", codRendimentosLink, new String[]{linkNif}));
        }
    }
}

