/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoA;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_Rosto;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.Date;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final long ANO_2014 = 2014;
    private static final long ANO_2013 = 2013;
    private static final long ANO_2012 = 2012;
    private static final int LINE_PAD = 1;
    private Long anoRosto;
    private static final int MAX_LINHAS_TABELA_4BA = 16;

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel = anexoAModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        this.anoRosto = rostoModel.getQuadro02().getQ02C02();
        result.addAllFrom(this.validaT4A(rostoModel, quadro04AnexoAModel));
        result.addAllFrom(this.validaT4B(rostoModel, quadro04AnexoAModel, model));
        result.addAllFrom(this.validaT4Ba(quadro04AnexoAModel));
        return result;
    }

    protected ValidationResult validaT4A(RostoModel rostoModel, pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel) {
        long anexoAq04C3Value;
        long anexoAq04C4Value;
        long anexoAq04C2Value;
        long anexoAq04C1Value;
        ValidationResult result = new ValidationResult();
        if (quadro04AnexoAModel.getAnexoAq04T4A().size() > 100) {
            result.add(new DeclValidationMessage("A006", "aAnexoA.qQuadro04.tanexoAq04T4A", new String[]{"aAnexoA.qQuadro04.tanexoAq04T4A"}));
        }
        this.validateA087(result, quadro04AnexoAModel, rostoModel);
        this.validateA134(result, quadro04AnexoAModel, rostoModel);
        this.validateA007(result, quadro04AnexoAModel);
        long somaControloRendimentos = 0;
        long somaControloRetencoes = 0;
        long somaControloContribuicoes = 0;
        long somaControloSobtretaxa = 0;
        for (int linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = quadro04AnexoAModel.getAnexoAq04T4A().get(linha);
            String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + (linha + 1);
            String linkNif = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.NIF.getIndex();
            String linkCodRendimentos = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.TITULAR.getIndex();
            String linkRendimentos = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.RENDIMENTOS.getIndex();
            String linkRetencoes = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.RETENCOES.getIndex();
            String linkContribuicoes = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.CONTRIBUICOES.getIndex();
            String linkDataContratoPreReforma = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.DATACONTRATOPREREFORMA.getIndex();
            String linkDataPrimeiroPagamento = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.DATAPRIMEIROPAGAMENTO.getIndex();
            Long nif = anexoAq04T4ALinha.getNIF();
            Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
            String titular = anexoAq04T4ALinha.getTitular();
            Long rendimentos = anexoAq04T4ALinha.getRendimentos();
            Long retencoes = anexoAq04T4ALinha.getRetencoes();
            Long contribuicoes = anexoAq04T4ALinha.getContribuicoes();
            Long sobretaxa = anexoAq04T4ALinha.getRetSobretaxa();
            Date dataContratoPreReforma = anexoAq04T4ALinha.getDataContratoPreReforma();
            Date dataPrimeiroPagamento = anexoAq04T4ALinha.getDataPrimeiroPagamento();
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && Modelo3IRSValidatorUtil.isEmptyOrZero(codRendimentos) && StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencoes) && Modelo3IRSValidatorUtil.isEmptyOrZero(contribuicoes) && Modelo3IRSValidatorUtil.isEmptyOrZero(sobretaxa)) {
                result.add(new DeclValidationMessage("A008", linkCurrentLine, new String[]{linkNif}));
            }
            if (!(nif == null || Modelo3IRSValidatorUtil.startsWith(nif, "1", "2", "3", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("A009", linkNif, new String[]{linkNif}));
            }
            if (!(nif == null || NifValidator.validate(nif))) {
                result.add(new DeclValidationMessage("A010", linkNif, new String[]{linkNif}));
            }
            this.validateA011(result, rostoModel, linkNif, nif);
            if (!(codRendimentos == null || CatalogManager.getInstance().getCatalog("Cat_M3V2015_AnexoAQ4A").containsKey(codRendimentos))) {
                result.add(new DeclValidationMessage("A014", linkCodRendimentos, new String[]{linkCodRendimentos}));
            }
            this.validateA098(result, anexoAq04T4ALinha, linha + 1);
            if (codRendimentos != null && codRendimentos == 403 && this.anoRosto != null && this.anoRosto >= 2007) {
                result.add(new DeclValidationMessage("A071", linkCodRendimentos, new String[]{linkCodRendimentos}));
            }
            if (codRendimentos != null && codRendimentos == 403 && retencoes != null && rendimentos != null && (double)retencoes.longValue() > (double)rendimentos.longValue() * 0.25 && this.anoRosto != null && this.anoRosto < 2007) {
                result.add(new DeclValidationMessage("A018", linkCodRendimentos, new String[]{linkRetencoes}));
            }
            if (codRendimentos != null && codRendimentos == 405 && this.anoRosto != null && this.anoRosto < 2007) {
                result.add(new DeclValidationMessage("A088", linkCodRendimentos, new String[]{linkCodRendimentos}));
            }
            if (codRendimentos != null && codRendimentos == 406 && (retencoes != null && retencoes > 0 || contribuicoes != null && contribuicoes > 0)) {
                result.add(new DeclValidationMessage("A072", linkCodRendimentos, new String[]{linkCodRendimentos, linkRetencoes, linkContribuicoes}));
            }
            if (codRendimentos != null && codRendimentos == 408 && titular != null && (dataContratoPreReforma == null || dataPrimeiroPagamento == null)) {
                result.add(new DeclValidationMessage("A019", linkTitular, new String[]{linkDataContratoPreReforma, linkDataPrimeiroPagamento}));
            }
            this.validateA119(result, anexoAq04T4ALinha, linha + 1);
            if (dataPrimeiroPagamento != null && dataContratoPreReforma != null && dataPrimeiroPagamento.before(dataContratoPreReforma)) {
                result.add(new DeclValidationMessage("A038", linkDataPrimeiroPagamento, new String[]{linkDataPrimeiroPagamento, linkDataContratoPreReforma}));
            }
            if (codRendimentos != null && !Modelo3IRSValidatorUtil.in(codRendimentos, new long[]{401}) && (dataContratoPreReforma != null && dataContratoPreReforma.after(new Date("2000-12-31")) || dataPrimeiroPagamento != null && dataPrimeiroPagamento.after(new Date("2000-12-31")))) {
                result.add(new DeclValidationMessage("A080", linkCurrentLine));
            }
            if ((rendimentos != null || retencoes != null || contribuicoes != null || dataContratoPreReforma != null || dataPrimeiroPagamento != null) && nif == null && codRendimentos == null && titular == null) {
                result.add(new DeclValidationMessage("A029", "aAnexoA.qQuadro04.tanexoAq04T4A", new String[]{linkNif, linkTitular, linkCodRendimentos}));
            }
            if ((dataContratoPreReforma != null || dataPrimeiroPagamento != null) && (codRendimentos != null && codRendimentos != 408 || codRendimentos == null)) {
                result.add(new DeclValidationMessage("A113", "aAnexoA.qQuadro04.tanexoAq04T4A", new String[]{linkDataPrimeiroPagamento, linkDataContratoPreReforma, linkCodRendimentos}));
            }
            this.validateA122(result, anexoAq04T4ALinha, linha + 1, this.anoRosto);
            this.validateA023(result, linkTitular, titular);
            this.validateA024(result, rostoModel, titular, linkTitular);
            this.validateA070(result, anexoAq04T4ALinha, this.anoRosto, linkRendimentos);
            this.validateA086(result, anexoAq04T4ALinha, linha + 1);
            this.validateA120(result, anexoAq04T4ALinha, this.anoRosto, linha + 1);
            this.validateA127(result, anexoAq04T4ALinha, linkRendimentos);
            this.validateA128(result, anexoAq04T4ALinha, linkNif, linkRendimentos);
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos)) {
                somaControloRendimentos+=rendimentos.longValue();
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(retencoes)) {
                somaControloRetencoes+=retencoes.longValue();
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(contribuicoes)) {
                somaControloContribuicoes+=contribuicoes.longValue();
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(sobretaxa)) continue;
            somaControloSobtretaxa+=sobretaxa.longValue();
        }
        long l = anexoAq04C1Value = quadro04AnexoAModel.getAnexoAq04C1() != null ? quadro04AnexoAModel.getAnexoAq04C1() : 0;
        if (anexoAq04C1Value != somaControloRendimentos) {
            result.add(new DeclValidationMessage("YA001", "aAnexoA.qQuadro04.fanexoAq04C1", new String[]{"aAnexoA.qQuadro04.fanexoAq04C1"}));
        }
        long l2 = anexoAq04C2Value = quadro04AnexoAModel.getAnexoAq04C2() != null ? quadro04AnexoAModel.getAnexoAq04C2() : 0;
        if (anexoAq04C2Value != somaControloRetencoes) {
            result.add(new DeclValidationMessage("YA002", "aAnexoA.qQuadro04.fanexoAq04C2", new String[]{"aAnexoA.qQuadro04.fanexoAq04C2"}));
        }
        long l3 = anexoAq04C3Value = quadro04AnexoAModel.getAnexoAq04C3() != null ? quadro04AnexoAModel.getAnexoAq04C3() : 0;
        if (anexoAq04C3Value != somaControloContribuicoes) {
            result.add(new DeclValidationMessage("YA003", "aAnexoA.qQuadro04.fanexoAq04C3", new String[]{"aAnexoA.qQuadro04.fanexoAq04C3"}));
        }
        long l4 = anexoAq04C4Value = quadro04AnexoAModel.getAnexoAq04C4() != null ? quadro04AnexoAModel.getAnexoAq04C4() : 0;
        if (anexoAq04C4Value != somaControloSobtretaxa) {
            result.add(new DeclValidationMessage("YA004", "aAnexoA.qQuadro04.fanexoAq04C4", new String[]{"aAnexoA.qQuadro04.fanexoAq04C4"}));
        }
        return result;
    }

    protected void validateA017(ValidationResult result, RostoModel rostoModel, AnexoAq04T4A_Linha linha, int numLinha) {
        boolean parte2;
        Long codRendimentos = linha.getCodRendimentos();
        Long retencoes = linha.getRetencoes();
        Long rendimentos = linha.getRendimentos();
        Long periodo = rostoModel.getQuadro02().getQ02C02();
        boolean parte1 = codRendimentos != null && rendimentos != null && retencoes != null && Modelo3IRSValidatorUtil.in(codRendimentos, new long[]{402}) && retencoes > Math.round((double)rendimentos.longValue() * 0.405);
        boolean bl = parte2 = codRendimentos != null && rendimentos != null && retencoes != null && periodo != null && Modelo3IRSValidatorUtil.in(codRendimentos, new long[]{405, 407, 404}) && (retencoes > Math.round((double)rendimentos.longValue() * 0.405) && periodo < 2013 || retencoes > Math.round((double)rendimentos.longValue() * 0.465) && periodo > 2012);
        if (parte1 || parte2) {
            String linkCodRendimentos = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS);
            String linkRetencoes = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.RETENCOES);
            result.add(new DeclValidationMessage("A017", linkCodRendimentos, new String[]{linkRetencoes}));
        }
    }

    protected void validateA086(ValidationResult result, AnexoAq04T4A_Linha linha, int numLinhas) {
        boolean isNifValid;
        Long nif = linha.getNIF();
        Long codRendimentos = linha.getCodRendimentos();
        Long contribuicoes = linha.getContribuicoes();
        String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + numLinhas;
        String linkCodRendimentos = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkContribuicoes = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.CONTRIBUICOES.getIndex();
        boolean bl = isNifValid = nif == null || nif != 600008878 && nif != 600006662;
        if (codRendimentos != null && contribuicoes != null && (codRendimentos == 407 || codRendimentos == 402 && isNifValid) && contribuicoes > 0) {
            result.add(new DeclValidationMessage("A086", linkCodRendimentos, new String[]{linkCodRendimentos, linkContribuicoes}));
        }
    }

    protected void validateA087(ValidationResult result, pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel, RostoModel rostoModel) {
        long ano2007 = 2007;
        long ano2010 = 2010;
        long ano2011 = 2011;
        long ano2012 = 2012;
        long ano2013 = 2013;
        double percent20 = 0.2;
        double percent50 = 0.505;
        long codRendimento404 = 404;
        long codRendimento405 = 405;
        if (!(rostoModel == null || rostoModel.getQuadro02().getQ02C02() == null || quadro04AnexoAModel == null || quadro04AnexoAModel.getAnexoAq04T4A() == null || quadro04AnexoAModel.getAnexoAq04T4A().isEmpty())) {
            long anoRendimentos = rostoModel.getQuadro02().getQ02C02();
            for (int i = 0; i < quadro04AnexoAModel.getAnexoAq04T4A().size(); ++i) {
                AnexoAq04T4A_Linha linha = quadro04AnexoAModel.getAnexoAq04T4A().get(i);
                long contribuicoes = linha.getContribuicoes() != null ? linha.getContribuicoes() : 0;
                long rendimentos = linha.getRendimentos() != null ? linha.getRendimentos() : 0;
                Long nifEntidadePagadora = linha.getNIF();
                if ((linha.getCodRendimentos() == null || !linha.getCodRendimentos().equals(404) || (anoRendimentos >= 2007 || contribuicoes <= (long)(0.2 * (double)rendimentos)) && (anoRendimentos < 2007 || anoRendimentos >= 2011 || contribuicoes <= (long)(0.2 * (double)rendimentos) || nifEntidadePagadora != null && Modelo3IRSValidatorUtil.in(nifEntidadePagadora, new long[]{503509027, 500077568, 500745439, 501449752})) && (anoRendimentos <= 2010 || anoRendimentos >= 2012 || contribuicoes <= (long)(0.2 * (double)rendimentos) || nifEntidadePagadora != null && Modelo3IRSValidatorUtil.in(nifEntidadePagadora, new long[]{500745439, 501449752})) && (anoRendimentos <= 2011 || anoRendimentos >= 2013 || contribuicoes <= (long)(0.2 * (double)rendimentos) || nifEntidadePagadora != null && Modelo3IRSValidatorUtil.in(nifEntidadePagadora, new long[]{500792968, 505305500, 500792771})) && (anoRendimentos <= 2011 || anoRendimentos >= 2013 || contribuicoes <= (long)(0.505 * (double)rendimentos))) && (linha.getCodRendimentos() == null || !linha.getCodRendimentos().equals(405) || (anoRendimentos >= 2007 || contribuicoes <= (long)(0.2 * (double)rendimentos)) && (anoRendimentos < 2007 || anoRendimentos >= 2011 || contribuicoes <= (long)(0.2 * (double)rendimentos) || nifEntidadePagadora != null && Modelo3IRSValidatorUtil.in(nifEntidadePagadora, new long[]{503509027, 500077568, 500745439, 501449752})) && (anoRendimentos <= 2010 || anoRendimentos >= 2013 || contribuicoes <= (long)(0.2 * (double)rendimentos) || nifEntidadePagadora != null && Modelo3IRSValidatorUtil.in(nifEntidadePagadora, new long[]{500745439, 501449752})))) continue;
                result.add(new DeclValidationMessage("A087", AnexoAq04T4A_Linha.getLink(i + 1, AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS), new String[]{AnexoAq04T4A_Linha.getLink(i + 1, AnexoAq04T4A_LinhaBase.Property.CONTRIBUICOES)}));
            }
        }
    }

    protected void validateA134(ValidationResult result, pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel, RostoModel rostoModel) {
        long ano2012 = 2012;
        double percent50 = 0.505;
        long[] codigosRendimentos = new long[]{404, 405};
        if (!(rostoModel == null || rostoModel.getQuadro02().getQ02C02() == null || quadro04AnexoAModel == null || quadro04AnexoAModel.getAnexoAq04T4A() == null || quadro04AnexoAModel.getAnexoAq04T4A().isEmpty())) {
            long anoRendimentos = rostoModel.getQuadro02().getQ02C02();
            for (int i = 0; i < quadro04AnexoAModel.getAnexoAq04T4A().size(); ++i) {
                long contribuicoes;
                AnexoAq04T4A_Linha linha = quadro04AnexoAModel.getAnexoAq04T4A().get(i);
                long codigoRendimentos = linha.getCodRendimentos() != null ? linha.getCodRendimentos() : 0;
                long rendimentos = linha.getRendimentos() != null ? linha.getRendimentos() : 0;
                long l = contribuicoes = linha.getContribuicoes() != null ? linha.getContribuicoes() : 0;
                if (!Modelo3IRSValidatorUtil.in(codigoRendimentos, codigosRendimentos) || anoRendimentos <= 2012 || contribuicoes <= (long)(0.505 * (double)rendimentos)) continue;
                result.add(new DeclValidationMessage("A134", AnexoAq04T4A_Linha.getLink(i + 1, AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS), new String[]{AnexoAq04T4A_Linha.getLink(i + 1, AnexoAq04T4A_LinhaBase.Property.CONTRIBUICOES)}));
            }
        }
    }

    protected void validateA007(ValidationResult result, pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04) {
        if (!(quadro04 == null || quadro04.getAnexoAq04T4A() == null || quadro04.getAnexoAq04T4A().isEmpty())) {
            boolean linePad = true;
            for (int i = 0; i < quadro04.getAnexoAq04T4A().size(); ++i) {
                AnexoAq04T4A_Linha linha = quadro04.getAnexoAq04T4A().get(i);
                if (linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCodRendimentos()) && StringUtil.isEmpty(linha.getTitular()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentos()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getContribuicoes()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetSobretaxa()) || !this.isLinhaAq04T4ARepetida(linha, quadro04.getAnexoAq04T4A(), i)) continue;
                result.add(new DeclValidationMessage("A007", AnexoAq04T4A_Linha.getLink(i + 1), new String[]{AnexoAq04T4A_Linha.getLink(i + 1, AnexoAq04T4A_LinhaBase.Property.NIF), AnexoAq04T4A_Linha.getLink(i + 1, AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS), AnexoAq04T4A_Linha.getLink(i + 1, AnexoAq04T4A_LinhaBase.Property.TITULAR)}));
            }
        }
    }

    private boolean isLinhaAq04T4ARepetida(AnexoAq04T4A_Linha linha, List<AnexoAq04T4A_Linha> tabela, int index) {
        boolean isDuplicada = false;
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCodRendimentos()) && StringUtil.isEmpty(linha.getTitular()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentos()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getContribuicoes()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetSobretaxa()) || tabela == null || tabela.isEmpty())) {
            int searchToIndex = index < tabela.size() ? index : tabela.size();
            for (int i = 0; i < searchToIndex; ++i) {
                AnexoAq04T4A_Linha anexoAq04T4A_Linha = tabela.get(i);
                if (anexoAq04T4A_Linha == null || !Modelo3IRSValidatorUtil.equals(anexoAq04T4A_Linha.getNIF(), linha.getNIF()) || !Modelo3IRSValidatorUtil.equals(anexoAq04T4A_Linha.getCodRendimentos(), linha.getCodRendimentos()) || !Modelo3IRSValidatorUtil.equals(anexoAq04T4A_Linha.getTitular(), linha.getTitular())) continue;
                isDuplicada = true;
            }
        }
        return isDuplicada;
    }

    protected void validateA127(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, String linkRendimentos) {
        Long nif = anexoAq04T4ALinha.getNIF();
        Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
        String titular = anexoAq04T4ALinha.getTitular();
        Long rendimentos = anexoAq04T4ALinha.getRendimentos();
        Long retencoes = anexoAq04T4ALinha.getRetencoes();
        Long contribuicoes = anexoAq04T4ALinha.getContribuicoes();
        Long retencoesSobretaxa = anexoAq04T4ALinha.getRetSobretaxa();
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos) && codRendimentos != null && codRendimentos.equals(401) && !StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencoes) && Modelo3IRSValidatorUtil.isEmptyOrZero(contribuicoes) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencoesSobretaxa)) {
            result.add(new DeclValidationMessage("A127", linkRendimentos, new String[]{linkRendimentos}));
        }
    }

    protected void validateA128(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, String linkNif, String linkRendimentos) {
        Long rendimentos = anexoAq04T4ALinha.getRendimentos();
        Long nif = anexoAq04T4ALinha.getNIF();
        Long contribuicoes = anexoAq04T4ALinha.getContribuicoes();
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos) && !Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && Modelo3IRSValidatorUtil.in(nif, new long[]{503509027, 500077568, 500745439, 501449752}) && Modelo3IRSValidatorUtil.isEmptyOrZero(contribuicoes)) {
            result.add(new DeclValidationMessage("A128", linkRendimentos, new String[]{linkNif, linkRendimentos}));
        }
    }

    protected void validateA024(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("A024", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    protected void validateA023(ValidationResult result, String linkTitular, String titular) {
        if (!(titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("A023", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateA011(ValidationResult result, RostoModel rostoModel, String linkNif, Long nif) {
        Quadro03 quadro03RostoModel = rostoModel.getQuadro03();
        Quadro07 quadro07RostoModel = rostoModel.getQuadro07();
        if (nif != null && (quadro03RostoModel.isSujeitoPassivoA(nif) || quadro03RostoModel.isSujeitoPassivoB(nif) || quadro03RostoModel.existsInDependentes(nif) || quadro07RostoModel.isConjugeFalecido(nif) || quadro07RostoModel.existsInAscendentes(nif) || quadro07RostoModel.existsInAfilhadosCivisEmComunhao(nif) || quadro03RostoModel.existsInDependentesEmGuardaConjunta(nif) || quadro07RostoModel.existsInAscendentesEColaterais(nif))) {
            result.add(new DeclValidationMessage("A011", linkNif, new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected void validateA070(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, Long anoExercicio, String linkRendimentos) {
        Long rendimentos = anexoAq04T4ALinha.getRendimentos();
        Long nif = anexoAq04T4ALinha.getNIF();
        Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
        if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos) || anoExercicio == null || anoExercicio <= 2008 || (Modelo3IRSValidatorUtil.isEmptyOrZero(nif) || codRendimentos != null && codRendimentos.equals(404)) && (Modelo3IRSValidatorUtil.isEmptyOrZero(nif) || nif.equals(501449752) || nif.equals(500792968) || codRendimentos == null || !codRendimentos.equals(404)))) {
            result.add(new DeclValidationMessage("A070", linkRendimentos, new String[]{linkRendimentos}));
        }
    }

    protected void validateA098(ValidationResult result, AnexoAq04T4A_Linha linha, int numLinha) {
        if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCodRendimentos()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) && StringUtil.isEmpty(linha.getTitular()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentos()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getContribuicoes()) && (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetSobretaxa()) || linha.getRetSobretaxa() <= 0))) {
            String linkCodRendimentos = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS);
            result.add(new DeclValidationMessage("A098", linkCodRendimentos, new String[]{linkCodRendimentos}));
        }
    }

    protected ValidationResult validaT4B(RostoModel rostoModel, pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel, DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        if (quadro04AnexoAModel.getAnexoAq04T4B().size() > 16) {
            result.add(new DeclValidationMessage("A039", "aAnexoA.qQuadro04.tanexoAq04T4B", new String[]{"aAnexoA.qQuadro04.tanexoAq04T4B"}));
        }
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        for (int linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4B().size(); ++linha) {
            AnexoAq04T4B_Linha anexoAq04T4BLinha = quadro04AnexoAModel.getAnexoAq04T4B().get(linha);
            String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4B.l" + (linha + 1);
            String linkCodDespesa = linkCurrentLine + ".c" + AnexoAq04T4B_LinhaBase.Property.CODDESPESA.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoAq04T4B_LinhaBase.Property.TITULAR.getIndex();
            String linkValor = linkCurrentLine + ".c" + AnexoAq04T4B_LinhaBase.Property.VALOR.getIndex();
            Long codDespesa = anexoAq04T4BLinha.getCodDespesa();
            String titular = anexoAq04T4BLinha.getTitular();
            Long valor = anexoAq04T4BLinha.getValor();
            if (codDespesa != null && titular != null) {
                Tuple tuple = new Tuple(new Object[]{codDespesa, titular});
                if (processedTuples.contains(tuple)) {
                    result.add(new DeclValidationMessage("A040", linkCodDespesa, new String[]{linkCodDespesa, linkTitular}));
                } else {
                    processedTuples.add(tuple);
                }
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(codDespesa) && StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(valor)) {
                result.add(new DeclValidationMessage("A041", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (codDespesa != null && (titular == null || valor == null)) {
                result.add(new DeclValidationMessage("A043", linkCodDespesa, new String[]{linkCodDespesa, linkTitular, linkValor}));
            }
            if (codDespesa == null && (titular != null || valor != null)) {
                result.add(new DeclValidationMessage("A045", linkCodDespesa, new String[]{linkCodDespesa}));
            }
            this.validateA046(result, linkTitular, titular);
            this.validateA047(result, rostoModel, titular, linkTitular);
            if (valor != null && valor == 0) {
                result.add(new DeclValidationMessage("A048", linkValor, new String[]{linkValor}));
            }
            AnexoJModel anexoJModel = null;
            if (model != null && rostoModel != null && rostoModel.getNIFTitular(titular) != null) {
                anexoJModel = ((Modelo3IRSv2015Model)model).getAnexoJByTitular(rostoModel.getNIFTitular(titular).toString());
            }
            this.validateA124(result, anexoAq04T4BLinha, quadro04AnexoAModel, anexoJModel, linha + 1);
            this.validateA125(result, anexoAq04T4BLinha, linkValor);
            this.validateA129(result, anexoAq04T4BLinha, quadro04AnexoAModel.getAnexoAq04T4A(), linkValor);
            this.validateA130(result, anexoAq04T4BLinha, quadro04AnexoAModel.getAnexoAq04T4A(), linkValor);
            this.validateA131(result, anexoAq04T4BLinha, quadro04AnexoAModel.getAnexoAq04T4A(), linkValor);
        }
        return result;
    }

    protected void validateA129(ValidationResult result, AnexoAq04T4B_Linha anexoAq04T4BLinha, EventList<AnexoAq04T4A_Linha> anexoAq04T4A, String linkValor) {
        Long valorDeducao;
        Long l = valorDeducao = anexoAq04T4BLinha != null ? anexoAq04T4BLinha.getValor() : null;
        if (!(anexoAq04T4A == null || anexoAq04T4A.isEmpty() || anexoAq04T4BLinha == null || valorDeducao == null || valorDeducao <= 500000 || valorDeducao >= 1000000 || anexoAq04T4BLinha.getCodDespesa() == null || !anexoAq04T4BLinha.getCodDespesa().equals(409) || StringUtil.isEmpty(anexoAq04T4BLinha.getTitular()))) {
            String titular = anexoAq04T4BLinha.getTitular();
            long sum = 0;
            boolean foundOtherCodes = false;
            boolean validationMark = false;
            for (AnexoAq04T4A_Linha anexoAq04T4A_Linha : anexoAq04T4A) {
                if (titular.equals(anexoAq04T4A_Linha.getTitular()) && anexoAq04T4A_Linha.getCodRendimentos() != null && (anexoAq04T4A_Linha.getCodRendimentos().equals(401) || anexoAq04T4A_Linha.getCodRendimentos().equals(403) || anexoAq04T4A_Linha.getCodRendimentos().equals(408)) && anexoAq04T4A_Linha.getRendimentos() != null) {
                    sum+=anexoAq04T4A_Linha.getRendimentos().longValue();
                    validationMark = true;
                }
                if (!titular.equals(anexoAq04T4A_Linha.getTitular()) || anexoAq04T4A_Linha.getCodRendimentos() == null || !anexoAq04T4A_Linha.getCodRendimentos().equals(404) && !anexoAq04T4A_Linha.getCodRendimentos().equals(405) && !anexoAq04T4A_Linha.getCodRendimentos().equals(406)) continue;
                foundOtherCodes = true;
                validationMark = true;
            }
            if ((double)valorDeducao.longValue() > 0.02 * (double)sum && !foundOtherCodes && validationMark) {
                result.add(new DeclValidationMessage("A129", linkValor, new String[]{linkValor}));
            }
        }
    }

    protected void validateA130(ValidationResult result, AnexoAq04T4B_Linha anexoAq04T4BLinha, EventList<AnexoAq04T4A_Linha> anexoAq04T4A, String linkValor) {
        Long valorDeducao;
        Long l = valorDeducao = anexoAq04T4BLinha != null ? anexoAq04T4BLinha.getValor() : null;
        if (!(anexoAq04T4A == null || anexoAq04T4A.isEmpty() || anexoAq04T4BLinha == null || valorDeducao == null || valorDeducao <= 500000 || valorDeducao >= 1000000 || anexoAq04T4BLinha.getCodDespesa() == null || !anexoAq04T4BLinha.getCodDespesa().equals(409) || StringUtil.isEmpty(anexoAq04T4BLinha.getTitular()))) {
            String titular = anexoAq04T4BLinha.getTitular();
            long sum = 0;
            boolean foundOtherCodes = false;
            boolean validationMark = false;
            for (AnexoAq04T4A_Linha anexoAq04T4A_Linha : anexoAq04T4A) {
                if (titular.equals(anexoAq04T4A_Linha.getTitular()) && anexoAq04T4A_Linha.getCodRendimentos() != null && (anexoAq04T4A_Linha.getCodRendimentos().equals(404) || anexoAq04T4A_Linha.getCodRendimentos().equals(405) || anexoAq04T4A_Linha.getCodRendimentos().equals(406)) && anexoAq04T4A_Linha.getRendimentos() != null) {
                    sum+=anexoAq04T4A_Linha.getRendimentos().longValue();
                    validationMark = true;
                }
                if (!titular.equals(anexoAq04T4A_Linha.getTitular()) || anexoAq04T4A_Linha.getCodRendimentos() == null || !anexoAq04T4A_Linha.getCodRendimentos().equals(401) && !anexoAq04T4A_Linha.getCodRendimentos().equals(403) && !anexoAq04T4A_Linha.getCodRendimentos().equals(408)) continue;
                foundOtherCodes = true;
                validationMark = true;
            }
            if ((double)valorDeducao.longValue() > 0.02 * (double)sum && !foundOtherCodes && validationMark) {
                result.add(new DeclValidationMessage("A130", linkValor, new String[]{linkValor}));
            }
        }
    }

    protected void validateA131(ValidationResult result, AnexoAq04T4B_Linha anexoAq04T4BLinha, EventList<AnexoAq04T4A_Linha> anexoAq04T4A, String linkValor) {
        Long valorDeducao;
        Long l = valorDeducao = anexoAq04T4BLinha != null ? anexoAq04T4BLinha.getValor() : null;
        if (!(anexoAq04T4A == null || anexoAq04T4A.isEmpty() || anexoAq04T4BLinha == null || valorDeducao == null || valorDeducao <= 500000 || valorDeducao >= 1000000 || anexoAq04T4BLinha.getCodDespesa() == null || !anexoAq04T4BLinha.getCodDespesa().equals(409) || StringUtil.isEmpty(anexoAq04T4BLinha.getTitular()))) {
            String titular = anexoAq04T4BLinha.getTitular();
            long max1 = 0;
            long max2 = 0;
            for (AnexoAq04T4A_Linha anexoAq04T4A_Linha : anexoAq04T4A) {
                if (titular.equals(anexoAq04T4A_Linha.getTitular()) && anexoAq04T4A_Linha.getCodRendimentos() != null && (anexoAq04T4A_Linha.getCodRendimentos().equals(401) || anexoAq04T4A_Linha.getCodRendimentos().equals(403) || anexoAq04T4A_Linha.getCodRendimentos().equals(408)) && anexoAq04T4A_Linha.getRendimentos() != null) {
                    max1+=anexoAq04T4A_Linha.getRendimentos().longValue();
                }
                if (!titular.equals(anexoAq04T4A_Linha.getTitular()) || anexoAq04T4A_Linha.getCodRendimentos() == null || !anexoAq04T4A_Linha.getCodRendimentos().equals(404) && !anexoAq04T4A_Linha.getCodRendimentos().equals(405) && !anexoAq04T4A_Linha.getCodRendimentos().equals(406) || anexoAq04T4A_Linha.getRendimentos() == null) continue;
                max2+=anexoAq04T4A_Linha.getRendimentos().longValue();
            }
            if ((double)valorDeducao.longValue() > 0.02 * (double)Math.max(max1, max2) && max1 > 0 && max2 > 0) {
                result.add(new DeclValidationMessage("A131", linkValor, new String[]{linkValor}));
            }
        }
    }

    protected void validateA047(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("A047", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    protected void validateA046(ValidationResult result, String linkTitular, String titular) {
        if (!(titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("A046", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateA124(ValidationResult result, AnexoAq04T4B_Linha current, pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel, AnexoJModel anexoJModel, int numLinha) {
        if (!(current == null || StringUtil.isEmpty(current.getTitular()) || quadro04AnexoAModel.existsTitularInAnexoAq04T4A(current.getTitular()) || anexoJModel != null && (anexoJModel.getQuadro04().getAnexoJq04C401b() != null || anexoJModel.getQuadro04().getAnexoJq04C402b() != null || anexoJModel.getQuadro04().getAnexoJq04C416b() != null || anexoJModel.getQuadro04().getAnexoJq04C417b() != null || anexoJModel.getQuadro04().getAnexoJq04C419b() != null))) {
            String linkTitular = AnexoAq04T4B_Linha.getLink(numLinha, AnexoAq04T4B_LinhaBase.Property.TITULAR);
            result.add(new DeclValidationMessage("A124", linkTitular, new String[]{linkTitular, "aAnexoA.qQuadro04.tanexoAq04T4A"}));
        }
    }

    public void validateA125(ValidationResult result, AnexoAq04T4B_Linha anexoAq04T4B, String linkValor) {
        if (anexoAq04T4B.getCodDespesa() != null && anexoAq04T4B.getValor() != null && anexoAq04T4B.getCodDespesa().equals(409) && anexoAq04T4B.getValor() > 999999) {
            result.add(new DeclValidationMessage("A125", linkValor, new String[]{linkValor}));
        }
    }

    protected ValidationResult validaT4Ba(pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel) {
        int linha;
        String linkCurrentLine;
        ValidationResult result = new ValidationResult();
        ListHashMap<String, Long> somaValorPorTitular = new ListHashMap<String, Long>();
        for (linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4Ba().size(); ++linha) {
            AnexoAq04T4Ba_Linha anexoAq04T4BaLinha = quadro04AnexoAModel.getAnexoAq04T4Ba().get(linha);
            linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4Ba.l" + (linha + 1);
            String linkTitular = linkCurrentLine + ".c" + AnexoAq04T4Ba_LinhaBase.Property.TITULAR.getIndex();
            String linkValor = linkCurrentLine + ".c" + AnexoAq04T4Ba_LinhaBase.Property.VALOR.getIndex();
            String linkNifPortugues = linkCurrentLine + ".c" + AnexoAq04T4Ba_LinhaBase.Property.NIFNIPCPORTUGUES.getIndex();
            String linkPais = linkCurrentLine + ".c" + AnexoAq04T4Ba_LinhaBase.Property.PAIS.getIndex();
            String linkNumFiscalUE = linkCurrentLine + ".c" + AnexoAq04T4Ba_LinhaBase.Property.NUMEROFISCALUE.getIndex();
            String titular = anexoAq04T4BaLinha.getTitular();
            Long valor = anexoAq04T4BaLinha.getValor();
            Long nifPortugues = anexoAq04T4BaLinha.getNifNipcPortugues();
            Long pais = anexoAq04T4BaLinha.getPais();
            String numFiscalUE = anexoAq04T4BaLinha.getNumeroFiscalUE();
            Long codigo = anexoAq04T4BaLinha.getProfissaoCodigo();
            if (!(StringUtil.isEmpty(titular) || valor == null)) {
                if (somaValorPorTitular.containsKey(titular)) {
                    Long valorExistente = (Long)somaValorPorTitular.get(titular);
                    somaValorPorTitular.put(titular, valorExistente + valor);
                } else {
                    somaValorPorTitular.put(titular, valor);
                }
            }
            this.validateA117(result, anexoAq04T4BaLinha, linha + 1);
            if (StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(codigo) && Modelo3IRSValidatorUtil.isEmptyOrZero(valor) && Modelo3IRSValidatorUtil.isEmptyOrZero(nifPortugues) && Modelo3IRSValidatorUtil.isEmptyOrZero(pais) && StringUtil.isEmpty(numFiscalUE)) {
                result.add(new DeclValidationMessage("A090", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            this.validateA050(result, anexoAq04T4BaLinha, linha + 1);
            this.validateA051(result, linkTitular, titular);
            if (!(nifPortugues == null || Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9"))) {
                result.add(new DeclValidationMessage("A054", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (!(nifPortugues == null || Modelo3IRSValidatorUtil.isNIFValid(nifPortugues))) {
                result.add(new DeclValidationMessage("A055", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_Rosto.class.getSimpleName());
            if (!(pais == null || paisesCatalog.containsKey(pais))) {
                result.add(new DeclValidationMessage("A056", linkPais, new String[]{linkPais}));
            }
            if (pais != null && numFiscalUE == null || pais == null && numFiscalUE != null) {
                result.add(new DeclValidationMessage("A081", linkPais, new String[]{linkPais, linkNumFiscalUE}));
            }
            if (numFiscalUE != null && StringUtil.isNumeric(numFiscalUE) && Long.parseLong(numFiscalUE) == 0 && pais != null) {
                result.add(new DeclValidationMessage("A057", linkNumFiscalUE, new String[]{linkNumFiscalUE, linkPais}));
            }
            if (numFiscalUE != null && numFiscalUE.startsWith(" ") && pais != null) {
                result.add(new DeclValidationMessage("A058", linkNumFiscalUE, new String[]{linkNumFiscalUE}));
            }
            if (nifPortugues != null && numFiscalUE != null) {
                result.add(new DeclValidationMessage("A082", linkNumFiscalUE, new String[]{linkNifPortugues, linkNumFiscalUE}));
            }
            if (!(titular == null || quadro04AnexoAModel.existsInAnexoAq04T4B(titular, 413))) {
                result.add(new DeclValidationMessage("A052", linkTitular, new String[]{"aAnexoA.qQuadro04.tanexoAq04T4B"}));
            }
            if (valor == null || valor != 0) continue;
            result.add(new DeclValidationMessage("A053", linkValor, new String[]{linkValor}));
        }
        if (quadro04AnexoAModel.getAnexoAq04T4Ba().size() > 16) {
            result.add(new DeclValidationMessage("A096", "aAnexoA.qQuadro04.tanexoAq04T4Ba", new String[]{"aAnexoA.qQuadro04.tanexoAq04T4Ba"}));
        }
        for (linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4B().size(); ++linha) {
            AnexoAq04T4B_Linha linha4B = quadro04AnexoAModel.getAnexoAq04T4B().get(linha);
            linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4B.l" + (linha + 1);
            String linkValor = linkCurrentLine + ".c4";
            Long codDespesa = linha4B.getCodDespesa();
            String titular = linha4B.getTitular();
            Long valor = linha4B.getValor();
            if (StringUtil.isEmpty(titular) || codDespesa == null || codDespesa != 413 || !somaValorPorTitular.containsKey(titular) || valor != null && (valor == null || ((Long)somaValorPorTitular.get(titular)).longValue() == valor.longValue())) continue;
            result.add(new DeclValidationMessage("A084", linkValor, new String[]{linkValor}));
        }
        return result;
    }

    protected void validateA051(ValidationResult result, String linkTitular, String titular) {
        if (!(titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("A051", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateA083(ValidationResult result, Set<Tuple> processedTuplesVersionONE, Set<Tuple> processedTuplesVersionTWO, AnexoAq04T4Ba_Linha current, int numLinha) {
        Tuple tuple;
        String linkNifPortugues = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.NIFNIPCPORTUGUES);
        String linkNumFiscalUE = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.NUMEROFISCALUE);
        Long nifPortugues = current.getNifNipcPortugues();
        String titular = current.getTitular();
        String numFiscalUE = current.getNumeroFiscalUE();
        Long pais = current.getPais();
        Long profissaoCodigo = current.getProfissaoCodigo();
        if (!(StringUtil.isEmpty(titular) || profissaoCodigo == null || nifPortugues == null || processedTuplesVersionONE.add(tuple = new Tuple(new Object[]{titular, profissaoCodigo, nifPortugues})))) {
            result.add(new DeclValidationMessage("A083", linkNifPortugues, new String[]{linkNifPortugues}));
        }
        if (!(StringUtil.isEmpty(titular) || profissaoCodigo == null || pais == null || StringUtil.isEmpty(numFiscalUE) || processedTuplesVersionTWO.add(tuple = new Tuple(new Object[]{titular, profissaoCodigo, pais, numFiscalUE})))) {
            result.add(new DeclValidationMessage("A083", linkNumFiscalUE, new String[]{linkNumFiscalUE}));
        }
    }

    protected void validateA050(ValidationResult result, AnexoAq04T4Ba_Linha current, int numLinha) {
        String titular = current.getTitular();
        Long valor = current.getValor();
        Long nifPortugues = current.getNifNipcPortugues();
        String numFiscalUE = current.getNumeroFiscalUE();
        Long profissaoCodigo = current.getProfissaoCodigo();
        if (titular == null && (valor != null || profissaoCodigo != null || nifPortugues != null || numFiscalUE != null)) {
            String linkTitular = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.TITULAR);
            result.add(new DeclValidationMessage("A050", linkTitular, new String[]{linkTitular}));
        }
    }

    public void validateA119(ValidationResult result, AnexoAq04T4A_Linha linha, int numLinha) {
        HashSet<Long> codigos = new HashSet<Long>(Arrays.asList(403, 406, 407));
        Long sobreTaxa = linha.getRetSobretaxa();
        Long codRendimentos = linha.getCodRendimentos();
        if (sobreTaxa != null && sobreTaxa > 0 && codRendimentos != null && codigos.contains(codRendimentos)) {
            String linkCodRendimentos = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS);
            String linkRetencao = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.RETSOBRETAXA);
            result.add(new DeclValidationMessage("A119", linkCodRendimentos, new String[]{linkCodRendimentos, linkRetencao}));
        }
    }

    public void validateA120(ValidationResult result, AnexoAq04T4A_Linha linha, Long anoRendimentos, int numLinha) {
        Long sobreTaxa = linha.getRetSobretaxa();
        HashSet<Long> codigos = new HashSet<Long>(Arrays.asList(401, 404, 405, 408));
        if (sobreTaxa != null && sobreTaxa > 0 && anoRendimentos != null && anoRendimentos.intValue() != 2011 && anoRendimentos.intValue() != 2013 && anoRendimentos.intValue() != 2014 && codigos.contains(linha.getCodRendimentos())) {
            String linkRetencao = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.RETSOBRETAXA);
            result.add(new DeclValidationMessage("A120", linkRetencao, new String[]{linkRetencao, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    public void validateA122(ValidationResult result, AnexoAq04T4A_Linha linha, int numLinha, Long anoRendimentos) {
        Long sobreTaxa = linha.getRetSobretaxa();
        Long rendimentos = linha.getRendimentos();
        if (anoRendimentos != null && (anoRendimentos.intValue() == 2011 || anoRendimentos.intValue() > 2012) && sobreTaxa != null && sobreTaxa > 0 && (rendimentos == null || sobreTaxa > rendimentos)) {
            String linkRetencao = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.RETSOBRETAXA);
            String linkRendimentos = AnexoAq04T4A_Linha.getLink(numLinha, AnexoAq04T4A_LinhaBase.Property.RENDIMENTOS);
            result.add(new DeclValidationMessage("A122", linkRetencao, new String[]{linkRetencao, linkRendimentos}));
        }
    }

    public void validateA117(ValidationResult result, AnexoAq04T4Ba_Linha linha, int numLinha) {
        HashSet<Long> supportedCodes = new HashSet<Long>(Arrays.asList(1, 2, 3));
        if (!(linha == null || linha.getProfissaoCodigo() == null || supportedCodes.contains(linha.getProfissaoCodigo()))) {
            String linkProfissao = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.PROFISSAOCODIGO);
            result.add(new DeclValidationMessage("A117", linkProfissao, new String[]{linkProfissao}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        int linha;
        ValidationResult result = new ValidationResult();
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        if (anexoAModel == null) {
            return result;
        }
        pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel = (pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04)anexoAModel.getQuadro(pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (int linha2 = 0; linha2 < quadro04AnexoAModel.getAnexoAq04T4A().size(); ++linha2) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = quadro04AnexoAModel.getAnexoAq04T4A().get(linha2);
            String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + (linha2 + 1);
            String linkRendimentos = linkCurrentLine + ".c5";
            String linkContribuicoes = linkCurrentLine + ".c7";
            String linkRetencoes = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.RETENCOES.getIndex();
            Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
            Long rendimentos = anexoAq04T4ALinha.getRendimentos();
            Long contribuicoes = anexoAq04T4ALinha.getContribuicoes();
            Long retencoes = anexoAq04T4ALinha.getRetencoes();
            this.validateA104(result, anexoAq04T4ALinha, linha2 + 1);
            this.validateA106(result, anexoAq04T4ALinha, linha2 + 1);
            this.validateA015(result, anexoAq04T4ALinha, this.anoRosto, linha2 + 1);
            this.validateA016(result, anexoAq04T4ALinha, this.anoRosto, linha2 + 1);
            this.validateA017(result, rostoModel, anexoAq04T4ALinha, linha2 + 1);
            this.validateA021(result, anexoAq04T4ALinha, linha2, this.anoRosto);
            if (codRendimentos != null && Modelo3IRSValidatorUtil.in(codRendimentos, new long[]{403}) && contribuicoes != null && rendimentos != null && (double)contribuicoes.longValue() > (double)rendimentos.longValue() * 0.2) {
                result.add(new DeclValidationMessage("A022", linkContribuicoes, new String[]{linkContribuicoes}));
            }
            this.validateA025(result, anexoAq04T4ALinha, this.anoRosto, linkRendimentos);
            if (retencoes != null && rendimentos != null && retencoes > rendimentos) {
                result.add(new DeclValidationMessage("A027", linkRetencoes, new String[]{linkRetencoes, linkRendimentos}));
            }
            if (retencoes == null || retencoes <= 0 || rendimentos != null || codRendimentos == null || codRendimentos == 406) continue;
            result.add(new DeclValidationMessage("A028", linkRetencoes, new String[]{linkRendimentos, linkRetencoes}));
        }
        HashSet<Tuple> processedTuplesVersionONE = new HashSet<Tuple>();
        HashSet<Tuple> processedTuplesVersionTWO = new HashSet<Tuple>();
        for (linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4Ba().size(); ++linha) {
            AnexoAq04T4Ba_Linha anexoAq04T4BaLinha = quadro04AnexoAModel.getAnexoAq04T4Ba().get(linha);
            this.validateA083(result, processedTuplesVersionONE, processedTuplesVersionTWO, anexoAq04T4BaLinha, linha + 1);
            this.validateA049(result, anexoAq04T4BaLinha, linha + 1);
        }
        for (linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4B().size(); ++linha) {
            AnexoAq04T4B_Linha anexoAq04T4BLinha = quadro04AnexoAModel.getAnexoAq04T4B().get(linha);
            String linkCodDespesa = AnexoAq04T4B_Linha.getLink(linha + 1, AnexoAq04T4B_LinhaBase.Property.CODDESPESA);
            Long codDespesa = anexoAq04T4BLinha.getCodDespesa();
            String titular = anexoAq04T4BLinha.getTitular();
            this.validateA042(result, linkCodDespesa, codDespesa);
            this.validateA044(result, quadro04AnexoAModel, linkCodDespesa, codDespesa, titular);
        }
        return result;
    }

    protected void validateA021(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, int linha, Long anoRosto) {
        String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + (linha + 1);
        String linkCodRendimentos = linkCurrentLine + ".c3";
        String linkContribuicoes = linkCurrentLine + ".c7";
        Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
        Long rendimentos = anexoAq04T4ALinha.getRendimentos();
        Long contribuicoes = anexoAq04T4ALinha.getContribuicoes();
        if (codRendimentos != null && codRendimentos == 408 && anoRosto != null && anoRosto < 2008 && contribuicoes != null && rendimentos != null && (double)contribuicoes.longValue() > (double)rendimentos.longValue() * 0.11) {
            result.add(new DeclValidationMessage("A021", linkCodRendimentos, new String[]{linkContribuicoes}));
        }
        if (codRendimentos != null && codRendimentos == 408 && anoRosto != null && anoRosto >= 2008 && contribuicoes != null && rendimentos != null && (double)contribuicoes.longValue() > (double)rendimentos.longValue() * 0.205) {
            result.add(new DeclValidationMessage("A021", linkCodRendimentos, new String[]{linkContribuicoes}));
        }
    }

    protected void validateA025(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, Long anoExercicio, String linkRendimentos) {
        if (anexoAq04T4ALinha != null && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoAq04T4ALinha.getRendimentos()) && anexoAq04T4ALinha.getNIF() != null && !Modelo3IRSValidatorUtil.in(anexoAq04T4ALinha.getNIF(), new long[]{503509027, 500077568, 500745439, 501449752}) && anoExercicio != null && anoExercicio <= 2008) {
            result.add(new DeclValidationMessage("A025", linkRendimentos, new String[]{linkRendimentos}));
        }
    }

    protected void validateA049(ValidationResult result, AnexoAq04T4Ba_Linha current, int numLinha) {
        if (current == null) {
            return;
        }
        String titular = current.getTitular();
        Long valor = current.getValor();
        Long nifPortugues = current.getNifNipcPortugues();
        String numFiscalUE = current.getNumeroFiscalUE();
        Long codProfissao = current.getProfissaoCodigo();
        if (titular != null && (valor == null || codProfissao == null || nifPortugues == null && numFiscalUE == null)) {
            String linkTitular = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.TITULAR);
            String linkValor = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.VALOR);
            String linkNifPortugues = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.NIFNIPCPORTUGUES);
            String linkNumFiscalUE = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.NUMEROFISCALUE);
            String linkCodigo = AnexoAq04T4Ba_Linha.getLink(numLinha, AnexoAq04T4Ba_LinhaBase.Property.PROFISSAOCODIGO);
            result.add(new DeclValidationMessage("A049", linkTitular, new String[]{linkTitular, linkCodigo, linkValor, linkNifPortugues, linkNumFiscalUE}));
        }
    }

    protected void validateA044(ValidationResult result, pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel, String linkCodDespesa, Long codDespesa, String titular) {
        if (!(codDespesa == null || codDespesa != 413 || titular == null || quadro04AnexoAModel.existsInAnexoAq04T4Ba(titular))) {
            result.add(new DeclValidationMessage("A044", linkCodDespesa, new String[]{linkCodDespesa, "aAnexoA.qQuadro04.tanexoAq04T4Ba", "aAnexoA.qQuadro04.tanexoAq04T4Ba", "aAnexoA.qQuadro04.tanexoAq04T4Ba"}));
        }
    }

    protected void validateA042(ValidationResult result, String linkCodDespesa, Long codDespesa) {
        if (!(codDespesa == null || CatalogManager.getInstance().getCatalog("Cat_M3V2015_Despesas").containsKey(codDespesa))) {
            result.add(new DeclValidationMessage("A042", linkCodDespesa, new String[]{linkCodDespesa}));
        }
    }

    protected void validateA104(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, int numLinha) {
        String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + numLinha;
        String linkCodRendimentos = linkCurrentLine + ".c3";
        String linkTitular = linkCurrentLine + ".c4";
        Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
        String titular = anexoAq04T4ALinha.getTitular();
        if (codRendimentos != null && StringUtil.isEmpty(titular)) {
            result.add(new DeclValidationMessage("A104", linkCodRendimentos, new String[]{linkCodRendimentos, linkTitular}));
        }
    }

    protected void validateA106(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, int numLinha) {
        String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + numLinha;
        String linkCodRendimentos = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkNif = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.NIF.getIndex();
        Long nif = anexoAq04T4ALinha.getNIF();
        Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
        if (codRendimentos != null && codRendimentos != 401 && nif == null) {
            result.add(new DeclValidationMessage("A106", linkCodRendimentos, new String[]{linkNif}));
        }
    }

    protected void validateA015(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, Long anoRosto, int numLinha) {
        String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + numLinha;
        String linkCodRendimentos = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkRetencoes = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.RETENCOES.getIndex();
        Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
        Long rendimentos = anexoAq04T4ALinha.getRendimentos();
        Long retencoes = anexoAq04T4ALinha.getRetencoes();
        if (codRendimentos != null && codRendimentos == 401 && retencoes != null && rendimentos != null && anoRosto != null && (retencoes > Math.round((double)rendimentos.longValue() * 0.46) && anoRosto < 2013 || retencoes > Math.round((double)rendimentos.longValue() * 0.485) && anoRosto == 2013 || retencoes > Math.round((double)rendimentos.longValue() * 0.8) && anoRosto == 2014)) {
            result.add(new DeclValidationMessage("A015", linkCodRendimentos, new String[]{linkRetencoes}));
        }
    }

    protected void validateA016(ValidationResult result, AnexoAq04T4A_Linha anexoAq04T4ALinha, Long anoRosto, int linha) {
        String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + (linha + 1);
        String linkCodRendimentos = linkCurrentLine + ".c3";
        String linkContribuicoes = linkCurrentLine + ".c7";
        Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
        Long rendimentos = anexoAq04T4ALinha.getRendimentos();
        Long contribuicoes = anexoAq04T4ALinha.getContribuicoes();
        if (codRendimentos != null && codRendimentos == 401 && contribuicoes != null && rendimentos != null && rendimentos > 0 && anoRosto != null && (double)contribuicoes.longValue() > (double)rendimentos.longValue() * 0.16 && anoRosto < 2008) {
            result.add(new DeclValidationMessage("A016", linkCodRendimentos, new String[]{linkContribuicoes}));
        }
        if (codRendimentos != null && codRendimentos == 401 && contribuicoes != null && rendimentos != null && rendimentos > 0 && anoRosto != null && (double)contribuicoes.longValue() > (double)rendimentos.longValue() * 0.205 && anoRosto >= 2008) {
            result.add(new DeclValidationMessage("A016", linkCodRendimentos, new String[]{linkContribuicoes}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04 quadro04AnexoAModel = anexoAModel.getQuadro04();
        for (int linha = 0; linha < quadro04AnexoAModel.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = quadro04AnexoAModel.getAnexoAq04T4A().get(linha);
            String linkCurrentLine = "aAnexoA.qQuadro04.tanexoAq04T4A.l" + (linha + 1);
            String linkCodRendimentos = linkCurrentLine + ".c3";
            String linkRetencoes = linkCurrentLine + ".c" + AnexoAq04T4A_LinhaBase.Property.RETENCOES.getIndex();
            Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
            Long rendimentos = anexoAq04T4ALinha.getRendimentos();
            Long retencoes = anexoAq04T4ALinha.getRetencoes();
            Date dataContratoPreReforma = anexoAq04T4ALinha.getDataContratoPreReforma();
            if (codRendimentos != null && codRendimentos == 408 && dataContratoPreReforma != null && dataContratoPreReforma.after(new Date("2000-12-31")) && retencoes != null && rendimentos != null && (double)retencoes.longValue() > (double)rendimentos.longValue() * 0.425) {
                result.add(new DeclValidationMessage("A020", linkCodRendimentos, new String[]{linkRetencoes}));
            }
            if (codRendimentos == null || codRendimentos != 408 || dataContratoPreReforma == null || !dataContratoPreReforma.beforeOrEquals(new Date("2000-12-31")) || retencoes == null || rendimentos == null || (double)retencoes.longValue() <= (double)rendimentos.longValue() * 0.405) continue;
            result.add(new DeclValidationMessage("A020", linkCodRendimentos, new String[]{linkRetencoes}));
        }
        return result;
    }
}

