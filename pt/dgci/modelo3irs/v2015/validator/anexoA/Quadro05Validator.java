/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoA;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        Quadro05 quadro05AnexoAModel = anexoAModel.getQuadro05();
        Quadro04 quadro04AnexoAModel = anexoAModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (quadro05AnexoAModel.getAnexoAq05T5().size() > 16) {
            result.add(new DeclValidationMessage("A074", "aAnexoA.qQuadro05.tanexoAq05T5", new String[]{"aAnexoA.qQuadro05.tanexoAq05T5"}));
        }
        HashSet<Tuple> processedTuples = new HashSet<Tuple>();
        HashMap<String, Long> valoresQuadro5 = new HashMap<String, Long>();
        for (int linha = 0; linha < quadro05AnexoAModel.getAnexoAq05T5().size(); ++linha) {
            AnexoAq05T5_Linha anexoAq05T5ALinha = quadro05AnexoAModel.getAnexoAq05T5().get(linha);
            String linkCurrentLine = "aAnexoA.qQuadro05.tanexoAq05T5.l" + (linha + 1);
            String linkNif = linkCurrentLine + ".c" + AnexoAq05T5_LinhaBase.Property.NIF.getIndex();
            String linkCodRendimentos = linkCurrentLine + ".c" + AnexoAq05T5_LinhaBase.Property.CODRENDIMENTOS.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoAq05T5_LinhaBase.Property.TITULAR.getIndex();
            String linkRendimentos = linkCurrentLine + ".c" + AnexoAq05T5_LinhaBase.Property.RENDIMENTOS.getIndex();
            String linkAnos = linkCurrentLine + ".c" + AnexoAq05T5_LinhaBase.Property.NANOS.getIndex();
            Long nif = anexoAq05T5ALinha.getNif();
            Long codRendimentos = anexoAq05T5ALinha.getCodRendimentos();
            String titular = anexoAq05T5ALinha.getTitular();
            Long rendimentos = anexoAq05T5ALinha.getRendimentos();
            Long anos = anexoAq05T5ALinha.getNanos();
            this.validateA075(result, processedTuples, anexoAq05T5ALinha, linha + 1);
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && Modelo3IRSValidatorUtil.isEmptyOrZero(codRendimentos) && StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentos) && Modelo3IRSValidatorUtil.isEmptyOrZero(anos)) {
                result.add(new DeclValidationMessage("A076", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (!(nif == null || Modelo3IRSValidatorUtil.startsWith(nif, "1", "2", "3", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("A059", linkNif, new String[]{linkNif}));
            }
            if (!(nif == null || NifValidator.validate(nif))) {
                result.add(new DeclValidationMessage("A060", linkNif, new String[]{linkNif}));
            }
            this.validateA061(result, rostoModel, linkNif, nif);
            if (nif != null && (codRendimentos == null || titular == null || rendimentos == null || anos == null || anos == 0)) {
                result.add(new DeclValidationMessage("A062", linkNif, new String[]{linkNif, linkCodRendimentos, linkTitular, linkRendimentos, linkAnos}));
            }
            if (!(codRendimentos == null || Modelo3IRSValidatorUtil.in(codRendimentos, new long[]{404, 408, 407, 406, 405, 401, 403}))) {
                result.add(new DeclValidationMessage("A064", linkCodRendimentos, new String[]{linkCodRendimentos}));
            }
            this.validateA065(result, linkTitular, titular);
            if (rendimentos != null && rendimentos == 0) {
                result.add(new DeclValidationMessage("A066", linkRendimentos, new String[]{linkRendimentos}));
            }
            if (anos != null && anos > 9) {
                result.add(new DeclValidationMessage("A067", linkAnos, new String[]{linkAnos}));
            }
            if (!(nif == null || titular == null || quadro04AnexoAModel.existsEntidadeTitularInAnexoAq04T4A(nif, titular))) {
                result.add(new DeclValidationMessage("A068", linkNif, new String[]{linkNif, "aAnexoA.qQuadro04.tanexoAq04T4A"}));
            }
            if (titular == null || codRendimentos == null || nif == null || rendimentos == null) continue;
            String key = nif + "-" + codRendimentos + "-" + titular;
            Long oldAmount = valoresQuadro5.containsKey(key) ? (Long)valoresQuadro5.remove(key) : 0;
            valoresQuadro5.put(key, oldAmount + rendimentos);
        }
        HashMap<String, Long> valoresQuadro4 = new HashMap<String, Long>();
        for (int linha2 = 0; linha2 < quadro04AnexoAModel.getAnexoAq04T4A().size(); ++linha2) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = quadro04AnexoAModel.getAnexoAq04T4A().get(linha2);
            Long nif = anexoAq04T4ALinha.getNIF();
            Long codRendimentos = anexoAq04T4ALinha.getCodRendimentos();
            String titular = anexoAq04T4ALinha.getTitular();
            Long rendimentos = anexoAq04T4ALinha.getRendimentos();
            if (titular == null || codRendimentos == null || nif == null || rendimentos == null) continue;
            String key = nif + "-" + codRendimentos + "-" + titular;
            Long oldAmount = valoresQuadro4.containsKey(key) ? (Long)valoresQuadro4.remove(key) : 0;
            valoresQuadro4.put(key, oldAmount + rendimentos);
        }
        for (String keyQuadro5 : valoresQuadro5.keySet()) {
            if (valoresQuadro4.containsKey(keyQuadro5) && (Long)valoresQuadro5.get(keyQuadro5) <= (Long)valoresQuadro4.get(keyQuadro5)) continue;
            result.add(new DeclValidationMessage("A069", "aAnexoA.qQuadro05.tanexoAq05T5", new String[]{"aAnexoA.qQuadro05.tanexoAq05T5", "aAnexoA.qQuadro04.tanexoAq04T4A"}));
        }
        return result;
    }

    protected void validateA065(ValidationResult result, String linkTitular, String titular) {
        if (!(titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro03.isDependenteEmGuardaConjunta(titular) || Quadro07.isConjugeFalecido(titular))) {
            result.add(new DeclValidationMessage("A065", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateA061(ValidationResult result, RostoModel rostoModel, String linkNif, Long nif) {
        Quadro03 quadro03RostoModel = rostoModel.getQuadro03();
        Quadro07 quadro07RostoModel = rostoModel.getQuadro07();
        if (nif != null && (quadro03RostoModel.isSujeitoPassivoA(nif) || quadro03RostoModel.isSujeitoPassivoB(nif) || quadro03RostoModel.existsInDependentes(nif) || quadro07RostoModel.isConjugeFalecido(nif) || quadro07RostoModel.existsInAscendentes(nif) || quadro07RostoModel.existsInAfilhadosCivisEmComunhao(nif) || quadro03RostoModel.existsInDependentesEmGuardaConjunta(nif) || quadro03RostoModel.existsInDependentesEmGuardaConjuntaOutro(nif) || quadro07RostoModel.existsInAscendentesEColaterais(nif))) {
            result.add(new DeclValidationMessage("A061", linkNif, new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected void validateA075(ValidationResult result, Set<Tuple> processedTuples, AnexoAq05T5_Linha current, int numLinha) {
        String titular;
        Tuple tuple;
        Long nif = current.getNif() != null ? current.getNif() : 0;
        Long codigoRendimentos = current.getCodRendimentos() != null ? current.getCodRendimentos() : 0;
        String string = titular = current.getTitular() != null ? current.getTitular() : "";
        if (!(nif == null && codigoRendimentos == 0 && StringUtil.isEmpty(titular) || processedTuples.add(tuple = new Tuple(new Object[]{nif, codigoRendimentos, titular})))) {
            String linkCurrentLine = AnexoAq05T5_Linha.getLink(numLinha);
            result.add(new DeclValidationMessage("A075", linkCurrentLine, new String[]{linkCurrentLine}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        if (anexoAModel == null) {
            return result;
        }
        Quadro05 quadro05AnexoAModel = anexoAModel.getQuadro05();
        for (int linha = 0; linha < quadro05AnexoAModel.getAnexoAq05T5().size(); ++linha) {
            AnexoAq05T5_Linha anexoAq05T5ALinha = quadro05AnexoAModel.getAnexoAq05T5().get(linha);
            String linkNif = AnexoAq05T5_Linha.getLink(linha + 1, AnexoAq05T5_LinhaBase.Property.NIF);
            Long nif = anexoAq05T5ALinha.getNif();
            Long codRendimentos = anexoAq05T5ALinha.getCodRendimentos();
            String titular = anexoAq05T5ALinha.getTitular();
            Long rendimentos = anexoAq05T5ALinha.getRendimentos();
            Long anos = anexoAq05T5ALinha.getNanos();
            this.validateA107(result, linkNif, nif, codRendimentos, titular, rendimentos, anos);
        }
        return result;
    }

    protected void validateA107(ValidationResult result, String linkNif, Long nif, Long codRendimentos, String titular, Long rendimentos, Long anos) {
        if (nif == null && (codRendimentos != null || titular != null || rendimentos != null || anos != null)) {
            result.add(new DeclValidationMessage("A107", linkNif, new String[]{linkNif}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

