/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator;

import com.jgoodies.validation.ValidationResult;
import java.util.List;
import javax.xml.parsers.SAXParser;
import pt.dgci.modelo3irs.v2015.flatfile.WriteDeclarationToFlatFile;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.validator.anexoA.net.AnexoANETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.AnexoBNETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.AnexoCNETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.AnexoDNETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoe.net.AnexoENETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexof.net.AnexoFNetValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.AnexoGNETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog1.net.AnexoG1NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.AnexoHNETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoi.net.AnexoINETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoj.net.AnexoJNETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexol.net.AnexoLNETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.net.AnexoSSNETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.RostoNETValidator;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoModelValidator;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.Timer;

public class Modelo3IRSv2015Validator
extends DeclaracaoModelValidator {
    private long validacoesLocaisTimes;
    private long inicializacaoSaxParserTimes;
    private long conversaoFlatFileTimes;
    private long calculoHashTimes;
    private SAXParser parser;

    public Modelo3IRSv2015Validator(SAXParser parser) {
        this();
        this.parser = parser;
    }

    public Modelo3IRSv2015Validator() {
    }

    @Override
    protected ValidationResult validateDeclaration(DeclaracaoModel declaracaoModel) {
        Timer timerForValidacoesLocais = new Timer();
        timerForValidacoesLocais.start();
        try {
            this.declarationValidationResult = new ValidationResult();
            this.declarationValidationResult.addAllFrom(new RostoNETValidator().validate(declaracaoModel));
            if (declaracaoModel.getAnexo(AnexoAModel.class) != null) {
                this.declarationValidationResult.addAllFrom(new AnexoANETValidator().validate(declaracaoModel));
            }
            if (declaracaoModel.getAnexo(AnexoHModel.class) != null) {
                this.declarationValidationResult.addAllFrom(new AnexoHNETValidator().validate(declaracaoModel));
            }
            if (!ListUtil.isEmpty(declaracaoModel.getAllAnexosByType("AnexoJ"))) {
                this.declarationValidationResult.addAllFrom(new AnexoJNETValidator().validate(declaracaoModel));
            }
            if (!ListUtil.isEmpty(declaracaoModel.getAllAnexosByType("AnexoL"))) {
                this.declarationValidationResult.addAllFrom(new AnexoLNETValidator().validate(declaracaoModel));
            }
            if (!ListUtil.isEmpty(declaracaoModel.getAllAnexosByType("AnexoB"))) {
                this.declarationValidationResult.addAllFrom(new AnexoBNETValidator().validate(declaracaoModel));
            }
            if (!ListUtil.isEmpty(declaracaoModel.getAllAnexosByType("AnexoC"))) {
                this.declarationValidationResult.addAllFrom(new AnexoCNETValidator().validate(declaracaoModel));
            }
            if (!ListUtil.isEmpty(declaracaoModel.getAllAnexosByType("AnexoD"))) {
                this.declarationValidationResult.addAllFrom(new AnexoDNETValidator().validate(declaracaoModel));
            }
            if (declaracaoModel.getAnexo(AnexoEModel.class) != null) {
                this.declarationValidationResult.addAllFrom(new AnexoENETValidator().validate(declaracaoModel));
            }
            if (declaracaoModel.getAnexo(AnexoFModel.class) != null) {
                this.declarationValidationResult.addAllFrom(new AnexoFNetValidator().validate(declaracaoModel));
            }
            if (declaracaoModel.getAnexo(AnexoGModel.class) != null) {
                this.declarationValidationResult.addAllFrom(new AnexoGNETValidator().validate(declaracaoModel));
            }
            if (declaracaoModel.getAnexo(AnexoG1Model.class) != null) {
                this.declarationValidationResult.addAllFrom(new AnexoG1NETValidator().validate(declaracaoModel));
            }
            if (!ListUtil.isEmpty(declaracaoModel.getAllAnexosByType("AnexoI"))) {
                this.declarationValidationResult.addAllFrom(new AnexoINETValidator().validate(declaracaoModel));
            }
            if (!ListUtil.isEmpty(declaracaoModel.getAllAnexosByType("AnexoSS"))) {
                this.declarationValidationResult.addAllFrom(new AnexoSSNETValidator().validate(declaracaoModel));
            }
        }
        finally {
            this.validacoesLocaisTimes = timerForValidacoesLocais.ellapsed();
        }
        if (!this.declarationValidationResult.hasErrors()) {
            WriteDeclarationToFlatFile declarationWrite = new WriteDeclarationToFlatFile(this.parser, declaracaoModel, "", "", "");
            try {
                if (!declarationWrite.process()) {
                    this.declarationValidationResult.addAllFrom(declarationWrite.getValidationResult());
                }
            }
            finally {
                this.inicializacaoSaxParserTimes = declarationWrite.getInitSaxParsersTimes();
                this.conversaoFlatFileTimes = declarationWrite.getFlatFileConversionTimes();
                this.calculoHashTimes = declarationWrite.getHashCalculationTimes();
            }
        }
        return this.declarationValidationResult;
    }

    public long getValidacoesLocaisTimes() {
        return this.validacoesLocaisTimes;
    }

    public long getInicializacaoSaxParserTimes() {
        return this.inicializacaoSaxParserTimes;
    }

    public long getConversaoFlatFileTimes() {
        return this.conversaoFlatFileTimes;
    }

    public long getCalculoHashTimes() {
        return this.calculoHashTimes;
    }
}

