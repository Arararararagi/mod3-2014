/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro03 quadro03AnexoHModel = anexoHModel.getQuadro03();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (!Modelo3IRSValidatorUtil.equals(quadro03AnexoHModel.getAnexoHq03C02(), rostoModel.getQuadro03().getQ03C03())) {
            result.add(new DeclValidationMessage("H003", "aAnexoH.qQuadro03.fanexoHq03C02", new String[]{"aAnexoH.qQuadro03.fanexoHq03C02", "aRosto.qQuadro03.fq03C03"}));
        }
        if (!Modelo3IRSValidatorUtil.equals(quadro03AnexoHModel.getAnexoHq03C03(), rostoModel.getQuadro03().getQ03C04())) {
            result.add(new DeclValidationMessage("H004", "aAnexoH.qQuadro03.fanexoHq03C03", new String[]{"aAnexoH.qQuadro03.fanexoHq03C03", "aRosto.qQuadro03.fq03C04"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

