/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxH_Q04;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final long ANO_2013 = 2013;
    private static final long ANO_2012 = 2012;

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        long totalRetencaoIRS;
        long totalRetencaoIRSPreenchido;
        long totalRentimentosIliquidosPreenchidos;
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro04 quadro04AnexoHModel = anexoHModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        if (quadro04AnexoHModel.getAnexoHq04T4().size() > 30) {
            result.add(new DeclValidationMessage("H006", "aAnexoH.qQuadro04", new String[]{"aAnexoH.qQuadro04"}));
        }
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        for (int linha = 0; linha < quadro04AnexoHModel.getAnexoHq04T4().size(); ++linha) {
            Long nifTitular;
            AnexoHq04T4_Linha anexoHq04T4Linha = quadro04AnexoHModel.getAnexoHq04T4().get(linha);
            Long codRendimento = anexoHq04T4Linha.getCodRendimentos();
            String titular = anexoHq04T4Linha.getTitular();
            Long rendimentoIliquido = anexoHq04T4Linha.getRendimentosIliquidos();
            Long retencaoIRS = anexoHq04T4Linha.getRentencaoIRS();
            Long nifPortugues = anexoHq04T4Linha.getNifPortugues();
            Long pais = anexoHq04T4Linha.getPais();
            String numFiscalUE = anexoHq04T4Linha.getNumeroFiscalUE();
            String lineLink = AnexoHq04T4_Linha.getLink(linha + 1);
            String codRendimentosLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.CODRENDIMENTOS.getIndex();
            String titularLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.TITULAR.getIndex();
            String rentencaoIRSLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.RENTENCAOIRS.getIndex();
            String nifEntidadePagadoraLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.NIFPORTUGUES.getIndex();
            if (!(codRendimento == null || StringUtil.isEmpty(String.valueOf(codRendimento)) || Modelo3IRSValidatorUtil.in(codRendimento, new long[]{401, 402, 403, 404, 405, 406, 407, 408, 409, 410}))) {
                result.add(new DeclValidationMessage("H005", codRendimentosLink, new String[]{codRendimentosLink}));
            }
            if (!(codRendimento == null || StringUtil.isEmpty(String.valueOf(codRendimento)) || codRendimento != 403 && codRendimento != 408 && codRendimento != 410 || Modelo3IRSv2015Parameters.instance().isFase2())) {
                result.add(new DeclValidationMessage("H007", codRendimentosLink, new String[]{codRendimentosLink}));
            }
            if (Modelo3IRSv2015Parameters.instance().isFase2() && titular != null && (nifTitular = rostoModel.getNIFTitular(titular)) != null) {
                AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(nifTitular.toString());
                AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(nifTitular.toString());
                if (codRendimento != null && (codRendimento == 403 || codRendimento == 408 || codRendimento == 410) && anexoBModel == null && anexoCModel == null) {
                    result.add(new DeclValidationMessage("H008", lineLink, new String[]{lineLink}));
                }
            }
            this.validateH011(result, anexoHq04T4Linha, titularLink);
            this.validateH012(result, rostoModel, anexoHq04T4Linha, titularLink);
            if (titular != null && "F".equals(titular) && rostoModel.getNIFTitular(titular) == null) {
                result.add(new DeclValidationMessage("H013", titularLink, new String[]{titularLink}));
            }
            if (codRendimento != null && titular != null) {
                String nifPortuguesForTuple = nifPortugues != null ? String.valueOf(nifPortugues) : "-1";
                String paisForTuple = pais != null ? String.valueOf(pais) : "-1";
                String numFiscalForTuple = numFiscalUE != null ? String.valueOf(numFiscalUE) : "-1";
                Tuple processedTuple = new Tuple(new Object[]{String.valueOf(codRendimento), titular, nifPortuguesForTuple, paisForTuple, numFiscalForTuple});
                if (processedTuples.contains(processedTuple)) {
                    result.add(new DeclValidationMessage("H014", nifEntidadePagadoraLink, new String[]{nifEntidadePagadoraLink, titularLink, codRendimentosLink}));
                } else {
                    processedTuples.add(processedTuple);
                }
            }
            this.validateH019(result, anoExercicio, codRendimento, rendimentoIliquido, retencaoIRS, rentencaoIRSLink);
            this.validateH020(result, anoExercicio, codRendimento, rendimentoIliquido, retencaoIRS, rentencaoIRSLink);
            if (!(nifPortugues == null || Modelo3IRSValidatorUtil.startsWith(nifPortugues, "1", "2", "3", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("H023", nifEntidadePagadoraLink, new String[]{nifEntidadePagadoraLink}));
            }
            if (!(nifPortugues == null || Modelo3IRSValidatorUtil.isNIFValid(nifPortugues))) {
                result.add(new DeclValidationMessage("H024", nifEntidadePagadoraLink, new String[]{nifEntidadePagadoraLink}));
            }
            this.validateH025(result, rostoModel, nifPortugues, nifEntidadePagadoraLink);
            this.validateH245(result, anexoHq04T4Linha, linha + 1);
            this.validateH246(result, anexoHq04T4Linha, linha + 1);
            this.validateH247(result, anexoHq04T4Linha, linha + 1);
            this.validateH248(result, anexoHq04T4Linha, linha + 1);
            this.validateH249(result, anexoHq04T4Linha, linha + 1);
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(codRendimento) && StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentoIliquido) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencaoIRS) && Modelo3IRSValidatorUtil.isEmptyOrZero(nifPortugues) && Modelo3IRSValidatorUtil.isEmptyOrZero(pais) && StringUtil.isEmpty(numFiscalUE)) {
                result.add(new DeclValidationMessage("H027", lineLink, new String[]{lineLink}));
            }
            this.validateH191(result, anexoHq04T4Linha, linha + 1);
        }
        long totalRendimentosIliquidos = quadro04AnexoHModel.getAnexoHq04C1() != null ? quadro04AnexoHModel.getAnexoHq04C1() : 0;
        if (totalRendimentosIliquidos != (totalRentimentosIliquidosPreenchidos = quadro04AnexoHModel.getTotalRendimentosIliquidosPreenchidos())) {
            result.add(new DeclValidationMessage("H184", "aAnexoH.qQuadro04.fanexoHq04C1", new String[]{"aAnexoH.qQuadro04.fanexoHq04C1"}));
        }
        if ((totalRetencaoIRS = quadro04AnexoHModel.getAnexoHq04C2() != null ? quadro04AnexoHModel.getAnexoHq04C2() : 0) != (totalRetencaoIRSPreenchido = quadro04AnexoHModel.getTotalRetencaoIRSPreenchido())) {
            result.add(new DeclValidationMessage("H185", "aAnexoH.qQuadro04.fanexoHq04C2", new String[]{"aAnexoH.qQuadro04.fanexoHq04C2"}));
        }
        return result;
    }

    protected void validateH019(ValidationResult result, Long anoExercicio, Long codRendimento, Long rendimentoIliquido, Long retencaoIRS, String rentencaoIRSLink) {
        if (!(retencaoIRS == null || rendimentoIliquido == null || codRendimento == null || anoExercicio == null || Modelo3IRSValidatorUtil.in(codRendimento, new long[]{408, 410, 403}) || ((float)retencaoIRS.longValue() <= (float)rendimentoIliquido.longValue() * 0.405f || anoExercicio >= 2013) && ((float)retencaoIRS.longValue() <= (float)rendimentoIliquido.longValue() * 0.465f || anoExercicio <= 2012))) {
            result.add(new DeclValidationMessage("H019", rentencaoIRSLink, new String[]{rentencaoIRSLink}));
        }
    }

    protected void validateH020(ValidationResult result, Long anoExercicio, Long codRendimento, Long rendimentoIliquido, Long retencaoIRS, String retencaoIRSLink) {
        long ano2010 = 2010;
        long ano2009 = 2009;
        long ano2013 = 2013;
        long ano2012 = 2012;
        if (retencaoIRS != null && rendimentoIliquido != null && ((float)retencaoIRS.longValue() > (float)rendimentoIliquido.longValue() * 0.205f && codRendimento != null && Modelo3IRSValidatorUtil.in(codRendimento, new long[]{408, 410, 403}) && Modelo3IRSv2015Parameters.instance().isFase2() && anoExercicio != null && anoExercicio < 2010 || (float)retencaoIRS.longValue() > (float)rendimentoIliquido.longValue() * 0.22f && codRendimento != null && Modelo3IRSValidatorUtil.in(codRendimento, new long[]{408, 410, 403}) && Modelo3IRSv2015Parameters.instance().isFase2() && anoExercicio != null && anoExercicio > 2009 && anoExercicio < 2013 || (float)retencaoIRS.longValue() > (float)rendimentoIliquido.longValue() * 0.265f && codRendimento != null && Modelo3IRSValidatorUtil.in(codRendimento, new long[]{408, 410, 403}) && Modelo3IRSv2015Parameters.instance().isFase2() && anoExercicio != null && anoExercicio > 2012)) {
            result.add(new DeclValidationMessage("H020", retencaoIRSLink, new String[]{retencaoIRSLink}));
        }
    }

    protected void validateH012(ValidationResult result, RostoModel rostoModel, AnexoHq04T4_Linha anexoHq04T4Linha, String titularLink) {
        String titular = anexoHq04T4Linha.getTitular();
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("H012", titularLink, new String[]{titularLink, "aRosto.qQuadro03"}));
        }
    }

    protected void validateH025(ValidationResult result, RostoModel rostoModel, Long nifPortugues, String nifEntidadePagadoraLink) {
        if (nifPortugues != null && (rostoModel.getQuadro03().isSujeitoPassivoA(nifPortugues) || rostoModel.getQuadro03().isSujeitoPassivoB(nifPortugues) || rostoModel.getQuadro03().existsInDependentes(nifPortugues) || rostoModel.getQuadro07().isConjugeFalecido(nifPortugues) || rostoModel.getQuadro07().existsInAscendentes(nifPortugues) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nifPortugues) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nifPortugues) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nifPortugues) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nifPortugues))) {
            result.add(new DeclValidationMessage("H025", nifEntidadePagadoraLink, new String[]{nifEntidadePagadoraLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected void validateH011(ValidationResult result, AnexoHq04T4_Linha anexoHq04T4Linha, String titularLink) {
        String titular = anexoHq04T4Linha.getTitular();
        if (!(titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro03.isDependenteEmGuardaConjunta(titular) || Quadro07.isConjugeFalecido(titular))) {
            result.add(new DeclValidationMessage("H011", titularLink, new String[]{titularLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateH191(ValidationResult result, AnexoHq04T4_Linha current, int numLinha) {
        Long codRendimento = current.getCodRendimentos();
        String titular = current.getTitular();
        Long rendimentoIliquido = current.getRendimentosIliquidos();
        Long nifPortugues = current.getNifPortugues();
        Long pais = current.getPais();
        Long retencao = current.getRentencaoIRS();
        String numFiscalUE = current.getNumeroFiscalUE();
        if (!(codRendimento != null || StringUtil.isEmpty(titular) && (rendimentoIliquido == null || rendimentoIliquido <= 0) && (retencao == null || retencao <= 0) && nifPortugues == null && pais == null && StringUtil.isEmpty(numFiscalUE))) {
            String codRendimentosLink = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.CODRENDIMENTOS);
            result.add(new DeclValidationMessage("H191", codRendimentosLink, new String[]{codRendimentosLink}));
        }
    }

    public void validateH245(ValidationResult result, AnexoHq04T4_Linha current, int numLinha) {
        Long pais = current.getPais();
        ListMap paises = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxH_Q04.class.getSimpleName());
        String paisLink = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.PAIS);
        if (pais != null && paises.get(pais) == null) {
            result.add(new DeclValidationMessage("H245", paisLink, new String[]{paisLink}));
        }
    }

    public void validateH246(ValidationResult result, AnexoHq04T4_Linha current, int numLinha) {
        Long pais = current.getPais();
        String numFiscalUE = current.getNumeroFiscalUE();
        String paisLink = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.PAIS);
        String linkNumFiscalUE = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.NUMEROFISCALUE);
        if (pais != null && StringUtil.isEmpty(numFiscalUE) || pais == null && !StringUtil.isEmpty(numFiscalUE)) {
            result.add(new DeclValidationMessage("H246", paisLink, new String[]{paisLink, linkNumFiscalUE}));
        }
    }

    public void validateH247(ValidationResult result, AnexoHq04T4_Linha current, int numLinha) {
        Long pais = current.getPais();
        String numFiscalUE = current.getNumeroFiscalUE();
        String paisLink = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.PAIS);
        String linkNumFiscalUE = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.NUMEROFISCALUE);
        if (pais != null && (StringUtil.isEmpty(numFiscalUE) || !StringUtil.isEmpty(numFiscalUE) && "0".equals(numFiscalUE))) {
            result.add(new DeclValidationMessage("H247", linkNumFiscalUE, new String[]{linkNumFiscalUE, paisLink}));
        }
    }

    public void validateH248(ValidationResult result, AnexoHq04T4_Linha current, int numLinha) {
        Long pais = current.getPais();
        String numFiscalUE = current.getNumeroFiscalUE();
        String linkNumFiscalUE = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.NUMEROFISCALUE);
        if (pais != null && !StringUtil.isEmpty(numFiscalUE) && StringUtil.checkCharactertAt(numFiscalUE, ' ', 0)) {
            result.add(new DeclValidationMessage("H248", linkNumFiscalUE, new String[]{linkNumFiscalUE}));
        }
    }

    public void validateH249(ValidationResult result, AnexoHq04T4_Linha current, int numLinha) {
        String linkNumFiscalUE = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.NUMEROFISCALUE);
        String linkNifPT = AnexoHq04T4_Linha.getLink(numLinha, AnexoHq04T4_LinhaBase.Property.NIFPORTUGUES);
        String numFiscalUE = current.getNumeroFiscalUE();
        Long nif = current.getNifPortugues();
        if (!(StringUtil.isEmpty(numFiscalUE) || nif == null)) {
            result.add(new DeclValidationMessage("H249", "aAnexoH.qQuadro04.tanexoHq04T4", new String[]{linkNifPT, linkNumFiscalUE}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro04 quadro04AnexoHModel = anexoHModel.getQuadro04();
        for (int linha = 0; linha < quadro04AnexoHModel.getAnexoHq04T4().size(); ++linha) {
            AnexoHq04T4_Linha anexoHq04T4Linha = quadro04AnexoHModel.getAnexoHq04T4().get(linha);
            Long codRendimento = anexoHq04T4Linha.getCodRendimentos();
            String titular = anexoHq04T4Linha.getTitular();
            Long rendimentoIliquido = anexoHq04T4Linha.getRendimentosIliquidos();
            String lineLink = AnexoHq04T4_Linha.getLink(linha + 1);
            String codRendimentosLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.CODRENDIMENTOS.getIndex();
            String titularLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.TITULAR.getIndex();
            String rendimentoIliquidoLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.RENDIMENTOSILIQUIDOS.getIndex();
            this.validateH190(result, anexoHq04T4Linha, linha + 1);
            this.validateH290(result, anexoHq04T4Linha, linha + 1);
            if (codRendimento != null && codRendimento == 402 && (StringUtil.isEmpty(titular) || rendimentoIliquido == null || rendimentoIliquido == 0)) {
                result.add(new DeclValidationMessage("H243", codRendimentosLink, new String[]{codRendimentosLink, titularLink, rendimentoIliquidoLink}));
            }
            this.validateH292(result, anexoHq04T4Linha, linha + 1);
        }
        return result;
    }

    protected void validateH190(ValidationResult result, AnexoHq04T4_Linha anexoHq04T4Linha, int numLinha) {
        Long codRendimento = anexoHq04T4Linha.getCodRendimentos();
        String titular = anexoHq04T4Linha.getTitular();
        Long rendimentoIliquido = anexoHq04T4Linha.getRendimentosIliquidos();
        Long nifPortugues = anexoHq04T4Linha.getNifPortugues();
        Long retencoesDeIRS = anexoHq04T4Linha.getRentencaoIRS();
        String numFiscalUE = anexoHq04T4Linha.getNumeroFiscalUE();
        String lineLink = "aAnexoH.qQuadro04.tanexoHq04T4.l" + numLinha;
        String codRendimentosLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String titularLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.TITULAR.getIndex();
        String rendimentoIliquidoLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.RENDIMENTOSILIQUIDOS.getIndex();
        String nifEntidadePagadoraLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.NIFPORTUGUES.getIndex();
        String numFiscalUELink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.NUMEROFISCALUE.getIndex();
        if (codRendimento != null && codRendimento != 402 && (StringUtil.isEmpty(titular) || nifPortugues == null && StringUtil.isEmpty(numFiscalUE) || Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentoIliquido)) && Modelo3IRSValidatorUtil.isEmptyOrZero(retencoesDeIRS)) {
            result.add(new DeclValidationMessage("H190", codRendimentosLink, new String[]{codRendimentosLink, titularLink, rendimentoIliquidoLink, nifEntidadePagadoraLink, numFiscalUELink}));
        }
    }

    protected void validateH290(ValidationResult result, AnexoHq04T4_Linha anexoHq04T4Linha, int numLinha) {
        Long codRendimento = anexoHq04T4Linha.getCodRendimentos();
        String titular = anexoHq04T4Linha.getTitular();
        Long rendimentoIliquido = anexoHq04T4Linha.getRendimentosIliquidos();
        Long nifPortugues = anexoHq04T4Linha.getNifPortugues();
        Long retencoesDeIRS = anexoHq04T4Linha.getRentencaoIRS();
        String lineLink = "aAnexoH.qQuadro04.tanexoHq04T4.l" + numLinha;
        String codRendimentosLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String titularLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.TITULAR.getIndex();
        String rendimentoIliquidoLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.RENDIMENTOSILIQUIDOS.getIndex();
        String nifEntidadePagadoraLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.NIFPORTUGUES.getIndex();
        if (codRendimento != null && codRendimento != 402 && (StringUtil.isEmpty(titular) || nifPortugues == null || Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentoIliquido)) && !Modelo3IRSValidatorUtil.isEmptyOrZero(retencoesDeIRS)) {
            result.add(new DeclValidationMessage("H290", codRendimentosLink, new String[]{codRendimentosLink, titularLink, rendimentoIliquidoLink, nifEntidadePagadoraLink}));
        }
    }

    protected void validateH292(ValidationResult result, AnexoHq04T4_Linha anexoHq04T4Linha, int numLinha) {
        Long codRendimento = anexoHq04T4Linha.getCodRendimentos();
        String titular = anexoHq04T4Linha.getTitular();
        Long rendimentoIliquido = anexoHq04T4Linha.getRendimentosIliquidos();
        Long retencoesDeIRS = anexoHq04T4Linha.getRentencaoIRS();
        Long nifPortugues = anexoHq04T4Linha.getNifPortugues();
        String lineLink = "aAnexoH.qQuadro04.tanexoHq04T4.l" + numLinha;
        String codRendimentosLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String nifEntidadePagadoraLink = lineLink + ".c" + AnexoHq04T4_LinhaBase.Property.NIFPORTUGUES.getIndex();
        if (!(codRendimento == null || !codRendimento.equals(402) || StringUtil.isEmpty(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rendimentoIliquido) || Modelo3IRSValidatorUtil.isEmptyOrZero(retencoesDeIRS) || nifPortugues != null)) {
            result.add(new DeclValidationMessage("H292", codRendimentosLink, new String[]{nifEntidadePagadoraLink}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

