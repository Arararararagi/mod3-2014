/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro10;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro10Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro10Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro10 quadro10Model = anexoHModel.getQuadro10();
        long somaControloColecta = 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1001() != null ? quadro10Model.getAnexoHq10C1001() : 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1002() != null ? quadro10Model.getAnexoHq10C1002() : 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1003() != null ? quadro10Model.getAnexoHq10C1003() : 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1004() != null ? quadro10Model.getAnexoHq10C1004() : 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1005() != null ? quadro10Model.getAnexoHq10C1005() : 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1006() != null ? quadro10Model.getAnexoHq10C1006() : 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1007() != null ? quadro10Model.getAnexoHq10C1007() : 0;
        somaControloColecta+=quadro10Model.getAnexoHq10C1008() != null ? quadro10Model.getAnexoHq10C1008() : 0;
        if (quadro10Model.getAnexoHq10C1() != null && quadro10Model.getAnexoHq10C1() != somaControloColecta || quadro10Model.getAnexoHq10C1() == null && (somaControloColecta+=quadro10Model.getAnexoHq10C1009() != null ? quadro10Model.getAnexoHq10C1009() : 0) != 0) {
            result.add(new DeclValidationMessage("H180", "aAnexoH.qQuadro10.fanexoHq10C1", new String[]{"aAnexoH.qQuadro10.fanexoHq10C1"}));
        }
        long somaControloRendimento = 0;
        somaControloRendimento+=quadro10Model.getAnexoHq10C1001a() != null ? quadro10Model.getAnexoHq10C1001a() : 0;
        somaControloRendimento+=quadro10Model.getAnexoHq10C1002a() != null ? quadro10Model.getAnexoHq10C1002a() : 0;
        somaControloRendimento+=quadro10Model.getAnexoHq10C1003a() != null ? quadro10Model.getAnexoHq10C1003a() : 0;
        somaControloRendimento+=quadro10Model.getAnexoHq10C1004a() != null ? quadro10Model.getAnexoHq10C1004a() : 0;
        somaControloRendimento+=quadro10Model.getAnexoHq10C1005a() != null ? quadro10Model.getAnexoHq10C1005a() : 0;
        somaControloRendimento+=quadro10Model.getAnexoHq10C1006a() != null ? quadro10Model.getAnexoHq10C1006a() : 0;
        somaControloRendimento+=quadro10Model.getAnexoHq10C1007a() != null ? quadro10Model.getAnexoHq10C1007a() : 0;
        if (quadro10Model.getAnexoHq10C1a() != null && quadro10Model.getAnexoHq10C1a() != somaControloRendimento || quadro10Model.getAnexoHq10C1a() == null && (somaControloRendimento+=quadro10Model.getAnexoHq10C1008a() != null ? quadro10Model.getAnexoHq10C1008a() : 0) != 0) {
            result.add(new DeclValidationMessage("H181", "aAnexoH.qQuadro10.fanexoHq10C1a", new String[]{"aAnexoH.qQuadro10.fanexoHq10C1a"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

