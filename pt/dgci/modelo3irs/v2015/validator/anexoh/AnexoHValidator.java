/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro10;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class AnexoHValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoHValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro04 quadro04Model = anexoHModel.getQuadro04();
        Quadro10 quadro10Model = anexoHModel.getQuadro10();
        if (anexoHModel.isEmpty()) {
            result.add(new DeclValidationMessage("H001", "aAnexoH", new String[]{"aAnexoH"}));
        }
        if (model.getAnexos().size() == 2 && quadro04Model.isEmpty() && quadro10Model.isEmpty()) {
            result.add(new DeclValidationMessage("H002", "aAnexoH", new String[]{"aAnexoH.qQuadro04", "aAnexoH.qQuadro10"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

