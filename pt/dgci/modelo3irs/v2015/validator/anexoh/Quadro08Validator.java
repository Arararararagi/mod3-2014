/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Freguesia;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxH;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro08Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro08Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        AnexoAModel anexoAModel = (AnexoAModel)model.getAnexo(AnexoAModel.class);
        Quadro08 quadro08Model = (Quadro08)anexoHModel.getQuadro(Quadro08.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        this.validateDespesasSaude(result, rostoModel, anexoHModel, anexoAModel);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        if (quadro08Model.getAnexoHq08T814().size() > 16) {
            result.add(new DeclValidationMessage("H132", "aAnexoH.qQuadro08.tanexoHq08T814", new String[]{"aAnexoH.qQuadro08.tanexoHq08T814"}));
        }
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        for (int linha = 0; linha < quadro08Model.getAnexoHq08T814().size(); ++linha) {
            Tuple predioTuple;
            AnexoHq08T814_Linha anexoHq08T814Linha = quadro08Model.getAnexoHq08T814().get(linha);
            Long codigo = anexoHq08T814Linha.getCodigo();
            String freguesia = anexoHq08T814Linha.getFreguesia();
            String tipoPredio = anexoHq08T814Linha.getTipoPredio();
            Long artigo = anexoHq08T814Linha.getArtigo();
            String fraccao = anexoHq08T814Linha.getFraccao();
            String titular = anexoHq08T814Linha.getTitular();
            String habitacaoPermanenteArrendada = anexoHq08T814Linha.getHabitacaoPermanenteArrendada();
            Long nifArrendatario = anexoHq08T814Linha.getNIFArrendatario();
            String classificacaoA = anexoHq08T814Linha.getClassificacaoA();
            String linkCurrentLine = "aAnexoH.qQuadro08.tanexoHq08T814.l" + (linha + 1);
            String linkCodigo = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.CODIGO.getIndex();
            String linkFreguesia = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.FREGUESIA.getIndex();
            String linkTipoPredio = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.TIPOPREDIO.getIndex();
            String linkArtigo = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.ARTIGO.getIndex();
            String linkFraccao = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.FRACCAO.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.TITULAR.getIndex();
            String linkHabitacaoPermanenteArrendada = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.HABITACAOPERMANENTEARRENDADA.getIndex();
            String linkNifArrendatario = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.NIFARRENDATARIO.getIndex();
            String linkClassificacaoA = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.CLASSIFICACAOA.getIndex();
            if (!(freguesia == null || "O".equals(tipoPredio) || "A".equals(habitacaoPermanenteArrendada))) {
                Object[] arrobject = new Object[7];
                arrobject[0] = codigo;
                arrobject[1] = freguesia;
                arrobject[2] = tipoPredio;
                arrobject[3] = artigo;
                arrobject[4] = fraccao != null ? fraccao.toLowerCase() : fraccao;
                arrobject[5] = titular;
                arrobject[6] = habitacaoPermanenteArrendada;
                predioTuple = new Tuple(arrobject);
                if (processedTuples.contains(predioTuple)) {
                    result.add(new DeclValidationMessage("H133", linkCurrentLine, new String[]{linkCurrentLine}));
                } else {
                    processedTuples.add(predioTuple);
                }
            }
            if (!(freguesia == null || "O".equals(tipoPredio) || "P".equals(habitacaoPermanenteArrendada))) {
                Object[] arrobject = new Object[8];
                arrobject[0] = codigo;
                arrobject[1] = freguesia;
                arrobject[2] = tipoPredio;
                arrobject[3] = artigo;
                arrobject[4] = fraccao != null ? fraccao.toLowerCase() : fraccao;
                arrobject[5] = titular;
                arrobject[6] = habitacaoPermanenteArrendada;
                arrobject[7] = nifArrendatario;
                predioTuple = new Tuple(arrobject);
                if (processedTuples.contains(predioTuple)) {
                    result.add(new DeclValidationMessage("H242", linkCurrentLine, new String[]{linkCurrentLine}));
                } else {
                    processedTuples.add(predioTuple);
                }
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(codigo) && StringUtil.isEmpty(freguesia) && StringUtil.isEmpty(tipoPredio) && Modelo3IRSValidatorUtil.isEmptyOrZero(artigo) && StringUtil.isEmpty(fraccao) && StringUtil.isEmpty(titular) && StringUtil.isEmpty(habitacaoPermanenteArrendada) && Modelo3IRSValidatorUtil.isEmptyOrZero(nifArrendatario) && StringUtil.isEmpty(classificacaoA)) {
                result.add(new DeclValidationMessage("H134", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            this.validateH135(result, anexoHq08T814Linha, linha + 1);
            ListMap freguesiasCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
            if (!(freguesia == null || freguesiasCatalog.containsKey(freguesia))) {
                result.add(new DeclValidationMessage("H144", linkFreguesia, new String[]{linkFreguesia}));
            }
            if (!(tipoPredio == null || StringUtil.in(tipoPredio, new String[]{"U", "O"}))) {
                result.add(new DeclValidationMessage("H147", linkTipoPredio, new String[]{linkTipoPredio}));
            }
            this.validateH282(result, rostoModel, anexoHq08T814Linha, linha + 1);
            if (tipoPredio != null && freguesia == null) {
                result.add(new DeclValidationMessage("H148", linkTipoPredio, new String[]{linkTipoPredio, linkFreguesia}));
            }
            if (!(tipoPredio == null || StringUtil.in(tipoPredio, new String[]{"O"}) || artigo != null)) {
                result.add(new DeclValidationMessage("H149", linkTipoPredio, new String[]{linkTipoPredio, linkArtigo}));
            }
            if (artigo != null && freguesia == null) {
                result.add(new DeclValidationMessage("H150", linkArtigo, new String[]{linkArtigo, linkFreguesia}));
            }
            if (fraccao != null && freguesia == null) {
                result.add(new DeclValidationMessage("H151", linkFraccao, new String[]{linkFraccao, linkFreguesia}));
            }
            if (!(titular == null || titular.equals("A") || titular.equals("B") || StringUtil.in(titular, Quadro03.COD_TITULARES_DEPENDENTES) || StringUtil.in(titular, Quadro03.COD_TITULARES_DEPENDENTES_DEFICIENTES) || titular.equals("F") || titular.equals("C"))) {
                result.add(new DeclValidationMessage("H152", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03C03"}));
            }
            if (!(habitacaoPermanenteArrendada == null || StringUtil.in(habitacaoPermanenteArrendada, new String[]{"A", "P"}))) {
                result.add(new DeclValidationMessage("H188", linkHabitacaoPermanenteArrendada, new String[]{linkHabitacaoPermanenteArrendada}));
            }
            if (!(titular == null || rostoModel.isNIFTitularPreenchido(titular))) {
                result.add(new DeclValidationMessage("H153", linkTitular, new String[]{linkTitular}));
            }
            if (titular != null && codigo == null) {
                result.add(new DeclValidationMessage("H154", linkTitular, new String[]{linkCodigo, linkTitular}));
            }
            if (!(nifArrendatario == null || Modelo3IRSValidatorUtil.isNIFValid(nifArrendatario))) {
                result.add(new DeclValidationMessage("H159", linkNifArrendatario, new String[]{linkNifArrendatario}));
            }
            if (nifArrendatario != null && (rostoModel.getQuadro03().isSujeitoPassivoA(nifArrendatario) || rostoModel.getQuadro03().isSujeitoPassivoB(nifArrendatario) || rostoModel.getQuadro03().existsInDependentes(nifArrendatario) || rostoModel.getQuadro07().isConjugeFalecido(nifArrendatario) || rostoModel.getQuadro07().existsInAscendentes(nifArrendatario) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nifArrendatario))) {
                result.add(new DeclValidationMessage("H235", "aAnexoH.qQuadro08", new String[]{linkNifArrendatario, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", rostoModel.getLinkByNifForDependente(nifArrendatario, "aRosto.qQuadro03.fq03CD1"), rostoModel.getLinkByNifForAscendentes(nifArrendatario, "aRosto.qQuadro07.fq07C01"), "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.trostoq07CT1"}));
            }
            this.validateH160(result, anoExercicio, classificacaoA, linkClassificacaoA);
            if (!(classificacaoA == null || StringUtil.in(classificacaoA, new String[]{"S", "N"}))) {
                result.add(new DeclValidationMessage("H195", linkClassificacaoA, new String[]{linkClassificacaoA}));
            }
            if (classificacaoA != null && codigo == null) {
                result.add(new DeclValidationMessage("H161", linkClassificacaoA, new String[]{linkClassificacaoA, linkCodigo}));
            }
            if (!(fraccao == null || Pattern.matches("[a-zA-Z0-9 ]*", (CharSequence)fraccao))) {
                result.add(new DeclValidationMessage("H196", linkFraccao, new String[]{linkFraccao}));
            }
            if (nifArrendatario != null && anexoHq08T814Linha.getCodigo() != null && anexoHq08T814Linha.getCodigo() != 741 && (StringUtil.isEmpty(habitacaoPermanenteArrendada) || !habitacaoPermanenteArrendada.equals("A"))) {
                result.add(new DeclValidationMessage("H193", linkNifArrendatario, new String[]{linkNifArrendatario, linkHabitacaoPermanenteArrendada}));
            }
            this.validateH255(result, anexoHq08T814Linha, linha + 1);
            this.validateH262(result, anexoHq08T814Linha, linha + 1);
            if (!(nifArrendatario == null || codigo == null || (Modelo3IRSValidatorUtil.startsWith(nifArrendatario, "1", "2") || codigo == 741) && (Modelo3IRSValidatorUtil.startsWith(nifArrendatario, "5", "7", "9") || codigo != 741))) {
                result.add(new DeclValidationMessage("H201", linkNifArrendatario, new String[]{linkNifArrendatario}));
            }
            if (codigo == null && habitacaoPermanenteArrendada != null && StringUtil.in(habitacaoPermanenteArrendada, new String[]{"A", "P"})) {
                result.add(new DeclValidationMessage("H157", linkHabitacaoPermanenteArrendada, new String[]{linkCodigo, linkHabitacaoPermanenteArrendada, linkHabitacaoPermanenteArrendada}));
            }
            this.validateH259(result, anexoHq08T814Linha, linha + 1);
            if (habitacaoPermanenteArrendada == null || !habitacaoPermanenteArrendada.equals("A") || nifArrendatario != null || anexoHq08T814Linha.getCodigo() == null || anexoHq08T814Linha.getCodigo().equals(741)) continue;
            result.add(new DeclValidationMessage("H158", linkNifArrendatario, new String[]{linkNifArrendatario}));
        }
        this.validateH200(result, anexoHModel);
        this.validateH202(result, rostoModel, quadro08Model);
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxH.class.getSimpleName());
        if (!(quadro08Model.getAnexoHq08C814() == null || paisesCatalog.containsKey(quadro08Model.getAnexoHq08C814()))) {
            result.add(new DeclValidationMessage("H162", "aAnexoH.qQuadro08.fanexoHq08C814", new String[]{"aAnexoH.qQuadro08.fanexoHq08C814"}));
        }
        this.validateH199(result, quadro08Model);
        long valor = quadro08Model.getAnexoHq08C81501() != null ? quadro08Model.getAnexoHq08C81501() : 0;
        long valorRenda = quadro08Model.getAnexoHq08C81502() != null ? quadro08Model.getAnexoHq08C81502() : 0;
        long totalImportanciasAplicadasForCodBeneficio732 = anexoHModel.getQuadro07().getTotalImportanciaAplicadaByCodigoBeneficio(732);
        if ((quadro08Model.getAnexoHq08C81501() != null || quadro08Model.getAnexoHq08C81502() != null) && valorRenda - valor != totalImportanciasAplicadasForCodBeneficio732) {
            result.add(new DeclValidationMessage("H166", "aAnexoH.qQuadro08.fanexoHq08C81502", new String[]{"aAnexoH.qQuadro08.fanexoHq08C81502", "aAnexoH.qQuadro08.fanexoHq08C81501", "aAnexoH.qQuadro07.tanexoHq07T7"}));
        }
        if (valorRenda > 0 && valorRenda < valor) {
            result.add(new DeclValidationMessage("H167", "aAnexoH.qQuadro08.fanexoHq08C81502", new String[]{"aAnexoH.qQuadro08.fanexoHq08C81502", "aAnexoH.qQuadro08.fanexoHq08C81501"}));
        }
        boolean hasCodBeneficio732InQuadro07 = anexoHModel.getQuadro07().existsAtLeastOneCodigoBeneficio(new long[]{732});
        if (quadro08Model.getAnexoHq08C81502() != null && quadro08Model.getAnexoHq08C81502() == 0 && (quadro08Model.getAnexoHq08C81501() == null || quadro08Model.getAnexoHq08C81501() != null && quadro08Model.getAnexoHq08C81501() == 0) && hasCodBeneficio732InQuadro07) {
            result.add(new DeclValidationMessage("H168", "aAnexoH.qQuadro08.fanexoHq08C81502", new String[]{"aAnexoH.qQuadro08.fanexoHq08C81502", "aAnexoH.qQuadro08.fanexoHq08C81501", "aAnexoH.qQuadro07.tanexoHq07T7"}));
        }
        if (hasCodBeneficio732InQuadro07 && totalImportanciasAplicadasForCodBeneficio732 == 0 && quadro08Model.getAnexoHq08C81502() != null && quadro08Model.getAnexoHq08C81502() == 0 && quadro08Model.getAnexoHq08C81501() != null && quadro08Model.getAnexoHq08C81501() == 0) {
            result.add(new DeclValidationMessage("H169", "aAnexoH.qQuadro08.fanexoHq08C81502", new String[]{"aAnexoH.qQuadro07.tanexoHq07T7", "aAnexoH.qQuadro08.fanexoHq08C81501", "aAnexoH.qQuadro08.fanexoHq08C81502"}));
        }
        if (quadro08Model.getAnexoHq08C81501() != null && quadro08Model.getAnexoHq08C81502() == null) {
            result.add(new DeclValidationMessage("H170", "aAnexoH.qQuadro08.fanexoHq08C81502", new String[]{"aAnexoH.qQuadro08.fanexoHq08C81502"}));
        }
        this.validateH284(result, rostoModel, anexoHModel);
        this.validateH283(result, anexoHModel);
        return result;
    }

    protected void validateH202(ValidationResult result, RostoModel rostoModel, Quadro08 quadro08Model) {
        Long codigo;
        Long periodo = rostoModel.getQuadro02().getQ02C02();
        if (quadro08Model.getAnexoHq08C814() != null && quadro08Model.getAnexoHq08T814() != null && quadro08Model.getAnexoHq08T814().size() == 1 && (codigo = quadro08Model.getAnexoHq08T814().get(0).getCodigo()) != null && (codigo.equals(736) || codigo.equals(739) && periodo != null && periodo < 2012)) {
            result.add(new DeclValidationMessage("H202", "aAnexoH.qQuadro08.fanexoHq08C814", new String[]{"aAnexoH.qQuadro08.fanexoHq08C814", "aAnexoH.qQuadro08.tanexoHq08T814"}));
        }
    }

    protected void validateH199(ValidationResult result, Quadro08 quadro08Model) {
        if (!(quadro08Model.getAnexoHq08C814() == null || Modelo3IRSValidatorUtil.in(quadro08Model.getAnexoHq08C814(), new long[]{276, 40, 56, 100, 203, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 352, 380, 428, 438, 440, 442, 470, 578, 528, 616, 642, 826, 752}))) {
            result.add(new DeclValidationMessage("H199", "aAnexoH.qQuadro08.fanexoHq08C814", new String[]{"aAnexoH.qQuadro08.fanexoHq08C814"}));
        }
    }

    protected void validateH200(ValidationResult resultLocal, AnexoHModel anexoHModel) {
        Quadro08 quadro08Model = anexoHModel.getQuadro08();
        if (!(quadro08Model.getAnexoHq08C814() == null || anexoHModel.getQuadro07().existsAtLeastOneCodigoBeneficio(new long[]{731, 739, 741}) || quadro08Model.existsAtLeastOneCodigo(new long[]{731, 739, 741}))) {
            resultLocal.add(new DeclValidationMessage("H200", "aAnexoH.qQuadro08.fanexoHq08C814", new String[]{"aAnexoH.qQuadro08.fanexoHq08C814", "aAnexoH.qQuadro07.tanexoHq07T7"}));
        }
    }

    protected void validateH160(ValidationResult result, Long anoExercicio, String classificacaoA, String linkClassificacaoA) {
        if (!StringUtil.isEmpty(classificacaoA) && "S".equals(classificacaoA) && anoExercicio != null && (anoExercicio < 2008 || anoExercicio > 2011)) {
            result.add(new DeclValidationMessage("H160", linkClassificacaoA, new String[]{linkClassificacaoA, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateDespesasSaude(ValidationResult result, RostoModel rostoModel, AnexoHModel anexoHModel, AnexoAModel anexoAModel) {
        if (anexoHModel != null && anexoHModel.getQuadro08() != null && anexoHModel.getQuadro08().getAnexoHq08T1() != null) {
            EventList<AnexoHq08T1_Linha> despesasSaudeLinhas = anexoHModel.getQuadro08().getAnexoHq08T1();
            HashSet<String> beneficiarios = new HashSet<String>();
            for (int idxLinha = 0; idxLinha < despesasSaudeLinhas.size(); ++idxLinha) {
                AnexoHq08T1_Linha linha = despesasSaudeLinhas.get(idxLinha);
                this.validateH278(result, linha, idxLinha + 1, beneficiarios);
                this.validateH269(result, linha, idxLinha + 1);
                this.validateH270(result, linha, idxLinha + 1);
                this.validateH271(result, linha, idxLinha + 1);
                this.validateH272(result, linha, idxLinha + 1);
                this.validateH273(result, linha, idxLinha + 1);
                this.validateH274(result, rostoModel, linha, idxLinha + 1);
                this.validateH276(result, linha, idxLinha + 1);
            }
            this.validateH281(result, rostoModel, anexoHModel);
        }
    }

    protected void validateH278(ValidationResult result, AnexoHq08T1_Linha linha, int linhaPos, Set<String> beneficiarios) {
        if (!(linha.getNIF() == null || beneficiarios.add(linha.getNIF()))) {
            result.add(new DeclValidationMessage("H278", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{Quadro08.getLinkCellForANEXOHQ08T1_LINK(linhaPos, AnexoHq08T1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateH269(ValidationResult result, AnexoHq08T1_Linha linha, int linhaPos) {
        if (StringUtil.isEmpty(linha.getNIF()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C801()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C802()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C803())) {
            result.add(new DeclValidationMessage("H269", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{Quadro08.getLinkCellForANEXOHQ08T1_LINK(linhaPos, AnexoHq08T1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateH281(ValidationResult result, RostoModel rostoModel, AnexoHModel anexoHModel) {
        if (!(anexoHModel == null || anexoHModel.getQuadro08().getAnexoHq08T1() == null || anexoHModel.getQuadro08().getAnexoHq08T1().isEmpty() || !rostoModel.getQuadro05().isQ05B1OP4Selected() || rostoModel.getQuadro05().isQ05B3OP9Selected())) {
            boolean hasNonEmptyLines = false;
            for (int idxLinha = 0; idxLinha < anexoHModel.getQuadro08().getAnexoHq08T1().size(); ++idxLinha) {
                AnexoHq08T1_Linha linha = anexoHModel.getQuadro08().getAnexoHq08T1().get(idxLinha);
                if (StringUtil.isEmpty(linha.getNIF()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C801()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C802()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C803())) continue;
                hasNonEmptyLines = true;
            }
            if (hasNonEmptyLines) {
                result.add(new DeclValidationMessage("H281", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{"aAnexoH.qQuadro08.tanexoHq08T1"}));
            }
        }
    }

    protected void validateH270(ValidationResult result, AnexoHq08T1_Linha linha, int linhaPos) {
        String titular = linha.getNIF();
        if (!((!Modelo3IRSValidatorUtil.isGreaterThanZero(linha.getAnexoHq08C801()) || StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isAscendentesEmComunhao(titular) || Quadro07.isAfilhadosCivisEmComunhao(titular) || Quadro07.isAscendentesEColaterais(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular)) && (!Modelo3IRSValidatorUtil.isGreaterThanZero(linha.getAnexoHq08C802()) || StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isAscendentesEmComunhao(titular) || Quadro07.isAscendentesEColaterais(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular)))) {
            result.add(new DeclValidationMessage("H270", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{Quadro08.getLinkCellForANEXOHQ08T1_LINK(linhaPos, AnexoHq08T1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateH271(ValidationResult result, AnexoHq08T1_Linha linha, int linhaPos) {
        String titular = linha.getNIF();
        if (!(!Modelo3IRSValidatorUtil.isGreaterThanZero(linha.getAnexoHq08C803()) || StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro03.isDependenteEmGuardaConjunta(titular) || Quadro07.isAfilhadosCivisEmComunhao(titular) || Quadro07.isConjugeFalecido(titular))) {
            result.add(new DeclValidationMessage("H271", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{Quadro08.getLinkCellForANEXOHQ08T1_LINK(linhaPos, AnexoHq08T1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateH272(ValidationResult result, AnexoHq08T1_Linha linha, int linhaPos) {
        if (linha.getNIF() != null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C801()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C802()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnexoHq08C803())) {
            result.add(new DeclValidationMessage("H272", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{Quadro08.getLinkCellForANEXOHQ08T1_LINK(linhaPos, AnexoHq08T1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateH273(ValidationResult result, AnexoHq08T1_Linha linha, int linhaPos) {
        if (StringUtil.isEmpty(linha.getNIF()) && (Modelo3IRSValidatorUtil.isGreaterThanZero(linha.getAnexoHq08C801()) || Modelo3IRSValidatorUtil.isGreaterThanZero(linha.getAnexoHq08C802()) || Modelo3IRSValidatorUtil.isGreaterThanZero(linha.getAnexoHq08C803()))) {
            result.add(new DeclValidationMessage("H273", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{Quadro08.getLinkCellForANEXOHQ08T1_LINK(linhaPos, AnexoHq08T1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateH274(ValidationResult result, RostoModel rostoModel, AnexoHq08T1_Linha linha, int linhaPos) {
        String titular = linha.getNIF();
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("H274", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{Quadro08.getLinkCellForANEXOHQ08T1_LINK(linhaPos, AnexoHq08T1_LinhaBase.Property.NIF), "aRosto.qQuadro03"}));
        }
    }

    protected void validateH276(ValidationResult result, AnexoHq08T1_Linha linha, int linhaPos) {
        String titular = linha.getNIF();
        if (!(titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro03.isDependenteEmGuardaConjunta(titular) || Quadro07.isAfilhadosCivisEmComunhao(titular) || Quadro07.isAscendentesEmComunhao(titular) || Quadro07.isAscendentesEColaterais(titular) || Quadro07.isConjugeFalecido(titular))) {
            result.add(new DeclValidationMessage("H276", "aAnexoH.qQuadro08.tanexoHq08T1", new String[]{"aAnexoH.qQuadro08.tanexoHq08T1", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07ET1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateH255(ValidationResult result, AnexoHq08T814_Linha current, int numLinha) {
        Long nifArrendatario = current.getNIFArrendatario();
        Long codigo = current.getCodigo();
        String habitacaoPermanente = current.getHabitacaoPermanenteArrendada();
        if (!(nifArrendatario == null || codigo == null || codigo != 741 || StringUtil.isEmpty(habitacaoPermanente) || habitacaoPermanente.equals("P"))) {
            String arrendatarioLink = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.NIFARRENDATARIO);
            String habitacaoPermanenteLink = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.HABITACAOPERMANENTEARRENDADA);
            result.add(new DeclValidationMessage("H255", arrendatarioLink, new String[]{arrendatarioLink, habitacaoPermanenteLink}));
        }
    }

    protected void validateH135(ValidationResult result, AnexoHq08T814_Linha current, int numLinha) {
        Long codigo = current.getCodigo();
        if (!(codigo == null || Modelo3IRSValidatorUtil.in(codigo, new long[]{731, 736, 739, 741}))) {
            String linkCodigo = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.CODIGO);
            result.add(new DeclValidationMessage("H135", linkCodigo, new String[]{linkCodigo}));
        }
    }

    protected void validateH282(ValidationResult result, RostoModel rostoModel, AnexoHq08T814_Linha linha, int numLinha) {
        Long periodo = rostoModel.getQuadro02().getQ02C02();
        Long codigo = linha.getCodigo();
        String tipoPredio = linha.getTipoPredio();
        if (codigo != null && tipoPredio != null && (codigo.equals(736) || codigo.equals(739) && periodo != null && periodo < 2012) && tipoPredio.equals("O")) {
            String linkCodigo = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.CODIGO);
            String linkTipoPredio = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.TIPOPREDIO);
            result.add(new DeclValidationMessage("H282", linkCodigo, new String[]{linkCodigo, linkTipoPredio}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        if (anexoHModel == null) {
            return result;
        }
        Long anoRendimentos = null;
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (rostoModel != null && rostoModel.getQuadro02() != null) {
            anoRendimentos = rostoModel.getQuadro02().getQ02C02();
        }
        Quadro08 quadro08Model = (Quadro08)anexoHModel.getQuadro(Quadro08.class.getSimpleName());
        for (int linha = 0; linha < quadro08Model.getAnexoHq08T814().size(); ++linha) {
            AnexoHq08T814_Linha anexoHq08T814Linha = quadro08Model.getAnexoHq08T814().get(linha);
            Long codigo = anexoHq08T814Linha.getCodigo();
            String freguesia = anexoHq08T814Linha.getFreguesia();
            String tipoPredio = anexoHq08T814Linha.getTipoPredio();
            String titular = anexoHq08T814Linha.getTitular();
            String habitacaoPermanenteArrendada = anexoHq08T814Linha.getHabitacaoPermanenteArrendada();
            Long pais = quadro08Model.getAnexoHq08C814();
            String linkCurrentLine = AnexoHq08T814_Linha.getLink(linha + 1);
            String linkCodigo = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.CODIGO.getIndex();
            String linkFreguesia = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.FREGUESIA.getIndex();
            String linkTipoPredio = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.TIPOPREDIO.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.TITULAR.getIndex();
            String linkHabitacaoPermanenteArrendada = linkCurrentLine + ".c" + AnexoHq08T814_LinhaBase.Property.HABITACAOPERMANENTEARRENDADA.getIndex();
            this.validateH136(result, anexoHModel, codigo, titular, linkCodigo);
            this.validateH137(result, anexoHModel, codigo, titular, anoRendimentos, linkCodigo);
            this.validateH140(result, codigo, freguesia, tipoPredio, titular, pais, linkCodigo, linkFreguesia, linkTipoPredio, linkTitular);
            this.validateH142(result, codigo, habitacaoPermanenteArrendada, linkCodigo, linkHabitacaoPermanenteArrendada);
            this.validateH256(result, anexoHq08T814Linha, linha + 1);
            this.validateH145(result, codigo, freguesia, linkCodigo, linkFreguesia);
        }
        this.validateH165(result, anexoHModel, quadro08Model);
        return result;
    }

    protected void validateH284(ValidationResult resultLocal, RostoModel rostoModel, AnexoHModel anexoHModel) {
        Long periodo = rostoModel.getQuadro02().getQ02C02();
        Quadro08 quadro08Model = anexoHModel.getQuadro08();
        pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07 quadro07Model = anexoHModel.getQuadro07();
        if (quadro08Model.getAnexoHq08C814() != null && (quadro07Model.existsAtLeastOneCodigoBeneficio(new long[]{736}) || quadro07Model.existsAtLeastOneCodigoBeneficio(new long[]{739}) && periodo != null && periodo < 2012) && !quadro07Model.existsAtLeastOneCodigoBeneficio(new long[]{731, 741})) {
            resultLocal.add(new DeclValidationMessage("H284", "aAnexoH.qQuadro08.fanexoHq08C814", new String[]{"aAnexoH.qQuadro08.fanexoHq08C814", "aAnexoH.qQuadro07.tanexoHq07T7"}));
        }
    }

    protected void validateH283(ValidationResult result, AnexoHModel anexoHModel) {
        Quadro08 quadro08Model = anexoHModel.getQuadro08();
        if (!(quadro08Model.getAnexoHq08C814() == null || quadro08Model.getAnexoHq08T814() == null || quadro08Model.getAnexoHq08T814().size() != 1 || quadro08Model.getAnexoHq08T814().get(0).getCodigo() == null || quadro08Model.getAnexoHq08T814().get(0).getCodigo().equals(736))) {
            result.add(new DeclValidationMessage("H283", "aAnexoH.qQuadro08.fanexoHq08C814", new String[]{"aAnexoH.qQuadro08.tanexoHq08T814", "aAnexoH.qQuadro08.fanexoHq08C814"}));
        }
    }

    protected void validateH165(ValidationResult result, AnexoHModel anexoHModel, Quadro08 quadro08Model) {
        long valorRenda;
        long valor = quadro08Model.getAnexoHq08C81501() != null ? quadro08Model.getAnexoHq08C81501() : 0;
        long l = valorRenda = quadro08Model.getAnexoHq08C81502() != null ? quadro08Model.getAnexoHq08C81502() : 0;
        if (!(anexoHModel.getQuadro07().existsAtLeastOneCodigoBeneficio(new long[]{732}) || quadro08Model.getAnexoHq08C81502() == null || valorRenda - valor == 0)) {
            result.add(new DeclValidationMessage("H165", "aAnexoH.qQuadro07", new String[]{"aAnexoH.qQuadro07"}));
        }
    }

    protected void validateH145(ValidationResult result, Long codigo, String freguesia, String linkCodigo, String linkFreguesia) {
        if (freguesia != null && codigo == null) {
            result.add(new DeclValidationMessage("H145", linkFreguesia, new String[]{linkFreguesia, linkCodigo}));
        }
    }

    protected void validateH142(ValidationResult result, Long codigo, String habitacaoPermanenteArrendada, String linkCodigo, String linkHabitacaoPermanenteArrendada) {
        if (codigo != null && Modelo3IRSValidatorUtil.in(codigo, new long[]{731, 739}) && habitacaoPermanenteArrendada == null) {
            result.add(new DeclValidationMessage("H142", linkCodigo, new String[]{linkCodigo, linkHabitacaoPermanenteArrendada, linkHabitacaoPermanenteArrendada}));
        }
    }

    protected void validateH140(ValidationResult result, Long codigo, String freguesia, String tipoPredio, String titular, Long pais, String linkCodigo, String linkFreguesia, String linkTipoPredio, String linkTitular) {
        if (codigo != null && Modelo3IRSValidatorUtil.in(codigo, new long[]{731, 736, 739, 741}) && (freguesia == null || tipoPredio == null || titular == null)) {
            result.add(new DeclValidationMessage("H140", linkCodigo, new String[]{linkCodigo, linkFreguesia, linkTipoPredio, linkTitular}));
        }
    }

    protected void validateH137(ValidationResult result, AnexoHModel anexoHModel, Long codigo, String titular, Long anoRendimentos, String linkCodigo) {
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(codigo) && !StringUtil.isEmpty(titular) && (anexoHModel.getQuadro07().existsAtLeastOneCodigoBeneficioTitular(new long[]{731, 736, 741}, titular) || anoRendimentos != null && anoRendimentos < 2012 && anexoHModel.getQuadro07().existsAtLeastOneCodigoBeneficioTitular(new long[]{739}, titular))) {
            result.add(new DeclValidationMessage("H137", linkCodigo, new String[]{linkCodigo, "aAnexoH.qQuadro07.tanexoHq07T7"}));
        }
    }

    protected void validateH256(ValidationResult result, AnexoHq08T814_Linha current, int numLinha) {
        Long codigo = current.getCodigo();
        if (codigo != null && codigo == 741 && current.getHabitacaoPermanenteArrendada() == null) {
            String linkCodigo = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.CODIGO);
            String linkHabitacaoPermanenteArrendada = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.HABITACAOPERMANENTEARRENDADA);
            result.add(new DeclValidationMessage("H256", linkCodigo, new String[]{linkCodigo, linkHabitacaoPermanenteArrendada}));
        }
    }

    protected void validateH136(ValidationResult result, AnexoHModel anexoHModel, Long codigo, String titular, String linkCodigo) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(codigo) || StringUtil.isEmpty(titular) || anexoHModel.getQuadro07().existsAtLeastOneCodigoBeneficioTitular(new long[]{codigo}, titular) || !Modelo3IRSValidatorUtil.in(codigo, new long[]{731, 736, 739, 741}))) {
            result.add(new DeclValidationMessage("H136", linkCodigo, new String[]{linkCodigo, "aAnexoH.qQuadro07.tanexoHq07T7"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public void validateH259(ValidationResult result, AnexoHq08T814_Linha current, int numLinha) {
        if (current.getCodigo() != null && current.getCodigo().equals(741) && current.getNIFArrendatario() == null) {
            String linkNifArrendatario = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.NIFARRENDATARIO);
            result.add(new DeclValidationMessage("H259", linkNifArrendatario, new String[]{linkNifArrendatario}));
        }
    }

    public void validateH262(ValidationResult result, AnexoHq08T814_Linha current, int numLinha) {
        if (current.getNIFArrendatario() != null && current.getNIFArrendatario() != 0 && current.getCodigo() == null) {
            String linkNifArrendatario = AnexoHq08T814_Linha.getLink(numLinha, AnexoHq08T814_LinhaBase.Property.NIFARRENDATARIO);
            result.add(new DeclValidationMessage("H262", linkNifArrendatario, new String[]{linkNifArrendatario}));
        }
    }
}

