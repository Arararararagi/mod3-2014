/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro09Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro09Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro09 quadro09Model = anexoHModel.getQuadro09();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (quadro09Model.isAnexoHq09B1OP1Selected() && quadro09Model.isAnexoHq09B1OP2Selected()) {
            result.add(new DeclValidationMessage("H172", "aAnexoH.qQuadro09.fanexoHq09B1OP1"));
        }
        if (quadro09Model.isAnexoHq09B1OP1Selected() && quadro09Model.getAnexoHq09C901() == null) {
            result.add(new DeclValidationMessage("H173", "aAnexoH.qQuadro09.fanexoHq09C901", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901"}));
        }
        if (quadro09Model.isAnexoHq09B1OP2Selected() && quadro09Model.getAnexoHq09C901() == null) {
            result.add(new DeclValidationMessage("H174", "aAnexoH.qQuadro09.fanexoHq09C901", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901"}));
        }
        this.validateH175(result, rostoModel, quadro09Model);
        if (!(quadro09Model.getAnexoHq09C901() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro09Model.getAnexoHq09C901()))) {
            result.add(new DeclValidationMessage("H176", "aAnexoH.qQuadro09.fanexoHq09C901", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901"}));
        }
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        if (quadro09Model.getAnexoHq09C901() != null && anoExercicio != null && anoExercicio == 2001) {
            result.add(new DeclValidationMessage("H177", "aAnexoH.qQuadro09.fanexoHq09B1OP1", new String[]{"aAnexoH.qQuadro09.fanexoHq09B1OP1", "aRosto.qQuadro02.fq02C02"}));
        }
        this.validateH293(result, rostoModel, quadro09Model);
        this.validateH287(result, rostoModel, quadro09Model);
        this.validateH288(result, rostoModel, quadro09Model);
        this.validateH289(result, rostoModel, quadro09Model);
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        if (anexoHModel == null) {
            return result;
        }
        Quadro09 quadro09Model = anexoHModel.getQuadro09();
        this.validateH178(result, quadro09Model);
        return result;
    }

    protected void validateH178(ValidationResult result, Quadro09 quadro09Model) {
        if (!(quadro09Model.getAnexoHq09C901() == null || quadro09Model.isAnexoHq09B1OP1Selected() || quadro09Model.isAnexoHq09B1OP2Selected())) {
            result.add(new DeclValidationMessage("H178", "aAnexoH.qQuadro09.fanexoHq09C901", new String[]{"aAnexoH.qQuadro09.fanexoHq09B1OP1", "aAnexoH.qQuadro09.fanexoHq09B1OP2"}));
        }
    }

    protected void validateH175(ValidationResult result, RostoModel rostoModel, Quadro09 quadro09Model) {
        if (quadro09Model.getAnexoHq09C901() != null && Modelo3IRSValidatorUtil.isNIFValid(quadro09Model.getAnexoHq09C901()) && !Modelo3IRSValidatorUtil.startsWith(quadro09Model.getAnexoHq09C901(), "5", "6", "9")) {
            result.add(new DeclValidationMessage("H175", "aAnexoH.qQuadro09.fanexoHq09C901", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901"}));
        }
    }

    protected void validateH293(ValidationResult result, RostoModel rostoModel, Quadro09 quadro09Model) {
        if (quadro09Model.getAnexoHq09C901IVA() != null && quadro09Model.getAnexoHq09C901IVA().booleanValue() && rostoModel != null && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2013) {
            result.add(new DeclValidationMessage("H293", "aAnexoH.qQuadro09.fanexoHq09C901IVA", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901IVA", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateH287(ValidationResult result, RostoModel rostoModel, Quadro09 quadro09Model) {
        if ((quadro09Model.getAnexoHq09C901IRS() != null && quadro09Model.getAnexoHq09C901IRS().booleanValue() || quadro09Model.getAnexoHq09C901IVA() != null && quadro09Model.getAnexoHq09C901IVA().booleanValue()) && quadro09Model.getAnexoHq09C901() == null) {
            result.add(new DeclValidationMessage("H287", "aAnexoH.qQuadro09.fanexoHq09C901", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901"}));
        }
    }

    protected void validateH288(ValidationResult result, RostoModel rostoModel, Quadro09 quadro09Model) {
        if (!(quadro09Model.getAnexoHq09C901() == null || quadro09Model.getAnexoHq09C901IRS() != null && quadro09Model.getAnexoHq09C901IRS().booleanValue())) {
            result.add(new DeclValidationMessage("H288", "aAnexoH.qQuadro09.fanexoHq09C901", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901", "aAnexoH.qQuadro09.fanexoHq09C901IRS"}));
        }
    }

    protected void validateH289(ValidationResult result, RostoModel rostoModel, Quadro09 quadro09Model) {
        if ((quadro09Model.getAnexoHq09C901IRS() == null || !quadro09Model.getAnexoHq09C901IRS().booleanValue()) && quadro09Model.getAnexoHq09C901IVA() != null && quadro09Model.getAnexoHq09C901IVA().booleanValue()) {
            result.add(new DeclValidationMessage("H289", "aAnexoH.qQuadro09.fanexoHq09C901IRS", new String[]{"aAnexoH.qQuadro09.fanexoHq09C901IRS"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

