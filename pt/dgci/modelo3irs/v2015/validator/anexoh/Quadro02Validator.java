/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro02Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String H258 = "H258";

    public Quadro02Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexohModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro02 quadro02AnexoHModel = (Quadro02)anexohModel.getQuadro(Quadro02.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (!Modelo3IRSValidatorUtil.equals(quadro02AnexoHModel.getAnexoHq02C01(), rostoModel.getQuadro02().getQ02C02())) {
            result.add(new DeclValidationMessage("H258", "aAnexoH.qQuadro02.fanexoHq02C01", new String[]{"aAnexoH.qQuadro02.fanexoHq02C01", "aRosto.qQuadro02.fq02C02"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

