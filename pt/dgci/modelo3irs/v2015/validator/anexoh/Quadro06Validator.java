/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.Tuple;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        long somaControlo;
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        Quadro06 quadro06Model = anexoHModel.getQuadro06();
        Quadro08 quadro08Model = anexoHModel.getQuadro08();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (quadro06Model.getAnexoHq06C602() != null && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() != 2001) {
            result.add(new DeclValidationMessage("H041", "aAnexoH.qQuadro06.fanexoHq06C602", new String[]{"aAnexoH.qQuadro06.fanexoHq06C602", "aAnexoH.qQuadro06.fanexoHq06C603", "aRosto.qQuadro02.fq02C02"}));
        }
        if (quadro06Model.getAnexoHq06C603() != null && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() != 2001) {
            result.add(new DeclValidationMessage("H042", "aAnexoH.qQuadro06.fanexoHq06C602", new String[]{"aAnexoH.qQuadro06.fanexoHq06C602", "aAnexoH.qQuadro06.fanexoHq06C603", "aRosto.qQuadro02.fq02C02"}));
        }
        if (quadro06Model.getAnexoHq06T1().size() > 16) {
            result.add(new DeclValidationMessage("H043", "aAnexoH.qQuadro06.tanexoHq06T1", new String[]{"aAnexoH.qQuadro06.tanexoHq06T1"}));
        }
        if (quadro06Model.getAnexoHq06T1().size() > 0 && quadro06Model.getAnexoHq06T1().get(0).getNifBeneficiarios() != null && (quadro06Model.getAnexoHq06C601() == null || quadro06Model.getAnexoHq06C601() == 0)) {
            result.add(new DeclValidationMessage("H046", "aAnexoH.qQuadro06.fanexoHq06C601", new String[]{"aAnexoH.qQuadro06.fanexoHq06C601"}));
        }
        this.validateBeneficiarios(result, quadro06Model, quadro08Model, rostoModel);
        long somaValores = 0;
        long l = somaControlo = quadro06Model.getAnexoHq06C1() != null ? quadro06Model.getAnexoHq06C1() : 0;
        if (quadro06Model.getAnexoHq06C601() != null) {
            somaValores+=quadro06Model.getAnexoHq06C601().longValue();
        }
        if (quadro06Model.getAnexoHq06C602() != null) {
            somaValores+=quadro06Model.getAnexoHq06C602().longValue();
        }
        if (quadro06Model.getAnexoHq06C603() != null) {
            somaValores+=quadro06Model.getAnexoHq06C603().longValue();
        }
        if (somaControlo != somaValores) {
            result.add(new DeclValidationMessage("H055", "aAnexoH.qQuadro06.fanexoHq06C1", new String[]{"aAnexoH.qQuadro06.fanexoHq06C601", "aAnexoH.qQuadro06.fanexoHq06C602", "aAnexoH.qQuadro06.fanexoHq06C603", "aAnexoH.qQuadro06.fanexoHq06C1"}));
        }
        if (rostoModel.getQuadro05().isQ05B1OP4Selected() && (rostoModel.getQuadro05().getQ05C5() != null || rostoModel.getQuadro05().getQ05C5_1() != null) && !rostoModel.getQuadro05().isQ05B3OP9Selected() && (anexoHModel.getQuadro06().getAnexoHq06C601() != null || anexoHModel.getQuadro06().getAnexoHq06C602() != null || anexoHModel.getQuadro06().getAnexoHq06C603() != null || anexoHModel.getQuadro06().getAnexoHq06T1().size() > 0)) {
            result.add(new DeclValidationMessage("H236", "aAnexoH.qQuadro06", new String[]{"aAnexoH.qQuadro06", "aRosto.qQuadro05.fq05B1OP4"}));
        }
        return result;
    }

    private void validateBeneficiarios(ValidationResult result, Quadro06 quadro06Model, Quadro08 quadro08Model, RostoModel rostoModel) {
        ArrayList<Long> repeatedNIFsPensoes = new ArrayList<Long>();
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        for (int linha = 0; linha < quadro06Model.getAnexoHq06T1().size(); ++linha) {
            AnexoHq06T1_Linha anexoHq06T1_Linha = quadro06Model.getAnexoHq06T1().get(linha);
            Long nifBeneficiario = anexoHq06T1_Linha.getNifBeneficiarios();
            Long valor = anexoHq06T1_Linha.getValor();
            String linkCurrentLine = "aAnexoH.qQuadro06.tanexoHq06T1.l" + (linha + 1);
            String linkNifBeneficiario = linkCurrentLine + ".c" + AnexoHq06T1_LinhaBase.Property.NIFBENEFICIARIOS.getIndex();
            String linkValor = linkCurrentLine + ".c" + AnexoHq06T1_LinhaBase.Property.VALOR.getIndex();
            this.validateBeneficiario(result, quadro08Model, rostoModel, linkCurrentLine, nifBeneficiario, linkNifBeneficiario, valor, linkValor, repeatedNIFsPensoes, processedTuples);
        }
    }

    private void validateBeneficiario(ValidationResult result, Quadro08 quadro08Model, RostoModel rostoModel, String lineLink, Long nif, String nifLink, Long valor, String valorLink, List<Long> repeatedNIFsValidationList, List<Tuple> processedTuples) {
        boolean hasNif;
        Tuple tuple;
        boolean bl = hasNif = nif != null;
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && Modelo3IRSValidatorUtil.isEmptyOrZero(valor)) {
            result.add(new DeclValidationMessage("H198", lineLink, new String[]{lineLink}));
        }
        if (processedTuples.contains(tuple = new Tuple(new Object[]{nif}))) {
            result.add(new DeclValidationMessage("H050", nifLink, new String[]{nifLink}));
        } else {
            processedTuples.add(tuple);
        }
        if (hasNif && !Modelo3IRSValidatorUtil.isNIFValid(nif)) {
            result.add(new DeclValidationMessage("H047", nifLink, new String[]{nifLink}));
        }
        if (hasNif && !Modelo3IRSValidatorUtil.startsWith(nif, "1", "2")) {
            result.add(new DeclValidationMessage("H048", nifLink, new String[]{nifLink}));
        }
        this.validateH051(result, rostoModel, nif, nifLink);
        if (nif == null && valor != null && valor != 0) {
            result.add(new DeclValidationMessage("H054", nifLink, new String[]{valorLink, nifLink}));
        }
    }

    protected void validateH051(ValidationResult result, RostoModel rostoModel, Long nif, String nifLink) {
        if (nif != null && (rostoModel.getQuadro03().isSujeitoPassivoA(nif) || rostoModel.getQuadro03().isSujeitoPassivoB(nif) || rostoModel.getQuadro03().existsInDependentes(nif) || rostoModel.getQuadro07().isConjugeFalecido(nif) || rostoModel.getQuadro07().existsInAscendentes(nif) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nif) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nif) || rostoModel.getQuadro05().isRepresentante(nif))) {
            result.add(new DeclValidationMessage("H051", nifLink, new String[]{nifLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07ET1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro05.fq05C5"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        if (anexoHModel == null) {
            return result;
        }
        Quadro06 quadro06Model = anexoHModel.getQuadro06();
        if (quadro06Model.getAnexoHq06C601() != null && quadro06Model.getAnexoHq06C601().longValue() != quadro06Model.getValorTotalPensoesPreenchido()) {
            result.add(new DeclValidationMessage("H039", "aAnexoH.qQuadro06.fanexoHq06C601", new String[]{"aAnexoH.qQuadro06.fanexoHq06C601", "aAnexoH.qQuadro06.tanexoHq06T1"}));
        }
        if (quadro06Model.getAnexoHq06C601() != null && quadro06Model.getAnexoHq06C601() > 0 && quadro06Model.getAnexoHq06T1().size() > 0 && quadro06Model.getAnexoHq06T1().get(0).getNifBeneficiarios() == null) {
            result.add(new DeclValidationMessage("H044", "aAnexoH.qQuadro06.tanexoHq06T1", new String[]{"aAnexoH.qQuadro06.tanexoHq06T1"}));
        }
        this.validateBeneficiarios(result, quadro06Model);
        return result;
    }

    private void validateBeneficiarios(ValidationResult result, Quadro06 quadro06Model) {
        for (int linha = 0; linha < quadro06Model.getAnexoHq06T1().size(); ++linha) {
            AnexoHq06T1_Linha anexoHq06T1_Linha = quadro06Model.getAnexoHq06T1().get(linha);
            Long nifBeneficiario = anexoHq06T1_Linha.getNifBeneficiarios();
            Long valor = anexoHq06T1_Linha.getValor();
            String linkCurrentLine = "aAnexoH.qQuadro06.tanexoHq06T1.l" + (linha + 1);
            String linkNifBeneficiario = linkCurrentLine + ".c" + AnexoHq06T1_LinhaBase.Property.NIFBENEFICIARIOS.getIndex();
            String linkValor = linkCurrentLine + ".c" + AnexoHq06T1_LinhaBase.Property.VALOR.getIndex();
            this.validateBeneficiario(result, nifBeneficiario, linkNifBeneficiario, valor, linkValor);
        }
    }

    private void validateBeneficiario(ValidationResult result, Long nif, String nifLink, Long valor, String valorLink) {
        if (nif != null && (valor == null || valor != null && valor == 0)) {
            result.add(new DeclValidationMessage("H052", nifLink, new String[]{nifLink, valorLink}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

