/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final int LINE_PAD = 1;

    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        if (anexoHModel == null) {
            return result;
        }
        pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05 quadro05Model = anexoHModel.getQuadro05();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (quadro05Model.getAnexoHq05T5().size() > 16) {
            result.add(new DeclValidationMessage("H028", "aAnexoH.qQuadro05.tanexoHq05T5", new String[]{"aAnexoH.qQuadro05.tanexoHq05T5"}));
        }
        this.validateRendimentos(result, quadro05Model, rostoModel, model);
        this.validateH037(result, anexoHModel, rostoModel, Modelo3IRSv2015Parameters.instance().isFase2());
        return result;
    }

    private void validateRendimentos(ValidationResult result, pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05 quadro05Model, RostoModel rostoModel, DeclaracaoModel model) {
        long anexoHq05C1Value;
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        ArrayList<String> processedTitulares = new ArrayList<String>();
        for (int linha = 0; linha < quadro05Model.getAnexoHq05T5().size(); ++linha) {
            AnexoHq05T5_Linha anexoHq05T5Linha = quadro05Model.getAnexoHq05T5().get(linha);
            String linkCurrentLine = "aAnexoH.qQuadro05.tanexoHq05T5.l" + (linha + 1);
            String linkTitular = linkCurrentLine + ".c" + AnexoHq05T5_LinhaBase.Property.TITULAR.getIndex();
            String linkMontanteRendimento = linkCurrentLine + ".c" + AnexoHq05T5_LinhaBase.Property.MONTANTERENDIMENTO.getIndex();
            String titular = anexoHq05T5Linha.getTitular();
            Long montanteRendimento = anexoHq05T5Linha.getMontanteRendimento();
            this.validateRendimento(result, linkCurrentLine, titular, linkTitular, montanteRendimento, linkMontanteRendimento, processedTuples, rostoModel, model, processedTitulares);
        }
        long l = anexoHq05C1Value = quadro05Model.getAnexoHq05C1() != null ? quadro05Model.getAnexoHq05C1() : 0;
        if (anexoHq05C1Value != quadro05Model.getTotalRendimentos()) {
            result.add(new DeclValidationMessage("YH001", "aAnexoH.qQuadro05.fanexoHq05C1", new String[]{"aAnexoH.qQuadro05.fanexoHq05C1"}));
        }
    }

    protected void validateH037(ValidationResult result, AnexoHModel anexoHModel, RostoModel rostoModel, boolean isFase2) {
        if (!(anexoHModel == null || anexoHModel.getQuadro05().getAnexoHq05T5().isEmpty() || rostoModel.getQuadro02().getQ02C02() == null)) {
            long anoRendimentos = rostoModel.getQuadro02().getQ02C02();
            long ano2001 = 2001;
            long ano2004 = 2004;
            long ano2011 = 2011;
            long ano2012 = 2012;
            long ano2013 = 2013;
            long limite30mil = 3000000;
            long limite20mil = 2000000;
            long limite10mil = 1000000;
            for (int i = 0; i < anexoHModel.getQuadro05().getAnexoHq05T5().size(); ++i) {
                AnexoHq05T5_Linha anexoHq05T5_Linha = anexoHModel.getQuadro05().getAnexoHq05T5().get(i);
                if (anexoHq05T5_Linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHq05T5_Linha.getMontanteRendimento()) || (anexoHq05T5_Linha.getMontanteRendimento() <= 3000000 || anoRendimentos >= 2001 && (anoRendimentos <= 2004 || anoRendimentos >= 2012)) && (anexoHq05T5_Linha.getMontanteRendimento() <= 2000000 || anoRendimentos <= 2011 || anoRendimentos >= 2013) && (anexoHq05T5_Linha.getMontanteRendimento() <= 1000000 || anoRendimentos <= 2012)) continue;
                result.add(new DeclValidationMessage("H037", AnexoHq05T5_Linha.getLink(i + 1, AnexoHq05T5_LinhaBase.Property.MONTANTERENDIMENTO), new String[]{AnexoHq05T5_Linha.getLink(i + 1, AnexoHq05T5_LinhaBase.Property.MONTANTERENDIMENTO)}));
            }
        }
    }

    private void validateRendimento(ValidationResult result, String lineLink, String titular, String titularLink, Long rendimento, String rendimentoLink, List<Tuple> processedTuples, RostoModel rostoModel, DeclaracaoModel model, List<String> processedTitulares) {
        if (StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(rendimento)) {
            result.add(new DeclValidationMessage("H183", lineLink, new String[]{lineLink}));
        }
        if (!Modelo3IRSv2015Parameters.instance().isFase2()) {
            if (titular != null || rendimento != null) {
                result.add(new DeclValidationMessage("H189", "aAnexoH.qQuadro05", new String[]{"aAnexoH.qQuadro05"}));
            }
        } else {
            Long nifTitular;
            this.validateH030(result, titular, titularLink);
            if (titular != null && rostoModel.getNIFTitular(titular) == null) {
                result.add(new DeclValidationMessage("H031", titularLink, new String[]{titularLink, rostoModel.getLinkByTitular(titular)}));
            }
            if (titular != null && (nifTitular = rostoModel.getNIFTitular(titular)) != null && ((Modelo3IRSv2015Model)model).getAnexoBByTitular(nifTitular) == null && ((Modelo3IRSv2015Model)model).getAnexoCByTitular(nifTitular) == null) {
                result.add(new DeclValidationMessage("H032", titularLink, new String[]{titularLink}));
            }
            if (titular != null) {
                if (processedTitulares.contains(titular)) {
                    result.add(new DeclValidationMessage("H033", titularLink, new String[]{titularLink}));
                } else {
                    processedTitulares.add(titular);
                }
            }
            if (titular != null && (nifTitular = rostoModel.getNIFTitular(titular)) != null) {
                boolean hasAnexoJ;
                boolean hasAnexoB = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(nifTitular.toString()) != null;
                boolean hasAnexoC = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(nifTitular.toString()) != null;
                boolean bl = hasAnexoJ = ((Modelo3IRSv2015Model)model).getAnexoJByTitular(nifTitular.toString()) != null;
                if (!(hasAnexoB || hasAnexoC || !hasAnexoJ)) {
                    Long field406;
                    AnexoJModel anexoJ = ((Modelo3IRSv2015Model)model).getAnexoJByTitular(nifTitular.toString());
                    Long l = field406 = anexoJ.getQuadro04().getAnexoJq04C406b() == null ? new Long(0) : anexoJ.getQuadro04().getAnexoJq04C406b();
                    if (field406 == 0) {
                        result.add(new DeclValidationMessage("H034", titularLink, new String[]{titularLink, anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b")}));
                    }
                }
            }
            if (titular != null && (rendimento == null || rendimento == 0)) {
                result.add(new DeclValidationMessage("H036", rendimentoLink, new String[]{rendimentoLink, titularLink}));
            }
            if (titular == null && rendimento != null && rendimento > 0) {
                result.add(new DeclValidationMessage("H038", titularLink, new String[]{titularLink, rendimentoLink}));
            }
            if (titular != null && rendimento != null) {
                Tuple tuple = new Tuple(new Object[]{titular, rendimento});
                if (processedTuples.contains(tuple)) {
                    result.add(new DeclValidationMessage("H182", titularLink, new String[]{titularLink, rendimentoLink}));
                } else {
                    processedTuples.add(tuple);
                }
            }
        }
    }

    protected void validateH030(ValidationResult result, String titular, String titularLink) {
        if (!(titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("H030", titularLink, new String[]{titularLink}));
        }
    }

    public void validateH244(ValidationResult result, AnexoHModel anexoHModel, RostoModel rostoModel) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHModel.getQuadro05().getAnexoHq05C1()) && rostoModel.getQuadro05().isQ05B1OP4Selected()) {
            result.add(new DeclValidationMessage("H244", "aAnexoH.qQuadro05", new String[]{"aAnexoH.qQuadro05"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        this.validateH244(result, anexoHModel, rostoModel);
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        return new ValidationResult();
    }
}

