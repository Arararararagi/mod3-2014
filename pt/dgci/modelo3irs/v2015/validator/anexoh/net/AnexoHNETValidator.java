/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.AnexoHValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro06NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro07NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro08NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro09NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoh.net.Quadro10NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoHNETValidator
extends AnexoHValidator {
    public AnexoHNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        result.addAllFrom(new Quadro07NETValidator().validate(model));
        result.addAllFrom(new Quadro08NETValidator().validate(model));
        result.addAllFrom(new Quadro09NETValidator().validate(model));
        result.addAllFrom(new Quadro10NETValidator().validate(model));
        return result;
    }
}

