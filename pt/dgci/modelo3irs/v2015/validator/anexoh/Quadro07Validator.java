/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoh;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxH;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final int LINE_PAD = 1;

    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        long totalImportanciaAplicadaPreenchida;
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        if (anexoHModel == null) {
            return result;
        }
        pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07 quadro07Model = (pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07)anexoHModel.getQuadro(pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (!(!rostoModel.getQuadro05().isQ05B1OP4Selected() || Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro05().getQ05C5()) && Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro05().getQ05C5_1()) || quadro07Model.getAnexoHq07T7().size() <= 0 || rostoModel.getQuadro05().isQ05B3OP9Selected())) {
            result.add(new DeclValidationMessage("H056", "aAnexoH.qQuadro07", new String[]{"aAnexoH.qQuadro07", "aRosto.qQuadro05.fq05B1OP4"}));
        }
        if (quadro07Model.getAnexoHq07T7().size() > 100) {
            result.add(new DeclValidationMessage("H057", "aAnexoH.qQuadro07", new String[]{"aAnexoH.qQuadro07"}));
        }
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        HashMap<String, String> processedLines = new HashMap<String, String>();
        for (int linha = 0; linha < quadro07Model.getAnexoHq07T7().size(); ++linha) {
            AnexoHq07T7_Linha anexoHq07T7Linha = quadro07Model.getAnexoHq07T7().get(linha);
            Long codBeneficio = anexoHq07T7Linha.getCodBeneficio();
            String titular = anexoHq07T7Linha.getTitular();
            Long importanciaAplicada = anexoHq07T7Linha.getImportanciaAplicada();
            Long nifPortugues = anexoHq07T7Linha.getNifNipcPortugues();
            Long pais = anexoHq07T7Linha.getPais();
            String numFiscalUE = anexoHq07T7Linha.getNumeroFiscalUE();
            String linkCurrentLine = AnexoHq07T7_Linha.getLink(linha + 1);
            String linkCodBeneficio = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.CODBENEFICIO.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.TITULAR.getIndex();
            String linkNifPortugues = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.NIFNIPCPORTUGUES.getIndex();
            String linkPais = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.PAIS.getIndex();
            String linkNumFiscalUE = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.NUMEROFISCALUE.getIndex();
            this.validateH224(result, codBeneficio, anoExercicio, pais, numFiscalUE, linkNumFiscalUE, linkCodBeneficio, linkNifPortugues);
            if (!(codBeneficio != null || StringUtil.isEmpty(titular) && importanciaAplicada == null && nifPortugues == null && numFiscalUE == null)) {
                result.add(new DeclValidationMessage("H063", linkCodBeneficio, new String[]{linkCodBeneficio}));
            }
            this.validateH250(result, anexoHq07T7Linha, anoExercicio, linha + 1);
            this.validateH261(result, anexoHq07T7Linha, anoExercicio, linha + 1);
            this.validateH234(result, anexoHq07T7Linha, anoExercicio, linkCodBeneficio, linkPais, linkNumFiscalUE);
            this.validateH068(result, anexoHq07T7Linha, titular, linkCodBeneficio);
            this.validateH267(result, anexoHq07T7Linha, linkCodBeneficio);
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{731}) && !StringUtil.in(titular, new String[]{"A", "B", "F", "C"})) {
                result.add(new DeclValidationMessage("H215", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03C03"}));
            }
            this.validateH069(result, rostoModel, anexoHq07T7Linha, linkTitular, linkCodBeneficio);
            this.validateH280(result, anexoHq07T7Linha, linkTitular, linkCodBeneficio);
            this.validateH070(result, rostoModel, anexoHq07T7Linha, linkTitular, linkCodBeneficio);
            if (codBeneficio != null && !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{731}) && StringUtil.in(titular, new String[]{"C"})) {
                result.add(new DeclValidationMessage("H216", linkTitular, new String[]{linkTitular, linkCodBeneficio}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{701, 711}) && anoExercicio != null && anoExercicio == 2005) {
                result.add(new DeclValidationMessage("H071", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{702, 705}) && anoExercicio != null && anoExercicio >= 2005) {
                result.add(new DeclValidationMessage("H072", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{703, 704}) && anoExercicio != null && anoExercicio >= 2003) {
                result.add(new DeclValidationMessage("H073", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{733, 734, 735}) && anoExercicio != null && anoExercicio < 2008) {
                result.add(new DeclValidationMessage("H074", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{725, 726, 727}) && anoExercicio != null && anoExercicio <= 2005) {
                result.add(new DeclValidationMessage("H075", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            if (codBeneficio != null && codBeneficio == 708 && anoExercicio != null && (anoExercicio < 2001 || anoExercicio > 2009 || anoExercicio == 2004 || anoExercicio == 2005)) {
                result.add(new DeclValidationMessage("H076", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            this.validateH268(result, rostoModel, anexoHq07T7Linha, linkCodBeneficio);
            this.validateH279(result, rostoModel, anexoHq07T7Linha, linkTitular, linkCodBeneficio);
            this.validateH221(result, rostoModel, anexoHq07T7Linha, linkCodBeneficio);
            if (codBeneficio != null && codBeneficio == 740 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2009) {
                result.add(new DeclValidationMessage("H222", linkCodBeneficio, new String[]{linkCodBeneficio}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{709, 710, 723}) && anoExercicio != null && anoExercicio >= 2007) {
                result.add(new DeclValidationMessage("H077", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            if (!(codBeneficio == null || codBeneficio != 712 || anoExercicio == null || Modelo3IRSValidatorUtil.in(anoExercicio, new long[]{2003, 2004}))) {
                result.add(new DeclValidationMessage("H078", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
            }
            this.validateH080(result, anexoHq07T7Linha, rostoModel.getQuadro02().getQ02C02(), linha + 1);
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{709, 710}) && nifPortugues != null && !Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5")) {
                result.add(new DeclValidationMessage("H081", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{715, 716}) && nifPortugues != null && !Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9")) {
                result.add(new DeclValidationMessage("H082", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            this.validateH225(result, anexoHq07T7Linha, anoExercicio, linkCodBeneficio, linkNifPortugues);
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{737}) && nifPortugues != null && !Modelo3IRSValidatorUtil.startsWith(nifPortugues, "1", "2", "5", "6", "70", "9")) {
                result.add(new DeclValidationMessage("H241", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{736}) && nifPortugues != null && !Modelo3IRSValidatorUtil.startsWith(nifPortugues, "1", "2", "5", "6", "9")) {
                result.add(new DeclValidationMessage("H085", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{711, 701, 743}) && nifPortugues != null && !Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "98", "72", "6")) {
                result.add(new DeclValidationMessage("H087", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{724, 725, 726, 727, 728}) && nifPortugues != null && !Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "6")) {
                result.add(new DeclValidationMessage("H088", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (anoExercicio != null && anoExercicio >= 2008 && codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{733}) && (nifPortugues != null && !Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{504863797}) || nifPortugues == null)) {
                result.add(new DeclValidationMessage("H090", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (!(anoExercicio == null || anoExercicio != 2009 || codBeneficio == null || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{717, 718, 719, 720, 721, 722}) || nifPortugues != null && (nifPortugues == null || Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9") || Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{110109554})))) {
                result.add(new DeclValidationMessage("H187", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
            }
            if (!(anoExercicio == null || anoExercicio != 2008 || codBeneficio == null || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{717, 718, 719, 720, 721, 722}) || nifPortugues != null && (nifPortugues == null || Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9") || Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{199037515, 110109554, 231844450, 212651846})))) {
                result.add(new DeclValidationMessage("H091", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
            }
            if (!(anoExercicio == null || anoExercicio != 2007 || codBeneficio == null || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{717, 718, 719, 720, 721, 722}) || nifPortugues != null && (Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9") || Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{206186215, 119160838, 122335740, 151308896, 192442490, 223450162, 110109554, 199037515, 231844450, 212651846, 207136076})))) {
                result.add(new DeclValidationMessage("H092", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
            }
            if (!(anoExercicio == null || anoExercicio != 2006 || codBeneficio == null || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{717, 718, 719, 720, 721, 722, 723}) || nifPortugues != null && (Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9") || Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{152056700, 206186215, 222843349, 119160838, 122335740, 151308896, 192442490, 223450162, 199037515, 231844450, 212651846, 207136076})))) {
                result.add(new DeclValidationMessage("H093", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
            }
            if (!(anoExercicio == null || anoExercicio != 2005 || codBeneficio == null || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{717, 718, 719, 720, 721, 722, 723}) || nifPortugues != null && (Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9") || Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{152056700, 100865097, 206186215, 122335740, 151308896, 192442490, 223450162})))) {
                result.add(new DeclValidationMessage("H094", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
            }
            if (!(anoExercicio == null || anoExercicio != 2004 || codBeneficio == null || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{717, 718, 719, 720, 721, 722, 723}) || nifPortugues != null && (Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9") || Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{100390358})))) {
                result.add(new DeclValidationMessage("H095", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
            }
            if (!(anoExercicio == null || anoExercicio != 2002 || codBeneficio == null || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{717, 718, 719, 720, 721, 722, 723}) || nifPortugues != null && (Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "9") || Modelo3IRSValidatorUtil.in(nifPortugues, new long[]{192885260})))) {
                result.add(new DeclValidationMessage("H096", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
            }
            this.validateH098(result, titular, linkTitular);
            this.validateH099(result, rostoModel, titular, linkTitular);
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(codBeneficio) && StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(importanciaAplicada) && Modelo3IRSValidatorUtil.isEmptyOrZero(nifPortugues) && Modelo3IRSValidatorUtil.isEmptyOrZero(pais) && StringUtil.isEmpty(numFiscalUE)) {
                result.add(new DeclValidationMessage("H100", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (!(nifPortugues == null || Modelo3IRSValidatorUtil.isNIFValid(nifPortugues))) {
                result.add(new DeclValidationMessage("H104", linkNifPortugues, new String[]{linkNifPortugues}));
            }
            if (codBeneficio != null && titular != null) {
                String key = new String("(" + codBeneficio + "," + titular + "," + nifPortugues + "," + pais + "," + numFiscalUE + ")").toLowerCase();
                if (processedLines.containsKey(key)) {
                    String linkEntidadeGestora = numFiscalUE != null ? linkNumFiscalUE : linkNifPortugues;
                    result.add(new DeclValidationMessage("H105", linkEntidadeGestora, new String[]{linkEntidadeGestora, linkTitular, linkCodBeneficio}));
                } else {
                    processedLines.put(key, key);
                }
            }
            this.validateH106(result, rostoModel, anexoHq07T7Linha, linkNifPortugues);
            this.validateH107(result, pais, linkPais);
            this.validateH194(result, pais, numFiscalUE, linkPais, linkNumFiscalUE);
            if (numFiscalUE != null && StringUtil.removeChars(numFiscalUE, "0").equalsIgnoreCase("") && pais != null) {
                result.add(new DeclValidationMessage("H108", linkNumFiscalUE, new String[]{linkNumFiscalUE, linkPais}));
            }
            if (numFiscalUE != null && numFiscalUE.charAt(0) == ' ' && pais != null) {
                result.add(new DeclValidationMessage("H109", linkNumFiscalUE, new String[]{linkNumFiscalUE}));
            }
            if (numFiscalUE != null && nifPortugues != null) {
                result.add(new DeclValidationMessage("H110", linkNumFiscalUE, new String[]{linkNifPortugues, linkNumFiscalUE}));
            }
            this.validateH265(result, anexoHq07T7Linha, anoExercicio, linha + 1);
        }
        long totalImportanciaAplicada = quadro07Model.getAnexoHq07C1() != null ? quadro07Model.getAnexoHq07C1() : 0;
        if (totalImportanciaAplicada != (totalImportanciaAplicadaPreenchida = quadro07Model.getTotalImportanciaAplicadaPreenchida())) {
            result.add(new DeclValidationMessage("H186", "aAnexoH.qQuadro07.fanexoHq07C1", new String[]{"aAnexoH.qQuadro07.fanexoHq07C1"}));
        }
        return result;
    }

    protected void validateH280(ValidationResult result, AnexoHq07T7_Linha anexoHq07T7Linha, String linkTitular, String linkCodBeneficio) {
        if (anexoHq07T7Linha != null && !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHq07T7Linha.getCodBeneficio()) && (Quadro07.isAscendentesEmComunhao(anexoHq07T7Linha.getTitular()) || Quadro07.isAscendentesEColaterais(anexoHq07T7Linha.getTitular()))) {
            Long codBeneficio = anexoHq07T7Linha.getCodBeneficio();
            long[] values = new long[]{702, 708, 709, 710, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728};
            if (codBeneficio != null && Modelo3IRSValidatorUtil.in(codBeneficio, values)) {
                result.add(new DeclValidationMessage("H280", linkCodBeneficio, new String[]{linkTitular, linkCodBeneficio}));
            }
        }
    }

    protected void validateH234(ValidationResult result, AnexoHq07T7_Linha anexoHq07T7Linha, Long anoExercicio, String linkCodBeneficio, String linkPais, String linkNumFiscalUE) {
        if (!(anexoHq07T7Linha == null || anexoHq07T7Linha.getCodBeneficio() == null || !Modelo3IRSValidatorUtil.in(anexoHq07T7Linha.getCodBeneficio(), new long[]{734, 735}) || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHq07T7Linha.getPais()) && StringUtil.isEmpty(anexoHq07T7Linha.getNumeroFiscalUE()) || anoExercicio == null || anoExercicio >= 2012)) {
            result.add(new DeclValidationMessage("H234", linkCodBeneficio, new String[]{linkCodBeneficio, linkPais, linkNumFiscalUE}));
        }
    }

    protected void validateH225(ValidationResult result, AnexoHq07T7_Linha anexoHq07T7Linha, Long anoExercicio, String linkCodBeneficio, String linkNifPortugues) {
        if (!(anexoHq07T7Linha == null || anexoHq07T7Linha.getCodBeneficio() == null || !Modelo3IRSValidatorUtil.in(anexoHq07T7Linha.getCodBeneficio(), new long[]{738, 739, 740}) || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHq07T7Linha.getNifNipcPortugues()) || Modelo3IRSValidatorUtil.startsWith(anexoHq07T7Linha.getNifNipcPortugues(), "1", "2", "5", "6", "9") || anoExercicio == null || anoExercicio >= 2012)) {
            result.add(new DeclValidationMessage("H225", linkCodBeneficio, new String[]{linkNifPortugues}));
        }
    }

    protected void validateH099(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("H099", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    protected void validateH106(ValidationResult result, RostoModel rostoModel, AnexoHq07T7_Linha anexoHq07T7Linha, String linkNifPortugues) {
        Long codBeneficio = anexoHq07T7Linha.getCodBeneficio();
        Long nifPortugues = anexoHq07T7Linha.getNifNipcPortugues();
        Long anoExercicio = null;
        if (rostoModel != null && rostoModel.getQuadro02() != null) {
            anoExercicio = rostoModel.getQuadro02().getQ02C02();
        }
        if (nifPortugues != null && codBeneficio != null && (rostoModel.getQuadro03().isSujeitoPassivoA(nifPortugues) || rostoModel.getQuadro03().isSujeitoPassivoB(nifPortugues) || rostoModel.getQuadro03().existsInDependentes(nifPortugues) || rostoModel.getQuadro07().isConjugeFalecido(nifPortugues) || rostoModel.getQuadro07().existsInAscendentes(nifPortugues) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nifPortugues) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nifPortugues) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nifPortugues) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nifPortugues)) && (Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{732, 736}) || Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{739}) && anoExercicio != null && anoExercicio < 2012)) {
            result.add(new DeclValidationMessage("H106", linkNifPortugues, new String[]{linkNifPortugues, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    private void validateH107(ValidationResult result, Long pais, String linkPais) {
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxH.class.getSimpleName());
        if (!(pais == null || paisesCatalog.containsKey(pais))) {
            result.add(new DeclValidationMessage("H107", linkPais, new String[]{linkPais}));
        }
    }

    protected void validateH098(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteEmGuardaConjunta(titular) || Quadro07.isAscendentesEmComunhao(titular) || Quadro07.isAscendentesEColaterais(titular))) {
            result.add(new DeclValidationMessage("H098", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03C03"}));
        }
    }

    protected void validateH268(ValidationResult result, RostoModel rostoModel, AnexoHq07T7_Linha anexoHq07T7Linha, String linkCodBeneficio) {
        Long codigo = null;
        Long anoRendimentos = rostoModel.getQuadro02().getQ02C02();
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getCodBeneficio() != null) {
            codigo = anexoHq07T7Linha.getCodBeneficio();
        }
        if (codigo != null && anoRendimentos != null && Modelo3IRSValidatorUtil.in(codigo, new long[]{742, 743}) && anoRendimentos < 2011) {
            result.add(new DeclValidationMessage("H268", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateH279(ValidationResult result, RostoModel rostoModel, AnexoHq07T7_Linha anexoHq07T7Linha, String linkTitular, String linkCodBeneficio) {
        Long codigo = anexoHq07T7Linha.getCodBeneficio();
        String titular = anexoHq07T7Linha.getTitular();
        if (codigo != null && codigo.equals(737) && Quadro03.isDependenteEmGuardaConjunta(titular) && rostoModel.getQuadro03().getRostoq03DT1LinhaByLabel(titular) != null && Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro03().getGrauDependentesGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("H279", linkCodBeneficio, new String[]{linkTitular, linkCodBeneficio}));
        }
    }

    protected void validateH221(ValidationResult result, RostoModel rostoModel, AnexoHq07T7_Linha anexoHq07T7Linha, String linkCodBeneficio) {
        Long codigo = null;
        Long anoRendimentos = null;
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getCodBeneficio() != null) {
            codigo = anexoHq07T7Linha.getCodBeneficio();
        }
        if (rostoModel != null && rostoModel.getQuadro02() != null) {
            anoRendimentos = rostoModel.getQuadro02().getQ02C02();
        }
        if (codigo != null && anoRendimentos != null && (Modelo3IRSValidatorUtil.in(codigo, new long[]{739}) && anoRendimentos < 2010 || Modelo3IRSValidatorUtil.in(codigo, new long[]{739, 740, 738, 734, 735}) && anoRendimentos > 2011)) {
            result.add(new DeclValidationMessage("H221", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateH069(ValidationResult result, RostoModel rostoModel, AnexoHq07T7_Linha anexoHq07T7Linha, String linkTitular, String linkCodBeneficio) {
        Long anoRendimentos = null;
        Long codigo = null;
        String titular = null;
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getCodBeneficio() != null) {
            codigo = anexoHq07T7Linha.getCodBeneficio();
        }
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getTitular() != null) {
            titular = anexoHq07T7Linha.getTitular();
        }
        if (rostoModel != null && rostoModel.getQuadro02() != null) {
            anoRendimentos = rostoModel.getQuadro02().getQ02C02();
        }
        if (!(codigo == null || titular == null || !Modelo3IRSValidatorUtil.in(codigo, new long[]{706, 707}) && (!Modelo3IRSValidatorUtil.in(codigo, new long[]{742, 743}) || anoRendimentos == null || anoRendimentos <= 2011) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("H069", linkCodBeneficio, new String[]{linkTitular, linkCodBeneficio}));
        }
    }

    protected void validateH070(ValidationResult result, RostoModel rostoModel, AnexoHq07T7_Linha anexoHq07T7Linha, String linkTitular, String linkCodBeneficio) {
        Long anoRendimentos = null;
        Long codigo = null;
        String titular = null;
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getCodBeneficio() != null) {
            codigo = anexoHq07T7Linha.getCodBeneficio();
        }
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getTitular() != null) {
            titular = anexoHq07T7Linha.getTitular();
        }
        if (rostoModel != null && rostoModel.getQuadro02() != null) {
            anoRendimentos = rostoModel.getQuadro02().getQ02C02();
        }
        if (codigo != null && titular != null && (Modelo3IRSValidatorUtil.in(codigo, new long[]{706, 707}) || Modelo3IRSValidatorUtil.in(codigo, new long[]{742, 743}) && anoRendimentos != null && anoRendimentos > 2011) && (Quadro03.isSujeitoPassivoA(titular) && (Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro03().getQ03C03a()) || rostoModel.getQuadro03().getQ03C03a() < 60) || Quadro03.isSujeitoPassivoB(titular) && (Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro03().getQ03C04a()) || rostoModel.getQuadro03().getQ03C04a() < 60) || Quadro07.isConjugeFalecido(titular) && (Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro07().getQ07C1a()) || rostoModel.getQuadro07().getQ07C1a() < 60) || Quadro03.isDependenteEmGuardaConjunta(titular) && (Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro03().getGrauDependentesGuardaConjunta(titular)) || rostoModel.getQuadro03().getGrauDependentesGuardaConjunta(titular) < 60))) {
            result.add(new DeclValidationMessage("H070", linkTitular, new String[]{linkTitular, linkCodBeneficio}));
        }
    }

    protected void validateH267(ValidationResult result, AnexoHq07T7_Linha anexoHq07T7Linha, String linkCodBeneficio) {
        Long codigo = null;
        String titular = null;
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getCodBeneficio() != null) {
            codigo = anexoHq07T7Linha.getCodBeneficio();
        }
        if (anexoHq07T7Linha != null && anexoHq07T7Linha.getTitular() != null) {
            titular = anexoHq07T7Linha.getTitular();
        }
        if (!(codigo == null || !new Long(737).equals(codigo) || titular == null || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro07.isConjugeFalecido(titular) || Quadro07.isAscendentesEColaterais(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("H267", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro07.trostoq07ET1"}));
        }
    }

    protected void validateH068(ValidationResult result, AnexoHq07T7_Linha anexoHq07T7Linha, String titular, String linkCodBeneficio) {
        if (!(anexoHq07T7Linha.getCodBeneficio() == null || !Modelo3IRSValidatorUtil.in(anexoHq07T7Linha.getCodBeneficio(), new long[]{701, 703, 704, 705, 711, 732, 733, 738, 739, 740, 741, 743}) || StringUtil.isEmpty(titular) || StringUtil.in(titular, new String[]{"A", "B", "F"}))) {
            result.add(new DeclValidationMessage("H068", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateH080(ValidationResult result, AnexoHq07T7_Linha current, Long anoRendimentos, int numLinha) {
        String linkNifPortugues = AnexoHq07T7_Linha.getLink(numLinha, AnexoHq07T7_LinhaBase.Property.NIFNIPCPORTUGUES);
        Long codBeneficio = current.getCodBeneficio();
        Long nifPortugues = current.getNifNipcPortugues();
        if (!(codBeneficio == null || anoRendimentos == null || (codBeneficio != 707 || anoRendimentos >= 2011) && (codBeneficio != 742 || anoRendimentos <= 2010) || nifPortugues == null || Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "98"))) {
            result.add(new DeclValidationMessage("H080", linkNifPortugues, new String[]{linkNifPortugues}));
        }
    }

    public void validateH250(ValidationResult result, AnexoHq07T7_Linha current, Long anoRendimentos, int numLinha) {
        Long codBeneficio = current.getCodBeneficio();
        String linkCodigo = AnexoHq07T7_Linha.getLink(numLinha, AnexoHq07T7_LinhaBase.Property.CODBENEFICIO);
        if (codBeneficio != null && codBeneficio == 707 && anoRendimentos != null && anoRendimentos > 2010) {
            result.add(new DeclValidationMessage("H250", linkCodigo, new String[]{linkCodigo, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    public void validateH265(ValidationResult result, AnexoHq07T7_Linha current, Long anoExercicio, int numLinha) {
        Long codBeneficio = current.getCodBeneficio();
        String linkCodBeneficio = AnexoHq07T7_Linha.getLink(numLinha, AnexoHq07T7_LinhaBase.Property.CODBENEFICIO);
        if (codBeneficio != null && codBeneficio == 741 && anoExercicio != null && anoExercicio < 2011) {
            result.add(new DeclValidationMessage("H265", linkCodBeneficio, new String[]{linkCodBeneficio, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
        if (anexoHModel == null) {
            return result;
        }
        pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07 quadro07Model = (pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07)anexoHModel.getQuadro(pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        for (int linha = 0; linha < quadro07Model.getAnexoHq07T7().size(); ++linha) {
            AnexoHq07T7_Linha anexoHq07T7Linha = quadro07Model.getAnexoHq07T7().get(linha);
            Long codBeneficio = anexoHq07T7Linha.getCodBeneficio();
            String titular = anexoHq07T7Linha.getTitular();
            Long importanciaAplicada = anexoHq07T7Linha.getImportanciaAplicada();
            Long nifPortugues = anexoHq07T7Linha.getNifNipcPortugues();
            String numFiscalUE = anexoHq07T7Linha.getNumeroFiscalUE();
            String linkCurrentLine = "aAnexoH.qQuadro07.tanexoHq07T7.l" + (linha + 1);
            String linkCodBeneficio = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.CODBENEFICIO.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.TITULAR.getIndex();
            String linkImportanciaAplicada = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.IMPORTANCIAAPLICADA.getIndex();
            String linkNifPortugues = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.NIFNIPCPORTUGUES.getIndex();
            String linkNumFiscalUE = linkCurrentLine + ".c" + AnexoHq07T7_LinhaBase.Property.NUMEROFISCALUE.getIndex();
            if (codBeneficio != null && (codBeneficio < 701 || codBeneficio > 743 || codBeneficio == 713 || codBeneficio == 714)) {
                result.add(new DeclValidationMessage("H059", linkCodBeneficio, new String[]{linkCodBeneficio}));
            }
            this.validateH060(result, codBeneficio, anoExercicio, nifPortugues, linkCodBeneficio, linkNifPortugues);
            this.validateH061(result, codBeneficio, titular, importanciaAplicada, linkCodBeneficio, linkTitular, linkImportanciaAplicada);
            this.validateH062(result, codBeneficio, titular, importanciaAplicada, nifPortugues, numFiscalUE, linkCodBeneficio, linkNifPortugues, linkNumFiscalUE);
            this.validateH083(result, codBeneficio, nifPortugues, linkNifPortugues, anoExercicio);
            this.validateH089(result, anexoHq07T7Linha, anoExercicio, linkNifPortugues, linkCodBeneficio);
            this.validateH101(result, codBeneficio, importanciaAplicada, linkImportanciaAplicada);
            this.validateH103(result, anexoHq07T7Linha, anoExercicio, linha + 1);
        }
        this.validateH192(result, rostoModel, anexoHModel);
        this.validateH285(result, rostoModel, anexoHModel);
        return result;
    }

    protected void validateH192(ValidationResult result, RostoModel rostoModel, AnexoHModel anexoHModel) {
        long ano2012 = 2012;
        if (!(anexoHModel == null || anexoHModel.getQuadro07().getAnexoHq07T7() == null || anexoHModel.getQuadro07().getAnexoHq07T7().isEmpty())) {
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            for (AnexoHq08T814_Linha linha814 : anexoHModel.getQuadro08().getAnexoHq08T814()) {
                Tuple tuple;
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha814.getCodigo()) || StringUtil.isEmpty(linha814.getTitular()) || processedTuples.contains(tuple = new Tuple(new Object[]{linha814.getCodigo(), linha814.getTitular()}))) continue;
                processedTuples.add(tuple);
            }
            Long codPais = anexoHModel.getQuadro08().getAnexoHq08C814();
            for (int idx = 0; idx < anexoHModel.getQuadro07().getAnexoHq07T7().size(); ++idx) {
                AnexoHq07T7_Linha anexoHq07T7Linha = anexoHModel.getQuadro07().getAnexoHq07T7().get(idx);
                Long codBeneficio = anexoHq07T7Linha.getCodBeneficio();
                String titular = anexoHq07T7Linha.getTitular();
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(codBeneficio) || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{731, 741}) && (!Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{739}) || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() >= 2012) || StringUtil.isEmpty(titular) || !Modelo3IRSValidatorUtil.isEmptyOrZero(codPais) || processedTuples.contains(new Tuple(new Object[]{codBeneficio, titular}))) continue;
                result.add(new DeclValidationMessage("H192", AnexoHq07T7_Linha.getLink(idx + 1, AnexoHq07T7_LinhaBase.Property.CODBENEFICIO), new String[]{"aAnexoH.qQuadro08.tanexoHq08T814", AnexoHq07T7_Linha.getLink(idx + 1, AnexoHq07T7_LinhaBase.Property.CODBENEFICIO)}));
            }
        }
    }

    protected void validateH194(ValidationResult result, Long pais, String numFiscalUE, String linkPais, String linkNumFiscalUE) {
        if (pais != null && Modelo3IRSValidatorUtil.in(pais, new long[]{276, 40, 56, 100, 203, 191, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 352, 380, 428, 438, 440, 442, 470, 578, 528, 616, 642, 826, 752}) && numFiscalUE == null || pais == null && numFiscalUE != null) {
            result.add(new DeclValidationMessage("H194", linkPais, new String[]{linkPais, linkNumFiscalUE}));
        }
    }

    protected void validateH285(ValidationResult result, RostoModel rostoModel, AnexoHModel anexoHModel) {
        if (!(anexoHModel == null || anexoHModel.getQuadro07().getAnexoHq07T7() == null || anexoHModel.getQuadro07().getAnexoHq07T7().isEmpty())) {
            ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
            for (AnexoHq08T814_Linha linha814 : anexoHModel.getQuadro08().getAnexoHq08T814()) {
                Tuple tuple;
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha814.getCodigo()) || StringUtil.isEmpty(linha814.getTitular()) || processedTuples.contains(tuple = new Tuple(new Object[]{linha814.getCodigo(), linha814.getTitular()}))) continue;
                processedTuples.add(tuple);
            }
            for (int idx = 0; idx < anexoHModel.getQuadro07().getAnexoHq07T7().size(); ++idx) {
                AnexoHq07T7_Linha anexoHq07T7Linha = anexoHModel.getQuadro07().getAnexoHq07T7().get(idx);
                Long codBeneficio = anexoHq07T7Linha.getCodBeneficio();
                String titular = anexoHq07T7Linha.getTitular();
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(codBeneficio) || !Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{736}) || StringUtil.isEmpty(titular) || processedTuples.contains(new Tuple(new Object[]{codBeneficio, titular}))) continue;
                result.add(new DeclValidationMessage("H285", AnexoHq07T7_Linha.getLink(idx + 1, AnexoHq07T7_LinhaBase.Property.CODBENEFICIO), new String[]{"aAnexoH.qQuadro08.tanexoHq08T814", AnexoHq07T7_Linha.getLink(idx + 1, AnexoHq07T7_LinhaBase.Property.CODBENEFICIO)}));
            }
        }
    }

    protected void validateH101(ValidationResult result, Long codBeneficio, Long importanciaAplicada, String linkImportanciaAplicada) {
        if (importanciaAplicada != null && importanciaAplicada == 0 && (codBeneficio == null || codBeneficio != null && codBeneficio != 732)) {
            result.add(new DeclValidationMessage("H101", linkImportanciaAplicada, new String[]{linkImportanciaAplicada}));
        }
    }

    protected void validateH089(ValidationResult result, AnexoHq07T7_Linha anexoHq07T7Linha, Long anoExercicio, String linkNifPortugues, String linkCodBeneficio) {
        if (!(anexoHq07T7Linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHq07T7Linha.getCodBeneficio()) || !Modelo3IRSValidatorUtil.in(anexoHq07T7Linha.getCodBeneficio(), new long[]{734, 735}) || anexoHq07T7Linha.getNifNipcPortugues() != null && Modelo3IRSValidatorUtil.in(anexoHq07T7Linha.getNifNipcPortugues(), new long[]{901775797}) || anoExercicio == null || anoExercicio >= 2012)) {
            result.add(new DeclValidationMessage("H089", linkCodBeneficio, new String[]{linkNifPortugues}));
        }
    }

    protected void validateH083(ValidationResult result, Long codBeneficio, Long nifPortugues, String linkNifPortugues, Long anoRendimentos) {
        if (codBeneficio != null && (codBeneficio == 729 && anoRendimentos != null && anoRendimentos < 2011 || Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{729, 730, 731})) && nifPortugues != null && !Modelo3IRSValidatorUtil.startsWith(nifPortugues, "5", "6", "9")) {
            result.add(new DeclValidationMessage("H083", linkNifPortugues, new String[]{linkNifPortugues}));
        }
    }

    protected void validateH062(ValidationResult result, Long codBeneficio, String titular, Long importanciaAplicada, Long nifPortugues, String numFiscalUE, String linkCodBeneficio, String linkNifPortugues, String linkNumFiscalUE) {
        if (!(codBeneficio == null || Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{723, 702, 709, 710, 715, 716, 724, 725, 726, 727, 728, 708, 706, 733, 737, 738, 739, 740}) || StringUtil.isEmpty(titular) || importanciaAplicada == null || nifPortugues != null || numFiscalUE != null)) {
            result.add(new DeclValidationMessage("H062", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues, linkNumFiscalUE}));
        }
    }

    protected void validateH061(ValidationResult result, Long codBeneficio, String titular, Long importanciaAplicada, String linkCodBeneficio, String linkTitular, String linkImportanciaAplicada) {
        if (codBeneficio != null && codBeneficio > 0 && (titular == null || importanciaAplicada == null)) {
            result.add(new DeclValidationMessage("H061", linkTitular, new String[]{linkCodBeneficio, linkTitular, linkImportanciaAplicada}));
        }
    }

    protected void validateH060(ValidationResult result, Long codBeneficio, Long anoExercicio, Long nifPortugues, String linkCodBeneficio, String linkNifPortugues) {
        if (codBeneficio != null && (Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{723, 702, 709, 710, 715, 716, 724, 725, 726, 727, 728, 737}) || Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{738, 739, 740}) && anoExercicio != null && anoExercicio < 2012) && Modelo3IRSValidatorUtil.isEmptyOrZero(nifPortugues)) {
            result.add(new DeclValidationMessage("H060", linkNifPortugues, new String[]{linkCodBeneficio, linkNifPortugues}));
        }
    }

    protected void validateH224(ValidationResult result, Long codBeneficio, Long anoExercicio, Long pais, String numFiscalUE, String linkNumFiscalUE, String linkCodBeneficio, String linkNifPortugues) {
        if (codBeneficio != null && (Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{723, 702, 709, 710, 715, 716, 724, 725, 726, 727, 728, 737}) || Modelo3IRSValidatorUtil.in(codBeneficio, new long[]{739, 740}) && anoExercicio != null && anoExercicio < 2012) && (!Modelo3IRSValidatorUtil.isEmptyOrZero(pais) || !StringUtil.isEmpty(numFiscalUE))) {
            result.add(new DeclValidationMessage("H224", linkNumFiscalUE, new String[]{linkCodBeneficio, linkNifPortugues}));
        }
    }

    protected void validateH103(ValidationResult result, AnexoHq07T7_Linha current, Long anoRendimentos, int numLinha) {
        Long codBeneficio = current.getCodBeneficio();
        Long importanciaAplicada = current.getImportanciaAplicada();
        String linkImportancia = AnexoHq07T7_Linha.getLink(numLinha, AnexoHq07T7_LinhaBase.Property.IMPORTANCIAAPLICADA);
        if (codBeneficio != null && importanciaAplicada != null && importanciaAplicada > 30000000 && (codBeneficio != 707 && anoRendimentos != null && anoRendimentos < 2011 || codBeneficio != 742 && anoRendimentos != null && anoRendimentos > 2010)) {
            result.add(new DeclValidationMessage("H103", linkImportancia, new String[]{linkImportancia}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public void validateH261(ValidationResult result, AnexoHq07T7_Linha current, Long anoRendimentos, int numLinha) {
        Long codBeneficio = current.getCodBeneficio();
        String linkCodigo = AnexoHq07T7_Linha.getLink(numLinha, AnexoHq07T7_LinhaBase.Property.CODBENEFICIO);
        if (codBeneficio != null && codBeneficio == 729 && anoRendimentos != null && anoRendimentos > 2010) {
            result.add(new DeclValidationMessage("H261", linkCodigo, new String[]{linkCodigo, "aRosto.qQuadro02.fq02C02"}));
        }
    }
}

