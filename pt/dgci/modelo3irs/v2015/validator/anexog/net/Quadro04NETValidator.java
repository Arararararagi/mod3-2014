/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.Quadro04Validator;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        Quadro04 quadro04AnexoGModel = anexoGModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        result.addAllFrom(this.validaNETQ4T1(rostoModel, quadro04AnexoGModel));
        result.addAllFrom(this.validaNETQ4AT1(rostoModel, quadro04AnexoGModel));
        result.addAllFrom(this.validaNETQ4T2(rostoModel, quadro04AnexoGModel));
        return result;
    }

    protected ValidationResult validaNETQ4AT1(RostoModel rostoModel, Quadro04 quadro04AnexoGModel) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected ValidationResult validaNETQ4T1(RostoModel rostoModel, Quadro04 quadro04AnexoGModel) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected ValidationResult validaNETQ4T2(RostoModel rostoModel, Quadro04 quadro04AnexoGModel) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

