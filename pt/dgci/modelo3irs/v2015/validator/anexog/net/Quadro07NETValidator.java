/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.Quadro07Validator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro07NETValidator
extends Quadro07Validator {
    public Quadro07NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

