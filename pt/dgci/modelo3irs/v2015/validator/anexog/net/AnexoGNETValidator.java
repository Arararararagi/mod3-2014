/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.AnexoGValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro06NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro07NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro08NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro09NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexog.net.Quadro10NETValidator;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoGNETValidator
extends AnexoGValidator {
    public AnexoGNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        result.addAllFrom(new Quadro07NETValidator().validate(model));
        result.addAllFrom(new Quadro08NETValidator().validate(model));
        result.addAllFrom(new Quadro09NETValidator().validate(model));
        result.addAllFrom(new Quadro10NETValidator().validate(model));
        return result;
    }
}

