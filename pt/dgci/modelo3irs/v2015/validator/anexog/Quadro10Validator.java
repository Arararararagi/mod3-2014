/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro10;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro10Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro10Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro10 quadro10AnexoGModel = anexoGModel.getQuadro10();
        String fTitularLinha1 = quadro10AnexoGModel.getAnexoGq10C1001a() != null ? quadro10AnexoGModel.getAnexoGq10C1001a() : "";
        String fTitularLinha2 = quadro10AnexoGModel.getAnexoGq10C1002a() != null ? quadro10AnexoGModel.getAnexoGq10C1002a() : "";
        Long fRendimentoLinha1 = quadro10AnexoGModel.getAnexoGq10C1001b() != null ? quadro10AnexoGModel.getAnexoGq10C1001b() : 0;
        Long fRendimentoLinha2 = quadro10AnexoGModel.getAnexoGq10C1002b() != null ? quadro10AnexoGModel.getAnexoGq10C1002b() : 0;
        Long fRetencaoLinha1 = quadro10AnexoGModel.getAnexoGq10C1001c() != null ? quadro10AnexoGModel.getAnexoGq10C1001c() : 0;
        Long fRetencaoLinha2 = quadro10AnexoGModel.getAnexoGq10C1002c() != null ? quadro10AnexoGModel.getAnexoGq10C1002c() : 0;
        Long fSomaControloRendimento = quadro10AnexoGModel.getAnexoGq10C1b() != null ? quadro10AnexoGModel.getAnexoGq10C1b() : 0;
        Long fSomaControloRetencao = quadro10AnexoGModel.getAnexoGq10C1c() != null ? quadro10AnexoGModel.getAnexoGq10C1c() : 0;
        String linkTitularLinha1 = "aAnexoG.qQuadro10.fanexoGq10C1001a";
        String linkTitularLinha2 = "aAnexoG.qQuadro10.fanexoGq10C1002a";
        String linkRendimentoLinha1 = "aAnexoG.qQuadro10.fanexoGq10C1001b";
        String linkRendimentoLinha2 = "aAnexoG.qQuadro10.fanexoGq10C1002b";
        String linkRetencaoLinha1 = "aAnexoG.qQuadro10.fanexoGq10C1001c";
        String linkRetencaoLinha2 = "aAnexoG.qQuadro10.fanexoGq10C1002c";
        String linkSomaControloRendimento = "aAnexoG.qQuadro10.fanexoGq10C1b";
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro10AnexoGModel.getAnexoGq10C1001a());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro10AnexoGModel.getAnexoGq10C1002a());
        boolean hasRendimentoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1001b());
        boolean hasRendimentoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1002b());
        boolean hasRetencaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1001c());
        boolean hasRetencaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1002c());
        boolean hasSomaControloRendimento = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1b());
        boolean hasSomaControloRetencao = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1c());
        result.addAllFrom(this.validaPrimeiraLinha(hasTitularLinha1, hasRendimentoLinha1, hasRetencaoLinha1, rostoModel, fTitularLinha1, fRendimentoLinha1, fRetencaoLinha1, linkTitularLinha1, linkRendimentoLinha1, linkRetencaoLinha1));
        result.addAllFrom(this.validaSegundaLinha(hasTitularLinha2, hasRendimentoLinha2, hasRetencaoLinha2, rostoModel, fTitularLinha2, fRendimentoLinha2, fRetencaoLinha2, linkTitularLinha2, linkRendimentoLinha2, linkRetencaoLinha2));
        long soma = 0;
        long somaControlo = 0;
        if (hasRendimentoLinha1) {
            soma = fRendimentoLinha1;
        }
        if (hasRendimentoLinha2) {
            soma+=fRendimentoLinha2.longValue();
        }
        if (hasSomaControloRendimento) {
            somaControlo = fSomaControloRendimento;
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("G270", linkSomaControloRendimento, new String[]{linkSomaControloRendimento, linkRendimentoLinha1, linkRendimentoLinha2}));
        }
        soma = 0;
        somaControlo = 0;
        if (hasRetencaoLinha1) {
            soma = fRetencaoLinha1;
        }
        if (hasRetencaoLinha2) {
            soma+=fRetencaoLinha2.longValue();
        }
        if (hasSomaControloRetencao) {
            somaControlo = fSomaControloRetencao;
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("G271", linkSomaControloRendimento, new String[]{linkSomaControloRendimento, linkRetencaoLinha1, linkRetencaoLinha2}));
        }
        if (quadro10AnexoGModel.getAnexoGq10T1().size() > 20) {
            result.add(new DeclValidationMessage("G284", "aAnexoG.qQuadro10.tanexoGq10T1", new String[]{"aAnexoG.qQuadro10.tanexoGq10T1"}));
        }
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        for (int linha = 0; linha < quadro10AnexoGModel.getAnexoGq10T1().size(); ++linha) {
            AnexoGq10T1_Linha anexoGq10T1Linha = quadro10AnexoGModel.getAnexoGq10T1().get(linha);
            Long fNIF = anexoGq10T1Linha.getNIF();
            Long fValor = anexoGq10T1Linha.getValor();
            boolean hasNIF = !Modelo3IRSValidatorUtil.isEmptyOrZero(fNIF);
            boolean hasValor = fValor != null;
            String linkCurrentLine = "aAnexoG.qQuadro10.tanexoGq10T1.l" + (linha + 1);
            String linkNIF = linkCurrentLine + ".c" + AnexoGq10T1_LinhaBase.Property.NIF.getIndex();
            String linkValor = linkCurrentLine + ".c" + AnexoGq10T1_LinhaBase.Property.VALOR.getIndex();
            if (hasNIF) {
                Tuple tuple = new Tuple(new Object[]{fNIF});
                if (processedTuples.contains(tuple)) {
                    result.add(new DeclValidationMessage("G285", linkCurrentLine, new String[]{linkNIF}));
                } else {
                    processedTuples.add(tuple);
                }
            }
            if (!(hasNIF || hasValor)) {
                result.add(new DeclValidationMessage("G286", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (hasNIF && !hasValor) {
                result.add(new DeclValidationMessage("G287", linkCurrentLine, new String[]{linkNIF, linkValor}));
            }
            if (!hasNIF && hasValor) {
                result.add(new DeclValidationMessage("G288", linkCurrentLine, new String[]{linkNIF}));
            }
            if (hasNIF && !NifValidator.checkFirstDigit(String.valueOf(fNIF), "125679")) {
                result.add(new DeclValidationMessage("G276", linkNIF, new String[]{linkNIF}));
            }
            if (hasNIF && !Modelo3IRSValidatorUtil.isNIFValid(fNIF)) {
                result.add(new DeclValidationMessage("G277", linkNIF, new String[]{linkNIF}));
            }
            this.validateG278(result, rostoModel, anexoGq10T1Linha, linkNIF);
        }
        return result;
    }

    protected void validateG278(ValidationResult result, RostoModel rostoModel, AnexoGq10T1_Linha anexoGq10T1Linha, String linkNIF) {
        Long nif = anexoGq10T1Linha.getNIF();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nif) && (rostoModel.getQuadro03().isSujeitoPassivoA(nif) || rostoModel.getQuadro03().isSujeitoPassivoB(nif) || rostoModel.getQuadro03().existsInDependentes(nif) || rostoModel.getQuadro07().isConjugeFalecido(nif) || rostoModel.getQuadro07().existsInAscendentes(nif) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nif) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nif))) {
            result.add(new DeclValidationMessage("G278", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    private ValidationResult validaPrimeiraLinha(boolean hasTitular, boolean hasRendimento, boolean hasRetencao, RostoModel rostoModel, String fTitular, Long fRendimento, Long fRetencao, String linkTitular, String linkRendimento, String linkRetencao) {
        ValidationResult result = new ValidationResult();
        boolean isTitularValido = true;
        boolean isTitularNIFPreenchidoRosto = true;
        this.validateG255(result, fTitular, linkTitular);
        this.validateG256(result, rostoModel, fTitular, linkTitular);
        this.validateG257(hasTitular, hasRendimento, rostoModel, fTitular, linkTitular, linkRendimento, result, isTitularValido, isTitularNIFPreenchidoRosto);
        if (hasRetencao && fRetencao > (hasRendimento ? fRendimento : 0)) {
            result.add(new DeclValidationMessage("G260", linkRetencao, new String[]{linkRetencao, linkRendimento}));
        }
        if (hasRendimento && hasRetencao && (double)fRetencao.longValue() > (double)fRendimento.longValue() * 0.17) {
            result.add(new DeclValidationMessage("G261", linkRetencao, new String[]{linkRetencao}));
        }
        return result;
    }

    protected void validateG257(boolean hasTitular, boolean hasRendimento, RostoModel rostoModel, String fTitular, String linkTitular, String linkRendimento, ValidationResult result, boolean isTitularValido, boolean isTitularNIFPreenchidoRosto) {
        if (!(StringUtil.isEmpty(fTitular) || Quadro03.isSujeitoPassivoA(fTitular) || Quadro03.isSujeitoPassivoB(fTitular) || Quadro03.isSujeitoPassivoC(fTitular) || Quadro03.isDependenteNaoDeficiente(fTitular) || Quadro03.isDependenteDeficiente(fTitular) || Quadro07.isConjugeFalecido(fTitular) || Quadro03.isDependenteEmGuardaConjunta(fTitular))) {
            isTitularValido = false;
        }
        if (!(StringUtil.isEmpty(fTitular) || rostoModel.isNIFTitularPreenchido(fTitular))) {
            isTitularNIFPreenchidoRosto = false;
        }
        if (hasTitular && isTitularValido && isTitularNIFPreenchidoRosto && !hasRendimento) {
            result.add(new DeclValidationMessage("G257", linkTitular, new String[]{linkRendimento, linkTitular}));
        }
    }

    protected void validateG255(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G255", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateG256(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G256", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    private ValidationResult validaSegundaLinha(boolean hasTitular, boolean hasRendimento, boolean hasRetencao, RostoModel rostoModel, String fTitular, Long fRendimento, Long fRetencao, String linkTitular, String linkRendimento, String linkRetencao) {
        ValidationResult result = new ValidationResult();
        boolean isTitularValido = true;
        boolean isTitularNIFPreenchidoRosto = true;
        this.validateG262(result, fTitular, linkTitular);
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G263", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G264", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (!(StringUtil.isEmpty(fTitular) || Quadro03.isSujeitoPassivoA(fTitular) || Quadro03.isSujeitoPassivoB(fTitular) || Quadro03.isSujeitoPassivoC(fTitular) || Quadro03.isDependenteNaoDeficiente(fTitular) || Quadro03.isDependenteDeficiente(fTitular) || Quadro07.isConjugeFalecido(fTitular) || Quadro03.isDependenteEmGuardaConjunta(fTitular))) {
            isTitularValido = false;
        }
        if (hasTitular && isTitularValido && isTitularNIFPreenchidoRosto && !hasRendimento) {
            result.add(new DeclValidationMessage("G265", linkTitular, new String[]{linkRendimento, linkTitular}));
        }
        if (hasRetencao && fRetencao > (hasRendimento ? fRendimento : 0)) {
            result.add(new DeclValidationMessage("G268", linkRetencao, new String[]{linkRetencao, linkRendimento}));
        }
        if (hasRendimento && hasRetencao && (double)fRetencao.longValue() > (double)fRendimento.longValue() * 0.17) {
            result.add(new DeclValidationMessage("G269", linkRetencao, new String[]{linkRetencao}));
        }
        return result;
    }

    protected void validateG262(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G262", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        Quadro10 quadro10AnexoGModel = anexoGModel.getQuadro10();
        Long fSomaControloRetencao = quadro10AnexoGModel.getAnexoGq10C1c() != null ? quadro10AnexoGModel.getAnexoGq10C1c() : 0;
        String linkTitularLinha1 = "aAnexoG.qQuadro10.fanexoGq10C1001a";
        String linkTitularLinha2 = "aAnexoG.qQuadro10.fanexoGq10C1002a";
        String linkRendimentoLinha1 = "aAnexoG.qQuadro10.fanexoGq10C1001b";
        String linkRendimentoLinha2 = "aAnexoG.qQuadro10.fanexoGq10C1002b";
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro10AnexoGModel.getAnexoGq10C1001a());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro10AnexoGModel.getAnexoGq10C1002a());
        boolean hasRendimentoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1001b());
        boolean hasRendimentoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1002b());
        boolean hasSomaControloRetencao = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10AnexoGModel.getAnexoGq10C1c());
        result.addAllFrom(this.validateG258(hasTitularLinha1, hasRendimentoLinha1, linkTitularLinha1, linkRendimentoLinha1));
        result.addAllFrom(this.validateG266(hasTitularLinha2, hasRendimentoLinha2, linkTitularLinha2, linkRendimentoLinha2));
        long total_retencoes = hasSomaControloRetencao ? fSomaControloRetencao : 0;
        long somaRetencoes = 0;
        for (int linha = 0; linha < quadro10AnexoGModel.getAnexoGq10T1().size(); ++linha) {
            boolean hasValor;
            AnexoGq10T1_Linha anexoGq10T1Linha = quadro10AnexoGModel.getAnexoGq10T1().get(linha);
            Long fValor = anexoGq10T1Linha.getValor();
            boolean bl = hasValor = !Modelo3IRSValidatorUtil.isEmptyOrZero(fValor);
            if (!hasValor) continue;
            somaRetencoes+=fValor.longValue();
        }
        if (total_retencoes != somaRetencoes) {
            result.add(new DeclValidationMessage("G281", "aAnexoG.qQuadro10.fanexoGq10C1c", new String[]{"aAnexoG.qQuadro10.fanexoGq10C1c"}));
        }
        return result;
    }

    private ValidationResult validateG258(boolean hasTitular, boolean hasRendimento, String linkTitular, String linkRendimento) {
        ValidationResult result = new ValidationResult();
        if (hasRendimento && !hasTitular) {
            result.add(new DeclValidationMessage("G258", linkRendimento, new String[]{linkTitular, linkRendimento}));
        }
        return result;
    }

    private ValidationResult validateG266(boolean hasTitular, boolean hasRendimento, String linkTitular, String linkRendimento) {
        ValidationResult result = new ValidationResult();
        if (hasRendimento && !hasTitular) {
            result.add(new DeclValidationMessage("G266", linkRendimento, new String[]{linkTitular, linkRendimento}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

