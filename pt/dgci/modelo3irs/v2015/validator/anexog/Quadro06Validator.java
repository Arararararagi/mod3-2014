/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro06 quadro06AnexoGModel = anexoGModel.getQuadro06();
        String fTitularLinha1 = quadro06AnexoGModel.getAnexoGq06C601b() != null ? quadro06AnexoGModel.getAnexoGq06C601b() : "";
        Long fValorRealizacaoLinha1 = quadro06AnexoGModel.getAnexoGq06C601c() != null ? quadro06AnexoGModel.getAnexoGq06C601c() : 0;
        Long fValorAquisicaoLinha1 = quadro06AnexoGModel.getAnexoGq06C601d() != null ? quadro06AnexoGModel.getAnexoGq06C601d() : 0;
        Long fDespesasLinha1 = quadro06AnexoGModel.getAnexoGq06C601e() != null ? quadro06AnexoGModel.getAnexoGq06C601e() : 0;
        String fTitularLinha2 = quadro06AnexoGModel.getAnexoGq06C602b() != null ? quadro06AnexoGModel.getAnexoGq06C602b() : "";
        Long fValorRealizacaoLinha2 = quadro06AnexoGModel.getAnexoGq06C602c() != null ? quadro06AnexoGModel.getAnexoGq06C602c() : 0;
        Long fValorAquisicaoLinha2 = quadro06AnexoGModel.getAnexoGq06C602d() != null ? quadro06AnexoGModel.getAnexoGq06C602d() : 0;
        Long fDespesasLinha2 = quadro06AnexoGModel.getAnexoGq06C602e() != null ? quadro06AnexoGModel.getAnexoGq06C602e() : 0;
        String linkTitularLinha1 = "aAnexoG.qQuadro06.fanexoGq06C601b";
        String linkValorRealizacaoLinha1 = "aAnexoG.qQuadro06.fanexoGq06C601c";
        String linkValorAquisicaoLinha1 = "aAnexoG.qQuadro06.fanexoGq06C601d";
        String linkDespesasLinha1 = "aAnexoG.qQuadro06.fanexoGq06C601e";
        String linkTitularLinha2 = "aAnexoG.qQuadro06.fanexoGq06C602b";
        String linkValorRealizacaoLinha2 = "aAnexoG.qQuadro06.fanexoGq06C602c";
        String linkValorAquisicaoLinha2 = "aAnexoG.qQuadro06.fanexoGq06C602d";
        String linkDespesasLinha2 = "aAnexoG.qQuadro06.fanexoGq06C602e";
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro06AnexoGModel.getAnexoGq06C601b());
        boolean hasValorRealizacaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C601c());
        boolean hasValorAquisicaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C601d());
        boolean hasDespesasLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C601e());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro06AnexoGModel.getAnexoGq06C602b());
        boolean hasValorRealizacaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C602c());
        boolean hasValorAquisicaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C602d());
        boolean hasDespesasLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C602e());
        this.validateG184(result, fTitularLinha1, linkTitularLinha1);
        this.validateG185(result, rostoModel, fTitularLinha1, linkTitularLinha1);
        result.addAllFrom(this.validaTitular_ValorRealizacao(hasValorRealizacaoLinha1, hasTitularLinha1, linkValorRealizacaoLinha1));
        result.addAllFrom(this.validateG187(quadro06AnexoGModel.getAnexoGq06C601d() != null, hasTitularLinha1, linkValorAquisicaoLinha1));
        this.validateG184(result, fTitularLinha2, linkTitularLinha2);
        this.validateG185(result, rostoModel, fTitularLinha2, linkTitularLinha2);
        result.addAllFrom(this.validaTitular_ValorRealizacao(hasValorRealizacaoLinha2, hasTitularLinha2, linkValorRealizacaoLinha2));
        result.addAllFrom(this.validateG187(quadro06AnexoGModel.getAnexoGq06C602d() != null, hasTitularLinha2, linkValorAquisicaoLinha2));
        result.addAllFrom(this.validaSomaValorRealizacao(quadro06AnexoGModel, hasValorRealizacaoLinha1, hasValorRealizacaoLinha2, fValorRealizacaoLinha1, fValorRealizacaoLinha2, linkValorRealizacaoLinha1, linkValorRealizacaoLinha2));
        result.addAllFrom(this.validaSomaValorAquisicao(quadro06AnexoGModel, hasValorAquisicaoLinha1, hasValorAquisicaoLinha2, fValorAquisicaoLinha1, fValorAquisicaoLinha2, linkValorAquisicaoLinha1, linkValorAquisicaoLinha2));
        result.addAllFrom(this.validaSomaValorDespesasEncargos(quadro06AnexoGModel, hasDespesasLinha1, hasDespesasLinha2, fDespesasLinha1, fDespesasLinha2, linkDespesasLinha1, linkDespesasLinha2));
        return result;
    }

    protected void validateG184(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G184", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateG185(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G185", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    private ValidationResult validaTitular_ValorRealizacao(boolean hasValorRealizacao, boolean hasTitular, String linkValorRealizacao) {
        ValidationResult result = new ValidationResult();
        if (!hasValorRealizacao && hasTitular) {
            result.add(new DeclValidationMessage("G186", linkValorRealizacao, new String[]{linkValorRealizacao}));
        }
        return result;
    }

    private ValidationResult validateG187(boolean hasValorAquisicao, boolean hasTitular, String linkValorAquisicao) {
        ValidationResult result = new ValidationResult();
        if (!hasValorAquisicao && hasTitular) {
            result.add(new DeclValidationMessage("G187", linkValorAquisicao, new String[]{linkValorAquisicao}));
        }
        return result;
    }

    private ValidationResult validaSomaValorRealizacao(Quadro06 quadro06AnexoGModel, boolean hasValorRealizacaoLinha1, boolean hasValorRealizacaoLinha2, Long fValorRealizacaoLinha1, Long fValorRealizacaoLinha2, String linkValorRealizacaoLinha1, String linkValorRealizacaoLinha2) {
        ValidationResult result = new ValidationResult();
        long soma = 0;
        long somaControlo = 0;
        if (hasValorRealizacaoLinha1) {
            soma = fValorRealizacaoLinha1;
        }
        if (hasValorRealizacaoLinha2) {
            soma+=fValorRealizacaoLinha2.longValue();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C1c())) {
            somaControlo = quadro06AnexoGModel.getAnexoGq06C1c();
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("YG004", "aAnexoG.qQuadro06.fanexoGq06C1c", new String[]{"aAnexoG.qQuadro06.fanexoGq06C1c", linkValorRealizacaoLinha1, linkValorRealizacaoLinha2}));
        }
        return result;
    }

    private ValidationResult validaSomaValorAquisicao(Quadro06 quadro06AnexoGModel, boolean hasValorAquisicaoLinha1, boolean hasValorAquisicaoLinha2, Long fValorAquisicaoLinha1, Long fValorAquisicaoLinha2, String linkValorAquisicaoLinha1, String linkValorAquisicaoLinha2) {
        ValidationResult result = new ValidationResult();
        long soma = 0;
        long somaControlo = 0;
        if (hasValorAquisicaoLinha1) {
            soma = fValorAquisicaoLinha1;
        }
        if (hasValorAquisicaoLinha2) {
            soma+=fValorAquisicaoLinha2.longValue();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C1d())) {
            somaControlo = quadro06AnexoGModel.getAnexoGq06C1d();
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("YG005", "aAnexoG.qQuadro06.fanexoGq06C1d", new String[]{"aAnexoG.qQuadro06.fanexoGq06C1d", linkValorAquisicaoLinha1, linkValorAquisicaoLinha2}));
        }
        return result;
    }

    private ValidationResult validaSomaValorDespesasEncargos(Quadro06 quadro06AnexoGModel, boolean hasDespesasLinha1, boolean hasDespesasLinha2, Long despesasLinha1, Long despesasLinha2, String linkDespesasLinha1, String linkDespesasLinha2) {
        ValidationResult result = new ValidationResult();
        long soma = 0;
        long somaControlo = 0;
        if (hasDespesasLinha1) {
            soma = despesasLinha1;
        }
        if (hasDespesasLinha2) {
            soma+=despesasLinha2.longValue();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C1e())) {
            somaControlo = quadro06AnexoGModel.getAnexoGq06C1e();
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("YG006", "aAnexoG.qQuadro06.fanexoGq06C1e", new String[]{"aAnexoG.qQuadro06.fanexoGq06C1e", linkDespesasLinha1, linkDespesasLinha2}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        Quadro06 quadro06AnexoGModel = anexoGModel.getQuadro06();
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro06AnexoGModel.getAnexoGq06C601b());
        boolean hasValorRealizacaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C601c());
        boolean hasValorAquisicaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C601d());
        boolean hasDespesasLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C601e());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro06AnexoGModel.getAnexoGq06C602b());
        boolean hasValorRealizacaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C602c());
        boolean hasValorAquisicaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C602d());
        boolean hasDespesasLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro06AnexoGModel.getAnexoGq06C602e());
        String linkTitularLinha1 = "aAnexoG.qQuadro06.fanexoGq06C601b";
        String linkTitularLinha2 = "aAnexoG.qQuadro06.fanexoGq06C602b";
        result.addAllFrom(this.validaValorRealizacao(hasTitularLinha1, hasValorRealizacaoLinha1, hasValorAquisicaoLinha1, hasDespesasLinha1, linkTitularLinha1));
        result.addAllFrom(this.validaValorAquisicao(hasTitularLinha1, hasValorRealizacaoLinha1, hasValorAquisicaoLinha1, hasDespesasLinha1, linkTitularLinha1));
        this.validateG335(result, hasValorRealizacaoLinha1, hasDespesasLinha1, "aAnexoG.qQuadro06.fanexoGq06C601e", "aAnexoG.qQuadro06.fanexoGq06C601c");
        result.addAllFrom(this.validaValorRealizacao(hasTitularLinha2, hasValorRealizacaoLinha2, hasValorAquisicaoLinha2, hasDespesasLinha2, linkTitularLinha2));
        result.addAllFrom(this.validaValorAquisicao(hasTitularLinha2, hasValorRealizacaoLinha2, hasValorAquisicaoLinha2, hasDespesasLinha2, linkTitularLinha2));
        this.validateG335(result, hasValorRealizacaoLinha2, hasDespesasLinha2, "aAnexoG.qQuadro06.fanexoGq06C602e", "aAnexoG.qQuadro06.fanexoGq06C602c");
        return result;
    }

    public void validateG335(ValidationResult result, boolean hasValorRealizacao, boolean hasDespesas, String linkDespesas, String linkRealizacao) {
        if (hasDespesas && !hasValorRealizacao) {
            result.add(new DeclValidationMessage("G335", "aAnexoG.qQuadro06", new String[]{linkDespesas, linkRealizacao}));
        }
    }

    private ValidationResult validaValorRealizacao(boolean hasTitularLinha, boolean hasValorRealizacaoLinha, boolean hasValorAquisicaoLinha, boolean hasDespesasLinha, String linkTitularLinha) {
        ValidationResult result = new ValidationResult();
        if (!hasTitularLinha && hasValorRealizacaoLinha) {
            result.add(new DeclValidationMessage("G188", linkTitularLinha, new String[]{linkTitularLinha}));
        }
        return result;
    }

    private ValidationResult validaValorAquisicao(boolean hasTitularLinha, boolean hasValorRealizacaoLinha, boolean hasValorAquisicaoLinha, boolean hasDespesasLinha, String linkTitularLinha) {
        ValidationResult result = new ValidationResult();
        if (!hasTitularLinha && hasValorAquisicaoLinha) {
            result.add(new DeclValidationMessage("G190", linkTitularLinha, new String[]{linkTitularLinha}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

