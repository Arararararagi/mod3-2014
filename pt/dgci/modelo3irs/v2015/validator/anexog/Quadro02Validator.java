/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro02Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String G346 = "G346";

    public Quadro02Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexogModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        Quadro02 quadro02AnexoGModel = (Quadro02)anexogModel.getQuadro(Quadro02.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        if (!Modelo3IRSValidatorUtil.equals(quadro02AnexoGModel.getAnexoGq02C01(), rostoModel.getQuadro02().getQ02C02())) {
            result.add(new DeclValidationMessage("G346", "aAnexoG.qQuadro02.fanexoGq02C01", new String[]{"aAnexoG.qQuadro02.fanexoGq02C01", "aRosto.qQuadro02.fq02C02"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

