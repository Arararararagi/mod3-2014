/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        long nif;
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        pt.dgci.modelo3irs.v2015.model.anexog.Quadro03 q03Model = (pt.dgci.modelo3irs.v2015.model.anexog.Quadro03)anexoGModel.getQuadro(pt.dgci.modelo3irs.v2015.model.anexog.Quadro03.class.getSimpleName());
        boolean hasNIF = !Modelo3IRSValidatorUtil.isEmptyOrZero(q03Model.getAnexoGq03C02());
        long l = nif = hasNIF ? q03Model.getAnexoGq03C02() : 0;
        if (!this.mimicsSujeitoPassivoA(rostoModel, hasNIF, nif)) {
            result.add(new DeclValidationMessage("G002", "aAnexoG.qQuadro03.fanexoGq03C02", new String[]{"aAnexoG.qQuadro03.fanexoGq03C02", "aRosto.qQuadro03.fq03C03"}));
        }
        hasNIF = !Modelo3IRSValidatorUtil.isEmptyOrZero(q03Model.getAnexoGq03C03());
        long l2 = nif = hasNIF ? q03Model.getAnexoGq03C03() : 0;
        if (!this.mimicsSujeitoPassivoB(rostoModel, hasNIF, nif)) {
            result.add(new DeclValidationMessage("G003", "aAnexoG.qQuadro03.fanexoGq03C03", new String[]{"aAnexoG.qQuadro03.fanexoGq03C03", "aRosto.qQuadro03.fq03C04"}));
        }
        return result;
    }

    private boolean mimicsSujeitoPassivoA(RostoModel rostoModel, boolean hasNif, long nif) {
        boolean hasNifA = rostoModel.getQuadro03().getQ03C03() != null && rostoModel.getQuadro03().getQ03C03() > 0;
        return !hasNifA && !hasNif || hasNifA && hasNif && rostoModel.getQuadro03().getQ03C03() == nif;
    }

    private boolean mimicsSujeitoPassivoB(RostoModel rostoModel, boolean hasNif, long nif) {
        boolean hasNifB = rostoModel.getQuadro03().getQ03C04() != null && rostoModel.getQuadro03().getQ03C04() > 0;
        return !hasNifB && !hasNif || hasNifB && hasNif && rostoModel.getQuadro03().getQ03C04() == nif;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

