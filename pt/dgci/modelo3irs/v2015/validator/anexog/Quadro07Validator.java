/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        pt.dgci.modelo3irs.v2015.model.anexog.Quadro07 quadro07AnexoGModel = anexoGModel.getQuadro07();
        String fTitularLinha1 = quadro07AnexoGModel.getAnexoGq07C701b() != null ? quadro07AnexoGModel.getAnexoGq07C701b() : "";
        Long fValorRealizacaoLinha1 = quadro07AnexoGModel.getAnexoGq07C701c() != null ? quadro07AnexoGModel.getAnexoGq07C701c() : 0;
        Long fValorAquisicaoLinha1 = quadro07AnexoGModel.getAnexoGq07C701d() != null ? quadro07AnexoGModel.getAnexoGq07C701d() : 0;
        String fTitularLinha2 = quadro07AnexoGModel.getAnexoGq07C702b() != null ? quadro07AnexoGModel.getAnexoGq07C702b() : "";
        Long fValorRealizacaoLinha2 = quadro07AnexoGModel.getAnexoGq07C702c() != null ? quadro07AnexoGModel.getAnexoGq07C702c() : 0;
        Long fValorAquisicaoLinha2 = quadro07AnexoGModel.getAnexoGq07C702d() != null ? quadro07AnexoGModel.getAnexoGq07C702d() : 0;
        String linkTitularLinha1 = "aAnexoG.qQuadro07.fanexoGq07C701b";
        String linkValorRealizacaoLinha1 = "aAnexoG.qQuadro07.fanexoGq07C701c";
        String linkValorAquisicaoLinha1 = "aAnexoG.qQuadro07.fanexoGq07C701d";
        String linkTitularLinha2 = "aAnexoG.qQuadro07.fanexoGq07C702b";
        String linkValorRealizacaoLinha2 = "aAnexoG.qQuadro07.fanexoGq07C702c";
        String linkValorAquisicaoLinha2 = "aAnexoG.qQuadro07.fanexoGq07C702d";
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro07AnexoGModel.getAnexoGq07C701b());
        boolean hasValorRealizacaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C701c());
        boolean hasValorAquisicaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C701d());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro07AnexoGModel.getAnexoGq07C702b());
        boolean hasValorRealizacaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C702c());
        boolean hasValorAquisicaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C702d());
        this.validateG193(result, fTitularLinha1, linkTitularLinha1);
        this.validateG194(result, rostoModel, fTitularLinha1, linkTitularLinha1);
        result.addAllFrom(this.validaTitular_ValorRealizacao(hasValorRealizacaoLinha1, hasTitularLinha1, linkValorRealizacaoLinha1));
        this.validateG193(result, fTitularLinha2, linkTitularLinha2);
        this.validateG194(result, rostoModel, fTitularLinha2, linkTitularLinha2);
        result.addAllFrom(this.validaTitular_ValorRealizacao(hasValorRealizacaoLinha2, hasTitularLinha2, linkValorRealizacaoLinha2));
        result.addAllFrom(this.validaSomaValorRealizacao(quadro07AnexoGModel, hasValorRealizacaoLinha1, hasValorRealizacaoLinha2, fValorRealizacaoLinha1, fValorRealizacaoLinha2, linkValorRealizacaoLinha1, linkValorRealizacaoLinha2));
        result.addAllFrom(this.validaSomaValorAquisicao(quadro07AnexoGModel, hasValorAquisicaoLinha1, hasValorAquisicaoLinha2, fValorAquisicaoLinha1, fValorAquisicaoLinha2, linkValorAquisicaoLinha1, linkValorAquisicaoLinha2));
        return result;
    }

    protected void validateG193(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G193", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateG194(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G194", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    private ValidationResult validaTitular_ValorRealizacao(boolean hasValorRealizacao, boolean hasTitular, String linkValorRealizacao) {
        ValidationResult result = new ValidationResult();
        if (!hasValorRealizacao && hasTitular) {
            result.add(new DeclValidationMessage("G195", linkValorRealizacao, new String[]{linkValorRealizacao}));
        }
        return result;
    }

    private ValidationResult validateG196(boolean hasValorAquisicao, boolean hasTitular, String linkValorAquisicao) {
        ValidationResult result = new ValidationResult();
        if (!hasValorAquisicao && hasTitular) {
            result.add(new DeclValidationMessage("G196", linkValorAquisicao, new String[]{linkValorAquisicao}));
        }
        return result;
    }

    private ValidationResult validaSomaValorRealizacao(pt.dgci.modelo3irs.v2015.model.anexog.Quadro07 quadro07AnexoGModel, boolean hasValorRealizacaoLinha1, boolean hasValorRealizacaoLinha2, Long fValorRealizacaoLinha1, Long fValorRealizacaoLinha2, String linkValorRealizacaoLinha1, String linkValorRealizacaoLinha2) {
        ValidationResult result = new ValidationResult();
        long soma = 0;
        long somaControlo = 0;
        if (hasValorRealizacaoLinha1) {
            soma = fValorRealizacaoLinha1;
        }
        if (hasValorRealizacaoLinha2) {
            soma+=fValorRealizacaoLinha2.longValue();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C1c())) {
            somaControlo = quadro07AnexoGModel.getAnexoGq07C1c();
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("YG007", "aAnexoG.qQuadro07.fanexoGq07C1c", new String[]{"aAnexoG.qQuadro07.fanexoGq07C1c", linkValorRealizacaoLinha1, linkValorRealizacaoLinha2}));
        }
        return result;
    }

    private ValidationResult validaSomaValorAquisicao(pt.dgci.modelo3irs.v2015.model.anexog.Quadro07 quadro07AnexoGModel, boolean hasValorAquisicaoLinha1, boolean hasValorAquisicaoLinha2, Long fValorAquisicaoLinha1, Long fValorAquisicaoLinha2, String linkValorAquisicaoLinha1, String linkValorAquisicaoLinha2) {
        ValidationResult result = new ValidationResult();
        long soma = 0;
        long somaControlo = 0;
        if (hasValorAquisicaoLinha1) {
            soma = fValorAquisicaoLinha1;
        }
        if (hasValorAquisicaoLinha2) {
            soma+=fValorAquisicaoLinha2.longValue();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C1d())) {
            somaControlo = quadro07AnexoGModel.getAnexoGq07C1d();
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("YG008", "aAnexoG.qQuadro07.fanexoGq07C1d", new String[]{"aAnexoG.qQuadro07.fanexoGq07C1d", linkValorAquisicaoLinha1, linkValorAquisicaoLinha2}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        pt.dgci.modelo3irs.v2015.model.anexog.Quadro07 quadro07AnexoGModel = anexoGModel.getQuadro07();
        String linkTitularLinha1 = "aAnexoG.qQuadro07.fanexoGq07C701b";
        String linkTitularLinha2 = "aAnexoG.qQuadro07.fanexoGq07C702b";
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro07AnexoGModel.getAnexoGq07C701b());
        boolean hasValorRealizacaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C701c());
        boolean hasValorAquisicaoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C701d());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro07AnexoGModel.getAnexoGq07C702b());
        boolean hasValorRealizacaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C702c());
        boolean hasValorAquisicaoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07AnexoGModel.getAnexoGq07C702d());
        result.addAllFrom(this.validaValorRealizacao(hasTitularLinha1, hasValorRealizacaoLinha1, linkTitularLinha1));
        result.addAllFrom(this.validaValorAquisicao(hasTitularLinha1, hasValorAquisicaoLinha1, linkTitularLinha1));
        result.addAllFrom(this.validaValorRealizacao(hasTitularLinha2, hasValorRealizacaoLinha2, linkTitularLinha2));
        result.addAllFrom(this.validaValorAquisicao(hasTitularLinha2, hasValorAquisicaoLinha2, linkTitularLinha2));
        return result;
    }

    private ValidationResult validaValorRealizacao(boolean hasTitularLinha, boolean hasValorRealizacaoLinha, String linkTitularLinha) {
        ValidationResult result = new ValidationResult();
        if (!hasTitularLinha && hasValorRealizacaoLinha) {
            result.add(new DeclValidationMessage("G197", linkTitularLinha, new String[]{linkTitularLinha}));
        }
        return result;
    }

    private ValidationResult validaValorAquisicao(boolean hasTitularLinha, boolean hasValorAquisicaoLinha, String linkTitularLinha) {
        ValidationResult result = new ValidationResult();
        if (!hasTitularLinha && hasValorAquisicaoLinha) {
            result.add(new DeclValidationMessage("G199", linkTitularLinha, new String[]{linkTitularLinha}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        pt.dgci.modelo3irs.v2015.model.anexog.Quadro07 quadro07AnexoGModel = anexoGModel.getQuadro07();
        boolean hasValorAquisicaoLinha1 = quadro07AnexoGModel.getAnexoGq07C701d() != null;
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro07AnexoGModel.getAnexoGq07C701b());
        boolean hasValorAquisicaoLinha2 = quadro07AnexoGModel.getAnexoGq07C702d() != null;
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro07AnexoGModel.getAnexoGq07C702b());
        String linkValorAquisicaoLinha1 = "aAnexoG.qQuadro07.fanexoGq07C701d";
        String linkValorAquisicaoLinha2 = "aAnexoG.qQuadro07.fanexoGq07C702d";
        result.addAllFrom(this.validateG196(hasValorAquisicaoLinha1, hasTitularLinha1, linkValorAquisicaoLinha1));
        result.addAllFrom(this.validateG196(hasValorAquisicaoLinha2, hasTitularLinha2, linkValorAquisicaoLinha2));
        return result;
    }
}

