/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.Month;
import pt.opensoft.util.StringUtil;

public abstract class Quadro08Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final int LINE_INDEX_PAD = 1;

    public Quadro08Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        long anexoGq08C2Value;
        long anexoGq08C1Value;
        long anexoGq08C3Value;
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro08 quadro08AnexoGModel = anexoGModel.getQuadro08();
        if (quadro08AnexoGModel.getAnexoGq08T1().size() > 99) {
            result.add(new DeclValidationMessage("G202", "aAnexoG.qQuadro08.tanexoGq08T1", new String[]{"aAnexoG.qQuadro08.tanexoGq08T1"}));
        }
        if (quadro08AnexoGModel.getAnexoGq08T2().size() > 99) {
            result.add(new DeclValidationMessage("G305", "aAnexoG.qQuadro08.tanexoGq08T2", new String[]{"aAnexoG.qQuadro08.tanexoGq08T2"}));
        }
        this.validateG204(result, anexoGModel);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        boolean hasAnoExercicio = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio);
        long somaControloValorRealizacao = 0;
        long somaControloValorAquisicao = 0;
        long somaControloValorDespesas = 0;
        HashMap<String, AnexoGq08T1_Linha> processedLines = new HashMap<String, AnexoGq08T1_Linha>(quadro08AnexoGModel.getAnexoGq08T1().size());
        HashMap<Long, Long> numeroDaLinhaMap = new HashMap<Long, Long>(quadro08AnexoGModel.getAnexoGq08T1().size());
        for (int linha = 0; linha < quadro08AnexoGModel.getAnexoGq08T1().size(); ++linha) {
            AnexoGq08T1_Linha anexoGq08T1LinhaJ;
            AnexoGq08T1_Linha anexoGq08T1Linha = quadro08AnexoGModel.getAnexoGq08T1().get(linha);
            String linkCurrentLine = AnexoGq08T1_Linha.getLink(linha + 1);
            String linkNLinha = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.NLINHA.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.TITULAR.getIndex();
            String linkAnoRealizacao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.ANOREALIZACAO.getIndex();
            String linkMesRealizacao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.MESREALIZACAO.getIndex();
            String linkAnoAquisicao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.ANOAQUISICAO.getIndex();
            String linkMesAquisicao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.MESAQUISICAO.getIndex();
            String titular = anexoGq08T1Linha.getTitular();
            Long anoRealizacao = anexoGq08T1Linha.getAnoRealizacao();
            Long mesRealizacao = anexoGq08T1Linha.getMesRealizacao();
            Long valorRealizacao = anexoGq08T1Linha.getValorRealizacao();
            Long anoAquisicao = anexoGq08T1Linha.getAnoAquisicao();
            Long mesAquisicao = anexoGq08T1Linha.getMesAquisicao();
            Long valorAquisicao = anexoGq08T1Linha.getValorAquisicao();
            Long despesas = anexoGq08T1Linha.getDespesas();
            boolean hasAnoRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoRealizacao);
            boolean hasMesRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesRealizacao);
            boolean hasValorRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(valorRealizacao);
            boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao);
            boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao);
            boolean hasValorAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(valorAquisicao);
            boolean hasDespesas = !Modelo3IRSValidatorUtil.isEmptyOrZero(despesas);
            String key = titular;
            if (hasAnoRealizacao) {
                key = key + anoRealizacao;
            }
            if (hasMesRealizacao) {
                key = key + mesRealizacao;
            }
            if (hasValorRealizacao) {
                key = key + valorRealizacao;
            }
            if (hasAnoAquisicao) {
                key = key + anoAquisicao;
            }
            if (hasMesAquisicao) {
                key = key + mesAquisicao;
            }
            if (hasValorAquisicao) {
                key = key + valorAquisicao;
            }
            if (hasDespesas) {
                key = key + despesas;
            }
            if (!processedLines.containsKey(key)) {
                processedLines.put(key, anexoGq08T1Linha);
            }
            AnexoGq08T1_Linha anexoGq08T1_Linha = anexoGq08T1LinhaJ = linha + 1 < quadro08AnexoGModel.getAnexoGq08T1().size() ? quadro08AnexoGModel.getAnexoGq08T1().get(linha + 1) : null;
            if (!(anexoGq08T1LinhaJ == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getNLinha()) || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1LinhaJ.getNLinha()) || anexoGq08T1Linha.getNLinha() <= anexoGq08T1LinhaJ.getNLinha() || linha >= linha + 1)) {
                result.add(new DeclValidationMessage("G301", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (anexoGq08T1Linha.getNLinha() != null && (anexoGq08T1Linha.getNLinha() < 801 || anexoGq08T1Linha.getNLinha() > 899)) {
                result.add(new DeclValidationMessage("G302", linkCurrentLine, new String[]{linkNLinha}));
            }
            if (anexoGq08T1Linha.getNLinha() != null) {
                if (numeroDaLinhaMap.containsKey(anexoGq08T1Linha.getNLinha())) {
                    result.add(new DeclValidationMessage("G329", linkCurrentLine, new String[]{linkNLinha}));
                } else {
                    numeroDaLinhaMap.put(anexoGq08T1Linha.getNLinha(), anexoGq08T1Linha.getNLinha());
                }
            }
            this.validateG205(result, titular, linkTitular);
            this.validateG206(result, rostoModel, titular, linkTitular);
            this.validateG345(result, anexoGq08T1Linha, linha + 1);
            if (hasAnoRealizacao && hasAnoExercicio && anoExercicio != null && !anoRealizacao.equals(anoExercicio)) {
                result.add(new DeclValidationMessage("G210", linkAnoRealizacao, new String[]{linkAnoRealizacao, "aRosto.qQuadro02.fq02C02"}));
            }
            if (hasMesRealizacao && !Month.isMonth(String.valueOf(mesRealizacao))) {
                result.add(new DeclValidationMessage("G212", linkMesRealizacao, new String[]{linkMesRealizacao}));
            }
            if (hasAnoAquisicao && !Modelo3IRSValidatorUtil.isValidYear(anoAquisicao)) {
                result.add(new DeclValidationMessage("G215", linkAnoAquisicao, new String[]{linkAnoAquisicao}));
            }
            if (hasAnoAquisicao && hasAnoRealizacao && anoAquisicao > anoRealizacao) {
                result.add(new DeclValidationMessage("G216", linkAnoAquisicao, new String[]{linkAnoAquisicao, linkAnoRealizacao}));
            }
            if (hasMesAquisicao && !Month.isMonth(String.valueOf(mesAquisicao))) {
                result.add(new DeclValidationMessage("G218", linkMesAquisicao, new String[]{linkMesAquisicao}));
            }
            if ((hasAnoAquisicao ? anoAquisicao : 0) > (hasAnoRealizacao ? anoRealizacao : 0) || (hasAnoAquisicao ? anoAquisicao : 0) == (hasAnoRealizacao ? anoRealizacao : 0) && (hasMesAquisicao ? mesAquisicao : 0) > (hasMesRealizacao ? mesRealizacao : 0)) {
                result.add(new DeclValidationMessage("G219", linkMesAquisicao, new String[]{linkAnoAquisicao, linkMesAquisicao, linkAnoRealizacao, linkMesRealizacao}));
            }
            if (hasValorRealizacao) {
                somaControloValorRealizacao+=valorRealizacao.longValue();
            }
            if (hasValorAquisicao) {
                somaControloValorAquisicao+=valorAquisicao.longValue();
            }
            if (!hasDespesas) continue;
            somaControloValorDespesas+=despesas.longValue();
        }
        this.validateG363(result, anexoGModel);
        this.validateG367(result, anexoGModel);
        long l = anexoGq08C1Value = quadro08AnexoGModel.getAnexoGq08C1() != null ? quadro08AnexoGModel.getAnexoGq08C1() : 0;
        if (anexoGq08C1Value != somaControloValorRealizacao) {
            result.add(new DeclValidationMessage("YG009", "aAnexoG.qQuadro08.fanexoGq08C1", new String[]{"aAnexoG.qQuadro08.fanexoGq08C1"}));
        }
        long l2 = anexoGq08C2Value = quadro08AnexoGModel.getAnexoGq08C2() != null ? quadro08AnexoGModel.getAnexoGq08C2() : 0;
        if (anexoGq08C2Value != somaControloValorAquisicao) {
            result.add(new DeclValidationMessage("YG010", "aAnexoG.qQuadro08.fanexoGq08C2", new String[]{"aAnexoG.qQuadro08.fanexoGq08C2"}));
        }
        long l3 = anexoGq08C3Value = quadro08AnexoGModel.getAnexoGq08C3() != null ? quadro08AnexoGModel.getAnexoGq08C3() : 0;
        if (anexoGq08C3Value != somaControloValorDespesas) {
            result.add(new DeclValidationMessage("YG011", "aAnexoG.qQuadro08.fanexoGq08C3", new String[]{"aAnexoG.qQuadro08.fanexoGq08C3"}));
        }
        List<Long> quadro08NlinhaList = quadro08AnexoGModel.getNlinhaList();
        ArrayList<Long> processedQuadro08NlinhaList = new ArrayList<Long>();
        for (int linha2 = 0; linha2 < quadro08AnexoGModel.getAnexoGq08T2().size(); ++linha2) {
            boolean hasNipc;
            AnexoGq08T2_Linha anexoGq08T2Linha = quadro08AnexoGModel.getAnexoGq08T2().get(linha2);
            String linkCurrentLine = "aAnexoG.qQuadro08.tanexoGq08T2.l" + (linha2 + 1);
            String linkCampoQ8 = linkCurrentLine + ".c" + AnexoGq08T2_LinhaBase.Property.CAMPOQ8.getIndex();
            String linkNipc = linkCurrentLine + ".c" + AnexoGq08T2_LinhaBase.Property.NIPC.getIndex();
            Long campoQ8 = anexoGq08T2Linha.getCampoQ8();
            boolean hasCampoQ8 = campoQ8 != null;
            Long nipc = anexoGq08T2Linha.getNipc();
            boolean bl = hasNipc = nipc != null;
            if (hasCampoQ8) {
                if (processedQuadro08NlinhaList.contains(campoQ8)) {
                    result.add(new DeclValidationMessage("G306", linkCurrentLine, new String[]{AnexoGq08T2_Linha.getLink(linha2, AnexoGq08T2_LinhaBase.Property.CAMPOQ8)}));
                } else {
                    processedQuadro08NlinhaList.add(campoQ8);
                }
            }
            if (!(hasCampoQ8 || hasNipc)) {
                result.add(new DeclValidationMessage("G307", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (hasCampoQ8 && (campoQ8 < 801 || campoQ8 > 899)) {
                result.add(new DeclValidationMessage("G308", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (hasCampoQ8 && !hasNipc || !hasCampoQ8 && hasNipc) {
                result.add(new DeclValidationMessage("G309", linkCurrentLine, new String[]{linkCampoQ8, linkNipc}));
            }
            this.validateG310(result, nipc, linkNipc);
            if (hasNipc && !Modelo3IRSValidatorUtil.isNIFValid(nipc)) {
                result.add(new DeclValidationMessage("G311", linkNipc, new String[]{linkNipc}));
            }
            if (hasCampoQ8 && !quadro08NlinhaList.contains(campoQ8) && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() > 2009) {
                result.add(new DeclValidationMessage("G312", linkCampoQ8, new String[]{linkCampoQ8, "aAnexoG.qQuadro08.tanexoGq08T1"}));
            }
            if (!hasCampoQ8 || !quadro08AnexoGModel.getAnexoGq08T1().isEmpty() || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() <= 2009) continue;
            result.add(new DeclValidationMessage("G314", linkCampoQ8, new String[]{"aAnexoG.qQuadro08.tanexoGq08T2", "aAnexoG.qQuadro08.tanexoGq08T1"}));
        }
        if (!(quadro08AnexoGModel.getAnexoGq08T2().isEmpty() || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() >= 2010)) {
            result.add(new DeclValidationMessage("G313", "aAnexoG.qQuadro08.tanexoGq08T2", new String[]{"aRosto.qQuadro02.fq02C02", "aAnexoG.qQuadro08.tanexoGq08T2"}));
        }
        return result;
    }

    protected void validateG204(ValidationResult result, AnexoGModel anexoGModel) {
        if (!(anexoGModel == null || anexoGModel.getQuadro08().getAnexoGq08T1() == null || anexoGModel.getQuadro08().getAnexoGq08T1().isEmpty())) {
            for (int idxLinha = 0; idxLinha < anexoGModel.getQuadro08().getAnexoGq08T1().size(); ++idxLinha) {
                AnexoGq08T1_Linha anexoGq08T1Linha = anexoGModel.getQuadro08().getAnexoGq08T1().get(idxLinha);
                if (anexoGq08T1Linha == null || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getNLinha()) || !StringUtil.isEmpty(anexoGq08T1Linha.getTitular()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getNIF()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getCodEncargos()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getDespesas()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getValorAquisicao()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getMesAquisicao()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getAnoRealizacao()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getMesRealizacao()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getValorRealizacao()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getAnoAquisicao())) continue;
                result.add(new DeclValidationMessage("G204", AnexoGq08T1_Linha.getLink(idxLinha + 1), new String[]{AnexoGq08T1_Linha.getLink(idxLinha + 1)}));
            }
        }
    }

    protected void validateG310(ValidationResult result, Long nipc, String linkNipc) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(nipc) || Modelo3IRSValidatorUtil.startsWith(nipc, "5"))) {
            result.add(new DeclValidationMessage("G310", linkNipc, new String[]{linkNipc}));
        }
    }

    protected void validateG205(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G205", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateG206(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G206", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    protected void validateG363(ValidationResult result, AnexoGModel anexoGModel) {
        if (!(anexoGModel == null || anexoGModel.getQuadro08().getAnexoGq08T1().isEmpty())) {
            String[] validNipc = new String[]{"5", "71", "98"};
            long[] nifEntidadeEmitente = new long[]{600006441, 600015688};
            for (int linha = 0; linha < anexoGModel.getQuadro08().getAnexoGq08T1().size(); ++linha) {
                AnexoGq08T1_Linha anexoGq08T1Linha = anexoGModel.getQuadro08().getAnexoGq08T1().get(linha);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getNIF()) || !Modelo3IRSValidatorUtil.isNIFValid(anexoGq08T1Linha.getNIF()) || Modelo3IRSValidatorUtil.startsWith(anexoGq08T1Linha.getNIF(), validNipc) || Modelo3IRSValidatorUtil.in(anexoGq08T1Linha.getNIF(), nifEntidadeEmitente)) continue;
                result.add(new DeclValidationMessage("G363", AnexoGq08T1_Linha.getLink(linha + 1, AnexoGq08T1_LinhaBase.Property.NIF), new String[]{AnexoGq08T1_Linha.getLink(linha + 1, AnexoGq08T1_LinhaBase.Property.NIF)}));
            }
        }
    }

    protected void validateG367(ValidationResult result, AnexoGModel anexoGModel) {
        if (!(anexoGModel == null || anexoGModel.getQuadro08().getAnexoGq08T1().isEmpty())) {
            for (int linha = 0; linha < anexoGModel.getQuadro08().getAnexoGq08T1().size(); ++linha) {
                AnexoGq08T1_Linha anexoGq08T1Linha = anexoGModel.getQuadro08().getAnexoGq08T1().get(linha);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getNIF()) || Modelo3IRSValidatorUtil.isNIFValid(anexoGq08T1Linha.getNIF())) continue;
                result.add(new DeclValidationMessage("G367", AnexoGq08T1_Linha.getLink(linha + 1, AnexoGq08T1_LinhaBase.Property.NIF), new String[]{AnexoGq08T1_Linha.getLink(linha + 1, AnexoGq08T1_LinhaBase.Property.NIF)}));
            }
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        Quadro08 quadro08AnexoGModel = anexoGModel.getQuadro08();
        for (int linha = 0; linha < quadro08AnexoGModel.getAnexoGq08T1().size(); ++linha) {
            AnexoGq08T1_Linha anexoGq08T1Linha = quadro08AnexoGModel.getAnexoGq08T1().get(linha);
            this.validateG315(result, anexoGq08T1Linha, linha + 1);
            this.validateG303(result, anexoGq08T1Linha, linha + 1);
        }
        return result;
    }

    protected void validateG315(ValidationResult result, AnexoGq08T1_Linha anexoGq08T1Linha, int linha) {
        Long nLinha = anexoGq08T1Linha.getNLinha();
        String titular = anexoGq08T1Linha.getTitular();
        Long anoRealizacao = anexoGq08T1Linha.getAnoRealizacao();
        Long mesRealizacao = anexoGq08T1Linha.getMesRealizacao();
        Long valorRealizacao = anexoGq08T1Linha.getValorRealizacao();
        Long anoAquisicao = anexoGq08T1Linha.getAnoAquisicao();
        Long mesAquisicao = anexoGq08T1Linha.getMesAquisicao();
        Long codigos = anexoGq08T1Linha.getCodEncargos();
        Long valorAquisicao = anexoGq08T1Linha.getValorAquisicao();
        Long despesas = anexoGq08T1Linha.getDespesas();
        Long entidadeEmitente = anexoGq08T1Linha.getNIF();
        boolean hasNLinha = !Modelo3IRSValidatorUtil.isEmptyOrZero(nLinha);
        boolean hasTitular = !StringUtil.isEmpty(titular);
        boolean hasAnoRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoRealizacao);
        boolean hasMesRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesRealizacao);
        boolean hasValorRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(valorRealizacao);
        boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao);
        boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao);
        boolean hasValorAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(valorAquisicao);
        boolean hasCodigos = !Modelo3IRSValidatorUtil.isEmptyOrZero(codigos);
        boolean hasDespesas = !Modelo3IRSValidatorUtil.isEmptyOrZero(despesas);
        boolean hasEntidadeEmitente = !Modelo3IRSValidatorUtil.isEmptyOrZero(entidadeEmitente);
        String linkCurrentLine = "aAnexoG.qQuadro08.tanexoGq08T1.l" + linha;
        String linkNLinha = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.NLINHA.getIndex();
        if (!hasNLinha && (hasTitular || hasAnoRealizacao || hasMesRealizacao || hasValorRealizacao || hasAnoAquisicao || hasMesAquisicao || hasValorAquisicao || hasDespesas || hasCodigos || hasEntidadeEmitente)) {
            result.add(new DeclValidationMessage("G315", linkCurrentLine, new String[]{linkNLinha}));
        }
    }

    public void validateG345(ValidationResult result, AnexoGq08T1_Linha current, int numLinha) {
        if (!(current == null || current.getCodEncargos() == null || Modelo3IRSValidatorUtil.in(current.getCodEncargos(), new long[]{1, 2, 3, 4}))) {
            String codigosLink = AnexoGq08T1_Linha.getLink(numLinha, AnexoGq08T1_LinhaBase.Property.CODENCARGOS);
            result.add(new DeclValidationMessage("G345", codigosLink, new String[]{codigosLink}));
        }
    }

    protected void validateG303(ValidationResult result, AnexoGq08T1_Linha anexoGq08T1Linha, int linha) {
        boolean hasEntidadeEmitente;
        String linkCurrentLine = "aAnexoG.qQuadro08.tanexoGq08T1.l" + linha;
        String linkNLinha = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.NLINHA.getIndex();
        String linkTitular = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.TITULAR.getIndex();
        String linkAnoRealizacao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.ANOREALIZACAO.getIndex();
        String linkMesRealizacao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.MESREALIZACAO.getIndex();
        String linkValorRealizacao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.VALORREALIZACAO.getIndex();
        String linkAnoAquisicao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.ANOAQUISICAO.getIndex();
        String linkMesAquisicao = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.MESAQUISICAO.getIndex();
        String linkCodigos = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.CODENCARGOS.getIndex();
        String linkEntidadeEmitente = linkCurrentLine + ".c" + AnexoGq08T1_LinhaBase.Property.NIF.getIndex();
        boolean hasNLinha = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getNLinha());
        boolean hasTitular = !StringUtil.isEmpty(anexoGq08T1Linha.getTitular());
        boolean hasAnoRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getAnoRealizacao());
        boolean hasMesRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getMesRealizacao());
        boolean hasValorRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getValorRealizacao());
        boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getAnoAquisicao());
        boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getMesAquisicao());
        boolean hasCodigos = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getCodEncargos());
        boolean bl = hasEntidadeEmitente = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq08T1Linha.getNIF());
        if (!(!hasNLinha || hasTitular && hasAnoRealizacao && hasMesRealizacao && hasAnoAquisicao && hasMesAquisicao && hasCodigos && hasEntidadeEmitente && hasValorRealizacao)) {
            result.add(new DeclValidationMessage("G303", linkCurrentLine, new String[]{linkNLinha, linkTitular, linkEntidadeEmitente, linkCodigos, linkAnoRealizacao, linkMesRealizacao, linkValorRealizacao, linkAnoAquisicao, linkMesAquisicao}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

