/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Freguesia;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.Month;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final int PADDING_INDICE_LINHA = 1;
    private Long anoExercicio;

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        pt.dgci.modelo3irs.v2015.model.anexog.Quadro04 quadro04AnexoGModel = anexoGModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        this.anoExercicio = rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0;
        result.addAllFrom(this.validaQ4T1(rostoModel, quadro04AnexoGModel, model));
        this.validateG010(result, anexoGModel);
        return result;
    }

    protected ValidationResult validaQ4T1(RostoModel rostoModel, pt.dgci.modelo3irs.v2015.model.anexog.Quadro04 quadro04AnexoGModel, DeclaracaoModel model) {
        long anexoGq04C2Value;
        boolean hasCampo6_7;
        long anexoGq04C1Value;
        String campo6_7;
        long anexoGq04C3Value;
        ValidationResult result = new ValidationResult();
        String linkCampo6_7SIM = "aAnexoG.qQuadro04.fanexoGq04B1OPSim";
        if (quadro04AnexoGModel.getAnexoGq04T1().size() > 93) {
            result.add(new DeclValidationMessage("G004", "aAnexoG.qQuadro04.tanexoGq04T1", new String[]{"aAnexoG.qQuadro04.tanexoGq04T1"}));
        }
        if (quadro04AnexoGModel.getAnexoGq04AT1().size() > 20) {
            result.add(new DeclValidationMessage("G035", "aAnexoG.qQuadro04.tanexoGq04AT1", new String[]{"aAnexoG.qQuadro04.tanexoGq04AT1"}));
        }
        if (quadro04AnexoGModel.getAnexoGq04T2().size() > 20) {
            result.add(new DeclValidationMessage("G042", "aAnexoG.qQuadro04.tanexoGq04T2", new String[]{"aAnexoG.qQuadro04.tanexoGq04T2"}));
        }
        boolean bl = hasCampo6_7 = !StringUtil.isEmpty(campo6_7 = quadro04AnexoGModel.getAnexoGq04B1OP());
        if (hasCampo6_7 && !StringUtil.in(campo6_7, new String[]{" ", "S", "N"})) {
            result.add(new DeclValidationMessage("G038", linkCampo6_7SIM, new String[]{linkCampo6_7SIM}));
        }
        if (!(quadro04AnexoGModel.getAnexoGq04AT1().isEmpty() || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04AnexoGModel.getAnexoGq04AT1().get(0).getCamposQuadro4()) || hasCampo6_7 || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() <= 2008)) {
            result.add(new DeclValidationMessage("G041", "aAnexoG.qQuadro04", new String[]{"aAnexoG.qQuadro04.fanexoGq04B1OPSim"}));
        }
        if ((!quadro04AnexoGModel.AnexoGq04AT1isEmpty() || hasCampo6_7) && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2009) {
            result.add(new DeclValidationMessage("G290", "aAnexoG.qQuadro04", new String[]{"aAnexoG.qQuadro04.tanexoGq04AT1"}));
        }
        Long ultimoNumLinha = new Long(0);
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        Hashtable<Tuple, Long> processedTuplesHT = new Hashtable<Tuple, Long>();
        ArrayList<Long> numLinhaList = new ArrayList<Long>();
        long somaQuota = 0;
        long somaControloValorRealizacao = 0;
        long somaControloValorAquisicao = 0;
        long somaControloValorDespesas = 0;
        for (int linha = 0; linha < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha) {
            boolean tipo_Omisso;
            boolean hasFrac;
            AnexoGq04T1_Linha anexoGq04T1Linha = quadro04AnexoGModel.getAnexoGq04T1().get(linha);
            String linkCurrentLine = "aAnexoG.qQuadro04.tanexoGq04T1.l" + (linha + 1);
            String linkNumLinha = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.NLINHA.getIndex();
            String linkTitular = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.TITULAR.getIndex();
            String linkAnoRealizacao = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.ANOREALIZACAO.getIndex();
            String linkMesRealizacao = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.MESREALIZACAO.getIndex();
            String linkValorRealizacao = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.VALORREALIZACAO.getIndex();
            String linkAnoAquisicao = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.ANOAQUISICAO.getIndex();
            String linkMesAquisicao = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.MESAQUISICAO.getIndex();
            String linkFreguesia = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.FREGUESIA.getIndex();
            String linkTipoPredio = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.TIPOPREDIO.getIndex();
            String linkArtigo = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.ARTIGO.getIndex();
            String linkFraccao = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.FRACCAO.getIndex();
            String linkQuotaParte = linkCurrentLine + ".c" + AnexoGq04T1_LinhaBase.Property.QUOTAPARTE.getIndex();
            Long numLinha = anexoGq04T1Linha.getNLinha();
            String titular = anexoGq04T1Linha.getTitular();
            Long anoRealizacao = anexoGq04T1Linha.getAnoRealizacao();
            Long mesRealizacao = anexoGq04T1Linha.getMesRealizacao();
            Long valorRealizacao = anexoGq04T1Linha.getValorRealizacao();
            Long anoAquisicao = anexoGq04T1Linha.getAnoAquisicao();
            Long mesAquisicao = anexoGq04T1Linha.getMesAquisicao();
            Long valorAquisicao = anexoGq04T1Linha.getValorAquisicao();
            Long despesas = anexoGq04T1Linha.getDespesas();
            String freguesia = anexoGq04T1Linha.getFreguesia();
            String tipoPredio = anexoGq04T1Linha.getTipoPredio();
            Long artigo = anexoGq04T1Linha.getArtigo();
            String fraccao = anexoGq04T1Linha.getFraccao();
            Long quotaParte = anexoGq04T1Linha.getQuotaParte();
            boolean hasNumLinha = !Modelo3IRSValidatorUtil.isEmptyOrZero(numLinha);
            boolean hasTitular = !StringUtil.isEmpty(titular);
            boolean hasAnoRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoRealizacao);
            boolean hasMesRealizacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesRealizacao);
            boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao);
            boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao);
            boolean hasFreguesia = !StringUtil.isEmpty(freguesia);
            boolean hasTipo = !StringUtil.isEmpty(tipoPredio);
            boolean hasArtigo = !Modelo3IRSValidatorUtil.isEmptyOrZero(artigo);
            boolean hasQuota = !Modelo3IRSValidatorUtil.isEmptyOrZero(quotaParte);
            boolean hasLinha = !Modelo3IRSValidatorUtil.isEmptyOrZero(numLinha);
            boolean bl2 = hasFrac = !StringUtil.isEmpty(fraccao);
            if (hasTitular && hasAnoRealizacao && hasMesRealizacao && hasAnoAquisicao && hasMesAquisicao && hasFreguesia && hasTipo && hasArtigo) {
                Tuple tuple = new Tuple(new Object[]{titular, anoRealizacao, mesRealizacao, anoAquisicao, mesAquisicao, freguesia, tipoPredio, artigo, fraccao});
                if (processedTuples.contains(tuple)) {
                    result.add(new DeclValidationMessage("G005", linkCurrentLine, new String[]{linkCurrentLine}));
                } else {
                    processedTuples.add(tuple);
                }
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(numLinha) && StringUtil.isEmpty(titular) && Modelo3IRSValidatorUtil.isEmptyOrZero(anoRealizacao) && mesRealizacao == null && Modelo3IRSValidatorUtil.isEmptyOrZero(valorRealizacao) && Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao) && mesAquisicao == null && Modelo3IRSValidatorUtil.isEmptyOrZero(valorAquisicao) && Modelo3IRSValidatorUtil.isEmptyOrZero(despesas) && StringUtil.isEmpty(freguesia) && StringUtil.isEmpty(tipoPredio) && Modelo3IRSValidatorUtil.isEmptyOrZero(artigo) && StringUtil.isEmpty(fraccao) && Modelo3IRSValidatorUtil.isEmptyOrZero(quotaParte)) {
                result.add(new DeclValidationMessage("G006", linkNumLinha, new String[]{linkNumLinha}));
            }
            if (hasNumLinha) {
                if (numLinha < ultimoNumLinha) {
                    result.add(new DeclValidationMessage("G007", linkNumLinha, new String[]{linkNumLinha}));
                }
                ultimoNumLinha = numLinha;
            }
            if (hasNumLinha && (numLinha < 401 || numLinha > 493)) {
                result.add(new DeclValidationMessage("G011", linkNumLinha, new String[]{linkNumLinha}));
            }
            if (hasNumLinha && numLinhaList.contains(numLinha)) {
                result.add(new DeclValidationMessage("G325", linkNumLinha, new String[]{linkNumLinha}));
            } else {
                numLinhaList.add(numLinha);
            }
            this.validateG013(result, linkTitular, titular);
            this.validateG014(result, rostoModel, titular, linkTitular);
            if (anoRealizacao != null && anoRealizacao == 0) {
                result.add(new DeclValidationMessage("G015", linkAnoRealizacao, new String[]{linkAnoRealizacao}, new String[]{"de Realiza\u00e7\u00e3o"}));
            }
            this.validateG016(result, hasAnoRealizacao, rostoModel, anoRealizacao, linkAnoRealizacao);
            if (mesRealizacao != null && (mesRealizacao < 1 || mesRealizacao > 12)) {
                result.add(new DeclValidationMessage("G017", linkMesRealizacao, new String[]{linkMesRealizacao}));
            }
            if (valorRealizacao != null && valorRealizacao == 0) {
                result.add(new DeclValidationMessage("G018", linkValorRealizacao, new String[]{linkValorRealizacao}));
            }
            if (hasAnoAquisicao && !Modelo3IRSValidatorUtil.isValidYear(anoAquisicao)) {
                result.add(new DeclValidationMessage("G019", linkAnoAquisicao, new String[]{linkAnoAquisicao}));
            }
            if (hasAnoAquisicao && anoAquisicao > (hasAnoRealizacao ? anoRealizacao : 0)) {
                result.add(new DeclValidationMessage("G020", linkAnoAquisicao, new String[]{linkAnoAquisicao, linkAnoRealizacao}));
            }
            if (mesAquisicao != null && (mesAquisicao < 1 || mesAquisicao > 12)) {
                result.add(new DeclValidationMessage("G021", linkMesAquisicao, new String[]{linkMesAquisicao}));
            }
            if ((hasAnoAquisicao ? anoAquisicao : 0) > (hasAnoRealizacao ? anoRealizacao : 0) || (hasAnoAquisicao ? anoAquisicao : 0) == (hasAnoRealizacao ? anoRealizacao : 0) && (hasMesAquisicao ? mesAquisicao : 0) > (hasMesRealizacao ? mesRealizacao : 0)) {
                result.add(new DeclValidationMessage("G022", linkAnoAquisicao, new String[]{linkAnoAquisicao, linkMesAquisicao, linkAnoRealizacao, linkMesRealizacao}));
            }
            if (hasFreguesia && !CatalogManager.getInstance().getCatalog("Cat_M3V2015_Freguesia").containsKey(freguesia)) {
                result.add(new DeclValidationMessage("G025", linkFreguesia, new String[]{linkFreguesia}));
            }
            if (hasTipo && !StringUtil.in(tipoPredio, new String[]{"U", "R", "O"})) {
                result.add(new DeclValidationMessage("G027", linkTipoPredio, new String[]{linkTipoPredio}));
            }
            if (hasTipo && StringUtil.in(tipoPredio, new String[]{"U", "R"}) && !hasArtigo) {
                result.add(new DeclValidationMessage("G028", linkTipoPredio, new String[]{linkTipoPredio, linkArtigo}));
            }
            boolean bl3 = tipo_Omisso = hasTipo && tipoPredio.equals("O");
            if (hasArtigo && tipo_Omisso && (hasLinha || hasFreguesia || hasTipo || hasFrac)) {
                result.add(new DeclValidationMessage("G029", linkNumLinha, new String[]{linkArtigo, linkTipoPredio}));
            }
            if (!StringUtil.isEmpty(fraccao) && !Pattern.matches("[a-zA-Z0-9 ]*", (CharSequence)fraccao) && (hasLinha || hasFreguesia || hasTipo || hasArtigo)) {
                result.add(new DeclValidationMessage("G031", linkFraccao, new String[]{linkFraccao}));
            }
            if (hasFrac && tipo_Omisso && (hasLinha || hasFreguesia || hasTipo || hasArtigo)) {
                result.add(new DeclValidationMessage("G032", linkNumLinha, new String[]{linkFraccao, linkTipoPredio}));
            }
            if (hasFreguesia && hasTipo && !tipo_Omisso && hasArtigo) {
                long tempQuota = 0;
                tempQuota = hasQuota ? quotaParte : 0;
                Object[] arrobject = new Object[4];
                arrobject[0] = freguesia;
                arrobject[1] = tipoPredio;
                arrobject[2] = artigo;
                arrobject[3] = fraccao != null ? fraccao : "";
                Tuple tuple = new Tuple(arrobject);
                if (processedTuplesHT.containsKey(tuple)) {
                    somaQuota = (Long)processedTuplesHT.get(tuple);
                    processedTuplesHT.remove(tuple);
                    processedTuplesHT.put(tuple, somaQuota+=tempQuota);
                } else {
                    processedTuplesHT.put(tuple, tempQuota);
                }
            }
            if (quotaParte != null && (quotaParte <= 0 || quotaParte > 10000)) {
                result.add(new DeclValidationMessage("G033", linkQuotaParte, new String[]{linkQuotaParte}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(valorRealizacao)) {
                somaControloValorRealizacao+=valorRealizacao.longValue();
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(valorAquisicao)) {
                somaControloValorAquisicao+=valorAquisicao.longValue();
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(despesas)) continue;
            somaControloValorDespesas+=despesas.longValue();
        }
        Collection somaQuotas = processedTuplesHT.values();
        for (Long value : somaQuotas) {
            if (value <= 10000) continue;
            result.add(new DeclValidationMessage("G034", "aAnexoG.qQuadro04", new String[]{"aAnexoG.qQuadro04"}));
        }
        long l = anexoGq04C1Value = quadro04AnexoGModel.getAnexoGq04C1() != null ? quadro04AnexoGModel.getAnexoGq04C1() : 0;
        if (anexoGq04C1Value != somaControloValorRealizacao) {
            result.add(new DeclValidationMessage("YG001", "aAnexoG.qQuadro04.fanexoGq04C1", new String[]{"aAnexoG.qQuadro04.fanexoGq04C1"}));
        }
        long l2 = anexoGq04C2Value = quadro04AnexoGModel.getAnexoGq04C2() != null ? quadro04AnexoGModel.getAnexoGq04C2() : 0;
        if (anexoGq04C2Value != somaControloValorAquisicao) {
            result.add(new DeclValidationMessage("YG002", "aAnexoG.qQuadro04.fanexoGq04C2", new String[]{"aAnexoG.qQuadro04.fanexoGq04C2"}));
        }
        long l3 = anexoGq04C3Value = quadro04AnexoGModel.getAnexoGq04C3() != null ? quadro04AnexoGModel.getAnexoGq04C3() : 0;
        if (anexoGq04C3Value != somaControloValorDespesas) {
            result.add(new DeclValidationMessage("YG003", "aAnexoG.qQuadro04.fanexoGq04C3", new String[]{"aAnexoG.qQuadro04.fanexoGq04C3"}));
        }
        ArrayList<Tuple> campoQ4Tuple = new ArrayList<Tuple>();
        for (int linha2 = 0; linha2 < quadro04AnexoGModel.getAnexoGq04AT1().size(); ++linha2) {
            AnexoGq04AT1_Linha anexoGq04AT1Linha = quadro04AnexoGModel.getAnexoGq04AT1().get(linha2);
            String linkCampoQ4 = AnexoGq04T1_Linha.getLink(linha2 + 1, AnexoGq04T1_LinhaBase.Property.NLINHA);
            Long campoQ4 = anexoGq04AT1Linha.getCamposQuadro4();
            boolean hasCampoQ4 = campoQ4 != null;
            Tuple tuple = new Tuple(new Object[]{campoQ4});
            if (hasCampoQ4 && campoQ4Tuple.contains(tuple)) {
                result.add(new DeclValidationMessage("G036", linkCampoQ4, new String[]{linkCampoQ4}));
            } else {
                campoQ4Tuple.add(tuple);
            }
            if (!hasCampoQ4) {
                result.add(new DeclValidationMessage("G037", linkCampoQ4, new String[]{linkCampoQ4}));
            }
            boolean quadro04T1containsField = false;
            for (int i = 0; i < quadro04AnexoGModel.getAnexoGq04T1().size(); ++i) {
                AnexoGq04T1_Linha line = quadro04AnexoGModel.getAnexoGq04T1().get(i);
                if (!hasCampoQ4 || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getNLinha()) || !line.getNLinha().equals(campoQ4)) continue;
                quadro04T1containsField = true;
            }
            if (!hasCampoQ4 || quadro04T1containsField || rostoModel.getQuadro02().getQ02C02() == null || rostoModel.getQuadro02().getQ02C02() <= 2008) continue;
            result.add(new DeclValidationMessage("G040", "aAnexoG.qQuadro04.tanexoGq04T1", new String[]{"aAnexoG.qQuadro04.tanexoGq04T1", linkCampoQ4}));
        }
        this.validateQuadro4B(result, quadro04AnexoGModel, model);
        return result;
    }

    protected void validateG016(ValidationResult result, boolean hasAnoRealizacao, RostoModel rostoModel, Long anoRealizacao, String linkAnoRealizacao) {
        if (hasAnoRealizacao && !Modelo3IRSValidatorUtil.equals(rostoModel.getQuadro02().getQ02C02(), (long)anoRealizacao)) {
            result.add(new DeclValidationMessage("G016", linkAnoRealizacao, new String[]{linkAnoRealizacao, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateG010(ValidationResult result, AnexoGModel anexoGModel) {
        if (!(anexoGModel == null || anexoGModel.getQuadro04().getAnexoGq04T1() == null || anexoGModel.getQuadro04().getAnexoGq04T1().isEmpty())) {
            for (int i = 0; i < anexoGModel.getQuadro04().getAnexoGq04T1().size(); ++i) {
                AnexoGq04T1_Linha linha = anexoGModel.getQuadro04().getAnexoGq04T1().get(i);
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || StringUtil.isEmpty(linha.getTitular()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesAquisicao()) && linha.getValorAquisicao() == null && StringUtil.isEmpty(linha.getFreguesia()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDespesas()) && StringUtil.isEmpty(linha.getTipoPredio()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) && StringUtil.isEmpty(linha.getFraccao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getQuotaParte())) continue;
                result.add(new DeclValidationMessage("G010", AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.NLINHA), new String[]{AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.NLINHA)}));
            }
        }
    }

    protected void validateQuadro4B(ValidationResult result, pt.dgci.modelo3irs.v2015.model.anexog.Quadro04 quadro04AnexoGModel, DeclaracaoModel model) {
        for (int linha = 0; linha < quadro04AnexoGModel.getAnexoGq04T2().size(); ++linha) {
            AnexoGq04T2_Linha anexoGq04T2Linha = quadro04AnexoGModel.getAnexoGq04T2().get(linha);
            String linkCurrentLine = AnexoGq04T2_Linha.getLink(linha + 1);
            String linkBens = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.MOVEISIMOVEISBENS.getIndex();
            String linkAnoAfectacao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.ANOAFECTACAO.getIndex();
            String linkMesAfectacao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.MESAFECTACAO.getIndex();
            String linkValorAfectacao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.VALORAFECTACAO.getIndex();
            String linkAnoAquisicao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.ANOAQUISICAO.getIndex();
            String linkMesAquisicao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.MESAQUISICAO.getIndex();
            String linkFreguesia = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.FREGUESIA.getIndex();
            String linkTipo = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.TIPOPREDIO.getIndex();
            String linkArtigo = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.ARTIGO.getIndex();
            String linkFraccao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.FRACCAO.getIndex();
            String linkQuotaParte = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.QUOTAPARTE.getIndex();
            Long anoAfectacao = anexoGq04T2Linha.getAnoAfectacao();
            Long mesAfectacao = anexoGq04T2Linha.getMesAfectacao();
            Long valorAfectacao = anexoGq04T2Linha.getValorAfectacao();
            Long anoAquisicao = anexoGq04T2Linha.getAnoAquisicao();
            Long mesAquisicao = anexoGq04T2Linha.getMesAquisicao();
            boolean hasAnoAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoAfectacao);
            boolean hasMesAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesAfectacao);
            boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoAquisicao);
            boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(mesAquisicao);
            this.validateG044(result, anexoGq04T2Linha, linkCurrentLine);
            this.validateG046(result, anexoGq04T2Linha, linkCurrentLine);
            if (hasAnoAfectacao && this.anoExercicio != null && anoAfectacao > this.anoExercicio) {
                result.add(new DeclValidationMessage("G047", linkAnoAfectacao, new String[]{linkAnoAfectacao, "aRosto.qQuadro02.fq02C02"}));
            }
            if (hasAnoAfectacao && !Modelo3IRSValidatorUtil.isValidYear(anoAfectacao)) {
                result.add(new DeclValidationMessage("G048", linkAnoAfectacao, new String[]{linkAnoAfectacao}));
            }
            if (hasMesAfectacao && !Month.isMonth(String.valueOf(mesAfectacao))) {
                result.add(new DeclValidationMessage("G049", linkMesAfectacao, new String[]{linkMesAfectacao}));
            }
            if (valorAfectacao != null && valorAfectacao == 0) {
                result.add(new DeclValidationMessage("G050", linkValorAfectacao, new String[]{linkValorAfectacao}));
            }
            if (hasAnoAquisicao && !Modelo3IRSValidatorUtil.isValidYear(anoAquisicao)) {
                result.add(new DeclValidationMessage("G052", linkAnoAquisicao, new String[]{linkAnoAquisicao}));
            }
            if (hasMesAquisicao && !Month.isMonth(String.valueOf(mesAquisicao))) {
                result.add(new DeclValidationMessage("G053", linkMesAquisicao, new String[]{linkMesAquisicao}));
            }
            this.validateG352(result, anexoGq04T2Linha, linkCurrentLine);
            this.validateG353(result, anexoGq04T2Linha, linkCurrentLine);
            this.validateG364(result, anexoGq04T2Linha, quadro04AnexoGModel.getAnexoGq04T1(), linkCurrentLine);
            this.validateG370(result, anexoGq04T2Linha, model, linkCurrentLine, linkBens);
            this.validateG371(result, anexoGq04T2Linha, model, linkCurrentLine, linkBens);
            this.validateG372(result, anexoGq04T2Linha, model, linkCurrentLine, linkBens);
            this.validateG365(result, anexoGq04T2Linha, model);
            this.validateG356(result, anexoGq04T2Linha, linkBens);
            this.validateG357(result, anexoGq04T2Linha, linkFreguesia);
            this.validateG358(result, anexoGq04T2Linha, linkTipo);
            this.validateG369(result, anexoGq04T2Linha, linkTipo, linkArtigo);
            this.validateG359(result, anexoGq04T2Linha, linkArtigo, linkTipo);
            this.validateG360(result, anexoGq04T2Linha, linkFraccao);
            this.validateG361(result, anexoGq04T2Linha, linkFraccao, linkTipo);
            this.validateG362(result, anexoGq04T2Linha, linkQuotaParte);
        }
        this.validateG045(result, quadro04AnexoGModel);
    }

    protected void validateG364(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, EventList<AnexoGq04T1_Linha> anexoGq04T1, String linkCurrentLine) {
        if (!(anexoGq04T2Linha == null || anexoGq04T1 == null || anexoGq04T1.isEmpty())) {
            String freguesia = anexoGq04T2Linha.getFreguesia();
            String tipoPredio = anexoGq04T2Linha.getTipoPredio();
            Long artigo = anexoGq04T2Linha.getArtigo();
            String fraccao = anexoGq04T2Linha.getFraccao();
            for (AnexoGq04T1_Linha anexoGq04T1_Linha : anexoGq04T1) {
                if (!Modelo3IRSValidatorUtil.equals(freguesia, anexoGq04T1_Linha.getFreguesia()) || !Modelo3IRSValidatorUtil.equals(tipoPredio, anexoGq04T1_Linha.getTipoPredio()) || !Modelo3IRSValidatorUtil.equals(artigo, anexoGq04T1_Linha.getArtigo()) || !Modelo3IRSValidatorUtil.equals(fraccao, anexoGq04T1_Linha.getFraccao())) continue;
                result.add(new DeclValidationMessage("G364", "aAnexoG.qQuadro04.tanexoGq04T2", new String[]{linkCurrentLine, "aAnexoG.qQuadro04.tanexoGq04T1", "aAnexoG.qQuadro04.tanexoGq04T2"}));
            }
        }
    }

    protected void validateG370(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, DeclaracaoModel model, String linkCurrentLine, String linkBens) {
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long _titular = rostoModel != null && anexoGq04T2Linha.getTitular() != null ? rostoModel.getNIFTitular(anexoGq04T2Linha.getTitular()) : null;
        String titular = _titular != null ? _titular.toString() : null;
        AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(titular);
        AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(titular);
        if (!(StringUtil.isEmpty(titular) || StringUtil.isEmpty(anexoGq04T2Linha.getMoveisImoveisBens()) || !anexoGq04T2Linha.getMoveisImoveisBens().equals("I") || (anexoBModel != null || anexoCModel != null) && (anexoBModel == null || anexoBModel.getQuadro04().getAnexoBq04B3() != null && anexoBModel.getQuadro04().getAnexoBq04B3().equals("1")) && (anexoCModel == null || anexoCModel.getQuadro14().getAnexoCq14B1() != null && anexoCModel.getQuadro14().getAnexoCq14B1().equals("1")))) {
            result.add(new DeclValidationMessage("G370", linkCurrentLine, new String[]{linkBens}));
        }
    }

    protected void validateG371(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, DeclaracaoModel model, String linkCurrentLine, String linkBens) {
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long _titular = rostoModel != null && anexoGq04T2Linha.getTitular() != null ? rostoModel.getNIFTitular(anexoGq04T2Linha.getTitular()) : null;
        String titular = _titular != null ? _titular.toString() : null;
        AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(titular);
        AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(titular);
        if (!(StringUtil.isEmpty(titular) || StringUtil.isEmpty(anexoGq04T2Linha.getMoveisImoveisBens()) || !anexoGq04T2Linha.getMoveisImoveisBens().equals("M") || anexoBModel != null || anexoCModel != null)) {
            result.add(new DeclValidationMessage("G371", linkCurrentLine, new String[]{linkBens}));
        }
    }

    protected void validateG372(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, DeclaracaoModel model, String linkCurrentLine, String linkBens) {
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long _titular = rostoModel != null && anexoGq04T2Linha.getTitular() != null ? rostoModel.getNIFTitular(anexoGq04T2Linha.getTitular()) : null;
        String titular = _titular != null ? _titular.toString() : null;
        AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(titular);
        AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(titular);
        if (!StringUtil.isEmpty(titular) && !StringUtil.isEmpty(anexoGq04T2Linha.getMoveisImoveisBens()) && anexoGq04T2Linha.getMoveisImoveisBens().equals("M") && (anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04B3() != null && anexoBModel.getQuadro04().getAnexoBq04B3().equals("1") || anexoCModel != null && anexoCModel.getQuadro14().getAnexoCq14B1() != null && anexoCModel.getQuadro14().getAnexoCq14B1().equals("1"))) {
            result.add(new DeclValidationMessage("G372", linkCurrentLine, new String[]{linkBens}));
        }
    }

    protected void validateG365(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, DeclaracaoModel model) {
        Long nif;
        RostoModel rostoModel;
        if (!(anexoGq04T2Linha == null || model == null || StringUtil.isEmpty(anexoGq04T2Linha.getTitular()) || Modelo3IRSValidatorUtil.isEmptyOrZero(nif = (rostoModel = (RostoModel)model.getAnexo(RostoModel.class)).getNIFTitular(anexoGq04T2Linha.getTitular())))) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(AnexoBModel.class, nif.toString());
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(AnexoCModel.class, nif.toString());
            if (anexoBModel == null && anexoCModel == null) {
                result.add(new DeclValidationMessage("G365", "aAnexoG.qQuadro04.tanexoGq04T2", new String[]{"aAnexoG.qQuadro04.tanexoGq04T2"}));
            }
        }
    }

    protected void validateG044(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkCurrentLine) {
        if (anexoGq04T2Linha != null) {
            boolean hasQuotaParte;
            boolean hasTitular = !StringUtil.isEmpty(anexoGq04T2Linha.getTitular());
            boolean hasAnoAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getAnoAfectacao());
            boolean hasMesAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getMesAfectacao());
            boolean hasValorAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getValorAfectacao());
            boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getAnoAquisicao());
            boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getMesAquisicao());
            boolean hasValorAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getValorAquisicao());
            boolean hasBens = !StringUtil.isEmpty(anexoGq04T2Linha.getMoveisImoveisBens());
            boolean hasFreguesia = !StringUtil.isEmpty(anexoGq04T2Linha.getFreguesia());
            boolean hasTipoPredio = !StringUtil.isEmpty(anexoGq04T2Linha.getTipoPredio());
            boolean hasArtigo = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getArtigo());
            boolean hasFraccao = !StringUtil.isEmpty(anexoGq04T2Linha.getFraccao());
            boolean bl = hasQuotaParte = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getQuotaParte());
            if (!(hasTitular || hasAnoAfectacao || hasMesAfectacao || hasValorAfectacao || hasAnoAquisicao || hasMesAquisicao || hasValorAquisicao || hasBens || hasFreguesia || hasTipoPredio || hasArtigo || hasFraccao || hasQuotaParte)) {
                result.add(new DeclValidationMessage("G044", linkCurrentLine, new String[]{linkCurrentLine}));
            }
        }
    }

    protected void validateG045(ValidationResult result, pt.dgci.modelo3irs.v2015.model.anexog.Quadro04 quadro04AnexoGModel) {
        if (!(quadro04AnexoGModel == null || quadro04AnexoGModel.getAnexoGq04T2() == null || quadro04AnexoGModel.getAnexoGq04T2().isEmpty())) {
            for (int idxLinha = 0; idxLinha < quadro04AnexoGModel.getAnexoGq04T2().size(); ++idxLinha) {
                AnexoGq04T2_Linha anexoGq04T2Linha = quadro04AnexoGModel.getAnexoGq04T2().get(idxLinha);
                if (StringUtil.isEmpty(anexoGq04T2Linha.getTitular()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getAnoAfectacao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getMesAfectacao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getValorAfectacao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getAnoAquisicao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getMesAquisicao()) && !StringUtil.isEmpty(anexoGq04T2Linha.getMoveisImoveisBens()) && anexoGq04T2Linha.getValorAquisicao() != null) continue;
                result.add(new DeclValidationMessage("G045", AnexoGq04T2_Linha.getLink(idxLinha + 1), new String[]{AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.TITULAR), AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.MOVEISIMOVEISBENS), AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.ANOAFECTACAO), AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.MESAFECTACAO), AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.VALORAFECTACAO), AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.ANOAQUISICAO), AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.MESAQUISICAO), AnexoGq04T2_Linha.getLink(idxLinha + 1, AnexoGq04T2_LinhaBase.Property.VALORAQUISICAO)}));
            }
        }
    }

    protected void validateG046(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkCurrentLine) {
        if (anexoGq04T2Linha != null) {
            boolean hasBens;
            String linkTitular = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.TITULAR.getIndex();
            boolean hasTitular = !StringUtil.isEmpty(anexoGq04T2Linha.getTitular());
            boolean hasAnoAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getAnoAfectacao());
            boolean hasMesAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getMesAfectacao());
            boolean hasValorAfectacao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getValorAfectacao());
            boolean hasAnoAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getAnoAquisicao());
            boolean hasMesAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getMesAquisicao());
            boolean hasValorAquisicao = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getValorAquisicao());
            boolean bl = hasBens = !StringUtil.isEmpty(anexoGq04T2Linha.getMoveisImoveisBens());
            if (!hasTitular && (hasAnoAfectacao || hasMesAfectacao || hasValorAfectacao || hasAnoAquisicao || hasMesAquisicao || hasValorAquisicao || hasBens)) {
                result.add(new DeclValidationMessage("G046", linkTitular, new String[]{linkTitular}));
            }
        }
    }

    protected void validateG014(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G014", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    protected void validateG013(ValidationResult result, String linkTitular, String titular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G013", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    public void validateG332(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G332", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    public void validateG333(ValidationResult result, RostoModel rostoModel, Long anoAquisicao, String linkAnoAquisicao) {
        if (anoAquisicao != null && rostoModel.getQuadro02().getQ02C02() != null && anoAquisicao > rostoModel.getQuadro02().getQ02C02()) {
            result.add(new DeclValidationMessage("G333", linkAnoAquisicao, new String[]{linkAnoAquisicao, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    public void validateG334(ValidationResult result, Long anoAfetacao, Long mesAfetacao, Long anoAquisicao, Long mesAquisicao, String linkAnoAfetacao, String linkMesAfetacao, String linkAnoAquisicao, String linkMesAquisicao) {
        if (anoAfetacao != null && mesAfetacao != null && anoAquisicao != null && mesAquisicao != null && (anoAfetacao < anoAquisicao || anoAfetacao.longValue() == anoAquisicao.longValue() && mesAfetacao < mesAquisicao)) {
            result.add(new DeclValidationMessage("G334", linkAnoAfetacao, new String[]{linkAnoAfetacao, linkMesAfetacao, linkAnoAquisicao, linkMesAquisicao}));
        }
    }

    protected void validateG352(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkCurrentLine) {
        if (anexoGq04T2Linha != null) {
            boolean hasQuotaParte;
            String bens = anexoGq04T2Linha.getMoveisImoveisBens();
            boolean hasFreguesia = !StringUtil.isEmpty(anexoGq04T2Linha.getFreguesia());
            boolean hasTipoPredio = !StringUtil.isEmpty(anexoGq04T2Linha.getTipoPredio());
            boolean hasArtigo = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getArtigo());
            boolean hasFraccao = !StringUtil.isEmpty(anexoGq04T2Linha.getFraccao());
            boolean bl = hasQuotaParte = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getQuotaParte());
            if ((hasFreguesia || hasTipoPredio || hasArtigo || hasFraccao || hasQuotaParte) && !"I".equalsIgnoreCase(bens)) {
                result.add(new DeclValidationMessage("G352", linkCurrentLine, new String[]{linkCurrentLine}));
            }
        }
    }

    protected void validateG353(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkCurrentLine) {
        if (anexoGq04T2Linha != null) {
            boolean hasQuotaParte;
            String bens = anexoGq04T2Linha.getMoveisImoveisBens();
            boolean hasFreguesia = !StringUtil.isEmpty(anexoGq04T2Linha.getFreguesia());
            boolean hasTipoPredio = !StringUtil.isEmpty(anexoGq04T2Linha.getTipoPredio());
            boolean bl = hasQuotaParte = !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getQuotaParte());
            if (!(!"I".equalsIgnoreCase(bens) || hasFreguesia && hasTipoPredio && hasQuotaParte)) {
                result.add(new DeclValidationMessage("G353", linkCurrentLine, new String[]{linkCurrentLine}));
            }
        }
    }

    protected void validateG356(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkBens) {
        if (!(anexoGq04T2Linha == null || StringUtil.isEmpty(anexoGq04T2Linha.getMoveisImoveisBens()) || "M".equalsIgnoreCase(anexoGq04T2Linha.getMoveisImoveisBens()) || "I".equalsIgnoreCase(anexoGq04T2Linha.getMoveisImoveisBens()))) {
            result.add(new DeclValidationMessage("G356", linkBens, new String[]{linkBens}));
        }
    }

    protected void validateG357(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkFreguesia) {
        ListMap fregCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
        if (!(anexoGq04T2Linha == null || anexoGq04T2Linha.getFreguesia() == null || fregCatalog == null || fregCatalog.containsKey(anexoGq04T2Linha.getFreguesia()))) {
            result.add(new DeclValidationMessage("G357", linkFreguesia, new String[]{linkFreguesia}));
        }
    }

    protected void validateG358(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkTipo) {
        if (!(anexoGq04T2Linha == null || StringUtil.isEmpty(anexoGq04T2Linha.getTipoPredio()) || "U".equals(anexoGq04T2Linha.getTipoPredio()) || "R".equals(anexoGq04T2Linha.getTipoPredio()) || "O".equals(anexoGq04T2Linha.getTipoPredio()))) {
            result.add(new DeclValidationMessage("G358", linkTipo, new String[]{linkTipo}));
        }
    }

    protected void validateG369(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkTipo, String linkArtigo) {
        if (anexoGq04T2Linha != null && !StringUtil.isEmpty(anexoGq04T2Linha.getTipoPredio()) && ("U".equals(anexoGq04T2Linha.getTipoPredio()) || "R".equals(anexoGq04T2Linha.getTipoPredio())) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getArtigo())) {
            result.add(new DeclValidationMessage("G369", linkTipo, new String[]{linkTipo, linkArtigo}));
        }
    }

    protected void validateG359(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkArtigo, String linkTipo) {
        if (!(anexoGq04T2Linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoGq04T2Linha.getArtigo()) || StringUtil.isEmpty(anexoGq04T2Linha.getTipoPredio()) || "U".equals(anexoGq04T2Linha.getTipoPredio()) || "R".equals(anexoGq04T2Linha.getTipoPredio()))) {
            result.add(new DeclValidationMessage("G359", linkArtigo, new String[]{linkTipo, linkArtigo}));
        }
    }

    protected void validateG360(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkFraccao) {
        if (!(anexoGq04T2Linha == null || StringUtil.isEmpty(anexoGq04T2Linha.getFraccao()))) {
            String frac = anexoGq04T2Linha.getFraccao().toUpperCase();
            String regex = "[^A-Z0-9 ]";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher((CharSequence)frac);
            if (matcher.find()) {
                result.add(new DeclValidationMessage("G360", linkFraccao, new String[]{linkFraccao}));
            }
        }
    }

    protected void validateG361(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkFraccao, String linkTipo) {
        if (!(anexoGq04T2Linha == null || StringUtil.isEmpty(anexoGq04T2Linha.getFraccao()) || "U".equals(anexoGq04T2Linha.getTipoPredio()) || "R".equals(anexoGq04T2Linha.getTipoPredio()))) {
            result.add(new DeclValidationMessage("G361", linkFraccao, new String[]{linkFraccao, linkTipo}));
        }
    }

    protected void validateG362(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkQuotaParte) {
        if (anexoGq04T2Linha != null && anexoGq04T2Linha.getQuotaParte() != null && (anexoGq04T2Linha.getQuotaParte() <= 0 || anexoGq04T2Linha.getQuotaParte() > 10000)) {
            result.add(new DeclValidationMessage("G362", linkQuotaParte, new String[]{linkQuotaParte}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        boolean hasCampo6_7;
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        pt.dgci.modelo3irs.v2015.model.anexog.Quadro04 quadro04AnexoGModel = anexoGModel.getQuadro04();
        this.validateG009(result, anexoGModel);
        String linkCampo6_7SIM = "aAnexoG.qQuadro04.fanexoGq04B1OPSim";
        String linkCampo6_7NAO = "aAnexoG.qQuadro04.fanexoGq04B1OPNao";
        String campo6_7 = quadro04AnexoGModel.getAnexoGq04B1OP();
        boolean bl = hasCampo6_7 = !StringUtil.isEmpty(campo6_7);
        if (hasCampo6_7 && anexoGModel.getQuadro04().getAnexoGq04AT1().isEmpty()) {
            result.add(new DeclValidationMessage("G039", linkCampo6_7SIM, new String[]{linkCampo6_7SIM, linkCampo6_7NAO, "aAnexoG.qQuadro04.tanexoGq04AT1"}));
        }
        ArrayList<Tuple> processedLinesG043 = new ArrayList<Tuple>();
        ArrayList<Tuple> processedLinesG350 = new ArrayList<Tuple>();
        for (int linha = 0; linha < quadro04AnexoGModel.getAnexoGq04T2().size(); ++linha) {
            AnexoGq04T2_Linha anexoGq04T2Linha = quadro04AnexoGModel.getAnexoGq04T2().get(linha);
            String titular = anexoGq04T2Linha.getTitular();
            Long anoAfetacao = anexoGq04T2Linha.getAnoAfectacao();
            Long mesAfetacao = anexoGq04T2Linha.getMesAfectacao();
            Long anoAquisicao = anexoGq04T2Linha.getAnoAquisicao();
            Long mesAquisicao = anexoGq04T2Linha.getMesAquisicao();
            String linkCurrentLine = "aAnexoG.qQuadro04.tanexoGq04T2.l" + (linha + 1);
            String linkTitular = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.TITULAR.getIndex();
            String linkAnoAfetacao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.ANOAFECTACAO.getIndex();
            String linkMesAfetacao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.MESAFECTACAO.getIndex();
            String linkAnoAquisicao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.ANOAQUISICAO.getIndex();
            String linkMesAquisicao = linkCurrentLine + ".c" + AnexoGq04T2_LinhaBase.Property.MESAQUISICAO.getIndex();
            this.validateG332(result, titular, linkTitular);
            this.validateG333(result, rostoModel, anoAquisicao, linkAnoAquisicao);
            this.validateG334(result, anoAfetacao, mesAfetacao, anoAquisicao, mesAquisicao, linkAnoAfetacao, linkMesAfetacao, linkAnoAquisicao, linkMesAquisicao);
            this.validateG043(result, anexoGq04T2Linha, linkCurrentLine, processedLinesG043);
            this.validateG350(result, anexoGq04T2Linha, linkCurrentLine, processedLinesG350);
        }
        return result;
    }

    protected void validateG009(ValidationResult result, AnexoGModel anexoGModel) {
        if (!(anexoGModel == null || anexoGModel.getQuadro04().getAnexoGq04T1() == null || anexoGModel.getQuadro04().getAnexoGq04T1().isEmpty())) {
            for (int i = 0; i < anexoGModel.getQuadro04().getAnexoGq04T1().size(); ++i) {
                AnexoGq04T1_Linha linha = anexoGModel.getQuadro04().getAnexoGq04T1().get(i);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || !StringUtil.isEmpty(linha.getTitular()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoRealizacao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesRealizacao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorRealizacao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMesAquisicao()) && linha.getValorAquisicao() != null && !StringUtil.isEmpty(linha.getFreguesia()) && !StringUtil.isEmpty(linha.getTipoPredio()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getQuotaParte())) continue;
                result.add(new DeclValidationMessage("G009", AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.NLINHA), new String[]{AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.NLINHA), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.TITULAR), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.ANOREALIZACAO), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.MESREALIZACAO), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.VALORREALIZACAO), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.ANOAQUISICAO), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.MESAQUISICAO), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.VALORAQUISICAO), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.FREGUESIA), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.TIPOPREDIO), AnexoGq04T1_Linha.getLink(i + 1, AnexoGq04T1_LinhaBase.Property.QUOTAPARTE)}));
            }
        }
    }

    protected void validateG043(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkCurrentLine, List<Tuple> processedLinesG043) {
        if ("M".equalsIgnoreCase(anexoGq04T2Linha.getMoveisImoveisBens())) {
            Tuple tuple = new Tuple(new Object[]{anexoGq04T2Linha.getTitular(), anexoGq04T2Linha.getAnoAfectacao(), anexoGq04T2Linha.getMesAfectacao(), anexoGq04T2Linha.getValorAfectacao(), anexoGq04T2Linha.getAnoAquisicao(), anexoGq04T2Linha.getMoveisImoveisBens()});
            if (processedLinesG043.contains(tuple)) {
                result.add(new DeclValidationMessage("G043", linkCurrentLine, new String[]{linkCurrentLine}));
            } else {
                processedLinesG043.add(tuple);
            }
        }
    }

    protected void validateG350(ValidationResult result, AnexoGq04T2_Linha anexoGq04T2Linha, String linkCurrentLine, List<Tuple> processedLinesG350) {
        if ("I".equalsIgnoreCase(anexoGq04T2Linha.getMoveisImoveisBens())) {
            Tuple tuple = new Tuple(new Object[]{anexoGq04T2Linha.getTitular(), anexoGq04T2Linha.getAnoAfectacao(), anexoGq04T2Linha.getMesAfectacao(), anexoGq04T2Linha.getValorAfectacao(), anexoGq04T2Linha.getAnoAquisicao(), anexoGq04T2Linha.getMoveisImoveisBens(), anexoGq04T2Linha.getFreguesia(), anexoGq04T2Linha.getTipoPredio(), anexoGq04T2Linha.getArtigo(), anexoGq04T2Linha.getFraccao(), anexoGq04T2Linha.getQuotaParte()});
            if (processedLinesG350.contains(tuple)) {
                result.add(new DeclValidationMessage("G350", linkCurrentLine, new String[]{linkCurrentLine}));
            } else {
                processedLinesG350.add(tuple);
            }
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

