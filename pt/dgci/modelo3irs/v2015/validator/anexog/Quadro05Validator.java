/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.Iterator;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Freguesia;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxG;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Tuple;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final String RUSTICO = "R";
    private static final String URBANO = "U";

    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        Tuple t503;
        AnexoGq04T1_Linha lineAux;
        boolean linhaIncompleta;
        AnexoGq04T1_Linha lineAux2;
        int linha;
        boolean hasQuotaLinha2;
        boolean predioOmisso;
        AnexoGq04T1_Linha lineAux3;
        long temp531;
        AnexoGq04T1_Linha lineAux4;
        long temp511;
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        Quadro04 quadro04AnexoGModel = anexoGModel.getQuadro04();
        Quadro05 quadro05AnexoGModel = anexoGModel.getQuadro05();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        Long f501 = quadro05AnexoGModel.getAnexoGq05C501() != null ? quadro05AnexoGModel.getAnexoGq05C501() : 0;
        Long f502 = quadro05AnexoGModel.getAnexoGq05C502() != null ? quadro05AnexoGModel.getAnexoGq05C502() : 0;
        Long f503 = quadro05AnexoGModel.getAnexoGq05C503() != null ? quadro05AnexoGModel.getAnexoGq05C503() : 0;
        Long f504 = quadro05AnexoGModel.getAnexoGq05C504() != null ? quadro05AnexoGModel.getAnexoGq05C504() : 0;
        Long f505 = quadro05AnexoGModel.getAnexoGq05C505() != null ? quadro05AnexoGModel.getAnexoGq05C505() : 0;
        Long f506 = quadro05AnexoGModel.getAnexoGq05C506() != null ? quadro05AnexoGModel.getAnexoGq05C506() : 0;
        Long f507 = quadro05AnexoGModel.getAnexoGq05C507() != null ? quadro05AnexoGModel.getAnexoGq05C507() : 0;
        Long f508 = quadro05AnexoGModel.getAnexoGq05C508() != null ? quadro05AnexoGModel.getAnexoGq05C508() : 0;
        Long f509 = quadro05AnexoGModel.getAnexoGq05C509() != null ? quadro05AnexoGModel.getAnexoGq05C509() : 0;
        Long f510 = quadro05AnexoGModel.getAnexoGq05C510() != null ? quadro05AnexoGModel.getAnexoGq05C510() : 0;
        Long f511 = quadro05AnexoGModel.getAnexoGq05C511() != null ? quadro05AnexoGModel.getAnexoGq05C511() : 0;
        Long f521 = quadro05AnexoGModel.getAnexoGq05C521() != null ? quadro05AnexoGModel.getAnexoGq05C521() : 0;
        Long f522 = quadro05AnexoGModel.getAnexoGq05C522() != null ? quadro05AnexoGModel.getAnexoGq05C522() : 0;
        Long f523 = quadro05AnexoGModel.getAnexoGq05C523() != null ? quadro05AnexoGModel.getAnexoGq05C523() : 0;
        Long f524 = quadro05AnexoGModel.getAnexoGq05C524() != null ? quadro05AnexoGModel.getAnexoGq05C524() : 0;
        Long f525 = quadro05AnexoGModel.getAnexoGq05C525() != null ? quadro05AnexoGModel.getAnexoGq05C525() : 0;
        Long f526 = quadro05AnexoGModel.getAnexoGq05C526() != null ? quadro05AnexoGModel.getAnexoGq05C526() : 0;
        Long f527 = quadro05AnexoGModel.getAnexoGq05C527() != null ? quadro05AnexoGModel.getAnexoGq05C527() : 0;
        Long f528 = quadro05AnexoGModel.getAnexoGq05C528() != null ? quadro05AnexoGModel.getAnexoGq05C528() : 0;
        Long f529 = quadro05AnexoGModel.getAnexoGq05C529() != null ? quadro05AnexoGModel.getAnexoGq05C529() : 0;
        Long f530 = quadro05AnexoGModel.getAnexoGq05C530() != null ? quadro05AnexoGModel.getAnexoGq05C530() : 0;
        Long f531 = quadro05AnexoGModel.getAnexoGq05C531() != null ? quadro05AnexoGModel.getAnexoGq05C531() : 0;
        String fTitularLinha1 = quadro05AnexoGModel.getAnexoGq05AC1() != null ? quadro05AnexoGModel.getAnexoGq05AC1() : "";
        String fFreguesiaLinha1 = quadro05AnexoGModel.getAnexoGq05AC2() != null ? quadro05AnexoGModel.getAnexoGq05AC2() : "";
        String fTipoLinha1 = quadro05AnexoGModel.getAnexoGq05AC3() != null ? quadro05AnexoGModel.getAnexoGq05AC3() : "";
        Long fArtigoLinha1 = quadro05AnexoGModel.getAnexoGq05AC4() != null ? quadro05AnexoGModel.getAnexoGq05AC4() : 0;
        String fFraccaoLinha1 = quadro05AnexoGModel.getAnexoGq05AC5() != null ? quadro05AnexoGModel.getAnexoGq05AC5() : "";
        Long fQuotaLinha1 = quadro05AnexoGModel.getAnexoGq05AC6() != null ? quadro05AnexoGModel.getAnexoGq05AC6() : 0;
        String fTitularLinha2 = quadro05AnexoGModel.getAnexoGq05AC7() != null ? quadro05AnexoGModel.getAnexoGq05AC7() : "";
        String fFreguesiaLinha2 = quadro05AnexoGModel.getAnexoGq05AC8() != null ? quadro05AnexoGModel.getAnexoGq05AC8() : "";
        String fTipoLinha2 = quadro05AnexoGModel.getAnexoGq05AC9() != null ? quadro05AnexoGModel.getAnexoGq05AC9() : "";
        Long fArtigoLinha2 = quadro05AnexoGModel.getAnexoGq05AC10() != null ? quadro05AnexoGModel.getAnexoGq05AC10() : 0;
        String fFraccaoLinha2 = quadro05AnexoGModel.getAnexoGq05AC11() != null ? quadro05AnexoGModel.getAnexoGq05AC11() : "";
        Long fQuotaLinha2 = quadro05AnexoGModel.getAnexoGq05AC12() != null ? quadro05AnexoGModel.getAnexoGq05AC12() : 0;
        String linkF501 = "aAnexoG.qQuadro05.fanexoGq05C501";
        String linkF502 = "aAnexoG.qQuadro05.fanexoGq05C502";
        String linkF503 = "aAnexoG.qQuadro05.fanexoGq05C503";
        String linkF504 = "aAnexoG.qQuadro05.fanexoGq05C504";
        String linkF505 = "aAnexoG.qQuadro05.fanexoGq05C505";
        String linkF506 = "aAnexoG.qQuadro05.fanexoGq05C506";
        String linkF507 = "aAnexoG.qQuadro05.fanexoGq05C507";
        String linkF508 = "aAnexoG.qQuadro05.fanexoGq05C508";
        String linkF509 = "aAnexoG.qQuadro05.fanexoGq05C509";
        String linkF510 = "aAnexoG.qQuadro05.fanexoGq05C510";
        String linkF511 = "aAnexoG.qQuadro05.fanexoGq05C511";
        String linkF521 = "aAnexoG.qQuadro05.fanexoGq05C521";
        String linkF522 = "aAnexoG.qQuadro05.fanexoGq05C522";
        String linkF523 = "aAnexoG.qQuadro05.fanexoGq05C523";
        String linkF524 = "aAnexoG.qQuadro05.fanexoGq05C524";
        String linkF525 = "aAnexoG.qQuadro05.fanexoGq05C525";
        String linkF526 = "aAnexoG.qQuadro05.fanexoGq05C526";
        String linkF527 = "aAnexoG.qQuadro05.fanexoGq05C527";
        String linkF528 = "aAnexoG.qQuadro05.fanexoGq05C528";
        String linkF529 = "aAnexoG.qQuadro05.fanexoGq05C529";
        String linkF530 = "aAnexoG.qQuadro05.fanexoGq05C530";
        String linkF531 = "aAnexoG.qQuadro05.fanexoGq05C531";
        String linkPais = "aAnexoG.qQuadro05.fanexoGq05AC13";
        String linkTitularLinha1 = "aAnexoG.qQuadro05.fanexoGq05AC1";
        String linkFreguesiaLinha1 = "aAnexoG.qQuadro05.fanexoGq05AC2";
        String linkTipoLinha1 = "aAnexoG.qQuadro05.fanexoGq05AC3";
        String linkArtigoLinha1 = "aAnexoG.qQuadro05.fanexoGq05AC4";
        String linkQuotaLinha1 = "aAnexoG.qQuadro05.fanexoGq05AC6";
        String linkTitularLinha2 = "aAnexoG.qQuadro05.fanexoGq05AC7";
        String linkFreguesiaLinha2 = "aAnexoG.qQuadro05.fanexoGq05AC8";
        String linkTipoLinha2 = "aAnexoG.qQuadro05.fanexoGq05AC9";
        String linkArtigoLinha2 = "aAnexoG.qQuadro05.fanexoGq05AC10";
        String linkQuotaLinha2 = "aAnexoG.qQuadro05.fanexoGq05AC12";
        boolean hasAnoExercicio = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio);
        boolean hasF501 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C501());
        boolean hasF502 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C502());
        boolean hasF503 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C503());
        boolean hasF504 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C504());
        boolean hasF505 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C505());
        boolean hasF506 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C506());
        boolean hasF507 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C507());
        boolean hasF508 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C508());
        boolean hasF509 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C509());
        boolean hasF510 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C510());
        boolean hasF511 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C511());
        boolean hasF521 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C521());
        boolean hasF522 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C522());
        boolean hasF523 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C523());
        boolean hasF524 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C524());
        boolean hasF525 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C525());
        boolean hasF526 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C526());
        boolean hasF527 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C527());
        boolean hasF528 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C528());
        boolean hasF529 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C529());
        boolean hasF530 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C530());
        boolean hasF531 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C531());
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC1());
        boolean hasFreguesiaLinha1 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC2());
        boolean hasTipoLinha1 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC3());
        boolean hasArtigoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05AC4());
        boolean hasFraccaoLinha1 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC5());
        boolean hasQuotaLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05AC6());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC7());
        boolean hasFreguesiaLinha2 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC8());
        boolean hasTipoLinha2 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC9());
        boolean hasArtigoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05AC10());
        boolean hasFraccaoLinha2 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC11());
        boolean bl = hasQuotaLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05AC12());
        if (hasF501 && hasAnoExercicio && f501 > anoExercicio) {
            result.add(new DeclValidationMessage("G055", linkF501, new String[]{linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF501 && hasAnoExercicio && anoExercicio - f501 > 3) {
            result.add(new DeclValidationMessage("G056", linkF501, new String[]{linkF501}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio) && hasF521 && f521 > f501) {
            result.add(new DeclValidationMessage("G057", linkF501, new String[]{linkF521, linkF501}));
        }
        if ((!hasF501 || f501 == 0) && hasF521 && f521 > 0) {
            result.add(new DeclValidationMessage("G058", linkF501, new String[]{linkF521, linkF501}));
        }
        if (hasF501 && !hasF502) {
            result.add(new DeclValidationMessage("G059", linkF501, new String[]{linkF502, linkF501}));
        }
        if (hasF501 && hasF521 && f501.equals(f521)) {
            result.add(new DeclValidationMessage("G060", linkF501, new String[]{linkF501, linkF521}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio) && !hasF506) {
            result.add(new DeclValidationMessage("G061", linkF501, new String[]{linkF501, "aRosto.qQuadro02.fq02C02", linkF506}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio) && hasF506 && hasF507 && hasF508 && f507 + f508 > f506) {
            result.add(new DeclValidationMessage("G062", linkF501, new String[]{linkF507, linkF508, linkF506}));
        }
        if (!hasF501 && hasAnoExercicio && (hasF502 || hasF503 || hasF504 || hasF505 || hasF506 || hasF507 || hasF508 || hasF509 || hasF510 || hasF511)) {
            result.add(new DeclValidationMessage("G063", linkF501, new String[]{linkF501}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio - 1) && !hasF509) {
            result.add(new DeclValidationMessage("G064", linkF501, new String[]{linkF509}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio - 2) && !hasF510) {
            result.add(new DeclValidationMessage("G065", linkF501, new String[]{linkF510}));
        }
        if (hasF501 && hasAnoExercicio && f501 < anoExercicio - 3 && (hasF505 || hasF506 || hasF507 || hasF508)) {
            result.add(new DeclValidationMessage("G066", linkF501, new String[]{linkF505, linkF506, linkF507, linkF508}));
        }
        long temp505 = hasF505 ? f505 : 0;
        long temp506 = hasF506 ? f506 : 0;
        long temp507 = hasF507 ? f507 : 0;
        long temp508 = hasF508 ? f508 : 0;
        long temp509 = hasF509 ? f509 : 0;
        long temp510 = hasF510 ? f510 : 0;
        long l = temp511 = hasF511 ? f511 : 0;
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio - 1) && temp505 + temp506 + temp507 + temp508 + temp510 + temp511 > 0) {
            result.add(new DeclValidationMessage("G067", linkF501, new String[]{linkF505, linkF506, linkF507, linkF508, linkF510, linkF511}));
        }
        long somaValorRealizacao = 0;
        long temp502 = hasF502 ? f502 : 0;
        long temp503 = hasF503 ? f503 : 0;
        long temp504 = hasF504 ? f504 : 0;
        for (int linha2 = 0; linha2 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha2) {
            AnexoGq04T1_Linha line = quadro04AnexoGModel.getAnexoGq04T1().get(linha2);
            if (line == null || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getNLinha()) || Modelo3IRSValidatorUtil.isEmptyOrZero(line.getValorRealizacao()) || !line.getNLinha().equals(temp502) && !line.getNLinha().equals(temp503) && !line.getNLinha().equals(temp504)) continue;
            somaValorRealizacao+=line.getValorRealizacao().longValue();
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio) && hasF506 && f506 > somaValorRealizacao) {
            result.add(new DeclValidationMessage("G068", linkF501, new String[]{linkF506, "aAnexoG.qQuadro04.tanexoGq04T1"}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio - 2) && temp505 + temp506 + temp507 + temp508 + temp509 + temp511 > 0) {
            result.add(new DeclValidationMessage("G069", linkF501, new String[]{linkF505, linkF506, linkF507, linkF508, linkF509}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio - 3) && temp505 + temp506 + temp507 + temp508 + temp509 + temp510 > 0) {
            result.add(new DeclValidationMessage("G322", linkF501, new String[]{linkF505, linkF506, linkF507, linkF508, linkF509, linkF510}));
        }
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio - 3) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C511())) {
            result.add(new DeclValidationMessage("G323", linkF511, new String[]{linkF511}));
        }
        if (hasF502 && (f502 < 401 || f502 > 493)) {
            result.add(new DeclValidationMessage("G072", linkF502, new String[]{linkF502}));
        }
        AnexoGq04T1_Linha line = null;
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio) && hasF502) {
            for (int linha3 = 0; linha3 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha3) {
                lineAux2 = quadro04AnexoGModel.getAnexoGq04T1().get(linha3);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux2.getNLinha()) || !lineAux2.getNLinha().equals(f502)) continue;
                line = lineAux2;
                break;
            }
            if (line == null) {
                result.add(new DeclValidationMessage("G073", linkF502, new String[]{linkF502}));
            }
        }
        if (hasF503 && (f503 < 401 || f503 > 493)) {
            result.add(new DeclValidationMessage("G074", linkF503, new String[]{linkF503}));
        }
        line = null;
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio) && hasF503) {
            for (int linha4 = 0; linha4 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha4) {
                lineAux2 = quadro04AnexoGModel.getAnexoGq04T1().get(linha4);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux2.getNLinha()) || !lineAux2.getNLinha().equals(f503)) continue;
                line = lineAux2;
                break;
            }
            if (line == null) {
                result.add(new DeclValidationMessage("G075", linkF503, new String[]{linkF503}));
            }
        }
        AnexoGq04T1_LinhaBase line502 = null;
        AnexoGq04T1_LinhaBase line503 = null;
        if (hasF503 && hasAnoExercicio && hasF502 && hasF501 && f501.equals(anoExercicio)) {
            for (int linha5 = 0; linha5 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha5) {
                lineAux4 = quadro04AnexoGModel.getAnexoGq04T1().get(linha5);
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux4.getNLinha()) && lineAux4.getNLinha().equals(f502)) {
                    line502 = lineAux4;
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux4.getNLinha()) && lineAux4.getNLinha().equals(f503)) {
                    line503 = lineAux4;
                }
                if (line502 != null && line503 != null) break;
            }
            Tuple t502 = null;
            t503 = null;
            if (line502 != null) {
                t502 = new Tuple(new Object[]{line502.getFreguesia(), line502.getTipoPredio(), line502.getArtigo(), line502.getFraccao()});
            }
            if (line503 != null) {
                t503 = new Tuple(new Object[]{line503.getFreguesia(), line503.getTipoPredio(), line503.getArtigo(), line503.getFraccao()});
            }
            if (!(t502 != null && t503 != null && t503.equals(t502))) {
                result.add(new DeclValidationMessage("G076", linkF503, new String[]{linkF502, linkF503, linkF504}));
            }
        }
        if (hasF503 && (hasF502 && f503.equals(f502) || hasF504 && f503.equals(f504))) {
            result.add(new DeclValidationMessage("G077", linkF503, new String[]{linkF503}));
        }
        if (hasF503 && !hasF502) {
            result.add(new DeclValidationMessage("G078", linkF503, new String[]{linkF502}));
        }
        if (hasF504 && (f504 < 401 || f504 > 493)) {
            result.add(new DeclValidationMessage("G079", linkF504, new String[]{linkF504}));
        }
        line = null;
        if (hasF501 && hasAnoExercicio && f501.equals(anoExercicio) && hasF504) {
            for (int linha6 = 0; linha6 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha6) {
                lineAux4 = quadro04AnexoGModel.getAnexoGq04T1().get(linha6);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux4.getNLinha()) || !lineAux4.getNLinha().equals(f504)) continue;
                line = lineAux4;
                break;
            }
            if (line == null) {
                result.add(new DeclValidationMessage("G080", linkF504, new String[]{linkF504}));
            }
        }
        AnexoGq04T1_LinhaBase line504 = null;
        line503 = null;
        if (hasF503 && hasAnoExercicio && hasF502 && hasF501 && hasF504 && f501.equals(anoExercicio)) {
            for (int linha7 = 0; linha7 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha7) {
                AnexoGq04T1_Linha lineAux5 = quadro04AnexoGModel.getAnexoGq04T1().get(linha7);
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux5.getNLinha()) && lineAux5.getNLinha().equals(f503)) {
                    line503 = lineAux5;
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux5.getNLinha()) && lineAux5.getNLinha().equals(f504)) {
                    line504 = lineAux5;
                }
                if (line504 != null && line503 != null) break;
            }
            t503 = null;
            Tuple t504 = null;
            if (line503 != null) {
                t503 = new Tuple(new Object[]{line503.getFreguesia(), line503.getTipoPredio(), line503.getArtigo(), line503.getFraccao()});
            }
            if (line504 != null) {
                t504 = new Tuple(new Object[]{line504.getFreguesia(), line504.getTipoPredio(), line504.getArtigo(), line504.getFraccao()});
            }
            if (!(t504 != null && t503 != null && t503.equals(t504))) {
                result.add(new DeclValidationMessage("G081", linkF504, new String[]{linkF502, linkF503, linkF504}));
            }
        }
        if (hasF504 && (hasF502 && f504.equals(f502) || hasF503 && f503.equals(f504))) {
            result.add(new DeclValidationMessage("G082", linkF504, new String[]{linkF504}));
        }
        if (hasF504 && !hasF503) {
            result.add(new DeclValidationMessage("G083", linkF504, new String[]{linkF503}));
        }
        if (hasF505 && f505 > 0 && hasF501 && hasAnoExercicio && !f501.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G084", linkF505, new String[]{linkF505, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasAnoExercicio && anoExercicio == 2001 && hasF505 && f505 > 0) {
            result.add(new DeclValidationMessage("G085", linkF505, new String[]{linkF505, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF506 && f506 > 0 && hasF501 && hasAnoExercicio && !f501.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G086", linkF506, new String[]{linkF506, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF507 && f507 > 0 && hasF501 && hasAnoExercicio && !f501.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G087", linkF507, new String[]{linkF507, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF507 && hasF506 && f507 > 0 && f507 > f506) {
            result.add(new DeclValidationMessage("G088", linkF507, new String[]{linkF507, linkF506}));
        }
        if (hasF508 && f508 > 0 && hasF501 && hasAnoExercicio && !f501.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G089", linkF508, new String[]{linkF508, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF508 && hasF506 && f508 > 0 && f508 > f506) {
            result.add(new DeclValidationMessage("G090", linkF508, new String[]{linkF508, linkF506}));
        }
        if (hasF509 && f509 > 0 && hasF501 && hasAnoExercicio && !f501.equals(anoExercicio - 1)) {
            result.add(new DeclValidationMessage("G091", linkF509, new String[]{linkF509, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF510 && f510 > 0 && hasF501 && hasAnoExercicio && !f501.equals(anoExercicio - 2)) {
            result.add(new DeclValidationMessage("G092", linkF510, new String[]{linkF510, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF511 && f511 > 0 && hasF501 && hasAnoExercicio && !f501.equals(anoExercicio - 3) && anoExercicio > 2009) {
            result.add(new DeclValidationMessage("G093", linkF511, new String[]{linkF511, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF521 && hasAnoExercicio && f521 > anoExercicio) {
            result.add(new DeclValidationMessage("G094", linkF521, new String[]{linkF521, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF521 && hasAnoExercicio && anoExercicio - f521 > 3) {
            result.add(new DeclValidationMessage("G095", linkF521, new String[]{linkF521}));
        }
        if (hasF521 && !hasF522) {
            result.add(new DeclValidationMessage("G096", linkF521, new String[]{linkF522, linkF521}));
        }
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio) && !hasF526) {
            result.add(new DeclValidationMessage("G097", linkF521, new String[]{linkF521, "aRosto.qQuadro02.fq02C02", linkF526}));
        }
        if (!hasF521 && (hasF522 || hasF523 || hasF524)) {
            result.add(new DeclValidationMessage("G100", linkF521, new String[]{linkF521}));
        }
        if (!hasF521 && (hasF525 && f525 > 0 || hasF526 && f526 > 0 || hasF527 && f527 > 0 || hasF528 && f528 > 0 || hasF529 && f529 > 0 || hasF530 && f530 > 0 || hasF531 && f531 > 0)) {
            result.add(new DeclValidationMessage("G101", linkF521, new String[]{linkF521}));
        }
        long temp525 = hasF525 ? f525 : 0;
        long temp526 = hasF526 ? f526 : 0;
        long temp527 = hasF527 ? f527 : 0;
        long temp528 = hasF528 ? f528 : 0;
        long temp529 = hasF529 ? f529 : 0;
        long temp530 = hasF530 ? f530 : 0;
        long l2 = temp531 = hasF531 ? f531 : 0;
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio - 1) && temp525 + temp526 + temp527 + temp528 + temp530 + temp531 > 0) {
            result.add(new DeclValidationMessage("G102", linkF521, new String[]{linkF525, linkF526, linkF527, linkF528, linkF530, linkF531}));
        }
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio - 1) && !hasF529) {
            result.add(new DeclValidationMessage("G103", linkF521, new String[]{linkF529}));
        }
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio - 2) && temp525 + temp526 + temp527 + temp528 + temp529 + temp531 > 0) {
            result.add(new DeclValidationMessage("G104", linkF521, new String[]{linkF525, linkF526, linkF527, linkF528, linkF529, linkF531}));
        }
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio - 2) && !hasF530) {
            result.add(new DeclValidationMessage("G105", linkF521, new String[]{linkF530}));
        }
        if (hasF521 && hasAnoExercicio && f521 < anoExercicio - 3 && (hasF525 && f525 > 0 || hasF526 && f526 > 0 || hasF527 && f527 > 0 || hasF528 && f528 > 0)) {
            result.add(new DeclValidationMessage("G108", linkF521, new String[]{linkF525, linkF526, linkF527, linkF528}));
        }
        if (!hasF521 && (hasF522 || hasF523 || hasF524 || hasF525 || hasF526 || hasF527 || hasF528 || hasF529 || hasF530 || hasF531)) {
            result.add(new DeclValidationMessage("G109", linkF521, new String[]{linkF521}));
        }
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio - 3) && temp525 + temp526 + temp527 + temp528 + temp529 + temp530 > 0) {
            result.add(new DeclValidationMessage("G321", linkF521, new String[]{linkF525, linkF526, linkF527, linkF528, linkF529, linkF530}));
        }
        if (hasAnoExercicio && f521.equals(anoExercicio - 2) && !hasF530) {
            result.add(new DeclValidationMessage("G111", linkF521, new String[]{linkF530}));
        }
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio - 3) && !hasF531) {
            result.add(new DeclValidationMessage("G324", linkF521, new String[]{linkF531}));
        }
        if (hasF522 && (f522 < 401 || f522 > 493)) {
            result.add(new DeclValidationMessage("G112", linkF522, new String[]{linkF522}));
        }
        line = null;
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio) && hasF522) {
            for (linha = 0; linha < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha) {
                lineAux = quadro04AnexoGModel.getAnexoGq04T1().get(linha);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux.getNLinha()) || !lineAux.getNLinha().equals(f522)) continue;
                line = lineAux;
                break;
            }
            if (line == null) {
                result.add(new DeclValidationMessage("G113", linkF522, new String[]{linkF522}));
            }
        }
        if (hasF523 && (f523 < 401 || f523 > 493)) {
            result.add(new DeclValidationMessage("G114", linkF523, new String[]{linkF523}));
        }
        line = null;
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio) && hasF523) {
            for (linha = 0; linha < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha) {
                lineAux = quadro04AnexoGModel.getAnexoGq04T1().get(linha);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux.getNLinha()) || !lineAux.getNLinha().equals(f523)) continue;
                line = lineAux;
                break;
            }
            if (line == null) {
                result.add(new DeclValidationMessage("G115", linkF523, new String[]{linkF523}));
            }
        }
        AnexoGq04T1_LinhaBase line523 = null;
        AnexoGq04T1_LinhaBase line522 = null;
        if (hasF523 && hasF522) {
            for (int linha8 = 0; linha8 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha8) {
                lineAux3 = quadro04AnexoGModel.getAnexoGq04T1().get(linha8);
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux3.getNLinha()) && lineAux3.getNLinha().equals(f523)) {
                    line523 = lineAux3;
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux3.getNLinha()) && lineAux3.getNLinha().equals(f522)) {
                    line522 = lineAux3;
                }
                if (line523 != null && line522 != null) break;
            }
            Tuple t523 = null;
            Tuple t522 = null;
            if (line523 != null) {
                t523 = new Tuple(new Object[]{line523.getFreguesia(), line523.getTipoPredio(), line523.getArtigo(), line523.getFraccao()});
            }
            if (line522 != null) {
                t522 = new Tuple(new Object[]{line522.getFreguesia(), line522.getTipoPredio(), line522.getArtigo(), line522.getFraccao()});
            }
            if (!(t523 != null && t522 != null && t523.equals(t522))) {
                result.add(new DeclValidationMessage("G116", linkF523, new String[]{linkF522, linkF523, linkF524}));
            }
        }
        if (hasF523 && (hasF522 && f523.equals(f522) || hasF524 && f523.equals(f524))) {
            result.add(new DeclValidationMessage("G117", linkF523, new String[]{linkF523}));
        }
        if (hasF523 && !hasF522) {
            result.add(new DeclValidationMessage("G118", linkF523, new String[]{linkF522}));
        }
        if (hasF524 && (f524 < 401 || f524 > 493)) {
            result.add(new DeclValidationMessage("G119", linkF524, new String[]{linkF524}));
        }
        line = null;
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio) && hasF524) {
            for (int linha9 = 0; linha9 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha9) {
                lineAux3 = quadro04AnexoGModel.getAnexoGq04T1().get(linha9);
                if (Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux3.getNLinha()) || !lineAux3.getNLinha().equals(f524)) continue;
                line = lineAux3;
                break;
            }
            if (line == null) {
                result.add(new DeclValidationMessage("G120", linkF524, new String[]{linkF524}));
            }
        }
        AnexoGq04T1_LinhaBase line524 = null;
        line523 = null;
        if (hasF524 && hasF523) {
            for (int linha10 = 0; linha10 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha10) {
                AnexoGq04T1_Linha lineAux6 = quadro04AnexoGModel.getAnexoGq04T1().get(linha10);
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux6.getNLinha()) && lineAux6.getNLinha().equals(f524)) {
                    line524 = lineAux6;
                }
                if (!Modelo3IRSValidatorUtil.isEmptyOrZero(lineAux6.getNLinha()) && lineAux6.getNLinha().equals(f523)) {
                    line523 = lineAux6;
                }
                if (line524 != null && line523 != null) break;
            }
            Tuple t523 = null;
            Tuple t524 = null;
            if (line523 != null) {
                t523 = new Tuple(new Object[]{line523.getFreguesia(), line523.getTipoPredio(), line523.getArtigo(), line523.getFraccao()});
            }
            if (line524 != null) {
                t524 = new Tuple(new Object[]{line524.getFreguesia(), line524.getTipoPredio(), line524.getArtigo(), line524.getFraccao()});
            }
            if (!(t524 != null && t523 != null && t523.equals(t524))) {
                result.add(new DeclValidationMessage("G121", linkF524, new String[]{linkF522, linkF523, linkF524}));
            }
        }
        if (hasF524 && !hasF523) {
            result.add(new DeclValidationMessage("G122", linkF524, new String[]{linkF523}));
        }
        if (hasF525 && f525 > 0 && hasF521 && !f521.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G123", linkF525, new String[]{linkF525, linkF521, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF525 && hasAnoExercicio && anoExercicio == 2001) {
            result.add(new DeclValidationMessage("G124", linkF525, new String[]{linkF525, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF526 && f526 > 0 && hasF521 && hasAnoExercicio && !f521.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G125", linkF526, new String[]{linkF526, linkF521, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF527 && f527 > 0 && hasF521 && hasAnoExercicio && !f521.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G126", linkF527, new String[]{linkF527, linkF521, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF527 && f527 > 0 && hasF526 && f527 > f526) {
            result.add(new DeclValidationMessage("G127", linkF527, new String[]{linkF527, linkF526}));
        }
        if (hasF528 && f528 > 0 && hasF521 && hasAnoExercicio && !f521.equals(anoExercicio)) {
            result.add(new DeclValidationMessage("G128", linkF528, new String[]{linkF528, linkF521, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF528 && f528 > 0 && hasF526 && f528 > f526) {
            result.add(new DeclValidationMessage("G129", linkF528, new String[]{linkF528, linkF526}));
        }
        if (hasF529 && f529 > 0 && hasF521 && hasAnoExercicio && !f521.equals(anoExercicio - 1)) {
            result.add(new DeclValidationMessage("G130", linkF529, new String[]{linkF529, linkF521, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF530 && f530 > 0 && hasF521 && hasAnoExercicio && !f521.equals(anoExercicio - 2)) {
            result.add(new DeclValidationMessage("G131", linkF530, new String[]{linkF530, linkF521, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF531 && f531 > 0 && hasF501 && hasAnoExercicio && f501.equals(anoExercicio - 3) && anoExercicio > 2009) {
            result.add(new DeclValidationMessage("G132", linkF531, new String[]{linkF531, linkF501, "aRosto.qQuadro02.fq02C02"}));
        }
        if (hasF521 && hasAnoExercicio && f521.equals(anoExercicio) && (hasF522 || hasF523 || hasF524)) {
            long somaQ5C40_5 = 0;
            for (int linha11 = 0; linha11 < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha11) {
                AnexoGq04T1_Linha lineAux7 = quadro04AnexoGModel.getAnexoGq04T1().get(linha11);
                if (lineAux7.getNLinha() == null || !lineAux7.getNLinha().equals(f522) && !lineAux7.getNLinha().equals(f523) && !lineAux7.getNLinha().equals(f524)) continue;
                somaQ5C40_5+=lineAux7.getValorRealizacao() != null ? lineAux7.getValorRealizacao() : 0;
            }
            long somaF525_F526 = 0;
            if (hasF525) {
                somaF525_F526+=f525.longValue();
            }
            if (hasF526) {
                somaF525_F526+=f526.longValue();
            }
            if (somaF525_F526 > somaQ5C40_5) {
                result.add(new DeclValidationMessage("G133", linkF531, new String[]{linkF525, linkF526, linkF522, linkF523, linkF524}));
            }
        }
        if ((hasF509 || hasF510 || hasF511) && !hasF502 && !hasF503 && !hasF504) {
            result.add(new DeclValidationMessage("G135", linkF502, new String[]{linkF502, linkF503, linkF504}));
        }
        if ((hasF529 && f529 > 0 || hasF530 && f530 > 0 || hasF531 && f531 > 0) && !hasF522 && !hasF523 && !hasF524) {
            result.add(new DeclValidationMessage("G136", linkF522, new String[]{linkF522, linkF523, linkF524}));
        }
        this.validateG344(result, quadro05AnexoGModel);
        this.validateG366(result, quadro04AnexoGModel, quadro05AnexoGModel);
        if ((quadro05AnexoGModel.getAnexoGq05AC2() != null || quadro05AnexoGModel.getAnexoGq05AC3() != null) && hasF501 && f501 >= 2007 && hasAnoExercicio && f501.equals(anoExercicio) && (!hasF506 || !hasF507 && !hasF508)) {
            result.add(new DeclValidationMessage("G142", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF501}));
        }
        if ((quadro05AnexoGModel.getAnexoGq05AC2() != null || quadro05AnexoGModel.getAnexoGq05AC13() != null) && hasF501 && f501 >= 2007 && hasAnoExercicio && f501.equals(anoExercicio - 1) && !hasF509) {
            result.add(new DeclValidationMessage("G143", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF501}));
        }
        if ((quadro05AnexoGModel.getAnexoGq05AC2() != null || quadro05AnexoGModel.getAnexoGq05AC13() != null) && hasF501 && f501 >= 2007 && hasAnoExercicio && f501.equals(anoExercicio - 2) && !hasF510) {
            result.add(new DeclValidationMessage("G144", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF501}));
        }
        if ((quadro05AnexoGModel.getAnexoGq05AC8() != null || quadro05AnexoGModel.getAnexoGq05AC13() != null) && hasF521 && f521 >= 2007 && hasAnoExercicio && f521.equals(anoExercicio) && (!hasF526 || !hasF527 && !hasF528)) {
            result.add(new DeclValidationMessage("G145", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF521}));
        }
        if ((quadro05AnexoGModel.getAnexoGq05AC8() != null || quadro05AnexoGModel.getAnexoGq05AC13() != null) && hasF521 && f521 >= 2007 && hasAnoExercicio && f521.equals(anoExercicio - 1) && !hasF529) {
            result.add(new DeclValidationMessage("G146", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF501}));
        }
        if ((quadro05AnexoGModel.getAnexoGq05AC8() != null || quadro05AnexoGModel.getAnexoGq05AC13() != null) && hasF521 && f521 >= 2007 && hasAnoExercicio && f521.equals(anoExercicio - 2) && !hasF530) {
            result.add(new DeclValidationMessage("G147", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF521}));
        }
        if ((quadro05AnexoGModel.getAnexoGq05AC2() != null || quadro05AnexoGModel.getAnexoGq05AC13() != null) && hasF501 && f501 >= 2007 && hasAnoExercicio && f501.equals(anoExercicio - 3) && !hasF511) {
            result.add(new DeclValidationMessage("G327", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF501}));
        }
        if ((quadro05AnexoGModel.getAnexoGq05AC8() != null || quadro05AnexoGModel.getAnexoGq05AC13() != null) && hasF521 && f521 >= 2007 && hasAnoExercicio && f521.equals(anoExercicio - 3) && !hasF531) {
            result.add(new DeclValidationMessage("G328", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkF521}));
        }
        if (anexoGModel.getQuadro05().getAnexoGq05AC13() != null && (hasF501 && f501 < 2007 || hasF521 && f521 < 2007)) {
            result.add(new DeclValidationMessage("G148", "aAnexoG.qQuadro05.fanexoGq05AC13", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13"}));
        }
        this.validateG149(result, quadro05AnexoGModel, linkPais);
        this.validateG152(result, linkTitularLinha1, fTitularLinha1);
        this.validateG153(result, rostoModel, fTitularLinha1, linkTitularLinha1);
        boolean bl2 = linhaIncompleta = (hasTitularLinha1 || hasTipoLinha1 || hasArtigoLinha1 || hasFraccaoLinha1 || hasQuotaLinha1) && !hasFreguesiaLinha1;
        if (linhaIncompleta) {
            result.add(new DeclValidationMessage("G157", linkFreguesiaLinha1, new String[]{linkFreguesiaLinha1}));
        }
        ListMap freguesiasCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
        if (hasFreguesiaLinha1 && !freguesiasCatalog.containsKey(fFreguesiaLinha1)) {
            result.add(new DeclValidationMessage("G158", linkFreguesiaLinha1, new String[]{linkFreguesiaLinha1}));
        }
        if (!(!hasFreguesiaLinha1 || hasF507 && f507 != 0 || hasF508 && f508 != 0 || hasF509 && f509 != 0 || hasF510 && f510 != 0 || hasF511 && f511 != 0)) {
            result.add(new DeclValidationMessage("G159", linkFreguesiaLinha1, new String[]{linkFreguesiaLinha1, linkF507}));
        }
        if ((hasFreguesiaLinha1 || hasFreguesiaLinha2) && !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05AC13()) && (hasF501 && f501 >= 2007 || hasF521 && f521 >= 2007)) {
            result.add(new DeclValidationMessage("G162", linkFreguesiaLinha1, new String[]{"aAnexoG.qQuadro05.fanexoGq05AC13", linkFreguesiaLinha1}));
        }
        boolean bl3 = linhaIncompleta = (hasFreguesiaLinha1 || hasArtigoLinha1 || hasFraccaoLinha1) && !hasTipoLinha1;
        if (linhaIncompleta) {
            result.add(new DeclValidationMessage("G164", linkTipoLinha1, new String[]{linkTipoLinha1}));
        }
        if (hasTipoLinha1 && !StringUtil.in(fTipoLinha1, new String[]{"U", "R", "O"})) {
            result.add(new DeclValidationMessage("G165", linkTipoLinha1, new String[]{linkTipoLinha1}));
        }
        linhaIncompleta = (hasTipoLinha1 || hasFreguesiaLinha1 || hasFraccaoLinha1) && !hasArtigoLinha1;
        boolean bl4 = predioOmisso = hasTipoLinha1 && fTipoLinha1.equals("O");
        if (!predioOmisso && linhaIncompleta) {
            result.add(new DeclValidationMessage("G166", linkArtigoLinha1, new String[]{linkArtigoLinha1}));
        }
        this.validateG340(result, quadro05AnexoGModel);
        this.validateG338(result, quadro05AnexoGModel);
        this.validateG339(result, quadro05AnexoGModel);
        if (hasQuotaLinha1 && fQuotaLinha1 > 10000) {
            result.add(new DeclValidationMessage("G167", linkQuotaLinha1, new String[]{linkQuotaLinha1}));
        }
        int linha12 = 0;
        this.validateG169(result, linkTitularLinha2, fTitularLinha2);
        this.validateG170(result, rostoModel, fTitularLinha2, linkTitularLinha2);
        boolean bl5 = linhaIncompleta = (hasTitularLinha2 || hasTipoLinha2 || hasArtigoLinha2 || hasFraccaoLinha2 || hasQuotaLinha2) && !hasFreguesiaLinha2;
        if (linhaIncompleta) {
            result.add(new DeclValidationMessage("G173", linkFreguesiaLinha2, new String[]{linkFreguesiaLinha2}));
        }
        freguesiasCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
        if (hasFreguesiaLinha2 && !freguesiasCatalog.containsKey(fFreguesiaLinha2)) {
            result.add(new DeclValidationMessage("G174", linkFreguesiaLinha2, new String[]{linkFreguesiaLinha2}));
        }
        if (!(!hasFreguesiaLinha2 || hasF527 || hasF528 || hasF529 || hasF530 || hasF531)) {
            result.add(new DeclValidationMessage("G175", linkFreguesiaLinha2, new String[]{linkFreguesiaLinha2, linkF527}));
        }
        boolean bl6 = linhaIncompleta = (hasFreguesiaLinha2 || hasArtigoLinha2 || hasFraccaoLinha2) && !hasTipoLinha2;
        if (linhaIncompleta) {
            result.add(new DeclValidationMessage("G179", linkTipoLinha2, new String[]{linkTipoLinha2}));
        }
        if (hasTipoLinha2 && !StringUtil.in(fTipoLinha2, new String[]{"U", "R", "O"})) {
            result.add(new DeclValidationMessage("G180", linkTipoLinha2, new String[]{linkTipoLinha2}));
        }
        linhaIncompleta = (hasTipoLinha2 || hasFreguesiaLinha2 || hasFraccaoLinha2) && !hasArtigoLinha2;
        boolean bl7 = predioOmisso = hasTipoLinha2 && fTipoLinha2.equals("O");
        if (!predioOmisso && linhaIncompleta) {
            result.add(new DeclValidationMessage("G181", linkArtigoLinha2, new String[]{linkArtigoLinha2}));
        }
        this.validateG341(result, quadro05AnexoGModel);
        this.validateG342(result, quadro05AnexoGModel);
        this.validateG343(result, quadro05AnexoGModel);
        if (hasQuotaLinha2 && fQuotaLinha2 > 10000) {
            result.add(new DeclValidationMessage("G182", linkQuotaLinha2, new String[]{linkQuotaLinha2}));
        }
        linha12 = 0;
        Iterator<AnexoGq04T1_Linha> i = quadro04AnexoGModel.getAnexoGq04T1().iterator();
        while (i.hasNext()) {
            String linkQ4Freguesia = "aAnexoG.qQuadro04.tanexoGq04T1.l" + (linha12 + 1) + ".c11";
            line = i.next();
            if (line.getFreguesia() != null || line.getTipoPredio() != null || line.getArtigo() != null || line.getFraccao() != null || line.getQuotaParte() != null) {
                boolean equalsLinha2;
                String fregLinha = line.getFreguesia() != null ? line.getFreguesia() : "";
                String tipoLinha = line.getTipoPredio() != null ? line.getTipoPredio() : "";
                long artgLinha = line.getArtigo() != null ? line.getArtigo() : 0;
                String fracLinha = line.getFraccao() != null ? line.getFraccao() : "";
                long qParteLinha = line.getQuotaParte() != null ? line.getQuotaParte() : 0;
                boolean equalsLinha1 = this.compareStringFields(fFreguesiaLinha1, fregLinha) && this.compareStringFields(fTipoLinha1, tipoLinha) && this.compareStringFields(fArtigoLinha1.toString(), Long.valueOf(artgLinha).toString()) && this.compareStringFields(fFraccaoLinha1, fracLinha) && this.compareStringFields(fQuotaLinha1.toString(), Long.valueOf(qParteLinha).toString());
                boolean bl8 = equalsLinha2 = this.compareStringFields(fFreguesiaLinha2, fregLinha) && this.compareStringFields(fTipoLinha2, tipoLinha) && this.compareStringFields(fArtigoLinha2.toString(), Long.valueOf(artgLinha).toString()) && this.compareStringFields(fFraccaoLinha2, fracLinha) && this.compareStringFields(fQuotaLinha2.toString(), Long.valueOf(qParteLinha).toString());
                if (equalsLinha1 || equalsLinha2) {
                    result.add(new DeclValidationMessage("G183", linkQuotaLinha2, new String[]{linkQ4Freguesia, linkFreguesiaLinha2}));
                }
            }
            ++linha12;
        }
        return result;
    }

    private boolean compareStringFields(String s1, String s2) {
        boolean s1Empty = StringUtil.isEmpty(s1);
        boolean s2Empty = StringUtil.isEmpty(s2);
        if (s1Empty && s2Empty) {
            return true;
        }
        if (s1Empty && !s2Empty || !s1Empty && s2Empty) {
            return false;
        }
        return s1.equals(s2);
    }

    protected void validateG149(ValidationResult result, Quadro05 quadro05AnexoGModel, String linkPais) {
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxG.class.getSimpleName());
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05AC13()) || paisesCatalog.containsKey(quadro05AnexoGModel.getAnexoGq05AC13()))) {
            result.add(new DeclValidationMessage("G149", linkPais, new String[]{linkPais}));
        }
    }

    protected void validateG366(ValidationResult result, Quadro04 quadro04AnexoGModel, Quadro05 quadro05AnexoGModel) {
        boolean hasF524;
        boolean hasF502 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C502());
        boolean hasF503 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C503());
        boolean hasF504 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C504());
        boolean hasF522 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C522());
        boolean hasF523 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C523());
        boolean bl = hasF524 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C524());
        if (hasF502 || hasF503 || hasF504 || hasF522 || hasF523 || hasF524) {
            for (int linha = 0; linha < quadro04AnexoGModel.getAnexoGq04T1().size(); ++linha) {
                AnexoGq04T1_Linha lineContent = quadro04AnexoGModel.getAnexoGq04T1().get(linha);
                if ((!hasF502 || lineContent.getNLinha() == null || !lineContent.getNLinha().equals(quadro05AnexoGModel.getAnexoGq05C502()) || !"O".equals(lineContent.getTipoPredio())) && (!hasF503 || lineContent.getNLinha() == null || !lineContent.getNLinha().equals(quadro05AnexoGModel.getAnexoGq05C503()) || !"O".equals(lineContent.getTipoPredio())) && (!hasF504 || lineContent.getNLinha() == null || !lineContent.getNLinha().equals(quadro05AnexoGModel.getAnexoGq05C504()) || !"O".equals(lineContent.getTipoPredio())) && (!hasF522 || lineContent.getNLinha() == null || !lineContent.getNLinha().equals(quadro05AnexoGModel.getAnexoGq05C522()) || !"O".equals(lineContent.getTipoPredio())) && (!hasF523 || lineContent.getNLinha() == null || !lineContent.getNLinha().equals(quadro05AnexoGModel.getAnexoGq05C523()) || !"O".equals(lineContent.getTipoPredio())) && (!hasF524 || lineContent.getNLinha() == null || !lineContent.getNLinha().equals(quadro05AnexoGModel.getAnexoGq05C524()) || !"O".equals(lineContent.getTipoPredio()))) continue;
                String linkTipoPredio = "aAnexoG.qQuadro04.tanexoGq04T1.l" + (linha + 1) + ".c" + AnexoGq04T1_LinhaBase.Property.TIPOPREDIO.getIndex();
                result.add(new DeclValidationMessage("G366", "aAnexoG.qQuadro05", new String[]{"aAnexoG.qQuadro05", linkTipoPredio}));
            }
        }
    }

    protected void validateG152(ValidationResult result, String linkTitularLinha1, String titular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G152", linkTitularLinha1, new String[]{linkTitularLinha1, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateG169(ValidationResult result, String linkTitularLinha2, String titular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G169", linkTitularLinha2, new String[]{linkTitularLinha2, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateG153(ValidationResult result, RostoModel rostoModel, String titular, String linkTitularLinha1) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G153", linkTitularLinha1, new String[]{linkTitularLinha1, "aRosto.qQuadro03"}));
        }
    }

    protected void validateG170(ValidationResult result, RostoModel rostoModel, String titular, String linkTitularLinha2) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G170", linkTitularLinha2, new String[]{linkTitularLinha2, "aRosto.qQuadro03"}));
        }
    }

    public void validateG344(ValidationResult result, Quadro05 quadro05) {
        long c528;
        boolean has507 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C507());
        boolean has508 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C508());
        boolean has527 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C527());
        boolean has528 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C528());
        long c507 = quadro05.getAnexoGq05C507() != null ? quadro05.getAnexoGq05C507() : 0;
        long c508 = quadro05.getAnexoGq05C508() != null ? quadro05.getAnexoGq05C508() : 0;
        long c527 = quadro05.getAnexoGq05C527() != null ? quadro05.getAnexoGq05C527() : 0;
        long l = c528 = quadro05.getAnexoGq05C528() != null ? quadro05.getAnexoGq05C528() : 0;
        if (has507 && has508 && c507 > 0 && c508 > 0 || has527 && has528 && c527 > 0 && c528 > 0) {
            result.add(new DeclValidationMessage("G344", "aAnexoG.qQuadro05.fanexoGq05AC1", new String[]{"aAnexoG.qQuadro05.fanexoGq05C507", "aAnexoG.qQuadro05.fanexoGq05C527", "aAnexoG.qQuadro05.fanexoGq05C508", "aAnexoG.qQuadro05.fanexoGq05C528"}));
        }
    }

    public void validateG340(ValidationResult result, Quadro05 quadro05) {
        if (!(quadro05.getAnexoGq05AC4() == null || !StringUtil.isEmpty(quadro05.getAnexoGq05AC3()) && (quadro05.getAnexoGq05AC3().equals("U") || quadro05.getAnexoGq05AC3().equals("R")))) {
            result.add(new DeclValidationMessage("G340", "aAnexoG.qQuadro05.fanexoGq05AC4", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC4", "aAnexoG.qQuadro05.fanexoGq05AC3"}));
        }
    }

    public void validateG338(ValidationResult result, Quadro05 quadro05) {
        String[] partes;
        for (String parte : partes = quadro05.getAnexoGq05AC5() != null ? quadro05.getAnexoGq05AC5().split(" ") : new String[]{}) {
            if (StringUtil.isAlphaNumeric(parte)) continue;
            result.add(new DeclValidationMessage("G338", "aAnexoG.qQuadro05.fanexoGq05AC5", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC5"}));
            break;
        }
    }

    public void validateG339(ValidationResult result, Quadro05 quadro05) {
        if (!(quadro05.getAnexoGq05AC5() == null || quadro05 != null && (quadro05.getAnexoGq05AC3() == null || Modelo3IRSValidatorUtil.in(quadro05.getAnexoGq05AC3(), new String[]{"R", "U"})))) {
            result.add(new DeclValidationMessage("G339", "aAnexoG.qQuadro05.fanexoGq05AC5", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC5", "aAnexoG.qQuadro05.fanexoGq05AC3"}));
        }
    }

    public void validateG341(ValidationResult result, Quadro05 quadro05) {
        if (!(quadro05.getAnexoGq05AC10() == null || !StringUtil.isEmpty(quadro05.getAnexoGq05AC9()) && (quadro05.getAnexoGq05AC9().equals("U") || quadro05.getAnexoGq05AC9().equals("R")))) {
            result.add(new DeclValidationMessage("G341", "aAnexoG.qQuadro05.fanexoGq05AC10", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC10", "aAnexoG.qQuadro05.fanexoGq05AC9"}));
        }
    }

    public void validateG342(ValidationResult result, Quadro05 quadro05) {
        String[] partes;
        for (String parte : partes = quadro05.getAnexoGq05AC11() != null ? quadro05.getAnexoGq05AC11().split(" ") : new String[]{}) {
            if (StringUtil.isAlphaNumeric(parte)) continue;
            result.add(new DeclValidationMessage("G342", "aAnexoG.qQuadro05.fanexoGq05AC11", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC11"}));
            break;
        }
    }

    public void validateG343(ValidationResult result, Quadro05 quadro05) {
        if (!(quadro05.getAnexoGq05AC11() == null || quadro05 != null && (quadro05.getAnexoGq05AC9() == null || Modelo3IRSValidatorUtil.in(quadro05.getAnexoGq05AC9(), new String[]{"R", "U"})))) {
            result.add(new DeclValidationMessage("G343", "aAnexoG.qQuadro05.fanexoGq05AC11", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC11", "aAnexoG.qQuadro05.fanexoGq05AC9"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        Quadro05 quadro05AnexoGModel = anexoGModel.getQuadro05();
        this.validateG134(result, anexoGModel, quadro05AnexoGModel);
        this.validateG336(result, quadro05AnexoGModel);
        this.validateG154(result, quadro05AnexoGModel);
        this.validateG160(result, quadro05AnexoGModel);
        this.validateG171(result, quadro05AnexoGModel);
        this.validateG176(result, quadro05AnexoGModel);
        return result;
    }

    protected void validateG176(ValidationResult result, Quadro05 quadro05AnexoGModel) {
        boolean hasFreguesiaLinha2 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC8());
        Long f527 = quadro05AnexoGModel.getAnexoGq05C527() != null ? quadro05AnexoGModel.getAnexoGq05C527() : 0;
        Long f528 = quadro05AnexoGModel.getAnexoGq05C528() != null ? quadro05AnexoGModel.getAnexoGq05C528() : 0;
        Long f529 = quadro05AnexoGModel.getAnexoGq05C529() != null ? quadro05AnexoGModel.getAnexoGq05C529() : 0;
        Long f530 = quadro05AnexoGModel.getAnexoGq05C530() != null ? quadro05AnexoGModel.getAnexoGq05C530() : 0;
        Long f531 = quadro05AnexoGModel.getAnexoGq05C531() != null ? quadro05AnexoGModel.getAnexoGq05C531() : 0;
        boolean hasF527 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C527());
        boolean hasF528 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C528());
        boolean hasF529 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C529());
        boolean hasF530 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C530());
        boolean hasF531 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C531());
        String linkFreguesiaLinha2 = "aAnexoG.qQuadro05.fanexoGq05AC8";
        if (!hasFreguesiaLinha2 && (hasF527 && f527 > 0 || hasF528 && f528 > 0 || hasF529 && f529 > 0 || hasF530 && f530 > 0 || hasF531 && f531 > 0) && quadro05AnexoGModel.getAnexoGq05AC13() == null) {
            result.add(new DeclValidationMessage("G176", linkFreguesiaLinha2, new String[]{linkFreguesiaLinha2}));
        }
    }

    protected void validateG171(ValidationResult result, Quadro05 quadro05AnexoGModel) {
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC7());
        boolean hasFreguesiaLinha2 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC8());
        String linkTitularLinha2 = "aAnexoG.qQuadro05.fanexoGq05AC7";
        if (!hasTitularLinha2 && hasFreguesiaLinha2) {
            result.add(new DeclValidationMessage("G171", linkTitularLinha2, new String[]{linkTitularLinha2}));
        }
    }

    protected void validateG154(ValidationResult result, Quadro05 quadro05AnexoGModel) {
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC1());
        boolean hasFreguesiaLinha1 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC2());
        String linkTitularLinha1 = "aAnexoG.qQuadro05.fanexoGq05AC1";
        if (!hasTitularLinha1 && hasFreguesiaLinha1) {
            result.add(new DeclValidationMessage("G154", linkTitularLinha1, new String[]{linkTitularLinha1}));
        }
    }

    protected void validateG134(ValidationResult result, AnexoGModel anexoGModel, Quadro05 quadro05AnexoGModel) {
        boolean hasF507 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C507());
        boolean hasF508 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C508());
        boolean hasF509 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C509());
        boolean hasF510 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C510());
        boolean hasF511 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C511());
        Long f507 = quadro05AnexoGModel.getAnexoGq05C507() != null ? quadro05AnexoGModel.getAnexoGq05C507() : 0;
        Long f508 = quadro05AnexoGModel.getAnexoGq05C508() != null ? quadro05AnexoGModel.getAnexoGq05C508() : 0;
        Long f509 = quadro05AnexoGModel.getAnexoGq05C509() != null ? quadro05AnexoGModel.getAnexoGq05C509() : 0;
        Long f510 = quadro05AnexoGModel.getAnexoGq05C510() != null ? quadro05AnexoGModel.getAnexoGq05C510() : 0;
        Long f511 = quadro05AnexoGModel.getAnexoGq05C511() != null ? quadro05AnexoGModel.getAnexoGq05C511() : 0;
        if ((hasF507 && f507 > 0 || hasF508 && f508 > 0 || hasF509 && f509 > 0 || hasF510 && f510 > 0 || hasF511 && f511 > 0) && anexoGModel.getQuadro05().getAnexoGq05AC2() == null && anexoGModel.getQuadro05().getAnexoGq05AC13() == null) {
            result.add(new DeclValidationMessage("G134", "aAnexoG.qQuadro05.fanexoGq05AC2", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC2"}));
        }
    }

    public void validateG336(ValidationResult result, Quadro05 quadro05) {
        boolean has531;
        boolean has527 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C527());
        boolean has528 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C528());
        boolean has529 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C529());
        boolean has530 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C530());
        boolean bl = has531 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoGq05C531());
        if ((has527 && quadro05.getAnexoGq05C527() > 0 || has528 && quadro05.getAnexoGq05C528() > 0 || has529 && quadro05.getAnexoGq05C529() > 0 || has530 && quadro05.getAnexoGq05C530() > 0 || has531 && quadro05.getAnexoGq05C531() > 0) && quadro05.getAnexoGq05AC8() == null && quadro05.getAnexoGq05AC13() == null) {
            result.add(new DeclValidationMessage("G336", "aAnexoG.qQuadro05.fanexoGq05AC8", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC8"}));
        }
    }

    protected void validateG160(ValidationResult result, Quadro05 quadro05AnexoGModel) {
        boolean hasFreguesiaLinha1 = !StringUtil.isEmpty(quadro05AnexoGModel.getAnexoGq05AC2());
        boolean hasF507 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C507());
        boolean hasF508 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C508());
        boolean hasF509 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C509());
        boolean hasF510 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C510());
        boolean hasF511 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05AnexoGModel.getAnexoGq05C511());
        Long f507 = quadro05AnexoGModel.getAnexoGq05C507() != null ? quadro05AnexoGModel.getAnexoGq05C507() : 0;
        Long f508 = quadro05AnexoGModel.getAnexoGq05C508() != null ? quadro05AnexoGModel.getAnexoGq05C508() : 0;
        Long f509 = quadro05AnexoGModel.getAnexoGq05C509() != null ? quadro05AnexoGModel.getAnexoGq05C509() : 0;
        Long f510 = quadro05AnexoGModel.getAnexoGq05C510() != null ? quadro05AnexoGModel.getAnexoGq05C510() : 0;
        Long f511 = quadro05AnexoGModel.getAnexoGq05C511() != null ? quadro05AnexoGModel.getAnexoGq05C511() : 0;
        if (!hasFreguesiaLinha1 && (hasF507 && f507 > 0 || hasF508 && f508 > 0 || hasF509 && f509 > 0 || hasF510 && f510 > 0 || hasF511 && f511 > 0) && quadro05AnexoGModel.getAnexoGq05AC13() == null) {
            result.add(new DeclValidationMessage("G160", "aAnexoG.qQuadro05.fanexoGq05AC2", new String[]{"aAnexoG.qQuadro05.fanexoGq05AC2"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

