/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexog;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro09Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro09Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro09 quadro09AnexoGModel = anexoGModel.getQuadro09();
        String fTitularLinha1 = quadro09AnexoGModel.getAnexoGq09C901a() != null ? quadro09AnexoGModel.getAnexoGq09C901a() : "";
        String fTitularLinha2 = quadro09AnexoGModel.getAnexoGq09C902a() != null ? quadro09AnexoGModel.getAnexoGq09C902a() : "";
        String fTitularLinha3 = quadro09AnexoGModel.getAnexoGq09C903a() != null ? quadro09AnexoGModel.getAnexoGq09C903a() : "";
        String fTitularLinha4 = quadro09AnexoGModel.getAnexoGq09C904a() != null ? quadro09AnexoGModel.getAnexoGq09C904a() : "";
        String fTitularLinha5 = quadro09AnexoGModel.getAnexoGq09C905a() != null ? quadro09AnexoGModel.getAnexoGq09C905a() : "";
        Long fRendimentoLinha1 = quadro09AnexoGModel.getAnexoGq09C901b() != null ? quadro09AnexoGModel.getAnexoGq09C901b() : 0;
        Long fRendimentoLinha2 = quadro09AnexoGModel.getAnexoGq09C902b() != null ? quadro09AnexoGModel.getAnexoGq09C902b() : 0;
        Long fRendimentoLinha3 = quadro09AnexoGModel.getAnexoGq09C903b() != null ? quadro09AnexoGModel.getAnexoGq09C903b() : 0;
        Long fRendimentoLinha4 = quadro09AnexoGModel.getAnexoGq09C904b() != null ? quadro09AnexoGModel.getAnexoGq09C904b() : 0;
        Long fRendimentoLinha5 = quadro09AnexoGModel.getAnexoGq09C905b() != null ? quadro09AnexoGModel.getAnexoGq09C905b() : 0;
        String linkTitularLinha1 = "aAnexoG.qQuadro09.fanexoGq09C901a";
        String linkTitularLinha2 = "aAnexoG.qQuadro09.fanexoGq09C902a";
        String linkTitularLinha3 = "aAnexoG.qQuadro09.fanexoGq09C903a";
        String linkTitularLinha4 = "aAnexoG.qQuadro09.fanexoGq09C904a";
        String linkTitularLinha5 = "aAnexoG.qQuadro09.fanexoGq09C905a";
        String linkRendimentoLinha1 = "aAnexoG.qQuadro09.fanexoGq09C901b";
        String linkRendimentoLinha2 = "aAnexoG.qQuadro09.fanexoGq09C902b";
        String linkRendimentoLinha3 = "aAnexoG.qQuadro09.fanexoGq09C903b";
        String linkRendimentoLinha4 = "aAnexoG.qQuadro09.fanexoGq09C904b";
        String linkRendimentoLinha5 = "aAnexoG.qQuadro09.fanexoGq09C905b";
        String linkSomaControlo = "aAnexoG.qQuadro09.fanexoGq09C1b";
        String linkOpcao = "aAnexoG.qQuadro09.fanexoGq09B1OP1";
        boolean hasTitularLinha1 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C901a());
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C902a());
        boolean hasTitularLinha3 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C903a());
        boolean hasTitularLinha4 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C904a());
        boolean hasTitularLinha5 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C905a());
        boolean hasRendimentoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha1);
        boolean hasRendimentoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha2);
        boolean hasRendimentoLinha3 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha3);
        boolean hasRendimentoLinha4 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha4);
        boolean hasRendimentoLinha5 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha5);
        boolean hasSomaControlo = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro09AnexoGModel.getAnexoGq09C1b());
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        boolean hasAnoExercicio = !Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio);
        result.addAllFrom(this.validaPrimeiraLinha(hasTitularLinha1, hasRendimentoLinha1, rostoModel, fTitularLinha1, linkTitularLinha1, linkRendimentoLinha1));
        result.addAllFrom(this.validaSegundaLinha(hasTitularLinha2, hasRendimentoLinha2, rostoModel, fTitularLinha2, linkTitularLinha2, linkRendimentoLinha2));
        result.addAllFrom(this.validaTerceiraLinha(hasTitularLinha3, hasRendimentoLinha3, rostoModel, fTitularLinha3, linkTitularLinha3, linkRendimentoLinha3));
        if (hasAnoExercicio && (anoExercicio == 2001 || anoExercicio == 2002) && hasRendimentoLinha3) {
            result.add(new DeclValidationMessage("G237", linkRendimentoLinha3, new String[]{"aRosto.qQuadro02.fq02C02", linkRendimentoLinha3, linkRendimentoLinha1}));
        }
        result.addAllFrom(this.validaQuartaLinha(hasTitularLinha4, hasRendimentoLinha4, rostoModel, fTitularLinha4, linkTitularLinha4, linkRendimentoLinha4));
        if (hasAnoExercicio && anoExercicio != 2002 && hasRendimentoLinha4) {
            result.add(new DeclValidationMessage("G244", linkRendimentoLinha4, new String[]{linkTitularLinha4, "aRosto.qQuadro02.fq02C02"}));
        }
        result.addAllFrom(this.validaQuintaLinha(hasTitularLinha5, hasRendimentoLinha5, rostoModel, fTitularLinha5, linkTitularLinha5, linkRendimentoLinha5));
        if (hasAnoExercicio && anoExercicio != 2001 && hasRendimentoLinha5) {
            result.add(new DeclValidationMessage("G251", linkRendimentoLinha5, new String[]{linkTitularLinha5, "aRosto.qQuadro02.fq02C02"}));
        }
        long soma = 0;
        long somaControlo = 0;
        if (hasRendimentoLinha1) {
            soma = fRendimentoLinha1;
        }
        if (hasRendimentoLinha2) {
            soma+=fRendimentoLinha2.longValue();
        }
        if (hasRendimentoLinha3) {
            soma+=fRendimentoLinha3.longValue();
        }
        if (hasRendimentoLinha4) {
            soma+=fRendimentoLinha4.longValue();
        }
        if (hasRendimentoLinha5) {
            soma+=fRendimentoLinha5.longValue();
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro09AnexoGModel.getAnexoGq09C1b())) {
            somaControlo = quadro09AnexoGModel.getAnexoGq09C1b();
        }
        if (soma != somaControlo) {
            result.add(new DeclValidationMessage("G252", linkSomaControlo, new String[]{linkSomaControlo}));
        }
        boolean hasValor = false;
        Quadro08 quadro08AnexoGModel = anexoGModel.getQuadro08();
        for (AnexoGq08T1_Linha linha : quadro08AnexoGModel.getAnexoGq08T1()) {
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorRealizacao())) {
                hasValor = true;
                break;
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorAquisicao())) {
                hasValor = true;
                break;
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDespesas())) continue;
            hasValor = true;
            break;
        }
        if (!hasValor) {
            hasValor = hasSomaControlo;
        }
        if (!(hasValor || StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09B1()) || !quadro09AnexoGModel.getAnexoGq09B1().equals("1"))) {
            result.add(new DeclValidationMessage("G253", linkOpcao, new String[]{linkOpcao, "aAnexoG.qQuadro08.tanexoGq08T1", linkSomaControlo}));
        } else if (hasValor && StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09B1())) {
            result.add(new DeclValidationMessage("G254", linkOpcao, new String[]{linkOpcao}));
        }
        if (!(quadro09AnexoGModel.getAnexoGq09B1() == null || StringUtil.in(quadro09AnexoGModel.getAnexoGq09B1(), new String[]{"1", "2"}))) {
            result.add(new DeclValidationMessage("G319", linkOpcao, new String[]{linkOpcao}));
        }
        return result;
    }

    private ValidationResult validaPrimeiraLinha(boolean hasTitular, boolean hasRendimento, RostoModel rostoModel, String fTitular, String linkTitular, String linkRendimento) {
        ValidationResult result = new ValidationResult();
        boolean isTitularValido = true;
        boolean isTitularNIFPreenchidoRosto = true;
        this.validateG220(result, fTitular, linkTitular);
        this.validateG221(result, rostoModel, fTitular, linkTitular);
        this.validateG222(hasTitular, hasRendimento, rostoModel, fTitular, linkTitular, linkRendimento, result, isTitularValido, isTitularNIFPreenchidoRosto);
        return result;
    }

    protected void validateG222(boolean hasTitular, boolean hasRendimento, RostoModel rostoModel, String fTitular, String linkTitular, String linkRendimento, ValidationResult result, boolean isTitularValido, boolean isTitularNIFPreenchidoRosto) {
        if (!(StringUtil.isEmpty(fTitular) || Quadro03.isSujeitoPassivoA(fTitular) || Quadro03.isSujeitoPassivoB(fTitular) || Quadro03.isSujeitoPassivoC(fTitular) || Quadro03.isDependenteNaoDeficiente(fTitular) || Quadro03.isDependenteDeficiente(fTitular) || Quadro07.isConjugeFalecido(fTitular) || Quadro03.isDependenteEmGuardaConjunta(fTitular))) {
            isTitularValido = false;
        }
        if (!(StringUtil.isEmpty(fTitular) || rostoModel.isNIFTitularPreenchido(fTitular))) {
            isTitularNIFPreenchidoRosto = false;
        }
        if (hasTitular && isTitularValido && isTitularNIFPreenchidoRosto && !hasRendimento) {
            result.add(new DeclValidationMessage("G222", linkTitular, new String[]{linkRendimento, linkTitular}));
        }
    }

    protected void validateG220(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G220", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateG221(ValidationResult result, RostoModel rostoModel, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || rostoModel.isNIFTitularPreenchido(titular))) {
            result.add(new DeclValidationMessage("G221", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03"}));
        }
    }

    private ValidationResult validaSegundaLinha(boolean hasTitular, boolean hasRendimento, RostoModel rostoModel, String fTitular, String linkTitular, String linkRendimento) {
        ValidationResult result = new ValidationResult();
        boolean isTitularValido = true;
        boolean isTitularNIFPreenchidoRosto = true;
        this.validateG225(result, fTitular, linkTitular);
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G226", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G227", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (!(StringUtil.isEmpty(fTitular) || Quadro03.isSujeitoPassivoA(fTitular) || Quadro03.isSujeitoPassivoB(fTitular) || Quadro03.isSujeitoPassivoC(fTitular) || Quadro03.isDependenteNaoDeficiente(fTitular) || Quadro03.isDependenteDeficiente(fTitular) || Quadro07.isConjugeFalecido(fTitular) || Quadro03.isDependenteEmGuardaConjunta(fTitular))) {
            isTitularValido = false;
        }
        if (hasTitular && isTitularValido && isTitularNIFPreenchidoRosto && !hasRendimento) {
            result.add(new DeclValidationMessage("G228", linkTitular, new String[]{linkRendimento, linkTitular}));
        }
        return result;
    }

    protected void validateG225(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G225", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    private ValidationResult validaTerceiraLinha(boolean hasTitular, boolean hasRendimento, RostoModel rostoModel, String fTitular, String linkTitular, String linkRendimento) {
        ValidationResult result = new ValidationResult();
        boolean isTitularValido = true;
        boolean isTitularNIFPreenchidoRosto = true;
        this.validateG231(result, fTitular, linkTitular);
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G232", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G233", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (!(StringUtil.isEmpty(fTitular) || Quadro03.isSujeitoPassivoA(fTitular) || Quadro03.isSujeitoPassivoB(fTitular) || Quadro03.isSujeitoPassivoC(fTitular) || Quadro03.isDependenteNaoDeficiente(fTitular) || Quadro03.isDependenteDeficiente(fTitular) || Quadro07.isConjugeFalecido(fTitular) || Quadro03.isDependenteEmGuardaConjunta(fTitular))) {
            isTitularValido = false;
        }
        if (hasTitular && isTitularValido && isTitularNIFPreenchidoRosto && !hasRendimento) {
            result.add(new DeclValidationMessage("G234", linkTitular, new String[]{linkRendimento, linkTitular}));
        }
        return result;
    }

    protected void validateG231(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G231", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    private ValidationResult validaQuartaLinha(boolean hasTitular, boolean hasRendimento, RostoModel rostoModel, String fTitular, String linkTitular, String linkRendimento) {
        ValidationResult result = new ValidationResult();
        boolean isTitularValido = true;
        boolean isTitularNIFPreenchidoRosto = true;
        this.validateG238(result, fTitular, linkTitular);
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G239", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G240", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (!(StringUtil.isEmpty(fTitular) || Quadro03.isSujeitoPassivoA(fTitular) || Quadro03.isSujeitoPassivoB(fTitular) || Quadro03.isSujeitoPassivoC(fTitular) || Quadro03.isDependenteNaoDeficiente(fTitular) || Quadro03.isDependenteDeficiente(fTitular) || Quadro07.isConjugeFalecido(fTitular) || Quadro03.isDependenteEmGuardaConjunta(fTitular))) {
            isTitularValido = false;
        }
        if (hasTitular && isTitularValido && isTitularNIFPreenchidoRosto && !hasRendimento) {
            result.add(new DeclValidationMessage("G241", linkTitular, new String[]{linkRendimento, linkTitular}));
        }
        return result;
    }

    protected void validateG238(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G238", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    private ValidationResult validaQuintaLinha(boolean hasTitular, boolean hasRendimento, RostoModel rostoModel, String fTitular, String linkTitular, String linkRendimento) {
        ValidationResult result = new ValidationResult();
        boolean isTitularValido = true;
        boolean isTitularNIFPreenchidoRosto = true;
        this.validateG245(result, fTitular, linkTitular);
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G246", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (hasTitular && !rostoModel.isNIFTitularPreenchido(fTitular)) {
            result.add(new DeclValidationMessage("G247", linkTitular, new String[]{linkTitular}));
            isTitularNIFPreenchidoRosto = false;
        }
        if (!(StringUtil.isEmpty(fTitular) || Quadro03.isSujeitoPassivoA(fTitular) || Quadro03.isSujeitoPassivoB(fTitular) || Quadro03.isSujeitoPassivoC(fTitular) || Quadro03.isDependenteNaoDeficiente(fTitular) || Quadro03.isDependenteDeficiente(fTitular) || Quadro07.isConjugeFalecido(fTitular) || Quadro03.isDependenteEmGuardaConjunta(fTitular))) {
            isTitularValido = false;
        }
        if (hasTitular && isTitularValido && isTitularNIFPreenchidoRosto && !hasRendimento) {
            result.add(new DeclValidationMessage("G248", linkTitular, new String[]{linkRendimento, linkTitular}));
        }
        return result;
    }

    protected void validateG245(ValidationResult result, String titular, String linkTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isSujeitoPassivoC(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("G245", linkTitular, new String[]{linkTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoGModel anexoGModel = (AnexoGModel)model.getAnexo(AnexoGModel.class);
        if (anexoGModel == null) {
            return result;
        }
        this.validateG223(result, anexoGModel);
        this.validateG229(result, anexoGModel);
        this.validateG235(result, anexoGModel);
        this.validateG242(result, anexoGModel);
        this.validateG249(result, anexoGModel);
        return result;
    }

    private void validateG223(ValidationResult result, AnexoGModel anexoGModel) {
        boolean hasTitularLinha1;
        Quadro09 quadro09AnexoGModel = anexoGModel.getQuadro09();
        Long fRendimentoLinha1 = quadro09AnexoGModel.getAnexoGq09C901b() != null ? quadro09AnexoGModel.getAnexoGq09C901b() : 0;
        boolean hasRendimentoLinha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha1);
        String linkTitularLinha1 = "aAnexoG.qQuadro09.fanexoGq09C901a";
        String linkRendimentoLinha1 = "aAnexoG.qQuadro09.fanexoGq09C901b";
        boolean bl = hasTitularLinha1 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C901a());
        if (hasRendimentoLinha1 && !hasTitularLinha1) {
            result.add(new DeclValidationMessage("G223", linkRendimentoLinha1, new String[]{linkTitularLinha1, linkRendimentoLinha1}));
        }
    }

    private void validateG229(ValidationResult result, AnexoGModel anexoGModel) {
        boolean hasRendimentoLinha2;
        Quadro09 quadro09AnexoGModel = anexoGModel.getQuadro09();
        Long fRendimentoLinha2 = quadro09AnexoGModel.getAnexoGq09C902b() != null ? quadro09AnexoGModel.getAnexoGq09C902b() : 0;
        String linkTitularLinha2 = "aAnexoG.qQuadro09.fanexoGq09C902a";
        String linkRendimentoLinha2 = "aAnexoG.qQuadro09.fanexoGq09C902b";
        boolean hasTitularLinha2 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C902a());
        boolean bl = hasRendimentoLinha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha2);
        if (hasRendimentoLinha2 && !hasTitularLinha2) {
            result.add(new DeclValidationMessage("G229", linkRendimentoLinha2, new String[]{linkTitularLinha2, linkRendimentoLinha2}));
        }
    }

    private void validateG235(ValidationResult result, AnexoGModel anexoGModel) {
        boolean hasTitularLinha3;
        Quadro09 quadro09AnexoGModel = anexoGModel.getQuadro09();
        Long fRendimentoLinha3 = quadro09AnexoGModel.getAnexoGq09C903b() != null ? quadro09AnexoGModel.getAnexoGq09C903b() : 0;
        String linkTitularLinha3 = "aAnexoG.qQuadro09.fanexoGq09C903a";
        String linkRendimentoLinha3 = "aAnexoG.qQuadro09.fanexoGq09C903b";
        boolean hasRendimentoLinha3 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha3);
        boolean bl = hasTitularLinha3 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C903a());
        if (hasRendimentoLinha3 && !hasTitularLinha3) {
            result.add(new DeclValidationMessage("G235", linkRendimentoLinha3, new String[]{linkTitularLinha3, linkRendimentoLinha3}));
        }
    }

    private void validateG242(ValidationResult result, AnexoGModel anexoGModel) {
        boolean hasRendimentoLinha4;
        Quadro09 quadro09AnexoGModel = anexoGModel.getQuadro09();
        Long fRendimentoLinha4 = quadro09AnexoGModel.getAnexoGq09C904b() != null ? quadro09AnexoGModel.getAnexoGq09C904b() : 0;
        String linkTitularLinha4 = "aAnexoG.qQuadro09.fanexoGq09C904a";
        boolean hasTitularLinha4 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C904a());
        String linkRendimentoLinha4 = "aAnexoG.qQuadro09.fanexoGq09C904b";
        boolean bl = hasRendimentoLinha4 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha4);
        if (hasRendimentoLinha4 && !hasTitularLinha4) {
            result.add(new DeclValidationMessage("G242", linkRendimentoLinha4, new String[]{linkTitularLinha4, linkRendimentoLinha4}));
        }
    }

    private void validateG249(ValidationResult result, AnexoGModel anexoGModel) {
        boolean hasRendimentoLinha5;
        Quadro09 quadro09AnexoGModel = anexoGModel.getQuadro09();
        Long fRendimentoLinha5 = quadro09AnexoGModel.getAnexoGq09C905b() != null ? quadro09AnexoGModel.getAnexoGq09C905b() : 0;
        String linkTitularLinha5 = "aAnexoG.qQuadro09.fanexoGq09C905a";
        String linkRendimentoLinha5 = "aAnexoG.qQuadro09.fanexoGq09C905b";
        boolean hasTitularLinha5 = !StringUtil.isEmpty(quadro09AnexoGModel.getAnexoGq09C905a());
        boolean bl = hasRendimentoLinha5 = !Modelo3IRSValidatorUtil.isEmptyOrZero(fRendimentoLinha5);
        if (hasRendimentoLinha5 && !hasTitularLinha5) {
            result.add(new DeclValidationMessage("G249", linkRendimentoLinha5, new String[]{linkTitularLinha5, linkRendimentoLinha5}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

