/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Freguesia;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final String B175 = "B175";
    private static final String ANEXO_B = "AnexoB";
    public static final String B133 = "B133";
    public static final String B134 = "B134";
    public static final String B170 = "B170";

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateB053(ValidationResult result, AnexoBModel anexoBModel) {
        if (anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04B2() != null && "3".equals(anexoBModel.getQuadro04().getAnexoBq04B2()) && (Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C401()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C402()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C404()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C409()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C411()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C420())) && anexoBModel.getQuadro02().getAnexoBq02C05() != null && anexoBModel.getQuadro02().getAnexoBq02C05() < 2014) {
            result.add(new DeclValidationMessage("B053", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B2OP3"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C410")}));
        }
    }

    protected void validateB200(ValidationResult result, AnexoBModel anexoBModel) {
        if (anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04B2() != null && "3".equals(anexoBModel.getQuadro04().getAnexoBq04B2()) && (Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C401()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C402()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C404()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C409()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C411()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C420())) && anexoBModel.getQuadro02().getAnexoBq02C05() != null && anexoBModel.getQuadro02().getAnexoBq02C05() > 2013) {
            result.add(new DeclValidationMessage("B200", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B2OP3"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C410")}));
        }
    }

    protected void validateB203(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, int numLinha) {
        long soma2;
        Long C425 = anexoBModel.getQuadro04().getAnexoBq04C425() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C425();
        Long C426 = anexoBModel.getQuadro04().getAnexoBq04C426() == null ? 0 : anexoBModel.getQuadro04().getAnexoBq04C426();
        Long C3i = linha.getSubsidiosNaoDestinadosExploracaoN() == null ? 0 : linha.getSubsidiosNaoDestinadosExploracaoN();
        Long C4i = linha.getSubsidiosNaoDestinadosExploracaoN1() == null ? 0 : linha.getSubsidiosNaoDestinadosExploracaoN1();
        Long C5i = linha.getSubsidiosNaoDestinadosExploracaoN2() == null ? 0 : linha.getSubsidiosNaoDestinadosExploracaoN2();
        Long C6i = linha.getSubsidiosNaoDestinadosExploracaoN3() == null ? 0 : linha.getSubsidiosNaoDestinadosExploracaoN3();
        Long C7i = linha.getSubsidiosNaoDestinadosExploracaoN4() == null ? 0 : linha.getSubsidiosNaoDestinadosExploracaoN4();
        long soma1 = C425 + C426;
        if ((double)soma1 < 0.2 * (double)(soma2 = C3i + C4i + C5i + C6i + C7i)) {
            String lineLink = anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T3.l" + (numLinha + 1));
            String linkSubisidioNaoDestinadoExploracaoN = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON.getIndex();
            String linkSubisidioNaoDestinadoExploracaoN1 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON1.getIndex();
            String linkSubisidioNaoDestinadoExploracaoN2 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON2.getIndex();
            String linkSubisidioNaoDestinadoExploracaoN3 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON3.getIndex();
            String linkSubisidioNaoDestinadoExploracaoN4 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON4.getIndex();
            result.add(new DeclValidationMessage("B203", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C425"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C426"), linkSubisidioNaoDestinadoExploracaoN, linkSubisidioNaoDestinadoExploracaoN1, linkSubisidioNaoDestinadoExploracaoN2, linkSubisidioNaoDestinadoExploracaoN3, linkSubisidioNaoDestinadoExploracaoN4}));
        }
    }

    protected void validateB054(ValidationResult result, AnexoBModel anexoBModel) {
        if (anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04B2() != null && "3".equals(anexoBModel.getQuadro04().getAnexoBq04B2())) {
            Quadro04 quadro04Model = anexoBModel.getQuadro04();
            long numCamposPreenchidos = 0;
            numCamposPreenchidos+=quadro04Model.getAnexoBq04C403() != null && quadro04Model.getAnexoBq04C403() > 0 ? 1 : 0;
            numCamposPreenchidos+=quadro04Model.getAnexoBq04C405() != null && quadro04Model.getAnexoBq04C405() > 0 ? 1 : 0;
            Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
            if ((numCamposPreenchidos+=quadro04Model.getAnexoBq04C410() != null && quadro04Model.getAnexoBq04C410() > 0 ? 1 : 0) > 1 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B054", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C410")}));
            }
        }
    }

    protected void validateB201(ValidationResult result, AnexoBModel anexoBModel) {
        if (anexoBModel != null && anexoBModel.getQuadro04().getAnexoBq04B2() != null && "3".equals(anexoBModel.getQuadro04().getAnexoBq04B2())) {
            Quadro04 quadro04Model = anexoBModel.getQuadro04();
            long numCamposPreenchidos = 0;
            numCamposPreenchidos+=quadro04Model.getAnexoBq04C440() != null && quadro04Model.getAnexoBq04C440() > 0 ? 1 : 0;
            numCamposPreenchidos+=quadro04Model.getAnexoBq04C405() != null && quadro04Model.getAnexoBq04C405() > 0 ? 1 : 0;
            Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
            if ((numCamposPreenchidos+=quadro04Model.getAnexoBq04C444() != null && quadro04Model.getAnexoBq04C444() > 0 ? 1 : 0) > 1 && ano != null && ano > 2013) {
                result.add(new DeclValidationMessage("B201", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C440"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C444")}));
            }
        }
    }

    protected void validateB202(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || Modelo3IRSValidatorUtil.in(linha.getCampoQ4(), new long[]{401, 442, 445}) || ano == null || ano <= 2013)) {
            result.add(new DeclValidationMessage("B202", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForQ4(numeroDaLinha))}));
        }
    }

    protected void validateB043(ValidationResult result, AnexoBModel anexoBModel) {
        Long Csoma1 = anexoBModel.getQuadro04().getAnexoBq04C1();
        Long somaControloC1 = anexoBModel.getQuadro04().getSumC1();
        if (!(Csoma1 == null || somaControloC1 == null || Csoma1.equals(somaControloC1))) {
            result.add(new DeclValidationMessage("B043", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C1")}));
        }
    }

    protected void validateB046(ValidationResult result, AnexoBModel anexoBModel) {
        Long Csoma3 = anexoBModel.getQuadro04().getAnexoBq04C3();
        Long somaControloC3 = anexoBModel.getQuadro04().getSumC3();
        if (!(Csoma3 == null || somaControloC3 == null || Csoma3.equals(somaControloC3))) {
            result.add(new DeclValidationMessage("B046", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C3")}));
        }
    }

    private void validateAnexoBq04T1(DeclaracaoModel model, AnexoBModel anexoBModel, Quadro04 quadro04Model, ValidationResult result, EventList<AnexoBq04T1_Linha> anexoBq04T1) {
        ArrayList<String> repeatedLines = new ArrayList<String>();
        HashMap<Long, Long> campoSum = new HashMap<Long, Long>();
        for (int i = 0; i < anexoBq04T1.size(); ++i) {
            long definitivo;
            Long sum;
            AnexoBq04T1_Linha linha = anexoBq04T1.get(i);
            this.validateB065(result, anexoBModel, linha, i, repeatedLines);
            this.validateB128(result, anexoBModel, linha, i);
            this.validateB068(result, anexoBModel, linha, i);
            this.validateB069(result, anexoBModel, linha, i);
            this.validateB070(result, anexoBModel, linha, i);
            this.validateB071(result, anexoBModel, linha, i);
            this.validateB072(result, anexoBModel, linha, i);
            this.validateB073(result, anexoBModel, linha, i);
            this.validateB074(result, anexoBModel, linha, i);
            this.validateB125(result, anexoBModel, linha, i);
            this.validateB202(result, anexoBModel, linha, i);
            this.validateB075(result, anexoBModel, linha, i);
            this.validateB076(result, anexoBModel, linha, i);
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4())) continue;
            long venda = linha.getValorVenda() != null ? linha.getValorVenda() : 0;
            long l = definitivo = linha.getValorDefinitivo() != null ? linha.getValorDefinitivo() : 0;
            if (campoSum.containsKey(linha.getCampoQ4())) {
                sum = campoSum.get(linha.getCampoQ4());
                sum = sum + Math.max(venda, definitivo);
            } else {
                sum = Math.max(venda, definitivo);
            }
            campoSum.put(linha.getCampoQ4(), sum);
        }
        this.validateB077(result, anexoBModel, campoSum, quadro04Model, anexoBq04T1);
    }

    protected void validateB065(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha, ArrayList<String> repeatedLines) {
        if (!(linha.getFreguesia() == null || linha.getTipoPredio() == null || linha.getTipoPredio().equalsIgnoreCase("O") || linha.getArtigo() == null || linha.getFraccao() == null)) {
            String key = linha.getFreguesia().toString() + linha.getTipoPredio() + linha.getArtigo().toString() + linha.getFraccao();
            if (repeatedLines.contains(key)) {
                result.add(new DeclValidationMessage("B065", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForLinha(numeroDaLinha))}));
            } else {
                repeatedLines.add(key);
            }
        }
    }

    protected void validateB128(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (linha.isEmpty()) {
            result.add(new DeclValidationMessage("B128", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForLinha(numeroDaLinha))}));
        }
    }

    protected void validateB068(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        ListMap fregCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
        if (!(linha.getFreguesia() == null || fregCatalog.containsKey(linha.getFreguesia()))) {
            result.add(new DeclValidationMessage("B068", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForFreguesia(numeroDaLinha))}));
        }
    }

    protected void validateB069(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (!(!StringUtil.isEmpty(linha.getFreguesia()) || StringUtil.isEmpty(linha.getTipoPredio()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) && StringUtil.isEmpty(linha.getFraccao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorVenda()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorDefinitivo()) && (linha.getArt139CIRC() == null || !linha.getArt139CIRC().equals("S")))) {
            result.add(new DeclValidationMessage("B069", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForFreguesia(numeroDaLinha))}));
        }
    }

    protected void validateB070(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (!(StringUtil.isEmpty(linha.getFreguesia()) || linha.getTipoPredio() != null && linha.getCampoQ4() != null && linha.getArt139CIRC() != null && linha.getValorVenda() != null && linha.getValorVenda() != 0)) {
            result.add(new DeclValidationMessage("B070", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForFreguesia(numeroDaLinha)), anexoBModel.getLink(this.getLinkForTipo(numeroDaLinha)), anexoBModel.getLink(this.getLinkForValorVenda(numeroDaLinha)), anexoBModel.getLink(this.getLinkForQ4(numeroDaLinha)), anexoBModel.getLink(this.getLinkForCIRC(numeroDaLinha))}));
        }
    }

    protected void validateB071(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (!(StringUtil.isEmpty(linha.getTipoPredio()) || linha.getTipoPredio().equals("U") || linha.getTipoPredio().equals("R") || linha.getTipoPredio().equals("O"))) {
            result.add(new DeclValidationMessage("B071", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForTipo(numeroDaLinha))}));
        }
    }

    protected void validateB072(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (!StringUtil.isEmpty(linha.getTipoPredio()) && (linha.getTipoPredio().equals("R") || linha.getTipoPredio().equals("U")) && (linha.getArtigo() == null || linha.getArtigo() == 0)) {
            result.add(new DeclValidationMessage("B072", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForTipo(numeroDaLinha)), anexoBModel.getLink(this.getLinkForArtigo(numeroDaLinha))}));
        }
    }

    protected void validateB073(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) || !StringUtil.isEmpty(linha.getTipoPredio()) && StringUtil.in(linha.getTipoPredio(), new String[]{"R", "U"}))) {
            result.add(new DeclValidationMessage("B073", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForArtigo(numeroDaLinha)), anexoBModel.getLink(this.getLinkForTipo(numeroDaLinha))}));
        }
    }

    protected void validateB074(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (!(StringUtil.isEmpty(linha.getFraccao()) || Pattern.matches("[a-zA-Z0-9 ]*", (CharSequence)linha.getFraccao()))) {
            result.add(new DeclValidationMessage("B074", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForFraccao(numeroDaLinha))}));
        }
    }

    protected void validateB125(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (!(StringUtil.isEmpty(linha.getFraccao()) || !StringUtil.isEmpty(linha.getTipoPredio()) && StringUtil.in(linha.getTipoPredio(), new String[]{"R", "U"}))) {
            result.add(new DeclValidationMessage("B125", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForFraccao(numeroDaLinha)), anexoBModel.getLink(this.getLinkForTipo(numeroDaLinha))}));
        }
    }

    protected void validateB075(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || Modelo3IRSValidatorUtil.in(linha.getCampoQ4(), new long[]{401, 403, 410}) || ano == null || ano >= 2014)) {
            result.add(new DeclValidationMessage("B075", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForQ4(numeroDaLinha))}));
        }
    }

    protected void validateB076(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T1_Linha linha, int numeroDaLinha) {
        if (StringUtil.isEmpty(linha.getArt139CIRC()) || !StringUtil.in(linha.getArt139CIRC(), new String[]{"S", "N"})) {
            result.add(new DeclValidationMessage("B076", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink(this.getLinkForCIRC(numeroDaLinha))}));
        }
    }

    protected void validateB077(ValidationResult result, AnexoBModel anexoBModel, HashMap<Long, Long> campoSum, Quadro04 quadro04Model, EventList<AnexoBq04T1_Linha> anexoBq04T1) {
        for (Long key : campoSum.keySet()) {
            Long sum = campoSum.get(key);
            String linkToValorVenda = anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T1");
            String linkToValorDefinitivo = anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T1");
            if (anexoBq04T1.size() > 0 && this.getLinkForLinhaByQ4(quadro04Model.getAnexoBq04T1(), key) >= 0) {
                int linhaPos = this.getLinkForLinhaByQ4(quadro04Model.getAnexoBq04T1(), key);
                linkToValorVenda = anexoBModel.getLink(this.getLinkForValorVenda(linhaPos));
                linkToValorDefinitivo = anexoBModel.getLink(this.getLinkForValorDefinitivo(linhaPos));
            }
            this.validateValuesQ4(key, sum, quadro04Model, result, anexoBModel, linkToValorVenda, linkToValorDefinitivo);
        }
    }

    private void validateAnexoBq04T3(DeclaracaoModel model, AnexoBModel anexoBModel, ValidationResult result) {
        Quadro04 quadro04 = anexoBModel.getQuadro04();
        EventList<AnexoBq04T3_Linha> anexoBq04T3 = quadro04.getAnexoBq04T3();
        ArrayList<Long> nipcList = new ArrayList<Long>();
        this.validateB205(result, anexoBModel);
        for (int i = 0; i < anexoBq04T3.size(); ++i) {
            AnexoBq04T3_Linha linha = anexoBq04T3.get(i);
            String lineLink = anexoBModel.getLink(this.getLinkForLinhaT3(i));
            String linkNipcDasEntidades = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
            this.validateB179(result, anexoBModel, linha, lineLink);
            this.validateB180(result, anexoBModel, linha, nipcList, i);
            this.validateB187(result, anexoBModel, linha, i);
            this.validateB188(result, anexoBModel, linha, linkNipcDasEntidades);
            this.validateB189(result, anexoBModel, linha, linkNipcDasEntidades);
            this.validateB190(result, anexoBModel, linha, i);
            this.validateB203(result, anexoBModel, linha, i);
            this.validateB204(result, anexoBModel, linha, i);
        }
    }

    protected void validateB179(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, String lineLink) {
        if (linha != null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidioDetinadoaExploracao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN4())) {
            result.add(new DeclValidationMessage("B179", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{lineLink}));
        }
    }

    protected void validateB205(ValidationResult result, AnexoBModel anexoBModel) {
        if (anexoBModel.getQuadro04().getAnexoBq04T3().size() > 100) {
            result.add(new DeclValidationMessage("B205", anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T3"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T3")}));
        }
    }

    protected void validateB180(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, List<Long> nipcList, int numLinha) {
        if (linha.getNIPCdasEntidades() != null) {
            String lineLink = anexoBModel.getLink(this.getLinkForLinhaT3(numLinha));
            String linkNipcDasEntidades = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
            Long nipcDasEntidades = linha.getNIPCdasEntidades();
            if (nipcList.contains(nipcDasEntidades)) {
                result.add(new DeclValidationMessage("B180", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkNipcDasEntidades}));
            } else {
                nipcList.add(nipcDasEntidades);
            }
        }
    }

    protected void validateB187(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, int numLinha) {
        String lineLink = anexoBModel.getLink(this.getLinkForLinhaT3(numLinha));
        String linkNipcDasEntidades = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
        if (!(linha == null || !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidioDetinadoaExploracao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN4()))) {
            result.add(new DeclValidationMessage("B187", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkNipcDasEntidades}));
        }
    }

    protected void validateB188(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, String linkNipcDasEntidades) {
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) || Modelo3IRSValidatorUtil.isNIFValid(linha.getNIPCdasEntidades()))) {
            result.add(new DeclValidationMessage("B188", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkNipcDasEntidades}));
        }
    }

    protected void validateB189(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, String linkNipcDasEntidades) {
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) || !Modelo3IRSValidatorUtil.isNIFValid(linha.getNIPCdasEntidades()) || Modelo3IRSValidatorUtil.startsWith(linha.getNIPCdasEntidades(), "5", "6"))) {
            result.add(new DeclValidationMessage("B189", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkNipcDasEntidades}));
        }
    }

    protected void validateB190(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, int numLinha) {
        if (linha != null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidioDetinadoaExploracao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN4()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades())) {
            String lineLink = anexoBModel.getLink(this.getLinkForLinhaT3(numLinha));
            String linkNIPC = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
            result.add(new DeclValidationMessage("B190", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkNIPC}));
        }
    }

    protected void validateB204(ValidationResult result, AnexoBModel anexoBModel, AnexoBq04T3_Linha linha, int numLinha) {
        Long C3i = linha.getSubsidiosNaoDestinadosExploracaoN();
        Long C4i = linha.getSubsidiosNaoDestinadosExploracaoN1();
        Long C5i = linha.getSubsidiosNaoDestinadosExploracaoN2();
        Long C6i = linha.getSubsidiosNaoDestinadosExploracaoN3();
        Long C7i = linha.getSubsidiosNaoDestinadosExploracaoN4();
        String lineLink = anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T3.l" + (numLinha + 1));
        String linkSubisidioNaoDestinadoExploracaoN = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN1 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON1.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN2 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON2.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN3 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON3.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN4 = lineLink + ".c" + AnexoBq04T3_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON4.getIndex();
        if (C3i != null && C3i > 0 && C4i == null && C5i == null && C6i == null && C7i == null || C3i == null && C4i != null && C4i > 0 && C5i == null && C6i == null && C7i == null || C3i == null && C4i == null && C5i != null && C5i > 0 && C6i == null && C7i == null || C3i == null && C4i == null && C5i == null && C6i != null && C6i > 0 && C7i == null || C3i == null && C4i == null && C5i == null && C6i == null && C7i != null && C7i > 0) {
            result.add(new DeclValidationMessage("B204", anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T3"), new String[]{linkSubisidioNaoDestinadoExploracaoN, linkSubisidioNaoDestinadoExploracaoN1, linkSubisidioNaoDestinadoExploracaoN2, linkSubisidioNaoDestinadoExploracaoN3, linkSubisidioNaoDestinadoExploracaoN4}));
        }
    }

    private int getLinkForLinhaByQ4(EventList<AnexoBq04T1_Linha> anexoBq04T1, Long key) {
        for (int i = 0; i < anexoBq04T1.size(); ++i) {
            AnexoBq04T1_Linha linha = anexoBq04T1.get(i);
            if (linha.getCampoQ4() == null || !linha.getCampoQ4().equals(key)) continue;
            return i;
        }
        return -1;
    }

    private void validateValuesQ4(Long key, Long sum, Quadro04 quadro04Model, ValidationResult result, AnexoBModel anexoBModel, String linkToValorVenda, String linkToValorDefinitivo) {
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (key == 401) {
            long value401;
            long l = value401 = quadro04Model.getAnexoBq04C401() != null ? quadro04Model.getAnexoBq04C401() : 0;
            if (sum > value401) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C401")}));
            }
            return;
        }
        if (key == 402) {
            long value402;
            long l = value402 = quadro04Model.getAnexoBq04C402() != null ? quadro04Model.getAnexoBq04C402() : 0;
            if (sum > value402 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C402")}));
            }
            return;
        }
        if (key == 403) {
            long value403;
            long l = value403 = quadro04Model.getAnexoBq04C403() != null ? quadro04Model.getAnexoBq04C403() : 0;
            if (sum > value403 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C403")}));
            }
            return;
        }
        if (key == 404) {
            long value402;
            long l = value402 = quadro04Model.getAnexoBq04C404() != null ? quadro04Model.getAnexoBq04C404() : 0;
            if (sum > value402 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C404")}));
            }
            return;
        }
        if (key == 405 && ano != null && ano < 2014) {
            long value405;
            long l = value405 = quadro04Model.getAnexoBq04C405() != null ? quadro04Model.getAnexoBq04C405() : 0;
            if (sum > value405) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C405")}));
            }
            return;
        }
        if (key == 406) {
            long value406;
            long l = value406 = quadro04Model.getAnexoBq04C406() != null ? quadro04Model.getAnexoBq04C406() : 0;
            if (sum > value406 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C406")}));
            }
            return;
        }
        if (key == 407) {
            long value407;
            long l = value407 = quadro04Model.getAnexoBq04C407() != null ? quadro04Model.getAnexoBq04C407() : 0;
            if (sum > value407 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C407")}));
            }
            return;
        }
        if (key == 409) {
            long value409;
            long l = value409 = quadro04Model.getAnexoBq04C409() != null ? quadro04Model.getAnexoBq04C409() : 0;
            if (sum > value409 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C409")}));
            }
            return;
        }
        if (key == 410) {
            long value410;
            long l = value410 = quadro04Model.getAnexoBq04C410() != null ? quadro04Model.getAnexoBq04C410() : 0;
            if (sum > value410 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C410")}));
            }
            return;
        }
        if (key == 411) {
            long value411;
            long l = value411 = quadro04Model.getAnexoBq04C411() != null ? quadro04Model.getAnexoBq04C411() : 0;
            if (sum > value411 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C411")}));
            }
            return;
        }
        if (key == 413) {
            long value413;
            long l = value413 = quadro04Model.getAnexoBq04C413() != null ? quadro04Model.getAnexoBq04C413() : 0;
            if (sum > value413 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C413")}));
            }
            return;
        }
        if (key == 414) {
            long value414;
            long l = value414 = quadro04Model.getAnexoBq04C414() != null ? quadro04Model.getAnexoBq04C414() : 0;
            if (sum > value414 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C414")}));
            }
            return;
        }
        if (key == 420) {
            long value420;
            long l = value420 = quadro04Model.getAnexoBq04C420() != null ? quadro04Model.getAnexoBq04C420() : 0;
            if (sum > value420 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C420")}));
            }
            return;
        }
        if (key == 421) {
            long value421;
            long l = value421 = quadro04Model.getAnexoBq04C421() != null ? quadro04Model.getAnexoBq04C421() : 0;
            if (sum > value421 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C421")}));
            }
            return;
        }
        if (key == 422) {
            long value422;
            long l = value422 = quadro04Model.getAnexoBq04C422() != null ? quadro04Model.getAnexoBq04C422() : 0;
            if (sum > value422 && ano != null && ano < 2014) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C422")}));
            }
            return;
        }
        if (key == 442) {
            long value442;
            long l = value442 = quadro04Model.getAnexoBq04C442() != null ? quadro04Model.getAnexoBq04C442() : 0;
            if (sum > value442 && ano != null && ano > 2013) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C442")}));
            }
            return;
        }
        if (key == 445) {
            long value445;
            long l = value445 = quadro04Model.getAnexoBq04C445() != null ? quadro04Model.getAnexoBq04C445() : 0;
            if (sum > value445 && ano != null && ano > 2013) {
                result.add(new DeclValidationMessage("B077", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkToValorVenda, linkToValorDefinitivo, anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C445")}));
            }
            return;
        }
        throw new RuntimeException("A valida\u00e7\u00e3o para o Campo " + key + " do Anexo B do Quadro04 nao est\u00e1 implementada");
    }

    private String getLinkForLinha(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1);
    }

    private String getLinkForLinhaT3(int linhaPos) {
        return AnexoBq04T3_Linha.getLink(linhaPos + 1);
    }

    private String getLinkForFreguesia(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.FREGUESIA);
    }

    private String getLinkForTipo(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.TIPOPREDIO);
    }

    private String getLinkForArtigo(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.ARTIGO);
    }

    private String getLinkForFraccao(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.FRACCAO);
    }

    private String getLinkForValorVenda(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.VALORVENDA);
    }

    private String getLinkForQ4(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.CAMPOQ4);
    }

    private String getLinkForValorDefinitivo(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.VALORDEFINITIVO);
    }

    private String getLinkForCIRC(int linhaPos) {
        return AnexoBq04T1_Linha.getLink(linhaPos + 1, AnexoBq04T1_LinhaBase.Property.ART139CIRC);
    }

    protected void validateB133(ValidationResult result, AnexoBModel anexoB) {
        Long q04c421 = anexoB.getQuadro04().getAnexoBq04C421();
        Long q02c05 = anexoB.getQuadro02().getAnexoBq02C05();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(q04c421) || Modelo3IRSValidatorUtil.isEmptyOrZero(q02c05) || q02c05 >= 2009)) {
            result.add(new DeclValidationMessage("B133", anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C421"), new String[]{anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C421")}));
        }
    }

    protected void validateB134(ValidationResult result, AnexoBModel anexoB) {
        Long q04c422 = anexoB.getQuadro04().getAnexoBq04C422();
        Long q02c05 = anexoB.getQuadro02().getAnexoBq02C05();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(q04c422) || Modelo3IRSValidatorUtil.isEmptyOrZero(q02c05) || q02c05 >= 2009)) {
            result.add(new DeclValidationMessage("B134", anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C422"), new String[]{anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C422")}));
        }
    }

    protected void validateB170(ValidationResult result, AnexoBModel anexoB) {
        Long q04c423 = anexoB.getQuadro04().getAnexoBq04C423();
        Long q02c05 = anexoB.getQuadro02().getAnexoBq02C05();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(q04c423) || Modelo3IRSValidatorUtil.isEmptyOrZero(q02c05) || q02c05 >= 2008)) {
            result.add(new DeclValidationMessage("B170", anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C423"), new String[]{anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C423")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            long somaControloC4;
            long somaControloC1;
            long somaControloC3;
            long somaControloC2;
            long q04C404val;
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro04 quadro04Model = anexoBModel.getQuadro04();
            AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            String idx = String.valueOf(anexoBModel.getQuadro03().getAnexoBq03C08() != null ? anexoBModel.getQuadro03().getAnexoBq03C08() : (anexoBModel.getQuadro03().getAnexoBq03C09() != null ? anexoBModel.getQuadro03().getAnexoBq03C09() : 0));
            long rendimento = 0;
            if (anexoHModel != null) {
                Long rendimento_val = anexoHModel.getQuadro05().getRendimentoByNif(new Long(idx), rostoModel);
                rendimento = rendimento_val != null ? rendimento_val : rendimento;
            }
            long arredondamento = 100;
            this.validateB191(result, anexoBModel);
            this.validateB192(result, anexoBModel);
            this.validateB193(result, anexoBModel);
            this.validateB194(result, anexoBModel);
            long l = q04C404val = quadro04Model.getAnexoBq04C404() != null ? quadro04Model.getAnexoBq04C404() : 0;
            if (q04C404val < rendimento - arredondamento) {
                result.add(new DeclValidationMessage("B041", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C404"), anexoHModel.getQuadro05().getLinkRendimentoByNif(new Long(idx), rostoModel)}));
            }
            if (quadro04Model.getAnexoBq04C420() != null && (rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0) < 2007) {
                result.add(new DeclValidationMessage("B042", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C420"), "aRosto.qQuadro02.fq02C02"}));
            }
            this.validateB133(result, anexoBModel);
            this.validateB134(result, anexoBModel);
            this.validateB170(result, anexoBModel);
            this.validateB175(result, anexoBModel);
            this.validateB176(result, anexoBModel, rostoModel.getQuadro02().getQ02C02());
            this.validateB177(result, anexoBModel, rostoModel.getQuadro02().getQ02C02());
            this.validateB195(result, anexoBModel);
            this.validateB043(result, anexoBModel);
            if (quadro04Model.getAnexoBq04C406() != null && quadro04Model.getAnexoBq04C406() > 0 && anexoBModel.getQuadro03().getAnexoBq03C11() == null && anexoBModel.getQuadro03().getAnexoBq03C12() == null) {
                result.add(new DeclValidationMessage("B152", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C406")}));
            }
            if ((quadro04Model.getAnexoBq04C407() != null ? quadro04Model.getAnexoBq04C407() : 0) != (anexoBModel.getQuadro09().getAnexoBq09C1a() != null ? anexoBModel.getQuadro09().getAnexoBq09C1a() : 0)) {
                result.add(new DeclValidationMessage("B044", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C407"), anexoBModel.getLink("aAnexoB.qQuadro09.fanexoBq09C1a")}));
            }
            this.validateB196(result, anexoBModel);
            this.validateB197(result, anexoBModel);
            this.validateB198(result, anexoBModel);
            this.validateB178(result, anexoBModel, rostoModel.getQuadro02().getQ02C02());
            this.validateB199(result, anexoBModel);
            long l2 = somaControloC2 = quadro04Model.getAnexoBq04C2() != null ? quadro04Model.getAnexoBq04C2() : 0;
            if (somaControloC2 != quadro04Model.getSumC2()) {
                result.add(new DeclValidationMessage("B045", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C2")}));
            }
            this.validateB046(result, anexoBModel);
            if ((quadro04Model.getAnexoBq04C414() != null ? quadro04Model.getAnexoBq04C414() : 0) != (anexoBModel.getQuadro09().getAnexoBq09C1b() != null ? anexoBModel.getQuadro09().getAnexoBq09C1b() : 0)) {
                result.add(new DeclValidationMessage("B047", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C414"), anexoBModel.getLink("aAnexoB.qQuadro09.fanexoBq09C1b")}));
            }
            long l3 = somaControloC4 = quadro04Model.getAnexoBq04C4() != null ? quadro04Model.getAnexoBq04C4() : 0;
            if (somaControloC4 != quadro04Model.getSumC4()) {
                result.add(new DeclValidationMessage("B048", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C4")}));
            }
            long l4 = somaControloC1 = quadro04Model.getAnexoBq04C1() != null ? quadro04Model.getAnexoBq04C1() : 0;
            if (!(somaControloC1 <= 0 && somaControloC2 <= 0 || anexoBModel.getQuadro01().isAnexoBq01B3Selected())) {
                result.add(new DeclValidationMessage("B049", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04"), anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B3")}));
            }
            long l5 = somaControloC3 = quadro04Model.getAnexoBq04C3() != null ? quadro04Model.getAnexoBq04C3() : 0;
            if (!(somaControloC3 <= 0 && somaControloC4 <= 0 || anexoBModel.getQuadro01().isAnexoBq01B4Selected())) {
                result.add(new DeclValidationMessage("B050", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04"), anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B4")}));
            }
            if (quadro04Model.getAnexoBq04B1() != null && quadro04Model.getAnexoBq04B1().equals("1") && anexoBModel.getQuadro01().isAnexoBq01B1OP2Selected()) {
                result.add(new DeclValidationMessage("B051", anexoBModel.getLink("aAnexoB.qQuadro04")));
            }
            if (quadro04Model.getAnexoBq04B1() != null && quadro04Model.getAnexoBq04B1().equals("2") && quadro04Model.getAnexoBq04B2() != null) {
                result.add(new DeclValidationMessage("B052", anexoBModel.getLink("aAnexoB.qQuadro04")));
            }
            if (!(quadro04Model.getAnexoBq04B1() == null || StringUtil.in(quadro04Model.getAnexoBq04B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("B146", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B1OP1")}));
            }
            this.validateB053(result, anexoBModel);
            this.validateB200(result, anexoBModel);
            if (!(quadro04Model.getAnexoBq04B2() == null || StringUtil.in(quadro04Model.getAnexoBq04B2(), new String[]{"3", "4"}))) {
                result.add(new DeclValidationMessage("B147", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B2OP3")}));
            }
            this.validateB054(result, anexoBModel);
            this.validateB201(result, anexoBModel);
            if (quadro04Model.getAnexoBq04B2() != null && quadro04Model.getAnexoBq04B2().equals("3") && quadro04Model.getAnexoBq04B1() != null && quadro04Model.getAnexoBq04B1().equals("2")) {
                result.add(new DeclValidationMessage("B055", anexoBModel.getLink("aAnexoB.qQuadro04")));
            }
            if (quadro04Model.getAnexoBq04B2() != null && quadro04Model.getAnexoBq04B2().equals("4") && quadro04Model.getAnexoBq04B1() != null && quadro04Model.getAnexoBq04B1().equals("2")) {
                result.add(new DeclValidationMessage("B056", anexoBModel.getLink("aAnexoB.qQuadro04")));
            }
            if (!(quadro04Model.getAnexoBq04B1() == null || !quadro04Model.getAnexoBq04B1().equals("1") || quadro04Model.getAnexoBq04B2() != null && ("3".equals(quadro04Model.getAnexoBq04B2()) || "4".equals(quadro04Model.getAnexoBq04B2())))) {
                result.add(new DeclValidationMessage("B057", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B2OP3")}));
            }
            if (quadro04Model.getAnexoBq04B2() != null && quadro04Model.getAnexoBq04B2().equals("3") && (quadro04Model.getAnexoBq04B1() == null || !quadro04Model.getAnexoBq04B1().equals("1"))) {
                result.add(new DeclValidationMessage("B060", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B2OP3"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B1OP1")}));
            }
            if (quadro04Model.getAnexoBq04B3() != null && quadro04Model.getAnexoBq04B3().equals("2") && !quadro04Model.getAnexoBq04T1().isEmpty()) {
                result.add(new DeclValidationMessage("B063", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B3OP2"), anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T1")}));
            }
            if (quadro04Model.getAnexoBq04T1().size() > 200) {
                result.add(new DeclValidationMessage("B064", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T1")}));
            }
            if (quadro04Model.getAnexoBq04T1().size() > 0 && quadro04Model.getAnexoBq04B3() == null) {
                result.add(new DeclValidationMessage("B067", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B3OP1")}));
            }
            this.validateAnexoBq04T1(model, anexoBModel, quadro04Model, result, quadro04Model.getAnexoBq04T1());
            this.validateAnexoBq04T3(model, anexoBModel, result);
        }
        return result;
    }

    protected void validateB191(ValidationResult result, AnexoBModel anexoBModel) {
        Long C403 = anexoBModel.getQuadro04().getAnexoBq04C403();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C403 != null && ano != null && C403 > 0 && ano > 2013) {
            result.add(new DeclValidationMessage("B191", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB192(ValidationResult result, AnexoBModel anexoBModel) {
        Long C440 = anexoBModel.getQuadro04().getAnexoBq04C440();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C440 != null && ano != null && C440 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("B192", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C440"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C440"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB193(ValidationResult result, AnexoBModel anexoBModel) {
        Long C441 = anexoBModel.getQuadro04().getAnexoBq04C441();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C441 != null && ano != null && C441 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("B193", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C441"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C441"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB194(ValidationResult result, AnexoBModel anexoBModel) {
        Long C442 = anexoBModel.getQuadro04().getAnexoBq04C442();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C442 != null && ano != null && C442 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("B194", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C442"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C442"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB176(ValidationResult result, AnexoBModel anexoB, Long anoExercicio) {
        Long subsidio = anexoB.getQuadro04().getAnexoBq04C424();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(subsidio) && subsidio > 0 && (Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio) || anoExercicio < 2011)) {
            result.add(new DeclValidationMessage("B176", anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C424"), new String[]{anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C424")}));
        }
    }

    protected void validateB177(ValidationResult result, AnexoBModel anexoB, Long anoExercicio) {
        Long subsidio = anexoB.getQuadro04().getAnexoBq04C425();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(subsidio) && subsidio > 0 && (Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio) || anoExercicio < 2012)) {
            result.add(new DeclValidationMessage("B177", anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C425"), new String[]{anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C425")}));
        }
    }

    protected void validateB195(ValidationResult result, AnexoBModel anexoBModel) {
        Long C443 = anexoBModel.getQuadro04().getAnexoBq04C443();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C443 != null && ano != null && C443 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("B195", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C443"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C443"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB196(ValidationResult result, AnexoBModel anexoBModel) {
        Long C410 = anexoBModel.getQuadro04().getAnexoBq04C410();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C410 != null && ano != null && C410 > 0 && ano > 2013) {
            result.add(new DeclValidationMessage("B196", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C410"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C410"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB197(ValidationResult result, AnexoBModel anexoBModel) {
        Long C444 = anexoBModel.getQuadro04().getAnexoBq04C444();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C444 != null && ano != null && C444 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("B197", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C444"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C444"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB198(ValidationResult result, AnexoBModel anexoBModel) {
        Long C445 = anexoBModel.getQuadro04().getAnexoBq04C445();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C445 != null && ano != null && C445 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("B198", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C445"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C445"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB178(ValidationResult result, AnexoBModel anexoB, Long anoExercicio) {
        Long subsidio = anexoB.getQuadro04().getAnexoBq04C426();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(subsidio) && subsidio > 0 && (Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio) || anoExercicio < 2012)) {
            result.add(new DeclValidationMessage("B178", anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C426"), new String[]{anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C426")}));
        }
    }

    protected void validateB199(ValidationResult result, AnexoBModel anexoBModel) {
        Long C446 = anexoBModel.getQuadro04().getAnexoBq04C446();
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (C446 != null && ano != null && C446 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("B199", anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C446"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C446"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    public void validateB175(ValidationResult result, AnexoBModel anexoB) {
        if (anexoB == null) {
            return;
        }
        Long q04c423 = anexoB.getQuadro04().getAnexoBq04C423();
        String q01c02 = anexoB.getQuadro01().getAnexoBq01B1();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(q04c423) && "2".equals(q01c02)) {
            result.add(new DeclValidationMessage("B175", anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C423"), new String[]{anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C423"), anexoB.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2")}));
        }
    }
}

