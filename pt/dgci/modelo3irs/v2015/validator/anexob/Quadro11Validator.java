/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.StringUtil;

public abstract class Quadro11Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String ANEXO_B = "AnexoB";
    public static final String B110 = "B110";
    public static final String B111 = "B111";
    public static final String B112 = "B112";
    public static final String B113 = "B113";
    public static final String B114 = "B114";
    public static final String B115 = "B115";
    public static final String B116 = "B116";
    public static final String B171 = "B171";
    public static final String B172 = "B172";
    public static final String B173 = "B173";

    public Quadro11Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel declaracao) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateB110(ValidationResult result, AnexoBModel anexoB) {
        Quadro11 quadro11 = anexoB.getQuadro11();
        long vendas = quadro11.getAnexoBq11C1101() != null ? quadro11.getAnexoBq11C1101() : 0;
        long soma = anexoB.getQuadro04().getAnexoBq04C401() != null ? anexoB.getQuadro04().getAnexoBq04C401() : 0;
        soma+=anexoB.getQuadro04().getAnexoBq04C409() != null ? anexoB.getQuadro04().getAnexoBq04C409() : 0;
        if (vendas != (soma+=anexoB.getQuadro04().getAnexoBq04C423() != null ? anexoB.getQuadro04().getAnexoBq04C423() : 0)) {
            result.add(new DeclValidationMessage("B110", anexoB.getLink("aAnexoB.qQuadro11"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1101"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C401"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C409"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C423")}));
        }
    }

    private long getSomaAnexoJ(AnexoJModel anexoJ) {
        long somaAnexoJ = 0;
        if (anexoJ != null) {
            somaAnexoJ+=anexoJ.getQuadro04().getAnexoJq04C403b() != null ? anexoJ.getQuadro04().getAnexoJq04C403b() : 0;
            somaAnexoJ+=anexoJ.getQuadro04().getAnexoJq04C404b() != null ? anexoJ.getQuadro04().getAnexoJq04C404b() : 0;
            somaAnexoJ+=anexoJ.getQuadro04().getAnexoJq04C405b() != null ? anexoJ.getQuadro04().getAnexoJq04C405b() : 0;
            somaAnexoJ+=anexoJ.getQuadro04().getAnexoJq04C406b() != null ? anexoJ.getQuadro04().getAnexoJq04C406b() : 0;
            somaAnexoJ+=anexoJ.getQuadro04().getAnexoJq04C421() != null ? anexoJ.getQuadro04().getAnexoJq04C421() : 0;
            somaAnexoJ+=anexoJ.getQuadro04().getAnexoJq04C426b() != null ? anexoJ.getQuadro04().getAnexoJq04C426b() : 0;
        }
        return somaAnexoJ;
    }

    private long getNifTitular(AnexoBModel anexoB) {
        long nifTitular = anexoB.getQuadro03().getAnexoBq03C08() != null ? anexoB.getQuadro03().getAnexoBq03C08() : (anexoB.getQuadro03().getAnexoBq03C09() != null ? anexoB.getQuadro03().getAnexoBq03C09() : -1);
        return nifTitular;
    }

    private long getSomaAnexoH(AnexoHModel anexoH, RostoModel rosto, long nifTitular) {
        long somaAnexoH = 0;
        if (anexoH != null) {
            Long titular;
            EventList<AnexoHq04T4_Linha> linhasHQ04 = anexoH.getQuadro04().getAnexoHq04T4();
            for (AnexoHq04T4_Linha linha2 : linhasHQ04) {
                titular = !StringUtil.isEmpty(linha2.getTitular()) ? rosto.getNIFTitular(linha2.getTitular()) : null;
                if (titular == null || titular != nifTitular || linha2.getCodRendimentos() == null || !Modelo3IRSValidatorUtil.in(linha2.getCodRendimentos(), new long[]{403, 408, 410})) continue;
                somaAnexoH+=linha2.getRendimentosIliquidos() != null ? linha2.getRendimentosIliquidos() : 0;
            }
            if (!(anexoH.getQuadro05().getAnexoHq05T5() == null || anexoH.getQuadro05().getAnexoHq05T5().isEmpty())) {
                for (AnexoHq05T5_Linha linha : anexoH.getQuadro05().getAnexoHq05T5()) {
                    long montante;
                    titular = !StringUtil.isEmpty(linha.getTitular()) ? rosto.getNIFTitular(linha.getTitular()) : null;
                    long l = montante = linha.getMontanteRendimento() != null ? linha.getMontanteRendimento() : 0;
                    if (titular == null || titular != nifTitular) continue;
                    somaAnexoH+=montante;
                }
            }
        }
        return somaAnexoH;
    }

    protected void validateB111(ValidationResult result, AnexoBModel anexoB, AnexoHModel anexoH, AnexoJModel anexoJ) {
        if (anexoH == null && anexoJ == null) {
            pt.dgci.modelo3irs.v2015.model.anexob.Quadro04 quadro04 = anexoB.getQuadro04();
            Quadro05 quadro05 = anexoB.getQuadro05();
            Quadro11 quadro11 = anexoB.getQuadro11();
            long soma = Modelo3IRSValidatorUtil.sumValue(quadro04.getRendimentos(), quadro05.getAnexoBq05C501());
            Long c1102 = quadro11.getAnexoBq11C1102();
            if (c1102 == null) {
                c1102 = 0;
            }
            long folgaNaDiferenca = 0;
            if (Math.abs(soma - c1102) > folgaNaDiferenca) {
                result.add(new DeclValidationMessage("B111", anexoB.getLink("aAnexoB.qQuadro11"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1102"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C402"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C404"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C410"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C411"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C420"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C421"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C422"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C424"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C425"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C426"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C440"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C441"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C442"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C443"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C444"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C445"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C446"), anexoB.getLink("aAnexoB.qQuadro05.fanexoBq05C501")}));
            }
        }
    }

    protected void validateB112(ValidationResult result, AnexoBModel anexoB, AnexoHModel anexoH, AnexoJModel anexoJ, RostoModel rosto) {
        long c1102;
        long soma = Modelo3IRSValidatorUtil.sumValue(anexoB.getQuadro04().getRendimentos(), anexoB.getQuadro05().getAnexoBq05C501(), this.getSomaAnexoH(anexoH, rosto, this.getNifTitular(anexoB)));
        boolean folgaNaDiferenca = true;
        long l = c1102 = anexoB.getQuadro11().getAnexoBq11C1102() == null ? 0 : anexoB.getQuadro11().getAnexoBq11C1102();
        if (anexoH != null && anexoJ == null && Math.abs(soma - c1102) > (long)folgaNaDiferenca ? 1 : 0) {
            result.add(new DeclValidationMessage("B112", anexoB.getLink("aAnexoB.qQuadro11"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1102"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C402"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C404"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C410"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C411"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C420"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C421"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C422"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C424"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C425"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C426"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C440"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C441"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C442"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C443"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C444"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C445"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C446"), anexoB.getLink("aAnexoB.qQuadro05.fanexoBq05C501"), "aAnexoH.qQuadro04.tanexoHq04T4", "aAnexoH.qQuadro04.tanexoHq04T4", "aAnexoH.qQuadro04.tanexoHq04T4", "aAnexoH.qQuadro05.fanexoHq05C1"}));
        }
    }

    protected void validateB113(ValidationResult result, AnexoBModel anexoB, AnexoHModel anexoH, AnexoJModel anexoJ) {
        long soma = Modelo3IRSValidatorUtil.sumValue(anexoB.getQuadro04().getRendimentos(), anexoB.getQuadro05().getAnexoBq05C501(), this.getSomaAnexoJ(anexoJ));
        Long c1102 = anexoB.getQuadro11().getAnexoBq11C1102();
        if (c1102 == null) {
            c1102 = 0;
        }
        long folgaNaDiferenca = 1;
        if (anexoH == null && anexoJ != null && Math.abs(soma - c1102) > folgaNaDiferenca) {
            result.add(new DeclValidationMessage("B113", anexoB.getLink("aAnexoB.qQuadro11"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1102"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C402"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C404"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C410"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C411"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C420"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C421"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C422"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C424"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C425"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C426"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C440"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C441"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C442"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C443"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C444"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C445"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C446"), anexoB.getLink("aAnexoB.qQuadro05.fanexoBq05C501"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C421"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b")}));
        }
    }

    protected void validateB114(ValidationResult result, AnexoBModel anexoB, AnexoHModel anexoH, AnexoJModel anexoJ, RostoModel rosto) {
        long soma = Modelo3IRSValidatorUtil.sumValue(anexoB.getQuadro04().getRendimentos(), anexoB.getQuadro05().getAnexoBq05C501(), this.getSomaAnexoH(anexoH, rosto, this.getNifTitular(anexoB)), this.getSomaAnexoJ(anexoJ));
        Long c1102 = anexoB.getQuadro11().getAnexoBq11C1102();
        if (c1102 == null) {
            c1102 = 0;
        }
        long folgaNaDiferenca = 1;
        if (anexoH != null && anexoJ != null && Math.abs(soma - c1102) > folgaNaDiferenca) {
            result.add(new DeclValidationMessage("B114", anexoB.getLink("aAnexoB.qQuadro11"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1102"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C402"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C403"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C404"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C405"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C410"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C411"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C420"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C421"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C422"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C424"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C425"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C426"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C440"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C441"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C442"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C443"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C444"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C445"), anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C446"), anexoB.getLink("aAnexoB.qQuadro05.fanexoBq05C501"), "aAnexoH.qQuadro04.tanexoHq04T4", "aAnexoH.qQuadro04.tanexoHq04T4", "aAnexoH.qQuadro04.tanexoHq04T4", "aAnexoH.qQuadro05.fanexoHq05C1", anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C421"), anexoJ.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b")}));
        }
    }

    protected void validateB115(ValidationResult result, FormKey key, AnexoBModel anexoB, AnexoHModel anexoH, AnexoJModel anexoJ, RostoModel rosto) {
        Long[] arrlong = new Long[3];
        arrlong[0] = anexoB.getQuadro04().getRendimentosWith423();
        arrlong[1] = anexoB.getQuadro04().getAnexoBq04C401() != null ? anexoB.getQuadro04().getAnexoBq04C401() : 0;
        arrlong[2] = anexoB.getQuadro04().getAnexoBq04C409() != null ? anexoB.getQuadro04().getAnexoBq04C409() : 0;
        long somaRedimentosQ04 = Modelo3IRSValidatorUtil.sumValue(arrlong);
        long soma1 = Modelo3IRSValidatorUtil.sumValue(anexoB.getQuadro11().getAnexoBq11C1101(), anexoB.getQuadro11().getAnexoBq11C1102());
        long soma2 = Modelo3IRSValidatorUtil.sumValue(somaRedimentosQ04, anexoB.getQuadro05().getAnexoBq05C501(), this.getSomaAnexoH(anexoH, rosto, this.getNifTitular(anexoB)), this.getSomaAnexoJ(anexoJ));
        if (!Modelo3IRSValidatorUtil.equals(soma1, soma2)) {
            String[] arrstring = new String[33];
            arrstring[0] = anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1101");
            arrstring[1] = anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1102");
            arrstring[2] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C401");
            arrstring[3] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C402");
            arrstring[4] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C403");
            arrstring[5] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C404");
            arrstring[6] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C405");
            arrstring[7] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C409");
            arrstring[8] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C410");
            arrstring[9] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C411");
            arrstring[10] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C420");
            arrstring[11] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C421");
            arrstring[12] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C422");
            arrstring[13] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C423");
            arrstring[14] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C424");
            arrstring[15] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C425");
            arrstring[16] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C426");
            arrstring[17] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C440");
            arrstring[18] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C441");
            arrstring[19] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C442");
            arrstring[20] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C443");
            arrstring[21] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C444");
            arrstring[22] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C445");
            arrstring[23] = anexoB.getLink("aAnexoB.qQuadro04.fanexoBq04C446");
            arrstring[24] = "aAnexoH.qQuadro04.tanexoHq04T4";
            arrstring[25] = "aAnexoH.qQuadro04.tanexoHq04T4";
            arrstring[26] = "aAnexoH.qQuadro04.tanexoHq04T4";
            arrstring[27] = anexoJ == null ? "aAnexoJ.qQuadro04.fanexoJq04C403b" : "aAnexoJ.qQuadro04.fanexoJq04C403b".replaceFirst("aAnexoJ", "aAnexoJ|" + key.getSubId());
            arrstring[28] = anexoJ == null ? "aAnexoJ.qQuadro04.fanexoJq04C404b" : "aAnexoJ.qQuadro04.fanexoJq04C404b".replaceFirst("aAnexoJ", "aAnexoJ|" + key.getSubId());
            arrstring[29] = anexoJ == null ? "aAnexoJ.qQuadro04.fanexoJq04C405b" : "aAnexoJ.qQuadro04.fanexoJq04C405b".replaceFirst("aAnexoJ", "aAnexoJ|" + key.getSubId());
            arrstring[30] = anexoJ == null ? "aAnexoJ.qQuadro04.fanexoJq04C406b" : "aAnexoJ.qQuadro04.fanexoJq04C406b".replaceFirst("aAnexoJ", "aAnexoJ|" + key.getSubId());
            arrstring[31] = anexoJ == null ? "aAnexoJ.qQuadro04.fanexoJq04C421" : "aAnexoJ.qQuadro04.fanexoJq04C421".replaceFirst("aAnexoJ", "aAnexoJ|" + key.getSubId());
            arrstring[32] = anexoJ == null ? "aAnexoJ.qQuadro04.fanexoJq04C426b" : "aAnexoJ.qQuadro04.fanexoJq04C426b".replaceFirst("aAnexoJ", "aAnexoJ|" + key.getSubId());
            result.add(new DeclValidationMessage("B115", anexoB.getLink("aAnexoB.qQuadro11"), arrstring));
        }
    }

    protected void validateB171(ValidationResult result, AnexoBModel anexoB) {
        Long c1107 = anexoB.getQuadro11().getAnexoBq11C1107();
        long soma = Modelo3IRSValidatorUtil.sumValue(anexoB.getQuadro11().getAnexoBq11C1101(), anexoB.getQuadro11().getAnexoBq11C1102());
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c1107) || Modelo3IRSValidatorUtil.equals(c1107, soma))) {
            result.add(new DeclValidationMessage("B171", anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1107"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1107")}));
        }
    }

    protected void validateB172(ValidationResult result, AnexoBModel anexoB) {
        Long c1108 = anexoB.getQuadro11().getAnexoBq11C1108();
        long soma = Modelo3IRSValidatorUtil.sumValue(anexoB.getQuadro11().getAnexoBq11C1103(), anexoB.getQuadro11().getAnexoBq11C1104());
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c1108) || Modelo3IRSValidatorUtil.equals(c1108, soma))) {
            result.add(new DeclValidationMessage("B172", anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1108"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1108")}));
        }
    }

    protected void validateB173(ValidationResult result, AnexoBModel anexoB) {
        Long c1109 = anexoB.getQuadro11().getAnexoBq11C1109();
        long soma = Modelo3IRSValidatorUtil.sumValue(anexoB.getQuadro11().getAnexoBq11C1105(), anexoB.getQuadro11().getAnexoBq11C1106());
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c1109) || Modelo3IRSValidatorUtil.equals(c1109, soma))) {
            result.add(new DeclValidationMessage("B173", anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1109"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1109")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel declaracao) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = declaracao.getAllAnexosByType("AnexoB");
        RostoModel rosto = (RostoModel)declaracao.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoBModel anexoB = (AnexoBModel)declaracao.getAnexo(key);
            Quadro11 quadro11 = anexoB.getQuadro11();
            AnexoHModel anexoH = (AnexoHModel)declaracao.getAnexo(AnexoHModel.class);
            long nifTitular = this.getNifTitular(anexoB);
            AnexoJModel anexoJ = ((Modelo3IRSv2015Model)declaracao).getAnexoJByTitular(String.valueOf(nifTitular));
            this.validateB110(result, anexoB);
            if (quadro11.getAnexoBq11C1103() == null || quadro11.getAnexoBq11C1104() == null || quadro11.getAnexoBq11C1105() == null || quadro11.getAnexoBq11C1106() == null) {
                result.add(new DeclValidationMessage("B116", anexoB.getLink("aAnexoB.qQuadro11"), new String[]{anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1103"), anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1104"), anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1105"), anexoB.getLink("aAnexoB.qQuadro11.fanexoBq11C1106")}));
            }
            this.validateB171(result, anexoB);
            this.validateB172(result, anexoB);
            this.validateB173(result, anexoB);
            this.validateB111(result, anexoB, anexoH, anexoJ);
            this.validateB112(result, anexoB, anexoH, anexoJ, rosto);
            this.validateB113(result, anexoB, anexoH, anexoJ);
            this.validateB114(result, anexoB, anexoH, anexoJ, rosto);
            this.validateB115(result, key, anexoB, anexoH, anexoJ, rosto);
        }
        return result;
    }
}

