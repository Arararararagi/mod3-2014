/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class AnexoBValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoBValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateB002(ValidationResult result, DeclaracaoModel model) {
        HashMap<String, String> processedNifs = new HashMap<String, String>();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            String nifAsString = String.valueOf(anexoBModel.getQuadro03().getAnexoBq03C08() != null ? anexoBModel.getQuadro03().getAnexoBq03C08() : (anexoBModel.getQuadro03().getAnexoBq03C09() != null ? anexoBModel.getQuadro03().getAnexoBq03C09() : 0));
            if (processedNifs.get(nifAsString) != null) {
                AnexoBModel otherModel = (AnexoBModel)model.getAnexo(new FormKey(AnexoBModel.class, (String)processedNifs.get(nifAsString)));
                if (otherModel == null) {
                    otherModel = anexoBModel;
                }
                result.add(new DeclValidationMessage("B002", anexoBModel.getLink("aAnexoB"), new String[]{otherModel.getLink("aAnexoB.qQuadro03"), anexoBModel.getLink("aAnexoB.qQuadro03")}));
                continue;
            }
            processedNifs.put(nifAsString, key.getSubId());
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            String idx = String.valueOf(anexoBModel.getQuadro03().getAnexoBq03C08() != null ? anexoBModel.getQuadro03().getAnexoBq03C08() : (anexoBModel.getQuadro03().getAnexoBq03C09() != null ? anexoBModel.getQuadro03().getAnexoBq03C09() : 0));
            AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(idx);
            this.validateB001(result, anexoBModel, anexoCModel);
            if (Modelo3IRSv2015Parameters.instance().isFase2()) continue;
            result.add(new DeclValidationMessage("B153", anexoBModel.getLink("aAnexoB"), new String[]{anexoBModel.getLink("aAnexoB")}));
        }
        this.validateB002(result, model);
        return result;
    }

    protected void validateB001(ValidationResult result, AnexoBModel anexoBModel, AnexoCModel anexoCModel) {
        if (anexoCModel != null && anexoBModel.isEmpty()) {
            result.add(new DeclValidationMessage("B001", anexoBModel.getLink("aAnexoB"), new String[]{anexoCModel.getLink("aAnexoC")}));
        }
    }
}

