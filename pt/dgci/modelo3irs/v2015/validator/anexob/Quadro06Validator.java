/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro06;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro06 quadro06Model = anexoBModel.getQuadro06();
            long somaControlo = quadro06Model.getAnexoBq06C1() != null ? quadro06Model.getAnexoBq06C1() : 0;
            long somaParcelas = quadro06Model.getAnexoBq06C601() == null ? 0 : quadro06Model.getAnexoBq06C601();
            if (somaControlo == (somaParcelas+=quadro06Model.getAnexoBq06C602() == null ? 0 : quadro06Model.getAnexoBq06C602())) continue;
            result.add(new DeclValidationMessage("B081", anexoBModel.getLink("aAnexoB.qQuadro06"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro06.fanexoBq06C1")}));
        }
        return result;
    }
}

