/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro09;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro09Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro09Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            long somaControloC1a;
            long somaControloC1b;
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro09 quadro09Model = anexoBModel.getQuadro09();
            long l = somaControloC1a = quadro09Model.getAnexoBq09C1a() != null ? quadro09Model.getAnexoBq09C1a() : 0;
            if (somaControloC1a != quadro09Model.getSomaC1a()) {
                result.add(new DeclValidationMessage("B107", anexoBModel.getLink("aAnexoB.qQuadro09"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro09.fanexoBq09C1a")}));
            }
            if ((somaControloC1b = quadro09Model.getAnexoBq09C1b() != null ? quadro09Model.getAnexoBq09C1b() : 0) == quadro09Model.getSomaC1b()) continue;
            result.add(new DeclValidationMessage("B108", anexoBModel.getLink("aAnexoB.qQuadro09"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro09.fanexoBq09C1b")}));
        }
        return result;
    }
}

