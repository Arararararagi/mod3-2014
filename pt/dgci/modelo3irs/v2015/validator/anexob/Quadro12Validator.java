/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.Date;
import pt.opensoft.util.StringUtil;

public abstract class Quadro12Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro12Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro12 quadro12Model = anexoBModel.getQuadro12();
            this.validateB117(result, anexoBModel, quadro12Model);
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            this.validateB118(result, anexoBModel, quadro12Model);
            this.validateB150(result, anexoBModel, quadro12Model);
            this.validateB119(result, anexoBModel, quadro12Model, rostoModel);
            this.validateB120(result, anexoBModel, quadro12Model);
            this.validateB121(result, anexoBModel, quadro12Model, rostoModel);
            this.validateB122(result, anexoBModel, quadro12Model);
            this.validateB151(result, anexoBModel, quadro12Model);
        }
        return result;
    }

    protected void validateB117(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model) {
        if ((quadro12Model.getAnexoBq12B1() == null || "0".equals(quadro12Model.getAnexoBq12B1())) && (anexoBModel.getQuadro01().getAnexoBq01B1() == null || !anexoBModel.getQuadro01().isAnexoBq01B1OP2Selected())) {
            result.add(new DeclValidationMessage("B117", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B1OP1")}));
        }
    }

    protected void validateB118(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model) {
        if (quadro12Model.getAnexoBq12B1() != null && StringUtil.in(quadro12Model.getAnexoBq12B1(), new String[]{"1", "2"}) && anexoBModel.getQuadro01().isAnexoBq01B1OP2Selected()) {
            result.add(new DeclValidationMessage("B118", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B1OP1"), anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2")}));
        }
    }

    protected void validateB150(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model) {
        if (!(quadro12Model.getAnexoBq12B1() == null || StringUtil.in(quadro12Model.getAnexoBq12B1(), new String[]{"0", "1", "2"}))) {
            result.add(new DeclValidationMessage("B150", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B1OP1")}));
        }
    }

    protected void validateB119(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model, RostoModel rostoModel) {
        String idx = String.valueOf(anexoBModel.getQuadro03().getAnexoBq03C08() != null ? anexoBModel.getQuadro03().getAnexoBq03C08() : (anexoBModel.getQuadro03().getAnexoBq03C09() != null ? anexoBModel.getQuadro03().getAnexoBq03C09() : 0));
        if (quadro12Model.getAnexoBq12B1() != null && !quadro12Model.getAnexoBq12B1().equals("1") && rostoModel.getQuadro07().isConjugeFalecido(Long.valueOf(idx))) {
            result.add(new DeclValidationMessage("B119", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{"aRosto.qQuadro07.fq07C1", anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B1OP1")}));
        }
    }

    protected void validateB120(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model) {
        if (quadro12Model.getAnexoBq12B1() != null && quadro12Model.getAnexoBq12B1().equals("1") && quadro12Model.getAnexoBq12C3() == null) {
            result.add(new DeclValidationMessage("B120", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12C3")}));
        }
    }

    protected void validateB121(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model, RostoModel rostoModel) {
        long ano_exercicio;
        long l = ano_exercicio = rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0;
        if (quadro12Model.getAnexoBq12C3() != null && ano_exercicio != (long)quadro12Model.getAnexoBq12C3().getYear()) {
            result.add(new DeclValidationMessage("B121", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12C3"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateB122(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model) {
        if (quadro12Model.getAnexoBq12C3() != null && quadro12Model.getAnexoBq12B1() != null && quadro12Model.getAnexoBq12B1().equals("2")) {
            result.add(new DeclValidationMessage("B122", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12C3"), anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B1OP2")}));
        }
    }

    protected void validateB151(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model) {
        if (!(quadro12Model.getAnexoBq12B4() == null || quadro12Model.getAnexoBq12B4().equals(Boolean.TRUE) || quadro12Model.getAnexoBq12B4().equals(Boolean.FALSE))) {
            result.add(new DeclValidationMessage("B151", anexoBModel.getLink(anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B4")), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B4")}));
        }
    }
}

