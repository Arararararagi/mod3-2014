/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro10;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro10Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro10Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro10 quadro10Model = anexoBModel.getQuadro10();
            long somaControlo = quadro10Model.getAnexoBq10C1() != null ? quadro10Model.getAnexoBq10C1() : 0;
            long somaParcelas = quadro10Model.getAnexoBq10C1001() != null ? quadro10Model.getAnexoBq10C1001() : 0;
            if (somaControlo == (somaParcelas+=quadro10Model.getAnexoBq10C1002() != null ? quadro10Model.getAnexoBq10C1002() : 0)) continue;
            result.add(new DeclValidationMessage("B109", anexoBModel.getLink("aAnexoB.qQuadro10"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro10.fanexoBq10C1")}));
        }
        return result;
    }
}

