/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Cirs;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateB017(ValidationResult result, AnexoBModel anexoBModel, RostoModel rostoModel, String linkNIF) {
        Long linhaNIF = anexoBModel.getQuadro03().getAnexoBq03C08();
        if (!(!anexoBModel.getQuadro03().hasAnexoBq03C08() || anexoBModel.getQuadro03().isAnexoBq03B1OPSim() || Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoA(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoB(linhaNIF) || rostoModel.getQuadro03().existsInDependentes(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(linhaNIF) || rostoModel.getQuadro07().isConjugeFalecido(linhaNIF))) {
            result.add(new DeclValidationMessage("B017", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro03 quadro03Model = anexoBModel.getQuadro03();
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoBq03C06(), rostoModel.getQuadro03().getQ03C03())) {
                result.add(new DeclValidationMessage("B015", anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C06"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C06"), "aRosto.qQuadro03.fq03C03"}));
            }
            if (!Modelo3IRSValidatorUtil.equals(quadro03Model.getAnexoBq03C07(), rostoModel.getQuadro03().getQ03C04())) {
                result.add(new DeclValidationMessage("B016", anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C07"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C07"), "aRosto.qQuadro03.fq03C04"}));
            }
            this.validateB017(result, anexoBModel, rostoModel, anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C08"));
            if (!(quadro03Model.isAnexoBq03B1OPSim() || quadro03Model.getAnexoBq03C08() != null)) {
                result.add(new DeclValidationMessage("B018", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C08")}));
            }
            if (!(quadro03Model.getAnexoBq03C08() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro03Model.getAnexoBq03C08()))) {
                result.add(new DeclValidationMessage("B019", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C08")}));
            }
            if (!(quadro03Model.getAnexoBq03C08() == null || NifValidator.isSingular(quadro03Model.getAnexoBq03C08().toString()))) {
                result.add(new DeclValidationMessage("B020", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C08")}));
            }
            if (quadro03Model.isAnexoBq03B1OPSim()) {
                String link_nifOrNipc = quadro03Model.hasAnexoBq03C08() ? "aAnexoB.qQuadro03.fanexoBq03C08" : "aAnexoB.qQuadro03.fanexoBq03C09";
                AnexoIModel anexoIModel = ((Modelo3IRSv2015Model)model).getAnexoIByTitular(anexoBModel.getAnexoBTitular());
                if (anexoIModel == null) {
                    result.add(new DeclValidationMessage("B021", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03B1OP1"), anexoBModel.getLink(link_nifOrNipc)}));
                }
            }
            if (!(quadro03Model.getAnexoBq03B1() == null || StringUtil.in(quadro03Model.getAnexoBq03B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("B143", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03B1OP1")}));
            }
            if (!(quadro03Model.getAnexoBq03B1() != null && StringUtil.in(quadro03Model.getAnexoBq03B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("B022", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03")}));
            }
            if (!(quadro03Model.getAnexoBq03C09() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro03Model.getAnexoBq03C09()))) {
                result.add(new DeclValidationMessage("B023", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C09")}));
            }
            if (!(quadro03Model.getAnexoBq03C09() == null || Modelo3IRSValidatorUtil.startsWith(String.valueOf(quadro03Model.getAnexoBq03C09()), "7", "9"))) {
                result.add(new DeclValidationMessage("B024", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C09")}));
            }
            if (quadro03Model.getAnexoBq03C09() != null && quadro03Model.getAnexoBq03B1() != null && quadro03Model.getAnexoBq03B1().equals("2")) {
                result.add(new DeclValidationMessage("B025", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C09"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03B1OP1")}));
            }
            if (quadro03Model.getAnexoBq03C09() != null && quadro03Model.getAnexoBq03C08() != null) {
                result.add(new DeclValidationMessage("B026", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C09"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C08")}));
            }
            if (quadro03Model.getAnexoBq03C09() == null && quadro03Model.getAnexoBq03C08() == null) {
                result.add(new DeclValidationMessage("B027", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C08"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C09")}));
            }
            ListMap cirsCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Cirs.class.getSimpleName());
            if (!(quadro03Model.getAnexoBq03C10() == null || cirsCatalog.containsKey(StringUtil.padZeros(quadro03Model.getAnexoBq03C10().toString(), 4)))) {
                result.add(new DeclValidationMessage("B028", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C10")}));
            }
            if (quadro03Model.getAnexoBq03C10() != null && anexoBModel.getQuadro01().isAnexoBq01B1OP1Selected() && !anexoBModel.getQuadro01().isAnexoBq01B3Selected()) {
                result.add(new DeclValidationMessage("B029", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C10")}));
            }
            if (quadro03Model.getAnexoBq03C11() != null && anexoBModel.getQuadro01().isAnexoBq01B1OP1Selected() && !anexoBModel.getQuadro01().isAnexoBq01B3Selected()) {
                result.add(new DeclValidationMessage("B033", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B3"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C11")}));
            }
            if (quadro03Model.getAnexoBq03C12() != null && anexoBModel.getQuadro01().isAnexoBq01B1OP1Selected() && !anexoBModel.getQuadro01().isAnexoBq01B4Selected()) {
                result.add(new DeclValidationMessage("B035", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B4"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C12")}));
            }
            if (quadro03Model.getAnexoBq03B13() == null) {
                result.add(new DeclValidationMessage("B036", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03B13OP13")}));
            }
            if (quadro03Model.getAnexoBq03B13() == null || StringUtil.in(quadro03Model.getAnexoBq03B13(), new String[]{"13", "14"})) continue;
            result.add(new DeclValidationMessage("B145", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03B13OP13")}));
        }
        return result;
    }
}

