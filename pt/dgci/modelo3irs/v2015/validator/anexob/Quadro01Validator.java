/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.StringUtil;

public abstract class Quadro01Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro01Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    public static void validateB004(DeclaracaoModel model, AnexoBModel anexoBModel, ValidationResult result) {
        long anexoBq04C1Value;
        Quadro01 quadro01Model = anexoBModel.getQuadro01();
        Quadro04 quadro04Model = anexoBModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoRosto = rostoModel.getQuadro02().getQ02C02();
        long l = anexoBq04C1Value = quadro04Model.getAnexoBq04C1() == null ? 0 : quadro04Model.getAnexoBq04C1();
        if (quadro01Model.isAnexoBq01B1OP2Selected() && anexoBq04C1Value <= 0 && quadro01Model.isAnexoBq01B3Selected() && anoRosto != null && anoRosto < 2011) {
            result.add(new DeclValidationMessage("B004", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C1")}));
        }
    }

    public static void validateB005(DeclaracaoModel model, AnexoBModel anexoBModel, ValidationResult result) {
        long anexoBq04C3Value;
        Quadro01 quadro01Model = anexoBModel.getQuadro01();
        Quadro04 quadro04Model = anexoBModel.getQuadro04();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoRosto = rostoModel.getQuadro02().getQ02C02();
        long l = anexoBq04C3Value = quadro04Model.getAnexoBq04C3() == null ? 0 : quadro04Model.getAnexoBq04C3();
        if (quadro01Model.isAnexoBq01B1OP2Selected() && anexoBq04C3Value <= 0 && quadro01Model.isAnexoBq01B4Selected() && anoRosto != null && anoRosto < 2011) {
            result.add(new DeclValidationMessage("B005", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2"), anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C3")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro01 quadro01Model = anexoBModel.getQuadro01();
            if (!(quadro01Model.isAnexoBq01B3Selected() || quadro01Model.isAnexoBq01B4Selected())) {
                result.add(new DeclValidationMessage("B003", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B3"), anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B4")}));
            }
            if (!(quadro01Model.getAnexoBq01B1() == null || StringUtil.in(quadro01Model.getAnexoBq01B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("B136", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP1")}));
                result.add(new DeclValidationMessage("B137", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2")}));
            }
            Quadro01Validator.validateB004(model, anexoBModel, result);
            Quadro01Validator.validateB005(model, anexoBModel, result);
            if (quadro01Model.isAnexoBq01B1OP2Selected() && quadro01Model.isAnexoBq01B3Selected() && quadro01Model.isAnexoBq01B4Selected()) {
                result.add(new DeclValidationMessage("B006", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B3"), anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B4")}));
            }
            if (quadro01Model.isAnexoBq01B1OP2Selected() && anexoBModel.getQuadro03().getAnexoBq03B13() != null && anexoBModel.getQuadro03().getAnexoBq03B13().equals("13")) {
                result.add(new DeclValidationMessage("B007", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03B13OP13")}));
            }
            if (quadro01Model.isAnexoBq01B1OP2Selected() && (anexoBModel.getQuadro04().isAnexoBq04B1OPSelected() || anexoBModel.getQuadro04().isAnexoBq04B2OPSelected())) {
                String link_Q4CatA = anexoBModel.getQuadro04().isAnexoBq04B1OPSelected() ? "aAnexoB.qQuadro04.fanexoBq04B1OP1" : "aAnexoB.qQuadro04.fanexoBq04B2OP3";
                result.add(new DeclValidationMessage("B010", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2"), anexoBModel.getLink(link_Q4CatA)}));
            }
            if (!(quadro01Model.getAnexoBq01B1() != null && StringUtil.in(quadro01Model.getAnexoBq01B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("B127", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP1"), anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B1OP2")}));
            }
            if (quadro01Model.isAnexoBq01B1OP1Selected() && quadro01Model.isAnexoBq01B3Selected() && anexoBModel.getQuadro03().getAnexoBq03C10() == null && anexoBModel.getQuadro03().getAnexoBq03C11() == null) {
                result.add(new DeclValidationMessage("B013", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C11"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C10")}));
            }
            if (!(quadro01Model.getAnexoBq01B3() == null || quadro01Model.getAnexoBq01B3().equals(Boolean.TRUE) || quadro01Model.getAnexoBq01B3().equals(Boolean.FALSE))) {
                result.add(new DeclValidationMessage("B138", anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B3"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B3")}));
            }
            if (!(quadro01Model.getAnexoBq01B4() == null || quadro01Model.getAnexoBq01B4().equals(Boolean.TRUE) || quadro01Model.getAnexoBq01B4().equals(Boolean.FALSE))) {
                result.add(new DeclValidationMessage("B139", anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B4"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro01.fanexoBq01B4")}));
            }
            if (!quadro01Model.isAnexoBq01B1OP1Selected() || !quadro01Model.isAnexoBq01B4Selected() || anexoBModel.getQuadro03().getAnexoBq03C12() != null) continue;
            result.add(new DeclValidationMessage("B014", anexoBModel.getLink("aAnexoB.qQuadro01"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C12")}));
        }
        return result;
    }
}

