/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro02Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final String AnexoB = "AnexoB";
    public static final String B174 = "B174";

    public Quadro02Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected ValidationResult validateB174(ValidationResult result, RostoModel rostoModel, Quadro02 quadro02AnexoBModel) {
        Long anoRosto;
        Long anoQuadro2 = quadro02AnexoBModel.getAnexoBq02C05();
        if (!Modelo3IRSValidatorUtil.equals(anoQuadro2, anoRosto = rostoModel.getQuadro02().getQ02C02())) {
            result.add(new DeclValidationMessage("B174", "aAnexoB.qQuadro02.fanexoBq02C05", new String[]{"aAnexoB.qQuadro02.fanexoBq02C05", "aRosto.qQuadro02.fq02C02"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro02 quadro02AnexoBModel = (Quadro02)anexoBModel.getQuadro(Quadro02.class.getSimpleName());
            this.validateB174(result, rostoModel, quadro02AnexoBModel);
        }
        return result;
    }
}

