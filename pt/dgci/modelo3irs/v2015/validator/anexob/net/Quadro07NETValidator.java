/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.Quadro07Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro07NETValidator
extends Quadro07Validator {
    public Quadro07NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            this.validateB082(result, anexoBModel);
            Quadro07 quadro07Model = anexoBModel.getQuadro07();
            EventList<AnexoBq07T1_Linha> linhas = quadro07Model.getAnexoBq07T1();
            long soma = 0;
            for (int i = 0; i < linhas.size(); ++i) {
                AnexoBq07T1_Linha linha = linhas.get(i);
                this.validateB089(result, rostoModel, anexoBModel, linha, i + 1);
                if (linha.getValor() == null) continue;
                soma+=linha.getValor().longValue();
            }
            this.validateB091(result, anexoBModel, quadro07Model, soma);
        }
        return result;
    }

    protected void validateB082(ValidationResult result, AnexoBModel anexoBModel) {
        boolean condicao2;
        double percent265 = 0.265;
        double percent285 = 0.285;
        long rendimentos = anexoBModel != null && anexoBModel.getQuadro07().getAnexoBq07C701() != null ? anexoBModel.getQuadro07().getAnexoBq07C701() : 0;
        boolean condicao1 = anexoBModel != null && anexoBModel.getQuadro07().getAnexoBq07C702() != null && anexoBModel.getQuadro07().getAnexoBq07C702() > Math.round((double)rendimentos * 0.265) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoBModel.getQuadro04().getAnexoBq04C422()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoBModel.getQuadro04().getAnexoBq04C445());
        boolean bl = condicao2 = anexoBModel != null && anexoBModel.getQuadro07().getAnexoBq07C702() != null && anexoBModel.getQuadro07().getAnexoBq07C702() > Math.round((double)rendimentos * 0.285) && (Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C422()) || Modelo3IRSValidatorUtil.isGreaterThanZero(anexoBModel.getQuadro04().getAnexoBq04C445()));
        if (anexoBModel != null && !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoBModel.getQuadro07().getAnexoBq07C702()) && (condicao1 || condicao2)) {
            result.add(new DeclValidationMessage("B082", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro07.fanexoBq07C702")}));
        }
    }
}

