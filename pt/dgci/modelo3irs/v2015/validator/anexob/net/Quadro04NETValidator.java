/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.Quadro04Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.StringUtil;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro04 quadro04Model = anexoBModel.getQuadro04();
            this.validateB038(result, anexoBModel, quadro04Model);
            if (!(quadro04Model.getAnexoBq04B3() != null && (quadro04Model.getAnexoBq04B3() == null || StringUtil.in(quadro04Model.getAnexoBq04B3(), new String[]{"1", "2"})))) {
                result.add(new DeclValidationMessage("B061", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B3OP1")}));
            }
            this.validateB058(result, anexoBModel, quadro04Model);
            this.validateB062(result, anexoBModel, quadro04Model);
            this.validateB181(result, anexoBModel);
            this.validateB183(result, anexoBModel);
        }
        return result;
    }

    protected void validateB181(ValidationResult result, AnexoBModel anexoBModel) {
        long somatorio;
        long C411;
        String linkC424 = anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C424");
        String linkC411 = anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C411");
        long C424 = anexoBModel.getQuadro04().getAnexoBq04C424() != null ? anexoBModel.getQuadro04().getAnexoBq04C424() : 0;
        long soma = C424 + (C411 = anexoBModel.getQuadro04().getAnexoBq04C411() != null ? anexoBModel.getQuadro04().getAnexoBq04C411() : 0);
        if (soma != (somatorio = this.somatorioQ4T2(anexoBModel.getQuadro04()).longValue())) {
            result.add(new DeclValidationMessage("B181", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkC424, linkC411}));
        }
    }

    protected void validateB183(ValidationResult result, AnexoBModel anexoBModel) {
        String linkQ4E = anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T3");
        Quadro04 quadro04 = anexoBModel.getQuadro04();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoBq04C411()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoBq04C424()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoBq04C425()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoBq04C426()) || !quadro04.getAnexoBq04T3().isEmpty())) {
            result.add(new DeclValidationMessage("B183", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{linkQ4E}));
        }
    }

    private Long somatorioQ4T2(Quadro04 quadro04) {
        Long somatorio = 0;
        if (quadro04.getAnexoBq04T3().size() > 0) {
            for (int i = 0; i < quadro04.getAnexoBq04T3().size(); ++i) {
                AnexoBq04T3_Linha linha = quadro04.getAnexoBq04T3().get(i);
                long valor = linha.getSubsidioDetinadoaExploracao() != null ? linha.getSubsidioDetinadoaExploracao() : 0;
                somatorio = somatorio + valor;
            }
            return somatorio;
        }
        return 0;
    }

    protected void validateB058(ValidationResult result, AnexoBModel anexoBModel, Quadro04 quadro04Model) {
        Long ano = anexoBModel.getQuadro02().getAnexoBq02C05();
        if (anexoBModel.getQuadro01().isAnexoBq01B1OP1Selected() && quadro04Model.getAnexoBq04B1() == null && ano != null) {
            long preenchido403 = quadro04Model.getAnexoBq04C403() == null ? 0 : quadro04Model.getAnexoBq04C403();
            long preenchido405 = quadro04Model.getAnexoBq04C405() == null ? 0 : quadro04Model.getAnexoBq04C405();
            long preenchido410 = quadro04Model.getAnexoBq04C410() == null ? 0 : quadro04Model.getAnexoBq04C410();
            long preenchido420 = quadro04Model.getAnexoBq04C420() == null ? 0 : quadro04Model.getAnexoBq04C420();
            long totalAntes2014 = preenchido403 + preenchido405 + preenchido410 + preenchido420;
            long preenchido440 = quadro04Model.getAnexoBq04C440() == null ? 0 : quadro04Model.getAnexoBq04C440();
            long preenchido444 = quadro04Model.getAnexoBq04C444() == null ? 0 : quadro04Model.getAnexoBq04C444();
            long totalDepois2013 = preenchido440 + preenchido405 + preenchido444 + preenchido420;
            if (totalAntes2014 > 0 && ano < 2014 || totalDepois2013 > 0 && ano > 2013) {
                result.add(new DeclValidationMessage("B058", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04B1OP1")}));
            }
        }
    }

    protected void validateB062(ValidationResult result, AnexoBModel anexoBModel, Quadro04 quadro04Model) {
        if (quadro04Model.getAnexoBq04B3() != null && quadro04Model.getAnexoBq04B3().equals("1") && quadro04Model.getAnexoBq04T1().isEmpty()) {
            result.add(new DeclValidationMessage("B062", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.tanexoBq04T1")}));
        }
    }

    protected void validateB038(ValidationResult result, AnexoBModel anexoBModel, Quadro04 quadro04Model) {
        long q03C11;
        long l = q03C11 = anexoBModel.getQuadro03().getAnexoBq03C11() != null ? anexoBModel.getQuadro03().getAnexoBq03C11() : 0;
        if (quadro04Model.getAnexoBq04C402() != null && quadro04Model.getAnexoBq04C402() > 0 && (q03C11 < 10000 || q03C11 >= 12000 && q03C11 < 55000 || q03C11 >= 58000 && q03C11 < 87000 || q03C11 >= 88000)) {
            result.add(new DeclValidationMessage("B038", anexoBModel.getLink("aAnexoB.qQuadro04"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro04.fanexoBq04C402"), anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C11")}));
        }
    }
}

