/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.Quadro12Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro12NETValidator
extends Quadro12Validator {
    public Quadro12NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro12 quadro12Model = anexoBModel.getQuadro12();
            long soma = anexoBModel.getQuadro11().getAnexoBq11C1101() != null ? anexoBModel.getQuadro11().getAnexoBq11C1101() : 0;
            this.validateB123(result, anexoBModel, quadro12Model, soma+=anexoBModel.getQuadro11().getAnexoBq11C1102() != null ? anexoBModel.getQuadro11().getAnexoBq11C1102() : 0);
            if (quadro12Model.getAnexoBq12B4() != null && quadro12Model.getAnexoBq12B4().booleanValue() || soma != 0) continue;
            result.add(new DeclValidationMessage("B124", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B4")}));
        }
        return result;
    }

    protected void validateB123(ValidationResult result, AnexoBModel anexoBModel, Quadro12 quadro12Model, long soma) {
        if (quadro12Model.getAnexoBq12B4() != null && quadro12Model.getAnexoBq12B4().booleanValue() && soma != 0) {
            result.add(new DeclValidationMessage("B123", anexoBModel.getLink("aAnexoB.qQuadro12"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro12.fanexoBq12B4")}));
        }
    }
}

