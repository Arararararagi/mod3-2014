/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.AnexoBValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro01NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro06NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro07NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro08NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro09NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro10NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro11NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.net.Quadro12NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoBNETValidator
extends AnexoBValidator {
    public AnexoBNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro01NETValidator().validate(model));
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        result.addAllFrom(new Quadro07NETValidator().validate(model));
        result.addAllFrom(new Quadro08NETValidator().validate(model));
        result.addAllFrom(new Quadro09NETValidator().validate(model));
        result.addAllFrom(new Quadro10NETValidator().validate(model));
        result.addAllFrom(new Quadro11NETValidator().validate(model));
        result.addAllFrom(new Quadro12NETValidator().validate(model));
        return result;
    }
}

