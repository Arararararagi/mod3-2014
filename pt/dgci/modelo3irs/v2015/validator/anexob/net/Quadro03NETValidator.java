/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Cae_vs3;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexob.Quadro03Validator;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public class Quadro03NETValidator
extends Quadro03Validator {
    public Quadro03NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro03 quadro03Model = anexoBModel.getQuadro03();
            this.validateB037(result, anexoBModel, quadro03Model);
        }
        return result;
    }

    protected void validateB037(ValidationResult result, AnexoBModel anexoBModel, Quadro03 quadro03Model) {
        ListMap caeCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Cae_vs3.class.getSimpleName());
        if (!(quadro03Model.getAnexoBq03C11() == null || caeCatalog.containsKey(StringUtil.padZeros(quadro03Model.getAnexoBq03C11().toString(), 5)))) {
            result.add(new DeclValidationMessage("B037", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C11"), "aRosto.qQuadro02.fq02C02", anexoBModel.getLink("aAnexoB.qQuadro03")}));
        }
        if (!(quadro03Model.getAnexoBq03C12() == null || caeCatalog.containsKey(StringUtil.padZeros(quadro03Model.getAnexoBq03C12().toString(), 5)))) {
            result.add(new DeclValidationMessage("B037", anexoBModel.getLink("aAnexoB.qQuadro03"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C12"), "aRosto.qQuadro02.fq02C02", anexoBModel.getLink("aAnexoB.qQuadro03")}));
        }
    }
}

