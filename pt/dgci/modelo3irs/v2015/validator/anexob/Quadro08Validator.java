/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro08Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro08Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateB096(ValidationResult result, AnexoBModel anexoBModel, RostoModel rostoModel) {
        Long nifFalecido = anexoBModel.getQuadro08().getAnexoBq08C801();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nifFalecido) && (rostoModel.getQuadro03().isSujeitoPassivoA(nifFalecido) || rostoModel.getQuadro03().isSujeitoPassivoB(nifFalecido) || rostoModel.getQuadro03().existsInDependentes(nifFalecido) || rostoModel.getQuadro07().existsInAscendentes(nifFalecido) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(nifFalecido) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nifFalecido) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(nifFalecido) || rostoModel.getQuadro07().existsInAscendentesEColaterais(nifFalecido) || rostoModel.getQuadro05().isRepresentante(nifFalecido))) {
            result.add(new DeclValidationMessage("B096", anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801"), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro05.fq05C5"}));
        }
    }

    protected void validateB097(DeclaracaoModel model, ValidationResult result, AnexoBModel anexoBModel, Quadro08 quadro08Model) {
        if (!(quadro08Model.getAnexoBq08C801() == null || model.getAllAnexosByType("AnexoC").isEmpty())) {
            List<FormKey> anexosC = model.getAllAnexosByType("AnexoC");
            for (FormKey keyC : anexosC) {
                AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(keyC);
                if (anexoCModel.getQuadro09().getAnexoCq09C901() == null || quadro08Model.getAnexoBq08C801().longValue() != anexoCModel.getQuadro09().getAnexoCq09C901().longValue()) continue;
                result.add(new DeclValidationMessage("B097", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801"), anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901")}));
                break;
            }
        }
    }

    private void validarAnos(ValidationResult result, DeclaracaoModel model, AnexoBModel anexoBModel, Quadro08 quadro08Model) {
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        ArrayList<String> processedAnos = new ArrayList<String>();
        Long ano_exercicio = rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0;
        String quadro_link = anexoBModel.getLink("aAnexoB.qQuadro08");
        this.validaLinha(result, quadro08Model.getAnexoBq08C802(), ano_exercicio, quadro08Model.getAnexoBq08C808(), quadro08Model.getAnexoBq08C814(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C802"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C808"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C814"), processedAnos);
        this.validaLinha(result, quadro08Model.getAnexoBq08C803(), ano_exercicio, quadro08Model.getAnexoBq08C809(), quadro08Model.getAnexoBq08C815(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C803"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C809"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C815"), processedAnos);
        this.validaLinha(result, quadro08Model.getAnexoBq08C804(), ano_exercicio, quadro08Model.getAnexoBq08C810(), quadro08Model.getAnexoBq08C816(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C804"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C810"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C816"), processedAnos);
        this.validaLinha(result, quadro08Model.getAnexoBq08C805(), ano_exercicio, quadro08Model.getAnexoBq08C811(), quadro08Model.getAnexoBq08C817(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C805"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C811"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C817"), processedAnos);
        this.validaLinha(result, quadro08Model.getAnexoBq08C806(), ano_exercicio, quadro08Model.getAnexoBq08C812(), quadro08Model.getAnexoBq08C818(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C806"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C812"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C818"), processedAnos);
        this.validaLinha(result, quadro08Model.getAnexoBq08C807(), ano_exercicio, quadro08Model.getAnexoBq08C813(), quadro08Model.getAnexoBq08C819(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C807"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C813"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C819"), processedAnos);
    }

    private void validarAnosNETDC(ValidationResult result, DeclaracaoModel model, AnexoBModel anexoBModel, Quadro08 quadro08Model) {
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        ArrayList<String> processedAnos = new ArrayList<String>();
        Long ano_exercicio = rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0;
        String quadro_link = anexoBModel.getLink("aAnexoB.qQuadro08");
        this.validaLinhaNETDC(result, quadro08Model.getAnexoBq08C802(), ano_exercicio, quadro08Model.getAnexoBq08C808(), quadro08Model.getAnexoBq08C814(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C802"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C808"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C814"), processedAnos);
        this.validaLinhaNETDC(result, quadro08Model.getAnexoBq08C803(), ano_exercicio, quadro08Model.getAnexoBq08C809(), quadro08Model.getAnexoBq08C815(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C803"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C809"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C815"), processedAnos);
        this.validaLinhaNETDC(result, quadro08Model.getAnexoBq08C804(), ano_exercicio, quadro08Model.getAnexoBq08C810(), quadro08Model.getAnexoBq08C816(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C804"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C810"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C816"), processedAnos);
        this.validaLinhaNETDC(result, quadro08Model.getAnexoBq08C805(), ano_exercicio, quadro08Model.getAnexoBq08C811(), quadro08Model.getAnexoBq08C817(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C805"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C811"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C817"), processedAnos);
        this.validaLinhaNETDC(result, quadro08Model.getAnexoBq08C806(), ano_exercicio, quadro08Model.getAnexoBq08C812(), quadro08Model.getAnexoBq08C818(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C806"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C812"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C818"), processedAnos);
        this.validaLinhaNETDC(result, quadro08Model.getAnexoBq08C807(), ano_exercicio, quadro08Model.getAnexoBq08C813(), quadro08Model.getAnexoBq08C819(), quadro_link, anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C807"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C813"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C819"), processedAnos);
    }

    protected void validateB103(ValidationResult result, AnexoBModel anexoBModel, Quadro08 quadro08Model) {
        if (quadro08Model.getAnexoBq08C803() != null && quadro08Model.getAnexoBq08C802() == null) {
            result.add(new DeclValidationMessage("B103", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C802"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C803")}));
        }
        if (quadro08Model.getAnexoBq08C804() != null && quadro08Model.getAnexoBq08C803() == null) {
            result.add(new DeclValidationMessage("B103", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C803"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C804")}));
        }
        if (quadro08Model.getAnexoBq08C805() != null && quadro08Model.getAnexoBq08C804() == null) {
            result.add(new DeclValidationMessage("B103", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C804"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C805")}));
        }
        if (quadro08Model.getAnexoBq08C806() != null && quadro08Model.getAnexoBq08C805() == null) {
            result.add(new DeclValidationMessage("B103", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C805"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C806")}));
        }
        if (quadro08Model.getAnexoBq08C807() != null && quadro08Model.getAnexoBq08C806() == null) {
            result.add(new DeclValidationMessage("B103", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C806"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C807")}));
        }
    }

    private void validaLinha(ValidationResult result, Long ano, Long ano_exercicio, Long ren_prof, Long ren_agri, String quadro_link, String ano_link, String ren_prof_link, String ren_agri_link, ArrayList<String> processedAnos) {
        if (ano != null && ano_exercicio != null && ano < ano_exercicio - 6) {
            result.add(new DeclValidationMessage("B099", quadro_link, new String[]{ano_link}));
        }
        if (ano != null && ano_exercicio != null && ano >= ano_exercicio) {
            result.add(new DeclValidationMessage("B100", quadro_link, new String[]{ano_link, "aRosto.qQuadro02.fq02C02"}));
        }
        if (ano != null && ren_prof == null && ren_agri == null) {
            result.add(new DeclValidationMessage("B101", quadro_link, new String[]{ren_prof_link, ren_agri_link, ano_link}));
        }
        if (ano != null) {
            if (processedAnos.contains(ano.toString())) {
                result.add(new DeclValidationMessage("B102", quadro_link, new String[]{ano_link}));
            } else {
                processedAnos.add(ano.toString());
            }
        }
    }

    private void validaLinhaNETDC(ValidationResult result, Long ano, Long ano_exercicio, Long ren_prof, Long ren_agri, String quadro_link, String ano_link, String ren_prof_link, String ren_agri_link, ArrayList<String> processedAnos) {
        if (ren_prof != null && ano == null) {
            result.add(new DeclValidationMessage("B104", quadro_link, new String[]{ano_link, ren_prof_link}));
        }
        if (ren_agri != null && ano == null) {
            result.add(new DeclValidationMessage("B105", quadro_link, new String[]{ano_link, ren_agri_link}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro08 quadro08Model = anexoBModel.getQuadro08();
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            if (!(quadro08Model.getAnexoBq08C801() != null || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C802()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C803()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C804()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C805()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C806()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C807()))) {
                result.add(new DeclValidationMessage("B093", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801")}));
            }
            if (!(quadro08Model.getAnexoBq08C801() == null || Modelo3IRSValidatorUtil.startsWith(quadro08Model.getAnexoBq08C801(), "1", "2"))) {
                result.add(new DeclValidationMessage("B094", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801")}));
            }
            if (!(quadro08Model.getAnexoBq08C801() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro08Model.getAnexoBq08C801()))) {
                result.add(new DeclValidationMessage("B095", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801")}));
            }
            this.validateB096(result, anexoBModel, rostoModel);
            this.validateB097(model, result, anexoBModel, quadro08Model);
            if (quadro08Model.getAnexoBq08C801() != null && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C802()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C803()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C804()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C805()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C806()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08Model.getAnexoBq08C807())) {
                result.add(new DeclValidationMessage("B098", anexoBModel.getLink("aAnexoB.qQuadro08"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C802"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801")}));
            }
            this.validarAnos(result, model, anexoBModel, quadro08Model);
            this.validateB103(result, anexoBModel, quadro08Model);
            this.validarAnosNETDC(result, model, anexoBModel, quadro08Model);
        }
        return result;
    }
}

