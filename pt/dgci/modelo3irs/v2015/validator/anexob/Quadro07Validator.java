/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private void validateAnexoBq07T1(DeclaracaoModel model, AnexoBModel anexoBModel, pt.dgci.modelo3irs.v2015.model.anexob.Quadro07 quadro07Model, ValidationResult result) {
        ArrayList<String> processedNifs = new ArrayList<String>();
        EventList<AnexoBq07T1_Linha> linhas = quadro07Model.getAnexoBq07T1();
        this.validateB206(result, anexoBModel);
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoBq07T1_Linha linha = linhas.get(i);
            if (linha.getNIF() != null) {
                if (processedNifs.contains(linha.getNIF().toString())) {
                    result.add(new DeclValidationMessage("B085", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink(this.getLinkForNif(i))}));
                } else {
                    processedNifs.add(linha.getNIF().toString());
                }
            }
            if (linha.getNIF() == null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValor())) {
                result.add(new DeclValidationMessage("B086", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink(this.getLinkForLinha(i))}));
            }
            if (!(linha.getNIF() == null || NifValidator.checkFirstDigit(linha.getNIF().toString(), "125679"))) {
                result.add(new DeclValidationMessage("B087", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink(this.getLinkForNif(i))}));
            }
            if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.isNIFValid(linha.getNIF()))) {
                result.add(new DeclValidationMessage("B088", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink(this.getLinkForNif(i))}));
            }
            if ((linha.getNIF() == null || linha.getValor() != null) && (linha.getNIF() != null || linha.getValor() == null)) continue;
            result.add(new DeclValidationMessage("B090", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink(this.getLinkForNif(i)), anexoBModel.getLink(this.getLinkForValor(i))}));
        }
    }

    protected void validateB206(ValidationResult result, AnexoBModel anexoBModel) {
        if (anexoBModel.getQuadro07().getAnexoBq07T1().size() > 2000) {
            result.add(new DeclValidationMessage("B206", anexoBModel.getLink("aAnexoB.qQuadro07.tanexoBq07T1"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro07.tanexoBq07T1")}));
        }
    }

    public String getLinkForLinha(int linhaPos) {
        return AnexoBq07T1_Linha.getLink(linhaPos + 1);
    }

    public String getLinkForNif(int linhaPos) {
        return AnexoBq07T1_Linha.getLink(linhaPos + 1, AnexoBq07T1_LinhaBase.Property.NIF);
    }

    public String getLinkForValor(int linhaPos) {
        return AnexoBq07T1_Linha.getLink(linhaPos + 1, AnexoBq07T1_LinhaBase.Property.VALOR);
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateB091(ValidationResult result, AnexoBModel anexoBModel, pt.dgci.modelo3irs.v2015.model.anexob.Quadro07 quadro07Model, long soma) {
        long campo702;
        long l = campo702 = quadro07Model.getAnexoBq07C702() != null ? quadro07Model.getAnexoBq07C702() : 0;
        if (soma != campo702) {
            result.add(new DeclValidationMessage("B091", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro07.fanexoBq07C702")}));
        }
    }

    protected void validateB089(ValidationResult result, RostoModel rostoModel, AnexoBModel anexoBModel, AnexoBq07T1_Linha current, int numLinha) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(current.getNIF()) && (rostoModel.getQuadro03().isSujeitoPassivoA(current.getNIF()) || rostoModel.getQuadro03().isSujeitoPassivoB(current.getNIF()) || rostoModel.getQuadro03().existsInDependentes(current.getNIF()) || rostoModel.getQuadro07().isConjugeFalecido(current.getNIF()) || rostoModel.getQuadro07().existsInAscendentes(current.getNIF()) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(current.getNIF()) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(current.getNIF()) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(current.getNIF()) || rostoModel.getQuadro07().existsInAscendentesEColaterais(current.getNIF()))) {
            result.add(new DeclValidationMessage("B089", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink(AnexoBq07T1_Linha.getLink(numLinha, AnexoBq07T1_LinhaBase.Property.NIF)), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            pt.dgci.modelo3irs.v2015.model.anexob.Quadro07 quadro07Model = anexoBModel.getQuadro07();
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07Model.getAnexoBq07C702()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro07Model.getAnexoBq07C701())) {
                result.add(new DeclValidationMessage("B084", anexoBModel.getLink("aAnexoB.qQuadro07"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro07.fanexoBq07C701")}));
            }
            this.validateAnexoBq07T1(model, anexoBModel, quadro07Model, result);
        }
        return result;
    }
}

