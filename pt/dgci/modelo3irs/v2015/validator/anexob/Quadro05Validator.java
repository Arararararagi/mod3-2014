/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexob;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    public ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : keys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            Quadro05 quadro5Model = anexoBModel.getQuadro05();
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            if (quadro5Model.getAnexoBq05C501() != null && quadro5Model.getAnexoBq05C501() > 0 && (Modelo3IRSValidatorUtil.isEmptyOrZero(anexoBModel.getQuadro03().getAnexoBq03C10()) || anexoBModel.getQuadro03().getAnexoBq03C10() != 1323)) {
                result.add(new DeclValidationMessage("B079", anexoBModel.getLink("aAnexoB.qQuadro05"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro03.fanexoBq03C10"), anexoBModel.getLink("aAnexoB.qQuadro05.fanexoBq05C501")}));
            }
            if (quadro5Model.getAnexoBq05C501() == null || quadro5Model.getAnexoBq05C501() <= 0 || (rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0) <= 2006) continue;
            result.add(new DeclValidationMessage("B080", anexoBModel.getLink("aAnexoB.qQuadro05"), new String[]{anexoBModel.getLink("aAnexoB.qQuadro05.fanexoBq05C501"), "aRosto.qQuadro02.fq02C02"}));
        }
        return result;
    }
}

