/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.LinkedList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateD071(ValidationResult result, RostoModel rostoModel, Long linhaNIF, String linkNIF) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (rostoModel.getQuadro03().isSujeitoPassivoA(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoB(linhaNIF) || rostoModel.getQuadro03().existsInDependentes(linhaNIF) || rostoModel.getQuadro07().existsInAscendentes(linhaNIF) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(linhaNIF) || rostoModel.getQuadro07().existsInAscendentesEColaterais(linhaNIF) || rostoModel.getQuadro05().isRepresentante(linhaNIF))) {
            result.add(new DeclValidationMessage("D071", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro05.fq05C5"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        if (keys == null || keys.isEmpty()) {
            return result;
        }
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            Quadro07 quadro07 = anexoDModel.getQuadro07();
            if (quadro07.getAnexoDq07C701() == null || quadro07.getAnexoDq07C701() == 0) {
                if (quadro07.getAnexoDq07C702() != null && quadro07.getAnexoDq07C702() > 0 || quadro07.getAnexoDq07C703() != null && quadro07.getAnexoDq07C703() > 0 || quadro07.getAnexoDq07C704() != null && quadro07.getAnexoDq07C704() > 0 || quadro07.getAnexoDq07C705() != null && quadro07.getAnexoDq07C705() > 0 || quadro07.getAnexoDq07C706() != null && quadro07.getAnexoDq07C706() > 0 || quadro07.getAnexoDq07C707() != null && quadro07.getAnexoDq07C707() > 0) {
                    result.add(new DeclValidationMessage("D068", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C701")}));
                }
            } else {
                this.validateD069(result, quadro07, anexoDModel);
                this.validateD070(result, quadro07, anexoDModel);
                this.validateD071(result, rostoModel, quadro07.getAnexoDq07C701(), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C701"));
                this.validateD072(result, quadro07, anexoDModel);
            }
            LinkedList<String> anos = new LinkedList<String>();
            int menorAno = 32767;
            String menorAnoLink = "";
            if (quadro07.getAnexoDq07C702() != null && quadro07.getAnexoDq07C702() > 0) {
                if (quadro07.getAnexoDq07C702() < (long)menorAno) {
                    menorAno = quadro07.getAnexoDq07C702().shortValue();
                    menorAnoLink = "aAnexoD.qQuadro07.fanexoDq07C702";
                }
                anos.add(String.valueOf(quadro07.getAnexoDq07C702()));
                if (anoExercicio != null && quadro07.getAnexoDq07C702() >= anoExercicio) {
                    result.add(new DeclValidationMessage("D074", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C702"), "aRosto.qQuadro02.fq02C02"}));
                }
                if ((quadro07.getAnexoDq07C708() == null || quadro07.getAnexoDq07C708() == 0) && (quadro07.getAnexoDq07C714() == null || quadro07.getAnexoDq07C714() == 0)) {
                    result.add(new DeclValidationMessage("D077", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C708"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C714"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C702")}));
                }
            } else {
                if (quadro07.getAnexoDq07C708() != null && quadro07.getAnexoDq07C708() > 0) {
                    result.add(new DeclValidationMessage("D078", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C702"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C708")}));
                }
                if (quadro07.getAnexoDq07C714() != null && quadro07.getAnexoDq07C714() > 0) {
                    result.add(new DeclValidationMessage("D079", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C702"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C714")}));
                }
            }
            if (quadro07.getAnexoDq07C703() != null && quadro07.getAnexoDq07C703() > 0) {
                if (quadro07.getAnexoDq07C703() < (long)menorAno) {
                    menorAno = quadro07.getAnexoDq07C703().shortValue();
                    menorAnoLink = "aAnexoD.qQuadro07.fanexoDq07C703";
                }
                if (anos.contains(String.valueOf(quadro07.getAnexoDq07C703()))) {
                    result.add(new DeclValidationMessage("D075", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C703")}));
                } else {
                    anos.add(String.valueOf(quadro07.getAnexoDq07C703()));
                }
                if (quadro07.getAnexoDq07C702() == null || quadro07.getAnexoDq07C702() == 0) {
                    result.add(new DeclValidationMessage("D076", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C702"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C703")}));
                }
                if (anoExercicio != null && quadro07.getAnexoDq07C703() >= anoExercicio) {
                    result.add(new DeclValidationMessage("D074", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C703"), "aRosto.qQuadro02.fq02C02"}));
                }
                if ((quadro07.getAnexoDq07C709() == null || quadro07.getAnexoDq07C709() == 0) && (quadro07.getAnexoDq07C715() == null || quadro07.getAnexoDq07C715() == 0)) {
                    result.add(new DeclValidationMessage("D077", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C709"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C715"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C703")}));
                }
            } else {
                if (quadro07.getAnexoDq07C709() != null && quadro07.getAnexoDq07C709() > 0) {
                    result.add(new DeclValidationMessage("D078", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C703"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C709")}));
                }
                if (quadro07.getAnexoDq07C715() != null && quadro07.getAnexoDq07C715() > 0) {
                    result.add(new DeclValidationMessage("D079", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C703"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C715")}));
                }
            }
            if (quadro07.getAnexoDq07C704() != null && quadro07.getAnexoDq07C704() > 0) {
                if (quadro07.getAnexoDq07C704() < (long)menorAno) {
                    menorAno = quadro07.getAnexoDq07C704().shortValue();
                    menorAnoLink = "aAnexoD.qQuadro07.fanexoDq07C704";
                }
                if (anos.contains(String.valueOf(quadro07.getAnexoDq07C704()))) {
                    result.add(new DeclValidationMessage("D075", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C704")}));
                } else {
                    anos.add(String.valueOf(quadro07.getAnexoDq07C704()));
                }
                if (quadro07.getAnexoDq07C703() == null || quadro07.getAnexoDq07C703() == 0) {
                    result.add(new DeclValidationMessage("D076", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C703"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C704")}));
                }
                if (anoExercicio != null && quadro07.getAnexoDq07C704() >= anoExercicio) {
                    result.add(new DeclValidationMessage("D074", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C704"), "aRosto.qQuadro02.fq02C02"}));
                }
                if ((quadro07.getAnexoDq07C710() == null || quadro07.getAnexoDq07C710() == 0) && (quadro07.getAnexoDq07C716() == null || quadro07.getAnexoDq07C716() == 0)) {
                    result.add(new DeclValidationMessage("D077", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C710"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C716"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C704")}));
                }
            } else {
                if (quadro07.getAnexoDq07C710() != null && quadro07.getAnexoDq07C710() > 0) {
                    result.add(new DeclValidationMessage("D078", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C704"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C710")}));
                }
                if (quadro07.getAnexoDq07C716() != null && quadro07.getAnexoDq07C716() > 0) {
                    result.add(new DeclValidationMessage("D079", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C704"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C716")}));
                }
            }
            if (quadro07.getAnexoDq07C705() != null && quadro07.getAnexoDq07C705() > 0) {
                if (quadro07.getAnexoDq07C705() < (long)menorAno) {
                    menorAno = quadro07.getAnexoDq07C705().shortValue();
                    menorAnoLink = "aAnexoD.qQuadro07.fanexoDq07C705";
                }
                if (anos.contains(String.valueOf(quadro07.getAnexoDq07C705()))) {
                    result.add(new DeclValidationMessage("D075", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C705")}));
                } else {
                    anos.add(String.valueOf(quadro07.getAnexoDq07C705()));
                }
                if (quadro07.getAnexoDq07C704() == null || quadro07.getAnexoDq07C704() == 0) {
                    result.add(new DeclValidationMessage("D076", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C704"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C705")}));
                }
                if (anoExercicio != null && quadro07.getAnexoDq07C705() >= anoExercicio) {
                    result.add(new DeclValidationMessage("D074", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C705"), "aRosto.qQuadro02.fq02C02"}));
                }
                if ((quadro07.getAnexoDq07C711() == null || quadro07.getAnexoDq07C711() == 0) && (quadro07.getAnexoDq07C717() == null || quadro07.getAnexoDq07C717() == 0)) {
                    result.add(new DeclValidationMessage("D077", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C711"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C717"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C705")}));
                }
            } else {
                if (quadro07.getAnexoDq07C711() != null && quadro07.getAnexoDq07C711() > 0) {
                    result.add(new DeclValidationMessage("D078", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C705"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C711")}));
                }
                if (quadro07.getAnexoDq07C717() != null && quadro07.getAnexoDq07C717() > 0) {
                    result.add(new DeclValidationMessage("D079", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C705"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C717")}));
                }
            }
            if (quadro07.getAnexoDq07C706() != null && quadro07.getAnexoDq07C706() > 0) {
                if (quadro07.getAnexoDq07C706() < (long)menorAno) {
                    menorAno = quadro07.getAnexoDq07C706().shortValue();
                    menorAnoLink = "aAnexoD.qQuadro07.fanexoDq07C706";
                }
                if (anos.contains(String.valueOf(quadro07.getAnexoDq07C706()))) {
                    result.add(new DeclValidationMessage("D075", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C706")}));
                } else {
                    anos.add(String.valueOf(quadro07.getAnexoDq07C706()));
                }
                if (quadro07.getAnexoDq07C705() == null || quadro07.getAnexoDq07C705() == 0) {
                    result.add(new DeclValidationMessage("D076", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C705"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C706")}));
                }
                if (anoExercicio != null && quadro07.getAnexoDq07C706() >= anoExercicio) {
                    result.add(new DeclValidationMessage("D074", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C706"), "aRosto.qQuadro02.fq02C02"}));
                }
                if ((quadro07.getAnexoDq07C712() == null || quadro07.getAnexoDq07C712() == 0) && (quadro07.getAnexoDq07C718() == null || quadro07.getAnexoDq07C718() == 0)) {
                    result.add(new DeclValidationMessage("D077", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C712"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C718"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C706")}));
                }
            } else {
                if (quadro07.getAnexoDq07C712() != null && quadro07.getAnexoDq07C712() > 0) {
                    result.add(new DeclValidationMessage("D078", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C706"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C712")}));
                }
                if (quadro07.getAnexoDq07C718() != null && quadro07.getAnexoDq07C718() > 0) {
                    result.add(new DeclValidationMessage("D079", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C706"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C718")}));
                }
            }
            if (quadro07.getAnexoDq07C707() != null && quadro07.getAnexoDq07C707() > 0) {
                if (quadro07.getAnexoDq07C707() < (long)menorAno) {
                    menorAno = quadro07.getAnexoDq07C707().shortValue();
                    menorAnoLink = "aAnexoD.qQuadro07.fanexoDq07C707";
                }
                if (anos.contains(String.valueOf(quadro07.getAnexoDq07C707()))) {
                    result.add(new DeclValidationMessage("D075", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C707")}));
                } else {
                    anos.add(String.valueOf(quadro07.getAnexoDq07C707()));
                }
                if (quadro07.getAnexoDq07C706() == null || quadro07.getAnexoDq07C706() == 0) {
                    result.add(new DeclValidationMessage("D076", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C706"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C707")}));
                }
                if (anoExercicio != null && quadro07.getAnexoDq07C707() >= anoExercicio) {
                    result.add(new DeclValidationMessage("D074", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C707"), "aRosto.qQuadro02.fq02C02"}));
                }
                if ((quadro07.getAnexoDq07C713() == null || quadro07.getAnexoDq07C713() == 0) && (quadro07.getAnexoDq07C719() == null || quadro07.getAnexoDq07C719() == 0)) {
                    result.add(new DeclValidationMessage("D077", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C713"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C719"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C707")}));
                }
            } else {
                if (quadro07.getAnexoDq07C713() != null && quadro07.getAnexoDq07C713() > 0) {
                    result.add(new DeclValidationMessage("D078", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C707"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C713")}));
                }
                if (quadro07.getAnexoDq07C719() != null && quadro07.getAnexoDq07C719() > 0) {
                    result.add(new DeclValidationMessage("D079", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C707"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C719")}));
                }
            }
            if (anoExercicio == null || anoExercicio <= 2000 || (long)menorAno >= anoExercicio - 6) continue;
            result.add(new DeclValidationMessage("D073", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink(menorAnoLink)}));
        }
        return result;
    }

    protected void validateD069(ValidationResult result, Quadro07 quadro07, AnexoDModel anexoDModel) {
        if (!NifValidator.checkFirstDigit(String.valueOf(quadro07.getAnexoDq07C701()), "12")) {
            result.add(new DeclValidationMessage("D069", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C701")}));
        }
    }

    protected void validateD070(ValidationResult result, Quadro07 quadro07, AnexoDModel anexoDModel) {
        if (!Modelo3IRSValidatorUtil.isNIFValid(quadro07.getAnexoDq07C701())) {
            result.add(new DeclValidationMessage("D070", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C701")}));
        }
    }

    protected void validateD072(ValidationResult result, Quadro07 quadro07, AnexoDModel anexoDModel) {
        if ((quadro07.getAnexoDq07C702() == null || quadro07.getAnexoDq07C702() == 0) && (quadro07.getAnexoDq07C703() == null || quadro07.getAnexoDq07C703() == 0) && (quadro07.getAnexoDq07C704() == null || quadro07.getAnexoDq07C704() == 0) && (quadro07.getAnexoDq07C705() == null || quadro07.getAnexoDq07C705() == 0) && (quadro07.getAnexoDq07C706() == null || quadro07.getAnexoDq07C706() == 0) && (quadro07.getAnexoDq07C707() == null || quadro07.getAnexoDq07C707() == 0)) {
            result.add(new DeclValidationMessage("D072", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C702"), anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C701")}));
        }
    }
}

