/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    private RostoModel rostoModel;

    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateD010(ValidationResult result, RostoModel rostoModel, Long linhaNIF, String linkNIF) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoA(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoB(linhaNIF) || rostoModel.getQuadro03().existsInDependentes(linhaNIF) || rostoModel.getQuadro07().isConjugeFalecido(linhaNIF) || rostoModel.getQuadro07().existsInAscendentes(linhaNIF) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(linhaNIF) || rostoModel.getQuadro07().existsInAscendentesEColaterais(linhaNIF))) {
            result.add(new DeclValidationMessage("D010", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    private boolean mimicsSujeitoPassivoA(RostoModel rostoModel, boolean hasNif, long nif) {
        boolean hasNifA = rostoModel.getQuadro03().getQ03C03() != null && rostoModel.getQuadro03().getQ03C03() > 0;
        return !hasNifA && !hasNif || hasNifA && hasNif && rostoModel.getQuadro03().getQ03C03() == nif;
    }

    private boolean mimicsSujeitoPassivoB(RostoModel rostoModel, boolean hasNif, long nif) {
        boolean hasNifB = rostoModel.getQuadro03().getQ03C04() != null && rostoModel.getQuadro03().getQ03C04() > 0;
        return !hasNifB && !hasNif || hasNifB && hasNif && rostoModel.getQuadro03().getQ03C04() == nif;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        this.rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        for (FormKey key : keys) {
            long nif;
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            pt.dgci.modelo3irs.v2015.model.anexod.Quadro03 quadro03 = anexoDModel.getQuadro03();
            boolean hasNIF = quadro03.getAnexoDq03C04() != null && quadro03.getAnexoDq03C04() > 0;
            long l = nif = hasNIF ? quadro03.getAnexoDq03C04() : 0;
            if (!this.mimicsSujeitoPassivoA(this.rostoModel, hasNIF, nif)) {
                result.add(new DeclValidationMessage("D005", anexoDModel.getLink("aAnexoD.qQuadro03"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C04"), anexoDModel.getLink("aAnexoD"), "aRosto.qQuadro03.fq03C03"}));
            }
            hasNIF = quadro03.getAnexoDq03C05() != null && quadro03.getAnexoDq03C05() > 0;
            long l2 = nif = hasNIF ? quadro03.getAnexoDq03C05() : 0;
            if (!this.mimicsSujeitoPassivoB(this.rostoModel, hasNIF, nif)) {
                result.add(new DeclValidationMessage("D006", anexoDModel.getLink("aAnexoD.qQuadro03"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C05"), anexoDModel.getLink("aAnexoD"), "aRosto.qQuadro03.fq03C04"}));
            }
            if (quadro03.getAnexoDq03C06() == null || quadro03.getAnexoDq03C06() == 0) {
                result.add(new DeclValidationMessage("D007", anexoDModel.getLink("aAnexoD.qQuadro03"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06")}));
            }
            if (!(quadro03.getAnexoDq03C06() == null || quadro03.getAnexoDq03C06() <= 0 || Modelo3IRSValidatorUtil.isNIFValid(quadro03.getAnexoDq03C06()))) {
                result.add(new DeclValidationMessage("D008", anexoDModel.getLink("aAnexoD.qQuadro03"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06")}));
            }
            if (!(quadro03.getAnexoDq03C06() == null || quadro03.getAnexoDq03C06() <= 0 || Modelo3IRSValidatorUtil.startsWith(quadro03.getAnexoDq03C06(), "1", "2"))) {
                result.add(new DeclValidationMessage("D009", anexoDModel.getLink("aAnexoD.qQuadro03"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06")}));
            }
            this.validateD010(result, this.rostoModel, quadro03.getAnexoDq03C06(), anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06"));
        }
        return result;
    }
}

