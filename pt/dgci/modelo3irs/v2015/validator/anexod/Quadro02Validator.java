/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro02Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String D108 = "D108";

    public Quadro02Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateD108(ValidationResult result, RostoModel rostoModel, Quadro02 quadro02AnexoDModel) {
        if (!Modelo3IRSValidatorUtil.equals(quadro02AnexoDModel.getAnexoDq02C03(), rostoModel.getQuadro02().getQ02C02())) {
            result.add(new DeclValidationMessage("D108", "aAnexoD.qQuadro02.fanexoDq02C03", new String[]{"aAnexoD.qQuadro02.fanexoDq02C03", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            Quadro02 quadro02AnexoDModel = (Quadro02)anexoDModel.getQuadro(Quadro02.class.getSimpleName());
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            this.validateD108(result, rostoModel, quadro02AnexoDModel);
        }
        return result;
    }
}

