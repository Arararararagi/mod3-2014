/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxJ;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.Tuple;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final String ANEXO_D = "AnexoD";

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private boolean existsNif(RostoModel rostoModel, long nif) {
        for (Cat_M3V2015_RostoTitulares titular : rostoModel.getQuadro03().getTodosTitularesAsCatalog()) {
            if (rostoModel.getNIFTitular((String)titular.getValue()) != nif) continue;
            return true;
        }
        return false;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateD013(ValidationResult result, AnexoDModel anexoDModel, Quadro04 quadro04) {
        if ((quadro04.getAnexoDq04C401() == null || quadro04.getAnexoDq04C401() == 0) && (quadro04.getAnexoDq04C401b() != null && quadro04.getAnexoDq04C401b() > 0 || quadro04.getAnexoDq04C401c() != null && quadro04.getAnexoDq04C401c() > 0)) {
            result.add(new DeclValidationMessage("D013", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401")}));
        }
        if ((quadro04.getAnexoDq04C402() == null || quadro04.getAnexoDq04C402() == 0) && (quadro04.getAnexoDq04C402b() != null && quadro04.getAnexoDq04C402b() > 0 || quadro04.getAnexoDq04C402c() != null && quadro04.getAnexoDq04C402c() > 0)) {
            result.add(new DeclValidationMessage("D013", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402")}));
        }
        if ((quadro04.getAnexoDq04C403() == null || quadro04.getAnexoDq04C403() == 0) && (quadro04.getAnexoDq04C403b() != null && quadro04.getAnexoDq04C403b() > 0 || quadro04.getAnexoDq04C403c() != null && quadro04.getAnexoDq04C403c() > 0)) {
            result.add(new DeclValidationMessage("D013", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403")}));
        }
    }

    protected void validateD018(ValidationResult result, AnexoDModel anexoDModel, Quadro04 quadro04) {
        if ((quadro04.getAnexoDq04C401a() == null || quadro04.getAnexoDq04C401a() == 0) && quadro04.getAnexoDq04C401() != null && quadro04.getAnexoDq04C401() > 0) {
            result.add(new DeclValidationMessage("D018", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401a")}));
        }
        if ((quadro04.getAnexoDq04C402a() == null || quadro04.getAnexoDq04C402a() == 0) && quadro04.getAnexoDq04C402() != null && quadro04.getAnexoDq04C402() > 0) {
            result.add(new DeclValidationMessage("D018", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402a")}));
        }
        if ((quadro04.getAnexoDq04C403a() == null || quadro04.getAnexoDq04C403a() == 0) && quadro04.getAnexoDq04C403() != null && quadro04.getAnexoDq04C403() > 0) {
            result.add(new DeclValidationMessage("D018", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403a")}));
        }
    }

    protected void validateD020(ValidationResult result, AnexoDModel anexoDModel, Quadro04 quadro04) {
        if (quadro04.getAnexoDq04C401a() != null && quadro04.getAnexoDq04C401a() > 0 && (quadro04.getAnexoDq04C401() == null || quadro04.getAnexoDq04C401() == 0)) {
            result.add(new DeclValidationMessage("D020", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401a")}));
        }
        if (quadro04.getAnexoDq04C402a() != null && quadro04.getAnexoDq04C402a() > 0 && (quadro04.getAnexoDq04C402() == null || quadro04.getAnexoDq04C402() == 0)) {
            result.add(new DeclValidationMessage("D020", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402a")}));
        }
        if (quadro04.getAnexoDq04C403a() != null && quadro04.getAnexoDq04C403a() > 0 && (quadro04.getAnexoDq04C403() == null || quadro04.getAnexoDq04C403() == 0)) {
            result.add(new DeclValidationMessage("D020", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403a")}));
        }
    }

    protected void validateD028(ValidationResult result, AnexoDModel anexoDModel, Quadro04 quadro04) {
        if (!(quadro04.getAnexoDq04C431() != null && quadro04.getAnexoDq04C431() != 0 || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C431b()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C431c()))) {
            result.add(new DeclValidationMessage("D028", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431")}));
        }
        if (!(quadro04.getAnexoDq04C432() != null && quadro04.getAnexoDq04C432() != 0 || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C432b()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C432c()))) {
            result.add(new DeclValidationMessage("D028", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432")}));
        }
    }

    protected void validateD031(ValidationResult result, AnexoDModel anexoDModel, Quadro04 quadro04) {
        if ((quadro04.getAnexoDq04C431a() == null || quadro04.getAnexoDq04C431a() == 0) && quadro04.getAnexoDq04C431() != null && quadro04.getAnexoDq04C431() > 0) {
            result.add(new DeclValidationMessage("D031", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431a")}));
        }
        if ((quadro04.getAnexoDq04C432a() == null || quadro04.getAnexoDq04C432a() == 0) && quadro04.getAnexoDq04C432() != null && quadro04.getAnexoDq04C432() > 0) {
            result.add(new DeclValidationMessage("D031", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432a")}));
        }
    }

    protected void validateD033(ValidationResult result, AnexoDModel anexoDModel, Quadro04 quadro04) {
        if (quadro04.getAnexoDq04C431a() != null && quadro04.getAnexoDq04C431a() > 0 && (quadro04.getAnexoDq04C431() == null || quadro04.getAnexoDq04C431() == 0)) {
            result.add(new DeclValidationMessage("D033", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431a")}));
        }
        if (quadro04.getAnexoDq04C432a() != null && quadro04.getAnexoDq04C432a() > 0 && (quadro04.getAnexoDq04C432() == null || quadro04.getAnexoDq04C432() == 0)) {
            result.add(new DeclValidationMessage("D033", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432a")}));
        }
    }

    protected void validateD043(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T1_Linha linha, String linkNif, String linkRendimentos, String linkRetencao) {
        if ((linha.getNIF() == null || linha.getNIF() == 0) && (linha.getImputacao() != null && linha.getImputacao() > 0 || linha.getRendimentos() != null && linha.getRendimentos() >= 0 || linha.getRetencao() != null && linha.getRetencao() >= 0)) {
            result.add(new DeclValidationMessage("D043", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNif), anexoDModel.getLink(linkRendimentos), anexoDModel.getLink(linkRetencao)}));
        }
    }

    protected void validateD042(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T1_Linha linha, String linkNif, String linkImputacao, String linkRendimentos) {
        if (linha.getNIF() != null && (linha.getImputacao() == null || linha.getRendimentos() == null)) {
            result.add(new DeclValidationMessage("D042", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNif), anexoDModel.getLink(linkImputacao), anexoDModel.getLink(linkRendimentos)}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        if (keys == null || keys.isEmpty()) {
            return result;
        }
        for (FormKey key : keys) {
            boolean hasLucroHerancaIndivisa_Linha2;
            AnexoDq04T1_Linha linha;
            long anexoDq04C4Value;
            long anexoDq04C2Value;
            long anexoDq04C3Value;
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            Quadro04 quadro04 = anexoDModel.getQuadro04();
            boolean hasMateriaColectavel_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C401b());
            boolean hasValor_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C401c());
            boolean hasAdiantamento_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C401d());
            boolean hasAjustament_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C401e());
            boolean hasMateriaColectavel_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C402b());
            boolean hasValor_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C402c());
            boolean hasAdiantamento_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C402d());
            boolean hasAjustament_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C402e());
            boolean hasMateriaColectavel_Linha3 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C403b());
            boolean hasValor_Linha3 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C403c());
            boolean hasAdiantamento_Linha3 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C403d());
            boolean hasAjustament_Linha3 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C403e());
            boolean hasLucroPrejuizoAce_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C431b());
            boolean hasValorAce_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C431c());
            boolean hasAdiantamentoAce_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C431d());
            boolean hasAjustamentoAce_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C431e());
            boolean hasLucroPrejuizoAce_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C432b());
            boolean hasValorAce_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C432c());
            boolean hasAdiantamentoAce_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C432d());
            boolean hasAjustamentoAce_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C432e());
            boolean hasLucroHerancaIndivisa_Linha1 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C480b());
            boolean bl = hasLucroHerancaIndivisa_Linha2 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoDq04C481b());
            if (!(quadro04.getAnexoDq04C401() == null || quadro04.getAnexoDq04C401() <= 0 || anoExercicio == null || (NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C401()), "59") || anoExercicio < 2008) && (NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C401()), "579") || anoExercicio >= 2008))) {
                result.add(new DeclValidationMessage("D011", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401")}));
            }
            if (!(quadro04.getAnexoDq04C402() == null || quadro04.getAnexoDq04C402() <= 0 || anoExercicio == null || (NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C402()), "59") || anoExercicio < 2008) && (NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C402()), "579") || anoExercicio >= 2008))) {
                result.add(new DeclValidationMessage("D011", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402")}));
            }
            if (!(quadro04.getAnexoDq04C403() == null || quadro04.getAnexoDq04C403() <= 0 || anoExercicio == null || (NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C403()), "59") || anoExercicio < 2008) && (NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C403()), "579") || anoExercicio >= 2008))) {
                result.add(new DeclValidationMessage("D011", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403")}));
            }
            if (!(quadro04.getAnexoDq04C401() == null || quadro04.getAnexoDq04C401() <= 0 || Modelo3IRSValidatorUtil.isNIFValid(quadro04.getAnexoDq04C401()))) {
                result.add(new DeclValidationMessage("D012", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401")}));
            }
            if (!(quadro04.getAnexoDq04C402() == null || quadro04.getAnexoDq04C402() <= 0 || Modelo3IRSValidatorUtil.isNIFValid(quadro04.getAnexoDq04C402()))) {
                result.add(new DeclValidationMessage("D012", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402")}));
            }
            if (!(quadro04.getAnexoDq04C403() == null || quadro04.getAnexoDq04C403() <= 0 || Modelo3IRSValidatorUtil.isNIFValid(quadro04.getAnexoDq04C403()))) {
                result.add(new DeclValidationMessage("D012", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403")}));
            }
            ArrayList<String> nifsRepetidos = new ArrayList<String>();
            if (quadro04.getAnexoDq04C401() != null && quadro04.getAnexoDq04C401() > 0) {
                nifsRepetidos.add(String.valueOf(quadro04.getAnexoDq04C401()));
            }
            if (quadro04.getAnexoDq04C402() != null && quadro04.getAnexoDq04C402() > 0) {
                if (nifsRepetidos.contains(String.valueOf(quadro04.getAnexoDq04C402()))) {
                    result.add(new DeclValidationMessage("D015", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402")}));
                } else {
                    nifsRepetidos.add(String.valueOf(quadro04.getAnexoDq04C402()));
                }
            }
            if (quadro04.getAnexoDq04C403() != null && quadro04.getAnexoDq04C403() > 0) {
                if (nifsRepetidos.contains(String.valueOf(quadro04.getAnexoDq04C403()))) {
                    result.add(new DeclValidationMessage("D015", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403")}));
                } else {
                    nifsRepetidos.add(String.valueOf(quadro04.getAnexoDq04C403()));
                }
            }
            if ((quadro04.getAnexoDq04C401() == null || quadro04.getAnexoDq04C401() == 0) && (quadro04.getAnexoDq04C401a() == null || quadro04.getAnexoDq04C401a() == 0) && (quadro04.getAnexoDq04C401b() == null || quadro04.getAnexoDq04C401b() == 0) && (quadro04.getAnexoDq04C401c() == null || quadro04.getAnexoDq04C401c() == 0) && (quadro04.getAnexoDq04C401d() == null || quadro04.getAnexoDq04C401d() == 0) && (quadro04.getAnexoDq04C402() != null && quadro04.getAnexoDq04C402() > 0 || quadro04.getAnexoDq04C402a() != null && quadro04.getAnexoDq04C402a() >= 0 && quadro04.getAnexoDq04C402b() != null && quadro04.getAnexoDq04C402b() >= 0 && quadro04.getAnexoDq04C402c() != null && quadro04.getAnexoDq04C402c() >= 0 && quadro04.getAnexoDq04C402d() != null && quadro04.getAnexoDq04C402d() >= 0)) {
                result.add(new DeclValidationMessage("D016", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401")}));
            }
            if ((quadro04.getAnexoDq04C402() == null || quadro04.getAnexoDq04C402() == 0) && (quadro04.getAnexoDq04C402a() == null || quadro04.getAnexoDq04C402a() == 0) && (quadro04.getAnexoDq04C402b() == null || quadro04.getAnexoDq04C402b() == 0) && (quadro04.getAnexoDq04C402c() == null || quadro04.getAnexoDq04C402c() == 0) && (quadro04.getAnexoDq04C402d() == null || quadro04.getAnexoDq04C402d() == 0) && (quadro04.getAnexoDq04C403() != null && quadro04.getAnexoDq04C403() > 0 || quadro04.getAnexoDq04C403a() != null && quadro04.getAnexoDq04C403a() >= 0 && quadro04.getAnexoDq04C403b() != null && quadro04.getAnexoDq04C403b() >= 0 && quadro04.getAnexoDq04C403c() != null && quadro04.getAnexoDq04C403c() >= 0 && quadro04.getAnexoDq04C403d() != null && quadro04.getAnexoDq04C403d() >= 0)) {
                result.add(new DeclValidationMessage("D016", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402")}));
            }
            if (quadro04.getAnexoDq04C401a() != null && (quadro04.getAnexoDq04C401a() > 10000 || quadro04.getAnexoDq04C401a() == 0)) {
                result.add(new DeclValidationMessage("D017", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401a")}));
            }
            if (quadro04.getAnexoDq04C402a() != null && (quadro04.getAnexoDq04C402a() > 10000 || quadro04.getAnexoDq04C402a() == 0)) {
                result.add(new DeclValidationMessage("D017", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402a")}));
            }
            if (quadro04.getAnexoDq04C403a() != null && (quadro04.getAnexoDq04C403a() > 10000 || quadro04.getAnexoDq04C403a() == 0)) {
                result.add(new DeclValidationMessage("D017", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403a")}));
            }
            if (quadro04.getAnexoDq04C401b() == null && (quadro04.getAnexoDq04C401() != null || quadro04.getAnexoDq04C401a() != null || quadro04.getAnexoDq04C401c() != null)) {
                result.add(new DeclValidationMessage("D022", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401b")}));
            }
            if (quadro04.getAnexoDq04C402b() == null && (quadro04.getAnexoDq04C402() != null || quadro04.getAnexoDq04C402a() != null || quadro04.getAnexoDq04C402c() != null)) {
                result.add(new DeclValidationMessage("D022", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402b")}));
            }
            if (quadro04.getAnexoDq04C403b() == null && (quadro04.getAnexoDq04C403() != null || quadro04.getAnexoDq04C403a() != null || quadro04.getAnexoDq04C403c() != null)) {
                result.add(new DeclValidationMessage("D022", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403b")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(anoExercicio) || anoExercicio >= 2009)) {
                if (hasAdiantamento_Linha1) {
                    result.add(new DeclValidationMessage("D087", anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401d"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401d")}));
                }
                if (hasAdiantamento_Linha2) {
                    result.add(new DeclValidationMessage("D087", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402d"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402d")}));
                }
                if (hasAdiantamento_Linha3) {
                    result.add(new DeclValidationMessage("D087", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403d"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403d")}));
                }
                if (hasAdiantamentoAce_Linha1) {
                    result.add(new DeclValidationMessage("D087", anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431d"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431d")}));
                }
                if (hasAdiantamentoAce_Linha2) {
                    result.add(new DeclValidationMessage("D087", anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432d"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432d")}));
                }
            }
            if (quadro04.getAnexoDq04C401e() != null && quadro04.getAnexoDq04C401e() > 0 && anoExercicio != null && anoExercicio <= 2009) {
                result.add(new DeclValidationMessage("D023", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401e")}));
            }
            if (quadro04.getAnexoDq04C402e() != null && quadro04.getAnexoDq04C402e() > 0 && anoExercicio != null && anoExercicio <= 2009) {
                result.add(new DeclValidationMessage("D023", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C402e")}));
            }
            if (quadro04.getAnexoDq04C403e() != null && quadro04.getAnexoDq04C403e() > 0 && anoExercicio != null && anoExercicio <= 2009) {
                result.add(new DeclValidationMessage("D023", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C403e")}));
            }
            if (quadro04.getAnexoDq04C431e() != null && quadro04.getAnexoDq04C431e() > 0 && anoExercicio != null && anoExercicio <= 2009) {
                result.add(new DeclValidationMessage("D023", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431e")}));
            }
            if (quadro04.getAnexoDq04C432e() != null && quadro04.getAnexoDq04C432e() > 0 && anoExercicio != null && anoExercicio <= 2009) {
                result.add(new DeclValidationMessage("D023", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432e")}));
            }
            if (!(quadro04.getAnexoDq04C431() == null || quadro04.getAnexoDq04C431() <= 0 || NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C431()), "59"))) {
                result.add(new DeclValidationMessage("D024", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431")}));
            }
            if (!(quadro04.getAnexoDq04C432() == null || quadro04.getAnexoDq04C432() <= 0 || NifValidator.checkFirstDigit(String.valueOf(quadro04.getAnexoDq04C432()), "59"))) {
                result.add(new DeclValidationMessage("D024", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432")}));
            }
            if (!(quadro04.getAnexoDq04C431() == null || quadro04.getAnexoDq04C431() <= 0 || Modelo3IRSValidatorUtil.isNIFValid(quadro04.getAnexoDq04C431()))) {
                result.add(new DeclValidationMessage("D025", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431")}));
            }
            if (!(quadro04.getAnexoDq04C432() == null || quadro04.getAnexoDq04C432() <= 0 || Modelo3IRSValidatorUtil.isNIFValid(quadro04.getAnexoDq04C432()))) {
                result.add(new DeclValidationMessage("D025", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432")}));
            }
            if (quadro04.getAnexoDq04C431() != null && quadro04.getAnexoDq04C431() > 0) {
                if (nifsRepetidos.contains(String.valueOf(quadro04.getAnexoDq04C431()))) {
                    result.add(new DeclValidationMessage("D026", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431")}));
                } else {
                    nifsRepetidos.add(String.valueOf(quadro04.getAnexoDq04C431()));
                }
            }
            if (quadro04.getAnexoDq04C432() != null && quadro04.getAnexoDq04C432() > 0) {
                if (nifsRepetidos.contains(String.valueOf(quadro04.getAnexoDq04C432()))) {
                    result.add(new DeclValidationMessage("D026", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432")}));
                } else {
                    nifsRepetidos.add(String.valueOf(quadro04.getAnexoDq04C432()));
                }
            }
            if ((quadro04.getAnexoDq04C431() == null || quadro04.getAnexoDq04C431() == 0) && (quadro04.getAnexoDq04C431a() == null || quadro04.getAnexoDq04C431a() == 0) && (quadro04.getAnexoDq04C431b() == null || quadro04.getAnexoDq04C431b() == 0) && (quadro04.getAnexoDq04C431c() == null || quadro04.getAnexoDq04C431c() == 0) && (quadro04.getAnexoDq04C431d() == null || quadro04.getAnexoDq04C431d() == 0) && (quadro04.getAnexoDq04C432() != null && quadro04.getAnexoDq04C432() > 0 || quadro04.getAnexoDq04C432a() != null && quadro04.getAnexoDq04C432a() >= 0 && quadro04.getAnexoDq04C432b() != null && quadro04.getAnexoDq04C432b() >= 0 && quadro04.getAnexoDq04C432c() != null && quadro04.getAnexoDq04C432c() >= 0 && quadro04.getAnexoDq04C432d() != null && quadro04.getAnexoDq04C432d() >= 0)) {
                result.add(new DeclValidationMessage("D027", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431")}));
            }
            if (quadro04.getAnexoDq04C431a() != null && (quadro04.getAnexoDq04C431a() > 10000 || quadro04.getAnexoDq04C431a() == 0)) {
                result.add(new DeclValidationMessage("D030", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431a")}));
            }
            if (quadro04.getAnexoDq04C432a() != null && (quadro04.getAnexoDq04C432a() > 10000 || quadro04.getAnexoDq04C432a() == 0)) {
                result.add(new DeclValidationMessage("D030", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432a")}));
            }
            if (quadro04.getAnexoDq04C431b() == null && (quadro04.getAnexoDq04C431() != null || quadro04.getAnexoDq04C431a() != null || quadro04.getAnexoDq04C431c() != null)) {
                result.add(new DeclValidationMessage("D035", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431b")}));
            }
            if (quadro04.getAnexoDq04C432b() == null && (quadro04.getAnexoDq04C432() != null || quadro04.getAnexoDq04C432a() != null || quadro04.getAnexoDq04C432c() != null)) {
                result.add(new DeclValidationMessage("D035", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C432b")}));
            }
            if (!(quadro04.getAnexoDq04T1() == null || quadro04.getAnexoDq04T1().isEmpty() || quadro04.getAnexoDq04T1().size() <= 10)) {
                result.add(new DeclValidationMessage("D037", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.tanexoDq04T1")}));
            }
            if (quadro04.getAnexoDq04T1() != null && quadro04.getAnexoDq04T1().size() == 1 && (linha = quadro04.getAnexoDq04T1().get(0)).getImputacao() != null && linha.getImputacao() == 10000) {
                String linkCurrentLine = "aAnexoD.qQuadro04.tanexoDq04T1.l1";
                String linkImputacao = linkCurrentLine + ".c" + AnexoDq04T1_LinhaBase.Property.IMPUTACAO.getIndex();
                result.add(new DeclValidationMessage("D048", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkImputacao)}));
            }
            this.validateD121(result, anexoDModel, quadro04.getAnexoDq04C480a(), quadro04.getAnexoDq04C480b());
            this.validateD122(result, anexoDModel, quadro04.getAnexoDq04C480a(), quadro04.getAnexoDq04C480b());
            this.validateD123(result, anexoDModel, quadro04.getAnexoDq04C481a(), quadro04.getAnexoDq04C481b());
            this.validateD124(result, anexoDModel, quadro04.getAnexoDq04C481a(), quadro04.getAnexoDq04C481b());
            long somaControloRendLiquidos = 0;
            long somaControloRetencoes = 0;
            ArrayList<String> nifsRepetidosQ04T1 = new ArrayList<String>();
            if (!(quadro04.getAnexoDq04T1() == null || quadro04.getAnexoDq04T1().isEmpty())) {
                for (int numLinha = 0; numLinha < quadro04.getAnexoDq04T1().size(); ++numLinha) {
                    AnexoIModel anexoIModel;
                    AnexoDq04T1_Linha linha2 = quadro04.getAnexoDq04T1().get(numLinha);
                    boolean hasLucroPrejuizo = !Modelo3IRSValidatorUtil.isEmptyOrZero(linha2.getRendimentos());
                    boolean hasValor = !Modelo3IRSValidatorUtil.isEmptyOrZero(linha2.getRetencao());
                    String linkCurrentLine = "aAnexoD.qQuadro04.tanexoDq04T1.l" + (numLinha + 1);
                    String linkNif = linkCurrentLine + ".c" + AnexoDq04T1_LinhaBase.Property.NIF.getIndex();
                    String linkImputacao = linkCurrentLine + ".c" + AnexoDq04T1_LinhaBase.Property.IMPUTACAO.getIndex();
                    if (linha2.getNIF() != null && linha2.getNIF() > 0) {
                        if (!nifsRepetidosQ04T1.contains(String.valueOf(linha2.getNIF()))) {
                            nifsRepetidosQ04T1.add(String.valueOf(linha2.getNIF()));
                        } else {
                            result.add(new DeclValidationMessage("D038", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNif)}));
                        }
                    }
                    if (linha2.isEmpty()) {
                        result.add(new DeclValidationMessage("D039", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNif)}));
                    }
                    if (!(linha2.getNIF() == null || linha2.getNIF() <= 0 || Modelo3IRSValidatorUtil.isNIFValid(linha2.getNIF()))) {
                        result.add(new DeclValidationMessage("D040", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNif)}));
                    }
                    if (!(linha2.getNIF() == null || linha2.getNIF() <= 0 || Modelo3IRSValidatorUtil.startsWith(linha2.getNIF(), "7", "9"))) {
                        result.add(new DeclValidationMessage("D041", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNif)}));
                    }
                    if (linha2.getNIF() != null && linha2.getNIF() > 0 && (linha2.getNIF() == anexoDModel.getQuadro03().getAnexoDq03C06() || this.existsNif(rostoModel, linha2.getNIF()))) {
                        result.add(new DeclValidationMessage("D045", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNif)}));
                    }
                    AnexoIModel anexoI = null;
                    if (linha2.getNIF() != null) {
                        anexoI = ((Modelo3IRSv2015Model)model).getAnexoIByTitular(linha2.getNIF());
                    }
                    RostoModel rosto = (RostoModel)model.getAnexo(RostoModel.class);
                    this.validateD109(result, linha2, anexoDModel, anexoI, numLinha + 1);
                    this.validateD110(result, linha2, anexoDModel, anexoI, rosto, numLinha + 1);
                    Long nifAnexoDq03C06 = anexoDModel.getQuadro03().getAnexoDq03C06();
                    if (nifAnexoDq03C06 != null && (rostoModel.getQuadro03().isSujeitoPassivoA(nifAnexoDq03C06) || rostoModel.getQuadro03().isSujeitoPassivoB(nifAnexoDq03C06)) && (rostoModel.getQuadro06().isQ06B1OP1Selected() || rostoModel.getQuadro06().isQ06B1OP4Selected()) && linha2.getImputacao() != null && (linha2.getImputacao() > 10000 || linha2.getImputacao() == 0)) {
                        result.add(new DeclValidationMessage("D046", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkImputacao)}));
                    }
                    if (nifAnexoDq03C06 != null && rostoModel.getQuadro07().isConjugeFalecido(nifAnexoDq03C06) && linha2.getImputacao() != null && linha2.getImputacao() != 0 && (anexoIModel = ((Modelo3IRSv2015Model)model).getAnexoIByTitular(linha2.getNIF())) != null && anexoIModel.getQuadro07().existNifInQ7C70i1(nifAnexoDq03C06)) {
                        result.add(new DeclValidationMessage("D103", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkImputacao)}));
                    }
                    if (hasLucroPrejuizo) {
                        somaControloRendLiquidos+=linha2.getRendimentos().longValue();
                    }
                    if (!hasValor) continue;
                    somaControloRetencoes+=linha2.getRetencao().longValue();
                }
            }
            if (hasMateriaColectavel_Linha1) {
                somaControloRendLiquidos+=quadro04.getAnexoDq04C401b().longValue();
            }
            if (hasMateriaColectavel_Linha2) {
                somaControloRendLiquidos+=quadro04.getAnexoDq04C402b().longValue();
            }
            if (hasMateriaColectavel_Linha3) {
                somaControloRendLiquidos+=quadro04.getAnexoDq04C403b().longValue();
            }
            if (hasLucroPrejuizoAce_Linha1) {
                somaControloRendLiquidos+=quadro04.getAnexoDq04C431b().longValue();
            }
            if (hasLucroPrejuizoAce_Linha2) {
                somaControloRendLiquidos+=quadro04.getAnexoDq04C432b().longValue();
            }
            if (hasLucroHerancaIndivisa_Linha1) {
                somaControloRendLiquidos+=quadro04.getAnexoDq04C480b().longValue();
            }
            if (hasLucroHerancaIndivisa_Linha2) {
                somaControloRendLiquidos+=quadro04.getAnexoDq04C481b().longValue();
            }
            this.validateYD001(result, anexoDModel, quadro04, somaControloRendLiquidos);
            if (hasValor_Linha1) {
                somaControloRetencoes+=quadro04.getAnexoDq04C401c().longValue();
            }
            if (hasValor_Linha2) {
                somaControloRetencoes+=quadro04.getAnexoDq04C402c().longValue();
            }
            if (hasValor_Linha3) {
                somaControloRetencoes+=quadro04.getAnexoDq04C403c().longValue();
            }
            if (hasValorAce_Linha1) {
                somaControloRetencoes+=quadro04.getAnexoDq04C431c().longValue();
            }
            if (hasValorAce_Linha2) {
                somaControloRetencoes+=quadro04.getAnexoDq04C432c().longValue();
            }
            long l = anexoDq04C2Value = quadro04.getAnexoDq04C2() != null ? quadro04.getAnexoDq04C2() : 0;
            if (anexoDq04C2Value != somaControloRetencoes) {
                result.add(new DeclValidationMessage("YD002", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C2")}));
            }
            long somaControloAdiantamentos = 0;
            if (hasAdiantamento_Linha1) {
                somaControloAdiantamentos+=quadro04.getAnexoDq04C401d().longValue();
            }
            if (hasAdiantamento_Linha2) {
                somaControloAdiantamentos+=quadro04.getAnexoDq04C402d().longValue();
            }
            if (hasAdiantamento_Linha3) {
                somaControloAdiantamentos+=quadro04.getAnexoDq04C403d().longValue();
            }
            if (hasAdiantamentoAce_Linha1) {
                somaControloAdiantamentos+=quadro04.getAnexoDq04C431d().longValue();
            }
            if (hasAdiantamentoAce_Linha2) {
                somaControloAdiantamentos+=quadro04.getAnexoDq04C432d().longValue();
            }
            long l2 = anexoDq04C3Value = quadro04.getAnexoDq04C3() != null ? quadro04.getAnexoDq04C3() : 0;
            if (anexoDq04C3Value != somaControloAdiantamentos) {
                result.add(new DeclValidationMessage("YD003", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C3")}));
            }
            long somaControloAjustamentos = 0;
            if (hasAjustament_Linha1) {
                somaControloAjustamentos+=quadro04.getAnexoDq04C401e().longValue();
            }
            if (hasAjustament_Linha2) {
                somaControloAjustamentos+=quadro04.getAnexoDq04C402e().longValue();
            }
            if (hasAjustament_Linha3) {
                somaControloAjustamentos+=quadro04.getAnexoDq04C403e().longValue();
            }
            if (hasAjustamentoAce_Linha1) {
                somaControloAjustamentos+=quadro04.getAnexoDq04C431e().longValue();
            }
            if (hasAjustamentoAce_Linha2) {
                somaControloAjustamentos+=quadro04.getAnexoDq04C432e().longValue();
            }
            long l3 = anexoDq04C4Value = quadro04.getAnexoDq04C4() != null ? quadro04.getAnexoDq04C4() : 0;
            if (anexoDq04C4Value != somaControloAjustamentos) {
                result.add(new DeclValidationMessage("YD004", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C4")}));
            }
            this.validateAnexoDq04T2(anexoDModel, result);
        }
        return result;
    }

    protected void validateD121(ValidationResult result, AnexoDModel anexoDModel, Long anexoDq04C480a, Long anexoDq04C480b) {
        if (anexoDq04C480b != null && anexoDq04C480b > 0 && (anexoDq04C480a == null || anexoDq04C480a == 0)) {
            result.add(new DeclValidationMessage("D121", anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C480a"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C480a")}));
        }
    }

    protected void validateD122(ValidationResult result, AnexoDModel anexoDModel, Long anexoDq04C480a, Long anexoDq04C480b) {
        if (anexoDq04C480b == null && anexoDq04C480a != null && anexoDq04C480a > 0) {
            result.add(new DeclValidationMessage("D122", anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C480b"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C480b")}));
        }
    }

    protected void validateD123(ValidationResult result, AnexoDModel anexoDModel, Long anexoDq04C481a, Long anexoDq04C481b) {
        if (anexoDq04C481b != null && anexoDq04C481b > 0 && (anexoDq04C481a == null || anexoDq04C481a == 0)) {
            result.add(new DeclValidationMessage("D123", anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C481a"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C481a")}));
        }
    }

    protected void validateD124(ValidationResult result, AnexoDModel anexoDModel, Long anexoDq04C481a, Long anexoDq04C481b) {
        if (anexoDq04C481b == null && anexoDq04C481a != null && anexoDq04C481a > 0) {
            result.add(new DeclValidationMessage("D124", anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C481b"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C481b")}));
        }
    }

    protected void validateYD001(ValidationResult result, AnexoDModel anexoDModel, Quadro04 quadro04, long somaControloRendLiquidos) {
        long anexoDq04C1Value;
        long l = anexoDq04C1Value = quadro04.getAnexoDq04C1() != null ? quadro04.getAnexoDq04C1() : 0;
        if (anexoDq04C1Value != somaControloRendLiquidos) {
            result.add(new DeclValidationMessage("YD001", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C1")}));
        }
    }

    private void validateAnexoDq04T2(AnexoDModel anexoDModel, ValidationResult result) {
        Quadro04 quadro04 = anexoDModel.getQuadro04();
        EventList<AnexoDq04T2_Linha> anexoDq04T2 = quadro04.getAnexoDq04T2();
        ArrayList<Tuple> processedTuples = new ArrayList<Tuple>();
        this.validateD125(result, anexoDModel);
        for (int i = 0; i < anexoDq04T2.size(); ++i) {
            AnexoDq04T2_Linha linha = anexoDq04T2.get(i);
            String lineLink = anexoDModel.getLink(this.getLinkForLinhaT2(i));
            String linkCampoQuadro4 = lineLink + ".c" + AnexoDq04T2_LinhaBase.Property.CAMPOQ4.getIndex();
            String linkCodigoDoPais = lineLink + ".c" + AnexoDq04T2_LinhaBase.Property.CODIGODOPAIS.getIndex();
            String linkMontanteDoRendimento = lineLink + ".c" + AnexoDq04T2_LinhaBase.Property.MONTANTEDORENDIMENTO.getIndex();
            String linkValor = lineLink + ".c" + AnexoDq04T2_LinhaBase.Property.VALOR.getIndex();
            this.validateD112(result, anexoDModel, linha, lineLink);
            this.validateD113(result, anexoDModel, linha, linkCampoQuadro4, linkCodigoDoPais, processedTuples);
            this.validateD115(result, anexoDModel, linha, linkCampoQuadro4);
            this.validateD116(result, anexoDModel, linha, linkCampoQuadro4, linkCodigoDoPais, linkMontanteDoRendimento);
            this.validateD117(result, anexoDModel, linha, linkCampoQuadro4);
            this.validateD118(result, anexoDModel, linha, linkCampoQuadro4);
            this.validateD119(result, anexoDModel, linha, linkCodigoDoPais);
            this.validateD120(result, anexoDModel, linha, linkValor);
        }
        String linkRendimentosLiquidosImputados = anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C1");
        String linkQuadro04 = anexoDModel.getLink("aAnexoD.qQuadro04.tanexoDq04T2");
        this.validateD114(result, anexoDModel, linkQuadro04, linkRendimentosLiquidosImputados);
    }

    private String getLinkForLinhaT2(int linhaPos) {
        return AnexoDq04T2_Linha.getLink(linhaPos + 1);
    }

    protected void validateD112(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String lineLink) {
        if (linha != null && linha.isEmpty()) {
            result.add(new DeclValidationMessage("D112", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{lineLink}));
        }
    }

    protected void validateD113(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String linkCampoQ4, String linkCodigoDoPais, List<Tuple> processedTuples) {
        if (linha != null && linha.getCampoQ4() != null && linha.getCodigoDoPais() != null) {
            long campoQ4 = linha.getCampoQ4();
            long codigoDoPais = linha.getCodigoDoPais();
            Tuple tuple = new Tuple(new Object[]{campoQ4, codigoDoPais});
            if (processedTuples.contains(tuple)) {
                result.add(new DeclValidationMessage("D113", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{linkCampoQ4, linkCodigoDoPais}));
            } else {
                processedTuples.add(tuple);
            }
        }
    }

    protected void validateD114(ValidationResult result, AnexoDModel anexoDModel, String linkMontanteDoRendimento, String linkRendimentosLiquidosImputados) {
        Quadro04 quadro04 = anexoDModel.getQuadro04();
        EventList<AnexoDq04T2_Linha> anexoDq04T2 = quadro04.getAnexoDq04T2();
        Long soma = 0;
        if (!anexoDq04T2.isEmpty()) {
            Long somaRendimentosLiquidosImputadosQuadro4 = anexoDModel.getQuadro04().getAnexoDq04C1() != null ? anexoDModel.getQuadro04().getAnexoDq04C1() : 0;
            for (int i = 0; i < anexoDq04T2.size(); ++i) {
                soma = soma + (anexoDq04T2.get(i).getMontanteDoRendimento() != null ? anexoDq04T2.get(i).getMontanteDoRendimento() : 0);
            }
            if (soma > somaRendimentosLiquidosImputadosQuadro4) {
                result.add(new DeclValidationMessage("D114", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{linkMontanteDoRendimento, linkRendimentosLiquidosImputados}));
            }
        }
    }

    protected void validateD125(ValidationResult result, AnexoDModel anexoDModel) {
        if (anexoDModel.getQuadro04().getAnexoDq04T2().size() > 10) {
            result.add(new DeclValidationMessage("D125", anexoDModel.getLink("aAnexoD.qQuadro04.tanexoDq04T2"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.tanexoDq04T2")}));
        }
    }

    protected void validateD115(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String linkCampoQ4) {
        HashSet<Long> supportedCodes = new HashSet<Long>(Arrays.asList(401, 402, 403));
        if (!(linha.getCampoQ4() == null || supportedCodes.contains(linha.getCampoQ4()))) {
            result.add(new DeclValidationMessage("D115", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{linkCampoQ4}));
        }
    }

    protected void validateD116(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String linkNumeroDoCampoQ4, String linkCodigoDoPais, String linkMontanteDoRendimento) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) && (linha.getCodigoDoPais() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMontanteDoRendimento()))) {
            result.add(new DeclValidationMessage("D116", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNumeroDoCampoQ4), anexoDModel.getLink(linkCodigoDoPais), anexoDModel.getLink(linkMontanteDoRendimento)}));
        }
    }

    protected void validateD117(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String linkNumeroDoCampoQ4) {
        if (!(!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || linha.getCodigoDoPais() == null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getMontanteDoRendimento()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValor()))) {
            result.add(new DeclValidationMessage("D117", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNumeroDoCampoQ4)}));
        }
    }

    protected void validateD118(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String linkNumeroDoCampoQ4) {
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ4()) || anexoDModel.getQuadro04().isMontanteRendimentoCampoPreenchido(linha.getCampoQ4()))) {
            result.add(new DeclValidationMessage("D118", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkNumeroDoCampoQ4), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C" + linha.getCampoQ4())}));
        }
    }

    protected void validateD119(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String linkCodigoDoPais) {
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxJ.class.getSimpleName());
        if (!(linha.getCodigoDoPais() == null || paisesCatalog.containsKey(linha.getCodigoDoPais()))) {
            result.add(new DeclValidationMessage("D119", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkCodigoDoPais)}));
        }
    }

    protected void validateD120(ValidationResult result, AnexoDModel anexoDModel, AnexoDq04T2_Linha linha, String linkImpostoPagoNoEstrangeiro) {
        Long impostoPagoNoEstrangeiro = linha.getValor() != null ? linha.getValor() : 0;
        Long montanteDeRendimento = linha.getMontanteDoRendimento() != null ? linha.getMontanteDoRendimento() : 0;
        double fracao = 0.5;
        if ((double)impostoPagoNoEstrangeiro.longValue() > (double)montanteDeRendimento.longValue() * 0.5) {
            result.add(new DeclValidationMessage("D120", anexoDModel.getLink("aAnexoD.qQuadro04"), new String[]{anexoDModel.getLink(linkImpostoPagoNoEstrangeiro)}));
        }
    }

    public void validateD109(ValidationResult result, AnexoDq04T1_Linha current, AnexoDModel anexoD, AnexoIModel anexoI, int numLinha) {
        Long anexoIQ4c05;
        Long nif = current.getNIF();
        Long anexoDQ3c06 = anexoD.getQuadro03().getAnexoDq03C06();
        Long l = anexoIQ4c05 = anexoI != null ? anexoI.getQuadro04().getAnexoIq04C05() : null;
        if (nif != null && anexoI != null && anexoIQ4c05 != null && anexoDQ3c06 != null && anexoIQ4c05.equals(nif) && !anexoI.getQuadro07().existNifInQ7C70i1(anexoDQ3c06)) {
            String linkNIF = anexoD.getLink(AnexoDq04T1_Linha.getLink(numLinha, AnexoDq04T1_LinhaBase.Property.NIF));
            String linkAnexoI = anexoI != null ? anexoI.getLink("aAnexoI.qQuadro07") : "aAnexoI.qQuadro07";
            result.add(new DeclValidationMessage("D109", linkNIF, new String[]{linkNIF, linkAnexoI}));
        }
    }

    public void validateD110(ValidationResult result, AnexoDq04T1_Linha current, AnexoDModel anexoD, AnexoIModel anexoI, RostoModel rosto, int numLinha) {
        long nifFalecido;
        long anexoDQ3c06;
        long nif = current.getNIF() != null ? current.getNIF() : 0;
        long l = anexoDQ3c06 = anexoD != null && anexoD.getQuadro03().getAnexoDq03C06() != null ? anexoD.getQuadro03().getAnexoDq03C06() : 0;
        long anexoIQ4c05 = anexoI != null ? (anexoI.getQuadro04().getAnexoIq04C05() != null ? anexoI.getQuadro04().getAnexoIq04C05() : 0) : 0;
        long l2 = nifFalecido = rosto != null && rosto.getQuadro07().getQ07C1() != null ? rosto.getQuadro07().getQ07C1() : 0;
        if (nif > 0 && anexoI != null && anexoIQ4c05 > 0 && anexoDQ3c06 > 0 && anexoIQ4c05 == nif && anexoDQ3c06 == nifFalecido) {
            String linkNIF = anexoD.getLink(AnexoDq04T1_Linha.getLink(numLinha, AnexoDq04T1_LinhaBase.Property.NIF));
            result.add(new DeclValidationMessage("D110", linkNIF, new String[0]));
        }
    }
}

