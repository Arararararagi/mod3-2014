/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro08Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro08Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        if (keys == null || keys.isEmpty()) {
            return result;
        }
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            Quadro08 quadro08 = anexoDModel.getQuadro08();
            Quadro04 quadro04 = anexoDModel.getQuadro04();
            if (quadro08.getAnexoDq08C801() == null || quadro08.getAnexoDq08C801() <= 0) continue;
            if (quadro04.getAnexoDq04T1() == null || quadro04.getAnexoDq04T1().isEmpty()) {
                result.add(new DeclValidationMessage("D080", anexoDModel.getLink("aAnexoD.qQuadro08"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.tanexoDq04T1")}));
            }
            if (anoExercicio == null || anoExercicio >= 2005) continue;
            result.add(new DeclValidationMessage("D081", anexoDModel.getLink("aAnexoD.qQuadro08"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro08.fanexoDq08C801"), "aRosto.qQuadro02.fq02C02"}));
        }
        return result;
    }
}

