/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro01Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro01Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            Quadro01 quadro01Model = anexoDModel.getQuadro01();
            if (anexoDModel.getQuadro01().isEmpty()) {
                result.add(new DeclValidationMessage("D003", anexoDModel.getLink("aAnexoD.qQuadro01"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B2"), anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B1")}));
            }
            if (!(quadro01Model.getAnexoDq01B1() == null || quadro01Model.getAnexoDq01B1().equals(Boolean.TRUE) || quadro01Model.getAnexoDq01B1().equals(Boolean.FALSE))) {
                result.add(new DeclValidationMessage("D092", anexoDModel.getLink(anexoDModel.getLink("aAnexoD.qQuadro01")), new String[]{anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B1")}));
            }
            if (quadro01Model.getAnexoDq01B2() == null || quadro01Model.getAnexoDq01B2().equals(Boolean.TRUE) || quadro01Model.getAnexoDq01B2().equals(Boolean.FALSE)) continue;
            result.add(new DeclValidationMessage("D093", anexoDModel.getLink(anexoDModel.getLink("aAnexoD.qQuadro01")), new String[]{anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B2")}));
        }
        return result;
    }
}

