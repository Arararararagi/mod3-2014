/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro03;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class AnexoDValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoDValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateD002(ValidationResult result, DeclaracaoModel model) {
        ArrayList<FormKey> processedKeys = new ArrayList<FormKey>();
        HashMap<Long, String> processedNifs = new HashMap<Long, String>();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        for (FormKey key : keys) {
            AnexoDModel otherModel;
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            if (processedKeys.contains(key)) {
                otherModel = (AnexoDModel)model.getAnexo((FormKey)processedKeys.get(processedKeys.indexOf(key)));
                if (otherModel == null) {
                    otherModel = anexoDModel;
                }
                result.add(new DeclValidationMessage("D002", anexoDModel.getLink("aAnexoD"), new String[]{otherModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06"), anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06")}));
            } else {
                processedKeys.add(key);
            }
            if (anexoDModel.getQuadro03().getAnexoDq03C06() == null) continue;
            if (processedNifs.get(anexoDModel.getQuadro03().getAnexoDq03C06()) != null) {
                otherModel = (AnexoDModel)model.getAnexo(new FormKey(AnexoDModel.class, (String)processedNifs.get(anexoDModel.getQuadro03().getAnexoDq03C06())));
                if (otherModel == null) {
                    otherModel = anexoDModel;
                }
                result.add(new DeclValidationMessage("D002", anexoDModel.getLink("aAnexoD"), new String[]{otherModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06"), anexoDModel.getLink("aAnexoD.qQuadro03.fanexoDq03C06")}));
                continue;
            }
            processedNifs.put(anexoDModel.getQuadro03().getAnexoDq03C06(), key.getSubId());
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            this.validateD001(result, anexoDModel);
            if (Modelo3IRSv2015Parameters.instance().isFase2()) continue;
            result.add(new DeclValidationMessage("D101", anexoDModel.getLink("aAnexoD"), new String[]{anexoDModel.getLink("aAnexoD")}));
        }
        this.validateD002(result, model);
        return result;
    }

    protected void validateD001(ValidationResult result, AnexoDModel anexoDModel) {
        if (anexoDModel.isEmpty()) {
            result.add(new DeclValidationMessage("D001", anexoDModel.getLink("aAnexoD"), new String[]{anexoDModel.getLink("aAnexoD")}));
        }
    }
}

