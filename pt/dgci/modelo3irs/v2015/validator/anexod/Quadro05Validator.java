/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro05;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateD065(ValidationResult result, AnexoDModel anexoDModel) {
        if (!(anexoDModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoDModel.getQuadro05().getAnexoDq05C501()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoDModel.getQuadro05().getAnexoDq05C502()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoDModel.getQuadro05().getAnexoDq05C503()) || anexoDModel.getQuadro01().isAnexoDq01B1Selected())) {
            result.add(new DeclValidationMessage("D065", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05"), anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B1")}));
        }
    }

    protected void validateD111(ValidationResult result, AnexoDModel anexoDModel) {
        if (!(anexoDModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoDModel.getQuadro05().getAnexoDq05C505()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoDModel.getQuadro05().getAnexoDq05C506()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoDModel.getQuadro05().getAnexoDq05C507()) || anexoDModel.getQuadro01().isAnexoDq01B2Selected())) {
            result.add(new DeclValidationMessage("D111", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05"), anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B2")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        if (keys == null || keys.isEmpty()) {
            return result;
        }
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            Quadro05 quadro05 = anexoDModel.getQuadro05();
            Quadro01 quadro01 = anexoDModel.getQuadro01();
            Quadro04 quadro04 = anexoDModel.getQuadro04();
            if (!(quadro05.getAnexoDq05C501() == null || quadro05.getAnexoDq05C501() <= 0 || quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("D051", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C501"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            if (!(quadro05.getAnexoDq05C502() == null || quadro05.getAnexoDq05C502() <= 0 || quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("D052", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C502"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            if (!(quadro05.getAnexoDq05C503() == null || quadro05.getAnexoDq05C503() <= 0 || quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("D053", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C503"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            if (!(quadro05.getAnexoDq05C504() == null || quadro05.getAnexoDq05C504() <= 0 || quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("D054", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C504"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            if (!(quadro05.getAnexoDq05C505() == null || quadro05.getAnexoDq05C505() <= 0 || quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("D055", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C505"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            if (!(quadro05.getAnexoDq05C506() == null || quadro05.getAnexoDq05C506() <= 0 || quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("D056", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C506"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            if (!(quadro05.getAnexoDq05C507() == null || quadro05.getAnexoDq05C507() <= 0 || quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("D057", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C507"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            if (!(quadro05.getAnexoDq05C508() == null || quadro05.getAnexoDq05C508() <= 0 || quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("D058", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C508"), anexoDModel.getLink("aAnexoD.qQuadro01")}));
            }
            boolean hasAdiantamentosLucrosPrejuizos = quadro04.hasAdiantamentosLucroPrejuizo();
            long lucrosPrejuizosQ4 = quadro04.getLucro() + quadro04.getPrejuizo();
            long lucrosQ5 = quadro05.getLucro();
            long prejuizosQ5 = quadro05.getPrejuizo();
            if (quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue() && quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue() && lucrosPrejuizosQ4 != lucrosQ5 - prejuizosQ5 && !hasAdiantamentosLucrosPrejuizos) {
                result.add(new DeclValidationMessage("D059", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C431b"), anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C502")}));
            }
            if (quadro05.getAnexoDq05C502() != null && quadro05.getAnexoDq05C502() > 0 && quadro05.getAnexoDq05C503() != null && quadro05.getAnexoDq05C503() > 0) {
                result.add(new DeclValidationMessage("D060", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C502"), anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C503")}));
            }
            if (quadro05.getAnexoDq05C507() != null && quadro05.getAnexoDq05C507() > 0 && quadro05.getAnexoDq05C506() != null && quadro05.getAnexoDq05C506() > 0) {
                result.add(new DeclValidationMessage("D061", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C506"), anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C507")}));
            }
            boolean hasAdiantamentosMateriaColectavel = quadro04.hasAdiantamentosMateriaColectavel();
            long materiaQ5 = quadro05.getMateriaColectavel();
            long materiaQ4 = quadro04.getMateriaColectavel();
            if (!(materiaQ5 <= 0 || materiaQ4 == materiaQ5 || hasAdiantamentosMateriaColectavel)) {
                result.add(new DeclValidationMessage("D062", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401b"), anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C501")}));
            }
            if (quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue() && quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue() && materiaQ4 != materiaQ5 && !hasAdiantamentosMateriaColectavel) {
                result.add(new DeclValidationMessage("D063", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C501"), anexoDModel.getLink("aAnexoD.qQuadro04.fanexoDq04C401b")}));
            }
            this.validateD064(result, anexoDModel);
            this.validateD065(result, anexoDModel);
            this.validateD111(result, anexoDModel);
            if (quadro05.getAnexoDq05C504() == null && quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue()) {
                result.add(new DeclValidationMessage("D082", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C504")}));
            }
            if (quadro05.getAnexoDq05C508() == null && quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue()) {
                result.add(new DeclValidationMessage("D083", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C508")}));
            }
            long materiaColectavelQ04 = quadro04.getMateriaColectavel();
            long materiaColectavelQ05 = quadro05.getMateriaColectavel();
            if (quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue() && quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue() && hasAdiantamentosMateriaColectavel && materiaColectavelQ05 != materiaColectavelQ04) {
                result.add(new DeclValidationMessage("D084", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05"), anexoDModel.getLink("aAnexoD.qQuadro04")}));
            }
            long prejuizoQ5 = quadro05.getPrejuizo();
            long prejuizoQ4 = quadro04.getPrejuizo();
            if (quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue() && quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue() && hasAdiantamentosLucrosPrejuizos && (prejuizoQ5*=-1) != prejuizoQ4) {
                result.add(new DeclValidationMessage("D085", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05"), anexoDModel.getLink("aAnexoD.qQuadro04")}));
            }
            long lucroQ5 = quadro05.getLucro();
            long lucroQ4 = quadro04.getLucro();
            if (quadro01.getAnexoDq01B1() == null || !quadro01.getAnexoDq01B1().booleanValue() || quadro01.getAnexoDq01B2() == null || !quadro01.getAnexoDq01B2().booleanValue() || !hasAdiantamentosLucrosPrejuizos || lucroQ5 == lucroQ4) continue;
            result.add(new DeclValidationMessage("D086", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05"), anexoDModel.getLink("aAnexoD.qQuadro04")}));
        }
        return result;
    }

    protected void validateD064(ValidationResult result, AnexoDModel anexoDModel) {
        if (anexoDModel != null) {
            Quadro05 quadro05 = anexoDModel.getQuadro05();
            Quadro04 quadro04 = anexoDModel.getQuadro04();
            Quadro01 quadro01 = anexoDModel.getQuadro01();
            long anexoDQ04C480_2 = quadro04.getAnexoDq04C480b() != null ? quadro04.getAnexoDq04C480b() : 0;
            long anexoDQ04C481_2 = quadro04.getAnexoDq04C481b() != null ? quadro04.getAnexoDq04C481b() : 0;
            long rendimentoIliquido = quadro05.getRendimentoIliquido();
            long rendimentoLiquidoPositivo = quadro04.getRendimentoLiquidoPositivo() + anexoDQ04C480_2 + anexoDQ04C481_2;
            if ((quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().booleanValue() || quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().booleanValue()) && rendimentoIliquido < rendimentoLiquidoPositivo) {
                result.add(new DeclValidationMessage("D064", anexoDModel.getLink("aAnexoD.qQuadro05"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C504"), anexoDModel.getLink("aAnexoD.qQuadro05.fanexoDq05C508"), anexoDModel.getLink("aAnexoD.qQuadro04")}));
            }
        }
    }
}

