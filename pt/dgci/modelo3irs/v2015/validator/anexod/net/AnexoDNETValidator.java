/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.AnexoDValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro01NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro06NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro07NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.net.Quadro08NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoDNETValidator
extends AnexoDValidator {
    public AnexoDNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro01NETValidator().validate(model));
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        result.addAllFrom(new Quadro07NETValidator().validate(model));
        result.addAllFrom(new Quadro08NETValidator().validate(model));
        return result;
    }
}

