/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.Quadro04Validator;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        if (keys == null || keys.isEmpty()) {
            return result;
        }
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            Quadro04 quadro04 = anexoDModel.getQuadro04();
            this.validateD013(result, anexoDModel, quadro04);
            this.validateD018(result, anexoDModel, quadro04);
            this.validateD020(result, anexoDModel, quadro04);
            this.validateD028(result, anexoDModel, quadro04);
            this.validateD031(result, anexoDModel, quadro04);
            this.validateD033(result, anexoDModel, quadro04);
            if (quadro04.getAnexoDq04T1() == null || quadro04.getAnexoDq04T1().isEmpty()) continue;
            for (int k = 0; k < quadro04.getAnexoDq04T1().size(); ++k) {
                AnexoDq04T1_Linha linha = quadro04.getAnexoDq04T1().get(k);
                String linkCurrentLine = "aAnexoD.qQuadro04.tanexoDq04T1.l" + (k + 1);
                String linkNif = linkCurrentLine + ".c" + AnexoDq04T1_LinhaBase.Property.NIF.getIndex();
                String linkImputacao = linkCurrentLine + ".c" + AnexoDq04T1_LinhaBase.Property.IMPUTACAO.getIndex();
                String linkRendimentos = linkCurrentLine + ".c" + AnexoDq04T1_LinhaBase.Property.RENDIMENTOS.getIndex();
                String linkRetencao = linkCurrentLine + ".c" + AnexoDq04T1_LinhaBase.Property.RETENCAO.getIndex();
                this.validateD042(result, anexoDModel, linha, linkNif, linkImputacao, linkRendimentos);
                this.validateD043(result, anexoDModel, linha, linkNif, linkRendimentos, linkRetencao);
            }
        }
        return result;
    }
}

