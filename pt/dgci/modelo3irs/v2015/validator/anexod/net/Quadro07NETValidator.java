/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro07;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.Quadro07Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro07NETValidator
extends Quadro07Validator {
    public Quadro07NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoD");
        if (keys == null || keys.isEmpty()) {
            return result;
        }
        for (FormKey key : keys) {
            AnexoDModel anexoDModel = (AnexoDModel)model.getAnexo(key);
            AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(anexoDModel.getAnexoDTitular());
            AnexoCModel anexoCModel = ((Modelo3IRSv2015Model)model).getAnexoCByTitular(anexoDModel.getAnexoDTitular());
            Quadro01 quadro01 = anexoDModel.getQuadro01();
            Quadro07 quadro07 = anexoDModel.getQuadro07();
            this.validateD105(result, anexoBModel, anexoCModel, anexoDModel, quadro07);
            this.validateD106(result, anexoDModel, quadro01, quadro07);
            this.validateD107(result, anexoDModel, quadro01, quadro07);
        }
        return result;
    }

    protected void validateD105(ValidationResult result, AnexoBModel anexoBModel, AnexoCModel anexoCModel, AnexoDModel anexoDModel, Quadro07 quadro07) {
        long c701;
        if (quadro07.getAnexoDq07C701() != null && anexoBModel != null) {
            long anxBc801;
            c701 = quadro07.getAnexoDq07C701();
            long l = anxBc801 = anexoBModel.getQuadro08().getAnexoBq08C801() != null ? anexoBModel.getQuadro08().getAnexoBq08C801() : 0;
            if (c701 == anxBc801) {
                result.add(new DeclValidationMessage("D105", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C701")}));
            }
        }
        if (quadro07.getAnexoDq07C701() != null && anexoCModel != null) {
            long anxC901;
            c701 = quadro07.getAnexoDq07C701();
            long l = anxC901 = anexoCModel.getQuadro09().getAnexoCq09C901() != null ? anexoCModel.getQuadro09().getAnexoCq09C901() : 0;
            if (c701 == anxC901) {
                result.add(new DeclValidationMessage("D105", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink("aAnexoD.qQuadro07.fanexoDq07C701")}));
            }
        }
    }

    protected void validateD106(ValidationResult result, AnexoDModel anexoDModel, Quadro01 quadro01, Quadro07 quadro07) {
        if ((quadro07.getAnexoDq07C708() != null || quadro07.getAnexoDq07C709() != null || quadro07.getAnexoDq07C710() != null || quadro07.getAnexoDq07C711() != null || quadro07.getAnexoDq07C712() != null || quadro07.getAnexoDq07C713() != null) && (quadro01.getAnexoDq01B1() == null || quadro01.getAnexoDq01B1() != null && quadro01.getAnexoDq01B1().equals(Boolean.FALSE))) {
            result.add(new DeclValidationMessage("D106", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink(anexoDModel.getLink("aAnexoD.qQuadro07")), anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B1")}));
        }
    }

    protected void validateD107(ValidationResult result, AnexoDModel anexoDModel, Quadro01 quadro01, Quadro07 quadro07) {
        if ((quadro07.getAnexoDq07C714() != null || quadro07.getAnexoDq07C715() != null || quadro07.getAnexoDq07C716() != null || quadro07.getAnexoDq07C717() != null || quadro07.getAnexoDq07C718() != null || quadro07.getAnexoDq07C719() != null) && (quadro01.getAnexoDq01B2() == null || quadro01.getAnexoDq01B2() != null && quadro01.getAnexoDq01B2().equals(Boolean.FALSE))) {
            result.add(new DeclValidationMessage("D107", anexoDModel.getLink("aAnexoD.qQuadro07"), new String[]{anexoDModel.getLink(anexoDModel.getLink("aAnexoD.qQuadro07")), anexoDModel.getLink("aAnexoD.qQuadro01.fanexoDq01B2")}));
        }
    }
}

