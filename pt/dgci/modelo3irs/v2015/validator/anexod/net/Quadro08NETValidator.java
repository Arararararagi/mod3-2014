/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexod.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexod.Quadro08Validator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro08NETValidator
extends Quadro08Validator {
    public Quadro08NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

