/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.HashSet;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NibField;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro07Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String DEPENDENTES_LINK_TEMPLATE = "aRosto.qQuadro03.fq03CD";
    public static final String DEPENDENTES_DEF_LINK_TEMPLATE = "aRosto.qQuadro03.fq03CDD";
    public static final String ASCENDENTES_LINK_TEMPLATE = "aRosto.qQuadro07.fq07C0";
    private static final long ANO_2011 = 2011;

    public Quadro07Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro02 quadro02Model = (Quadro02)rostoModel.getQuadro(Quadro02.class.getSimpleName());
        Quadro03 quadro03Model = (Quadro03)rostoModel.getQuadro(Quadro03.class.getSimpleName());
        Quadro05 quadro05Model = (Quadro05)rostoModel.getQuadro(Quadro05.class.getSimpleName());
        Quadro07 quadro07Model = (Quadro07)rostoModel.getQuadro(Quadro07.class.getSimpleName());
        if (!(quadro07Model.getQ07C1() == null || Modelo3IRSValidatorUtil.startsWith(quadro07Model.getQ07C1(), "1", "2", "3"))) {
            result.add(new DeclValidationMessage("R097", "aRosto.qQuadro07.fq07C1", new String[]{"aRosto.qQuadro07.fq07C1"}));
        }
        if (!(quadro07Model.getQ07C1() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro07Model.getQ07C1()))) {
            result.add(new DeclValidationMessage("R098", "aRosto.qQuadro07.fq07C1", new String[]{"aRosto.qQuadro07.fq07C1"}));
        }
        this.validateR099(result, quadro03Model, quadro05Model, quadro07Model);
        if (quadro07Model.getQ07C1a() != null && quadro07Model.getQ07C1a() < 60) {
            result.add(new DeclValidationMessage("R100", "aRosto.qQuadro07.fq07C1a", new String[]{"aRosto.qQuadro07.fq07C1a"}));
        }
        if (quadro07Model.getQ07C1a() != null && quadro07Model.getQ07C1() == null) {
            result.add(new DeclValidationMessage("R101", "aRosto.qQuadro07.fq07C1a", new String[]{"aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C1a"}));
        }
        if (quadro07Model.getQ07C1b() != null && quadro07Model.getQ07C1b().booleanValue() && quadro07Model.getQ07C1a() == null) {
            result.add(new DeclValidationMessage("R102", "aRosto.qQuadro07.fq07C1b", new String[]{"aRosto.qQuadro07.fq07C1b", "aRosto.qQuadro07.fq07C1a"}));
        }
        this.validateAscendentesComunhaoHabitacao(result, quadro03Model, quadro07Model);
        this.validateR225(result, quadro07Model);
        this.validateR231(result, quadro02Model, quadro07Model);
        int numLinha = 0;
        HashSet<Long> existNifs = new HashSet<Long>();
        for (Rostoq07CT1_Linha linha : quadro07Model.getRostoq07CT1()) {
            this.validateR196(result, linha, ++numLinha, existNifs);
            this.validateR197(result, linha, numLinha);
            this.validateR193(result, linha, numLinha);
            this.validateR194(result, linha, numLinha);
            this.validateR195(result, linha, numLinha, quadro07Model, quadro05Model, quadro03Model);
        }
        this.validateAscendentesEColaterais(result, quadro03Model, quadro05Model, quadro07Model);
        return result;
    }

    protected void validateR225(ValidationResult result, Quadro07 quadro07Model) {
        if (quadro07Model != null && quadro07Model.getRostoq07CT1() != null && quadro07Model.getRostoq07CT1().size() > 9) {
            result.add(new DeclValidationMessage("R225", "aRosto.qQuadro07.trostoq07CT1", new String[]{"aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected void validateR231(ValidationResult result, Quadro02 quadro02Model, Quadro07 quadro07Model) {
        if (quadro02Model != null && quadro02Model.getQ02C02() != null && quadro02Model.getQ02C02() < 2011 && quadro07Model != null && quadro07Model.getRostoq07CT1() != null && quadro07Model.getRostoq07CT1().size() > 0) {
            result.add(new DeclValidationMessage("R231", "aRosto.qQuadro07.trostoq07CT1", new String[]{"aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected void validateR099(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model) {
        if (quadro07Model.getQ07C1() != null && (quadro03Model.isSujeitoPassivoA(quadro07Model.getQ07C1()) || quadro03Model.isSujeitoPassivoB(quadro07Model.getQ07C1()) || quadro03Model.existsInDependentes(quadro07Model.getQ07C1()) || quadro07Model.existsInAscendentes(quadro07Model.getQ07C1()) || quadro05Model.isRepresentante(quadro07Model.getQ07C1()) || quadro07Model.existsInAfilhadosCivisEmComunhao(quadro07Model.getQ07C1()) || quadro03Model.existsInDependentesEmGuardaConjunta(quadro07Model.getQ07C1()) || quadro03Model.existsInDependentesEmGuardaConjuntaOutro(quadro07Model.getQ07C1()) || quadro07Model.existsInAscendentesEColaterais(quadro07Model.getQ07C1()))) {
            result.add(new DeclValidationMessage("R099", "aRosto.qQuadro07.fq07C1", new String[]{"aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro05.fq05C5"}));
        }
    }

    protected void validateAscendentesComunhaoHabitacao(ValidationResult result, Quadro03 quadro03Model, Quadro07 quadro07Model) {
        this.validateAscendenteComunhaoHabitacao(result, quadro03Model, quadro07Model, quadro07Model.getQ07C01(), "aRosto.qQuadro07.fq07C01", quadro07Model.getQ07C01a(), "aRosto.qQuadro07.fq07C01a", 1);
        this.validateAscendenteComunhaoHabitacao(result, quadro03Model, quadro07Model, quadro07Model.getQ07C02(), "aRosto.qQuadro07.fq07C02", quadro07Model.getQ07C02a(), "aRosto.qQuadro07.fq07C02a", 2);
        if (quadro07Model.getQ07C02() != null && quadro07Model.getQ07C01() == null) {
            result.add(new DeclValidationMessage("R106", "aRosto.qQuadro07.fq07C02", new String[]{"aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.fq07C02"}));
        }
        this.validateAscendenteComunhaoHabitacao(result, quadro03Model, quadro07Model, quadro07Model.getQ07C03(), "aRosto.qQuadro07.fq07C03", quadro07Model.getQ07C03a(), "aRosto.qQuadro07.fq07C03a", 3);
        if (quadro07Model.getQ07C03() != null && quadro07Model.getQ07C02() == null) {
            result.add(new DeclValidationMessage("R106", "aRosto.qQuadro07.fq07C03", new String[]{"aRosto.qQuadro07.fq07C02", "aRosto.qQuadro07.fq07C03"}));
        }
        this.validateAscendenteComunhaoHabitacao(result, quadro03Model, quadro07Model, quadro07Model.getQ07C04(), "aRosto.qQuadro07.fq07C04", quadro07Model.getQ07C04a(), "aRosto.qQuadro07.fq07C04a", 4);
        if (quadro07Model.getQ07C04() != null && quadro07Model.getQ07C03() == null) {
            result.add(new DeclValidationMessage("R106", "aRosto.qQuadro07.fq07C04", new String[]{"aRosto.qQuadro07.fq07C03", "aRosto.qQuadro07.fq07C04"}));
        }
    }

    protected void validateAscendenteComunhaoHabitacao(ValidationResult result, Quadro03 quadro03Model, Quadro07 quadro07Model, Long nif, String nifLink, Long grauInv, String grauInvLink, int nAscendente) {
        boolean hasGrauInv;
        boolean hasNif = nif != null;
        boolean bl = hasGrauInv = grauInv != null;
        if (hasNif) {
            if (!Modelo3IRSValidatorUtil.startsWith(nif, "1", "2", "3")) {
                result.add(new DeclValidationMessage("R103", nifLink, new String[]{nifLink}));
            }
            if (!Modelo3IRSValidatorUtil.isNIFValid(nif)) {
                result.add(new DeclValidationMessage("R104", nifLink, new String[]{nifLink}));
            }
            if (quadro03Model.isSujeitoPassivoA(nif) || quadro03Model.isSujeitoPassivoB(nif) || quadro03Model.existsInDependentes(nif) || quadro07Model.existsInAscendentes(nif, nAscendente) || quadro07Model.existsInAfilhadosCivisEmComunhao(nif) || quadro07Model.existsInAscendentesEColaterais(nif) || quadro03Model.existsInDependentesEmGuardaConjunta(nif) || quadro03Model.existsInDependentesEmGuardaConjuntaOutro(nif) || quadro07Model.isConjugeFalecido(nif)) {
                result.add(new DeclValidationMessage("R105", nifLink, new String[]{nifLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C01"}));
            }
        }
        if (hasGrauInv && grauInv < 60) {
            result.add(new DeclValidationMessage("R107", grauInvLink, new String[]{grauInvLink}));
        }
        if (hasGrauInv && !hasNif) {
            result.add(new DeclValidationMessage("R108", grauInvLink, new String[]{nifLink, grauInvLink}));
        }
    }

    public void validateR196(ValidationResult result, Rostoq07CT1_Linha linha, int linhaPos, Set<Long> nifs) {
        if (!(linha.getNIF() == null || nifs.add(linha.getNIF()))) {
            result.add(new DeclValidationMessage("R196", "aRosto.qQuadro07.trostoq07CT1", new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF)}));
        }
    }

    public void validateR197(ValidationResult result, Rostoq07CT1_Linha linha, int linhaPos) {
        if (linha.getNIF() == null) {
            result.add(new DeclValidationMessage("R197", "aRosto.qQuadro07.trostoq07CT1", new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF)}));
        }
    }

    public void validateR193(ValidationResult result, Rostoq07CT1_Linha linha, int linhaPos) {
        Long nif = linha.getNIF();
        if (!(nif == null || NifValidator.isEmpNomeIndividual(String.valueOf(nif)))) {
            result.add(new DeclValidationMessage("R193", "aRosto.qQuadro07.trostoq07CT1", new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF)}));
        }
    }

    public void validateR194(ValidationResult result, Rostoq07CT1_Linha linha, int linhaPos) {
        Long nif = linha.getNIF();
        if (!(nif == null || NifValidator.validate(nif))) {
            result.add(new DeclValidationMessage("R194", "aRosto.qQuadro07.trostoq07CT1", new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF)}));
        }
    }

    public void validateR195(ValidationResult result, Rostoq07CT1_Linha linha, int linhaPos, Quadro07 quadro07model, Quadro05 quadro05model, Quadro03 quadro03model) {
        Long nif = linha.getNIF();
        if (nif != null) {
            String[] links;
            if (nif.equals(quadro03model.getQ03C03()) || nif.equals(quadro03model.getQ03C04()) || nif.equals(quadro07model.getQ07C1()) || nif.equals(quadro05model.getQ05C5())) {
                String[] links2 = new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro05.fq05C5"};
                result.add(new DeclValidationMessage("R195", "aRosto.qQuadro07.trostoq07CT1", links2));
            }
            Long[] dependentes = new Long[]{quadro03model.getQ03CD1(), quadro03model.getQ03CD2(), quadro03model.getQ03CD3(), quadro03model.getQ03CD4(), quadro03model.getQ03CD5(), quadro03model.getQ03CD6(), quadro03model.getQ03CD7(), quadro03model.getQ03CD8(), quadro03model.getQ03CD9(), quadro03model.getQ03CD10(), quadro03model.getQ03CD11(), quadro03model.getQ03CD12(), quadro03model.getQ03CD13(), quadro03model.getQ03CD14()};
            Long[] dependentesDeficientes = new Long[]{quadro03model.getQ03CDD1(), quadro03model.getQ03CDD2(), quadro03model.getQ03CDD3(), quadro03model.getQ03CDD4(), quadro03model.getQ03CDD5(), quadro03model.getQ03CDD6(), quadro03model.getQ03CDD7(), quadro03model.getQ03CDD8()};
            int campo = 1;
            for (Long nifDependente : dependentes) {
                if (nifDependente != null && nif.equals(nifDependente)) {
                    links = new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CD" + campo, "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro05.fq05C5"};
                    result.add(new DeclValidationMessage("R195", "aRosto.qQuadro07.trostoq07CT1", links));
                }
                ++campo;
            }
            campo = 1;
            for (Long nifDependenteDef : dependentesDeficientes) {
                if (nifDependenteDef != null && nif.equals(nifDependenteDef)) {
                    links = new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CDD" + campo, "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro05.fq05C5"};
                    result.add(new DeclValidationMessage("R195", "aRosto.qQuadro07.trostoq07CT1", links));
                }
                ++campo;
            }
            Long[] ascendentes = new Long[]{quadro07model.getQ07C01(), quadro07model.getQ07C02(), quadro07model.getQ07C03(), quadro07model.getQ07C04()};
            campo = 1;
            for (Long ascendente : ascendentes) {
                if (ascendente != null && nif.equals(ascendente)) {
                    String[] links3 = new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C0" + campo, "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro05.fq05C5"};
                    result.add(new DeclValidationMessage("R195", "aRosto.qQuadro07.trostoq07CT1", links3));
                }
                ++campo;
            }
            if (quadro07model.existsInAscendentesEColaterais(nif) || quadro03model.existsInDependentesEmGuardaConjunta(nif) || quadro03model.existsInDependentesEmGuardaConjuntaOutro(nif)) {
                String[] links2 = new String[]{Quadro07.getLinkForROSTOQ07CT1_LINK(linhaPos, Rostoq07CT1_LinhaBase.Property.NIF), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro05.fq05C5"};
                result.add(new DeclValidationMessage("R195", "aRosto.qQuadro07.trostoq07CT1", links2));
            }
        }
    }

    protected void validateAscendentesEColaterais(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model) {
        if (quadro07Model != null) {
            HashSet<Long> nifsUnicos = new HashSet<Long>();
            EventList<Rostoq07ET1_Linha> linhas = quadro07Model.getRostoq07ET1();
            Rostoq07ET1_Linha linhaActual = null;
            this.validateR224(result, quadro07Model);
            for (int i = 0; i < linhas.size(); ++i) {
                linhaActual = linhas.get(i);
                this.validateR218(result, linhaActual, i + 1, nifsUnicos);
                this.validateR219(result, linhaActual, i + 1);
                this.validateR220(result, linhaActual, i + 1);
                this.validateR221(result, linhaActual, i + 1);
                this.validateR222(result, quadro03Model, quadro05Model, quadro07Model, linhaActual, i + 1);
            }
        }
    }

    protected void validateR224(ValidationResult result, Quadro07 quadro07Model) {
        if (quadro07Model != null && quadro07Model.getRostoq07ET1() != null && quadro07Model.getRostoq07ET1().size() > 9) {
            result.add(new DeclValidationMessage("R224", "aRosto.qQuadro07.trostoq07ET1", new String[]{"aRosto.qQuadro07.trostoq07ET1"}));
        }
    }

    protected void validateR218(ValidationResult result, Rostoq07ET1_Linha linha, int linhaPos, Set<Long> nifs) {
        if (!(linha.getNIF() == null || nifs.add(linha.getNIF()))) {
            result.add(new DeclValidationMessage("R218", "aRosto.qQuadro07.trostoq07ET1", new String[]{Quadro07.getLinkCellForROSTOQ07ET1_LINK(linhaPos, Rostoq07ET1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR219(ValidationResult result, Rostoq07ET1_Linha linha, int linhaPos) {
        if (linha.getNIF() == null) {
            result.add(new DeclValidationMessage("R219", "aRosto.qQuadro07.trostoq07ET1", new String[]{Quadro07.getLinkCellForROSTOQ07ET1_LINK(linhaPos, Rostoq07ET1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR220(ValidationResult result, Rostoq07ET1_Linha linha, int linhaPos) {
        if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.isNIFValid(linha.getNIF()))) {
            result.add(new DeclValidationMessage("R220", "aRosto.qQuadro07.trostoq07ET1", new String[]{Quadro07.getLinkCellForROSTOQ07ET1_LINK(linhaPos, Rostoq07ET1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR221(ValidationResult result, Rostoq07ET1_Linha linha, int linhaPos) {
        if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.startsWith(linha.getNIF(), "1", "2", "3"))) {
            result.add(new DeclValidationMessage("R221", "aRosto.qQuadro07.trostoq07ET1", new String[]{Quadro07.getLinkCellForROSTOQ07ET1_LINK(linhaPos, Rostoq07ET1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR222(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model, Rostoq07ET1_Linha linha, int linhaPos) {
        boolean hasNif;
        boolean bl = hasNif = linha.getNIF() != null;
        if (hasNif && (quadro03Model.isSujeitoPassivoA(linha.getNIF()) || quadro03Model.isSujeitoPassivoB(linha.getNIF()) || quadro03Model.existsInDependentes(linha.getNIF()) || quadro07Model.isConjugeFalecido(linha.getNIF()) || quadro07Model.existsInAscendentes(linha.getNIF()) || quadro03Model.existsInDependentesEmGuardaConjunta(linha.getNIF()) || quadro03Model.existsInDependentesEmGuardaConjuntaOutro(linha.getNIF()) || quadro07Model.existsInAfilhadosCivisEmComunhao(linha.getNIF()) || quadro05Model.isRepresentante(linha.getNIF()))) {
            result.add(new DeclValidationMessage("R222", "aRosto.qQuadro07.trostoq07ET1", new String[]{Quadro07.getLinkCellForROSTOQ07ET1_LINK(linhaPos, Rostoq07ET1_LinhaBase.Property.NIF), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro05.fq05C5"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro07 quadro07Model = (Quadro07)rostoModel.getQuadro(Quadro07.class.getSimpleName());
        this.validateR110(result, quadro07Model);
        this.validateR111(result, quadro07Model);
        this.validateR112(result, quadro07Model);
        this.validateR113(result, quadro07Model);
        return result;
    }

    protected void validateR113(ValidationResult result, Quadro07 quadro07Model) {
        if (!StringUtil.isEmpty(quadro07Model.getQ07D01()) && (String.valueOf(quadro07Model.getQ07D01()).startsWith("0006") || String.valueOf(quadro07Model.getQ07D01()).startsWith("0015") || String.valueOf(quadro07Model.getQ07D01()).startsWith("0017") || String.valueOf(quadro07Model.getQ07D01()).startsWith("0024") || String.valueOf(quadro07Model.getQ07D01()).startsWith("0041") || String.valueOf(quadro07Model.getQ07D01()).startsWith("0076") || String.valueOf(quadro07Model.getQ07D01()).startsWith("0781"))) {
            result.add(new DeclValidationMessage("R113", "aRosto.qQuadro07.fq07D01", new String[]{"aRosto.qQuadro07.fq07D01"}));
        }
    }

    protected void validateR112(ValidationResult result, Quadro07 quadro07Model) {
        if (StringUtil.isEmpty(quadro07Model.getQ07D01()) && quadro07Model.getQ07D01a() != null && quadro07Model.getQ07D01a().booleanValue()) {
            result.add(new DeclValidationMessage("R112", "aRosto.qQuadro07.fq07D01", new String[]{"aRosto.qQuadro07.fq07D01"}));
        }
    }

    protected void validateR111(ValidationResult result, Quadro07 quadro07Model) {
        if (!(StringUtil.isEmpty(quadro07Model.getQ07D01()) || quadro07Model.getQ07D01a() != null && quadro07Model.getQ07D01a().booleanValue())) {
            result.add(new DeclValidationMessage("R111", "aRosto.qQuadro07.fq07D01a", new String[]{"aRosto.qQuadro07.fq07D01a"}));
        }
    }

    protected void validateR110(ValidationResult result, Quadro07 quadro07Model) {
        if (!(StringUtil.isEmpty(quadro07Model.getQ07D01()) || NibField.validate(String.valueOf(quadro07Model.getQ07D01())))) {
            result.add(new DeclValidationMessage("R110", "aRosto.qQuadro07.fq07D01", new String[]{"aRosto.qQuadro07.fq07D01"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

