/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro01;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro01Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro01Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro01 quadro01Model = rostoModel.getQuadro01();
        if (quadro01Model.getQ01C01() == null) {
            result.add(new DeclValidationMessage("R002", "aRosto.qQuadro01.fq01C01", new String[]{"aRosto.qQuadro01.fq01C01"}));
        }
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

