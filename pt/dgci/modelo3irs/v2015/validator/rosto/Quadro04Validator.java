/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.Date;
import pt.opensoft.util.StringUtil;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro04 quadro04Model = (Quadro04)rostoModel.getQuadro(Quadro04.class.getSimpleName());
        Quadro09 quadro09Model = (Quadro09)rostoModel.getQuadro(Quadro09.class.getSimpleName());
        this.validateR038(result, quadro04Model);
        this.validateR153(result, quadro04Model, quadro09Model);
        this.validateR039(result, quadro04Model);
        return result;
    }

    protected void validateR039(ValidationResult result, Quadro04 quadro04Model) {
        if (quadro04Model.getQ04B1() == null) {
            result.add(new DeclValidationMessage("R039", "aRosto.qQuadro04.fq04B1OP1", new String[]{"aRosto.qQuadro04.fq04B1OP1", "aRosto.qQuadro04.fq04B1OP2"}));
        }
    }

    protected void validateR153(ValidationResult result, Quadro04 quadro04Model, Quadro09 quadro09Model) {
        if (quadro04Model.getQ04B1() != null && quadro04Model.getQ04B1().equals("1") && (quadro09Model.isQ09B1OP1Selected() || quadro09Model.isQ09B1OP2Selected() || quadro09Model.getQ09C03() != null)) {
            result.add(new DeclValidationMessage("R153", "aRosto.qQuadro04.fq04B1OP1", new String[]{"aRosto.qQuadro04.fq04B1OP1"}));
        }
    }

    protected void validateR038(ValidationResult result, Quadro04 quadro04Model) {
        if (!(quadro04Model.getQ04B1() == null || StringUtil.in(quadro04Model.getQ04B1(), new String[]{"1", "2"}))) {
            result.add(new DeclValidationMessage("R038", "aRosto.qQuadro04.fq04B1OP1", new String[]{"aRosto.qQuadro04.fq04B1OP1"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

