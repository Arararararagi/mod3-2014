/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.Quadro05Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro05NETValidator
extends Quadro05Validator {
    public Quadro05NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro05 quadro05Model = rostoModel.getQuadro05();
        Quadro07 quadro07Model = rostoModel.getQuadro07();
        Long anoExercicio = rostoModel.getQuadro02().getQ02C02();
        if (!(quadro05Model.getQ05C5() == null || quadro07Model.getQ07C01() == null && quadro07Model.getQ07C02() == null && quadro07Model.getQ07C03() == null && quadro07Model.getQ07C04() == null || quadro05Model.isQ05B3OP9Selected() || anoExercicio == null || anoExercicio <= 2008)) {
            result.add(new DeclValidationMessage("R177", "aRosto.qQuadro05.fq05C5", new String[]{"aRosto.qQuadro07.fq07C01", "aRosto.qQuadro05.fq05C5"}));
        }
        return result;
    }
}

