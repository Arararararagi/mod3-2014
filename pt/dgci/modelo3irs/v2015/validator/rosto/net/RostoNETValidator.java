/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.RostoValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro01NETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro06NETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro07NETValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.net.Quadro09NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class RostoNETValidator
extends RostoValidator {
    public RostoNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro01NETValidator().validate(model));
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        result.addAllFrom(new Quadro07NETValidator().validate(model));
        result.addAllFrom(new Quadro09NETValidator().validate(model));
        return result;
    }
}

