/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.Quadro09Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.Date;
import pt.opensoft.util.ListUtil;

public class Quadro09NETValidator
extends Quadro09Validator {
    public Quadro09NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro09 quadro09Model = (Quadro09)rostoModel.getQuadro(Quadro09.class.getSimpleName());
        this.validateR046(model, result, quadro09Model);
        this.validateR048(result, quadro09Model);
        this.validateR049(result, quadro09Model);
        return result;
    }

    protected void validateR049(ValidationResult result, Quadro09 quadro09Model) {
        if (quadro09Model.getQ09C03() != null && quadro09Model.getQ09C03().after(new Date())) {
            result.add(new DeclValidationMessage("R049", "aRosto.qQuadro09.fq09C03", new String[]{"aRosto.qQuadro09.fq09C03"}));
        }
    }

    protected void validateR048(ValidationResult result, Quadro09 quadro09Model) {
        if (quadro09Model.getQ09C03() != null && quadro09Model.getQ09C03().beforeOrEquals(new Date(2002, 11, 31))) {
            result.add(new DeclValidationMessage("R048", "aRosto.qQuadro09.fq09C03", new String[]{"aRosto.qQuadro09.fq09C03"}));
        }
    }

    protected void validateR046(DeclaracaoModel model, ValidationResult result, Quadro09 quadro09Model) {
        if (Modelo3IRSv2015Parameters.instance().isFase2() && quadro09Model.isQ09B1OP2Selected()) {
            if (ListUtil.isEmpty(model.getAllAnexosByType("AnexoB")) && ListUtil.isEmpty(model.getAllAnexosByType("AnexoC"))) {
                result.add(new DeclValidationMessage("R046", "aRosto.qQuadro09"));
            } else {
                List<FormKey> anexoBKeys = model.getAllAnexosByType("AnexoB");
                int preenchido = 0;
                for (FormKey keyB : anexoBKeys) {
                    AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(keyB);
                    if (anexoBModel.getQuadro04().getAnexoBq04T1().isEmpty()) continue;
                    ++preenchido;
                }
                List<FormKey> anexoCKeys = model.getAllAnexosByType("AnexoC");
                for (FormKey keyC : anexoCKeys) {
                    AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(keyC);
                    if (!anexoCModel.getQuadro14().getAnexoCq14T1().isEmpty()) {
                        ++preenchido;
                    }
                    if (Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro04().getAnexoCq04C434())) continue;
                    ++preenchido;
                }
                if (preenchido == 0) {
                    result.add(new DeclValidationMessage("R046", "aRosto.qQuadro09"));
                }
            }
        }
    }
}

