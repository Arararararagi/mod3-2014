/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.Quadro06Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro06NETValidator
extends Quadro06Validator {
    public Quadro06NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        this.validateR228(result, rostoModel);
        return result;
    }

    protected void validateR228(ValidationResult result, RostoModel rostoModel) {
        if (rostoModel != null && rostoModel.getQuadro06().isQ06B1OP3Selected() && rostoModel.getQuadro03().getRostoq03DT1() != null && !rostoModel.getQuadro03().getRostoq03DT1().isEmpty()) {
            result.add(new DeclValidationMessage("R228", "aRosto.qQuadro06.fq06B1OP3", new String[]{"aRosto.qQuadro06.fq06B1OP3", "aRosto.qQuadro03.trostoq03DT1"}));
        }
    }
}

