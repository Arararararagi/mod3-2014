/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.rosto.Quadro02Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.DateTime;

public class Quadro02NETValidator
extends Quadro02Validator {
    public Quadro02NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro02 quadro02Model = rostoModel.getQuadro02();
        long currentYear = new DateTime().getYear();
        if (quadro02Model.getQ02C02() != null && (quadro02Model.getQ02C02() < 2003 || quadro02Model.getQ02C02() > currentYear - 1)) {
            result.add(new DeclValidationMessage("R004", "aRosto.qQuadro02.fq02C02", new String[]{"aRosto.qQuadro02.fq02C02"}));
        }
        return result;
    }
}

