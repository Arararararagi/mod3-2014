/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro03 quadro03Model = rostoModel.getQuadro03();
        Quadro06 quadro06Model = rostoModel.getQuadro06();
        if (quadro06Model.getQ06B1() == null) {
            result.add(new DeclValidationMessage("R093", "aRosto.qQuadro06", new String[]{"aRosto.qQuadro06"}));
        }
        this.validateR094(result, quadro06Model);
        this.validateR095(result, quadro03Model, quadro06Model);
        this.validateR096(result, quadro03Model, quadro06Model);
        return result;
    }

    protected void validateR094(ValidationResult result, Quadro06 quadro06Model) {
        if (!(quadro06Model.getQ06B1() == null || StringUtil.in(quadro06Model.getQ06B1(), new String[]{"1", "2", "3", "4"}))) {
            result.add(new DeclValidationMessage("R094", "aRosto.qQuadro06", new String[]{"aRosto.qQuadro06"}));
        }
    }

    protected void validateR095(ValidationResult result, Quadro03 quadro03Model, Quadro06 quadro06Model) {
        if (quadro03Model.getQ03C04() == null && (quadro06Model.isQ06B1OP1Selected() || quadro06Model.isQ06B1OP4Selected())) {
            result.add(new DeclValidationMessage("R095", "aRosto.qQuadro06", new String[]{"aRosto.qQuadro06", "aRosto.qQuadro03.fq03C04"}));
        }
    }

    protected void validateR096(ValidationResult result, Quadro03 quadro03Model, Quadro06 quadro06Model) {
        if (quadro03Model.getQ03C04() != null && (quadro06Model.isQ06B1OP2Selected() || quadro06Model.isQ06B1OP3Selected())) {
            result.add(new DeclValidationMessage("R096", "aRosto.qQuadro06", new String[]{"aRosto.qQuadro06", "aRosto.qQuadro03.fq03C04"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

