/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.HashSet;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final long ANO_2012 = 2012;

    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro02 quadro02Model = rostoModel.getQuadro02();
        Quadro03 quadro03Model = rostoModel.getQuadro03();
        Quadro05 quadro05Model = rostoModel.getQuadro05();
        Quadro07 quadro07Model = rostoModel.getQuadro07();
        if (!(quadro03Model.getQ03C03() == null || Modelo3IRSValidatorUtil.startsWith(quadro03Model.getQ03C03(), "1", "2", "3"))) {
            result.add(new DeclValidationMessage("R006", "aRosto.qQuadro03.fq03C03", new String[]{"aRosto.qQuadro03.fq03C03"}));
        }
        if (!(quadro03Model.getQ03C03() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro03Model.getQ03C03()))) {
            result.add(new DeclValidationMessage("R007", "aRosto.qQuadro03.fq03C03", new String[]{"aRosto.qQuadro03.fq03C03"}));
        }
        if (quadro03Model.getQ03C03() != null && quadro03Model.getQ03C04() != null && quadro03Model.getQ03C03().equals(quadro03Model.getQ03C04())) {
            result.add(new DeclValidationMessage("R008", "aRosto.qQuadro03.fq03C03", new String[]{"aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04"}));
        }
        if (quadro03Model.getQ03C03() == null) {
            result.add(new DeclValidationMessage("R009", "aRosto.qQuadro03.fq03C03", new String[]{"aRosto.qQuadro03.fq03C03"}));
        }
        if (quadro03Model.getQ03C03a() != null && quadro03Model.getQ03C03a() < 60) {
            result.add(new DeclValidationMessage("R010", "aRosto.qQuadro03.fq03C03a", new String[]{"aRosto.qQuadro03.fq03C03a"}));
        }
        if (quadro03Model.getQ03B03() != null && quadro03Model.getQ03B03().booleanValue() && (quadro03Model.getQ03C03a() == null || quadro03Model.getQ03C03a() == 0)) {
            result.add(new DeclValidationMessage("R011", "aRosto.qQuadro03.fq03B03", new String[]{"aRosto.qQuadro03.fq03B03", "aRosto.qQuadro03.fq03C03a"}));
        }
        if (!(quadro03Model.getQ03C04() == null || Modelo3IRSValidatorUtil.startsWith(quadro03Model.getQ03C04(), "1", "2", "3"))) {
            result.add(new DeclValidationMessage("R012", "aRosto.qQuadro03.fq03C04", new String[]{"aRosto.qQuadro03.fq03C04"}));
        }
        if (!(quadro03Model.getQ03C04() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro03Model.getQ03C04()))) {
            result.add(new DeclValidationMessage("R013", "aRosto.qQuadro03.fq03C04", new String[]{"aRosto.qQuadro03.fq03C04"}));
        }
        if (quadro03Model.getQ03C04a() != null && quadro03Model.getQ03C04a() < 60) {
            result.add(new DeclValidationMessage("R015", "aRosto.qQuadro03.fq03C04a", new String[]{"aRosto.qQuadro03.fq03C04a"}));
        }
        if (quadro03Model.getQ03B04() != null && quadro03Model.getQ03B04().booleanValue() && (quadro03Model.getQ03C04a() == null || quadro03Model.getQ03C04a() == 0)) {
            result.add(new DeclValidationMessage("R016", "aRosto.qQuadro03.fq03B04", new String[]{"aRosto.qQuadro03.fq03C04a", "aRosto.qQuadro03.fq03B04"}));
        }
        if (quadro03Model.getQ03C04() == null && quadro03Model.getQ03C04a() != null && quadro03Model.getQ03C04a() > 0) {
            result.add(new DeclValidationMessage("R017", "aRosto.qQuadro03.fq03C04a", new String[]{"aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03C04a"}));
        }
        this.validateDependentesNaoDeficientes(result, quadro03Model, quadro05Model, quadro07Model);
        this.validateDependentesDeficientes(result, quadro03Model, quadro05Model, quadro07Model);
        this.validateDependentesEmGuardaConjunta(result, quadro02Model, quadro03Model, quadro05Model, quadro07Model);
        return result;
    }

    protected void validateDependentesNaoDeficientes(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model) {
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD1(), "aRosto.qQuadro03.fq03CD1", 1);
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD2(), "aRosto.qQuadro03.fq03CD2", 2);
        if (quadro03Model.getQ03CD2() != null && quadro03Model.getQ03CD1() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD2", new String[]{"aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CD2"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD3(), "aRosto.qQuadro03.fq03CD3", 3);
        if (quadro03Model.getQ03CD3() != null && quadro03Model.getQ03CD2() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD3", new String[]{"aRosto.qQuadro03.fq03CD2", "aRosto.qQuadro03.fq03CD3"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD4(), "aRosto.qQuadro03.fq03CD4", 4);
        if (quadro03Model.getQ03CD4() != null && quadro03Model.getQ03CD3() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD4", new String[]{"aRosto.qQuadro03.fq03CD3", "aRosto.qQuadro03.fq03CD4"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD5(), "aRosto.qQuadro03.fq03CD5", 5);
        if (quadro03Model.getQ03CD5() != null && quadro03Model.getQ03CD4() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD5", new String[]{"aRosto.qQuadro03.fq03CD4", "aRosto.qQuadro03.fq03CD5"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD6(), "aRosto.qQuadro03.fq03CD6", 6);
        if (quadro03Model.getQ03CD6() != null && quadro03Model.getQ03CD5() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD6", new String[]{"aRosto.qQuadro03.fq03CD5", "aRosto.qQuadro03.fq03CD6"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD7(), "aRosto.qQuadro03.fq03CD7", 7);
        if (quadro03Model.getQ03CD7() != null && quadro03Model.getQ03CD6() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD7", new String[]{"aRosto.qQuadro03.fq03CD6", "aRosto.qQuadro03.fq03CD7"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD8(), "aRosto.qQuadro03.fq03CD8", 8);
        if (quadro03Model.getQ03CD8() != null && quadro03Model.getQ03CD7() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD8", new String[]{"aRosto.qQuadro03.fq03CD7", "aRosto.qQuadro03.fq03CD8"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD9(), "aRosto.qQuadro03.fq03CD9", 9);
        if (quadro03Model.getQ03CD9() != null && quadro03Model.getQ03CD8() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD9", new String[]{"aRosto.qQuadro03.fq03CD8", "aRosto.qQuadro03.fq03CD9"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD10(), "aRosto.qQuadro03.fq03CD10", 10);
        if (quadro03Model.getQ03CD10() != null && quadro03Model.getQ03CD9() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD10", new String[]{"aRosto.qQuadro03.fq03CD9", "aRosto.qQuadro03.fq03CD10"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD11(), "aRosto.qQuadro03.fq03CD11", 11);
        if (quadro03Model.getQ03CD11() != null && quadro03Model.getQ03CD10() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD11", new String[]{"aRosto.qQuadro03.fq03CD10", "aRosto.qQuadro03.fq03CD11"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD12(), "aRosto.qQuadro03.fq03CD12", 12);
        if (quadro03Model.getQ03CD12() != null && quadro03Model.getQ03CD11() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD12", new String[]{"aRosto.qQuadro03.fq03CD11", "aRosto.qQuadro03.fq03CD12"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD13(), "aRosto.qQuadro03.fq03CD13", 13);
        if (quadro03Model.getQ03CD13() != null && quadro03Model.getQ03CD12() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD13", new String[]{"aRosto.qQuadro03.fq03CD12", "aRosto.qQuadro03.fq03CD13"}));
        }
        this.validateDependenteNaoDeficiente(result, quadro03Model, quadro05Model, quadro07Model, quadro03Model.getQ03CD14(), "aRosto.qQuadro03.fq03CD14", 14);
        if (quadro03Model.getQ03CD14() != null && quadro03Model.getQ03CD13() == null) {
            result.add(new DeclValidationMessage("R024", "aRosto.qQuadro03.fq03CD14", new String[]{"aRosto.qQuadro03.fq03CD13", "aRosto.qQuadro03.fq03CD14"}));
        }
    }

    protected void validateDependenteNaoDeficiente(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model, Long nif, String nifLink, int nDep) {
        boolean hasNif;
        boolean bl = hasNif = nif != null;
        if (hasNif) {
            if (!Modelo3IRSValidatorUtil.startsWith(nif, "1", "2", "3")) {
                result.add(new DeclValidationMessage("R021", nifLink, new String[]{nifLink}));
            }
            if (!Modelo3IRSValidatorUtil.isNIFValid(nif)) {
                result.add(new DeclValidationMessage("R022", nifLink, new String[]{nifLink}));
            }
        }
        if (hasNif && (quadro03Model.isSujeitoPassivoA(nif) || quadro03Model.isSujeitoPassivoB(nif) || quadro07Model.isConjugeFalecido(nif) || quadro07Model.existsInAscendentes(nif) || quadro03Model.existsInDependentes(nif, nDep, -1) || quadro05Model.isRepresentante(nif) || quadro07Model.existsInAfilhadosCivisEmComunhao(nif) || quadro03Model.existsInDependentesEmGuardaConjunta(nif) || quadro03Model.existsInDependentesEmGuardaConjuntaOutro(nif) || quadro07Model.existsInAscendentesEColaterais(nif))) {
            result.add(new DeclValidationMessage("R023", nifLink, new String[]{nifLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro05.fq05C5", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro03.fq03CD1"}));
        }
    }

    protected void validateDependentesDeficientes(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model) {
        Long nif = quadro03Model.getQ03CDD1();
        Long grauInv = quadro03Model.getQ03CDD1a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD1", grauInv, "aRosto.qQuadro03.fq03CDD1a", 1);
        nif = quadro03Model.getQ03CDD2();
        grauInv = quadro03Model.getQ03CDD2a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD2", grauInv, "aRosto.qQuadro03.fq03CDD2a", 2);
        if (quadro03Model.getQ03CDD2() != null && quadro03Model.getQ03CDD1() == null) {
            result.add(new DeclValidationMessage("R032", "aRosto.qQuadro03.fq03CDD2", new String[]{"aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.fq03CDD2"}));
        }
        nif = quadro03Model.getQ03CDD3();
        grauInv = quadro03Model.getQ03CDD3a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD3", grauInv, "aRosto.qQuadro03.fq03CDD3a", 3);
        if (quadro03Model.getQ03CDD3() != null && quadro03Model.getQ03CDD2() == null) {
            result.add(new DeclValidationMessage("R032", "aRosto.qQuadro03.fq03CDD3", new String[]{"aRosto.qQuadro03.fq03CDD2", "aRosto.qQuadro03.fq03CDD3"}));
        }
        nif = quadro03Model.getQ03CDD4();
        grauInv = quadro03Model.getQ03CDD4a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD4", grauInv, "aRosto.qQuadro03.fq03CDD4a", 4);
        if (quadro03Model.getQ03CDD4() != null && quadro03Model.getQ03CDD3() == null) {
            result.add(new DeclValidationMessage("R032", "aRosto.qQuadro03.fq03CDD4", new String[]{"aRosto.qQuadro03.fq03CDD3", "aRosto.qQuadro03.fq03CDD4"}));
        }
        nif = quadro03Model.getQ03CDD5();
        grauInv = quadro03Model.getQ03CDD5a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD5", grauInv, "aRosto.qQuadro03.fq03CDD5a", 5);
        if (quadro03Model.getQ03CDD5() != null && quadro03Model.getQ03CDD4() == null) {
            result.add(new DeclValidationMessage("R032", "aRosto.qQuadro03.fq03CDD5", new String[]{"aRosto.qQuadro03.fq03CDD4", "aRosto.qQuadro03.fq03CDD5"}));
        }
        nif = quadro03Model.getQ03CDD6();
        grauInv = quadro03Model.getQ03CDD6a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD6", grauInv, "aRosto.qQuadro03.fq03CDD6a", 6);
        if (quadro03Model.getQ03CDD6() != null && quadro03Model.getQ03CDD5() == null) {
            result.add(new DeclValidationMessage("R032", "aRosto.qQuadro03.fq03CDD6", new String[]{"aRosto.qQuadro03.fq03CDD5", "aRosto.qQuadro03.fq03CDD6"}));
        }
        nif = quadro03Model.getQ03CDD7();
        grauInv = quadro03Model.getQ03CDD7a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD7", grauInv, "aRosto.qQuadro03.fq03CDD7a", 7);
        if (quadro03Model.getQ03CDD7() != null && quadro03Model.getQ03CDD6() == null) {
            result.add(new DeclValidationMessage("R032", "aRosto.qQuadro03.fq03CDD7", new String[]{"aRosto.qQuadro03.fq03CDD6", "aRosto.qQuadro03.fq03CDD7"}));
        }
        nif = quadro03Model.getQ03CDD8();
        grauInv = quadro03Model.getQ03CDD8a();
        this.validateDependenteDeficiente(result, quadro03Model, quadro05Model, quadro07Model, nif, "aRosto.qQuadro03.fq03CDD8", grauInv, "aRosto.qQuadro03.fq03CDD8a", 8);
        if (quadro03Model.getQ03CDD8() != null && quadro03Model.getQ03CDD7() == null) {
            result.add(new DeclValidationMessage("R032", "aRosto.qQuadro03.fq03CDD8", new String[]{"aRosto.qQuadro03.fq03CDD7", "aRosto.qQuadro03.fq03CDD8"}));
        }
    }

    protected void validateDependenteDeficiente(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model, Long nif, String nifLink, Long grauInv, String grauInvLink, int nDep) {
        boolean hasGrauInv;
        boolean hasNif = nif != null;
        boolean bl = hasGrauInv = grauInv != null;
        if (hasNif) {
            if (!Modelo3IRSValidatorUtil.startsWith(nif, "1", "2", "3")) {
                result.add(new DeclValidationMessage("R025", nifLink, new String[]{nifLink}));
            }
            if (!Modelo3IRSValidatorUtil.isNIFValid(nif)) {
                result.add(new DeclValidationMessage("R026", nifLink, new String[]{nifLink}));
            }
        }
        if (hasNif && (quadro03Model.isSujeitoPassivoA(nif) || quadro03Model.isSujeitoPassivoB(nif) || quadro07Model.isConjugeFalecido(nif) || quadro07Model.existsInAscendentes(nif) || quadro03Model.existsInDependentes(nif, -1, nDep) || quadro07Model.existsInAfilhadosCivisEmComunhao(nif) || quadro03Model.existsInDependentesEmGuardaConjunta(nif) || quadro03Model.existsInDependentesEmGuardaConjuntaOutro(nif) || quadro07Model.existsInAscendentesEColaterais(nif) || quadro05Model.isRepresentante(nif))) {
            result.add(new DeclValidationMessage("R027", nifLink, new String[]{nifLink, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro05.fq05C5", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro03.fq03CD1"}));
        }
        if (hasGrauInv && !hasNif) {
            result.add(new DeclValidationMessage("R029", nifLink, new String[]{nifLink, grauInvLink}));
        }
        if (!hasGrauInv && hasNif) {
            result.add(new DeclValidationMessage("R030", grauInvLink, new String[]{grauInvLink}));
        }
    }

    protected void validateDependentesEmGuardaConjunta(ValidationResult result, Quadro02 quadro02Model, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model) {
        if (quadro03Model != null) {
            HashSet<Long> nifsUnicos = new HashSet<Long>();
            EventList<Rostoq03DT1_Linha> linhas = quadro03Model.getRostoq03DT1();
            Rostoq03DT1_Linha linhaActual = null;
            this.validateR223(result, quadro03Model);
            this.validateR230(result, quadro02Model, quadro03Model);
            for (int i = 0; i < linhas.size(); ++i) {
                linhaActual = linhas.get(i);
                this.validateR216(result, linhaActual, i + 1, nifsUnicos);
                this.validateR217(result, linhaActual, i + 1);
                this.validateR210(result, linhaActual, i + 1);
                this.validateR211(result, linhaActual, i + 1);
                this.validateR212(result, linhaActual, i + 1);
                this.validateR206(result, linhaActual, i + 1);
                this.validateR207(result, linhaActual, i + 1);
                this.validateR215(result, quadro03Model, quadro05Model, quadro07Model, linhaActual, i + 1);
                this.validateR213(result, linhaActual, i + 1);
                this.validateR208(result, linhaActual, i + 1);
                this.validateR209(result, linhaActual, i + 1);
                this.validateR214(result, quadro03Model, quadro05Model, quadro07Model, linhaActual, i + 1);
            }
        }
    }

    protected void validateR223(ValidationResult result, Quadro03 quadro03Model) {
        if (quadro03Model != null && quadro03Model.getRostoq03DT1() != null && quadro03Model.getRostoq03DT1().size() > 9) {
            result.add(new DeclValidationMessage("R223", "aRosto.qQuadro03.trostoq03DT1", new String[]{"aRosto.qQuadro03.trostoq03DT1"}));
        }
    }

    protected void validateR216(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos, Set<Long> nifs) {
        if (!(linha.getNIF() == null || nifs.add(linha.getNIF()))) {
            result.add(new DeclValidationMessage("R216", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR230(ValidationResult result, Quadro02 quadro02Model, Quadro03 quadro03Model) {
        if (quadro02Model != null && quadro02Model.getQ02C02() != null && quadro02Model.getQ02C02() < 2012 && quadro03Model != null && quadro03Model.getRostoq03DT1() != null && quadro03Model.getRostoq03DT1().size() > 0) {
            result.add(new DeclValidationMessage("R230", "aRosto.qQuadro03.trostoq03DT1", new String[]{"aRosto.qQuadro03.trostoq03DT1"}));
        }
    }

    protected void validateR217(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (linha.getNIF() == null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau()) && linha.getNifProgenitor() == null) {
            result.add(new DeclValidationMessage("R217", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR210(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (linha.getNIF() != null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau()) && linha.getNifProgenitor() == null) {
            result.add(new DeclValidationMessage("R210", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIFPROGENITOR)}));
        }
    }

    protected void validateR211(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau()) || linha.getNIF() != null && linha.getNifProgenitor() != null)) {
            result.add(new DeclValidationMessage("R211", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIF), Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIFPROGENITOR)}));
        }
    }

    protected void validateR212(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (linha.getNIF() == null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau()) && linha.getNifProgenitor() != null) {
            result.add(new DeclValidationMessage("R212", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR206(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.isNIFValid(linha.getNIF()))) {
            result.add(new DeclValidationMessage("R206", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR207(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.startsWith(linha.getNIF(), "1", "2", "3"))) {
            result.add(new DeclValidationMessage("R207", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIF)}));
        }
    }

    protected void validateR215(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model, Rostoq03DT1_Linha linha, int linhaPos) {
        if (linha.getNIF() != null && (quadro03Model.isSujeitoPassivoA(linha.getNIF()) || quadro03Model.isSujeitoPassivoB(linha.getNIF()) || quadro03Model.existsInDependentes(linha.getNIF()) || quadro07Model.isConjugeFalecido(linha.getNIF()) || quadro07Model.existsInAscendentes(linha.getNIF()) || quadro07Model.existsInAscendentesEColaterais(linha.getNIF()) || quadro07Model.existsInAfilhadosCivisEmComunhao(linha.getNIF()) || quadro03Model.existsInDependentesEmGuardaConjuntaOutro(linha.getNIF()) || quadro05Model.isRepresentante(linha.getNIF()))) {
            result.add(new DeclValidationMessage("R215", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIF), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1", Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIFPROGENITOR), "aRosto.qQuadro05.fq05C5"}));
        }
    }

    protected void validateR213(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau()) || linha.getDeficienteGrau() <= 100 && linha.getDeficienteGrau() >= 0)) {
            result.add(new DeclValidationMessage("R213", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.DEFICIENTEGRAU)}));
        }
    }

    protected void validateR208(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (!(linha.getNifProgenitor() == null || Modelo3IRSValidatorUtil.isNIFValid(linha.getNifProgenitor()))) {
            result.add(new DeclValidationMessage("R208", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIFPROGENITOR)}));
        }
    }

    protected void validateR209(ValidationResult result, Rostoq03DT1_Linha linha, int linhaPos) {
        if (!(linha.getNifProgenitor() == null || Modelo3IRSValidatorUtil.startsWith(linha.getNifProgenitor(), "1", "2", "3"))) {
            result.add(new DeclValidationMessage("R209", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIFPROGENITOR)}));
        }
    }

    protected void validateR214(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model, Rostoq03DT1_Linha linha, int linhaPos) {
        boolean hasNif;
        boolean bl = hasNif = linha.getNifProgenitor() != null;
        if (hasNif && (quadro03Model.isSujeitoPassivoA(linha.getNifProgenitor()) || quadro03Model.isSujeitoPassivoB(linha.getNifProgenitor()) || quadro03Model.existsInDependentes(linha.getNifProgenitor()) || quadro07Model.isConjugeFalecido(linha.getNifProgenitor()) || quadro07Model.existsInAscendentes(linha.getNifProgenitor()) || quadro07Model.existsInAscendentesEColaterais(linha.getNifProgenitor()) || quadro07Model.existsInAfilhadosCivisEmComunhao(linha.getNifProgenitor()) || quadro03Model.existsInDependentesEmGuardaConjunta(linha.getNifProgenitor()))) {
            result.add(new DeclValidationMessage("R214", "aRosto.qQuadro03.trostoq03DT1", new String[]{Quadro03.getLinkCellForROSTOQ03DT1_LINK(linhaPos, Rostoq03DT1_LinhaBase.Property.NIFPROGENITOR), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

