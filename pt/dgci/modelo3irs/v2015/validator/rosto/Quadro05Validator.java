/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_Rosto;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro02 quadro02Model = (Quadro02)rostoModel.getQuadro(Quadro02.class.getSimpleName());
        Quadro03 quadro03Model = (Quadro03)rostoModel.getQuadro(Quadro03.class.getSimpleName());
        Quadro05 quadro05Model = (Quadro05)rostoModel.getQuadro(Quadro05.class.getSimpleName());
        if (!(quadro05Model.getQ05B1() == null || StringUtil.in(quadro05Model.getQ05B1(), new String[]{"1", "2", "3", "4"}))) {
            result.add(new DeclValidationMessage("R062", "aRosto.qQuadro05.fq05B1OP1", new String[]{"aRosto.qQuadro05.fq05B1OP1"}));
        }
        if (quadro05Model.getQ05B1() == null) {
            result.add(new DeclValidationMessage("R063", "aRosto.qQuadro05.fq05B1OP1", new String[]{"aRosto.qQuadro05.fq05B1OP1"}));
        }
        if ((quadro05Model.isQ05B1OP1Selected() || quadro05Model.isQ05B1OP2Selected() || quadro05Model.isQ05B1OP3Selected()) && quadro05Model.isQ05B1OP4Selected()) {
            result.add(new DeclValidationMessage("R064", "aRosto.qQuadro05.fq05B1OP1", new String[]{"aRosto.qQuadro05.fq05B1OP4"}));
        }
        this.validateR227(result, quadro05Model);
        this.validateR068(result, quadro05Model);
        this.validateR190(result, quadro05Model);
        this.validateR226(result, rostoModel);
        this.validateR192(result, quadro05Model);
        if (quadro05Model.isQ05B2OP6Selected() && !quadro05Model.isQ05B1OP4Selected()) {
            result.add(new DeclValidationMessage("R070", "aRosto.qQuadro05.fq05B2OP6", new String[]{"aRosto.qQuadro05.fq05B2OP6", "aRosto.qQuadro05.fq05B1OP4", "aRosto.qQuadro05.fq05C5", "aRosto.qQuadro05.fq05C5_1"}));
        }
        if (quadro05Model.isQ05B2OP6Selected() && (quadro05Model.isQ05B2OP7Selected() || quadro05Model.isQ05B3OP8Selected() || quadro05Model.isQ05B3OP9Selected() || quadro05Model.isQ05B4OP10Selected() || quadro05Model.isQ05B4OP11Selected() || quadro05Model.isQ05C12Filled() || quadro05Model.isQ05C13Selected())) {
            result.add(new DeclValidationMessage("R072", "aRosto.qQuadro05.fq05B2OP6", new String[]{"aRosto.qQuadro05.fq05B2OP6", "aRosto.qQuadro05.fq05B2OP7", "aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05B3OP9", "aRosto.qQuadro05.fq05B4OP10", "aRosto.qQuadro05.fq05B4OP11", "aRosto.qQuadro05.fq05C12", "aRosto.qQuadro05.fq05C13"}));
        }
        Long anoExercicio = quadro02Model.getQ02C02() != null ? quadro02Model.getQ02C02() : 0;
        if (!(!quadro05Model.isQ05B2OP7Selected() || quadro05Model.isQ05B1OP4Selected() && (quadro05Model.isQ05B3OP8Selected() || quadro05Model.isQ05B3OP9Selected()) || anoExercicio == null || anoExercicio <= 2008)) {
            result.add(new DeclValidationMessage("R074", "aRosto.qQuadro05.fq05B2OP7", new String[]{"aRosto.qQuadro05.fq05B2OP7", "aRosto.qQuadro05.fq05B1OP4", "aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05B3OP9"}));
        }
        if (!(quadro05Model.getQ05C5() == null || anoExercicio >= 2005 || Modelo3IRSValidatorUtil.startsWith(quadro05Model.getQ05C5(), "1", "2", "3", "5"))) {
            result.add(new DeclValidationMessage("R145", "aRosto.qQuadro05.fq05C5", new String[]{"aRosto.qQuadro05.fq05C5"}));
        }
        if (!(quadro05Model.getQ05C5() == null || anoExercicio < 2005 || Modelo3IRSValidatorUtil.startsWith(quadro05Model.getQ05C5(), "1", "2", "3", "5", "98"))) {
            result.add(new DeclValidationMessage("R065", "aRosto.qQuadro05.fq05C5", new String[]{"aRosto.qQuadro05.fq05C5"}));
        }
        if (!(quadro05Model.getQ05C5() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro05Model.getQ05C5()))) {
            result.add(new DeclValidationMessage("R066", "aRosto.qQuadro05.fq05C5", new String[]{"aRosto.qQuadro05.fq05C5"}));
        }
        if (quadro05Model.isQ05B3OP8Selected() && quadro05Model.isQ05B3OP9Selected()) {
            result.add(new DeclValidationMessage("R079", "aRosto.qQuadro05.fq05B3OP8", new String[]{"aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05B3OP9"}));
        }
        if (anoExercicio == 2008 && (quadro05Model.isQ05B3OP9Selected() || quadro05Model.isQ05B4OP10Selected() || quadro05Model.isQ05B4OP11Selected())) {
            result.add(new DeclValidationMessage("R155", "aRosto.qQuadro05.fq05B3OP9", new String[]{"aRosto.qQuadro05.fq05B3OP9", "aRosto.qQuadro05.fq05B4OP10", "aRosto.qQuadro05.fq05B4OP11"}));
        }
        if (!(StringUtil.isEmpty(quadro05Model.getQ05B4()) || quadro05Model.getQ05B4().equals("10") || quadro05Model.getQ05B4().equals("11"))) {
            result.add(new DeclValidationMessage("R171", "aRosto.qQuadro05.fq05B4OP10", new String[]{"aRosto.qQuadro05.fq05B4OP10", "aRosto.qQuadro05.fq05B4OP11"}));
        }
        if (quadro05Model.isQ05B4OP10Selected() && quadro05Model.isQ05B4OP11Selected()) {
            result.add(new DeclValidationMessage("R086", "aRosto.qQuadro05.fq05B4OP10", new String[]{"aRosto.qQuadro05.fq05B4OP10", "aRosto.qQuadro05.fq05B4OP11"}));
        }
        if (quadro05Model.getQ05C12() != null && quadro05Model.getQ05C12() >= 0 && quadro05Model.getQ05C13() == null || (quadro05Model.getQ05C12() == null || quadro05Model.getQ05C12() < 0) && quadro05Model.getQ05C13() != null) {
            result.add(new DeclValidationMessage("R091", "aRosto.qQuadro05.fq05C12", new String[]{"aRosto.qQuadro05.fq05C12", "aRosto.qQuadro05.fq05C13"}));
        }
        ListMap paisesCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_Rosto.class.getSimpleName());
        if (!(quadro05Model.getQ05C13() == null || paisesCatalog.containsKey(quadro05Model.getQ05C13()))) {
            result.add(new DeclValidationMessage("R092", "aRosto.qQuadro05.fq05C13", new String[]{"aRosto.qQuadro05.fq05C13"}));
        }
        if (!(!quadro05Model.isQ05B4OP10Selected() || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03Model.getQ03C03()) || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03Model.getQ03C04()))) {
            result.add(new DeclValidationMessage("R162", "aRosto.qQuadro05.fq05B4OP10", new String[]{"aRosto.qQuadro05.fq05B4OP10", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03"}));
        }
        if (quadro05Model.isQ05B4OP11Selected() && (Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03Model.getQ03C03()) || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03Model.getQ03C04()))) {
            result.add(new DeclValidationMessage("R163", "aRosto.qQuadro05.fq05B4OP11", new String[]{"aRosto.qQuadro05.fq05B4OP11"}));
        }
        return result;
    }

    protected void validateR227(ValidationResult result, Quadro05 quadro05Model) {
        if (quadro05Model != null && (quadro05Model.isQ05B1OP1Selected() || quadro05Model.isQ05B1OP2Selected() || quadro05Model.isQ05B1OP3Selected()) && (quadro05Model.isQ05C5Filled() || quadro05Model.isQ05C5Filled() || quadro05Model.isQ05B2OP6Selected() || quadro05Model.isQ05B2OP7Selected() || quadro05Model.isQ05B3OP8Selected() || quadro05Model.isQ05B3OP9Selected() || quadro05Model.isQ05B4OP10Selected() || quadro05Model.isQ05B4OP11Selected() || quadro05Model.isQ05C12Filled() || quadro05Model.isQ05C13Selected())) {
            result.add(new DeclValidationMessage("R227", "aRosto.qQuadro05.fq05B1OP4", new String[]{"aRosto.qQuadro05.fq05B1OP1", "aRosto.qQuadro05.fq05B1OP4"}));
        }
    }

    protected void validateR068(ValidationResult result, Quadro05 quadro05Model) {
        if (!(quadro05Model.isQ05B1OP4Selected() || quadro05Model.getQ05C5() == null && quadro05Model.getQ05C5_1() == null)) {
            result.add(new DeclValidationMessage("R068", "aRosto.qQuadro05.fq05C5", new String[]{"aRosto.qQuadro05.fq05B1OP4"}));
        }
    }

    public void validateR190(ValidationResult result, Quadro05 quadro05Model) {
        ListMap paises = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_Rosto.class.getSimpleName());
        if (quadro05Model.getQ05C5_1() != null && paises.get(quadro05Model.getQ05C5_1()) == null) {
            result.add(new DeclValidationMessage("R190", "aRosto.qQuadro05.fq05C5_1", new String[]{"aRosto.qQuadro05.fq05C5_1"}));
        }
    }

    protected void validateR226(ValidationResult result, RostoModel rostoModel) {
        if (rostoModel != null && rostoModel.getQuadro05() != null && rostoModel.getQuadro05().getQ05C5_1() != null) {
            Long codigoPaisUE = rostoModel.getQuadro05().getQ05C5_1();
            long[] values = new long[]{276, 40, 56, 100, 196, 208, 703, 705, 724, 233, 246, 250, 300, 348, 372, 380, 428, 440, 442, 470, 528, 616, 203, 826, 642, 752, 352, 578, 438, 191};
            if (!(!Modelo3IRSValidatorUtil.in(codigoPaisUE, values) || rostoModel.getQuadro05().isQ05B2OP6Selected() || rostoModel.getQuadro05().isQ05B2OP7Selected())) {
                result.add(new DeclValidationMessage("R226", "aRosto.qQuadro05.fq05C5_1", new String[]{"aRosto.qQuadro05.fq05B2OP6", "aRosto.qQuadro05.fq05B2OP7"}));
            }
        }
    }

    protected void validateR192(ValidationResult result, Quadro05 quadro05Model) {
        if (quadro05Model.isQ05B1OP4Selected() && quadro05Model.getQ05C5() == null && quadro05Model.getQ05C5_1() == null) {
            result.add(new DeclValidationMessage("R192", "aRosto.qQuadro05.fq05C5_1", new String[]{"aRosto.qQuadro05.fq05B1OP4", "aRosto.qQuadro05.fq05C5", "aRosto.qQuadro05.fq05C5_1"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro02 quadro02Model = (Quadro02)rostoModel.getQuadro(Quadro02.class.getSimpleName());
        Quadro03 quadro03Model = (Quadro03)rostoModel.getQuadro(Quadro03.class.getSimpleName());
        Quadro05 quadro05Model = (Quadro05)rostoModel.getQuadro(Quadro05.class.getSimpleName());
        Quadro07 quadro07Model = rostoModel.getQuadro07();
        this.validateR067(result, quadro03Model, quadro05Model, quadro07Model);
        this.validateR149(result, quadro05Model, quadro02Model);
        this.validateR080(result, quadro05Model, quadro02Model);
        this.validateR150(result, quadro05Model, quadro02Model);
        this.validateR082(result, quadro05Model, quadro02Model);
        this.validateR152(result, quadro05Model, quadro02Model);
        this.validateR154(result, quadro05Model, quadro02Model);
        this.validateR089(result, quadro05Model);
        return result;
    }

    protected void validateR149(ValidationResult result, Quadro05 quadro05Model, Quadro02 quadro02Model) {
        Long anoExercicio = quadro02Model.getQ02C02();
        if (!(!quadro05Model.isQ05B2OP7Selected() || quadro05Model.isQ05B1OP4Selected() && quadro05Model.isQ05B3OP8Selected() || anoExercicio == null || anoExercicio != 2008)) {
            result.add(new DeclValidationMessage("R149", "aRosto.qQuadro05.fq05B2OP7", new String[]{"aRosto.qQuadro05.fq05B2OP7", "aRosto.qQuadro05.fq05B1OP4", "aRosto.qQuadro05.fq05B3OP8"}));
        }
    }

    protected void validateR067(ValidationResult result, Quadro03 quadro03Model, Quadro05 quadro05Model, Quadro07 quadro07Model) {
        boolean hasNif;
        boolean bl = hasNif = quadro05Model.getQ05C5() != null;
        if (hasNif && (quadro03Model.isSujeitoPassivoA(quadro05Model.getQ05C5()) || quadro03Model.isSujeitoPassivoB(quadro05Model.getQ05C5()) || quadro07Model.isConjugeFalecido(quadro05Model.getQ05C5()) || quadro03Model.existsInDependentes(quadro05Model.getQ05C5()) || quadro03Model.existsInDependentesEmGuardaConjunta(quadro05Model.getQ05C5()) || quadro07Model.existsInAscendentes(quadro05Model.getQ05C5()) || quadro07Model.existsInAscendentesEColaterais(quadro05Model.getQ05C5()) || quadro07Model.existsInAfilhadosCivisEmComunhao(quadro05Model.getQ05C5()))) {
            result.add(new DeclValidationMessage("R067", "aRosto.qQuadro05.fq05C5", new String[]{"aRosto.qQuadro05.fq05C5", "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateR080(ValidationResult result, Quadro05 quadro05Model, Quadro02 quadro02Model) {
        Long anoExercicio = quadro02Model.getQ02C02();
        if (quadro05Model.isQ05B3OP8Selected() && quadro05Model.getQ05C12() == null && quadro05Model.getQ05C13() == null && anoExercicio != null && anoExercicio > 2008) {
            result.add(new DeclValidationMessage("R080", "aRosto.qQuadro05.fq05B3OP8", new String[]{"aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05C12", "aRosto.qQuadro05.fq05C13"}));
        }
    }

    protected void validateR150(ValidationResult result, Quadro05 quadro05Model, Quadro02 quadro02Model) {
        Long anoExercicio = quadro02Model.getQ02C02();
        if (quadro05Model.isQ05B3OP8Selected() && quadro05Model.getQ05C12() == null && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05Model.getQ05C13()) && anoExercicio != null && anoExercicio == 2008) {
            result.add(new DeclValidationMessage("R150", "aRosto.qQuadro05.fq05B3OP8", new String[]{"aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05C12", "aRosto.qQuadro05.fq05C13"}));
        }
    }

    protected void validateR082(ValidationResult result, Quadro05 quadro05Model, Quadro02 quadro02Model) {
        Long anoExercicio = quadro02Model.getQ02C02();
        if (quadro05Model.isQ05B3OP8Selected() && !quadro05Model.isQ05B2OP7Selected() && anoExercicio != null && anoExercicio > 2008) {
            result.add(new DeclValidationMessage("R082", "aRosto.qQuadro05.fq05B3OP8", new String[]{"aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05B2OP7"}));
        }
    }

    protected void validateR152(ValidationResult result, Quadro05 quadro05Model, Quadro02 quadro02Model) {
        Long anoExercicio = quadro02Model.getQ02C02();
        if (quadro05Model.isQ05B3OP8Selected() && !quadro05Model.isQ05B2OP7Selected() && anoExercicio != null && anoExercicio == 2008) {
            result.add(new DeclValidationMessage("R152", "aRosto.qQuadro05.fq05B3OP8", new String[]{"aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05B2OP7"}));
        }
    }

    protected void validateR154(ValidationResult result, Quadro05 quadro05Model, Quadro02 quadro02Model) {
        Long anoExercicio = quadro02Model.getQ02C02();
        if ((quadro05Model.isQ05B2OP7Selected() || quadro05Model.isQ05B3OP8Selected() || quadro05Model.isQ05B3OP9Selected() || quadro05Model.isQ05B4OP10Selected() || quadro05Model.isQ05B4OP11Selected() || quadro05Model.isQ05C12Filled() || quadro05Model.isQ05C13Selected()) && anoExercicio != null && anoExercicio < 2008) {
            result.add(new DeclValidationMessage("R154", "aRosto.qQuadro05.fq05B3OP9", new String[]{"aRosto.qQuadro05.fq05B2OP7", "aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05B3OP9", "aRosto.qQuadro05.fq05B4OP10", "aRosto.qQuadro05.fq05B4OP11", "aRosto.qQuadro05.fq05C12", "aRosto.qQuadro05.fq05C13"}));
        }
    }

    protected void validateR089(ValidationResult result, Quadro05 quadro05Model) {
        if (!(quadro05Model.getQ05C12() == null || quadro05Model.getQ05C12() < 0 || quadro05Model.isQ05B3OP8Selected() || quadro05Model.isQ05B3OP9Selected())) {
            result.add(new DeclValidationMessage("R089", "aRosto.qQuadro05.fq05C12", new String[]{"aRosto.qQuadro05.fq05C12", "aRosto.qQuadro05.fq05B3OP8", "aRosto.qQuadro05.fq05B3OP9"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

