/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.rosto;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.util.Date;
import pt.opensoft.util.StringUtil;

public abstract class Quadro09Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro09Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        Quadro04 quadro04Model = (Quadro04)rostoModel.getQuadro(Quadro04.class.getSimpleName());
        Quadro09 quadro09Model = (Quadro09)rostoModel.getQuadro(Quadro09.class.getSimpleName());
        this.validateR040(result, quadro09Model);
        this.validateR041(result, quadro09Model);
        this.validateR042(result, quadro09Model);
        this.validateR043(result, quadro04Model, quadro09Model);
        this.validateR044(result, quadro09Model);
        this.validateR045(result, quadro04Model, quadro09Model);
        this.validateR179(result, quadro09Model);
        return result;
    }

    protected void validateR043(ValidationResult result, Quadro04 quadro04Model, Quadro09 quadro09Model) {
        if (quadro09Model.isQ09B1OP1Selected() && quadro04Model.getQ04B1() != null && quadro04Model.getQ04B1().equals("1")) {
            result.add(new DeclValidationMessage("R043", "aRosto.qQuadro09.fq09B1OP1", new String[]{"aRosto.qQuadro09.fq09B1OP1", "aRosto.qQuadro04.fq04B1OP2"}));
        }
    }

    protected void validateR042(ValidationResult result, Quadro09 quadro09Model) {
        if (quadro09Model.isQ09B1OP1Selected() && quadro09Model.getQ09C03() == null) {
            result.add(new DeclValidationMessage("R042", "aRosto.qQuadro09.fq09C03", new String[]{"aRosto.qQuadro09.fq09C03"}));
        }
    }

    protected void validateR041(ValidationResult result, Quadro09 quadro09Model) {
        if (!(quadro09Model.getQ09B1() == null || StringUtil.in(quadro09Model.getQ09B1(), new String[]{"1", "2"}))) {
            result.add(new DeclValidationMessage("R041", "aRosto.qQuadro09.fq09B1OP1", new String[]{"aRosto.qQuadro09.fq09B1OP1"}));
        }
    }

    protected void validateR040(ValidationResult result, Quadro09 quadro09Model) {
        if (quadro09Model.isQ09B1OP1Selected() && quadro09Model.isQ09B1OP2Selected()) {
            result.add(new DeclValidationMessage("R040", "aRosto.qQuadro09", new String[]{"aRosto.qQuadro09.fq09B1OP1", "aRosto.qQuadro09.fq09B1OP2"}));
        }
    }

    protected void validateR045(ValidationResult result, Quadro04 quadro04Model, Quadro09 quadro09Model) {
        if (quadro09Model.isQ09B1OP2Selected() && quadro04Model.getQ04B1() != null && quadro04Model.getQ04B1().equals("1")) {
            result.add(new DeclValidationMessage("R045", "aRosto.qQuadro09.fq09B1OP2", new String[]{"aRosto.qQuadro09.fq09B1OP2", "aRosto.qQuadro04.fq04B1OP2"}));
        }
    }

    protected void validateR044(ValidationResult result, Quadro09 quadro09Model) {
        if (quadro09Model.isQ09B1OP2Selected() && quadro09Model.getQ09C03() == null) {
            result.add(new DeclValidationMessage("R044", "aRosto.qQuadro09.fq09C03", new String[]{"aRosto.qQuadro09.fq09C03"}));
        }
    }

    public void validateR179(ValidationResult result, Quadro09 quadro09model) {
        if (quadro09model.getQ09C03() != null && quadro09model.getQ09B1() == null) {
            result.add(new DeclValidationMessage("R179", "aRosto.qQuadro09.fq09C03", new String[]{"aRosto.qQuadro09.fq09C03", "aRosto.qQuadro09.fq09B1OP1"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

