/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Cirs;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03 quadro03 = anexoCModel.getQuadro03();
            if (!Modelo3IRSValidatorUtil.equals(rostoModel.getQuadro03().getQ03C03(), quadro03.getAnexoCq03C04())) {
                result.add(new DeclValidationMessage("C006", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C04"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C04"), "aRosto.qQuadro03.fq03C03"}));
            }
            if (!Modelo3IRSValidatorUtil.equals(rostoModel.getQuadro03().getQ03C04(), quadro03.getAnexoCq03C05())) {
                result.add(new DeclValidationMessage("C007", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C05"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C05"), "aRosto.qQuadro03.fq03C04"}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C06()) || Modelo3IRSValidatorUtil.isNIFValid(quadro03.getAnexoCq03C06()))) {
                result.add(new DeclValidationMessage("C008", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C06()) || NifValidator.isSingular(String.valueOf(quadro03.getAnexoCq03C06())))) {
                result.add(new DeclValidationMessage("C009", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06")}));
            }
            this.validateC010(result, rostoModel, quadro03.getAnexoCq03B1(), quadro03.getAnexoCq03C06(), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"));
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C06()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C07())) {
                result.add(new DeclValidationMessage("C011", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07")}));
            }
            AnexoIModel anexoIModel = ((Modelo3IRSv2015Model)model).getAnexoIByTitular(anexoCModel.getAnexoCTitular());
            if (quadro03.getAnexoCq03B1() != null && quadro03.getAnexoCq03B1().equals("1") && anexoIModel == null) {
                result.add(new DeclValidationMessage("C012", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B1OP1"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07")}));
            }
            if (quadro03.getAnexoCq03B1() == null) {
                result.add(new DeclValidationMessage("C013", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B1OP1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B1OP1")}));
            }
            if (quadro03.getAnexoCq03B1() != null && quadro03.getAnexoCq03B1().equals("2") && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C06())) {
                result.add(new DeclValidationMessage("C014", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06")}));
            }
            if (!(quadro03.getAnexoCq03B1() == null || StringUtil.in(quadro03.getAnexoCq03B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("C157", anexoCModel.getLink("aAnexoC.qQuadro03"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B1OP1")}));
            }
            if (quadro03.getAnexoCq03B1() != null && quadro03.getAnexoCq03B1().equals("2") && !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C07())) {
                result.add(new DeclValidationMessage("C015", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B1OP2")}));
            }
            if (!(quadro03.getAnexoCq03C07() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C07()) || Modelo3IRSValidatorUtil.isNIFValid(quadro03.getAnexoCq03C07()))) {
                result.add(new DeclValidationMessage("C016", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07")}));
            }
            if (!(quadro03.getAnexoCq03C07() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C07()) || !Modelo3IRSValidatorUtil.isNIFValid(quadro03.getAnexoCq03C07()) || Modelo3IRSValidatorUtil.startsWith(String.valueOf(quadro03.getAnexoCq03C07()), "7", "9"))) {
                result.add(new DeclValidationMessage("C017", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07")}));
            }
            if (!(quadro03.getAnexoCq03C07() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C07()) || quadro03.getAnexoCq03C06() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C06()))) {
                result.add(new DeclValidationMessage("C018", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C07"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06")}));
            }
            ListMap cirsCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Cirs.class.getSimpleName());
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C08()) || cirsCatalog.containsKey(quadro03.getAnexoCq03C08().toString()))) {
                result.add(new DeclValidationMessage("C019", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C08"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C08")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C08()) || anexoCModel.getQuadro01().getAnexoCq01B1() != null && anexoCModel.getQuadro01().getAnexoCq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("C020", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C08"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B1"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C08")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C09()) || anexoCModel.getQuadro01().getAnexoCq01B1() != null && anexoCModel.getQuadro01().getAnexoCq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("C024", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C09"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B1"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C09")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C10()) || anexoCModel.getQuadro01().getAnexoCq01B2() != null && anexoCModel.getQuadro01().getAnexoCq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("C026", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C10"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B2"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C10")}));
            }
            if (quadro03.getAnexoCq03B2() == null) {
                result.add(new DeclValidationMessage("C027", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B2OP11"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B2OP11")}));
            }
            if (quadro03.getAnexoCq03B2() == null || StringUtil.in(quadro03.getAnexoCq03B2(), new String[]{"11", "12"})) continue;
            result.add(new DeclValidationMessage("C159", anexoCModel.getLink("aAnexoC.qQuadro03"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03B2OP11")}));
        }
        return result;
    }

    protected void validateC010(ValidationResult result, RostoModel rostoModel, String herancaIndivisa, Long linhaNIF, String linkNIF) {
        if (!(!"2".equalsIgnoreCase(herancaIndivisa) || rostoModel.getQuadro03().isSujeitoPassivoA(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoB(linhaNIF) || rostoModel.getQuadro03().existsInDependentes(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(linhaNIF) || rostoModel.getQuadro07().isConjugeFalecido(linhaNIF))) {
            result.add(new DeclValidationMessage("C010", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1"}));
        }
    }
}

