/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro02Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String C168 = "C168";

    public Quadro02Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateC168(ValidationResult result, Quadro02 quadro02AnexoCModel, RostoModel rostoModel) {
        if (!Modelo3IRSValidatorUtil.equals(quadro02AnexoCModel.getAnexoCq02C03(), rostoModel.getQuadro02().getQ02C02())) {
            result.add(new DeclValidationMessage("C168", "aAnexoC.qQuadro02.fanexoCq02C03", new String[]{"aAnexoC.qQuadro02.fanexoCq02C03", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro02 quadro02AnexoCModel = (Quadro02)anexoCModel.getQuadro(Quadro02.class.getSimpleName());
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            this.validateC168(result, quadro02AnexoCModel, rostoModel);
        }
        return result;
    }
}

