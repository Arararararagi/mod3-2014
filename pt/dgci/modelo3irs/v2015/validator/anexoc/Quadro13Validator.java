/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro13Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro13Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            long dobroF431;
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro13 quadro13 = anexoCModel.getQuadro13();
            Quadro04 quadro04 = anexoCModel.getQuadro04();
            this.validateC194(result, anexoCModel);
            if (Modelo3IRSValidatorUtil.isGreaterThanZero(quadro13.getAnexoCq13C1301()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1302())) {
                result.add(new DeclValidationMessage("C097", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1302"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1302"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1301")}));
            }
            if (Modelo3IRSValidatorUtil.isGreaterThanZero(quadro13.getAnexoCq13C1302()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1301())) {
                result.add(new DeclValidationMessage("C098", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1301"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1302"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1301")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1302()) || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1301()) || quadro13.getAnexoCq13C1302() <= quadro13.getAnexoCq13C1301())) {
                result.add(new DeclValidationMessage("C099", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1302"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1302"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1301")}));
            }
            this.validateC100(result, anexoCModel);
            this.validateC195(result, anexoCModel);
            this.validateC196(result, anexoCModel);
            this.validateC197(result, anexoCModel);
            this.validateC198(result, anexoCModel);
            if (Modelo3IRSValidatorUtil.isGreaterThanZero(quadro13.getAnexoCq13C1308()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1307())) {
                result.add(new DeclValidationMessage("C101", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1307"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1308"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1307")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1308()) || Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1307()) || quadro13.getAnexoCq13C1308() <= quadro13.getAnexoCq13C1307())) {
                result.add(new DeclValidationMessage("C102", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1308"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1308"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1307")}));
            }
            long somaSaldo = 0;
            if (quadro13.getAnexoCq13C1302() != null) {
                somaSaldo+=quadro13.getAnexoCq13C1302().longValue();
            }
            if (quadro13.getAnexoCq13C1308() != null) {
                somaSaldo+=quadro13.getAnexoCq13C1308().longValue();
            }
            if (Math.abs(somaSaldo - (dobroF431 = quadro04.getAnexoCq04C431() != null ? 2 * quadro04.getAnexoCq04C431() : 0)) <= 100) continue;
            String _link = quadro13.getAnexoCq13C1302() == null ? anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1308") : anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1302");
            result.add(new DeclValidationMessage("C103", anexoCModel.getLink("aAnexoC.qQuadro13"), new String[]{_link, anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C431")}));
        }
        return result;
    }

    protected void validateC100(ValidationResult result, AnexoCModel anexoCModel) {
        Quadro13 quadro13 = anexoCModel.getQuadro13();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (Modelo3IRSValidatorUtil.isGreaterThanZero(quadro13.getAnexoCq13C1307()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro13.getAnexoCq13C1308()) && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("C100", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1308"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1308"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1307")}));
        }
    }

    protected void validateC195(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1307 = anexoCModel.getQuadro13().getAnexoCq13C1307();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C1307 != null && ano != null && C1307 > 0 && ano > 2013) {
            result.add(new DeclValidationMessage("C195", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1307"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1307"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC196(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1312 = anexoCModel.getQuadro13().getAnexoCq13C1312();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C1312 != null && ano != null && C1312 > 0 && ano > 2013) {
            result.add(new DeclValidationMessage("C196", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1312"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1312"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC197(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1313 = anexoCModel.getQuadro13().getAnexoCq13C1313();
        Long C1314 = anexoCModel.getQuadro13().getAnexoCq13C1314();
        if (C1313 != null && C1313 > 0 && Modelo3IRSValidatorUtil.isEmptyOrZero(C1314) || C1314 != null && C1314 > 0 && Modelo3IRSValidatorUtil.isEmptyOrZero(C1313)) {
            result.add(new DeclValidationMessage("C197", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1314"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1314"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1313")}));
        }
    }

    protected void validateC198(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1314 = anexoCModel.getQuadro13().getAnexoCq13C1314();
        Long C1313 = anexoCModel.getQuadro13().getAnexoCq13C1313();
        if (C1313 != null && C1314 != null && C1314 > C1313) {
            result.add(new DeclValidationMessage("C198", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1314"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1314"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC194(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1313 = anexoCModel.getQuadro13().getAnexoCq13C1313();
        Long C1314 = anexoCModel.getQuadro13().getAnexoCq13C1314();
        Long C1315 = anexoCModel.getQuadro13().getAnexoCq13C1315();
        Long C1316 = anexoCModel.getQuadro13().getAnexoCq13C1316();
        Long C1317 = anexoCModel.getQuadro13().getAnexoCq13C1317();
        Long C1318 = anexoCModel.getQuadro13().getAnexoCq13C1318();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if ((C1313 != null && C1313 > 0 || C1314 != null && C1314 > 0 || C1315 != null && C1315 > 0 || C1316 != null && C1316 > 0 || C1317 != null && C1317 > 0 || C1318 != null && C1318 > 0) && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("C194", anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1302"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1313"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1314"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1315"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1316"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1317"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1318"), "aRosto.qQuadro02.fq02C02"}));
        }
    }
}

