/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro06 quadro06 = anexoCModel.getQuadro06();
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            Quadro04 quadro04 = anexoCModel.getQuadro04();
            this.validateC056(result, rostoModel, anexoCModel, quadro06);
            this.validateC057(result, rostoModel, anexoCModel, quadro06);
            this.validateC058(result, rostoModel, anexoCModel, quadro06);
            this.validateC059(result, rostoModel, anexoCModel, quadro06);
            this.validateC060(result, anexoCModel, quadro04, quadro06);
        }
        return result;
    }

    protected void validateC056(ValidationResult result, RostoModel rostoModel, AnexoCModel anexoCModel, Quadro06 quadro06) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro02().getQ02C02()) || rostoModel.getQuadro02().getQ02C02() == 2001 || quadro06.isEmpty())) {
            result.add(new DeclValidationMessage("C056", anexoCModel.getLink("aAnexoC.qQuadro06"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro06"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC057(ValidationResult result, RostoModel rostoModel, AnexoCModel anexoCModel, Quadro06 quadro06) {
        long c605;
        long l = c605 = quadro06.getAnexoCq06C605() != null ? quadro06.getAnexoCq06C605() : 0;
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro02().getQ02C02()) || rostoModel.getQuadro02().getQ02C02() != 2001 || Modelo3IRSValidatorUtil.equals(c605, quadro06.getSumC1()))) {
            result.add(new DeclValidationMessage("C057", anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C605"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C605")}));
        }
    }

    protected void validateC058(ValidationResult result, RostoModel rostoModel, AnexoCModel anexoCModel, Quadro06 quadro06) {
        long c610;
        long l = c610 = quadro06.getAnexoCq06C610() != null ? quadro06.getAnexoCq06C610() : 0;
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro02().getQ02C02()) || rostoModel.getQuadro02().getQ02C02() != 2001 || Modelo3IRSValidatorUtil.equals(c610, quadro06.getSumC2()))) {
            result.add(new DeclValidationMessage("C058", anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C610"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C610")}));
        }
    }

    protected void validateC059(ValidationResult result, RostoModel rostoModel, AnexoCModel anexoCModel, Quadro06 quadro06) {
        long c615;
        long l = c615 = quadro06.getAnexoCq06C615() != null ? quadro06.getAnexoCq06C615() : 0;
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro02().getQ02C02()) || rostoModel.getQuadro02().getQ02C02() != 2001 || Modelo3IRSValidatorUtil.equals(c615, quadro06.getSumC3()))) {
            result.add(new DeclValidationMessage("C059", anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C615"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C615")}));
        }
    }

    protected void validateC060(ValidationResult result, AnexoCModel anexoCModel, Quadro04 quadro04, Quadro06 quadro06) {
        if (quadro06.getAnexoCq06C616() != null && quadro04.getAnexoCq04C418() != null && quadro06.getAnexoCq06C616() < quadro04.getAnexoCq04C418()) {
            result.add(new DeclValidationMessage("C060", anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C616"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro06.fanexoCq06C616"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C418")}));
        }
    }
}

