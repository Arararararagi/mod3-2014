/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Cae_vs3;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.Quadro03Validator;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public class Quadro03NETValidator
extends Quadro03Validator {
    public Quadro03NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro03 quadro03 = anexoCModel.getQuadro03();
            ListMap catCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Cae_vs3.class.getSimpleName());
            if (!(quadro03.getAnexoCq03C09() == null || catCatalog.containsKey(StringUtil.padZeros(quadro03.getAnexoCq03C09().toString(), 5)))) {
                result.add(new DeclValidationMessage("C021", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C09"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C09")}));
            }
            if (quadro03.getAnexoCq03C10() == null || catCatalog.containsKey(StringUtil.padZeros(quadro03.getAnexoCq03C10().toString(), 5))) continue;
            result.add(new DeclValidationMessage("C025", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C10"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C10")}));
        }
        return result;
    }
}

