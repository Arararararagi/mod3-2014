/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.Quadro12Validator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro12NETValidator
extends Quadro12Validator {
    public Quadro12NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }
}

