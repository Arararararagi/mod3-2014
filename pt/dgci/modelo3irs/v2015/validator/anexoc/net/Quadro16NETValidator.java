/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.Quadro16Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro16NETValidator
extends Quadro16Validator {
    public Quadro16NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            this.validateC128(result, anexoCModel);
            this.validateC129(result, anexoCModel);
        }
        return result;
    }

    protected void validateC128(ValidationResult result, AnexoCModel anexoCModel) {
        if (!(anexoCModel.getQuadro16().getAnexoCq16B4() == null || !anexoCModel.getQuadro16().getAnexoCq16B4().booleanValue() || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro12().getAnexoCq12C1201()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro12().getAnexoCq12C1202()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro12().getAnexoCq12C1210()))) {
            result.add(new DeclValidationMessage("C128", anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B4"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B4")}));
        }
    }

    protected void validateC129(ValidationResult result, AnexoCModel anexoCModel) {
        if ((anexoCModel.getQuadro16().getAnexoCq16B4() == null || !anexoCModel.getQuadro16().getAnexoCq16B4().booleanValue()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro12().getAnexoCq12C1201()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro12().getAnexoCq12C1202()) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro12().getAnexoCq12C1210())) {
            result.add(new DeclValidationMessage("C129", anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B4"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B4")}));
        }
    }
}

