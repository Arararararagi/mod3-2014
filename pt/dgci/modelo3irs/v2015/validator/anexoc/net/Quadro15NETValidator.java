/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.Quadro15Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro15NETValidator
extends Quadro15Validator {
    public Quadro15NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro15 quadro15 = anexoCModel.getQuadro15();
            Quadro12 quadro12 = anexoCModel.getQuadro12();
            long somaRendQ12 = quadro12.getAnexoCq12C1202() != null ? quadro12.getAnexoCq12C1202() : 0;
            long somaRendQ15 = 0;
            if (quadro15.getAnexoCq15C1501() != null) {
                somaRendQ15+=quadro15.getAnexoCq15C1501().longValue();
            }
            if (quadro15.getAnexoCq15C1502() != null) {
                somaRendQ15+=quadro15.getAnexoCq15C1502().longValue();
            }
            if (somaRendQ12 >= somaRendQ15) continue;
            result.add(new DeclValidationMessage("C122", anexoCModel.getLink("aAnexoC.qQuadro15"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202"), anexoCModel.getLink("aAnexoC.qQuadro15")}));
        }
        return result;
    }
}

