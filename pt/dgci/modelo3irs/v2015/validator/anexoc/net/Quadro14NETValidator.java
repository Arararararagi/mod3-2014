/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.Quadro14Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro14NETValidator
extends Quadro14Validator {
    public Quadro14NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro14 quadro14 = anexoCModel.getQuadro14();
            if (quadro14.getAnexoCq14B1() != null && quadro14.getAnexoCq14B1().equals("1") && quadro14.getContagemLinhasTabela() <= 0) {
                result.add(new DeclValidationMessage("C105", anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1")}));
            }
            if (quadro14.getAnexoCq14B1() != null) continue;
            result.add(new DeclValidationMessage("C104", anexoCModel.getLink("aAnexoC.qQuadro14"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro14")}));
        }
        return result;
    }
}

