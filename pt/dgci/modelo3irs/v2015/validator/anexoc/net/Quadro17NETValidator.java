/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.Quadro17Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro17NETValidator
extends Quadro17Validator {
    public Quadro17NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro17 quadro17 = anexoCModel.getQuadro17();
            if (quadro17.getAnexoCq17C1701() != null) continue;
            result.add(new DeclValidationMessage("C130", anexoCModel.getLink("aAnexoC.qQuadro17.fanexoCq17C1701"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro17.fanexoCq17C1701")}));
        }
        return result;
    }
}

