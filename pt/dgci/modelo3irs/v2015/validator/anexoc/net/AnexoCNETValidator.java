/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.AnexoCValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro01NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro06NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro07NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro08NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro09NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro10NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro11NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro12NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro13NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro14NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro15NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro16NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.net.Quadro17NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoCNETValidator
extends AnexoCValidator {
    public AnexoCNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro01NETValidator().validate(model));
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        result.addAllFrom(new Quadro07NETValidator().validate(model));
        result.addAllFrom(new Quadro08NETValidator().validate(model));
        result.addAllFrom(new Quadro09NETValidator().validate(model));
        result.addAllFrom(new Quadro10NETValidator().validate(model));
        result.addAllFrom(new Quadro11NETValidator().validate(model));
        result.addAllFrom(new Quadro12NETValidator().validate(model));
        result.addAllFrom(new Quadro13NETValidator().validate(model));
        result.addAllFrom(new Quadro14NETValidator().validate(model));
        result.addAllFrom(new Quadro15NETValidator().validate(model));
        result.addAllFrom(new Quadro16NETValidator().validate(model));
        result.addAllFrom(new Quadro17NETValidator().validate(model));
        return result;
    }
}

