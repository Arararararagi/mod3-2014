/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoc.Quadro08Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro08NETValidator
extends Quadro08Validator {
    public Quadro08NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            long c802;
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro08 quadro08 = anexoCModel.getQuadro08();
            this.validateC061(result, anexoCModel);
            long somaRetencoesTabela = quadro08.getSomaRetencoesTabela();
            if (somaRetencoesTabela == (c802 = quadro08.getAnexoCq08C802() != null ? quadro08.getAnexoCq08C802() : 0)) continue;
            result.add(new DeclValidationMessage("C071", anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C802"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C802")}));
        }
        return result;
    }

    protected void validateC061(ValidationResult result, AnexoCModel anexoCModel) {
        long rendimentos;
        double percent285 = 0.285;
        double percent265 = 0.265;
        long l = rendimentos = anexoCModel != null && anexoCModel.getQuadro08().getAnexoCq08C801() != null ? anexoCModel.getQuadro08().getAnexoCq08C801() : 0;
        if (anexoCModel != null && anexoCModel.getQuadro08().getAnexoCq08C802() != null && (anexoCModel.getQuadro08().getAnexoCq08C802() > Math.round((double)rendimentos * 0.265) && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro15().getAnexoCq15C1502()) || anexoCModel.getQuadro08().getAnexoCq08C802() > Math.round((double)rendimentos * 0.285) && !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoCModel.getQuadro15().getAnexoCq15C1502()))) {
            result.add(new DeclValidationMessage("C061", anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C802"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C802")}));
        }
    }
}

