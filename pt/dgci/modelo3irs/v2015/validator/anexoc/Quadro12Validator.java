/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro12Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String C165 = "C165";
    public static final String C164 = "C164";
    public static final String C163 = "C163";

    public Quadro12Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateC163(ValidationResult result, AnexoCModel anexoC) {
        Long soma = Modelo3IRSValidatorUtil.sum(anexoC.getQuadro12().getAnexoCq12C1201(), anexoC.getQuadro12().getAnexoCq12C1202(), anexoC.getQuadro12().getAnexoCq12C1210());
        Long c1207 = anexoC.getQuadro12().getAnexoCq12C1207();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c1207) || Modelo3IRSValidatorUtil.equals(c1207, soma))) {
            result.add(new DeclValidationMessage("C163", anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1207"), new String[]{anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1207")}));
        }
    }

    protected void validateC164(ValidationResult result, AnexoCModel anexoC) {
        Long soma = Modelo3IRSValidatorUtil.sum(anexoC.getQuadro12().getAnexoCq12C1203(), anexoC.getQuadro12().getAnexoCq12C1204(), anexoC.getQuadro12().getAnexoCq12C1211());
        Long c1208 = anexoC.getQuadro12().getAnexoCq12C1208();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c1208) || Modelo3IRSValidatorUtil.equals(c1208, soma))) {
            result.add(new DeclValidationMessage("C164", anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1208"), new String[]{anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1208")}));
        }
    }

    protected void validateC165(ValidationResult result, AnexoCModel anexoC) {
        Long soma = Modelo3IRSValidatorUtil.sum(anexoC.getQuadro12().getAnexoCq12C1205(), anexoC.getQuadro12().getAnexoCq12C1206(), anexoC.getQuadro12().getAnexoCq12C1212());
        Long c1209 = anexoC.getQuadro12().getAnexoCq12C1209();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c1209) || Modelo3IRSValidatorUtil.equals(c1209, soma))) {
            result.add(new DeclValidationMessage("C165", anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1209"), new String[]{anexoC.getLink("aAnexoC.qQuadro12.fanexoCq12C1209")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            AnexoJModel anexoJModel = ((Modelo3IRSv2015Model)model).getAnexoJByTitular(anexoCModel.getAnexoCTitular());
            Quadro12 quadro12 = anexoCModel.getQuadro12();
            Quadro11 quadro11 = anexoCModel.getQuadro11();
            if (quadro12.getAnexoCq12C1202() != null && quadro11.getAnexoCq11C1101() != null && quadro12.getAnexoCq12C1202() < quadro11.getAnexoCq11C1101() && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() <= 2006) {
                result.add(new DeclValidationMessage("C091", anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202"), anexoCModel.getLink("aAnexoC.qQuadro11.fanexoCq11C1101")}));
            }
            this.validateC163(result, anexoCModel);
            this.validateC164(result, anexoCModel);
            this.validateC165(result, anexoCModel);
            this.validateC135(result, anexoCModel);
            this.validateC136(result, anexoCModel);
            this.validateC096(result, anexoCModel, anexoJModel);
            this.validateAnexoCq12T1(model, anexoCModel, result);
        }
        return result;
    }

    private void validateAnexoCq12T1(DeclaracaoModel model, AnexoCModel anexoCModel, ValidationResult result) {
        int i;
        AnexoCq12T1_Linha linha;
        Quadro12 quadro12 = anexoCModel.getQuadro12();
        EventList<AnexoCq12T1_Linha> AnexoCq12T1 = quadro12.getAnexoCq12T1();
        ArrayList<Long> nipcList = new ArrayList<Long>();
        this.validateC200(result, anexoCModel);
        Long subsidioDestinadoaExploracao = null;
        for (i = 0; i < AnexoCq12T1.size(); ++i) {
            linha = AnexoCq12T1.get(i);
            if (linha.getSubsidioDetinadoaExploracao() == null) continue;
            subsidioDestinadoaExploracao = subsidioDestinadoaExploracao == null ? linha.getSubsidioDetinadoaExploracao() : Long.valueOf(subsidioDestinadoaExploracao + linha.getSubsidioDetinadoaExploracao());
        }
        for (i = 0; i < AnexoCq12T1.size(); ++i) {
            linha = AnexoCq12T1.get(i);
            String lineLink = anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1.l" + (i + 1));
            String linkNipcDasEntidades = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
            this.validateC169(result, anexoCModel, linha, i);
            this.validateC170(result, anexoCModel, linha, nipcList, i);
            this.validateC171(result, anexoCModel, linha, i);
            this.validateC172(result, anexoCModel, linha, linkNipcDasEntidades);
            this.validateC173(result, anexoCModel, linha, linkNipcDasEntidades);
            this.validateC174(result, anexoCModel, linha, i);
            this.validateC191(result, anexoCModel, linha, i);
            this.validateC192(result, anexoCModel, linha, subsidioDestinadoaExploracao, i);
            this.validateC193(result, anexoCModel, linha);
        }
        this.validateC176(result, anexoCModel, quadro12);
    }

    protected void validateC169(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, int numLinha) {
        if (linha != null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidioDetinadoaExploracao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN4())) {
            String lineLink = anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1.l" + (numLinha + 1));
            result.add(new DeclValidationMessage("C169", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{lineLink}));
        }
    }

    protected void validateC200(ValidationResult result, AnexoCModel anexoCModel) {
        if (anexoCModel.getQuadro12().getAnexoCq12T1().size() > 100) {
            result.add(new DeclValidationMessage("C200", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1")}));
        }
    }

    protected void validateC170(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, List<Long> nipcList, int numLinha) {
        if (linha.getNIPCdasEntidades() != null) {
            long nipcDasEntidades = linha.getNIPCdasEntidades();
            String lineLink = anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1.l" + (numLinha + 1));
            String linkNipcDasEntidades = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
            if (nipcList.contains(nipcDasEntidades)) {
                result.add(new DeclValidationMessage("C170", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{linkNipcDasEntidades}));
            } else {
                nipcList.add(nipcDasEntidades);
            }
        }
    }

    protected void validateC171(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, int numLinha) {
        if (!(linha == null || !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidioDetinadoaExploracao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN4()))) {
            String lineLink = anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1.l" + (numLinha + 1));
            String linkNipcDasEntidades = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
            result.add(new DeclValidationMessage("C171", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{linkNipcDasEntidades}));
        }
    }

    protected void validateC172(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, String linkNipcDasEntidades) {
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) || Modelo3IRSValidatorUtil.isNIFValid(linha.getNIPCdasEntidades()))) {
            result.add(new DeclValidationMessage("C172", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{linkNipcDasEntidades}));
        }
    }

    protected void validateC173(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, String linkNipcDasEntidades) {
        if (!(linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades()) || !Modelo3IRSValidatorUtil.isNIFValid(linha.getNIPCdasEntidades()) || Modelo3IRSValidatorUtil.startsWith(linha.getNIPCdasEntidades(), "5", "6"))) {
            result.add(new DeclValidationMessage("C173", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{linkNipcDasEntidades}));
        }
    }

    protected void validateC174(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, int numLinha) {
        if (linha != null && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidioDetinadoaExploracao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getSubsidiosNaoDestinadosExploracaoN4()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIPCdasEntidades())) {
            String lineLink = anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1.l" + (numLinha + 1));
            String linkNipcDasEntidades = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.NIPCDASENTIDADES.getIndex();
            result.add(new DeclValidationMessage("C174", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{linkNipcDasEntidades}));
        }
    }

    protected void validateC191(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, int numLinha) {
        Long C3i = linha.getSubsidiosNaoDestinadosExploracaoN();
        Long C4i = linha.getSubsidiosNaoDestinadosExploracaoN1();
        Long C5i = linha.getSubsidiosNaoDestinadosExploracaoN2();
        Long C6i = linha.getSubsidiosNaoDestinadosExploracaoN3();
        Long C7i = linha.getSubsidiosNaoDestinadosExploracaoN4();
        String lineLink = anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1.l" + (numLinha + 1));
        String linkSubisidioNaoDestinadoExploracaoN = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN1 = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON1.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN2 = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON2.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN3 = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON3.getIndex();
        String linkSubisidioNaoDestinadoExploracaoN4 = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.SUBSIDIOSNAODESTINADOSEXPLORACAON4.getIndex();
        if (C3i != null && C3i > 0 && C4i == null && C5i == null && C6i == null && C7i == null || C3i == null && C4i != null && C4i > 0 && C5i == null && C6i == null && C7i == null || C3i == null && C4i == null && C5i != null && C5i > 0 && C6i == null && C7i == null || C3i == null && C4i == null && C5i == null && C6i != null && C6i > 0 && C7i == null || C3i == null && C4i == null && C5i == null && C6i == null && C7i != null && C7i > 0) {
            result.add(new DeclValidationMessage("C191", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{linkSubisidioNaoDestinadoExploracaoN, linkSubisidioNaoDestinadoExploracaoN1, linkSubisidioNaoDestinadoExploracaoN2, linkSubisidioNaoDestinadoExploracaoN3, linkSubisidioNaoDestinadoExploracaoN4}));
        }
    }

    protected void validateC192(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha, Long subsidioDestinadoaExploracao, int numLinha) {
        Long NIPC = linha.getNIPCdasEntidades();
        Long C3i = linha.getSubsidiosNaoDestinadosExploracaoN();
        Long C4i = linha.getSubsidiosNaoDestinadosExploracaoN1();
        Long C5i = linha.getSubsidiosNaoDestinadosExploracaoN2();
        Long C6i = linha.getSubsidiosNaoDestinadosExploracaoN3();
        Long C7i = linha.getSubsidiosNaoDestinadosExploracaoN4();
        Long C1210N = anexoCModel.getQuadro12().getAnexoCq12C1210();
        String lineLink = anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1.l" + (numLinha + 1));
        String linkSubisidioDestinadoExploracao = lineLink + ".c" + AnexoCq12T1_LinhaBase.Property.SUBSIDIODETINADOAEXPLORACAO.getIndex();
        if (!(C1210N == null || Modelo3IRSValidatorUtil.isEmptyOrZero(NIPC) || !Modelo3IRSValidatorUtil.isNIFValid(NIPC) || subsidioDestinadoaExploracao == null || subsidioDestinadoaExploracao <= 0 || C3i != null || C4i != null || C5i != null || C6i != null || C7i != null || C1210N.equals(subsidioDestinadoaExploracao))) {
            result.add(new DeclValidationMessage("C192", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1210"), linkSubisidioDestinadoExploracao}));
        }
    }

    protected void validateC193(ValidationResult result, AnexoCModel anexoCModel, AnexoCq12T1_Linha linha) {
        Long NIPC = linha.getNIPCdasEntidades();
        Long C2i = linha.getSubsidioDetinadoaExploracao();
        Long C3i = linha.getSubsidiosNaoDestinadosExploracaoN();
        Long C4i = linha.getSubsidiosNaoDestinadosExploracaoN1();
        Long C5i = linha.getSubsidiosNaoDestinadosExploracaoN2();
        Long C6i = linha.getSubsidiosNaoDestinadosExploracaoN3();
        Long C7i = linha.getSubsidiosNaoDestinadosExploracaoN4();
        Long C1210N = anexoCModel.getQuadro12().getAnexoCq12C1210();
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(NIPC) && Modelo3IRSValidatorUtil.isNIFValid(NIPC) && C2i == null && (C3i != null && C3i > 0 || C4i != null && C4i > 0 || C5i != null && C5i > 0 || C6i != null && C6i > 0 || C7i != null && C7i > 0) && (C1210N == null || C1210N == 0)) {
            result.add(new DeclValidationMessage("C193", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1210"), anexoCModel.getLink("aAnexoC.qQuadro12")}));
        }
    }

    protected void validateC176(ValidationResult result, AnexoCModel anexoCModel, Quadro12 quadro12Model) {
        if (!(quadro12Model.getAnexoCq12T1().isEmpty() || quadro12Model.getSomaValoresT1() <= 0 || quadro12Model.getAnexoCq12C1210() != null && quadro12Model.getAnexoCq12C1210() != 0)) {
            result.add(new DeclValidationMessage("C176", anexoCModel.getLink("aAnexoC.qQuadro12"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1210"), anexoCModel.getLink("aAnexoC.qQuadro12.tanexoCq12T1")}));
        }
    }

    protected void validateC096(ValidationResult result, AnexoCModel anexoCModel, AnexoJModel anexoJModel) {
        Quadro12 quadro12 = anexoCModel.getQuadro12();
        pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04 quadro04 = anexoCModel.getQuadro04();
        Quadro15 quadro15 = anexoCModel.getQuadro15();
        Quadro14 quadro14 = anexoCModel.getQuadro14();
        long rendimentosCatBAnexoC = 0;
        long rendimentosCatBAnexoJ = 0;
        long totalVendasPrestacoesServQ12 = 0;
        rendimentosCatBAnexoC+=quadro04.getAnexoCq04C443() != null ? quadro04.getAnexoCq04C443() : 0;
        rendimentosCatBAnexoC+=quadro14.getSomaValoresVendasTabela() != null ? quadro14.getSomaValoresVendasTabela() : 0;
        rendimentosCatBAnexoC+=quadro15.getAnexoCq15C1501() != null ? quadro15.getAnexoCq15C1501() : 0;
        rendimentosCatBAnexoC+=quadro15.getAnexoCq15C1502() != null ? quadro15.getAnexoCq15C1502() : 0;
        if (anexoJModel != null) {
            if (anexoJModel.getQuadro04().getAnexoJq04C403b() != null) {
                rendimentosCatBAnexoJ+=anexoJModel.getQuadro04().getAnexoJq04C403b().longValue();
            }
            if (anexoJModel.getQuadro04().getAnexoJq04C404b() != null) {
                rendimentosCatBAnexoJ+=anexoJModel.getQuadro04().getAnexoJq04C404b().longValue();
            }
            if (anexoJModel.getQuadro04().getAnexoJq04C405b() != null) {
                rendimentosCatBAnexoJ+=anexoJModel.getQuadro04().getAnexoJq04C405b().longValue();
            }
            if (anexoJModel.getQuadro04().getAnexoJq04C406b() != null) {
                rendimentosCatBAnexoJ+=anexoJModel.getQuadro04().getAnexoJq04C406b().longValue();
            }
            if (anexoJModel.getQuadro04().getAnexoJq04C426b() != null) {
                rendimentosCatBAnexoJ+=anexoJModel.getQuadro04().getAnexoJq04C426b().longValue();
            }
        }
        if (quadro12.getAnexoCq12C1201() != null) {
            totalVendasPrestacoesServQ12+=quadro12.getAnexoCq12C1201().longValue();
        }
        if (quadro12.getAnexoCq12C1202() != null) {
            totalVendasPrestacoesServQ12+=quadro12.getAnexoCq12C1202().longValue();
        }
        if (quadro12.getAnexoCq12C1210() != null) {
            totalVendasPrestacoesServQ12+=quadro12.getAnexoCq12C1210().longValue();
        }
        if (quadro04.getAnexoCq04C401() != null && quadro04.getAnexoCq04C401() >= 0 && rendimentosCatBAnexoC + rendimentosCatBAnexoJ > totalVendasPrestacoesServQ12) {
            String[] arrstring = new String[10];
            arrstring[0] = anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1201");
            arrstring[1] = anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202");
            arrstring[2] = anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C443");
            arrstring[3] = anexoCModel.getLink("aAnexoC.qQuadro14");
            arrstring[4] = anexoCModel.getLink("aAnexoC.qQuadro15");
            arrstring[5] = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b") : "aAnexoJ.qQuadro04.fanexoJq04C403b";
            arrstring[6] = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b") : "aAnexoJ.qQuadro04.fanexoJq04C404b";
            arrstring[7] = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b") : "aAnexoJ.qQuadro04.fanexoJq04C405b";
            arrstring[8] = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C406b") : "aAnexoJ.qQuadro04.fanexoJq04C406b";
            arrstring[9] = anexoJModel != null ? anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C426b") : "aAnexoJ.qQuadro04.fanexoJq04C426b";
            result.add(new DeclValidationMessage("C096", anexoCModel.getLink("aAnexoC.qQuadro12"), arrstring));
        }
    }

    protected void validateC136(ValidationResult result, AnexoCModel anexoCModel) {
        if (anexoCModel != null) {
            long anexoCq12C1210;
            Quadro12 quadro12 = anexoCModel.getQuadro12();
            pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04 quadro04 = anexoCModel.getQuadro04();
            long anexoCq04C401 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C401()) ? quadro04.getAnexoCq04C401() : 0;
            long anexoCq12C1201 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro12.getAnexoCq12C1201()) ? quadro12.getAnexoCq12C1201() : 0;
            long anexoCq12C1202 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro12.getAnexoCq12C1202()) ? quadro12.getAnexoCq12C1202() : 0;
            long l = anexoCq12C1210 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro12.getAnexoCq12C1210()) ? quadro12.getAnexoCq12C1210() : 0;
            if (anexoCq04C401 > anexoCq12C1201 + anexoCq12C1202 + anexoCq12C1210) {
                result.add(new DeclValidationMessage("C136", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C401"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C401"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1201"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1210"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202")}));
            }
        }
    }

    protected void validateC135(ValidationResult result, AnexoCModel anexoCModel) {
        Quadro12 quadro12 = anexoCModel.getQuadro12();
        if (quadro12 != null && (quadro12.getAnexoCq12C1201() == null || quadro12.getAnexoCq12C1202() == null || quadro12.getAnexoCq12C1203() == null || quadro12.getAnexoCq12C1204() == null || quadro12.getAnexoCq12C1205() == null || quadro12.getAnexoCq12C1206() == null || quadro12.getAnexoCq12C1210() == null || quadro12.getAnexoCq12C1211() == null || quadro12.getAnexoCq12C1212() == null)) {
            result.add(new DeclValidationMessage("C135", anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1203"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1201"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1203"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1204"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1205"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1206"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1210"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1211"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1212")}));
        }
    }
}

