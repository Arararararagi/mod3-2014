/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.Date;
import pt.opensoft.util.StringUtil;

public abstract class Quadro16Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro16Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro16 quadro16 = anexoCModel.getQuadro16();
            Quadro03 quadro03 = anexoCModel.getQuadro03();
            if (quadro16.getAnexoCq16B1() == null) {
                result.add(new DeclValidationMessage("C123", anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B1OP2"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B1OP2")}));
            }
            if (!(quadro16.getAnexoCq16B1() == null || StringUtil.in(quadro16.getAnexoCq16B1(), new String[]{"1", "2"}))) {
                result.add(new DeclValidationMessage("C149", anexoCModel.getLink("aAnexoC.qQuadro16"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B1OP1")}));
            }
            if (quadro16.getAnexoCq16B1() != null && quadro16.getAnexoCq16B1().equals("2") && quadro03.getAnexoCq03C06() != null && rostoModel.getQuadro07().isConjugeFalecido(quadro03.getAnexoCq03C06())) {
                result.add(new DeclValidationMessage("C124", anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B1OP1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C06"), anexoCModel.getLink("aRosto.qQuadro07.fq07C1"), anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B1OP1")}));
            }
            if (quadro16.getAnexoCq16B1() != null && quadro16.getAnexoCq16B1().equals("1") && quadro16.getAnexoCq16C3() == null) {
                result.add(new DeclValidationMessage("C125", anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16C3"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16C3")}));
            }
            if (quadro16.getAnexoCq16B1() != null && quadro16.getAnexoCq16B1().equals("2") && quadro16.getAnexoCq16C3() != null) {
                result.add(new DeclValidationMessage("C126", anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16C3"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16C3"), anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B1OP2")}));
            }
            if (!(quadro16.getAnexoCq16C3() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro02().getQ02C02()) || Modelo3IRSValidatorUtil.equals(new Long(quadro16.getAnexoCq16C3().getYear()), rostoModel.getQuadro02().getQ02C02()))) {
                result.add(new DeclValidationMessage("C127", anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16C3"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16C3"), "aRosto.qQuadro02.fq02C02"}));
            }
            if (quadro16.getAnexoCq16B4() == null || quadro16.getAnexoCq16B4().equals(Boolean.TRUE) || quadro16.getAnexoCq16B4().equals(Boolean.FALSE)) continue;
            result.add(new DeclValidationMessage("C150", anexoCModel.getLink(anexoCModel.getLink("aAnexoC.qQuadro16")), new String[]{anexoCModel.getLink("aAnexoC.qQuadro16.fanexoCq16B4")}));
        }
        return result;
    }
}

