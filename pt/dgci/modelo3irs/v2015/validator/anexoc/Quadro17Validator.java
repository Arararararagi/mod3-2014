/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro17Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro17Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro17 quadro17 = anexoCModel.getQuadro17();
            this.validateC131(result, anexoCModel, quadro17);
            this.validateC132(result, anexoCModel, quadro17);
        }
        return result;
    }

    protected void validateC131(ValidationResult result, AnexoCModel anexoCModel, Quadro17 quadro17) {
        if (!(quadro17.getAnexoCq17C1701() == null || Modelo3IRSValidatorUtil.startsWith(quadro17.getAnexoCq17C1701(), "1", "2"))) {
            result.add(new DeclValidationMessage("C131", anexoCModel.getLink("aAnexoC.qQuadro17.fanexoCq17C1701"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro17.fanexoCq17C1701")}));
        }
    }

    protected void validateC132(ValidationResult result, AnexoCModel anexoCModel, Quadro17 quadro17) {
        if (!(quadro17.getAnexoCq17C1701() == null || Modelo3IRSValidatorUtil.isNIFValid(quadro17.getAnexoCq17C1701()))) {
            result.add(new DeclValidationMessage("C132", anexoCModel.getLink("aAnexoC.qQuadro17.fanexoCq17C1701"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro17.fanexoCq17C1701")}));
        }
    }
}

