/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro10Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro10Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            this.validateC187(result, anexoCModel);
            this.validateC188(result, anexoCModel);
            this.validateC189(result, anexoCModel);
            this.validateC190(result, anexoCModel);
            this.validateC086(result, anexoCModel);
        }
        return result;
    }

    protected void validateC187(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1002 = anexoCModel.getQuadro10().getAnexoCq10C1002();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (ano != null && C1002 != null && C1002 > 0 && ano > 2013) {
            result.add(new DeclValidationMessage("C187", anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1002"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1002"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC188(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1006 = anexoCModel.getQuadro10().getAnexoCq10C1006();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (ano != null && C1006 != null && C1006 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C188", anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1006"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1006"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC189(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1003 = anexoCModel.getQuadro10().getAnexoCq10C1003();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (ano != null && C1003 != null && C1003 > 0 && ano > 2013) {
            result.add(new DeclValidationMessage("C189", anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1003"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1003"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC190(ValidationResult result, AnexoCModel anexoCModel) {
        Long C1007 = anexoCModel.getQuadro10().getAnexoCq10C1007();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (ano != null && C1007 != null && C1007 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C190", anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1007"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1007"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC086(ValidationResult result, AnexoCModel anexoCModel) {
        long c10C1;
        long l = c10C1 = anexoCModel.getQuadro10().getAnexoCq10C1() != null ? anexoCModel.getQuadro10().getAnexoCq10C1() : 0;
        if (!Modelo3IRSValidatorUtil.equals(c10C1, anexoCModel.getQuadro10().getSumC1())) {
            result.add(new DeclValidationMessage("C086", anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1")}));
        }
    }
}

