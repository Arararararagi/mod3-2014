/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro05;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro05Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro05Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro05 quadro05 = anexoCModel.getQuadro05();
            Quadro01 quadro01 = anexoCModel.getQuadro01();
            Quadro04 quadro04 = anexoCModel.getQuadro04();
            long lucrosQ4 = quadro04.getAnexoCq04C460() != null ? quadro04.getAnexoCq04C460() : 0;
            long prejuizosQ4 = quadro04.getAnexoCq04C459() != null ? quadro04.getAnexoCq04C459() : 0;
            long _501_prejuizosProf = quadro05.getAnexoCq05C501() != null ? quadro05.getAnexoCq05C501() : 0;
            long _503_lucrosProf = quadro05.getAnexoCq05C503() != null ? quadro05.getAnexoCq05C503() : 0;
            long _502_prejuizosAgric = quadro05.getAnexoCq05C502() != null ? quadro05.getAnexoCq05C502() : 0;
            long _504_lucrosAgric = quadro05.getAnexoCq05C504() != null ? quadro05.getAnexoCq05C504() : 0;
            long _505_prejuizosFinan = quadro05.getAnexoCq05C505() != null ? quadro05.getAnexoCq05C505() : 0;
            long _506_lucrosFinan = quadro05.getAnexoCq05C506() != null ? quadro05.getAnexoCq05C506() : 0;
            long prejuizosQ5 = _501_prejuizosProf + _502_prejuizosAgric + _505_prejuizosFinan;
            long lucrosQ5 = _503_lucrosProf + _504_lucrosAgric + _506_lucrosFinan;
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C501()) || quadro05.getAnexoCq05C501() <= 0 || quadro01.getAnexoCq01B1() != null && quadro01.getAnexoCq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("C045", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C501"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C501"), anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B1")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C502()) || quadro05.getAnexoCq05C502() <= 0 || quadro01.getAnexoCq01B2() != null && quadro01.getAnexoCq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("C046", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B2")}));
            }
            if (lucrosQ5 + prejuizosQ5 > 0 && lucrosQ4 - prejuizosQ4 != lucrosQ5 - prejuizosQ5) {
                result.add(new DeclValidationMessage("C047", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C501"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C436"), anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C501")}));
            }
            if (_501_prejuizosProf > 0 && _503_lucrosProf > 0) {
                result.add(new DeclValidationMessage("C048", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C501"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C501"), anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C503")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C503()) || quadro05.getAnexoCq05C503() <= 0 || quadro01.getAnexoCq01B1() != null && quadro01.getAnexoCq01B1().booleanValue())) {
                result.add(new DeclValidationMessage("C049", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C503"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C503"), anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B1")}));
            }
            if (_502_prejuizosAgric > 0 && _504_lucrosAgric > 0) {
                result.add(new DeclValidationMessage("C050", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C504")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C504()) || quadro05.getAnexoCq05C504() <= 0 || quadro01.getAnexoCq01B2() != null && quadro01.getAnexoCq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("C051", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C504"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C504"), anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B2")}));
            }
            if (quadro01.getAnexoCq01B2() != null && quadro01.getAnexoCq01B2().booleanValue() && quadro01.getAnexoCq01B1() != null && quadro01.getAnexoCq01B1().booleanValue() && lucrosQ4 - prejuizosQ4 != lucrosQ5 - prejuizosQ5) {
                result.add(new DeclValidationMessage("C052", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C436")}));
            }
            if (quadro01.getAnexoCq01B2() != null && quadro01.getAnexoCq01B2().booleanValue() && quadro01.getAnexoCq01B1() != null && quadro01.getAnexoCq01B1().booleanValue() && (_501_prejuizosProf > 0 || _503_lucrosProf > 0) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C502()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C504())) {
                result.add(new DeclValidationMessage("C053", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C436")}));
            }
            if (quadro01.getAnexoCq01B2() != null && quadro01.getAnexoCq01B2().booleanValue() && quadro01.getAnexoCq01B1() != null && quadro01.getAnexoCq01B1().booleanValue() && (_502_prejuizosAgric > 0 || _504_lucrosAgric > 0) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C501()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro05.getAnexoCq05C503())) {
                result.add(new DeclValidationMessage("C054", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C502"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C436")}));
            }
            if (_505_prejuizosFinan <= 0 || _506_lucrosFinan <= 0) continue;
            result.add(new DeclValidationMessage("C055", anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C505"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C505"), anexoCModel.getLink("aAnexoC.qQuadro05.fanexoCq05C506")}));
        }
        return result;
    }
}

