/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Freguesia;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public abstract class Quadro14Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro14Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected ValidationResult validateLinhas(DeclaracaoModel model, AnexoCModel anexoCModel, Quadro14 quadro14) {
        ValidationResult result = new ValidationResult();
        EventList<AnexoCq14T1_Linha> linhas = quadro14.getAnexoCq14T1();
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoCq14T1_Linha linha = linhas.get(i);
            String linkCurrentLine = anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1") + ".l" + (i + 1);
            String linkFreguesia = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.FREGUESIA.getIndex();
            String linkTipoPredio = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.TIPOPREDIO.getIndex();
            String linkArtigo = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.ARTIGO.getIndex();
            String linkFraccao = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.FRACCAO.getIndex();
            String linkValorVenda = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.VALORVENDA.getIndex();
            String linkValorDefinitivo = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.VALORDEFINITIVO.getIndex();
            String linkArt139CIRS = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.ART139CIRC.getIndex();
            if (this.isLineRepeated(linha, i, linhas)) {
                result.add(new DeclValidationMessage("C108", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (StringUtil.isEmpty(linha.getFreguesia()) && StringUtil.isEmpty(linha.getTipoPredio()) && StringUtil.isEmpty(linha.getFraccao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorVenda()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorDefinitivo()) && (linha.getArt139CIRC() == null || linha.getArt139CIRC().equals("N"))) {
                result.add(new DeclValidationMessage("C134", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            ListMap fregCatalog = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Freguesia.class.getSimpleName());
            if (!(linha.getFreguesia() == null || fregCatalog.containsKey(linha.getFreguesia()))) {
                result.add(new DeclValidationMessage("C111", linkFreguesia, new String[]{linkFreguesia}));
            }
            if (!(!StringUtil.isEmpty(linha.getFreguesia()) || StringUtil.isEmpty(linha.getTipoPredio()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) && StringUtil.isEmpty(linha.getFraccao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorVenda()) && (StringUtil.isEmpty(linha.getArt139CIRC()) || !linha.getArt139CIRC().equals("S")))) {
                result.add(new DeclValidationMessage("C112", linkFreguesia, new String[]{linkFreguesia}));
            }
            if (!StringUtil.isEmpty(linha.getFreguesia()) && (StringUtil.isEmpty(linha.getTipoPredio()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorVenda()) || StringUtil.isEmpty(linha.getArt139CIRC()))) {
                result.add(new DeclValidationMessage("C113", linkFreguesia, new String[]{linkFreguesia, linkTipoPredio, linkValorVenda, linkValorDefinitivo, linkArt139CIRS}));
            }
            if (!(linha.getTipoPredio() == null || StringUtil.in(linha.getTipoPredio(), new String[]{"U", "R", "O"}))) {
                result.add(new DeclValidationMessage("C114", linkTipoPredio, new String[]{linkTipoPredio}));
            }
            if (linha.getTipoPredio() != null && StringUtil.in(linha.getTipoPredio(), new String[]{"U", "R"}) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo())) {
                result.add(new DeclValidationMessage("C115", linkArtigo, new String[]{linkTipoPredio, linkArtigo}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) || linha.getTipoPredio() == null || StringUtil.in(linha.getTipoPredio(), new String[]{"U", "R"}))) {
                result.add(new DeclValidationMessage("C116", linkArtigo, new String[]{linkArtigo, linkTipoPredio}));
            }
            if (!(linha.getFraccao() == null || Modelo3IRSValidatorUtil.isAlphaNumericOrSpace(linha.getFraccao()))) {
                result.add(new DeclValidationMessage("C117", linkFraccao, new String[]{linkFraccao}));
            }
            if (!(StringUtil.isEmpty(linha.getFraccao()) || linha.getTipoPredio() == null || StringUtil.in(linha.getTipoPredio(), new String[]{"U", "R"}))) {
                result.add(new DeclValidationMessage("C109", linkFraccao, new String[]{linkFraccao, linkTipoPredio}));
            }
            if (linha.getArt139CIRC() != null && StringUtil.in(linha.getArt139CIRC(), new String[]{"S", "N"})) continue;
            result.add(new DeclValidationMessage("C118", linkArt139CIRS, new String[]{linkArt139CIRS}));
        }
        return result;
    }

    protected boolean isLineRepeated(AnexoCq14T1_Linha linha, int linePosition, EventList<AnexoCq14T1_Linha> linhas) {
        for (int i = linePosition - 1; i >= 0; --i) {
            AnexoCq14T1_Linha linhaComparar = linhas.get(i);
            if (!Modelo3IRSValidatorUtil.equals(linha.getFreguesia(), linhaComparar.getFreguesia()) || "O".equals(linha.getTipoPredio()) || !Modelo3IRSValidatorUtil.equals(linha.getTipoPredio(), linhaComparar.getTipoPredio()) || !Modelo3IRSValidatorUtil.equals(linha.getArtigo(), linhaComparar.getArtigo()) || !Modelo3IRSValidatorUtil.equals(linha.getFraccao(), linhaComparar.getFraccao())) continue;
            return true;
        }
        return false;
    }

    protected boolean isLineBlank(AnexoCq14T1_Linha linha) {
        return StringUtil.isEmpty(linha.getFreguesia()) && StringUtil.isEmpty(linha.getTipoPredio()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getArtigo()) && StringUtil.isEmpty(linha.getFraccao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorVenda()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorDefinitivo()) && StringUtil.isEmpty(linha.getArt139CIRC());
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            long somatorioValoresVendas;
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro14 quadro14 = anexoCModel.getQuadro14();
            Quadro12 quadro12 = anexoCModel.getQuadro12();
            Quadro04 quadro04 = anexoCModel.getQuadro04();
            if (quadro14.getContagemLinhasTabela() > 0 && (quadro14.getAnexoCq14B1() == null || quadro14.getAnexoCq14B1().equals("2"))) {
                result.add(new DeclValidationMessage("C106", anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro14.fanexoCq14B1OP2"), anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1")}));
            }
            if (quadro14.getContagemLinhasTabela() > 200) {
                result.add(new DeclValidationMessage("C107", anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1")}));
            }
            ValidationResult linhasValidationResult = this.validateLinhas(model, anexoCModel, quadro14);
            result.addAllFrom(linhasValidationResult);
            long totalVendasPrestacoesServQ12 = 0;
            if (quadro12.getAnexoCq12C1201() != null) {
                totalVendasPrestacoesServQ12+=quadro12.getAnexoCq12C1201().longValue();
            }
            if (quadro12.getAnexoCq12C1202() != null) {
                totalVendasPrestacoesServQ12+=quadro12.getAnexoCq12C1202().longValue();
            }
            long l = somatorioValoresVendas = quadro14.getSomaValoresVendasTabela() != null ? quadro14.getSomaValoresVendasTabela() : 0;
            if (somatorioValoresVendas > totalVendasPrestacoesServQ12) {
                if (quadro14.getAnexoCq14T1().isEmpty()) {
                    result.add(new DeclValidationMessage("C119", anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1201"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202")}));
                } else {
                    String linkCurrentLine = anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1") + ".l1";
                    String linkValorVenda = linkCurrentLine + ".c" + AnexoCq14T1_LinhaBase.Property.VALORVENDA.getIndex();
                    result.add(new DeclValidationMessage("C119", anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), new String[]{linkValorVenda, anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1201"), anexoCModel.getLink("aAnexoC.qQuadro12.fanexoCq12C1202")}));
                }
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C437()) || quadro14.getSomaDiferencasC120() <= quadro04.getAnexoCq04C437()) continue;
            result.add(new DeclValidationMessage("C120", anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro14.tanexoCq14T1"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C437")}));
        }
        return result;
    }
}

