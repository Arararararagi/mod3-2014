/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro08Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro08Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected ValidationResult validateLinhas(DeclaracaoModel model, AnexoCModel anexoCModel, Quadro08 quadro08, RostoModel rostoModel) {
        ValidationResult result = new ValidationResult();
        EventList<AnexoCq08T1_Linha> linhas = quadro08.getAnexoCq08T1();
        this.validateC199(result, anexoCModel);
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoCq08T1_Linha linha = linhas.get(i);
            String linkCurrentLine = anexoCModel.getLink("aAnexoC.qQuadro08.tanexoCq08T1") + ".l" + (i + 1);
            String linkNif = linkCurrentLine + ".c" + AnexoCq08T1_LinhaBase.Property.NIF.getIndex();
            String linkValor = linkCurrentLine + ".c" + AnexoCq08T1_LinhaBase.Property.VALOR.getIndex();
            if (linha.getNIF() != null && this.isNifRepeated(linha.getNIF(), i, linhas)) {
                result.add(new DeclValidationMessage("C065", linkNif, new String[]{linkNif}));
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValor())) {
                result.add(new DeclValidationMessage("C066", linkCurrentLine, new String[]{linkCurrentLine}));
            }
            if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.startsWith(linha.getNIF(), "1", "2", "5", "6", "7", "9"))) {
                result.add(new DeclValidationMessage("C067", linkNif, new String[]{linkNif}));
            }
            if (!(linha.getNIF() == null || Modelo3IRSValidatorUtil.isNIFValid(linha.getNIF()))) {
                result.add(new DeclValidationMessage("C068", linkNif, new String[]{linkNif}));
            }
            this.validateC069(result, rostoModel, linha.getNIF(), linkNif);
            if ((linha.getNIF() == null || !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValor())) && (linha.getNIF() != null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValor()))) continue;
            result.add(new DeclValidationMessage("C070", linkNif, new String[]{linkNif, linkValor}));
        }
        return result;
    }

    protected void validateC199(ValidationResult result, AnexoCModel anexoCModel) {
        if (anexoCModel.getQuadro08().getAnexoCq08T1().size() > 2000) {
            result.add(new DeclValidationMessage("C199", anexoCModel.getLink("aAnexoC.qQuadro08.tanexoCq08T1"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro08.tanexoCq08T1")}));
        }
    }

    protected void validateC069(ValidationResult result, RostoModel rostoModel, Long linhaNIF, String linkNif) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (rostoModel.getQuadro03().isSujeitoPassivoA(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoB(linhaNIF) || rostoModel.getQuadro03().existsInDependentes(linhaNIF) || rostoModel.getQuadro07().isConjugeFalecido(linhaNIF) || rostoModel.getQuadro07().existsInAscendentes(linhaNIF) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(linhaNIF) || rostoModel.getQuadro07().existsInAscendentesEColaterais(linhaNIF))) {
            result.add(new DeclValidationMessage("C069", linkNif, new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01"}));
        }
    }

    protected boolean isNifRepeated(Long nif, int nifPosition, EventList<AnexoCq08T1_Linha> linhas) {
        for (int i = nifPosition - 1; i >= 0; --i) {
            AnexoCq08T1_Linha linhaComparar = linhas.get(i);
            if (linhaComparar.getNIF() == null || !linhaComparar.getNIF().equals(nif)) continue;
            return true;
        }
        return false;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            boolean opcaoMadeira;
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro08 quadro08 = anexoCModel.getQuadro08();
            if (quadro08.getAnexoCq08C802() != null && quadro08.getAnexoCq08C801() == null) {
                result.add(new DeclValidationMessage("C063", anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C801"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C801")}));
            }
            boolean anoAnterior2008 = !Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro02().getQ02C02()) && rostoModel.getQuadro02().getQ02C02() < 2008;
            boolean bl = opcaoMadeira = rostoModel.getQuadro05().getQ05B1() != null && rostoModel.getQuadro05().isQ05B1OP3Selected();
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro08.getAnexoCq08C805()) || !anoAnterior2008 && opcaoMadeira)) {
                result.add(new DeclValidationMessage("C064", anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C805"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro08.fanexoCq08C805"), "aRosto.qQuadro05.fq05B1OP3"}));
            }
            ValidationResult linhasValidationResult = this.validateLinhas(model, anexoCModel, quadro08, rostoModel);
            result.addAllFrom(linhasValidationResult);
        }
        return result;
    }
}

