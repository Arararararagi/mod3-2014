/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro01Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro01Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro01 quadro01 = anexoCModel.getQuadro01();
            Quadro03 quadro03 = anexoCModel.getQuadro03();
            if (!(quadro01.getAnexoCq01B1() != null && quadro01.getAnexoCq01B1().booleanValue() || quadro01.getAnexoCq01B2() != null && quadro01.getAnexoCq01B2().booleanValue())) {
                result.add(new DeclValidationMessage("C003", anexoCModel.getLink("aAnexoC.qQuadro01"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B2"), anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B1")}));
            }
            if (quadro01.getAnexoCq01B1() != null && quadro01.getAnexoCq01B1().booleanValue() && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C08()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C09())) {
                result.add(new DeclValidationMessage("C004", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C09"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C09"), anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C08")}));
            }
            if (!(quadro01.getAnexoCq01B1() == null || quadro01.getAnexoCq01B1().equals(Boolean.TRUE) || quadro01.getAnexoCq01B1().equals(Boolean.FALSE))) {
                result.add(new DeclValidationMessage("C152", anexoCModel.getLink(anexoCModel.getLink("aAnexoC.qQuadro01")), new String[]{anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B1")}));
            }
            if (quadro01.getAnexoCq01B2() != null && quadro01.getAnexoCq01B2().booleanValue() && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro03.getAnexoCq03C10())) {
                result.add(new DeclValidationMessage("C005", anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C10"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C10")}));
            }
            if (quadro01.getAnexoCq01B2() == null || quadro01.getAnexoCq01B2().equals(Boolean.TRUE) || quadro01.getAnexoCq01B2().equals(Boolean.FALSE)) continue;
            result.add(new DeclValidationMessage("C153", anexoCModel.getLink(anexoCModel.getLink("aAnexoC.qQuadro01")), new String[]{anexoCModel.getLink("aAnexoC.qQuadro01.fanexoCq01B2")}));
        }
        return result;
    }
}

