/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro11Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro11Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro11 quadro11 = anexoCModel.getQuadro11();
            Quadro03 quadro03 = anexoCModel.getQuadro03();
            this.validateC087(result, quadro11, quadro03, anexoCModel);
            this.validateC088(result, quadro11, quadro03, rostoModel, anexoCModel);
        }
        return result;
    }

    protected void validateC087(ValidationResult result, Quadro11 quadro11, Quadro03 quadro03, AnexoCModel anexoCModel) {
        if (Modelo3IRSValidatorUtil.isGreaterThanZero(quadro11.getAnexoCq11C1101()) && (quadro03.getAnexoCq03C08() == null || quadro03.getAnexoCq03C08() != 1323)) {
            result.add(new DeclValidationMessage("C087", anexoCModel.getLink("aAnexoC.qQuadro11.fanexoCq11C1101"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro03.fanexoCq03C08"), anexoCModel.getLink("aAnexoC.qQuadro11.fanexoCq11C1101")}));
        }
    }

    protected void validateC088(ValidationResult result, Quadro11 quadro11, Quadro03 quadro03, RostoModel rostoModel, AnexoCModel anexoCModel) {
        if (Modelo3IRSValidatorUtil.isGreaterThanZero(quadro11.getAnexoCq11C1101()) && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() > 2006) {
            result.add(new DeclValidationMessage("C088", anexoCModel.getLink("aAnexoC.qQuadro11.fanexoCq11C1101"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro11.fanexoCq11C1101"), "aRosto.qQuadro02.fq02C02"}));
        }
    }
}

