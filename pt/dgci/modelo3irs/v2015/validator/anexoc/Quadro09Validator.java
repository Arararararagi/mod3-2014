/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro09Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro09Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected Long getAnoByPos(int pos, Quadro09 quadro09) {
        if (pos == 1) {
            return quadro09.getAnexoCq09C902();
        }
        if (pos == 2) {
            return quadro09.getAnexoCq09C903();
        }
        if (pos == 3) {
            return quadro09.getAnexoCq09C904();
        }
        if (pos == 4) {
            return quadro09.getAnexoCq09C905();
        }
        if (pos == 5) {
            return quadro09.getAnexoCq09C906();
        }
        if (pos == 6) {
            return quadro09.getAnexoCq09C907();
        }
        return null;
    }

    protected String getAnoLinkByPos(int pos, Quadro09 quadro09, AnexoCModel anexoCModel) {
        if (pos == 1) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C902");
        }
        if (pos == 2) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C903");
        }
        if (pos == 3) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C904");
        }
        if (pos == 4) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C905");
        }
        if (pos == 5) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C906");
        }
        if (pos == 6) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C907");
        }
        return "";
    }

    protected Long getRendimentosPCIByPos(int pos, Quadro09 quadro09) {
        if (pos == 1) {
            return quadro09.getAnexoCq09C908();
        }
        if (pos == 2) {
            return quadro09.getAnexoCq09C909();
        }
        if (pos == 3) {
            return quadro09.getAnexoCq09C910();
        }
        if (pos == 4) {
            return quadro09.getAnexoCq09C911();
        }
        if (pos == 5) {
            return quadro09.getAnexoCq09C912();
        }
        if (pos == 6) {
            return quadro09.getAnexoCq09C913();
        }
        return null;
    }

    protected String getRendimentosPCILinkByPos(int pos, Quadro09 quadro09, AnexoCModel anexoCModel) {
        if (pos == 1) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C908");
        }
        if (pos == 2) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C909");
        }
        if (pos == 3) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C910");
        }
        if (pos == 4) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C911");
        }
        if (pos == 5) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C912");
        }
        if (pos == 6) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C913");
        }
        return "";
    }

    protected Long getRendimentosASPByPos(int pos, Quadro09 quadro09) {
        if (pos == 1) {
            return quadro09.getAnexoCq09C914();
        }
        if (pos == 2) {
            return quadro09.getAnexoCq09C915();
        }
        if (pos == 3) {
            return quadro09.getAnexoCq09C916();
        }
        if (pos == 4) {
            return quadro09.getAnexoCq09C917();
        }
        if (pos == 5) {
            return quadro09.getAnexoCq09C918();
        }
        if (pos == 6) {
            return quadro09.getAnexoCq09C919();
        }
        return null;
    }

    protected String getRendimentosASPLinkByPos(int pos, Quadro09 quadro09, AnexoCModel anexoCModel) {
        if (pos == 1) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C914");
        }
        if (pos == 2) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C915");
        }
        if (pos == 3) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C916");
        }
        if (pos == 4) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C917");
        }
        if (pos == 5) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C918");
        }
        if (pos == 6) {
            return anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C919");
        }
        return "";
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        List<FormKey> keysAnexosB = model.getAllAnexosByType("AnexoB");
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            Quadro09 quadro09 = anexoCModel.getQuadro09();
            Long nif = quadro09.getAnexoCq09C901();
            this.validateC073(result, nif, anexoCModel);
            this.validateC074(result, nif, anexoCModel);
            boolean yearDataFill = quadro09.getAnexoCq09C902() != null || quadro09.getAnexoCq09C903() != null || quadro09.getAnexoCq09C904() != null || quadro09.getAnexoCq09C905() != null || quadro09.getAnexoCq09C906() != null || quadro09.getAnexoCq09C907() != null;
            this.validateC075(result, nif, anexoCModel, yearDataFill);
            this.validateC076(result, model, nif, anexoCModel, keysAnexosB);
            this.validateC077(result, rostoModel, nif, anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901"));
            this.validateC078(result, nif, anexoCModel, yearDataFill);
            this.validateC079(result, anexoCModel, rostoModel, anexoCModel.getQuadro09());
            this.validateC080(result, anexoCModel, rostoModel, anexoCModel.getQuadro09());
            this.validateC081(result, anexoCModel, anexoCModel.getQuadro09());
            this.validateC082(result, anexoCModel, quadro09);
            this.validateC083(result, anexoCModel, quadro09);
            this.validateC084(result, anexoCModel, quadro09);
            this.validateC085(result, anexoCModel, quadro09);
        }
        return result;
    }

    protected void validateC077(ValidationResult result, RostoModel rostoModel, Long linhaNIF, String linkNif) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (rostoModel.getQuadro03().isSujeitoPassivoA(linhaNIF) || rostoModel.getQuadro03().isSujeitoPassivoB(linhaNIF) || rostoModel.getQuadro03().existsInDependentes(linhaNIF) || rostoModel.getQuadro07().existsInAscendentes(linhaNIF) || rostoModel.getQuadro07().existsInAfilhadosCivisEmComunhao(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(linhaNIF) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjuntaOutro(linhaNIF) || rostoModel.getQuadro07().existsInAscendentesEColaterais(linhaNIF) || rostoModel.getQuadro05().isRepresentante(linhaNIF))) {
            result.add(new DeclValidationMessage("C077", linkNif, new String[]{linkNif, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.trostoq07CT1", "aRosto.qQuadro05.fq05C5"}));
        }
    }

    protected void validateC073(ValidationResult result, Long nif, AnexoCModel anexoCModel) {
        if (!(nif == null || Modelo3IRSValidatorUtil.startsWith(nif, "1", "2"))) {
            result.add(new DeclValidationMessage("C073", anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901")}));
        }
    }

    protected void validateC074(ValidationResult result, Long nif, AnexoCModel anexoCModel) {
        if (!(nif == null || Modelo3IRSValidatorUtil.isNIFValid(nif))) {
            result.add(new DeclValidationMessage("C074", anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901")}));
        }
    }

    protected void validateC075(ValidationResult result, Long nif, AnexoCModel anexoCModel, boolean yearDataFill) {
        if (nif == null && yearDataFill) {
            result.add(new DeclValidationMessage("C075", anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901")}));
        }
    }

    protected void validateC076(ValidationResult result, DeclaracaoModel model, Long nif, AnexoCModel anexoCModel, List<FormKey> keysAnexosB) {
        for (FormKey keyAnxB : keysAnexosB) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(keyAnxB);
            Long nifAnxB = anexoBModel.getQuadro08().getAnexoBq08C801();
            if (nifAnxB == null || !Modelo3IRSValidatorUtil.equals(nif, nifAnxB)) continue;
            result.add(new DeclValidationMessage("C076", anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901"), anexoBModel.getLink("aAnexoB.qQuadro08.fanexoBq08C801")}));
            break;
        }
    }

    protected void validateC078(ValidationResult result, Long nif, AnexoCModel anexoCModel, boolean yearDataFill) {
        if (!(nif == null || yearDataFill)) {
            result.add(new DeclValidationMessage("C078", anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C902"), anexoCModel.getLink("aAnexoC.qQuadro09.fanexoCq09C901")}));
        }
    }

    protected void validateC079(ValidationResult result, AnexoCModel anexoCModel, RostoModel rostoModel, Quadro09 quadro09) {
        for (int i = 1; i <= 6; ++i) {
            if (rostoModel.getQuadro02().getQ02C02() == null || this.getAnoByPos(i, quadro09) == null || this.getAnoByPos(i, quadro09) >= rostoModel.getQuadro02().getQ02C02() - 6) continue;
            result.add(new DeclValidationMessage("C079", this.getAnoLinkByPos(i, quadro09, anexoCModel), new String[]{this.getAnoLinkByPos(i, quadro09, anexoCModel)}));
        }
    }

    protected void validateC080(ValidationResult result, AnexoCModel anexoCModel, RostoModel rostoModel, Quadro09 quadro09) {
        for (int i = 1; i <= 6; ++i) {
            if (rostoModel.getQuadro02().getQ02C02() == null || this.getAnoByPos(i, quadro09) == null || this.getAnoByPos(i, quadro09) < rostoModel.getQuadro02().getQ02C02()) continue;
            result.add(new DeclValidationMessage("C080", this.getAnoLinkByPos(i, quadro09, anexoCModel), new String[]{this.getAnoLinkByPos(i, quadro09, anexoCModel), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC081(ValidationResult result, AnexoCModel anexoCModel, Quadro09 quadro09) {
        for (int i = 1; i <= 6; ++i) {
            if (this.getAnoByPos(i, quadro09) == null) continue;
            for (int j = i - 1; j >= 1; --j) {
                if (this.getAnoByPos(j, quadro09) == null || !Modelo3IRSValidatorUtil.equals(this.getAnoByPos(j, quadro09), this.getAnoByPos(i, quadro09))) continue;
                result.add(new DeclValidationMessage("C081", this.getAnoLinkByPos(j, quadro09, anexoCModel), new String[]{this.getAnoLinkByPos(i, quadro09, anexoCModel)}));
            }
        }
    }

    protected void validateC082(ValidationResult result, AnexoCModel anexoCModel, Quadro09 quadro09) {
        for (int i = 2; i <= 6; ++i) {
            if (this.getAnoByPos(i, quadro09) == null || this.getAnoByPos(i - 1, quadro09) != null) continue;
            result.add(new DeclValidationMessage("C082", this.getAnoLinkByPos(i - 1, quadro09, anexoCModel), new String[]{this.getAnoLinkByPos(i - 1, quadro09, anexoCModel), this.getAnoLinkByPos(i, quadro09, anexoCModel)}));
        }
    }

    protected void validateC083(ValidationResult result, AnexoCModel anexoCModel, Quadro09 quadro09) {
        for (int i = 1; i <= 6; ++i) {
            if (this.getAnoByPos(i, quadro09) == null || this.getRendimentosPCIByPos(i, quadro09) != null || this.getRendimentosASPByPos(i, quadro09) != null) continue;
            result.add(new DeclValidationMessage("C083", this.getAnoLinkByPos(i, quadro09, anexoCModel), new String[]{this.getRendimentosPCILinkByPos(i, quadro09, anexoCModel), this.getRendimentosASPLinkByPos(i, quadro09, anexoCModel), this.getAnoLinkByPos(i, quadro09, anexoCModel)}));
        }
    }

    protected void validateC084(ValidationResult result, AnexoCModel anexoCModel, Quadro09 quadro09) {
        for (int i = 1; i <= 6; ++i) {
            if (this.getRendimentosPCIByPos(i, quadro09) == null || this.getAnoByPos(i, quadro09) != null) continue;
            result.add(new DeclValidationMessage("C084", this.getAnoLinkByPos(i, quadro09, anexoCModel), new String[]{this.getAnoLinkByPos(i, quadro09, anexoCModel), this.getRendimentosPCILinkByPos(i, quadro09, anexoCModel)}));
        }
    }

    protected void validateC085(ValidationResult result, AnexoCModel anexoCModel, Quadro09 quadro09) {
        for (int i = 1; i <= 6; ++i) {
            if (this.getRendimentosASPByPos(i, quadro09) == null || this.getAnoByPos(i, quadro09) != null) continue;
            result.add(new DeclValidationMessage("C085", this.getAnoLinkByPos(i, quadro09, anexoCModel), new String[]{this.getAnoLinkByPos(i, quadro09, anexoCModel), this.getRendimentosASPLinkByPos(i, quadro09, anexoCModel)}));
        }
    }
}

