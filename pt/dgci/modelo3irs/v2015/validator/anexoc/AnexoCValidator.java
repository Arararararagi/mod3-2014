/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class AnexoCValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoCValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            AnexoBModel anexoBModel = ((Modelo3IRSv2015Model)model).getAnexoBByTitular(anexoCModel.getAnexoCTitular());
            if (anexoBModel != null) {
                result.add(new DeclValidationMessage("C001", anexoCModel.getLink("aAnexoC"), new String[]{anexoBModel.getLink("aAnexoB")}));
            }
            if (Modelo3IRSv2015Parameters.instance().isFase2()) continue;
            result.add(new DeclValidationMessage("C162", anexoCModel.getLink("aAnexoC"), new String[]{anexoCModel.getLink("aAnexoC")}));
        }
        this.validateC002(result, model);
        return result;
    }

    protected void validateC002(ValidationResult result, DeclaracaoModel model) {
        ArrayList<FormKey> processedKeys = new ArrayList<FormKey>();
        HashMap<Long, String> processedNifs = new HashMap<Long, String>();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        Long nif = null;
        for (FormKey key : keys) {
            AnexoCModel otherModel;
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            if (processedKeys.contains(key)) {
                otherModel = (AnexoCModel)model.getAnexo((FormKey)processedKeys.get(processedKeys.indexOf(key)));
                if (otherModel == null) {
                    otherModel = anexoCModel;
                }
                result.add(new DeclValidationMessage("C002", anexoCModel.getLink("aAnexoC"), new String[]{otherModel.getLink("aAnexoC.qQuadro03"), anexoCModel.getLink("aAnexoC.qQuadro03")}));
            } else {
                processedKeys.add(key);
            }
            if (anexoCModel.getQuadro03().getAnexoCq03C06() == null && anexoCModel.getQuadro03().getAnexoCq03C07() == null) continue;
            Long l = nif = anexoCModel.getQuadro03().getAnexoCq03C06() != null ? anexoCModel.getQuadro03().getAnexoCq03C06() : anexoCModel.getQuadro03().getAnexoCq03C07();
            if (processedNifs.get(nif) != null) {
                otherModel = (AnexoCModel)model.getAnexo(new FormKey(AnexoCModel.class, (String)processedNifs.get(nif)));
                if (otherModel == null) {
                    otherModel = anexoCModel;
                }
                result.add(new DeclValidationMessage("C002", anexoCModel.getLink("aAnexoC"), new String[]{otherModel.getLink("aAnexoC.qQuadro03"), anexoCModel.getLink("aAnexoC.qQuadro03")}));
                continue;
            }
            processedNifs.put(nif, key.getSubId());
        }
    }
}

