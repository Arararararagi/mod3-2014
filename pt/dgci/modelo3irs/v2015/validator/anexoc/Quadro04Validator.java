/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoc;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String C166 = "C166";
    public static final String C167 = "C167";
    private static final String SILVICOLAS_02 = "2";
    protected static final String ANEXO_C = "AnexoC";

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateC166(ValidationResult result, AnexoCModel anexoC) {
        Long c462 = anexoC.getQuadro04().getAnexoCq04C462();
        Long CAE = anexoC.getQuadro03().getAnexoCq03C10();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c462) || !Modelo3IRSValidatorUtil.isEmptyOrZero(CAE) && (Modelo3IRSValidatorUtil.isEmptyOrZero(CAE) || Modelo3IRSValidatorUtil.startsWith(CAE, "2")))) {
            result.add(new DeclValidationMessage("C166", anexoC.getLink("aAnexoC.qQuadro04.fanexoCq04C462"), new String[]{anexoC.getLink("aAnexoC.qQuadro04.fanexoCq04C462"), anexoC.getLink("aAnexoC.qQuadro03.fanexoCq03C10")}));
        }
    }

    protected void validateC167(ValidationResult result, AnexoCModel anexoC) {
        Long c463 = anexoC.getQuadro04().getAnexoCq04C463();
        Long ano = anexoC.getQuadro02().getAnexoCq02C03();
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(c463) || Modelo3IRSValidatorUtil.isEmptyOrZero(ano) || ano >= 2008)) {
            result.add(new DeclValidationMessage("C167", anexoC.getLink("aAnexoC.qQuadro04.fanexoCq04C463"), new String[]{anexoC.getLink("aAnexoC.qQuadro04.fanexoCq04C463")}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : keys) {
            long ajudas1005;
            long somaCq04C408;
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04 quadro04 = anexoCModel.getQuadro04();
            Quadro10 quadro10 = anexoCModel.getQuadro10();
            Quadro13 quadro13 = anexoCModel.getQuadro13();
            Quadro03 quadro03 = anexoCModel.getQuadro03();
            RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
            AnexoHModel anexoHModel = (AnexoHModel)model.getAnexo(AnexoHModel.class);
            AnexoJModel anexoJModel = ((Modelo3IRSv2015Model)model).getAnexoJByTitular(anexoCModel.getAnexoCTitular());
            if (quadro04.getAnexoCq04C403() != null && quadro04.getAnexoCq04C403() > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
                result.add(new DeclValidationMessage("C145", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C403"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C403")}));
            }
            if (quadro04.getAnexoCq04C405() != null && quadro04.getAnexoCq04C405() > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
                result.add(new DeclValidationMessage("C146", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C405"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C405")}));
            }
            if (quadro04.getAnexoCq04C406() != null && quadro04.getAnexoCq04C406() > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
                result.add(new DeclValidationMessage("C147", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C406"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C406")}));
            }
            if (quadro04.getAnexoCq04C407() != null && quadro04.getAnexoCq04C407() > 0 && rostoModel.getQuadro02().getQ02C02() != null && rostoModel.getQuadro02().getQ02C02() < 2010) {
                result.add(new DeclValidationMessage("C148", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C407"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C407")}));
            }
            this.validateC177(result, anexoCModel);
            long l = somaCq04C408 = quadro04.getAnexoCq04C408() != null ? quadro04.getAnexoCq04C408() : 0;
            if (somaCq04C408 != quadro04.getSumC1()) {
                result.add(new DeclValidationMessage("C028", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C408"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C408")}));
            }
            if (!(quadro04.getAnexoCq04C412() == null || quadro04.getAnexoCq04C412() <= 0 || Modelo3IRSValidatorUtil.equals(quadro04.getAnexoCq04C412(), quadro10.getAnexoCq10C1001()))) {
                result.add(new DeclValidationMessage("C029", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C412"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C412"), anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1001")}));
            }
            this.validateC178(result, anexoCModel);
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C431()) && quadro13.isEmptyOrZero()) {
                result.add(new DeclValidationMessage("C030", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C431"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C431"), anexoCModel.getLink("aAnexoC.qQuadro13.fanexoCq13C1301")}));
            }
            this.validateC180(result, anexoCModel);
            this.validateC181(result, anexoCModel);
            long ajudas424 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C424()) ? quadro04.getAnexoCq04C424() : 0;
            long l2 = ajudas1005 = !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10.getAnexoCq10C1005()) ? quadro10.getAnexoCq10C1005() : 0;
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C459()) || ajudas424 <= ajudas1005)) {
                result.add(new DeclValidationMessage("C032", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C459"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C459"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C424"), anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1005")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C435()) || quadro04.getAnexoCq04C435() <= 0 || !Modelo3IRSValidatorUtil.isEmptyOrZero(quadro10.getAnexoCq10C1004()) && Modelo3IRSValidatorUtil.equals(quadro04.getAnexoCq04C435(), quadro10.getAnexoCq10C1004()))) {
                result.add(new DeclValidationMessage("C033", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C435"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C435"), anexoCModel.getLink("aAnexoC.qQuadro10.fanexoCq10C1004")}));
            }
            this.validateC179(result, anexoCModel);
            this.validateC034(result, anexoCModel);
            this.validateC182(result, anexoCModel);
            this.validateC183(result, anexoCModel);
            this.validateC184(result, anexoCModel);
            this.validateC185(result, anexoCModel);
            this.validateC186(result, anexoCModel);
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C453()) && quadro04.getAnexoCq04C453() > 0 && (rostoModel.getHandicapPercentByNif(quadro03.getNifTitular()) != -1 && rostoModel.getHandicapPercentByNif(quadro03.getNifTitular()) < 60 || rostoModel.getQuadro07().isConjugeFalecido(quadro03.getNifTitular()) && rostoModel.getQuadro07().getQ07C1a() == null || rostoModel.getQuadro07().isConjugeFalecido(quadro03.getNifTitular()) && rostoModel.getQuadro07().getQ07C1a() != null && rostoModel.getQuadro07().getQ07C1a() < 60)) {
                result.add(new DeclValidationMessage("C035", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C453"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C453"), rostoModel.getLinkByNif(quadro03.getNifTitular())}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C454()) && (anexoHModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHModel.getQuadro05().getRendimentoByNif(quadro03.getNifTitular(), rostoModel)))) {
                result.add(new DeclValidationMessage("C036", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C454"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C454")}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C454()) || anexoHModel == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoHModel.getQuadro05().getRendimentoByNif(quadro03.getNifTitular(), rostoModel)) || Modelo3IRSValidatorUtil.equals(anexoHModel.getQuadro05().getRendimentoByNif(quadro03.getNifTitular(), rostoModel), quadro04.getAnexoCq04C454()))) {
                result.add(new DeclValidationMessage("C037", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C454"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C454"), anexoHModel.getQuadro05().getLinkRendimentoByNif(quadro03.getNifTitular(), rostoModel)}));
            }
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C456()) || quadro04.getAnexoCq04C456() <= 0 || anexoJModel != null)) {
                result.add(new DeclValidationMessage("C038", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C456"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C456")}));
            }
            this.validateC175(result, anexoCModel, anexoJModel);
            if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C455()) || Modelo3IRSValidatorUtil.isEmptyOrZero(rostoModel.getQuadro02().getQ02C02()) || rostoModel.getQuadro02().getQ02C02() >= 2007)) {
                result.add(new DeclValidationMessage("C039", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C455"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C455"), "aRosto.qQuadro02.fq02C02"}));
            }
            this.validateC040(result, anexoCModel, quadro04);
            long cq04C439_SomaAcrescer = quadro04.getAnexoCq04C439() == null ? 0 : quadro04.getAnexoCq04C439();
            long cq04C458_SomaDeduzir = quadro04.getAnexoCq04C458() == null ? 0 : quadro04.getAnexoCq04C458();
            long prejuizoLucro = cq04C439_SomaAcrescer - cq04C458_SomaDeduzir;
            if (prejuizoLucro < 0) {
                if (quadro04.getAnexoCq04C459() == null || quadro04.getAnexoCq04C459() != 0 - prejuizoLucro || quadro04.getAnexoCq04C460() != null && quadro04.getAnexoCq04C460() != 0) {
                    result.add(new DeclValidationMessage("C041", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C459"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C459"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C439"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C458")}));
                }
            } else if (prejuizoLucro > 0) {
                if (quadro04.getAnexoCq04C460() == null || quadro04.getAnexoCq04C460() != prejuizoLucro || quadro04.getAnexoCq04C459() != null && quadro04.getAnexoCq04C459() != 0) {
                    result.add(new DeclValidationMessage("C042", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C460"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C460"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C439"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C458")}));
                }
            } else {
                if (quadro04.getAnexoCq04C459() != null && quadro04.getAnexoCq04C459() != 0) {
                    result.add(new DeclValidationMessage("C041", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C459"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C459"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C439"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C458")}));
                }
                if (quadro04.getAnexoCq04C460() != null && quadro04.getAnexoCq04C460() != 0) {
                    result.add(new DeclValidationMessage("C042", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C460"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C460"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C439"), anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C458")}));
                }
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C461()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C454())) {
                result.add(new DeclValidationMessage("C043", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C454"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C454")}));
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C454()) && Modelo3IRSValidatorUtil.isEmptyOrZero(quadro04.getAnexoCq04C461())) {
                result.add(new DeclValidationMessage("C044", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C461"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C461")}));
            }
            this.validateC166(result, anexoCModel);
            this.validateC167(result, anexoCModel);
        }
        return result;
    }

    protected void validateC177(ValidationResult result, AnexoCModel anexoCModel) {
        Long C464 = anexoCModel.getQuadro04().getAnexoCq04C464();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C464 != null && ano != null && C464 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C177", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C464"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C464"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC178(ValidationResult result, AnexoCModel anexoCModel) {
        Long C465 = anexoCModel.getQuadro04().getAnexoCq04C465();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C465 != null && ano != null && C465 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C178", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C465"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C465"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC179(ValidationResult result, AnexoCModel anexoCModel) {
        Long C466 = anexoCModel.getQuadro04().getAnexoCq04C466();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C466 != null && ano != null && C466 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C179", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C466"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C466"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC034(ValidationResult result, AnexoCModel anexoCModel) {
        long somaCq04C439;
        long l = somaCq04C439 = anexoCModel.getQuadro04().getAnexoCq04C439() != null ? anexoCModel.getQuadro04().getAnexoCq04C439() : 0;
        if (somaCq04C439 != anexoCModel.getQuadro04().getSumC2()) {
            result.add(new DeclValidationMessage("C034", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C439"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C439")}));
        }
    }

    protected void validateC180(ValidationResult result, AnexoCModel anexoCModel) {
        Long C467 = anexoCModel.getQuadro04().getAnexoCq04C467();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C467 != null && ano != null && C467 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C180", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C467"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C467"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC181(ValidationResult result, AnexoCModel anexoCModel) {
        Long C468 = anexoCModel.getQuadro04().getAnexoCq04C468();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C468 != null && ano != null && C468 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C181", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C468"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C468"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC182(ValidationResult result, AnexoCModel anexoCModel) {
        Long C469 = anexoCModel.getQuadro04().getAnexoCq04C469();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C469 != null && ano != null && C469 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C182", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C469"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C469"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC183(ValidationResult result, AnexoCModel anexoCModel) {
        Long C470 = anexoCModel.getQuadro04().getAnexoCq04C470();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C470 != null && ano != null && C470 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C183", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C470"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C470"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC184(ValidationResult result, AnexoCModel anexoCModel) {
        Long C471 = anexoCModel.getQuadro04().getAnexoCq04C471();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C471 != null && ano != null && C471 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C184", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C471"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C471"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC185(ValidationResult result, AnexoCModel anexoCModel) {
        Long C472 = anexoCModel.getQuadro04().getAnexoCq04C472();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C472 != null && ano != null && C472 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C185", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C472"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C472"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC186(ValidationResult result, AnexoCModel anexoCModel) {
        Long C473 = anexoCModel.getQuadro04().getAnexoCq04C473();
        Long ano = anexoCModel.getQuadro02().getAnexoCq02C03();
        if (C473 != null && ano != null && C473 > 0 && ano < 2014) {
            result.add(new DeclValidationMessage("C186", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C473"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C473"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateC175(ValidationResult result, AnexoCModel anexoCModel, AnexoJModel anexoJModel) {
        if (anexoCModel != null && anexoCModel.getQuadro04() != null && anexoJModel != null && anexoJModel.getQuadro04() != null) {
            long rendimentoC405;
            long rendimentoC403 = anexoJModel.getQuadro04().getAnexoJq04C403b() != null ? anexoJModel.getQuadro04().getAnexoJq04C403b() : 0;
            long rendimentoC404 = anexoJModel.getQuadro04().getAnexoJq04C404b() != null ? anexoJModel.getQuadro04().getAnexoJq04C404b() : 0;
            long l = rendimentoC405 = anexoJModel.getQuadro04().getAnexoJq04C405b() != null ? anexoJModel.getQuadro04().getAnexoJq04C405b() : 0;
            if (anexoCModel.getQuadro04().getAnexoCq04C456() != null && anexoCModel.getAnexoCTitular().equals(anexoJModel.getAnexoJTitular()) && anexoCModel.getQuadro04().getAnexoCq04C456() > rendimentoC403 + rendimentoC404 + rendimentoC405) {
                result.add(new DeclValidationMessage("C175", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C456"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C456"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C403b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C404b"), anexoJModel.getLink("aAnexoJ.qQuadro04.fanexoJq04C405b")}));
            }
        }
    }

    public void validateC040(ValidationResult result, AnexoCModel anexoCModel, pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04 quadro04) {
        long somaCq04C458;
        long l = somaCq04C458 = quadro04.getAnexoCq04C458() != null ? quadro04.getAnexoCq04C458() : 0;
        if (somaCq04C458 != quadro04.getSumC3()) {
            result.add(new DeclValidationMessage("C040", anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C458"), new String[]{anexoCModel.getLink("aAnexoC.qQuadro04.fanexoCq04C458")}));
        }
    }
}

