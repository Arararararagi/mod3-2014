/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro06Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro06Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        return new ValidationResult();
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        return new ValidationResult();
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        return new ValidationResult();
    }
}

