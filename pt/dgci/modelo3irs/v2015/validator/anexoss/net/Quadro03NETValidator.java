/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.Quadro03Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NISSValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro03NETValidator
extends Quadro03Validator {
    public Quadro03NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        for (FormKey key : keys) {
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            this.validateS008(result, anexoSSModel, rostoModel);
            this.validateS009(result, anexoSSModel);
            this.validateS010(result, anexoSSModel);
            this.validateS012(result, anexoSSModel);
            this.validateS014(result, anexoSSModel);
            this.validateS038(result, anexoSSModel);
        }
        return result;
    }

    protected void validateS008(ValidationResult result, AnexoSSModel anexoSSModel, RostoModel rostoModel) {
        Long nif;
        if (!(anexoSSModel == null || anexoSSModel.getQuadro03() == null || rostoModel == null || !Modelo3IRSValidatorUtil.isEmptyOrZero(nif = anexoSSModel.getQuadro03().getAnexoSSq03C06()) && (rostoModel.getQuadro03().isSujeitoPassivoA(nif) || rostoModel.getQuadro03().isSujeitoPassivoB(nif) || rostoModel.getQuadro03().existsInDependentes(nif) || rostoModel.getQuadro07().isConjugeFalecido(nif) || rostoModel.getQuadro03().existsInDependentesEmGuardaConjunta(nif)))) {
            result.add(new DeclValidationMessage("S008", anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06"), "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateS009(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro03() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro03().getAnexoSSq03C06()) || Modelo3IRSValidatorUtil.isNIFValid(anexoSSModel.getQuadro03().getAnexoSSq03C06()))) {
            result.add(new DeclValidationMessage("S009", anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06")}));
        }
    }

    protected void validateS010(ValidationResult result, AnexoSSModel anexoSSModel) {
        String[] nifStartsWithValues = new String[]{"1", "2"};
        if (!(anexoSSModel == null || anexoSSModel.getQuadro03() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro03().getAnexoSSq03C06()) || Modelo3IRSValidatorUtil.startsWith(anexoSSModel.getQuadro03().getAnexoSSq03C06(), nifStartsWithValues))) {
            result.add(new DeclValidationMessage("S010", anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06")}));
        }
    }

    protected void validateS012(ValidationResult result, AnexoSSModel anexoSSModel) {
        String[] nissStartsWithValues = new String[]{"1"};
        if (!(anexoSSModel == null || anexoSSModel.getQuadro03() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro03().getAnexoSSq03C07()) || Modelo3IRSValidatorUtil.startsWith(anexoSSModel.getQuadro03().getAnexoSSq03C07(), nissStartsWithValues) && NISSValidator.validateAll(String.valueOf(anexoSSModel.getQuadro03().getAnexoSSq03C07())))) {
            result.add(new DeclValidationMessage("S012", anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C07"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C07")}));
        }
    }

    protected void validateS014(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro03() == null || anexoSSModel.getQuadro04() == null || anexoSSModel.getQuadro05() == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro03().isEmptyAnexoSSq03C08() || anexoSSModel.getQuadro04().isEmpty() && anexoSSModel.getQuadro05().isEmpty() && anexoSSModel.getQuadro06().isEmpty())) {
            result.add(new DeclValidationMessage("S014", anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C08"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C08")}));
        }
    }

    protected void validateS038(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro03() == null || anexoSSModel.getQuadro01() == null || anexoSSModel.getQuadro03().isEmptyAnexoSSq03C08() || anexoSSModel.getQuadro01().isRegimeSelecionado())) {
            result.add(new DeclValidationMessage("S038", anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C08"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro01")}));
        }
    }
}

