/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.Quadro01Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro01NETValidator
extends Quadro01Validator {
    public Quadro01NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        boolean existsAnexoBHerancaIndivisa = this.existsAnexoBHerancaIndivisa(model);
        boolean existsAnexoCHerancaIndivisa = this.existsAnexoCHerancaIndivisa(model);
        for (FormKey key : keys) {
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            AnexoBModel anexoBModel = null;
            AnexoCModel anexoCModel = null;
            Long nifTitular = null;
            if (anexoSSModel != null && anexoSSModel.getQuadro03() != null && anexoSSModel.getQuadro03().getAnexoSSq03C06() != null) {
                nifTitular = anexoSSModel.getQuadro03().getAnexoSSq03C06();
                anexoBModel = (AnexoBModel)model.getAnexo(new FormKey(AnexoBModel.class, nifTitular.toString()));
                anexoCModel = (AnexoCModel)model.getAnexo(new FormKey(AnexoCModel.class, nifTitular.toString()));
            }
            this.validateS004(result, anexoSSModel, anexoBModel, existsAnexoBHerancaIndivisa);
            this.validateS005(result, anexoSSModel, anexoCModel, existsAnexoCHerancaIndivisa);
            this.validateS006(result, anexoSSModel);
        }
        return result;
    }

    protected void validateS004(ValidationResult result, AnexoSSModel anexoSSModel, AnexoBModel anexoBModel, boolean existsAnexoBHerancaIndivisa) {
        if (anexoSSModel != null && anexoBModel == null && !existsAnexoBHerancaIndivisa && anexoSSModel.getQuadro01() != null && anexoSSModel.getQuadro01().getAnexoSSq01B1() != null && anexoSSModel.getQuadro01().getAnexoSSq01B1().booleanValue()) {
            result.add(new DeclValidationMessage("S004", anexoSSModel.getLink("aAnexoSS.qQuadro01"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B1")}));
        }
    }

    protected boolean existsAnexoBHerancaIndivisa(DeclaracaoModel model) {
        List<FormKey> anexoBKeys = model.getAllAnexosByType("AnexoB");
        for (FormKey key : anexoBKeys) {
            AnexoBModel anexoBModel = (AnexoBModel)model.getAnexo(key);
            if (anexoBModel.getQuadro03().getAnexoBq03B1() == null || !anexoBModel.getQuadro03().getAnexoBq03B1().equals("1")) continue;
            return true;
        }
        return false;
    }

    protected boolean existsAnexoCHerancaIndivisa(DeclaracaoModel model) {
        List<FormKey> anexoBKeys = model.getAllAnexosByType("AnexoC");
        for (FormKey key : anexoBKeys) {
            AnexoCModel anexoCModel = (AnexoCModel)model.getAnexo(key);
            if (anexoCModel.getQuadro03().getAnexoCq03B1() == null || !anexoCModel.getQuadro03().getAnexoCq03B1().equals("1")) continue;
            return true;
        }
        return false;
    }

    protected void validateS005(ValidationResult result, AnexoSSModel anexoSSModel, AnexoCModel anexoCModel, boolean existsAnexoCHerancaIndivisa) {
        if (anexoSSModel != null && anexoCModel == null && !existsAnexoCHerancaIndivisa && anexoSSModel.getQuadro01() != null && anexoSSModel.getQuadro01().getAnexoSSq01B2() != null && anexoSSModel.getQuadro01().getAnexoSSq01B2().booleanValue()) {
            result.add(new DeclValidationMessage("S005", anexoSSModel.getLink("aAnexoSS.qQuadro01"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B2")}));
        }
    }

    protected void validateS006(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (anexoSSModel != null && anexoSSModel.getQuadro01().getAnexoSSq01B1() != null && anexoSSModel.getQuadro01().getAnexoSSq01B2() != null && anexoSSModel.getQuadro01().getAnexoSSq01B1().booleanValue() && anexoSSModel.getQuadro01().getAnexoSSq01B2().booleanValue()) {
            result.add(new DeclValidationMessage("S006", anexoSSModel.getLink("aAnexoSS.qQuadro01"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B1"), anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B2")}));
        }
    }
}

