/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.HashMap;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.AnexoSSValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.net.Quadro01NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.net.Quadro04NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.net.Quadro05NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.net.Quadro06NETValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class AnexoSSNETValidator
extends AnexoSSValidator {
    public AnexoSSNETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        for (FormKey key : keys) {
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            this.validateS001(result, anexoSSModel);
            this.validateS040(result, anexoSSModel);
        }
        this.validateS002(result, model);
        result.addAllFrom(new Quadro01NETValidator().validate(model));
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        result.addAllFrom(new Quadro05NETValidator().validate(model));
        result.addAllFrom(new Quadro06NETValidator().validate(model));
        return result;
    }

    protected void validateS001(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (anexoSSModel != null && anexoSSModel.getQuadro03() != null && anexoSSModel.getQuadro04() != null && anexoSSModel.getQuadro05() != null && anexoSSModel.getQuadro06() != null && anexoSSModel.getQuadro03().isEmpty() && anexoSSModel.getQuadro04().isEmpty() && anexoSSModel.getQuadro05().isEmpty() && anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty()) {
            result.add(new DeclValidationMessage("S001", anexoSSModel.getLink("aAnexoSS"), new String[]{anexoSSModel.getLink("aAnexoSS")}));
        }
    }

    protected void validateS040(ValidationResult result, AnexoSSModel anexoSSModel) {
        boolean jaExisteErro = false;
        if (anexoSSModel != null && (anexoSSModel.getQuadro04().getAnexoSSq04C401() != null && anexoSSModel.getQuadro04().getAnexoSSq04C401() < 0 || anexoSSModel.getQuadro04().getAnexoSSq04C402() != null && anexoSSModel.getQuadro04().getAnexoSSq04C402() < 0 || anexoSSModel.getQuadro04().getAnexoSSq04C403() != null && anexoSSModel.getQuadro04().getAnexoSSq04C403() < 0 || anexoSSModel.getQuadro04().getAnexoSSq04C404() != null && anexoSSModel.getQuadro04().getAnexoSSq04C404() < 0 || anexoSSModel.getQuadro04().getAnexoSSq04C405() != null && anexoSSModel.getQuadro04().getAnexoSSq04C405() < 0 || anexoSSModel.getQuadro04().getAnexoSSq04C406() != null && anexoSSModel.getQuadro04().getAnexoSSq04C406() < 0 || anexoSSModel.getQuadro05().getAnexoSSq05C501() != null && anexoSSModel.getQuadro05().getAnexoSSq05C501() < 0 || anexoSSModel.getQuadro05().getAnexoSSq05C502() != null && anexoSSModel.getQuadro05().getAnexoSSq05C502() < 0)) {
            jaExisteErro = true;
            result.add(new DeclValidationMessage("S040", anexoSSModel.getLink("aAnexoSS"), new String[0]));
        }
        if (!(anexoSSModel == null || jaExisteErro)) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                AnexoSSq06T1_Linha linha = anexoSSq06T1.get(i);
                if (linha == null || linha.getValor() == null || linha.getValor() >= 0) continue;
                result.add(new DeclValidationMessage("S040", anexoSSModel.getLink("aAnexoSS"), new String[0]));
            }
        }
    }

    protected void validateS002(ValidationResult result, DeclaracaoModel model) {
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        HashMap<Long, String> processedNifs = new HashMap<Long, String>();
        for (FormKey key : keys) {
            AnexoSSModel otherModel;
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            if (anexoSSModel == null || key == null || keys == null) continue;
            int count = 0;
            for (FormKey formKey : keys) {
                if (formKey == null || !formKey.equals(key)) continue;
                ++count;
            }
            if (count > 1) {
                otherModel = (AnexoSSModel)model.getAnexo(new FormKey(AnexoSSModel.class, key.getSubId()));
                if (otherModel == null) {
                    otherModel = anexoSSModel;
                }
                result.add(new DeclValidationMessage("S002", anexoSSModel.getLink("aAnexoSS"), new String[]{otherModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06"), anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06")}));
            }
            if (anexoSSModel.getQuadro03().getAnexoSSq03C06() == null) continue;
            if (processedNifs.get(anexoSSModel.getQuadro03().getAnexoSSq03C06()) != null) {
                otherModel = (AnexoSSModel)model.getAnexo(new FormKey(AnexoSSModel.class, (String)processedNifs.get(anexoSSModel.getQuadro03().getAnexoSSq03C06())));
                if (otherModel == null) {
                    otherModel = anexoSSModel;
                }
                result.add(new DeclValidationMessage("S002", anexoSSModel.getLink("aAnexoSS"), new String[]{otherModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06"), anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06")}));
                continue;
            }
            processedNifs.put(anexoSSModel.getQuadro03().getAnexoSSq03C06(), key.getSubId());
        }
    }
}

