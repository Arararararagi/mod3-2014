/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.Quadro02Validator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro02NETValidator
extends Quadro02Validator {
    public Quadro02NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        for (FormKey key : keys) {
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            this.validateS007(result, anexoSSModel, rostoModel);
            this.validateS039(result, anexoSSModel);
        }
        return result;
    }

    protected void validateS007(ValidationResult result, AnexoSSModel anexoSSModel, RostoModel rostoModel) {
        if (!(anexoSSModel == null || rostoModel == null || anexoSSModel.getQuadro02() == null || rostoModel.getQuadro02() == null || anexoSSModel.getQuadro02().getAnexoSSq02C04() != null && anexoSSModel.getQuadro02().getAnexoSSq02C04().equals(rostoModel.getQuadro02().getQ02C02()))) {
            result.add(new DeclValidationMessage("S007", anexoSSModel.getLink("aAnexoSS.qQuadro02"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro02.fanexoSSq02C04"), "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateS039(ValidationResult result, AnexoSSModel anexoSSModel) {
        long anoMinimoAnexoSS = 2012;
        if (anexoSSModel != null && anexoSSModel.getQuadro02() != null && anexoSSModel.getQuadro02().getAnexoSSq02C04() != null && anexoSSModel.getQuadro02().getAnexoSSq02C04() < anoMinimoAnexoSS) {
            result.add(new DeclValidationMessage("S039", anexoSSModel.getLink("aAnexoSS.qQuadro02"), new String[]{"aRosto.qQuadro02.fq02C02"}));
        }
    }
}

