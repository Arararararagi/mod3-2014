/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.Quadro04Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro04NETValidator
extends Quadro04Validator {
    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        for (FormKey key : keys) {
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            this.validateYS001(result, anexoSSModel);
            this.validateS015(result, anexoSSModel);
        }
        return result;
    }

    protected void validateYS001(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro04() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro04().getAnexoSSq04C1()))) {
            long somaParcelas = 0;
            if (anexoSSModel.getQuadro04().getAnexoSSq04C1() != (somaParcelas+=(anexoSSModel.getQuadro04().getAnexoSSq04C401() == null ? 0 : anexoSSModel.getQuadro04().getAnexoSSq04C401()) + (anexoSSModel.getQuadro04().getAnexoSSq04C402() == null ? 0 : anexoSSModel.getQuadro04().getAnexoSSq04C402()) + (anexoSSModel.getQuadro04().getAnexoSSq04C403() == null ? 0 : anexoSSModel.getQuadro04().getAnexoSSq04C403()) + (anexoSSModel.getQuadro04().getAnexoSSq04C404() == null ? 0 : anexoSSModel.getQuadro04().getAnexoSSq04C404()) + (anexoSSModel.getQuadro04().getAnexoSSq04C405() == null ? 0 : anexoSSModel.getQuadro04().getAnexoSSq04C405()) + (anexoSSModel.getQuadro04().getAnexoSSq04C406() == null ? 0 : anexoSSModel.getQuadro04().getAnexoSSq04C406()) + (anexoSSModel.getQuadro04().getAnexoSSq04C407() == null ? 0 : anexoSSModel.getQuadro04().getAnexoSSq04C407()))) {
                result.add(new DeclValidationMessage("YS001", anexoSSModel.getLink("aAnexoSS.qQuadro04.fanexoSSq04C1"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro04.fanexoSSq04C1")}));
            }
        }
    }

    protected void validateS015(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro01() == null || anexoSSModel.getQuadro04() == null || anexoSSModel.getQuadro05() == null || anexoSSModel.getQuadro04().isEmpty() || anexoSSModel.getQuadro05().getAnexoSSq05C501() != null || anexoSSModel.getQuadro01().isRegimeSelecionado())) {
            result.add(new DeclValidationMessage("S015", anexoSSModel.getLink("aAnexoSS.qQuadro04"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro01")}));
        }
    }
}

