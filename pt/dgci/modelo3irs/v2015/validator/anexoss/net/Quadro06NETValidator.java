/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_AnxSS;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.Quadro06Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public class Quadro06NETValidator
extends Quadro06Validator {
    public Quadro06NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        for (FormKey key : keys) {
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            this.validateS033(result, anexoSSModel);
            this.validateS032(result, anexoSSModel);
            this.validateS036(result, anexoSSModel);
            this.validateS020(result, anexoSSModel);
            this.validateS021(result, anexoSSModel);
            this.validateS025(result, anexoSSModel);
            this.validateS028(result, anexoSSModel);
            this.validateS022(result, anexoSSModel);
            this.validateS029(result, anexoSSModel);
            this.validateS023(result, anexoSSModel);
            this.validateS024(result, anexoSSModel);
            this.validateS026(result, anexoSSModel);
            this.validateS027(result, anexoSSModel);
            this.validateS030(result, anexoSSModel);
            this.validateS031(result, anexoSSModel);
            this.validateS034(result, anexoSSModel);
            this.validateS035(result, anexoSSModel);
            this.validateS037(result, anexoSSModel);
        }
        return result;
    }

    protected void validateS033(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (anexoSSModel != null && anexoSSModel.getQuadro06() != null && "N".equals(anexoSSModel.getQuadro06().getAnexoSSq06B1()) && !anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty()) {
            boolean hasLinhasPreenchidas = false;
            if (anexoSSModel.getQuadro06().getAnexoSSq06T1() != null) {
                for (AnexoSSq06T1_Linha linha : anexoSSModel.getQuadro06().getAnexoSSq06T1()) {
                    if (linha.isEmpty()) continue;
                    hasLinhasPreenchidas = true;
                    break;
                }
            }
            if (hasLinhasPreenchidas) {
                result.add(new DeclValidationMessage("S033", anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Nao"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Nao")}));
            }
        }
    }

    protected void validateS032(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (anexoSSModel != null && anexoSSModel.getQuadro06() != null && "S".equals(anexoSSModel.getQuadro06().getAnexoSSq06B1()) && anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty()) {
            result.add(new DeclValidationMessage("S032", anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Sim"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Sim"), anexoSSModel.getLink("aAnexoSS.qQuadro06.tanexoSSq06T1")}));
        }
    }

    protected void validateS036(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (anexoSSModel != null && anexoSSModel.getQuadro06() != null && anexoSSModel.getQuadro06().isCampoC1C2Empty() && !anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty()) {
            result.add(new DeclValidationMessage("S036", anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Sim"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro06.tanexoSSq06T1"), anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Sim"), anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Nao")}));
        }
    }

    protected void validateS020(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getNifPortugues()) || Modelo3IRSValidatorUtil.isNIFValid(anexoSSq06T1.get(i).getNifPortugues())) continue;
                result.add(new DeclValidationMessage("S020", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NIFPORTUGUES))}));
            }
        }
    }

    protected void validateS021(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro03() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            Long nifTitularAnexo = anexoSSModel.getQuadro03().getAnexoSSq03C06();
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getNifPortugues()) || !anexoSSq06T1.get(i).getNifPortugues().equals(nifTitularAnexo)) continue;
                result.add(new DeclValidationMessage("S021", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NIFPORTUGUES)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NIFPORTUGUES)), anexoSSModel.getLink("aAnexoSS.qQuadro03.fanexoSSq03C06")}));
            }
        }
    }

    protected void validateS025(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getNifPortugues()) || !Modelo3IRSValidatorUtil.isNIFValid(anexoSSq06T1.get(i).getNifPortugues()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getValor())) continue;
                result.add(new DeclValidationMessage("S025", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NIFPORTUGUES)), anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.VALOR))}));
            }
        }
    }

    protected void validateS028(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getNifPortugues()) || !Modelo3IRSValidatorUtil.isNIFValid(anexoSSq06T1.get(i).getNifPortugues()) || StringUtil.isEmpty(anexoSSq06T1.get(i).getNFiscalEstrangeiro()) || Modelo3IRSValidatorUtil.isZeroString(anexoSSq06T1.get(i).getNFiscalEstrangeiro())) continue;
                result.add(new DeclValidationMessage("S028", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NIFPORTUGUES)), anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NFISCALESTRANGEIRO))}));
            }
        }
    }

    protected void validateS022(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            ListMap paises = CatalogManager.getInstance().getCatalog(Cat_M3V2015_Pais_AnxSS.class.getSimpleName());
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getPais()) || paises.get(anexoSSq06T1.get(i).getPais()) != null) continue;
                result.add(new DeclValidationMessage("S022", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.PAIS))}));
            }
        }
    }

    protected void validateS029(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || (Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getPais()) || !StringUtil.isEmpty(anexoSSq06T1.get(i).getNFiscalEstrangeiro())) && (!Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getPais()) || StringUtil.isEmpty(anexoSSq06T1.get(i).getNFiscalEstrangeiro()))) continue;
                result.add(new DeclValidationMessage("S029", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.PAIS)), anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NFISCALESTRANGEIRO))}));
            }
        }
    }

    protected void validateS023(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getPais()) || anexoSSq06T1.get(i).getNFiscalEstrangeiro() != null && !StringUtil.isEmpty(anexoSSq06T1.get(i).getNFiscalEstrangeiro()) && !Modelo3IRSValidatorUtil.isZeroString(anexoSSq06T1.get(i).getNFiscalEstrangeiro())) continue;
                result.add(new DeclValidationMessage("S023", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NFISCALESTRANGEIRO))}));
            }
        }
    }

    protected void validateS024(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            String startsWithSpace = " ";
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getPais()) || anexoSSq06T1.get(i).getNFiscalEstrangeiro() == null || !anexoSSq06T1.get(i).getNFiscalEstrangeiro().startsWith(startsWithSpace)) continue;
                result.add(new DeclValidationMessage("S024", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NFISCALESTRANGEIRO))}));
            }
        }
    }

    protected void validateS026(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || StringUtil.isEmpty(anexoSSq06T1.get(i).getNFiscalEstrangeiro()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getValor())) continue;
                result.add(new DeclValidationMessage("S026", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NFISCALESTRANGEIRO)), anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.VALOR))}));
            }
        }
    }

    protected void validateS027(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || !Modelo3IRSValidatorUtil.isGreaterThanZero(anexoSSq06T1.get(i).getValor()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getNifPortugues()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSq06T1.get(i).getPais()) || !StringUtil.isEmpty(anexoSSq06T1.get(i).getNFiscalEstrangeiro())) continue;
                result.add(new DeclValidationMessage("S027", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.VALOR)), anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NIFPORTUGUES)), anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1, AnexoSSq06T1_LinhaBase.Property.NFISCALESTRANGEIRO))}));
            }
        }
    }

    protected void validateS030(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || !anexoSSq06T1.get(i).isEmpty()) continue;
                result.add(new DeclValidationMessage("S030", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1))}));
            }
        }
    }

    protected void validateS031(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || anexoSSq06T1.get(i).isEmpty() || !anexoSSModel.getQuadro06().isLinhaRepetida(anexoSSq06T1.get(i), i)) continue;
                result.add(new DeclValidationMessage("S031", anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1)), new String[]{anexoSSModel.getLink(AnexoSSq06T1_Linha.getLink(i + 1))}));
            }
        }
    }

    protected void validateS034(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (anexoSSModel != null && (anexoSSModel.getQuadro01().isRegimeSimplificadoSelecionado() || anexoSSModel.getQuadro01().isRegimeContabilidadeOrganizadaSelecionado()) && anexoSSModel.getQuadro06().isCampoC1C2Empty() && anexoSSModel.getQuadro03().isEmptyAnexoSSq03C08()) {
            result.add(new DeclValidationMessage("S034", anexoSSModel.getLink("aAnexoSS.qQuadro06"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Sim"), anexoSSModel.getLink("aAnexoSS.qQuadro06.fanexoSSq06B1Nao")}));
        }
    }

    protected void validateS035(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro01() == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro01().isRegimeSimplificadoSelecionado() || anexoSSModel.getQuadro01().isRegimeContabilidadeOrganizadaSelecionado() || anexoSSModel.getQuadro06().isCampoC1C2Empty())) {
            result.add(new DeclValidationMessage("S035", anexoSSModel.getLink("aAnexoSS.qQuadro06"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro06"), anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B1"), anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B2")}));
        }
    }

    protected void validateS037(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro04() == null || anexoSSModel.getQuadro06() == null || anexoSSModel.getQuadro06().isTabelaIdentificacaoAdquirenteEmpty())) {
            Long somatorioValor = 0;
            EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = anexoSSModel.getQuadro06().getAnexoSSq06T1();
            for (int i = 0; i < anexoSSq06T1.size(); ++i) {
                if (anexoSSq06T1.get(i) == null || anexoSSq06T1.get(i).getValor() == null) continue;
                somatorioValor = somatorioValor + anexoSSq06T1.get(i).getValor();
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro04().getAnexoSSq04C406()) && Modelo3IRSValidatorUtil.isGreaterThanZero(somatorioValor) || !Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro04().getAnexoSSq04C406()) && somatorioValor > anexoSSModel.getQuadro04().getAnexoSSq04C406()) {
                result.add(new DeclValidationMessage("S037", anexoSSModel.getLink("aAnexoSS.qQuadro06.tanexoSSq06T1"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro06.tanexoSSq06T1"), anexoSSModel.getLink("aAnexoSS.qQuadro04.fanexoSSq04C406")}));
            }
        }
    }
}

