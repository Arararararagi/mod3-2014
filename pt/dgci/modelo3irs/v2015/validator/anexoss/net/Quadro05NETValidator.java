/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoss.net;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoss.Quadro05Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;

public class Quadro05NETValidator
extends Quadro05Validator {
    public Quadro05NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    public ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        List<FormKey> keys = model.getAllAnexosByType("AnexoSS");
        for (FormKey key : keys) {
            AnexoSSModel anexoSSModel = (AnexoSSModel)model.getAnexo(key);
            this.validateS017(result, anexoSSModel);
            this.validateS018(result, anexoSSModel);
            this.validateYS002(result, anexoSSModel);
        }
        return result;
    }

    protected void validateS017(ValidationResult result, AnexoSSModel anexoSSModel) {
        long minValue = 0;
        if (!(anexoSSModel == null || anexoSSModel.getQuadro01() == null || anexoSSModel.getQuadro05() == null || anexoSSModel.getQuadro05().getAnexoSSq05C501() == null || anexoSSModel.getQuadro05().getAnexoSSq05C501() < minValue || anexoSSModel.getQuadro01().isRegimeContabilidadeOrganizadaSelecionado())) {
            result.add(new DeclValidationMessage("S017", anexoSSModel.getLink("aAnexoSS.qQuadro05.fanexoSSq05C501"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro05.fanexoSSq05C501"), anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B2")}));
        }
    }

    protected void validateS018(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (anexoSSModel != null && anexoSSModel.getQuadro01() != null && anexoSSModel.getQuadro05() != null && (anexoSSModel.getQuadro05().getAnexoSSq05C502() != null && Modelo3IRSValidatorUtil.isGreaterThanZero((long)anexoSSModel.getQuadro05().getAnexoSSq05C502()) && !anexoSSModel.getQuadro01().isImputacaoRendimentosSelecionado() || anexoSSModel.getQuadro01().isImputacaoRendimentosSelecionado() && Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro05().getAnexoSSq05C502()))) {
            result.add(new DeclValidationMessage("S018", anexoSSModel.getLink("aAnexoSS.qQuadro05.fanexoSSq05C502"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro05.fanexoSSq05C502"), anexoSSModel.getLink("aAnexoSS.qQuadro01.fanexoSSq01B3")}));
        }
    }

    protected void validateYS002(ValidationResult result, AnexoSSModel anexoSSModel) {
        if (!(anexoSSModel == null || anexoSSModel.getQuadro05() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(anexoSSModel.getQuadro05().getAnexoSSq05C1()))) {
            long somaParcelas = 0;
            if (anexoSSModel.getQuadro05().getAnexoSSq05C1() != (somaParcelas+=(anexoSSModel.getQuadro05().getAnexoSSq05C501() == null ? 0 : anexoSSModel.getQuadro05().getAnexoSSq05C501()) + (anexoSSModel.getQuadro05().getAnexoSSq05C502() == null ? 0 : anexoSSModel.getQuadro05().getAnexoSSq05C502()))) {
                result.add(new DeclValidationMessage("YS002", anexoSSModel.getLink("aAnexoSS.qQuadro05.fanexoSSq05C1"), new String[]{anexoSSModel.getLink("aAnexoSS.qQuadro05.fanexoSSq05C1")}));
            }
        }
    }
}

