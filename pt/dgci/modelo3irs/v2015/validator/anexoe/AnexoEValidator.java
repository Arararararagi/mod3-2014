/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoe;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class AnexoEValidator
extends AmbitosValidator<DeclaracaoModel> {
    public AnexoEValidator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoEModel anexoEModel = (AnexoEModel)model.getAnexo(AnexoEModel.class);
        if (anexoEModel.isEmpty()) {
            result.add(new DeclValidationMessage("E001", "aAnexoE", new String[]{"aAnexoE"}));
        }
        if (!Modelo3IRSv2015Parameters.instance().isFase2()) {
            result.add(new DeclValidationMessage("E058", "aAnexoE", new String[]{"aAnexoE"}));
        }
        return result;
    }
}

