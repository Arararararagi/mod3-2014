/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoe;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class Quadro02Validator
extends AmbitosValidator<DeclaracaoModel> {
    public static final String E059 = "E059";

    public Quadro02Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateE059(ValidationResult result, RostoModel rosto, Quadro02 quadro02) {
        if (!Modelo3IRSValidatorUtil.equals(quadro02.getAnexoEq02C01(), rosto.getQuadro02().getQ02C02())) {
            result.add(new DeclValidationMessage("E059", "aAnexoE.qQuadro02.fanexoEq02C01", new String[]{"aAnexoE.qQuadro02.fanexoEq02C01", "aRosto.qQuadro02.fq02C02"}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        AnexoEModel anexoEModel = (AnexoEModel)model.getAnexo(AnexoEModel.class);
        Quadro02 quadro02AnexoEModel = (Quadro02)anexoEModel.getQuadro(Quadro02.class.getSimpleName());
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        this.validateE059(result, rostoModel, quadro02AnexoEModel);
        return result;
    }
}

