/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoe;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class Quadro03Validator
extends AmbitosValidator<DeclaracaoModel> {
    public Quadro03Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoEModel anexoEModel = (AnexoEModel)model.getAnexo(AnexoEModel.class);
        Quadro03 q03Model = anexoEModel.getQuadro03();
        if (!Modelo3IRSValidatorUtil.equals(rostoModel.getQuadro03().getQ03C03(), q03Model.getAnexoEq03C02())) {
            result.add(new DeclValidationMessage("E002", "aAnexoE.qQuadro03.fanexoEq03C02", new String[]{"aAnexoE.qQuadro03.fanexoEq03C02", "aRosto.qQuadro03.fq03C03"}));
        }
        if (!Modelo3IRSValidatorUtil.equals(rostoModel.getQuadro03().getQ03C04(), q03Model.getAnexoEq03C03())) {
            result.add(new DeclValidationMessage("E003", "aAnexoE.qQuadro03.fanexoEq03C03", new String[]{"aAnexoE.qQuadro03.fanexoEq03C03", "aRosto.qQuadro03.fq03C04"}));
        }
        return result;
    }
}

