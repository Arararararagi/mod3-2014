/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoe.net;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.HashMap;
import java.util.Map;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoe.Quadro04Validator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class Quadro04NETValidator
extends Quadro04Validator {
    boolean hasE038Error = false;
    String linkToE038 = null;

    public Quadro04NETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoEModel anexoEModel = (AnexoEModel)model.getAnexo(AnexoEModel.class);
        if (anexoEModel != null) {
            this.validateStep2Q04A(result, anexoEModel);
            this.validateStep2Q04B(result, anexoEModel, rostoModel);
            this.validateE038(result, anexoEModel);
        }
        return result;
    }

    protected void validateE038(ValidationResult result, AnexoEModel anexoEModel) {
        Long ano = anexoEModel.getQuadro02().getAnexoEq02C01();
        if (this.hasE038Error && ano != null && ano < 2014) {
            String linkEnglobamento = "aAnexoE.qQuadro04.fanexoEq04B1OP1";
            result.add(new DeclValidationMessage("E038", linkEnglobamento, new String[]{this.linkToE038, linkEnglobamento}));
        }
    }

    private void validateStep2Q04A(ValidationResult result, AnexoEModel anexoEModel) {
        HashMap<String, String> linhasUnicas = new HashMap<String, String>();
        Quadro04 anexoEQ04Model = anexoEModel.getQuadro04();
        int nLinha = 0;
        for (AnexoEq04T1_Linha linha : anexoEQ04Model.getAnexoEq04T1()) {
            this.validateStep2Q04ALinha(result, anexoEModel, linha, ++nLinha, linhasUnicas);
        }
    }

    private void validateStep2Q04B(ValidationResult result, AnexoEModel anexoEModel, RostoModel rostoModel) {
        HashMap<String, String> linhasUnicas = new HashMap<String, String>();
        Quadro04 anexoEQ04Model = anexoEModel.getQuadro04();
        int nLinha = 0;
        for (AnexoEq04T2_Linha linha : anexoEQ04Model.getAnexoEq04T2()) {
            this.validateStep2Q04BLinha(result, anexoEModel, linha, ++nLinha, linhasUnicas, rostoModel);
        }
    }

    private void validateStep2Q04BLinha(ValidationResult result, AnexoEModel anexoEModel, AnexoEq04T2_Linha linha, int nLinha, Map<String, String> linhasUnicas, RostoModel rostoModel) {
        Long linhaNIF = linha.getNIF();
        String linhaCodRend = linha.getCodRendimentos();
        String linhaTitular = linha.getTitular();
        Long linhaRend = linha.getRendimentos();
        String linkLine = AnexoEq04T2_Linha.getLink(nLinha);
        String linkNIF = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.NIF.getIndex();
        String linkCodRend = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkTitular = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.TITULAR.getIndex();
        String linkMontanteRendimento = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.RENDIMENTOS.getIndex();
        Quadro04 anexoEQ04 = anexoEModel.getQuadro04();
        this.validateStep2Q04BLinhaNifEntidadeDevedora(result, anexoEModel, linhaNIF, linhaRend, linhaTitular, linhaCodRend, linkLine, linkNIF, linkCodRend, linkTitular, linkMontanteRendimento);
        if (!this.hasE038Error) {
            boolean hasNif1 = linhaNIF != null;
            this.validateStep1Q04BOptouEnglobamento(result, anexoEQ04, hasNif1, linkNIF);
        }
    }

    private void validateStep1Q04BOptouEnglobamento(ValidationResult result, Quadro04 anexoEQ04, boolean hasNif1, String linkNIF) {
        String optouEnglobamento = anexoEQ04.getAnexoEq04B1();
        EventList<AnexoEq04T2_Linha> anexoEQ04B = anexoEQ04.getAnexoEq04T2();
        if (!("1".equals(optouEnglobamento) || "2".equals(optouEnglobamento) || anexoEQ04B.isEmpty() || !hasNif1)) {
            this.hasE038Error = true;
            this.linkToE038 = linkNIF;
        }
    }

    private void validateStep2Q04BLinhaNifEntidadeDevedora(ValidationResult result, AnexoEModel anexoEModel, Long linhaNIF, Long linhaRend, String linhaTitular, String linhaCodRend, String linkLine, String linkNIF, String linkCodRend, String linkTitular, String linkMontanteRendimento) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (linhaCodRend == null || linhaTitular == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linhaRend))) {
            result.add(new DeclValidationMessage("E041", linkLine, new String[]{linkNIF, linkCodRend, linkTitular, linkMontanteRendimento}));
        }
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (linhaCodRend != null || linhaTitular != null || Modelo3IRSValidatorUtil.isGreaterThanZero(linhaRend))) {
            result.add(new DeclValidationMessage("E042", linkLine, new String[]{linkNIF}));
        }
    }

    private void validateStep2Q04ALinha(ValidationResult result, AnexoEModel anexoEModel, AnexoEq04T1_Linha linha, int nLinha, Map<String, String> linhasUnicas) {
        Long linhaNIF = linha.getNIF();
        String linhaCodRend = linha.getCodRendimentos();
        String linhaTitular = linha.getTitular();
        Long linhaRend = linha.getRendimentos();
        Long linhaReten = linha.getRetencoes();
        String linkLine = AnexoEq04T1_Linha.getLink(nLinha);
        String linkNIF = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.NIF.getIndex();
        String linkCodRend = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkTitular = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.TITULAR.getIndex();
        String linkMontanteRendimento = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.RENDIMENTOS.getIndex();
        String linkReten = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.RETENCOES.getIndex();
        this.validateStep2Q04ALinhaNifEntidadeDevedora(result, anexoEModel, linhaNIF, linhaRend, linhaCodRend, linhaTitular, linkLine, linkNIF, linkCodRend, linkTitular, linkMontanteRendimento);
        this.validateStep2Q04ALinhaRetencoes(result, linhaNIF, linhaCodRend, linhaRend, linhaReten, linkReten, linkNIF);
    }

    private void validateStep2Q04ALinhaNifEntidadeDevedora(ValidationResult result, AnexoEModel anexoEModel, Long linhaNIF, Long linhaRend, String linhaCodRend, String linhaTitular, String linkLine, String linkNIF, String linkCodRend, String linkTitular, String linkMontanteRendimento) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (linhaCodRend == null || linhaTitular == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linhaRend))) {
            result.add(new DeclValidationMessage("E010", linkLine, new String[]{linkNIF, linkCodRend, linkTitular, linkMontanteRendimento}));
        }
        this.validateE011(result, linhaNIF, linhaRend, linhaCodRend, linhaTitular, linkLine, linkNIF);
    }

    private void validateStep2Q04ALinhaRetencoes(ValidationResult result, Long linhaNIF, String linhaCodRend, Long linhaRend, Long linhaReten, String linkReten, String linkNIF) {
        if (Modelo3IRSValidatorUtil.isGreaterThanZero(linhaReten) && Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && !"E5".equals(linhaCodRend)) {
            result.add(new DeclValidationMessage("E016", linkReten, new String[]{linkNIF}));
        }
    }
}

