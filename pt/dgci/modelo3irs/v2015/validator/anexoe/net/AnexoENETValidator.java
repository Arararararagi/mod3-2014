/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoe.net;

import com.jgoodies.validation.ValidationResult;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoe.AnexoEValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoe.net.Quadro02NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoe.net.Quadro03NETValidator;
import pt.dgci.modelo3irs.v2015.validator.anexoe.net.Quadro04NETValidator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AnexoENETValidator
extends AnexoEValidator {
    public AnexoENETValidator() {
        super(AmbitosValidator.AmbitoIRS.NET);
    }

    @Override
    protected ValidationResult validateSpecific(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        result.addAllFrom(new Quadro02NETValidator().validate(model));
        result.addAllFrom(new Quadro03NETValidator().validate(model));
        result.addAllFrom(new Quadro04NETValidator().validate(model));
        return result;
    }
}

