/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator.anexoe;

import ca.odell.glazedlists.EventList;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.HashMap;
import java.util.Map;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.validator.AmbitosValidator;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public abstract class Quadro04Validator
extends AmbitosValidator<DeclaracaoModel> {
    private static final int PADDING_INDICE_LINHA = 1;

    public Quadro04Validator(AmbitosValidator.AmbitoIRS ambito) {
        super(ambito);
    }

    @Override
    protected ValidationResult validateCommons(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    private void validateStep1Q04A(ValidationResult result, RostoModel rostoModel, AnexoEModel anexoEModel) {
        long anexoEq04C1Value;
        long anexoEq04C2Value;
        Quadro04 anexoEQ04Model = anexoEModel.getQuadro04();
        Quadro02 rostoQ02Model = rostoModel.getQuadro02();
        Quadro03 rostoQ03Model = rostoModel.getQuadro03();
        Quadro07 rostoQ07Model = rostoModel.getQuadro07();
        String linkNifARosto = "aRosto.qQuadro03.fq03C03";
        String linkNifBRosto = "aRosto.qQuadro03.fq03C04";
        String linkDependentesRosto = "aRosto.qQuadro03.fq03B03";
        String linkConjugeFalecidoRosto = "aRosto.qQuadro07.fq07C1";
        String linkAscendentesRosto = "aRosto.qQuadro07.fq07C01";
        HashMap<String, String> linhasUnicas = new HashMap<String, String>();
        if (anexoEQ04Model.getAnexoEq04T1().size() > 49) {
            result.add(new DeclValidationMessage("E004", "aAnexoE.qQuadro04.tanexoEq04T1", new String[]{"aAnexoE.qQuadro04.tanexoEq04T1"}));
        }
        int nLinha = 0;
        long somaControloRendimentos = 0;
        long somaControloRetencoes = 0;
        for (AnexoEq04T1_Linha linha : anexoEQ04Model.getAnexoEq04T1()) {
            this.validateStep1Q04ALinha(result, rostoQ03Model, rostoQ02Model, rostoQ07Model, linha, ++nLinha, linkNifARosto, linkNifBRosto, linkDependentesRosto, linkConjugeFalecidoRosto, linkAscendentesRosto, linhasUnicas);
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentos())) {
                somaControloRendimentos+=linha.getRendimentos().longValue();
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes())) continue;
            somaControloRetencoes+=linha.getRetencoes().longValue();
        }
        long l = anexoEq04C1Value = anexoEQ04Model.getAnexoEq04C1() != null ? anexoEQ04Model.getAnexoEq04C1() : 0;
        if (anexoEq04C1Value != somaControloRendimentos) {
            result.add(new DeclValidationMessage("YE001", "aAnexoE.qQuadro04", new String[]{"aAnexoE.qQuadro04.fanexoEq04C1"}));
        }
        long l2 = anexoEq04C2Value = anexoEQ04Model.getAnexoEq04C2() != null ? anexoEQ04Model.getAnexoEq04C2() : 0;
        if (anexoEq04C2Value != somaControloRetencoes) {
            result.add(new DeclValidationMessage("YE002", "aAnexoE.qQuadro04", new String[]{"aAnexoE.qQuadro04.fanexoEq04C2"}));
        }
    }

    private void validateStep1Q04ALinha(ValidationResult result, Quadro03 rostoQ03Model, Quadro02 rostoQ02Model, Quadro07 rostoQ07Model, AnexoEq04T1_Linha linha, int nLinha, String linkNifARosto, String linkNifBRosto, String linkDependentesRosto, String linkConjugeFalecidoRosto, String linkAscendentesRosto, Map<String, String> linhasUnicas) {
        String linkLine = AnexoEq04T1_Linha.getLink(nLinha);
        String linkNIF = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.NIF.getIndex();
        String linkCodRend = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkTitular = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.TITULAR.getIndex();
        String linkReten = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.RETENCOES.getIndex();
        Long linhaNIF = linha.getNIF();
        String linhaCodRend = linha.getCodRendimentos();
        String linhaTitular = linha.getTitular();
        Long linhaRend = linha.getRendimentos();
        Long linhaReten = linha.getRetencoes();
        String linhaID = linhaNIF + "-" + linhaCodRend + "-" + linhaTitular;
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) || StringUtil.isEmpty(linhaCodRend) || StringUtil.isEmpty(linhaTitular) || !linhasUnicas.containsKey(linhaID))) {
            result.add(new DeclValidationMessage("E005", linkLine, new String[]{linkLine}));
        } else {
            linhasUnicas.put(linhaID, null);
        }
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && linhaCodRend == null && linhaTitular == null && Modelo3IRSValidatorUtil.isEmptyOrZero(linhaRend) && Modelo3IRSValidatorUtil.isEmptyOrZero(linhaReten)) {
            result.add(new DeclValidationMessage("E006", linkLine, new String[]{linkLine}));
        }
        this.validateStep1Q04ALinhaNifEntidadeDevedora(result, rostoQ03Model, rostoQ07Model, linhaNIF, linkNIF, linkNifARosto, linkNifBRosto, linkDependentesRosto, linkConjugeFalecidoRosto, linkAscendentesRosto);
        this.validateStep1Q04ALinhaCodRend(result, linhaCodRend, linkCodRend);
        this.validateE062(result, linha, nLinha);
        this.validateStep1Q04ALinhaTitular(result, rostoQ03Model, rostoQ07Model, linhaTitular, linkTitular);
        this.validateStep1Q04ALinhaRetencoes(result, rostoQ02Model.getQ02C02(), linhaNIF, linhaCodRend, linhaRend, linhaReten, linkReten, linkNIF);
    }

    protected void validateE062(ValidationResult result, AnexoEq04T1_Linha linha, int nLinha) {
        String codRend = linha.getCodRendimentos();
        if (codRend != null && codRend.equals("E5")) {
            String linkLine = AnexoEq04T1_Linha.getLink(nLinha);
            String linkCodRend = linkLine + ".c" + AnexoEq04T1_LinhaBase.Property.CODRENDIMENTOS.getIndex();
            String linkAnoDeRendimentos = "aRosto.qQuadro02.fq02C02";
            result.add(new DeclValidationMessage("E062", linkCodRend, new String[]{linkCodRend, linkAnoDeRendimentos}));
        }
    }

    private void validateStep1Q04ALinhaNifEntidadeDevedora(ValidationResult result, Quadro03 rostoQ03Model, Quadro07 rostoQ07Model, Long linhaNIF, String linkNIF, String linkNifARosto, String linkNifBRosto, String linkDependentesRosto, String linkConjugeFalecidoRosto, String linkAscendentesRosto) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) || Modelo3IRSValidatorUtil.startsWith(linhaNIF, "1", "2", "3", "5", "6", "7", "9"))) {
            result.add(new DeclValidationMessage("E007", linkNIF, new String[]{linkNIF}));
        }
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) || Modelo3IRSValidatorUtil.isNIFValid(linhaNIF))) {
            result.add(new DeclValidationMessage("E008", linkNIF, new String[]{linkNIF}));
        }
        this.validateE009(result, rostoQ03Model, rostoQ07Model, linhaNIF, linkNIF);
    }

    protected void validateE009(ValidationResult result, Quadro03 rostoQ03Model, Quadro07 rostoQ07Model, Long linhaNIF, String linkNIF) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (rostoQ03Model.isSujeitoPassivoA(linhaNIF) || rostoQ03Model.isSujeitoPassivoB(linhaNIF) || rostoQ03Model.existsInDependentes(linhaNIF) || rostoQ07Model.isConjugeFalecido(linhaNIF) || rostoQ07Model.existsInAscendentes(linhaNIF) || rostoQ07Model.existsInAfilhadosCivisEmComunhao(linhaNIF) || rostoQ03Model.existsInDependentesEmGuardaConjunta(linhaNIF) || rostoQ03Model.existsInDependentesEmGuardaConjuntaOutro(linhaNIF) || rostoQ07Model.existsInAscendentesEColaterais(linhaNIF))) {
            result.add(new DeclValidationMessage("E009", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    private void validateStep1Q04ALinhaCodRend(ValidationResult result, String linhaCodRend, String linkCodRend) {
        if (!(linhaCodRend == null || linhaCodRend.equals("E") || linhaCodRend.equals("E5"))) {
            result.add(new DeclValidationMessage("E012", linkCodRend, new String[]{linkCodRend}));
        }
    }

    private void validateStep1Q04ALinhaTitular(ValidationResult result, Quadro03 rostoQ03Model, Quadro07 rostoQ07Model, String linhaTitular, String linkTitular) {
        this.validateE013(result, linhaTitular, linkTitular);
    }

    protected void validateE013(ValidationResult result, String titular, String linkForTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("E013", linkForTitular, new String[]{linkForTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1"}));
        }
    }

    protected void validateStep1Q04ALinhaRetencoes(ValidationResult result, Long anoRendimento, Long linhaNIF, String linhaCodRend, Long linhaRend, Long linhaReten, String linkReten, String linkNIF) {
        if (linhaCodRend != null && linhaCodRend.equals("E") && linhaReten != null && linhaRend != null && linhaReten > Math.round((double)linhaRend.longValue() * 0.285)) {
            result.add(new DeclValidationMessage("E014", linkReten, new String[]{linkReten}));
        }
    }

    private void validateStep1Q04B(ValidationResult result, RostoModel rostoModel, AnexoEModel anexoEModel) {
        long anexoEq04C3Value;
        long anexoEq04C4Value;
        Quadro04 anexoEQ04Model = anexoEModel.getQuadro04();
        Quadro02 rostoQ02Model = rostoModel.getQuadro02();
        Quadro03 rostoQ03Model = rostoModel.getQuadro03();
        Quadro07 rostoQ07Model = rostoModel.getQuadro07();
        String linkNifARosto = "aRosto.qQuadro03.fq03C03";
        String linkNifBRosto = "aRosto.qQuadro03.fq03C04";
        String linkDependentesRosto = "aRosto.qQuadro03.fq03B03";
        String linkConjugeFalecidoRosto = "aRosto.qQuadro07.fq07C1";
        String linkAscendentesRosto = "aRosto.qQuadro07.fq07C01";
        HashMap<String, String> linhasUnicas = new HashMap<String, String>();
        this.validateE063(result, anexoEModel);
        if (anexoEQ04Model.getAnexoEq04T2().size() > 49) {
            result.add(new DeclValidationMessage("E022", "aAnexoE.qQuadro04.tanexoEq04T2", new String[]{"aAnexoE.qQuadro04.tanexoEq04T2"}));
        }
        int nLinha = 0;
        int somaControloRendimentos = 0;
        int somaControloRetencoes = 0;
        for (AnexoEq04T2_Linha linha : anexoEQ04Model.getAnexoEq04T2()) {
            this.validateStep1Q04BLinha(result, rostoQ02Model, rostoQ03Model, rostoQ07Model, linha, ++nLinha, linkNifARosto, linkNifBRosto, linkDependentesRosto, linkConjugeFalecidoRosto, linkAscendentesRosto, linhasUnicas);
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendimentos())) {
                somaControloRendimentos = (int)((long)somaControloRendimentos + linha.getRendimentos());
            }
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRetencoes())) continue;
            somaControloRetencoes = (int)((long)somaControloRetencoes + linha.getRetencoes());
        }
        this.validateE030(result, rostoModel, anexoEModel);
        this.validateE031(result, rostoModel, anexoEModel);
        long l = anexoEq04C3Value = anexoEQ04Model.getAnexoEq04C3() != null ? anexoEQ04Model.getAnexoEq04C3() : 0;
        if (anexoEq04C3Value != (long)somaControloRendimentos) {
            result.add(new DeclValidationMessage("YE003", "aAnexoE.qQuadro04", new String[]{"aAnexoE.qQuadro04.fanexoEq04C3"}));
        }
        long l2 = anexoEq04C4Value = anexoEQ04Model.getAnexoEq04C4() != null ? anexoEQ04Model.getAnexoEq04C4() : 0;
        if (anexoEq04C4Value != (long)somaControloRetencoes) {
            result.add(new DeclValidationMessage("YE004", "aAnexoE.qQuadro04", new String[]{"aAnexoE.qQuadro04.fanexoEq04C4"}));
        }
        this.validateStep1Q04BOptouEnglobamento(result, anexoEModel);
    }

    protected void validateE030(ValidationResult result, RostoModel rostoModel, AnexoEModel anexoEModel) {
        long anoExercicio = rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0;
        String codingoRendimentosE3 = "E3";
        long ano2006 = 2006;
        long ano2011 = 2011;
        long ano2012 = 2012;
        long ano2013 = 2013;
        double percent255 = 0.255;
        double percent266 = 0.266;
        double percent570 = 0.57;
        if (!(anoExercicio <= 0 || anexoEModel == null || anexoEModel.getQuadro04().getAnexoEq04T2() == null || anexoEModel.getQuadro04().getAnexoEq04T2().isEmpty())) {
            for (int i = 0; i < anexoEModel.getQuadro04().getAnexoEq04T2().size(); ++i) {
                long rendimentos;
                AnexoEq04T2_Linha anexoEq04T2_Linha = anexoEModel.getQuadro04().getAnexoEq04T2().get(i);
                long retencoes = anexoEq04T2_Linha.getRetencoes() != null ? anexoEq04T2_Linha.getRetencoes() : 0;
                long l = rendimentos = anexoEq04T2_Linha.getRendimentos() != null ? anexoEq04T2_Linha.getRendimentos() : 0;
                if (!"E3".equals(anexoEq04T2_Linha.getCodRendimentos()) || (retencoes <= (long)(0.255 * (double)rendimentos) || anoExercicio <= 2006 || anoExercicio >= 2012) && (retencoes <= (long)(0.266 * (double)rendimentos) || anoExercicio <= 2011 || anoExercicio >= 2013) && (retencoes <= (long)(0.57 * (double)rendimentos) || anoExercicio <= 2012)) continue;
                result.add(new DeclValidationMessage("E030", AnexoEq04T2_Linha.getLink(i + 1, AnexoEq04T2_LinhaBase.Property.RETENCOES), new String[]{AnexoEq04T2_Linha.getLink(i + 1, AnexoEq04T2_LinhaBase.Property.RETENCOES)}));
            }
        }
    }

    protected void validateE031(ValidationResult result, RostoModel rostoModel, AnexoEModel anexoEModel) {
        long anoExercicio = rostoModel.getQuadro02().getQ02C02() != null ? rostoModel.getQuadro02().getQ02C02() : 0;
        String codingoRendimentosE1 = "E1";
        String codingoRendimentosE4 = "E4";
        long ano2005 = 2005;
        long ano2006 = 2006;
        long ano2011 = 2011;
        long ano2012 = 2012;
        long ano2013 = 2013;
        double percent305 = 0.305;
        double percent435 = 0.435;
        double percent532 = 0.532;
        double percent580 = 0.58;
        if (!(anoExercicio <= 0 || anexoEModel == null || anexoEModel.getQuadro04().getAnexoEq04T2() == null || anexoEModel.getQuadro04().getAnexoEq04T2().isEmpty())) {
            for (int i = 0; i < anexoEModel.getQuadro04().getAnexoEq04T2().size(); ++i) {
                long rendimentos;
                AnexoEq04T2_Linha anexoEq04T2_Linha = anexoEModel.getQuadro04().getAnexoEq04T2().get(i);
                long retencoes = anexoEq04T2_Linha.getRetencoes() != null ? anexoEq04T2_Linha.getRetencoes() : 0;
                long l = rendimentos = anexoEq04T2_Linha.getRendimentos() != null ? anexoEq04T2_Linha.getRendimentos() : 0;
                if (!"E1".equals(anexoEq04T2_Linha.getCodRendimentos()) && !"E4".equals(anexoEq04T2_Linha.getCodRendimentos()) || (retencoes <= (long)(0.305 * (double)rendimentos) || anoExercicio >= 2006) && (retencoes <= Math.round(0.435 * (double)rendimentos) || anoExercicio <= 2005 || anoExercicio >= 2012) && (retencoes <= Math.round(0.532 * (double)rendimentos) || anoExercicio <= 2011 || anoExercicio >= 2013) && (retencoes <= Math.round(0.58 * (double)rendimentos) || anoExercicio <= 2012)) continue;
                result.add(new DeclValidationMessage("E031", AnexoEq04T2_Linha.getLink(i + 1, AnexoEq04T2_LinhaBase.Property.RETENCOES), new String[]{AnexoEq04T2_Linha.getLink(i + 1, AnexoEq04T2_LinhaBase.Property.RETENCOES)}));
            }
        }
    }

    private void validateStep1Q04BLinha(ValidationResult result, Quadro02 rostoQ02Model, Quadro03 rostoQ03Model, Quadro07 rostoQ07Model, AnexoEq04T2_Linha linha, int nLinha, String linkNifARosto, String linkNifBRosto, String linkDependentesRosto, String linkConjugeFalecidoRosto, String linkAscendentesRosto, Map<String, String> linhasUnicas) {
        String linkLine = AnexoEq04T2_Linha.getLink(nLinha);
        String linkNIF = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.NIF.getIndex();
        String linkCodRend = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkTitular = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.TITULAR.getIndex();
        String linkRendimento = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.RENDIMENTOS.getIndex();
        String linkReten = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.RETENCOES.getIndex();
        Long linhaNIF = linha.getNIF();
        String linhaCodRend = linha.getCodRendimentos();
        String linhaTitular = linha.getTitular();
        Long linhaRend = linha.getRendimentos();
        Long linhaReten = linha.getRetencoes();
        String linhaID = linhaNIF + "-" + linhaCodRend + "-" + linhaTitular;
        Long anoExercicio = rostoQ02Model.getQ02C02();
        if (linhasUnicas.containsKey(linhaID)) {
            result.add(new DeclValidationMessage("E023", linkLine, new String[]{linkLine}));
        } else {
            linhasUnicas.put(linhaID, null);
        }
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && linhaCodRend == null && linhaTitular == null && Modelo3IRSValidatorUtil.isEmptyOrZero(linhaRend) && Modelo3IRSValidatorUtil.isEmptyOrZero(linhaReten)) {
            result.add(new DeclValidationMessage("E024", linkLine, new String[]{linkLine}));
        }
        this.validateStep1Q04BLinhaNifEntidadeDevedora(result, rostoQ03Model, rostoQ07Model, linhaNIF, linkNIF, linkNifARosto, linkNifBRosto, linkDependentesRosto, linkConjugeFalecidoRosto, linkAscendentesRosto);
        this.validateE028(result, linhaCodRend, linkCodRend);
        this.validateStep1Q04BLinhaTitular(result, linhaTitular, linkCodRend, linkTitular);
        this.validateStep1Q04BLinhaRetencoes(result, anoExercicio, linhaCodRend, linhaRend, linhaReten, linkReten, linhaTitular, linhaNIF, linkCodRend, linkTitular, linkRendimento);
        this.validateE043(result, rostoQ02Model, linhaRend, linhaReten, linhaCodRend, linkReten);
        this.validateE044(result, rostoQ02Model, linhaCodRend, nLinha);
    }

    protected void validateE044(ValidationResult result, Quadro02 rostoQ02Model, String linhaCodRend, int numLinha) {
        String linkLine = AnexoEq04T2_Linha.getLink(numLinha);
        String linkCodRend = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.CODRENDIMENTOS.getIndex();
        String linkReten = linkLine + ".c" + AnexoEq04T2_LinhaBase.Property.RETENCOES.getIndex();
        if (rostoQ02Model.getQ02C02() != null && linhaCodRend != null && (rostoQ02Model.getQ02C02() < 2009 && linhaCodRend.equals("E6") || rostoQ02Model.getQ02C02() < 2014 && linhaCodRend.equals("E7"))) {
            result.add(new DeclValidationMessage("E044", linkReten, new String[]{linkCodRend, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    protected void validateE043(ValidationResult result, Quadro02 rostoQ02Model, Long linhaRend, Long linhaReten, String linhaCodRend, String linkReten) {
        if (rostoQ02Model.getQ02C02() != null && linhaCodRend != null && linhaReten != null && linhaRend != null && (rostoQ02Model.getQ02C02() > 2008 && linhaCodRend.equals("E6") && linhaReten > Math.round((double)linhaRend.longValue() * 0.285) || rostoQ02Model.getQ02C02() > 2013 && linhaCodRend.equals("E7") && linhaReten > Math.round((double)linhaRend.longValue() * 0.565))) {
            result.add(new DeclValidationMessage("E043", linkReten, new String[]{linkReten}));
        }
    }

    private void validateStep1Q04BLinhaNifEntidadeDevedora(ValidationResult result, Quadro03 rostoQ03Model, Quadro07 rostoQ07Model, Long linhaNIF, String linkNIF, String linkNifARosto, String linkNifBRosto, String linkDependentesRosto, String linkConjugeFalecidoRosto, String linkAscendentesRosto) {
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) || Modelo3IRSValidatorUtil.startsWith(linhaNIF, "1", "2", "3", "5", "6", "7", "9"))) {
            result.add(new DeclValidationMessage("E025", linkNIF, new String[]{linkNIF}));
        }
        if (!(Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) || Modelo3IRSValidatorUtil.isNIFValid(linhaNIF))) {
            result.add(new DeclValidationMessage("E026", linkNIF, new String[]{linkNIF}));
        }
        this.validateE027(result, rostoQ03Model, rostoQ07Model, linhaNIF, linkNIF);
    }

    protected void validateE027(ValidationResult result, Quadro03 rostoQ03Model, Quadro07 rostoQ07Model, Long linhaNIF, String linkNIF) {
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (rostoQ03Model.isSujeitoPassivoA(linhaNIF) || rostoQ03Model.isSujeitoPassivoB(linhaNIF) || rostoQ03Model.existsInDependentes(linhaNIF) || rostoQ07Model.isConjugeFalecido(linhaNIF) || rostoQ07Model.existsInAscendentes(linhaNIF) || rostoQ07Model.existsInAfilhadosCivisEmComunhao(linhaNIF) || rostoQ03Model.existsInDependentesEmGuardaConjunta(linhaNIF) || rostoQ03Model.existsInDependentesEmGuardaConjuntaOutro(linhaNIF) || rostoQ07Model.existsInAscendentesEColaterais(linhaNIF))) {
            result.add(new DeclValidationMessage("E027", linkNIF, new String[]{linkNIF, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.trostoq03DT1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro07.fq07C01", "aRosto.qQuadro07.trostoq07CT1"}));
        }
    }

    protected void validateE028(ValidationResult result, String linhaCodRend, String linkCodRend) {
        if (!(linhaCodRend == null || StringUtil.in(linhaCodRend, new String[]{"E1", "E3", "E4", "E6", "E7"}))) {
            result.add(new DeclValidationMessage("E028", linkCodRend, new String[]{linkCodRend}));
        }
    }

    private void validateStep1Q04BLinhaTitular(ValidationResult result, String linhaTitular, String linkCodRend, String linkTitular) {
        this.validateE029(result, linhaTitular, linkTitular);
    }

    protected void validateE029(ValidationResult result, String titular, String linkForTitular) {
        if (!(StringUtil.isEmpty(titular) || Quadro03.isSujeitoPassivoA(titular) || Quadro03.isSujeitoPassivoB(titular) || Quadro03.isDependenteNaoDeficiente(titular) || Quadro03.isDependenteDeficiente(titular) || Quadro07.isConjugeFalecido(titular) || Quadro03.isDependenteEmGuardaConjunta(titular))) {
            result.add(new DeclValidationMessage("E029", linkForTitular, new String[]{linkForTitular, "aRosto.qQuadro03.fq03C03", "aRosto.qQuadro03.fq03C04", "aRosto.qQuadro03.fq03CD1", "aRosto.qQuadro03.fq03CDD1", "aRosto.qQuadro07.fq07C1", "aRosto.qQuadro03.trostoq03DT1"}));
        }
    }

    private void validateStep1Q04BLinhaRetencoes(ValidationResult result, Long anoExercicio, String linhaCodRend, Long linhaRend, Long linhaReten, String linkReten, String linhaTitular, Long linhaNIF, String linkCodRend, String linkTitular, String linkRendimento) {
        if (linhaReten != null && (linhaRend != null && linhaRend == 0 || linhaRend == null) && linhaTitular == null && linhaCodRend == null && linhaNIF == null) {
            result.add(new DeclValidationMessage("E057", linkReten, new String[]{linkReten, linkCodRend, linkTitular, linkRendimento}));
        }
    }

    private void validateStep1Q04BOptouEnglobamento(ValidationResult result, AnexoEModel anexoEModel) {
        String linkEnglobamento = "aAnexoE.qQuadro04.fanexoEq04B1OP1";
        Quadro04 anexoEQ04Model = anexoEModel.getQuadro04();
        if (!(anexoEModel.getQuadro04().getAnexoEq04B1() == null || anexoEModel.getQuadro04().getAnexoEq04B1().equals("1") || anexoEModel.getQuadro04().getAnexoEq04B1().equals("2"))) {
            result.add(new DeclValidationMessage("E040", linkEnglobamento, new String[]{linkEnglobamento}));
        } else {
            this.validateE039(result, anexoEModel);
        }
    }

    protected void validateE039(ValidationResult result, AnexoEModel anexoEModel) {
        String linkEnglobamento = "aAnexoE.qQuadro04.fanexoEq04B1OP1";
        String linkDocumentos = "aAnexoE.qQuadro04.tanexoEq04T2";
        Long ano = anexoEModel.getQuadro02().getAnexoEq02C01();
        if (anexoEModel.getQuadro04().getAnexoEq04B1() != null && anexoEModel.getQuadro04().getAnexoEq04T2().isEmpty() && ano != null && ano < 2014) {
            result.add(new DeclValidationMessage("E039", linkEnglobamento, new String[]{linkEnglobamento, linkDocumentos}));
        }
    }

    @Override
    protected ValidationResult validateNETPapel(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        return result;
    }

    protected void validateE011(ValidationResult result, Long linhaNIF, Long linhaRend, String linhaCodRend, String linhaTitular, String linkLine, String linkNIF) {
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(linhaNIF) && (linhaCodRend != null || linhaTitular != null || Modelo3IRSValidatorUtil.isGreaterThanZero(linhaRend)) && !"E5".equals(linhaCodRend)) {
            result.add(new DeclValidationMessage("E011", linkLine, new String[]{linkNIF}));
        }
    }

    protected void validateE063(ValidationResult result, AnexoEModel anexoEModel) {
        Long ano = anexoEModel.getQuadro02().getAnexoEq02C01();
        String linkEnglobamento = "aAnexoE.qQuadro04.fanexoEq04B1OP1";
        if (anexoEModel.getQuadro04().getAnexoEq04B1() != null && ano != null && ano > 2013) {
            result.add(new DeclValidationMessage("E063", linkEnglobamento, new String[]{linkEnglobamento, "aRosto.qQuadro02.fq02C02"}));
        }
    }

    @Override
    protected ValidationResult validateNETDC(DeclaracaoModel model) {
        ValidationResult result = new ValidationResult();
        RostoModel rostoModel = (RostoModel)model.getAnexo(RostoModel.class);
        AnexoEModel anexoEModel = (AnexoEModel)model.getAnexo(AnexoEModel.class);
        if (anexoEModel != null) {
            this.validateStep1Q04A(result, rostoModel, anexoEModel);
            this.validateStep1Q04B(result, rostoModel, anexoEModel);
        }
        return result;
    }
}

