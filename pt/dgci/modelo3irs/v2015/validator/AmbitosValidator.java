/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.validator;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.Validator;

public abstract class AmbitosValidator<T>
implements Validator<T> {
    protected AmbitoIRS ambitoIRS;

    public AmbitosValidator(AmbitoIRS ambito) {
        if (ambito == null) {
            throw new RuntimeException("\u00c2mbito n\u00e3o definido");
        }
        this.ambitoIRS = ambito;
    }

    @Override
    public ValidationResult validate(T model) {
        ValidationResult netPapelValidationResult;
        ValidationResult netDCValidationResult;
        ValidationResult specificValidationResult;
        ValidationResult result = new ValidationResult();
        ValidationResult commonsValidationResult = this.validateCommons(model);
        if (commonsValidationResult != null) {
            result.addAllFrom(commonsValidationResult);
        }
        if (AmbitosValidator.in(this.ambitoIRS, new AmbitoIRS[]{AmbitoIRS.NET, AmbitoIRS.PAPEL}) && (netPapelValidationResult = this.validateNETPapel(model)) != null) {
            result.addAllFrom(netPapelValidationResult);
        }
        if (AmbitosValidator.in(this.ambitoIRS, new AmbitoIRS[]{AmbitoIRS.NET, AmbitoIRS.DC}) && (netDCValidationResult = this.validateNETDC(model)) != null) {
            result.addAllFrom(netDCValidationResult);
        }
        if ((specificValidationResult = this.validateSpecific(model)) != null) {
            result.addAllFrom(specificValidationResult);
        }
        return result;
    }

    public static boolean in(Enum<AmbitoIRS> ambitoCorrente, Enum<AmbitoIRS>[] ambitosSuportados) {
        for (int i = 0; i < ambitosSuportados.length; ++i) {
            if (ambitoCorrente != ambitosSuportados[i]) continue;
            return true;
        }
        return false;
    }

    protected abstract ValidationResult validateCommons(T var1);

    protected abstract ValidationResult validateNETPapel(T var1);

    protected abstract ValidationResult validateNETDC(T var1);

    protected abstract ValidationResult validateSpecific(T var1);

    public static enum AmbitoIRS {
        NET,
        DC,
        PAPEL;
        

        private AmbitoIRS() {
        }
    }

}

