/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015;

import java.awt.Frame;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Initializer;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.exception.Modelo3IRSv2015ExceptionHandler;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS0Dialog;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.TaxClientRevampedApplication;
import pt.opensoft.taxclient.TaxClientRevampedInitializer;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.TaxclientParameters;

public class Modelo3IRSv2015Application
extends TaxClientRevampedApplication {
    private static final long serialVersionUID = 7157323352152659878L;

    @Override
    protected void preInit() {
        Session.setExceptionHandler(new Modelo3IRSv2015ExceptionHandler());
        this.registerTaxClientRevampedGUI(new Modelo3IRSv2015Initializer());
        this.setIconImage(IconFactory.getImage("icon.gif"));
        if (System.getProperty("prod") != null) {
            TaxclientParameters.setProduction(Boolean.parseBoolean(System.getProperty("prod")));
        }
    }

    @Override
    protected void postInit() {
        Session.getCurrentDeclaracao().setSelectedAnexo(new FormKey(RostoModel.class));
        Session.getCurrentDeclaracao().setSelectedQuadro(QuadroInicio.class.getSimpleName());
    }

    public static void main(String[] args) throws UnsupportedLookAndFeelException, InterruptedException, InvocationTargetException {
        final Modelo3IRSv2015Application app = new Modelo3IRSv2015Application();
        app.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent e) {
                app.dispose();
                System.exit(0);
            }
        });
        app.start();
        app.setLocation(0, 0);
        SwingUtilities.invokeAndWait(new Runnable(){

            @Override
            public void run() {
                try {
                    if (!TaxclientParameters.isProduction()) {
                        app.setTitle(app.getTitle() + " - TESTES");
                    }
                    app.setVisible(true);
                    app.setSize(800, 600);
                    app.setExtendedState(6);
                    if (!Session.isApplet()) {
                        ((Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame()).setApp(app);
                        if (Modelo3IRSv2015Parameters.instance().isPrePreenchimentoActive()) {
                            new QuadroAS0Dialog(app).setVisible(true);
                        }
                    }
                }
                catch (Exception evtException) {
                    Session.getExceptionHandler().handle(evtException);
                }
            }
        });
    }

}

