/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015;

import java.awt.Component;
import java.awt.Frame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.text.html.parser.ParserDelegator;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Initializer;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.exception.Modelo3IRSv2015ExceptionHandler;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.dgci.modelo3irs.v2015.gui.dialogs.PrePreenchimentoReceiptDialog;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro02PanelExtension;
import pt.dgci.modelo3irs.v2015.ui.rosto.Quadro03PanelExtension;
import pt.dgci.modelo3irs.v2015.ui.rosto.QuadroAS0Dialog;
import pt.dgci.modelo3irs.v2015.util.Modelo3IRSAppletUtil;
import pt.dgci.modelo3irs.v2015.util.Modelo3IRSSessionDisableManager;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JNIFTextField;
import pt.opensoft.taxclient.TaxClientRevampedApplet;
import pt.opensoft.taxclient.TaxClientRevampedInitializer;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class Modelo3IRSv2015Applet
extends TaxClientRevampedApplet {
    private static final long serialVersionUID = -2328076726950379420L;

    @Override
    protected void preInit() {
        ParserDelegator workaround = new ParserDelegator();
        Session.setExceptionHandler(new Modelo3IRSv2015ExceptionHandler());
        this.registerTaxClientRevampedGUI(new Modelo3IRSv2015Initializer());
        String isProdEnv = this.getParameter("isProductionEnv");
        if (!StringUtil.isEmpty(isProdEnv)) {
            TaxclientParameters.setProduction(Boolean.parseBoolean(isProdEnv));
        }
        if (!StringUtil.isEmpty(this.getParameter("irs.fase2"))) {
            Modelo3IRSv2015Parameters.fase2AppletLoaded = Boolean.parseBoolean(this.getParameter("irs.fase2"));
        }
        Modelo3IRSv2015Parameters.isAppletOffline = !StringUtil.isEmpty(this.getParameter("appletOffline"));
    }

    @Override
    protected void postInit() {
        Modelo3IRSAppletUtil.loadDeclFromAppletParameter(Session.getCurrentDeclaracao().getDeclaracaoModel());
        final Modelo3IRSv2015ApplicationMainFrame mainframe = (Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame();
        AnexoTabbedPane rostoTabbedPane = mainframe.getDeclarationDisplayer().getAnexoTabbedPane(new FormKey(RostoModel.class));
        Session.getCurrentDeclaracao().setSelectedAnexo(new FormKey(RostoModel.class));
        Session.getCurrentDeclaracao().setSelectedQuadro(QuadroInicio.class.getSimpleName());
        final JScrollPane quadroInicioScrollPane = (JScrollPane)rostoTabbedPane.getComponentInfoByName(QuadroInicio.class.getSimpleName()).getSecond();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                quadroInicioScrollPane.getVerticalScrollBar().setValue(quadroInicioScrollPane.getVerticalScrollBar().getMinimum());
            }
        });
        String isAppletOffline = this.getParameter("appletOffline");
        String declStatus = this.getParameter("declStatus");
        boolean isSubstituirEnabled = Boolean.parseBoolean(this.getParameter("isSubstituirEnabled"));
        if (StringUtil.isEmpty(isAppletOffline) && isSubstituirEnabled) {
            Modelo3IRSSessionDisableManager.disableAll();
            ActionsTaxClientManager.getAction("Substituir").setEnabled(true);
        }
        boolean isPPConversaoImpossivel = Boolean.parseBoolean(this.getParameter("isPPConversaoImpossivel"));
        if (Modelo3IRSv2015Parameters.DECL_STATUS.COM_PRE_PREENCHIMENTO.equalsDeclStatus(declStatus) || isPPConversaoImpossivel) {
            RostoModel rostoModel = ((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).getRosto();
            String exercicio = rostoModel.getQuadro02().getQ02C02() != null ? String.valueOf(rostoModel.getQuadro02().getQ02C02()) : null;
            String nifA = rostoModel.getQuadro03().getQ03C03() != null ? String.valueOf(rostoModel.getQuadro03().getQ03C03()) : null;
            String nifB = rostoModel.getQuadro03().getQ03C04() != null ? String.valueOf(rostoModel.getQuadro03().getQ03C04()) : null;
            boolean hasAnexoF = false;
            if (((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).getAnexoF() != null) {
                hasAnexoF = true;
            }
            final PrePreenchimentoReceiptDialog prePreenchimentoReceiptDialog = new PrePreenchimentoReceiptDialog(this, exercicio, nifA, nifB, declStatus, isPPConversaoImpossivel, false, hasAnexoF);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    prePreenchimentoReceiptDialog.showDialog();
                    if (!prePreenchimentoReceiptDialog.keepAnexoF()) {
                        ((Modelo3IRSv2015Model)Session.getCurrentDeclaracao().getDeclaracaoModel()).removeAnexo(new FormKey(AnexoFModel.class));
                    }
                }
            });
        }
        if (!StringUtil.isEmpty(isAppletOffline)) {
            Session.setApplet(null);
            if (Modelo3IRSv2015Parameters.instance().isPrePreenchimentoActive()) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        new QuadroAS0Dialog(JOptionPane.getFrameForComponent(mainframe)).setVisible(true);
                    }
                });
            }
            Quadro02PanelExtension q02 = (Quadro02PanelExtension)rostoTabbedPane.getPanelExtensionByModel(Quadro02.class.getSimpleName());
            Quadro03PanelExtension q03 = (Quadro03PanelExtension)rostoTabbedPane.getPanelExtensionByModel(Quadro03.class.getSimpleName());
            q02.getQ02C02().setEnabled(true);
            q03.getQ03C03().setEnabled(true);
            q03.getQ03C04().setEnabled(true);
        }
        Session.getCurrentDeclaracao().setSelectedRootAnexo(new FormKey(RostoModel.class));
        Session.getCurrentDeclaracao().setSelectedQuadro(QuadroInicio.class.getSimpleName());
    }

    public boolean isGravada() {
        return !Session.getCurrentDeclaracao().getDeclaracaoModel().isDirty();
    }

}

