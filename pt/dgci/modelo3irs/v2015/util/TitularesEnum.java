/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util;

public enum TitularesEnum {
    SPAB,
    SPC,
    D,
    DD,
    DG,
    F,
    AF,
    AS,
    AC;
    

    private TitularesEnum() {
    }
}

