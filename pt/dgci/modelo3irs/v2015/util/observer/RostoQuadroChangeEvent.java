/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util.observer;

import java.util.Observable;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeState;

public abstract class RostoQuadroChangeEvent
extends Observable {
    public void fireChange() {
        this.setChanged();
        this.notifyObservers();
    }

    public void fireDeleteRow(int rowIndex, boolean isEmptyLine, boolean isLastRow) {
        this.setChanged();
        this.notifyObservers(new RostoQuadroChangeState(rowIndex, isEmptyLine, RostoQuadroChangeState.OperationEnum.DELETE, isLastRow));
    }

    public void fireUpdateRow(int rowIndex, boolean isEmptyLine, boolean isLastRow) {
        this.setChanged();
        this.notifyObservers(new RostoQuadroChangeState(rowIndex, isEmptyLine, RostoQuadroChangeState.OperationEnum.UPDATE, isLastRow));
    }

    public abstract String getPrefixoTitulares();
}

