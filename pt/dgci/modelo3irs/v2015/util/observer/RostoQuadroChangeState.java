/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util.observer;

public class RostoQuadroChangeState {
    private int rowIndex = -1;
    private boolean isEmptyLine = false;
    private boolean isLastRow = false;
    private OperationEnum operation = null;

    public RostoQuadroChangeState() {
    }

    public RostoQuadroChangeState(int rowIndex, boolean isEmptyLine, OperationEnum operation, boolean isLastRow) {
        this.rowIndex = rowIndex;
        this.isEmptyLine = isEmptyLine;
        this.operation = operation;
        this.isLastRow = isLastRow;
    }

    public int getRowIndex() {
        return this.rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public boolean isEmptyLine() {
        return this.isEmptyLine;
    }

    public void setEmptyLine(boolean isEmptyLine) {
        this.isEmptyLine = isEmptyLine;
    }

    public OperationEnum getOperation() {
        return this.operation;
    }

    public void setOperation(OperationEnum operation) {
        this.operation = operation;
    }

    public boolean isLastRow() {
        return this.isLastRow;
    }

    public void setLastRow(boolean isLastRow) {
        this.isLastRow = isLastRow;
    }

    public static enum OperationEnum {
        ADD("C"),
        UPDATE("U"),
        DELETE("D");
        
        String value;

        private OperationEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

}

