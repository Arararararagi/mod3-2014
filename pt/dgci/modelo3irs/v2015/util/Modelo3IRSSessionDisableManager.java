/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util;

import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.text.JTextComponent;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.structural.add.AddAnexoAction;
import pt.opensoft.taxclient.actions.structural.remove.RemoveAnexoAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoPresentationModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.util.Session;

public class Modelo3IRSSessionDisableManager {
    private static final List<Component> disabledComponents = new ArrayList<Component>();
    private static final List<EnhancedAction> disabledActions = new ArrayList<EnhancedAction>();

    public static void disableAll() {
        DeclaracaoPresentationModel declaracaoPresModel = Session.getCurrentDeclaracao();
        DeclaracaoModel declaracaoModel = Session.getCurrentDeclaracao().getDeclaracaoModel();
        Session.setEditable(false);
        FormKey selectedAnexo = declaracaoPresModel.getSelectedAnexo();
        String selectedQuadro = declaracaoPresModel.getSelectedQuadro();
        ((Modelo3IRSv2015Model)declaracaoModel).reload();
        declaracaoPresModel.setSelectedAnexo(selectedAnexo);
        declaracaoPresModel.setSelectedQuadro(selectedQuadro);
        DeclarationDisplayer declarationDisplayer = Session.getMainFrame().getDeclarationDisplayer();
        DeclaracaoMapModel<FormKey, AnexoModel> declMapModel = declaracaoModel.getAnexos();
        for (FormKey formKey : declMapModel.keySet()) {
            AnexoTabbedPane anexoTabbedPane = declarationDisplayer.getAnexoTabbedPane(formKey);
            Modelo3IRSSessionDisableManager.disableComponentTree(anexoTabbedPane);
        }
        List<AddAnexoAction> addAnexoActions = ActionsTaxClientManager.getAllAddActions();
        for (AddAnexoAction addAnexoAction : addAnexoActions) {
            if (!addAnexoAction.isEnabled()) continue;
            Modelo3IRSSessionDisableManager.disableAction(addAnexoAction);
        }
        List<RemoveAnexoAction> removeAnexoActions = ActionsTaxClientManager.getAllEnabledRemoveActions();
        for (RemoveAnexoAction removeAnexoAction : removeAnexoActions) {
            if (!removeAnexoAction.isEnabled()) continue;
            Modelo3IRSSessionDisableManager.disableAction(removeAnexoAction);
        }
        Modelo3IRSSessionDisableManager.disableAction(ActionsTaxClientManager.getAction("Novo"));
        Modelo3IRSSessionDisableManager.disableAction(ActionsTaxClientManager.getAction("Abrir"));
        Modelo3IRSSessionDisableManager.disableAction(ActionsTaxClientManager.getAction("Validar"));
        Modelo3IRSSessionDisableManager.disableAction(ActionsTaxClientManager.getAction("Submeter"));
        Modelo3IRSSessionDisableManager.disableAction(ActionsTaxClientManager.getAction("Wizard"));
    }

    public static void revertDisableAll() {
        DeclaracaoPresentationModel declaracaoPresModel = Session.getCurrentDeclaracao();
        DeclaracaoModel declaracaoModel = Session.getCurrentDeclaracao().getDeclaracaoModel();
        Session.setEditable(true);
        FormKey selectedAnexo = declaracaoPresModel.getSelectedAnexo();
        String selectedQuadro = declaracaoPresModel.getSelectedQuadro();
        ((Modelo3IRSv2015Model)declaracaoModel).reload();
        declaracaoPresModel.setSelectedAnexo(selectedAnexo);
        declaracaoPresModel.setSelectedQuadro(selectedQuadro);
        for (Component component : disabledComponents) {
            if (component instanceof JTextComponent) {
                ((JTextComponent)component).setEditable(true);
                continue;
            }
            if (component instanceof JLabel || component instanceof JScrollBar) continue;
            component.setEnabled(true);
        }
        disabledComponents.clear();
        for (EnhancedAction action : disabledActions) {
            action.setEnabled(true);
        }
        disabledActions.clear();
    }

    private static void disableComponentTree(Container container) {
        int componentCount = container.getComponentCount();
        for (int i = 0; i < componentCount; ++i) {
            Component child = container.getComponent(i);
            if (child instanceof JTextComponent) {
                if (((JTextComponent)child).isEditable()) {
                    Modelo3IRSSessionDisableManager.disableTextField((JTextComponent)child);
                }
            } else if (!(child instanceof JLabel || child instanceof JScrollBar || !child.isEnabled())) {
                Modelo3IRSSessionDisableManager.disableComponent(child);
            }
            if (!(child instanceof Container)) continue;
            Modelo3IRSSessionDisableManager.disableComponentTree((Container)child);
        }
    }

    private static void disableTextField(JTextComponent textField) {
        textField.setEditable(false);
        disabledComponents.add(textField);
    }

    private static void disableComponent(Component component) {
        component.setEnabled(false);
        disabledComponents.add(component);
    }

    private static void disableAction(EnhancedAction action) {
        if (action != null) {
            action.setEnabled(false);
            disabledActions.add(action);
        }
    }
}

