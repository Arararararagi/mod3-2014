/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util;

import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;

public class DeclaracaoModelInitializer {
    public static Modelo3IRSv2015Model init() {
        Modelo3IRSv2015Model model = new Modelo3IRSv2015Model();
        RostoModel rostoModel = new RostoModel(new FormKey(RostoModel.class), true);
        model.addAnexo(rostoModel);
        return model;
    }
}

