/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util;

import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import pt.opensoft.taxclient.app.ErrorsParameters;

public class Modelo3IRSv2015ErrorsParameters
extends ErrorsParameters {
    public static Modelo3IRSv2015ErrorsParameters instance = null;

    public static synchronized Modelo3IRSv2015ErrorsParameters instance() {
        return instance;
    }

    public static void load(String parametersFileName) {
        InputStream paramInput = Modelo3IRSv2015ErrorsParameters.class.getResourceAsStream(parametersFileName);
        instance = new Modelo3IRSv2015ErrorsParameters(paramInput);
    }

    protected Modelo3IRSv2015ErrorsParameters(InputStream paramInput) {
        this.read(paramInput);
    }

    public Set<Object> getKeys() {
        return this.values.keySet();
    }
}

