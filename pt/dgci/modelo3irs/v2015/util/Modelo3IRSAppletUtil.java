/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util;

import java.io.StringReader;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.WebServicesUtil;
import pt.opensoft.util.StringUtil;

public class Modelo3IRSAppletUtil {
    public static void loadDeclFromAppletParameter(DeclaracaoModel declaracaoModel) {
        Modelo3IRSAppletUtil.loadDeclFromAppletParameter(declaracaoModel, false);
    }

    public static void loadDeclFromAppletParameter(DeclaracaoModel declaracaoModel, boolean serverSide) {
        String encodedDecl;
        String string = encodedDecl = !StringUtil.isEmpty(Session.getApplet().getParameter("decl")) ? Session.getApplet().getParameter("decl") : Session.getApplet().getParameter("Impressos");
        if (!StringUtil.isEmpty(encodedDecl)) {
            try {
                String decl = WebServicesUtil.getZipDecodedData(encodedDecl);
                declaracaoModel.resetModel();
                DeclarationReader.read(new StringReader(decl), declaracaoModel, false, serverSide);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}

