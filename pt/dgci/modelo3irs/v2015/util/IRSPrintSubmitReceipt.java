/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import pt.dgci.modelo3irs.v2015.actions.declaracao.IRSSubmitAction;
import pt.opensoft.swing.BrowserControl;
import pt.opensoft.swing.Util;
import pt.opensoft.taxclient.http.SubmitReceipt;
import pt.opensoft.taxclient.http.SubmitUser;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.SimpleLog;

public class IRSPrintSubmitReceipt {
    private static final String IMAGE_FILE_NAME = "Logo_DE.png";
    private static final String TMP_FILE_NAME = "BigLogo_DE.jpeg";
    private static final String PATH = "/ajuda/printReceipt.htm";

    public static void print(IRSSubmitAction submitAction, SubmitReceipt submitReceipt) {
        if (submitReceipt == null) {
            throw new IllegalArgumentException("SubmitReceipt to print is null!");
        }
        try {
            File tempFile = File.createTempFile("printComp", ".html", new File(System.getProperty("user.home")));
            tempFile.deleteOnExit();
            FileWriter fWriter = new FileWriter(tempFile);
            try {
                fWriter.write(IRSPrintSubmitReceipt.generateHtmlFromReceipt(submitAction, submitReceipt));
            }
            finally {
                fWriter.close();
            }
            BrowserControl.displayURL("file:///" + tempFile.getCanonicalPath());
        }
        catch (Exception execp) {
            SimpleLog.log("Error creating tmp html file to print the submit receipt. ", execp);
        }
    }

    private static String generateHtmlFromReceipt(IRSSubmitAction submitAction, SubmitReceipt submitReceipt) {
        String html;
        try {
            html = Util.getHtmlText("/ajuda/printReceipt.htm");
            for (SubmitUser submitUser : submitReceipt.getUsers()) {
                html = html.replace((CharSequence)("#" + submitUser.getId() + "#"), (CharSequence)submitUser.getNif());
                html = html.replace((CharSequence)("<!--" + submitUser.getId()), (CharSequence)"");
                html = html.replace((CharSequence)(submitUser.getId() + "-->"), (CharSequence)"");
            }
            html = html.replace((CharSequence)"#AnoExercicio#", (CharSequence)submitReceipt.getYear());
            html = html.replace((CharSequence)"#IdentDecl#", (CharSequence)submitReceipt.getDeclId());
            html = html.replace((CharSequence)"#DataRecep#", (CharSequence)submitReceipt.getDate().formatDateTime());
        }
        catch (IOException e) {
            throw new RuntimeException("N\u00e3o foi poss\u00edvel gerar a impress\u00e3o", e);
        }
        return html.toString();
    }
}

