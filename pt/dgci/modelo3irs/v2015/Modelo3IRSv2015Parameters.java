/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015;

import java.io.InputStream;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.SimpleParameters;
import pt.opensoft.util.SystemParameters;

public class Modelo3IRSv2015Parameters
extends SimpleParameters {
    public static final int FASE1 = 1;
    public static final int FASE2 = 2;
    public static final String PROPERTIES_FILENAME = "Modelo3IRSv2015.properties";
    public static final String PROPERTIES_FILENAME_DEV = "Modelo3IRSv2015.developer.properties";
    public static final String AJUDA_TEMAS = "Ajuda por Temas";
    public static final String ABRIR_AJUDA_PF = "AbrirPF";
    public static final String URL_PF = "http://www.portaldasfinancas.gov.pt/";
    public static final String APP_VERSION = "app.version";
    public static final String PROP_APP_NAME = "app.name";
    public static final String APP_DATE = "app.date";
    public static final String APP_NAME = "Modelo 3 - IRS";
    public static final String APP_ID = "app.id";
    public static final String DADOS_VERSAO = "Dados da Versao";
    public static final String FLATFILE_FOLDER = "flatfile.folder";
    public static final String DECL = "decl";
    public static final String DECLID = "declId";
    public static final String DECLSTATUS = "declStatus";
    public static final String NOME_SUJA = "nomeSujA";
    public static final String NOME_SUJB = "nomeSujB";
    public static final String SF = "sf";
    public static final String ISPRODUCTION = "isProductionEnv";
    public static final String NIF_TOC = "nifTOC";
    public static final String IRS_FASE2 = "irs.fase2";
    public static final String IS_SUBSTITUIR_ENABLED = "isSubstituirEnabled";
    public static final String IS_PP_CONVERSAO_IMPOSSIVEL = "isPPConversaoImpossivel";
    public static final String APPLET_OFFLINE = "appletOffline";
    public static Boolean fase2AppletLoaded = null;
    public static boolean isAppletOffline = false;
    public static final String NIF_A = "nifA";
    public static final String NIF_B = "nifB";
    public static final String REP_FIN = "repFin";
    public static final String ANO_EXERCICIO = "anoExercicio";
    public static final String IMPRESSOS = "Impressos";
    public static final String MOTIVO_PREENCHIMENTO = "motivo";
    public static final String DF_BASE = "df";
    public static final String LOTE_BASE = "lote";
    public static final String NUM_DECL_BASE = "numDecl";
    public static final String DATA_ANALISE = "dataAnalise";
    public static final String READ_WRITE = "ReadWrite";
    public static final String DECL_ORIGINAL = "declModelOriginal";
    private boolean isFase2;
    public static Modelo3IRSv2015Parameters instance = null;

    public static synchronized Modelo3IRSv2015Parameters instance() {
        return instance;
    }

    public static void load(String parametersFileName) {
        InputStream paramInput = Modelo3IRSv2015Parameters.class.getResourceAsStream(parametersFileName);
        instance = new Modelo3IRSv2015Parameters(paramInput);
        instance.setFase2(instance.getString("irs.fase2", "false").equals("true"));
    }

    public Modelo3IRSv2015Parameters(InputStream paramInput) {
        this.isFase2 = this.getString("irs.fase2", "false").equals("true");
        this.read(paramInput);
    }

    public void add(String parametersFileName) {
        this.read(Modelo3IRSv2015Parameters.class.getResourceAsStream(parametersFileName));
        instance.setFase2(instance.getString("irs.fase2", "false").equals("true"));
    }

    public void add(InputStream parametersInputStream) {
        this.read(parametersInputStream);
        instance.setFase2(instance.getString("irs.fase2", "false").equals("true"));
    }

    public String getDefaultLocaleName() {
        return this.getString("default.locale", SystemParameters.DEFAULT_LOCALE_NAME);
    }

    public String getAppVersion() {
        return this.getString("app.version");
    }

    public String getAppBuildVersion() {
        return this.getString("app.build.version");
    }

    public String getAppInternalVersion() {
        return this.getString("app.internal.version", "v2015");
    }

    public String getAppVersionDate() {
        return this.getString("app.date");
    }

    public String getAppName() {
        return this.getString("app.name", "Modelo 3 - IRS");
    }

    public String getUrlSubmit() {
        return TaxclientParameters.isProduction() ? this.getUrlSubmitPrd() : (TaxclientParameters.isQuality() ? this.getUrlSubmitQua() : this.getUrlSubmitDev());
    }

    public String getUrlSubmitAppletOffline() {
        return TaxclientParameters.isProduction() ? this.getUrlSubmitAppletOfflinePrd() : (TaxclientParameters.isQuality() ? this.getUrlSubmitAppletOfflineQua() : this.getUrlSubmitAppletOfflineDev());
    }

    public String getUrlTOCSubmit() {
        return TaxclientParameters.isProduction() ? this.getUrlTOCSubmitPrd() : (TaxclientParameters.isQuality() ? this.getUrlTOCSubmitQua() : this.getUrlTOCSubmitDev());
    }

    public String getUrlTOCSubmitAppletOffline() {
        return TaxclientParameters.isProduction() ? this.getUrlTOCSubmitAppletOfflinePrd() : (TaxclientParameters.isQuality() ? this.getUrlTOCSubmitAppletOfflineQua() : this.getUrlTOCSubmitAppletOfflineDev());
    }

    public String getUrlSubmitApplet() {
        return TaxclientParameters.isProduction() ? this.getUrlSubmitAppletPrd() : (TaxclientParameters.isQuality() ? this.getUrlSubmitAppletQua() : this.getUrlSubmitAppletDev());
    }

    public String getUrlTOCSubmitApplet() {
        return TaxclientParameters.isProduction() ? this.getUrlTOCSubmitAppletPrd() : (TaxclientParameters.isQuality() ? this.getUrlTOCSubmitAppletQua() : this.getUrlTOCSubmitAppletDev());
    }

    public String getUrlPrePreenchimento() {
        return TaxclientParameters.isProduction() ? this.getUrlPrePreenchimentoPrd() : (TaxclientParameters.isQuality() ? this.getUrlPrePreenchimentoQua() : this.getUrlPrePreenchimentoDev());
    }

    public String getUrlUltimaDeclSubmetida() {
        return TaxclientParameters.isProduction() ? this.getUrlObterUltimaDeclSubmetidaPrd() : (TaxclientParameters.isQuality() ? this.getUrlObterUltimaDeclSubmetidaQua() : this.getUrlObterUltimaDeclSubmetidaDev());
    }

    public String getUrlPrePreenchimentoPrd() {
        return this.getString("url.prePreenchimento.prd");
    }

    public String getUrlObterUltimaDeclSubmetidaPrd() {
        return this.getString("url.obterUltimaDeclSubmetida.prd");
    }

    public String getUrlSubmitPrd() {
        return this.getString("url.submit.prd");
    }

    public String getUrlSubmitAppletOfflinePrd() {
        return this.getString("url.submit.prd.appletoffline");
    }

    public String getUrlTOCSubmitPrd() {
        return this.getString("url.toc.submit.prd");
    }

    public String getUrlTOCSubmitAppletOfflinePrd() {
        return this.getString("url.toc.submit.prd.appletoffline");
    }

    public String getUrlSubmitAppletPrd() {
        return this.getString("url.submit.prd.applet");
    }

    public String getUrlTOCSubmitAppletPrd() {
        return this.getString("url.toc.submit.prd.applet");
    }

    public String getUrlPrePreenchimentoQua() {
        return this.getString("url.prePreenchimento.qua");
    }

    public String getUrlObterUltimaDeclSubmetidaQua() {
        return this.getString("url.obterUltimaDeclSubmetida.qua");
    }

    public String getUrlSubmitQua() {
        return this.getString("url.submit.qua");
    }

    public String getUrlSubmitAppletOfflineQua() {
        return this.getString("url.submit.qua.appletoffline");
    }

    public String getUrlTOCSubmitQua() {
        return this.getString("url.toc.submit.qua");
    }

    public String getUrlTOCSubmitAppletOfflineQua() {
        return this.getString("url.toc.submit.qua.appletoffline");
    }

    public String getUrlSubmitAppletQua() {
        return this.getString("url.submit.qua.applet");
    }

    public String getUrlTOCSubmitAppletQua() {
        return this.getString("url.toc.submit.qua.applet");
    }

    public String getUrlPrePreenchimentoDev() {
        return this.getString("url.prePreenchimento.dev");
    }

    public String getUrlObterUltimaDeclSubmetidaDev() {
        return this.getString("url.obterUltimaDeclSubmetida.dev");
    }

    public String getUrlSubmitAppletDev() {
        return this.getString("url.submit.dev.applet");
    }

    public String getUrlTOCSubmitAppletDev() {
        return this.getString("url.toc.submit.dev.applet");
    }

    public String getUrlSubmitDev() {
        return this.getString("url.submit.dev");
    }

    public String getUrlSubmitAppletOfflineDev() {
        return this.getString("url.submit.dev.appletoffline");
    }

    public String getUrlTOCSubmitDev() {
        return this.getString("url.toc.submit.dev");
    }

    public String getUrlTOCSubmitAppletOfflineDev() {
        return this.getString("url.toc.submit.dev.appletoffline");
    }

    public boolean isPrePreenchimentoActive() {
        return this.getString("prePreenchimento.action.active", "true").equals("true");
    }

    public boolean isFase2() {
        return fase2AppletLoaded != null ? fase2AppletLoaded : this.isFase2;
    }

    public int getFase() {
        return this.isFase2() ? 2 : 1;
    }

    public void setFase2(boolean isFase2) {
        this.isFase2 = isFase2;
    }

    public boolean IsSimulatePossible() {
        return this.getString("simulate.action.IsPossible", "true").equals("true");
    }

    public boolean IsSimulatePossibleForFase2() {
        return this.getString("simulate.action.IsPossible.Fase2", "true").equals("true");
    }

    public boolean isDC() {
        return this.getString("app.id", "NET").equals("DC");
    }

    public boolean isDpapelIRS() {
        return this.getString("app.id", "NET").equals("DPapel");
    }

    public boolean isNET() {
        return !this.isDC();
    }

    public boolean isAppletOffline() {
        return isAppletOffline;
    }

    public boolean setAppletOffline(boolean isAppletOffline) {
        Modelo3IRSv2015Parameters.isAppletOffline = isAppletOffline;
        return Modelo3IRSv2015Parameters.isAppletOffline;
    }

    public String getFlatfileFolder() {
        return this.getString("flatfile.folder", "/flatfile/irs/v2015/");
    }

    public String getMinDataCaeV3() {
        return this.getString("min.data.cae.v3", "2009-01-01 00:00:00");
    }

    public String getTestSimHt() {
        return this.getString("test.simulador.ht.file");
    }

    public boolean isAdminActionsEnabled() {
        return System.getProperty("admin") != null;
    }

    public boolean isAutomaticPasswordFillingEnabled() {
        return !TaxclientParameters.isProduction() && this.getString("automatic.password.filling.enabled", "false").equals("true");
    }

    public static enum DECL_STATUS {
        COM_PRE_PREENCHIMENTO("Z", "Com pr\u00e9-preenchimento"),
        INIT("0", "Iniciada"),
        EM_PREENCH("1", "Em preenchimento"),
        PRE_SUBMETIDA("P", "Pr\u00e9-submetida"),
        SUBMETIDA("2", "Submetida"),
        ACEITE("3", "Aceite"),
        COM_ERROS("4", "Com erros"),
        ENVIADA("5", "Submetida"),
        ANULADA("9", "Anulada"),
        STATUS_NULL(null, "Nova");
        
        private String status;
        private String descr;

        private DECL_STATUS(String status, String descr) {
            this.status = status;
            this.descr = descr;
        }

        public String getStatus() {
            return this.status;
        }

        public String getDescr() {
            return this.descr;
        }

        public boolean equalsDeclStatus(String status) {
            return this.status == null && status == null || this.status != null && status != null && this.status.equals(status);
        }

        public static String getDescr(String status) {
            for (DECL_STATUS declStatusEnum : DECL_STATUS.values()) {
                if (!declStatusEnum.equalsDeclStatus(status)) continue;
                return declStatusEnum.getDescr();
            }
            return "";
        }
    }

}

