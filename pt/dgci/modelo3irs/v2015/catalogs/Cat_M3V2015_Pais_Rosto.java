/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_RostoBase;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_Pais_Rosto
extends Cat_M3V2015_Pais_RostoBase
implements ICatalogItem {
    public Cat_M3V2015_Pais_Rosto(Long codigo, String descricao) {
        super(codigo, descricao);
    }

    public Cat_M3V2015_Pais_Rosto() {
    }

    @Override
    public String getDescription() {
        return super.getCodigo() == null ? " " : super.getCodigo() + " - " + super.getDescricao();
    }

    @Override
    public Long getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

