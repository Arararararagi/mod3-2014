/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.opensoft.taxclient.persistence.Type;

public class Cat_M3V2015_Pais_AnxHBase {
    private Long codigo;
    private String descricao;

    public Cat_M3V2015_Pais_AnxHBase() {
    }

    public Cat_M3V2015_Pais_AnxHBase(Long codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Long getCodigo() {
        return this.codigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}

