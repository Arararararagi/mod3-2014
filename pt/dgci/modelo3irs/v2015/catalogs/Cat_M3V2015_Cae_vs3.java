/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Cae_vs3Base;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_Cae_vs3
extends Cat_M3V2015_Cae_vs3Base
implements ICatalogItem {
    public static final int CAE_SIZE = 5;

    public Cat_M3V2015_Cae_vs3(String codigo, String descricao) {
        super(codigo, descricao);
    }

    @Override
    public String getDescription() {
        return super.getDescricao();
    }

    public Cat_M3V2015_Cae_vs3() {
    }

    @Override
    public String getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

