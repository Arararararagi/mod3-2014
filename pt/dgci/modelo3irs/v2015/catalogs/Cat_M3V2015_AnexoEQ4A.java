/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_AnexoEQ4ABase;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_AnexoEQ4A
extends Cat_M3V2015_AnexoEQ4ABase
implements ICatalogItem {
    public Cat_M3V2015_AnexoEQ4A(String codigo, String descricao) {
        super(codigo, descricao);
    }

    @Override
    public String getDescription() {
        return super.getDescricao();
    }

    public Cat_M3V2015_AnexoEQ4A() {
    }

    @Override
    public String getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

