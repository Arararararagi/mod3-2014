/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_PermanenteArrendadaBase;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_PermanenteArrendada
extends Cat_M3V2015_PermanenteArrendadaBase
implements ICatalogItem {
    public Cat_M3V2015_PermanenteArrendada(String codigo, String descricao) {
        super(codigo, descricao);
    }

    @Override
    public String getDescription() {
        return super.getDescricao();
    }

    public Cat_M3V2015_PermanenteArrendada() {
    }

    @Override
    public String getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

