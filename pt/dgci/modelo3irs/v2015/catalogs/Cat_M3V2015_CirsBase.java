/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.opensoft.taxclient.persistence.Type;

public class Cat_M3V2015_CirsBase {
    private String codigo;
    private String descricao;

    public Cat_M3V2015_CirsBase() {
    }

    public Cat_M3V2015_CirsBase(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public String getCodigo() {
        return this.codigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}

