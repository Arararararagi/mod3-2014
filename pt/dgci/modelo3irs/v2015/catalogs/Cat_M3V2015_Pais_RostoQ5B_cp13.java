/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_Pais_RostoQ5B_cp13Base;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_Pais_RostoQ5B_cp13
extends Cat_M3V2015_Pais_RostoQ5B_cp13Base
implements ICatalogItem {
    public Cat_M3V2015_Pais_RostoQ5B_cp13(Long codigo, String descricao) {
        super(codigo, descricao);
    }

    @Override
    public String getDescription() {
        return super.getCodigo() == null ? " " : super.getCodigo() + " - " + super.getDescricao();
    }

    public Cat_M3V2015_Pais_RostoQ5B_cp13() {
    }

    @Override
    public Long getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

