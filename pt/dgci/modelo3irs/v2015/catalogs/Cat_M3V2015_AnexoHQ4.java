/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_AnexoHQ4Base;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_AnexoHQ4
extends Cat_M3V2015_AnexoHQ4Base
implements ICatalogItem {
    public Cat_M3V2015_AnexoHQ4(Long codigo, String descricao) {
        super(codigo, descricao);
    }

    @Override
    public String getDescription() {
        return super.getDescricao();
    }

    public Cat_M3V2015_AnexoHQ4() {
    }

    @Override
    public Long getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

