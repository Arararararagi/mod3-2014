/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_SimNaoBase;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_SimNao
extends Cat_M3V2015_SimNaoBase
implements ICatalogItem {
    public Cat_M3V2015_SimNao(String codigo, String descricao) {
        super(codigo, descricao);
    }

    @Override
    public String getDescription() {
        return super.getDescricao();
    }

    public Cat_M3V2015_SimNao() {
    }

    @Override
    public String getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

