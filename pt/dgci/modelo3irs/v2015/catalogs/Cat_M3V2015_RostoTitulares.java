/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.util.StringUtil;

public class Cat_M3V2015_RostoTitulares
implements ICatalogItem {
    private Long nif;
    private String innerValue;

    public Cat_M3V2015_RostoTitulares(Long nif, String innerValue) {
        this.nif = nif;
        this.innerValue = innerValue;
    }

    @Override
    public String getDescription() {
        String desc = this.innerValue != null && this.innerValue.equals("C") ? "Sujeitos Passivos A e B" : (this.innerValue != null && this.innerValue.equals("X") ? "Valor Inv\u00e1lido" : String.valueOf(this.nif));
        return this.innerValue == null || StringUtil.isEmpty(this.innerValue, true) ? " " : this.innerValue + " - " + desc;
    }

    @Override
    public Object getValue() {
        return this.innerValue;
    }

    public String toString() {
        return this.getDescription();
    }
}

