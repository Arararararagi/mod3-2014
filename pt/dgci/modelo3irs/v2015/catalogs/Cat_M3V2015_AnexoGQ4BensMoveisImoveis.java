/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.catalogs;

import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_AnexoGQ4BensMoveisImoveisBase;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class Cat_M3V2015_AnexoGQ4BensMoveisImoveis
extends Cat_M3V2015_AnexoGQ4BensMoveisImoveisBase
implements ICatalogItem {
    public Cat_M3V2015_AnexoGQ4BensMoveisImoveis(String codigo, String descricao) {
        super(codigo, descricao);
    }

    @Override
    public String getDescription() {
        return super.getDescricao();
    }

    public Cat_M3V2015_AnexoGQ4BensMoveisImoveis() {
    }

    @Override
    public String getValue() {
        return super.getCodigo();
    }

    public String toString() {
        return this.getDescription();
    }
}

