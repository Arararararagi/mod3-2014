/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_LinhaBase;

public class AnexoJq05T2_Linha
extends AnexoJq05T2_LinhaBase {
    public AnexoJq05T2_Linha() {
    }

    public AnexoJq05T2_Linha(String outrosNumIdentificacao) {
        super(outrosNumIdentificacao);
    }

    public static String getLink(int line) {
        return "aAnexoJ.qQuadro05.tanexoJq05T2.l" + line;
    }
}

