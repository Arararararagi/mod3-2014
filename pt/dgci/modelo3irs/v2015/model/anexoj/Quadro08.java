/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.EventList;
import java.util.HashSet;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro08Base;

public class Quadro08
extends Quadro08Base {
    public long getTotalRetencoesIRSPreenchido() {
        long totalRetencoesIRSPreenchido = 0;
        for (AnexoJq08T1_Linha anexoJq08T1Linha : this.getAnexoJq08T1()) {
            if (anexoJq08T1Linha.getRetencoesIRS() == null) continue;
            totalRetencoesIRSPreenchido+=anexoJq08T1Linha.getRetencoesIRS().longValue();
        }
        return totalRetencoesIRSPreenchido;
    }

    public Set<Long> getDistinctCodigosCamposQ4() {
        HashSet<Long> result = new HashSet<Long>();
        for (AnexoJq08T1_Linha anexoJq08T1Linha : this.getAnexoJq08T1()) {
            if (anexoJq08T1Linha.getCampoQ4() == null) continue;
            result.add(anexoJq08T1Linha.getCampoQ4());
        }
        return result;
    }

    public long getTotalRetencoesIRSPreenchidoByCodigoCampoQ4(long campoQ4) {
        long totalRetencoesIRSPreenchidoByCampoQ4 = 0;
        for (AnexoJq08T1_Linha anexoJq08T1Linha : this.getAnexoJq08T1()) {
            if (anexoJq08T1Linha.getCampoQ4() != null && anexoJq08T1Linha.getCampoQ4() == campoQ4 && anexoJq08T1Linha.getRetencoesIRS() != null) {
                totalRetencoesIRSPreenchidoByCampoQ4+=anexoJq08T1Linha.getRetencoesIRS().longValue();
            }
            if (anexoJq08T1Linha.getCampoQ4() == null || anexoJq08T1Linha.getCampoQ4() != campoQ4 || anexoJq08T1Linha.getRetencaoSobretaxa() == null) continue;
            totalRetencoesIRSPreenchidoByCampoQ4+=anexoJq08T1Linha.getRetencaoSobretaxa().longValue();
        }
        return totalRetencoesIRSPreenchidoByCampoQ4;
    }

    public long getTotalRendimentosIRSPreenchidoByCodigoCampoQ4(long campoQ4) {
        long totalRendimentosIRSPreenchidoByCampoQ4 = 0;
        for (AnexoJq08T1_Linha anexoJq08T1Linha : this.getAnexoJq08T1()) {
            if (anexoJq08T1Linha.getCampoQ4() == null || anexoJq08T1Linha.getCampoQ4() != campoQ4 || anexoJq08T1Linha.getRendimentos() == null) continue;
            totalRendimentosIRSPreenchidoByCampoQ4+=anexoJq08T1Linha.getRendimentos().longValue();
        }
        return totalRendimentosIRSPreenchidoByCampoQ4;
    }

    public boolean isEmpty() {
        return this.getAnexoJq08T1().isEmpty();
    }
}

