/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq08T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro08Base
extends QuadroModel {
    public static final String QUADRO08_LINK = "aAnexoJ.qQuadro08";
    public static final String ANEXOJQ08T1_LINK = "aAnexoJ.qQuadro08.tanexoJq08T1";
    private EventList<AnexoJq08T1_Linha> anexoJq08T1 = new BasicEventList<AnexoJq08T1_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoJq08T1_Linha> getAnexoJq08T1() {
        return this.anexoJq08T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoJq08T1(EventList<AnexoJq08T1_Linha> anexoJq08T1) {
        this.anexoJq08T1 = anexoJq08T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoJq08T1);
        return result;
    }
}

