/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_LinhaBase;

public class AnexoJq07T1_Linha
extends AnexoJq07T1_LinhaBase {
    public AnexoJq07T1_Linha() {
    }

    public AnexoJq07T1_Linha(Long campoQ4, Long rendimento, Long nanos) {
        super(campoQ4, rendimento, nanos);
    }

    public static String getLink(int numLinha) {
        return "aAnexoJ.qQuadro07.tanexoJq07T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoJq07T1_LinhaBase.Property column) {
        return "aAnexoJ.qQuadro07.tanexoJq07T1.l" + numLinha + ".c" + column.getIndex();
    }
}

