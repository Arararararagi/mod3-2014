/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq08T1_LinhaBase;

public class AnexoJq08T1_Linha
extends AnexoJq08T1_LinhaBase {
    public AnexoJq08T1_Linha() {
    }

    public AnexoJq08T1_Linha(Long nifEntidadeRetentora, Long campoQ4, Long rendimentos, Long retencoesIRS, Long retencaoSobretaxa) {
        super(nifEntidadeRetentora, campoQ4, rendimentos, retencoesIRS, retencaoSobretaxa);
    }

    public static String getLink(int numLinha) {
        return "aAnexoJ.qQuadro08.tanexoJq08T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoJq08T1_LinhaBase.Property column) {
        return "aAnexoJ.qQuadro08.tanexoJq08T1.l" + numLinha + ".c" + column.getIndex();
    }
}

