/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoJq04T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoJq04T1_Linha>,
WritableTableFormat<AnexoJq04T1_Linha> {
    @Override
    public boolean isEditable(AnexoJq04T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoJq04T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getAnoRealizacao();
            }
            case 2: {
                return line.getMesRealizacao();
            }
            case 3: {
                return line.getValorRealizacao();
            }
            case 4: {
                return line.getAnoAquisicao();
            }
            case 5: {
                return line.getMesAquisicao();
            }
            case 6: {
                return line.getValorAquisicao();
            }
            case 7: {
                return line.getDespesasEncargos();
            }
            case 8: {
                return line.getImpostoPagoNoEstrangeiro();
            }
            case 9: {
                if (line == null || line.getCodigoDoPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxJ", line.getCodigoDoPais());
            }
        }
        return null;
    }

    @Override
    public AnexoJq04T1_Linha setColumnValue(AnexoJq04T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setAnoRealizacao((Long)value);
                return line;
            }
            case 2: {
                line.setMesRealizacao((Long)value);
                return line;
            }
            case 3: {
                line.setValorRealizacao((Long)value);
                return line;
            }
            case 4: {
                line.setAnoAquisicao((Long)value);
                return line;
            }
            case 5: {
                line.setMesAquisicao((Long)value);
                return line;
            }
            case 6: {
                line.setValorAquisicao((Long)value);
                return line;
            }
            case 7: {
                line.setDespesasEncargos((Long)value);
                return line;
            }
            case 8: {
                line.setImpostoPagoNoEstrangeiro((Long)value);
                return line;
            }
            case 9: {
                if (value != null) {
                    line.setCodigoDoPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 10;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Ano";
            }
            case 2: {
                return "M\u00eas";
            }
            case 3: {
                return "Valor";
            }
            case 4: {
                return "Ano";
            }
            case 5: {
                return "M\u00eas";
            }
            case 6: {
                return "Valor";
            }
            case 7: {
                return "Despesas e Encargos";
            }
            case 8: {
                return "Imposto pago no Estrangeiro";
            }
            case 9: {
                return "C\u00f3digo do Pa\u00eds";
            }
        }
        return null;
    }
}

