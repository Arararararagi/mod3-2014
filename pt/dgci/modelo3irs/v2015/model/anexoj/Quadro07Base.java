/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoJ.qQuadro07";
    public static final String ANEXOJQ07T1_LINK = "aAnexoJ.qQuadro07.tanexoJq07T1";
    private EventList<AnexoJq07T1_Linha> anexoJq07T1 = new BasicEventList<AnexoJq07T1_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoJq07T1_Linha> getAnexoJq07T1() {
        return this.anexoJq07T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoJq07T1(EventList<AnexoJq07T1_Linha> anexoJq07T1) {
        this.anexoJq07T1 = anexoJq07T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoJq07T1);
        return result;
    }
}

