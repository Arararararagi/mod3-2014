/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoJq05T1_LinhaBase
extends Model {
    public static final String IBAN_LINK = "aAnexoJ.qQuadro05.fiban";
    public static final String IBAN = "iban";
    private String iban;
    public static final String BIC_LINK = "aAnexoJ.qQuadro05.fbic";
    public static final String BIC = "bic";
    private String bic;

    public AnexoJq05T1_LinhaBase(String iban, String bic) {
        this.iban = iban;
        this.bic = bic;
    }

    public AnexoJq05T1_LinhaBase() {
        this.iban = null;
        this.bic = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getIban() {
        return this.iban;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setIban(String iban) {
        String oldValue = this.iban;
        this.iban = iban;
        this.firePropertyChange("iban", oldValue, this.iban);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getBic() {
        return this.bic;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setBic(String bic) {
        String oldValue = this.bic;
        this.bic = bic;
        this.firePropertyChange("bic", oldValue, this.bic);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.iban);
        result = HashCodeUtil.hash(result, this.bic);
        return result;
    }

    public static enum Property {
        IBAN("iban", 2),
        BIC("bic", 3);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

