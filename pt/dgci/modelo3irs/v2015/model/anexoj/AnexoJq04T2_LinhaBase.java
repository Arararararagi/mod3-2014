/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoJq04T2_LinhaBase
extends Model {
    public static final String CODIGOS_LINK = "aAnexoJ.qQuadro04.fcodigos";
    public static final String CODIGOS = "codigos";
    private Long codigos;
    public static final String ANOREALIZACAO_LINK = "aAnexoJ.qQuadro04.fanoRealizacao";
    public static final String ANOREALIZACAO = "anoRealizacao";
    private Long anoRealizacao;
    public static final String MESREALIZACAO_LINK = "aAnexoJ.qQuadro04.fmesRealizacao";
    public static final String MESREALIZACAO = "mesRealizacao";
    private Long mesRealizacao;
    public static final String VALORREALIZACAO_LINK = "aAnexoJ.qQuadro04.fvalorRealizacao";
    public static final String VALORREALIZACAO = "valorRealizacao";
    private Long valorRealizacao;
    public static final String ANOAQUISICAO_LINK = "aAnexoJ.qQuadro04.fanoAquisicao";
    public static final String ANOAQUISICAO = "anoAquisicao";
    private Long anoAquisicao;
    public static final String MESAQUISICAO_LINK = "aAnexoJ.qQuadro04.fmesAquisicao";
    public static final String MESAQUISICAO = "mesAquisicao";
    private Long mesAquisicao;
    public static final String VALORAQUISICAO_LINK = "aAnexoJ.qQuadro04.fvalorAquisicao";
    public static final String VALORAQUISICAO = "valorAquisicao";
    private Long valorAquisicao;
    public static final String DESPESASENCARGOS_LINK = "aAnexoJ.qQuadro04.fdespesasEncargos";
    public static final String DESPESASENCARGOS = "despesasEncargos";
    private Long despesasEncargos;
    public static final String IMPOSTOPAGONOESTRANGEIRO_LINK = "aAnexoJ.qQuadro04.fimpostoPagoNoEstrangeiro";
    public static final String IMPOSTOPAGONOESTRANGEIRO = "impostoPagoNoEstrangeiro";
    private Long impostoPagoNoEstrangeiro;
    public static final String CODIGODOPAIS_LINK = "aAnexoJ.qQuadro04.fcodigoDoPais";
    public static final String CODIGODOPAIS = "codigoDoPais";
    private Long codigoDoPais;

    public AnexoJq04T2_LinhaBase(Long codigos, Long anoRealizacao, Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, Long despesasEncargos, Long impostoPagoNoEstrangeiro, Long codigoDoPais) {
        this.codigos = codigos;
        this.anoRealizacao = anoRealizacao;
        this.mesRealizacao = mesRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.anoAquisicao = anoAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.despesasEncargos = despesasEncargos;
        this.impostoPagoNoEstrangeiro = impostoPagoNoEstrangeiro;
        this.codigoDoPais = codigoDoPais;
    }

    public AnexoJq04T2_LinhaBase() {
        this.codigos = null;
        this.anoRealizacao = null;
        this.mesRealizacao = null;
        this.valorRealizacao = null;
        this.anoAquisicao = null;
        this.mesAquisicao = null;
        this.valorAquisicao = null;
        this.despesasEncargos = null;
        this.impostoPagoNoEstrangeiro = null;
        this.codigoDoPais = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoJQ4BCodigos")
    public Long getCodigos() {
        return this.codigos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodigos(Long codigos) {
        Long oldValue = this.codigos;
        this.codigos = codigos;
        this.firePropertyChange("codigos", oldValue, this.codigos);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoRealizacao() {
        return this.anoRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoRealizacao(Long anoRealizacao) {
        Long oldValue = this.anoRealizacao;
        this.anoRealizacao = anoRealizacao;
        this.firePropertyChange("anoRealizacao", oldValue, this.anoRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesRealizacao() {
        return this.mesRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesRealizacao(Long mesRealizacao) {
        Long oldValue = this.mesRealizacao;
        this.mesRealizacao = mesRealizacao;
        this.firePropertyChange("mesRealizacao", oldValue, this.mesRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorRealizacao() {
        return this.valorRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorRealizacao(Long valorRealizacao) {
        Long oldValue = this.valorRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.firePropertyChange("valorRealizacao", oldValue, this.valorRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoAquisicao() {
        return this.anoAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoAquisicao(Long anoAquisicao) {
        Long oldValue = this.anoAquisicao;
        this.anoAquisicao = anoAquisicao;
        this.firePropertyChange("anoAquisicao", oldValue, this.anoAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesAquisicao() {
        return this.mesAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesAquisicao(Long mesAquisicao) {
        Long oldValue = this.mesAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.firePropertyChange("mesAquisicao", oldValue, this.mesAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorAquisicao() {
        return this.valorAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorAquisicao(Long valorAquisicao) {
        Long oldValue = this.valorAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.firePropertyChange("valorAquisicao", oldValue, this.valorAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getDespesasEncargos() {
        return this.despesasEncargos;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setDespesasEncargos(Long despesasEncargos) {
        Long oldValue = this.despesasEncargos;
        this.despesasEncargos = despesasEncargos;
        this.firePropertyChange("despesasEncargos", oldValue, this.despesasEncargos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getImpostoPagoNoEstrangeiro() {
        return this.impostoPagoNoEstrangeiro;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setImpostoPagoNoEstrangeiro(Long impostoPagoNoEstrangeiro) {
        Long oldValue = this.impostoPagoNoEstrangeiro;
        this.impostoPagoNoEstrangeiro = impostoPagoNoEstrangeiro;
        this.firePropertyChange("impostoPagoNoEstrangeiro", oldValue, this.impostoPagoNoEstrangeiro);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxJ")
    public Long getCodigoDoPais() {
        return this.codigoDoPais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodigoDoPais(Long codigoDoPais) {
        Long oldValue = this.codigoDoPais;
        this.codigoDoPais = codigoDoPais;
        this.firePropertyChange("codigoDoPais", oldValue, this.codigoDoPais);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.codigos);
        result = HashCodeUtil.hash(result, this.anoRealizacao);
        result = HashCodeUtil.hash(result, this.mesRealizacao);
        result = HashCodeUtil.hash(result, this.valorRealizacao);
        result = HashCodeUtil.hash(result, this.anoAquisicao);
        result = HashCodeUtil.hash(result, this.mesAquisicao);
        result = HashCodeUtil.hash(result, this.valorAquisicao);
        result = HashCodeUtil.hash(result, this.despesasEncargos);
        result = HashCodeUtil.hash(result, this.impostoPagoNoEstrangeiro);
        result = HashCodeUtil.hash(result, this.codigoDoPais);
        return result;
    }

    public static enum Property {
        CODIGOS("codigos", 2),
        ANOREALIZACAO("anoRealizacao", 3),
        MESREALIZACAO("mesRealizacao", 4),
        VALORREALIZACAO("valorRealizacao", 5),
        ANOAQUISICAO("anoAquisicao", 6),
        MESAQUISICAO("mesAquisicao", 7),
        VALORAQUISICAO("valorAquisicao", 8),
        DESPESASENCARGOS("despesasEncargos", 9),
        IMPOSTOPAGONOESTRANGEIRO("impostoPagoNoEstrangeiro", 10),
        CODIGODOPAIS("codigoDoPais", 11);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

