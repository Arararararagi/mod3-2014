/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoJq06T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoJq06T1_Linha>,
WritableTableFormat<AnexoJq06T1_Linha> {
    @Override
    public boolean isEditable(AnexoJq06T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoJq06T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getNLinha() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoJQ6NLinha", line.getNLinha());
            }
            case 2: {
                if (line == null || line.getCampoQ4() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoJQ4", line.getCampoQ4());
            }
            case 3: {
                if (line == null || line.getInstalacaoFixa() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_SimNao", line.getInstalacaoFixa());
            }
            case 4: {
                if (line == null || line.getIdentificacaoPaisCodPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxJ", line.getIdentificacaoPaisCodPais());
            }
            case 5: {
                return line.getMontanteRendimento();
            }
            case 6: {
                return line.getPaisDaFonteValor();
            }
            case 7: {
                if (line == null || line.getPaisAgentePagadorCodPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxJ", line.getPaisAgentePagadorCodPais());
            }
            case 8: {
                return line.getPaisAgentePagadorValor();
            }
        }
        return null;
    }

    @Override
    public AnexoJq06T1_Linha setColumnValue(AnexoJq06T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setNLinha((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCampoQ4((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                if (value != null) {
                    line.setInstalacaoFixa((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 4: {
                if (value != null) {
                    line.setIdentificacaoPaisCodPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 5: {
                line.setMontanteRendimento((Long)value);
                return line;
            }
            case 6: {
                line.setPaisDaFonteValor((Long)value);
                return line;
            }
            case 7: {
                if (value != null) {
                    line.setPaisAgentePagadorCodPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 8: {
                line.setPaisAgentePagadorValor((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "N\u00ba de linha do quadro 6";
            }
            case 2: {
                return "N\u00famero do Campo do Quadro 4";
            }
            case 3: {
                return "Instala\u00e7\u00e3o Fixa";
            }
            case 4: {
                return "C\u00f3digo do Pa\u00eds";
            }
            case 5: {
                return "Montante do Rendimento";
            }
            case 6: {
                return "Valor";
            }
            case 7: {
                return "C\u00f3digo do Pa\u00eds";
            }
            case 8: {
                return "Valor";
            }
        }
        return null;
    }
}

