/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro08;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.util.HashCodeUtil;

@Min(value=0)
@Max(value=9)
public class AnexoJModelBase
extends AnexoModel {
    public static final String ANEXOJ_LINK = "aAnexoJ";

    public AnexoJModelBase(FormKey key, boolean addQuadrosToModel) {
        super(key);
        if (addQuadrosToModel) {
            this.addQuadro(new Quadro02());
            this.addQuadro(new Quadro03());
            this.addQuadro(new Quadro04());
            this.addQuadro(new Quadro05());
            this.addQuadro(new Quadro06());
            this.addQuadro(new Quadro07());
            this.addQuadro(new Quadro08());
        }
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro02.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro03.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro04.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro05.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro06.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro07.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro08.class.getSimpleName()));
        return result;
    }
}

