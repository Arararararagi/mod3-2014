/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq07T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoJq07T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoJq07T1_Linha>,
WritableTableFormat<AnexoJq07T1_Linha> {
    @Override
    public boolean isEditable(AnexoJq07T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoJq07T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCampoNLinhaQ6() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoJQ7LinhaQ6", line.getCampoNLinhaQ6());
            }
            case 2: {
                return line.getRendimento();
            }
            case 3: {
                return line.getNanos();
            }
        }
        return null;
    }

    @Override
    public AnexoJq07T1_Linha setColumnValue(AnexoJq07T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCampoNLinhaQ6((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setRendimento((Long)value);
                return line;
            }
            case 3: {
                line.setNanos((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "N\u00ba de linha do quadro 6";
            }
            case 2: {
                return "Rendimento";
            }
            case 3: {
                return "N.\u00ba Anos";
            }
        }
        return null;
    }
}

