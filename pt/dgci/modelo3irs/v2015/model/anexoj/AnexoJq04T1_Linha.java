/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T1_LinhaBase;

public class AnexoJq04T1_Linha
extends AnexoJq04T1_LinhaBase {
    public AnexoJq04T1_Linha() {
    }

    public AnexoJq04T1_Linha(Long anoRealizacao, Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, Long despesasEncargos, Long impostoPagoNoEstrangeiro, Long codigoDoPais) {
        super(anoRealizacao, mesRealizacao, valorRealizacao, anoAquisicao, mesAquisicao, valorAquisicao, despesasEncargos, impostoPagoNoEstrangeiro, codigoDoPais);
    }

    public static String getLink(int line) {
        return "aAnexoJ.qQuadro04.tanexoJq04T1.l" + line;
    }
}

