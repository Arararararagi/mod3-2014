/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.EventList;
import java.util.HashSet;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro06Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro06
extends Quadro06Base {
    public long getTotalMontanteRendimentoPreenchido() {
        long totalMontanteRendimentoPreenchido = 0;
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getMontanteRendimento() == null) continue;
            totalMontanteRendimentoPreenchido+=anexoJq06T1Linha.getMontanteRendimento().longValue();
        }
        return totalMontanteRendimentoPreenchido;
    }

    public long getTotalMontanteRendimentoPreenchidoByCampoQ6(long campoQ6) {
        long totalMontanteRendimentoPreenchido = 0;
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getMontanteRendimento() == null || anexoJq06T1Linha.getNLinha() == null || anexoJq06T1Linha.getNLinha() != campoQ6 || anexoJq06T1Linha.getCampoQ4() == null || !Modelo3IRSValidatorUtil.in(anexoJq06T1Linha.getCampoQ4(), new long[]{401, 402, 403, 406, 426})) continue;
            totalMontanteRendimentoPreenchido+=anexoJq06T1Linha.getMontanteRendimento().longValue();
        }
        return totalMontanteRendimentoPreenchido;
    }

    public long getTotalImpostoByCampoQ6(long campoQ6) {
        long totalImposto = 0;
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getNLinha() == null || anexoJq06T1Linha.getNLinha() != campoQ6 || anexoJq06T1Linha.getCampoQ4() == null || !Modelo3IRSValidatorUtil.in(anexoJq06T1Linha.getCampoQ4(), new long[]{401, 402, 403, 406, 426})) continue;
            if (anexoJq06T1Linha.getPaisDaFonteValor() != null) {
                totalImposto+=anexoJq06T1Linha.getPaisDaFonteValor().longValue();
            }
            if (anexoJq06T1Linha.getPaisAgentePagadorValor() == null) continue;
            totalImposto+=anexoJq06T1Linha.getPaisAgentePagadorValor().longValue();
        }
        return totalImposto;
    }

    public long getTotalImpostoEstrangeiroPreenchido() {
        long totalImpostoEstrangeiroPreenchido = 0;
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getPaisDaFonteValor() == null) continue;
            totalImpostoEstrangeiroPreenchido+=anexoJq06T1Linha.getPaisDaFonteValor().longValue();
        }
        return totalImpostoEstrangeiroPreenchido;
    }

    public long getTotalPaisAgentePagadorPreenchido() {
        long totalPaisAgentePagadorPreenchido = 0;
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getPaisAgentePagadorValor() == null) continue;
            totalPaisAgentePagadorPreenchido+=anexoJq06T1Linha.getPaisAgentePagadorValor().longValue();
        }
        return totalPaisAgentePagadorPreenchido;
    }

    public int getDistinctNumCodigosNatureza() {
        HashSet<Long> distinctCodigos = new HashSet<Long>();
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getCampoQ4() == null) continue;
            distinctCodigos.add(anexoJq06T1Linha.getCampoQ4());
        }
        return distinctCodigos.size();
    }

    public boolean existCampoQ6IdentificacaoPais(long campoQ6, long pais) {
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getNLinha() == null || anexoJq06T1Linha.getIdentificacaoPaisCodPais() == null || anexoJq06T1Linha.getCampoQ4() == null || !anexoJq06T1Linha.getNLinha().equals(campoQ6) || !anexoJq06T1Linha.getIdentificacaoPaisCodPais().equals(pais) || !Modelo3IRSValidatorUtil.in(anexoJq06T1Linha.getCampoQ4(), new long[]{401, 402, 403, 406, 426})) continue;
            return true;
        }
        return false;
    }

    public boolean existCampoQ6IdentificacaoPaisCodC4Invalido(long campoQ6, long pais) {
        for (AnexoJq06T1_Linha anexoJq06T1Linha : this.getAnexoJq06T1()) {
            if (anexoJq06T1Linha.getNLinha() == null || anexoJq06T1Linha.getIdentificacaoPaisCodPais() == null || anexoJq06T1Linha.getCampoQ4() == null || anexoJq06T1Linha.getNLinha() != campoQ6 || anexoJq06T1Linha.getIdentificacaoPaisCodPais() != pais || Modelo3IRSValidatorUtil.in(anexoJq06T1Linha.getCampoQ4(), new long[]{401, 402, 403, 406, 426})) continue;
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        return this.getAnexoJq06T1().isEmpty();
    }

    public void setAnexoJq06C1Formula_WithoutOverwriteBehavior() {
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoJq06T1().size(); ++i) {
            temporaryValue0+=this.getAnexoJq06T1().get(i).getMontanteRendimento() == null ? 0 : this.getAnexoJq06T1().get(i).getMontanteRendimento();
        }
        this.setAnexoJq06C1(newValue+=temporaryValue0);
    }

    public void setAnexoJq06C2Formula_WithoutOverwriteBehavior() {
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoJq06T1().size(); ++i) {
            temporaryValue0+=this.getAnexoJq06T1().get(i).getPaisDaFonteValor() == null ? 0 : this.getAnexoJq06T1().get(i).getPaisDaFonteValor();
        }
        this.setAnexoJq06C2(newValue+=temporaryValue0);
    }

    public void setAnexoJq06C3Formula_WithoutOverwriteBehavior() {
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoJq06T1().size(); ++i) {
            temporaryValue0+=this.getAnexoJq06T1().get(i).getPaisAgentePagadorValor() == null ? 0 : this.getAnexoJq06T1().get(i).getPaisAgentePagadorValor();
        }
        this.setAnexoJq06C3(newValue+=temporaryValue0);
    }
}

