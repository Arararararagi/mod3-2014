/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T2_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoJq04T2_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoJq04T2_Linha>,
WritableTableFormat<AnexoJq04T2_Linha> {
    @Override
    public boolean isEditable(AnexoJq04T2_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoJq04T2_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCodigos() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoJQ4BCodigos", line.getCodigos());
            }
            case 2: {
                return line.getAnoRealizacao();
            }
            case 3: {
                return line.getMesRealizacao();
            }
            case 4: {
                return line.getValorRealizacao();
            }
            case 5: {
                return line.getAnoAquisicao();
            }
            case 6: {
                return line.getMesAquisicao();
            }
            case 7: {
                return line.getValorAquisicao();
            }
            case 8: {
                return line.getDespesasEncargos();
            }
            case 9: {
                return line.getImpostoPagoNoEstrangeiro();
            }
            case 10: {
                if (line == null || line.getCodigoDoPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxJ", line.getCodigoDoPais());
            }
        }
        return null;
    }

    @Override
    public AnexoJq04T2_Linha setColumnValue(AnexoJq04T2_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCodigos((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setAnoRealizacao((Long)value);
                return line;
            }
            case 3: {
                line.setMesRealizacao((Long)value);
                return line;
            }
            case 4: {
                line.setValorRealizacao((Long)value);
                return line;
            }
            case 5: {
                line.setAnoAquisicao((Long)value);
                return line;
            }
            case 6: {
                line.setMesAquisicao((Long)value);
                return line;
            }
            case 7: {
                line.setValorAquisicao((Long)value);
                return line;
            }
            case 8: {
                line.setDespesasEncargos((Long)value);
                return line;
            }
            case 9: {
                line.setImpostoPagoNoEstrangeiro((Long)value);
                return line;
            }
            case 10: {
                if (value != null) {
                    line.setCodigoDoPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return Long.class;
            }
            case 10: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 11;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "C\u00f3digos";
            }
            case 2: {
                return "Ano";
            }
            case 3: {
                return "M\u00eas";
            }
            case 4: {
                return "Valor";
            }
            case 5: {
                return "Ano";
            }
            case 6: {
                return "M\u00eas";
            }
            case 7: {
                return "Valor";
            }
            case 8: {
                return "Despesas e Encargos";
            }
            case 9: {
                return "Imposto pago no Estrangeiro";
            }
            case 10: {
                return "C\u00f3digo do Pa\u00eds";
            }
        }
        return null;
    }
}

