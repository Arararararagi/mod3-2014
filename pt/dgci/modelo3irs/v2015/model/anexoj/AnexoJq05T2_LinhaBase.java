/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoJq05T2_LinhaBase
extends Model {
    public static final String OUTROSNUMIDENTIFICACAO_LINK = "aAnexoJ.qQuadro05.foutrosNumIdentificacao";
    public static final String OUTROSNUMIDENTIFICACAO = "outrosNumIdentificacao";
    private String outrosNumIdentificacao;

    public AnexoJq05T2_LinhaBase(String outrosNumIdentificacao) {
        this.outrosNumIdentificacao = outrosNumIdentificacao;
    }

    public AnexoJq05T2_LinhaBase() {
        this.outrosNumIdentificacao = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getOutrosNumIdentificacao() {
        return this.outrosNumIdentificacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setOutrosNumIdentificacao(String outrosNumIdentificacao) {
        String oldValue = this.outrosNumIdentificacao;
        this.outrosNumIdentificacao = outrosNumIdentificacao;
        this.firePropertyChange("outrosNumIdentificacao", oldValue, this.outrosNumIdentificacao);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.outrosNumIdentificacao);
        return result;
    }

    public static enum Property {
        OUTROSNUMIDENTIFICACAO("outrosNumIdentificacao", 2);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

