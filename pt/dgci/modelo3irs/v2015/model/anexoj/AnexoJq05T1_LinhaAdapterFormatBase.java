/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_Linha;

public class AnexoJq05T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoJq05T1_Linha>,
WritableTableFormat<AnexoJq05T1_Linha> {
    @Override
    public boolean isEditable(AnexoJq05T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoJq05T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getIban();
            }
            case 2: {
                return line.getBic();
            }
        }
        return null;
    }

    @Override
    public AnexoJq05T1_Linha setColumnValue(AnexoJq05T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setIban((String)value);
                return line;
            }
            case 2: {
                line.setBic((String)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
            case 2: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "IBAN";
            }
            case 2: {
                return "BIC";
            }
        }
        return null;
    }
}

