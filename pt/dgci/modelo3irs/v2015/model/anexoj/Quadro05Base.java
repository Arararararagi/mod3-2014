/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoJ.qQuadro05";
    public static final String ANEXOJQ05T1_LINK = "aAnexoJ.qQuadro05.tanexoJq05T1";
    private EventList<AnexoJq05T1_Linha> anexoJq05T1 = new BasicEventList<AnexoJq05T1_Linha>();
    public static final String ANEXOJQ05T2_LINK = "aAnexoJ.qQuadro05.tanexoJq05T2";
    private EventList<AnexoJq05T2_Linha> anexoJq05T2 = new BasicEventList<AnexoJq05T2_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoJq05T1_Linha> getAnexoJq05T1() {
        return this.anexoJq05T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoJq05T1(EventList<AnexoJq05T1_Linha> anexoJq05T1) {
        this.anexoJq05T1 = anexoJq05T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoJq05T2_Linha> getAnexoJq05T2() {
        return this.anexoJq05T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoJq05T2(EventList<AnexoJq05T2_Linha> anexoJq05T2) {
        this.anexoJq05T2 = anexoJq05T2;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoJq05T1);
        result = HashCodeUtil.hash(result, this.anexoJq05T2);
        return result;
    }
}

