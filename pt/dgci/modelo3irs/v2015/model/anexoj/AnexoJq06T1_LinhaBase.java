/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoJq06T1_LinhaBase
extends Model {
    public static final String NLINHA_LINK = "aAnexoJ.qQuadro06.fnLinha";
    public static final String NLINHA = "nLinha";
    private Long nLinha;
    public static final String CAMPOQ4_LINK = "aAnexoJ.qQuadro06.fcampoQ4";
    public static final String CAMPOQ4 = "campoQ4";
    private Long campoQ4;
    public static final String INSTALACAOFIXA_LINK = "aAnexoJ.qQuadro06.finstalacaoFixa";
    public static final String INSTALACAOFIXA = "instalacaoFixa";
    private String instalacaoFixa;
    public static final String IDENTIFICACAOPAISCODPAIS_LINK = "aAnexoJ.qQuadro06.fidentificacaoPaisCodPais";
    public static final String IDENTIFICACAOPAISCODPAIS = "identificacaoPaisCodPais";
    private Long identificacaoPaisCodPais;
    public static final String MONTANTERENDIMENTO_LINK = "aAnexoJ.qQuadro06.fmontanteRendimento";
    public static final String MONTANTERENDIMENTO = "montanteRendimento";
    private Long montanteRendimento;
    public static final String PAISDAFONTEVALOR_LINK = "aAnexoJ.qQuadro06.fpaisDaFonteValor";
    public static final String PAISDAFONTEVALOR = "paisDaFonteValor";
    private Long paisDaFonteValor;
    public static final String PAISAGENTEPAGADORCODPAIS_LINK = "aAnexoJ.qQuadro06.fpaisAgentePagadorCodPais";
    public static final String PAISAGENTEPAGADORCODPAIS = "paisAgentePagadorCodPais";
    private Long paisAgentePagadorCodPais;
    public static final String PAISAGENTEPAGADORVALOR_LINK = "aAnexoJ.qQuadro06.fpaisAgentePagadorValor";
    public static final String PAISAGENTEPAGADORVALOR = "paisAgentePagadorValor";
    private Long paisAgentePagadorValor;

    public AnexoJq06T1_LinhaBase(Long nLinha, Long campoQ4, String instalacaoFixa, Long identificacaoPaisCodPais, Long montanteRendimento, Long paisDaFonteValor, Long paisAgentePagadorCodPais, Long paisAgentePagadorValor) {
        this.nLinha = nLinha;
        this.campoQ4 = campoQ4;
        this.instalacaoFixa = instalacaoFixa;
        this.identificacaoPaisCodPais = identificacaoPaisCodPais;
        this.montanteRendimento = montanteRendimento;
        this.paisDaFonteValor = paisDaFonteValor;
        this.paisAgentePagadorCodPais = paisAgentePagadorCodPais;
        this.paisAgentePagadorValor = paisAgentePagadorValor;
    }

    public AnexoJq06T1_LinhaBase() {
        this.nLinha = null;
        this.campoQ4 = null;
        this.instalacaoFixa = null;
        this.identificacaoPaisCodPais = null;
        this.montanteRendimento = null;
        this.paisDaFonteValor = null;
        this.paisAgentePagadorCodPais = null;
        this.paisAgentePagadorValor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoJQ6NLinha")
    public Long getNLinha() {
        return this.nLinha;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNLinha(Long nLinha) {
        Long oldValue = this.nLinha;
        this.nLinha = nLinha;
        this.firePropertyChange("nLinha", oldValue, this.nLinha);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoJQ4")
    public Long getCampoQ4() {
        return this.campoQ4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoQ4(Long campoQ4) {
        Long oldValue = this.campoQ4;
        this.campoQ4 = campoQ4;
        this.firePropertyChange("campoQ4", oldValue, this.campoQ4);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_SimNao")
    public String getInstalacaoFixa() {
        return this.instalacaoFixa;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setInstalacaoFixa(String instalacaoFixa) {
        String oldValue = this.instalacaoFixa;
        this.instalacaoFixa = instalacaoFixa;
        this.firePropertyChange("instalacaoFixa", oldValue, this.instalacaoFixa);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxJ")
    public Long getIdentificacaoPaisCodPais() {
        return this.identificacaoPaisCodPais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setIdentificacaoPaisCodPais(Long identificacaoPaisCodPais) {
        Long oldValue = this.identificacaoPaisCodPais;
        this.identificacaoPaisCodPais = identificacaoPaisCodPais;
        this.firePropertyChange("identificacaoPaisCodPais", oldValue, this.identificacaoPaisCodPais);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getMontanteRendimento() {
        return this.montanteRendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setMontanteRendimento(Long montanteRendimento) {
        Long oldValue = this.montanteRendimento;
        this.montanteRendimento = montanteRendimento;
        this.firePropertyChange("montanteRendimento", oldValue, this.montanteRendimento);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getPaisDaFonteValor() {
        return this.paisDaFonteValor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setPaisDaFonteValor(Long paisDaFonteValor) {
        Long oldValue = this.paisDaFonteValor;
        this.paisDaFonteValor = paisDaFonteValor;
        this.firePropertyChange("paisDaFonteValor", oldValue, this.paisDaFonteValor);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxJ")
    public Long getPaisAgentePagadorCodPais() {
        return this.paisAgentePagadorCodPais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setPaisAgentePagadorCodPais(Long paisAgentePagadorCodPais) {
        Long oldValue = this.paisAgentePagadorCodPais;
        this.paisAgentePagadorCodPais = paisAgentePagadorCodPais;
        this.firePropertyChange("paisAgentePagadorCodPais", oldValue, this.paisAgentePagadorCodPais);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getPaisAgentePagadorValor() {
        return this.paisAgentePagadorValor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setPaisAgentePagadorValor(Long paisAgentePagadorValor) {
        Long oldValue = this.paisAgentePagadorValor;
        this.paisAgentePagadorValor = paisAgentePagadorValor;
        this.firePropertyChange("paisAgentePagadorValor", oldValue, this.paisAgentePagadorValor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nLinha);
        result = HashCodeUtil.hash(result, this.campoQ4);
        result = HashCodeUtil.hash(result, this.instalacaoFixa);
        result = HashCodeUtil.hash(result, this.identificacaoPaisCodPais);
        result = HashCodeUtil.hash(result, this.montanteRendimento);
        result = HashCodeUtil.hash(result, this.paisDaFonteValor);
        result = HashCodeUtil.hash(result, this.paisAgentePagadorCodPais);
        result = HashCodeUtil.hash(result, this.paisAgentePagadorValor);
        return result;
    }

    public static enum Property {
        NLINHA("nLinha", 2),
        CAMPOQ4("campoQ4", 3),
        INSTALACAOFIXA("instalacaoFixa", 4),
        IDENTIFICACAOPAISCODPAIS("identificacaoPaisCodPais", 5),
        MONTANTERENDIMENTO("montanteRendimento", 6),
        PAISDAFONTEVALOR("paisDaFonteValor", 7),
        PAISAGENTEPAGADORCODPAIS("paisAgentePagadorCodPais", 8),
        PAISAGENTEPAGADORVALOR("paisAgentePagadorValor", 9);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

