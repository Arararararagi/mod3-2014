/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoJq07T1_LinhaBase
extends Model {
    public static final String CAMPONLINHAQ6_LINK = "aAnexoJ.qQuadro07.fcampoNLinhaQ6";
    public static final String CAMPONLINHAQ6 = "campoNLinhaQ6";
    private Long campoNLinhaQ6;
    public static final String RENDIMENTO_LINK = "aAnexoJ.qQuadro07.frendimento";
    public static final String RENDIMENTO = "rendimento";
    private Long rendimento;
    public static final String NANOS_LINK = "aAnexoJ.qQuadro07.fnanos";
    public static final String NANOS = "nanos";
    private Long nanos;

    public AnexoJq07T1_LinhaBase(Long campoNLinhaQ6, Long rendimento, Long nanos) {
        this.campoNLinhaQ6 = campoNLinhaQ6;
        this.rendimento = rendimento;
        this.nanos = nanos;
    }

    public AnexoJq07T1_LinhaBase() {
        this.campoNLinhaQ6 = null;
        this.rendimento = null;
        this.nanos = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoJQ7LinhaQ6")
    public Long getCampoNLinhaQ6() {
        return this.campoNLinhaQ6;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoNLinhaQ6(Long campoNLinhaQ6) {
        Long oldValue = this.campoNLinhaQ6;
        this.campoNLinhaQ6 = campoNLinhaQ6;
        this.firePropertyChange("campoNLinhaQ6", oldValue, this.campoNLinhaQ6);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimento() {
        return this.rendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimento(Long rendimento) {
        Long oldValue = this.rendimento;
        this.rendimento = rendimento;
        this.firePropertyChange("rendimento", oldValue, this.rendimento);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNanos() {
        return this.nanos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNanos(Long nanos) {
        Long oldValue = this.nanos;
        this.nanos = nanos;
        this.firePropertyChange("nanos", oldValue, this.nanos);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.campoNLinhaQ6);
        result = HashCodeUtil.hash(result, this.rendimento);
        result = HashCodeUtil.hash(result, this.nanos);
        return result;
    }

    public static enum Property {
        CAMPONLINHAQ6("campoNLinhaQ6", 2),
        RENDIMENTO("rendimento", 3),
        NANOS("nanos", 4);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

