/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro05Base;

public class Quadro05
extends Quadro05Base {
    public boolean isEmpty() {
        return this.getAnexoJq05T1().isEmpty() && this.getAnexoJq05T2().isEmpty();
    }
}

