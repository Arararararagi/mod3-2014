/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro04
extends Quadro04Base {
    public long getTotalRendimentoPreenchido() {
        long totalRendimentosPreenchidos = 0;
        if (this.getAnexoJq04C401b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C401b().longValue();
        }
        if (this.getAnexoJq04C402b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C402b().longValue();
        }
        if (this.getAnexoJq04C403b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C403b().longValue();
        }
        if (this.getAnexoJq04C404b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C404b().longValue();
        }
        if (this.getAnexoJq04C405b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C405b().longValue();
        }
        if (this.getAnexoJq04C406b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C406b().longValue();
        }
        if (this.getAnexoJq04C407b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C407b().longValue();
        }
        if (this.getAnexoJq04C408b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C408b().longValue();
        }
        if (this.getAnexoJq04C409b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C409b().longValue();
        }
        if (this.getAnexoJq04C410b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C410b().longValue();
        }
        if (this.getAnexoJq04C411b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C411b().longValue();
        }
        if (this.getAnexoJq04C412b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C412b().longValue();
        }
        if (this.getAnexoJq04C425b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C425b().longValue();
        }
        if (this.getAnexoJq04C415b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C415b().longValue();
        }
        if (this.getAnexoJq04C416b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C416b().longValue();
        }
        if (this.getAnexoJq04C417b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C417b().longValue();
        }
        if (this.getAnexoJq04C418b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C418b().longValue();
        }
        if (this.getAnexoJq04C419b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C419b().longValue();
        }
        if (this.getAnexoJq04C420b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C420b().longValue();
        }
        if (this.getAnexoJq04C422b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C422b().longValue();
        }
        if (this.getAnexoJq04C423b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C423b().longValue();
        }
        if (this.getAnexoJq04C424b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C424b().longValue();
        }
        if (this.getAnexoJq04C426b() != null) {
            totalRendimentosPreenchidos+=this.getAnexoJq04C426b().longValue();
        }
        return totalRendimentosPreenchidos;
    }

    public long getTotalImpostoEstrangeiroPreenchido() {
        long totalImpostoEstrangeiro = 0;
        if (this.getAnexoJq04C401c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C401c().longValue();
        }
        if (this.getAnexoJq04C402c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C402c().longValue();
        }
        if (this.getAnexoJq04C403c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C403c().longValue();
        }
        if (this.getAnexoJq04C404c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C404c().longValue();
        }
        if (this.getAnexoJq04C405c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C405c().longValue();
        }
        if (this.getAnexoJq04C406c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C406c().longValue();
        }
        if (this.getAnexoJq04C407c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C407c().longValue();
        }
        if (this.getAnexoJq04C408c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C408c().longValue();
        }
        if (this.getAnexoJq04C409c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C409c().longValue();
        }
        if (this.getAnexoJq04C410c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C410c().longValue();
        }
        if (this.getAnexoJq04C411c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C411c().longValue();
        }
        if (this.getAnexoJq04C412c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C412c().longValue();
        }
        if (this.getAnexoJq04C425c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C425c().longValue();
        }
        if (this.getAnexoJq04C415c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C415c().longValue();
        }
        if (this.getAnexoJq04C416c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C416c().longValue();
        }
        if (this.getAnexoJq04C417c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C417c().longValue();
        }
        if (this.getAnexoJq04C418c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C418c().longValue();
        }
        if (this.getAnexoJq04C419c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C419c().longValue();
        }
        if (this.getAnexoJq04C420c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C420c().longValue();
        }
        if (this.getAnexoJq04C422c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C422c().longValue();
        }
        if (this.getAnexoJq04C423c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C423c().longValue();
        }
        if (this.getAnexoJq04C424c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C424c().longValue();
        }
        if (this.getAnexoJq04C426c() != null) {
            totalImpostoEstrangeiro+=this.getAnexoJq04C426c().longValue();
        }
        return totalImpostoEstrangeiro;
    }

    public long getTotalImpostoPortugalPreenchido() {
        long totalImpostoPortugal = 0;
        if (this.getAnexoJq04C401d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C401d().longValue();
        }
        if (this.getAnexoJq04C402d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C402d().longValue();
        }
        if (this.getAnexoJq04C403d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C403d().longValue();
        }
        if (this.getAnexoJq04C404d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C404d().longValue();
        }
        if (this.getAnexoJq04C405d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C405d().longValue();
        }
        if (this.getAnexoJq04C406d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C406d().longValue();
        }
        if (this.getAnexoJq04C407d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C407d().longValue();
        }
        if (this.getAnexoJq04C408d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C408d().longValue();
        }
        if (this.getAnexoJq04C409d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C409d().longValue();
        }
        if (this.getAnexoJq04C410d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C410d().longValue();
        }
        if (this.getAnexoJq04C411d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C411d().longValue();
        }
        if (this.getAnexoJq04C412d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C412d().longValue();
        }
        if (this.getAnexoJq04C425d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C425d().longValue();
        }
        if (this.getAnexoJq04C415d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C415d().longValue();
        }
        if (this.getAnexoJq04C416d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C416d().longValue();
        }
        if (this.getAnexoJq04C417d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C417d().longValue();
        }
        if (this.getAnexoJq04C418d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C418d().longValue();
        }
        if (this.getAnexoJq04C419d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C419d().longValue();
        }
        if (this.getAnexoJq04C420d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C420d().longValue();
        }
        if (this.getAnexoJq04C422d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C422d().longValue();
        }
        if (this.getAnexoJq04C423d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C423d().longValue();
        }
        if (this.getAnexoJq04C424d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C424d().longValue();
        }
        if (this.getAnexoJq04C426d() != null) {
            totalImpostoPortugal+=this.getAnexoJq04C426d().longValue();
        }
        return totalImpostoPortugal;
    }

    public boolean isAnexoJq04B1SimSelected() {
        return this.getAnexoJq04B1() != null && this.getAnexoJq04B1().equals(ANEXOJQ04B1SIM_VALUE);
    }

    public boolean isAnexoJq04B1NaoSelected() {
        return this.getAnexoJq04B1() != null && this.getAnexoJq04B1().equals(ANEXOJQ04B1NAO_VALUE);
    }

    public boolean hasLinhaPreenchida(long codigo) {
        if (codigo == 401 && (this.getAnexoJq04C401a() != null || this.getAnexoJq04C401b() != null || this.getAnexoJq04C401c() != null || this.getAnexoJq04C401d() != null)) {
            return true;
        }
        if (codigo == 402 && (this.getAnexoJq04C402a() != null || this.getAnexoJq04C402b() != null || this.getAnexoJq04C402c() != null || this.getAnexoJq04C402d() != null)) {
            return true;
        }
        if (codigo == 403 && (this.getAnexoJq04C403b() != null || this.getAnexoJq04C403c() != null || this.getAnexoJq04C403d() != null)) {
            return true;
        }
        if (codigo == 404 && (this.getAnexoJq04C404b() != null || this.getAnexoJq04C404c() != null || this.getAnexoJq04C404d() != null)) {
            return true;
        }
        if (codigo == 405 && (this.getAnexoJq04C405b() != null || this.getAnexoJq04C405c() != null || this.getAnexoJq04C405d() != null)) {
            return true;
        }
        if (codigo == 406 && (this.getAnexoJq04C406b() != null || this.getAnexoJq04C406c() != null || this.getAnexoJq04C406d() != null)) {
            return true;
        }
        if (codigo == 407 && (this.getAnexoJq04C407b() != null || this.getAnexoJq04C407c() != null || this.getAnexoJq04C407d() != null)) {
            return true;
        }
        if (codigo == 408 && (this.getAnexoJq04C408b() != null || this.getAnexoJq04C408c() != null || this.getAnexoJq04C408d() != null)) {
            return true;
        }
        if (codigo == 409 && (this.getAnexoJq04C409b() != null || this.getAnexoJq04C409c() != null || this.getAnexoJq04C409d() != null)) {
            return true;
        }
        if (codigo == 410 && (this.getAnexoJq04C410b() != null || this.getAnexoJq04C410c() != null || this.getAnexoJq04C410d() != null)) {
            return true;
        }
        if (codigo == 411 && (this.getAnexoJq04C411b() != null || this.getAnexoJq04C411c() != null || this.getAnexoJq04C411d() != null)) {
            return true;
        }
        if (codigo == 412 && (this.getAnexoJq04C412b() != null || this.getAnexoJq04C412c() != null || this.getAnexoJq04C412d() != null)) {
            return true;
        }
        if (codigo == 425 && (this.getAnexoJq04C425b() != null || this.getAnexoJq04C425c() != null || this.getAnexoJq04C425d() != null)) {
            return true;
        }
        if (codigo == 415 && (this.getAnexoJq04C415b() != null || this.getAnexoJq04C415c() != null || this.getAnexoJq04C415d() != null)) {
            return true;
        }
        if (codigo == 416 && (this.getAnexoJq04C416a() != null || this.getAnexoJq04C416b() != null || this.getAnexoJq04C416c() != null || this.getAnexoJq04C416d() != null)) {
            return true;
        }
        if (codigo == 417 && (this.getAnexoJq04C417a() != null || this.getAnexoJq04C417b() != null || this.getAnexoJq04C417c() != null || this.getAnexoJq04C417d() != null)) {
            return true;
        }
        if (codigo == 418 && (this.getAnexoJq04C418b() != null || this.getAnexoJq04C418c() != null || this.getAnexoJq04C418d() != null)) {
            return true;
        }
        if (codigo == 419 && (this.getAnexoJq04C419b() != null || this.getAnexoJq04C419c() != null || this.getAnexoJq04C419d() != null)) {
            return true;
        }
        if (codigo == 420 && (this.getAnexoJq04C420b() != null || this.getAnexoJq04C420c() != null || this.getAnexoJq04C420d() != null)) {
            return true;
        }
        if (codigo == 422 && (this.getAnexoJq04C422b() != null || this.getAnexoJq04C422c() != null || this.getAnexoJq04C422d() != null)) {
            return true;
        }
        if (codigo == 423 && (this.getAnexoJq04C423b() != null || this.getAnexoJq04C423c() == null && this.getAnexoJq04C423d() != null)) {
            return true;
        }
        if (codigo == 424 && (this.getAnexoJq04C424b() != null || this.getAnexoJq04C424c() == null && this.getAnexoJq04C424d() != null)) {
            return true;
        }
        if (codigo == 426 && (this.getAnexoJq04C426b() != null || this.getAnexoJq04C426c() != null || this.getAnexoJq04C426d() != null)) {
            return true;
        }
        return false;
    }

    public boolean hasRendimento(long codigo) {
        if (codigo == 401 && this.getAnexoJq04C401b() != null) {
            return true;
        }
        if (codigo == 402 && this.getAnexoJq04C402b() != null) {
            return true;
        }
        if (codigo == 403 && this.getAnexoJq04C403b() != null) {
            return true;
        }
        if (codigo == 404 && this.getAnexoJq04C404b() != null) {
            return true;
        }
        if (codigo == 405 && this.getAnexoJq04C405b() != null) {
            return true;
        }
        if (codigo == 406 && this.getAnexoJq04C406b() != null) {
            return true;
        }
        if (codigo == 407 && this.getAnexoJq04C407b() != null) {
            return true;
        }
        if (codigo == 408 && this.getAnexoJq04C408b() != null) {
            return true;
        }
        if (codigo == 409 && this.getAnexoJq04C409b() != null) {
            return true;
        }
        if (codigo == 410 && this.getAnexoJq04C410b() != null) {
            return true;
        }
        if (codigo == 411 && this.getAnexoJq04C411b() != null) {
            return true;
        }
        if (codigo == 412 && this.getAnexoJq04C412b() != null) {
            return true;
        }
        if (codigo == 425 && this.getAnexoJq04C425b() != null) {
            return true;
        }
        if (codigo == 415 && this.getAnexoJq04C415b() != null) {
            return true;
        }
        if (codigo == 416 && this.getAnexoJq04C416b() != null) {
            return true;
        }
        if (codigo == 417 && this.getAnexoJq04C417b() != null) {
            return true;
        }
        if (codigo == 418 && this.getAnexoJq04C418b() != null) {
            return true;
        }
        if (codigo == 419 && this.getAnexoJq04C419b() != null) {
            return true;
        }
        if (codigo == 420 && this.getAnexoJq04C420b() != null) {
            return true;
        }
        if (codigo == 422 && this.getAnexoJq04C422b() != null) {
            return true;
        }
        if (codigo == 423 && this.getAnexoJq04C423b() != null) {
            return true;
        }
        return false;
    }

    public long getRetencaoByCodigo(long codigo) {
        if (codigo == 401) {
            return this.getAnexoJq04C401d() != null ? this.getAnexoJq04C401d() : 0;
        }
        if (codigo == 402) {
            return this.getAnexoJq04C402d() != null ? this.getAnexoJq04C402d() : 0;
        }
        if (codigo == 403) {
            return this.getAnexoJq04C403d() != null ? this.getAnexoJq04C403d() : 0;
        }
        if (codigo == 404) {
            return this.getAnexoJq04C404d() != null ? this.getAnexoJq04C404d() : 0;
        }
        if (codigo == 405) {
            return this.getAnexoJq04C405d() != null ? this.getAnexoJq04C405d() : 0;
        }
        if (codigo == 406) {
            return this.getAnexoJq04C406d() != null ? this.getAnexoJq04C406d() : 0;
        }
        if (codigo == 407) {
            return this.getAnexoJq04C407d() != null ? this.getAnexoJq04C407d() : 0;
        }
        if (codigo == 408) {
            return this.getAnexoJq04C408d() != null ? this.getAnexoJq04C408d() : 0;
        }
        if (codigo == 409) {
            return this.getAnexoJq04C409d() != null ? this.getAnexoJq04C409d() : 0;
        }
        if (codigo == 410) {
            return this.getAnexoJq04C410d() != null ? this.getAnexoJq04C410d() : 0;
        }
        if (codigo == 411) {
            return this.getAnexoJq04C411d() != null ? this.getAnexoJq04C411d() : 0;
        }
        if (codigo == 412) {
            return this.getAnexoJq04C412d() != null ? this.getAnexoJq04C412d() : 0;
        }
        if (codigo == 425) {
            return this.getAnexoJq04C425d() != null ? this.getAnexoJq04C425d() : 0;
        }
        if (codigo == 415) {
            return this.getAnexoJq04C415d() != null ? this.getAnexoJq04C415d() : 0;
        }
        if (codigo == 416) {
            return this.getAnexoJq04C416d() != null ? this.getAnexoJq04C416d() : 0;
        }
        if (codigo == 417) {
            return this.getAnexoJq04C417d() != null ? this.getAnexoJq04C417d() : 0;
        }
        if (codigo == 418) {
            return this.getAnexoJq04C418d() != null ? this.getAnexoJq04C418d() : 0;
        }
        if (codigo == 419) {
            return this.getAnexoJq04C419d() != null ? this.getAnexoJq04C419d() : 0;
        }
        if (codigo == 420) {
            return this.getAnexoJq04C420d() != null ? this.getAnexoJq04C420d() : 0;
        }
        if (codigo == 422) {
            return this.getAnexoJq04C422d() != null ? this.getAnexoJq04C422d() : 0;
        }
        if (codigo == 424) {
            return this.getAnexoJq04C424d() != null ? this.getAnexoJq04C424d() : 0;
        }
        if (codigo == 426) {
            return this.getAnexoJq04C426d() != null ? this.getAnexoJq04C426d() : 0;
        }
        return 0;
    }

    public long getRendimentosByCodigo(long codigo) {
        if (codigo == 401) {
            return this.getAnexoJq04C401b() != null ? this.getAnexoJq04C401b() : 0;
        }
        if (codigo == 402) {
            return this.getAnexoJq04C402b() != null ? this.getAnexoJq04C402b() : 0;
        }
        if (codigo == 403) {
            return this.getAnexoJq04C403b() != null ? this.getAnexoJq04C403b() : 0;
        }
        if (codigo == 404) {
            return this.getAnexoJq04C404b() != null ? this.getAnexoJq04C404b() : 0;
        }
        if (codigo == 405) {
            return this.getAnexoJq04C405b() != null ? this.getAnexoJq04C405b() : 0;
        }
        if (codigo == 406) {
            return this.getAnexoJq04C406b() != null ? this.getAnexoJq04C406b() : 0;
        }
        if (codigo == 407) {
            return this.getAnexoJq04C407b() != null ? this.getAnexoJq04C407b() : 0;
        }
        if (codigo == 408) {
            return this.getAnexoJq04C408b() != null ? this.getAnexoJq04C408b() : 0;
        }
        if (codigo == 409) {
            return this.getAnexoJq04C409b() != null ? this.getAnexoJq04C409b() : 0;
        }
        if (codigo == 410) {
            return this.getAnexoJq04C410b() != null ? this.getAnexoJq04C410b() : 0;
        }
        if (codigo == 411) {
            return this.getAnexoJq04C411b() != null ? this.getAnexoJq04C411b() : 0;
        }
        if (codigo == 412) {
            return this.getAnexoJq04C412b() != null ? this.getAnexoJq04C412b() : 0;
        }
        if (codigo == 425) {
            return this.getAnexoJq04C425b() != null ? this.getAnexoJq04C425b() : 0;
        }
        if (codigo == 415) {
            return this.getAnexoJq04C415b() != null ? this.getAnexoJq04C415b() : 0;
        }
        if (codigo == 416) {
            return this.getAnexoJq04C416b() != null ? this.getAnexoJq04C416b() : 0;
        }
        if (codigo == 417) {
            return this.getAnexoJq04C417b() != null ? this.getAnexoJq04C417b() : 0;
        }
        if (codigo == 418) {
            return this.getAnexoJq04C418b() != null ? this.getAnexoJq04C418b() : 0;
        }
        if (codigo == 419) {
            return this.getAnexoJq04C419b() != null ? this.getAnexoJq04C419b() : 0;
        }
        if (codigo == 420) {
            return this.getAnexoJq04C420b() != null ? this.getAnexoJq04C420b() : 0;
        }
        if (codigo == 422) {
            return this.getAnexoJq04C422b() != null ? this.getAnexoJq04C422b() : 0;
        }
        if (codigo == 424) {
            return this.getAnexoJq04C424b() != null ? this.getAnexoJq04C424b() : 0;
        }
        if (codigo == 426) {
            return this.getAnexoJq04C426b() != null ? this.getAnexoJq04C426b() : 0;
        }
        return 0;
    }

    public long getDistinctNumCodigosNatureza() {
        int numCodigos = 0;
        if (this.getAnexoJq04C401a() != null && this.getAnexoJq04C401a() != 0 || this.getAnexoJq04C401b() != null && this.getAnexoJq04C401b() != 0 || this.getAnexoJq04C401c() != null && this.getAnexoJq04C401c() != 0 || this.getAnexoJq04C401d() != null && this.getAnexoJq04C401d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C402a() != null && this.getAnexoJq04C402a() != 0 || this.getAnexoJq04C402b() != null && this.getAnexoJq04C402b() != 0 || this.getAnexoJq04C402c() != null && this.getAnexoJq04C402c() != 0 || this.getAnexoJq04C402d() != null && this.getAnexoJq04C402d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C403b() != null && this.getAnexoJq04C403b() != 0 || this.getAnexoJq04C403c() != null && this.getAnexoJq04C403c() != 0 || this.getAnexoJq04C403d() != null && this.getAnexoJq04C403d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C404b() != null && this.getAnexoJq04C404b() != 0 || this.getAnexoJq04C404c() != null && this.getAnexoJq04C404c() != 0 || this.getAnexoJq04C404d() != null && this.getAnexoJq04C404d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C405b() != null && this.getAnexoJq04C405b() != 0 || this.getAnexoJq04C405c() != null && this.getAnexoJq04C405c() != 0 || this.getAnexoJq04C405d() != null && this.getAnexoJq04C405d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C406b() != null && this.getAnexoJq04C406b() != 0 || this.getAnexoJq04C406c() != null && this.getAnexoJq04C406c() != 0 || this.getAnexoJq04C406d() != null && this.getAnexoJq04C406d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C407b() != null && this.getAnexoJq04C407b() != 0 || this.getAnexoJq04C407c() != null && this.getAnexoJq04C407c() != 0 || this.getAnexoJq04C407d() != null && this.getAnexoJq04C407d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C408b() != null && this.getAnexoJq04C408b() != 0 || this.getAnexoJq04C408c() != null && this.getAnexoJq04C408c() != 0 || this.getAnexoJq04C408d() != null && this.getAnexoJq04C408d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C409b() != null && this.getAnexoJq04C409b() != 0 || this.getAnexoJq04C409c() != null && this.getAnexoJq04C409c() != 0 || this.getAnexoJq04C409d() != null && this.getAnexoJq04C409d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C410b() != null && this.getAnexoJq04C410b() != 0 || this.getAnexoJq04C410c() != null && this.getAnexoJq04C410c() != 0 || this.getAnexoJq04C410d() != null && this.getAnexoJq04C410d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C411b() != null && this.getAnexoJq04C411b() != 0 || this.getAnexoJq04C411c() != null && this.getAnexoJq04C411c() != 0 || this.getAnexoJq04C411d() != null && this.getAnexoJq04C411d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C412b() != null && this.getAnexoJq04C412b() != 0 || this.getAnexoJq04C412c() != null && this.getAnexoJq04C412c() != 0 || this.getAnexoJq04C412d() != null && this.getAnexoJq04C412d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C425b() != null && this.getAnexoJq04C425b() != 0 || this.getAnexoJq04C425c() != null && this.getAnexoJq04C425c() != 0 || this.getAnexoJq04C425d() != null && this.getAnexoJq04C425d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C415b() != null && this.getAnexoJq04C415b() != 0 || this.getAnexoJq04C415c() != null && this.getAnexoJq04C415c() != 0 || this.getAnexoJq04C415d() != null && this.getAnexoJq04C415d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C416a() != null && this.getAnexoJq04C416a() != 0 || this.getAnexoJq04C416b() != null && this.getAnexoJq04C416b() != 0 || this.getAnexoJq04C416c() != null && this.getAnexoJq04C416c() != 0 || this.getAnexoJq04C416d() != null && this.getAnexoJq04C416d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C417a() != null && this.getAnexoJq04C417a() != 0 || this.getAnexoJq04C417b() != null && this.getAnexoJq04C417b() != 0 || this.getAnexoJq04C417c() != null && this.getAnexoJq04C417c() != 0 || this.getAnexoJq04C417d() != null && this.getAnexoJq04C417d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C418b() != null && this.getAnexoJq04C418b() != 0 || this.getAnexoJq04C418c() != null && this.getAnexoJq04C418c() != 0 || this.getAnexoJq04C418d() != null && this.getAnexoJq04C418d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C419b() != null && this.getAnexoJq04C419b() != 0 || this.getAnexoJq04C419c() != null && this.getAnexoJq04C419c() != 0 || this.getAnexoJq04C419d() != null && this.getAnexoJq04C419d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C420b() != null && this.getAnexoJq04C420b() != 0 || this.getAnexoJq04C420c() != null && this.getAnexoJq04C420c() != 0 || this.getAnexoJq04C420d() != null && this.getAnexoJq04C420d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C422b() != null && this.getAnexoJq04C422b() != 0 || this.getAnexoJq04C422c() != null && this.getAnexoJq04C422c() != 0 || this.getAnexoJq04C422d() != null && this.getAnexoJq04C422d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C423b() != null && this.getAnexoJq04C423b() != 0 || this.getAnexoJq04C423c() != null && this.getAnexoJq04C423c() != 0 || this.getAnexoJq04C423d() != null && this.getAnexoJq04C423d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C424b() != null && this.getAnexoJq04C424b() != 0 || this.getAnexoJq04C424c() != null && this.getAnexoJq04C424c() != 0 || this.getAnexoJq04C424d() != null && this.getAnexoJq04C424d() != 0) {
            ++numCodigos;
        }
        if (this.getAnexoJq04C426b() != null && this.getAnexoJq04C426b() != 0 || this.getAnexoJq04C426c() != null && this.getAnexoJq04C426c() != 0 || this.getAnexoJq04C426d() != null && this.getAnexoJq04C426d() != 0) {
            ++numCodigos;
        }
        return numCodigos;
    }

    public boolean isEmpty() {
        return (this.getAnexoJq04C401a() == null || this.getAnexoJq04C401a() == 0) && (this.getAnexoJq04C402a() == null || this.getAnexoJq04C402a() == 0) && (this.getAnexoJq04C416a() == null || this.getAnexoJq04C416a() == 0) && (this.getAnexoJq04C417a() == null || this.getAnexoJq04C417a() == 0) && (this.getAnexoJq04C1b() == null || this.getAnexoJq04C1b() == 0) && (this.getAnexoJq04C1c() == null || this.getAnexoJq04C1c() == 0) && (this.getAnexoJq04C1d() == null || this.getAnexoJq04C1d() == 0) && (this.getAnexoJq04T1() == null || this.getAnexoJq04T1().size() == 0) && (this.getAnexoJq04T2() == null || this.getAnexoJq04T2().size() == 0);
    }

    public boolean isMontanteRendimentoCampoPreenchido(Long campoQ4) {
        if (campoQ4 == null) {
            return false;
        }
        long campo = campoQ4;
        if (!(campo != 401 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C401b()))) {
            return true;
        }
        if (!(campo != 402 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C402b()))) {
            return true;
        }
        if (!(campo != 403 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C403b()))) {
            return true;
        }
        if (!(campo != 404 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C404b()))) {
            return true;
        }
        if (!(campo != 405 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C405b()))) {
            return true;
        }
        if (!(campo != 406 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C406b()))) {
            return true;
        }
        if (!(campo != 407 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C407b()))) {
            return true;
        }
        if (!(campo != 408 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C408b()))) {
            return true;
        }
        if (!(campo != 409 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C409b()))) {
            return true;
        }
        if (!(campo != 410 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C410b()))) {
            return true;
        }
        if (!(campo != 411 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C411b()))) {
            return true;
        }
        if (!(campo != 412 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C412b()))) {
            return true;
        }
        if (!(campo != 425 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C425b()))) {
            return true;
        }
        if (!(campo != 415 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C415b()))) {
            return true;
        }
        if (!(campo != 416 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C416b()))) {
            return true;
        }
        if (!(campo != 417 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C417b()))) {
            return true;
        }
        if (!(campo != 418 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C418b()))) {
            return true;
        }
        if (!(campo != 419 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C419b()))) {
            return true;
        }
        if (!(campo != 420 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C420b()))) {
            return true;
        }
        if (!(campo != 422 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C422b()))) {
            return true;
        }
        if (!(campo != 423 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C423b()))) {
            return true;
        }
        if (!(campo != 424 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C424b()))) {
            return true;
        }
        if (!(campo != 426 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoJq04C426b()))) {
            return true;
        }
        return false;
    }

    public void setAnexoJq04C1bFormula_WithoutOverwriteBehavior() {
        long newValue = 0;
        this.setAnexoJq04C1b(newValue+=(this.getAnexoJq04C401b() == null ? 0 : this.getAnexoJq04C401b()) + (this.getAnexoJq04C402b() == null ? 0 : this.getAnexoJq04C402b()) + (this.getAnexoJq04C403b() == null ? 0 : this.getAnexoJq04C403b()) + (this.getAnexoJq04C404b() == null ? 0 : this.getAnexoJq04C404b()) + (this.getAnexoJq04C405b() == null ? 0 : this.getAnexoJq04C405b()) + (this.getAnexoJq04C406b() == null ? 0 : this.getAnexoJq04C406b()) + (this.getAnexoJq04C407b() == null ? 0 : this.getAnexoJq04C407b()) + (this.getAnexoJq04C408b() == null ? 0 : this.getAnexoJq04C408b()) + (this.getAnexoJq04C409b() == null ? 0 : this.getAnexoJq04C409b()) + (this.getAnexoJq04C410b() == null ? 0 : this.getAnexoJq04C410b()) + (this.getAnexoJq04C411b() == null ? 0 : this.getAnexoJq04C411b()) + (this.getAnexoJq04C412b() == null ? 0 : this.getAnexoJq04C412b()) + (this.getAnexoJq04C425b() == null ? 0 : this.getAnexoJq04C425b()) + (this.getAnexoJq04C415b() == null ? 0 : this.getAnexoJq04C415b()) + (this.getAnexoJq04C416b() == null ? 0 : this.getAnexoJq04C416b()) + (this.getAnexoJq04C417b() == null ? 0 : this.getAnexoJq04C417b()) + (this.getAnexoJq04C418b() == null ? 0 : this.getAnexoJq04C418b()) + (this.getAnexoJq04C419b() == null ? 0 : this.getAnexoJq04C419b()) + (this.getAnexoJq04C420b() == null ? 0 : this.getAnexoJq04C420b()) + (this.getAnexoJq04C422b() == null ? 0 : this.getAnexoJq04C422b()) + (this.getAnexoJq04C423b() == null ? 0 : this.getAnexoJq04C423b()) + (this.getAnexoJq04C424b() == null ? 0 : this.getAnexoJq04C424b()));
    }

    public void setAnexoJq04C1cFormula_WithoutOverwriteBehavior() {
        long newValue = 0;
        this.setAnexoJq04C1c(newValue+=(this.getAnexoJq04C401c() == null ? 0 : this.getAnexoJq04C401c()) + (this.getAnexoJq04C402c() == null ? 0 : this.getAnexoJq04C402c()) + (this.getAnexoJq04C403c() == null ? 0 : this.getAnexoJq04C403c()) + (this.getAnexoJq04C404c() == null ? 0 : this.getAnexoJq04C404c()) + (this.getAnexoJq04C405c() == null ? 0 : this.getAnexoJq04C405c()) + (this.getAnexoJq04C406c() == null ? 0 : this.getAnexoJq04C406c()) + (this.getAnexoJq04C407c() == null ? 0 : this.getAnexoJq04C407c()) + (this.getAnexoJq04C408c() == null ? 0 : this.getAnexoJq04C408c()) + (this.getAnexoJq04C409c() == null ? 0 : this.getAnexoJq04C409c()) + (this.getAnexoJq04C410c() == null ? 0 : this.getAnexoJq04C410c()) + (this.getAnexoJq04C411c() == null ? 0 : this.getAnexoJq04C411c()) + (this.getAnexoJq04C412c() == null ? 0 : this.getAnexoJq04C412c()) + (this.getAnexoJq04C425c() == null ? 0 : this.getAnexoJq04C425c()) + (this.getAnexoJq04C415c() == null ? 0 : this.getAnexoJq04C415c()) + (this.getAnexoJq04C416c() == null ? 0 : this.getAnexoJq04C416c()) + (this.getAnexoJq04C417c() == null ? 0 : this.getAnexoJq04C417c()) + (this.getAnexoJq04C418c() == null ? 0 : this.getAnexoJq04C418c()) + (this.getAnexoJq04C419c() == null ? 0 : this.getAnexoJq04C419c()) + (this.getAnexoJq04C420c() == null ? 0 : this.getAnexoJq04C420c()) + (this.getAnexoJq04C422c() == null ? 0 : this.getAnexoJq04C422c()) + (this.getAnexoJq04C423c() == null ? 0 : this.getAnexoJq04C423c()) + (this.getAnexoJq04C424c() == null ? 0 : this.getAnexoJq04C424c()));
    }

    public void setAnexoJq04C1dFormula_WithoutOverwriteBehavior() {
        long newValue = 0;
        this.setAnexoJq04C1d(newValue+=(this.getAnexoJq04C401d() == null ? 0 : this.getAnexoJq04C401d()) + (this.getAnexoJq04C402d() == null ? 0 : this.getAnexoJq04C402d()) + (this.getAnexoJq04C403d() == null ? 0 : this.getAnexoJq04C403d()) + (this.getAnexoJq04C404d() == null ? 0 : this.getAnexoJq04C404d()) + (this.getAnexoJq04C405d() == null ? 0 : this.getAnexoJq04C405d()) + (this.getAnexoJq04C406d() == null ? 0 : this.getAnexoJq04C406d()) + (this.getAnexoJq04C407d() == null ? 0 : this.getAnexoJq04C407d()) + (this.getAnexoJq04C408d() == null ? 0 : this.getAnexoJq04C408d()) + (this.getAnexoJq04C409d() == null ? 0 : this.getAnexoJq04C409d()) + (this.getAnexoJq04C410d() == null ? 0 : this.getAnexoJq04C410d()) + (this.getAnexoJq04C411d() == null ? 0 : this.getAnexoJq04C411d()) + (this.getAnexoJq04C412d() == null ? 0 : this.getAnexoJq04C412d()) + (this.getAnexoJq04C425d() == null ? 0 : this.getAnexoJq04C425d()) + (this.getAnexoJq04C415d() == null ? 0 : this.getAnexoJq04C415d()) + (this.getAnexoJq04C416d() == null ? 0 : this.getAnexoJq04C416d()) + (this.getAnexoJq04C417d() == null ? 0 : this.getAnexoJq04C417d()) + (this.getAnexoJq04C418d() == null ? 0 : this.getAnexoJq04C418d()) + (this.getAnexoJq04C419d() == null ? 0 : this.getAnexoJq04C419d()) + (this.getAnexoJq04C420d() == null ? 0 : this.getAnexoJq04C420d()) + (this.getAnexoJq04C422d() == null ? 0 : this.getAnexoJq04C422d()) + (this.getAnexoJq04C423d() == null ? 0 : this.getAnexoJq04C423d()) + (this.getAnexoJq04C424d() == null ? 0 : this.getAnexoJq04C424d()));
    }
}

