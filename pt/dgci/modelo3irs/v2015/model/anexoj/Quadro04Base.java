/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq04T2_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoJ.qQuadro04";
    public static final String ANEXOJQ04C401A_LINK = "aAnexoJ.qQuadro04.fanexoJq04C401a";
    public static final String ANEXOJQ04C401A = "anexoJq04C401a";
    private Long anexoJq04C401a;
    public static final String ANEXOJQ04C401B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C401b";
    public static final String ANEXOJQ04C401B = "anexoJq04C401b";
    private Long anexoJq04C401b;
    public static final String ANEXOJQ04C401C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C401c";
    public static final String ANEXOJQ04C401C = "anexoJq04C401c";
    private Long anexoJq04C401c;
    public static final String ANEXOJQ04C401D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C401d";
    public static final String ANEXOJQ04C401D = "anexoJq04C401d";
    private Long anexoJq04C401d;
    public static final String ANEXOJQ04C402A_LINK = "aAnexoJ.qQuadro04.fanexoJq04C402a";
    public static final String ANEXOJQ04C402A = "anexoJq04C402a";
    private Long anexoJq04C402a;
    public static final String ANEXOJQ04C402B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C402b";
    public static final String ANEXOJQ04C402B = "anexoJq04C402b";
    private Long anexoJq04C402b;
    public static final String ANEXOJQ04C402C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C402c";
    public static final String ANEXOJQ04C402C = "anexoJq04C402c";
    private Long anexoJq04C402c;
    public static final String ANEXOJQ04C402D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C402d";
    public static final String ANEXOJQ04C402D = "anexoJq04C402d";
    private Long anexoJq04C402d;
    public static final String ANEXOJQ04C403B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C403b";
    public static final String ANEXOJQ04C403B = "anexoJq04C403b";
    private Long anexoJq04C403b;
    public static final String ANEXOJQ04C403C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C403c";
    public static final String ANEXOJQ04C403C = "anexoJq04C403c";
    private Long anexoJq04C403c;
    public static final String ANEXOJQ04C403D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C403d";
    public static final String ANEXOJQ04C403D = "anexoJq04C403d";
    private Long anexoJq04C403d;
    public static final String ANEXOJQ04C404B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C404b";
    public static final String ANEXOJQ04C404B = "anexoJq04C404b";
    private Long anexoJq04C404b;
    public static final String ANEXOJQ04C404C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C404c";
    public static final String ANEXOJQ04C404C = "anexoJq04C404c";
    private Long anexoJq04C404c;
    public static final String ANEXOJQ04C404D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C404d";
    public static final String ANEXOJQ04C404D = "anexoJq04C404d";
    private Long anexoJq04C404d;
    public static final String ANEXOJQ04C405B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C405b";
    public static final String ANEXOJQ04C405B = "anexoJq04C405b";
    private Long anexoJq04C405b;
    public static final String ANEXOJQ04C405C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C405c";
    public static final String ANEXOJQ04C405C = "anexoJq04C405c";
    private Long anexoJq04C405c;
    public static final String ANEXOJQ04C405D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C405d";
    public static final String ANEXOJQ04C405D = "anexoJq04C405d";
    private Long anexoJq04C405d;
    public static final String ANEXOJQ04C406B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C406b";
    public static final String ANEXOJQ04C406B = "anexoJq04C406b";
    private Long anexoJq04C406b;
    public static final String ANEXOJQ04C406C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C406c";
    public static final String ANEXOJQ04C406C = "anexoJq04C406c";
    private Long anexoJq04C406c;
    public static final String ANEXOJQ04C406D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C406d";
    public static final String ANEXOJQ04C406D = "anexoJq04C406d";
    private Long anexoJq04C406d;
    public static final String ANEXOJQ04C426B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C426b";
    public static final String ANEXOJQ04C426B = "anexoJq04C426b";
    private Long anexoJq04C426b;
    public static final String ANEXOJQ04C426C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C426c";
    public static final String ANEXOJQ04C426C = "anexoJq04C426c";
    private Long anexoJq04C426c;
    public static final String ANEXOJQ04C426D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C426d";
    public static final String ANEXOJQ04C426D = "anexoJq04C426d";
    private Long anexoJq04C426d;
    public static final String ANEXOJQ04C407B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C407b";
    public static final String ANEXOJQ04C407B = "anexoJq04C407b";
    private Long anexoJq04C407b;
    public static final String ANEXOJQ04C407C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C407c";
    public static final String ANEXOJQ04C407C = "anexoJq04C407c";
    private Long anexoJq04C407c;
    public static final String ANEXOJQ04C407D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C407d";
    public static final String ANEXOJQ04C407D = "anexoJq04C407d";
    private Long anexoJq04C407d;
    public static final String ANEXOJQ04C408B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C408b";
    public static final String ANEXOJQ04C408B = "anexoJq04C408b";
    private Long anexoJq04C408b;
    public static final String ANEXOJQ04C408C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C408c";
    public static final String ANEXOJQ04C408C = "anexoJq04C408c";
    private Long anexoJq04C408c;
    public static final String ANEXOJQ04C408D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C408d";
    public static final String ANEXOJQ04C408D = "anexoJq04C408d";
    private Long anexoJq04C408d;
    public static final String ANEXOJQ04C409B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C409b";
    public static final String ANEXOJQ04C409B = "anexoJq04C409b";
    private Long anexoJq04C409b;
    public static final String ANEXOJQ04C409C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C409c";
    public static final String ANEXOJQ04C409C = "anexoJq04C409c";
    private Long anexoJq04C409c;
    public static final String ANEXOJQ04C409D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C409d";
    public static final String ANEXOJQ04C409D = "anexoJq04C409d";
    private Long anexoJq04C409d;
    public static final String ANEXOJQ04C410B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C410b";
    public static final String ANEXOJQ04C410B = "anexoJq04C410b";
    private Long anexoJq04C410b;
    public static final String ANEXOJQ04C410C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C410c";
    public static final String ANEXOJQ04C410C = "anexoJq04C410c";
    private Long anexoJq04C410c;
    public static final String ANEXOJQ04C410D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C410d";
    public static final String ANEXOJQ04C410D = "anexoJq04C410d";
    private Long anexoJq04C410d;
    public static final String ANEXOJQ04C411B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C411b";
    public static final String ANEXOJQ04C411B = "anexoJq04C411b";
    private Long anexoJq04C411b;
    public static final String ANEXOJQ04C411C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C411c";
    public static final String ANEXOJQ04C411C = "anexoJq04C411c";
    private Long anexoJq04C411c;
    public static final String ANEXOJQ04C411D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C411d";
    public static final String ANEXOJQ04C411D = "anexoJq04C411d";
    private Long anexoJq04C411d;
    public static final String ANEXOJQ04C412B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C412b";
    public static final String ANEXOJQ04C412B = "anexoJq04C412b";
    private Long anexoJq04C412b;
    public static final String ANEXOJQ04C412C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C412c";
    public static final String ANEXOJQ04C412C = "anexoJq04C412c";
    private Long anexoJq04C412c;
    public static final String ANEXOJQ04C412D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C412d";
    public static final String ANEXOJQ04C412D = "anexoJq04C412d";
    private Long anexoJq04C412d;
    public static final String ANEXOJQ04C425B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C425b";
    public static final String ANEXOJQ04C425B = "anexoJq04C425b";
    private Long anexoJq04C425b;
    public static final String ANEXOJQ04C425C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C425c";
    public static final String ANEXOJQ04C425C = "anexoJq04C425c";
    private Long anexoJq04C425c;
    public static final String ANEXOJQ04C425D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C425d";
    public static final String ANEXOJQ04C425D = "anexoJq04C425d";
    private Long anexoJq04C425d;
    public static final String ANEXOJQ04C415B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C415b";
    public static final String ANEXOJQ04C415B = "anexoJq04C415b";
    private Long anexoJq04C415b;
    public static final String ANEXOJQ04C415C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C415c";
    public static final String ANEXOJQ04C415C = "anexoJq04C415c";
    private Long anexoJq04C415c;
    public static final String ANEXOJQ04C415D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C415d";
    public static final String ANEXOJQ04C415D = "anexoJq04C415d";
    private Long anexoJq04C415d;
    public static final String ANEXOJQ04C416A_LINK = "aAnexoJ.qQuadro04.fanexoJq04C416a";
    public static final String ANEXOJQ04C416A = "anexoJq04C416a";
    private Long anexoJq04C416a;
    public static final String ANEXOJQ04C416B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C416b";
    public static final String ANEXOJQ04C416B = "anexoJq04C416b";
    private Long anexoJq04C416b;
    public static final String ANEXOJQ04C416C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C416c";
    public static final String ANEXOJQ04C416C = "anexoJq04C416c";
    private Long anexoJq04C416c;
    public static final String ANEXOJQ04C416D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C416d";
    public static final String ANEXOJQ04C416D = "anexoJq04C416d";
    private Long anexoJq04C416d;
    public static final String ANEXOJQ04C417A_LINK = "aAnexoJ.qQuadro04.fanexoJq04C417a";
    public static final String ANEXOJQ04C417A = "anexoJq04C417a";
    private Long anexoJq04C417a;
    public static final String ANEXOJQ04C417B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C417b";
    public static final String ANEXOJQ04C417B = "anexoJq04C417b";
    private Long anexoJq04C417b;
    public static final String ANEXOJQ04C417C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C417c";
    public static final String ANEXOJQ04C417C = "anexoJq04C417c";
    private Long anexoJq04C417c;
    public static final String ANEXOJQ04C417D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C417d";
    public static final String ANEXOJQ04C417D = "anexoJq04C417d";
    private Long anexoJq04C417d;
    public static final String ANEXOJQ04C418B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C418b";
    public static final String ANEXOJQ04C418B = "anexoJq04C418b";
    private Long anexoJq04C418b;
    public static final String ANEXOJQ04C418C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C418c";
    public static final String ANEXOJQ04C418C = "anexoJq04C418c";
    private Long anexoJq04C418c;
    public static final String ANEXOJQ04C418D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C418d";
    public static final String ANEXOJQ04C418D = "anexoJq04C418d";
    private Long anexoJq04C418d;
    public static final String ANEXOJQ04C419B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C419b";
    public static final String ANEXOJQ04C419B = "anexoJq04C419b";
    private Long anexoJq04C419b;
    public static final String ANEXOJQ04C419C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C419c";
    public static final String ANEXOJQ04C419C = "anexoJq04C419c";
    private Long anexoJq04C419c;
    public static final String ANEXOJQ04C419D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C419d";
    public static final String ANEXOJQ04C419D = "anexoJq04C419d";
    private Long anexoJq04C419d;
    public static final String ANEXOJQ04C420B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C420b";
    public static final String ANEXOJQ04C420B = "anexoJq04C420b";
    private Long anexoJq04C420b;
    public static final String ANEXOJQ04C420C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C420c";
    public static final String ANEXOJQ04C420C = "anexoJq04C420c";
    private Long anexoJq04C420c;
    public static final String ANEXOJQ04C420D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C420d";
    public static final String ANEXOJQ04C420D = "anexoJq04C420d";
    private Long anexoJq04C420d;
    public static final String ANEXOJQ04C422B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C422b";
    public static final String ANEXOJQ04C422B = "anexoJq04C422b";
    private Long anexoJq04C422b;
    public static final String ANEXOJQ04C422C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C422c";
    public static final String ANEXOJQ04C422C = "anexoJq04C422c";
    private Long anexoJq04C422c;
    public static final String ANEXOJQ04C422D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C422d";
    public static final String ANEXOJQ04C422D = "anexoJq04C422d";
    private Long anexoJq04C422d;
    public static final String ANEXOJQ04C423B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C423b";
    public static final String ANEXOJQ04C423B = "anexoJq04C423b";
    private Long anexoJq04C423b;
    public static final String ANEXOJQ04C423C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C423c";
    public static final String ANEXOJQ04C423C = "anexoJq04C423c";
    private Long anexoJq04C423c;
    public static final String ANEXOJQ04C423D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C423d";
    public static final String ANEXOJQ04C423D = "anexoJq04C423d";
    private Long anexoJq04C423d;
    public static final String ANEXOJQ04C424B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C424b";
    public static final String ANEXOJQ04C424B = "anexoJq04C424b";
    private Long anexoJq04C424b;
    public static final String ANEXOJQ04C424C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C424c";
    public static final String ANEXOJQ04C424C = "anexoJq04C424c";
    private Long anexoJq04C424c;
    public static final String ANEXOJQ04C424D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C424d";
    public static final String ANEXOJQ04C424D = "anexoJq04C424d";
    private Long anexoJq04C424d;
    public static final String ANEXOJQ04C1B_LINK = "aAnexoJ.qQuadro04.fanexoJq04C1b";
    public static final String ANEXOJQ04C1B = "anexoJq04C1b";
    private Long anexoJq04C1b;
    public static final String ANEXOJQ04C1C_LINK = "aAnexoJ.qQuadro04.fanexoJq04C1c";
    public static final String ANEXOJQ04C1C = "anexoJq04C1c";
    private Long anexoJq04C1c;
    public static final String ANEXOJQ04C1D_LINK = "aAnexoJ.qQuadro04.fanexoJq04C1d";
    public static final String ANEXOJQ04C1D = "anexoJq04C1d";
    private Long anexoJq04C1d;
    public static final String ANEXOJQ04C421_LINK = "aAnexoJ.qQuadro04.fanexoJq04C421";
    public static final String ANEXOJQ04C421 = "anexoJq04C421";
    private Long anexoJq04C421;
    public static final String ANEXOJQ04T1_LINK = "aAnexoJ.qQuadro04.tanexoJq04T1";
    private EventList<AnexoJq04T1_Linha> anexoJq04T1 = new BasicEventList<AnexoJq04T1_Linha>();
    public static final String ANEXOJQ04T2_LINK = "aAnexoJ.qQuadro04.tanexoJq04T2";
    private EventList<AnexoJq04T2_Linha> anexoJq04T2 = new BasicEventList<AnexoJq04T2_Linha>();
    public static final String ANEXOJQ04B1SIM_LINK = "aAnexoJ.qQuadro04.fanexoJq04B1Sim";
    public static final Boolean ANEXOJQ04B1SIM_VALUE = true;
    public static final String ANEXOJQ04B1NAO_LINK = "aAnexoJ.qQuadro04.fanexoJq04B1Nao";
    public static final Boolean ANEXOJQ04B1NAO_VALUE = false;
    public static final String ANEXOJQ04B1 = "anexoJq04B1";
    private Boolean anexoJq04B1;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C401a() {
        return this.anexoJq04C401a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C401a(Long anexoJq04C401a) {
        Long oldValue = this.anexoJq04C401a;
        this.anexoJq04C401a = anexoJq04C401a;
        this.firePropertyChange("anexoJq04C401a", oldValue, this.anexoJq04C401a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C401b() {
        return this.anexoJq04C401b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C401b(Long anexoJq04C401b) {
        Long oldValue = this.anexoJq04C401b;
        this.anexoJq04C401b = anexoJq04C401b;
        this.firePropertyChange("anexoJq04C401b", oldValue, this.anexoJq04C401b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C401c() {
        return this.anexoJq04C401c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C401c(Long anexoJq04C401c) {
        Long oldValue = this.anexoJq04C401c;
        this.anexoJq04C401c = anexoJq04C401c;
        this.firePropertyChange("anexoJq04C401c", oldValue, this.anexoJq04C401c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C401d() {
        return this.anexoJq04C401d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C401d(Long anexoJq04C401d) {
        Long oldValue = this.anexoJq04C401d;
        this.anexoJq04C401d = anexoJq04C401d;
        this.firePropertyChange("anexoJq04C401d", oldValue, this.anexoJq04C401d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C402a() {
        return this.anexoJq04C402a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C402a(Long anexoJq04C402a) {
        Long oldValue = this.anexoJq04C402a;
        this.anexoJq04C402a = anexoJq04C402a;
        this.firePropertyChange("anexoJq04C402a", oldValue, this.anexoJq04C402a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C402b() {
        return this.anexoJq04C402b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C402b(Long anexoJq04C402b) {
        Long oldValue = this.anexoJq04C402b;
        this.anexoJq04C402b = anexoJq04C402b;
        this.firePropertyChange("anexoJq04C402b", oldValue, this.anexoJq04C402b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C402c() {
        return this.anexoJq04C402c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C402c(Long anexoJq04C402c) {
        Long oldValue = this.anexoJq04C402c;
        this.anexoJq04C402c = anexoJq04C402c;
        this.firePropertyChange("anexoJq04C402c", oldValue, this.anexoJq04C402c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C402d() {
        return this.anexoJq04C402d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C402d(Long anexoJq04C402d) {
        Long oldValue = this.anexoJq04C402d;
        this.anexoJq04C402d = anexoJq04C402d;
        this.firePropertyChange("anexoJq04C402d", oldValue, this.anexoJq04C402d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C403b() {
        return this.anexoJq04C403b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C403b(Long anexoJq04C403b) {
        Long oldValue = this.anexoJq04C403b;
        this.anexoJq04C403b = anexoJq04C403b;
        this.firePropertyChange("anexoJq04C403b", oldValue, this.anexoJq04C403b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C403c() {
        return this.anexoJq04C403c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C403c(Long anexoJq04C403c) {
        Long oldValue = this.anexoJq04C403c;
        this.anexoJq04C403c = anexoJq04C403c;
        this.firePropertyChange("anexoJq04C403c", oldValue, this.anexoJq04C403c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C403d() {
        return this.anexoJq04C403d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C403d(Long anexoJq04C403d) {
        Long oldValue = this.anexoJq04C403d;
        this.anexoJq04C403d = anexoJq04C403d;
        this.firePropertyChange("anexoJq04C403d", oldValue, this.anexoJq04C403d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C404b() {
        return this.anexoJq04C404b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C404b(Long anexoJq04C404b) {
        Long oldValue = this.anexoJq04C404b;
        this.anexoJq04C404b = anexoJq04C404b;
        this.firePropertyChange("anexoJq04C404b", oldValue, this.anexoJq04C404b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C404c() {
        return this.anexoJq04C404c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C404c(Long anexoJq04C404c) {
        Long oldValue = this.anexoJq04C404c;
        this.anexoJq04C404c = anexoJq04C404c;
        this.firePropertyChange("anexoJq04C404c", oldValue, this.anexoJq04C404c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C404d() {
        return this.anexoJq04C404d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C404d(Long anexoJq04C404d) {
        Long oldValue = this.anexoJq04C404d;
        this.anexoJq04C404d = anexoJq04C404d;
        this.firePropertyChange("anexoJq04C404d", oldValue, this.anexoJq04C404d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C405b() {
        return this.anexoJq04C405b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C405b(Long anexoJq04C405b) {
        Long oldValue = this.anexoJq04C405b;
        this.anexoJq04C405b = anexoJq04C405b;
        this.firePropertyChange("anexoJq04C405b", oldValue, this.anexoJq04C405b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C405c() {
        return this.anexoJq04C405c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C405c(Long anexoJq04C405c) {
        Long oldValue = this.anexoJq04C405c;
        this.anexoJq04C405c = anexoJq04C405c;
        this.firePropertyChange("anexoJq04C405c", oldValue, this.anexoJq04C405c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C405d() {
        return this.anexoJq04C405d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C405d(Long anexoJq04C405d) {
        Long oldValue = this.anexoJq04C405d;
        this.anexoJq04C405d = anexoJq04C405d;
        this.firePropertyChange("anexoJq04C405d", oldValue, this.anexoJq04C405d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C406b() {
        return this.anexoJq04C406b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C406b(Long anexoJq04C406b) {
        Long oldValue = this.anexoJq04C406b;
        this.anexoJq04C406b = anexoJq04C406b;
        this.firePropertyChange("anexoJq04C406b", oldValue, this.anexoJq04C406b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C406c() {
        return this.anexoJq04C406c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C406c(Long anexoJq04C406c) {
        Long oldValue = this.anexoJq04C406c;
        this.anexoJq04C406c = anexoJq04C406c;
        this.firePropertyChange("anexoJq04C406c", oldValue, this.anexoJq04C406c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C406d() {
        return this.anexoJq04C406d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C406d(Long anexoJq04C406d) {
        Long oldValue = this.anexoJq04C406d;
        this.anexoJq04C406d = anexoJq04C406d;
        this.firePropertyChange("anexoJq04C406d", oldValue, this.anexoJq04C406d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C426b() {
        return this.anexoJq04C426b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C426b(Long anexoJq04C426b) {
        Long oldValue = this.anexoJq04C426b;
        this.anexoJq04C426b = anexoJq04C426b;
        this.firePropertyChange("anexoJq04C426b", oldValue, this.anexoJq04C426b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C426c() {
        return this.anexoJq04C426c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C426c(Long anexoJq04C426c) {
        Long oldValue = this.anexoJq04C426c;
        this.anexoJq04C426c = anexoJq04C426c;
        this.firePropertyChange("anexoJq04C426c", oldValue, this.anexoJq04C426c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C426d() {
        return this.anexoJq04C426d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C426d(Long anexoJq04C426d) {
        Long oldValue = this.anexoJq04C426d;
        this.anexoJq04C426d = anexoJq04C426d;
        this.firePropertyChange("anexoJq04C426d", oldValue, this.anexoJq04C426d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C407b() {
        return this.anexoJq04C407b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C407b(Long anexoJq04C407b) {
        Long oldValue = this.anexoJq04C407b;
        this.anexoJq04C407b = anexoJq04C407b;
        this.firePropertyChange("anexoJq04C407b", oldValue, this.anexoJq04C407b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C407c() {
        return this.anexoJq04C407c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C407c(Long anexoJq04C407c) {
        Long oldValue = this.anexoJq04C407c;
        this.anexoJq04C407c = anexoJq04C407c;
        this.firePropertyChange("anexoJq04C407c", oldValue, this.anexoJq04C407c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C407d() {
        return this.anexoJq04C407d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C407d(Long anexoJq04C407d) {
        Long oldValue = this.anexoJq04C407d;
        this.anexoJq04C407d = anexoJq04C407d;
        this.firePropertyChange("anexoJq04C407d", oldValue, this.anexoJq04C407d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C408b() {
        return this.anexoJq04C408b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C408b(Long anexoJq04C408b) {
        Long oldValue = this.anexoJq04C408b;
        this.anexoJq04C408b = anexoJq04C408b;
        this.firePropertyChange("anexoJq04C408b", oldValue, this.anexoJq04C408b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C408c() {
        return this.anexoJq04C408c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C408c(Long anexoJq04C408c) {
        Long oldValue = this.anexoJq04C408c;
        this.anexoJq04C408c = anexoJq04C408c;
        this.firePropertyChange("anexoJq04C408c", oldValue, this.anexoJq04C408c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C408d() {
        return this.anexoJq04C408d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C408d(Long anexoJq04C408d) {
        Long oldValue = this.anexoJq04C408d;
        this.anexoJq04C408d = anexoJq04C408d;
        this.firePropertyChange("anexoJq04C408d", oldValue, this.anexoJq04C408d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C409b() {
        return this.anexoJq04C409b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C409b(Long anexoJq04C409b) {
        Long oldValue = this.anexoJq04C409b;
        this.anexoJq04C409b = anexoJq04C409b;
        this.firePropertyChange("anexoJq04C409b", oldValue, this.anexoJq04C409b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C409c() {
        return this.anexoJq04C409c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C409c(Long anexoJq04C409c) {
        Long oldValue = this.anexoJq04C409c;
        this.anexoJq04C409c = anexoJq04C409c;
        this.firePropertyChange("anexoJq04C409c", oldValue, this.anexoJq04C409c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C409d() {
        return this.anexoJq04C409d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C409d(Long anexoJq04C409d) {
        Long oldValue = this.anexoJq04C409d;
        this.anexoJq04C409d = anexoJq04C409d;
        this.firePropertyChange("anexoJq04C409d", oldValue, this.anexoJq04C409d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C410b() {
        return this.anexoJq04C410b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C410b(Long anexoJq04C410b) {
        Long oldValue = this.anexoJq04C410b;
        this.anexoJq04C410b = anexoJq04C410b;
        this.firePropertyChange("anexoJq04C410b", oldValue, this.anexoJq04C410b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C410c() {
        return this.anexoJq04C410c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C410c(Long anexoJq04C410c) {
        Long oldValue = this.anexoJq04C410c;
        this.anexoJq04C410c = anexoJq04C410c;
        this.firePropertyChange("anexoJq04C410c", oldValue, this.anexoJq04C410c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C410d() {
        return this.anexoJq04C410d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C410d(Long anexoJq04C410d) {
        Long oldValue = this.anexoJq04C410d;
        this.anexoJq04C410d = anexoJq04C410d;
        this.firePropertyChange("anexoJq04C410d", oldValue, this.anexoJq04C410d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C411b() {
        return this.anexoJq04C411b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C411b(Long anexoJq04C411b) {
        Long oldValue = this.anexoJq04C411b;
        this.anexoJq04C411b = anexoJq04C411b;
        this.firePropertyChange("anexoJq04C411b", oldValue, this.anexoJq04C411b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C411c() {
        return this.anexoJq04C411c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C411c(Long anexoJq04C411c) {
        Long oldValue = this.anexoJq04C411c;
        this.anexoJq04C411c = anexoJq04C411c;
        this.firePropertyChange("anexoJq04C411c", oldValue, this.anexoJq04C411c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C411d() {
        return this.anexoJq04C411d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C411d(Long anexoJq04C411d) {
        Long oldValue = this.anexoJq04C411d;
        this.anexoJq04C411d = anexoJq04C411d;
        this.firePropertyChange("anexoJq04C411d", oldValue, this.anexoJq04C411d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C412b() {
        return this.anexoJq04C412b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C412b(Long anexoJq04C412b) {
        Long oldValue = this.anexoJq04C412b;
        this.anexoJq04C412b = anexoJq04C412b;
        this.firePropertyChange("anexoJq04C412b", oldValue, this.anexoJq04C412b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C412c() {
        return this.anexoJq04C412c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C412c(Long anexoJq04C412c) {
        Long oldValue = this.anexoJq04C412c;
        this.anexoJq04C412c = anexoJq04C412c;
        this.firePropertyChange("anexoJq04C412c", oldValue, this.anexoJq04C412c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C412d() {
        return this.anexoJq04C412d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C412d(Long anexoJq04C412d) {
        Long oldValue = this.anexoJq04C412d;
        this.anexoJq04C412d = anexoJq04C412d;
        this.firePropertyChange("anexoJq04C412d", oldValue, this.anexoJq04C412d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C425b() {
        return this.anexoJq04C425b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C425b(Long anexoJq04C425b) {
        Long oldValue = this.anexoJq04C425b;
        this.anexoJq04C425b = anexoJq04C425b;
        this.firePropertyChange("anexoJq04C425b", oldValue, this.anexoJq04C425b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C425c() {
        return this.anexoJq04C425c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C425c(Long anexoJq04C425c) {
        Long oldValue = this.anexoJq04C425c;
        this.anexoJq04C425c = anexoJq04C425c;
        this.firePropertyChange("anexoJq04C425c", oldValue, this.anexoJq04C425c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C425d() {
        return this.anexoJq04C425d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C425d(Long anexoJq04C425d) {
        Long oldValue = this.anexoJq04C425d;
        this.anexoJq04C425d = anexoJq04C425d;
        this.firePropertyChange("anexoJq04C425d", oldValue, this.anexoJq04C425d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C415b() {
        return this.anexoJq04C415b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C415b(Long anexoJq04C415b) {
        Long oldValue = this.anexoJq04C415b;
        this.anexoJq04C415b = anexoJq04C415b;
        this.firePropertyChange("anexoJq04C415b", oldValue, this.anexoJq04C415b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C415c() {
        return this.anexoJq04C415c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C415c(Long anexoJq04C415c) {
        Long oldValue = this.anexoJq04C415c;
        this.anexoJq04C415c = anexoJq04C415c;
        this.firePropertyChange("anexoJq04C415c", oldValue, this.anexoJq04C415c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C415d() {
        return this.anexoJq04C415d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C415d(Long anexoJq04C415d) {
        Long oldValue = this.anexoJq04C415d;
        this.anexoJq04C415d = anexoJq04C415d;
        this.firePropertyChange("anexoJq04C415d", oldValue, this.anexoJq04C415d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C416a() {
        return this.anexoJq04C416a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C416a(Long anexoJq04C416a) {
        Long oldValue = this.anexoJq04C416a;
        this.anexoJq04C416a = anexoJq04C416a;
        this.firePropertyChange("anexoJq04C416a", oldValue, this.anexoJq04C416a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C416b() {
        return this.anexoJq04C416b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C416b(Long anexoJq04C416b) {
        Long oldValue = this.anexoJq04C416b;
        this.anexoJq04C416b = anexoJq04C416b;
        this.firePropertyChange("anexoJq04C416b", oldValue, this.anexoJq04C416b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C416c() {
        return this.anexoJq04C416c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C416c(Long anexoJq04C416c) {
        Long oldValue = this.anexoJq04C416c;
        this.anexoJq04C416c = anexoJq04C416c;
        this.firePropertyChange("anexoJq04C416c", oldValue, this.anexoJq04C416c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C416d() {
        return this.anexoJq04C416d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C416d(Long anexoJq04C416d) {
        Long oldValue = this.anexoJq04C416d;
        this.anexoJq04C416d = anexoJq04C416d;
        this.firePropertyChange("anexoJq04C416d", oldValue, this.anexoJq04C416d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C417a() {
        return this.anexoJq04C417a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C417a(Long anexoJq04C417a) {
        Long oldValue = this.anexoJq04C417a;
        this.anexoJq04C417a = anexoJq04C417a;
        this.firePropertyChange("anexoJq04C417a", oldValue, this.anexoJq04C417a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C417b() {
        return this.anexoJq04C417b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C417b(Long anexoJq04C417b) {
        Long oldValue = this.anexoJq04C417b;
        this.anexoJq04C417b = anexoJq04C417b;
        this.firePropertyChange("anexoJq04C417b", oldValue, this.anexoJq04C417b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C417c() {
        return this.anexoJq04C417c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C417c(Long anexoJq04C417c) {
        Long oldValue = this.anexoJq04C417c;
        this.anexoJq04C417c = anexoJq04C417c;
        this.firePropertyChange("anexoJq04C417c", oldValue, this.anexoJq04C417c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C417d() {
        return this.anexoJq04C417d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C417d(Long anexoJq04C417d) {
        Long oldValue = this.anexoJq04C417d;
        this.anexoJq04C417d = anexoJq04C417d;
        this.firePropertyChange("anexoJq04C417d", oldValue, this.anexoJq04C417d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C418b() {
        return this.anexoJq04C418b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C418b(Long anexoJq04C418b) {
        Long oldValue = this.anexoJq04C418b;
        this.anexoJq04C418b = anexoJq04C418b;
        this.firePropertyChange("anexoJq04C418b", oldValue, this.anexoJq04C418b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C418c() {
        return this.anexoJq04C418c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C418c(Long anexoJq04C418c) {
        Long oldValue = this.anexoJq04C418c;
        this.anexoJq04C418c = anexoJq04C418c;
        this.firePropertyChange("anexoJq04C418c", oldValue, this.anexoJq04C418c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C418d() {
        return this.anexoJq04C418d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C418d(Long anexoJq04C418d) {
        Long oldValue = this.anexoJq04C418d;
        this.anexoJq04C418d = anexoJq04C418d;
        this.firePropertyChange("anexoJq04C418d", oldValue, this.anexoJq04C418d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C419b() {
        return this.anexoJq04C419b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C419b(Long anexoJq04C419b) {
        Long oldValue = this.anexoJq04C419b;
        this.anexoJq04C419b = anexoJq04C419b;
        this.firePropertyChange("anexoJq04C419b", oldValue, this.anexoJq04C419b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C419c() {
        return this.anexoJq04C419c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C419c(Long anexoJq04C419c) {
        Long oldValue = this.anexoJq04C419c;
        this.anexoJq04C419c = anexoJq04C419c;
        this.firePropertyChange("anexoJq04C419c", oldValue, this.anexoJq04C419c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C419d() {
        return this.anexoJq04C419d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C419d(Long anexoJq04C419d) {
        Long oldValue = this.anexoJq04C419d;
        this.anexoJq04C419d = anexoJq04C419d;
        this.firePropertyChange("anexoJq04C419d", oldValue, this.anexoJq04C419d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C420b() {
        return this.anexoJq04C420b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C420b(Long anexoJq04C420b) {
        Long oldValue = this.anexoJq04C420b;
        this.anexoJq04C420b = anexoJq04C420b;
        this.firePropertyChange("anexoJq04C420b", oldValue, this.anexoJq04C420b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C420c() {
        return this.anexoJq04C420c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C420c(Long anexoJq04C420c) {
        Long oldValue = this.anexoJq04C420c;
        this.anexoJq04C420c = anexoJq04C420c;
        this.firePropertyChange("anexoJq04C420c", oldValue, this.anexoJq04C420c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C420d() {
        return this.anexoJq04C420d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C420d(Long anexoJq04C420d) {
        Long oldValue = this.anexoJq04C420d;
        this.anexoJq04C420d = anexoJq04C420d;
        this.firePropertyChange("anexoJq04C420d", oldValue, this.anexoJq04C420d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C422b() {
        return this.anexoJq04C422b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C422b(Long anexoJq04C422b) {
        Long oldValue = this.anexoJq04C422b;
        this.anexoJq04C422b = anexoJq04C422b;
        this.firePropertyChange("anexoJq04C422b", oldValue, this.anexoJq04C422b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C422c() {
        return this.anexoJq04C422c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C422c(Long anexoJq04C422c) {
        Long oldValue = this.anexoJq04C422c;
        this.anexoJq04C422c = anexoJq04C422c;
        this.firePropertyChange("anexoJq04C422c", oldValue, this.anexoJq04C422c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C422d() {
        return this.anexoJq04C422d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C422d(Long anexoJq04C422d) {
        Long oldValue = this.anexoJq04C422d;
        this.anexoJq04C422d = anexoJq04C422d;
        this.firePropertyChange("anexoJq04C422d", oldValue, this.anexoJq04C422d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C423b() {
        return this.anexoJq04C423b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C423b(Long anexoJq04C423b) {
        Long oldValue = this.anexoJq04C423b;
        this.anexoJq04C423b = anexoJq04C423b;
        this.firePropertyChange("anexoJq04C423b", oldValue, this.anexoJq04C423b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C423c() {
        return this.anexoJq04C423c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C423c(Long anexoJq04C423c) {
        Long oldValue = this.anexoJq04C423c;
        this.anexoJq04C423c = anexoJq04C423c;
        this.firePropertyChange("anexoJq04C423c", oldValue, this.anexoJq04C423c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C423d() {
        return this.anexoJq04C423d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C423d(Long anexoJq04C423d) {
        Long oldValue = this.anexoJq04C423d;
        this.anexoJq04C423d = anexoJq04C423d;
        this.firePropertyChange("anexoJq04C423d", oldValue, this.anexoJq04C423d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C424b() {
        return this.anexoJq04C424b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C424b(Long anexoJq04C424b) {
        Long oldValue = this.anexoJq04C424b;
        this.anexoJq04C424b = anexoJq04C424b;
        this.firePropertyChange("anexoJq04C424b", oldValue, this.anexoJq04C424b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C424c() {
        return this.anexoJq04C424c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C424c(Long anexoJq04C424c) {
        Long oldValue = this.anexoJq04C424c;
        this.anexoJq04C424c = anexoJq04C424c;
        this.firePropertyChange("anexoJq04C424c", oldValue, this.anexoJq04C424c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C424d() {
        return this.anexoJq04C424d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C424d(Long anexoJq04C424d) {
        Long oldValue = this.anexoJq04C424d;
        this.anexoJq04C424d = anexoJq04C424d;
        this.firePropertyChange("anexoJq04C424d", oldValue, this.anexoJq04C424d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C1b() {
        return this.anexoJq04C1b;
    }

    public void setAnexoJq04C1bFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoJq04C1b")) {
            return;
        }
        long newValue = 0;
        this.setAnexoJq04C1b(newValue+=(this.getAnexoJq04C401b() == null ? 0 : this.getAnexoJq04C401b()) + (this.getAnexoJq04C402b() == null ? 0 : this.getAnexoJq04C402b()) + (this.getAnexoJq04C403b() == null ? 0 : this.getAnexoJq04C403b()) + (this.getAnexoJq04C404b() == null ? 0 : this.getAnexoJq04C404b()) + (this.getAnexoJq04C405b() == null ? 0 : this.getAnexoJq04C405b()) + (this.getAnexoJq04C406b() == null ? 0 : this.getAnexoJq04C406b()) + (this.getAnexoJq04C407b() == null ? 0 : this.getAnexoJq04C407b()) + (this.getAnexoJq04C408b() == null ? 0 : this.getAnexoJq04C408b()) + (this.getAnexoJq04C409b() == null ? 0 : this.getAnexoJq04C409b()) + (this.getAnexoJq04C410b() == null ? 0 : this.getAnexoJq04C410b()) + (this.getAnexoJq04C411b() == null ? 0 : this.getAnexoJq04C411b()) + (this.getAnexoJq04C412b() == null ? 0 : this.getAnexoJq04C412b()) + (this.getAnexoJq04C425b() == null ? 0 : this.getAnexoJq04C425b()) + (this.getAnexoJq04C415b() == null ? 0 : this.getAnexoJq04C415b()) + (this.getAnexoJq04C416b() == null ? 0 : this.getAnexoJq04C416b()) + (this.getAnexoJq04C417b() == null ? 0 : this.getAnexoJq04C417b()) + (this.getAnexoJq04C418b() == null ? 0 : this.getAnexoJq04C418b()) + (this.getAnexoJq04C419b() == null ? 0 : this.getAnexoJq04C419b()) + (this.getAnexoJq04C420b() == null ? 0 : this.getAnexoJq04C420b()) + (this.getAnexoJq04C422b() == null ? 0 : this.getAnexoJq04C422b()) + (this.getAnexoJq04C423b() == null ? 0 : this.getAnexoJq04C423b()) + (this.getAnexoJq04C424b() == null ? 0 : this.getAnexoJq04C424b()) + (this.getAnexoJq04C426b() == null ? 0 : this.getAnexoJq04C426b()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C1b(Long anexoJq04C1b) {
        Long oldValue = this.anexoJq04C1b;
        this.anexoJq04C1b = anexoJq04C1b;
        this.firePropertyChange("anexoJq04C1b", oldValue, this.anexoJq04C1b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C1c() {
        return this.anexoJq04C1c;
    }

    public void setAnexoJq04C1cFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoJq04C1c")) {
            return;
        }
        long newValue = 0;
        this.setAnexoJq04C1c(newValue+=(this.getAnexoJq04C401c() == null ? 0 : this.getAnexoJq04C401c()) + (this.getAnexoJq04C402c() == null ? 0 : this.getAnexoJq04C402c()) + (this.getAnexoJq04C403c() == null ? 0 : this.getAnexoJq04C403c()) + (this.getAnexoJq04C404c() == null ? 0 : this.getAnexoJq04C404c()) + (this.getAnexoJq04C405c() == null ? 0 : this.getAnexoJq04C405c()) + (this.getAnexoJq04C406c() == null ? 0 : this.getAnexoJq04C406c()) + (this.getAnexoJq04C407c() == null ? 0 : this.getAnexoJq04C407c()) + (this.getAnexoJq04C408c() == null ? 0 : this.getAnexoJq04C408c()) + (this.getAnexoJq04C409c() == null ? 0 : this.getAnexoJq04C409c()) + (this.getAnexoJq04C410c() == null ? 0 : this.getAnexoJq04C410c()) + (this.getAnexoJq04C411c() == null ? 0 : this.getAnexoJq04C411c()) + (this.getAnexoJq04C412c() == null ? 0 : this.getAnexoJq04C412c()) + (this.getAnexoJq04C425c() == null ? 0 : this.getAnexoJq04C425c()) + (this.getAnexoJq04C415c() == null ? 0 : this.getAnexoJq04C415c()) + (this.getAnexoJq04C416c() == null ? 0 : this.getAnexoJq04C416c()) + (this.getAnexoJq04C417c() == null ? 0 : this.getAnexoJq04C417c()) + (this.getAnexoJq04C418c() == null ? 0 : this.getAnexoJq04C418c()) + (this.getAnexoJq04C419c() == null ? 0 : this.getAnexoJq04C419c()) + (this.getAnexoJq04C420c() == null ? 0 : this.getAnexoJq04C420c()) + (this.getAnexoJq04C422c() == null ? 0 : this.getAnexoJq04C422c()) + (this.getAnexoJq04C423c() == null ? 0 : this.getAnexoJq04C423c()) + (this.getAnexoJq04C424c() == null ? 0 : this.getAnexoJq04C424c()) + (this.getAnexoJq04C426c() == null ? 0 : this.getAnexoJq04C426c()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C1c(Long anexoJq04C1c) {
        Long oldValue = this.anexoJq04C1c;
        this.anexoJq04C1c = anexoJq04C1c;
        this.firePropertyChange("anexoJq04C1c", oldValue, this.anexoJq04C1c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C1d() {
        return this.anexoJq04C1d;
    }

    public void setAnexoJq04C1dFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoJq04C1d")) {
            return;
        }
        long newValue = 0;
        this.setAnexoJq04C1d(newValue+=(this.getAnexoJq04C401d() == null ? 0 : this.getAnexoJq04C401d()) + (this.getAnexoJq04C402d() == null ? 0 : this.getAnexoJq04C402d()) + (this.getAnexoJq04C403d() == null ? 0 : this.getAnexoJq04C403d()) + (this.getAnexoJq04C404d() == null ? 0 : this.getAnexoJq04C404d()) + (this.getAnexoJq04C405d() == null ? 0 : this.getAnexoJq04C405d()) + (this.getAnexoJq04C406d() == null ? 0 : this.getAnexoJq04C406d()) + (this.getAnexoJq04C407d() == null ? 0 : this.getAnexoJq04C407d()) + (this.getAnexoJq04C408d() == null ? 0 : this.getAnexoJq04C408d()) + (this.getAnexoJq04C409d() == null ? 0 : this.getAnexoJq04C409d()) + (this.getAnexoJq04C410d() == null ? 0 : this.getAnexoJq04C410d()) + (this.getAnexoJq04C411d() == null ? 0 : this.getAnexoJq04C411d()) + (this.getAnexoJq04C412d() == null ? 0 : this.getAnexoJq04C412d()) + (this.getAnexoJq04C425d() == null ? 0 : this.getAnexoJq04C425d()) + (this.getAnexoJq04C415d() == null ? 0 : this.getAnexoJq04C415d()) + (this.getAnexoJq04C416d() == null ? 0 : this.getAnexoJq04C416d()) + (this.getAnexoJq04C417d() == null ? 0 : this.getAnexoJq04C417d()) + (this.getAnexoJq04C418d() == null ? 0 : this.getAnexoJq04C418d()) + (this.getAnexoJq04C419d() == null ? 0 : this.getAnexoJq04C419d()) + (this.getAnexoJq04C420d() == null ? 0 : this.getAnexoJq04C420d()) + (this.getAnexoJq04C422d() == null ? 0 : this.getAnexoJq04C422d()) + (this.getAnexoJq04C423d() == null ? 0 : this.getAnexoJq04C423d()) + (this.getAnexoJq04C424d() == null ? 0 : this.getAnexoJq04C424d()) + (this.getAnexoJq04C426d() == null ? 0 : this.getAnexoJq04C426d()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C1d(Long anexoJq04C1d) {
        Long oldValue = this.anexoJq04C1d;
        this.anexoJq04C1d = anexoJq04C1d;
        this.firePropertyChange("anexoJq04C1d", oldValue, this.anexoJq04C1d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq04C421() {
        return this.anexoJq04C421;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq04C421(Long anexoJq04C421) {
        Long oldValue = this.anexoJq04C421;
        this.anexoJq04C421 = anexoJq04C421;
        this.firePropertyChange("anexoJq04C421", oldValue, this.anexoJq04C421);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoJq04T1_Linha> getAnexoJq04T1() {
        return this.anexoJq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoJq04T1(EventList<AnexoJq04T1_Linha> anexoJq04T1) {
        this.anexoJq04T1 = anexoJq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoJq04T2_Linha> getAnexoJq04T2() {
        return this.anexoJq04T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoJq04T2(EventList<AnexoJq04T2_Linha> anexoJq04T2) {
        this.anexoJq04T2 = anexoJq04T2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoJq04B1() {
        return this.anexoJq04B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoJq04B1(Boolean anexoJq04B1) {
        Boolean oldValue = this.anexoJq04B1;
        this.anexoJq04B1 = anexoJq04B1;
        this.firePropertyChange("anexoJq04B1", oldValue, this.anexoJq04B1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoJq04C401a);
        result = HashCodeUtil.hash(result, this.anexoJq04C401b);
        result = HashCodeUtil.hash(result, this.anexoJq04C401c);
        result = HashCodeUtil.hash(result, this.anexoJq04C401d);
        result = HashCodeUtil.hash(result, this.anexoJq04C402a);
        result = HashCodeUtil.hash(result, this.anexoJq04C402b);
        result = HashCodeUtil.hash(result, this.anexoJq04C402c);
        result = HashCodeUtil.hash(result, this.anexoJq04C402d);
        result = HashCodeUtil.hash(result, this.anexoJq04C403b);
        result = HashCodeUtil.hash(result, this.anexoJq04C403c);
        result = HashCodeUtil.hash(result, this.anexoJq04C403d);
        result = HashCodeUtil.hash(result, this.anexoJq04C404b);
        result = HashCodeUtil.hash(result, this.anexoJq04C404c);
        result = HashCodeUtil.hash(result, this.anexoJq04C404d);
        result = HashCodeUtil.hash(result, this.anexoJq04C405b);
        result = HashCodeUtil.hash(result, this.anexoJq04C405c);
        result = HashCodeUtil.hash(result, this.anexoJq04C405d);
        result = HashCodeUtil.hash(result, this.anexoJq04C406b);
        result = HashCodeUtil.hash(result, this.anexoJq04C406c);
        result = HashCodeUtil.hash(result, this.anexoJq04C406d);
        result = HashCodeUtil.hash(result, this.anexoJq04C426b);
        result = HashCodeUtil.hash(result, this.anexoJq04C426c);
        result = HashCodeUtil.hash(result, this.anexoJq04C426d);
        result = HashCodeUtil.hash(result, this.anexoJq04C407b);
        result = HashCodeUtil.hash(result, this.anexoJq04C407c);
        result = HashCodeUtil.hash(result, this.anexoJq04C407d);
        result = HashCodeUtil.hash(result, this.anexoJq04C408b);
        result = HashCodeUtil.hash(result, this.anexoJq04C408c);
        result = HashCodeUtil.hash(result, this.anexoJq04C408d);
        result = HashCodeUtil.hash(result, this.anexoJq04C409b);
        result = HashCodeUtil.hash(result, this.anexoJq04C409c);
        result = HashCodeUtil.hash(result, this.anexoJq04C409d);
        result = HashCodeUtil.hash(result, this.anexoJq04C410b);
        result = HashCodeUtil.hash(result, this.anexoJq04C410c);
        result = HashCodeUtil.hash(result, this.anexoJq04C410d);
        result = HashCodeUtil.hash(result, this.anexoJq04C411b);
        result = HashCodeUtil.hash(result, this.anexoJq04C411c);
        result = HashCodeUtil.hash(result, this.anexoJq04C411d);
        result = HashCodeUtil.hash(result, this.anexoJq04C412b);
        result = HashCodeUtil.hash(result, this.anexoJq04C412c);
        result = HashCodeUtil.hash(result, this.anexoJq04C412d);
        result = HashCodeUtil.hash(result, this.anexoJq04C425b);
        result = HashCodeUtil.hash(result, this.anexoJq04C425c);
        result = HashCodeUtil.hash(result, this.anexoJq04C425d);
        result = HashCodeUtil.hash(result, this.anexoJq04C415b);
        result = HashCodeUtil.hash(result, this.anexoJq04C415c);
        result = HashCodeUtil.hash(result, this.anexoJq04C415d);
        result = HashCodeUtil.hash(result, this.anexoJq04C416a);
        result = HashCodeUtil.hash(result, this.anexoJq04C416b);
        result = HashCodeUtil.hash(result, this.anexoJq04C416c);
        result = HashCodeUtil.hash(result, this.anexoJq04C416d);
        result = HashCodeUtil.hash(result, this.anexoJq04C417a);
        result = HashCodeUtil.hash(result, this.anexoJq04C417b);
        result = HashCodeUtil.hash(result, this.anexoJq04C417c);
        result = HashCodeUtil.hash(result, this.anexoJq04C417d);
        result = HashCodeUtil.hash(result, this.anexoJq04C418b);
        result = HashCodeUtil.hash(result, this.anexoJq04C418c);
        result = HashCodeUtil.hash(result, this.anexoJq04C418d);
        result = HashCodeUtil.hash(result, this.anexoJq04C419b);
        result = HashCodeUtil.hash(result, this.anexoJq04C419c);
        result = HashCodeUtil.hash(result, this.anexoJq04C419d);
        result = HashCodeUtil.hash(result, this.anexoJq04C420b);
        result = HashCodeUtil.hash(result, this.anexoJq04C420c);
        result = HashCodeUtil.hash(result, this.anexoJq04C420d);
        result = HashCodeUtil.hash(result, this.anexoJq04C422b);
        result = HashCodeUtil.hash(result, this.anexoJq04C422c);
        result = HashCodeUtil.hash(result, this.anexoJq04C422d);
        result = HashCodeUtil.hash(result, this.anexoJq04C423b);
        result = HashCodeUtil.hash(result, this.anexoJq04C423c);
        result = HashCodeUtil.hash(result, this.anexoJq04C423d);
        result = HashCodeUtil.hash(result, this.anexoJq04C424b);
        result = HashCodeUtil.hash(result, this.anexoJq04C424c);
        result = HashCodeUtil.hash(result, this.anexoJq04C424d);
        result = HashCodeUtil.hash(result, this.anexoJq04C1b);
        result = HashCodeUtil.hash(result, this.anexoJq04C1c);
        result = HashCodeUtil.hash(result, this.anexoJq04C1d);
        result = HashCodeUtil.hash(result, this.anexoJq04C421);
        result = HashCodeUtil.hash(result, this.anexoJq04T1);
        result = HashCodeUtil.hash(result, this.anexoJq04T2);
        result = HashCodeUtil.hash(result, this.anexoJq04B1);
        return result;
    }
}

