/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoJ.qQuadro06";
    public static final String ANEXOJQ06T1_LINK = "aAnexoJ.qQuadro06.tanexoJq06T1";
    private EventList<AnexoJq06T1_Linha> anexoJq06T1 = new BasicEventList<AnexoJq06T1_Linha>();
    public static final String ANEXOJQ06C1_LINK = "aAnexoJ.qQuadro06.fanexoJq06C1";
    public static final String ANEXOJQ06C1 = "anexoJq06C1";
    private Long anexoJq06C1;
    public static final String ANEXOJQ06C2_LINK = "aAnexoJ.qQuadro06.fanexoJq06C2";
    public static final String ANEXOJQ06C2 = "anexoJq06C2";
    private Long anexoJq06C2;
    public static final String ANEXOJQ06C3_LINK = "aAnexoJ.qQuadro06.fanexoJq06C3";
    public static final String ANEXOJQ06C3 = "anexoJq06C3";
    private Long anexoJq06C3;
    public static final String ANEXOJQ06C4OP1_LINK = "aAnexoJ.qQuadro06.fanexoJq06C4OP1";
    public static final String ANEXOJQ06C4OP1_VALUE = "1";
    public static final String ANEXOJQ06C4OP2_LINK = "aAnexoJ.qQuadro06.fanexoJq06C4OP2";
    public static final String ANEXOJQ06C4OP2_VALUE = "2";
    public static final String ANEXOJQ06C4OP3_LINK = "aAnexoJ.qQuadro06.fanexoJq06C4OP3";
    public static final String ANEXOJQ06C4OP3_VALUE = "3";
    public static final String ANEXOJQ06C4 = "anexoJq06C4";
    private String anexoJq06C4;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoJq06T1_Linha> getAnexoJq06T1() {
        return this.anexoJq06T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoJq06T1(EventList<AnexoJq06T1_Linha> anexoJq06T1) {
        this.anexoJq06T1 = anexoJq06T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq06C1() {
        return this.anexoJq06C1;
    }

    public void setAnexoJq06C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoJq06C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoJq06T1().size(); ++i) {
            temporaryValue0+=this.getAnexoJq06T1().get(i).getMontanteRendimento() == null ? 0 : this.getAnexoJq06T1().get(i).getMontanteRendimento();
        }
        this.setAnexoJq06C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq06C1(Long anexoJq06C1) {
        Long oldValue = this.anexoJq06C1;
        this.anexoJq06C1 = anexoJq06C1;
        this.firePropertyChange("anexoJq06C1", oldValue, this.anexoJq06C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq06C2() {
        return this.anexoJq06C2;
    }

    public void setAnexoJq06C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoJq06C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoJq06T1().size(); ++i) {
            temporaryValue0+=this.getAnexoJq06T1().get(i).getPaisDaFonteValor() == null ? 0 : this.getAnexoJq06T1().get(i).getPaisDaFonteValor();
        }
        this.setAnexoJq06C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq06C2(Long anexoJq06C2) {
        Long oldValue = this.anexoJq06C2;
        this.anexoJq06C2 = anexoJq06C2;
        this.firePropertyChange("anexoJq06C2", oldValue, this.anexoJq06C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoJq06C3() {
        return this.anexoJq06C3;
    }

    public void setAnexoJq06C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoJq06C3")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoJq06T1().size(); ++i) {
            temporaryValue0+=this.getAnexoJq06T1().get(i).getPaisAgentePagadorValor() == null ? 0 : this.getAnexoJq06T1().get(i).getPaisAgentePagadorValor();
        }
        this.setAnexoJq06C3(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoJq06C3(Long anexoJq06C3) {
        Long oldValue = this.anexoJq06C3;
        this.anexoJq06C3 = anexoJq06C3;
        this.firePropertyChange("anexoJq06C3", oldValue, this.anexoJq06C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoJq06C4() {
        return this.anexoJq06C4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoJq06C4(String anexoJq06C4) {
        String oldValue = this.anexoJq06C4;
        this.anexoJq06C4 = anexoJq06C4;
        this.firePropertyChange("anexoJq06C4", oldValue, this.anexoJq06C4);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoJq06T1);
        result = HashCodeUtil.hash(result, this.anexoJq06C1);
        result = HashCodeUtil.hash(result, this.anexoJq06C2);
        result = HashCodeUtil.hash(result, this.anexoJq06C3);
        result = HashCodeUtil.hash(result, this.anexoJq06C4);
        return result;
    }
}

