/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoJq08T1_LinhaBase
extends Model {
    public static final String NIFENTIDADERETENTORA_LINK = "aAnexoJ.qQuadro08.fnifEntidadeRetentora";
    public static final String NIFENTIDADERETENTORA = "nifEntidadeRetentora";
    private Long nifEntidadeRetentora;
    public static final String CAMPOQ4_LINK = "aAnexoJ.qQuadro08.fcampoQ4";
    public static final String CAMPOQ4 = "campoQ4";
    private Long campoQ4;
    public static final String RENDIMENTOS_LINK = "aAnexoJ.qQuadro08.frendimentos";
    public static final String RENDIMENTOS = "rendimentos";
    private Long rendimentos;
    public static final String RETENCOESIRS_LINK = "aAnexoJ.qQuadro08.fretencoesIRS";
    public static final String RETENCOESIRS = "retencoesIRS";
    private Long retencoesIRS;
    public static final String RETENCAOSOBRETAXA_LINK = "aAnexoJ.qQuadro08.fretencaoSobretaxa";
    public static final String RETENCAOSOBRETAXA = "retencaoSobretaxa";
    private Long retencaoSobretaxa;

    public AnexoJq08T1_LinhaBase(Long nifEntidadeRetentora, Long campoQ4, Long rendimentos, Long retencoesIRS, Long retencaoSobretaxa) {
        this.nifEntidadeRetentora = nifEntidadeRetentora;
        this.campoQ4 = campoQ4;
        this.rendimentos = rendimentos;
        this.retencoesIRS = retencoesIRS;
        this.retencaoSobretaxa = retencaoSobretaxa;
    }

    public AnexoJq08T1_LinhaBase() {
        this.nifEntidadeRetentora = null;
        this.campoQ4 = null;
        this.rendimentos = null;
        this.retencoesIRS = null;
        this.retencaoSobretaxa = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifEntidadeRetentora() {
        return this.nifEntidadeRetentora;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifEntidadeRetentora(Long nifEntidadeRetentora) {
        Long oldValue = this.nifEntidadeRetentora;
        this.nifEntidadeRetentora = nifEntidadeRetentora;
        this.firePropertyChange("nifEntidadeRetentora", oldValue, this.nifEntidadeRetentora);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoJQ4")
    public Long getCampoQ4() {
        return this.campoQ4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoQ4(Long campoQ4) {
        Long oldValue = this.campoQ4;
        this.campoQ4 = campoQ4;
        this.firePropertyChange("campoQ4", oldValue, this.campoQ4);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentos() {
        return this.rendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentos(Long rendimentos) {
        Long oldValue = this.rendimentos;
        this.rendimentos = rendimentos;
        this.firePropertyChange("rendimentos", oldValue, this.rendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencoesIRS() {
        return this.retencoesIRS;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencoesIRS(Long retencoesIRS) {
        Long oldValue = this.retencoesIRS;
        this.retencoesIRS = retencoesIRS;
        this.firePropertyChange("retencoesIRS", oldValue, this.retencoesIRS);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencaoSobretaxa() {
        return this.retencaoSobretaxa;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencaoSobretaxa(Long retencaoSobretaxa) {
        Long oldValue = this.retencaoSobretaxa;
        this.retencaoSobretaxa = retencaoSobretaxa;
        this.firePropertyChange("retencaoSobretaxa", oldValue, this.retencaoSobretaxa);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nifEntidadeRetentora);
        result = HashCodeUtil.hash(result, this.campoQ4);
        result = HashCodeUtil.hash(result, this.rendimentos);
        result = HashCodeUtil.hash(result, this.retencoesIRS);
        result = HashCodeUtil.hash(result, this.retencaoSobretaxa);
        return result;
    }

    public static enum Property {
        NIFENTIDADERETENTORA("nifEntidadeRetentora", 2),
        CAMPOQ4("campoQ4", 3),
        RENDIMENTOS("rendimentos", 4),
        RETENCOESIRS("retencoesIRS", 5),
        RETENCAOSOBRETAXA("retencaoSobretaxa", 6);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

