/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoJ.qQuadro02";
    public static final String ANEXOJQ02C01_LINK = "aAnexoJ.qQuadro02.fanexoJq02C01";
    public static final String ANEXOJQ02C01 = "anexoJq02C01";
    private Long anexoJq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoJq02C01() {
        return this.anexoJq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoJq02C01(Long anexoJq02C01) {
        Long oldValue = this.anexoJq02C01;
        this.anexoJq02C01 = anexoJq02C01;
        this.firePropertyChange("anexoJq02C01", oldValue, this.anexoJq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoJq02C01);
        return result;
    }
}

