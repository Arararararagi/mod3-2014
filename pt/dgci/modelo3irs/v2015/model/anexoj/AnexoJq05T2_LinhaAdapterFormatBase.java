/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq05T2_Linha;

public class AnexoJq05T2_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoJq05T2_Linha>,
WritableTableFormat<AnexoJq05T2_Linha> {
    @Override
    public boolean isEditable(AnexoJq05T2_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoJq05T2_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getOutrosNumIdentificacao();
            }
        }
        return null;
    }

    @Override
    public AnexoJq05T2_Linha setColumnValue(AnexoJq05T2_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setOutrosNumIdentificacao((String)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Outros N\u00fameros de Identifica\u00e7\u00e3o";
            }
        }
        return null;
    }
}

