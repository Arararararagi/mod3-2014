/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoJ.qQuadro03";
    public static final String ANEXOJQ03C02_LINK = "aAnexoJ.qQuadro03.fanexoJq03C02";
    public static final String ANEXOJQ03C02 = "anexoJq03C02";
    private Long anexoJq03C02;
    public static final String ANEXOJQ03C03_LINK = "aAnexoJ.qQuadro03.fanexoJq03C03";
    public static final String ANEXOJQ03C03 = "anexoJq03C03";
    private Long anexoJq03C03;
    public static final String ANEXOJQ03C04_LINK = "aAnexoJ.qQuadro03.fanexoJq03C04";
    public static final String ANEXOJQ03C04 = "anexoJq03C04";
    private Long anexoJq03C04;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoJq03C02() {
        return this.anexoJq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoJq03C02(Long anexoJq03C02) {
        Long oldValue = this.anexoJq03C02;
        this.anexoJq03C02 = anexoJq03C02;
        this.firePropertyChange("anexoJq03C02", oldValue, this.anexoJq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoJq03C03() {
        return this.anexoJq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoJq03C03(Long anexoJq03C03) {
        Long oldValue = this.anexoJq03C03;
        this.anexoJq03C03 = anexoJq03C03;
        this.firePropertyChange("anexoJq03C03", oldValue, this.anexoJq03C03);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoJq03C04() {
        return this.anexoJq03C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoJq03C04(Long anexoJq03C04) {
        Long oldValue = this.anexoJq03C04;
        this.anexoJq03C04 = anexoJq03C04;
        this.firePropertyChange("anexoJq03C04", oldValue, this.anexoJq03C04);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoJq03C02);
        result = HashCodeUtil.hash(result, this.anexoJq03C03);
        result = HashCodeUtil.hash(result, this.anexoJq03C04);
        return result;
    }
}

