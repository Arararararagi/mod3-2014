/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq08T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoJq08T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoJq08T1_Linha>,
WritableTableFormat<AnexoJq08T1_Linha> {
    @Override
    public boolean isEditable(AnexoJq08T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoJq08T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNifEntidadeRetentora();
            }
            case 2: {
                if (line == null || line.getCampoQ4() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoJQ4", line.getCampoQ4());
            }
            case 3: {
                return line.getRendimentos();
            }
            case 4: {
                return line.getRetencoesIRS();
            }
            case 5: {
                return line.getRetencaoSobretaxa();
            }
        }
        return null;
    }

    @Override
    public AnexoJq08T1_Linha setColumnValue(AnexoJq08T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNifEntidadeRetentora((Long)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCampoQ4((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setRendimentos((Long)value);
                return line;
            }
            case 4: {
                line.setRetencoesIRS((Long)value);
                return line;
            }
            case 5: {
                line.setRetencaoSobretaxa((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIPC da Entidade";
            }
            case 2: {
                return "Campos do Q4";
            }
            case 3: {
                return "Rendimento";
            }
            case 4: {
                return "Reten\u00e7\u00e3o de IRS";
            }
            case 5: {
                return "Reten\u00e7\u00e3o de Sobretaxa";
            }
        }
        return null;
    }
}

