/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoj;

import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJq06T1_LinhaBase;

public class AnexoJq06T1_Linha
extends AnexoJq06T1_LinhaBase {
    public AnexoJq06T1_Linha() {
    }

    public AnexoJq06T1_Linha(Long nLinha, Long campoQ4, String instalacaoFixa, Long identificacaoPaisCodPais, Long montanteRendimento, Long paisDaFonteValor, Long paisAgentePagadorCodPais, Long paisAgentePagadorValor) {
        super(nLinha, campoQ4, instalacaoFixa, identificacaoPaisCodPais, montanteRendimento, paisDaFonteValor, paisAgentePagadorCodPais, paisAgentePagadorValor);
    }

    public static String getLink(int numLinha) {
        return "aAnexoJ.qQuadro06.tanexoJq06T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoJq06T1_LinhaBase.Property column) {
        return "aAnexoJ.qQuadro06.tanexoJq06T1.l" + numLinha + ".c" + column.getIndex();
    }
}

