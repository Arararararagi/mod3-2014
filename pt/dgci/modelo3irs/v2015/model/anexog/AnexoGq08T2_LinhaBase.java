/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoGq08T2_LinhaBase
extends Model {
    public static final String CAMPOQ8_LINK = "aAnexoG.qQuadro08.fcampoQ8";
    public static final String CAMPOQ8 = "campoQ8";
    private Long campoQ8;
    public static final String NIPC_LINK = "aAnexoG.qQuadro08.fnipc";
    public static final String NIPC = "nipc";
    private Long nipc;

    public AnexoGq08T2_LinhaBase(Long campoQ8, Long nipc) {
        this.campoQ8 = campoQ8;
        this.nipc = nipc;
    }

    public AnexoGq08T2_LinhaBase() {
        this.campoQ8 = null;
        this.nipc = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ8")
    public Long getCampoQ8() {
        return this.campoQ8;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoQ8(Long campoQ8) {
        Long oldValue = this.campoQ8;
        this.campoQ8 = campoQ8;
        this.firePropertyChange("campoQ8", oldValue, this.campoQ8);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNipc() {
        return this.nipc;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNipc(Long nipc) {
        Long oldValue = this.nipc;
        this.nipc = nipc;
        this.firePropertyChange("nipc", oldValue, this.nipc);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.campoQ8);
        result = HashCodeUtil.hash(result, this.nipc);
        return result;
    }

    public static enum Property {
        CAMPOQ8("campoQ8", 2),
        NIPC("nipc", 3);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

