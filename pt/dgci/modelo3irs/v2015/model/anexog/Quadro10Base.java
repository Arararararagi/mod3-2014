/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro10Base
extends QuadroModel {
    public static final String QUADRO10_LINK = "aAnexoG.qQuadro10";
    public static final String ANEXOGQ10C1001A_LINK = "aAnexoG.qQuadro10.fanexoGq10C1001a";
    public static final String ANEXOGQ10C1001A = "anexoGq10C1001a";
    private String anexoGq10C1001a;
    public static final String ANEXOGQ10C1001B_LINK = "aAnexoG.qQuadro10.fanexoGq10C1001b";
    public static final String ANEXOGQ10C1001B = "anexoGq10C1001b";
    private Long anexoGq10C1001b;
    public static final String ANEXOGQ10C1001C_LINK = "aAnexoG.qQuadro10.fanexoGq10C1001c";
    public static final String ANEXOGQ10C1001C = "anexoGq10C1001c";
    private Long anexoGq10C1001c;
    public static final String ANEXOGQ10C1002A_LINK = "aAnexoG.qQuadro10.fanexoGq10C1002a";
    public static final String ANEXOGQ10C1002A = "anexoGq10C1002a";
    private String anexoGq10C1002a;
    public static final String ANEXOGQ10C1002B_LINK = "aAnexoG.qQuadro10.fanexoGq10C1002b";
    public static final String ANEXOGQ10C1002B = "anexoGq10C1002b";
    private Long anexoGq10C1002b;
    public static final String ANEXOGQ10C1002C_LINK = "aAnexoG.qQuadro10.fanexoGq10C1002c";
    public static final String ANEXOGQ10C1002C = "anexoGq10C1002c";
    private Long anexoGq10C1002c;
    public static final String ANEXOGQ10C1B_LINK = "aAnexoG.qQuadro10.fanexoGq10C1b";
    public static final String ANEXOGQ10C1B = "anexoGq10C1b";
    private Long anexoGq10C1b;
    public static final String ANEXOGQ10C1C_LINK = "aAnexoG.qQuadro10.fanexoGq10C1c";
    public static final String ANEXOGQ10C1C = "anexoGq10C1c";
    private Long anexoGq10C1c;
    public static final String ANEXOGQ10T1_LINK = "aAnexoG.qQuadro10.tanexoGq10T1";
    private EventList<AnexoGq10T1_Linha> anexoGq10T1 = new BasicEventList<AnexoGq10T1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq10C1001a() {
        return this.anexoGq10C1001a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq10C1001a(String anexoGq10C1001a) {
        String oldValue = this.anexoGq10C1001a;
        this.anexoGq10C1001a = anexoGq10C1001a;
        this.firePropertyChange("anexoGq10C1001a", oldValue, this.anexoGq10C1001a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq10C1001b() {
        return this.anexoGq10C1001b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq10C1001b(Long anexoGq10C1001b) {
        Long oldValue = this.anexoGq10C1001b;
        this.anexoGq10C1001b = anexoGq10C1001b;
        this.firePropertyChange("anexoGq10C1001b", oldValue, this.anexoGq10C1001b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq10C1001c() {
        return this.anexoGq10C1001c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq10C1001c(Long anexoGq10C1001c) {
        Long oldValue = this.anexoGq10C1001c;
        this.anexoGq10C1001c = anexoGq10C1001c;
        this.firePropertyChange("anexoGq10C1001c", oldValue, this.anexoGq10C1001c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq10C1002a() {
        return this.anexoGq10C1002a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq10C1002a(String anexoGq10C1002a) {
        String oldValue = this.anexoGq10C1002a;
        this.anexoGq10C1002a = anexoGq10C1002a;
        this.firePropertyChange("anexoGq10C1002a", oldValue, this.anexoGq10C1002a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq10C1002b() {
        return this.anexoGq10C1002b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq10C1002b(Long anexoGq10C1002b) {
        Long oldValue = this.anexoGq10C1002b;
        this.anexoGq10C1002b = anexoGq10C1002b;
        this.firePropertyChange("anexoGq10C1002b", oldValue, this.anexoGq10C1002b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq10C1002c() {
        return this.anexoGq10C1002c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq10C1002c(Long anexoGq10C1002c) {
        Long oldValue = this.anexoGq10C1002c;
        this.anexoGq10C1002c = anexoGq10C1002c;
        this.firePropertyChange("anexoGq10C1002c", oldValue, this.anexoGq10C1002c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq10C1b() {
        return this.anexoGq10C1b;
    }

    public void setAnexoGq10C1bFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq10C1b")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq10C1b(newValue+=(this.getAnexoGq10C1001b() == null ? 0 : this.getAnexoGq10C1001b()) + (this.getAnexoGq10C1002b() == null ? 0 : this.getAnexoGq10C1002b()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq10C1b(Long anexoGq10C1b) {
        Long oldValue = this.anexoGq10C1b;
        this.anexoGq10C1b = anexoGq10C1b;
        this.firePropertyChange("anexoGq10C1b", oldValue, this.anexoGq10C1b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq10C1c() {
        return this.anexoGq10C1c;
    }

    public void setAnexoGq10C1cFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq10C1c")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq10C1c(newValue+=(this.getAnexoGq10C1001c() == null ? 0 : this.getAnexoGq10C1001c()) + (this.getAnexoGq10C1002c() == null ? 0 : this.getAnexoGq10C1002c()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq10C1c(Long anexoGq10C1c) {
        Long oldValue = this.anexoGq10C1c;
        this.anexoGq10C1c = anexoGq10C1c;
        this.firePropertyChange("anexoGq10C1c", oldValue, this.anexoGq10C1c);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoGq10T1_Linha> getAnexoGq10T1() {
        return this.anexoGq10T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoGq10T1(EventList<AnexoGq10T1_Linha> anexoGq10T1) {
        this.anexoGq10T1 = anexoGq10T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq10C1001a);
        result = HashCodeUtil.hash(result, this.anexoGq10C1001b);
        result = HashCodeUtil.hash(result, this.anexoGq10C1001c);
        result = HashCodeUtil.hash(result, this.anexoGq10C1002a);
        result = HashCodeUtil.hash(result, this.anexoGq10C1002b);
        result = HashCodeUtil.hash(result, this.anexoGq10C1002c);
        result = HashCodeUtil.hash(result, this.anexoGq10C1b);
        result = HashCodeUtil.hash(result, this.anexoGq10C1c);
        result = HashCodeUtil.hash(result, this.anexoGq10T1);
        return result;
    }
}

