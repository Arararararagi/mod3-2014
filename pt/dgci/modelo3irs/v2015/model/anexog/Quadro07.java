/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro07
extends Quadro07Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq07C1c()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq07C1d());
    }
}

