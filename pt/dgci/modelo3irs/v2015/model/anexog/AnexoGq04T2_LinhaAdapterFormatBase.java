/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoGq04T2_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoGq04T2_Linha>,
WritableTableFormat<AnexoGq04T2_Linha> {
    @Override
    public boolean isEditable(AnexoGq04T2_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoGq04T2_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos", line.getTitular());
            }
            case 2: {
                if (line == null || line.getMoveisImoveisBens() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoGQ4BensMoveisImoveis", line.getMoveisImoveisBens());
            }
            case 3: {
                return line.getAnoAfectacao();
            }
            case 4: {
                return line.getMesAfectacao();
            }
            case 5: {
                return line.getValorAfectacao();
            }
            case 6: {
                return line.getAnoAquisicao();
            }
            case 7: {
                return line.getMesAquisicao();
            }
            case 8: {
                return line.getValorAquisicao();
            }
            case 9: {
                return line.getFreguesia();
            }
            case 10: {
                if (line == null || line.getTipoPredio() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_TipoPredio", line.getTipoPredio());
            }
            case 11: {
                return line.getArtigo();
            }
            case 12: {
                return line.getFraccao();
            }
            case 13: {
                return line.getQuotaParte();
            }
        }
        return null;
    }

    @Override
    public AnexoGq04T2_Linha setColumnValue(AnexoGq04T2_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setMoveisImoveisBens((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setAnoAfectacao((Long)value);
                return line;
            }
            case 4: {
                line.setMesAfectacao((Long)value);
                return line;
            }
            case 5: {
                line.setValorAfectacao((Long)value);
                return line;
            }
            case 6: {
                line.setAnoAquisicao((Long)value);
                return line;
            }
            case 7: {
                line.setMesAquisicao((Long)value);
                return line;
            }
            case 8: {
                line.setValorAquisicao((Long)value);
                return line;
            }
            case 9: {
                line.setFreguesia((String)value);
                return line;
            }
            case 10: {
                if (value != null) {
                    line.setTipoPredio((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 11: {
                line.setArtigo((Long)value);
                return line;
            }
            case 12: {
                line.setFraccao((String)value);
                return line;
            }
            case 13: {
                line.setQuotaParte((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return String.class;
            }
            case 10: {
                return String.class;
            }
            case 11: {
                return Long.class;
            }
            case 12: {
                return String.class;
            }
            case 13: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 14;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Titular";
            }
            case 2: {
                return "M\u00f3veis / Im\u00f3veis";
            }
            case 3: {
                return "Ano";
            }
            case 4: {
                return "M\u00eas";
            }
            case 5: {
                return "Valor";
            }
            case 6: {
                return "Ano";
            }
            case 7: {
                return "M\u00eas";
            }
            case 8: {
                return "Valor";
            }
            case 9: {
                return "Freguesia (c\u00f3digo)";
            }
            case 10: {
                return "Tipo";
            }
            case 11: {
                return "Artigo";
            }
            case 12: {
                return "Fra\u00e7\u00e3o / Sec\u00e7\u00e3o";
            }
            case 13: {
                return "Quota-Parte %";
            }
        }
        return null;
    }
}

