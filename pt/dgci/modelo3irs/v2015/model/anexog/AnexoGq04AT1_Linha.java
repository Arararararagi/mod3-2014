/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class AnexoGq04AT1_Linha
extends AnexoGq04AT1_LinhaBase {
    public AnexoGq04AT1_Linha() {
    }

    public AnexoGq04AT1_Linha(Long camposQuadro4) {
        super(camposQuadro4);
    }

    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getCamposQuadro4());
    }
}

