/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.EventList;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro08
extends Quadro08Base {
    public List<Long> getNlinhaList() {
        ArrayList<Long> list = new ArrayList<Long>();
        EventList<AnexoGq08T1_Linha> linhas = this.getAnexoGq08T1();
        for (AnexoGq08T1_Linha linha : linhas) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || list.contains(linha.getNLinha())) continue;
            list.add(linha.getNLinha());
        }
        return list;
    }

    public List<Long> getNlinhaListT1ByTitular(String titular) {
        ArrayList<Long> list = new ArrayList<Long>();
        EventList<AnexoGq08T1_Linha> linhas = this.getAnexoGq08T1();
        for (AnexoGq08T1_Linha linha : linhas) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || linha.getTitular() == null || !linha.getTitular().equals(titular) || list.contains(linha.getNLinha())) continue;
            list.add(linha.getNLinha());
        }
        return list;
    }

    public AnexoGq08T1_Linha getNlinhaT1ByTitular(String titular) {
        EventList<AnexoGq08T1_Linha> linhas = this.getAnexoGq08T1();
        for (AnexoGq08T1_Linha linha : linhas) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || linha.getTitular() == null || !linha.getTitular().equals(titular)) continue;
            return linha;
        }
        return null;
    }

    public boolean existsCampoQ8T2(long cQ8) {
        EventList<AnexoGq08T2_Linha> linhas = this.getAnexoGq08T2();
        for (AnexoGq08T2_Linha linha : linhas) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCampoQ8()) || linha.getCampoQ8() != cQ8) continue;
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        if (this.getAnexoGq08T1().isEmpty()) {
            return true;
        }
        for (AnexoGq08T1_Linha linha : this.getAnexoGq08T1()) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getValorRealizacao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getAnoAquisicao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDespesas())) continue;
            return false;
        }
        if (this.getAnexoGq08T2().isEmpty()) {
            return true;
        }
        return true;
    }
}

