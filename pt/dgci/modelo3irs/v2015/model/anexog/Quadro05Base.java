/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoG.qQuadro05";
    public static final String ANEXOGQ05C501_LINK = "aAnexoG.qQuadro05.fanexoGq05C501";
    public static final String ANEXOGQ05C501 = "anexoGq05C501";
    private Long anexoGq05C501;
    public static final String ANEXOGQ05C502_LINK = "aAnexoG.qQuadro05.fanexoGq05C502";
    public static final String ANEXOGQ05C502 = "anexoGq05C502";
    private Long anexoGq05C502;
    public static final String ANEXOGQ05C503_LINK = "aAnexoG.qQuadro05.fanexoGq05C503";
    public static final String ANEXOGQ05C503 = "anexoGq05C503";
    private Long anexoGq05C503;
    public static final String ANEXOGQ05C504_LINK = "aAnexoG.qQuadro05.fanexoGq05C504";
    public static final String ANEXOGQ05C504 = "anexoGq05C504";
    private Long anexoGq05C504;
    public static final String ANEXOGQ05C505_LINK = "aAnexoG.qQuadro05.fanexoGq05C505";
    public static final String ANEXOGQ05C505 = "anexoGq05C505";
    private Long anexoGq05C505;
    public static final String ANEXOGQ05C506_LINK = "aAnexoG.qQuadro05.fanexoGq05C506";
    public static final String ANEXOGQ05C506 = "anexoGq05C506";
    private Long anexoGq05C506;
    public static final String ANEXOGQ05C507_LINK = "aAnexoG.qQuadro05.fanexoGq05C507";
    public static final String ANEXOGQ05C507 = "anexoGq05C507";
    private Long anexoGq05C507;
    public static final String ANEXOGQ05C508_LINK = "aAnexoG.qQuadro05.fanexoGq05C508";
    public static final String ANEXOGQ05C508 = "anexoGq05C508";
    private Long anexoGq05C508;
    public static final String ANEXOGQ05C509_LINK = "aAnexoG.qQuadro05.fanexoGq05C509";
    public static final String ANEXOGQ05C509 = "anexoGq05C509";
    private Long anexoGq05C509;
    public static final String ANEXOGQ05C510_LINK = "aAnexoG.qQuadro05.fanexoGq05C510";
    public static final String ANEXOGQ05C510 = "anexoGq05C510";
    private Long anexoGq05C510;
    public static final String ANEXOGQ05C511_LINK = "aAnexoG.qQuadro05.fanexoGq05C511";
    public static final String ANEXOGQ05C511 = "anexoGq05C511";
    private Long anexoGq05C511;
    public static final String ANEXOGQ05C521_LINK = "aAnexoG.qQuadro05.fanexoGq05C521";
    public static final String ANEXOGQ05C521 = "anexoGq05C521";
    private Long anexoGq05C521;
    public static final String ANEXOGQ05C522_LINK = "aAnexoG.qQuadro05.fanexoGq05C522";
    public static final String ANEXOGQ05C522 = "anexoGq05C522";
    private Long anexoGq05C522;
    public static final String ANEXOGQ05C523_LINK = "aAnexoG.qQuadro05.fanexoGq05C523";
    public static final String ANEXOGQ05C523 = "anexoGq05C523";
    private Long anexoGq05C523;
    public static final String ANEXOGQ05C524_LINK = "aAnexoG.qQuadro05.fanexoGq05C524";
    public static final String ANEXOGQ05C524 = "anexoGq05C524";
    private Long anexoGq05C524;
    public static final String ANEXOGQ05C525_LINK = "aAnexoG.qQuadro05.fanexoGq05C525";
    public static final String ANEXOGQ05C525 = "anexoGq05C525";
    private Long anexoGq05C525;
    public static final String ANEXOGQ05C526_LINK = "aAnexoG.qQuadro05.fanexoGq05C526";
    public static final String ANEXOGQ05C526 = "anexoGq05C526";
    private Long anexoGq05C526;
    public static final String ANEXOGQ05C527_LINK = "aAnexoG.qQuadro05.fanexoGq05C527";
    public static final String ANEXOGQ05C527 = "anexoGq05C527";
    private Long anexoGq05C527;
    public static final String ANEXOGQ05C528_LINK = "aAnexoG.qQuadro05.fanexoGq05C528";
    public static final String ANEXOGQ05C528 = "anexoGq05C528";
    private Long anexoGq05C528;
    public static final String ANEXOGQ05C529_LINK = "aAnexoG.qQuadro05.fanexoGq05C529";
    public static final String ANEXOGQ05C529 = "anexoGq05C529";
    private Long anexoGq05C529;
    public static final String ANEXOGQ05C530_LINK = "aAnexoG.qQuadro05.fanexoGq05C530";
    public static final String ANEXOGQ05C530 = "anexoGq05C530";
    private Long anexoGq05C530;
    public static final String ANEXOGQ05C531_LINK = "aAnexoG.qQuadro05.fanexoGq05C531";
    public static final String ANEXOGQ05C531 = "anexoGq05C531";
    private Long anexoGq05C531;
    public static final String ANEXOGQ05AC1_LINK = "aAnexoG.qQuadro05.fanexoGq05AC1";
    public static final String ANEXOGQ05AC1 = "anexoGq05AC1";
    private String anexoGq05AC1;
    public static final String ANEXOGQ05AC2_LINK = "aAnexoG.qQuadro05.fanexoGq05AC2";
    public static final String ANEXOGQ05AC2 = "anexoGq05AC2";
    private String anexoGq05AC2;
    public static final String ANEXOGQ05AC3_LINK = "aAnexoG.qQuadro05.fanexoGq05AC3";
    public static final String ANEXOGQ05AC3 = "anexoGq05AC3";
    private String anexoGq05AC3;
    public static final String ANEXOGQ05AC4_LINK = "aAnexoG.qQuadro05.fanexoGq05AC4";
    public static final String ANEXOGQ05AC4 = "anexoGq05AC4";
    private Long anexoGq05AC4;
    public static final String ANEXOGQ05AC5_LINK = "aAnexoG.qQuadro05.fanexoGq05AC5";
    public static final String ANEXOGQ05AC5 = "anexoGq05AC5";
    private String anexoGq05AC5;
    public static final String ANEXOGQ05AC6_LINK = "aAnexoG.qQuadro05.fanexoGq05AC6";
    public static final String ANEXOGQ05AC6 = "anexoGq05AC6";
    private Long anexoGq05AC6;
    public static final String ANEXOGQ05AC7_LINK = "aAnexoG.qQuadro05.fanexoGq05AC7";
    public static final String ANEXOGQ05AC7 = "anexoGq05AC7";
    private String anexoGq05AC7;
    public static final String ANEXOGQ05AC8_LINK = "aAnexoG.qQuadro05.fanexoGq05AC8";
    public static final String ANEXOGQ05AC8 = "anexoGq05AC8";
    private String anexoGq05AC8;
    public static final String ANEXOGQ05AC9_LINK = "aAnexoG.qQuadro05.fanexoGq05AC9";
    public static final String ANEXOGQ05AC9 = "anexoGq05AC9";
    private String anexoGq05AC9;
    public static final String ANEXOGQ05AC10_LINK = "aAnexoG.qQuadro05.fanexoGq05AC10";
    public static final String ANEXOGQ05AC10 = "anexoGq05AC10";
    private Long anexoGq05AC10;
    public static final String ANEXOGQ05AC11_LINK = "aAnexoG.qQuadro05.fanexoGq05AC11";
    public static final String ANEXOGQ05AC11 = "anexoGq05AC11";
    private String anexoGq05AC11;
    public static final String ANEXOGQ05AC12_LINK = "aAnexoG.qQuadro05.fanexoGq05AC12";
    public static final String ANEXOGQ05AC12 = "anexoGq05AC12";
    private Long anexoGq05AC12;
    public static final String ANEXOGQ05AC13_LINK = "aAnexoG.qQuadro05.fanexoGq05AC13";
    public static final String ANEXOGQ05AC13 = "anexoGq05AC13";
    private Long anexoGq05AC13;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq05C501() {
        return this.anexoGq05C501;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C501(Long anexoGq05C501) {
        Long oldValue = this.anexoGq05C501;
        this.anexoGq05C501 = anexoGq05C501;
        this.firePropertyChange("anexoGq05C501", oldValue, this.anexoGq05C501);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4")
    public Long getAnexoGq05C502() {
        return this.anexoGq05C502;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C502(Long anexoGq05C502) {
        Long oldValue = this.anexoGq05C502;
        this.anexoGq05C502 = anexoGq05C502;
        this.firePropertyChange("anexoGq05C502", oldValue, this.anexoGq05C502);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4")
    public Long getAnexoGq05C503() {
        return this.anexoGq05C503;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C503(Long anexoGq05C503) {
        Long oldValue = this.anexoGq05C503;
        this.anexoGq05C503 = anexoGq05C503;
        this.firePropertyChange("anexoGq05C503", oldValue, this.anexoGq05C503);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4")
    public Long getAnexoGq05C504() {
        return this.anexoGq05C504;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C504(Long anexoGq05C504) {
        Long oldValue = this.anexoGq05C504;
        this.anexoGq05C504 = anexoGq05C504;
        this.firePropertyChange("anexoGq05C504", oldValue, this.anexoGq05C504);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C505() {
        return this.anexoGq05C505;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C505(Long anexoGq05C505) {
        Long oldValue = this.anexoGq05C505;
        this.anexoGq05C505 = anexoGq05C505;
        this.firePropertyChange("anexoGq05C505", oldValue, this.anexoGq05C505);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C506() {
        return this.anexoGq05C506;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C506(Long anexoGq05C506) {
        Long oldValue = this.anexoGq05C506;
        this.anexoGq05C506 = anexoGq05C506;
        this.firePropertyChange("anexoGq05C506", oldValue, this.anexoGq05C506);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C507() {
        return this.anexoGq05C507;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C507(Long anexoGq05C507) {
        Long oldValue = this.anexoGq05C507;
        this.anexoGq05C507 = anexoGq05C507;
        this.firePropertyChange("anexoGq05C507", oldValue, this.anexoGq05C507);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C508() {
        return this.anexoGq05C508;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C508(Long anexoGq05C508) {
        Long oldValue = this.anexoGq05C508;
        this.anexoGq05C508 = anexoGq05C508;
        this.firePropertyChange("anexoGq05C508", oldValue, this.anexoGq05C508);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C509() {
        return this.anexoGq05C509;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C509(Long anexoGq05C509) {
        Long oldValue = this.anexoGq05C509;
        this.anexoGq05C509 = anexoGq05C509;
        this.firePropertyChange("anexoGq05C509", oldValue, this.anexoGq05C509);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C510() {
        return this.anexoGq05C510;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C510(Long anexoGq05C510) {
        Long oldValue = this.anexoGq05C510;
        this.anexoGq05C510 = anexoGq05C510;
        this.firePropertyChange("anexoGq05C510", oldValue, this.anexoGq05C510);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C511() {
        return this.anexoGq05C511;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C511(Long anexoGq05C511) {
        Long oldValue = this.anexoGq05C511;
        this.anexoGq05C511 = anexoGq05C511;
        this.firePropertyChange("anexoGq05C511", oldValue, this.anexoGq05C511);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq05C521() {
        return this.anexoGq05C521;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C521(Long anexoGq05C521) {
        Long oldValue = this.anexoGq05C521;
        this.anexoGq05C521 = anexoGq05C521;
        this.firePropertyChange("anexoGq05C521", oldValue, this.anexoGq05C521);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4")
    public Long getAnexoGq05C522() {
        return this.anexoGq05C522;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C522(Long anexoGq05C522) {
        Long oldValue = this.anexoGq05C522;
        this.anexoGq05C522 = anexoGq05C522;
        this.firePropertyChange("anexoGq05C522", oldValue, this.anexoGq05C522);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4")
    public Long getAnexoGq05C523() {
        return this.anexoGq05C523;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C523(Long anexoGq05C523) {
        Long oldValue = this.anexoGq05C523;
        this.anexoGq05C523 = anexoGq05C523;
        this.firePropertyChange("anexoGq05C523", oldValue, this.anexoGq05C523);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4")
    public Long getAnexoGq05C524() {
        return this.anexoGq05C524;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05C524(Long anexoGq05C524) {
        Long oldValue = this.anexoGq05C524;
        this.anexoGq05C524 = anexoGq05C524;
        this.firePropertyChange("anexoGq05C524", oldValue, this.anexoGq05C524);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C525() {
        return this.anexoGq05C525;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C525(Long anexoGq05C525) {
        Long oldValue = this.anexoGq05C525;
        this.anexoGq05C525 = anexoGq05C525;
        this.firePropertyChange("anexoGq05C525", oldValue, this.anexoGq05C525);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C526() {
        return this.anexoGq05C526;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C526(Long anexoGq05C526) {
        Long oldValue = this.anexoGq05C526;
        this.anexoGq05C526 = anexoGq05C526;
        this.firePropertyChange("anexoGq05C526", oldValue, this.anexoGq05C526);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C527() {
        return this.anexoGq05C527;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C527(Long anexoGq05C527) {
        Long oldValue = this.anexoGq05C527;
        this.anexoGq05C527 = anexoGq05C527;
        this.firePropertyChange("anexoGq05C527", oldValue, this.anexoGq05C527);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C528() {
        return this.anexoGq05C528;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C528(Long anexoGq05C528) {
        Long oldValue = this.anexoGq05C528;
        this.anexoGq05C528 = anexoGq05C528;
        this.firePropertyChange("anexoGq05C528", oldValue, this.anexoGq05C528);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C529() {
        return this.anexoGq05C529;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C529(Long anexoGq05C529) {
        Long oldValue = this.anexoGq05C529;
        this.anexoGq05C529 = anexoGq05C529;
        this.firePropertyChange("anexoGq05C529", oldValue, this.anexoGq05C529);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C530() {
        return this.anexoGq05C530;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C530(Long anexoGq05C530) {
        Long oldValue = this.anexoGq05C530;
        this.anexoGq05C530 = anexoGq05C530;
        this.firePropertyChange("anexoGq05C530", oldValue, this.anexoGq05C530);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq05C531() {
        return this.anexoGq05C531;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq05C531(Long anexoGq05C531) {
        Long oldValue = this.anexoGq05C531;
        this.anexoGq05C531 = anexoGq05C531;
        this.firePropertyChange("anexoGq05C531", oldValue, this.anexoGq05C531);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq05AC1() {
        return this.anexoGq05AC1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC1(String anexoGq05AC1) {
        String oldValue = this.anexoGq05AC1;
        this.anexoGq05AC1 = anexoGq05AC1;
        this.firePropertyChange("anexoGq05AC1", oldValue, this.anexoGq05AC1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoGq05AC2() {
        return this.anexoGq05AC2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC2(String anexoGq05AC2) {
        String oldValue = this.anexoGq05AC2;
        this.anexoGq05AC2 = anexoGq05AC2;
        this.firePropertyChange("anexoGq05AC2", oldValue, this.anexoGq05AC2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio")
    public String getAnexoGq05AC3() {
        return this.anexoGq05AC3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC3(String anexoGq05AC3) {
        String oldValue = this.anexoGq05AC3;
        this.anexoGq05AC3 = anexoGq05AC3;
        this.firePropertyChange("anexoGq05AC3", oldValue, this.anexoGq05AC3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq05AC4() {
        return this.anexoGq05AC4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC4(Long anexoGq05AC4) {
        Long oldValue = this.anexoGq05AC4;
        this.anexoGq05AC4 = anexoGq05AC4;
        this.firePropertyChange("anexoGq05AC4", oldValue, this.anexoGq05AC4);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoGq05AC5() {
        return this.anexoGq05AC5;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC5(String anexoGq05AC5) {
        String oldValue = this.anexoGq05AC5;
        this.anexoGq05AC5 = anexoGq05AC5;
        this.firePropertyChange("anexoGq05AC5", oldValue, this.anexoGq05AC5);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq05AC6() {
        return this.anexoGq05AC6;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC6(Long anexoGq05AC6) {
        Long oldValue = this.anexoGq05AC6;
        this.anexoGq05AC6 = anexoGq05AC6;
        this.firePropertyChange("anexoGq05AC6", oldValue, this.anexoGq05AC6);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq05AC7() {
        return this.anexoGq05AC7;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC7(String anexoGq05AC7) {
        String oldValue = this.anexoGq05AC7;
        this.anexoGq05AC7 = anexoGq05AC7;
        this.firePropertyChange("anexoGq05AC7", oldValue, this.anexoGq05AC7);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoGq05AC8() {
        return this.anexoGq05AC8;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC8(String anexoGq05AC8) {
        String oldValue = this.anexoGq05AC8;
        this.anexoGq05AC8 = anexoGq05AC8;
        this.firePropertyChange("anexoGq05AC8", oldValue, this.anexoGq05AC8);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio")
    public String getAnexoGq05AC9() {
        return this.anexoGq05AC9;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC9(String anexoGq05AC9) {
        String oldValue = this.anexoGq05AC9;
        this.anexoGq05AC9 = anexoGq05AC9;
        this.firePropertyChange("anexoGq05AC9", oldValue, this.anexoGq05AC9);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq05AC10() {
        return this.anexoGq05AC10;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC10(Long anexoGq05AC10) {
        Long oldValue = this.anexoGq05AC10;
        this.anexoGq05AC10 = anexoGq05AC10;
        this.firePropertyChange("anexoGq05AC10", oldValue, this.anexoGq05AC10);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoGq05AC11() {
        return this.anexoGq05AC11;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC11(String anexoGq05AC11) {
        String oldValue = this.anexoGq05AC11;
        this.anexoGq05AC11 = anexoGq05AC11;
        this.firePropertyChange("anexoGq05AC11", oldValue, this.anexoGq05AC11);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq05AC12() {
        return this.anexoGq05AC12;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC12(Long anexoGq05AC12) {
        Long oldValue = this.anexoGq05AC12;
        this.anexoGq05AC12 = anexoGq05AC12;
        this.firePropertyChange("anexoGq05AC12", oldValue, this.anexoGq05AC12);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxG")
    public Long getAnexoGq05AC13() {
        return this.anexoGq05AC13;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq05AC13(Long anexoGq05AC13) {
        Long oldValue = this.anexoGq05AC13;
        this.anexoGq05AC13 = anexoGq05AC13;
        this.firePropertyChange("anexoGq05AC13", oldValue, this.anexoGq05AC13);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq05C501);
        result = HashCodeUtil.hash(result, this.anexoGq05C502);
        result = HashCodeUtil.hash(result, this.anexoGq05C503);
        result = HashCodeUtil.hash(result, this.anexoGq05C504);
        result = HashCodeUtil.hash(result, this.anexoGq05C505);
        result = HashCodeUtil.hash(result, this.anexoGq05C506);
        result = HashCodeUtil.hash(result, this.anexoGq05C507);
        result = HashCodeUtil.hash(result, this.anexoGq05C508);
        result = HashCodeUtil.hash(result, this.anexoGq05C509);
        result = HashCodeUtil.hash(result, this.anexoGq05C510);
        result = HashCodeUtil.hash(result, this.anexoGq05C511);
        result = HashCodeUtil.hash(result, this.anexoGq05C521);
        result = HashCodeUtil.hash(result, this.anexoGq05C522);
        result = HashCodeUtil.hash(result, this.anexoGq05C523);
        result = HashCodeUtil.hash(result, this.anexoGq05C524);
        result = HashCodeUtil.hash(result, this.anexoGq05C525);
        result = HashCodeUtil.hash(result, this.anexoGq05C526);
        result = HashCodeUtil.hash(result, this.anexoGq05C527);
        result = HashCodeUtil.hash(result, this.anexoGq05C528);
        result = HashCodeUtil.hash(result, this.anexoGq05C529);
        result = HashCodeUtil.hash(result, this.anexoGq05C530);
        result = HashCodeUtil.hash(result, this.anexoGq05C531);
        result = HashCodeUtil.hash(result, this.anexoGq05AC1);
        result = HashCodeUtil.hash(result, this.anexoGq05AC2);
        result = HashCodeUtil.hash(result, this.anexoGq05AC3);
        result = HashCodeUtil.hash(result, this.anexoGq05AC4);
        result = HashCodeUtil.hash(result, this.anexoGq05AC5);
        result = HashCodeUtil.hash(result, this.anexoGq05AC6);
        result = HashCodeUtil.hash(result, this.anexoGq05AC7);
        result = HashCodeUtil.hash(result, this.anexoGq05AC8);
        result = HashCodeUtil.hash(result, this.anexoGq05AC9);
        result = HashCodeUtil.hash(result, this.anexoGq05AC10);
        result = HashCodeUtil.hash(result, this.anexoGq05AC11);
        result = HashCodeUtil.hash(result, this.anexoGq05AC12);
        result = HashCodeUtil.hash(result, this.anexoGq05AC13);
        return result;
    }
}

