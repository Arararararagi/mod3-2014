/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoGq04T1_LinhaBase
extends Model {
    public static final String NLINHA_LINK = "aAnexoG.qQuadro04.fnLinha";
    public static final String NLINHA = "nLinha";
    private Long nLinha;
    public static final String TITULAR_LINK = "aAnexoG.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String ANOREALIZACAO_LINK = "aAnexoG.qQuadro04.fanoRealizacao";
    public static final String ANOREALIZACAO = "anoRealizacao";
    private Long anoRealizacao;
    public static final String MESREALIZACAO_LINK = "aAnexoG.qQuadro04.fmesRealizacao";
    public static final String MESREALIZACAO = "mesRealizacao";
    private Long mesRealizacao;
    public static final String VALORREALIZACAO_LINK = "aAnexoG.qQuadro04.fvalorRealizacao";
    public static final String VALORREALIZACAO = "valorRealizacao";
    private Long valorRealizacao;
    public static final String ANOAQUISICAO_LINK = "aAnexoG.qQuadro04.fanoAquisicao";
    public static final String ANOAQUISICAO = "anoAquisicao";
    private Long anoAquisicao;
    public static final String MESAQUISICAO_LINK = "aAnexoG.qQuadro04.fmesAquisicao";
    public static final String MESAQUISICAO = "mesAquisicao";
    private Long mesAquisicao;
    public static final String VALORAQUISICAO_LINK = "aAnexoG.qQuadro04.fvalorAquisicao";
    public static final String VALORAQUISICAO = "valorAquisicao";
    private Long valorAquisicao;
    public static final String DESPESAS_LINK = "aAnexoG.qQuadro04.fdespesas";
    public static final String DESPESAS = "despesas";
    private Long despesas;
    public static final String FREGUESIA_LINK = "aAnexoG.qQuadro04.ffreguesia";
    public static final String FREGUESIA = "freguesia";
    private String freguesia;
    public static final String TIPOPREDIO_LINK = "aAnexoG.qQuadro04.ftipoPredio";
    public static final String TIPOPREDIO = "tipoPredio";
    private String tipoPredio;
    public static final String ARTIGO_LINK = "aAnexoG.qQuadro04.fartigo";
    public static final String ARTIGO = "artigo";
    private Long artigo;
    public static final String FRACCAO_LINK = "aAnexoG.qQuadro04.ffraccao";
    public static final String FRACCAO = "fraccao";
    private String fraccao;
    public static final String QUOTAPARTE_LINK = "aAnexoG.qQuadro04.fquotaParte";
    public static final String QUOTAPARTE = "quotaParte";
    private Long quotaParte;

    public AnexoGq04T1_LinhaBase(Long nLinha, String titular, Long anoRealizacao, Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, Long despesas, String freguesia, String tipoPredio, Long artigo, String fraccao, Long quotaParte) {
        this.nLinha = nLinha;
        this.titular = titular;
        this.anoRealizacao = anoRealizacao;
        this.mesRealizacao = mesRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.anoAquisicao = anoAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.despesas = despesas;
        this.freguesia = freguesia;
        this.tipoPredio = tipoPredio;
        this.artigo = artigo;
        this.fraccao = fraccao;
        this.quotaParte = quotaParte;
    }

    public AnexoGq04T1_LinhaBase() {
        this.nLinha = null;
        this.titular = null;
        this.anoRealizacao = null;
        this.mesRealizacao = null;
        this.valorRealizacao = null;
        this.anoAquisicao = null;
        this.mesAquisicao = null;
        this.valorAquisicao = null;
        this.despesas = null;
        this.freguesia = null;
        this.tipoPredio = null;
        this.artigo = null;
        this.fraccao = null;
        this.quotaParte = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4")
    public Long getNLinha() {
        return this.nLinha;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNLinha(Long nLinha) {
        Long oldValue = this.nLinha;
        this.nLinha = nLinha;
        this.firePropertyChange("nLinha", oldValue, this.nLinha);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoRealizacao() {
        return this.anoRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoRealizacao(Long anoRealizacao) {
        Long oldValue = this.anoRealizacao;
        this.anoRealizacao = anoRealizacao;
        this.firePropertyChange("anoRealizacao", oldValue, this.anoRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesRealizacao() {
        return this.mesRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesRealizacao(Long mesRealizacao) {
        Long oldValue = this.mesRealizacao;
        this.mesRealizacao = mesRealizacao;
        this.firePropertyChange("mesRealizacao", oldValue, this.mesRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorRealizacao() {
        return this.valorRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorRealizacao(Long valorRealizacao) {
        Long oldValue = this.valorRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.firePropertyChange("valorRealizacao", oldValue, this.valorRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoAquisicao() {
        return this.anoAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoAquisicao(Long anoAquisicao) {
        Long oldValue = this.anoAquisicao;
        this.anoAquisicao = anoAquisicao;
        this.firePropertyChange("anoAquisicao", oldValue, this.anoAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesAquisicao() {
        return this.mesAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesAquisicao(Long mesAquisicao) {
        Long oldValue = this.mesAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.firePropertyChange("mesAquisicao", oldValue, this.mesAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorAquisicao() {
        return this.valorAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorAquisicao(Long valorAquisicao) {
        Long oldValue = this.valorAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.firePropertyChange("valorAquisicao", oldValue, this.valorAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getDespesas() {
        return this.despesas;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setDespesas(Long despesas) {
        Long oldValue = this.despesas;
        this.despesas = despesas;
        this.firePropertyChange("despesas", oldValue, this.despesas);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFreguesia() {
        return this.freguesia;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFreguesia(String freguesia) {
        String oldValue = this.freguesia;
        this.freguesia = freguesia;
        this.firePropertyChange("freguesia", oldValue, this.freguesia);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio")
    public String getTipoPredio() {
        return this.tipoPredio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTipoPredio(String tipoPredio) {
        String oldValue = this.tipoPredio;
        this.tipoPredio = tipoPredio;
        this.firePropertyChange("tipoPredio", oldValue, this.tipoPredio);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getArtigo() {
        return this.artigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setArtigo(Long artigo) {
        Long oldValue = this.artigo;
        this.artigo = artigo;
        this.firePropertyChange("artigo", oldValue, this.artigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFraccao() {
        return this.fraccao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFraccao(String fraccao) {
        String oldValue = this.fraccao;
        this.fraccao = fraccao;
        this.firePropertyChange("fraccao", oldValue, this.fraccao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQuotaParte() {
        return this.quotaParte;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQuotaParte(Long quotaParte) {
        Long oldValue = this.quotaParte;
        this.quotaParte = quotaParte;
        this.firePropertyChange("quotaParte", oldValue, this.quotaParte);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nLinha);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.anoRealizacao);
        result = HashCodeUtil.hash(result, this.mesRealizacao);
        result = HashCodeUtil.hash(result, this.valorRealizacao);
        result = HashCodeUtil.hash(result, this.anoAquisicao);
        result = HashCodeUtil.hash(result, this.mesAquisicao);
        result = HashCodeUtil.hash(result, this.valorAquisicao);
        result = HashCodeUtil.hash(result, this.despesas);
        result = HashCodeUtil.hash(result, this.freguesia);
        result = HashCodeUtil.hash(result, this.tipoPredio);
        result = HashCodeUtil.hash(result, this.artigo);
        result = HashCodeUtil.hash(result, this.fraccao);
        result = HashCodeUtil.hash(result, this.quotaParte);
        return result;
    }

    public static enum Property {
        NLINHA("nLinha", 2),
        TITULAR("titular", 3),
        ANOREALIZACAO("anoRealizacao", 4),
        MESREALIZACAO("mesRealizacao", 5),
        VALORREALIZACAO("valorRealizacao", 6),
        ANOAQUISICAO("anoAquisicao", 7),
        MESAQUISICAO("mesAquisicao", 8),
        VALORAQUISICAO("valorAquisicao", 9),
        DESPESAS("despesas", 10),
        FREGUESIA("freguesia", 11),
        TIPOPREDIO("tipoPredio", 12),
        ARTIGO("artigo", 13),
        FRACCAO("fraccao", 14),
        QUOTAPARTE("quotaParte", 15);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

