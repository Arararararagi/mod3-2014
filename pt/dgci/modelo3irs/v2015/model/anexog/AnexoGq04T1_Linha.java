/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_LinhaBase;

public class AnexoGq04T1_Linha
extends AnexoGq04T1_LinhaBase {
    public AnexoGq04T1_Linha() {
    }

    public AnexoGq04T1_Linha(Long nLinha, String titular, Long anoRealizacao, Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, Long despesas, String freguesia, String tipoPredio, Long artigo, String fraccao, Long quotaParte) {
        super(nLinha, titular, anoRealizacao, mesRealizacao, valorRealizacao, anoAquisicao, mesAquisicao, valorAquisicao, despesas, freguesia, tipoPredio, artigo, fraccao, quotaParte);
    }

    public static String getLink(int numLinha) {
        return "aAnexoG.qQuadro04.tanexoGq04T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoGq04T1_LinhaBase.Property column) {
        return AnexoGq04T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

