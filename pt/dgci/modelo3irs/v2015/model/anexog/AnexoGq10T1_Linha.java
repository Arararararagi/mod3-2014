/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_LinhaBase;

public class AnexoGq10T1_Linha
extends AnexoGq10T1_LinhaBase {
    public AnexoGq10T1_Linha() {
    }

    public AnexoGq10T1_Linha(Long nIF, Long valor) {
        super(nIF, valor);
    }

    public static String getLink(int numLinha) {
        return "aAnexoG.qQuadro10.tanexoGq10T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoGq10T1_LinhaBase.Property column) {
        return AnexoGq10T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

