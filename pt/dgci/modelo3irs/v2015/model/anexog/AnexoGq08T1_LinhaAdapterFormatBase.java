/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoGq08T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoGq08T1_Linha>,
WritableTableFormat<AnexoGq08T1_Linha> {
    @Override
    public boolean isEditable(AnexoGq08T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoGq08T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getNLinha() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoGQ8", line.getNLinha());
            }
            case 2: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos", line.getTitular());
            }
            case 3: {
                return line.getNIF();
            }
            case 4: {
                if (line == null || line.getCodEncargos() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoGQ8CodEncargos", line.getCodEncargos());
            }
            case 5: {
                return line.getAnoRealizacao();
            }
            case 6: {
                return line.getMesRealizacao();
            }
            case 7: {
                return line.getValorRealizacao();
            }
            case 8: {
                return line.getAnoAquisicao();
            }
            case 9: {
                return line.getMesAquisicao();
            }
            case 10: {
                return line.getValorAquisicao();
            }
            case 11: {
                return line.getDespesas();
            }
        }
        return null;
    }

    @Override
    public AnexoGq08T1_Linha setColumnValue(AnexoGq08T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setNLinha((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setNIF((Long)value);
                return line;
            }
            case 4: {
                if (value != null) {
                    line.setCodEncargos((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 5: {
                line.setAnoRealizacao((Long)value);
                return line;
            }
            case 6: {
                line.setMesRealizacao((Long)value);
                return line;
            }
            case 7: {
                line.setValorRealizacao((Long)value);
                return line;
            }
            case 8: {
                line.setAnoAquisicao((Long)value);
                return line;
            }
            case 9: {
                line.setMesAquisicao((Long)value);
                return line;
            }
            case 10: {
                line.setValorAquisicao((Long)value);
                return line;
            }
            case 11: {
                line.setDespesas((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return Long.class;
            }
            case 10: {
                return Long.class;
            }
            case 11: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 12;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "N\u00ba da Linha";
            }
            case 2: {
                return "Titular";
            }
            case 3: {
                return "Entidade Emitente";
            }
            case 4: {
                return "C\u00f3digos";
            }
            case 5: {
                return "Ano";
            }
            case 6: {
                return "M\u00eas";
            }
            case 7: {
                return "Valor";
            }
            case 8: {
                return "Ano";
            }
            case 9: {
                return "M\u00eas";
            }
            case 10: {
                return "Valor";
            }
            case 11: {
                return "Despesas e Encargos";
            }
        }
        return null;
    }
}

