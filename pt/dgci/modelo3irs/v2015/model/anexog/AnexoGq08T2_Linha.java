/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_LinhaBase;

public class AnexoGq08T2_Linha
extends AnexoGq08T2_LinhaBase {
    public AnexoGq08T2_Linha() {
    }

    public AnexoGq08T2_Linha(Long campoQ8, Long nipc) {
        super(campoQ8, nipc);
    }

    public static String getLink(int numLinha) {
        return "aAnexoG.qQuadro08.tanexoGq08T2.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoGq08T2_LinhaBase.Property column) {
        return AnexoGq08T2_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

