/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.EventList;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro04
extends Quadro04Base {
    public List<AnexoGq04T1_Linha> getByTitular(String titular) {
        ArrayList<AnexoGq04T1_Linha> result = new ArrayList<AnexoGq04T1_Linha>();
        for (AnexoGq04T1_Linha linha : this.getAnexoGq04T1()) {
            if (linha.getTitular() == null || !linha.getTitular().equals(titular)) continue;
            result.add(linha);
        }
        return result;
    }

    public int getSimulatorLineIndexByNLinha(Long nLinha) {
        if (nLinha == null) {
            return 0;
        }
        EventList<AnexoGq04T1_Linha> linhas = this.getAnexoGq04T1();
        for (int i = 0; i < linhas.size(); ++i) {
            if (!nLinha.equals(linhas.get(i).getNLinha())) continue;
            return i + 1;
        }
        return 0;
    }

    public boolean isEmpty() {
        return this.getAnexoGq04T1().isEmpty() && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq04C1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq04C2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq04C3()) && this.getAnexoGq04AT1().isEmpty() && this.getAnexoGq04B1OP() == null && this.getAnexoGq04T2().isEmpty();
    }

    public boolean AnexoGq04AT1isEmpty() {
        if (this.getAnexoGq04AT1().isEmpty()) {
            return true;
        }
        EventList<AnexoGq04AT1_Linha> linhas = this.getAnexoGq04AT1();
        for (AnexoGq04AT1_Linha linha : linhas) {
            if (linha.isEmpty()) continue;
            return false;
        }
        return true;
    }
}

