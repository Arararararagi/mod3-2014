/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoG.qQuadro04";
    public static final String ANEXOGQ04T1_LINK = "aAnexoG.qQuadro04.tanexoGq04T1";
    private EventList<AnexoGq04T1_Linha> anexoGq04T1 = new BasicEventList<AnexoGq04T1_Linha>();
    public static final String ANEXOGQ04C1_LINK = "aAnexoG.qQuadro04.fanexoGq04C1";
    public static final String ANEXOGQ04C1 = "anexoGq04C1";
    private Long anexoGq04C1;
    public static final String ANEXOGQ04C2_LINK = "aAnexoG.qQuadro04.fanexoGq04C2";
    public static final String ANEXOGQ04C2 = "anexoGq04C2";
    private Long anexoGq04C2;
    public static final String ANEXOGQ04C3_LINK = "aAnexoG.qQuadro04.fanexoGq04C3";
    public static final String ANEXOGQ04C3 = "anexoGq04C3";
    private Long anexoGq04C3;
    public static final String ANEXOGQ04AT1_LINK = "aAnexoG.qQuadro04.tanexoGq04AT1";
    private EventList<AnexoGq04AT1_Linha> anexoGq04AT1 = new BasicEventList<AnexoGq04AT1_Linha>();
    public static final String ANEXOGQ04B1OPSIM_LINK = "aAnexoG.qQuadro04.fanexoGq04B1OPSim";
    public static final String ANEXOGQ04B1OPSIM_VALUE = "S";
    public static final String ANEXOGQ04B1OPNAO_LINK = "aAnexoG.qQuadro04.fanexoGq04B1OPNao";
    public static final String ANEXOGQ04B1OPNAO_VALUE = "N";
    public static final String ANEXOGQ04B1OP = "anexoGq04B1OP";
    private String anexoGq04B1OP;
    public static final String ANEXOGQ04T2_LINK = "aAnexoG.qQuadro04.tanexoGq04T2";
    private EventList<AnexoGq04T2_Linha> anexoGq04T2 = new BasicEventList<AnexoGq04T2_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoGq04T1_Linha> getAnexoGq04T1() {
        return this.anexoGq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoGq04T1(EventList<AnexoGq04T1_Linha> anexoGq04T1) {
        this.anexoGq04T1 = anexoGq04T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq04C1() {
        return this.anexoGq04C1;
    }

    public void setAnexoGq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq04C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoGq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoGq04T1().get(i).getValorRealizacao() == null ? 0 : this.getAnexoGq04T1().get(i).getValorRealizacao();
        }
        this.setAnexoGq04C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq04C1(Long anexoGq04C1) {
        Long oldValue = this.anexoGq04C1;
        this.anexoGq04C1 = anexoGq04C1;
        this.firePropertyChange("anexoGq04C1", oldValue, this.anexoGq04C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq04C2() {
        return this.anexoGq04C2;
    }

    public void setAnexoGq04C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq04C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoGq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoGq04T1().get(i).getValorAquisicao() == null ? 0 : this.getAnexoGq04T1().get(i).getValorAquisicao();
        }
        this.setAnexoGq04C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq04C2(Long anexoGq04C2) {
        Long oldValue = this.anexoGq04C2;
        this.anexoGq04C2 = anexoGq04C2;
        this.firePropertyChange("anexoGq04C2", oldValue, this.anexoGq04C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq04C3() {
        return this.anexoGq04C3;
    }

    public void setAnexoGq04C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq04C3")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoGq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoGq04T1().get(i).getDespesas() == null ? 0 : this.getAnexoGq04T1().get(i).getDespesas();
        }
        this.setAnexoGq04C3(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq04C3(Long anexoGq04C3) {
        Long oldValue = this.anexoGq04C3;
        this.anexoGq04C3 = anexoGq04C3;
        this.firePropertyChange("anexoGq04C3", oldValue, this.anexoGq04C3);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoGq04AT1_Linha> getAnexoGq04AT1() {
        return this.anexoGq04AT1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoGq04AT1(EventList<AnexoGq04AT1_Linha> anexoGq04AT1) {
        this.anexoGq04AT1 = anexoGq04AT1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoGq04B1OP() {
        return this.anexoGq04B1OP;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq04B1OP(String anexoGq04B1OP) {
        String oldValue = this.anexoGq04B1OP;
        this.anexoGq04B1OP = anexoGq04B1OP;
        this.firePropertyChange("anexoGq04B1OP", oldValue, this.anexoGq04B1OP);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoGq04T2_Linha> getAnexoGq04T2() {
        return this.anexoGq04T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoGq04T2(EventList<AnexoGq04T2_Linha> anexoGq04T2) {
        this.anexoGq04T2 = anexoGq04T2;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq04T1);
        result = HashCodeUtil.hash(result, this.anexoGq04C1);
        result = HashCodeUtil.hash(result, this.anexoGq04C2);
        result = HashCodeUtil.hash(result, this.anexoGq04C3);
        result = HashCodeUtil.hash(result, this.anexoGq04AT1);
        result = HashCodeUtil.hash(result, this.anexoGq04B1OP);
        result = HashCodeUtil.hash(result, this.anexoGq04T2);
        return result;
    }
}

