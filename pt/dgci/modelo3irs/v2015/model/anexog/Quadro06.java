/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.Quadro06Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro06
extends Quadro06Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq06C602c()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq06C1d()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq06C1e());
    }
}

