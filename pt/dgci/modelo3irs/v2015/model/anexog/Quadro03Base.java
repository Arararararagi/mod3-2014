/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoG.qQuadro03";
    public static final String ANEXOGQ03C02_LINK = "aAnexoG.qQuadro03.fanexoGq03C02";
    public static final String ANEXOGQ03C02 = "anexoGq03C02";
    private Long anexoGq03C02;
    public static final String ANEXOGQ03C03_LINK = "aAnexoG.qQuadro03.fanexoGq03C03";
    public static final String ANEXOGQ03C03 = "anexoGq03C03";
    private Long anexoGq03C03;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq03C02() {
        return this.anexoGq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq03C02(Long anexoGq03C02) {
        Long oldValue = this.anexoGq03C02;
        this.anexoGq03C02 = anexoGq03C02;
        this.firePropertyChange("anexoGq03C02", oldValue, this.anexoGq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoGq03C03() {
        return this.anexoGq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq03C03(Long anexoGq03C03) {
        Long oldValue = this.anexoGq03C03;
        this.anexoGq03C03 = anexoGq03C03;
        this.firePropertyChange("anexoGq03C03", oldValue, this.anexoGq03C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq03C02);
        result = HashCodeUtil.hash(result, this.anexoGq03C03);
        return result;
    }
}

