/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoG.qQuadro07";
    public static final String ANEXOGQ07C701B_LINK = "aAnexoG.qQuadro07.fanexoGq07C701b";
    public static final String ANEXOGQ07C701B = "anexoGq07C701b";
    private String anexoGq07C701b;
    public static final String ANEXOGQ07C701C_LINK = "aAnexoG.qQuadro07.fanexoGq07C701c";
    public static final String ANEXOGQ07C701C = "anexoGq07C701c";
    private Long anexoGq07C701c;
    public static final String ANEXOGQ07C701D_LINK = "aAnexoG.qQuadro07.fanexoGq07C701d";
    public static final String ANEXOGQ07C701D = "anexoGq07C701d";
    private Long anexoGq07C701d;
    public static final String ANEXOGQ07C702B_LINK = "aAnexoG.qQuadro07.fanexoGq07C702b";
    public static final String ANEXOGQ07C702B = "anexoGq07C702b";
    private String anexoGq07C702b;
    public static final String ANEXOGQ07C702C_LINK = "aAnexoG.qQuadro07.fanexoGq07C702c";
    public static final String ANEXOGQ07C702C = "anexoGq07C702c";
    private Long anexoGq07C702c;
    public static final String ANEXOGQ07C702D_LINK = "aAnexoG.qQuadro07.fanexoGq07C702d";
    public static final String ANEXOGQ07C702D = "anexoGq07C702d";
    private Long anexoGq07C702d;
    public static final String ANEXOGQ07C1C_LINK = "aAnexoG.qQuadro07.fanexoGq07C1c";
    public static final String ANEXOGQ07C1C = "anexoGq07C1c";
    private Long anexoGq07C1c;
    public static final String ANEXOGQ07C1D_LINK = "aAnexoG.qQuadro07.fanexoGq07C1d";
    public static final String ANEXOGQ07C1D = "anexoGq07C1d";
    private Long anexoGq07C1d;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq07C701b() {
        return this.anexoGq07C701b;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq07C701b(String anexoGq07C701b) {
        String oldValue = this.anexoGq07C701b;
        this.anexoGq07C701b = anexoGq07C701b;
        this.firePropertyChange("anexoGq07C701b", oldValue, this.anexoGq07C701b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq07C701c() {
        return this.anexoGq07C701c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq07C701c(Long anexoGq07C701c) {
        Long oldValue = this.anexoGq07C701c;
        this.anexoGq07C701c = anexoGq07C701c;
        this.firePropertyChange("anexoGq07C701c", oldValue, this.anexoGq07C701c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq07C701d() {
        return this.anexoGq07C701d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq07C701d(Long anexoGq07C701d) {
        Long oldValue = this.anexoGq07C701d;
        this.anexoGq07C701d = anexoGq07C701d;
        this.firePropertyChange("anexoGq07C701d", oldValue, this.anexoGq07C701d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq07C702b() {
        return this.anexoGq07C702b;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq07C702b(String anexoGq07C702b) {
        String oldValue = this.anexoGq07C702b;
        this.anexoGq07C702b = anexoGq07C702b;
        this.firePropertyChange("anexoGq07C702b", oldValue, this.anexoGq07C702b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq07C702c() {
        return this.anexoGq07C702c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq07C702c(Long anexoGq07C702c) {
        Long oldValue = this.anexoGq07C702c;
        this.anexoGq07C702c = anexoGq07C702c;
        this.firePropertyChange("anexoGq07C702c", oldValue, this.anexoGq07C702c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq07C702d() {
        return this.anexoGq07C702d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq07C702d(Long anexoGq07C702d) {
        Long oldValue = this.anexoGq07C702d;
        this.anexoGq07C702d = anexoGq07C702d;
        this.firePropertyChange("anexoGq07C702d", oldValue, this.anexoGq07C702d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq07C1c() {
        return this.anexoGq07C1c;
    }

    public void setAnexoGq07C1cFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq07C1c")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq07C1c(newValue+=(this.getAnexoGq07C701c() == null ? 0 : this.getAnexoGq07C701c()) + (this.getAnexoGq07C702c() == null ? 0 : this.getAnexoGq07C702c()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq07C1c(Long anexoGq07C1c) {
        Long oldValue = this.anexoGq07C1c;
        this.anexoGq07C1c = anexoGq07C1c;
        this.firePropertyChange("anexoGq07C1c", oldValue, this.anexoGq07C1c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq07C1d() {
        return this.anexoGq07C1d;
    }

    public void setAnexoGq07C1dFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq07C1d")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq07C1d(newValue+=(this.getAnexoGq07C701d() == null ? 0 : this.getAnexoGq07C701d()) + (this.getAnexoGq07C702d() == null ? 0 : this.getAnexoGq07C702d()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq07C1d(Long anexoGq07C1d) {
        Long oldValue = this.anexoGq07C1d;
        this.anexoGq07C1d = anexoGq07C1d;
        this.firePropertyChange("anexoGq07C1d", oldValue, this.anexoGq07C1d);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq07C701b);
        result = HashCodeUtil.hash(result, this.anexoGq07C701c);
        result = HashCodeUtil.hash(result, this.anexoGq07C701d);
        result = HashCodeUtil.hash(result, this.anexoGq07C702b);
        result = HashCodeUtil.hash(result, this.anexoGq07C702c);
        result = HashCodeUtil.hash(result, this.anexoGq07C702d);
        result = HashCodeUtil.hash(result, this.anexoGq07C1c);
        result = HashCodeUtil.hash(result, this.anexoGq07C1d);
        return result;
    }
}

