/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoG.qQuadro02";
    public static final String ANEXOGQ02C01_LINK = "aAnexoG.qQuadro02.fanexoGq02C01";
    public static final String ANEXOGQ02C01 = "anexoGq02C01";
    private Long anexoGq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoGq02C01() {
        return this.anexoGq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq02C01(Long anexoGq02C01) {
        Long oldValue = this.anexoGq02C01;
        this.anexoGq02C01 = anexoGq02C01;
        this.firePropertyChange("anexoGq02C01", oldValue, this.anexoGq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq02C01);
        return result;
    }
}

