/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro08Base
extends QuadroModel {
    public static final String QUADRO08_LINK = "aAnexoG.qQuadro08";
    public static final String ANEXOGQ08T1_LINK = "aAnexoG.qQuadro08.tanexoGq08T1";
    private EventList<AnexoGq08T1_Linha> anexoGq08T1 = new BasicEventList<AnexoGq08T1_Linha>();
    public static final String ANEXOGQ08C1_LINK = "aAnexoG.qQuadro08.fanexoGq08C1";
    public static final String ANEXOGQ08C1 = "anexoGq08C1";
    private Long anexoGq08C1;
    public static final String ANEXOGQ08C2_LINK = "aAnexoG.qQuadro08.fanexoGq08C2";
    public static final String ANEXOGQ08C2 = "anexoGq08C2";
    private Long anexoGq08C2;
    public static final String ANEXOGQ08C3_LINK = "aAnexoG.qQuadro08.fanexoGq08C3";
    public static final String ANEXOGQ08C3 = "anexoGq08C3";
    private Long anexoGq08C3;
    public static final String ANEXOGQ08T2_LINK = "aAnexoG.qQuadro08.tanexoGq08T2";
    private EventList<AnexoGq08T2_Linha> anexoGq08T2 = new BasicEventList<AnexoGq08T2_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoGq08T1_Linha> getAnexoGq08T1() {
        return this.anexoGq08T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoGq08T1(EventList<AnexoGq08T1_Linha> anexoGq08T1) {
        this.anexoGq08T1 = anexoGq08T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq08C1() {
        return this.anexoGq08C1;
    }

    public void setAnexoGq08C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq08C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoGq08T1().size(); ++i) {
            temporaryValue0+=this.getAnexoGq08T1().get(i).getValorRealizacao() == null ? 0 : this.getAnexoGq08T1().get(i).getValorRealizacao();
        }
        this.setAnexoGq08C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq08C1(Long anexoGq08C1) {
        Long oldValue = this.anexoGq08C1;
        this.anexoGq08C1 = anexoGq08C1;
        this.firePropertyChange("anexoGq08C1", oldValue, this.anexoGq08C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq08C2() {
        return this.anexoGq08C2;
    }

    public void setAnexoGq08C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq08C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoGq08T1().size(); ++i) {
            temporaryValue0+=this.getAnexoGq08T1().get(i).getValorAquisicao() == null ? 0 : this.getAnexoGq08T1().get(i).getValorAquisicao();
        }
        this.setAnexoGq08C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq08C2(Long anexoGq08C2) {
        Long oldValue = this.anexoGq08C2;
        this.anexoGq08C2 = anexoGq08C2;
        this.firePropertyChange("anexoGq08C2", oldValue, this.anexoGq08C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq08C3() {
        return this.anexoGq08C3;
    }

    public void setAnexoGq08C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq08C3")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoGq08T1().size(); ++i) {
            temporaryValue0+=this.getAnexoGq08T1().get(i).getDespesas() == null ? 0 : this.getAnexoGq08T1().get(i).getDespesas();
        }
        this.setAnexoGq08C3(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq08C3(Long anexoGq08C3) {
        Long oldValue = this.anexoGq08C3;
        this.anexoGq08C3 = anexoGq08C3;
        this.firePropertyChange("anexoGq08C3", oldValue, this.anexoGq08C3);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoGq08T2_Linha> getAnexoGq08T2() {
        return this.anexoGq08T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoGq08T2(EventList<AnexoGq08T2_Linha> anexoGq08T2) {
        this.anexoGq08T2 = anexoGq08T2;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq08T1);
        result = HashCodeUtil.hash(result, this.anexoGq08C1);
        result = HashCodeUtil.hash(result, this.anexoGq08C2);
        result = HashCodeUtil.hash(result, this.anexoGq08C3);
        result = HashCodeUtil.hash(result, this.anexoGq08T2);
        return result;
    }
}

