/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro10;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.util.HashCodeUtil;

@Min(value=0)
@Max(value=1)
public class AnexoGModelBase
extends AnexoModel {
    public static final String ANEXOG_LINK = "aAnexoG";

    public AnexoGModelBase(FormKey key, boolean addQuadrosToModel) {
        super(key);
        if (addQuadrosToModel) {
            this.addQuadro(new Quadro02());
            this.addQuadro(new Quadro03());
            this.addQuadro(new Quadro04());
            this.addQuadro(new Quadro05());
            this.addQuadro(new Quadro06());
            this.addQuadro(new Quadro07());
            this.addQuadro(new Quadro08());
            this.addQuadro(new Quadro09());
            this.addQuadro(new Quadro10());
        }
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro02.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro03.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro04.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro05.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro06.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro07.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro08.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro09.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro10.class.getSimpleName()));
        return result;
    }
}

