/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModelBase;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro10;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=1)
public class AnexoGModel
extends AnexoGModelBase {
    public AnexoGModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public Quadro07 getQuadro07() {
        return (Quadro07)this.getQuadro(Quadro07.class.getSimpleName());
    }

    public Quadro08 getQuadro08() {
        return (Quadro08)this.getQuadro(Quadro08.class.getSimpleName());
    }

    public Quadro09 getQuadro09() {
        return (Quadro09)this.getQuadro(Quadro09.class.getSimpleName());
    }

    public Quadro10 getQuadro10() {
        return (Quadro10)this.getQuadro(Quadro10.class.getSimpleName());
    }

    public boolean isEmpty() {
        return this.getQuadro04().isEmpty() && this.getQuadro05().isEmpty() && this.getQuadro06().isEmpty() && this.getQuadro07().isEmpty() && this.getQuadro08().isEmpty() && this.getQuadro09().isEmpty() && this.getQuadro10().isEmpty();
    }
}

