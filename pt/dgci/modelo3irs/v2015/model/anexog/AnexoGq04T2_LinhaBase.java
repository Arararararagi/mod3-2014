/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoGq04T2_LinhaBase
extends Model {
    public static final String TITULAR_LINK = "aAnexoG.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String MOVEISIMOVEISBENS_LINK = "aAnexoG.qQuadro04.fmoveisImoveisBens";
    public static final String MOVEISIMOVEISBENS = "moveisImoveisBens";
    private String moveisImoveisBens;
    public static final String ANOAFECTACAO_LINK = "aAnexoG.qQuadro04.fanoAfectacao";
    public static final String ANOAFECTACAO = "anoAfectacao";
    private Long anoAfectacao;
    public static final String MESAFECTACAO_LINK = "aAnexoG.qQuadro04.fmesAfectacao";
    public static final String MESAFECTACAO = "mesAfectacao";
    private Long mesAfectacao;
    public static final String VALORAFECTACAO_LINK = "aAnexoG.qQuadro04.fvalorAfectacao";
    public static final String VALORAFECTACAO = "valorAfectacao";
    private Long valorAfectacao;
    public static final String ANOAQUISICAO_LINK = "aAnexoG.qQuadro04.fanoAquisicao";
    public static final String ANOAQUISICAO = "anoAquisicao";
    private Long anoAquisicao;
    public static final String MESAQUISICAO_LINK = "aAnexoG.qQuadro04.fmesAquisicao";
    public static final String MESAQUISICAO = "mesAquisicao";
    private Long mesAquisicao;
    public static final String VALORAQUISICAO_LINK = "aAnexoG.qQuadro04.fvalorAquisicao";
    public static final String VALORAQUISICAO = "valorAquisicao";
    private Long valorAquisicao;
    public static final String FREGUESIA_LINK = "aAnexoG.qQuadro04.ffreguesia";
    public static final String FREGUESIA = "freguesia";
    private String freguesia;
    public static final String TIPOPREDIO_LINK = "aAnexoG.qQuadro04.ftipoPredio";
    public static final String TIPOPREDIO = "tipoPredio";
    private String tipoPredio;
    public static final String ARTIGO_LINK = "aAnexoG.qQuadro04.fartigo";
    public static final String ARTIGO = "artigo";
    private Long artigo;
    public static final String FRACCAO_LINK = "aAnexoG.qQuadro04.ffraccao";
    public static final String FRACCAO = "fraccao";
    private String fraccao;
    public static final String QUOTAPARTE_LINK = "aAnexoG.qQuadro04.fquotaParte";
    public static final String QUOTAPARTE = "quotaParte";
    private Long quotaParte;

    public AnexoGq04T2_LinhaBase(String titular, String moveisImoveisBens, Long anoAfectacao, Long mesAfectacao, Long valorAfectacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, String freguesia, String tipoPredio, Long artigo, String fraccao, Long quotaParte) {
        this.titular = titular;
        this.moveisImoveisBens = moveisImoveisBens;
        this.anoAfectacao = anoAfectacao;
        this.mesAfectacao = mesAfectacao;
        this.valorAfectacao = valorAfectacao;
        this.anoAquisicao = anoAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.freguesia = freguesia;
        this.tipoPredio = tipoPredio;
        this.artigo = artigo;
        this.fraccao = fraccao;
        this.quotaParte = quotaParte;
    }

    public AnexoGq04T2_LinhaBase() {
        this.titular = null;
        this.moveisImoveisBens = null;
        this.anoAfectacao = null;
        this.mesAfectacao = null;
        this.valorAfectacao = null;
        this.anoAquisicao = null;
        this.mesAquisicao = null;
        this.valorAquisicao = null;
        this.freguesia = null;
        this.tipoPredio = null;
        this.artigo = null;
        this.fraccao = null;
        this.quotaParte = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ4BensMoveisImoveis")
    public String getMoveisImoveisBens() {
        return this.moveisImoveisBens;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMoveisImoveisBens(String moveisImoveisBens) {
        String oldValue = this.moveisImoveisBens;
        this.moveisImoveisBens = moveisImoveisBens;
        this.firePropertyChange("moveisImoveisBens", oldValue, this.moveisImoveisBens);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoAfectacao() {
        return this.anoAfectacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoAfectacao(Long anoAfectacao) {
        Long oldValue = this.anoAfectacao;
        this.anoAfectacao = anoAfectacao;
        this.firePropertyChange("anoAfectacao", oldValue, this.anoAfectacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesAfectacao() {
        return this.mesAfectacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesAfectacao(Long mesAfectacao) {
        Long oldValue = this.mesAfectacao;
        this.mesAfectacao = mesAfectacao;
        this.firePropertyChange("mesAfectacao", oldValue, this.mesAfectacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorAfectacao() {
        return this.valorAfectacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorAfectacao(Long valorAfectacao) {
        Long oldValue = this.valorAfectacao;
        this.valorAfectacao = valorAfectacao;
        this.firePropertyChange("valorAfectacao", oldValue, this.valorAfectacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoAquisicao() {
        return this.anoAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoAquisicao(Long anoAquisicao) {
        Long oldValue = this.anoAquisicao;
        this.anoAquisicao = anoAquisicao;
        this.firePropertyChange("anoAquisicao", oldValue, this.anoAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesAquisicao() {
        return this.mesAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesAquisicao(Long mesAquisicao) {
        Long oldValue = this.mesAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.firePropertyChange("mesAquisicao", oldValue, this.mesAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorAquisicao() {
        return this.valorAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorAquisicao(Long valorAquisicao) {
        Long oldValue = this.valorAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.firePropertyChange("valorAquisicao", oldValue, this.valorAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFreguesia() {
        return this.freguesia;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFreguesia(String freguesia) {
        String oldValue = this.freguesia;
        this.freguesia = freguesia;
        this.firePropertyChange("freguesia", oldValue, this.freguesia);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio")
    public String getTipoPredio() {
        return this.tipoPredio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTipoPredio(String tipoPredio) {
        String oldValue = this.tipoPredio;
        this.tipoPredio = tipoPredio;
        this.firePropertyChange("tipoPredio", oldValue, this.tipoPredio);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getArtigo() {
        return this.artigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setArtigo(Long artigo) {
        Long oldValue = this.artigo;
        this.artigo = artigo;
        this.firePropertyChange("artigo", oldValue, this.artigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFraccao() {
        return this.fraccao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFraccao(String fraccao) {
        String oldValue = this.fraccao;
        this.fraccao = fraccao;
        this.firePropertyChange("fraccao", oldValue, this.fraccao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQuotaParte() {
        return this.quotaParte;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQuotaParte(Long quotaParte) {
        Long oldValue = this.quotaParte;
        this.quotaParte = quotaParte;
        this.firePropertyChange("quotaParte", oldValue, this.quotaParte);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.moveisImoveisBens);
        result = HashCodeUtil.hash(result, this.anoAfectacao);
        result = HashCodeUtil.hash(result, this.mesAfectacao);
        result = HashCodeUtil.hash(result, this.valorAfectacao);
        result = HashCodeUtil.hash(result, this.anoAquisicao);
        result = HashCodeUtil.hash(result, this.mesAquisicao);
        result = HashCodeUtil.hash(result, this.valorAquisicao);
        result = HashCodeUtil.hash(result, this.freguesia);
        result = HashCodeUtil.hash(result, this.tipoPredio);
        result = HashCodeUtil.hash(result, this.artigo);
        result = HashCodeUtil.hash(result, this.fraccao);
        result = HashCodeUtil.hash(result, this.quotaParte);
        return result;
    }

    public static enum Property {
        TITULAR("titular", 2),
        MOVEISIMOVEISBENS("moveisImoveisBens", 3),
        ANOAFECTACAO("anoAfectacao", 4),
        MESAFECTACAO("mesAfectacao", 5),
        VALORAFECTACAO("valorAfectacao", 6),
        ANOAQUISICAO("anoAquisicao", 7),
        MESAQUISICAO("mesAquisicao", 8),
        VALORAQUISICAO("valorAquisicao", 9),
        FREGUESIA("freguesia", 10),
        TIPOPREDIO("tipoPredio", 11),
        ARTIGO("artigo", 12),
        FRACCAO("fraccao", 13),
        QUOTAPARTE("quotaParte", 14);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

