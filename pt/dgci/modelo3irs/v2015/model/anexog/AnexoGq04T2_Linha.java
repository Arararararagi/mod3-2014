/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T2_LinhaBase;

public class AnexoGq04T2_Linha
extends AnexoGq04T2_LinhaBase {
    public AnexoGq04T2_Linha() {
    }

    public AnexoGq04T2_Linha(String titular, String bensMoveisImoveis, Long anoAfectacao, Long mesAfectacao, Long valorAfectacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, String freguesia, String tipoPredio, Long artigo, String fraccao, Long quotaParte) {
        super(titular, bensMoveisImoveis, anoAfectacao, mesAfectacao, valorAfectacao, anoAquisicao, mesAquisicao, valorAquisicao, freguesia, tipoPredio, artigo, fraccao, quotaParte);
    }

    public static String getLink(int numLinha) {
        return "aAnexoG.qQuadro04.tanexoGq04T2.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoGq04T2_LinhaBase.Property column) {
        return AnexoGq04T2_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

