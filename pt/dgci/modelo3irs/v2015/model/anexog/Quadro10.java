/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq10T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro10Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro10
extends Quadro10Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq10C1b()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq10C1c()) && this.getAnexoGq10T1().isEmpty();
    }
}

