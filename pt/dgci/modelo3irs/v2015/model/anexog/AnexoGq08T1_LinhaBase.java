/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoGq08T1_LinhaBase
extends Model {
    public static final String NLINHA_LINK = "aAnexoG.qQuadro08.fnLinha";
    public static final String NLINHA = "nLinha";
    private Long nLinha;
    public static final String TITULAR_LINK = "aAnexoG.qQuadro08.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String NIF_LINK = "aAnexoG.qQuadro08.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;
    public static final String CODENCARGOS_LINK = "aAnexoG.qQuadro08.fcodEncargos";
    public static final String CODENCARGOS = "codEncargos";
    private Long codEncargos;
    public static final String ANOREALIZACAO_LINK = "aAnexoG.qQuadro08.fanoRealizacao";
    public static final String ANOREALIZACAO = "anoRealizacao";
    private Long anoRealizacao;
    public static final String MESREALIZACAO_LINK = "aAnexoG.qQuadro08.fmesRealizacao";
    public static final String MESREALIZACAO = "mesRealizacao";
    private Long mesRealizacao;
    public static final String VALORREALIZACAO_LINK = "aAnexoG.qQuadro08.fvalorRealizacao";
    public static final String VALORREALIZACAO = "valorRealizacao";
    private Long valorRealizacao;
    public static final String ANOAQUISICAO_LINK = "aAnexoG.qQuadro08.fanoAquisicao";
    public static final String ANOAQUISICAO = "anoAquisicao";
    private Long anoAquisicao;
    public static final String MESAQUISICAO_LINK = "aAnexoG.qQuadro08.fmesAquisicao";
    public static final String MESAQUISICAO = "mesAquisicao";
    private Long mesAquisicao;
    public static final String VALORAQUISICAO_LINK = "aAnexoG.qQuadro08.fvalorAquisicao";
    public static final String VALORAQUISICAO = "valorAquisicao";
    private Long valorAquisicao;
    public static final String DESPESAS_LINK = "aAnexoG.qQuadro08.fdespesas";
    public static final String DESPESAS = "despesas";
    private Long despesas;

    public AnexoGq08T1_LinhaBase(Long nLinha, String titular, Long nIF, Long codEncargos, Long anoRealizacao, Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, Long despesas) {
        this.nLinha = nLinha;
        this.titular = titular;
        this.nIF = nIF;
        this.codEncargos = codEncargos;
        this.anoRealizacao = anoRealizacao;
        this.mesRealizacao = mesRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.anoAquisicao = anoAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.despesas = despesas;
    }

    public AnexoGq08T1_LinhaBase() {
        this.nLinha = null;
        this.titular = null;
        this.nIF = null;
        this.codEncargos = null;
        this.anoRealizacao = null;
        this.mesRealizacao = null;
        this.valorRealizacao = null;
        this.anoAquisicao = null;
        this.mesAquisicao = null;
        this.valorAquisicao = null;
        this.despesas = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ8")
    public Long getNLinha() {
        return this.nLinha;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNLinha(Long nLinha) {
        Long oldValue = this.nLinha;
        this.nLinha = nLinha;
        this.firePropertyChange("nLinha", oldValue, this.nLinha);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoGQ8CodEncargos")
    public Long getCodEncargos() {
        return this.codEncargos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodEncargos(Long codEncargos) {
        Long oldValue = this.codEncargos;
        this.codEncargos = codEncargos;
        this.firePropertyChange("codEncargos", oldValue, this.codEncargos);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoRealizacao() {
        return this.anoRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoRealizacao(Long anoRealizacao) {
        Long oldValue = this.anoRealizacao;
        this.anoRealizacao = anoRealizacao;
        this.firePropertyChange("anoRealizacao", oldValue, this.anoRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesRealizacao() {
        return this.mesRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesRealizacao(Long mesRealizacao) {
        Long oldValue = this.mesRealizacao;
        this.mesRealizacao = mesRealizacao;
        this.firePropertyChange("mesRealizacao", oldValue, this.mesRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorRealizacao() {
        return this.valorRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorRealizacao(Long valorRealizacao) {
        Long oldValue = this.valorRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.firePropertyChange("valorRealizacao", oldValue, this.valorRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoAquisicao() {
        return this.anoAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoAquisicao(Long anoAquisicao) {
        Long oldValue = this.anoAquisicao;
        this.anoAquisicao = anoAquisicao;
        this.firePropertyChange("anoAquisicao", oldValue, this.anoAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesAquisicao() {
        return this.mesAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesAquisicao(Long mesAquisicao) {
        Long oldValue = this.mesAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.firePropertyChange("mesAquisicao", oldValue, this.mesAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorAquisicao() {
        return this.valorAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorAquisicao(Long valorAquisicao) {
        Long oldValue = this.valorAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.firePropertyChange("valorAquisicao", oldValue, this.valorAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getDespesas() {
        return this.despesas;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setDespesas(Long despesas) {
        Long oldValue = this.despesas;
        this.despesas = despesas;
        this.firePropertyChange("despesas", oldValue, this.despesas);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nLinha);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.codEncargos);
        result = HashCodeUtil.hash(result, this.anoRealizacao);
        result = HashCodeUtil.hash(result, this.mesRealizacao);
        result = HashCodeUtil.hash(result, this.valorRealizacao);
        result = HashCodeUtil.hash(result, this.anoAquisicao);
        result = HashCodeUtil.hash(result, this.mesAquisicao);
        result = HashCodeUtil.hash(result, this.valorAquisicao);
        result = HashCodeUtil.hash(result, this.despesas);
        return result;
    }

    public static enum Property {
        NLINHA("nLinha", 2),
        TITULAR("titular", 3),
        NIF("nIF", 4),
        CODENCARGOS("codEncargos", 5),
        ANOREALIZACAO("anoRealizacao", 6),
        MESREALIZACAO("mesRealizacao", 7),
        VALORREALIZACAO("valorRealizacao", 8),
        ANOAQUISICAO("anoAquisicao", 9),
        MESAQUISICAO("mesAquisicao", 10),
        VALORAQUISICAO("valorAquisicao", 11),
        DESPESAS("despesas", 12);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

