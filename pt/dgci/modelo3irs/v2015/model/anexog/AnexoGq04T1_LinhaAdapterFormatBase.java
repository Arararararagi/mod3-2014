/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoGq04T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoGq04T1_Linha>,
WritableTableFormat<AnexoGq04T1_Linha> {
    @Override
    public boolean isEditable(AnexoGq04T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoGq04T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getNLinha() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoGQ4", line.getNLinha());
            }
            case 2: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos", line.getTitular());
            }
            case 3: {
                return line.getAnoRealizacao();
            }
            case 4: {
                return line.getMesRealizacao();
            }
            case 5: {
                return line.getValorRealizacao();
            }
            case 6: {
                return line.getAnoAquisicao();
            }
            case 7: {
                return line.getMesAquisicao();
            }
            case 8: {
                return line.getValorAquisicao();
            }
            case 9: {
                return line.getDespesas();
            }
            case 10: {
                return line.getFreguesia();
            }
            case 11: {
                if (line == null || line.getTipoPredio() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_TipoPredio", line.getTipoPredio());
            }
            case 12: {
                return line.getArtigo();
            }
            case 13: {
                return line.getFraccao();
            }
            case 14: {
                return line.getQuotaParte();
            }
        }
        return null;
    }

    @Override
    public AnexoGq04T1_Linha setColumnValue(AnexoGq04T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setNLinha((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setAnoRealizacao((Long)value);
                return line;
            }
            case 4: {
                line.setMesRealizacao((Long)value);
                return line;
            }
            case 5: {
                line.setValorRealizacao((Long)value);
                return line;
            }
            case 6: {
                line.setAnoAquisicao((Long)value);
                return line;
            }
            case 7: {
                line.setMesAquisicao((Long)value);
                return line;
            }
            case 8: {
                line.setValorAquisicao((Long)value);
                return line;
            }
            case 9: {
                line.setDespesas((Long)value);
                return line;
            }
            case 10: {
                line.setFreguesia((String)value);
                return line;
            }
            case 11: {
                if (value != null) {
                    line.setTipoPredio((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 12: {
                line.setArtigo((Long)value);
                return line;
            }
            case 13: {
                line.setFraccao((String)value);
                return line;
            }
            case 14: {
                line.setQuotaParte((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return Long.class;
            }
            case 10: {
                return String.class;
            }
            case 11: {
                return String.class;
            }
            case 12: {
                return Long.class;
            }
            case 13: {
                return String.class;
            }
            case 14: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 15;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "N\u00ba da Linha";
            }
            case 2: {
                return "Titular";
            }
            case 3: {
                return "Ano";
            }
            case 4: {
                return "M\u00eas";
            }
            case 5: {
                return "Valor";
            }
            case 6: {
                return "Ano";
            }
            case 7: {
                return "M\u00eas";
            }
            case 8: {
                return "Valor";
            }
            case 9: {
                return "Despesas e Encargos";
            }
            case 10: {
                return "Freguesia (c\u00f3digo)";
            }
            case 11: {
                return "Tipo";
            }
            case 12: {
                return "Artigo";
            }
            case 13: {
                return "Fra\u00e7\u00e3o / Sec\u00e7\u00e3o";
            }
            case 14: {
                return "Quota-Parte %";
            }
        }
        return null;
    }
}

