/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T2_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoGq08T2_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoGq08T2_Linha>,
WritableTableFormat<AnexoGq08T2_Linha> {
    @Override
    public boolean isEditable(AnexoGq08T2_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoGq08T2_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCampoQ8() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoGQ8", line.getCampoQ8());
            }
            case 2: {
                return line.getNipc();
            }
        }
        return null;
    }

    @Override
    public AnexoGq08T2_Linha setColumnValue(AnexoGq08T2_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCampoQ8((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setNipc((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Campo do Q. 8";
            }
            case 2: {
                return "NIPC da Sociedade";
            }
        }
        return null;
    }
}

