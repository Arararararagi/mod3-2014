/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoG.qQuadro06";
    public static final String ANEXOGQ06C601B_LINK = "aAnexoG.qQuadro06.fanexoGq06C601b";
    public static final String ANEXOGQ06C601B = "anexoGq06C601b";
    private String anexoGq06C601b;
    public static final String ANEXOGQ06C601C_LINK = "aAnexoG.qQuadro06.fanexoGq06C601c";
    public static final String ANEXOGQ06C601C = "anexoGq06C601c";
    private Long anexoGq06C601c;
    public static final String ANEXOGQ06C601D_LINK = "aAnexoG.qQuadro06.fanexoGq06C601d";
    public static final String ANEXOGQ06C601D = "anexoGq06C601d";
    private Long anexoGq06C601d;
    public static final String ANEXOGQ06C601E_LINK = "aAnexoG.qQuadro06.fanexoGq06C601e";
    public static final String ANEXOGQ06C601E = "anexoGq06C601e";
    private Long anexoGq06C601e;
    public static final String ANEXOGQ06C602B_LINK = "aAnexoG.qQuadro06.fanexoGq06C602b";
    public static final String ANEXOGQ06C602B = "anexoGq06C602b";
    private String anexoGq06C602b;
    public static final String ANEXOGQ06C602C_LINK = "aAnexoG.qQuadro06.fanexoGq06C602c";
    public static final String ANEXOGQ06C602C = "anexoGq06C602c";
    private Long anexoGq06C602c;
    public static final String ANEXOGQ06C602D_LINK = "aAnexoG.qQuadro06.fanexoGq06C602d";
    public static final String ANEXOGQ06C602D = "anexoGq06C602d";
    private Long anexoGq06C602d;
    public static final String ANEXOGQ06C602E_LINK = "aAnexoG.qQuadro06.fanexoGq06C602e";
    public static final String ANEXOGQ06C602E = "anexoGq06C602e";
    private Long anexoGq06C602e;
    public static final String ANEXOGQ06C1C_LINK = "aAnexoG.qQuadro06.fanexoGq06C1c";
    public static final String ANEXOGQ06C1C = "anexoGq06C1c";
    private Long anexoGq06C1c;
    public static final String ANEXOGQ06C1D_LINK = "aAnexoG.qQuadro06.fanexoGq06C1d";
    public static final String ANEXOGQ06C1D = "anexoGq06C1d";
    private Long anexoGq06C1d;
    public static final String ANEXOGQ06C1E_LINK = "aAnexoG.qQuadro06.fanexoGq06C1e";
    public static final String ANEXOGQ06C1E = "anexoGq06C1e";
    private Long anexoGq06C1e;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq06C601b() {
        return this.anexoGq06C601b;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq06C601b(String anexoGq06C601b) {
        String oldValue = this.anexoGq06C601b;
        this.anexoGq06C601b = anexoGq06C601b;
        this.firePropertyChange("anexoGq06C601b", oldValue, this.anexoGq06C601b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C601c() {
        return this.anexoGq06C601c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C601c(Long anexoGq06C601c) {
        Long oldValue = this.anexoGq06C601c;
        this.anexoGq06C601c = anexoGq06C601c;
        this.firePropertyChange("anexoGq06C601c", oldValue, this.anexoGq06C601c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C601d() {
        return this.anexoGq06C601d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C601d(Long anexoGq06C601d) {
        Long oldValue = this.anexoGq06C601d;
        this.anexoGq06C601d = anexoGq06C601d;
        this.firePropertyChange("anexoGq06C601d", oldValue, this.anexoGq06C601d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C601e() {
        return this.anexoGq06C601e;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C601e(Long anexoGq06C601e) {
        Long oldValue = this.anexoGq06C601e;
        this.anexoGq06C601e = anexoGq06C601e;
        this.firePropertyChange("anexoGq06C601e", oldValue, this.anexoGq06C601e);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq06C602b() {
        return this.anexoGq06C602b;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq06C602b(String anexoGq06C602b) {
        String oldValue = this.anexoGq06C602b;
        this.anexoGq06C602b = anexoGq06C602b;
        this.firePropertyChange("anexoGq06C602b", oldValue, this.anexoGq06C602b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C602c() {
        return this.anexoGq06C602c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C602c(Long anexoGq06C602c) {
        Long oldValue = this.anexoGq06C602c;
        this.anexoGq06C602c = anexoGq06C602c;
        this.firePropertyChange("anexoGq06C602c", oldValue, this.anexoGq06C602c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C602d() {
        return this.anexoGq06C602d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C602d(Long anexoGq06C602d) {
        Long oldValue = this.anexoGq06C602d;
        this.anexoGq06C602d = anexoGq06C602d;
        this.firePropertyChange("anexoGq06C602d", oldValue, this.anexoGq06C602d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C602e() {
        return this.anexoGq06C602e;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C602e(Long anexoGq06C602e) {
        Long oldValue = this.anexoGq06C602e;
        this.anexoGq06C602e = anexoGq06C602e;
        this.firePropertyChange("anexoGq06C602e", oldValue, this.anexoGq06C602e);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C1c() {
        return this.anexoGq06C1c;
    }

    public void setAnexoGq06C1cFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq06C1c")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq06C1c(newValue+=(this.getAnexoGq06C601c() == null ? 0 : this.getAnexoGq06C601c()) + (this.getAnexoGq06C602c() == null ? 0 : this.getAnexoGq06C602c()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C1c(Long anexoGq06C1c) {
        Long oldValue = this.anexoGq06C1c;
        this.anexoGq06C1c = anexoGq06C1c;
        this.firePropertyChange("anexoGq06C1c", oldValue, this.anexoGq06C1c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C1d() {
        return this.anexoGq06C1d;
    }

    public void setAnexoGq06C1dFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq06C1d")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq06C1d(newValue+=(this.getAnexoGq06C601d() == null ? 0 : this.getAnexoGq06C601d()) + (this.getAnexoGq06C602d() == null ? 0 : this.getAnexoGq06C602d()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C1d(Long anexoGq06C1d) {
        Long oldValue = this.anexoGq06C1d;
        this.anexoGq06C1d = anexoGq06C1d;
        this.firePropertyChange("anexoGq06C1d", oldValue, this.anexoGq06C1d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq06C1e() {
        return this.anexoGq06C1e;
    }

    public void setAnexoGq06C1eFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq06C1e")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq06C1e(newValue+=(this.getAnexoGq06C601e() == null ? 0 : this.getAnexoGq06C601e()) + (this.getAnexoGq06C602e() == null ? 0 : this.getAnexoGq06C602e()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq06C1e(Long anexoGq06C1e) {
        Long oldValue = this.anexoGq06C1e;
        this.anexoGq06C1e = anexoGq06C1e;
        this.firePropertyChange("anexoGq06C1e", oldValue, this.anexoGq06C1e);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq06C601b);
        result = HashCodeUtil.hash(result, this.anexoGq06C601c);
        result = HashCodeUtil.hash(result, this.anexoGq06C601d);
        result = HashCodeUtil.hash(result, this.anexoGq06C601e);
        result = HashCodeUtil.hash(result, this.anexoGq06C602b);
        result = HashCodeUtil.hash(result, this.anexoGq06C602c);
        result = HashCodeUtil.hash(result, this.anexoGq06C602d);
        result = HashCodeUtil.hash(result, this.anexoGq06C602e);
        result = HashCodeUtil.hash(result, this.anexoGq06C1c);
        result = HashCodeUtil.hash(result, this.anexoGq06C1d);
        result = HashCodeUtil.hash(result, this.anexoGq06C1e);
        return result;
    }
}

