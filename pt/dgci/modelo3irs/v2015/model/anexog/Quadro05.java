/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.Quadro05Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class Quadro05
extends Quadro05Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C505()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C506()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C507()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C508()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C509()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C510()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C511()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C525()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C526()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C527()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C528()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C529()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C530()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05C531()) && StringUtil.isEmpty(this.getAnexoGq05AC1()) && StringUtil.isEmpty(this.getAnexoGq05AC2()) && StringUtil.isEmpty(this.getAnexoGq05AC3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05AC4()) && StringUtil.isEmpty(this.getAnexoGq05AC5()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05AC6()) && StringUtil.isEmpty(this.getAnexoGq05AC7()) && StringUtil.isEmpty(this.getAnexoGq05AC8()) && StringUtil.isEmpty(this.getAnexoGq05AC9()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05AC10()) && StringUtil.isEmpty(this.getAnexoGq05AC11()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoGq05AC12());
    }
}

