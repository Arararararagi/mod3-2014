/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_LinhaBase;

public class AnexoGq08T1_Linha
extends AnexoGq08T1_LinhaBase {
    public AnexoGq08T1_Linha() {
    }

    public AnexoGq08T1_Linha(Long nLinha, String titular, Long nIF, Long codEncargos, Long anoRealizacao, Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao, Long despesas) {
        super(nLinha, titular, nIF, codEncargos, anoRealizacao, mesRealizacao, valorRealizacao, anoAquisicao, mesAquisicao, valorAquisicao, despesas);
    }

    public static String getLink(int numLinha) {
        return "aAnexoG.qQuadro08.tanexoGq08T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoGq08T1_LinhaBase.Property column) {
        return AnexoGq08T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

