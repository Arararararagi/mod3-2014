/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro09Base
extends QuadroModel {
    public static final String QUADRO09_LINK = "aAnexoG.qQuadro09";
    public static final String ANEXOGQ09C901A_LINK = "aAnexoG.qQuadro09.fanexoGq09C901a";
    public static final String ANEXOGQ09C901A = "anexoGq09C901a";
    private String anexoGq09C901a;
    public static final String ANEXOGQ09C901B_LINK = "aAnexoG.qQuadro09.fanexoGq09C901b";
    public static final String ANEXOGQ09C901B = "anexoGq09C901b";
    private Long anexoGq09C901b;
    public static final String ANEXOGQ09C902A_LINK = "aAnexoG.qQuadro09.fanexoGq09C902a";
    public static final String ANEXOGQ09C902A = "anexoGq09C902a";
    private String anexoGq09C902a;
    public static final String ANEXOGQ09C902B_LINK = "aAnexoG.qQuadro09.fanexoGq09C902b";
    public static final String ANEXOGQ09C902B = "anexoGq09C902b";
    private Long anexoGq09C902b;
    public static final String ANEXOGQ09C903A_LINK = "aAnexoG.qQuadro09.fanexoGq09C903a";
    public static final String ANEXOGQ09C903A = "anexoGq09C903a";
    private String anexoGq09C903a;
    public static final String ANEXOGQ09C903B_LINK = "aAnexoG.qQuadro09.fanexoGq09C903b";
    public static final String ANEXOGQ09C903B = "anexoGq09C903b";
    private Long anexoGq09C903b;
    public static final String ANEXOGQ09C904A_LINK = "aAnexoG.qQuadro09.fanexoGq09C904a";
    public static final String ANEXOGQ09C904A = "anexoGq09C904a";
    private String anexoGq09C904a;
    public static final String ANEXOGQ09C904B_LINK = "aAnexoG.qQuadro09.fanexoGq09C904b";
    public static final String ANEXOGQ09C904B = "anexoGq09C904b";
    private Long anexoGq09C904b;
    public static final String ANEXOGQ09C905A_LINK = "aAnexoG.qQuadro09.fanexoGq09C905a";
    public static final String ANEXOGQ09C905A = "anexoGq09C905a";
    private String anexoGq09C905a;
    public static final String ANEXOGQ09C905B_LINK = "aAnexoG.qQuadro09.fanexoGq09C905b";
    public static final String ANEXOGQ09C905B = "anexoGq09C905b";
    private Long anexoGq09C905b;
    public static final String ANEXOGQ09C1B_LINK = "aAnexoG.qQuadro09.fanexoGq09C1b";
    public static final String ANEXOGQ09C1B = "anexoGq09C1b";
    private Long anexoGq09C1b;
    public static final String ANEXOGQ09B1OP1_LINK = "aAnexoG.qQuadro09.fanexoGq09B1OP1";
    public static final String ANEXOGQ09B1OP1_VALUE = "1";
    public static final String ANEXOGQ09B1OP2_LINK = "aAnexoG.qQuadro09.fanexoGq09B1OP2";
    public static final String ANEXOGQ09B1OP2_VALUE = "2";
    public static final String ANEXOGQ09B1 = "anexoGq09B1";
    private String anexoGq09B1;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq09C901a() {
        return this.anexoGq09C901a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq09C901a(String anexoGq09C901a) {
        String oldValue = this.anexoGq09C901a;
        this.anexoGq09C901a = anexoGq09C901a;
        this.firePropertyChange("anexoGq09C901a", oldValue, this.anexoGq09C901a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq09C901b() {
        return this.anexoGq09C901b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq09C901b(Long anexoGq09C901b) {
        Long oldValue = this.anexoGq09C901b;
        this.anexoGq09C901b = anexoGq09C901b;
        this.firePropertyChange("anexoGq09C901b", oldValue, this.anexoGq09C901b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq09C902a() {
        return this.anexoGq09C902a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq09C902a(String anexoGq09C902a) {
        String oldValue = this.anexoGq09C902a;
        this.anexoGq09C902a = anexoGq09C902a;
        this.firePropertyChange("anexoGq09C902a", oldValue, this.anexoGq09C902a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq09C902b() {
        return this.anexoGq09C902b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq09C902b(Long anexoGq09C902b) {
        Long oldValue = this.anexoGq09C902b;
        this.anexoGq09C902b = anexoGq09C902b;
        this.firePropertyChange("anexoGq09C902b", oldValue, this.anexoGq09C902b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq09C903a() {
        return this.anexoGq09C903a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq09C903a(String anexoGq09C903a) {
        String oldValue = this.anexoGq09C903a;
        this.anexoGq09C903a = anexoGq09C903a;
        this.firePropertyChange("anexoGq09C903a", oldValue, this.anexoGq09C903a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq09C903b() {
        return this.anexoGq09C903b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq09C903b(Long anexoGq09C903b) {
        Long oldValue = this.anexoGq09C903b;
        this.anexoGq09C903b = anexoGq09C903b;
        this.firePropertyChange("anexoGq09C903b", oldValue, this.anexoGq09C903b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq09C904a() {
        return this.anexoGq09C904a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq09C904a(String anexoGq09C904a) {
        String oldValue = this.anexoGq09C904a;
        this.anexoGq09C904a = anexoGq09C904a;
        this.firePropertyChange("anexoGq09C904a", oldValue, this.anexoGq09C904a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq09C904b() {
        return this.anexoGq09C904b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq09C904b(Long anexoGq09C904b) {
        Long oldValue = this.anexoGq09C904b;
        this.anexoGq09C904b = anexoGq09C904b;
        this.firePropertyChange("anexoGq09C904b", oldValue, this.anexoGq09C904b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getAnexoGq09C905a() {
        return this.anexoGq09C905a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq09C905a(String anexoGq09C905a) {
        String oldValue = this.anexoGq09C905a;
        this.anexoGq09C905a = anexoGq09C905a;
        this.firePropertyChange("anexoGq09C905a", oldValue, this.anexoGq09C905a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq09C905b() {
        return this.anexoGq09C905b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq09C905b(Long anexoGq09C905b) {
        Long oldValue = this.anexoGq09C905b;
        this.anexoGq09C905b = anexoGq09C905b;
        this.firePropertyChange("anexoGq09C905b", oldValue, this.anexoGq09C905b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoGq09C1b() {
        return this.anexoGq09C1b;
    }

    public void setAnexoGq09C1bFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoGq09C1b")) {
            return;
        }
        long newValue = 0;
        this.setAnexoGq09C1b(newValue+=(this.getAnexoGq09C901b() == null ? 0 : this.getAnexoGq09C901b()) + (this.getAnexoGq09C902b() == null ? 0 : this.getAnexoGq09C902b()) + (this.getAnexoGq09C903b() == null ? 0 : this.getAnexoGq09C903b()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoGq09C1b(Long anexoGq09C1b) {
        Long oldValue = this.anexoGq09C1b;
        this.anexoGq09C1b = anexoGq09C1b;
        this.firePropertyChange("anexoGq09C1b", oldValue, this.anexoGq09C1b);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoGq09B1() {
        return this.anexoGq09B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoGq09B1(String anexoGq09B1) {
        String oldValue = this.anexoGq09B1;
        this.anexoGq09B1 = anexoGq09B1;
        this.firePropertyChange("anexoGq09B1", oldValue, this.anexoGq09B1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoGq09C901a);
        result = HashCodeUtil.hash(result, this.anexoGq09C901b);
        result = HashCodeUtil.hash(result, this.anexoGq09C902a);
        result = HashCodeUtil.hash(result, this.anexoGq09C902b);
        result = HashCodeUtil.hash(result, this.anexoGq09C903a);
        result = HashCodeUtil.hash(result, this.anexoGq09C903b);
        result = HashCodeUtil.hash(result, this.anexoGq09C904a);
        result = HashCodeUtil.hash(result, this.anexoGq09C904b);
        result = HashCodeUtil.hash(result, this.anexoGq09C905a);
        result = HashCodeUtil.hash(result, this.anexoGq09C905b);
        result = HashCodeUtil.hash(result, this.anexoGq09C1b);
        result = HashCodeUtil.hash(result, this.anexoGq09B1);
        return result;
    }
}

