/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;

public class AnexoIq07T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoIq07T1_Linha>,
WritableTableFormat<AnexoIq07T1_Linha> {
    @Override
    public boolean isEditable(AnexoIq07T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoIq07T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getContitulares();
            }
            case 2: {
                return line.getParticipacao();
            }
            case 3: {
                return line.getRendimentoBruto();
            }
            case 4: {
                return line.getRendimentosComerciais();
            }
            case 5: {
                return line.getRendimentosAgricolas();
            }
            case 6: {
                return line.getRetencoes();
            }
            case 7: {
                return line.getValorImposto();
            }
        }
        return null;
    }

    @Override
    public AnexoIq07T1_Linha setColumnValue(AnexoIq07T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setContitulares((Long)value);
                return line;
            }
            case 2: {
                line.setParticipacao((Long)value);
                return line;
            }
            case 3: {
                line.setRendimentoBruto((Long)value);
                return line;
            }
            case 4: {
                line.setRendimentosComerciais((Long)value);
                return line;
            }
            case 5: {
                line.setRendimentosAgricolas((Long)value);
                return line;
            }
            case 6: {
                line.setRetencoes((Long)value);
                return line;
            }
            case 7: {
                line.setValorImposto((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Contitulares (n\u00famero fiscal de contribuinte)";
            }
            case 2: {
                return "% de Participa\u00e7\u00e3o";
            }
            case 3: {
                return "Rendimento Bruto da Heran\u00e7a";
            }
            case 4: {
                return "Rendimentos Comerciais e Industriais";
            }
            case 5: {
                return "Rendimentos Agr\u00edcolas, Silv\u00edcolas e Pecu\u00e1rios";
            }
            case 6: {
                return "Reten\u00e7\u00f5es na Fonte";
            }
            case 7: {
                return "Valor do Imposto a Imputar";
            }
        }
        return null;
    }
}

