/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro05Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro05
extends Quadro05Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq05C501()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq05C501a()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq05C502()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq05C502a()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq05C1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq05C1a()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq05C503());
    }
}

