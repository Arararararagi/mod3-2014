/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07Base;

public class Quadro07
extends Quadro07Base {
    public boolean isEmpty() {
        return this.getAnexoIq07T1().isEmpty();
    }

    public boolean existNifInQ7C70i1(long nif) {
        for (AnexoIq07T1_Linha anexoIq07T1Linha : this.getAnexoIq07T1()) {
            if (anexoIq07T1Linha.getContitulares() == null || anexoIq07T1Linha.getContitulares() != nif) continue;
            return true;
        }
        return false;
    }
}

