/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoI.qQuadro04";
    public static final String ANEXOIQ04C04_LINK = "aAnexoI.qQuadro04.fanexoIq04C04";
    public static final String ANEXOIQ04C04 = "anexoIq04C04";
    private Long anexoIq04C04;
    public static final String ANEXOIQ04C05_LINK = "aAnexoI.qQuadro04.fanexoIq04C05";
    public static final String ANEXOIQ04C05 = "anexoIq04C05";
    private Long anexoIq04C05;
    public static final String ANEXOIQ04C06_LINK = "aAnexoI.qQuadro04.fanexoIq04C06";
    public static final String ANEXOIQ04C06 = "anexoIq04C06";
    private Long anexoIq04C06;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoIq04C04() {
        return this.anexoIq04C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoIq04C04(Long anexoIq04C04) {
        Long oldValue = this.anexoIq04C04;
        this.anexoIq04C04 = anexoIq04C04;
        this.firePropertyChange("anexoIq04C04", oldValue, this.anexoIq04C04);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoIq04C05() {
        return this.anexoIq04C05;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoIq04C05(Long anexoIq04C05) {
        Long oldValue = this.anexoIq04C05;
        this.anexoIq04C05 = anexoIq04C05;
        this.firePropertyChange("anexoIq04C05", oldValue, this.anexoIq04C05);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoIq04C06() {
        return this.anexoIq04C06;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoIq04C06(Long anexoIq04C06) {
        Long oldValue = this.anexoIq04C06;
        this.anexoIq04C06 = anexoIq04C06;
        this.firePropertyChange("anexoIq04C06", oldValue, this.anexoIq04C06);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoIq04C04);
        result = HashCodeUtil.hash(result, this.anexoIq04C05);
        result = HashCodeUtil.hash(result, this.anexoIq04C06);
        return result;
    }
}

