/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoIq07T1_LinhaBase
extends Model {
    public static final String CONTITULARES_LINK = "aAnexoI.qQuadro07.fcontitulares";
    public static final String CONTITULARES = "contitulares";
    private Long contitulares;
    public static final String PARTICIPACAO_LINK = "aAnexoI.qQuadro07.fparticipacao";
    public static final String PARTICIPACAO = "participacao";
    private Long participacao;
    public static final String RENDIMENTOBRUTO_LINK = "aAnexoI.qQuadro07.frendimentoBruto";
    public static final String RENDIMENTOBRUTO = "rendimentoBruto";
    private Long rendimentoBruto;
    public static final String RENDIMENTOSCOMERCIAIS_LINK = "aAnexoI.qQuadro07.frendimentosComerciais";
    public static final String RENDIMENTOSCOMERCIAIS = "rendimentosComerciais";
    private Long rendimentosComerciais;
    public static final String RENDIMENTOSAGRICOLAS_LINK = "aAnexoI.qQuadro07.frendimentosAgricolas";
    public static final String RENDIMENTOSAGRICOLAS = "rendimentosAgricolas";
    private Long rendimentosAgricolas;
    public static final String RETENCOES_LINK = "aAnexoI.qQuadro07.fretencoes";
    public static final String RETENCOES = "retencoes";
    private Long retencoes;
    public static final String VALORIMPOSTO_LINK = "aAnexoI.qQuadro07.fvalorImposto";
    public static final String VALORIMPOSTO = "valorImposto";
    private Long valorImposto;

    public AnexoIq07T1_LinhaBase(Long contitulares, Long participacao, Long rendimentoBruto, Long rendimentosComerciais, Long rendimentosAgricolas, Long retencoes, Long valorImposto) {
        this.contitulares = contitulares;
        this.participacao = participacao;
        this.rendimentoBruto = rendimentoBruto;
        this.rendimentosComerciais = rendimentosComerciais;
        this.rendimentosAgricolas = rendimentosAgricolas;
        this.retencoes = retencoes;
        this.valorImposto = valorImposto;
    }

    public AnexoIq07T1_LinhaBase() {
        this.contitulares = null;
        this.participacao = null;
        this.rendimentoBruto = null;
        this.rendimentosComerciais = null;
        this.rendimentosAgricolas = null;
        this.retencoes = null;
        this.valorImposto = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getContitulares() {
        return this.contitulares;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setContitulares(Long contitulares) {
        Long oldValue = this.contitulares;
        this.contitulares = contitulares;
        this.firePropertyChange("contitulares", oldValue, this.contitulares);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getParticipacao() {
        return this.participacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setParticipacao(Long participacao) {
        Long oldValue = this.participacao;
        this.participacao = participacao;
        this.firePropertyChange("participacao", oldValue, this.participacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentoBruto() {
        return this.rendimentoBruto;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentoBruto(Long rendimentoBruto) {
        Long oldValue = this.rendimentoBruto;
        this.rendimentoBruto = rendimentoBruto;
        this.firePropertyChange("rendimentoBruto", oldValue, this.rendimentoBruto);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentosComerciais() {
        return this.rendimentosComerciais;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentosComerciais(Long rendimentosComerciais) {
        Long oldValue = this.rendimentosComerciais;
        this.rendimentosComerciais = rendimentosComerciais;
        this.firePropertyChange("rendimentosComerciais", oldValue, this.rendimentosComerciais);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentosAgricolas() {
        return this.rendimentosAgricolas;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentosAgricolas(Long rendimentosAgricolas) {
        Long oldValue = this.rendimentosAgricolas;
        this.rendimentosAgricolas = rendimentosAgricolas;
        this.firePropertyChange("rendimentosAgricolas", oldValue, this.rendimentosAgricolas);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencoes() {
        return this.retencoes;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencoes(Long retencoes) {
        Long oldValue = this.retencoes;
        this.retencoes = retencoes;
        this.firePropertyChange("retencoes", oldValue, this.retencoes);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorImposto() {
        return this.valorImposto;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorImposto(Long valorImposto) {
        Long oldValue = this.valorImposto;
        this.valorImposto = valorImposto;
        this.firePropertyChange("valorImposto", oldValue, this.valorImposto);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.contitulares);
        result = HashCodeUtil.hash(result, this.participacao);
        result = HashCodeUtil.hash(result, this.rendimentoBruto);
        result = HashCodeUtil.hash(result, this.rendimentosComerciais);
        result = HashCodeUtil.hash(result, this.rendimentosAgricolas);
        result = HashCodeUtil.hash(result, this.retencoes);
        result = HashCodeUtil.hash(result, this.valorImposto);
        return result;
    }

    public static enum Property {
        CONTITULARES("contitulares", 2),
        PARTICIPACAO("participacao", 3),
        RENDIMENTOBRUTO("rendimentoBruto", 4),
        RENDIMENTOSCOMERCIAIS("rendimentosComerciais", 5),
        RENDIMENTOSAGRICOLAS("rendimentosAgricolas", 6),
        RETENCOES("retencoes", 7),
        VALORIMPOSTO("valorImposto", 8);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

