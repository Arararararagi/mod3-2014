/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoI.qQuadro06";
    public static final String ANEXOIQ06C601_LINK = "aAnexoI.qQuadro06.fanexoIq06C601";
    public static final String ANEXOIQ06C601 = "anexoIq06C601";
    private Long anexoIq06C601;
    public static final String ANEXOIQ06C602_LINK = "aAnexoI.qQuadro06.fanexoIq06C602";
    public static final String ANEXOIQ06C602 = "anexoIq06C602";
    private Long anexoIq06C602;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq06C601() {
        return this.anexoIq06C601;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq06C601(Long anexoIq06C601) {
        Long oldValue = this.anexoIq06C601;
        this.anexoIq06C601 = anexoIq06C601;
        this.firePropertyChange("anexoIq06C601", oldValue, this.anexoIq06C601);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq06C602() {
        return this.anexoIq06C602;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq06C602(Long anexoIq06C602) {
        Long oldValue = this.anexoIq06C602;
        this.anexoIq06C602 = anexoIq06C602;
        this.firePropertyChange("anexoIq06C602", oldValue, this.anexoIq06C602);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoIq06C601);
        result = HashCodeUtil.hash(result, this.anexoIq06C602);
        return result;
    }
}

