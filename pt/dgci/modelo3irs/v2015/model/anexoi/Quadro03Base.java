/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoI.qQuadro03";
    public static final String ANEXOIQ03C02_LINK = "aAnexoI.qQuadro03.fanexoIq03C02";
    public static final String ANEXOIQ03C02 = "anexoIq03C02";
    private Long anexoIq03C02;
    public static final String ANEXOIQ03C03_LINK = "aAnexoI.qQuadro03.fanexoIq03C03";
    public static final String ANEXOIQ03C03 = "anexoIq03C03";
    private Long anexoIq03C03;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoIq03C02() {
        return this.anexoIq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoIq03C02(Long anexoIq03C02) {
        Long oldValue = this.anexoIq03C02;
        this.anexoIq03C02 = anexoIq03C02;
        this.firePropertyChange("anexoIq03C02", oldValue, this.anexoIq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoIq03C03() {
        return this.anexoIq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoIq03C03(Long anexoIq03C03) {
        Long oldValue = this.anexoIq03C03;
        this.anexoIq03C03 = anexoIq03C03;
        this.firePropertyChange("anexoIq03C03", oldValue, this.anexoIq03C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoIq03C02);
        result = HashCodeUtil.hash(result, this.anexoIq03C03);
        return result;
    }
}

