/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro06Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro06
extends Quadro06Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq06C601()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoIq06C602());
    }
}

