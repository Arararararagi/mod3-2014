/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoI.qQuadro05";
    public static final String ANEXOIQ05C501_LINK = "aAnexoI.qQuadro05.fanexoIq05C501";
    public static final String ANEXOIQ05C501 = "anexoIq05C501";
    private Long anexoIq05C501;
    public static final String ANEXOIQ05C501A_LINK = "aAnexoI.qQuadro05.fanexoIq05C501a";
    public static final String ANEXOIQ05C501A = "anexoIq05C501a";
    private Long anexoIq05C501a;
    public static final String ANEXOIQ05C502_LINK = "aAnexoI.qQuadro05.fanexoIq05C502";
    public static final String ANEXOIQ05C502 = "anexoIq05C502";
    private Long anexoIq05C502;
    public static final String ANEXOIQ05C502A_LINK = "aAnexoI.qQuadro05.fanexoIq05C502a";
    public static final String ANEXOIQ05C502A = "anexoIq05C502a";
    private Long anexoIq05C502a;
    public static final String ANEXOIQ05C1_LINK = "aAnexoI.qQuadro05.fanexoIq05C1";
    public static final String ANEXOIQ05C1 = "anexoIq05C1";
    private Long anexoIq05C1;
    public static final String ANEXOIQ05C1A_LINK = "aAnexoI.qQuadro05.fanexoIq05C1a";
    public static final String ANEXOIQ05C1A = "anexoIq05C1a";
    private Long anexoIq05C1a;
    public static final String ANEXOIQ05C504_LINK = "aAnexoI.qQuadro05.fanexoIq05C504";
    public static final String ANEXOIQ05C504 = "anexoIq05C504";
    private Long anexoIq05C504;
    public static final String ANEXOIQ05C504A_LINK = "aAnexoI.qQuadro05.fanexoIq05C504a";
    public static final String ANEXOIQ05C504A = "anexoIq05C504a";
    private Long anexoIq05C504a;
    public static final String ANEXOIQ05C505_LINK = "aAnexoI.qQuadro05.fanexoIq05C505";
    public static final String ANEXOIQ05C505 = "anexoIq05C505";
    private Long anexoIq05C505;
    public static final String ANEXOIQ05C505A_LINK = "aAnexoI.qQuadro05.fanexoIq05C505a";
    public static final String ANEXOIQ05C505A = "anexoIq05C505a";
    private Long anexoIq05C505a;
    public static final String ANEXOIQ05C506_LINK = "aAnexoI.qQuadro05.fanexoIq05C506";
    public static final String ANEXOIQ05C506 = "anexoIq05C506";
    private Long anexoIq05C506;
    public static final String ANEXOIQ05C506A_LINK = "aAnexoI.qQuadro05.fanexoIq05C506a";
    public static final String ANEXOIQ05C506A = "anexoIq05C506a";
    private Long anexoIq05C506a;
    public static final String ANEXOIQ05C507_LINK = "aAnexoI.qQuadro05.fanexoIq05C507";
    public static final String ANEXOIQ05C507 = "anexoIq05C507";
    private Long anexoIq05C507;
    public static final String ANEXOIQ05C507A_LINK = "aAnexoI.qQuadro05.fanexoIq05C507a";
    public static final String ANEXOIQ05C507A = "anexoIq05C507a";
    private Long anexoIq05C507a;
    public static final String ANEXOIQ05C508_LINK = "aAnexoI.qQuadro05.fanexoIq05C508";
    public static final String ANEXOIQ05C508 = "anexoIq05C508";
    private Long anexoIq05C508;
    public static final String ANEXOIQ05C508A_LINK = "aAnexoI.qQuadro05.fanexoIq05C508a";
    public static final String ANEXOIQ05C508A = "anexoIq05C508a";
    private Long anexoIq05C508a;
    public static final String ANEXOIQ05C2_LINK = "aAnexoI.qQuadro05.fanexoIq05C2";
    public static final String ANEXOIQ05C2 = "anexoIq05C2";
    private Long anexoIq05C2;
    public static final String ANEXOIQ05C2A_LINK = "aAnexoI.qQuadro05.fanexoIq05C2a";
    public static final String ANEXOIQ05C2A = "anexoIq05C2a";
    private Long anexoIq05C2a;
    public static final String ANEXOIQ05C503_LINK = "aAnexoI.qQuadro05.fanexoIq05C503";
    public static final String ANEXOIQ05C503 = "anexoIq05C503";
    private Long anexoIq05C503;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C501() {
        return this.anexoIq05C501;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C501(Long anexoIq05C501) {
        Long oldValue = this.anexoIq05C501;
        this.anexoIq05C501 = anexoIq05C501;
        this.firePropertyChange("anexoIq05C501", oldValue, this.anexoIq05C501);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C501a() {
        return this.anexoIq05C501a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C501a(Long anexoIq05C501a) {
        Long oldValue = this.anexoIq05C501a;
        this.anexoIq05C501a = anexoIq05C501a;
        this.firePropertyChange("anexoIq05C501a", oldValue, this.anexoIq05C501a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C502() {
        return this.anexoIq05C502;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C502(Long anexoIq05C502) {
        Long oldValue = this.anexoIq05C502;
        this.anexoIq05C502 = anexoIq05C502;
        this.firePropertyChange("anexoIq05C502", oldValue, this.anexoIq05C502);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C502a() {
        return this.anexoIq05C502a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C502a(Long anexoIq05C502a) {
        Long oldValue = this.anexoIq05C502a;
        this.anexoIq05C502a = anexoIq05C502a;
        this.firePropertyChange("anexoIq05C502a", oldValue, this.anexoIq05C502a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C1() {
        return this.anexoIq05C1;
    }

    public void setAnexoIq05C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq05C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoIq05C1(newValue+=(this.getAnexoIq05C501() == null ? 0 : this.getAnexoIq05C501()) + (this.getAnexoIq05C502() == null ? 0 : this.getAnexoIq05C502()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C1(Long anexoIq05C1) {
        Long oldValue = this.anexoIq05C1;
        this.anexoIq05C1 = anexoIq05C1;
        this.firePropertyChange("anexoIq05C1", oldValue, this.anexoIq05C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C1a() {
        return this.anexoIq05C1a;
    }

    public void setAnexoIq05C1aFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq05C1a")) {
            return;
        }
        long newValue = 0;
        this.setAnexoIq05C1a(newValue+=(this.getAnexoIq05C501a() == null ? 0 : this.getAnexoIq05C501a()) + (this.getAnexoIq05C502a() == null ? 0 : this.getAnexoIq05C502a()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C1a(Long anexoIq05C1a) {
        Long oldValue = this.anexoIq05C1a;
        this.anexoIq05C1a = anexoIq05C1a;
        this.firePropertyChange("anexoIq05C1a", oldValue, this.anexoIq05C1a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C504() {
        return this.anexoIq05C504;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C504(Long anexoIq05C504) {
        Long oldValue = this.anexoIq05C504;
        this.anexoIq05C504 = anexoIq05C504;
        this.firePropertyChange("anexoIq05C504", oldValue, this.anexoIq05C504);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C504a() {
        return this.anexoIq05C504a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C504a(Long anexoIq05C504a) {
        Long oldValue = this.anexoIq05C504a;
        this.anexoIq05C504a = anexoIq05C504a;
        this.firePropertyChange("anexoIq05C504a", oldValue, this.anexoIq05C504a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C505() {
        return this.anexoIq05C505;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C505(Long anexoIq05C505) {
        Long oldValue = this.anexoIq05C505;
        this.anexoIq05C505 = anexoIq05C505;
        this.firePropertyChange("anexoIq05C505", oldValue, this.anexoIq05C505);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C505a() {
        return this.anexoIq05C505a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C505a(Long anexoIq05C505a) {
        Long oldValue = this.anexoIq05C505a;
        this.anexoIq05C505a = anexoIq05C505a;
        this.firePropertyChange("anexoIq05C505a", oldValue, this.anexoIq05C505a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C506() {
        return this.anexoIq05C506;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C506(Long anexoIq05C506) {
        Long oldValue = this.anexoIq05C506;
        this.anexoIq05C506 = anexoIq05C506;
        this.firePropertyChange("anexoIq05C506", oldValue, this.anexoIq05C506);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C506a() {
        return this.anexoIq05C506a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C506a(Long anexoIq05C506a) {
        Long oldValue = this.anexoIq05C506a;
        this.anexoIq05C506a = anexoIq05C506a;
        this.firePropertyChange("anexoIq05C506a", oldValue, this.anexoIq05C506a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C507() {
        return this.anexoIq05C507;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C507(Long anexoIq05C507) {
        Long oldValue = this.anexoIq05C507;
        this.anexoIq05C507 = anexoIq05C507;
        this.firePropertyChange("anexoIq05C507", oldValue, this.anexoIq05C507);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C507a() {
        return this.anexoIq05C507a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C507a(Long anexoIq05C507a) {
        Long oldValue = this.anexoIq05C507a;
        this.anexoIq05C507a = anexoIq05C507a;
        this.firePropertyChange("anexoIq05C507a", oldValue, this.anexoIq05C507a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C508() {
        return this.anexoIq05C508;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C508(Long anexoIq05C508) {
        Long oldValue = this.anexoIq05C508;
        this.anexoIq05C508 = anexoIq05C508;
        this.firePropertyChange("anexoIq05C508", oldValue, this.anexoIq05C508);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C508a() {
        return this.anexoIq05C508a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C508a(Long anexoIq05C508a) {
        Long oldValue = this.anexoIq05C508a;
        this.anexoIq05C508a = anexoIq05C508a;
        this.firePropertyChange("anexoIq05C508a", oldValue, this.anexoIq05C508a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C2() {
        return this.anexoIq05C2;
    }

    public void setAnexoIq05C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq05C2")) {
            return;
        }
        long newValue = 0;
        this.setAnexoIq05C2(newValue+=(this.getAnexoIq05C504() == null ? 0 : this.getAnexoIq05C504()) + (this.getAnexoIq05C505() == null ? 0 : this.getAnexoIq05C505()) + (this.getAnexoIq05C506() == null ? 0 : this.getAnexoIq05C506()) + (this.getAnexoIq05C507() == null ? 0 : this.getAnexoIq05C507()) + (this.getAnexoIq05C508() == null ? 0 : this.getAnexoIq05C508()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C2(Long anexoIq05C2) {
        Long oldValue = this.anexoIq05C2;
        this.anexoIq05C2 = anexoIq05C2;
        this.firePropertyChange("anexoIq05C2", oldValue, this.anexoIq05C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C2a() {
        return this.anexoIq05C2a;
    }

    public void setAnexoIq05C2aFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq05C2a")) {
            return;
        }
        long newValue = 0;
        this.setAnexoIq05C2a(newValue+=(this.getAnexoIq05C504a() == null ? 0 : this.getAnexoIq05C504a()) + (this.getAnexoIq05C505a() == null ? 0 : this.getAnexoIq05C505a()) + (this.getAnexoIq05C506a() == null ? 0 : this.getAnexoIq05C506a()) + (this.getAnexoIq05C507a() == null ? 0 : this.getAnexoIq05C507a()) + (this.getAnexoIq05C508a() == null ? 0 : this.getAnexoIq05C508a()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C2a(Long anexoIq05C2a) {
        Long oldValue = this.anexoIq05C2a;
        this.anexoIq05C2a = anexoIq05C2a;
        this.firePropertyChange("anexoIq05C2a", oldValue, this.anexoIq05C2a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq05C503() {
        return this.anexoIq05C503;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq05C503(Long anexoIq05C503) {
        Long oldValue = this.anexoIq05C503;
        this.anexoIq05C503 = anexoIq05C503;
        this.firePropertyChange("anexoIq05C503", oldValue, this.anexoIq05C503);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoIq05C501);
        result = HashCodeUtil.hash(result, this.anexoIq05C501a);
        result = HashCodeUtil.hash(result, this.anexoIq05C502);
        result = HashCodeUtil.hash(result, this.anexoIq05C502a);
        result = HashCodeUtil.hash(result, this.anexoIq05C1);
        result = HashCodeUtil.hash(result, this.anexoIq05C1a);
        result = HashCodeUtil.hash(result, this.anexoIq05C504);
        result = HashCodeUtil.hash(result, this.anexoIq05C504a);
        result = HashCodeUtil.hash(result, this.anexoIq05C505);
        result = HashCodeUtil.hash(result, this.anexoIq05C505a);
        result = HashCodeUtil.hash(result, this.anexoIq05C506);
        result = HashCodeUtil.hash(result, this.anexoIq05C506a);
        result = HashCodeUtil.hash(result, this.anexoIq05C507);
        result = HashCodeUtil.hash(result, this.anexoIq05C507a);
        result = HashCodeUtil.hash(result, this.anexoIq05C508);
        result = HashCodeUtil.hash(result, this.anexoIq05C508a);
        result = HashCodeUtil.hash(result, this.anexoIq05C2);
        result = HashCodeUtil.hash(result, this.anexoIq05C2a);
        result = HashCodeUtil.hash(result, this.anexoIq05C503);
        return result;
    }
}

