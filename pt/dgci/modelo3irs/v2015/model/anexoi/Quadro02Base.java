/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoI.qQuadro02";
    public static final String ANEXOIQ02C01_LINK = "aAnexoI.qQuadro02.fanexoIq02C01";
    public static final String ANEXOIQ02C01 = "anexoIq02C01";
    private Long anexoIq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoIq02C01() {
        return this.anexoIq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoIq02C01(Long anexoIq02C01) {
        Long oldValue = this.anexoIq02C01;
        this.anexoIq02C01 = anexoIq02C01;
        this.firePropertyChange("anexoIq02C01", oldValue, this.anexoIq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoIq02C01);
        return result;
    }
}

