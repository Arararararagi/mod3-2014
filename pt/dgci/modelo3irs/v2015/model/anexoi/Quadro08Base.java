/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro08Base
extends QuadroModel {
    public static final String QUADRO08_LINK = "aAnexoI.qQuadro08";
    public static final String ANEXOIQ08C801_LINK = "aAnexoI.qQuadro08.fanexoIq08C801";
    public static final String ANEXOIQ08C801 = "anexoIq08C801";
    private Long anexoIq08C801;
    public static final String ANEXOIQ08C801A_LINK = "aAnexoI.qQuadro08.fanexoIq08C801a";
    public static final String ANEXOIQ08C801A = "anexoIq08C801a";
    private Long anexoIq08C801a;
    public static final String ANEXOIQ08C802_LINK = "aAnexoI.qQuadro08.fanexoIq08C802";
    public static final String ANEXOIQ08C802 = "anexoIq08C802";
    private Long anexoIq08C802;
    public static final String ANEXOIQ08C802A_LINK = "aAnexoI.qQuadro08.fanexoIq08C802a";
    public static final String ANEXOIQ08C802A = "anexoIq08C802a";
    private Long anexoIq08C802a;
    public static final String ANEXOIQ08C806_LINK = "aAnexoI.qQuadro08.fanexoIq08C806";
    public static final String ANEXOIQ08C806 = "anexoIq08C806";
    private Long anexoIq08C806;
    public static final String ANEXOIQ08C806A_LINK = "aAnexoI.qQuadro08.fanexoIq08C806a";
    public static final String ANEXOIQ08C806A = "anexoIq08C806a";
    private Long anexoIq08C806a;
    public static final String ANEXOIQ08C803_LINK = "aAnexoI.qQuadro08.fanexoIq08C803";
    public static final String ANEXOIQ08C803 = "anexoIq08C803";
    private Long anexoIq08C803;
    public static final String ANEXOIQ08C803A_LINK = "aAnexoI.qQuadro08.fanexoIq08C803a";
    public static final String ANEXOIQ08C803A = "anexoIq08C803a";
    private Long anexoIq08C803a;
    public static final String ANEXOIQ08C807_LINK = "aAnexoI.qQuadro08.fanexoIq08C807";
    public static final String ANEXOIQ08C807 = "anexoIq08C807";
    private Long anexoIq08C807;
    public static final String ANEXOIQ08C807A_LINK = "aAnexoI.qQuadro08.fanexoIq08C807a";
    public static final String ANEXOIQ08C807A = "anexoIq08C807a";
    private Long anexoIq08C807a;
    public static final String ANEXOIQ08C804_LINK = "aAnexoI.qQuadro08.fanexoIq08C804";
    public static final String ANEXOIQ08C804 = "anexoIq08C804";
    private Long anexoIq08C804;
    public static final String ANEXOIQ08C804A_LINK = "aAnexoI.qQuadro08.fanexoIq08C804a";
    public static final String ANEXOIQ08C804A = "anexoIq08C804a";
    private Long anexoIq08C804a;
    public static final String ANEXOIQ08C805_LINK = "aAnexoI.qQuadro08.fanexoIq08C805";
    public static final String ANEXOIQ08C805 = "anexoIq08C805";
    private Long anexoIq08C805;
    public static final String ANEXOIQ08C805A_LINK = "aAnexoI.qQuadro08.fanexoIq08C805a";
    public static final String ANEXOIQ08C805A = "anexoIq08C805a";
    private Long anexoIq08C805a;
    public static final String ANEXOIQ08C1_LINK = "aAnexoI.qQuadro08.fanexoIq08C1";
    public static final String ANEXOIQ08C1 = "anexoIq08C1";
    private Long anexoIq08C1;
    public static final String ANEXOIQ08C1A_LINK = "aAnexoI.qQuadro08.fanexoIq08C1a";
    public static final String ANEXOIQ08C1A = "anexoIq08C1a";
    private Long anexoIq08C1a;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C801() {
        return this.anexoIq08C801;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C801(Long anexoIq08C801) {
        Long oldValue = this.anexoIq08C801;
        this.anexoIq08C801 = anexoIq08C801;
        this.firePropertyChange("anexoIq08C801", oldValue, this.anexoIq08C801);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C801a() {
        return this.anexoIq08C801a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C801a(Long anexoIq08C801a) {
        Long oldValue = this.anexoIq08C801a;
        this.anexoIq08C801a = anexoIq08C801a;
        this.firePropertyChange("anexoIq08C801a", oldValue, this.anexoIq08C801a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C802() {
        return this.anexoIq08C802;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C802(Long anexoIq08C802) {
        Long oldValue = this.anexoIq08C802;
        this.anexoIq08C802 = anexoIq08C802;
        this.firePropertyChange("anexoIq08C802", oldValue, this.anexoIq08C802);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C802a() {
        return this.anexoIq08C802a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C802a(Long anexoIq08C802a) {
        Long oldValue = this.anexoIq08C802a;
        this.anexoIq08C802a = anexoIq08C802a;
        this.firePropertyChange("anexoIq08C802a", oldValue, this.anexoIq08C802a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C806() {
        return this.anexoIq08C806;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C806(Long anexoIq08C806) {
        Long oldValue = this.anexoIq08C806;
        this.anexoIq08C806 = anexoIq08C806;
        this.firePropertyChange("anexoIq08C806", oldValue, this.anexoIq08C806);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C806a() {
        return this.anexoIq08C806a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C806a(Long anexoIq08C806a) {
        Long oldValue = this.anexoIq08C806a;
        this.anexoIq08C806a = anexoIq08C806a;
        this.firePropertyChange("anexoIq08C806a", oldValue, this.anexoIq08C806a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C803() {
        return this.anexoIq08C803;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C803(Long anexoIq08C803) {
        Long oldValue = this.anexoIq08C803;
        this.anexoIq08C803 = anexoIq08C803;
        this.firePropertyChange("anexoIq08C803", oldValue, this.anexoIq08C803);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C803a() {
        return this.anexoIq08C803a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C803a(Long anexoIq08C803a) {
        Long oldValue = this.anexoIq08C803a;
        this.anexoIq08C803a = anexoIq08C803a;
        this.firePropertyChange("anexoIq08C803a", oldValue, this.anexoIq08C803a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C807() {
        return this.anexoIq08C807;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C807(Long anexoIq08C807) {
        Long oldValue = this.anexoIq08C807;
        this.anexoIq08C807 = anexoIq08C807;
        this.firePropertyChange("anexoIq08C807", oldValue, this.anexoIq08C807);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C807a() {
        return this.anexoIq08C807a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C807a(Long anexoIq08C807a) {
        Long oldValue = this.anexoIq08C807a;
        this.anexoIq08C807a = anexoIq08C807a;
        this.firePropertyChange("anexoIq08C807a", oldValue, this.anexoIq08C807a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C804() {
        return this.anexoIq08C804;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C804(Long anexoIq08C804) {
        Long oldValue = this.anexoIq08C804;
        this.anexoIq08C804 = anexoIq08C804;
        this.firePropertyChange("anexoIq08C804", oldValue, this.anexoIq08C804);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C804a() {
        return this.anexoIq08C804a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C804a(Long anexoIq08C804a) {
        Long oldValue = this.anexoIq08C804a;
        this.anexoIq08C804a = anexoIq08C804a;
        this.firePropertyChange("anexoIq08C804a", oldValue, this.anexoIq08C804a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C805() {
        return this.anexoIq08C805;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C805(Long anexoIq08C805) {
        Long oldValue = this.anexoIq08C805;
        this.anexoIq08C805 = anexoIq08C805;
        this.firePropertyChange("anexoIq08C805", oldValue, this.anexoIq08C805);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C805a() {
        return this.anexoIq08C805a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C805a(Long anexoIq08C805a) {
        Long oldValue = this.anexoIq08C805a;
        this.anexoIq08C805a = anexoIq08C805a;
        this.firePropertyChange("anexoIq08C805a", oldValue, this.anexoIq08C805a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C1() {
        return this.anexoIq08C1;
    }

    public void setAnexoIq08C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq08C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoIq08C1(newValue+=(this.getAnexoIq08C801() == null ? 0 : this.getAnexoIq08C801()) + (this.getAnexoIq08C802() == null ? 0 : this.getAnexoIq08C802()) + (this.getAnexoIq08C803() == null ? 0 : this.getAnexoIq08C803()) + (this.getAnexoIq08C804() == null ? 0 : this.getAnexoIq08C804()) + (this.getAnexoIq08C805() == null ? 0 : this.getAnexoIq08C805()) + (this.getAnexoIq08C806() == null ? 0 : this.getAnexoIq08C806()) + (this.getAnexoIq08C807() == null ? 0 : this.getAnexoIq08C807()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C1(Long anexoIq08C1) {
        Long oldValue = this.anexoIq08C1;
        this.anexoIq08C1 = anexoIq08C1;
        this.firePropertyChange("anexoIq08C1", oldValue, this.anexoIq08C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq08C1a() {
        return this.anexoIq08C1a;
    }

    public void setAnexoIq08C1aFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq08C1a")) {
            return;
        }
        long newValue = 0;
        this.setAnexoIq08C1a(newValue+=(this.getAnexoIq08C801a() == null ? 0 : this.getAnexoIq08C801a()) + (this.getAnexoIq08C802a() == null ? 0 : this.getAnexoIq08C802a()) + (this.getAnexoIq08C803a() == null ? 0 : this.getAnexoIq08C803a()) + (this.getAnexoIq08C804a() == null ? 0 : this.getAnexoIq08C804a()) + (this.getAnexoIq08C805a() == null ? 0 : this.getAnexoIq08C805a()) + (this.getAnexoIq08C806a() == null ? 0 : this.getAnexoIq08C806a()) + (this.getAnexoIq08C807a() == null ? 0 : this.getAnexoIq08C807a()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq08C1a(Long anexoIq08C1a) {
        Long oldValue = this.anexoIq08C1a;
        this.anexoIq08C1a = anexoIq08C1a;
        this.firePropertyChange("anexoIq08C1a", oldValue, this.anexoIq08C1a);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoIq08C801);
        result = HashCodeUtil.hash(result, this.anexoIq08C801a);
        result = HashCodeUtil.hash(result, this.anexoIq08C802);
        result = HashCodeUtil.hash(result, this.anexoIq08C802a);
        result = HashCodeUtil.hash(result, this.anexoIq08C806);
        result = HashCodeUtil.hash(result, this.anexoIq08C806a);
        result = HashCodeUtil.hash(result, this.anexoIq08C803);
        result = HashCodeUtil.hash(result, this.anexoIq08C803a);
        result = HashCodeUtil.hash(result, this.anexoIq08C807);
        result = HashCodeUtil.hash(result, this.anexoIq08C807a);
        result = HashCodeUtil.hash(result, this.anexoIq08C804);
        result = HashCodeUtil.hash(result, this.anexoIq08C804a);
        result = HashCodeUtil.hash(result, this.anexoIq08C805);
        result = HashCodeUtil.hash(result, this.anexoIq08C805a);
        result = HashCodeUtil.hash(result, this.anexoIq08C1);
        result = HashCodeUtil.hash(result, this.anexoIq08C1a);
        return result;
    }
}

