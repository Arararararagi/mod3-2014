/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoI.qQuadro07";
    public static final String ANEXOIQ07T1_LINK = "aAnexoI.qQuadro07.tanexoIq07T1";
    private EventList<AnexoIq07T1_Linha> anexoIq07T1 = new BasicEventList<AnexoIq07T1_Linha>();
    public static final String ANEXOIQ07C1_LINK = "aAnexoI.qQuadro07.fanexoIq07C1";
    public static final String ANEXOIQ07C1 = "anexoIq07C1";
    private Long anexoIq07C1;
    public static final String ANEXOIQ07C2_LINK = "aAnexoI.qQuadro07.fanexoIq07C2";
    public static final String ANEXOIQ07C2 = "anexoIq07C2";
    private Long anexoIq07C2;
    public static final String ANEXOIQ07C3_LINK = "aAnexoI.qQuadro07.fanexoIq07C3";
    public static final String ANEXOIQ07C3 = "anexoIq07C3";
    private Long anexoIq07C3;
    public static final String ANEXOIQ07C4_LINK = "aAnexoI.qQuadro07.fanexoIq07C4";
    public static final String ANEXOIQ07C4 = "anexoIq07C4";
    private Long anexoIq07C4;
    public static final String ANEXOIQ07C5_LINK = "aAnexoI.qQuadro07.fanexoIq07C5";
    public static final String ANEXOIQ07C5 = "anexoIq07C5";
    private Long anexoIq07C5;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoIq07T1_Linha> getAnexoIq07T1() {
        return this.anexoIq07T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoIq07T1(EventList<AnexoIq07T1_Linha> anexoIq07T1) {
        this.anexoIq07T1 = anexoIq07T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq07C1() {
        return this.anexoIq07C1;
    }

    public void setAnexoIq07C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq07C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoIq07T1().size(); ++i) {
            temporaryValue0+=this.getAnexoIq07T1().get(i).getRendimentoBruto() == null ? 0 : this.getAnexoIq07T1().get(i).getRendimentoBruto();
        }
        this.setAnexoIq07C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq07C1(Long anexoIq07C1) {
        Long oldValue = this.anexoIq07C1;
        this.anexoIq07C1 = anexoIq07C1;
        this.firePropertyChange("anexoIq07C1", oldValue, this.anexoIq07C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq07C2() {
        return this.anexoIq07C2;
    }

    public void setAnexoIq07C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq07C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoIq07T1().size(); ++i) {
            temporaryValue0+=this.getAnexoIq07T1().get(i).getRendimentosComerciais() == null ? 0 : this.getAnexoIq07T1().get(i).getRendimentosComerciais();
        }
        this.setAnexoIq07C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq07C2(Long anexoIq07C2) {
        Long oldValue = this.anexoIq07C2;
        this.anexoIq07C2 = anexoIq07C2;
        this.firePropertyChange("anexoIq07C2", oldValue, this.anexoIq07C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq07C3() {
        return this.anexoIq07C3;
    }

    public void setAnexoIq07C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq07C3")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoIq07T1().size(); ++i) {
            temporaryValue0+=this.getAnexoIq07T1().get(i).getRendimentosAgricolas() == null ? 0 : this.getAnexoIq07T1().get(i).getRendimentosAgricolas();
        }
        this.setAnexoIq07C3(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq07C3(Long anexoIq07C3) {
        Long oldValue = this.anexoIq07C3;
        this.anexoIq07C3 = anexoIq07C3;
        this.firePropertyChange("anexoIq07C3", oldValue, this.anexoIq07C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq07C4() {
        return this.anexoIq07C4;
    }

    public void setAnexoIq07C4Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq07C4")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoIq07T1().size(); ++i) {
            temporaryValue0+=this.getAnexoIq07T1().get(i).getRetencoes() == null ? 0 : this.getAnexoIq07T1().get(i).getRetencoes();
        }
        this.setAnexoIq07C4(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq07C4(Long anexoIq07C4) {
        Long oldValue = this.anexoIq07C4;
        this.anexoIq07C4 = anexoIq07C4;
        this.firePropertyChange("anexoIq07C4", oldValue, this.anexoIq07C4);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoIq07C5() {
        return this.anexoIq07C5;
    }

    public void setAnexoIq07C5Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoIq07C5")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoIq07T1().size(); ++i) {
            temporaryValue0+=this.getAnexoIq07T1().get(i).getValorImposto() == null ? 0 : this.getAnexoIq07T1().get(i).getValorImposto();
        }
        this.setAnexoIq07C5(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoIq07C5(Long anexoIq07C5) {
        Long oldValue = this.anexoIq07C5;
        this.anexoIq07C5 = anexoIq07C5;
        this.firePropertyChange("anexoIq07C5", oldValue, this.anexoIq07C5);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoIq07T1);
        result = HashCodeUtil.hash(result, this.anexoIq07C1);
        result = HashCodeUtil.hash(result, this.anexoIq07C2);
        result = HashCodeUtil.hash(result, this.anexoIq07C3);
        result = HashCodeUtil.hash(result, this.anexoIq07C4);
        result = HashCodeUtil.hash(result, this.anexoIq07C5);
        return result;
    }
}

