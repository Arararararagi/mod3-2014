/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04Base;

public class Quadro04
extends Quadro04Base {
    private AnexoIModel anexoIModel;

    public void setAnexoPai(AnexoIModel anexoIModel) {
        this.anexoIModel = anexoIModel;
    }

    public AnexoIModel getAnexoPai() {
        return this.anexoIModel;
    }
}

