/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIq07T1_LinhaBase;

public class AnexoIq07T1_Linha
extends AnexoIq07T1_LinhaBase {
    public AnexoIq07T1_Linha() {
    }

    public AnexoIq07T1_Linha(Long contitulares, Long participacao, Long rendimentoBruto, Long rendimentosComerciais, Long rendimentosAgricolas, Long retencoes, Long valorImposto) {
        super(contitulares, participacao, rendimentoBruto, rendimentosComerciais, rendimentosAgricolas, retencoes, valorImposto);
    }

    public static String getLink(int linha) {
        return "aAnexoI.qQuadro07.tanexoIq07T1.l" + linha;
    }

    public static String getLink(int linha, AnexoIq07T1_LinhaBase.Property column) {
        return "aAnexoI.qQuadro07.tanexoIq07T1.l" + linha + ".c" + column.getIndex();
    }
}

