/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoi;

import pt.dgci.modelo3irs.v2015.model.OwnableAnexo;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModelBase;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexoi.Quadro08;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=9)
public class AnexoIModel
extends AnexoIModelBase
implements OwnableAnexo {
    public AnexoIModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
        this.postCreate();
    }

    protected void postCreate() {
        Quadro04 quadro04Model = this.getQuadro04();
        if (quadro04Model != null) {
            if (NifValidator.isSingular(this.formKey.getSubId())) {
                quadro04Model.setAnexoIq04C04(Long.parseLong(this.formKey.getSubId()));
            } else {
                quadro04Model.setAnexoIq04C05(Long.parseLong(this.formKey.getSubId()));
            }
        }
    }

    @Override
    public boolean isOwnedBy(Long nif) {
        return nif != null && (nif.equals(this.getQuadro04().getAnexoIq04C04()) || nif.equals(this.getQuadro04().getAnexoIq04C05()));
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public Quadro07 getQuadro07() {
        return (Quadro07)this.getQuadro(Quadro07.class.getSimpleName());
    }

    public Quadro08 getQuadro08() {
        return (Quadro08)this.getQuadro(Quadro08.class.getSimpleName());
    }

    public boolean isEmpty() {
        return this.getQuadro05().isEmpty() && this.getQuadro06().isEmpty() && this.getQuadro07().isEmpty();
    }

    public String getLink(String anexoiLink) {
        return anexoiLink.replaceFirst("aAnexoI", "aAnexoI|" + this.getFormKey().getSubId());
    }

    public Long getNifTitular() {
        return this.getQuadro04().getAnexoIq04C05();
    }

    public String getStringNifTitular() {
        Long nifTitular = this.getNifTitular();
        if (nifTitular == null) {
            return null;
        }
        return String.valueOf(nifTitular);
    }

    public String getNifTitularLink() {
        Long nifAutorHeranca = this.getQuadro04().getAnexoIq04C04();
        String nifAutorHerancaLink = this.getLink("aAnexoI.qQuadro04.fanexoIq04C04");
        Long nipcHerancaIndivisa = this.getQuadro04().getAnexoIq04C05();
        String nipcHerancaIndivisaLink = this.getLink("aAnexoI.qQuadro04.fanexoIq04C05");
        if (Modelo3IRSValidatorUtil.isEmptyOrZero(nifAutorHeranca) && Modelo3IRSValidatorUtil.isEmptyOrZero(nipcHerancaIndivisa)) {
            return null;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nipcHerancaIndivisa)) {
            return nipcHerancaIndivisaLink;
        }
        return nifAutorHerancaLink;
    }

    @Override
    public boolean addQuadro(QuadroModel quadro) {
        if (quadro instanceof Quadro04) {
            Quadro04 quadro4 = (Quadro04)quadro;
            quadro4.setAnexoPai(this);
        }
        return super.addQuadro(quadro);
    }
}

