/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoE.qQuadro02";
    public static final String ANEXOEQ02C01_LINK = "aAnexoE.qQuadro02.fanexoEq02C01";
    public static final String ANEXOEQ02C01 = "anexoEq02C01";
    private Long anexoEq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoEq02C01() {
        return this.anexoEq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoEq02C01(Long anexoEq02C01) {
        Long oldValue = this.anexoEq02C01;
        this.anexoEq02C01 = anexoEq02C01;
        this.firePropertyChange("anexoEq02C01", oldValue, this.anexoEq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoEq02C01);
        return result;
    }
}

