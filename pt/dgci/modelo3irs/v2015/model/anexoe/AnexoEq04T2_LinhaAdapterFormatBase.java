/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoEq04T2_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoEq04T2_Linha>,
WritableTableFormat<AnexoEq04T2_Linha> {
    @Override
    public boolean isEditable(AnexoEq04T2_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoEq04T2_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNIF();
            }
            case 2: {
                if (line == null || line.getCodRendimentos() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoEQ4B", line.getCodRendimentos());
            }
            case 3: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepEFalecidos", line.getTitular());
            }
            case 4: {
                return line.getRendimentos();
            }
            case 5: {
                return line.getRetencoes();
            }
        }
        return null;
    }

    @Override
    public AnexoEq04T2_Linha setColumnValue(AnexoEq04T2_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNIF((Long)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCodRendimentos((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 4: {
                line.setRendimentos((Long)value);
                return line;
            }
            case 5: {
                line.setRetencoes((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF da Entidade Devedora, Registadora ou Deposit\u00e1ria";
            }
            case 2: {
                return "C\u00f3digo dos Rendimentos";
            }
            case 3: {
                return "Titular";
            }
            case 4: {
                return "Rendimentos";
            }
            case 5: {
                return "Reten\u00e7\u00f5es";
            }
        }
        return null;
    }
}

