/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04Base;

public class Quadro04
extends Quadro04Base {
    public boolean isEmpty() {
        return this.getAnexoEq04T1().isEmpty() && this.getAnexoEq04T2().isEmpty();
    }

    public void setAnexoEq04C1Formula_WithoutOverwriteBehavior() {
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoEq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoEq04T1().get(i).getRendimentos() == null ? 0 : this.getAnexoEq04T1().get(i).getRendimentos();
        }
        this.setAnexoEq04C1(newValue+=temporaryValue0);
    }

    public void setAnexoEq04C2Formula_WithoutOverwriteBehavior() {
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoEq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoEq04T1().get(i).getRetencoes() == null ? 0 : this.getAnexoEq04T1().get(i).getRetencoes();
        }
        this.setAnexoEq04C2(newValue+=temporaryValue0);
    }
}

