/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoE.qQuadro03";
    public static final String ANEXOEQ03C02_LINK = "aAnexoE.qQuadro03.fanexoEq03C02";
    public static final String ANEXOEQ03C02 = "anexoEq03C02";
    private Long anexoEq03C02;
    public static final String ANEXOEQ03C03_LINK = "aAnexoE.qQuadro03.fanexoEq03C03";
    public static final String ANEXOEQ03C03 = "anexoEq03C03";
    private Long anexoEq03C03;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoEq03C02() {
        return this.anexoEq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoEq03C02(Long anexoEq03C02) {
        Long oldValue = this.anexoEq03C02;
        this.anexoEq03C02 = anexoEq03C02;
        this.firePropertyChange("anexoEq03C02", oldValue, this.anexoEq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoEq03C03() {
        return this.anexoEq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoEq03C03(Long anexoEq03C03) {
        Long oldValue = this.anexoEq03C03;
        this.anexoEq03C03 = anexoEq03C03;
        this.firePropertyChange("anexoEq03C03", oldValue, this.anexoEq03C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoEq03C02);
        result = HashCodeUtil.hash(result, this.anexoEq03C03);
        return result;
    }
}

