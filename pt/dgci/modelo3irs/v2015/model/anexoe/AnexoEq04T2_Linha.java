/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_LinhaBase;

public class AnexoEq04T2_Linha
extends AnexoEq04T2_LinhaBase {
    public AnexoEq04T2_Linha() {
    }

    public AnexoEq04T2_Linha(Long nIF, String codRendimentos, String titular, Long rendimentos, Long retencoes) {
        super(nIF, codRendimentos, titular, rendimentos, retencoes);
    }

    public static String getLink(int numLinha) {
        return "aAnexoE.qQuadro04.tanexoEq04T2.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoEq04T2_LinhaBase.Property column) {
        return AnexoEq04T2_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

