/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModelBase;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoe.Quadro04;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=1)
public class AnexoEModel
extends AnexoEModelBase {
    public AnexoEModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public boolean isEmpty() {
        return this.getQuadro04().isEmpty();
    }
}

