/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_LinhaBase;

public class AnexoEq04T1_Linha
extends AnexoEq04T1_LinhaBase {
    public AnexoEq04T1_Linha() {
    }

    public AnexoEq04T1_Linha(Long nIF, String codRendimentos, String titular, Long rendimentos, Long retencoes) {
        super(nIF, codRendimentos, titular, rendimentos, retencoes);
    }

    public static String getLink(int numLinha) {
        return "aAnexoE.qQuadro04.tanexoEq04T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoEq04T1_LinhaBase.Property column) {
        return AnexoEq04T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

