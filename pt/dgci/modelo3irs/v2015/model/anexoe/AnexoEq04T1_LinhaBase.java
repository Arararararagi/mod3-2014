/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoEq04T1_LinhaBase
extends Model {
    public static final String NIF_LINK = "aAnexoE.qQuadro04.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;
    public static final String CODRENDIMENTOS_LINK = "aAnexoE.qQuadro04.fcodRendimentos";
    public static final String CODRENDIMENTOS = "codRendimentos";
    private String codRendimentos;
    public static final String TITULAR_LINK = "aAnexoE.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String RENDIMENTOS_LINK = "aAnexoE.qQuadro04.frendimentos";
    public static final String RENDIMENTOS = "rendimentos";
    private Long rendimentos;
    public static final String RETENCOES_LINK = "aAnexoE.qQuadro04.fretencoes";
    public static final String RETENCOES = "retencoes";
    private Long retencoes;

    public AnexoEq04T1_LinhaBase(Long nIF, String codRendimentos, String titular, Long rendimentos, Long retencoes) {
        this.nIF = nIF;
        this.codRendimentos = codRendimentos;
        this.titular = titular;
        this.rendimentos = rendimentos;
        this.retencoes = retencoes;
    }

    public AnexoEq04T1_LinhaBase() {
        this.nIF = null;
        this.codRendimentos = null;
        this.titular = null;
        this.rendimentos = null;
        this.retencoes = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoEQ4A")
    public String getCodRendimentos() {
        return this.codRendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodRendimentos(String codRendimentos) {
        String oldValue = this.codRendimentos;
        this.codRendimentos = codRendimentos;
        this.firePropertyChange("codRendimentos", oldValue, this.codRendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentos() {
        return this.rendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentos(Long rendimentos) {
        Long oldValue = this.rendimentos;
        this.rendimentos = rendimentos;
        this.firePropertyChange("rendimentos", oldValue, this.rendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencoes() {
        return this.retencoes;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencoes(Long retencoes) {
        Long oldValue = this.retencoes;
        this.retencoes = retencoes;
        this.firePropertyChange("retencoes", oldValue, this.retencoes);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.codRendimentos);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.rendimentos);
        result = HashCodeUtil.hash(result, this.retencoes);
        return result;
    }

    public static enum Property {
        NIF("nIF", 2),
        CODRENDIMENTOS("codRendimentos", 3),
        TITULAR("titular", 4),
        RENDIMENTOS("rendimentos", 5),
        RETENCOES("retencoes", 6);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

