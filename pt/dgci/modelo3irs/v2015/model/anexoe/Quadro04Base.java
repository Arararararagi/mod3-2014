/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoe;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoE.qQuadro04";
    public static final String ANEXOEQ04T1_LINK = "aAnexoE.qQuadro04.tanexoEq04T1";
    private EventList<AnexoEq04T1_Linha> anexoEq04T1 = new BasicEventList<AnexoEq04T1_Linha>();
    public static final String ANEXOEQ04C1_LINK = "aAnexoE.qQuadro04.fanexoEq04C1";
    public static final String ANEXOEQ04C1 = "anexoEq04C1";
    private Long anexoEq04C1;
    public static final String ANEXOEQ04C2_LINK = "aAnexoE.qQuadro04.fanexoEq04C2";
    public static final String ANEXOEQ04C2 = "anexoEq04C2";
    private Long anexoEq04C2;
    public static final String ANEXOEQ04T2_LINK = "aAnexoE.qQuadro04.tanexoEq04T2";
    private EventList<AnexoEq04T2_Linha> anexoEq04T2 = new BasicEventList<AnexoEq04T2_Linha>();
    public static final String ANEXOEQ04C3_LINK = "aAnexoE.qQuadro04.fanexoEq04C3";
    public static final String ANEXOEQ04C3 = "anexoEq04C3";
    private Long anexoEq04C3;
    public static final String ANEXOEQ04C4_LINK = "aAnexoE.qQuadro04.fanexoEq04C4";
    public static final String ANEXOEQ04C4 = "anexoEq04C4";
    private Long anexoEq04C4;
    public static final String ANEXOEQ04B1OP1_LINK = "aAnexoE.qQuadro04.fanexoEq04B1OP1";
    public static final String ANEXOEQ04B1OP1_VALUE = "1";
    public static final String ANEXOEQ04B1OP2_LINK = "aAnexoE.qQuadro04.fanexoEq04B1OP2";
    public static final String ANEXOEQ04B1OP2_VALUE = "2";
    public static final String ANEXOEQ04B1 = "anexoEq04B1";
    private String anexoEq04B1;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoEq04T1_Linha> getAnexoEq04T1() {
        return this.anexoEq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoEq04T1(EventList<AnexoEq04T1_Linha> anexoEq04T1) {
        this.anexoEq04T1 = anexoEq04T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoEq04C1() {
        return this.anexoEq04C1;
    }

    public void setAnexoEq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoEq04C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoEq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoEq04T1().get(i).getRendimentos() == null ? 0 : this.getAnexoEq04T1().get(i).getRendimentos();
        }
        this.setAnexoEq04C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoEq04C1(Long anexoEq04C1) {
        Long oldValue = this.anexoEq04C1;
        this.anexoEq04C1 = anexoEq04C1;
        this.firePropertyChange("anexoEq04C1", oldValue, this.anexoEq04C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoEq04C2() {
        return this.anexoEq04C2;
    }

    public void setAnexoEq04C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoEq04C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoEq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoEq04T1().get(i).getRetencoes() == null ? 0 : this.getAnexoEq04T1().get(i).getRetencoes();
        }
        this.setAnexoEq04C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoEq04C2(Long anexoEq04C2) {
        Long oldValue = this.anexoEq04C2;
        this.anexoEq04C2 = anexoEq04C2;
        this.firePropertyChange("anexoEq04C2", oldValue, this.anexoEq04C2);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoEq04T2_Linha> getAnexoEq04T2() {
        return this.anexoEq04T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoEq04T2(EventList<AnexoEq04T2_Linha> anexoEq04T2) {
        this.anexoEq04T2 = anexoEq04T2;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoEq04C3() {
        return this.anexoEq04C3;
    }

    public void setAnexoEq04C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoEq04C3")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoEq04T2().size(); ++i) {
            temporaryValue0+=this.getAnexoEq04T2().get(i).getRendimentos() == null ? 0 : this.getAnexoEq04T2().get(i).getRendimentos();
        }
        this.setAnexoEq04C3(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoEq04C3(Long anexoEq04C3) {
        Long oldValue = this.anexoEq04C3;
        this.anexoEq04C3 = anexoEq04C3;
        this.firePropertyChange("anexoEq04C3", oldValue, this.anexoEq04C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoEq04C4() {
        return this.anexoEq04C4;
    }

    public void setAnexoEq04C4Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoEq04C4")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoEq04T2().size(); ++i) {
            temporaryValue0+=this.getAnexoEq04T2().get(i).getRetencoes() == null ? 0 : this.getAnexoEq04T2().get(i).getRetencoes();
        }
        this.setAnexoEq04C4(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoEq04C4(Long anexoEq04C4) {
        Long oldValue = this.anexoEq04C4;
        this.anexoEq04C4 = anexoEq04C4;
        this.firePropertyChange("anexoEq04C4", oldValue, this.anexoEq04C4);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoEq04B1() {
        return this.anexoEq04B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoEq04B1(String anexoEq04B1) {
        String oldValue = this.anexoEq04B1;
        this.anexoEq04B1 = anexoEq04B1;
        this.firePropertyChange("anexoEq04B1", oldValue, this.anexoEq04B1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoEq04T1);
        result = HashCodeUtil.hash(result, this.anexoEq04C1);
        result = HashCodeUtil.hash(result, this.anexoEq04C2);
        result = HashCodeUtil.hash(result, this.anexoEq04T2);
        result = HashCodeUtil.hash(result, this.anexoEq04C3);
        result = HashCodeUtil.hash(result, this.anexoEq04C4);
        result = HashCodeUtil.hash(result, this.anexoEq04B1);
        return result;
    }
}

