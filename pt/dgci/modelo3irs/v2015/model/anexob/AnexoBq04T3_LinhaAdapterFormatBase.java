/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;

public class AnexoBq04T3_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoBq04T3_Linha>,
WritableTableFormat<AnexoBq04T3_Linha> {
    @Override
    public boolean isEditable(AnexoBq04T3_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoBq04T3_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNIPCdasEntidades();
            }
            case 2: {
                return line.getSubsidioDetinadoaExploracao();
            }
            case 3: {
                return line.getSubsidiosNaoDestinadosExploracaoN();
            }
            case 4: {
                return line.getSubsidiosNaoDestinadosExploracaoN1();
            }
            case 5: {
                return line.getSubsidiosNaoDestinadosExploracaoN2();
            }
            case 6: {
                return line.getSubsidiosNaoDestinadosExploracaoN3();
            }
            case 7: {
                return line.getSubsidiosNaoDestinadosExploracaoN4();
            }
        }
        return null;
    }

    @Override
    public AnexoBq04T3_Linha setColumnValue(AnexoBq04T3_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNIPCdasEntidades((Long)value);
                return line;
            }
            case 2: {
                line.setSubsidioDetinadoaExploracao((Long)value);
                return line;
            }
            case 3: {
                line.setSubsidiosNaoDestinadosExploracaoN((Long)value);
                return line;
            }
            case 4: {
                line.setSubsidiosNaoDestinadosExploracaoN1((Long)value);
                return line;
            }
            case 5: {
                line.setSubsidiosNaoDestinadosExploracaoN2((Long)value);
                return line;
            }
            case 6: {
                line.setSubsidiosNaoDestinadosExploracaoN3((Long)value);
                return line;
            }
            case 7: {
                line.setSubsidiosNaoDestinadosExploracaoN4((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIPC das Entidades";
            }
            case 2: {
                return "Subs\u00eddios destinados \u00e0 explora\u00e7\u00e3o";
            }
            case 3: {
                return "N";
            }
            case 4: {
                return "N-1";
            }
            case 5: {
                return "N-2";
            }
            case 6: {
                return "N-3";
            }
            case 7: {
                return "N-4";
            }
        }
        return null;
    }
}

