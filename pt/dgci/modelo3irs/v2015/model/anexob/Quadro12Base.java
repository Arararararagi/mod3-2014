/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;
import pt.opensoft.util.Date;

public class Quadro12Base
extends QuadroModel {
    public static final String QUADRO12_LINK = "aAnexoB.qQuadro12";
    public static final String ANEXOBQ12B1OP0_LINK = "aAnexoB.qQuadro12.fanexoBq12B1OP1";
    public static final String ANEXOBQ12B1OP0_VALUE = "0";
    public static final String ANEXOBQ12B1OP1_LINK = "aAnexoB.qQuadro12.fanexoBq12B1OP1";
    public static final String ANEXOBQ12B1OP1_VALUE = "1";
    public static final String ANEXOBQ12B1OP2_LINK = "aAnexoB.qQuadro12.fanexoBq12B1OP2";
    public static final String ANEXOBQ12B1OP2_VALUE = "2";
    public static final String ANEXOBQ12B1 = "anexoBq12B1";
    private String anexoBq12B1;
    public static final String ANEXOBQ12C3_LINK = "aAnexoB.qQuadro12.fanexoBq12C3";
    public static final String ANEXOBQ12C3 = "anexoBq12C3";
    private Date anexoBq12C3;
    public static final String ANEXOBQ12B4_LINK = "aAnexoB.qQuadro12.fanexoBq12B4";
    public static final String ANEXOBQ12B4 = "anexoBq12B4";
    private Boolean anexoBq12B4;

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoBq12B1() {
        return this.anexoBq12B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq12B1(String anexoBq12B1) {
        String oldValue = this.anexoBq12B1;
        this.anexoBq12B1 = anexoBq12B1;
        this.firePropertyChange("anexoBq12B1", oldValue, this.anexoBq12B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Date getAnexoBq12C3() {
        return this.anexoBq12C3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq12C3(Date anexoBq12C3) {
        Date oldValue = this.anexoBq12C3;
        this.anexoBq12C3 = anexoBq12C3;
        this.firePropertyChange("anexoBq12C3", oldValue, this.anexoBq12C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoBq12B4() {
        return this.anexoBq12B4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq12B4(Boolean anexoBq12B4) {
        Boolean oldValue = this.anexoBq12B4;
        this.anexoBq12B4 = anexoBq12B4;
        this.firePropertyChange("anexoBq12B4", oldValue, this.anexoBq12B4);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq12B1);
        result = HashCodeUtil.hash(result, this.anexoBq12C3);
        result = HashCodeUtil.hash(result, this.anexoBq12B4);
        return result;
    }
}

