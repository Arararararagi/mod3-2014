/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class AnexoBq04T1_Linha
extends AnexoBq04T1_LinhaBase {
    public AnexoBq04T1_Linha() {
    }

    public AnexoBq04T1_Linha(String freguesia, String tipoPredio, Long artigo, String fraccao, Long valorVenda, Long campoQ4, Long valorDefinitivo, String art139CIRC) {
        super(freguesia, tipoPredio, artigo, fraccao, valorVenda, campoQ4, valorDefinitivo, art139CIRC);
    }

    public boolean isEmpty() {
        return StringUtil.isEmpty(this.getFreguesia()) && StringUtil.isEmpty(this.getTipoPredio()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getArtigo()) && StringUtil.isEmpty(this.getFraccao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getValorVenda()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getCampoQ4()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getValorDefinitivo()) && (StringUtil.isEmpty(this.getArt139CIRC()) || this.getArt139CIRC().equals("N"));
    }

    public static String getLink(int numLinha) {
        return "aAnexoB.qQuadro04.tanexoBq04T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoBq04T1_LinhaBase.Property column) {
        return AnexoBq04T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

