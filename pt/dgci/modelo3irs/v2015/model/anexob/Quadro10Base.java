/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro10Base
extends QuadroModel {
    public static final String QUADRO10_LINK = "aAnexoB.qQuadro10";
    public static final String ANEXOBQ10C1001_LINK = "aAnexoB.qQuadro10.fanexoBq10C1001";
    public static final String ANEXOBQ10C1001 = "anexoBq10C1001";
    private Long anexoBq10C1001;
    public static final String ANEXOBQ10C1002_LINK = "aAnexoB.qQuadro10.fanexoBq10C1002";
    public static final String ANEXOBQ10C1002 = "anexoBq10C1002";
    private Long anexoBq10C1002;
    public static final String ANEXOBQ10C1_LINK = "aAnexoB.qQuadro10.fanexoBq10C1";
    public static final String ANEXOBQ10C1 = "anexoBq10C1";
    private Long anexoBq10C1;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq10C1001() {
        return this.anexoBq10C1001;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq10C1001(Long anexoBq10C1001) {
        Long oldValue = this.anexoBq10C1001;
        this.anexoBq10C1001 = anexoBq10C1001;
        this.firePropertyChange("anexoBq10C1001", oldValue, this.anexoBq10C1001);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq10C1002() {
        return this.anexoBq10C1002;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq10C1002(Long anexoBq10C1002) {
        Long oldValue = this.anexoBq10C1002;
        this.anexoBq10C1002 = anexoBq10C1002;
        this.firePropertyChange("anexoBq10C1002", oldValue, this.anexoBq10C1002);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq10C1() {
        return this.anexoBq10C1;
    }

    public void setAnexoBq10C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq10C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq10C1(newValue+=(this.getAnexoBq10C1001() == null ? 0 : this.getAnexoBq10C1001()) + (this.getAnexoBq10C1002() == null ? 0 : this.getAnexoBq10C1002()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq10C1(Long anexoBq10C1) {
        Long oldValue = this.anexoBq10C1;
        this.anexoBq10C1 = anexoBq10C1;
        this.firePropertyChange("anexoBq10C1", oldValue, this.anexoBq10C1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq10C1001);
        result = HashCodeUtil.hash(result, this.anexoBq10C1002);
        result = HashCodeUtil.hash(result, this.anexoBq10C1);
        return result;
    }
}

