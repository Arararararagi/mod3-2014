/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro09Base
extends QuadroModel {
    public static final String QUADRO09_LINK = "aAnexoB.qQuadro09";
    public static final String ANEXOBQ09C901_LINK = "aAnexoB.qQuadro09.fanexoBq09C901";
    public static final String ANEXOBQ09C901 = "anexoBq09C901";
    private Long anexoBq09C901;
    public static final String ANEXOBQ09C902_LINK = "aAnexoB.qQuadro09.fanexoBq09C902";
    public static final String ANEXOBQ09C902 = "anexoBq09C902";
    private Long anexoBq09C902;
    public static final String ANEXOBQ09C903_LINK = "aAnexoB.qQuadro09.fanexoBq09C903";
    public static final String ANEXOBQ09C903 = "anexoBq09C903";
    private Long anexoBq09C903;
    public static final String ANEXOBQ09C904_LINK = "aAnexoB.qQuadro09.fanexoBq09C904";
    public static final String ANEXOBQ09C904 = "anexoBq09C904";
    private Long anexoBq09C904;
    public static final String ANEXOBQ09C905_LINK = "aAnexoB.qQuadro09.fanexoBq09C905";
    public static final String ANEXOBQ09C905 = "anexoBq09C905";
    private Long anexoBq09C905;
    public static final String ANEXOBQ09C906_LINK = "aAnexoB.qQuadro09.fanexoBq09C906";
    public static final String ANEXOBQ09C906 = "anexoBq09C906";
    private Long anexoBq09C906;
    public static final String ANEXOBQ09C907_LINK = "aAnexoB.qQuadro09.fanexoBq09C907";
    public static final String ANEXOBQ09C907 = "anexoBq09C907";
    private Long anexoBq09C907;
    public static final String ANEXOBQ09C908_LINK = "aAnexoB.qQuadro09.fanexoBq09C908";
    public static final String ANEXOBQ09C908 = "anexoBq09C908";
    private Long anexoBq09C908;
    public static final String ANEXOBQ09C1A_LINK = "aAnexoB.qQuadro09.fanexoBq09C1a";
    public static final String ANEXOBQ09C1A = "anexoBq09C1a";
    private Long anexoBq09C1a;
    public static final String ANEXOBQ09C910_LINK = "aAnexoB.qQuadro09.fanexoBq09C910";
    public static final String ANEXOBQ09C910 = "anexoBq09C910";
    private Long anexoBq09C910;
    public static final String ANEXOBQ09C911_LINK = "aAnexoB.qQuadro09.fanexoBq09C911";
    public static final String ANEXOBQ09C911 = "anexoBq09C911";
    private Long anexoBq09C911;
    public static final String ANEXOBQ09C912_LINK = "aAnexoB.qQuadro09.fanexoBq09C912";
    public static final String ANEXOBQ09C912 = "anexoBq09C912";
    private Long anexoBq09C912;
    public static final String ANEXOBQ09C913_LINK = "aAnexoB.qQuadro09.fanexoBq09C913";
    public static final String ANEXOBQ09C913 = "anexoBq09C913";
    private Long anexoBq09C913;
    public static final String ANEXOBQ09C914_LINK = "aAnexoB.qQuadro09.fanexoBq09C914";
    public static final String ANEXOBQ09C914 = "anexoBq09C914";
    private Long anexoBq09C914;
    public static final String ANEXOBQ09C915_LINK = "aAnexoB.qQuadro09.fanexoBq09C915";
    public static final String ANEXOBQ09C915 = "anexoBq09C915";
    private Long anexoBq09C915;
    public static final String ANEXOBQ09C916_LINK = "aAnexoB.qQuadro09.fanexoBq09C916";
    public static final String ANEXOBQ09C916 = "anexoBq09C916";
    private Long anexoBq09C916;
    public static final String ANEXOBQ09C917_LINK = "aAnexoB.qQuadro09.fanexoBq09C917";
    public static final String ANEXOBQ09C917 = "anexoBq09C917";
    private Long anexoBq09C917;
    public static final String ANEXOBQ09C1B_LINK = "aAnexoB.qQuadro09.fanexoBq09C1b";
    public static final String ANEXOBQ09C1B = "anexoBq09C1b";
    private Long anexoBq09C1b;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C901() {
        return this.anexoBq09C901;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C901(Long anexoBq09C901) {
        Long oldValue = this.anexoBq09C901;
        this.anexoBq09C901 = anexoBq09C901;
        this.firePropertyChange("anexoBq09C901", oldValue, this.anexoBq09C901);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C902() {
        return this.anexoBq09C902;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C902(Long anexoBq09C902) {
        Long oldValue = this.anexoBq09C902;
        this.anexoBq09C902 = anexoBq09C902;
        this.firePropertyChange("anexoBq09C902", oldValue, this.anexoBq09C902);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C903() {
        return this.anexoBq09C903;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C903(Long anexoBq09C903) {
        Long oldValue = this.anexoBq09C903;
        this.anexoBq09C903 = anexoBq09C903;
        this.firePropertyChange("anexoBq09C903", oldValue, this.anexoBq09C903);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C904() {
        return this.anexoBq09C904;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C904(Long anexoBq09C904) {
        Long oldValue = this.anexoBq09C904;
        this.anexoBq09C904 = anexoBq09C904;
        this.firePropertyChange("anexoBq09C904", oldValue, this.anexoBq09C904);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C905() {
        return this.anexoBq09C905;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C905(Long anexoBq09C905) {
        Long oldValue = this.anexoBq09C905;
        this.anexoBq09C905 = anexoBq09C905;
        this.firePropertyChange("anexoBq09C905", oldValue, this.anexoBq09C905);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C906() {
        return this.anexoBq09C906;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C906(Long anexoBq09C906) {
        Long oldValue = this.anexoBq09C906;
        this.anexoBq09C906 = anexoBq09C906;
        this.firePropertyChange("anexoBq09C906", oldValue, this.anexoBq09C906);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C907() {
        return this.anexoBq09C907;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C907(Long anexoBq09C907) {
        Long oldValue = this.anexoBq09C907;
        this.anexoBq09C907 = anexoBq09C907;
        this.firePropertyChange("anexoBq09C907", oldValue, this.anexoBq09C907);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C908() {
        return this.anexoBq09C908;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C908(Long anexoBq09C908) {
        Long oldValue = this.anexoBq09C908;
        this.anexoBq09C908 = anexoBq09C908;
        this.firePropertyChange("anexoBq09C908", oldValue, this.anexoBq09C908);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C1a() {
        return this.anexoBq09C1a;
    }

    public void setAnexoBq09C1aFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq09C1a")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq09C1a(newValue+=(this.getAnexoBq09C901() == null ? 0 : this.getAnexoBq09C901()) + (this.getAnexoBq09C902() == null ? 0 : this.getAnexoBq09C902()) + (this.getAnexoBq09C903() == null ? 0 : this.getAnexoBq09C903()) + (this.getAnexoBq09C904() == null ? 0 : this.getAnexoBq09C904()) + (this.getAnexoBq09C905() == null ? 0 : this.getAnexoBq09C905()) + (this.getAnexoBq09C906() == null ? 0 : this.getAnexoBq09C906()) + (this.getAnexoBq09C907() == null ? 0 : this.getAnexoBq09C907()) + (this.getAnexoBq09C908() == null ? 0 : this.getAnexoBq09C908()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C1a(Long anexoBq09C1a) {
        Long oldValue = this.anexoBq09C1a;
        this.anexoBq09C1a = anexoBq09C1a;
        this.firePropertyChange("anexoBq09C1a", oldValue, this.anexoBq09C1a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C910() {
        return this.anexoBq09C910;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C910(Long anexoBq09C910) {
        Long oldValue = this.anexoBq09C910;
        this.anexoBq09C910 = anexoBq09C910;
        this.firePropertyChange("anexoBq09C910", oldValue, this.anexoBq09C910);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C911() {
        return this.anexoBq09C911;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C911(Long anexoBq09C911) {
        Long oldValue = this.anexoBq09C911;
        this.anexoBq09C911 = anexoBq09C911;
        this.firePropertyChange("anexoBq09C911", oldValue, this.anexoBq09C911);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C912() {
        return this.anexoBq09C912;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C912(Long anexoBq09C912) {
        Long oldValue = this.anexoBq09C912;
        this.anexoBq09C912 = anexoBq09C912;
        this.firePropertyChange("anexoBq09C912", oldValue, this.anexoBq09C912);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C913() {
        return this.anexoBq09C913;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C913(Long anexoBq09C913) {
        Long oldValue = this.anexoBq09C913;
        this.anexoBq09C913 = anexoBq09C913;
        this.firePropertyChange("anexoBq09C913", oldValue, this.anexoBq09C913);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C914() {
        return this.anexoBq09C914;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C914(Long anexoBq09C914) {
        Long oldValue = this.anexoBq09C914;
        this.anexoBq09C914 = anexoBq09C914;
        this.firePropertyChange("anexoBq09C914", oldValue, this.anexoBq09C914);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C915() {
        return this.anexoBq09C915;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C915(Long anexoBq09C915) {
        Long oldValue = this.anexoBq09C915;
        this.anexoBq09C915 = anexoBq09C915;
        this.firePropertyChange("anexoBq09C915", oldValue, this.anexoBq09C915);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C916() {
        return this.anexoBq09C916;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C916(Long anexoBq09C916) {
        Long oldValue = this.anexoBq09C916;
        this.anexoBq09C916 = anexoBq09C916;
        this.firePropertyChange("anexoBq09C916", oldValue, this.anexoBq09C916);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C917() {
        return this.anexoBq09C917;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C917(Long anexoBq09C917) {
        Long oldValue = this.anexoBq09C917;
        this.anexoBq09C917 = anexoBq09C917;
        this.firePropertyChange("anexoBq09C917", oldValue, this.anexoBq09C917);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq09C1b() {
        return this.anexoBq09C1b;
    }

    public void setAnexoBq09C1bFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq09C1b")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq09C1b(newValue+=(this.getAnexoBq09C910() == null ? 0 : this.getAnexoBq09C910()) + (this.getAnexoBq09C911() == null ? 0 : this.getAnexoBq09C911()) + (this.getAnexoBq09C912() == null ? 0 : this.getAnexoBq09C912()) + (this.getAnexoBq09C913() == null ? 0 : this.getAnexoBq09C913()) + (this.getAnexoBq09C914() == null ? 0 : this.getAnexoBq09C914()) + (this.getAnexoBq09C915() == null ? 0 : this.getAnexoBq09C915()) + (this.getAnexoBq09C916() == null ? 0 : this.getAnexoBq09C916()) + (this.getAnexoBq09C917() == null ? 0 : this.getAnexoBq09C917()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq09C1b(Long anexoBq09C1b) {
        Long oldValue = this.anexoBq09C1b;
        this.anexoBq09C1b = anexoBq09C1b;
        this.firePropertyChange("anexoBq09C1b", oldValue, this.anexoBq09C1b);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq09C901);
        result = HashCodeUtil.hash(result, this.anexoBq09C902);
        result = HashCodeUtil.hash(result, this.anexoBq09C903);
        result = HashCodeUtil.hash(result, this.anexoBq09C904);
        result = HashCodeUtil.hash(result, this.anexoBq09C905);
        result = HashCodeUtil.hash(result, this.anexoBq09C906);
        result = HashCodeUtil.hash(result, this.anexoBq09C907);
        result = HashCodeUtil.hash(result, this.anexoBq09C908);
        result = HashCodeUtil.hash(result, this.anexoBq09C1a);
        result = HashCodeUtil.hash(result, this.anexoBq09C910);
        result = HashCodeUtil.hash(result, this.anexoBq09C911);
        result = HashCodeUtil.hash(result, this.anexoBq09C912);
        result = HashCodeUtil.hash(result, this.anexoBq09C913);
        result = HashCodeUtil.hash(result, this.anexoBq09C914);
        result = HashCodeUtil.hash(result, this.anexoBq09C915);
        result = HashCodeUtil.hash(result, this.anexoBq09C916);
        result = HashCodeUtil.hash(result, this.anexoBq09C917);
        result = HashCodeUtil.hash(result, this.anexoBq09C1b);
        return result;
    }
}

