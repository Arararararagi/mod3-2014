/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro07Base;

public class Quadro07
extends Quadro07Base {
    public boolean isEmpty() {
        return this.getAnexoBq07C701() == null && this.getAnexoBq07C702() == null && this.getAnexoBq07C703() == null && this.getAnexoBq07C704() == null && this.getAnexoBq07T1().isEmpty();
    }
}

