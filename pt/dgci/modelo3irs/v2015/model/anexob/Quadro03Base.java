/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoB.qQuadro03";
    public static final String ANEXOBQ03C06_LINK = "aAnexoB.qQuadro03.fanexoBq03C06";
    public static final String ANEXOBQ03C06 = "anexoBq03C06";
    private Long anexoBq03C06;
    public static final String ANEXOBQ03C07_LINK = "aAnexoB.qQuadro03.fanexoBq03C07";
    public static final String ANEXOBQ03C07 = "anexoBq03C07";
    private Long anexoBq03C07;
    public static final String ANEXOBQ03B1OP1_LINK = "aAnexoB.qQuadro03.fanexoBq03B1OP1";
    public static final String ANEXOBQ03B1OP1_VALUE = "1";
    public static final String ANEXOBQ03B1OP2_LINK = "aAnexoB.qQuadro03.fanexoBq03B1OP2";
    public static final String ANEXOBQ03B1OP2_VALUE = "2";
    public static final String ANEXOBQ03B1 = "anexoBq03B1";
    private String anexoBq03B1;
    public static final String ANEXOBQ03C08_LINK = "aAnexoB.qQuadro03.fanexoBq03C08";
    public static final String ANEXOBQ03C08 = "anexoBq03C08";
    private Long anexoBq03C08;
    public static final String ANEXOBQ03C09_LINK = "aAnexoB.qQuadro03.fanexoBq03C09";
    public static final String ANEXOBQ03C09 = "anexoBq03C09";
    private Long anexoBq03C09;
    public static final String ANEXOBQ03C10_LINK = "aAnexoB.qQuadro03.fanexoBq03C10";
    public static final String ANEXOBQ03C10 = "anexoBq03C10";
    private Long anexoBq03C10;
    public static final String ANEXOBQ03C11_LINK = "aAnexoB.qQuadro03.fanexoBq03C11";
    public static final String ANEXOBQ03C11 = "anexoBq03C11";
    private Long anexoBq03C11;
    public static final String ANEXOBQ03C12_LINK = "aAnexoB.qQuadro03.fanexoBq03C12";
    public static final String ANEXOBQ03C12 = "anexoBq03C12";
    private Long anexoBq03C12;
    public static final String ANEXOBQ03B13OP13_LINK = "aAnexoB.qQuadro03.fanexoBq03B13OP13";
    public static final String ANEXOBQ03B13OP13_VALUE = "13";
    public static final String ANEXOBQ03B13OP14_LINK = "aAnexoB.qQuadro03.fanexoBq03B13OP14";
    public static final String ANEXOBQ03B13OP14_VALUE = "14";
    public static final String ANEXOBQ03B13 = "anexoBq03B13";
    private String anexoBq03B13;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq03C06() {
        return this.anexoBq03C06;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03C06(Long anexoBq03C06) {
        Long oldValue = this.anexoBq03C06;
        this.anexoBq03C06 = anexoBq03C06;
        this.firePropertyChange("anexoBq03C06", oldValue, this.anexoBq03C06);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq03C07() {
        return this.anexoBq03C07;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03C07(Long anexoBq03C07) {
        Long oldValue = this.anexoBq03C07;
        this.anexoBq03C07 = anexoBq03C07;
        this.firePropertyChange("anexoBq03C07", oldValue, this.anexoBq03C07);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoBq03B1() {
        return this.anexoBq03B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03B1(String anexoBq03B1) {
        String oldValue = this.anexoBq03B1;
        this.anexoBq03B1 = anexoBq03B1;
        this.firePropertyChange("anexoBq03B1", oldValue, this.anexoBq03B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq03C08() {
        return this.anexoBq03C08;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03C08(Long anexoBq03C08) {
        Long oldValue = this.anexoBq03C08;
        this.anexoBq03C08 = anexoBq03C08;
        this.firePropertyChange("anexoBq03C08", oldValue, this.anexoBq03C08);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq03C09() {
        return this.anexoBq03C09;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03C09(Long anexoBq03C09) {
        Long oldValue = this.anexoBq03C09;
        this.anexoBq03C09 = anexoBq03C09;
        this.firePropertyChange("anexoBq03C09", oldValue, this.anexoBq03C09);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq03C10() {
        return this.anexoBq03C10;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03C10(Long anexoBq03C10) {
        Long oldValue = this.anexoBq03C10;
        this.anexoBq03C10 = anexoBq03C10;
        this.firePropertyChange("anexoBq03C10", oldValue, this.anexoBq03C10);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq03C11() {
        return this.anexoBq03C11;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03C11(Long anexoBq03C11) {
        Long oldValue = this.anexoBq03C11;
        this.anexoBq03C11 = anexoBq03C11;
        this.firePropertyChange("anexoBq03C11", oldValue, this.anexoBq03C11);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq03C12() {
        return this.anexoBq03C12;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03C12(Long anexoBq03C12) {
        Long oldValue = this.anexoBq03C12;
        this.anexoBq03C12 = anexoBq03C12;
        this.firePropertyChange("anexoBq03C12", oldValue, this.anexoBq03C12);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoBq03B13() {
        return this.anexoBq03B13;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq03B13(String anexoBq03B13) {
        String oldValue = this.anexoBq03B13;
        this.anexoBq03B13 = anexoBq03B13;
        this.firePropertyChange("anexoBq03B13", oldValue, this.anexoBq03B13);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq03C06);
        result = HashCodeUtil.hash(result, this.anexoBq03C07);
        result = HashCodeUtil.hash(result, this.anexoBq03B1);
        result = HashCodeUtil.hash(result, this.anexoBq03C08);
        result = HashCodeUtil.hash(result, this.anexoBq03C09);
        result = HashCodeUtil.hash(result, this.anexoBq03C10);
        result = HashCodeUtil.hash(result, this.anexoBq03C11);
        result = HashCodeUtil.hash(result, this.anexoBq03C12);
        result = HashCodeUtil.hash(result, this.anexoBq03B13);
        return result;
    }
}

