/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.OwnableAnexo;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModelBase;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro00;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro10;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=9)
public class AnexoBModel
extends AnexoBModelBase
implements OwnableAnexo {
    public AnexoBModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
        this.postCreate();
    }

    protected void postCreate() {
        this.setOwner(this.formKey.getSubId());
    }

    public void setOwner(String nif) {
        Quadro03 quadro03Model = this.getQuadro03();
        if (quadro03Model != null) {
            if (NifValidator.isSingular(nif)) {
                quadro03Model.setAnexoBq03C08(Long.parseLong(nif));
            } else {
                quadro03Model.setAnexoBq03C09(Long.parseLong(nif));
            }
        }
    }

    @Override
    public boolean isOwnedBy(Long nif) {
        return nif != null && (nif.equals(this.getQuadro03().getAnexoBq03C08()) || nif.equals(this.getQuadro03().getAnexoBq03C09()));
    }

    public Long getAnexoBTitular() {
        return this.getQuadro03().getAnexoBq03C08() != null ? this.getQuadro03().getAnexoBq03C08() : this.getQuadro03().getAnexoBq03C09();
    }

    public String getLink(String linkWithoutSubId) {
        return linkWithoutSubId.replaceFirst("aAnexoB", "aAnexoB|" + this.getFormKey().getSubId());
    }

    public Quadro00 getQuadro00() {
        return (Quadro00)this.getQuadro(Quadro00.class.getSimpleName());
    }

    public Quadro01 getQuadro01() {
        return (Quadro01)this.getQuadro(Quadro01.class.getSimpleName());
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public Quadro07 getQuadro07() {
        return (Quadro07)this.getQuadro(Quadro07.class.getSimpleName());
    }

    public Quadro08 getQuadro08() {
        return (Quadro08)this.getQuadro(Quadro08.class.getSimpleName());
    }

    public Quadro09 getQuadro09() {
        return (Quadro09)this.getQuadro(Quadro09.class.getSimpleName());
    }

    public Quadro10 getQuadro10() {
        return (Quadro10)this.getQuadro(Quadro10.class.getSimpleName());
    }

    public Quadro11 getQuadro11() {
        return (Quadro11)this.getQuadro(Quadro11.class.getSimpleName());
    }

    public Quadro12 getQuadro12() {
        return (Quadro12)this.getQuadro(Quadro12.class.getSimpleName());
    }

    @Override
    public boolean addQuadro(QuadroModel quadro) {
        if ("Quadro04".equals(quadro.getQuadroName())) {
            Quadro04 quadro4 = (Quadro04)quadro;
            quadro4.setAnexoPai(this);
            return super.addQuadro(quadro4);
        }
        if ("Quadro12".equals(quadro.getQuadroName())) {
            Quadro12 quadro12 = (Quadro12)quadro;
            quadro12.setAnexoPai(this);
            return super.addQuadro(quadro12);
        }
        return super.addQuadro(quadro);
    }

    public boolean isEmpty() {
        return this.getQuadro03().isEmpty() && this.getQuadro04().isEmpty() && this.getQuadro05().isEmpty() && this.getQuadro06().isEmpty() && this.getQuadro07().isEmpty() && this.getQuadro08().isEmpty() && this.getQuadro09().isEmpty() && this.getQuadro10().isEmpty() && this.getQuadro11().isEmpty() && this.getQuadro12().isEmpty();
    }
}

