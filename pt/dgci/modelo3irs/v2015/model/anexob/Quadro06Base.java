/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoB.qQuadro06";
    public static final String ANEXOBQ06C601_LINK = "aAnexoB.qQuadro06.fanexoBq06C601";
    public static final String ANEXOBQ06C601 = "anexoBq06C601";
    private Long anexoBq06C601;
    public static final String ANEXOBQ06C602_LINK = "aAnexoB.qQuadro06.fanexoBq06C602";
    public static final String ANEXOBQ06C602 = "anexoBq06C602";
    private Long anexoBq06C602;
    public static final String ANEXOBQ06C1_LINK = "aAnexoB.qQuadro06.fanexoBq06C1";
    public static final String ANEXOBQ06C1 = "anexoBq06C1";
    private Long anexoBq06C1;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq06C601() {
        return this.anexoBq06C601;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq06C601(Long anexoBq06C601) {
        Long oldValue = this.anexoBq06C601;
        this.anexoBq06C601 = anexoBq06C601;
        this.firePropertyChange("anexoBq06C601", oldValue, this.anexoBq06C601);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq06C602() {
        return this.anexoBq06C602;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq06C602(Long anexoBq06C602) {
        Long oldValue = this.anexoBq06C602;
        this.anexoBq06C602 = anexoBq06C602;
        this.firePropertyChange("anexoBq06C602", oldValue, this.anexoBq06C602);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq06C1() {
        return this.anexoBq06C1;
    }

    public void setAnexoBq06C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq06C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq06C1(newValue+=(this.getAnexoBq06C601() == null ? 0 : this.getAnexoBq06C601()) + (this.getAnexoBq06C602() == null ? 0 : this.getAnexoBq06C602()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq06C1(Long anexoBq06C1) {
        Long oldValue = this.anexoBq06C1;
        this.anexoBq06C1 = anexoBq06C1;
        this.firePropertyChange("anexoBq06C1", oldValue, this.anexoBq06C1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq06C601);
        result = HashCodeUtil.hash(result, this.anexoBq06C602);
        result = HashCodeUtil.hash(result, this.anexoBq06C1);
        return result;
    }
}

