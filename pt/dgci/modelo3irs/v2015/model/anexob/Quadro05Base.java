/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoB.qQuadro05";
    public static final String ANEXOBQ05C501_LINK = "aAnexoB.qQuadro05.fanexoBq05C501";
    public static final String ANEXOBQ05C501 = "anexoBq05C501";
    private Long anexoBq05C501;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq05C501() {
        return this.anexoBq05C501;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq05C501(Long anexoBq05C501) {
        Long oldValue = this.anexoBq05C501;
        this.anexoBq05C501 = anexoBq05C501;
        this.firePropertyChange("anexoBq05C501", oldValue, this.anexoBq05C501);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq05C501);
        return result;
    }
}

