/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.Quadro10Base;

public class Quadro10
extends Quadro10Base {
    public boolean isEmpty() {
        return this.getAnexoBq10C1001() == null && this.getAnexoBq10C1002() == null && this.getAnexoBq10C1() == null;
    }
}

