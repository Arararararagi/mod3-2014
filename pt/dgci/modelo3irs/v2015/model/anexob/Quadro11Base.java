/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro11Base
extends QuadroModel {
    public static final String QUADRO11_LINK = "aAnexoB.qQuadro11";
    public static final String ANEXOBQ11C1101_LINK = "aAnexoB.qQuadro11.fanexoBq11C1101";
    public static final String ANEXOBQ11C1101 = "anexoBq11C1101";
    private Long anexoBq11C1101;
    public static final String ANEXOBQ11C1102_LINK = "aAnexoB.qQuadro11.fanexoBq11C1102";
    public static final String ANEXOBQ11C1102 = "anexoBq11C1102";
    private Long anexoBq11C1102;
    public static final String ANEXOBQ11C1103_LINK = "aAnexoB.qQuadro11.fanexoBq11C1103";
    public static final String ANEXOBQ11C1103 = "anexoBq11C1103";
    private Long anexoBq11C1103;
    public static final String ANEXOBQ11C1104_LINK = "aAnexoB.qQuadro11.fanexoBq11C1104";
    public static final String ANEXOBQ11C1104 = "anexoBq11C1104";
    private Long anexoBq11C1104;
    public static final String ANEXOBQ11C1105_LINK = "aAnexoB.qQuadro11.fanexoBq11C1105";
    public static final String ANEXOBQ11C1105 = "anexoBq11C1105";
    private Long anexoBq11C1105;
    public static final String ANEXOBQ11C1106_LINK = "aAnexoB.qQuadro11.fanexoBq11C1106";
    public static final String ANEXOBQ11C1106 = "anexoBq11C1106";
    private Long anexoBq11C1106;
    public static final String ANEXOBQ11C1107_LINK = "aAnexoB.qQuadro11.fanexoBq11C1107";
    public static final String ANEXOBQ11C1107 = "anexoBq11C1107";
    private Long anexoBq11C1107;
    public static final String ANEXOBQ11C1108_LINK = "aAnexoB.qQuadro11.fanexoBq11C1108";
    public static final String ANEXOBQ11C1108 = "anexoBq11C1108";
    private Long anexoBq11C1108;
    public static final String ANEXOBQ11C1109_LINK = "aAnexoB.qQuadro11.fanexoBq11C1109";
    public static final String ANEXOBQ11C1109 = "anexoBq11C1109";
    private Long anexoBq11C1109;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1101() {
        return this.anexoBq11C1101;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1101(Long anexoBq11C1101) {
        Long oldValue = this.anexoBq11C1101;
        this.anexoBq11C1101 = anexoBq11C1101;
        this.firePropertyChange("anexoBq11C1101", oldValue, this.anexoBq11C1101);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1102() {
        return this.anexoBq11C1102;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1102(Long anexoBq11C1102) {
        Long oldValue = this.anexoBq11C1102;
        this.anexoBq11C1102 = anexoBq11C1102;
        this.firePropertyChange("anexoBq11C1102", oldValue, this.anexoBq11C1102);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1103() {
        return this.anexoBq11C1103;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1103(Long anexoBq11C1103) {
        Long oldValue = this.anexoBq11C1103;
        this.anexoBq11C1103 = anexoBq11C1103;
        this.firePropertyChange("anexoBq11C1103", oldValue, this.anexoBq11C1103);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1104() {
        return this.anexoBq11C1104;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1104(Long anexoBq11C1104) {
        Long oldValue = this.anexoBq11C1104;
        this.anexoBq11C1104 = anexoBq11C1104;
        this.firePropertyChange("anexoBq11C1104", oldValue, this.anexoBq11C1104);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1105() {
        return this.anexoBq11C1105;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1105(Long anexoBq11C1105) {
        Long oldValue = this.anexoBq11C1105;
        this.anexoBq11C1105 = anexoBq11C1105;
        this.firePropertyChange("anexoBq11C1105", oldValue, this.anexoBq11C1105);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1106() {
        return this.anexoBq11C1106;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1106(Long anexoBq11C1106) {
        Long oldValue = this.anexoBq11C1106;
        this.anexoBq11C1106 = anexoBq11C1106;
        this.firePropertyChange("anexoBq11C1106", oldValue, this.anexoBq11C1106);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1107() {
        return this.anexoBq11C1107;
    }

    public void setAnexoBq11C1107Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq11C1107")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq11C1107(newValue+=(this.getAnexoBq11C1101() == null ? 0 : this.getAnexoBq11C1101()) + (this.getAnexoBq11C1102() == null ? 0 : this.getAnexoBq11C1102()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1107(Long anexoBq11C1107) {
        Long oldValue = this.anexoBq11C1107;
        this.anexoBq11C1107 = anexoBq11C1107;
        this.firePropertyChange("anexoBq11C1107", oldValue, this.anexoBq11C1107);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1108() {
        return this.anexoBq11C1108;
    }

    public void setAnexoBq11C1108Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq11C1108")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq11C1108(newValue+=(this.getAnexoBq11C1103() == null ? 0 : this.getAnexoBq11C1103()) + (this.getAnexoBq11C1104() == null ? 0 : this.getAnexoBq11C1104()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1108(Long anexoBq11C1108) {
        Long oldValue = this.anexoBq11C1108;
        this.anexoBq11C1108 = anexoBq11C1108;
        this.firePropertyChange("anexoBq11C1108", oldValue, this.anexoBq11C1108);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq11C1109() {
        return this.anexoBq11C1109;
    }

    public void setAnexoBq11C1109Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq11C1109")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq11C1109(newValue+=(this.getAnexoBq11C1105() == null ? 0 : this.getAnexoBq11C1105()) + (this.getAnexoBq11C1106() == null ? 0 : this.getAnexoBq11C1106()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq11C1109(Long anexoBq11C1109) {
        Long oldValue = this.anexoBq11C1109;
        this.anexoBq11C1109 = anexoBq11C1109;
        this.firePropertyChange("anexoBq11C1109", oldValue, this.anexoBq11C1109);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq11C1101);
        result = HashCodeUtil.hash(result, this.anexoBq11C1102);
        result = HashCodeUtil.hash(result, this.anexoBq11C1103);
        result = HashCodeUtil.hash(result, this.anexoBq11C1104);
        result = HashCodeUtil.hash(result, this.anexoBq11C1105);
        result = HashCodeUtil.hash(result, this.anexoBq11C1106);
        result = HashCodeUtil.hash(result, this.anexoBq11C1107);
        result = HashCodeUtil.hash(result, this.anexoBq11C1108);
        result = HashCodeUtil.hash(result, this.anexoBq11C1109);
        return result;
    }
}

