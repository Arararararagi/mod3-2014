/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01Base;

public class Quadro01
extends Quadro01Base {
    public boolean isAnexoBq01B1OP1Selected() {
        return this.getAnexoBq01B1() == null ? false : this.getAnexoBq01B1().equals("1");
    }

    public boolean isAnexoBq01B1OP2Selected() {
        return this.getAnexoBq01B1() == null ? false : this.getAnexoBq01B1().equals("2");
    }

    public boolean isAnexoBq01B3Selected() {
        return this.getAnexoBq01B3() == null ? false : this.getAnexoBq01B3();
    }

    public boolean isAnexoBq01B4Selected() {
        return this.getAnexoBq01B4() == null ? false : this.getAnexoBq01B4();
    }
}

