/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoB.qQuadro07";
    public static final String ANEXOBQ07C701_LINK = "aAnexoB.qQuadro07.fanexoBq07C701";
    public static final String ANEXOBQ07C701 = "anexoBq07C701";
    private Long anexoBq07C701;
    public static final String ANEXOBQ07C702_LINK = "aAnexoB.qQuadro07.fanexoBq07C702";
    public static final String ANEXOBQ07C702 = "anexoBq07C702";
    private Long anexoBq07C702;
    public static final String ANEXOBQ07C703_LINK = "aAnexoB.qQuadro07.fanexoBq07C703";
    public static final String ANEXOBQ07C703 = "anexoBq07C703";
    private Long anexoBq07C703;
    public static final String ANEXOBQ07C704_LINK = "aAnexoB.qQuadro07.fanexoBq07C704";
    public static final String ANEXOBQ07C704 = "anexoBq07C704";
    private Long anexoBq07C704;
    public static final String ANEXOBQ07T1_LINK = "aAnexoB.qQuadro07.tanexoBq07T1";
    private EventList<AnexoBq07T1_Linha> anexoBq07T1 = new BasicEventList<AnexoBq07T1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq07C701() {
        return this.anexoBq07C701;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq07C701(Long anexoBq07C701) {
        Long oldValue = this.anexoBq07C701;
        this.anexoBq07C701 = anexoBq07C701;
        this.firePropertyChange("anexoBq07C701", oldValue, this.anexoBq07C701);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq07C702() {
        return this.anexoBq07C702;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq07C702(Long anexoBq07C702) {
        Long oldValue = this.anexoBq07C702;
        this.anexoBq07C702 = anexoBq07C702;
        this.firePropertyChange("anexoBq07C702", oldValue, this.anexoBq07C702);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq07C703() {
        return this.anexoBq07C703;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq07C703(Long anexoBq07C703) {
        Long oldValue = this.anexoBq07C703;
        this.anexoBq07C703 = anexoBq07C703;
        this.firePropertyChange("anexoBq07C703", oldValue, this.anexoBq07C703);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq07C704() {
        return this.anexoBq07C704;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq07C704(Long anexoBq07C704) {
        Long oldValue = this.anexoBq07C704;
        this.anexoBq07C704 = anexoBq07C704;
        this.firePropertyChange("anexoBq07C704", oldValue, this.anexoBq07C704);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoBq07T1_Linha> getAnexoBq07T1() {
        return this.anexoBq07T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoBq07T1(EventList<AnexoBq07T1_Linha> anexoBq07T1) {
        this.anexoBq07T1 = anexoBq07T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq07C701);
        result = HashCodeUtil.hash(result, this.anexoBq07C702);
        result = HashCodeUtil.hash(result, this.anexoBq07C703);
        result = HashCodeUtil.hash(result, this.anexoBq07C704);
        result = HashCodeUtil.hash(result, this.anexoBq07T1);
        return result;
    }
}

