/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.Quadro09Base;

public class Quadro09
extends Quadro09Base {
    public long getSomaC1a() {
        long sum = 0;
        sum+=this.getAnexoBq09C901() != null ? this.getAnexoBq09C901() : 0;
        sum+=this.getAnexoBq09C902() != null ? this.getAnexoBq09C902() : 0;
        sum+=this.getAnexoBq09C903() != null ? this.getAnexoBq09C903() : 0;
        sum+=this.getAnexoBq09C904() != null ? this.getAnexoBq09C904() : 0;
        sum+=this.getAnexoBq09C905() != null ? this.getAnexoBq09C905() : 0;
        sum+=this.getAnexoBq09C906() != null ? this.getAnexoBq09C906() : 0;
        sum+=this.getAnexoBq09C907() != null ? this.getAnexoBq09C907() : 0;
        return sum+=this.getAnexoBq09C908() != null ? this.getAnexoBq09C908() : 0;
    }

    public long getSomaC1b() {
        long sum = 0;
        sum+=this.getAnexoBq09C910() != null ? this.getAnexoBq09C910() : 0;
        sum+=this.getAnexoBq09C911() != null ? this.getAnexoBq09C911() : 0;
        sum+=this.getAnexoBq09C912() != null ? this.getAnexoBq09C912() : 0;
        sum+=this.getAnexoBq09C913() != null ? this.getAnexoBq09C913() : 0;
        sum+=this.getAnexoBq09C914() != null ? this.getAnexoBq09C914() : 0;
        sum+=this.getAnexoBq09C915() != null ? this.getAnexoBq09C915() : 0;
        sum+=this.getAnexoBq09C916() != null ? this.getAnexoBq09C916() : 0;
        return sum+=this.getAnexoBq09C917() != null ? this.getAnexoBq09C917() : 0;
    }

    public boolean isEmpty() {
        return this.getAnexoBq09C1a() == null && this.getAnexoBq09C1b() == null && this.getAnexoBq09C901() == null && this.getAnexoBq09C902() == null && this.getAnexoBq09C903() == null && this.getAnexoBq09C904() == null && this.getAnexoBq09C904() == null && this.getAnexoBq09C905() == null && this.getAnexoBq09C906() == null && this.getAnexoBq09C907() == null && this.getAnexoBq09C908() == null && this.getAnexoBq09C910() == null && this.getAnexoBq09C911() == null && this.getAnexoBq09C911() == null && this.getAnexoBq09C912() == null && this.getAnexoBq09C913() == null && this.getAnexoBq09C914() == null && this.getAnexoBq09C915() == null && this.getAnexoBq09C916() == null && this.getAnexoBq09C917() == null;
    }
}

