/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro01Base
extends QuadroModel {
    public static final String QUADRO01_LINK = "aAnexoB.qQuadro01";
    public static final String ANEXOBQ01B1OP1_LINK = "aAnexoB.qQuadro01.fanexoBq01B1OP1";
    public static final String ANEXOBQ01B1OP1_VALUE = "1";
    public static final String ANEXOBQ01B1OP2_LINK = "aAnexoB.qQuadro01.fanexoBq01B1OP2";
    public static final String ANEXOBQ01B1OP2_VALUE = "2";
    public static final String ANEXOBQ01B1 = "anexoBq01B1";
    private String anexoBq01B1;
    public static final String ANEXOBQ01B3_LINK = "aAnexoB.qQuadro01.fanexoBq01B3";
    public static final String ANEXOBQ01B3 = "anexoBq01B3";
    private Boolean anexoBq01B3;
    public static final String ANEXOBQ01B4_LINK = "aAnexoB.qQuadro01.fanexoBq01B4";
    public static final String ANEXOBQ01B4 = "anexoBq01B4";
    private Boolean anexoBq01B4;

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoBq01B1() {
        return this.anexoBq01B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq01B1(String anexoBq01B1) {
        String oldValue = this.anexoBq01B1;
        this.anexoBq01B1 = anexoBq01B1;
        this.firePropertyChange("anexoBq01B1", oldValue, this.anexoBq01B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoBq01B3() {
        return this.anexoBq01B3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq01B3(Boolean anexoBq01B3) {
        Boolean oldValue = this.anexoBq01B3;
        this.anexoBq01B3 = anexoBq01B3;
        this.firePropertyChange("anexoBq01B3", oldValue, this.anexoBq01B3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoBq01B4() {
        return this.anexoBq01B4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq01B4(Boolean anexoBq01B4) {
        Boolean oldValue = this.anexoBq01B4;
        this.anexoBq01B4 = anexoBq01B4;
        this.firePropertyChange("anexoBq01B4", oldValue, this.anexoBq01B4);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq01B1);
        result = HashCodeUtil.hash(result, this.anexoBq01B3);
        result = HashCodeUtil.hash(result, this.anexoBq01B4);
        return result;
    }
}

