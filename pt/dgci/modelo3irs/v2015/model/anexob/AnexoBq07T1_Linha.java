/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_LinhaBase;

public class AnexoBq07T1_Linha
extends AnexoBq07T1_LinhaBase {
    public AnexoBq07T1_Linha() {
    }

    public AnexoBq07T1_Linha(Long nIF, Long valor) {
        super(nIF, valor);
    }

    public static String getLink(int numLinha) {
        return "aAnexoB.qQuadro07.tanexoBq07T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoBq07T1_LinhaBase.Property column) {
        return AnexoBq07T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

