/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoBq04T1_LinhaBase
extends Model {
    public static final String FREGUESIA_LINK = "aAnexoB.qQuadro04.ffreguesia";
    public static final String FREGUESIA = "freguesia";
    private String freguesia;
    public static final String TIPOPREDIO_LINK = "aAnexoB.qQuadro04.ftipoPredio";
    public static final String TIPOPREDIO = "tipoPredio";
    private String tipoPredio;
    public static final String ARTIGO_LINK = "aAnexoB.qQuadro04.fartigo";
    public static final String ARTIGO = "artigo";
    private Long artigo;
    public static final String FRACCAO_LINK = "aAnexoB.qQuadro04.ffraccao";
    public static final String FRACCAO = "fraccao";
    private String fraccao;
    public static final String VALORVENDA_LINK = "aAnexoB.qQuadro04.fvalorVenda";
    public static final String VALORVENDA = "valorVenda";
    private Long valorVenda;
    public static final String CAMPOQ4_LINK = "aAnexoB.qQuadro04.fcampoQ4";
    public static final String CAMPOQ4 = "campoQ4";
    private Long campoQ4;
    public static final String VALORDEFINITIVO_LINK = "aAnexoB.qQuadro04.fvalorDefinitivo";
    public static final String VALORDEFINITIVO = "valorDefinitivo";
    private Long valorDefinitivo;
    public static final String ART139CIRC_LINK = "aAnexoB.qQuadro04.fart139CIRC";
    public static final String ART139CIRC = "art139CIRC";
    private String art139CIRC;

    public AnexoBq04T1_LinhaBase(String freguesia, String tipoPredio, Long artigo, String fraccao, Long valorVenda, Long campoQ4, Long valorDefinitivo, String art139CIRC) {
        this.freguesia = freguesia;
        this.tipoPredio = tipoPredio;
        this.artigo = artigo;
        this.fraccao = fraccao;
        this.valorVenda = valorVenda;
        this.campoQ4 = campoQ4;
        this.valorDefinitivo = valorDefinitivo;
        this.art139CIRC = art139CIRC;
    }

    public AnexoBq04T1_LinhaBase() {
        this.freguesia = null;
        this.tipoPredio = null;
        this.artigo = null;
        this.fraccao = null;
        this.valorVenda = null;
        this.campoQ4 = null;
        this.valorDefinitivo = null;
        this.art139CIRC = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFreguesia() {
        return this.freguesia;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFreguesia(String freguesia) {
        String oldValue = this.freguesia;
        this.freguesia = freguesia;
        this.firePropertyChange("freguesia", oldValue, this.freguesia);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio")
    public String getTipoPredio() {
        return this.tipoPredio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTipoPredio(String tipoPredio) {
        String oldValue = this.tipoPredio;
        this.tipoPredio = tipoPredio;
        this.firePropertyChange("tipoPredio", oldValue, this.tipoPredio);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getArtigo() {
        return this.artigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setArtigo(Long artigo) {
        Long oldValue = this.artigo;
        this.artigo = artigo;
        this.firePropertyChange("artigo", oldValue, this.artigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFraccao() {
        return this.fraccao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFraccao(String fraccao) {
        String oldValue = this.fraccao;
        this.fraccao = fraccao;
        this.firePropertyChange("fraccao", oldValue, this.fraccao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorVenda() {
        return this.valorVenda;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorVenda(Long valorVenda) {
        Long oldValue = this.valorVenda;
        this.valorVenda = valorVenda;
        this.firePropertyChange("valorVenda", oldValue, this.valorVenda);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoBQ4")
    public Long getCampoQ4() {
        return this.campoQ4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoQ4(Long campoQ4) {
        Long oldValue = this.campoQ4;
        this.campoQ4 = campoQ4;
        this.firePropertyChange("campoQ4", oldValue, this.campoQ4);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorDefinitivo() {
        return this.valorDefinitivo;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorDefinitivo(Long valorDefinitivo) {
        Long oldValue = this.valorDefinitivo;
        this.valorDefinitivo = valorDefinitivo;
        this.firePropertyChange("valorDefinitivo", oldValue, this.valorDefinitivo);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_SimNao")
    public String getArt139CIRC() {
        return this.art139CIRC;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setArt139CIRC(String art139CIRC) {
        String oldValue = this.art139CIRC;
        this.art139CIRC = art139CIRC;
        this.firePropertyChange("art139CIRC", oldValue, this.art139CIRC);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.freguesia);
        result = HashCodeUtil.hash(result, this.tipoPredio);
        result = HashCodeUtil.hash(result, this.artigo);
        result = HashCodeUtil.hash(result, this.fraccao);
        result = HashCodeUtil.hash(result, this.valorVenda);
        result = HashCodeUtil.hash(result, this.campoQ4);
        result = HashCodeUtil.hash(result, this.valorDefinitivo);
        result = HashCodeUtil.hash(result, this.art139CIRC);
        return result;
    }

    public static enum Property {
        FREGUESIA("freguesia", 2),
        TIPOPREDIO("tipoPredio", 3),
        ARTIGO("artigo", 4),
        FRACCAO("fraccao", 5),
        VALORVENDA("valorVenda", 6),
        CAMPOQ4("campoQ4", 7),
        VALORDEFINITIVO("valorDefinitivo", 8),
        ART139CIRC("art139CIRC", 9);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

