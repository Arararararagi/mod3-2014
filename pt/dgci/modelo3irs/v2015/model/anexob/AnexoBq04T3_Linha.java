/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class AnexoBq04T3_Linha
extends AnexoBq04T3_LinhaBase {
    public AnexoBq04T3_Linha() {
    }

    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getNIPCdasEntidades()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getSubsidioDetinadoaExploracao()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getSubsidiosNaoDestinadosExploracaoN()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getSubsidiosNaoDestinadosExploracaoN1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getSubsidiosNaoDestinadosExploracaoN2()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getSubsidiosNaoDestinadosExploracaoN3()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getSubsidiosNaoDestinadosExploracaoN4());
    }

    public AnexoBq04T3_Linha(Long nIPCdasEntidades, Long subsidioDestinadoaExploracao, Long subsidiosNaoDestinadosExploracaoN, Long subsidiosNaoDestinadosExploracaoN1, Long subsidiosNaoDestinadosExploracaoN2, Long subsidiosNaoDestinadosExploracaoN3, Long subsidiosNaoDestinadosExploracaoN4) {
        super(nIPCdasEntidades, subsidioDestinadoaExploracao, subsidiosNaoDestinadosExploracaoN, subsidiosNaoDestinadosExploracaoN1, subsidiosNaoDestinadosExploracaoN2, subsidiosNaoDestinadosExploracaoN3, subsidiosNaoDestinadosExploracaoN4);
    }

    public static String getLink(int numLinha) {
        return "aAnexoB.qQuadro04.tanexoBq04T3.l" + numLinha;
    }
}

