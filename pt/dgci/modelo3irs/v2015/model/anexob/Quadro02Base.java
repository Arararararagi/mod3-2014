/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoB.qQuadro02";
    public static final String ANEXOBQ02C05_LINK = "aAnexoB.qQuadro02.fanexoBq02C05";
    public static final String ANEXOBQ02C05 = "anexoBq02C05";
    private Long anexoBq02C05;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoBq02C05() {
        return this.anexoBq02C05;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq02C05(Long anexoBq02C05) {
        Long oldValue = this.anexoBq02C05;
        this.anexoBq02C05 = anexoBq02C05;
        this.firePropertyChange("anexoBq02C05", oldValue, this.anexoBq02C05);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq02C05);
        return result;
    }
}

