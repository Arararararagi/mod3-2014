/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq07T1_Linha;

public class AnexoBq07T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoBq07T1_Linha>,
WritableTableFormat<AnexoBq07T1_Linha> {
    @Override
    public boolean isEditable(AnexoBq07T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoBq07T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNIF();
            }
            case 2: {
                return line.getValor();
            }
        }
        return null;
    }

    @Override
    public AnexoBq07T1_Linha setColumnValue(AnexoBq07T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNIF((Long)value);
                return line;
            }
            case 2: {
                line.setValor((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF";
            }
            case 2: {
                return "Valor";
            }
        }
        return null;
    }
}

