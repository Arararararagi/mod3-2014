/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro08Base
extends QuadroModel {
    public static final String QUADRO08_LINK = "aAnexoB.qQuadro08";
    public static final String ANEXOBQ08C801_LINK = "aAnexoB.qQuadro08.fanexoBq08C801";
    public static final String ANEXOBQ08C801 = "anexoBq08C801";
    private Long anexoBq08C801;
    public static final String ANEXOBQ08C802_LINK = "aAnexoB.qQuadro08.fanexoBq08C802";
    public static final String ANEXOBQ08C802 = "anexoBq08C802";
    private Long anexoBq08C802;
    public static final String ANEXOBQ08C803_LINK = "aAnexoB.qQuadro08.fanexoBq08C803";
    public static final String ANEXOBQ08C803 = "anexoBq08C803";
    private Long anexoBq08C803;
    public static final String ANEXOBQ08C804_LINK = "aAnexoB.qQuadro08.fanexoBq08C804";
    public static final String ANEXOBQ08C804 = "anexoBq08C804";
    private Long anexoBq08C804;
    public static final String ANEXOBQ08C805_LINK = "aAnexoB.qQuadro08.fanexoBq08C805";
    public static final String ANEXOBQ08C805 = "anexoBq08C805";
    private Long anexoBq08C805;
    public static final String ANEXOBQ08C806_LINK = "aAnexoB.qQuadro08.fanexoBq08C806";
    public static final String ANEXOBQ08C806 = "anexoBq08C806";
    private Long anexoBq08C806;
    public static final String ANEXOBQ08C807_LINK = "aAnexoB.qQuadro08.fanexoBq08C807";
    public static final String ANEXOBQ08C807 = "anexoBq08C807";
    private Long anexoBq08C807;
    public static final String ANEXOBQ08C808_LINK = "aAnexoB.qQuadro08.fanexoBq08C808";
    public static final String ANEXOBQ08C808 = "anexoBq08C808";
    private Long anexoBq08C808;
    public static final String ANEXOBQ08C809_LINK = "aAnexoB.qQuadro08.fanexoBq08C809";
    public static final String ANEXOBQ08C809 = "anexoBq08C809";
    private Long anexoBq08C809;
    public static final String ANEXOBQ08C810_LINK = "aAnexoB.qQuadro08.fanexoBq08C810";
    public static final String ANEXOBQ08C810 = "anexoBq08C810";
    private Long anexoBq08C810;
    public static final String ANEXOBQ08C811_LINK = "aAnexoB.qQuadro08.fanexoBq08C811";
    public static final String ANEXOBQ08C811 = "anexoBq08C811";
    private Long anexoBq08C811;
    public static final String ANEXOBQ08C812_LINK = "aAnexoB.qQuadro08.fanexoBq08C812";
    public static final String ANEXOBQ08C812 = "anexoBq08C812";
    private Long anexoBq08C812;
    public static final String ANEXOBQ08C813_LINK = "aAnexoB.qQuadro08.fanexoBq08C813";
    public static final String ANEXOBQ08C813 = "anexoBq08C813";
    private Long anexoBq08C813;
    public static final String ANEXOBQ08C814_LINK = "aAnexoB.qQuadro08.fanexoBq08C814";
    public static final String ANEXOBQ08C814 = "anexoBq08C814";
    private Long anexoBq08C814;
    public static final String ANEXOBQ08C815_LINK = "aAnexoB.qQuadro08.fanexoBq08C815";
    public static final String ANEXOBQ08C815 = "anexoBq08C815";
    private Long anexoBq08C815;
    public static final String ANEXOBQ08C816_LINK = "aAnexoB.qQuadro08.fanexoBq08C816";
    public static final String ANEXOBQ08C816 = "anexoBq08C816";
    private Long anexoBq08C816;
    public static final String ANEXOBQ08C817_LINK = "aAnexoB.qQuadro08.fanexoBq08C817";
    public static final String ANEXOBQ08C817 = "anexoBq08C817";
    private Long anexoBq08C817;
    public static final String ANEXOBQ08C818_LINK = "aAnexoB.qQuadro08.fanexoBq08C818";
    public static final String ANEXOBQ08C818 = "anexoBq08C818";
    private Long anexoBq08C818;
    public static final String ANEXOBQ08C819_LINK = "aAnexoB.qQuadro08.fanexoBq08C819";
    public static final String ANEXOBQ08C819 = "anexoBq08C819";
    private Long anexoBq08C819;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq08C801() {
        return this.anexoBq08C801;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq08C801(Long anexoBq08C801) {
        Long oldValue = this.anexoBq08C801;
        this.anexoBq08C801 = anexoBq08C801;
        this.firePropertyChange("anexoBq08C801", oldValue, this.anexoBq08C801);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq08C802() {
        return this.anexoBq08C802;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq08C802(Long anexoBq08C802) {
        Long oldValue = this.anexoBq08C802;
        this.anexoBq08C802 = anexoBq08C802;
        this.firePropertyChange("anexoBq08C802", oldValue, this.anexoBq08C802);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq08C803() {
        return this.anexoBq08C803;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq08C803(Long anexoBq08C803) {
        Long oldValue = this.anexoBq08C803;
        this.anexoBq08C803 = anexoBq08C803;
        this.firePropertyChange("anexoBq08C803", oldValue, this.anexoBq08C803);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq08C804() {
        return this.anexoBq08C804;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq08C804(Long anexoBq08C804) {
        Long oldValue = this.anexoBq08C804;
        this.anexoBq08C804 = anexoBq08C804;
        this.firePropertyChange("anexoBq08C804", oldValue, this.anexoBq08C804);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq08C805() {
        return this.anexoBq08C805;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq08C805(Long anexoBq08C805) {
        Long oldValue = this.anexoBq08C805;
        this.anexoBq08C805 = anexoBq08C805;
        this.firePropertyChange("anexoBq08C805", oldValue, this.anexoBq08C805);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq08C806() {
        return this.anexoBq08C806;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq08C806(Long anexoBq08C806) {
        Long oldValue = this.anexoBq08C806;
        this.anexoBq08C806 = anexoBq08C806;
        this.firePropertyChange("anexoBq08C806", oldValue, this.anexoBq08C806);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoBq08C807() {
        return this.anexoBq08C807;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq08C807(Long anexoBq08C807) {
        Long oldValue = this.anexoBq08C807;
        this.anexoBq08C807 = anexoBq08C807;
        this.firePropertyChange("anexoBq08C807", oldValue, this.anexoBq08C807);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C808() {
        return this.anexoBq08C808;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C808(Long anexoBq08C808) {
        Long oldValue = this.anexoBq08C808;
        this.anexoBq08C808 = anexoBq08C808;
        this.firePropertyChange("anexoBq08C808", oldValue, this.anexoBq08C808);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C809() {
        return this.anexoBq08C809;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C809(Long anexoBq08C809) {
        Long oldValue = this.anexoBq08C809;
        this.anexoBq08C809 = anexoBq08C809;
        this.firePropertyChange("anexoBq08C809", oldValue, this.anexoBq08C809);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C810() {
        return this.anexoBq08C810;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C810(Long anexoBq08C810) {
        Long oldValue = this.anexoBq08C810;
        this.anexoBq08C810 = anexoBq08C810;
        this.firePropertyChange("anexoBq08C810", oldValue, this.anexoBq08C810);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C811() {
        return this.anexoBq08C811;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C811(Long anexoBq08C811) {
        Long oldValue = this.anexoBq08C811;
        this.anexoBq08C811 = anexoBq08C811;
        this.firePropertyChange("anexoBq08C811", oldValue, this.anexoBq08C811);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C812() {
        return this.anexoBq08C812;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C812(Long anexoBq08C812) {
        Long oldValue = this.anexoBq08C812;
        this.anexoBq08C812 = anexoBq08C812;
        this.firePropertyChange("anexoBq08C812", oldValue, this.anexoBq08C812);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C813() {
        return this.anexoBq08C813;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C813(Long anexoBq08C813) {
        Long oldValue = this.anexoBq08C813;
        this.anexoBq08C813 = anexoBq08C813;
        this.firePropertyChange("anexoBq08C813", oldValue, this.anexoBq08C813);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C814() {
        return this.anexoBq08C814;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C814(Long anexoBq08C814) {
        Long oldValue = this.anexoBq08C814;
        this.anexoBq08C814 = anexoBq08C814;
        this.firePropertyChange("anexoBq08C814", oldValue, this.anexoBq08C814);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C815() {
        return this.anexoBq08C815;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C815(Long anexoBq08C815) {
        Long oldValue = this.anexoBq08C815;
        this.anexoBq08C815 = anexoBq08C815;
        this.firePropertyChange("anexoBq08C815", oldValue, this.anexoBq08C815);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C816() {
        return this.anexoBq08C816;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C816(Long anexoBq08C816) {
        Long oldValue = this.anexoBq08C816;
        this.anexoBq08C816 = anexoBq08C816;
        this.firePropertyChange("anexoBq08C816", oldValue, this.anexoBq08C816);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C817() {
        return this.anexoBq08C817;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C817(Long anexoBq08C817) {
        Long oldValue = this.anexoBq08C817;
        this.anexoBq08C817 = anexoBq08C817;
        this.firePropertyChange("anexoBq08C817", oldValue, this.anexoBq08C817);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C818() {
        return this.anexoBq08C818;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C818(Long anexoBq08C818) {
        Long oldValue = this.anexoBq08C818;
        this.anexoBq08C818 = anexoBq08C818;
        this.firePropertyChange("anexoBq08C818", oldValue, this.anexoBq08C818);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq08C819() {
        return this.anexoBq08C819;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq08C819(Long anexoBq08C819) {
        Long oldValue = this.anexoBq08C819;
        this.anexoBq08C819 = anexoBq08C819;
        this.firePropertyChange("anexoBq08C819", oldValue, this.anexoBq08C819);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq08C801);
        result = HashCodeUtil.hash(result, this.anexoBq08C802);
        result = HashCodeUtil.hash(result, this.anexoBq08C803);
        result = HashCodeUtil.hash(result, this.anexoBq08C804);
        result = HashCodeUtil.hash(result, this.anexoBq08C805);
        result = HashCodeUtil.hash(result, this.anexoBq08C806);
        result = HashCodeUtil.hash(result, this.anexoBq08C807);
        result = HashCodeUtil.hash(result, this.anexoBq08C808);
        result = HashCodeUtil.hash(result, this.anexoBq08C809);
        result = HashCodeUtil.hash(result, this.anexoBq08C810);
        result = HashCodeUtil.hash(result, this.anexoBq08C811);
        result = HashCodeUtil.hash(result, this.anexoBq08C812);
        result = HashCodeUtil.hash(result, this.anexoBq08C813);
        result = HashCodeUtil.hash(result, this.anexoBq08C814);
        result = HashCodeUtil.hash(result, this.anexoBq08C815);
        result = HashCodeUtil.hash(result, this.anexoBq08C816);
        result = HashCodeUtil.hash(result, this.anexoBq08C817);
        result = HashCodeUtil.hash(result, this.anexoBq08C818);
        result = HashCodeUtil.hash(result, this.anexoBq08C819);
        return result;
    }
}

