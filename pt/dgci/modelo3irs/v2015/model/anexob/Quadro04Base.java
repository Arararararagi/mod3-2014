/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoB.qQuadro04";
    public static final String ANEXOBQ04C401_LINK = "aAnexoB.qQuadro04.fanexoBq04C401";
    public static final String ANEXOBQ04C401 = "anexoBq04C401";
    private Long anexoBq04C401;
    public static final String ANEXOBQ04C402_LINK = "aAnexoB.qQuadro04.fanexoBq04C402";
    public static final String ANEXOBQ04C402 = "anexoBq04C402";
    private Long anexoBq04C402;
    public static final String ANEXOBQ04C403_LINK = "aAnexoB.qQuadro04.fanexoBq04C403";
    public static final String ANEXOBQ04C403 = "anexoBq04C403";
    private Long anexoBq04C403;
    public static final String ANEXOBQ04C440_LINK = "aAnexoB.qQuadro04.fanexoBq04C440";
    public static final String ANEXOBQ04C440 = "anexoBq04C440";
    private Long anexoBq04C440;
    public static final String ANEXOBQ04C441_LINK = "aAnexoB.qQuadro04.fanexoBq04C441";
    public static final String ANEXOBQ04C441 = "anexoBq04C441";
    private Long anexoBq04C441;
    public static final String ANEXOBQ04C404_LINK = "aAnexoB.qQuadro04.fanexoBq04C404";
    public static final String ANEXOBQ04C404 = "anexoBq04C404";
    private Long anexoBq04C404;
    public static final String ANEXOBQ04C442_LINK = "aAnexoB.qQuadro04.fanexoBq04C442";
    public static final String ANEXOBQ04C442 = "anexoBq04C442";
    private Long anexoBq04C442;
    public static final String ANEXOBQ04C405_LINK = "aAnexoB.qQuadro04.fanexoBq04C405";
    public static final String ANEXOBQ04C405 = "anexoBq04C405";
    private Long anexoBq04C405;
    public static final String ANEXOBQ04C420_LINK = "aAnexoB.qQuadro04.fanexoBq04C420";
    public static final String ANEXOBQ04C420 = "anexoBq04C420";
    private Long anexoBq04C420;
    public static final String ANEXOBQ04C421_LINK = "aAnexoB.qQuadro04.fanexoBq04C421";
    public static final String ANEXOBQ04C421 = "anexoBq04C421";
    private Long anexoBq04C421;
    public static final String ANEXOBQ04C422_LINK = "aAnexoB.qQuadro04.fanexoBq04C422";
    public static final String ANEXOBQ04C422 = "anexoBq04C422";
    private Long anexoBq04C422;
    public static final String ANEXOBQ04C423_LINK = "aAnexoB.qQuadro04.fanexoBq04C423";
    public static final String ANEXOBQ04C423 = "anexoBq04C423";
    private Long anexoBq04C423;
    public static final String ANEXOBQ04C424_LINK = "aAnexoB.qQuadro04.fanexoBq04C424";
    public static final String ANEXOBQ04C424 = "anexoBq04C424";
    private Long anexoBq04C424;
    public static final String ANEXOBQ04C425_LINK = "aAnexoB.qQuadro04.fanexoBq04C425";
    public static final String ANEXOBQ04C425 = "anexoBq04C425";
    private Long anexoBq04C425;
    public static final String ANEXOBQ04C443_LINK = "aAnexoB.qQuadro04.fanexoBq04C443";
    public static final String ANEXOBQ04C443 = "anexoBq04C443";
    private Long anexoBq04C443;
    public static final String ANEXOBQ04C1_LINK = "aAnexoB.qQuadro04.fanexoBq04C1";
    public static final String ANEXOBQ04C1 = "anexoBq04C1";
    private Long anexoBq04C1;
    public static final String ANEXOBQ04C406_LINK = "aAnexoB.qQuadro04.fanexoBq04C406";
    public static final String ANEXOBQ04C406 = "anexoBq04C406";
    private Long anexoBq04C406;
    public static final String ANEXOBQ04C407_LINK = "aAnexoB.qQuadro04.fanexoBq04C407";
    public static final String ANEXOBQ04C407 = "anexoBq04C407";
    private Long anexoBq04C407;
    public static final String ANEXOBQ04C2_LINK = "aAnexoB.qQuadro04.fanexoBq04C2";
    public static final String ANEXOBQ04C2 = "anexoBq04C2";
    private Long anexoBq04C2;
    public static final String ANEXOBQ04C409_LINK = "aAnexoB.qQuadro04.fanexoBq04C409";
    public static final String ANEXOBQ04C409 = "anexoBq04C409";
    private Long anexoBq04C409;
    public static final String ANEXOBQ04C410_LINK = "aAnexoB.qQuadro04.fanexoBq04C410";
    public static final String ANEXOBQ04C410 = "anexoBq04C410";
    private Long anexoBq04C410;
    public static final String ANEXOBQ04C444_LINK = "aAnexoB.qQuadro04.fanexoBq04C444";
    public static final String ANEXOBQ04C444 = "anexoBq04C444";
    private Long anexoBq04C444;
    public static final String ANEXOBQ04C445_LINK = "aAnexoB.qQuadro04.fanexoBq04C445";
    public static final String ANEXOBQ04C445 = "anexoBq04C445";
    private Long anexoBq04C445;
    public static final String ANEXOBQ04C411_LINK = "aAnexoB.qQuadro04.fanexoBq04C411";
    public static final String ANEXOBQ04C411 = "anexoBq04C411";
    private Long anexoBq04C411;
    public static final String ANEXOBQ04C426_LINK = "aAnexoB.qQuadro04.fanexoBq04C426";
    public static final String ANEXOBQ04C426 = "anexoBq04C426";
    private Long anexoBq04C426;
    public static final String ANEXOBQ04C446_LINK = "aAnexoB.qQuadro04.fanexoBq04C446";
    public static final String ANEXOBQ04C446 = "anexoBq04C446";
    private Long anexoBq04C446;
    public static final String ANEXOBQ04C3_LINK = "aAnexoB.qQuadro04.fanexoBq04C3";
    public static final String ANEXOBQ04C3 = "anexoBq04C3";
    private Long anexoBq04C3;
    public static final String ANEXOBQ04C413_LINK = "aAnexoB.qQuadro04.fanexoBq04C413";
    public static final String ANEXOBQ04C413 = "anexoBq04C413";
    private Long anexoBq04C413;
    public static final String ANEXOBQ04C414_LINK = "aAnexoB.qQuadro04.fanexoBq04C414";
    public static final String ANEXOBQ04C414 = "anexoBq04C414";
    private Long anexoBq04C414;
    public static final String ANEXOBQ04C4_LINK = "aAnexoB.qQuadro04.fanexoBq04C4";
    public static final String ANEXOBQ04C4 = "anexoBq04C4";
    private Long anexoBq04C4;
    public static final String ANEXOBQ04B1OP1_LINK = "aAnexoB.qQuadro04.fanexoBq04B1OP1";
    public static final String ANEXOBQ04B1OP1_VALUE = "1";
    public static final String ANEXOBQ04B1OP2_LINK = "aAnexoB.qQuadro04.fanexoBq04B1OP2";
    public static final String ANEXOBQ04B1OP2_VALUE = "2";
    public static final String ANEXOBQ04B1 = "anexoBq04B1";
    private String anexoBq04B1;
    public static final String ANEXOBQ04B2OP3_LINK = "aAnexoB.qQuadro04.fanexoBq04B2OP3";
    public static final String ANEXOBQ04B2OP3_VALUE = "3";
    public static final String ANEXOBQ04B2OP4_LINK = "aAnexoB.qQuadro04.fanexoBq04B2OP4";
    public static final String ANEXOBQ04B2OP4_VALUE = "4";
    public static final String ANEXOBQ04B2 = "anexoBq04B2";
    private String anexoBq04B2;
    public static final String ANEXOBQ04B3OP1_LINK = "aAnexoB.qQuadro04.fanexoBq04B3OP1";
    public static final String ANEXOBQ04B3OP1_VALUE = "1";
    public static final String ANEXOBQ04B3OP2_LINK = "aAnexoB.qQuadro04.fanexoBq04B3OP2";
    public static final String ANEXOBQ04B3OP2_VALUE = "2";
    public static final String ANEXOBQ04B3 = "anexoBq04B3";
    private String anexoBq04B3;
    public static final String ANEXOBQ04T1_LINK = "aAnexoB.qQuadro04.tanexoBq04T1";
    private EventList<AnexoBq04T1_Linha> anexoBq04T1 = new BasicEventList<AnexoBq04T1_Linha>();
    public static final String ANEXOBQ04T3_LINK = "aAnexoB.qQuadro04.tanexoBq04T3";
    private EventList<AnexoBq04T3_Linha> anexoBq04T3 = new BasicEventList<AnexoBq04T3_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C401() {
        return this.anexoBq04C401;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C401(Long anexoBq04C401) {
        Long oldValue = this.anexoBq04C401;
        this.anexoBq04C401 = anexoBq04C401;
        this.firePropertyChange("anexoBq04C401", oldValue, this.anexoBq04C401);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C402() {
        return this.anexoBq04C402;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C402(Long anexoBq04C402) {
        Long oldValue = this.anexoBq04C402;
        this.anexoBq04C402 = anexoBq04C402;
        this.firePropertyChange("anexoBq04C402", oldValue, this.anexoBq04C402);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C403() {
        return this.anexoBq04C403;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C403(Long anexoBq04C403) {
        Long oldValue = this.anexoBq04C403;
        this.anexoBq04C403 = anexoBq04C403;
        this.firePropertyChange("anexoBq04C403", oldValue, this.anexoBq04C403);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C440() {
        return this.anexoBq04C440;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C440(Long anexoBq04C440) {
        Long oldValue = this.anexoBq04C440;
        this.anexoBq04C440 = anexoBq04C440;
        this.firePropertyChange("anexoBq04C440", oldValue, this.anexoBq04C440);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C441() {
        return this.anexoBq04C441;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C441(Long anexoBq04C441) {
        Long oldValue = this.anexoBq04C441;
        this.anexoBq04C441 = anexoBq04C441;
        this.firePropertyChange("anexoBq04C441", oldValue, this.anexoBq04C441);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C404() {
        return this.anexoBq04C404;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C404(Long anexoBq04C404) {
        Long oldValue = this.anexoBq04C404;
        this.anexoBq04C404 = anexoBq04C404;
        this.firePropertyChange("anexoBq04C404", oldValue, this.anexoBq04C404);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C442() {
        return this.anexoBq04C442;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C442(Long anexoBq04C442) {
        Long oldValue = this.anexoBq04C442;
        this.anexoBq04C442 = anexoBq04C442;
        this.firePropertyChange("anexoBq04C442", oldValue, this.anexoBq04C442);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C405() {
        return this.anexoBq04C405;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C405(Long anexoBq04C405) {
        Long oldValue = this.anexoBq04C405;
        this.anexoBq04C405 = anexoBq04C405;
        this.firePropertyChange("anexoBq04C405", oldValue, this.anexoBq04C405);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C420() {
        return this.anexoBq04C420;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C420(Long anexoBq04C420) {
        Long oldValue = this.anexoBq04C420;
        this.anexoBq04C420 = anexoBq04C420;
        this.firePropertyChange("anexoBq04C420", oldValue, this.anexoBq04C420);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C421() {
        return this.anexoBq04C421;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C421(Long anexoBq04C421) {
        Long oldValue = this.anexoBq04C421;
        this.anexoBq04C421 = anexoBq04C421;
        this.firePropertyChange("anexoBq04C421", oldValue, this.anexoBq04C421);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C422() {
        return this.anexoBq04C422;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C422(Long anexoBq04C422) {
        Long oldValue = this.anexoBq04C422;
        this.anexoBq04C422 = anexoBq04C422;
        this.firePropertyChange("anexoBq04C422", oldValue, this.anexoBq04C422);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C423() {
        return this.anexoBq04C423;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C423(Long anexoBq04C423) {
        Long oldValue = this.anexoBq04C423;
        this.anexoBq04C423 = anexoBq04C423;
        this.firePropertyChange("anexoBq04C423", oldValue, this.anexoBq04C423);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C424() {
        return this.anexoBq04C424;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C424(Long anexoBq04C424) {
        Long oldValue = this.anexoBq04C424;
        this.anexoBq04C424 = anexoBq04C424;
        this.firePropertyChange("anexoBq04C424", oldValue, this.anexoBq04C424);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C425() {
        return this.anexoBq04C425;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C425(Long anexoBq04C425) {
        Long oldValue = this.anexoBq04C425;
        this.anexoBq04C425 = anexoBq04C425;
        this.firePropertyChange("anexoBq04C425", oldValue, this.anexoBq04C425);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C443() {
        return this.anexoBq04C443;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C443(Long anexoBq04C443) {
        Long oldValue = this.anexoBq04C443;
        this.anexoBq04C443 = anexoBq04C443;
        this.firePropertyChange("anexoBq04C443", oldValue, this.anexoBq04C443);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C1() {
        return this.anexoBq04C1;
    }

    public void setAnexoBq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq04C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq04C1(newValue+=(this.getAnexoBq04C401() == null ? 0 : this.getAnexoBq04C401()) + (this.getAnexoBq04C402() == null ? 0 : this.getAnexoBq04C402()) + (this.getAnexoBq04C403() == null ? 0 : this.getAnexoBq04C403()) + (this.getAnexoBq04C404() == null ? 0 : this.getAnexoBq04C404()) + (this.getAnexoBq04C405() == null ? 0 : this.getAnexoBq04C405()) + (this.getAnexoBq04C420() == null ? 0 : this.getAnexoBq04C420()) + (this.getAnexoBq04C421() == null ? 0 : this.getAnexoBq04C421()) + (this.getAnexoBq04C422() == null ? 0 : this.getAnexoBq04C422()) + (this.getAnexoBq04C423() == null ? 0 : this.getAnexoBq04C423()) + (this.getAnexoBq04C424() == null ? 0 : this.getAnexoBq04C424()) + (this.getAnexoBq04C425() == null ? 0 : this.getAnexoBq04C425()) + (this.getAnexoBq04C440() == null ? 0 : this.getAnexoBq04C440()) + (this.getAnexoBq04C441() == null ? 0 : this.getAnexoBq04C441()) + (this.getAnexoBq04C442() == null ? 0 : this.getAnexoBq04C442()) + (this.getAnexoBq04C443() == null ? 0 : this.getAnexoBq04C443()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C1(Long anexoBq04C1) {
        Long oldValue = this.anexoBq04C1;
        this.anexoBq04C1 = anexoBq04C1;
        this.firePropertyChange("anexoBq04C1", oldValue, this.anexoBq04C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C406() {
        return this.anexoBq04C406;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C406(Long anexoBq04C406) {
        Long oldValue = this.anexoBq04C406;
        this.anexoBq04C406 = anexoBq04C406;
        this.firePropertyChange("anexoBq04C406", oldValue, this.anexoBq04C406);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C407() {
        return this.anexoBq04C407;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C407(Long anexoBq04C407) {
        Long oldValue = this.anexoBq04C407;
        this.anexoBq04C407 = anexoBq04C407;
        this.firePropertyChange("anexoBq04C407", oldValue, this.anexoBq04C407);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C2() {
        return this.anexoBq04C2;
    }

    public void setAnexoBq04C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq04C2")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq04C2(newValue+=(this.getAnexoBq04C406() == null ? 0 : this.getAnexoBq04C406()) + (this.getAnexoBq04C407() == null ? 0 : this.getAnexoBq04C407()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C2(Long anexoBq04C2) {
        Long oldValue = this.anexoBq04C2;
        this.anexoBq04C2 = anexoBq04C2;
        this.firePropertyChange("anexoBq04C2", oldValue, this.anexoBq04C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C409() {
        return this.anexoBq04C409;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C409(Long anexoBq04C409) {
        Long oldValue = this.anexoBq04C409;
        this.anexoBq04C409 = anexoBq04C409;
        this.firePropertyChange("anexoBq04C409", oldValue, this.anexoBq04C409);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C410() {
        return this.anexoBq04C410;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C410(Long anexoBq04C410) {
        Long oldValue = this.anexoBq04C410;
        this.anexoBq04C410 = anexoBq04C410;
        this.firePropertyChange("anexoBq04C410", oldValue, this.anexoBq04C410);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C444() {
        return this.anexoBq04C444;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C444(Long anexoBq04C444) {
        Long oldValue = this.anexoBq04C444;
        this.anexoBq04C444 = anexoBq04C444;
        this.firePropertyChange("anexoBq04C444", oldValue, this.anexoBq04C444);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C445() {
        return this.anexoBq04C445;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C445(Long anexoBq04C445) {
        Long oldValue = this.anexoBq04C445;
        this.anexoBq04C445 = anexoBq04C445;
        this.firePropertyChange("anexoBq04C445", oldValue, this.anexoBq04C445);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C411() {
        return this.anexoBq04C411;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C411(Long anexoBq04C411) {
        Long oldValue = this.anexoBq04C411;
        this.anexoBq04C411 = anexoBq04C411;
        this.firePropertyChange("anexoBq04C411", oldValue, this.anexoBq04C411);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C426() {
        return this.anexoBq04C426;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C426(Long anexoBq04C426) {
        Long oldValue = this.anexoBq04C426;
        this.anexoBq04C426 = anexoBq04C426;
        this.firePropertyChange("anexoBq04C426", oldValue, this.anexoBq04C426);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C446() {
        return this.anexoBq04C446;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C446(Long anexoBq04C446) {
        Long oldValue = this.anexoBq04C446;
        this.anexoBq04C446 = anexoBq04C446;
        this.firePropertyChange("anexoBq04C446", oldValue, this.anexoBq04C446);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C3() {
        return this.anexoBq04C3;
    }

    public void setAnexoBq04C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq04C3")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq04C3(newValue+=(this.getAnexoBq04C409() == null ? 0 : this.getAnexoBq04C409()) + (this.getAnexoBq04C410() == null ? 0 : this.getAnexoBq04C410()) + (this.getAnexoBq04C411() == null ? 0 : this.getAnexoBq04C411()) + (this.getAnexoBq04C426() == null ? 0 : this.getAnexoBq04C426()) + (this.getAnexoBq04C444() == null ? 0 : this.getAnexoBq04C444()) + (this.getAnexoBq04C445() == null ? 0 : this.getAnexoBq04C445()) + (this.getAnexoBq04C446() == null ? 0 : this.getAnexoBq04C446()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C3(Long anexoBq04C3) {
        Long oldValue = this.anexoBq04C3;
        this.anexoBq04C3 = anexoBq04C3;
        this.firePropertyChange("anexoBq04C3", oldValue, this.anexoBq04C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C413() {
        return this.anexoBq04C413;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C413(Long anexoBq04C413) {
        Long oldValue = this.anexoBq04C413;
        this.anexoBq04C413 = anexoBq04C413;
        this.firePropertyChange("anexoBq04C413", oldValue, this.anexoBq04C413);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C414() {
        return this.anexoBq04C414;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C414(Long anexoBq04C414) {
        Long oldValue = this.anexoBq04C414;
        this.anexoBq04C414 = anexoBq04C414;
        this.firePropertyChange("anexoBq04C414", oldValue, this.anexoBq04C414);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoBq04C4() {
        return this.anexoBq04C4;
    }

    public void setAnexoBq04C4Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoBq04C4")) {
            return;
        }
        long newValue = 0;
        this.setAnexoBq04C4(newValue+=(this.getAnexoBq04C413() == null ? 0 : this.getAnexoBq04C413()) + (this.getAnexoBq04C414() == null ? 0 : this.getAnexoBq04C414()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoBq04C4(Long anexoBq04C4) {
        Long oldValue = this.anexoBq04C4;
        this.anexoBq04C4 = anexoBq04C4;
        this.firePropertyChange("anexoBq04C4", oldValue, this.anexoBq04C4);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoBq04B1() {
        return this.anexoBq04B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq04B1(String anexoBq04B1) {
        String oldValue = this.anexoBq04B1;
        this.anexoBq04B1 = anexoBq04B1;
        this.firePropertyChange("anexoBq04B1", oldValue, this.anexoBq04B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoBq04B2() {
        return this.anexoBq04B2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq04B2(String anexoBq04B2) {
        String oldValue = this.anexoBq04B2;
        this.anexoBq04B2 = anexoBq04B2;
        this.firePropertyChange("anexoBq04B2", oldValue, this.anexoBq04B2);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoBq04B3() {
        return this.anexoBq04B3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoBq04B3(String anexoBq04B3) {
        String oldValue = this.anexoBq04B3;
        this.anexoBq04B3 = anexoBq04B3;
        this.firePropertyChange("anexoBq04B3", oldValue, this.anexoBq04B3);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoBq04T1_Linha> getAnexoBq04T1() {
        return this.anexoBq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoBq04T1(EventList<AnexoBq04T1_Linha> anexoBq04T1) {
        this.anexoBq04T1 = anexoBq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoBq04T3_Linha> getAnexoBq04T3() {
        return this.anexoBq04T3;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoBq04T3(EventList<AnexoBq04T3_Linha> anexoBq04T3) {
        this.anexoBq04T3 = anexoBq04T3;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoBq04C401);
        result = HashCodeUtil.hash(result, this.anexoBq04C402);
        result = HashCodeUtil.hash(result, this.anexoBq04C403);
        result = HashCodeUtil.hash(result, this.anexoBq04C440);
        result = HashCodeUtil.hash(result, this.anexoBq04C441);
        result = HashCodeUtil.hash(result, this.anexoBq04C404);
        result = HashCodeUtil.hash(result, this.anexoBq04C442);
        result = HashCodeUtil.hash(result, this.anexoBq04C405);
        result = HashCodeUtil.hash(result, this.anexoBq04C420);
        result = HashCodeUtil.hash(result, this.anexoBq04C421);
        result = HashCodeUtil.hash(result, this.anexoBq04C422);
        result = HashCodeUtil.hash(result, this.anexoBq04C423);
        result = HashCodeUtil.hash(result, this.anexoBq04C424);
        result = HashCodeUtil.hash(result, this.anexoBq04C425);
        result = HashCodeUtil.hash(result, this.anexoBq04C443);
        result = HashCodeUtil.hash(result, this.anexoBq04C1);
        result = HashCodeUtil.hash(result, this.anexoBq04C406);
        result = HashCodeUtil.hash(result, this.anexoBq04C407);
        result = HashCodeUtil.hash(result, this.anexoBq04C2);
        result = HashCodeUtil.hash(result, this.anexoBq04C409);
        result = HashCodeUtil.hash(result, this.anexoBq04C410);
        result = HashCodeUtil.hash(result, this.anexoBq04C444);
        result = HashCodeUtil.hash(result, this.anexoBq04C445);
        result = HashCodeUtil.hash(result, this.anexoBq04C411);
        result = HashCodeUtil.hash(result, this.anexoBq04C426);
        result = HashCodeUtil.hash(result, this.anexoBq04C446);
        result = HashCodeUtil.hash(result, this.anexoBq04C3);
        result = HashCodeUtil.hash(result, this.anexoBq04C413);
        result = HashCodeUtil.hash(result, this.anexoBq04C414);
        result = HashCodeUtil.hash(result, this.anexoBq04C4);
        result = HashCodeUtil.hash(result, this.anexoBq04B1);
        result = HashCodeUtil.hash(result, this.anexoBq04B2);
        result = HashCodeUtil.hash(result, this.anexoBq04B3);
        result = HashCodeUtil.hash(result, this.anexoBq04T1);
        result = HashCodeUtil.hash(result, this.anexoBq04T3);
        return result;
    }
}

