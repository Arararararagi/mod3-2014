/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro12Base;
import pt.opensoft.util.Date;

public class Quadro12
extends Quadro12Base {
    private AnexoBModel anexoBModel;

    public Quadro12() {
        this.setQuadroName(Quadro12.class.getSimpleName());
    }

    public void setAnexoPai(AnexoBModel anexoBModel) {
        this.anexoBModel = anexoBModel;
    }

    public AnexoBModel getAnexoPai() {
        return this.anexoBModel;
    }

    public boolean isEmpty() {
        return this.getAnexoBq12B1() == null && this.getAnexoBq12C3() == null && this.getAnexoBq12B4() == null;
    }
}

