/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoBq04T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoBq04T1_Linha>,
WritableTableFormat<AnexoBq04T1_Linha> {
    @Override
    public boolean isEditable(AnexoBq04T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoBq04T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getFreguesia();
            }
            case 2: {
                if (line == null || line.getTipoPredio() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_TipoPredio", line.getTipoPredio());
            }
            case 3: {
                return line.getArtigo();
            }
            case 4: {
                return line.getFraccao();
            }
            case 5: {
                return line.getValorVenda();
            }
            case 6: {
                if (line == null || line.getCampoQ4() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoBQ4", line.getCampoQ4());
            }
            case 7: {
                return line.getValorDefinitivo();
            }
            case 8: {
                if (line == null || line.getArt139CIRC() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_SimNao", line.getArt139CIRC());
            }
        }
        return null;
    }

    @Override
    public AnexoBq04T1_Linha setColumnValue(AnexoBq04T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setFreguesia((String)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setTipoPredio((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setArtigo((Long)value);
                return line;
            }
            case 4: {
                line.setFraccao((String)value);
                return line;
            }
            case 5: {
                line.setValorVenda((Long)value);
                return line;
            }
            case 6: {
                if (value != null) {
                    line.setCampoQ4((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 7: {
                line.setValorDefinitivo((Long)value);
                return line;
            }
            case 8: {
                if (value != null) {
                    line.setArt139CIRC((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return String.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Freguesia";
            }
            case 2: {
                return "Tipo";
            }
            case 3: {
                return "Artigo";
            }
            case 4: {
                return "Fra\u00e7\u00e3o/Sec\u00e7\u00e3o";
            }
            case 5: {
                return "Valor de Venda";
            }
            case 6: {
                return "Campo Q4";
            }
            case 7: {
                return "Valor Definitivo";
            }
            case 8: {
                return "Art.\u00ba 139.\u00ba CIRC";
            }
        }
        return null;
    }
}

