/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import pt.dgci.modelo3irs.v2015.model.anexob.Quadro03Base;
import pt.opensoft.util.StringUtil;

public class Quadro03
extends Quadro03Base {
    private static final long serialVersionUID = 3787629460847828927L;

    public boolean hasAnexoBq03C08() {
        return this.getAnexoBq03C08() != null;
    }

    public boolean isAnexoBq03B1OPSim() {
        if (!StringUtil.isEmpty(this.getAnexoBq03B1()) && this.getAnexoBq03B1().equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        return this.getAnexoBq03C08() == null && this.getAnexoBq03C09() == null && this.getAnexoBq03C10() == null && this.getAnexoBq03C11() == null && this.getAnexoBq03C12() == null && this.getAnexoBq03B13() == null && this.getAnexoBq03B1() == null;
    }
}

