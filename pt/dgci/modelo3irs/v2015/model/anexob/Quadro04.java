/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexob;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class Quadro04
extends Quadro04Base {
    private static final long serialVersionUID = 3551026579143226353L;
    private AnexoBModel anexoBModel;

    public Quadro04() {
        this.setQuadroName(Quadro04.class.getSimpleName());
    }

    public boolean isAnexoBq04B1OPSelected() {
        if (!StringUtil.isEmpty(this.getAnexoBq04B1()) && this.getAnexoBq04B1().equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isAnexoBq04B2OPSelected() {
        if (!StringUtil.isEmpty(this.getAnexoBq04B2()) && this.getAnexoBq04B2().equals("3")) {
            return true;
        }
        return false;
    }

    public Long getSumC1() {
        long val401 = this.getAnexoBq04C401() == null ? 0 : this.getAnexoBq04C401();
        long val402 = this.getAnexoBq04C402() == null ? 0 : this.getAnexoBq04C402();
        long val403 = this.getAnexoBq04C403() == null ? 0 : this.getAnexoBq04C403();
        long val404 = this.getAnexoBq04C404() == null ? 0 : this.getAnexoBq04C404();
        long val405 = this.getAnexoBq04C405() == null ? 0 : this.getAnexoBq04C405();
        long val420 = this.getAnexoBq04C420() == null ? 0 : this.getAnexoBq04C420();
        long val421 = this.getAnexoBq04C421() == null ? 0 : this.getAnexoBq04C421();
        long val422 = this.getAnexoBq04C422() == null ? 0 : this.getAnexoBq04C422();
        long val423 = this.getAnexoBq04C423() == null ? 0 : this.getAnexoBq04C423();
        long val424 = this.getAnexoBq04C424() == null ? 0 : this.getAnexoBq04C424();
        long val425 = this.getAnexoBq04C425() == null ? 0 : this.getAnexoBq04C425();
        long val440 = this.getAnexoBq04C440() == null ? 0 : this.getAnexoBq04C440();
        long val441 = this.getAnexoBq04C441() == null ? 0 : this.getAnexoBq04C441();
        long val442 = this.getAnexoBq04C442() == null ? 0 : this.getAnexoBq04C442();
        long val443 = this.getAnexoBq04C443() == null ? 0 : this.getAnexoBq04C443();
        return val401 + val402 + val403 + val404 + val405 + val420 + val421 + val422 + val423 + val424 + val425 + val440 + val441 + val442 + val443;
    }

    public Long getSumC2() {
        long val406 = this.getAnexoBq04C406() == null ? 0 : this.getAnexoBq04C406();
        long val407 = this.getAnexoBq04C407() == null ? 0 : this.getAnexoBq04C407();
        return val406 + val407;
    }

    public Long getSumC3() {
        long val409 = this.getAnexoBq04C409() == null ? 0 : this.getAnexoBq04C409();
        long val410 = this.getAnexoBq04C410() == null ? 0 : this.getAnexoBq04C410();
        long val411 = this.getAnexoBq04C411() == null ? 0 : this.getAnexoBq04C411();
        long val426 = this.getAnexoBq04C426() == null ? 0 : this.getAnexoBq04C426();
        long val444 = this.getAnexoBq04C444() == null ? 0 : this.getAnexoBq04C444();
        long val445 = this.getAnexoBq04C445() == null ? 0 : this.getAnexoBq04C445();
        long val446 = this.getAnexoBq04C446() == null ? 0 : this.getAnexoBq04C446();
        return val409 + val410 + val444 + val445 + val411 + val426 + val446;
    }

    public Long getSumC4() {
        long val413 = this.getAnexoBq04C413() == null ? 0 : this.getAnexoBq04C413();
        long val414 = this.getAnexoBq04C414() == null ? 0 : this.getAnexoBq04C414();
        return val413 + val414;
    }

    public void setAnexoPai(AnexoBModel anexoBModel) {
        this.anexoBModel = anexoBModel;
    }

    public AnexoBModel getAnexoPai() {
        return this.anexoBModel;
    }

    public Long getRendimentos() {
        return Modelo3IRSValidatorUtil.sum(this.getAnexoBq04C402(), this.getAnexoBq04C403(), this.getAnexoBq04C404(), this.getAnexoBq04C405(), this.getAnexoBq04C410(), this.getAnexoBq04C411(), this.getAnexoBq04C420(), this.getAnexoBq04C421(), this.getAnexoBq04C422(), this.getAnexoBq04C424(), this.getAnexoBq04C425(), this.getAnexoBq04C426(), this.getAnexoBq04C440(), this.getAnexoBq04C441(), this.getAnexoBq04C442(), this.getAnexoBq04C443(), this.getAnexoBq04C444(), this.getAnexoBq04C445(), this.getAnexoBq04C446());
    }

    public Long getRendimentosWith423() {
        return Modelo3IRSValidatorUtil.sum(this.getRendimentos(), this.getAnexoBq04C423());
    }

    public boolean isQuadro4AEmpty() {
        Long[] values;
        for (Long value : values = new Long[]{this.getAnexoBq04C401(), this.getAnexoBq04C402(), this.getAnexoBq04C403(), this.getAnexoBq04C404(), this.getAnexoBq04C405(), this.getAnexoBq04C406(), this.getAnexoBq04C407(), this.getAnexoBq04C420(), this.getAnexoBq04C421(), this.getAnexoBq04C422(), this.getAnexoBq04C423(), this.getAnexoBq04C424(), this.getAnexoBq04C425(), this.getAnexoBq04C426()}) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(value)) continue;
            return false;
        }
        return true;
    }

    public boolean isQuadro4BEmpty() {
        Long[] values;
        for (Long value : values = new Long[]{this.getAnexoBq04C409(), this.getAnexoBq04C410(), this.getAnexoBq04C411(), this.getAnexoBq04C413(), this.getAnexoBq04C414()}) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(value)) continue;
            return false;
        }
        return true;
    }

    public boolean isEmpty() {
        Long[] values = new Long[]{this.getAnexoBq04C401(), this.getAnexoBq04C402(), this.getAnexoBq04C403(), this.getAnexoBq04C404(), this.getAnexoBq04C405(), this.getAnexoBq04C406(), this.getAnexoBq04C407(), this.getAnexoBq04C409(), this.getAnexoBq04C410(), this.getAnexoBq04C411(), this.getAnexoBq04C413(), this.getAnexoBq04C414(), this.getAnexoBq04C420(), this.getAnexoBq04C421(), this.getAnexoBq04C422(), this.getAnexoBq04C423(), this.getAnexoBq04C424(), this.getAnexoBq04C425(), this.getAnexoBq04C426(), this.getAnexoBq04C440(), this.getAnexoBq04C441(), this.getAnexoBq04C442(), this.getAnexoBq04C443(), this.getAnexoBq04C444(), this.getAnexoBq04C445(), this.getAnexoBq04C446(), this.getAnexoBq04C1(), this.getAnexoBq04C2(), this.getAnexoBq04C3(), this.getAnexoBq04C4()};
        String[] stringValues = new String[]{this.getAnexoBq04B1(), this.getAnexoBq04B2(), this.getAnexoBq04B3()};
        EventList<AnexoBq04T1_Linha> listT1 = this.getAnexoBq04T1();
        EventList<AnexoBq04T3_Linha> listT3 = this.getAnexoBq04T3();
        for (Long value : values) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(value)) continue;
            return false;
        }
        for (Long stringValue : stringValues) {
            if (stringValue == null) continue;
            return false;
        }
        if (listT1 != null && listT1.size() > 0) {
            for (AnexoBq04T1_Linha linha : listT1) {
                if (linha.isEmpty()) continue;
                return false;
            }
        }
        if (listT3 != null && listT3.size() > 0) {
            for (AnexoBq04T3_Linha linha : listT3) {
                if (linha.isEmpty()) continue;
                return false;
            }
        }
        return true;
    }
}

