/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;
import pt.opensoft.util.Date;

public class AnexoAq04T4A_LinhaBase
extends Model {
    public static final String NIF_LINK = "aAnexoA.qQuadro04.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;
    public static final String CODRENDIMENTOS_LINK = "aAnexoA.qQuadro04.fcodRendimentos";
    public static final String CODRENDIMENTOS = "codRendimentos";
    private Long codRendimentos;
    public static final String TITULAR_LINK = "aAnexoA.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String RENDIMENTOS_LINK = "aAnexoA.qQuadro04.frendimentos";
    public static final String RENDIMENTOS = "rendimentos";
    private Long rendimentos;
    public static final String RETENCOES_LINK = "aAnexoA.qQuadro04.fretencoes";
    public static final String RETENCOES = "retencoes";
    private Long retencoes;
    public static final String CONTRIBUICOES_LINK = "aAnexoA.qQuadro04.fcontribuicoes";
    public static final String CONTRIBUICOES = "contribuicoes";
    private Long contribuicoes;
    public static final String RETSOBRETAXA_LINK = "aAnexoA.qQuadro04.fretSobretaxa";
    public static final String RETSOBRETAXA = "retSobretaxa";
    private Long retSobretaxa;
    public static final String DATACONTRATOPREREFORMA_LINK = "aAnexoA.qQuadro04.fdataContratoPreReforma";
    public static final String DATACONTRATOPREREFORMA = "dataContratoPreReforma";
    private Date dataContratoPreReforma;
    public static final String DATAPRIMEIROPAGAMENTO_LINK = "aAnexoA.qQuadro04.fdataPrimeiroPagamento";
    public static final String DATAPRIMEIROPAGAMENTO = "dataPrimeiroPagamento";
    private Date dataPrimeiroPagamento;

    public AnexoAq04T4A_LinhaBase(Long nIF, Long codRendimentos, String titular, Long rendimentos, Long retencoes, Long contribuicoes, Long retSobretaxa, Date dataContratoPreReforma, Date dataPrimeiroPagamento) {
        this.nIF = nIF;
        this.codRendimentos = codRendimentos;
        this.titular = titular;
        this.rendimentos = rendimentos;
        this.retencoes = retencoes;
        this.contribuicoes = contribuicoes;
        this.retSobretaxa = retSobretaxa;
        this.dataContratoPreReforma = dataContratoPreReforma;
        this.dataPrimeiroPagamento = dataPrimeiroPagamento;
    }

    public AnexoAq04T4A_LinhaBase() {
        this.nIF = null;
        this.codRendimentos = null;
        this.titular = null;
        this.rendimentos = null;
        this.retencoes = null;
        this.contribuicoes = null;
        this.retSobretaxa = null;
        this.dataContratoPreReforma = null;
        this.dataPrimeiroPagamento = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoAQ4A")
    public Long getCodRendimentos() {
        return this.codRendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodRendimentos(Long codRendimentos) {
        Long oldValue = this.codRendimentos;
        this.codRendimentos = codRendimentos;
        this.firePropertyChange("codRendimentos", oldValue, this.codRendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentos() {
        return this.rendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentos(Long rendimentos) {
        Long oldValue = this.rendimentos;
        this.rendimentos = rendimentos;
        this.firePropertyChange("rendimentos", oldValue, this.rendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencoes() {
        return this.retencoes;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencoes(Long retencoes) {
        Long oldValue = this.retencoes;
        this.retencoes = retencoes;
        this.firePropertyChange("retencoes", oldValue, this.retencoes);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getContribuicoes() {
        return this.contribuicoes;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setContribuicoes(Long contribuicoes) {
        Long oldValue = this.contribuicoes;
        this.contribuicoes = contribuicoes;
        this.firePropertyChange("contribuicoes", oldValue, this.contribuicoes);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetSobretaxa() {
        return this.retSobretaxa;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetSobretaxa(Long retSobretaxa) {
        Long oldValue = this.retSobretaxa;
        this.retSobretaxa = retSobretaxa;
        this.firePropertyChange("retSobretaxa", oldValue, this.retSobretaxa);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Date getDataContratoPreReforma() {
        return this.dataContratoPreReforma;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setDataContratoPreReforma(Date dataContratoPreReforma) {
        Date oldValue = this.dataContratoPreReforma;
        this.dataContratoPreReforma = dataContratoPreReforma;
        this.firePropertyChange("dataContratoPreReforma", oldValue, this.dataContratoPreReforma);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Date getDataPrimeiroPagamento() {
        return this.dataPrimeiroPagamento;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setDataPrimeiroPagamento(Date dataPrimeiroPagamento) {
        Date oldValue = this.dataPrimeiroPagamento;
        this.dataPrimeiroPagamento = dataPrimeiroPagamento;
        this.firePropertyChange("dataPrimeiroPagamento", oldValue, this.dataPrimeiroPagamento);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.codRendimentos);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.rendimentos);
        result = HashCodeUtil.hash(result, this.retencoes);
        result = HashCodeUtil.hash(result, this.contribuicoes);
        result = HashCodeUtil.hash(result, this.retSobretaxa);
        result = HashCodeUtil.hash(result, this.dataContratoPreReforma);
        result = HashCodeUtil.hash(result, this.dataPrimeiroPagamento);
        return result;
    }

    public static enum Property {
        NIF("nIF", 2),
        CODRENDIMENTOS("codRendimentos", 3),
        TITULAR("titular", 4),
        RENDIMENTOS("rendimentos", 5),
        RETENCOES("retencoes", 6),
        CONTRIBUICOES("contribuicoes", 7),
        RETSOBRETAXA("retSobretaxa", 8),
        DATACONTRATOPREREFORMA("dataContratoPreReforma", 9),
        DATAPRIMEIROPAGAMENTO("dataPrimeiroPagamento", 10);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

