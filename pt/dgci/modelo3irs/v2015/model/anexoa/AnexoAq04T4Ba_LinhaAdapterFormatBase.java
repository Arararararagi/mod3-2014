/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoAq04T4Ba_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoAq04T4Ba_Linha>,
WritableTableFormat<AnexoAq04T4Ba_Linha> {
    @Override
    public boolean isEditable(AnexoAq04T4Ba_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoAq04T4Ba_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getProfissaoCodigo() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoAQ4BA", line.getProfissaoCodigo());
            }
            case 2: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepEFalecidos", line.getTitular());
            }
            case 3: {
                return line.getValor();
            }
            case 4: {
                return line.getNifNipcPortugues();
            }
            case 5: {
                if (line == null || line.getPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_Rosto", line.getPais());
            }
            case 6: {
                return line.getNumeroFiscalUE();
            }
        }
        return null;
    }

    @Override
    public AnexoAq04T4Ba_Linha setColumnValue(AnexoAq04T4Ba_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setProfissaoCodigo((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setValor((Long)value);
                return line;
            }
            case 4: {
                line.setNifNipcPortugues((Long)value);
                return line;
            }
            case 5: {
                if (value != null) {
                    line.setPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 6: {
                line.setNumeroFiscalUE((String)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Profiss\u00e3o/C\u00f3digo";
            }
            case 2: {
                return "Titular";
            }
            case 3: {
                return "Valor";
            }
            case 4: {
                return "NIF/NIPC Portugu\u00eas";
            }
            case 5: {
                return "Pa\u00eds";
            }
            case 6: {
                return "N\u00famero Fiscal (UE ou EEE)";
            }
        }
        return null;
    }
}

