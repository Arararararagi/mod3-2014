/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoA.qQuadro02";
    public static final String ANEXOAQ02C01_LINK = "aAnexoA.qQuadro02.fanexoAq02C01";
    public static final String ANEXOAQ02C01 = "anexoAq02C01";
    private Long anexoAq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoAq02C01() {
        return this.anexoAq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoAq02C01(Long anexoAq02C01) {
        Long oldValue = this.anexoAq02C01;
        this.anexoAq02C01 = anexoAq02C01;
        this.firePropertyChange("anexoAq02C01", oldValue, this.anexoAq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoAq02C01);
        return result;
    }
}

