/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_LinhaBase;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.util.Date;

public class AnexoAq04T4A_Linha
extends AnexoAq04T4A_LinhaBase {
    private static final long serialVersionUID = -7571090009825478247L;
    private Long nLinha;

    public AnexoAq04T4A_Linha() {
    }

    public AnexoAq04T4A_Linha(Long nIF, Long codRendimentos, String titular, Long rendimentos, Long retencoes, Long contribuicoes, Long retSobretaxa, Date dataContratoPreReforma, Date dataPrimeiroPagamento) {
        super(nIF, codRendimentos, titular, rendimentos, retencoes, contribuicoes, retSobretaxa, dataContratoPreReforma, dataPrimeiroPagamento);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Override
    public void setCodRendimentos(Long codRendimentos) {
        super.setCodRendimentos(codRendimentos);
        this.firePropertyChange("bananas", null, null);
    }

    public Long getNLinha() {
        return this.nLinha;
    }

    public void setNLinha(Long nLinha) {
        this.nLinha = nLinha;
    }

    public static String getLink(int numLinha) {
        return "aAnexoA.qQuadro04.tanexoAq04T4A.l" + numLinha;
    }

    public static String getLink(int lineIndex, AnexoAq04T4A_LinhaBase.Property column) {
        return AnexoAq04T4A_Linha.getLink(lineIndex) + ".c" + column.getIndex();
    }
}

