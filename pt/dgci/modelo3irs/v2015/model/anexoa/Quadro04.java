/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import ca.odell.glazedlists.EventList;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.Date;

public class Quadro04
extends Quadro04Base {
    public List<Integer> getNumsLinhaQ4T4A(String titular, long codRendimentos) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        if (titular == null) {
            return result;
        }
        for (int linha = 0; linha < this.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = this.getAnexoAq04T4A().get(linha);
            if (anexoAq04T4ALinha.getTitular() == null || !anexoAq04T4ALinha.getTitular().equals(titular) || anexoAq04T4ALinha.getCodRendimentos() == null || anexoAq04T4ALinha.getCodRendimentos() != codRendimentos) continue;
            result.add(linha);
        }
        return result;
    }

    public List<AnexoAq04T4A_Linha> getLinhasQ4T4AByCodigo(long codRendimentos) {
        ArrayList<AnexoAq04T4A_Linha> result = new ArrayList<AnexoAq04T4A_Linha>();
        for (int linha = 0; linha < this.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = this.getAnexoAq04T4A().get(linha);
            if (anexoAq04T4ALinha.getCodRendimentos() == null || anexoAq04T4ALinha.getCodRendimentos() != codRendimentos) continue;
            result.add(anexoAq04T4ALinha);
        }
        return result;
    }

    public List<AnexoAq04T4A_Linha> getLinhasQ4T4A(Long nif, Long codRendimentos, String titular) {
        ArrayList<AnexoAq04T4A_Linha> result = new ArrayList<AnexoAq04T4A_Linha>();
        if (titular == null) {
            return result;
        }
        for (int linha = 0; linha < this.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = this.getAnexoAq04T4A().get(linha);
            if ((anexoAq04T4ALinha.getNIF() == null || !anexoAq04T4ALinha.getNIF().equals(nif)) && (anexoAq04T4ALinha.getNIF() != null || nif != null) || anexoAq04T4ALinha.getTitular() == null || !anexoAq04T4ALinha.getTitular().equals(titular) || anexoAq04T4ALinha.getCodRendimentos() == null || !anexoAq04T4ALinha.getCodRendimentos().equals(codRendimentos)) continue;
            result.add(anexoAq04T4ALinha);
        }
        return result;
    }

    public List<AnexoAq04T4A_Linha> getLinhasQ4T4A(long[] codsRendimentos, String titular) {
        ArrayList<AnexoAq04T4A_Linha> result = new ArrayList<AnexoAq04T4A_Linha>();
        if (titular == null) {
            return result;
        }
        for (int linha = 0; linha < this.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = this.getAnexoAq04T4A().get(linha);
            if (anexoAq04T4ALinha.getTitular() == null || !anexoAq04T4ALinha.getTitular().equals(titular) || anexoAq04T4ALinha.getCodRendimentos() == null || !Modelo3IRSValidatorUtil.in(anexoAq04T4ALinha.getCodRendimentos(), codsRendimentos)) continue;
            result.add(anexoAq04T4ALinha);
        }
        return result;
    }

    public List<AnexoAq04T4A_Linha> getLinhasQ4T4A(long[] codsRendimentos) {
        ArrayList<AnexoAq04T4A_Linha> result = new ArrayList<AnexoAq04T4A_Linha>();
        for (int linha = 0; linha < this.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = this.getAnexoAq04T4A().get(linha);
            if (anexoAq04T4ALinha.getCodRendimentos() == null || !Modelo3IRSValidatorUtil.in(anexoAq04T4ALinha.getCodRendimentos(), codsRendimentos)) continue;
            result.add(anexoAq04T4ALinha);
        }
        return result;
    }

    public long getTotalRendimentosQ4T4AByEntidadeTitular(long entidade, String titular) {
        long result = 0;
        for (int linha = 0; linha < this.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = this.getAnexoAq04T4A().get(linha);
            if (anexoAq04T4ALinha.getNIF() == null || anexoAq04T4ALinha.getTitular() == null || !anexoAq04T4ALinha.getNIF().equals(entidade) || !anexoAq04T4ALinha.getTitular().equals(titular)) continue;
            result+=anexoAq04T4ALinha.getRendimentos() != null ? anexoAq04T4ALinha.getRendimentos() : 0;
        }
        return result;
    }

    public long getTotalRendimentosQ4T4AByEntidadeCodRendimentosTitular(long entidade, long codRendimentos, String titular) {
        long result = 0;
        for (int linha = 0; linha < this.getAnexoAq04T4A().size(); ++linha) {
            AnexoAq04T4A_Linha anexoAq04T4ALinha = this.getAnexoAq04T4A().get(linha);
            if (anexoAq04T4ALinha.getNIF() == null || anexoAq04T4ALinha.getTitular() == null || anexoAq04T4ALinha.getCodRendimentos() == null || !anexoAq04T4ALinha.getNIF().equals(entidade) || !anexoAq04T4ALinha.getTitular().equals(titular) || !anexoAq04T4ALinha.getCodRendimentos().equals(codRendimentos)) continue;
            result+=anexoAq04T4ALinha.getRendimentos() != null ? anexoAq04T4ALinha.getRendimentos() : 0;
        }
        return result;
    }

    public boolean existsInAnexoAq04T4Ba(String titular) {
        for (AnexoAq04T4Ba_Linha anexoAq04T4BaLinha : this.getAnexoAq04T4Ba()) {
            if (anexoAq04T4BaLinha.getTitular() == null || !anexoAq04T4BaLinha.getTitular().equals(titular)) continue;
            return true;
        }
        return false;
    }

    public boolean existsInAnexoAq04T4B(String titular, long codDespesa) {
        for (AnexoAq04T4B_Linha anexoAq04T4BLinha : this.getAnexoAq04T4B()) {
            if (anexoAq04T4BLinha.getTitular() == null || !anexoAq04T4BLinha.getTitular().equals(titular) || anexoAq04T4BLinha.getCodDespesa() == null || anexoAq04T4BLinha.getCodDespesa() != codDespesa) continue;
            return true;
        }
        return false;
    }

    public boolean existsEntidadeTitularInAnexoAq04T4A(long entidade, String titular) {
        for (AnexoAq04T4A_Linha anexoAq04T4ALinha : this.getAnexoAq04T4A()) {
            if (anexoAq04T4ALinha.getTitular() == null || !anexoAq04T4ALinha.getTitular().equals(titular) || anexoAq04T4ALinha.getNIF() == null || anexoAq04T4ALinha.getNIF() != entidade) continue;
            return true;
        }
        return false;
    }

    public boolean existsTitularInAnexoAq04T4A(String titular) {
        for (AnexoAq04T4A_Linha anexoAq04T4ALinha : this.getAnexoAq04T4A()) {
            if (anexoAq04T4ALinha.getTitular() == null || !anexoAq04T4ALinha.getTitular().equals(titular)) continue;
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        return this.isEmptyQ04T4A() && this.isEmptyQ04T4B() && this.isEmptyQ04T4Ba();
    }

    public boolean isEmptyQ04T4A() {
        if (this.getAnexoAq04T4A().size() == 0) {
            return true;
        }
        for (AnexoAq04T4A_Linha anexoAq04T4ALinha : this.getAnexoAq04T4A()) {
            if (anexoAq04T4ALinha.getNIF() == null && anexoAq04T4ALinha.getCodRendimentos() == null && anexoAq04T4ALinha.getTitular() == null && anexoAq04T4ALinha.getRendimentos() == null && anexoAq04T4ALinha.getRetencoes() == null && anexoAq04T4ALinha.getContribuicoes() == null && anexoAq04T4ALinha.getRetSobretaxa() == null && anexoAq04T4ALinha.getDataContratoPreReforma() == null && anexoAq04T4ALinha.getDataPrimeiroPagamento() == null) continue;
            return false;
        }
        return true;
    }

    public boolean isEmptyQ04T4B() {
        if (this.getAnexoAq04T4B().size() == 0) {
            return true;
        }
        for (AnexoAq04T4B_Linha anexoAq04T4BLinha : this.getAnexoAq04T4B()) {
            if (anexoAq04T4BLinha.getCodDespesa() == null && anexoAq04T4BLinha.getTitular() == null && anexoAq04T4BLinha.getValor() == null) continue;
            return false;
        }
        return true;
    }

    public boolean isEmptyQ04T4Ba() {
        if (this.getAnexoAq04T4Ba().size() == 0) {
            return true;
        }
        for (AnexoAq04T4Ba_Linha anexoAq04T4BaLinha : this.getAnexoAq04T4Ba()) {
            if (anexoAq04T4BaLinha.getTitular() == null && anexoAq04T4BaLinha.getValor() == null && anexoAq04T4BaLinha.getNifNipcPortugues() == null && anexoAq04T4BaLinha.getPais() == null && anexoAq04T4BaLinha.getNumeroFiscalUE() == null) continue;
            return false;
        }
        return true;
    }
}

