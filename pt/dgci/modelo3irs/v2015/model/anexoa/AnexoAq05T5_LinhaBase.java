/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoAq05T5_LinhaBase
extends Model {
    public static final String NIF_LINK = "aAnexoA.qQuadro05.fnif";
    public static final String NIF = "nif";
    private Long nif;
    public static final String CODRENDIMENTOS_LINK = "aAnexoA.qQuadro05.fcodRendimentos";
    public static final String CODRENDIMENTOS = "codRendimentos";
    private Long codRendimentos;
    public static final String TITULAR_LINK = "aAnexoA.qQuadro05.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String RENDIMENTOS_LINK = "aAnexoA.qQuadro05.frendimentos";
    public static final String RENDIMENTOS = "rendimentos";
    private Long rendimentos;
    public static final String NANOS_LINK = "aAnexoA.qQuadro05.fnanos";
    public static final String NANOS = "nanos";
    private Long nanos;

    public AnexoAq05T5_LinhaBase(Long nif, Long codRendimentos, String titular, Long rendimentos, Long nanos) {
        this.nif = nif;
        this.codRendimentos = codRendimentos;
        this.titular = titular;
        this.rendimentos = rendimentos;
        this.nanos = nanos;
    }

    public AnexoAq05T5_LinhaBase() {
        this.nif = null;
        this.codRendimentos = null;
        this.titular = null;
        this.rendimentos = null;
        this.nanos = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNif() {
        return this.nif;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNif(Long nif) {
        Long oldValue = this.nif;
        this.nif = nif;
        this.firePropertyChange("nif", oldValue, this.nif);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoAQ5")
    public Long getCodRendimentos() {
        return this.codRendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodRendimentos(Long codRendimentos) {
        Long oldValue = this.codRendimentos;
        this.codRendimentos = codRendimentos;
        this.firePropertyChange("codRendimentos", oldValue, this.codRendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentos() {
        return this.rendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentos(Long rendimentos) {
        Long oldValue = this.rendimentos;
        this.rendimentos = rendimentos;
        this.firePropertyChange("rendimentos", oldValue, this.rendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNanos() {
        return this.nanos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNanos(Long nanos) {
        Long oldValue = this.nanos;
        this.nanos = nanos;
        this.firePropertyChange("nanos", oldValue, this.nanos);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nif);
        result = HashCodeUtil.hash(result, this.codRendimentos);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.rendimentos);
        result = HashCodeUtil.hash(result, this.nanos);
        return result;
    }

    public static enum Property {
        NIF("nif", 2),
        CODRENDIMENTOS("codRendimentos", 3),
        TITULAR("titular", 4),
        RENDIMENTOS("rendimentos", 5),
        NANOS("nanos", 6);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

