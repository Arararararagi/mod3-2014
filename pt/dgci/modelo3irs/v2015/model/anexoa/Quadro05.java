/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro05Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro05
extends Quadro05Base {
    public long getTotalRendimentosByCodigoRendimento(long[] codigosRendimento) {
        long result = 0;
        for (AnexoAq05T5_Linha anexoAq05T5Linha : this.getAnexoAq05T5()) {
            if (anexoAq05T5Linha.getCodRendimentos() == null || !Modelo3IRSValidatorUtil.in(anexoAq05T5Linha.getCodRendimentos(), codigosRendimento)) continue;
            result+=anexoAq05T5Linha.getRendimentos() != null ? anexoAq05T5Linha.getRendimentos() : 0;
        }
        return result;
    }

    public long getTotalRendimentosByCodigoRendimentoTitular(long[] codigosRendimento, String titular) {
        long result = 0;
        for (AnexoAq05T5_Linha anexoAq05T5Linha : this.getAnexoAq05T5()) {
            if (anexoAq05T5Linha.getTitular() == null || !anexoAq05T5Linha.getTitular().equals(titular) || anexoAq05T5Linha.getCodRendimentos() == null || !Modelo3IRSValidatorUtil.in(anexoAq05T5Linha.getCodRendimentos(), codigosRendimento)) continue;
            result+=anexoAq05T5Linha.getRendimentos() != null ? anexoAq05T5Linha.getRendimentos() : 0;
        }
        return result;
    }

    public long getTotalNAnosByCodigoRendimento(long[] codigosRendimento) {
        long result = 0;
        for (AnexoAq05T5_Linha anexoAq05T5Linha : this.getAnexoAq05T5()) {
            if (anexoAq05T5Linha.getCodRendimentos() == null || !Modelo3IRSValidatorUtil.in(anexoAq05T5Linha.getCodRendimentos(), codigosRendimento)) continue;
            result+=anexoAq05T5Linha.getNanos() != null ? anexoAq05T5Linha.getNanos() : 0;
        }
        return result;
    }

    public long getTotalNAnosByCodigoRendimentoTitular(long[] codigosRendimento, String titular) {
        long result = 0;
        for (AnexoAq05T5_Linha anexoAq05T5Linha : this.getAnexoAq05T5()) {
            if (anexoAq05T5Linha.getTitular() == null || !anexoAq05T5Linha.getTitular().equals(titular) || anexoAq05T5Linha.getCodRendimentos() == null || !Modelo3IRSValidatorUtil.in(anexoAq05T5Linha.getCodRendimentos(), codigosRendimento)) continue;
            result+=anexoAq05T5Linha.getNanos() != null ? anexoAq05T5Linha.getNanos() : 0;
        }
        return result;
    }

    public boolean isEmpty() {
        if (this.getAnexoAq05T5().size() == 0) {
            return true;
        }
        for (AnexoAq05T5_Linha anexoAq05T5Linha : this.getAnexoAq05T5()) {
            if (anexoAq05T5Linha.getNif() == null && anexoAq05T5Linha.getCodRendimentos() == null && anexoAq05T5Linha.getTitular() == null && anexoAq05T5Linha.getRendimentos() == null && anexoAq05T5Linha.getNanos() == null) continue;
            return false;
        }
        return true;
    }
}

