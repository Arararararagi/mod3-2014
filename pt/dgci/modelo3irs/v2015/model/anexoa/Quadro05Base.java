/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoA.qQuadro05";
    public static final String ANEXOAQ05T5_LINK = "aAnexoA.qQuadro05.tanexoAq05T5";
    private EventList<AnexoAq05T5_Linha> anexoAq05T5 = new BasicEventList<AnexoAq05T5_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoAq05T5_Linha> getAnexoAq05T5() {
        return this.anexoAq05T5;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoAq05T5(EventList<AnexoAq05T5_Linha> anexoAq05T5) {
        this.anexoAq05T5 = anexoAq05T5;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoAq05T5);
        return result;
    }
}

