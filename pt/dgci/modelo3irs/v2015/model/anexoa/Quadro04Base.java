/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoA.qQuadro04";
    public static final String ANEXOAQ04T4A_LINK = "aAnexoA.qQuadro04.tanexoAq04T4A";
    private EventList<AnexoAq04T4A_Linha> anexoAq04T4A = new BasicEventList<AnexoAq04T4A_Linha>();
    public static final String ANEXOAQ04C1_LINK = "aAnexoA.qQuadro04.fanexoAq04C1";
    public static final String ANEXOAQ04C1 = "anexoAq04C1";
    private Long anexoAq04C1;
    public static final String ANEXOAQ04C2_LINK = "aAnexoA.qQuadro04.fanexoAq04C2";
    public static final String ANEXOAQ04C2 = "anexoAq04C2";
    private Long anexoAq04C2;
    public static final String ANEXOAQ04C3_LINK = "aAnexoA.qQuadro04.fanexoAq04C3";
    public static final String ANEXOAQ04C3 = "anexoAq04C3";
    private Long anexoAq04C3;
    public static final String ANEXOAQ04C4_LINK = "aAnexoA.qQuadro04.fanexoAq04C4";
    public static final String ANEXOAQ04C4 = "anexoAq04C4";
    private Long anexoAq04C4;
    public static final String ANEXOAQ04T4B_LINK = "aAnexoA.qQuadro04.tanexoAq04T4B";
    private EventList<AnexoAq04T4B_Linha> anexoAq04T4B = new BasicEventList<AnexoAq04T4B_Linha>();
    public static final String ANEXOAQ04T4BA_LINK = "aAnexoA.qQuadro04.tanexoAq04T4Ba";
    private EventList<AnexoAq04T4Ba_Linha> anexoAq04T4Ba = new BasicEventList<AnexoAq04T4Ba_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoAq04T4A_Linha> getAnexoAq04T4A() {
        return this.anexoAq04T4A;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoAq04T4A(EventList<AnexoAq04T4A_Linha> anexoAq04T4A) {
        this.anexoAq04T4A = anexoAq04T4A;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoAq04C1() {
        return this.anexoAq04C1;
    }

    public void setAnexoAq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoAq04C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoAq04T4A().size(); ++i) {
            temporaryValue0+=this.getAnexoAq04T4A().get(i).getRendimentos() == null ? 0 : this.getAnexoAq04T4A().get(i).getRendimentos();
        }
        this.setAnexoAq04C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoAq04C1(Long anexoAq04C1) {
        Long oldValue = this.anexoAq04C1;
        this.anexoAq04C1 = anexoAq04C1;
        this.firePropertyChange("anexoAq04C1", oldValue, this.anexoAq04C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoAq04C2() {
        return this.anexoAq04C2;
    }

    public void setAnexoAq04C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoAq04C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoAq04T4A().size(); ++i) {
            temporaryValue0+=this.getAnexoAq04T4A().get(i).getRetencoes() == null ? 0 : this.getAnexoAq04T4A().get(i).getRetencoes();
        }
        this.setAnexoAq04C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoAq04C2(Long anexoAq04C2) {
        Long oldValue = this.anexoAq04C2;
        this.anexoAq04C2 = anexoAq04C2;
        this.firePropertyChange("anexoAq04C2", oldValue, this.anexoAq04C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoAq04C3() {
        return this.anexoAq04C3;
    }

    public void setAnexoAq04C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoAq04C3")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoAq04T4A().size(); ++i) {
            temporaryValue0+=this.getAnexoAq04T4A().get(i).getContribuicoes() == null ? 0 : this.getAnexoAq04T4A().get(i).getContribuicoes();
        }
        this.setAnexoAq04C3(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoAq04C3(Long anexoAq04C3) {
        Long oldValue = this.anexoAq04C3;
        this.anexoAq04C3 = anexoAq04C3;
        this.firePropertyChange("anexoAq04C3", oldValue, this.anexoAq04C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoAq04C4() {
        return this.anexoAq04C4;
    }

    public void setAnexoAq04C4Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoAq04C4")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoAq04T4A().size(); ++i) {
            temporaryValue0+=this.getAnexoAq04T4A().get(i).getRetSobretaxa() == null ? 0 : this.getAnexoAq04T4A().get(i).getRetSobretaxa();
        }
        this.setAnexoAq04C4(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoAq04C4(Long anexoAq04C4) {
        Long oldValue = this.anexoAq04C4;
        this.anexoAq04C4 = anexoAq04C4;
        this.firePropertyChange("anexoAq04C4", oldValue, this.anexoAq04C4);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoAq04T4B_Linha> getAnexoAq04T4B() {
        return this.anexoAq04T4B;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoAq04T4B(EventList<AnexoAq04T4B_Linha> anexoAq04T4B) {
        this.anexoAq04T4B = anexoAq04T4B;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoAq04T4Ba_Linha> getAnexoAq04T4Ba() {
        return this.anexoAq04T4Ba;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoAq04T4Ba(EventList<AnexoAq04T4Ba_Linha> anexoAq04T4Ba) {
        this.anexoAq04T4Ba = anexoAq04T4Ba;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoAq04T4A);
        result = HashCodeUtil.hash(result, this.anexoAq04C1);
        result = HashCodeUtil.hash(result, this.anexoAq04C2);
        result = HashCodeUtil.hash(result, this.anexoAq04C3);
        result = HashCodeUtil.hash(result, this.anexoAq04C4);
        result = HashCodeUtil.hash(result, this.anexoAq04T4B);
        result = HashCodeUtil.hash(result, this.anexoAq04T4Ba);
        return result;
    }
}

