/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.util.Date;

public class AnexoAq04T4A_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoAq04T4A_Linha>,
WritableTableFormat<AnexoAq04T4A_Linha> {
    @Override
    public boolean isEditable(AnexoAq04T4A_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoAq04T4A_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNIF();
            }
            case 2: {
                if (line == null || line.getCodRendimentos() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoAQ4A", line.getCodRendimentos());
            }
            case 3: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepEFalecidos", line.getTitular());
            }
            case 4: {
                return line.getRendimentos();
            }
            case 5: {
                return line.getRetencoes();
            }
            case 6: {
                return line.getContribuicoes();
            }
            case 7: {
                return line.getRetSobretaxa();
            }
            case 8: {
                return line.getDataContratoPreReforma();
            }
            case 9: {
                return line.getDataPrimeiroPagamento();
            }
        }
        return null;
    }

    @Override
    public AnexoAq04T4A_Linha setColumnValue(AnexoAq04T4A_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNIF((Long)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCodRendimentos((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 4: {
                line.setRendimentos((Long)value);
                return line;
            }
            case 5: {
                line.setRetencoes((Long)value);
                return line;
            }
            case 6: {
                line.setContribuicoes((Long)value);
                return line;
            }
            case 7: {
                line.setRetSobretaxa((Long)value);
                return line;
            }
            case 8: {
                line.setDataContratoPreReforma((Date)value);
                return line;
            }
            case 9: {
                line.setDataPrimeiroPagamento((Date)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Date.class;
            }
            case 9: {
                return Date.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 10;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF da Entidade Pagadora";
            }
            case 2: {
                return "C\u00f3digo dos Rendimentos";
            }
            case 3: {
                return "Titular";
            }
            case 4: {
                return "Rendimentos";
            }
            case 5: {
                return "Reten\u00e7\u00f5es";
            }
            case 6: {
                return "Contribui\u00e7\u00f5es";
            }
            case 7: {
                return "Reten\u00e7\u00e3o Sobretaxa";
            }
            case 8: {
                return "Data do contrato de pr\u00e9-reforma";
            }
            case 9: {
                return "Data do primeiro pagamento";
            }
        }
        return null;
    }
}

