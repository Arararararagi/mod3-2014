/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoA.qQuadro03";
    public static final String ANEXOAQ03C02_LINK = "aAnexoA.qQuadro03.fanexoAq03C02";
    public static final String ANEXOAQ03C02 = "anexoAq03C02";
    private Long anexoAq03C02;
    public static final String ANEXOAQ03C03_LINK = "aAnexoA.qQuadro03.fanexoAq03C03";
    public static final String ANEXOAQ03C03 = "anexoAq03C03";
    private Long anexoAq03C03;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoAq03C02() {
        return this.anexoAq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoAq03C02(Long anexoAq03C02) {
        Long oldValue = this.anexoAq03C02;
        this.anexoAq03C02 = anexoAq03C02;
        this.firePropertyChange("anexoAq03C02", oldValue, this.anexoAq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoAq03C03() {
        return this.anexoAq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoAq03C03(Long anexoAq03C03) {
        Long oldValue = this.anexoAq03C03;
        this.anexoAq03C03 = anexoAq03C03;
        this.firePropertyChange("anexoAq03C03", oldValue, this.anexoAq03C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoAq03C02);
        result = HashCodeUtil.hash(result, this.anexoAq03C03);
        return result;
    }
}

