/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_LinhaBase;

public class AnexoAq05T5_Linha
extends AnexoAq05T5_LinhaBase {
    public AnexoAq05T5_Linha() {
    }

    public AnexoAq05T5_Linha(Long nif, Long codRendimentos, String titular, Long rendimentos, Long nanos) {
        super(nif, codRendimentos, titular, rendimentos, nanos);
    }

    public static String getLink(int numLinha) {
        return "aAnexoA.qQuadro05.tanexoAq05T5.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoAq05T5_LinhaBase.Property column) {
        return "aAnexoA.qQuadro05.tanexoAq05T5.l" + numLinha + ".c" + column.getIndex();
    }
}

