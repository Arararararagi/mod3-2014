/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4Ba_LinhaBase;

public class AnexoAq04T4Ba_Linha
extends AnexoAq04T4Ba_LinhaBase {
    public AnexoAq04T4Ba_Linha() {
    }

    public AnexoAq04T4Ba_Linha(Long profissaoCodigo, String titular, Long valor, Long nifNipcPortugues, Long pais, String numeroFiscalUE) {
        super(profissaoCodigo, titular, valor, nifNipcPortugues, pais, numeroFiscalUE);
    }

    public static String getLink(int numLinha) {
        return "aAnexoA.qQuadro04.tanexoAq04T4Ba.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoAq04T4Ba_LinhaBase.Property campo) {
        return "aAnexoA.qQuadro04.tanexoAq04T4Ba.l" + numLinha + ".c" + campo.getIndex();
    }
}

