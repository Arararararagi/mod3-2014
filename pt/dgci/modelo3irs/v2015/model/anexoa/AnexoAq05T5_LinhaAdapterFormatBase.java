/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoAq05T5_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoAq05T5_Linha>,
WritableTableFormat<AnexoAq05T5_Linha> {
    @Override
    public boolean isEditable(AnexoAq05T5_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoAq05T5_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNif();
            }
            case 2: {
                if (line == null || line.getCodRendimentos() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoAQ5", line.getCodRendimentos());
            }
            case 3: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepEFalecidos", line.getTitular());
            }
            case 4: {
                return line.getRendimentos();
            }
            case 5: {
                return line.getNanos();
            }
        }
        return null;
    }

    @Override
    public AnexoAq05T5_Linha setColumnValue(AnexoAq05T5_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNif((Long)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCodRendimentos((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 4: {
                line.setRendimentos((Long)value);
                return line;
            }
            case 5: {
                line.setNanos((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF da Entidade Pagadora";
            }
            case 2: {
                return "C\u00f3digo dos Rendimentos";
            }
            case 3: {
                return "Titular";
            }
            case 4: {
                return "Rendimentos";
            }
            case 5: {
                return "N.\u00ba anos";
            }
        }
        return null;
    }
}

