/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoAq04T4B_LinhaBase
extends Model {
    public static final String CODDESPESA_LINK = "aAnexoA.qQuadro04.fcodDespesa";
    public static final String CODDESPESA = "codDespesa";
    private Long codDespesa;
    public static final String TITULAR_LINK = "aAnexoA.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String VALOR_LINK = "aAnexoA.qQuadro04.fvalor";
    public static final String VALOR = "valor";
    private Long valor;

    public AnexoAq04T4B_LinhaBase(Long codDespesa, String titular, Long valor) {
        this.codDespesa = codDespesa;
        this.titular = titular;
        this.valor = valor;
    }

    public AnexoAq04T4B_LinhaBase() {
        this.codDespesa = null;
        this.titular = null;
        this.valor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Despesas")
    public Long getCodDespesa() {
        return this.codDespesa;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodDespesa(Long codDespesa) {
        Long oldValue = this.codDespesa;
        this.codDespesa = codDespesa;
        this.firePropertyChange("codDespesa", oldValue, this.codDespesa);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValor() {
        return this.valor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValor(Long valor) {
        Long oldValue = this.valor;
        this.valor = valor;
        this.firePropertyChange("valor", oldValue, this.valor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.codDespesa);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.valor);
        return result;
    }

    public static enum Property {
        CODDESPESA("codDespesa", 2),
        TITULAR("titular", 3),
        VALOR("valor", 4);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

