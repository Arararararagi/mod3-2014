/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_LinhaBase;

public class AnexoAq04T4B_Linha
extends AnexoAq04T4B_LinhaBase {
    public AnexoAq04T4B_Linha() {
    }

    public AnexoAq04T4B_Linha(Long codDespesa, String titular, Long valor) {
        super(codDespesa, titular, valor);
    }

    public static String getLink(int numLinha, AnexoAq04T4B_LinhaBase.Property campo) {
        return "aAnexoA.qQuadro04.tanexoAq04T4B.l" + numLinha + ".c" + campo.getIndex();
    }
}

