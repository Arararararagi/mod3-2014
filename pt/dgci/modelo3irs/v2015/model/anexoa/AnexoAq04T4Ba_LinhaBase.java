/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoa;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoAq04T4Ba_LinhaBase
extends Model {
    public static final String PROFISSAOCODIGO_LINK = "aAnexoA.qQuadro04.fprofissaoCodigo";
    public static final String PROFISSAOCODIGO = "profissaoCodigo";
    private Long profissaoCodigo;
    public static final String TITULAR_LINK = "aAnexoA.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String VALOR_LINK = "aAnexoA.qQuadro04.fvalor";
    public static final String VALOR = "valor";
    private Long valor;
    public static final String NIFNIPCPORTUGUES_LINK = "aAnexoA.qQuadro04.fnifNipcPortugues";
    public static final String NIFNIPCPORTUGUES = "nifNipcPortugues";
    private Long nifNipcPortugues;
    public static final String PAIS_LINK = "aAnexoA.qQuadro04.fpais";
    public static final String PAIS = "pais";
    private Long pais;
    public static final String NUMEROFISCALUE_LINK = "aAnexoA.qQuadro04.fnumeroFiscalUE";
    public static final String NUMEROFISCALUE = "numeroFiscalUE";
    private String numeroFiscalUE;

    public AnexoAq04T4Ba_LinhaBase(Long profissaoCodigo, String titular, Long valor, Long nifNipcPortugues, Long pais, String numeroFiscalUE) {
        this.profissaoCodigo = profissaoCodigo;
        this.titular = titular;
        this.valor = valor;
        this.nifNipcPortugues = nifNipcPortugues;
        this.pais = pais;
        this.numeroFiscalUE = numeroFiscalUE;
    }

    public AnexoAq04T4Ba_LinhaBase() {
        this.profissaoCodigo = null;
        this.titular = null;
        this.valor = null;
        this.nifNipcPortugues = null;
        this.pais = null;
        this.numeroFiscalUE = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoAQ4BA")
    public Long getProfissaoCodigo() {
        return this.profissaoCodigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setProfissaoCodigo(Long profissaoCodigo) {
        Long oldValue = this.profissaoCodigo;
        this.profissaoCodigo = profissaoCodigo;
        this.firePropertyChange("profissaoCodigo", oldValue, this.profissaoCodigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValor() {
        return this.valor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValor(Long valor) {
        Long oldValue = this.valor;
        this.valor = valor;
        this.firePropertyChange("valor", oldValue, this.valor);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifNipcPortugues() {
        return this.nifNipcPortugues;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifNipcPortugues(Long nifNipcPortugues) {
        Long oldValue = this.nifNipcPortugues;
        this.nifNipcPortugues = nifNipcPortugues;
        this.firePropertyChange("nifNipcPortugues", oldValue, this.nifNipcPortugues);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_Rosto")
    public Long getPais() {
        return this.pais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setPais(Long pais) {
        Long oldValue = this.pais;
        this.pais = pais;
        this.firePropertyChange("pais", oldValue, this.pais);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getNumeroFiscalUE() {
        return this.numeroFiscalUE;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNumeroFiscalUE(String numeroFiscalUE) {
        String oldValue = this.numeroFiscalUE;
        this.numeroFiscalUE = numeroFiscalUE;
        this.firePropertyChange("numeroFiscalUE", oldValue, this.numeroFiscalUE);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.profissaoCodigo);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.valor);
        result = HashCodeUtil.hash(result, this.nifNipcPortugues);
        result = HashCodeUtil.hash(result, this.pais);
        result = HashCodeUtil.hash(result, this.numeroFiscalUE);
        return result;
    }

    public static enum Property {
        PROFISSAOCODIGO("profissaoCodigo", 2),
        TITULAR("titular", 3),
        VALOR("valor", 4),
        NIFNIPCPORTUGUES("nifNipcPortugues", 5),
        PAIS("pais", 6),
        NUMEROFISCALUE("numeroFiscalUE", 7);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

