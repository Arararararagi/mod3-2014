/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015ModelBase;
import pt.dgci.modelo3irs.v2015.model.OwnableAnexo;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.persistence.DeclarationWriter;
import pt.opensoft.taxclient.persistence.SchemaNamespace;
import pt.opensoft.util.StringUtil;

@SchemaNamespace(value="http://www.dgci.gov.pt/2009/Modelo3IRSv2015")
public class Modelo3IRSv2015Model
extends Modelo3IRSv2015ModelBase {
    private static final long serialVersionUID = -6510093599633632760L;

    public RostoModel getRosto() {
        return (RostoModel)this.getAnexo(RostoModel.class);
    }

    public AnexoAModel getAnexoA() {
        return (AnexoAModel)this.getAnexo(AnexoAModel.class);
    }

    public List<AnexoModel> getAnexosB() {
        return this.getAllAnexosByType(AnexoBModel.class);
    }

    protected <E extends AnexoModel> E getAnexoByOwner(Class<E> anexoType, Long nif) {
        List<AnexoModel> models = this.getAllAnexosByType(anexoType);
        for (AnexoModel model : models) {
            if (!((OwnableAnexo)model).isOwnedBy(nif)) continue;
            return (E)model;
        }
        return null;
    }

    public AnexoBModel getAnexoBByTitular(Long titular) {
        return (AnexoBModel)this.getAnexoByOwner(AnexoBModel.class, titular);
    }

    public AnexoBModel getAnexoBByTitular(String titular) {
        if (titular != null && StringUtil.isNumeric(titular)) {
            return this.getAnexoBByTitular(Long.valueOf(titular));
        }
        return null;
    }

    public List<AnexoModel> getAnexosC() {
        return this.getAllAnexosByType(AnexoCModel.class);
    }

    public AnexoCModel getAnexoCByTitular(String titular) {
        if (titular != null && StringUtil.isNumeric(titular)) {
            return this.getAnexoCByTitular(Long.valueOf(titular));
        }
        return null;
    }

    public AnexoCModel getAnexoCByTitular(Long titular) {
        return (AnexoCModel)this.getAnexoByOwner(AnexoCModel.class, titular);
    }

    public List<AnexoModel> getAnexosD() {
        return this.getAllAnexosByType(AnexoDModel.class);
    }

    public AnexoDModel getAnexoDByTitular(String titular) {
        if (titular != null && StringUtil.isNumeric(titular)) {
            return this.getAnexoDByTitular(Long.valueOf(titular));
        }
        return null;
    }

    public AnexoDModel getAnexoDByTitular(Long titular) {
        return (AnexoDModel)this.getAnexoByOwner(AnexoDModel.class, titular);
    }

    public List<AnexoBModel> getAnexoB() {
        List<FormKey> allAnexosByType = this.getAllAnexosByType("AnexoB");
        ArrayList<AnexoBModel> anexoBList = new ArrayList<AnexoBModel>();
        for (FormKey formKey : allAnexosByType) {
            anexoBList.add((AnexoBModel)this.getAnexo(formKey));
        }
        return anexoBList;
    }

    public List<AnexoCModel> getAnexoC() {
        List<FormKey> allAnexosByType = this.getAllAnexosByType("AnexoC");
        ArrayList<AnexoCModel> anexoCList = new ArrayList<AnexoCModel>();
        for (FormKey formKey : allAnexosByType) {
            anexoCList.add((AnexoCModel)this.getAnexo(formKey));
        }
        return anexoCList;
    }

    public List<AnexoDModel> getAnexoD() {
        List<FormKey> allAnexosByType = this.getAllAnexosByType("AnexoD");
        ArrayList<AnexoDModel> anexoDList = new ArrayList<AnexoDModel>();
        for (FormKey formKey : allAnexosByType) {
            anexoDList.add((AnexoDModel)this.getAnexo(formKey));
        }
        return anexoDList;
    }

    public AnexoEModel getAnexoE() {
        return (AnexoEModel)this.getAnexo(AnexoEModel.class);
    }

    public AnexoFModel getAnexoF() {
        return (AnexoFModel)this.getAnexo(AnexoFModel.class);
    }

    public AnexoGModel getAnexoG() {
        return (AnexoGModel)this.getAnexo(AnexoGModel.class);
    }

    public AnexoG1Model getAnexoG1() {
        return (AnexoG1Model)this.getAnexo(AnexoG1Model.class);
    }

    public AnexoHModel getAnexoH() {
        return (AnexoHModel)this.getAnexo(AnexoHModel.class);
    }

    public List<AnexoModel> getAnexosI() {
        return this.getAllAnexosByType(AnexoIModel.class);
    }

    public AnexoIModel getAnexoIByTitular(String titular) {
        if (titular != null && StringUtil.isNumeric(titular)) {
            return this.getAnexoIByTitular(Long.valueOf(titular));
        }
        return null;
    }

    public AnexoIModel getAnexoIByTitular(Long titular) {
        return (AnexoIModel)this.getAnexoByOwner(AnexoIModel.class, titular);
    }

    public List<AnexoModel> getAnexosJ() {
        return this.getAllAnexosByType(AnexoJModel.class);
    }

    public AnexoJModel getAnexoJByTitular(String titular) {
        if (titular != null && StringUtil.isNumeric(titular)) {
            return this.getAnexoJByTitular(Long.valueOf(titular));
        }
        return null;
    }

    public AnexoJModel getAnexoJByTitular(Long titular) {
        return (AnexoJModel)this.getAnexoByOwner(AnexoJModel.class, titular);
    }

    public List<AnexoModel> getAnexosL() {
        return this.getAllAnexosByType(AnexoLModel.class);
    }

    public AnexoLModel getAnexoLByTitular(String titular) {
        if (titular != null && StringUtil.isNumeric(titular)) {
            return this.getAnexoLByTitular(Long.valueOf(titular));
        }
        return null;
    }

    public AnexoLModel getAnexoLByTitular(Long titular) {
        return (AnexoLModel)this.getAnexoByOwner(AnexoLModel.class, titular);
    }

    public List<AnexoIModel> getAnexoI() {
        List<FormKey> allAnexosByType = this.getAllAnexosByType("AnexoI");
        ArrayList<AnexoIModel> anexoIList = new ArrayList<AnexoIModel>();
        for (FormKey formKey : allAnexosByType) {
            anexoIList.add((AnexoIModel)this.getAnexo(formKey));
        }
        return anexoIList;
    }

    public List<AnexoJModel> getAnexoJ() {
        List<FormKey> allAnexosByType = this.getAllAnexosByType("AnexoJ");
        ArrayList<AnexoJModel> anexoJList = new ArrayList<AnexoJModel>();
        for (FormKey formKey : allAnexosByType) {
            anexoJList.add((AnexoJModel)this.getAnexo(formKey));
        }
        return anexoJList;
    }

    public List<AnexoLModel> getAnexoL() {
        List<FormKey> allAnexosByType = this.getAllAnexosByType("AnexoL");
        ArrayList<AnexoLModel> anexoLList = new ArrayList<AnexoLModel>();
        for (FormKey formKey : allAnexosByType) {
            anexoLList.add((AnexoLModel)this.getAnexo(formKey));
        }
        return anexoLList;
    }

    public List<AnexoModel> getAnexosSS() {
        return this.getAllAnexosByType(AnexoSSModel.class);
    }

    public List<AnexoSSModel> getAnexoSS() {
        List<FormKey> allAnexosByType = this.getAllAnexosByType("AnexoSS");
        ArrayList<AnexoSSModel> anexoSSList = new ArrayList<AnexoSSModel>();
        for (FormKey formKey : allAnexosByType) {
            anexoSSList.add((AnexoSSModel)this.getAnexo(formKey));
        }
        return anexoSSList;
    }

    public AnexoSSModel getAnexoSSByTitular(String titular) {
        if (titular != null && StringUtil.isNumeric(titular)) {
            return this.getAnexoSSByTitular(Long.valueOf(titular));
        }
        return null;
    }

    public AnexoSSModel getAnexoSSByTitular(Long titular) {
        return (AnexoSSModel)this.getAnexoByOwner(AnexoSSModel.class, titular);
    }

    public void reload() {
        try {
            DeclarationWriter declWriter = new DeclarationWriter();
            StringWriter declXml = new StringWriter();
            declWriter.write(declXml, this, null, null, false, false);
            this.resetModel();
            DeclarationReader.read(new StringReader(declXml.getBuffer().toString()), this, false, false);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Object clone() throws CloneNotSupportedException {
        try {
            DeclarationWriter declWriter = new DeclarationWriter();
            StringWriter declXml = new StringWriter();
            declWriter.write(declXml, this, null, null, false, false);
            DeclaracaoModel other = (DeclaracaoModel)this.getClass().newInstance();
            DeclarationReader.read(new StringReader(declXml.getBuffer().toString()), other, false, true);
            return other;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

