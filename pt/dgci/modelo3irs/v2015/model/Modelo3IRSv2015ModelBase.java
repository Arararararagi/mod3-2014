/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model;

import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.persistence.SchemaNamespace;
import pt.opensoft.taxclient.util.HashCodeUtil;

@SchemaNamespace(value="http://www.dgci.gov.pt/2009/Modelo3IRSv2015")
public class Modelo3IRSv2015ModelBase
extends DeclaracaoModel {
    public Modelo3IRSv2015ModelBase() {
        super("Modelo3IRSv2015", 1);
    }

    @Override
    protected AnexoModel createAnexo(FormKey formKey) {
        if ("Rosto".equals(formKey.getId())) {
            return new RostoModel(formKey, true);
        }
        if ("AnexoA".equals(formKey.getId())) {
            return new AnexoAModel(formKey, true);
        }
        if ("AnexoB".equals(formKey.getId())) {
            return new AnexoBModel(formKey, true);
        }
        if ("AnexoC".equals(formKey.getId())) {
            return new AnexoCModel(formKey, true);
        }
        if ("AnexoD".equals(formKey.getId())) {
            return new AnexoDModel(formKey, true);
        }
        if ("AnexoE".equals(formKey.getId())) {
            return new AnexoEModel(formKey, true);
        }
        if ("AnexoF".equals(formKey.getId())) {
            return new AnexoFModel(formKey, true);
        }
        if ("AnexoG".equals(formKey.getId())) {
            return new AnexoGModel(formKey, true);
        }
        if ("AnexoG1".equals(formKey.getId())) {
            return new AnexoG1Model(formKey, true);
        }
        if ("AnexoH".equals(formKey.getId())) {
            return new AnexoHModel(formKey, true);
        }
        if ("AnexoI".equals(formKey.getId())) {
            return new AnexoIModel(formKey, true);
        }
        if ("AnexoJ".equals(formKey.getId())) {
            return new AnexoJModel(formKey, true);
        }
        if ("AnexoL".equals(formKey.getId())) {
            return new AnexoLModel(formKey, true);
        }
        if ("AnexoSS".equals(formKey.getId())) {
            return new AnexoSSModel(formKey, true);
        }
        return null;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(RostoModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoAModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoBModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoCModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoDModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoEModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoFModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoGModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoG1Model.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoHModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoIModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoJModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoLModel.class));
        result = HashCodeUtil.hash(result, this.getAllAnexosByType(AnexoSSModel.class));
        return result;
    }
}

