/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class Quadro06
extends Quadro06Base {
    public boolean isEmpty() {
        return this.isCampoC1C2Empty() && this.isTabelaIdentificacaoAdquirenteEmpty();
    }

    public boolean isCampoC1C2Empty() {
        return StringUtil.isEmpty(this.getAnexoSSq06B1());
    }

    public boolean isTabelaIdentificacaoAdquirenteEmpty() {
        return this.getAnexoSSq06T1() == null || this.getAnexoSSq06T1().isEmpty();
    }

    public boolean isLinhaRepetida(AnexoSSq06T1_Linha linha, int index) {
        boolean isDuplicada = false;
        EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = this.getAnexoSSq06T1();
        if (!(linha == null || linha.isEmpty() || anexoSSq06T1 == null || anexoSSq06T1.isEmpty())) {
            int searchToIndex = index < anexoSSq06T1.size() ? index : anexoSSq06T1.size();
            for (int i = 0; i < searchToIndex; ++i) {
                AnexoSSq06T1_Linha anexoSSq06T1_Linha = anexoSSq06T1.get(i);
                if (anexoSSq06T1_Linha == null || !Modelo3IRSValidatorUtil.equals(anexoSSq06T1_Linha.getNifPortugues(), linha.getNifPortugues()) || !Modelo3IRSValidatorUtil.equals(anexoSSq06T1_Linha.getPais(), linha.getPais()) || !Modelo3IRSValidatorUtil.equals(anexoSSq06T1_Linha.getNFiscalEstrangeiro(), linha.getNFiscalEstrangeiro())) continue;
                isDuplicada = true;
            }
        }
        return isDuplicada;
    }
}

