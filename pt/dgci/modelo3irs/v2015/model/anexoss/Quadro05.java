/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro05
extends Quadro05Base {
    public boolean isEmpty() {
        return this.getAnexoSSq05C501() == null && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq05C502());
    }
}

