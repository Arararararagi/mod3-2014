/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoSS.qQuadro05";
    public static final String ANEXOSSQ05C501_LINK = "aAnexoSS.qQuadro05.fanexoSSq05C501";
    public static final String ANEXOSSQ05C501 = "anexoSSq05C501";
    private Long anexoSSq05C501;
    public static final String ANEXOSSQ05C502_LINK = "aAnexoSS.qQuadro05.fanexoSSq05C502";
    public static final String ANEXOSSQ05C502 = "anexoSSq05C502";
    private Long anexoSSq05C502;
    public static final String ANEXOSSQ05C1_LINK = "aAnexoSS.qQuadro05.fanexoSSq05C1";
    public static final String ANEXOSSQ05C1 = "anexoSSq05C1";
    private Long anexoSSq05C1;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq05C501() {
        return this.anexoSSq05C501;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq05C501(Long anexoSSq05C501) {
        Long oldValue = this.anexoSSq05C501;
        this.anexoSSq05C501 = anexoSSq05C501;
        this.firePropertyChange("anexoSSq05C501", oldValue, this.anexoSSq05C501);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq05C502() {
        return this.anexoSSq05C502;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq05C502(Long anexoSSq05C502) {
        Long oldValue = this.anexoSSq05C502;
        this.anexoSSq05C502 = anexoSSq05C502;
        this.firePropertyChange("anexoSSq05C502", oldValue, this.anexoSSq05C502);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq05C1() {
        return this.anexoSSq05C1;
    }

    public void setAnexoSSq05C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoSSq05C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoSSq05C1(newValue+=(this.getAnexoSSq05C501() == null ? 0 : this.getAnexoSSq05C501()) + (this.getAnexoSSq05C502() == null ? 0 : this.getAnexoSSq05C502()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq05C1(Long anexoSSq05C1) {
        Long oldValue = this.anexoSSq05C1;
        this.anexoSSq05C1 = anexoSSq05C1;
        this.firePropertyChange("anexoSSq05C1", oldValue, this.anexoSSq05C1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoSSq05C501);
        result = HashCodeUtil.hash(result, this.anexoSSq05C502);
        result = HashCodeUtil.hash(result, this.anexoSSq05C1);
        return result;
    }
}

