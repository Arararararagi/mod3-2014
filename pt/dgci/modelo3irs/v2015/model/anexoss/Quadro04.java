/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro04
extends Quadro04Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq04C401()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq04C402()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq04C403()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq04C404()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq04C405()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq04C406()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoSSq04C407());
    }
}

