/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoSS.qQuadro03";
    public static final String ANEXOSSQ03C05_LINK = "aAnexoSS.qQuadro03.fanexoSSq03C05";
    public static final String ANEXOSSQ03C05 = "anexoSSq03C05";
    private String anexoSSq03C05;
    public static final String ANEXOSSQ03C06_LINK = "aAnexoSS.qQuadro03.fanexoSSq03C06";
    public static final String ANEXOSSQ03C06 = "anexoSSq03C06";
    private Long anexoSSq03C06;
    public static final String ANEXOSSQ03C07_LINK = "aAnexoSS.qQuadro03.fanexoSSq03C07";
    public static final String ANEXOSSQ03C07 = "anexoSSq03C07";
    private Long anexoSSq03C07;
    public static final String ANEXOSSQ03C08_LINK = "aAnexoSS.qQuadro03.fanexoSSq03C08";
    public static final String ANEXOSSQ03C08 = "anexoSSq03C08";
    private Boolean anexoSSq03C08;

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoSSq03C05() {
        return this.anexoSSq03C05;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq03C05(String anexoSSq03C05) {
        String oldValue = this.anexoSSq03C05;
        this.anexoSSq03C05 = anexoSSq03C05;
        this.firePropertyChange("anexoSSq03C05", oldValue, this.anexoSSq03C05);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoSSq03C06() {
        return this.anexoSSq03C06;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq03C06(Long anexoSSq03C06) {
        Long oldValue = this.anexoSSq03C06;
        this.anexoSSq03C06 = anexoSSq03C06;
        this.firePropertyChange("anexoSSq03C06", oldValue, this.anexoSSq03C06);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoSSq03C07() {
        return this.anexoSSq03C07;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq03C07(Long anexoSSq03C07) {
        Long oldValue = this.anexoSSq03C07;
        this.anexoSSq03C07 = anexoSSq03C07;
        this.firePropertyChange("anexoSSq03C07", oldValue, this.anexoSSq03C07);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoSSq03C08() {
        return this.anexoSSq03C08;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq03C08(Boolean anexoSSq03C08) {
        Boolean oldValue = this.anexoSSq03C08;
        this.anexoSSq03C08 = anexoSSq03C08;
        this.firePropertyChange("anexoSSq03C08", oldValue, this.anexoSSq03C08);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoSSq03C05);
        result = HashCodeUtil.hash(result, this.anexoSSq03C06);
        result = HashCodeUtil.hash(result, this.anexoSSq03C07);
        result = HashCodeUtil.hash(result, this.anexoSSq03C08);
        return result;
    }
}

