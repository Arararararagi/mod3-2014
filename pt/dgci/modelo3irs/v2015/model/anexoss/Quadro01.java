/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01Base;

public class Quadro01
extends Quadro01Base {
    public boolean isRegimeSelecionado() {
        return this.isRegimeSimplificadoSelecionado() || this.isRegimeContabilidadeOrganizadaSelecionado();
    }

    public boolean isRegimeSimplificadoSelecionado() {
        return this.getAnexoSSq01B1() != null && this.getAnexoSSq01B1() != false;
    }

    public boolean isRegimeContabilidadeOrganizadaSelecionado() {
        return this.getAnexoSSq01B2() != null && this.getAnexoSSq01B2() != false;
    }

    public boolean isImputacaoRendimentosSelecionado() {
        return this.getAnexoSSq01B3() != null && this.getAnexoSSq01B3() != false;
    }
}

