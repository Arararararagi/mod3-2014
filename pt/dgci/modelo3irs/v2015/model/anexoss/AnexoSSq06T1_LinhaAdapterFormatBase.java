/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoSSq06T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoSSq06T1_Linha>,
WritableTableFormat<AnexoSSq06T1_Linha> {
    @Override
    public boolean isEditable(AnexoSSq06T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoSSq06T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNifPortugues();
            }
            case 2: {
                if (line == null || line.getPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxSS", line.getPais());
            }
            case 3: {
                return line.getNFiscalEstrangeiro();
            }
            case 4: {
                return line.getValor();
            }
        }
        return null;
    }

    @Override
    public AnexoSSq06T1_Linha setColumnValue(AnexoSSq06T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNifPortugues((Long)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setNFiscalEstrangeiro((String)value);
                return line;
            }
            case 4: {
                line.setValor((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF / NIPC Portugu\u00eas";
            }
            case 2: {
                return "Pa\u00eds";
            }
            case 3: {
                return "N.\u00ba Fiscal estrangeiro";
            }
            case 4: {
                return "Valor";
            }
        }
        return null;
    }
}

