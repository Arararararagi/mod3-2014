/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoSSq06T1_LinhaBase
extends Model {
    public static final String NIFPORTUGUES_LINK = "aAnexoSS.qQuadro06.fnifPortugues";
    public static final String NIFPORTUGUES = "nifPortugues";
    private Long nifPortugues;
    public static final String PAIS_LINK = "aAnexoSS.qQuadro06.fpais";
    public static final String PAIS = "pais";
    private Long pais;
    public static final String NFISCALESTRANGEIRO_LINK = "aAnexoSS.qQuadro06.fnFiscalEstrangeiro";
    public static final String NFISCALESTRANGEIRO = "nFiscalEstrangeiro";
    private String nFiscalEstrangeiro;
    public static final String VALOR_LINK = "aAnexoSS.qQuadro06.fvalor";
    public static final String VALOR = "valor";
    private Long valor;

    public AnexoSSq06T1_LinhaBase(Long nifPortugues, Long pais, String nFiscalEstrangeiro, Long valor) {
        this.nifPortugues = nifPortugues;
        this.pais = pais;
        this.nFiscalEstrangeiro = nFiscalEstrangeiro;
        this.valor = valor;
    }

    public AnexoSSq06T1_LinhaBase() {
        this.nifPortugues = null;
        this.pais = null;
        this.nFiscalEstrangeiro = null;
        this.valor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifPortugues() {
        return this.nifPortugues;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifPortugues(Long nifPortugues) {
        Long oldValue = this.nifPortugues;
        this.nifPortugues = nifPortugues;
        this.firePropertyChange("nifPortugues", oldValue, this.nifPortugues);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxSS")
    public Long getPais() {
        return this.pais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setPais(Long pais) {
        Long oldValue = this.pais;
        this.pais = pais;
        this.firePropertyChange("pais", oldValue, this.pais);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getNFiscalEstrangeiro() {
        return this.nFiscalEstrangeiro;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNFiscalEstrangeiro(String nFiscalEstrangeiro) {
        String oldValue = this.nFiscalEstrangeiro;
        this.nFiscalEstrangeiro = nFiscalEstrangeiro;
        this.firePropertyChange("nFiscalEstrangeiro", oldValue, this.nFiscalEstrangeiro);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValor() {
        return this.valor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValor(Long valor) {
        Long oldValue = this.valor;
        this.valor = valor;
        this.firePropertyChange("valor", oldValue, this.valor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nifPortugues);
        result = HashCodeUtil.hash(result, this.pais);
        result = HashCodeUtil.hash(result, this.nFiscalEstrangeiro);
        result = HashCodeUtil.hash(result, this.valor);
        return result;
    }

    public static enum Property {
        NIFPORTUGUES("nifPortugues", 2),
        PAIS("pais", 3),
        NFISCALESTRANGEIRO("nFiscalEstrangeiro", 4),
        VALOR("valor", 5);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

