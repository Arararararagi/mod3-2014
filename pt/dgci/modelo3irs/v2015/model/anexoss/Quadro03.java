/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03Base;

public class Quadro03
extends Quadro03Base {
    public boolean isEmpty() {
        return this.isEmptyAnexoSSq03C08();
    }

    public boolean isEmptyAnexoSSq03C08() {
        return this.getAnexoSSq03C08() == null || this.getAnexoSSq03C08() == false;
    }
}

