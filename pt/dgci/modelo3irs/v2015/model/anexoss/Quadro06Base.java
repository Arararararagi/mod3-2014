/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoSS.qQuadro06";
    public static final String ANEXOSSQ06B1SIM_LINK = "aAnexoSS.qQuadro06.fanexoSSq06B1Sim";
    public static final String ANEXOSSQ06B1SIM_VALUE = "S";
    public static final String ANEXOSSQ06B1NAO_LINK = "aAnexoSS.qQuadro06.fanexoSSq06B1Nao";
    public static final String ANEXOSSQ06B1NAO_VALUE = "N";
    public static final String ANEXOSSQ06B1 = "anexoSSq06B1";
    private String anexoSSq06B1;
    public static final String ANEXOSSQ06T1_LINK = "aAnexoSS.qQuadro06.tanexoSSq06T1";
    private EventList<AnexoSSq06T1_Linha> anexoSSq06T1 = new BasicEventList<AnexoSSq06T1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoSSq06B1() {
        return this.anexoSSq06B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq06B1(String anexoSSq06B1) {
        String oldValue = this.anexoSSq06B1;
        this.anexoSSq06B1 = anexoSSq06B1;
        this.firePropertyChange("anexoSSq06B1", oldValue, this.anexoSSq06B1);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoSSq06T1_Linha> getAnexoSSq06T1() {
        return this.anexoSSq06T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoSSq06T1(EventList<AnexoSSq06T1_Linha> anexoSSq06T1) {
        this.anexoSSq06T1 = anexoSSq06T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoSSq06B1);
        result = HashCodeUtil.hash(result, this.anexoSSq06T1);
        return result;
    }
}

