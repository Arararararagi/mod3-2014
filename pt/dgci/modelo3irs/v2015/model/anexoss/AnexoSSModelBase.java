/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.util.HashCodeUtil;

@Min(value=0)
@Max(value=99)
public class AnexoSSModelBase
extends AnexoModel {
    public static final String ANEXOSS_LINK = "aAnexoSS";

    public AnexoSSModelBase(FormKey key, boolean addQuadrosToModel) {
        super(key);
        if (addQuadrosToModel) {
            this.addQuadro(new Quadro01());
            this.addQuadro(new Quadro02());
            this.addQuadro(new Quadro03());
            this.addQuadro(new Quadro04());
            this.addQuadro(new Quadro05());
            this.addQuadro(new Quadro06());
        }
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro01.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro02.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro03.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro04.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro05.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro06.class.getSimpleName()));
        return result;
    }
}

