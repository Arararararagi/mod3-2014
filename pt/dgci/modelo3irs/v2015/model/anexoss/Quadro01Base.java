/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro01Base
extends QuadroModel {
    public static final String QUADRO01_LINK = "aAnexoSS.qQuadro01";
    public static final String ANEXOSSQ01B1_LINK = "aAnexoSS.qQuadro01.fanexoSSq01B1";
    public static final String ANEXOSSQ01B1 = "anexoSSq01B1";
    private Boolean anexoSSq01B1;
    public static final String ANEXOSSQ01B2_LINK = "aAnexoSS.qQuadro01.fanexoSSq01B2";
    public static final String ANEXOSSQ01B2 = "anexoSSq01B2";
    private Boolean anexoSSq01B2;
    public static final String ANEXOSSQ01B3_LINK = "aAnexoSS.qQuadro01.fanexoSSq01B3";
    public static final String ANEXOSSQ01B3 = "anexoSSq01B3";
    private Boolean anexoSSq01B3;

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoSSq01B1() {
        return this.anexoSSq01B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq01B1(Boolean anexoSSq01B1) {
        Boolean oldValue = this.anexoSSq01B1;
        this.anexoSSq01B1 = anexoSSq01B1;
        this.firePropertyChange("anexoSSq01B1", oldValue, this.anexoSSq01B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoSSq01B2() {
        return this.anexoSSq01B2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq01B2(Boolean anexoSSq01B2) {
        Boolean oldValue = this.anexoSSq01B2;
        this.anexoSSq01B2 = anexoSSq01B2;
        this.firePropertyChange("anexoSSq01B2", oldValue, this.anexoSSq01B2);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoSSq01B3() {
        return this.anexoSSq01B3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq01B3(Boolean anexoSSq01B3) {
        Boolean oldValue = this.anexoSSq01B3;
        this.anexoSSq01B3 = anexoSSq01B3;
        this.firePropertyChange("anexoSSq01B3", oldValue, this.anexoSSq01B3);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoSSq01B1);
        result = HashCodeUtil.hash(result, this.anexoSSq01B2);
        result = HashCodeUtil.hash(result, this.anexoSSq01B3);
        return result;
    }
}

