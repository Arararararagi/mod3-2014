/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSq06T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class AnexoSSq06T1_Linha
extends AnexoSSq06T1_LinhaBase {
    public AnexoSSq06T1_Linha() {
    }

    public AnexoSSq06T1_Linha(Long nifPortugues, Long pais, String nFiscalEstrangeiro, Long valor) {
        super(nifPortugues, pais, nFiscalEstrangeiro, valor);
    }

    public static String getLink(int line) {
        return "aAnexoSS.qQuadro06.tanexoSSq06T1.l" + line;
    }

    public static String getLink(int line, AnexoSSq06T1_LinhaBase.Property column) {
        return "aAnexoSS.qQuadro06.tanexoSSq06T1.l" + line + ".c" + column.getIndex();
    }

    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getNifPortugues()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getPais()) && StringUtil.isEmpty(this.getNFiscalEstrangeiro()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getValor());
    }
}

