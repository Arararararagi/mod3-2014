/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoSS.qQuadro04";
    public static final String ANEXOSSQ04C401_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C401";
    public static final String ANEXOSSQ04C401 = "anexoSSq04C401";
    private Long anexoSSq04C401;
    public static final String ANEXOSSQ04C402_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C402";
    public static final String ANEXOSSQ04C402 = "anexoSSq04C402";
    private Long anexoSSq04C402;
    public static final String ANEXOSSQ04C403_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C403";
    public static final String ANEXOSSQ04C403 = "anexoSSq04C403";
    private Long anexoSSq04C403;
    public static final String ANEXOSSQ04C404_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C404";
    public static final String ANEXOSSQ04C404 = "anexoSSq04C404";
    private Long anexoSSq04C404;
    public static final String ANEXOSSQ04C405_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C405";
    public static final String ANEXOSSQ04C405 = "anexoSSq04C405";
    private Long anexoSSq04C405;
    public static final String ANEXOSSQ04C406_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C406";
    public static final String ANEXOSSQ04C406 = "anexoSSq04C406";
    private Long anexoSSq04C406;
    public static final String ANEXOSSQ04C407_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C407";
    public static final String ANEXOSSQ04C407 = "anexoSSq04C407";
    private Long anexoSSq04C407;
    public static final String ANEXOSSQ04C1_LINK = "aAnexoSS.qQuadro04.fanexoSSq04C1";
    public static final String ANEXOSSQ04C1 = "anexoSSq04C1";
    private Long anexoSSq04C1;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C401() {
        return this.anexoSSq04C401;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C401(Long anexoSSq04C401) {
        Long oldValue = this.anexoSSq04C401;
        this.anexoSSq04C401 = anexoSSq04C401;
        this.firePropertyChange("anexoSSq04C401", oldValue, this.anexoSSq04C401);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C402() {
        return this.anexoSSq04C402;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C402(Long anexoSSq04C402) {
        Long oldValue = this.anexoSSq04C402;
        this.anexoSSq04C402 = anexoSSq04C402;
        this.firePropertyChange("anexoSSq04C402", oldValue, this.anexoSSq04C402);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C403() {
        return this.anexoSSq04C403;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C403(Long anexoSSq04C403) {
        Long oldValue = this.anexoSSq04C403;
        this.anexoSSq04C403 = anexoSSq04C403;
        this.firePropertyChange("anexoSSq04C403", oldValue, this.anexoSSq04C403);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C404() {
        return this.anexoSSq04C404;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C404(Long anexoSSq04C404) {
        Long oldValue = this.anexoSSq04C404;
        this.anexoSSq04C404 = anexoSSq04C404;
        this.firePropertyChange("anexoSSq04C404", oldValue, this.anexoSSq04C404);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C405() {
        return this.anexoSSq04C405;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C405(Long anexoSSq04C405) {
        Long oldValue = this.anexoSSq04C405;
        this.anexoSSq04C405 = anexoSSq04C405;
        this.firePropertyChange("anexoSSq04C405", oldValue, this.anexoSSq04C405);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C406() {
        return this.anexoSSq04C406;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C406(Long anexoSSq04C406) {
        Long oldValue = this.anexoSSq04C406;
        this.anexoSSq04C406 = anexoSSq04C406;
        this.firePropertyChange("anexoSSq04C406", oldValue, this.anexoSSq04C406);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C407() {
        return this.anexoSSq04C407;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C407(Long anexoSSq04C407) {
        Long oldValue = this.anexoSSq04C407;
        this.anexoSSq04C407 = anexoSSq04C407;
        this.firePropertyChange("anexoSSq04C407", oldValue, this.anexoSSq04C407);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoSSq04C1() {
        return this.anexoSSq04C1;
    }

    public void setAnexoSSq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoSSq04C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoSSq04C1(newValue+=(this.getAnexoSSq04C401() == null ? 0 : this.getAnexoSSq04C401()) + (this.getAnexoSSq04C402() == null ? 0 : this.getAnexoSSq04C402()) + (this.getAnexoSSq04C403() == null ? 0 : this.getAnexoSSq04C403()) + (this.getAnexoSSq04C404() == null ? 0 : this.getAnexoSSq04C404()) + (this.getAnexoSSq04C405() == null ? 0 : this.getAnexoSSq04C405()) + (this.getAnexoSSq04C406() == null ? 0 : this.getAnexoSSq04C406()) + (this.getAnexoSSq04C407() == null ? 0 : this.getAnexoSSq04C407()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoSSq04C1(Long anexoSSq04C1) {
        Long oldValue = this.anexoSSq04C1;
        this.anexoSSq04C1 = anexoSSq04C1;
        this.firePropertyChange("anexoSSq04C1", oldValue, this.anexoSSq04C1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoSSq04C401);
        result = HashCodeUtil.hash(result, this.anexoSSq04C402);
        result = HashCodeUtil.hash(result, this.anexoSSq04C403);
        result = HashCodeUtil.hash(result, this.anexoSSq04C404);
        result = HashCodeUtil.hash(result, this.anexoSSq04C405);
        result = HashCodeUtil.hash(result, this.anexoSSq04C406);
        result = HashCodeUtil.hash(result, this.anexoSSq04C407);
        result = HashCodeUtil.hash(result, this.anexoSSq04C1);
        return result;
    }
}

