/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.dgci.modelo3irs.v2015.model.OwnableAnexo;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModelBase;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoss.Quadro06;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=99)
public class AnexoSSModel
extends AnexoSSModelBase
implements OwnableAnexo {
    public AnexoSSModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
        this.postCreate();
    }

    protected void postCreate() {
        this.setOwner(this.formKey.getSubId());
    }

    public void setOwner(String nif) {
        Quadro03 quadro03Model = this.getQuadro03();
        if (quadro03Model != null) {
            quadro03Model.setAnexoSSq03C06(Long.parseLong(nif));
        }
    }

    @Override
    public boolean isOwnedBy(Long nif) {
        return nif != null && nif.equals(this.getQuadro03().getAnexoSSq03C06());
    }

    public Quadro01 getQuadro01() {
        return (Quadro01)this.getQuadro(Quadro01.class.getSimpleName());
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public String getLink(String linkWithoutSubId) {
        return linkWithoutSubId.replaceFirst("aAnexoSS", "aAnexoSS|" + this.getFormKey().getSubId());
    }
}

