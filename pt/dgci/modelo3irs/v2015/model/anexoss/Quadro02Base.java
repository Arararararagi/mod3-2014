/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoss;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoSS.qQuadro02";
    public static final String ANEXOSSQ02C04_LINK = "aAnexoSS.qQuadro02.fanexoSSq02C04";
    public static final String ANEXOSSQ02C04 = "anexoSSq02C04";
    private Long anexoSSq02C04;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos_AnexoSS")
    public Long getAnexoSSq02C04() {
        return this.anexoSSq02C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoSSq02C04(Long anexoSSq02C04) {
        Long oldValue = this.anexoSSq02C04;
        this.anexoSSq02C04 = anexoSSq02C04;
        this.firePropertyChange("anexoSSq02C04", oldValue, this.anexoSSq02C04);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoSSq02C04);
        return result;
    }
}

