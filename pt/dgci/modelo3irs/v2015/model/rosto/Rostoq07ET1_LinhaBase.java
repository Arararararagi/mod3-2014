/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Rostoq07ET1_LinhaBase
extends Model {
    public static final String NIF_LINK = "aRosto.qQuadro07.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;

    public Rostoq07ET1_LinhaBase(Long nIF) {
        this.nIF = nIF;
    }

    public Rostoq07ET1_LinhaBase() {
        this.nIF = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIF);
        return result;
    }

    public static enum Property {
        NIF("nIF", 2);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

