/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aRosto.qQuadro05";
    public static final String Q05B1OP1_LINK = "aRosto.qQuadro05.fq05B1OP1";
    public static final String Q05B1OP1_VALUE = "1";
    public static final String Q05B1OP2_LINK = "aRosto.qQuadro05.fq05B1OP2";
    public static final String Q05B1OP2_VALUE = "2";
    public static final String Q05B1OP3_LINK = "aRosto.qQuadro05.fq05B1OP3";
    public static final String Q05B1OP3_VALUE = "3";
    public static final String Q05B1OP4_LINK = "aRosto.qQuadro05.fq05B1OP4";
    public static final String Q05B1OP4_VALUE = "4";
    public static final String Q05B1 = "q05B1";
    private String q05B1;
    public static final String Q05C5_LINK = "aRosto.qQuadro05.fq05C5";
    public static final String Q05C5 = "q05C5";
    private Long q05C5;
    public static final String Q05C5_1_LINK = "aRosto.qQuadro05.fq05C5_1";
    public static final String Q05C5_1 = "q05C5_1";
    private Long q05C5_1;
    public static final String Q05B2OP6_LINK = "aRosto.qQuadro05.fq05B2OP6";
    public static final String Q05B2OP6_VALUE = "6";
    public static final String Q05B2OP7_LINK = "aRosto.qQuadro05.fq05B2OP7";
    public static final String Q05B2OP7_VALUE = "7";
    public static final String Q05B2 = "q05B2";
    private String q05B2;
    public static final String Q05B3OP8_LINK = "aRosto.qQuadro05.fq05B3OP8";
    public static final String Q05B3OP8_VALUE = "8";
    public static final String Q05B3OP9_LINK = "aRosto.qQuadro05.fq05B3OP9";
    public static final String Q05B3OP9_VALUE = "9";
    public static final String Q05B3 = "q05B3";
    private String q05B3;
    public static final String Q05B4OP10_LINK = "aRosto.qQuadro05.fq05B4OP10";
    public static final String Q05B4OP10_VALUE = "10";
    public static final String Q05B4OP11_LINK = "aRosto.qQuadro05.fq05B4OP11";
    public static final String Q05B4OP11_VALUE = "11";
    public static final String Q05B4 = "q05B4";
    private String q05B4;
    public static final String Q05C12_LINK = "aRosto.qQuadro05.fq05C12";
    public static final String Q05C12 = "q05C12";
    private Long q05C12;
    public static final String Q05C13_LINK = "aRosto.qQuadro05.fq05C13";
    public static final String Q05C13 = "q05C13";
    private Long q05C13;

    @Type(value=Type.TYPE.CAMPO)
    public String getQ05B1() {
        return this.q05B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ05B1(String q05B1) {
        String oldValue = this.q05B1;
        this.q05B1 = q05B1;
        this.firePropertyChange("q05B1", oldValue, this.q05B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ05C5() {
        return this.q05C5;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ05C5(Long q05C5) {
        Long oldValue = this.q05C5;
        this.q05C5 = q05C5;
        this.firePropertyChange("q05C5", oldValue, this.q05C5);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_Rosto")
    public Long getQ05C5_1() {
        return this.q05C5_1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ05C5_1(Long q05C5_1) {
        Long oldValue = this.q05C5_1;
        this.q05C5_1 = q05C5_1;
        this.firePropertyChange("q05C5_1", oldValue, this.q05C5_1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getQ05B2() {
        return this.q05B2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ05B2(String q05B2) {
        String oldValue = this.q05B2;
        this.q05B2 = q05B2;
        this.firePropertyChange("q05B2", oldValue, this.q05B2);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getQ05B3() {
        return this.q05B3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ05B3(String q05B3) {
        String oldValue = this.q05B3;
        this.q05B3 = q05B3;
        this.firePropertyChange("q05B3", oldValue, this.q05B3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getQ05B4() {
        return this.q05B4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ05B4(String q05B4) {
        String oldValue = this.q05B4;
        this.q05B4 = q05B4;
        this.firePropertyChange("q05B4", oldValue, this.q05B4);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getQ05C12() {
        return this.q05C12;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setQ05C12(Long q05C12) {
        Long oldValue = this.q05C12;
        this.q05C12 = q05C12;
        this.firePropertyChange("q05C12", oldValue, this.q05C12);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_RostoQ5B_cp13")
    public Long getQ05C13() {
        return this.q05C13;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ05C13(Long q05C13) {
        Long oldValue = this.q05C13;
        this.q05C13 = q05C13;
        this.firePropertyChange("q05C13", oldValue, this.q05C13);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q05B1);
        result = HashCodeUtil.hash(result, this.q05C5);
        result = HashCodeUtil.hash(result, this.q05C5_1);
        result = HashCodeUtil.hash(result, this.q05B2);
        result = HashCodeUtil.hash(result, this.q05B3);
        result = HashCodeUtil.hash(result, this.q05B4);
        result = HashCodeUtil.hash(result, this.q05C12);
        result = HashCodeUtil.hash(result, this.q05C13);
        return result;
    }
}

