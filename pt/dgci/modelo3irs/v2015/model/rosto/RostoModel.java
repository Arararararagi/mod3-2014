/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro01;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModelBase;
import pt.dgci.modelo3irs.v2015.util.TitularesEnum;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.Rostoq03DT1ChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.Rostoq07CT1ChangeEvent;
import pt.dgci.modelo3irs.v2015.util.observer.Rostoq07ET1ChangeEvent;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=1)
@Max(value=1)
public class RostoModel
extends RostoModelBase {
    private static final long serialVersionUID = -2208417170822522746L;
    private static RostoQuadroChangeEvent rostoq07CT1ChangeEvent = null;
    private static RostoQuadroChangeEvent rostoq07ET1ChangeEvent = null;
    private static RostoQuadroChangeEvent rostoq03DT1ChangeEvent = null;

    public RostoModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
        rostoq07CT1ChangeEvent = new Rostoq07CT1ChangeEvent();
        rostoq07ET1ChangeEvent = new Rostoq07ET1ChangeEvent();
        rostoq03DT1ChangeEvent = new Rostoq03DT1ChangeEvent();
    }

    public RostoQuadroChangeEvent getRostoq07CT1ChangeEvent() {
        return rostoq07CT1ChangeEvent;
    }

    public RostoQuadroChangeEvent getRostoq07ET1ChangeEvent() {
        return rostoq07ET1ChangeEvent;
    }

    public RostoQuadroChangeEvent getRostoq03DT1ChangeEvent() {
        return rostoq03DT1ChangeEvent;
    }

    public Long getNIFTitular(String titular) {
        if ("A".equalsIgnoreCase(titular)) {
            return this.getQuadro03().getQ03C03();
        }
        if ("B".equalsIgnoreCase(titular)) {
            return this.getQuadro03().getQ03C04();
        }
        if (Quadro03.isDependenteNaoDeficiente(titular)) {
            return this.getQuadro03().getNifDependenteNaoDeficiente(titular);
        }
        if (Quadro03.isDependenteDeficiente(titular)) {
            return this.getQuadro03().getNifDependenteDeficiente(titular);
        }
        if (Quadro03.isDependenteEmGuardaConjunta(titular)) {
            return this.getQuadro03().getNifDependentesGuardaConjunta(titular);
        }
        if ("F".equalsIgnoreCase(titular)) {
            return this.getQuadro07().getQ07C1();
        }
        if (Quadro07.isAfilhadosCivisEmComunhao(titular)) {
            return this.getQuadro07().getNifAfilhadosCivisEmComunhao(titular);
        }
        if (Quadro07.isAscendentesEColaterais(titular)) {
            return this.getQuadro07().getNifAscendentesEColaterais(titular);
        }
        if (Quadro07.isAscendentesEmComunhao(titular)) {
            return this.getQuadro07().getNifAscendentesEmComunhao(titular);
        }
        return null;
    }

    public String getTitularByNIF(Long nif) {
        if (this.getQuadro03().isSujeitoPassivoA(nif)) {
            return "A";
        }
        if (this.getQuadro03().isSujeitoPassivoB(nif)) {
            return "B";
        }
        if (this.getQuadro03().existsInDependentesNaoDeficientes(nif)) {
            return this.getQuadro03().getTitularDependenteNaoDeficienteByNIF(nif);
        }
        if (this.getQuadro03().existsInDependentesDeficientes(nif)) {
            return this.getQuadro03().getTitularDependenteDeficienteByNIF(nif);
        }
        if (this.getQuadro03().existsInDependentesEmGuardaConjunta(nif)) {
            return this.getQuadro03().getTitularDependenteGuardaConjunta(nif);
        }
        if (this.getQuadro07().isConjugeFalecido(nif)) {
            return "F";
        }
        if (this.getQuadro07().existsInAfilhadosCivisEmComunhao(nif)) {
            return this.getQuadro07().getTitularAfilhadosCivisEmComunhao(nif);
        }
        if (this.getQuadro07().existsInAscendentesEColaterais(nif)) {
            return this.getQuadro07().getTitularAscendentesEColaterais(nif);
        }
        if (this.getQuadro07().existsInAscendentes(nif)) {
            return this.getQuadro07().getTitularAscendentesEmComunhao(nif);
        }
        return null;
    }

    public boolean isNIFTitularPreenchido(String titular) {
        return this.isNIFTitularPreenchido(titular, null);
    }

    public boolean isNIFTitularPreenchido(String titular, List<TitularesEnum> titulares) {
        List<TitularesEnum> listaTitulares = titulares;
        if (listaTitulares == null) {
            listaTitulares = new ArrayList<TitularesEnum>();
        }
        if (listaTitulares.isEmpty()) {
            listaTitulares.add(TitularesEnum.SPAB);
            listaTitulares.add(TitularesEnum.SPC);
            listaTitulares.add(TitularesEnum.D);
            listaTitulares.add(TitularesEnum.DD);
            listaTitulares.add(TitularesEnum.DG);
            listaTitulares.add(TitularesEnum.AS);
            listaTitulares.add(TitularesEnum.AC);
            listaTitulares.add(TitularesEnum.AF);
            listaTitulares.add(TitularesEnum.F);
        }
        if (listaTitulares.contains((Object)TitularesEnum.SPAB) && "A".equalsIgnoreCase(titular)) {
            return this.getQuadro03().getQ03C03() != null;
        }
        if (listaTitulares.contains((Object)TitularesEnum.SPAB) && "B".equalsIgnoreCase(titular)) {
            return this.getQuadro03().getQ03C04() != null;
        }
        if (listaTitulares.contains((Object)TitularesEnum.SPC) && "C".equalsIgnoreCase(titular)) {
            return this.getQuadro03().getQ03C03() != null && this.getQuadro03().getQ03C04() != null;
        }
        if (listaTitulares.contains((Object)TitularesEnum.D) && Quadro03.isDependenteNaoDeficiente(titular)) {
            return this.getQuadro03().existsInDependentesNaoDeficientes(titular);
        }
        if (listaTitulares.contains((Object)TitularesEnum.DD) && Quadro03.isDependenteDeficiente(titular)) {
            return this.getQuadro03().existsInDependentesDeficientes(titular);
        }
        if (listaTitulares.contains((Object)TitularesEnum.DG) && Quadro03.isDependenteEmGuardaConjunta(titular)) {
            return this.getQuadro03().existsInDependentesEmGuardaConjunta(titular);
        }
        if (listaTitulares.contains((Object)TitularesEnum.AS) && Quadro07.isAscendentesEmComunhao(titular)) {
            return this.getQuadro07().existsInAscendentes(titular);
        }
        if (listaTitulares.contains((Object)TitularesEnum.AC) && Quadro07.isAscendentesEColaterais(titular)) {
            return this.getQuadro07().existsInAscendentesEColaterais(titular);
        }
        if (listaTitulares.contains((Object)TitularesEnum.AF) && Quadro07.isAfilhadosCivisEmComunhao(titular)) {
            return this.getQuadro07().existsInAfilhadosCivisEmComunhao(titular);
        }
        if (listaTitulares.contains((Object)TitularesEnum.F) && "F".equalsIgnoreCase(titular)) {
            return this.getQuadro07().getQ07C1() != null;
        }
        return false;
    }

    @Deprecated
    public List<Cat_M3V2015_RostoTitulares> getTitularesAsCatalogo() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        titularesList.add(new Cat_M3V2015_RostoTitulares(null, null));
        titularesList.addAll(this.getQuadro03().getTodosTitularesAsCatalog());
        titularesList.addAll(this.getQuadro07().getTitularesFalecidosAsCatalog());
        return titularesList;
    }

    @Deprecated
    public List<Cat_M3V2015_RostoTitulares> getTitularesComCAsCatalogo() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        titularesList.add(new Cat_M3V2015_RostoTitulares(null, null));
        titularesList.addAll(this.getQuadro03().getTodosTitularesComCAsCatalog());
        titularesList.addAll(this.getQuadro07().getTitularesFalecidosAsCatalog());
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesAsCatalogo(List<TitularesEnum> titulares) {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        titularesList.add(new Cat_M3V2015_RostoTitulares(null, null));
        for (TitularesEnum tipoTitular : titulares) {
            switch (tipoTitular) {
                case SPAB: {
                    titularesList.addAll(this.getQuadro03().getTitularesABAsCatalog());
                    break;
                }
                case SPC: {
                    titularesList.addAll(this.getQuadro03().getTitularesCAsCatalog());
                    break;
                }
                case D: {
                    titularesList.addAll(this.getQuadro03().getTitularesDependentesNaoDeficientesAsCatalog());
                    break;
                }
                case DD: {
                    titularesList.addAll(this.getQuadro03().getTitularesDependentesDeficientesAsCatalog());
                    break;
                }
                case DG: {
                    titularesList.addAll(this.getQuadro03().getTitularesDependentesEmGuardaConjuntaAsCatalogo());
                    break;
                }
                case AF: {
                    titularesList.addAll(this.getQuadro07().getTitularesAfilhadosCivisEmComunhaoAsCatalog());
                    break;
                }
                case AS: {
                    titularesList.addAll(this.getQuadro07().getTitularesAscendentesEmComunhaoAsCatalogo());
                    break;
                }
                case AC: {
                    titularesList.addAll(this.getQuadro07().getTitularesAscendentesEColateraisAsCatalogo());
                    break;
                }
                case F: {
                    titularesList.addAll(this.getQuadro07().getTitularesFalecidosAsCatalog());
                    break;
                }
            }
        }
        return titularesList;
    }

    public Long getHandicapPercentByNif(Long nif) {
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03C03(), nif)) {
            return this.getQuadro03().getQ03C03a() != null ? this.getQuadro03().getQ03C03a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03C04(), nif)) {
            return this.getQuadro03().getQ03C04a() != null ? this.getQuadro03().getQ03C04a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD1(), nif)) {
            return this.getQuadro03().getQ03CDD1a() != null ? this.getQuadro03().getQ03CDD1a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD2(), nif)) {
            return this.getQuadro03().getQ03CDD2a() != null ? this.getQuadro03().getQ03CDD2a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD3(), nif)) {
            return this.getQuadro03().getQ03CDD3a() != null ? this.getQuadro03().getQ03CDD3a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD4(), nif)) {
            return this.getQuadro03().getQ03CDD4a() != null ? this.getQuadro03().getQ03CDD4a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD5(), nif)) {
            return this.getQuadro03().getQ03CDD5a() != null ? this.getQuadro03().getQ03CDD5a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD6(), nif)) {
            return this.getQuadro03().getQ03CDD6a() != null ? this.getQuadro03().getQ03CDD6a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD7(), nif)) {
            return this.getQuadro03().getQ03CDD7a() != null ? this.getQuadro03().getQ03CDD7a() : new Long(0);
        }
        if (Modelo3IRSValidatorUtil.equals(this.getQuadro03().getQ03CDD8(), nif)) {
            return this.getQuadro03().getQ03CDD8a() != null ? this.getQuadro03().getQ03CDD8a() : new Long(0);
        }
        return new Long(-1);
    }

    public String getLinkByTitular(String titular) {
        if ("A".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03C03";
        }
        if ("B".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03C04";
        }
        if (Quadro03.isDependenteNaoDeficiente(titular)) {
            return Quadro03.getLinkToDependenteNaoDeficiente(titular);
        }
        if (Quadro03.isDependenteDeficiente(titular)) {
            return Quadro03.getLinkToDependenteDeficiente(titular);
        }
        if (Quadro03.isDependenteEmGuardaConjunta(titular)) {
            return this.getQuadro03().getLinkToDependentesGuardaConjunta(titular);
        }
        if ("F".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro07.fq07C1";
        }
        if (Quadro07.isAfilhadosCivisEmComunhao(titular)) {
            return this.getQuadro07().getLinkToAfilhadosCivisEmComunhao(titular);
        }
        if (Quadro07.isAscendentesEColaterais(titular)) {
            return this.getQuadro07().getLinkToAscendentesEColaterais(titular);
        }
        if (Quadro07.isAscendentesEmComunhao(titular)) {
            return Quadro07.getLinkToAscendentesEmComunhao(titular);
        }
        return "";
    }

    public String getLinkByNif(Long nif) {
        if (this.getQuadro03().isSujeitoPassivoA(nif)) {
            return "aRosto.qQuadro03.fq03C03";
        }
        if (this.getQuadro03().isSujeitoPassivoB(nif)) {
            return "aRosto.qQuadro03.fq03C04";
        }
        if (this.getQuadro03().existsInDependentesNaoDeficientes(nif)) {
            return this.getQuadro03().getLinkToDependenteNaoDeficienteByNif(nif);
        }
        if (this.getQuadro03().existsInDependentesDeficientes(nif)) {
            return this.getQuadro03().getLinkToDependenteDeficienteByNif(nif);
        }
        if (this.getQuadro03().existsInDependentesEmGuardaConjunta(nif)) {
            return this.getQuadro03().getLinkToDependenteGuardaConjuntaByNif(nif);
        }
        if (this.getQuadro07().isConjugeFalecido(nif)) {
            return "aRosto.qQuadro07.fq07C1";
        }
        if (this.getQuadro07().existsInAfilhadosCivisEmComunhao(nif)) {
            return this.getQuadro07().getLinkToAfilhadosCivisEmComunhaoByNif(nif);
        }
        if (this.getQuadro07().existsInAscendentesEColaterais(nif)) {
            return this.getQuadro07().getLinkToAscendentesEColateraisByNif(nif);
        }
        if (this.getQuadro07().existsInAscendentes(nif)) {
            return this.getQuadro07().getLinkToAscendentesEmComunhaoByNif(nif);
        }
        return "";
    }

    public String getLinkByNifForDependente(Long nif) {
        return this.getLinkByNifForDependente(nif, "");
    }

    public String getLinkByNifForDependente(Long nif, String defaultLink) {
        String link = null;
        if (this.getQuadro03().existsInDependentesNaoDeficientes(nif)) {
            link = this.getQuadro03().getLinkToDependenteNaoDeficienteByNif(nif);
        } else if (this.getQuadro03().existsInDependentesDeficientes(nif)) {
            link = this.getQuadro03().getLinkToDependenteDeficienteByNif(nif);
        } else if (this.getQuadro03().existsInDependentesEmGuardaConjunta(nif)) {
            link = this.getQuadro03().getLinkToDependenteGuardaConjuntaByNif(nif);
        }
        if (link == null || "".equals(link)) {
            link = defaultLink;
        }
        return link;
    }

    public String getLinkByNifForAscendentes(Long nif) {
        return this.getLinkByNifForAscendentes(nif, "");
    }

    public String getLinkByNifForAscendentes(Long nif, String defaultLink) {
        String link = null;
        if (this.getQuadro07().existsInAscendentes(nif)) {
            link = this.getQuadro07().getLinkToAscendentesEmComunhaoByNif(nif);
        }
        if (link == null || "".equals(link)) {
            link = defaultLink;
        }
        return link;
    }

    @Override
    public int compareTo(AnexoModel anexoToCompare) {
        return -1;
    }

    public QuadroInicio getQuadroInicio() {
        return (QuadroInicio)this.getQuadro(QuadroInicio.class.getSimpleName());
    }

    public Quadro01 getQuadro01() {
        return (Quadro01)this.getQuadro(Quadro01.class.getSimpleName());
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public Quadro07 getQuadro07() {
        return (Quadro07)this.getQuadro(Quadro07.class.getSimpleName());
    }

    public Quadro09 getQuadro09() {
        return (Quadro09)this.getQuadro(Quadro09.class.getSimpleName());
    }

    public void addTitularesPropertyChangeListener(PropertyChangeListener listener) {
        Quadro03 quadro03 = this.getQuadro03();
        Quadro07 quadro07 = this.getQuadro07();
        quadro03.addPropertyChangeListener("q03C03", listener);
        quadro03.addPropertyChangeListener("q03C04", listener);
        quadro03.addPropertyChangeListener("q03CD1", listener);
        quadro03.addPropertyChangeListener("q03CD2", listener);
        quadro03.addPropertyChangeListener("q03CD3", listener);
        quadro03.addPropertyChangeListener("q03CD4", listener);
        quadro03.addPropertyChangeListener("q03CD5", listener);
        quadro03.addPropertyChangeListener("q03CD6", listener);
        quadro03.addPropertyChangeListener("q03CD7", listener);
        quadro03.addPropertyChangeListener("q03CD8", listener);
        quadro03.addPropertyChangeListener("q03CD9", listener);
        quadro03.addPropertyChangeListener("q03CD10", listener);
        quadro03.addPropertyChangeListener("q03CD11", listener);
        quadro03.addPropertyChangeListener("q03CD12", listener);
        quadro03.addPropertyChangeListener("q03CD13", listener);
        quadro03.addPropertyChangeListener("q03CD14", listener);
        quadro03.addPropertyChangeListener("q03CDD1", listener);
        quadro03.addPropertyChangeListener("q03CDD2", listener);
        quadro03.addPropertyChangeListener("q03CDD3", listener);
        quadro03.addPropertyChangeListener("q03CDD4", listener);
        quadro03.addPropertyChangeListener("q03CDD5", listener);
        quadro03.addPropertyChangeListener("q03CDD6", listener);
        quadro03.addPropertyChangeListener("q03CDD7", listener);
        quadro03.addPropertyChangeListener("q03CDD8", listener);
        quadro07.addPropertyChangeListener("q07C1", listener);
    }

}

