/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aRosto.qQuadro04";
    public static final String Q04B1OP1_LINK = "aRosto.qQuadro04.fq04B1OP1";
    public static final String Q04B1OP1_VALUE = "1";
    public static final String Q04B1OP2_LINK = "aRosto.qQuadro04.fq04B1OP2";
    public static final String Q04B1OP2_VALUE = "2";
    public static final String Q04B1 = "q04B1";
    private String q04B1;

    @Type(value=Type.TYPE.CAMPO)
    public String getQ04B1() {
        return this.q04B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ04B1(String q04B1) {
        String oldValue = this.q04B1;
        this.q04B1 = q04B1;
        this.firePropertyChange("q04B1", oldValue, this.q04B1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q04B1);
        return result;
    }
}

