/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro01Base
extends QuadroModel {
    public static final String QUADRO01_LINK = "aRosto.qQuadro01";
    public static final String Q01C01_LINK = "aRosto.qQuadro01.fq01C01";
    public static final String Q01C01 = "q01C01";
    private Long q01C01;

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ01C01() {
        return this.q01C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ01C01(Long q01C01) {
        Long oldValue = this.q01C01;
        this.q01C01 = q01C01;
        this.firePropertyChange("q01C01", oldValue, this.q01C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q01C01);
        return result;
    }
}

