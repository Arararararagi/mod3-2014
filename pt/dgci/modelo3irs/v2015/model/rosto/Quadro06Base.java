/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aRosto.qQuadro06";
    public static final String Q06B1OP1_LINK = "aRosto.qQuadro06.fq06B1OP1";
    public static final String Q06B1OP1_VALUE = "1";
    public static final String Q06B1OP2_LINK = "aRosto.qQuadro06.fq06B1OP2";
    public static final String Q06B1OP2_VALUE = "2";
    public static final String Q06B1OP3_LINK = "aRosto.qQuadro06.fq06B1OP3";
    public static final String Q06B1OP3_VALUE = "3";
    public static final String Q06B1OP4_LINK = "aRosto.qQuadro06.fq06B1OP4";
    public static final String Q06B1OP4_VALUE = "4";
    public static final String Q06B1 = "q06B1";
    private String q06B1;

    @Type(value=Type.TYPE.CAMPO)
    public String getQ06B1() {
        return this.q06B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ06B1(String q06B1) {
        String oldValue = this.q06B1;
        this.q06B1 = q06B1;
        this.firePropertyChange("q06B1", oldValue, this.q06B1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q06B1);
        return result;
    }
}

