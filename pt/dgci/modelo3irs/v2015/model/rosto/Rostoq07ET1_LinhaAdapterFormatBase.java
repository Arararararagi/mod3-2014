/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;

public class Rostoq07ET1_LinhaAdapterFormatBase
implements AdvancedTableFormat<Rostoq07ET1_Linha>,
WritableTableFormat<Rostoq07ET1_Linha> {
    @Override
    public boolean isEditable(Rostoq07ET1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(Rostoq07ET1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNIF();
            }
        }
        return null;
    }

    @Override
    public Rostoq07ET1_Linha setColumnValue(Rostoq07ET1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNIF((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF";
            }
        }
        return null;
    }
}

