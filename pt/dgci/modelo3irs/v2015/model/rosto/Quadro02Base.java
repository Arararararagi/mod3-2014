/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aRosto.qQuadro02";
    public static final String Q02C02_LINK = "aRosto.qQuadro02.fq02C02";
    public static final String Q02C02 = "q02C02";
    private Long q02C02;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getQ02C02() {
        return this.q02C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ02C02(Long q02C02) {
        Long oldValue = this.q02C02;
        this.q02C02 = q02C02;
        this.firePropertyChange("q02C02", oldValue, this.q02C02);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q02C02);
        return result;
    }
}

