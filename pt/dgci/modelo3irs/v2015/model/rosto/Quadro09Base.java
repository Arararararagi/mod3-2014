/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;
import pt.opensoft.util.Date;

public class Quadro09Base
extends QuadroModel {
    public static final String QUADRO09_LINK = "aRosto.qQuadro09";
    public static final String Q09B1OP1_LINK = "aRosto.qQuadro09.fq09B1OP1";
    public static final String Q09B1OP1_VALUE = "1";
    public static final String Q09B1OP2_LINK = "aRosto.qQuadro09.fq09B1OP2";
    public static final String Q09B1OP2_VALUE = "2";
    public static final String Q09B1 = "q09B1";
    private String q09B1;
    public static final String Q09C03_LINK = "aRosto.qQuadro09.fq09C03";
    public static final String Q09C03 = "q09C03";
    private Date q09C03;

    @Type(value=Type.TYPE.CAMPO)
    public String getQ09B1() {
        return this.q09B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ09B1(String q09B1) {
        String oldValue = this.q09B1;
        this.q09B1 = q09B1;
        this.firePropertyChange("q09B1", oldValue, this.q09B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Date getQ09C03() {
        return this.q09C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ09C03(Date q09C03) {
        Date oldValue = this.q09C03;
        this.q09C03 = q09C03;
        this.firePropertyChange("q09C03", oldValue, this.q09C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q09B1);
        result = HashCodeUtil.hash(result, this.q09C03);
        return result;
    }
}

