/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Rostoq03DT1_LinhaBase
extends Model {
    public static final String NIF_LINK = "aRosto.qQuadro03.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;
    public static final String DEFICIENTEGRAU_LINK = "aRosto.qQuadro03.fdeficienteGrau";
    public static final String DEFICIENTEGRAU = "deficienteGrau";
    private Long deficienteGrau;
    public static final String NIFPROGENITOR_LINK = "aRosto.qQuadro03.fnifProgenitor";
    public static final String NIFPROGENITOR = "nifProgenitor";
    private Long nifProgenitor;

    public Rostoq03DT1_LinhaBase(Long nIF, Long deficienteGrau, Long nifProgenitor) {
        this.nIF = nIF;
        this.deficienteGrau = deficienteGrau;
        this.nifProgenitor = nifProgenitor;
    }

    public Rostoq03DT1_LinhaBase() {
        this.nIF = null;
        this.deficienteGrau = null;
        this.nifProgenitor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getDeficienteGrau() {
        return this.deficienteGrau;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setDeficienteGrau(Long deficienteGrau) {
        Long oldValue = this.deficienteGrau;
        this.deficienteGrau = deficienteGrau;
        this.firePropertyChange("deficienteGrau", oldValue, this.deficienteGrau);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifProgenitor() {
        return this.nifProgenitor;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifProgenitor(Long nifProgenitor) {
        Long oldValue = this.nifProgenitor;
        this.nifProgenitor = nifProgenitor;
        this.firePropertyChange("nifProgenitor", oldValue, this.nifProgenitor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.deficienteGrau);
        result = HashCodeUtil.hash(result, this.nifProgenitor);
        return result;
    }

    public static enum Property {
        NIF("nIF", 2),
        DEFICIENTEGRAU("deficienteGrau", 3),
        NIFPROGENITOR("nifProgenitor", 4);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

