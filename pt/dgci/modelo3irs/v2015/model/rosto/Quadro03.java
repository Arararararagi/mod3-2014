/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.EventList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03Base;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class Quadro03
extends Quadro03Base {
    public static final String SUJEITO_A = "A";
    public static final String SUJEITO_B = "B";
    public static final String SUJEITO_C = "C";
    public static final String DEPENDENTE_1 = "D1";
    public static final String DEPENDENTE_2 = "D2";
    public static final String DEPENDENTE_3 = "D3";
    public static final String DEPENDENTE_4 = "D4";
    public static final String DEPENDENTE_5 = "D5";
    public static final String DEPENDENTE_6 = "D6";
    public static final String DEPENDENTE_7 = "D7";
    public static final String DEPENDENTE_8 = "D8";
    public static final String DEPENDENTE_9 = "D9";
    public static final String DEPENDENTE_10 = "D10";
    public static final String DEPENDENTE_11 = "D11";
    public static final String DEPENDENTE_12 = "D12";
    public static final String DEPENDENTE_13 = "D13";
    public static final String DEPENDENTE_14 = "D14";
    public static final String DEPENDENTE_DEFICIENTE_1 = "DD1";
    public static final String DEPENDENTE_DEFICIENTE_2 = "DD2";
    public static final String DEPENDENTE_DEFICIENTE_3 = "DD3";
    public static final String DEPENDENTE_DEFICIENTE_4 = "DD4";
    public static final String DEPENDENTE_DEFICIENTE_5 = "DD5";
    public static final String DEPENDENTE_DEFICIENTE_6 = "DD6";
    public static final String DEPENDENTE_DEFICIENTE_7 = "DD7";
    public static final String DEPENDENTE_DEFICIENTE_8 = "DD8";
    public static final String DEPENDENTES_GUARDA_CONJUNTA = "DG";
    public static final String[] COD_TITULARES_DEPENDENTES = new String[]{"D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10", "D11", "D12", "D13", "D14"};
    public static final String[] COD_TITULARES_DEPENDENTES_DEFICIENTES = new String[]{"DD1", "DD2", "DD3", "DD4", "DD5", "DD6", "DD7", "DD8"};

    public long getNumDependentesNaoDeficientesPreenchidos() {
        long numDependentes = 0;
        if (this.getQ03CD1() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD2() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD3() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD4() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD5() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD6() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD7() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD8() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD9() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD10() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD11() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD12() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD13() != null) {
            ++numDependentes;
        }
        if (this.getQ03CD14() != null) {
            ++numDependentes;
        }
        return numDependentes;
    }

    public long getNumDependentesDeficientesPreenchidos() {
        int numDependentes = 0;
        if (this.getQ03CDD1() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        if (this.getQ03CDD2() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        if (this.getQ03CDD3() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        if (this.getQ03CDD4() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        if (this.getQ03CDD5() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        if (this.getQ03CDD6() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        if (this.getQ03CDD7() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        if (this.getQ03CDD8() != null) {
            numDependentes = (byte)(numDependentes + 1);
        }
        return numDependentes;
    }

    public boolean isSujeitoPassivoA(Long nif) {
        return this.getQ03C03() != null && this.getQ03C03().equals(nif);
    }

    public boolean isSujeitoPassivoB(Long nif) {
        return this.getQ03C04() != null && this.getQ03C04().equals(nif);
    }

    public static boolean isSujeitoPassivoA(String titular) {
        return "A".equals(titular);
    }

    public static boolean isSujeitoPassivoB(String titular) {
        return "B".equals(titular);
    }

    public static boolean isSujeitoPassivoC(String titular) {
        return "C".equals(titular);
    }

    public int getCountTodosDependentesDeficientes() {
        return this.getCountDependentesDeficientes() + this.getCountDependentesDeficientesGuardaConjunta();
    }

    public int getCountDependentesNaoDeficientes() {
        int count = 0;
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD1())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD2())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD3())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD4())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD5())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD6())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD7())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD8())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD9())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD10())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD11())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD12())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD13())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CD14())) {
            ++count;
        }
        return count;
    }

    public int getCountDependentesDeficientes() {
        int count = 0;
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD1())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD2())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD3())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD4())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD5())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD6())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD7())) {
            ++count;
        }
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ03CDD8())) {
            ++count;
        }
        return count;
    }

    public int getCountDependentes() {
        return this.getCountDependentesDeficientes() + this.getCountDependentesNaoDeficientes();
    }

    public int getCountDependentesGuardaConjunta() {
        return this.getRostoq03DT1() != null ? this.getRostoq03DT1().size() : 0;
    }

    public int getCountDependentesDeficientesGuardaConjunta() {
        int count = 0;
        for (Rostoq03DT1_Linha linha : this.getRostoq03DT1()) {
            if (linha == null || linha.getDeficienteGrau() == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau())) continue;
            ++count;
        }
        return count;
    }

    public boolean existsInDependentes(Long nif) {
        return this.existsInDependentes(nif, -1, -1);
    }

    public boolean existsInDependentesNaoDeficientes(Long nif) {
        return this.existsInDependentesNaoDeficientes(nif, -1);
    }

    public boolean existsInDependentesDeficientes(Long nif) {
        return this.existsInDependentesDeficientes(nif, -1);
    }

    public boolean existsInDependentes(Long nif, int nDep, int nDepDef) {
        boolean existsInD = this.existsInDependentesNaoDeficientes(nif, nDep);
        boolean existsInDD = this.existsInDependentesDeficientes(nif, nDepDef);
        return existsInD || existsInDD;
    }

    private boolean existsInDependentesNaoDeficientes(Long nif, int nDep) {
        if (nDep != 1 && this.getQ03CD1() != null && this.getQ03CD1().equals(nif)) {
            return true;
        }
        if (nDep != 2 && this.getQ03CD2() != null && this.getQ03CD2().equals(nif)) {
            return true;
        }
        if (nDep != 3 && this.getQ03CD3() != null && this.getQ03CD3().equals(nif)) {
            return true;
        }
        if (nDep != 4 && this.getQ03CD4() != null && this.getQ03CD4().equals(nif)) {
            return true;
        }
        if (nDep != 5 && this.getQ03CD5() != null && this.getQ03CD5().equals(nif)) {
            return true;
        }
        if (nDep != 6 && this.getQ03CD6() != null && this.getQ03CD6().equals(nif)) {
            return true;
        }
        if (nDep != 7 && this.getQ03CD7() != null && this.getQ03CD7().equals(nif)) {
            return true;
        }
        if (nDep != 8 && this.getQ03CD8() != null && this.getQ03CD8().equals(nif)) {
            return true;
        }
        if (nDep != 9 && this.getQ03CD9() != null && this.getQ03CD9().equals(nif)) {
            return true;
        }
        if (nDep != 10 && this.getQ03CD10() != null && this.getQ03CD10().equals(nif)) {
            return true;
        }
        if (nDep != 11 && this.getQ03CD11() != null && this.getQ03CD11().equals(nif)) {
            return true;
        }
        if (nDep != 12 && this.getQ03CD12() != null && this.getQ03CD12().equals(nif)) {
            return true;
        }
        if (nDep != 13 && this.getQ03CD13() != null && this.getQ03CD13().equals(nif)) {
            return true;
        }
        if (nDep != 14 && this.getQ03CD14() != null && this.getQ03CD14().equals(nif)) {
            return true;
        }
        return false;
    }

    private boolean existsInDependentesDeficientes(Long nif, int nDepDef) {
        if (nDepDef != 1 && this.getQ03CDD1() != null && this.getQ03CDD1().equals(nif)) {
            return true;
        }
        if (nDepDef != 2 && this.getQ03CDD2() != null && this.getQ03CDD2().equals(nif)) {
            return true;
        }
        if (nDepDef != 3 && this.getQ03CDD3() != null && this.getQ03CDD3().equals(nif)) {
            return true;
        }
        if (nDepDef != 4 && this.getQ03CDD4() != null && this.getQ03CDD4().equals(nif)) {
            return true;
        }
        if (nDepDef != 5 && this.getQ03CDD5() != null && this.getQ03CDD5().equals(nif)) {
            return true;
        }
        if (nDepDef != 6 && this.getQ03CDD6() != null && this.getQ03CDD6().equals(nif)) {
            return true;
        }
        if (nDepDef != 7 && this.getQ03CDD7() != null && this.getQ03CDD7().equals(nif)) {
            return true;
        }
        if (nDepDef != 8 && this.getQ03CDD8() != null && this.getQ03CDD8().equals(nif)) {
            return true;
        }
        return false;
    }

    public boolean existsInDependentesNaoDeficientes(String titular) {
        if ("D1".equalsIgnoreCase(titular)) {
            return this.getQ03CD1() != null;
        }
        if ("D2".equalsIgnoreCase(titular)) {
            return this.getQ03CD2() != null;
        }
        if ("D3".equalsIgnoreCase(titular)) {
            return this.getQ03CD3() != null;
        }
        if ("D4".equalsIgnoreCase(titular)) {
            return this.getQ03CD4() != null;
        }
        if ("D5".equalsIgnoreCase(titular)) {
            return this.getQ03CD5() != null;
        }
        if ("D6".equalsIgnoreCase(titular)) {
            return this.getQ03CD6() != null;
        }
        if ("D7".equalsIgnoreCase(titular)) {
            return this.getQ03CD7() != null;
        }
        if ("D8".equalsIgnoreCase(titular)) {
            return this.getQ03CD8() != null;
        }
        if ("D9".equalsIgnoreCase(titular)) {
            return this.getQ03CD9() != null;
        }
        if ("D10".equalsIgnoreCase(titular)) {
            return this.getQ03CD10() != null;
        }
        if ("D11".equalsIgnoreCase(titular)) {
            return this.getQ03CD11() != null;
        }
        if ("D12".equalsIgnoreCase(titular)) {
            return this.getQ03CD12() != null;
        }
        if ("D13".equalsIgnoreCase(titular)) {
            return this.getQ03CD13() != null;
        }
        if ("D14".equalsIgnoreCase(titular)) {
            return this.getQ03CD14() != null;
        }
        return false;
    }

    public boolean existsInDependentesDeficientes(String titular) {
        if ("DD1".equalsIgnoreCase(titular)) {
            return this.getQ03CDD1() != null;
        }
        if ("DD2".equalsIgnoreCase(titular)) {
            return this.getQ03CDD2() != null;
        }
        if ("DD3".equalsIgnoreCase(titular)) {
            return this.getQ03CDD3() != null;
        }
        if ("DD4".equalsIgnoreCase(titular)) {
            return this.getQ03CDD4() != null;
        }
        if ("DD5".equalsIgnoreCase(titular)) {
            return this.getQ03CDD5() != null;
        }
        if ("DD6".equalsIgnoreCase(titular)) {
            return this.getQ03CDD6() != null;
        }
        if ("DD7".equalsIgnoreCase(titular)) {
            return this.getQ03CDD7() != null;
        }
        if ("DD8".equalsIgnoreCase(titular)) {
            return this.getQ03CDD8() != null;
        }
        return false;
    }

    public static boolean isDependenteNaoDeficiente(String titular) {
        return titular != null && StringUtil.in(titular, COD_TITULARES_DEPENDENTES);
    }

    public static boolean isDependenteDeficiente(String titular) {
        return titular != null && StringUtil.in(titular, COD_TITULARES_DEPENDENTES_DEFICIENTES);
    }

    public boolean existsInDependentesEmGuardaConjunta(Long nif) {
        for (Rostoq03DT1_Linha linha : this.getRostoq03DT1()) {
            if (linha.getNIF() == null || !linha.getNIF().equals(nif)) continue;
            return true;
        }
        return false;
    }

    public boolean existsInDependentesEmGuardaConjunta(String titular) {
        int idx = Modelo3IRSValidatorUtil.getRowNumberFromTitular(titular, "DG");
        return idx > 0 && this.getRostoq03DT1() != null && this.getRostoq03DT1().size() >= idx && this.getRostoq03DT1().get(idx - 1) != null && this.getRostoq03DT1().get(idx - 1).getNIF() != null;
    }

    public static boolean isDependenteEmGuardaConjunta(String titular) {
        return titular != null && titular.startsWith("DG");
    }

    public boolean existsInDependentesEmGuardaConjuntaOutro(Long nif) {
        for (Rostoq03DT1_Linha linha : this.getRostoq03DT1()) {
            if (linha.getNifProgenitor() == null || !linha.getNifProgenitor().equals(nif)) continue;
            return true;
        }
        return false;
    }

    @Deprecated
    public List<Cat_M3V2015_RostoTitulares> getTodosTitularesAsCatalog() {
        return this.getTodosTitularesAsCatalog(false);
    }

    @Deprecated
    public List<Cat_M3V2015_RostoTitulares> getTodosTitularesComCAsCatalog() {
        return this.getTodosTitularesAsCatalog(true);
    }

    @Deprecated
    private List<Cat_M3V2015_RostoTitulares> getTodosTitularesAsCatalog(boolean includeC) {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        titularesList.addAll(this.getTitularesABAsCatalog());
        if (includeC) {
            titularesList.addAll(this.getTitularesCAsCatalog());
        }
        titularesList.addAll(this.getTitularesDependentesNaoDeficientesAsCatalog());
        titularesList.addAll(this.getTitularesDependentesDeficientesAsCatalog());
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesABAsCatalog() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        if (this.getQ03C03() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03C03(), "A"));
        }
        if (this.getQ03C04() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03C04(), "B"));
        }
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesCAsCatalog() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        if (this.getQ03C03() != null && this.getQ03C04() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03C04(), "C"));
        }
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesDependentesNaoDeficientesAsCatalog() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        if (this.getQ03CD1() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD1(), "D1"));
        }
        if (this.getQ03CD2() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD2(), "D2"));
        }
        if (this.getQ03CD3() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD3(), "D3"));
        }
        if (this.getQ03CD4() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD4(), "D4"));
        }
        if (this.getQ03CD5() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD5(), "D5"));
        }
        if (this.getQ03CD6() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD6(), "D6"));
        }
        if (this.getQ03CD7() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD7(), "D7"));
        }
        if (this.getQ03CD8() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD8(), "D8"));
        }
        if (this.getQ03CD9() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD9(), "D9"));
        }
        if (this.getQ03CD10() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD10(), "D10"));
        }
        if (this.getQ03CD11() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD11(), "D11"));
        }
        if (this.getQ03CD12() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD12(), "D12"));
        }
        if (this.getQ03CD13() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD13(), "D13"));
        }
        if (this.getQ03CD14() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CD14(), "D14"));
        }
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesDependentesDeficientesAsCatalog() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        if (this.getQ03CDD1() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD1(), "DD1"));
        }
        if (this.getQ03CDD2() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD2(), "DD2"));
        }
        if (this.getQ03CDD3() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD3(), "DD3"));
        }
        if (this.getQ03CDD4() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD4(), "DD4"));
        }
        if (this.getQ03CDD5() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD5(), "DD5"));
        }
        if (this.getQ03CDD6() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD6(), "DD6"));
        }
        if (this.getQ03CDD7() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD7(), "DD7"));
        }
        if (this.getQ03CDD8() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ03CDD8(), "DD8"));
        }
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesDependentesEmGuardaConjuntaAsCatalogo() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        EventList<Rostoq03DT1_Linha> table = this.getRostoq03DT1();
        if (table != null) {
            for (int i = 0; i < table.size(); ++i) {
                Rostoq03DT1_Linha linha = table.get(i);
                if (linha.getNIF() == null) continue;
                titularesList.add(new Cat_M3V2015_RostoTitulares(linha.getNIF(), "DG" + (i + 1)));
            }
        }
        return titularesList;
    }

    public NifsDeficienciaAssoc[] getNifsDepDeficienciaList() {
        long nifA = this.getQ03C03() != null ? this.getQ03C03() : 0;
        long nifADef = this.getQ03C03a() != null ? this.getQ03C03a() : 0;
        long nifB = this.getQ03C04() != null ? this.getQ03C04() : 0;
        long nifBDef = this.getQ03C04a() != null ? this.getQ03C04a() : 0;
        long nifDD1 = this.getQ03CDD1() != null ? this.getQ03CDD1() : 0;
        long nifDD1Def = this.getQ03CDD1a() != null ? this.getQ03CDD1a() : 0;
        long nifDD2 = this.getQ03CDD2() != null ? this.getQ03CDD2() : 0;
        long nifDD2Def = this.getQ03CDD2a() != null ? this.getQ03CDD2a() : 0;
        long nifDD3 = this.getQ03CDD3() != null ? this.getQ03CDD3() : 0;
        long nifDD3Def = this.getQ03CDD3a() != null ? this.getQ03CDD3a() : 0;
        long nifDD4 = this.getQ03CDD4() != null ? this.getQ03CDD4() : 0;
        long nifDD4Def = this.getQ03CDD4a() != null ? this.getQ03CDD4a() : 0;
        long nifDD5 = this.getQ03CDD5() != null ? this.getQ03CDD5() : 0;
        long nifDD5Def = this.getQ03CDD5a() != null ? this.getQ03CDD5a() : 0;
        long nifDD6 = this.getQ03CDD6() != null ? this.getQ03CDD6() : 0;
        long nifDD6Def = this.getQ03CDD6a() != null ? this.getQ03CDD6a() : 0;
        long nifDD7 = this.getQ03CDD7() != null ? this.getQ03CDD7() : 0;
        long nifDD7Def = this.getQ03CDD7a() != null ? this.getQ03CDD7a() : 0;
        long nifDD8 = this.getQ03CDD8() != null ? this.getQ03CDD8() : 0;
        long nifDD8Def = this.getQ03CDD8a() != null ? this.getQ03CDD8a() : 0;
        return new NifsDeficienciaAssoc[]{new NifsDeficienciaAssoc(nifA, nifADef), new NifsDeficienciaAssoc(nifB, nifBDef), new NifsDeficienciaAssoc(nifDD1, nifDD1Def), new NifsDeficienciaAssoc(nifDD2, nifDD2Def), new NifsDeficienciaAssoc(nifDD3, nifDD3Def), new NifsDeficienciaAssoc(nifDD4, nifDD4Def), new NifsDeficienciaAssoc(nifDD5, nifDD5Def), new NifsDeficienciaAssoc(nifDD6, nifDD6Def), new NifsDeficienciaAssoc(nifDD7, nifDD7Def), new NifsDeficienciaAssoc(nifDD8, nifDD8Def)};
    }

    public static String getLinkRowForROSTOQ03DT1_LINK(int linhaPos) {
        return "aRosto.qQuadro03.trostoq03DT1.l" + linhaPos;
    }

    public static String getLinkCellForROSTOQ03DT1_LINK(int linhaPos, Rostoq03DT1_LinhaBase.Property property) {
        int index = property.getIndex();
        return Quadro03.getLinkRowForROSTOQ03DT1_LINK(linhaPos) + ".c" + index;
    }

    public Rostoq03DT1_Linha getRostoq03DT1LinhaByLabel(String titular) {
        int idx = this.getIndiceTitularDependenteEmGuardaConjunta(titular);
        if (idx > 0) {
            return this.getRostoq03DT1().get(idx - 1);
        }
        return null;
    }

    private int getIndiceTitularDependenteEmGuardaConjunta(String titular) {
        int i;
        int idx = -1;
        if (titular != null && Quadro03.isDependenteEmGuardaConjunta(titular) && (i = Integer.parseInt(titular.substring("DG".length(), titular.length()))) > 0 && i <= this.getRostoq03DT1().size()) {
            idx = i;
        }
        return idx;
    }

    private int getIndiceTitularDependenteEmGuardaConjuntaByNif(Long nif) {
        int idx = -1;
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nif)) {
            EventList<Rostoq03DT1_Linha> list = this.getRostoq03DT1();
            for (int i = 0; i < list.size(); ++i) {
                Rostoq03DT1_Linha linha = list.get(i);
                if (!nif.equals(linha.getNIF())) continue;
                idx = i + 1;
                break;
            }
        }
        return idx;
    }

    public Long getNifDependentesGuardaConjunta(String titular) {
        Long nif = null;
        Rostoq03DT1_Linha linha = this.getRostoq03DT1LinhaByLabel(titular);
        if (linha != null) {
            nif = linha.getNIF();
        }
        return nif;
    }

    public Long getGrauDependentesGuardaConjunta(String titular) {
        Long nif = null;
        Rostoq03DT1_Linha linha = this.getRostoq03DT1LinhaByLabel(titular);
        if (linha != null) {
            nif = linha.getDeficienteGrau();
        }
        return nif;
    }

    public String getTitularDependenteGuardaConjunta(Long nif) {
        String titular = null;
        if (nif != null && this.getRostoq03DT1() != null) {
            EventList<Rostoq03DT1_Linha> list = this.getRostoq03DT1();
            for (int i = 0; i < list.size(); ++i) {
                Rostoq03DT1_Linha linha = list.get(i);
                if (!nif.equals(linha.getNIF())) continue;
                titular = "DG" + (i + 1);
                break;
            }
        }
        return titular;
    }

    public String getLinkToDependentesGuardaConjunta(String titular) {
        String link = null;
        int idx = this.getIndiceTitularDependenteEmGuardaConjunta(titular);
        link = idx > 0 ? Quadro03.getLinkRowForROSTOQ03DT1_LINK(idx) : "";
        return link;
    }

    public String getLinkToDependenteGuardaConjuntaByNif(Long nif) {
        int idx;
        String link = "";
        if (nif != null && this.getRostoq03DT1() != null && (idx = this.getIndiceTitularDependenteEmGuardaConjuntaByNif(nif)) > 0) {
            link = Quadro03.getLinkRowForROSTOQ03DT1_LINK(idx);
        }
        return link;
    }

    public String getTitularDependenteNaoDeficienteByNIF(Long nif) {
        String titular = null;
        if (this.getQ03CD1() != null && this.getQ03CD1().equals(nif)) {
            titular = "D1";
        } else if (this.getQ03CD2() != null && this.getQ03CD2().equals(nif)) {
            titular = "D2";
        } else if (this.getQ03CD3() != null && this.getQ03CD3().equals(nif)) {
            titular = "D3";
        } else if (this.getQ03CD4() != null && this.getQ03CD4().equals(nif)) {
            titular = "D4";
        } else if (this.getQ03CD5() != null && this.getQ03CD5().equals(nif)) {
            titular = "D5";
        } else if (this.getQ03CD6() != null && this.getQ03CD6().equals(nif)) {
            titular = "D6";
        } else if (this.getQ03CD7() != null && this.getQ03CD7().equals(nif)) {
            titular = "D7";
        } else if (this.getQ03CD8() != null && this.getQ03CD8().equals(nif)) {
            titular = "D8";
        } else if (this.getQ03CD9() != null && this.getQ03CD9().equals(nif)) {
            titular = "D9";
        } else if (this.getQ03CD10() != null && this.getQ03CD10().equals(nif)) {
            titular = "D10";
        } else if (this.getQ03CD11() != null && this.getQ03CD11().equals(nif)) {
            titular = "D11";
        } else if (this.getQ03CD12() != null && this.getQ03CD12().equals(nif)) {
            titular = "D12";
        } else if (this.getQ03CD13() != null && this.getQ03CD13().equals(nif)) {
            titular = "D13";
        } else if (this.getQ03CD14() != null && this.getQ03CD14().equals(nif)) {
            titular = "D14";
        }
        return titular;
    }

    public Long getNifDependenteNaoDeficiente(String titular) {
        Long nif = null;
        if ("D1".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD1();
        } else if ("D2".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD2();
        } else if ("D3".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD3();
        } else if ("D4".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD4();
        } else if ("D5".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD5();
        } else if ("D6".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD6();
        } else if ("D7".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD7();
        } else if ("D8".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD8();
        } else if ("D9".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD9();
        } else if ("D10".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD10();
        } else if ("D11".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD11();
        } else if ("D12".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD12();
        } else if ("D13".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD13();
        } else if ("D14".equalsIgnoreCase(titular)) {
            nif = this.getQ03CD14();
        }
        return nif;
    }

    public static String getLinkToDependenteNaoDeficiente(String titular) {
        if ("D1".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD1";
        }
        if ("D2".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD2";
        }
        if ("D3".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD3";
        }
        if ("D4".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD4";
        }
        if ("D5".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD5";
        }
        if ("D6".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD6";
        }
        if ("D7".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD7";
        }
        if ("D8".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD8";
        }
        if ("D9".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD9";
        }
        if ("D10".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD10";
        }
        if ("D11".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD11";
        }
        if ("D12".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD12";
        }
        if ("D13".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD13";
        }
        if ("D14".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CD14";
        }
        return "";
    }

    public String getLinkToDependenteNaoDeficienteByNif(Long nif) {
        if (this.getQ03CD1() != null && this.getQ03CD1().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD1";
        }
        if (this.getQ03CD2() != null && this.getQ03CD2().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD2";
        }
        if (this.getQ03CD3() != null && this.getQ03CD3().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD3";
        }
        if (this.getQ03CD4() != null && this.getQ03CD4().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD4";
        }
        if (this.getQ03CD5() != null && this.getQ03CD5().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD5";
        }
        if (this.getQ03CD6() != null && this.getQ03CD6().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD6";
        }
        if (this.getQ03CD7() != null && this.getQ03CD7().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD7";
        }
        if (this.getQ03CD8() != null && this.getQ03CD8().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD8";
        }
        if (this.getQ03CD9() != null && this.getQ03CD9().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD9";
        }
        if (this.getQ03CD10() != null && this.getQ03CD10().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD10";
        }
        if (this.getQ03CD11() != null && this.getQ03CD11().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD11";
        }
        if (this.getQ03CD12() != null && this.getQ03CD12().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD12";
        }
        if (this.getQ03CD13() != null && this.getQ03CD13().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD13";
        }
        if (this.getQ03CD14() != null && this.getQ03CD14().equals(nif)) {
            return "aRosto.qQuadro03.fq03CD14";
        }
        return "";
    }

    public String getTitularDependenteDeficienteByNIF(Long nif) {
        String titular = null;
        if (this.getQ03CDD1() != null && this.getQ03CDD1().equals(nif)) {
            titular = "DD1";
        } else if (this.getQ03CDD2() != null && this.getQ03CDD2().equals(nif)) {
            titular = "DD2";
        } else if (this.getQ03CDD3() != null && this.getQ03CDD3().equals(nif)) {
            titular = "DD3";
        } else if (this.getQ03CDD4() != null && this.getQ03CDD4().equals(nif)) {
            titular = "DD4";
        } else if (this.getQ03CDD5() != null && this.getQ03CDD5().equals(nif)) {
            titular = "DD5";
        } else if (this.getQ03CDD6() != null && this.getQ03CDD6().equals(nif)) {
            titular = "DD6";
        } else if (this.getQ03CDD7() != null && this.getQ03CDD7().equals(nif)) {
            titular = "DD7";
        } else if (this.getQ03CDD8() != null && this.getQ03CDD8().equals(nif)) {
            titular = "DD8";
        }
        return titular;
    }

    public Long getNifDependenteDeficiente(String titular) {
        Long nif = null;
        if ("DD1".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD1();
        } else if ("DD2".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD2();
        } else if ("DD3".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD3();
        } else if ("DD4".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD4();
        } else if ("DD5".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD5();
        } else if ("DD6".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD6();
        } else if ("DD7".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD7();
        } else if ("DD8".equalsIgnoreCase(titular)) {
            nif = this.getQ03CDD8();
        }
        return nif;
    }

    public static String getLinkToDependenteDeficiente(String titular) {
        if ("DD1".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD1";
        }
        if ("DD2".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD2";
        }
        if ("DD3".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD3";
        }
        if ("DD4".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD4";
        }
        if ("DD5".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD5";
        }
        if ("DD6".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD6";
        }
        if ("DD7".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD7";
        }
        if ("DD8".equalsIgnoreCase(titular)) {
            return "aRosto.qQuadro03.fq03CDD8";
        }
        return "";
    }

    public String getLinkToDependenteDeficienteByNif(Long nif) {
        if (this.getQ03CDD1() != null && this.getQ03CDD1().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD1";
        }
        if (this.getQ03CDD2() != null && this.getQ03CDD2().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD2";
        }
        if (this.getQ03CDD3() != null && this.getQ03CDD3().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD3";
        }
        if (this.getQ03CDD4() != null && this.getQ03CDD4().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD4";
        }
        if (this.getQ03CDD5() != null && this.getQ03CDD5().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD5";
        }
        if (this.getQ03CDD6() != null && this.getQ03CDD6().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD6";
        }
        if (this.getQ03CDD7() != null && this.getQ03CDD7().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD7";
        }
        if (this.getQ03CDD8() != null && this.getQ03CDD8().equals(nif)) {
            return "aRosto.qQuadro03.fq03CDD8";
        }
        return "";
    }

    public List<Long> getNIFTitularesDependentesNaoDeficientes() {
        ArrayList<Long> nifs = new ArrayList<Long>();
        if (this.getQ03CD1() != null) {
            nifs.add(this.getQ03CD1());
        }
        if (this.getQ03CD2() != null) {
            nifs.add(this.getQ03CD2());
        }
        if (this.getQ03CD3() != null) {
            nifs.add(this.getQ03CD3());
        }
        if (this.getQ03CD4() != null) {
            nifs.add(this.getQ03CD4());
        }
        if (this.getQ03CD5() != null) {
            nifs.add(this.getQ03CD5());
        }
        if (this.getQ03CD6() != null) {
            nifs.add(this.getQ03CD6());
        }
        if (this.getQ03CD7() != null) {
            nifs.add(this.getQ03CD7());
        }
        if (this.getQ03CD8() != null) {
            nifs.add(this.getQ03CD8());
        }
        if (this.getQ03CD9() != null) {
            nifs.add(this.getQ03CD9());
        }
        if (this.getQ03CD10() != null) {
            nifs.add(this.getQ03CD10());
        }
        if (this.getQ03CD11() != null) {
            nifs.add(this.getQ03CD11());
        }
        if (this.getQ03CD12() != null) {
            nifs.add(this.getQ03CD12());
        }
        if (this.getQ03CD13() != null) {
            nifs.add(this.getQ03CD13());
        }
        if (this.getQ03CD14() != null) {
            nifs.add(this.getQ03CD14());
        }
        return nifs;
    }

    public List<Long> getNIFTitularesDependentesDeficientes() {
        ArrayList<Long> nifs = new ArrayList<Long>();
        if (this.getQ03CDD1() != null) {
            nifs.add(this.getQ03CDD1());
        }
        if (this.getQ03CDD2() != null) {
            nifs.add(this.getQ03CDD2());
        }
        if (this.getQ03CDD3() != null) {
            nifs.add(this.getQ03CDD3());
        }
        if (this.getQ03CDD4() != null) {
            nifs.add(this.getQ03CDD4());
        }
        if (this.getQ03CDD5() != null) {
            nifs.add(this.getQ03CDD5());
        }
        if (this.getQ03CDD6() != null) {
            nifs.add(this.getQ03CDD6());
        }
        if (this.getQ03CDD7() != null) {
            nifs.add(this.getQ03CDD7());
        }
        if (this.getQ03CDD8() != null) {
            nifs.add(this.getQ03CDD8());
        }
        return nifs;
    }

    public List<Long> getNIFTitularesDependentesEmGuardaConjunta() {
        ArrayList<Long> nifs = new ArrayList<Long>();
        EventList<Rostoq03DT1_Linha> depGC = this.getRostoq03DT1();
        for (Rostoq03DT1_Linha rostoq03dt1_Linha : depGC) {
            if (rostoq03dt1_Linha.getNIF() == null) continue;
            nifs.add(rostoq03dt1_Linha.getNIF());
        }
        return nifs;
    }

    public static class NifsDeficienciaAssoc {
        private final long nif;
        private final long def;

        public NifsDeficienciaAssoc(long nif, long def) {
            this.nif = nif;
            this.def = def;
        }

        public long getNif() {
            return this.nif;
        }

        public long getDef() {
            return this.def;
        }
    }

}

