/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06Base;

public class Quadro06
extends Quadro06Base {
    public boolean isQ06B1OP1Selected() {
        return this.getQ06B1() != null && this.getQ06B1().equals("1");
    }

    public boolean isQ06B1OP2Selected() {
        return this.getQ06B1() != null && this.getQ06B1().equals("2");
    }

    public boolean isQ06B1OP3Selected() {
        return this.getQ06B1() != null && this.getQ06B1().equals("3");
    }

    public boolean isQ06B1OP4Selected() {
        return this.getQ06B1() != null && this.getQ06B1().equals("4");
    }

    public boolean isEmpty() {
        return !this.isQ06B1OP1Selected() && !this.isQ06B1OP2Selected() && !this.isQ06B1OP3Selected() && !this.isQ06B1OP4Selected();
    }
}

