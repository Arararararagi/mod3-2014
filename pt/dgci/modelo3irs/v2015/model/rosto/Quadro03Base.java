/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aRosto.qQuadro03";
    public static final String Q03C01_LINK = "aRosto.qQuadro03.fq03C01";
    public static final String Q03C01 = "q03C01";
    private String q03C01;
    public static final String Q03C02_LINK = "aRosto.qQuadro03.fq03C02";
    public static final String Q03C02 = "q03C02";
    private String q03C02;
    public static final String Q03C03_LINK = "aRosto.qQuadro03.fq03C03";
    public static final String Q03C03 = "q03C03";
    private Long q03C03;
    public static final String Q03C03A_LINK = "aRosto.qQuadro03.fq03C03a";
    public static final String Q03C03A = "q03C03a";
    private Long q03C03a;
    public static final String Q03B03_LINK = "aRosto.qQuadro03.fq03B03";
    public static final String Q03B03 = "q03B03";
    private Boolean q03B03;
    public static final String Q03C04_LINK = "aRosto.qQuadro03.fq03C04";
    public static final String Q03C04 = "q03C04";
    private Long q03C04;
    public static final String Q03C04A_LINK = "aRosto.qQuadro03.fq03C04a";
    public static final String Q03C04A = "q03C04a";
    private Long q03C04a;
    public static final String Q03B04_LINK = "aRosto.qQuadro03.fq03B04";
    public static final String Q03B04 = "q03B04";
    private Boolean q03B04;
    public static final String Q03CD1_LINK = "aRosto.qQuadro03.fq03CD1";
    public static final String Q03CD1 = "q03CD1";
    private Long q03CD1;
    public static final String Q03CD2_LINK = "aRosto.qQuadro03.fq03CD2";
    public static final String Q03CD2 = "q03CD2";
    private Long q03CD2;
    public static final String Q03CD3_LINK = "aRosto.qQuadro03.fq03CD3";
    public static final String Q03CD3 = "q03CD3";
    private Long q03CD3;
    public static final String Q03CD4_LINK = "aRosto.qQuadro03.fq03CD4";
    public static final String Q03CD4 = "q03CD4";
    private Long q03CD4;
    public static final String Q03CD5_LINK = "aRosto.qQuadro03.fq03CD5";
    public static final String Q03CD5 = "q03CD5";
    private Long q03CD5;
    public static final String Q03CD6_LINK = "aRosto.qQuadro03.fq03CD6";
    public static final String Q03CD6 = "q03CD6";
    private Long q03CD6;
    public static final String Q03CD7_LINK = "aRosto.qQuadro03.fq03CD7";
    public static final String Q03CD7 = "q03CD7";
    private Long q03CD7;
    public static final String Q03CD8_LINK = "aRosto.qQuadro03.fq03CD8";
    public static final String Q03CD8 = "q03CD8";
    private Long q03CD8;
    public static final String Q03CD9_LINK = "aRosto.qQuadro03.fq03CD9";
    public static final String Q03CD9 = "q03CD9";
    private Long q03CD9;
    public static final String Q03CD10_LINK = "aRosto.qQuadro03.fq03CD10";
    public static final String Q03CD10 = "q03CD10";
    private Long q03CD10;
    public static final String Q03CD11_LINK = "aRosto.qQuadro03.fq03CD11";
    public static final String Q03CD11 = "q03CD11";
    private Long q03CD11;
    public static final String Q03CD12_LINK = "aRosto.qQuadro03.fq03CD12";
    public static final String Q03CD12 = "q03CD12";
    private Long q03CD12;
    public static final String Q03CD13_LINK = "aRosto.qQuadro03.fq03CD13";
    public static final String Q03CD13 = "q03CD13";
    private Long q03CD13;
    public static final String Q03CD14_LINK = "aRosto.qQuadro03.fq03CD14";
    public static final String Q03CD14 = "q03CD14";
    private Long q03CD14;
    public static final String Q03CDD1_LINK = "aRosto.qQuadro03.fq03CDD1";
    public static final String Q03CDD1 = "q03CDD1";
    private Long q03CDD1;
    public static final String Q03CDD1A_LINK = "aRosto.qQuadro03.fq03CDD1a";
    public static final String Q03CDD1A = "q03CDD1a";
    private Long q03CDD1a;
    public static final String Q03CDD2_LINK = "aRosto.qQuadro03.fq03CDD2";
    public static final String Q03CDD2 = "q03CDD2";
    private Long q03CDD2;
    public static final String Q03CDD2A_LINK = "aRosto.qQuadro03.fq03CDD2a";
    public static final String Q03CDD2A = "q03CDD2a";
    private Long q03CDD2a;
    public static final String Q03CDD3_LINK = "aRosto.qQuadro03.fq03CDD3";
    public static final String Q03CDD3 = "q03CDD3";
    private Long q03CDD3;
    public static final String Q03CDD3A_LINK = "aRosto.qQuadro03.fq03CDD3a";
    public static final String Q03CDD3A = "q03CDD3a";
    private Long q03CDD3a;
    public static final String Q03CDD4_LINK = "aRosto.qQuadro03.fq03CDD4";
    public static final String Q03CDD4 = "q03CDD4";
    private Long q03CDD4;
    public static final String Q03CDD4A_LINK = "aRosto.qQuadro03.fq03CDD4a";
    public static final String Q03CDD4A = "q03CDD4a";
    private Long q03CDD4a;
    public static final String Q03CDD5_LINK = "aRosto.qQuadro03.fq03CDD5";
    public static final String Q03CDD5 = "q03CDD5";
    private Long q03CDD5;
    public static final String Q03CDD5A_LINK = "aRosto.qQuadro03.fq03CDD5a";
    public static final String Q03CDD5A = "q03CDD5a";
    private Long q03CDD5a;
    public static final String Q03CDD6_LINK = "aRosto.qQuadro03.fq03CDD6";
    public static final String Q03CDD6 = "q03CDD6";
    private Long q03CDD6;
    public static final String Q03CDD6A_LINK = "aRosto.qQuadro03.fq03CDD6a";
    public static final String Q03CDD6A = "q03CDD6a";
    private Long q03CDD6a;
    public static final String Q03CDD7_LINK = "aRosto.qQuadro03.fq03CDD7";
    public static final String Q03CDD7 = "q03CDD7";
    private Long q03CDD7;
    public static final String Q03CDD7A_LINK = "aRosto.qQuadro03.fq03CDD7a";
    public static final String Q03CDD7A = "q03CDD7a";
    private Long q03CDD7a;
    public static final String Q03CDD8_LINK = "aRosto.qQuadro03.fq03CDD8";
    public static final String Q03CDD8 = "q03CDD8";
    private Long q03CDD8;
    public static final String Q03CDD8A_LINK = "aRosto.qQuadro03.fq03CDD8a";
    public static final String Q03CDD8A = "q03CDD8a";
    private Long q03CDD8a;
    public static final String ROSTOQ03DT1_LINK = "aRosto.qQuadro03.trostoq03DT1";
    private EventList<Rostoq03DT1_Linha> rostoq03DT1 = new BasicEventList<Rostoq03DT1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    public String getQ03C01() {
        return this.q03C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03C01(String q03C01) {
        String oldValue = this.q03C01;
        this.q03C01 = q03C01;
        this.firePropertyChange("q03C01", oldValue, this.q03C01);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getQ03C02() {
        return this.q03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03C02(String q03C02) {
        String oldValue = this.q03C02;
        this.q03C02 = q03C02;
        this.firePropertyChange("q03C02", oldValue, this.q03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03C03() {
        return this.q03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03C03(Long q03C03) {
        Long oldValue = this.q03C03;
        this.q03C03 = q03C03;
        this.firePropertyChange("q03C03", oldValue, this.q03C03);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03C03a() {
        return this.q03C03a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03C03a(Long q03C03a) {
        Long oldValue = this.q03C03a;
        this.q03C03a = q03C03a;
        this.firePropertyChange("q03C03a", oldValue, this.q03C03a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getQ03B03() {
        return this.q03B03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03B03(Boolean q03B03) {
        Boolean oldValue = this.q03B03;
        this.q03B03 = q03B03;
        this.firePropertyChange("q03B03", oldValue, this.q03B03);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03C04() {
        return this.q03C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03C04(Long q03C04) {
        Long oldValue = this.q03C04;
        this.q03C04 = q03C04;
        this.firePropertyChange("q03C04", oldValue, this.q03C04);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03C04a() {
        return this.q03C04a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03C04a(Long q03C04a) {
        Long oldValue = this.q03C04a;
        this.q03C04a = q03C04a;
        this.firePropertyChange("q03C04a", oldValue, this.q03C04a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getQ03B04() {
        return this.q03B04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03B04(Boolean q03B04) {
        Boolean oldValue = this.q03B04;
        this.q03B04 = q03B04;
        this.firePropertyChange("q03B04", oldValue, this.q03B04);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD1() {
        return this.q03CD1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD1(Long q03CD1) {
        Long oldValue = this.q03CD1;
        this.q03CD1 = q03CD1;
        this.firePropertyChange("q03CD1", oldValue, this.q03CD1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD2() {
        return this.q03CD2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD2(Long q03CD2) {
        Long oldValue = this.q03CD2;
        this.q03CD2 = q03CD2;
        this.firePropertyChange("q03CD2", oldValue, this.q03CD2);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD3() {
        return this.q03CD3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD3(Long q03CD3) {
        Long oldValue = this.q03CD3;
        this.q03CD3 = q03CD3;
        this.firePropertyChange("q03CD3", oldValue, this.q03CD3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD4() {
        return this.q03CD4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD4(Long q03CD4) {
        Long oldValue = this.q03CD4;
        this.q03CD4 = q03CD4;
        this.firePropertyChange("q03CD4", oldValue, this.q03CD4);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD5() {
        return this.q03CD5;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD5(Long q03CD5) {
        Long oldValue = this.q03CD5;
        this.q03CD5 = q03CD5;
        this.firePropertyChange("q03CD5", oldValue, this.q03CD5);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD6() {
        return this.q03CD6;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD6(Long q03CD6) {
        Long oldValue = this.q03CD6;
        this.q03CD6 = q03CD6;
        this.firePropertyChange("q03CD6", oldValue, this.q03CD6);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD7() {
        return this.q03CD7;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD7(Long q03CD7) {
        Long oldValue = this.q03CD7;
        this.q03CD7 = q03CD7;
        this.firePropertyChange("q03CD7", oldValue, this.q03CD7);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD8() {
        return this.q03CD8;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD8(Long q03CD8) {
        Long oldValue = this.q03CD8;
        this.q03CD8 = q03CD8;
        this.firePropertyChange("q03CD8", oldValue, this.q03CD8);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD9() {
        return this.q03CD9;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD9(Long q03CD9) {
        Long oldValue = this.q03CD9;
        this.q03CD9 = q03CD9;
        this.firePropertyChange("q03CD9", oldValue, this.q03CD9);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD10() {
        return this.q03CD10;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD10(Long q03CD10) {
        Long oldValue = this.q03CD10;
        this.q03CD10 = q03CD10;
        this.firePropertyChange("q03CD10", oldValue, this.q03CD10);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD11() {
        return this.q03CD11;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD11(Long q03CD11) {
        Long oldValue = this.q03CD11;
        this.q03CD11 = q03CD11;
        this.firePropertyChange("q03CD11", oldValue, this.q03CD11);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD12() {
        return this.q03CD12;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD12(Long q03CD12) {
        Long oldValue = this.q03CD12;
        this.q03CD12 = q03CD12;
        this.firePropertyChange("q03CD12", oldValue, this.q03CD12);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD13() {
        return this.q03CD13;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD13(Long q03CD13) {
        Long oldValue = this.q03CD13;
        this.q03CD13 = q03CD13;
        this.firePropertyChange("q03CD13", oldValue, this.q03CD13);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CD14() {
        return this.q03CD14;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CD14(Long q03CD14) {
        Long oldValue = this.q03CD14;
        this.q03CD14 = q03CD14;
        this.firePropertyChange("q03CD14", oldValue, this.q03CD14);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD1() {
        return this.q03CDD1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD1(Long q03CDD1) {
        Long oldValue = this.q03CDD1;
        this.q03CDD1 = q03CDD1;
        this.firePropertyChange("q03CDD1", oldValue, this.q03CDD1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD1a() {
        return this.q03CDD1a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD1a(Long q03CDD1a) {
        Long oldValue = this.q03CDD1a;
        this.q03CDD1a = q03CDD1a;
        this.firePropertyChange("q03CDD1a", oldValue, this.q03CDD1a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD2() {
        return this.q03CDD2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD2(Long q03CDD2) {
        Long oldValue = this.q03CDD2;
        this.q03CDD2 = q03CDD2;
        this.firePropertyChange("q03CDD2", oldValue, this.q03CDD2);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD2a() {
        return this.q03CDD2a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD2a(Long q03CDD2a) {
        Long oldValue = this.q03CDD2a;
        this.q03CDD2a = q03CDD2a;
        this.firePropertyChange("q03CDD2a", oldValue, this.q03CDD2a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD3() {
        return this.q03CDD3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD3(Long q03CDD3) {
        Long oldValue = this.q03CDD3;
        this.q03CDD3 = q03CDD3;
        this.firePropertyChange("q03CDD3", oldValue, this.q03CDD3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD3a() {
        return this.q03CDD3a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD3a(Long q03CDD3a) {
        Long oldValue = this.q03CDD3a;
        this.q03CDD3a = q03CDD3a;
        this.firePropertyChange("q03CDD3a", oldValue, this.q03CDD3a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD4() {
        return this.q03CDD4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD4(Long q03CDD4) {
        Long oldValue = this.q03CDD4;
        this.q03CDD4 = q03CDD4;
        this.firePropertyChange("q03CDD4", oldValue, this.q03CDD4);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD4a() {
        return this.q03CDD4a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD4a(Long q03CDD4a) {
        Long oldValue = this.q03CDD4a;
        this.q03CDD4a = q03CDD4a;
        this.firePropertyChange("q03CDD4a", oldValue, this.q03CDD4a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD5() {
        return this.q03CDD5;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD5(Long q03CDD5) {
        Long oldValue = this.q03CDD5;
        this.q03CDD5 = q03CDD5;
        this.firePropertyChange("q03CDD5", oldValue, this.q03CDD5);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD5a() {
        return this.q03CDD5a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD5a(Long q03CDD5a) {
        Long oldValue = this.q03CDD5a;
        this.q03CDD5a = q03CDD5a;
        this.firePropertyChange("q03CDD5a", oldValue, this.q03CDD5a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD6() {
        return this.q03CDD6;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD6(Long q03CDD6) {
        Long oldValue = this.q03CDD6;
        this.q03CDD6 = q03CDD6;
        this.firePropertyChange("q03CDD6", oldValue, this.q03CDD6);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD6a() {
        return this.q03CDD6a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD6a(Long q03CDD6a) {
        Long oldValue = this.q03CDD6a;
        this.q03CDD6a = q03CDD6a;
        this.firePropertyChange("q03CDD6a", oldValue, this.q03CDD6a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD7() {
        return this.q03CDD7;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD7(Long q03CDD7) {
        Long oldValue = this.q03CDD7;
        this.q03CDD7 = q03CDD7;
        this.firePropertyChange("q03CDD7", oldValue, this.q03CDD7);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD7a() {
        return this.q03CDD7a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD7a(Long q03CDD7a) {
        Long oldValue = this.q03CDD7a;
        this.q03CDD7a = q03CDD7a;
        this.firePropertyChange("q03CDD7a", oldValue, this.q03CDD7a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD8() {
        return this.q03CDD8;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD8(Long q03CDD8) {
        Long oldValue = this.q03CDD8;
        this.q03CDD8 = q03CDD8;
        this.firePropertyChange("q03CDD8", oldValue, this.q03CDD8);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ03CDD8a() {
        return this.q03CDD8a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ03CDD8a(Long q03CDD8a) {
        Long oldValue = this.q03CDD8a;
        this.q03CDD8a = q03CDD8a;
        this.firePropertyChange("q03CDD8a", oldValue, this.q03CDD8a);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<Rostoq03DT1_Linha> getRostoq03DT1() {
        return this.rostoq03DT1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setRostoq03DT1(EventList<Rostoq03DT1_Linha> rostoq03DT1) {
        this.rostoq03DT1 = rostoq03DT1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q03C01);
        result = HashCodeUtil.hash(result, this.q03C02);
        result = HashCodeUtil.hash(result, this.q03C03);
        result = HashCodeUtil.hash(result, this.q03C03a);
        result = HashCodeUtil.hash(result, this.q03B03);
        result = HashCodeUtil.hash(result, this.q03C04);
        result = HashCodeUtil.hash(result, this.q03C04a);
        result = HashCodeUtil.hash(result, this.q03B04);
        result = HashCodeUtil.hash(result, this.q03CD1);
        result = HashCodeUtil.hash(result, this.q03CD2);
        result = HashCodeUtil.hash(result, this.q03CD3);
        result = HashCodeUtil.hash(result, this.q03CD4);
        result = HashCodeUtil.hash(result, this.q03CD5);
        result = HashCodeUtil.hash(result, this.q03CD6);
        result = HashCodeUtil.hash(result, this.q03CD7);
        result = HashCodeUtil.hash(result, this.q03CD8);
        result = HashCodeUtil.hash(result, this.q03CD9);
        result = HashCodeUtil.hash(result, this.q03CD10);
        result = HashCodeUtil.hash(result, this.q03CD11);
        result = HashCodeUtil.hash(result, this.q03CD12);
        result = HashCodeUtil.hash(result, this.q03CD13);
        result = HashCodeUtil.hash(result, this.q03CD14);
        result = HashCodeUtil.hash(result, this.q03CDD1);
        result = HashCodeUtil.hash(result, this.q03CDD1a);
        result = HashCodeUtil.hash(result, this.q03CDD2);
        result = HashCodeUtil.hash(result, this.q03CDD2a);
        result = HashCodeUtil.hash(result, this.q03CDD3);
        result = HashCodeUtil.hash(result, this.q03CDD3a);
        result = HashCodeUtil.hash(result, this.q03CDD4);
        result = HashCodeUtil.hash(result, this.q03CDD4a);
        result = HashCodeUtil.hash(result, this.q03CDD5);
        result = HashCodeUtil.hash(result, this.q03CDD5a);
        result = HashCodeUtil.hash(result, this.q03CDD6);
        result = HashCodeUtil.hash(result, this.q03CDD6a);
        result = HashCodeUtil.hash(result, this.q03CDD7);
        result = HashCodeUtil.hash(result, this.q03CDD7a);
        result = HashCodeUtil.hash(result, this.q03CDD8);
        result = HashCodeUtil.hash(result, this.q03CDD8a);
        result = HashCodeUtil.hash(result, this.rostoq03DT1);
        return result;
    }
}

