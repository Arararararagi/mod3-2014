/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;

public class Rostoq03DT1_LinhaAdapterFormatBase
implements AdvancedTableFormat<Rostoq03DT1_Linha>,
WritableTableFormat<Rostoq03DT1_Linha> {
    @Override
    public boolean isEditable(Rostoq03DT1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(Rostoq03DT1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNIF();
            }
            case 2: {
                return line.getDeficienteGrau();
            }
            case 3: {
                return line.getNifProgenitor();
            }
        }
        return null;
    }

    @Override
    public Rostoq03DT1_Linha setColumnValue(Rostoq03DT1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNIF((Long)value);
                return line;
            }
            case 2: {
                line.setDeficienteGrau((Long)value);
                return line;
            }
            case 3: {
                line.setNifProgenitor((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF";
            }
            case 2: {
                return "Grau";
            }
            case 3: {
                return "NIF";
            }
        }
        return null;
    }
}

