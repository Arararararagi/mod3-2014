/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aRosto.qQuadro07";
    public static final String Q07C1_LINK = "aRosto.qQuadro07.fq07C1";
    public static final String Q07C1 = "q07C1";
    private Long q07C1;
    public static final String Q07C1A_LINK = "aRosto.qQuadro07.fq07C1a";
    public static final String Q07C1A = "q07C1a";
    private Long q07C1a;
    public static final String Q07C1B_LINK = "aRosto.qQuadro07.fq07C1b";
    public static final String Q07C1B = "q07C1b";
    private Boolean q07C1b;
    public static final String Q07C01_LINK = "aRosto.qQuadro07.fq07C01";
    public static final String Q07C01 = "q07C01";
    private Long q07C01;
    public static final String Q07C01A_LINK = "aRosto.qQuadro07.fq07C01a";
    public static final String Q07C01A = "q07C01a";
    private Long q07C01a;
    public static final String Q07C02_LINK = "aRosto.qQuadro07.fq07C02";
    public static final String Q07C02 = "q07C02";
    private Long q07C02;
    public static final String Q07C02A_LINK = "aRosto.qQuadro07.fq07C02a";
    public static final String Q07C02A = "q07C02a";
    private Long q07C02a;
    public static final String Q07C03_LINK = "aRosto.qQuadro07.fq07C03";
    public static final String Q07C03 = "q07C03";
    private Long q07C03;
    public static final String Q07C03A_LINK = "aRosto.qQuadro07.fq07C03a";
    public static final String Q07C03A = "q07C03a";
    private Long q07C03a;
    public static final String Q07C04_LINK = "aRosto.qQuadro07.fq07C04";
    public static final String Q07C04 = "q07C04";
    private Long q07C04;
    public static final String Q07C04A_LINK = "aRosto.qQuadro07.fq07C04a";
    public static final String Q07C04A = "q07C04a";
    private Long q07C04a;
    public static final String ROSTOQ07ET1_LINK = "aRosto.qQuadro07.trostoq07ET1";
    private EventList<Rostoq07ET1_Linha> rostoq07ET1 = new BasicEventList<Rostoq07ET1_Linha>();
    public static final String ROSTOQ07CT1_LINK = "aRosto.qQuadro07.trostoq07CT1";
    private EventList<Rostoq07CT1_Linha> rostoq07CT1 = new BasicEventList<Rostoq07CT1_Linha>();
    public static final String Q07D01_LINK = "aRosto.qQuadro07.fq07D01";
    public static final String Q07D01 = "q07D01";
    private String q07D01;
    public static final String Q07D01A_LINK = "aRosto.qQuadro07.fq07D01a";
    public static final String Q07D01A = "q07D01a";
    private Boolean q07D01a;

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C1() {
        return this.q07C1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C1(Long q07C1) {
        Long oldValue = this.q07C1;
        this.q07C1 = q07C1;
        this.firePropertyChange("q07C1", oldValue, this.q07C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C1a() {
        return this.q07C1a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C1a(Long q07C1a) {
        Long oldValue = this.q07C1a;
        this.q07C1a = q07C1a;
        this.firePropertyChange("q07C1a", oldValue, this.q07C1a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getQ07C1b() {
        return this.q07C1b;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C1b(Boolean q07C1b) {
        Boolean oldValue = this.q07C1b;
        this.q07C1b = q07C1b;
        this.firePropertyChange("q07C1b", oldValue, this.q07C1b);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C01() {
        return this.q07C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C01(Long q07C01) {
        Long oldValue = this.q07C01;
        this.q07C01 = q07C01;
        this.firePropertyChange("q07C01", oldValue, this.q07C01);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C01a() {
        return this.q07C01a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C01a(Long q07C01a) {
        Long oldValue = this.q07C01a;
        this.q07C01a = q07C01a;
        this.firePropertyChange("q07C01a", oldValue, this.q07C01a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C02() {
        return this.q07C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C02(Long q07C02) {
        Long oldValue = this.q07C02;
        this.q07C02 = q07C02;
        this.firePropertyChange("q07C02", oldValue, this.q07C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C02a() {
        return this.q07C02a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C02a(Long q07C02a) {
        Long oldValue = this.q07C02a;
        this.q07C02a = q07C02a;
        this.firePropertyChange("q07C02a", oldValue, this.q07C02a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C03() {
        return this.q07C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C03(Long q07C03) {
        Long oldValue = this.q07C03;
        this.q07C03 = q07C03;
        this.firePropertyChange("q07C03", oldValue, this.q07C03);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C03a() {
        return this.q07C03a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C03a(Long q07C03a) {
        Long oldValue = this.q07C03a;
        this.q07C03a = q07C03a;
        this.firePropertyChange("q07C03a", oldValue, this.q07C03a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C04() {
        return this.q07C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C04(Long q07C04) {
        Long oldValue = this.q07C04;
        this.q07C04 = q07C04;
        this.firePropertyChange("q07C04", oldValue, this.q07C04);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQ07C04a() {
        return this.q07C04a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07C04a(Long q07C04a) {
        Long oldValue = this.q07C04a;
        this.q07C04a = q07C04a;
        this.firePropertyChange("q07C04a", oldValue, this.q07C04a);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<Rostoq07ET1_Linha> getRostoq07ET1() {
        return this.rostoq07ET1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setRostoq07ET1(EventList<Rostoq07ET1_Linha> rostoq07ET1) {
        this.rostoq07ET1 = rostoq07ET1;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<Rostoq07CT1_Linha> getRostoq07CT1() {
        return this.rostoq07CT1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setRostoq07CT1(EventList<Rostoq07CT1_Linha> rostoq07CT1) {
        this.rostoq07CT1 = rostoq07CT1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getQ07D01() {
        return this.q07D01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07D01(String q07D01) {
        String oldValue = this.q07D01;
        this.q07D01 = q07D01;
        this.firePropertyChange("q07D01", oldValue, this.q07D01);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getQ07D01a() {
        return this.q07D01a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQ07D01a(Boolean q07D01a) {
        Boolean oldValue = this.q07D01a;
        this.q07D01a = q07D01a;
        this.firePropertyChange("q07D01a", oldValue, this.q07D01a);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.q07C1);
        result = HashCodeUtil.hash(result, this.q07C1a);
        result = HashCodeUtil.hash(result, this.q07C1b);
        result = HashCodeUtil.hash(result, this.q07C01);
        result = HashCodeUtil.hash(result, this.q07C01a);
        result = HashCodeUtil.hash(result, this.q07C02);
        result = HashCodeUtil.hash(result, this.q07C02a);
        result = HashCodeUtil.hash(result, this.q07C03);
        result = HashCodeUtil.hash(result, this.q07C03a);
        result = HashCodeUtil.hash(result, this.q07C04);
        result = HashCodeUtil.hash(result, this.q07C04a);
        result = HashCodeUtil.hash(result, this.rostoq07ET1);
        result = HashCodeUtil.hash(result, this.rostoq07CT1);
        result = HashCodeUtil.hash(result, this.q07D01);
        result = HashCodeUtil.hash(result, this.q07D01a);
        return result;
    }
}

