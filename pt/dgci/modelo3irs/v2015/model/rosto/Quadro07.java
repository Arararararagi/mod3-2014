/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.EventList;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07Base;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07ET1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class Quadro07
extends Quadro07Base {
    public static final String FALECIDO = "F";
    public static final String AFILHADOS = "AF";
    public static final String ASCENDENTES_COLATERAIS = "AC";
    public static final String ASCENDENTES_COMUNHAO_1 = "AS1";
    public static final String ASCENDENTES_COMUNHAO_2 = "AS2";
    public static final String ASCENDENTES_COMUNHAO_3 = "AS3";
    public static final String ASCENDENTES_COMUNHAO_4 = "AS4";
    public static final String[] COD_TITULARES_ASCENDENTES_COMUNHAO = new String[]{"AS1", "AS2", "AS3", "AS3"};

    public boolean isConjugeFalecido(Long nif) {
        return this.getQ07C1() != null && this.getQ07C1().equals(nif);
    }

    public static boolean isConjugeFalecido(String titular) {
        return "F".equals(titular);
    }

    public static boolean isAscendentesEmComunhao(String titular) {
        return titular != null && StringUtil.in(titular, COD_TITULARES_ASCENDENTES_COMUNHAO);
    }

    public boolean existsInAscendentes(String titular) {
        if ("AS1".equalsIgnoreCase(titular)) {
            return this.getQ07C01() != null;
        }
        if ("AS2".equalsIgnoreCase(titular)) {
            return this.getQ07C02() != null;
        }
        if ("AS3".equalsIgnoreCase(titular)) {
            return this.getQ07C03() != null;
        }
        if ("AS4".equalsIgnoreCase(titular)) {
            return this.getQ07C04() != null;
        }
        return false;
    }

    public boolean existsInAscendentes(long nif) {
        return this.existsInAscendentes(nif, -1);
    }

    public boolean existsInAscendentes(long nif, int nAscendente) {
        if (nAscendente != 1 && this.getQ07C01() != null && this.getQ07C01() == nif) {
            return true;
        }
        if (nAscendente != 2 && this.getQ07C02() != null && this.getQ07C02() == nif) {
            return true;
        }
        if (nAscendente != 3 && this.getQ07C03() != null && this.getQ07C03() == nif) {
            return true;
        }
        if (nAscendente != 4 && this.getQ07C04() != null && this.getQ07C04() == nif) {
            return true;
        }
        return false;
    }

    public static boolean isAfilhadosCivisEmComunhao(String titular) {
        return titular != null && titular.startsWith("AF");
    }

    public boolean existsInAfilhadosCivisEmComunhao(Long nif) {
        for (Rostoq07CT1_Linha linha : this.getRostoq07CT1()) {
            if (linha.getNIF() == null || !linha.getNIF().equals(nif)) continue;
            return true;
        }
        return false;
    }

    public boolean existsInAfilhadosCivisEmComunhao(String titular) {
        int idx = Modelo3IRSValidatorUtil.getRowNumberFromTitular(titular, "AF");
        return idx > 0 && this.getRostoq07CT1() != null && this.getRostoq07CT1().size() >= idx && this.getRostoq07CT1().get(idx - 1) != null && this.getRostoq07CT1().get(idx - 1).getNIF() != null;
    }

    public static boolean isAscendentesEColaterais(String titular) {
        return titular != null && titular.startsWith("AC");
    }

    public boolean existsInAscendentesEColaterais(Long nif) {
        for (Rostoq07ET1_Linha linha : this.getRostoq07ET1()) {
            if (linha.getNIF() == null || !linha.getNIF().equals(nif)) continue;
            return true;
        }
        return false;
    }

    public boolean existsInAscendentesEColaterais(String titular) {
        int idx = Modelo3IRSValidatorUtil.getRowNumberFromTitular(titular, "AC");
        return idx > 0 && this.getRostoq07ET1() != null && this.getRostoq07ET1().size() >= idx && this.getRostoq07ET1().get(idx - 1) != null && this.getRostoq07ET1().get(idx - 1).getNIF() != null;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesFalecidosAsCatalog() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        if (this.getQ07C1() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ07C1(), "F"));
        }
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesAscendentesEmComunhaoAsCatalogo() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        if (this.getQ07C01() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ07C01(), "AS1"));
        }
        if (this.getQ07C02() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ07C02(), "AS2"));
        }
        if (this.getQ07C03() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ07C03(), "AS3"));
        }
        if (this.getQ07C04() != null) {
            titularesList.add(new Cat_M3V2015_RostoTitulares(this.getQ07C04(), "AS4"));
        }
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesAscendentesEColateraisAsCatalogo() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        EventList<Rostoq07ET1_Linha> table = this.getRostoq07ET1();
        if (table != null) {
            for (int i = 0; i < table.size(); ++i) {
                Rostoq07ET1_LinhaBase linha = table.get(i);
                if (linha.getNIF() == null) continue;
                titularesList.add(new Cat_M3V2015_RostoTitulares(linha.getNIF(), "AC" + (i + 1)));
            }
        }
        return titularesList;
    }

    public List<Cat_M3V2015_RostoTitulares> getTitularesAfilhadosCivisEmComunhaoAsCatalog() {
        ArrayList<Cat_M3V2015_RostoTitulares> titularesList = new ArrayList<Cat_M3V2015_RostoTitulares>();
        EventList<Rostoq07CT1_Linha> table = this.getRostoq07CT1();
        if (table != null) {
            for (int i = 0; i < table.size(); ++i) {
                Rostoq07CT1_LinhaBase linha = table.get(i);
                if (linha.getNIF() == null) continue;
                titularesList.add(new Cat_M3V2015_RostoTitulares(linha.getNIF(), "AF" + (i + 1)));
            }
        }
        return titularesList;
    }

    public static String getLinkRowForROSTOQ07CT1_LINK(int linhaPos) {
        return "aRosto.qQuadro07.trostoq07CT1.l" + linhaPos;
    }

    public static String getLinkForROSTOQ07CT1_LINK(int linhaPos, Rostoq07CT1_LinhaBase.Property property) {
        int index = property.getIndex();
        return Quadro07.getLinkRowForROSTOQ07CT1_LINK(linhaPos) + ".c" + index;
    }

    public static String getLinkRowForROSTOQ07ET1_LINK(int linhaPos) {
        return "aRosto.qQuadro07.trostoq07ET1.l" + linhaPos;
    }

    public static String getLinkCellForROSTOQ07ET1_LINK(int linhaPos, Rostoq07ET1_LinhaBase.Property property) {
        int index = property.getIndex();
        return Quadro07.getLinkRowForROSTOQ07ET1_LINK(linhaPos) + ".c" + index;
    }

    public Long getNifAfilhadosCivisEmComunhao(String titular) {
        Rostoq07CT1_Linha linha;
        Long nif = null;
        int idx = this.getIndiceTitularAfilhadosCivisEmComunhao(titular);
        if (idx > 0 && (linha = this.getRostoq07CT1().get(idx - 1)) != null) {
            nif = linha.getNIF();
        }
        return nif;
    }

    private int getIndiceTitularAfilhadosCivisEmComunhao(String titular) {
        int i;
        int idx = -1;
        if (titular != null && Quadro07.isAfilhadosCivisEmComunhao(titular) && (i = Integer.parseInt(titular.substring("AF".length(), titular.length()))) > 0 && i <= this.getRostoq07CT1().size()) {
            idx = i;
        }
        return idx;
    }

    private int getIndiceTitularAfilhadosCivisEmComunhaoByNif(Long nif) {
        int idx = -1;
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nif)) {
            EventList<Rostoq07CT1_Linha> list = this.getRostoq07CT1();
            for (int i = 0; i < list.size(); ++i) {
                Rostoq07CT1_Linha linha = list.get(i);
                if (!nif.equals(linha.getNIF())) continue;
                idx = i + 1;
                break;
            }
        }
        return idx;
    }

    public String getTitularAfilhadosCivisEmComunhao(Long nif) {
        String titular = null;
        if (nif != null && this.getRostoq07CT1() != null) {
            EventList<Rostoq07CT1_Linha> list = this.getRostoq07CT1();
            for (int i = 0; i < list.size(); ++i) {
                Rostoq07CT1_Linha linha = list.get(i);
                if (!nif.equals(linha.getNIF())) continue;
                titular = "AF" + (i + 1);
                break;
            }
        }
        return titular;
    }

    public String getLinkToAfilhadosCivisEmComunhao(String titular) {
        String link = null;
        int idx = this.getIndiceTitularAfilhadosCivisEmComunhao(titular);
        link = idx > 0 ? Quadro07.getLinkRowForROSTOQ07CT1_LINK(idx) : "";
        return link;
    }

    public String getLinkToAfilhadosCivisEmComunhaoByNif(Long nif) {
        int idx;
        String link = "";
        if (nif != null && this.getRostoq07CT1() != null && (idx = this.getIndiceTitularAfilhadosCivisEmComunhaoByNif(nif)) > 0) {
            link = Quadro07.getLinkRowForROSTOQ07CT1_LINK(idx);
        }
        return link;
    }

    public Long getNifAscendentesEColaterais(String titular) {
        Rostoq07ET1_Linha linha;
        Long nif = null;
        int idx = this.getIndiceTitularAscendentesEColaterais(titular);
        if (idx > 0 && (linha = this.getRostoq07ET1().get(idx - 1)) != null) {
            nif = linha.getNIF();
        }
        return nif;
    }

    private int getIndiceTitularAscendentesEColaterais(String titular) {
        int i;
        int idx = -1;
        if (titular != null && Quadro07.isAscendentesEColaterais(titular) && (i = Integer.parseInt(titular.substring("AC".length(), titular.length()))) > 0 && i <= this.getRostoq07ET1().size()) {
            idx = i;
        }
        return idx;
    }

    private int getIndiceTitularAscendentesEColateraisByNif(Long nif) {
        int idx = -1;
        if (!Modelo3IRSValidatorUtil.isEmptyOrZero(nif)) {
            EventList<Rostoq07ET1_Linha> list = this.getRostoq07ET1();
            for (int i = 0; i < list.size(); ++i) {
                Rostoq07ET1_Linha linha = list.get(i);
                if (!nif.equals(linha.getNIF())) continue;
                idx = i + 1;
                break;
            }
        }
        return idx;
    }

    public String getTitularAscendentesEColaterais(Long nif) {
        String titular = null;
        if (nif != null && this.getRostoq07ET1() != null) {
            EventList<Rostoq07ET1_Linha> list = this.getRostoq07ET1();
            for (int i = 0; i < list.size(); ++i) {
                Rostoq07ET1_Linha linha = list.get(i);
                if (!nif.equals(linha.getNIF())) continue;
                titular = "AC" + (i + 1);
                break;
            }
        }
        return titular;
    }

    public String getLinkToAscendentesEColaterais(String titular) {
        String link = null;
        int idx = this.getIndiceTitularAscendentesEColaterais(titular);
        link = idx > 0 ? Quadro07.getLinkRowForROSTOQ07ET1_LINK(idx) : "";
        return link;
    }

    public String getLinkToAscendentesEColateraisByNif(Long nif) {
        int idx;
        String link = "";
        if (nif != null && this.getRostoq07ET1() != null && (idx = this.getIndiceTitularAscendentesEColateraisByNif(nif)) > 0) {
            link = Quadro07.getLinkRowForROSTOQ07ET1_LINK(idx);
        }
        return link;
    }

    public Long getNifAscendentesEmComunhao(String titular) {
        Long nif = null;
        if (titular != null && Quadro07.isAscendentesEmComunhao(titular)) {
            if ("AS1".equalsIgnoreCase(titular)) {
                nif = this.getQ07C01();
            } else if ("AS2".equalsIgnoreCase(titular)) {
                nif = this.getQ07C02();
            } else if ("AS3".equalsIgnoreCase(titular)) {
                nif = this.getQ07C03();
            } else if ("AS4".equalsIgnoreCase(titular)) {
                nif = this.getQ07C04();
            }
        }
        return nif;
    }

    public String getTitularAscendentesEmComunhao(Long nif) {
        String titular = null;
        if (nif != null) {
            if (nif.equals(this.getQ07C01())) {
                titular = "AS1";
            } else if (nif.equals(this.getQ07C02())) {
                titular = "AS2";
            } else if (nif.equals(this.getQ07C03())) {
                titular = "AS3";
            } else if (nif.equals(this.getQ07C04())) {
                titular = "AS4";
            }
        }
        return titular;
    }

    public static String getLinkToAscendentesEmComunhao(String titular) {
        String link = "";
        if (titular != null) {
            if ("AS1".equalsIgnoreCase(titular)) {
                link = "aRosto.qQuadro07.fq07C01";
            } else if ("AS2".equalsIgnoreCase(titular)) {
                link = "aRosto.qQuadro07.fq07C02";
            } else if ("AS3".equalsIgnoreCase(titular)) {
                link = "aRosto.qQuadro07.fq07C03";
            } else if ("AS4".equalsIgnoreCase(titular)) {
                link = "aRosto.qQuadro07.fq07C04";
            }
        }
        return link;
    }

    public String getLinkToAscendentesEmComunhaoByNif(Long nif) {
        String link = null;
        if (nif != null) {
            link = nif.equals(this.getQ07C01()) ? "aRosto.qQuadro07.fq07C01" : (nif.equals(this.getQ07C02()) ? "aRosto.qQuadro07.fq07C02" : (nif.equals(this.getQ07C03()) ? "aRosto.qQuadro07.fq07C03" : (nif.equals(this.getQ07C04()) ? "aRosto.qQuadro07.fq07C04" : "")));
        }
        return link;
    }

    public boolean isEmpty() {
        return this.isSubQuadroAEmpty() && this.isSubQuadroBEmpty() && this.isSubQuadroEEmpty() && this.isSubQuadroCEmpty() && this.isSubQuadroDEmpty();
    }

    private boolean isSubQuadroAEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C1()) && (this.getQ07C1b() == null || this.getQ07C1b() == false) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C1a());
    }

    private boolean isSubQuadroBEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C01()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C01a()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C02()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C02a()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C03()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C03a()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C04()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ07C04a());
    }

    private boolean isSubQuadroCEmpty() {
        return this.getRostoq07CT1() == null || this.getRostoq07CT1().isEmpty();
    }

    private boolean isSubQuadroDEmpty() {
        return StringUtil.isEmpty(this.getQ07D01()) && (this.getQ07D01a() == null || this.getQ07D01a() == false);
    }

    private boolean isSubQuadroEEmpty() {
        return this.getRostoq07ET1() == null || this.getRostoq07ET1().isEmpty();
    }
}

