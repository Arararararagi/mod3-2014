/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Rostoq07CT1_Linha
extends Rostoq07CT1_LinhaBase {
    public Rostoq07CT1_Linha() {
    }

    public Rostoq07CT1_Linha(Long nIF) {
        super(nIF);
    }

    public static String getlink(int line) {
        return "aRosto.qQuadro07.trostoq07CT1.l" + line;
    }

    public static String getlink(int line, Rostoq07CT1_LinhaBase.Property column) {
        return "aRosto.qQuadro07.trostoq07CT1.l" + line + ".c" + column.getIndex();
    }

    @Override
    public void setNIF(Long nIF) {
        RostoModel rosto;
        boolean isTofireEvent = false;
        int rowIndex = -1;
        Long oldNif = this.getNIF();
        if (!(oldNif == null || oldNif.equals(nIF))) {
            isTofireEvent = true;
            rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
            EventList<Rostoq07CT1_Linha> rostoq07CT1Rows = rosto.getQuadro07().getRostoq07CT1();
            if (rostoq07CT1Rows != null) {
                for (int i = 0; i < rostoq07CT1Rows.size(); ++i) {
                    if (oldNif != rostoq07CT1Rows.get(i).getNIF()) continue;
                    rowIndex = i;
                    break;
                }
            }
        }
        super.setNIF(nIF != null ? new Long(nIF) : null);
        if (isTofireEvent) {
            rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
            rosto.getRostoq07CT1ChangeEvent().fireUpdateRow(rowIndex, false, false);
        }
    }
}

