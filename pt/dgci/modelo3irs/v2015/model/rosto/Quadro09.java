/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09Base;

public class Quadro09
extends Quadro09Base {
    public boolean isQ09B1OP1Selected() {
        return this.getQ09B1() != null && this.getQ09B1().equals("1");
    }

    public boolean isQ09B1OP2Selected() {
        return this.getQ09B1() != null && this.getQ09B1().equals("2");
    }
}

