/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.dgci.modelo3irs.v2015.model.rosto.Quadro01;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro04;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro06;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro07;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro09;
import pt.dgci.modelo3irs.v2015.model.rosto.QuadroInicio;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.util.HashCodeUtil;

@Min(value=1)
@Max(value=1)
public class RostoModelBase
extends AnexoModel {
    public static final String ROSTO_LINK = "aRosto";

    public RostoModelBase(FormKey key, boolean addQuadrosToModel) {
        super(key);
        if (addQuadrosToModel) {
            this.addQuadro(new QuadroInicio());
            this.addQuadro(new Quadro01());
            this.addQuadro(new Quadro02());
            this.addQuadro(new Quadro03());
            this.addQuadro(new Quadro04());
            this.addQuadro(new Quadro05());
            this.addQuadro(new Quadro06());
            this.addQuadro(new Quadro07());
            this.addQuadro(new Quadro09());
        }
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.getQuadro(QuadroInicio.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro01.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro02.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro03.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro04.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro05.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro06.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro07.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro09.class.getSimpleName()));
        return result;
    }
}

