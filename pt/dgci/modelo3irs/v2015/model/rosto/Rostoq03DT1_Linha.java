/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_LinhaBase;
import pt.dgci.modelo3irs.v2015.util.observer.RostoQuadroChangeEvent;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class Rostoq03DT1_Linha
extends Rostoq03DT1_LinhaBase {
    public Rostoq03DT1_Linha() {
    }

    public Rostoq03DT1_Linha(Long nIF, Long deficienteGrau, Long nifProgenitor) {
        super(nIF, deficienteGrau, nifProgenitor);
    }

    public static String getlink(int line) {
        return "aRosto.qQuadro03.trostoq03DT1.l" + line;
    }

    public static String getlink(int line, Rostoq03DT1_LinhaBase.Property column) {
        return "aRosto.qQuadro03.trostoq03DT1.l" + line + ".c" + column.getIndex();
    }

    @Override
    public void setNIF(Long nIF) {
        RostoModel rosto;
        boolean isTofireEvent = false;
        int rowIndex = -1;
        Long oldNif = this.getNIF();
        if (!(oldNif == null || oldNif.equals(nIF))) {
            isTofireEvent = true;
            rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
            EventList<Rostoq03DT1_Linha> rostoq03DT1Rows = rosto.getQuadro03().getRostoq03DT1();
            if (rostoq03DT1Rows != null) {
                for (int i = 0; i < rostoq03DT1Rows.size(); ++i) {
                    if (oldNif != rostoq03DT1Rows.get(i).getNIF()) continue;
                    rowIndex = i;
                    break;
                }
            }
        }
        super.setNIF(nIF != null ? new Long(nIF) : null);
        if (isTofireEvent) {
            rosto = (RostoModel)Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(RostoModel.class);
            rosto.getRostoq03DT1ChangeEvent().fireUpdateRow(rowIndex, false, false);
        }
    }
}

