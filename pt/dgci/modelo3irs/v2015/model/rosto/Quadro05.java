/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.rosto;

import pt.dgci.modelo3irs.v2015.model.rosto.Quadro05Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro05
extends Quadro05Base {
    public boolean isRepresentante(Long nif) {
        return this.getQ05C5() != null && this.getQ05C5().equals(nif);
    }

    public boolean isQ05B1OP1Selected() {
        return this.getQ05B1() != null && this.getQ05B1().equals("1");
    }

    public boolean isQ05B1OP2Selected() {
        return this.getQ05B1() != null && this.getQ05B1().equals("2");
    }

    public boolean isQ05B1OP3Selected() {
        return this.getQ05B1() != null && this.getQ05B1().equals("3");
    }

    public boolean isQ05B1OP4Selected() {
        return this.getQ05B1() != null && this.getQ05B1().equals("4");
    }

    public boolean isQ05B2OP6Selected() {
        return this.getQ05B2() != null && this.getQ05B2().equals("6");
    }

    public boolean isQ05B2OP7Selected() {
        return this.getQ05B2() != null && this.getQ05B2().equals("7");
    }

    public boolean isQ05B3OP8Selected() {
        return this.getQ05B3() != null && this.getQ05B3().equals("8");
    }

    public boolean isQ05B3OP9Selected() {
        return this.getQ05B3() != null && this.getQ05B3().equals("9");
    }

    public boolean isQ05B4OP10Selected() {
        return this.getQ05B4() != null && this.getQ05B4().equals("10");
    }

    public boolean isQ05B4OP11Selected() {
        return this.getQ05B4() != null && this.getQ05B4().equals("11");
    }

    public boolean isQ05C5Filled() {
        return !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ05C5());
    }

    public boolean isQ05C12Filled() {
        return !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ05C12());
    }

    public boolean isQ05C13Selected() {
        return !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQ05C13());
    }
}

