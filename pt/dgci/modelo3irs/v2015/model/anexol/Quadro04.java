/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import ca.odell.glazedlists.EventList;
import java.util.HashSet;
import java.util.Set;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04Base;
import pt.opensoft.util.Tuple;

public class Quadro04
extends Quadro04Base {
    public Set<Tuple> getDistinctEntidadeCodRendimento() {
        HashSet<Tuple> result = new HashSet<Tuple>();
        for (int linha = 0; linha < this.getAnexoLq04T1().size(); ++linha) {
            AnexoLq04T1_Linha anexoLq04T1_Linha = this.getAnexoLq04T1().get(linha);
            if (anexoLq04T1_Linha.getEntidade() == null || anexoLq04T1_Linha.getCodRendAnexoA() == null) continue;
            Tuple tuple = new Tuple(new Object[]{anexoLq04T1_Linha.getEntidade(), anexoLq04T1_Linha.getCodRendAnexoA()});
            result.add(tuple);
        }
        return result;
    }

    public long getTotalRendimentosQ4T4AByEntidadeCodRendimentos(long entidade, long codRendimentos) {
        long result = 0;
        for (int linha = 0; linha < this.getAnexoLq04T1().size(); ++linha) {
            AnexoLq04T1_Linha anexoLq04T1Linha = this.getAnexoLq04T1().get(linha);
            if (anexoLq04T1Linha.getEntidade() == null || !anexoLq04T1Linha.getEntidade().equals(entidade) || anexoLq04T1Linha.getCodRendAnexoA() == null || !anexoLq04T1Linha.getCodRendAnexoA().equals(codRendimentos)) continue;
            result+=anexoLq04T1Linha.getRendimento() != null ? anexoLq04T1Linha.getRendimento() : 0;
        }
        return result;
    }

    public boolean isEmpty() {
        return this.getAnexoLq04T1().isEmpty() && this.getAnexoLq04T2().isEmpty() && this.getAnexoLq04T3().isEmpty();
    }
}

