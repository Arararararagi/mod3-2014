/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_LinhaBase;

public class AnexoLq05T1_Linha
extends AnexoLq05T1_LinhaBase {
    public AnexoLq05T1_Linha() {
    }

    public AnexoLq05T1_Linha(Long campoNLinhaQ6, Long codActividade, String catAouB, Long pais, Long rendimento, Long impostoPagoAoEstado, Boolean semImpostoPagoNoEstrangeiro) {
        super(campoNLinhaQ6, codActividade, catAouB, pais, rendimento, impostoPagoAoEstado, semImpostoPagoNoEstrangeiro);
    }

    public static String getLink(int line) {
        return "aAnexoL.qQuadro05.tanexoLq05T1.l" + line;
    }

    public static String getLink(int line, AnexoLq05T1_LinhaBase.Property column) {
        return "aAnexoL.qQuadro05.tanexoLq05T1.l" + line + ".c" + column.getIndex();
    }
}

