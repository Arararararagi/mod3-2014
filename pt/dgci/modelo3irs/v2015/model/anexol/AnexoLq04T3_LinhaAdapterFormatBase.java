/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoLq04T3_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoLq04T3_Linha>,
WritableTableFormat<AnexoLq04T3_Linha> {
    @Override
    public boolean isEditable(AnexoLq04T3_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoLq04T3_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getEntidade();
            }
            case 2: {
                if (line == null || line.getCodActividade() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_CodActividade", line.getCodActividade());
            }
            case 3: {
                return line.getLucro();
            }
            case 4: {
                return line.getPrejuizo();
            }
        }
        return null;
    }

    @Override
    public AnexoLq04T3_Linha setColumnValue(AnexoLq04T3_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setEntidade((Long)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCodActividade((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setLucro((Long)value);
                return line;
            }
            case 4: {
                line.setPrejuizo((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF da entidade pagadora";
            }
            case 2: {
                return "C\u00f3digo Atividade";
            }
            case 3: {
                return "Lucro";
            }
            case 4: {
                return "Preju\u00edzo";
            }
        }
        return null;
    }
}

