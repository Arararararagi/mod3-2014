/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_LinhaBase;

public class AnexoLq04T3_Linha
extends AnexoLq04T3_LinhaBase {
    public AnexoLq04T3_Linha() {
    }

    public AnexoLq04T3_Linha(Long entidade, Long codActividade, Long lucro, Long prejuizo) {
        super(entidade, codActividade, lucro, prejuizo);
    }

    public static String getLink(int line) {
        return "aAnexoL.qQuadro04.tanexoLq04T3.l" + line;
    }

    public static String getLink(int line, AnexoLq04T3_LinhaBase.Property column) {
        return "aAnexoL.qQuadro04.tanexoLq04T3.l" + line + ".c" + column.getIndex();
    }
}

