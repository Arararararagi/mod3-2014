/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoL.qQuadro03";
    public static final String ANEXOLQ03C02_LINK = "aAnexoL.qQuadro03.fanexoLq03C02";
    public static final String ANEXOLQ03C02 = "anexoLq03C02";
    private Long anexoLq03C02;
    public static final String ANEXOLQ03C03_LINK = "aAnexoL.qQuadro03.fanexoLq03C03";
    public static final String ANEXOLQ03C03 = "anexoLq03C03";
    private Long anexoLq03C03;
    public static final String ANEXOLQ03C04_LINK = "aAnexoL.qQuadro03.fanexoLq03C04";
    public static final String ANEXOLQ03C04 = "anexoLq03C04";
    private Long anexoLq03C04;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoLq03C02() {
        return this.anexoLq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoLq03C02(Long anexoLq03C02) {
        Long oldValue = this.anexoLq03C02;
        this.anexoLq03C02 = anexoLq03C02;
        this.firePropertyChange("anexoLq03C02", oldValue, this.anexoLq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoLq03C03() {
        return this.anexoLq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoLq03C03(Long anexoLq03C03) {
        Long oldValue = this.anexoLq03C03;
        this.anexoLq03C03 = anexoLq03C03;
        this.firePropertyChange("anexoLq03C03", oldValue, this.anexoLq03C03);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoLq03C04() {
        return this.anexoLq03C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoLq03C04(Long anexoLq03C04) {
        Long oldValue = this.anexoLq03C04;
        this.anexoLq03C04 = anexoLq03C04;
        this.firePropertyChange("anexoLq03C04", oldValue, this.anexoLq03C04);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoLq03C02);
        result = HashCodeUtil.hash(result, this.anexoLq03C03);
        result = HashCodeUtil.hash(result, this.anexoLq03C04);
        return result;
    }
}

