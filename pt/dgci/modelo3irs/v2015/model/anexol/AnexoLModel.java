/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.dgci.modelo3irs.v2015.model.OwnableAnexo;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModelBase;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro06;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=9)
public class AnexoLModel
extends AnexoLModelBase
implements OwnableAnexo {
    public AnexoLModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
        this.postCreate();
    }

    protected final void postCreate() {
        Quadro03 quadro03Model = this.getQuadro03();
        if (quadro03Model != null) {
            quadro03Model.setAnexoLq03C04(Long.parseLong(this.formKey.getSubId()));
        }
    }

    public Long getAnexoLTitular() {
        Quadro03 quadro03Model = this.getQuadro03();
        if (quadro03Model != null) {
            return this.getQuadro03().getAnexoLq03C04();
        }
        return null;
    }

    @Override
    public boolean isOwnedBy(Long nif) {
        return nif != null && nif.equals(this.getQuadro03().getAnexoLq03C04());
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public String getLink(String linkWithoutSubId) {
        return linkWithoutSubId.replaceFirst("aAnexoL", "aAnexoL|" + this.getFormKey().getSubId());
    }

    public boolean isEmpty() {
        return this.getQuadro04().isEmpty() && this.getQuadro05().isEmpty() && this.getQuadro06().isEmpty();
    }
}

