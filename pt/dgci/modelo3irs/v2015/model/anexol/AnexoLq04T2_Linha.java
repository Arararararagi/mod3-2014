/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_LinhaBase;

public class AnexoLq04T2_Linha
extends AnexoLq04T2_LinhaBase {
    public AnexoLq04T2_Linha() {
    }

    public AnexoLq04T2_Linha(Long entidade, Long campoQ4AnexoB, Long codActividade, Long rendimento) {
        super(entidade, campoQ4AnexoB, codActividade, rendimento);
    }

    public static String getLink(int line) {
        return "aAnexoL.qQuadro04.tanexoLq04T2.l" + line;
    }

    public static String getLink(int line, AnexoLq04T2_LinhaBase.Property column) {
        return "aAnexoL.qQuadro04.tanexoLq04T2.l" + line + ".c" + column.getIndex();
    }
}

