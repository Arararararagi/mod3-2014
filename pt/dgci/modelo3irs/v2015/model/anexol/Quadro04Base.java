/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T3_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoL.qQuadro04";
    public static final String ANEXOLQ04T1_LINK = "aAnexoL.qQuadro04.tanexoLq04T1";
    private EventList<AnexoLq04T1_Linha> anexoLq04T1 = new BasicEventList<AnexoLq04T1_Linha>();
    public static final String ANEXOLQ04T2_LINK = "aAnexoL.qQuadro04.tanexoLq04T2";
    private EventList<AnexoLq04T2_Linha> anexoLq04T2 = new BasicEventList<AnexoLq04T2_Linha>();
    public static final String ANEXOLQ04T3_LINK = "aAnexoL.qQuadro04.tanexoLq04T3";
    private EventList<AnexoLq04T3_Linha> anexoLq04T3 = new BasicEventList<AnexoLq04T3_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoLq04T1_Linha> getAnexoLq04T1() {
        return this.anexoLq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoLq04T1(EventList<AnexoLq04T1_Linha> anexoLq04T1) {
        this.anexoLq04T1 = anexoLq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoLq04T2_Linha> getAnexoLq04T2() {
        return this.anexoLq04T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoLq04T2(EventList<AnexoLq04T2_Linha> anexoLq04T2) {
        this.anexoLq04T2 = anexoLq04T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoLq04T3_Linha> getAnexoLq04T3() {
        return this.anexoLq04T3;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoLq04T3(EventList<AnexoLq04T3_Linha> anexoLq04T3) {
        this.anexoLq04T3 = anexoLq04T3;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoLq04T1);
        result = HashCodeUtil.hash(result, this.anexoLq04T2);
        result = HashCodeUtil.hash(result, this.anexoLq04T3);
        return result;
    }
}

