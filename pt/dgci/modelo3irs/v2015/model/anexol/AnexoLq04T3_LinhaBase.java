/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoLq04T3_LinhaBase
extends Model {
    public static final String ENTIDADE_LINK = "aAnexoL.qQuadro04.fentidade";
    public static final String ENTIDADE = "entidade";
    private Long entidade;
    public static final String CODACTIVIDADE_LINK = "aAnexoL.qQuadro04.fcodActividade";
    public static final String CODACTIVIDADE = "codActividade";
    private Long codActividade;
    public static final String LUCRO_LINK = "aAnexoL.qQuadro04.flucro";
    public static final String LUCRO = "lucro";
    private Long lucro;
    public static final String PREJUIZO_LINK = "aAnexoL.qQuadro04.fprejuizo";
    public static final String PREJUIZO = "prejuizo";
    private Long prejuizo;

    public AnexoLq04T3_LinhaBase(Long entidade, Long codActividade, Long lucro, Long prejuizo) {
        this.entidade = entidade;
        this.codActividade = codActividade;
        this.lucro = lucro;
        this.prejuizo = prejuizo;
    }

    public AnexoLq04T3_LinhaBase() {
        this.entidade = null;
        this.codActividade = null;
        this.lucro = null;
        this.prejuizo = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getEntidade() {
        return this.entidade;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setEntidade(Long entidade) {
        Long oldValue = this.entidade;
        this.entidade = entidade;
        this.firePropertyChange("entidade", oldValue, this.entidade);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_CodActividade")
    public Long getCodActividade() {
        return this.codActividade;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodActividade(Long codActividade) {
        Long oldValue = this.codActividade;
        this.codActividade = codActividade;
        this.firePropertyChange("codActividade", oldValue, this.codActividade);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getLucro() {
        return this.lucro;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setLucro(Long lucro) {
        Long oldValue = this.lucro;
        this.lucro = lucro;
        this.firePropertyChange("lucro", oldValue, this.lucro);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getPrejuizo() {
        return this.prejuizo;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setPrejuizo(Long prejuizo) {
        Long oldValue = this.prejuizo;
        this.prejuizo = prejuizo;
        this.firePropertyChange("prejuizo", oldValue, this.prejuizo);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.entidade);
        result = HashCodeUtil.hash(result, this.codActividade);
        result = HashCodeUtil.hash(result, this.lucro);
        result = HashCodeUtil.hash(result, this.prejuizo);
        return result;
    }

    public static enum Property {
        ENTIDADE("entidade", 2),
        CODACTIVIDADE("codActividade", 3),
        LUCRO("lucro", 4),
        PREJUIZO("prejuizo", 5);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

