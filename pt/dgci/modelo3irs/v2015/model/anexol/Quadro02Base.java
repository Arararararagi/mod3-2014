/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoL.qQuadro02";
    public static final String ANEXOLQ02C01_LINK = "aAnexoL.qQuadro02.fanexoLq02C01";
    public static final String ANEXOLQ02C01 = "anexoLq02C01";
    private Long anexoLq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoLq02C01() {
        return this.anexoLq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoLq02C01(Long anexoLq02C01) {
        Long oldValue = this.anexoLq02C01;
        this.anexoLq02C01 = anexoLq02C01;
        this.firePropertyChange("anexoLq02C01", oldValue, this.anexoLq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoLq02C01);
        return result;
    }
}

