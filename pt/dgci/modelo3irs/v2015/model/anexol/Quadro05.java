/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexol.Quadro05Base;

public class Quadro05
extends Quadro05Base {
    public long getTotalRendimentoByCampoQ6AnexoJ(long campoQ6AnexoJ) {
        long totalRendimento = 0;
        for (AnexoLq05T1_Linha anexoLq05T1Linha : this.getAnexoLq05T1()) {
            if (anexoLq05T1Linha.getRendimento() == null || anexoLq05T1Linha.getCampoNLinhaQ6() == null || anexoLq05T1Linha.getCampoNLinhaQ6() != campoQ6AnexoJ) continue;
            totalRendimento+=anexoLq05T1Linha.getRendimento().longValue();
        }
        return totalRendimento;
    }

    public long getTotalImpostoByCampoQ4AnexoJ(long campoQ6AnexoJ) {
        long totalImposto = 0;
        for (AnexoLq05T1_Linha anexoLq05T1Linha : this.getAnexoLq05T1()) {
            if (anexoLq05T1Linha.getImpostoPagoNoEstrangeiro() == null || anexoLq05T1Linha.getCampoNLinhaQ6() == null || anexoLq05T1Linha.getCampoNLinhaQ6() != campoQ6AnexoJ) continue;
            totalImposto+=anexoLq05T1Linha.getImpostoPagoNoEstrangeiro().longValue();
        }
        return totalImposto;
    }

    public boolean isEmpty() {
        return this.getAnexoLq05T1().isEmpty();
    }
}

