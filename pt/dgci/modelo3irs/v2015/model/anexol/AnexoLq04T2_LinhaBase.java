/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoLq04T2_LinhaBase
extends Model {
    public static final String ENTIDADE_LINK = "aAnexoL.qQuadro04.fentidade";
    public static final String ENTIDADE = "entidade";
    private Long entidade;
    public static final String CAMPOQ4ANEXOB_LINK = "aAnexoL.qQuadro04.fcampoQ4AnexoB";
    public static final String CAMPOQ4ANEXOB = "campoQ4AnexoB";
    private Long campoQ4AnexoB;
    public static final String CODACTIVIDADE_LINK = "aAnexoL.qQuadro04.fcodActividade";
    public static final String CODACTIVIDADE = "codActividade";
    private Long codActividade;
    public static final String RENDIMENTO_LINK = "aAnexoL.qQuadro04.frendimento";
    public static final String RENDIMENTO = "rendimento";
    private Long rendimento;

    public AnexoLq04T2_LinhaBase(Long entidade, Long campoQ4AnexoB, Long codActividade, Long rendimento) {
        this.entidade = entidade;
        this.campoQ4AnexoB = campoQ4AnexoB;
        this.codActividade = codActividade;
        this.rendimento = rendimento;
    }

    public AnexoLq04T2_LinhaBase() {
        this.entidade = null;
        this.campoQ4AnexoB = null;
        this.codActividade = null;
        this.rendimento = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getEntidade() {
        return this.entidade;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setEntidade(Long entidade) {
        Long oldValue = this.entidade;
        this.entidade = entidade;
        this.firePropertyChange("entidade", oldValue, this.entidade);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoLCampoQ4AnexoB")
    public Long getCampoQ4AnexoB() {
        return this.campoQ4AnexoB;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoQ4AnexoB(Long campoQ4AnexoB) {
        Long oldValue = this.campoQ4AnexoB;
        this.campoQ4AnexoB = campoQ4AnexoB;
        this.firePropertyChange("campoQ4AnexoB", oldValue, this.campoQ4AnexoB);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_CodActividade")
    public Long getCodActividade() {
        return this.codActividade;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodActividade(Long codActividade) {
        Long oldValue = this.codActividade;
        this.codActividade = codActividade;
        this.firePropertyChange("codActividade", oldValue, this.codActividade);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimento() {
        return this.rendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimento(Long rendimento) {
        Long oldValue = this.rendimento;
        this.rendimento = rendimento;
        this.firePropertyChange("rendimento", oldValue, this.rendimento);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.entidade);
        result = HashCodeUtil.hash(result, this.campoQ4AnexoB);
        result = HashCodeUtil.hash(result, this.codActividade);
        result = HashCodeUtil.hash(result, this.rendimento);
        return result;
    }

    public static enum Property {
        ENTIDADE("entidade", 2),
        CAMPOQ4ANEXOB("campoQ4AnexoB", 3),
        CODACTIVIDADE("codActividade", 4),
        RENDIMENTO("rendimento", 5);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

