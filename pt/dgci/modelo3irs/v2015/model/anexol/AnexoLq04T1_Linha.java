/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq04T1_LinhaBase;

public class AnexoLq04T1_Linha
extends AnexoLq04T1_LinhaBase {
    public AnexoLq04T1_Linha() {
    }

    public AnexoLq04T1_Linha(Long entidade, Long codRendAnexoA, Long codActividade, Long rendimento) {
        super(entidade, codRendAnexoA, codActividade, rendimento);
    }

    public static String getLink(int line) {
        return "aAnexoL.qQuadro04.tanexoLq04T1.l" + line;
    }

    public static String getLink(int line, AnexoLq04T1_LinhaBase.Property column) {
        return "aAnexoL.qQuadro04.tanexoLq04T1.l" + line + ".c" + column.getIndex();
    }
}

