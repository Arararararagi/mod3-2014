/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import pt.dgci.modelo3irs.v2015.model.anexol.Quadro06Base;

public class Quadro06
extends Quadro06Base {
    public boolean isQ06B1OP1Selected() {
        return this.getQ06B1() != null && this.getQ06B1().equals("1");
    }

    public boolean isQ06B1OP2Selected() {
        return this.getQ06B1() != null && this.getQ06B1().equals("2");
    }

    public boolean isQ06B2OP3Selected() {
        return this.getQ06B2() != null && this.getQ06B2().equals("3");
    }

    public boolean isQ06B2OP4Selected() {
        return this.getQ06B2() != null && this.getQ06B2().equals("4");
    }

    public boolean isEmpty() {
        return this.getQ06B1() == null && this.getQ06B2() == null;
    }
}

