/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoLq05T1_LinhaBase
extends Model {
    public static final String CAMPONLINHAQ6_LINK = "aAnexoL.qQuadro05.fcampoNLinhaQ6";
    public static final String CAMPONLINHAQ6 = "campoNLinhaQ6";
    private Long campoNLinhaQ6;
    public static final String CODACTIVIDADE_LINK = "aAnexoL.qQuadro05.fcodActividade";
    public static final String CODACTIVIDADE = "codActividade";
    private Long codActividade;
    public static final String CATAOUB_LINK = "aAnexoL.qQuadro05.fcatAouB";
    public static final String CATAOUB = "catAouB";
    private String catAouB;
    public static final String PAIS_LINK = "aAnexoL.qQuadro05.fpais";
    public static final String PAIS = "pais";
    private Long pais;
    public static final String RENDIMENTO_LINK = "aAnexoL.qQuadro05.frendimento";
    public static final String RENDIMENTO = "rendimento";
    private Long rendimento;
    public static final String IMPOSTOPAGONOESTRANGEIRO_LINK = "aAnexoL.qQuadro05.fimpostoPagoNoEstrangeiro";
    public static final String IMPOSTOPAGONOESTRANGEIRO = "impostoPagoNoEstrangeiro";
    private Long impostoPagoNoEstrangeiro;
    public static final String SEMIMPOSTOPAGONOESTRANGEIRO_LINK = "aAnexoL.qQuadro05.fsemImpostoPagoNoEstrangeiro";
    public static final String SEMIMPOSTOPAGONOESTRANGEIRO = "semImpostoPagoNoEstrangeiro";
    private Boolean semImpostoPagoNoEstrangeiro;

    public AnexoLq05T1_LinhaBase(Long campoNLinhaQ6, Long codActividade, String catAouB, Long pais, Long rendimento, Long impostoPagoNoEstrangeiro, Boolean semImpostoPagoNoEstrangeiro) {
        this.campoNLinhaQ6 = campoNLinhaQ6;
        this.codActividade = codActividade;
        this.catAouB = catAouB;
        this.pais = pais;
        this.rendimento = rendimento;
        this.impostoPagoNoEstrangeiro = impostoPagoNoEstrangeiro;
        this.semImpostoPagoNoEstrangeiro = semImpostoPagoNoEstrangeiro;
    }

    public AnexoLq05T1_LinhaBase() {
        this.campoNLinhaQ6 = null;
        this.codActividade = null;
        this.catAouB = null;
        this.pais = null;
        this.rendimento = null;
        this.impostoPagoNoEstrangeiro = null;
        this.semImpostoPagoNoEstrangeiro = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoLCampoQ6AnexoJ")
    public Long getCampoNLinhaQ6() {
        return this.campoNLinhaQ6;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoNLinhaQ6(Long campoNLinhaQ6) {
        Long oldValue = this.campoNLinhaQ6;
        this.campoNLinhaQ6 = campoNLinhaQ6;
        this.firePropertyChange("campoNLinhaQ6", oldValue, this.campoNLinhaQ6);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_CodActividade")
    public Long getCodActividade() {
        return this.codActividade;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodActividade(Long codActividade) {
        Long oldValue = this.codActividade;
        this.codActividade = codActividade;
        this.firePropertyChange("codActividade", oldValue, this.codActividade);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoLCatAouB")
    public String getCatAouB() {
        return this.catAouB;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCatAouB(String catAouB) {
        String oldValue = this.catAouB;
        this.catAouB = catAouB;
        this.firePropertyChange("catAouB", oldValue, this.catAouB);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxL")
    public Long getPais() {
        return this.pais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setPais(Long pais) {
        Long oldValue = this.pais;
        this.pais = pais;
        this.firePropertyChange("pais", oldValue, this.pais);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimento() {
        return this.rendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimento(Long rendimento) {
        Long oldValue = this.rendimento;
        this.rendimento = rendimento;
        this.firePropertyChange("rendimento", oldValue, this.rendimento);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getImpostoPagoNoEstrangeiro() {
        return this.impostoPagoNoEstrangeiro;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setImpostoPagoNoEstrangeiro(Long impostoPagoNoEstrangeiro) {
        Long oldValue = this.impostoPagoNoEstrangeiro;
        this.impostoPagoNoEstrangeiro = impostoPagoNoEstrangeiro;
        this.firePropertyChange("impostoPagoNoEstrangeiro", oldValue, this.impostoPagoNoEstrangeiro);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getSemImpostoPagoNoEstrangeiro() {
        return this.semImpostoPagoNoEstrangeiro;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setSemImpostoPagoNoEstrangeiro(Boolean semImpostoPagoNoEstrangeiro) {
        Boolean oldValue = this.semImpostoPagoNoEstrangeiro;
        this.semImpostoPagoNoEstrangeiro = semImpostoPagoNoEstrangeiro;
        this.firePropertyChange("semImpostoPagoNoEstrangeiro", oldValue, this.semImpostoPagoNoEstrangeiro);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.campoNLinhaQ6);
        result = HashCodeUtil.hash(result, this.codActividade);
        result = HashCodeUtil.hash(result, this.catAouB);
        result = HashCodeUtil.hash(result, this.pais);
        result = HashCodeUtil.hash(result, this.rendimento);
        result = HashCodeUtil.hash(result, this.impostoPagoNoEstrangeiro);
        result = HashCodeUtil.hash(result, this.semImpostoPagoNoEstrangeiro);
        return result;
    }

    public static enum Property {
        CAMPONLINHAQ6("campoNLinhaQ6", 2),
        CODACTIVIDADE("codActividade", 3),
        CATAOUB("catAouB", 4),
        PAIS("pais", 5),
        RENDIMENTO("rendimento", 6),
        IMPOSTOPAGONOESTRANGEIRO("impostoPagoNoEstrangeiro", 7),
        SEMIMPOSTOPAGONOESTRANGEIRO("semImpostoPagoNoEstrangeiro", 8);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

