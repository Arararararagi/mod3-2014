/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoLq04T1_LinhaBase
extends Model {
    public static final String ENTIDADE_LINK = "aAnexoL.qQuadro04.fentidade";
    public static final String ENTIDADE = "entidade";
    private Long entidade;
    public static final String CODRENDANEXOA_LINK = "aAnexoL.qQuadro04.fcodRendAnexoA";
    public static final String CODRENDANEXOA = "codRendAnexoA";
    private Long codRendAnexoA;
    public static final String CODACTIVIDADE_LINK = "aAnexoL.qQuadro04.fcodActividade";
    public static final String CODACTIVIDADE = "codActividade";
    private Long codActividade;
    public static final String RENDIMENTO_LINK = "aAnexoL.qQuadro04.frendimento";
    public static final String RENDIMENTO = "rendimento";
    private Long rendimento;

    public AnexoLq04T1_LinhaBase(Long entidade, Long codRendAnexoA, Long codActividade, Long rendimento) {
        this.entidade = entidade;
        this.codRendAnexoA = codRendAnexoA;
        this.codActividade = codActividade;
        this.rendimento = rendimento;
    }

    public AnexoLq04T1_LinhaBase() {
        this.entidade = null;
        this.codRendAnexoA = null;
        this.codActividade = null;
        this.rendimento = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getEntidade() {
        return this.entidade;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setEntidade(Long entidade) {
        Long oldValue = this.entidade;
        this.entidade = entidade;
        this.firePropertyChange("entidade", oldValue, this.entidade);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoLCodRendAnexoA")
    public Long getCodRendAnexoA() {
        return this.codRendAnexoA;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodRendAnexoA(Long codRendAnexoA) {
        Long oldValue = this.codRendAnexoA;
        this.codRendAnexoA = codRendAnexoA;
        this.firePropertyChange("codRendAnexoA", oldValue, this.codRendAnexoA);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_CodActividade")
    public Long getCodActividade() {
        return this.codActividade;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodActividade(Long codActividade) {
        Long oldValue = this.codActividade;
        this.codActividade = codActividade;
        this.firePropertyChange("codActividade", oldValue, this.codActividade);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimento() {
        return this.rendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimento(Long rendimento) {
        Long oldValue = this.rendimento;
        this.rendimento = rendimento;
        this.firePropertyChange("rendimento", oldValue, this.rendimento);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.entidade);
        result = HashCodeUtil.hash(result, this.codRendAnexoA);
        result = HashCodeUtil.hash(result, this.codActividade);
        result = HashCodeUtil.hash(result, this.rendimento);
        return result;
    }

    public static enum Property {
        ENTIDADE("entidade", 2),
        CODRENDANEXOA("codRendAnexoA", 3),
        CODACTIVIDADE("codActividade", 4),
        RENDIMENTO("rendimento", 5);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

