/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoLq05T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoLq05T1_Linha>,
WritableTableFormat<AnexoLq05T1_Linha> {
    @Override
    public boolean isEditable(AnexoLq05T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoLq05T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCampoNLinhaQ6() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoLCampoQ6AnexoJ", line.getCampoNLinhaQ6());
            }
            case 2: {
                if (line == null || line.getCodActividade() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_CodActividade", line.getCodActividade());
            }
            case 3: {
                if (line == null || line.getCatAouB() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoLCatAouB", line.getCatAouB());
            }
            case 4: {
                if (line == null || line.getPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxL", line.getPais());
            }
            case 5: {
                return line.getRendimento();
            }
            case 6: {
                return line.getImpostoPagoNoEstrangeiro();
            }
            case 7: {
                return line.getSemImpostoPagoNoEstrangeiro();
            }
        }
        return null;
    }

    @Override
    public AnexoLq05T1_Linha setColumnValue(AnexoLq05T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCampoNLinhaQ6((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCodActividade((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                if (value != null) {
                    line.setCatAouB((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 4: {
                if (value != null) {
                    line.setPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 5: {
                line.setRendimento((Long)value);
                return line;
            }
            case 6: {
                line.setImpostoPagoNoEstrangeiro((Long)value);
                return line;
            }
            case 7: {
                line.setSemImpostoPagoNoEstrangeiro((Boolean)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Boolean.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "N\u00ba Linha Q6 Anexo J";
            }
            case 2: {
                return "C\u00f3digo Atividade";
            }
            case 3: {
                return "Categoria A ou B";
            }
            case 4: {
                return "Pa\u00eds";
            }
            case 5: {
                return "Rendimento";
            }
            case 6: {
                return "Imposto Pago no Estrangeiro";
            }
            case 7: {
                return "Sem Imposto Pago no Estrangeiro";
            }
        }
        return null;
    }
}

