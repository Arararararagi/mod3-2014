/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexol;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLq05T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoL.qQuadro05";
    public static final String ANEXOLQ05T1_LINK = "aAnexoL.qQuadro05.tanexoLq05T1";
    private EventList<AnexoLq05T1_Linha> anexoLq05T1 = new BasicEventList<AnexoLq05T1_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoLq05T1_Linha> getAnexoLq05T1() {
        return this.anexoLq05T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoLq05T1(EventList<AnexoLq05T1_Linha> anexoLq05T1) {
        this.anexoLq05T1 = anexoLq05T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoLq05T1);
        return result;
    }
}

