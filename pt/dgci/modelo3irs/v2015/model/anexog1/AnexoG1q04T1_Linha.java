/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_LinhaBase;

public class AnexoG1q04T1_Linha
extends AnexoG1q04T1_LinhaBase {
    public AnexoG1q04T1_Linha() {
    }

    public AnexoG1q04T1_Linha(Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao) {
        super(mesRealizacao, valorRealizacao, anoAquisicao, mesAquisicao, valorAquisicao);
    }
}

