/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoG1q04T1_LinhaBase
extends Model {
    public static final String MESREALIZACAO_LINK = "aAnexoG1.qQuadro04.fmesRealizacao";
    public static final String MESREALIZACAO = "mesRealizacao";
    private Long mesRealizacao;
    public static final String VALORREALIZACAO_LINK = "aAnexoG1.qQuadro04.fvalorRealizacao";
    public static final String VALORREALIZACAO = "valorRealizacao";
    private Long valorRealizacao;
    public static final String ANOAQUISICAO_LINK = "aAnexoG1.qQuadro04.fanoAquisicao";
    public static final String ANOAQUISICAO = "anoAquisicao";
    private Long anoAquisicao;
    public static final String MESAQUISICAO_LINK = "aAnexoG1.qQuadro04.fmesAquisicao";
    public static final String MESAQUISICAO = "mesAquisicao";
    private Long mesAquisicao;
    public static final String VALORAQUISICAO_LINK = "aAnexoG1.qQuadro04.fvalorAquisicao";
    public static final String VALORAQUISICAO = "valorAquisicao";
    private Long valorAquisicao;

    public AnexoG1q04T1_LinhaBase(Long mesRealizacao, Long valorRealizacao, Long anoAquisicao, Long mesAquisicao, Long valorAquisicao) {
        this.mesRealizacao = mesRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.anoAquisicao = anoAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.valorAquisicao = valorAquisicao;
    }

    public AnexoG1q04T1_LinhaBase() {
        this.mesRealizacao = null;
        this.valorRealizacao = null;
        this.anoAquisicao = null;
        this.mesAquisicao = null;
        this.valorAquisicao = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesRealizacao() {
        return this.mesRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesRealizacao(Long mesRealizacao) {
        Long oldValue = this.mesRealizacao;
        this.mesRealizacao = mesRealizacao;
        this.firePropertyChange("mesRealizacao", oldValue, this.mesRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorRealizacao() {
        return this.valorRealizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorRealizacao(Long valorRealizacao) {
        Long oldValue = this.valorRealizacao;
        this.valorRealizacao = valorRealizacao;
        this.firePropertyChange("valorRealizacao", oldValue, this.valorRealizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnoAquisicao() {
        return this.anoAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnoAquisicao(Long anoAquisicao) {
        Long oldValue = this.anoAquisicao;
        this.anoAquisicao = anoAquisicao;
        this.firePropertyChange("anoAquisicao", oldValue, this.anoAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMesAquisicao() {
        return this.mesAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMesAquisicao(Long mesAquisicao) {
        Long oldValue = this.mesAquisicao;
        this.mesAquisicao = mesAquisicao;
        this.firePropertyChange("mesAquisicao", oldValue, this.mesAquisicao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValorAquisicao() {
        return this.valorAquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValorAquisicao(Long valorAquisicao) {
        Long oldValue = this.valorAquisicao;
        this.valorAquisicao = valorAquisicao;
        this.firePropertyChange("valorAquisicao", oldValue, this.valorAquisicao);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.mesRealizacao);
        result = HashCodeUtil.hash(result, this.valorRealizacao);
        result = HashCodeUtil.hash(result, this.anoAquisicao);
        result = HashCodeUtil.hash(result, this.mesAquisicao);
        result = HashCodeUtil.hash(result, this.valorAquisicao);
        return result;
    }

    public static enum Property {
        MESREALIZACAO("mesRealizacao", 2),
        VALORREALIZACAO("valorRealizacao", 3),
        ANOAQUISICAO("anoAquisicao", 4),
        MESAQUISICAO("mesAquisicao", 5),
        VALORAQUISICAO("valorAquisicao", 6);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

