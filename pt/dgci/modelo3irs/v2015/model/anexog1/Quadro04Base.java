/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoG1.qQuadro04";
    public static final String ANEXOG1Q04T1_LINK = "aAnexoG1.qQuadro04.tanexoG1q04T1";
    private EventList<AnexoG1q04T1_Linha> anexoG1q04T1 = new BasicEventList<AnexoG1q04T1_Linha>();
    public static final String ANEXOG1Q04C401_LINK = "aAnexoG1.qQuadro04.fanexoG1q04C401";
    public static final String ANEXOG1Q04C401 = "anexoG1q04C401";
    private Long anexoG1q04C401;
    public static final String ANEXOG1Q04C401A_LINK = "aAnexoG1.qQuadro04.fanexoG1q04C401a";
    public static final String ANEXOG1Q04C401A = "anexoG1q04C401a";
    private Long anexoG1q04C401a;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoG1q04T1_Linha> getAnexoG1q04T1() {
        return this.anexoG1q04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoG1q04T1(EventList<AnexoG1q04T1_Linha> anexoG1q04T1) {
        this.anexoG1q04T1 = anexoG1q04T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoG1q04C401() {
        return this.anexoG1q04C401;
    }

    public void setAnexoG1q04C401Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoG1q04C401")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoG1q04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoG1q04T1().get(i).getValorRealizacao() == null ? 0 : this.getAnexoG1q04T1().get(i).getValorRealizacao();
        }
        this.setAnexoG1q04C401(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoG1q04C401(Long anexoG1q04C401) {
        Long oldValue = this.anexoG1q04C401;
        this.anexoG1q04C401 = anexoG1q04C401;
        this.firePropertyChange("anexoG1q04C401", oldValue, this.anexoG1q04C401);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoG1q04C401a() {
        return this.anexoG1q04C401a;
    }

    public void setAnexoG1q04C401aFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoG1q04C401a")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoG1q04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoG1q04T1().get(i).getValorAquisicao() == null ? 0 : this.getAnexoG1q04T1().get(i).getValorAquisicao();
        }
        this.setAnexoG1q04C401a(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoG1q04C401a(Long anexoG1q04C401a) {
        Long oldValue = this.anexoG1q04C401a;
        this.anexoG1q04C401a = anexoG1q04C401a;
        this.firePropertyChange("anexoG1q04C401a", oldValue, this.anexoG1q04C401a);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoG1q04T1);
        result = HashCodeUtil.hash(result, this.anexoG1q04C401);
        result = HashCodeUtil.hash(result, this.anexoG1q04C401a);
        return result;
    }
}

