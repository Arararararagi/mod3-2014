/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoG1q05T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoG1q05T1_Linha>,
WritableTableFormat<AnexoG1q05T1_Linha> {
    @Override
    public boolean isEditable(AnexoG1q05T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoG1q05T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getFreguesia();
            }
            case 2: {
                if (line == null || line.getTipoPredio() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_TipoPredio", line.getTipoPredio());
            }
            case 3: {
                return line.getArtigo();
            }
            case 4: {
                return line.getFraccao();
            }
            case 5: {
                if (line == null || line.getCodigo() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoG1Q5", line.getCodigo());
            }
            case 6: {
                return line.getAno();
            }
            case 7: {
                return line.getMes();
            }
            case 8: {
                return line.getDia();
            }
            case 9: {
                return line.getRealizacao();
            }
            case 10: {
                return line.getAquisicao();
            }
        }
        return null;
    }

    @Override
    public AnexoG1q05T1_Linha setColumnValue(AnexoG1q05T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setFreguesia((String)value);
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setTipoPredio((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setArtigo((Long)value);
                return line;
            }
            case 4: {
                line.setFraccao((String)value);
                return line;
            }
            case 5: {
                if (value != null) {
                    line.setCodigo((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 6: {
                line.setAno((Long)value);
                return line;
            }
            case 7: {
                line.setMes((Long)value);
                return line;
            }
            case 8: {
                line.setDia((Long)value);
                return line;
            }
            case 9: {
                line.setRealizacao((Long)value);
                return line;
            }
            case 10: {
                line.setAquisicao((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return String.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return Long.class;
            }
            case 10: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 11;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Freguesia";
            }
            case 2: {
                return "Tipo";
            }
            case 3: {
                return "Artigo";
            }
            case 4: {
                return "Fra\u00e7\u00e3o";
            }
            case 5: {
                return "C\u00f3digo";
            }
            case 6: {
                return "Ano";
            }
            case 7: {
                return "M\u00eas";
            }
            case 8: {
                return "Dia";
            }
            case 9: {
                return "Realiza\u00e7\u00e3o";
            }
            case 10: {
                return "Aquisi\u00e7\u00e3o";
            }
        }
        return null;
    }
}

