/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro05
extends Quadro05Base {
    public boolean isEmpty() {
        return this.getAnexoG1q05T1().isEmpty() && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoG1q05C1()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoG1q05C2());
    }
}

