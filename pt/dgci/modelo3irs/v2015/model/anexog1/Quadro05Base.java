/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoG1.qQuadro05";
    public static final String ANEXOG1Q05T1_LINK = "aAnexoG1.qQuadro05.tanexoG1q05T1";
    private EventList<AnexoG1q05T1_Linha> anexoG1q05T1 = new BasicEventList<AnexoG1q05T1_Linha>();
    public static final String ANEXOG1Q05C1_LINK = "aAnexoG1.qQuadro05.fanexoG1q05C1";
    public static final String ANEXOG1Q05C1 = "anexoG1q05C1";
    private Long anexoG1q05C1;
    public static final String ANEXOG1Q05C2_LINK = "aAnexoG1.qQuadro05.fanexoG1q05C2";
    public static final String ANEXOG1Q05C2 = "anexoG1q05C2";
    private Long anexoG1q05C2;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoG1q05T1_Linha> getAnexoG1q05T1() {
        return this.anexoG1q05T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoG1q05T1(EventList<AnexoG1q05T1_Linha> anexoG1q05T1) {
        this.anexoG1q05T1 = anexoG1q05T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoG1q05C1() {
        return this.anexoG1q05C1;
    }

    public void setAnexoG1q05C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoG1q05C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoG1q05T1().size(); ++i) {
            temporaryValue0+=this.getAnexoG1q05T1().get(i).getRealizacao() == null ? 0 : this.getAnexoG1q05T1().get(i).getRealizacao();
        }
        this.setAnexoG1q05C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoG1q05C1(Long anexoG1q05C1) {
        Long oldValue = this.anexoG1q05C1;
        this.anexoG1q05C1 = anexoG1q05C1;
        this.firePropertyChange("anexoG1q05C1", oldValue, this.anexoG1q05C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoG1q05C2() {
        return this.anexoG1q05C2;
    }

    public void setAnexoG1q05C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoG1q05C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoG1q05T1().size(); ++i) {
            temporaryValue0+=this.getAnexoG1q05T1().get(i).getAquisicao() == null ? 0 : this.getAnexoG1q05T1().get(i).getAquisicao();
        }
        this.setAnexoG1q05C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoG1q05C2(Long anexoG1q05C2) {
        Long oldValue = this.anexoG1q05C2;
        this.anexoG1q05C2 = anexoG1q05C2;
        this.firePropertyChange("anexoG1q05C2", oldValue, this.anexoG1q05C2);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoG1q05T1);
        result = HashCodeUtil.hash(result, this.anexoG1q05C1);
        result = HashCodeUtil.hash(result, this.anexoG1q05C2);
        return result;
    }
}

