/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoG1.qQuadro02";
    public static final String ANEXOG1Q02C01_LINK = "aAnexoG1.qQuadro02.fanexoG1q02C01";
    public static final String ANEXOG1Q02C01 = "anexoG1q02C01";
    private Long anexoG1q02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoG1q02C01() {
        return this.anexoG1q02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoG1q02C01(Long anexoG1q02C01) {
        Long oldValue = this.anexoG1q02C01;
        this.anexoG1q02C01 = anexoG1q02C01;
        this.firePropertyChange("anexoG1q02C01", oldValue, this.anexoG1q02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoG1q02C01);
        return result;
    }
}

