/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1ModelBase;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro05;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=1)
public class AnexoG1Model
extends AnexoG1ModelBase {
    private static final long serialVersionUID = -8037680091777713314L;

    public AnexoG1Model(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public boolean isEmpty() {
        return this.getQuadro04().isEmpty() && this.getQuadro05().isEmpty();
    }
}

