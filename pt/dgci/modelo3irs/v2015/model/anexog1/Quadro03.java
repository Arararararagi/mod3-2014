/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro03Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro03
extends Quadro03Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoG1q03C02()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoG1q03C03());
    }
}

