/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog1.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro04
extends Quadro04Base {
    public boolean isEmpty() {
        return this.getAnexoG1q04T1().isEmpty() && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoG1q04C401()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoG1q04C401a());
    }
}

