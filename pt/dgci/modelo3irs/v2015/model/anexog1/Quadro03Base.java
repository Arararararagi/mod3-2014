/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoG1.qQuadro03";
    public static final String ANEXOG1Q03C02_LINK = "aAnexoG1.qQuadro03.fanexoG1q03C02";
    public static final String ANEXOG1Q03C02 = "anexoG1q03C02";
    private Long anexoG1q03C02;
    public static final String ANEXOG1Q03C03_LINK = "aAnexoG1.qQuadro03.fanexoG1q03C03";
    public static final String ANEXOG1Q03C03 = "anexoG1q03C03";
    private Long anexoG1q03C03;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoG1q03C02() {
        return this.anexoG1q03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoG1q03C02(Long anexoG1q03C02) {
        Long oldValue = this.anexoG1q03C02;
        this.anexoG1q03C02 = anexoG1q03C02;
        this.firePropertyChange("anexoG1q03C02", oldValue, this.anexoG1q03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoG1q03C03() {
        return this.anexoG1q03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoG1q03C03(Long anexoG1q03C03) {
        Long oldValue = this.anexoG1q03C03;
        this.anexoG1q03C03 = anexoG1q03C03;
        this.firePropertyChange("anexoG1q03C03", oldValue, this.anexoG1q03C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoG1q03C02);
        result = HashCodeUtil.hash(result, this.anexoG1q03C03);
        return result;
    }
}

