/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoG1q05T1_LinhaBase
extends Model {
    public static final String FREGUESIA_LINK = "aAnexoG1.qQuadro05.ffreguesia";
    public static final String FREGUESIA = "freguesia";
    private String freguesia;
    public static final String TIPOPREDIO_LINK = "aAnexoG1.qQuadro05.ftipoPredio";
    public static final String TIPOPREDIO = "tipoPredio";
    private String tipoPredio;
    public static final String ARTIGO_LINK = "aAnexoG1.qQuadro05.fartigo";
    public static final String ARTIGO = "artigo";
    private Long artigo;
    public static final String FRACCAO_LINK = "aAnexoG1.qQuadro05.ffraccao";
    public static final String FRACCAO = "fraccao";
    private String fraccao;
    public static final String CODIGO_LINK = "aAnexoG1.qQuadro05.fcodigo";
    public static final String CODIGO = "codigo";
    private Long codigo;
    public static final String ANO_LINK = "aAnexoG1.qQuadro05.fano";
    public static final String ANO = "ano";
    private Long ano;
    public static final String MES_LINK = "aAnexoG1.qQuadro05.fmes";
    public static final String MES = "mes";
    private Long mes;
    public static final String DIA_LINK = "aAnexoG1.qQuadro05.fdia";
    public static final String DIA = "dia";
    private Long dia;
    public static final String REALIZACAO_LINK = "aAnexoG1.qQuadro05.frealizacao";
    public static final String REALIZACAO = "realizacao";
    private Long realizacao;
    public static final String AQUISICAO_LINK = "aAnexoG1.qQuadro05.faquisicao";
    public static final String AQUISICAO = "aquisicao";
    private Long aquisicao;

    public AnexoG1q05T1_LinhaBase(String freguesia, String tipoPredio, Long artigo, String fraccao, Long codigo, Long ano, Long mes, Long dia, Long realizacao, Long aquisicao) {
        this.freguesia = freguesia;
        this.tipoPredio = tipoPredio;
        this.artigo = artigo;
        this.fraccao = fraccao;
        this.codigo = codigo;
        this.ano = ano;
        this.mes = mes;
        this.dia = dia;
        this.realizacao = realizacao;
        this.aquisicao = aquisicao;
    }

    public AnexoG1q05T1_LinhaBase() {
        this.freguesia = null;
        this.tipoPredio = null;
        this.artigo = null;
        this.fraccao = null;
        this.codigo = null;
        this.ano = null;
        this.mes = null;
        this.dia = null;
        this.realizacao = null;
        this.aquisicao = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFreguesia() {
        return this.freguesia;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFreguesia(String freguesia) {
        String oldValue = this.freguesia;
        this.freguesia = freguesia;
        this.firePropertyChange("freguesia", oldValue, this.freguesia);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio")
    public String getTipoPredio() {
        return this.tipoPredio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTipoPredio(String tipoPredio) {
        String oldValue = this.tipoPredio;
        this.tipoPredio = tipoPredio;
        this.firePropertyChange("tipoPredio", oldValue, this.tipoPredio);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getArtigo() {
        return this.artigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setArtigo(Long artigo) {
        Long oldValue = this.artigo;
        this.artigo = artigo;
        this.firePropertyChange("artigo", oldValue, this.artigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFraccao() {
        return this.fraccao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFraccao(String fraccao) {
        String oldValue = this.fraccao;
        this.fraccao = fraccao;
        this.firePropertyChange("fraccao", oldValue, this.fraccao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoG1Q5")
    public Long getCodigo() {
        return this.codigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodigo(Long codigo) {
        Long oldValue = this.codigo;
        this.codigo = codigo;
        this.firePropertyChange("codigo", oldValue, this.codigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAno() {
        return this.ano;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAno(Long ano) {
        Long oldValue = this.ano;
        this.ano = ano;
        this.firePropertyChange("ano", oldValue, this.ano);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getMes() {
        return this.mes;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setMes(Long mes) {
        Long oldValue = this.mes;
        this.mes = mes;
        this.firePropertyChange("mes", oldValue, this.mes);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getDia() {
        return this.dia;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setDia(Long dia) {
        Long oldValue = this.dia;
        this.dia = dia;
        this.firePropertyChange("dia", oldValue, this.dia);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRealizacao() {
        return this.realizacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRealizacao(Long realizacao) {
        Long oldValue = this.realizacao;
        this.realizacao = realizacao;
        this.firePropertyChange("realizacao", oldValue, this.realizacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAquisicao() {
        return this.aquisicao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAquisicao(Long aquisicao) {
        Long oldValue = this.aquisicao;
        this.aquisicao = aquisicao;
        this.firePropertyChange("aquisicao", oldValue, this.aquisicao);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.freguesia);
        result = HashCodeUtil.hash(result, this.tipoPredio);
        result = HashCodeUtil.hash(result, this.artigo);
        result = HashCodeUtil.hash(result, this.fraccao);
        result = HashCodeUtil.hash(result, this.codigo);
        result = HashCodeUtil.hash(result, this.ano);
        result = HashCodeUtil.hash(result, this.mes);
        result = HashCodeUtil.hash(result, this.dia);
        result = HashCodeUtil.hash(result, this.realizacao);
        result = HashCodeUtil.hash(result, this.aquisicao);
        return result;
    }

    public static enum Property {
        FREGUESIA("freguesia", 2),
        TIPOPREDIO("tipoPredio", 3),
        ARTIGO("artigo", 4),
        FRACCAO("fraccao", 5),
        CODIGO("codigo", 6),
        ANO("ano", 7),
        MES("mes", 8),
        DIA("dia", 9),
        REALIZACAO("realizacao", 10),
        AQUISICAO("aquisicao", 11);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

