/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q04T1_Linha;

public class AnexoG1q04T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoG1q04T1_Linha>,
WritableTableFormat<AnexoG1q04T1_Linha> {
    @Override
    public boolean isEditable(AnexoG1q04T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoG1q04T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getMesRealizacao();
            }
            case 2: {
                return line.getValorRealizacao();
            }
            case 3: {
                return line.getAnoAquisicao();
            }
            case 4: {
                return line.getMesAquisicao();
            }
            case 5: {
                return line.getValorAquisicao();
            }
        }
        return null;
    }

    @Override
    public AnexoG1q04T1_Linha setColumnValue(AnexoG1q04T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setMesRealizacao((Long)value);
                return line;
            }
            case 2: {
                line.setValorRealizacao((Long)value);
                return line;
            }
            case 3: {
                line.setAnoAquisicao((Long)value);
                return line;
            }
            case 4: {
                line.setMesAquisicao((Long)value);
                return line;
            }
            case 5: {
                line.setValorAquisicao((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "M\u00eas";
            }
            case 2: {
                return "Valor";
            }
            case 3: {
                return "Ano";
            }
            case 4: {
                return "M\u00eas";
            }
            case 5: {
                return "Valor";
            }
        }
        return null;
    }
}

