/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexog1;

import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1q05T1_LinhaBase;

public class AnexoG1q05T1_Linha
extends AnexoG1q05T1_LinhaBase {
    public AnexoG1q05T1_Linha() {
    }

    public AnexoG1q05T1_Linha(String freguesia, String tipoPredio, Long artigo, String fraccao, Long codigo, Long ano, Long mes, Long dia, Long realizacao, Long aquisicao) {
        super(freguesia, tipoPredio, artigo, fraccao, codigo, ano, mes, dia, realizacao, aquisicao);
    }
}

