/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro09Base
extends QuadroModel {
    public static final String QUADRO09_LINK = "aAnexoC.qQuadro09";
    public static final String ANEXOCQ09C901_LINK = "aAnexoC.qQuadro09.fanexoCq09C901";
    public static final String ANEXOCQ09C901 = "anexoCq09C901";
    private Long anexoCq09C901;
    public static final String ANEXOCQ09C902_LINK = "aAnexoC.qQuadro09.fanexoCq09C902";
    public static final String ANEXOCQ09C902 = "anexoCq09C902";
    private Long anexoCq09C902;
    public static final String ANEXOCQ09C903_LINK = "aAnexoC.qQuadro09.fanexoCq09C903";
    public static final String ANEXOCQ09C903 = "anexoCq09C903";
    private Long anexoCq09C903;
    public static final String ANEXOCQ09C904_LINK = "aAnexoC.qQuadro09.fanexoCq09C904";
    public static final String ANEXOCQ09C904 = "anexoCq09C904";
    private Long anexoCq09C904;
    public static final String ANEXOCQ09C905_LINK = "aAnexoC.qQuadro09.fanexoCq09C905";
    public static final String ANEXOCQ09C905 = "anexoCq09C905";
    private Long anexoCq09C905;
    public static final String ANEXOCQ09C906_LINK = "aAnexoC.qQuadro09.fanexoCq09C906";
    public static final String ANEXOCQ09C906 = "anexoCq09C906";
    private Long anexoCq09C906;
    public static final String ANEXOCQ09C907_LINK = "aAnexoC.qQuadro09.fanexoCq09C907";
    public static final String ANEXOCQ09C907 = "anexoCq09C907";
    private Long anexoCq09C907;
    public static final String ANEXOCQ09C908_LINK = "aAnexoC.qQuadro09.fanexoCq09C908";
    public static final String ANEXOCQ09C908 = "anexoCq09C908";
    private Long anexoCq09C908;
    public static final String ANEXOCQ09C909_LINK = "aAnexoC.qQuadro09.fanexoCq09C909";
    public static final String ANEXOCQ09C909 = "anexoCq09C909";
    private Long anexoCq09C909;
    public static final String ANEXOCQ09C910_LINK = "aAnexoC.qQuadro09.fanexoCq09C910";
    public static final String ANEXOCQ09C910 = "anexoCq09C910";
    private Long anexoCq09C910;
    public static final String ANEXOCQ09C911_LINK = "aAnexoC.qQuadro09.fanexoCq09C911";
    public static final String ANEXOCQ09C911 = "anexoCq09C911";
    private Long anexoCq09C911;
    public static final String ANEXOCQ09C912_LINK = "aAnexoC.qQuadro09.fanexoCq09C912";
    public static final String ANEXOCQ09C912 = "anexoCq09C912";
    private Long anexoCq09C912;
    public static final String ANEXOCQ09C913_LINK = "aAnexoC.qQuadro09.fanexoCq09C913";
    public static final String ANEXOCQ09C913 = "anexoCq09C913";
    private Long anexoCq09C913;
    public static final String ANEXOCQ09C914_LINK = "aAnexoC.qQuadro09.fanexoCq09C914";
    public static final String ANEXOCQ09C914 = "anexoCq09C914";
    private Long anexoCq09C914;
    public static final String ANEXOCQ09C915_LINK = "aAnexoC.qQuadro09.fanexoCq09C915";
    public static final String ANEXOCQ09C915 = "anexoCq09C915";
    private Long anexoCq09C915;
    public static final String ANEXOCQ09C916_LINK = "aAnexoC.qQuadro09.fanexoCq09C916";
    public static final String ANEXOCQ09C916 = "anexoCq09C916";
    private Long anexoCq09C916;
    public static final String ANEXOCQ09C917_LINK = "aAnexoC.qQuadro09.fanexoCq09C917";
    public static final String ANEXOCQ09C917 = "anexoCq09C917";
    private Long anexoCq09C917;
    public static final String ANEXOCQ09C918_LINK = "aAnexoC.qQuadro09.fanexoCq09C918";
    public static final String ANEXOCQ09C918 = "anexoCq09C918";
    private Long anexoCq09C918;
    public static final String ANEXOCQ09C919_LINK = "aAnexoC.qQuadro09.fanexoCq09C919";
    public static final String ANEXOCQ09C919 = "anexoCq09C919";
    private Long anexoCq09C919;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq09C901() {
        return this.anexoCq09C901;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq09C901(Long anexoCq09C901) {
        Long oldValue = this.anexoCq09C901;
        this.anexoCq09C901 = anexoCq09C901;
        this.firePropertyChange("anexoCq09C901", oldValue, this.anexoCq09C901);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq09C902() {
        return this.anexoCq09C902;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq09C902(Long anexoCq09C902) {
        Long oldValue = this.anexoCq09C902;
        this.anexoCq09C902 = anexoCq09C902;
        this.firePropertyChange("anexoCq09C902", oldValue, this.anexoCq09C902);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq09C903() {
        return this.anexoCq09C903;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq09C903(Long anexoCq09C903) {
        Long oldValue = this.anexoCq09C903;
        this.anexoCq09C903 = anexoCq09C903;
        this.firePropertyChange("anexoCq09C903", oldValue, this.anexoCq09C903);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq09C904() {
        return this.anexoCq09C904;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq09C904(Long anexoCq09C904) {
        Long oldValue = this.anexoCq09C904;
        this.anexoCq09C904 = anexoCq09C904;
        this.firePropertyChange("anexoCq09C904", oldValue, this.anexoCq09C904);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq09C905() {
        return this.anexoCq09C905;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq09C905(Long anexoCq09C905) {
        Long oldValue = this.anexoCq09C905;
        this.anexoCq09C905 = anexoCq09C905;
        this.firePropertyChange("anexoCq09C905", oldValue, this.anexoCq09C905);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq09C906() {
        return this.anexoCq09C906;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq09C906(Long anexoCq09C906) {
        Long oldValue = this.anexoCq09C906;
        this.anexoCq09C906 = anexoCq09C906;
        this.firePropertyChange("anexoCq09C906", oldValue, this.anexoCq09C906);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq09C907() {
        return this.anexoCq09C907;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq09C907(Long anexoCq09C907) {
        Long oldValue = this.anexoCq09C907;
        this.anexoCq09C907 = anexoCq09C907;
        this.firePropertyChange("anexoCq09C907", oldValue, this.anexoCq09C907);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C908() {
        return this.anexoCq09C908;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C908(Long anexoCq09C908) {
        Long oldValue = this.anexoCq09C908;
        this.anexoCq09C908 = anexoCq09C908;
        this.firePropertyChange("anexoCq09C908", oldValue, this.anexoCq09C908);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C909() {
        return this.anexoCq09C909;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C909(Long anexoCq09C909) {
        Long oldValue = this.anexoCq09C909;
        this.anexoCq09C909 = anexoCq09C909;
        this.firePropertyChange("anexoCq09C909", oldValue, this.anexoCq09C909);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C910() {
        return this.anexoCq09C910;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C910(Long anexoCq09C910) {
        Long oldValue = this.anexoCq09C910;
        this.anexoCq09C910 = anexoCq09C910;
        this.firePropertyChange("anexoCq09C910", oldValue, this.anexoCq09C910);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C911() {
        return this.anexoCq09C911;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C911(Long anexoCq09C911) {
        Long oldValue = this.anexoCq09C911;
        this.anexoCq09C911 = anexoCq09C911;
        this.firePropertyChange("anexoCq09C911", oldValue, this.anexoCq09C911);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C912() {
        return this.anexoCq09C912;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C912(Long anexoCq09C912) {
        Long oldValue = this.anexoCq09C912;
        this.anexoCq09C912 = anexoCq09C912;
        this.firePropertyChange("anexoCq09C912", oldValue, this.anexoCq09C912);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C913() {
        return this.anexoCq09C913;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C913(Long anexoCq09C913) {
        Long oldValue = this.anexoCq09C913;
        this.anexoCq09C913 = anexoCq09C913;
        this.firePropertyChange("anexoCq09C913", oldValue, this.anexoCq09C913);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C914() {
        return this.anexoCq09C914;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C914(Long anexoCq09C914) {
        Long oldValue = this.anexoCq09C914;
        this.anexoCq09C914 = anexoCq09C914;
        this.firePropertyChange("anexoCq09C914", oldValue, this.anexoCq09C914);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C915() {
        return this.anexoCq09C915;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C915(Long anexoCq09C915) {
        Long oldValue = this.anexoCq09C915;
        this.anexoCq09C915 = anexoCq09C915;
        this.firePropertyChange("anexoCq09C915", oldValue, this.anexoCq09C915);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C916() {
        return this.anexoCq09C916;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C916(Long anexoCq09C916) {
        Long oldValue = this.anexoCq09C916;
        this.anexoCq09C916 = anexoCq09C916;
        this.firePropertyChange("anexoCq09C916", oldValue, this.anexoCq09C916);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C917() {
        return this.anexoCq09C917;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C917(Long anexoCq09C917) {
        Long oldValue = this.anexoCq09C917;
        this.anexoCq09C917 = anexoCq09C917;
        this.firePropertyChange("anexoCq09C917", oldValue, this.anexoCq09C917);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C918() {
        return this.anexoCq09C918;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C918(Long anexoCq09C918) {
        Long oldValue = this.anexoCq09C918;
        this.anexoCq09C918 = anexoCq09C918;
        this.firePropertyChange("anexoCq09C918", oldValue, this.anexoCq09C918);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq09C919() {
        return this.anexoCq09C919;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq09C919(Long anexoCq09C919) {
        Long oldValue = this.anexoCq09C919;
        this.anexoCq09C919 = anexoCq09C919;
        this.firePropertyChange("anexoCq09C919", oldValue, this.anexoCq09C919);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq09C901);
        result = HashCodeUtil.hash(result, this.anexoCq09C902);
        result = HashCodeUtil.hash(result, this.anexoCq09C903);
        result = HashCodeUtil.hash(result, this.anexoCq09C904);
        result = HashCodeUtil.hash(result, this.anexoCq09C905);
        result = HashCodeUtil.hash(result, this.anexoCq09C906);
        result = HashCodeUtil.hash(result, this.anexoCq09C907);
        result = HashCodeUtil.hash(result, this.anexoCq09C908);
        result = HashCodeUtil.hash(result, this.anexoCq09C909);
        result = HashCodeUtil.hash(result, this.anexoCq09C910);
        result = HashCodeUtil.hash(result, this.anexoCq09C911);
        result = HashCodeUtil.hash(result, this.anexoCq09C912);
        result = HashCodeUtil.hash(result, this.anexoCq09C913);
        result = HashCodeUtil.hash(result, this.anexoCq09C914);
        result = HashCodeUtil.hash(result, this.anexoCq09C915);
        result = HashCodeUtil.hash(result, this.anexoCq09C916);
        result = HashCodeUtil.hash(result, this.anexoCq09C917);
        result = HashCodeUtil.hash(result, this.anexoCq09C918);
        result = HashCodeUtil.hash(result, this.anexoCq09C919);
        return result;
    }
}

