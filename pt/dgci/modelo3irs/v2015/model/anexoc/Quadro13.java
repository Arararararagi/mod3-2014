/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13Base;

public class Quadro13
extends Quadro13Base {
    public boolean isEmpty() {
        return this.getAnexoCq13C1301() == null && this.getAnexoCq13C1307() == null;
    }

    public boolean isEmptyOrZero() {
        return (this.getAnexoCq13C1301() == null || this.getAnexoCq13C1301() == 0) && (this.getAnexoCq13C1307() == null || this.getAnexoCq13C1307() == 0);
    }
}

