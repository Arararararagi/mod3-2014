/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro06Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro06
extends Quadro06Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C601()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C602()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C603()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C604()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C605()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C606()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C607()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C608()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C609()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C610()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C611()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C612()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C613()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C614()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C615()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq06C616());
    }

    public Long getSumC1() {
        long soma = 0;
        if (this.getAnexoCq06C601() != null) {
            soma+=this.getAnexoCq06C601().longValue();
        }
        if (this.getAnexoCq06C602() != null) {
            soma+=this.getAnexoCq06C602().longValue();
        }
        if (this.getAnexoCq06C603() != null) {
            soma+=this.getAnexoCq06C603().longValue();
        }
        if (this.getAnexoCq06C604() != null) {
            soma+=this.getAnexoCq06C604().longValue();
        }
        return soma;
    }

    public Long getSumC2() {
        long soma = 0;
        if (this.getAnexoCq06C606() != null) {
            soma+=this.getAnexoCq06C606().longValue();
        }
        if (this.getAnexoCq06C607() != null) {
            soma+=this.getAnexoCq06C607().longValue();
        }
        if (this.getAnexoCq06C608() != null) {
            soma+=this.getAnexoCq06C608().longValue();
        }
        if (this.getAnexoCq06C609() != null) {
            soma+=this.getAnexoCq06C609().longValue();
        }
        return soma;
    }

    public Long getSumC3() {
        long soma = 0;
        if (this.getAnexoCq06C611() != null) {
            soma+=this.getAnexoCq06C611().longValue();
        }
        if (this.getAnexoCq06C612() != null) {
            soma+=this.getAnexoCq06C612().longValue();
        }
        if (this.getAnexoCq06C613() != null) {
            soma+=this.getAnexoCq06C613().longValue();
        }
        if (this.getAnexoCq06C614() != null) {
            soma+=this.getAnexoCq06C614().longValue();
        }
        return soma;
    }
}

