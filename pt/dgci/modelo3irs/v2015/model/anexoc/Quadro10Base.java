/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro10Base
extends QuadroModel {
    public static final String QUADRO10_LINK = "aAnexoC.qQuadro10";
    public static final String ANEXOCQ10C1001_LINK = "aAnexoC.qQuadro10.fanexoCq10C1001";
    public static final String ANEXOCQ10C1001 = "anexoCq10C1001";
    private Long anexoCq10C1001;
    public static final String ANEXOCQ10C1002_LINK = "aAnexoC.qQuadro10.fanexoCq10C1002";
    public static final String ANEXOCQ10C1002 = "anexoCq10C1002";
    private Long anexoCq10C1002;
    public static final String ANEXOCQ10C1006_LINK = "aAnexoC.qQuadro10.fanexoCq10C1006";
    public static final String ANEXOCQ10C1006 = "anexoCq10C1006";
    private Long anexoCq10C1006;
    public static final String ANEXOCQ10C1003_LINK = "aAnexoC.qQuadro10.fanexoCq10C1003";
    public static final String ANEXOCQ10C1003 = "anexoCq10C1003";
    private Long anexoCq10C1003;
    public static final String ANEXOCQ10C1007_LINK = "aAnexoC.qQuadro10.fanexoCq10C1007";
    public static final String ANEXOCQ10C1007 = "anexoCq10C1007";
    private Long anexoCq10C1007;
    public static final String ANEXOCQ10C1004_LINK = "aAnexoC.qQuadro10.fanexoCq10C1004";
    public static final String ANEXOCQ10C1004 = "anexoCq10C1004";
    private Long anexoCq10C1004;
    public static final String ANEXOCQ10C1005_LINK = "aAnexoC.qQuadro10.fanexoCq10C1005";
    public static final String ANEXOCQ10C1005 = "anexoCq10C1005";
    private Long anexoCq10C1005;
    public static final String ANEXOCQ10C1_LINK = "aAnexoC.qQuadro10.fanexoCq10C1";
    public static final String ANEXOCQ10C1 = "anexoCq10C1";
    private Long anexoCq10C1;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1001() {
        return this.anexoCq10C1001;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1001(Long anexoCq10C1001) {
        Long oldValue = this.anexoCq10C1001;
        this.anexoCq10C1001 = anexoCq10C1001;
        this.firePropertyChange("anexoCq10C1001", oldValue, this.anexoCq10C1001);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1002() {
        return this.anexoCq10C1002;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1002(Long anexoCq10C1002) {
        Long oldValue = this.anexoCq10C1002;
        this.anexoCq10C1002 = anexoCq10C1002;
        this.firePropertyChange("anexoCq10C1002", oldValue, this.anexoCq10C1002);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1006() {
        return this.anexoCq10C1006;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1006(Long anexoCq10C1006) {
        Long oldValue = this.anexoCq10C1006;
        this.anexoCq10C1006 = anexoCq10C1006;
        this.firePropertyChange("anexoCq10C1006", oldValue, this.anexoCq10C1006);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1003() {
        return this.anexoCq10C1003;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1003(Long anexoCq10C1003) {
        Long oldValue = this.anexoCq10C1003;
        this.anexoCq10C1003 = anexoCq10C1003;
        this.firePropertyChange("anexoCq10C1003", oldValue, this.anexoCq10C1003);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1007() {
        return this.anexoCq10C1007;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1007(Long anexoCq10C1007) {
        Long oldValue = this.anexoCq10C1007;
        this.anexoCq10C1007 = anexoCq10C1007;
        this.firePropertyChange("anexoCq10C1007", oldValue, this.anexoCq10C1007);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1004() {
        return this.anexoCq10C1004;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1004(Long anexoCq10C1004) {
        Long oldValue = this.anexoCq10C1004;
        this.anexoCq10C1004 = anexoCq10C1004;
        this.firePropertyChange("anexoCq10C1004", oldValue, this.anexoCq10C1004);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1005() {
        return this.anexoCq10C1005;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1005(Long anexoCq10C1005) {
        Long oldValue = this.anexoCq10C1005;
        this.anexoCq10C1005 = anexoCq10C1005;
        this.firePropertyChange("anexoCq10C1005", oldValue, this.anexoCq10C1005);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq10C1() {
        return this.anexoCq10C1;
    }

    public void setAnexoCq10C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq10C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq10C1(newValue+=(this.getAnexoCq10C1001() == null ? 0 : this.getAnexoCq10C1001()) + (this.getAnexoCq10C1002() == null ? 0 : this.getAnexoCq10C1002()) + (this.getAnexoCq10C1003() == null ? 0 : this.getAnexoCq10C1003()) + (this.getAnexoCq10C1004() == null ? 0 : this.getAnexoCq10C1004()) + (this.getAnexoCq10C1005() == null ? 0 : this.getAnexoCq10C1005()) + (this.getAnexoCq10C1006() == null ? 0 : this.getAnexoCq10C1006()) + (this.getAnexoCq10C1007() == null ? 0 : this.getAnexoCq10C1007()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq10C1(Long anexoCq10C1) {
        Long oldValue = this.anexoCq10C1;
        this.anexoCq10C1 = anexoCq10C1;
        this.firePropertyChange("anexoCq10C1", oldValue, this.anexoCq10C1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq10C1001);
        result = HashCodeUtil.hash(result, this.anexoCq10C1002);
        result = HashCodeUtil.hash(result, this.anexoCq10C1006);
        result = HashCodeUtil.hash(result, this.anexoCq10C1003);
        result = HashCodeUtil.hash(result, this.anexoCq10C1007);
        result = HashCodeUtil.hash(result, this.anexoCq10C1004);
        result = HashCodeUtil.hash(result, this.anexoCq10C1005);
        result = HashCodeUtil.hash(result, this.anexoCq10C1);
        return result;
    }
}

