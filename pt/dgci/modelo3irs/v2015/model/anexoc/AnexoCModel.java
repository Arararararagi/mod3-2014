/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.OwnableAnexo;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModelBase;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.opensoft.field.NifValidator;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=9)
public class AnexoCModel
extends AnexoCModelBase
implements OwnableAnexo {
    public AnexoCModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
        this.postCreate();
    }

    protected final void postCreate() {
        Quadro03 quadro03Model = this.getQuadro03();
        if (quadro03Model != null) {
            if (NifValidator.isSingular(this.formKey.getSubId())) {
                quadro03Model.setAnexoCq03C06(Long.parseLong(this.formKey.getSubId()));
            } else {
                quadro03Model.setAnexoCq03C07(Long.parseLong(this.formKey.getSubId()));
            }
        }
    }

    public Long getAnexoCTitular() {
        return this.getQuadro03().getAnexoCq03C06() != null ? this.getQuadro03().getAnexoCq03C06() : this.getQuadro03().getAnexoCq03C07();
    }

    @Override
    public boolean isOwnedBy(Long nif) {
        return nif != null && (nif.equals(this.getQuadro03().getAnexoCq03C06()) || nif.equals(this.getQuadro03().getAnexoCq03C07()));
    }

    public String getLink(String linkWithoutSubId) {
        return linkWithoutSubId.replaceFirst("aAnexoC", "aAnexoC|" + this.getFormKey().getSubId());
    }

    public Quadro01 getQuadro01() {
        return (Quadro01)this.getQuadro(Quadro01.class.getSimpleName());
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public Quadro07 getQuadro07() {
        return (Quadro07)this.getQuadro(Quadro07.class.getSimpleName());
    }

    public Quadro08 getQuadro08() {
        return (Quadro08)this.getQuadro(Quadro08.class.getSimpleName());
    }

    public Quadro09 getQuadro09() {
        return (Quadro09)this.getQuadro(Quadro09.class.getSimpleName());
    }

    public Quadro10 getQuadro10() {
        return (Quadro10)this.getQuadro(Quadro10.class.getSimpleName());
    }

    public Quadro11 getQuadro11() {
        return (Quadro11)this.getQuadro(Quadro11.class.getSimpleName());
    }

    public Quadro12 getQuadro12() {
        return (Quadro12)this.getQuadro(Quadro12.class.getSimpleName());
    }

    public Quadro13 getQuadro13() {
        return (Quadro13)this.getQuadro(Quadro13.class.getSimpleName());
    }

    public Quadro14 getQuadro14() {
        return (Quadro14)this.getQuadro(Quadro14.class.getSimpleName());
    }

    public Quadro15 getQuadro15() {
        return (Quadro15)this.getQuadro(Quadro15.class.getSimpleName());
    }

    public Quadro16 getQuadro16() {
        return (Quadro16)this.getQuadro(Quadro16.class.getSimpleName());
    }

    public Quadro17 getQuadro17() {
        return (Quadro17)this.getQuadro(Quadro17.class.getSimpleName());
    }
}

