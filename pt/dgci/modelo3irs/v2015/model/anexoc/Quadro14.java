/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14Base;

public class Quadro14
extends Quadro14Base {
    public Long getContagemLinhasTabela() {
        return this.getAnexoCq14T1() != null ? new Long(this.getAnexoCq14T1().size()) : 0;
    }

    public Long getSomaValoresVendasTabela() {
        EventList<AnexoCq14T1_Linha> linhas = this.getAnexoCq14T1();
        long soma = 0;
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoCq14T1_Linha linhaSomar = linhas.get(i);
            if (linhaSomar.getValorVenda() == null) continue;
            soma+=linhaSomar.getValorVenda().longValue();
        }
        return soma;
    }

    public Long getSomaDiferencasC120() {
        EventList<AnexoCq14T1_Linha> linhas = this.getAnexoCq14T1();
        long soma = 0;
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoCq14T1_Linha linhaSomar = linhas.get(i);
            if (linhaSomar.getValorDefinitivo() == null || linhaSomar.getValorVenda() == null || linhaSomar.getValorDefinitivo() <= linhaSomar.getValorVenda()) continue;
            soma+=linhaSomar.getValorDefinitivo() - linhaSomar.getValorVenda();
        }
        return soma;
    }
}

