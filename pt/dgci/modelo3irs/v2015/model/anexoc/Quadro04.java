/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04Base;

public class Quadro04
extends Quadro04Base {
    public Long getSumC1() {
        long val401 = this.getAnexoCq04C401() == null ? 0 : this.getAnexoCq04C401();
        long val402 = this.getAnexoCq04C402() == null ? 0 : this.getAnexoCq04C402();
        long val403 = this.getAnexoCq04C403() == null ? 0 : this.getAnexoCq04C403();
        long val404 = this.getAnexoCq04C404() == null ? 0 : this.getAnexoCq04C404();
        long val405 = this.getAnexoCq04C405() == null ? 0 : this.getAnexoCq04C405();
        long val406 = this.getAnexoCq04C406() == null ? 0 : this.getAnexoCq04C406();
        long val407 = this.getAnexoCq04C407() == null ? 0 : this.getAnexoCq04C407();
        return val401 + val402 + val403 - val404 - val405 + val406 - val407;
    }

    public Long getSumC2() {
        long soma = 0;
        if (this.getAnexoCq04C408() != null) {
            soma+=this.getAnexoCq04C408().longValue();
        }
        if (this.getAnexoCq04C409() != null) {
            soma+=this.getAnexoCq04C409().longValue();
        }
        if (this.getAnexoCq04C410() != null) {
            soma+=this.getAnexoCq04C410().longValue();
        }
        if (this.getAnexoCq04C411() != null) {
            soma+=this.getAnexoCq04C411().longValue();
        }
        if (this.getAnexoCq04C412() != null) {
            soma+=this.getAnexoCq04C412().longValue();
        }
        if (this.getAnexoCq04C413() != null) {
            soma+=this.getAnexoCq04C413().longValue();
        }
        if (this.getAnexoCq04C414() != null) {
            soma+=this.getAnexoCq04C414().longValue();
        }
        if (this.getAnexoCq04C415() != null) {
            soma+=this.getAnexoCq04C415().longValue();
        }
        if (this.getAnexoCq04C416() != null) {
            soma+=this.getAnexoCq04C416().longValue();
        }
        if (this.getAnexoCq04C417() != null) {
            soma+=this.getAnexoCq04C417().longValue();
        }
        if (this.getAnexoCq04C418() != null) {
            soma+=this.getAnexoCq04C418().longValue();
        }
        if (this.getAnexoCq04C419() != null) {
            soma+=this.getAnexoCq04C419().longValue();
        }
        if (this.getAnexoCq04C420() != null) {
            soma+=this.getAnexoCq04C420().longValue();
        }
        if (this.getAnexoCq04C421() != null) {
            soma+=this.getAnexoCq04C421().longValue();
        }
        if (this.getAnexoCq04C422() != null) {
            soma+=this.getAnexoCq04C422().longValue();
        }
        if (this.getAnexoCq04C423() != null) {
            soma+=this.getAnexoCq04C423().longValue();
        }
        if (this.getAnexoCq04C424() != null) {
            soma+=this.getAnexoCq04C424().longValue();
        }
        if (this.getAnexoCq04C425() != null) {
            soma+=this.getAnexoCq04C425().longValue();
        }
        if (this.getAnexoCq04C426() != null) {
            soma+=this.getAnexoCq04C426().longValue();
        }
        if (this.getAnexoCq04C427() != null) {
            soma+=this.getAnexoCq04C427().longValue();
        }
        if (this.getAnexoCq04C428() != null) {
            soma+=this.getAnexoCq04C428().longValue();
        }
        if (this.getAnexoCq04C429() != null) {
            soma+=this.getAnexoCq04C429().longValue();
        }
        if (this.getAnexoCq04C430() != null) {
            soma+=this.getAnexoCq04C430().longValue();
        }
        if (this.getAnexoCq04C431() != null) {
            soma+=this.getAnexoCq04C431().longValue();
        }
        if (this.getAnexoCq04C432() != null) {
            soma+=this.getAnexoCq04C432().longValue();
        }
        if (this.getAnexoCq04C433() != null) {
            soma+=this.getAnexoCq04C433().longValue();
        }
        if (this.getAnexoCq04C434() != null) {
            soma+=this.getAnexoCq04C434().longValue();
        }
        if (this.getAnexoCq04C435() != null) {
            soma+=this.getAnexoCq04C435().longValue();
        }
        if (this.getAnexoCq04C436() != null) {
            soma+=this.getAnexoCq04C436().longValue();
        }
        if (this.getAnexoCq04C437() != null) {
            soma+=this.getAnexoCq04C437().longValue();
        }
        if (this.getAnexoCq04C438() != null) {
            soma+=this.getAnexoCq04C438().longValue();
        }
        if (this.getAnexoCq04C464() != null) {
            soma+=this.getAnexoCq04C464().longValue();
        }
        if (this.getAnexoCq04C465() != null) {
            soma+=this.getAnexoCq04C465().longValue();
        }
        if (this.getAnexoCq04C466() != null) {
            soma+=this.getAnexoCq04C466().longValue();
        }
        if (this.getAnexoCq04C467() != null) {
            soma+=this.getAnexoCq04C467().longValue();
        }
        if (this.getAnexoCq04C468() != null) {
            soma+=this.getAnexoCq04C468().longValue();
        }
        return soma;
    }

    public Long getSumC3() {
        long soma = 0;
        if (this.getAnexoCq04C440() != null) {
            soma+=this.getAnexoCq04C440().longValue();
        }
        if (this.getAnexoCq04C441() != null) {
            soma+=this.getAnexoCq04C441().longValue();
        }
        if (this.getAnexoCq04C442() != null) {
            soma+=this.getAnexoCq04C442().longValue();
        }
        if (this.getAnexoCq04C443() != null) {
            soma+=this.getAnexoCq04C443().longValue();
        }
        if (this.getAnexoCq04C444() != null) {
            soma+=this.getAnexoCq04C444().longValue();
        }
        if (this.getAnexoCq04C445() != null) {
            soma+=this.getAnexoCq04C445().longValue();
        }
        if (this.getAnexoCq04C446() != null) {
            soma+=this.getAnexoCq04C446().longValue();
        }
        if (this.getAnexoCq04C447() != null) {
            soma+=this.getAnexoCq04C447().longValue();
        }
        if (this.getAnexoCq04C448() != null) {
            soma+=this.getAnexoCq04C448().longValue();
        }
        if (this.getAnexoCq04C449() != null) {
            soma+=this.getAnexoCq04C449().longValue();
        }
        if (this.getAnexoCq04C450() != null) {
            soma+=this.getAnexoCq04C450().longValue();
        }
        if (this.getAnexoCq04C451() != null) {
            soma+=this.getAnexoCq04C451().longValue();
        }
        if (this.getAnexoCq04C452() != null) {
            soma+=this.getAnexoCq04C452().longValue();
        }
        if (this.getAnexoCq04C453() != null) {
            soma+=this.getAnexoCq04C453().longValue();
        }
        if (this.getAnexoCq04C454() != null) {
            soma+=this.getAnexoCq04C454().longValue();
        }
        if (this.getAnexoCq04C455() != null) {
            soma+=this.getAnexoCq04C455().longValue();
        }
        if (this.getAnexoCq04C456() != null) {
            soma+=this.getAnexoCq04C456().longValue();
        }
        if (this.getAnexoCq04C457() != null) {
            soma+=this.getAnexoCq04C457().longValue();
        }
        if (this.getAnexoCq04C462() != null) {
            soma+=this.getAnexoCq04C462().longValue();
        }
        if (this.getAnexoCq04C463() != null) {
            soma+=this.getAnexoCq04C463().longValue();
        }
        if (this.getAnexoCq04C469() != null) {
            soma+=this.getAnexoCq04C469().longValue();
        }
        if (this.getAnexoCq04C470() != null) {
            soma+=this.getAnexoCq04C470().longValue();
        }
        if (this.getAnexoCq04C471() != null) {
            soma+=this.getAnexoCq04C471().longValue();
        }
        if (this.getAnexoCq04C472() != null) {
            soma+=this.getAnexoCq04C472().longValue();
        }
        if (this.getAnexoCq04C473() != null) {
            soma+=this.getAnexoCq04C473().longValue();
        }
        return soma;
    }
}

