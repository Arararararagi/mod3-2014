/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro01Base
extends QuadroModel {
    public static final String QUADRO01_LINK = "aAnexoC.qQuadro01";
    public static final String ANEXOCQ01B1_LINK = "aAnexoC.qQuadro01.fanexoCq01B1";
    public static final String ANEXOCQ01B1 = "anexoCq01B1";
    private Boolean anexoCq01B1;
    public static final String ANEXOCQ01B2_LINK = "aAnexoC.qQuadro01.fanexoCq01B2";
    public static final String ANEXOCQ01B2 = "anexoCq01B2";
    private Boolean anexoCq01B2;

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoCq01B1() {
        return this.anexoCq01B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq01B1(Boolean anexoCq01B1) {
        Boolean oldValue = this.anexoCq01B1;
        this.anexoCq01B1 = anexoCq01B1;
        this.firePropertyChange("anexoCq01B1", oldValue, this.anexoCq01B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoCq01B2() {
        return this.anexoCq01B2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq01B2(Boolean anexoCq01B2) {
        Boolean oldValue = this.anexoCq01B2;
        this.anexoCq01B2 = anexoCq01B2;
        this.firePropertyChange("anexoCq01B2", oldValue, this.anexoCq01B2);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq01B1);
        result = HashCodeUtil.hash(result, this.anexoCq01B2);
        return result;
    }
}

