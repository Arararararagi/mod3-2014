/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro14Base
extends QuadroModel {
    public static final String QUADRO14_LINK = "aAnexoC.qQuadro14";
    public static final String ANEXOCQ14B1OP1_LINK = "aAnexoC.qQuadro14.fanexoCq14B1OP1";
    public static final String ANEXOCQ14B1OP1_VALUE = "1";
    public static final String ANEXOCQ14B1OP2_LINK = "aAnexoC.qQuadro14.fanexoCq14B1OP2";
    public static final String ANEXOCQ14B1OP2_VALUE = "2";
    public static final String ANEXOCQ14B1 = "anexoCq14B1";
    private String anexoCq14B1;
    public static final String ANEXOCQ14T1_LINK = "aAnexoC.qQuadro14.tanexoCq14T1";
    private EventList<AnexoCq14T1_Linha> anexoCq14T1 = new BasicEventList<AnexoCq14T1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoCq14B1() {
        return this.anexoCq14B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq14B1(String anexoCq14B1) {
        String oldValue = this.anexoCq14B1;
        this.anexoCq14B1 = anexoCq14B1;
        this.firePropertyChange("anexoCq14B1", oldValue, this.anexoCq14B1);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoCq14T1_Linha> getAnexoCq14T1() {
        return this.anexoCq14T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoCq14T1(EventList<AnexoCq14T1_Linha> anexoCq14T1) {
        this.anexoCq14T1 = anexoCq14T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq14B1);
        result = HashCodeUtil.hash(result, this.anexoCq14T1);
        return result;
    }
}

