/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_LinhaBase;

public class AnexoCq08T1_Linha
extends AnexoCq08T1_LinhaBase {
    public AnexoCq08T1_Linha() {
    }

    public AnexoCq08T1_Linha(Long nIF, Long valor) {
        super(nIF, valor);
    }

    public static String getLink(int numLinha) {
        return "aAnexoC.qQuadro08.tanexoCq08T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoCq08T1_LinhaBase.Property column) {
        return AnexoCq08T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

