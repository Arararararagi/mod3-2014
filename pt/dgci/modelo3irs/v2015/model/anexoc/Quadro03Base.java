/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoC.qQuadro03";
    public static final String ANEXOCQ03C04_LINK = "aAnexoC.qQuadro03.fanexoCq03C04";
    public static final String ANEXOCQ03C04 = "anexoCq03C04";
    private Long anexoCq03C04;
    public static final String ANEXOCQ03C05_LINK = "aAnexoC.qQuadro03.fanexoCq03C05";
    public static final String ANEXOCQ03C05 = "anexoCq03C05";
    private Long anexoCq03C05;
    public static final String ANEXOCQ03B1OP1_LINK = "aAnexoC.qQuadro03.fanexoCq03B1OP1";
    public static final String ANEXOCQ03B1OP1_VALUE = "1";
    public static final String ANEXOCQ03B1OP2_LINK = "aAnexoC.qQuadro03.fanexoCq03B1OP2";
    public static final String ANEXOCQ03B1OP2_VALUE = "2";
    public static final String ANEXOCQ03B1 = "anexoCq03B1";
    private String anexoCq03B1;
    public static final String ANEXOCQ03C06_LINK = "aAnexoC.qQuadro03.fanexoCq03C06";
    public static final String ANEXOCQ03C06 = "anexoCq03C06";
    private Long anexoCq03C06;
    public static final String ANEXOCQ03C07_LINK = "aAnexoC.qQuadro03.fanexoCq03C07";
    public static final String ANEXOCQ03C07 = "anexoCq03C07";
    private Long anexoCq03C07;
    public static final String ANEXOCQ03C08_LINK = "aAnexoC.qQuadro03.fanexoCq03C08";
    public static final String ANEXOCQ03C08 = "anexoCq03C08";
    private Long anexoCq03C08;
    public static final String ANEXOCQ03C09_LINK = "aAnexoC.qQuadro03.fanexoCq03C09";
    public static final String ANEXOCQ03C09 = "anexoCq03C09";
    private Long anexoCq03C09;
    public static final String ANEXOCQ03C10_LINK = "aAnexoC.qQuadro03.fanexoCq03C10";
    public static final String ANEXOCQ03C10 = "anexoCq03C10";
    private Long anexoCq03C10;
    public static final String ANEXOCQ03B2OP11_LINK = "aAnexoC.qQuadro03.fanexoCq03B2OP11";
    public static final String ANEXOCQ03B2OP11_VALUE = "11";
    public static final String ANEXOCQ03B2OP12_LINK = "aAnexoC.qQuadro03.fanexoCq03B2OP12";
    public static final String ANEXOCQ03B2OP12_VALUE = "12";
    public static final String ANEXOCQ03B2 = "anexoCq03B2";
    private String anexoCq03B2;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq03C04() {
        return this.anexoCq03C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03C04(Long anexoCq03C04) {
        Long oldValue = this.anexoCq03C04;
        this.anexoCq03C04 = anexoCq03C04;
        this.firePropertyChange("anexoCq03C04", oldValue, this.anexoCq03C04);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq03C05() {
        return this.anexoCq03C05;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03C05(Long anexoCq03C05) {
        Long oldValue = this.anexoCq03C05;
        this.anexoCq03C05 = anexoCq03C05;
        this.firePropertyChange("anexoCq03C05", oldValue, this.anexoCq03C05);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoCq03B1() {
        return this.anexoCq03B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03B1(String anexoCq03B1) {
        String oldValue = this.anexoCq03B1;
        this.anexoCq03B1 = anexoCq03B1;
        this.firePropertyChange("anexoCq03B1", oldValue, this.anexoCq03B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq03C06() {
        return this.anexoCq03C06;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03C06(Long anexoCq03C06) {
        Long oldValue = this.anexoCq03C06;
        this.anexoCq03C06 = anexoCq03C06;
        this.firePropertyChange("anexoCq03C06", oldValue, this.anexoCq03C06);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq03C07() {
        return this.anexoCq03C07;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03C07(Long anexoCq03C07) {
        Long oldValue = this.anexoCq03C07;
        this.anexoCq03C07 = anexoCq03C07;
        this.firePropertyChange("anexoCq03C07", oldValue, this.anexoCq03C07);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq03C08() {
        return this.anexoCq03C08;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03C08(Long anexoCq03C08) {
        Long oldValue = this.anexoCq03C08;
        this.anexoCq03C08 = anexoCq03C08;
        this.firePropertyChange("anexoCq03C08", oldValue, this.anexoCq03C08);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq03C09() {
        return this.anexoCq03C09;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03C09(Long anexoCq03C09) {
        Long oldValue = this.anexoCq03C09;
        this.anexoCq03C09 = anexoCq03C09;
        this.firePropertyChange("anexoCq03C09", oldValue, this.anexoCq03C09);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq03C10() {
        return this.anexoCq03C10;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03C10(Long anexoCq03C10) {
        Long oldValue = this.anexoCq03C10;
        this.anexoCq03C10 = anexoCq03C10;
        this.firePropertyChange("anexoCq03C10", oldValue, this.anexoCq03C10);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoCq03B2() {
        return this.anexoCq03B2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq03B2(String anexoCq03B2) {
        String oldValue = this.anexoCq03B2;
        this.anexoCq03B2 = anexoCq03B2;
        this.firePropertyChange("anexoCq03B2", oldValue, this.anexoCq03B2);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq03C04);
        result = HashCodeUtil.hash(result, this.anexoCq03C05);
        result = HashCodeUtil.hash(result, this.anexoCq03B1);
        result = HashCodeUtil.hash(result, this.anexoCq03C06);
        result = HashCodeUtil.hash(result, this.anexoCq03C07);
        result = HashCodeUtil.hash(result, this.anexoCq03C08);
        result = HashCodeUtil.hash(result, this.anexoCq03C09);
        result = HashCodeUtil.hash(result, this.anexoCq03C10);
        result = HashCodeUtil.hash(result, this.anexoCq03B2);
        return result;
    }
}

