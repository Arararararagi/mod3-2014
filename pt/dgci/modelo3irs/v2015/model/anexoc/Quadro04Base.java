/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoC.qQuadro04";
    public static final String ANEXOCQ04C401_LINK = "aAnexoC.qQuadro04.fanexoCq04C401";
    public static final String ANEXOCQ04C401 = "anexoCq04C401";
    private Long anexoCq04C401;
    public static final String ANEXOCQ04C402_LINK = "aAnexoC.qQuadro04.fanexoCq04C402";
    public static final String ANEXOCQ04C402 = "anexoCq04C402";
    private Long anexoCq04C402;
    public static final String ANEXOCQ04C403_LINK = "aAnexoC.qQuadro04.fanexoCq04C403";
    public static final String ANEXOCQ04C403 = "anexoCq04C403";
    private Long anexoCq04C403;
    public static final String ANEXOCQ04C404_LINK = "aAnexoC.qQuadro04.fanexoCq04C404";
    public static final String ANEXOCQ04C404 = "anexoCq04C404";
    private Long anexoCq04C404;
    public static final String ANEXOCQ04C405_LINK = "aAnexoC.qQuadro04.fanexoCq04C405";
    public static final String ANEXOCQ04C405 = "anexoCq04C405";
    private Long anexoCq04C405;
    public static final String ANEXOCQ04C406_LINK = "aAnexoC.qQuadro04.fanexoCq04C406";
    public static final String ANEXOCQ04C406 = "anexoCq04C406";
    private Long anexoCq04C406;
    public static final String ANEXOCQ04C407_LINK = "aAnexoC.qQuadro04.fanexoCq04C407";
    public static final String ANEXOCQ04C407 = "anexoCq04C407";
    private Long anexoCq04C407;
    public static final String ANEXOCQ04C408_LINK = "aAnexoC.qQuadro04.fanexoCq04C408";
    public static final String ANEXOCQ04C408 = "anexoCq04C408";
    private Long anexoCq04C408;
    public static final String ANEXOCQ04C409_LINK = "aAnexoC.qQuadro04.fanexoCq04C409";
    public static final String ANEXOCQ04C409 = "anexoCq04C409";
    private Long anexoCq04C409;
    public static final String ANEXOCQ04C410_LINK = "aAnexoC.qQuadro04.fanexoCq04C410";
    public static final String ANEXOCQ04C410 = "anexoCq04C410";
    private Long anexoCq04C410;
    public static final String ANEXOCQ04C464_LINK = "aAnexoC.qQuadro04.fanexoCq04C464";
    public static final String ANEXOCQ04C464 = "anexoCq04C464";
    private Long anexoCq04C464;
    public static final String ANEXOCQ04C411_LINK = "aAnexoC.qQuadro04.fanexoCq04C411";
    public static final String ANEXOCQ04C411 = "anexoCq04C411";
    private Long anexoCq04C411;
    public static final String ANEXOCQ04C412_LINK = "aAnexoC.qQuadro04.fanexoCq04C412";
    public static final String ANEXOCQ04C412 = "anexoCq04C412";
    private Long anexoCq04C412;
    public static final String ANEXOCQ04C413_LINK = "aAnexoC.qQuadro04.fanexoCq04C413";
    public static final String ANEXOCQ04C413 = "anexoCq04C413";
    private Long anexoCq04C413;
    public static final String ANEXOCQ04C414_LINK = "aAnexoC.qQuadro04.fanexoCq04C414";
    public static final String ANEXOCQ04C414 = "anexoCq04C414";
    private Long anexoCq04C414;
    public static final String ANEXOCQ04C415_LINK = "aAnexoC.qQuadro04.fanexoCq04C415";
    public static final String ANEXOCQ04C415 = "anexoCq04C415";
    private Long anexoCq04C415;
    public static final String ANEXOCQ04C416_LINK = "aAnexoC.qQuadro04.fanexoCq04C416";
    public static final String ANEXOCQ04C416 = "anexoCq04C416";
    private Long anexoCq04C416;
    public static final String ANEXOCQ04C417_LINK = "aAnexoC.qQuadro04.fanexoCq04C417";
    public static final String ANEXOCQ04C417 = "anexoCq04C417";
    private Long anexoCq04C417;
    public static final String ANEXOCQ04C418_LINK = "aAnexoC.qQuadro04.fanexoCq04C418";
    public static final String ANEXOCQ04C418 = "anexoCq04C418";
    private Long anexoCq04C418;
    public static final String ANEXOCQ04C419_LINK = "aAnexoC.qQuadro04.fanexoCq04C419";
    public static final String ANEXOCQ04C419 = "anexoCq04C419";
    private Long anexoCq04C419;
    public static final String ANEXOCQ04C420_LINK = "aAnexoC.qQuadro04.fanexoCq04C420";
    public static final String ANEXOCQ04C420 = "anexoCq04C420";
    private Long anexoCq04C420;
    public static final String ANEXOCQ04C465_LINK = "aAnexoC.qQuadro04.fanexoCq04C465";
    public static final String ANEXOCQ04C465 = "anexoCq04C465";
    private Long anexoCq04C465;
    public static final String ANEXOCQ04C421_LINK = "aAnexoC.qQuadro04.fanexoCq04C421";
    public static final String ANEXOCQ04C421 = "anexoCq04C421";
    private Long anexoCq04C421;
    public static final String ANEXOCQ04C422_LINK = "aAnexoC.qQuadro04.fanexoCq04C422";
    public static final String ANEXOCQ04C422 = "anexoCq04C422";
    private Long anexoCq04C422;
    public static final String ANEXOCQ04C423_LINK = "aAnexoC.qQuadro04.fanexoCq04C423";
    public static final String ANEXOCQ04C423 = "anexoCq04C423";
    private Long anexoCq04C423;
    public static final String ANEXOCQ04C424_LINK = "aAnexoC.qQuadro04.fanexoCq04C424";
    public static final String ANEXOCQ04C424 = "anexoCq04C424";
    private Long anexoCq04C424;
    public static final String ANEXOCQ04C425_LINK = "aAnexoC.qQuadro04.fanexoCq04C425";
    public static final String ANEXOCQ04C425 = "anexoCq04C425";
    private Long anexoCq04C425;
    public static final String ANEXOCQ04C426_LINK = "aAnexoC.qQuadro04.fanexoCq04C426";
    public static final String ANEXOCQ04C426 = "anexoCq04C426";
    private Long anexoCq04C426;
    public static final String ANEXOCQ04C427_LINK = "aAnexoC.qQuadro04.fanexoCq04C427";
    public static final String ANEXOCQ04C427 = "anexoCq04C427";
    private Long anexoCq04C427;
    public static final String ANEXOCQ04C466_LINK = "aAnexoC.qQuadro04.fanexoCq04C466";
    public static final String ANEXOCQ04C466 = "anexoCq04C466";
    private Long anexoCq04C466;
    public static final String ANEXOCQ04C428_LINK = "aAnexoC.qQuadro04.fanexoCq04C428";
    public static final String ANEXOCQ04C428 = "anexoCq04C428";
    private Long anexoCq04C428;
    public static final String ANEXOCQ04C429_LINK = "aAnexoC.qQuadro04.fanexoCq04C429";
    public static final String ANEXOCQ04C429 = "anexoCq04C429";
    private Long anexoCq04C429;
    public static final String ANEXOCQ04C430_LINK = "aAnexoC.qQuadro04.fanexoCq04C430";
    public static final String ANEXOCQ04C430 = "anexoCq04C430";
    private Long anexoCq04C430;
    public static final String ANEXOCQ04C431_LINK = "aAnexoC.qQuadro04.fanexoCq04C431";
    public static final String ANEXOCQ04C431 = "anexoCq04C431";
    private Long anexoCq04C431;
    public static final String ANEXOCQ04C432_LINK = "aAnexoC.qQuadro04.fanexoCq04C432";
    public static final String ANEXOCQ04C432 = "anexoCq04C432";
    private Long anexoCq04C432;
    public static final String ANEXOCQ04C433_LINK = "aAnexoC.qQuadro04.fanexoCq04C433";
    public static final String ANEXOCQ04C433 = "anexoCq04C433";
    private Long anexoCq04C433;
    public static final String ANEXOCQ04C434_LINK = "aAnexoC.qQuadro04.fanexoCq04C434";
    public static final String ANEXOCQ04C434 = "anexoCq04C434";
    private Long anexoCq04C434;
    public static final String ANEXOCQ04C467_LINK = "aAnexoC.qQuadro04.fanexoCq04C467";
    public static final String ANEXOCQ04C467 = "anexoCq04C467";
    private Long anexoCq04C467;
    public static final String ANEXOCQ04C468_LINK = "aAnexoC.qQuadro04.fanexoCq04C468";
    public static final String ANEXOCQ04C468 = "anexoCq04C468";
    private Long anexoCq04C468;
    public static final String ANEXOCQ04C435_LINK = "aAnexoC.qQuadro04.fanexoCq04C435";
    public static final String ANEXOCQ04C435 = "anexoCq04C435";
    private Long anexoCq04C435;
    public static final String ANEXOCQ04C436_LINK = "aAnexoC.qQuadro04.fanexoCq04C436";
    public static final String ANEXOCQ04C436 = "anexoCq04C436";
    private Long anexoCq04C436;
    public static final String ANEXOCQ04C437_LINK = "aAnexoC.qQuadro04.fanexoCq04C437";
    public static final String ANEXOCQ04C437 = "anexoCq04C437";
    private Long anexoCq04C437;
    public static final String ANEXOCQ04C438_LINK = "aAnexoC.qQuadro04.fanexoCq04C438";
    public static final String ANEXOCQ04C438 = "anexoCq04C438";
    private Long anexoCq04C438;
    public static final String ANEXOCQ04C439_LINK = "aAnexoC.qQuadro04.fanexoCq04C439";
    public static final String ANEXOCQ04C439 = "anexoCq04C439";
    private Long anexoCq04C439;
    public static final String ANEXOCQ04C440_LINK = "aAnexoC.qQuadro04.fanexoCq04C440";
    public static final String ANEXOCQ04C440 = "anexoCq04C440";
    private Long anexoCq04C440;
    public static final String ANEXOCQ04C441_LINK = "aAnexoC.qQuadro04.fanexoCq04C441";
    public static final String ANEXOCQ04C441 = "anexoCq04C441";
    private Long anexoCq04C441;
    public static final String ANEXOCQ04C442_LINK = "aAnexoC.qQuadro04.fanexoCq04C442";
    public static final String ANEXOCQ04C442 = "anexoCq04C442";
    private Long anexoCq04C442;
    public static final String ANEXOCQ04C469_LINK = "aAnexoC.qQuadro04.fanexoCq04C469";
    public static final String ANEXOCQ04C469 = "anexoCq04C469";
    private Long anexoCq04C469;
    public static final String ANEXOCQ04C443_LINK = "aAnexoC.qQuadro04.fanexoCq04C443";
    public static final String ANEXOCQ04C443 = "anexoCq04C443";
    private Long anexoCq04C443;
    public static final String ANEXOCQ04C444_LINK = "aAnexoC.qQuadro04.fanexoCq04C444";
    public static final String ANEXOCQ04C444 = "anexoCq04C444";
    private Long anexoCq04C444;
    public static final String ANEXOCQ04C445_LINK = "aAnexoC.qQuadro04.fanexoCq04C445";
    public static final String ANEXOCQ04C445 = "anexoCq04C445";
    private Long anexoCq04C445;
    public static final String ANEXOCQ04C470_LINK = "aAnexoC.qQuadro04.fanexoCq04C470";
    public static final String ANEXOCQ04C470 = "anexoCq04C470";
    private Long anexoCq04C470;
    public static final String ANEXOCQ04C446_LINK = "aAnexoC.qQuadro04.fanexoCq04C446";
    public static final String ANEXOCQ04C446 = "anexoCq04C446";
    private Long anexoCq04C446;
    public static final String ANEXOCQ04C447_LINK = "aAnexoC.qQuadro04.fanexoCq04C447";
    public static final String ANEXOCQ04C447 = "anexoCq04C447";
    private Long anexoCq04C447;
    public static final String ANEXOCQ04C471_LINK = "aAnexoC.qQuadro04.fanexoCq04C471";
    public static final String ANEXOCQ04C471 = "anexoCq04C471";
    private Long anexoCq04C471;
    public static final String ANEXOCQ04C448_LINK = "aAnexoC.qQuadro04.fanexoCq04C448";
    public static final String ANEXOCQ04C448 = "anexoCq04C448";
    private Long anexoCq04C448;
    public static final String ANEXOCQ04C449_LINK = "aAnexoC.qQuadro04.fanexoCq04C449";
    public static final String ANEXOCQ04C449 = "anexoCq04C449";
    private Long anexoCq04C449;
    public static final String ANEXOCQ04C450_LINK = "aAnexoC.qQuadro04.fanexoCq04C450";
    public static final String ANEXOCQ04C450 = "anexoCq04C450";
    private Long anexoCq04C450;
    public static final String ANEXOCQ04C472_LINK = "aAnexoC.qQuadro04.fanexoCq04C472";
    public static final String ANEXOCQ04C472 = "anexoCq04C472";
    private Long anexoCq04C472;
    public static final String ANEXOCQ04C451_LINK = "aAnexoC.qQuadro04.fanexoCq04C451";
    public static final String ANEXOCQ04C451 = "anexoCq04C451";
    private Long anexoCq04C451;
    public static final String ANEXOCQ04C473_LINK = "aAnexoC.qQuadro04.fanexoCq04C473";
    public static final String ANEXOCQ04C473 = "anexoCq04C473";
    private Long anexoCq04C473;
    public static final String ANEXOCQ04C452_LINK = "aAnexoC.qQuadro04.fanexoCq04C452";
    public static final String ANEXOCQ04C452 = "anexoCq04C452";
    private Long anexoCq04C452;
    public static final String ANEXOCQ04C453_LINK = "aAnexoC.qQuadro04.fanexoCq04C453";
    public static final String ANEXOCQ04C453 = "anexoCq04C453";
    private Long anexoCq04C453;
    public static final String ANEXOCQ04C454_LINK = "aAnexoC.qQuadro04.fanexoCq04C454";
    public static final String ANEXOCQ04C454 = "anexoCq04C454";
    private Long anexoCq04C454;
    public static final String ANEXOCQ04C455_LINK = "aAnexoC.qQuadro04.fanexoCq04C455";
    public static final String ANEXOCQ04C455 = "anexoCq04C455";
    private Long anexoCq04C455;
    public static final String ANEXOCQ04C456_LINK = "aAnexoC.qQuadro04.fanexoCq04C456";
    public static final String ANEXOCQ04C456 = "anexoCq04C456";
    private Long anexoCq04C456;
    public static final String ANEXOCQ04C462_LINK = "aAnexoC.qQuadro04.fanexoCq04C462";
    public static final String ANEXOCQ04C462 = "anexoCq04C462";
    private Long anexoCq04C462;
    public static final String ANEXOCQ04C463_LINK = "aAnexoC.qQuadro04.fanexoCq04C463";
    public static final String ANEXOCQ04C463 = "anexoCq04C463";
    private Long anexoCq04C463;
    public static final String ANEXOCQ04C457_LINK = "aAnexoC.qQuadro04.fanexoCq04C457";
    public static final String ANEXOCQ04C457 = "anexoCq04C457";
    private Long anexoCq04C457;
    public static final String ANEXOCQ04C458_LINK = "aAnexoC.qQuadro04.fanexoCq04C458";
    public static final String ANEXOCQ04C458 = "anexoCq04C458";
    private Long anexoCq04C458;
    public static final String ANEXOCQ04C459_LINK = "aAnexoC.qQuadro04.fanexoCq04C459";
    public static final String ANEXOCQ04C459 = "anexoCq04C459";
    private Long anexoCq04C459;
    public static final String ANEXOCQ04C460_LINK = "aAnexoC.qQuadro04.fanexoCq04C460";
    public static final String ANEXOCQ04C460 = "anexoCq04C460";
    private Long anexoCq04C460;
    public static final String ANEXOCQ04C461_LINK = "aAnexoC.qQuadro04.fanexoCq04C461";
    public static final String ANEXOCQ04C461 = "anexoCq04C461";
    private Long anexoCq04C461;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C401() {
        return this.anexoCq04C401;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C401(Long anexoCq04C401) {
        Long oldValue = this.anexoCq04C401;
        this.anexoCq04C401 = anexoCq04C401;
        this.firePropertyChange("anexoCq04C401", oldValue, this.anexoCq04C401);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C402() {
        return this.anexoCq04C402;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C402(Long anexoCq04C402) {
        Long oldValue = this.anexoCq04C402;
        this.anexoCq04C402 = anexoCq04C402;
        this.firePropertyChange("anexoCq04C402", oldValue, this.anexoCq04C402);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C403() {
        return this.anexoCq04C403;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C403(Long anexoCq04C403) {
        Long oldValue = this.anexoCq04C403;
        this.anexoCq04C403 = anexoCq04C403;
        this.firePropertyChange("anexoCq04C403", oldValue, this.anexoCq04C403);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C404() {
        return this.anexoCq04C404;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C404(Long anexoCq04C404) {
        Long oldValue = this.anexoCq04C404;
        this.anexoCq04C404 = anexoCq04C404;
        this.firePropertyChange("anexoCq04C404", oldValue, this.anexoCq04C404);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C405() {
        return this.anexoCq04C405;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C405(Long anexoCq04C405) {
        Long oldValue = this.anexoCq04C405;
        this.anexoCq04C405 = anexoCq04C405;
        this.firePropertyChange("anexoCq04C405", oldValue, this.anexoCq04C405);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C406() {
        return this.anexoCq04C406;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C406(Long anexoCq04C406) {
        Long oldValue = this.anexoCq04C406;
        this.anexoCq04C406 = anexoCq04C406;
        this.firePropertyChange("anexoCq04C406", oldValue, this.anexoCq04C406);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C407() {
        return this.anexoCq04C407;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C407(Long anexoCq04C407) {
        Long oldValue = this.anexoCq04C407;
        this.anexoCq04C407 = anexoCq04C407;
        this.firePropertyChange("anexoCq04C407", oldValue, this.anexoCq04C407);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C408() {
        return this.anexoCq04C408;
    }

    public void setAnexoCq04C408Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq04C408")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq04C408(newValue+=(this.getAnexoCq04C401() == null ? 0 : this.getAnexoCq04C401()) + (this.getAnexoCq04C402() == null ? 0 : this.getAnexoCq04C402()) + (this.getAnexoCq04C403() == null ? 0 : this.getAnexoCq04C403()) - (this.getAnexoCq04C404() == null ? 0 : this.getAnexoCq04C404()) - (this.getAnexoCq04C405() == null ? 0 : this.getAnexoCq04C405()) + (this.getAnexoCq04C406() == null ? 0 : this.getAnexoCq04C406()) - (this.getAnexoCq04C407() == null ? 0 : this.getAnexoCq04C407()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C408(Long anexoCq04C408) {
        Long oldValue = this.anexoCq04C408;
        this.anexoCq04C408 = anexoCq04C408;
        this.firePropertyChange("anexoCq04C408", oldValue, this.anexoCq04C408);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C409() {
        return this.anexoCq04C409;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C409(Long anexoCq04C409) {
        Long oldValue = this.anexoCq04C409;
        this.anexoCq04C409 = anexoCq04C409;
        this.firePropertyChange("anexoCq04C409", oldValue, this.anexoCq04C409);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C410() {
        return this.anexoCq04C410;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C410(Long anexoCq04C410) {
        Long oldValue = this.anexoCq04C410;
        this.anexoCq04C410 = anexoCq04C410;
        this.firePropertyChange("anexoCq04C410", oldValue, this.anexoCq04C410);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C464() {
        return this.anexoCq04C464;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C464(Long anexoCq04C464) {
        Long oldValue = this.anexoCq04C464;
        this.anexoCq04C464 = anexoCq04C464;
        this.firePropertyChange("anexoCq04C464", oldValue, this.anexoCq04C464);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C411() {
        return this.anexoCq04C411;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C411(Long anexoCq04C411) {
        Long oldValue = this.anexoCq04C411;
        this.anexoCq04C411 = anexoCq04C411;
        this.firePropertyChange("anexoCq04C411", oldValue, this.anexoCq04C411);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C412() {
        return this.anexoCq04C412;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C412(Long anexoCq04C412) {
        Long oldValue = this.anexoCq04C412;
        this.anexoCq04C412 = anexoCq04C412;
        this.firePropertyChange("anexoCq04C412", oldValue, this.anexoCq04C412);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C413() {
        return this.anexoCq04C413;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C413(Long anexoCq04C413) {
        Long oldValue = this.anexoCq04C413;
        this.anexoCq04C413 = anexoCq04C413;
        this.firePropertyChange("anexoCq04C413", oldValue, this.anexoCq04C413);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C414() {
        return this.anexoCq04C414;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C414(Long anexoCq04C414) {
        Long oldValue = this.anexoCq04C414;
        this.anexoCq04C414 = anexoCq04C414;
        this.firePropertyChange("anexoCq04C414", oldValue, this.anexoCq04C414);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C415() {
        return this.anexoCq04C415;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C415(Long anexoCq04C415) {
        Long oldValue = this.anexoCq04C415;
        this.anexoCq04C415 = anexoCq04C415;
        this.firePropertyChange("anexoCq04C415", oldValue, this.anexoCq04C415);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C416() {
        return this.anexoCq04C416;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C416(Long anexoCq04C416) {
        Long oldValue = this.anexoCq04C416;
        this.anexoCq04C416 = anexoCq04C416;
        this.firePropertyChange("anexoCq04C416", oldValue, this.anexoCq04C416);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C417() {
        return this.anexoCq04C417;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C417(Long anexoCq04C417) {
        Long oldValue = this.anexoCq04C417;
        this.anexoCq04C417 = anexoCq04C417;
        this.firePropertyChange("anexoCq04C417", oldValue, this.anexoCq04C417);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C418() {
        return this.anexoCq04C418;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C418(Long anexoCq04C418) {
        Long oldValue = this.anexoCq04C418;
        this.anexoCq04C418 = anexoCq04C418;
        this.firePropertyChange("anexoCq04C418", oldValue, this.anexoCq04C418);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C419() {
        return this.anexoCq04C419;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C419(Long anexoCq04C419) {
        Long oldValue = this.anexoCq04C419;
        this.anexoCq04C419 = anexoCq04C419;
        this.firePropertyChange("anexoCq04C419", oldValue, this.anexoCq04C419);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C420() {
        return this.anexoCq04C420;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C420(Long anexoCq04C420) {
        Long oldValue = this.anexoCq04C420;
        this.anexoCq04C420 = anexoCq04C420;
        this.firePropertyChange("anexoCq04C420", oldValue, this.anexoCq04C420);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C465() {
        return this.anexoCq04C465;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C465(Long anexoCq04C465) {
        Long oldValue = this.anexoCq04C465;
        this.anexoCq04C465 = anexoCq04C465;
        this.firePropertyChange("anexoCq04C465", oldValue, this.anexoCq04C465);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C421() {
        return this.anexoCq04C421;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C421(Long anexoCq04C421) {
        Long oldValue = this.anexoCq04C421;
        this.anexoCq04C421 = anexoCq04C421;
        this.firePropertyChange("anexoCq04C421", oldValue, this.anexoCq04C421);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C422() {
        return this.anexoCq04C422;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C422(Long anexoCq04C422) {
        Long oldValue = this.anexoCq04C422;
        this.anexoCq04C422 = anexoCq04C422;
        this.firePropertyChange("anexoCq04C422", oldValue, this.anexoCq04C422);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C423() {
        return this.anexoCq04C423;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C423(Long anexoCq04C423) {
        Long oldValue = this.anexoCq04C423;
        this.anexoCq04C423 = anexoCq04C423;
        this.firePropertyChange("anexoCq04C423", oldValue, this.anexoCq04C423);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C424() {
        return this.anexoCq04C424;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C424(Long anexoCq04C424) {
        Long oldValue = this.anexoCq04C424;
        this.anexoCq04C424 = anexoCq04C424;
        this.firePropertyChange("anexoCq04C424", oldValue, this.anexoCq04C424);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C425() {
        return this.anexoCq04C425;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C425(Long anexoCq04C425) {
        Long oldValue = this.anexoCq04C425;
        this.anexoCq04C425 = anexoCq04C425;
        this.firePropertyChange("anexoCq04C425", oldValue, this.anexoCq04C425);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C426() {
        return this.anexoCq04C426;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C426(Long anexoCq04C426) {
        Long oldValue = this.anexoCq04C426;
        this.anexoCq04C426 = anexoCq04C426;
        this.firePropertyChange("anexoCq04C426", oldValue, this.anexoCq04C426);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C427() {
        return this.anexoCq04C427;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C427(Long anexoCq04C427) {
        Long oldValue = this.anexoCq04C427;
        this.anexoCq04C427 = anexoCq04C427;
        this.firePropertyChange("anexoCq04C427", oldValue, this.anexoCq04C427);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C466() {
        return this.anexoCq04C466;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C466(Long anexoCq04C466) {
        Long oldValue = this.anexoCq04C466;
        this.anexoCq04C466 = anexoCq04C466;
        this.firePropertyChange("anexoCq04C466", oldValue, this.anexoCq04C466);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C428() {
        return this.anexoCq04C428;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C428(Long anexoCq04C428) {
        Long oldValue = this.anexoCq04C428;
        this.anexoCq04C428 = anexoCq04C428;
        this.firePropertyChange("anexoCq04C428", oldValue, this.anexoCq04C428);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C429() {
        return this.anexoCq04C429;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C429(Long anexoCq04C429) {
        Long oldValue = this.anexoCq04C429;
        this.anexoCq04C429 = anexoCq04C429;
        this.firePropertyChange("anexoCq04C429", oldValue, this.anexoCq04C429);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C430() {
        return this.anexoCq04C430;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C430(Long anexoCq04C430) {
        Long oldValue = this.anexoCq04C430;
        this.anexoCq04C430 = anexoCq04C430;
        this.firePropertyChange("anexoCq04C430", oldValue, this.anexoCq04C430);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C431() {
        return this.anexoCq04C431;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C431(Long anexoCq04C431) {
        Long oldValue = this.anexoCq04C431;
        this.anexoCq04C431 = anexoCq04C431;
        this.firePropertyChange("anexoCq04C431", oldValue, this.anexoCq04C431);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C432() {
        return this.anexoCq04C432;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C432(Long anexoCq04C432) {
        Long oldValue = this.anexoCq04C432;
        this.anexoCq04C432 = anexoCq04C432;
        this.firePropertyChange("anexoCq04C432", oldValue, this.anexoCq04C432);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C433() {
        return this.anexoCq04C433;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C433(Long anexoCq04C433) {
        Long oldValue = this.anexoCq04C433;
        this.anexoCq04C433 = anexoCq04C433;
        this.firePropertyChange("anexoCq04C433", oldValue, this.anexoCq04C433);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C434() {
        return this.anexoCq04C434;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C434(Long anexoCq04C434) {
        Long oldValue = this.anexoCq04C434;
        this.anexoCq04C434 = anexoCq04C434;
        this.firePropertyChange("anexoCq04C434", oldValue, this.anexoCq04C434);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C467() {
        return this.anexoCq04C467;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C467(Long anexoCq04C467) {
        Long oldValue = this.anexoCq04C467;
        this.anexoCq04C467 = anexoCq04C467;
        this.firePropertyChange("anexoCq04C467", oldValue, this.anexoCq04C467);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C468() {
        return this.anexoCq04C468;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C468(Long anexoCq04C468) {
        Long oldValue = this.anexoCq04C468;
        this.anexoCq04C468 = anexoCq04C468;
        this.firePropertyChange("anexoCq04C468", oldValue, this.anexoCq04C468);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C435() {
        return this.anexoCq04C435;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C435(Long anexoCq04C435) {
        Long oldValue = this.anexoCq04C435;
        this.anexoCq04C435 = anexoCq04C435;
        this.firePropertyChange("anexoCq04C435", oldValue, this.anexoCq04C435);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C436() {
        return this.anexoCq04C436;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C436(Long anexoCq04C436) {
        Long oldValue = this.anexoCq04C436;
        this.anexoCq04C436 = anexoCq04C436;
        this.firePropertyChange("anexoCq04C436", oldValue, this.anexoCq04C436);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C437() {
        return this.anexoCq04C437;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C437(Long anexoCq04C437) {
        Long oldValue = this.anexoCq04C437;
        this.anexoCq04C437 = anexoCq04C437;
        this.firePropertyChange("anexoCq04C437", oldValue, this.anexoCq04C437);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C438() {
        return this.anexoCq04C438;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C438(Long anexoCq04C438) {
        Long oldValue = this.anexoCq04C438;
        this.anexoCq04C438 = anexoCq04C438;
        this.firePropertyChange("anexoCq04C438", oldValue, this.anexoCq04C438);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C439() {
        return this.anexoCq04C439;
    }

    public void setAnexoCq04C439Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq04C439")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq04C439(newValue+=(this.getAnexoCq04C408() == null ? 0 : this.getAnexoCq04C408()) + (this.getAnexoCq04C409() == null ? 0 : this.getAnexoCq04C409()) + (this.getAnexoCq04C410() == null ? 0 : this.getAnexoCq04C410()) + (this.getAnexoCq04C411() == null ? 0 : this.getAnexoCq04C411()) + (this.getAnexoCq04C412() == null ? 0 : this.getAnexoCq04C412()) + (this.getAnexoCq04C413() == null ? 0 : this.getAnexoCq04C413()) + (this.getAnexoCq04C414() == null ? 0 : this.getAnexoCq04C414()) + (this.getAnexoCq04C415() == null ? 0 : this.getAnexoCq04C415()) + (this.getAnexoCq04C416() == null ? 0 : this.getAnexoCq04C416()) + (this.getAnexoCq04C417() == null ? 0 : this.getAnexoCq04C417()) + (this.getAnexoCq04C418() == null ? 0 : this.getAnexoCq04C418()) + (this.getAnexoCq04C419() == null ? 0 : this.getAnexoCq04C419()) + (this.getAnexoCq04C420() == null ? 0 : this.getAnexoCq04C420()) + (this.getAnexoCq04C421() == null ? 0 : this.getAnexoCq04C421()) + (this.getAnexoCq04C422() == null ? 0 : this.getAnexoCq04C422()) + (this.getAnexoCq04C423() == null ? 0 : this.getAnexoCq04C423()) + (this.getAnexoCq04C424() == null ? 0 : this.getAnexoCq04C424()) + (this.getAnexoCq04C425() == null ? 0 : this.getAnexoCq04C425()) + (this.getAnexoCq04C426() == null ? 0 : this.getAnexoCq04C426()) + (this.getAnexoCq04C427() == null ? 0 : this.getAnexoCq04C427()) + (this.getAnexoCq04C428() == null ? 0 : this.getAnexoCq04C428()) + (this.getAnexoCq04C429() == null ? 0 : this.getAnexoCq04C429()) + (this.getAnexoCq04C430() == null ? 0 : this.getAnexoCq04C430()) + (this.getAnexoCq04C431() == null ? 0 : this.getAnexoCq04C431()) + (this.getAnexoCq04C432() == null ? 0 : this.getAnexoCq04C432()) + (this.getAnexoCq04C433() == null ? 0 : this.getAnexoCq04C433()) + (this.getAnexoCq04C434() == null ? 0 : this.getAnexoCq04C434()) + (this.getAnexoCq04C435() == null ? 0 : this.getAnexoCq04C435()) + (this.getAnexoCq04C436() == null ? 0 : this.getAnexoCq04C436()) + (this.getAnexoCq04C437() == null ? 0 : this.getAnexoCq04C437()) + (this.getAnexoCq04C438() == null ? 0 : this.getAnexoCq04C438()) + (this.getAnexoCq04C464() == null ? 0 : this.getAnexoCq04C464()) + (this.getAnexoCq04C465() == null ? 0 : this.getAnexoCq04C465()) + (this.getAnexoCq04C466() == null ? 0 : this.getAnexoCq04C466()) + (this.getAnexoCq04C467() == null ? 0 : this.getAnexoCq04C467()) + (this.getAnexoCq04C468() == null ? 0 : this.getAnexoCq04C468()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C439(Long anexoCq04C439) {
        Long oldValue = this.anexoCq04C439;
        this.anexoCq04C439 = anexoCq04C439;
        this.firePropertyChange("anexoCq04C439", oldValue, this.anexoCq04C439);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C440() {
        return this.anexoCq04C440;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C440(Long anexoCq04C440) {
        Long oldValue = this.anexoCq04C440;
        this.anexoCq04C440 = anexoCq04C440;
        this.firePropertyChange("anexoCq04C440", oldValue, this.anexoCq04C440);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C441() {
        return this.anexoCq04C441;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C441(Long anexoCq04C441) {
        Long oldValue = this.anexoCq04C441;
        this.anexoCq04C441 = anexoCq04C441;
        this.firePropertyChange("anexoCq04C441", oldValue, this.anexoCq04C441);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C442() {
        return this.anexoCq04C442;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C442(Long anexoCq04C442) {
        Long oldValue = this.anexoCq04C442;
        this.anexoCq04C442 = anexoCq04C442;
        this.firePropertyChange("anexoCq04C442", oldValue, this.anexoCq04C442);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C469() {
        return this.anexoCq04C469;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C469(Long anexoCq04C469) {
        Long oldValue = this.anexoCq04C469;
        this.anexoCq04C469 = anexoCq04C469;
        this.firePropertyChange("anexoCq04C469", oldValue, this.anexoCq04C469);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C443() {
        return this.anexoCq04C443;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C443(Long anexoCq04C443) {
        Long oldValue = this.anexoCq04C443;
        this.anexoCq04C443 = anexoCq04C443;
        this.firePropertyChange("anexoCq04C443", oldValue, this.anexoCq04C443);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C444() {
        return this.anexoCq04C444;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C444(Long anexoCq04C444) {
        Long oldValue = this.anexoCq04C444;
        this.anexoCq04C444 = anexoCq04C444;
        this.firePropertyChange("anexoCq04C444", oldValue, this.anexoCq04C444);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C445() {
        return this.anexoCq04C445;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C445(Long anexoCq04C445) {
        Long oldValue = this.anexoCq04C445;
        this.anexoCq04C445 = anexoCq04C445;
        this.firePropertyChange("anexoCq04C445", oldValue, this.anexoCq04C445);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C470() {
        return this.anexoCq04C470;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C470(Long anexoCq04C470) {
        Long oldValue = this.anexoCq04C470;
        this.anexoCq04C470 = anexoCq04C470;
        this.firePropertyChange("anexoCq04C470", oldValue, this.anexoCq04C470);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C446() {
        return this.anexoCq04C446;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C446(Long anexoCq04C446) {
        Long oldValue = this.anexoCq04C446;
        this.anexoCq04C446 = anexoCq04C446;
        this.firePropertyChange("anexoCq04C446", oldValue, this.anexoCq04C446);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C447() {
        return this.anexoCq04C447;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C447(Long anexoCq04C447) {
        Long oldValue = this.anexoCq04C447;
        this.anexoCq04C447 = anexoCq04C447;
        this.firePropertyChange("anexoCq04C447", oldValue, this.anexoCq04C447);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C471() {
        return this.anexoCq04C471;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C471(Long anexoCq04C471) {
        Long oldValue = this.anexoCq04C471;
        this.anexoCq04C471 = anexoCq04C471;
        this.firePropertyChange("anexoCq04C471", oldValue, this.anexoCq04C471);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C448() {
        return this.anexoCq04C448;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C448(Long anexoCq04C448) {
        Long oldValue = this.anexoCq04C448;
        this.anexoCq04C448 = anexoCq04C448;
        this.firePropertyChange("anexoCq04C448", oldValue, this.anexoCq04C448);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C449() {
        return this.anexoCq04C449;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C449(Long anexoCq04C449) {
        Long oldValue = this.anexoCq04C449;
        this.anexoCq04C449 = anexoCq04C449;
        this.firePropertyChange("anexoCq04C449", oldValue, this.anexoCq04C449);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C450() {
        return this.anexoCq04C450;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C450(Long anexoCq04C450) {
        Long oldValue = this.anexoCq04C450;
        this.anexoCq04C450 = anexoCq04C450;
        this.firePropertyChange("anexoCq04C450", oldValue, this.anexoCq04C450);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C472() {
        return this.anexoCq04C472;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C472(Long anexoCq04C472) {
        Long oldValue = this.anexoCq04C472;
        this.anexoCq04C472 = anexoCq04C472;
        this.firePropertyChange("anexoCq04C472", oldValue, this.anexoCq04C472);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C451() {
        return this.anexoCq04C451;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C451(Long anexoCq04C451) {
        Long oldValue = this.anexoCq04C451;
        this.anexoCq04C451 = anexoCq04C451;
        this.firePropertyChange("anexoCq04C451", oldValue, this.anexoCq04C451);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C473() {
        return this.anexoCq04C473;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C473(Long anexoCq04C473) {
        Long oldValue = this.anexoCq04C473;
        this.anexoCq04C473 = anexoCq04C473;
        this.firePropertyChange("anexoCq04C473", oldValue, this.anexoCq04C473);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C452() {
        return this.anexoCq04C452;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C452(Long anexoCq04C452) {
        Long oldValue = this.anexoCq04C452;
        this.anexoCq04C452 = anexoCq04C452;
        this.firePropertyChange("anexoCq04C452", oldValue, this.anexoCq04C452);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C453() {
        return this.anexoCq04C453;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C453(Long anexoCq04C453) {
        Long oldValue = this.anexoCq04C453;
        this.anexoCq04C453 = anexoCq04C453;
        this.firePropertyChange("anexoCq04C453", oldValue, this.anexoCq04C453);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C454() {
        return this.anexoCq04C454;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C454(Long anexoCq04C454) {
        Long oldValue = this.anexoCq04C454;
        this.anexoCq04C454 = anexoCq04C454;
        this.firePropertyChange("anexoCq04C454", oldValue, this.anexoCq04C454);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C455() {
        return this.anexoCq04C455;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C455(Long anexoCq04C455) {
        Long oldValue = this.anexoCq04C455;
        this.anexoCq04C455 = anexoCq04C455;
        this.firePropertyChange("anexoCq04C455", oldValue, this.anexoCq04C455);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C456() {
        return this.anexoCq04C456;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C456(Long anexoCq04C456) {
        Long oldValue = this.anexoCq04C456;
        this.anexoCq04C456 = anexoCq04C456;
        this.firePropertyChange("anexoCq04C456", oldValue, this.anexoCq04C456);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C462() {
        return this.anexoCq04C462;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C462(Long anexoCq04C462) {
        Long oldValue = this.anexoCq04C462;
        this.anexoCq04C462 = anexoCq04C462;
        this.firePropertyChange("anexoCq04C462", oldValue, this.anexoCq04C462);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C463() {
        return this.anexoCq04C463;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C463(Long anexoCq04C463) {
        Long oldValue = this.anexoCq04C463;
        this.anexoCq04C463 = anexoCq04C463;
        this.firePropertyChange("anexoCq04C463", oldValue, this.anexoCq04C463);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C457() {
        return this.anexoCq04C457;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C457(Long anexoCq04C457) {
        Long oldValue = this.anexoCq04C457;
        this.anexoCq04C457 = anexoCq04C457;
        this.firePropertyChange("anexoCq04C457", oldValue, this.anexoCq04C457);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C458() {
        return this.anexoCq04C458;
    }

    public void setAnexoCq04C458Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq04C458")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq04C458(newValue+=(this.getAnexoCq04C440() == null ? 0 : this.getAnexoCq04C440()) + (this.getAnexoCq04C441() == null ? 0 : this.getAnexoCq04C441()) + (this.getAnexoCq04C442() == null ? 0 : this.getAnexoCq04C442()) + (this.getAnexoCq04C443() == null ? 0 : this.getAnexoCq04C443()) + (this.getAnexoCq04C444() == null ? 0 : this.getAnexoCq04C444()) + (this.getAnexoCq04C445() == null ? 0 : this.getAnexoCq04C445()) + (this.getAnexoCq04C446() == null ? 0 : this.getAnexoCq04C446()) + (this.getAnexoCq04C447() == null ? 0 : this.getAnexoCq04C447()) + (this.getAnexoCq04C448() == null ? 0 : this.getAnexoCq04C448()) + (this.getAnexoCq04C449() == null ? 0 : this.getAnexoCq04C449()) + (this.getAnexoCq04C450() == null ? 0 : this.getAnexoCq04C450()) + (this.getAnexoCq04C451() == null ? 0 : this.getAnexoCq04C451()) + (this.getAnexoCq04C452() == null ? 0 : this.getAnexoCq04C452()) + (this.getAnexoCq04C453() == null ? 0 : this.getAnexoCq04C453()) + (this.getAnexoCq04C454() == null ? 0 : this.getAnexoCq04C454()) + (this.getAnexoCq04C455() == null ? 0 : this.getAnexoCq04C455()) + (this.getAnexoCq04C456() == null ? 0 : this.getAnexoCq04C456()) + (this.getAnexoCq04C457() == null ? 0 : this.getAnexoCq04C457()) + (this.getAnexoCq04C462() == null ? 0 : this.getAnexoCq04C462()) + (this.getAnexoCq04C463() == null ? 0 : this.getAnexoCq04C463()) + (this.getAnexoCq04C469() == null ? 0 : this.getAnexoCq04C469()) + (this.getAnexoCq04C470() == null ? 0 : this.getAnexoCq04C470()) + (this.getAnexoCq04C471() == null ? 0 : this.getAnexoCq04C471()) + (this.getAnexoCq04C472() == null ? 0 : this.getAnexoCq04C472()) + (this.getAnexoCq04C473() == null ? 0 : this.getAnexoCq04C473()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C458(Long anexoCq04C458) {
        Long oldValue = this.anexoCq04C458;
        this.anexoCq04C458 = anexoCq04C458;
        this.firePropertyChange("anexoCq04C458", oldValue, this.anexoCq04C458);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C459() {
        return this.anexoCq04C459;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C459(Long anexoCq04C459) {
        Long oldValue = this.anexoCq04C459;
        this.anexoCq04C459 = anexoCq04C459;
        this.firePropertyChange("anexoCq04C459", oldValue, this.anexoCq04C459);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C460() {
        return this.anexoCq04C460;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C460(Long anexoCq04C460) {
        Long oldValue = this.anexoCq04C460;
        this.anexoCq04C460 = anexoCq04C460;
        this.firePropertyChange("anexoCq04C460", oldValue, this.anexoCq04C460);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq04C461() {
        return this.anexoCq04C461;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq04C461(Long anexoCq04C461) {
        Long oldValue = this.anexoCq04C461;
        this.anexoCq04C461 = anexoCq04C461;
        this.firePropertyChange("anexoCq04C461", oldValue, this.anexoCq04C461);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq04C401);
        result = HashCodeUtil.hash(result, this.anexoCq04C402);
        result = HashCodeUtil.hash(result, this.anexoCq04C403);
        result = HashCodeUtil.hash(result, this.anexoCq04C404);
        result = HashCodeUtil.hash(result, this.anexoCq04C405);
        result = HashCodeUtil.hash(result, this.anexoCq04C406);
        result = HashCodeUtil.hash(result, this.anexoCq04C407);
        result = HashCodeUtil.hash(result, this.anexoCq04C408);
        result = HashCodeUtil.hash(result, this.anexoCq04C409);
        result = HashCodeUtil.hash(result, this.anexoCq04C410);
        result = HashCodeUtil.hash(result, this.anexoCq04C464);
        result = HashCodeUtil.hash(result, this.anexoCq04C411);
        result = HashCodeUtil.hash(result, this.anexoCq04C412);
        result = HashCodeUtil.hash(result, this.anexoCq04C413);
        result = HashCodeUtil.hash(result, this.anexoCq04C414);
        result = HashCodeUtil.hash(result, this.anexoCq04C415);
        result = HashCodeUtil.hash(result, this.anexoCq04C416);
        result = HashCodeUtil.hash(result, this.anexoCq04C417);
        result = HashCodeUtil.hash(result, this.anexoCq04C418);
        result = HashCodeUtil.hash(result, this.anexoCq04C419);
        result = HashCodeUtil.hash(result, this.anexoCq04C420);
        result = HashCodeUtil.hash(result, this.anexoCq04C465);
        result = HashCodeUtil.hash(result, this.anexoCq04C421);
        result = HashCodeUtil.hash(result, this.anexoCq04C422);
        result = HashCodeUtil.hash(result, this.anexoCq04C423);
        result = HashCodeUtil.hash(result, this.anexoCq04C424);
        result = HashCodeUtil.hash(result, this.anexoCq04C425);
        result = HashCodeUtil.hash(result, this.anexoCq04C426);
        result = HashCodeUtil.hash(result, this.anexoCq04C427);
        result = HashCodeUtil.hash(result, this.anexoCq04C466);
        result = HashCodeUtil.hash(result, this.anexoCq04C428);
        result = HashCodeUtil.hash(result, this.anexoCq04C429);
        result = HashCodeUtil.hash(result, this.anexoCq04C430);
        result = HashCodeUtil.hash(result, this.anexoCq04C431);
        result = HashCodeUtil.hash(result, this.anexoCq04C432);
        result = HashCodeUtil.hash(result, this.anexoCq04C433);
        result = HashCodeUtil.hash(result, this.anexoCq04C434);
        result = HashCodeUtil.hash(result, this.anexoCq04C467);
        result = HashCodeUtil.hash(result, this.anexoCq04C468);
        result = HashCodeUtil.hash(result, this.anexoCq04C435);
        result = HashCodeUtil.hash(result, this.anexoCq04C436);
        result = HashCodeUtil.hash(result, this.anexoCq04C437);
        result = HashCodeUtil.hash(result, this.anexoCq04C438);
        result = HashCodeUtil.hash(result, this.anexoCq04C439);
        result = HashCodeUtil.hash(result, this.anexoCq04C440);
        result = HashCodeUtil.hash(result, this.anexoCq04C441);
        result = HashCodeUtil.hash(result, this.anexoCq04C442);
        result = HashCodeUtil.hash(result, this.anexoCq04C469);
        result = HashCodeUtil.hash(result, this.anexoCq04C443);
        result = HashCodeUtil.hash(result, this.anexoCq04C444);
        result = HashCodeUtil.hash(result, this.anexoCq04C445);
        result = HashCodeUtil.hash(result, this.anexoCq04C470);
        result = HashCodeUtil.hash(result, this.anexoCq04C446);
        result = HashCodeUtil.hash(result, this.anexoCq04C447);
        result = HashCodeUtil.hash(result, this.anexoCq04C471);
        result = HashCodeUtil.hash(result, this.anexoCq04C448);
        result = HashCodeUtil.hash(result, this.anexoCq04C449);
        result = HashCodeUtil.hash(result, this.anexoCq04C450);
        result = HashCodeUtil.hash(result, this.anexoCq04C472);
        result = HashCodeUtil.hash(result, this.anexoCq04C451);
        result = HashCodeUtil.hash(result, this.anexoCq04C473);
        result = HashCodeUtil.hash(result, this.anexoCq04C452);
        result = HashCodeUtil.hash(result, this.anexoCq04C453);
        result = HashCodeUtil.hash(result, this.anexoCq04C454);
        result = HashCodeUtil.hash(result, this.anexoCq04C455);
        result = HashCodeUtil.hash(result, this.anexoCq04C456);
        result = HashCodeUtil.hash(result, this.anexoCq04C462);
        result = HashCodeUtil.hash(result, this.anexoCq04C463);
        result = HashCodeUtil.hash(result, this.anexoCq04C457);
        result = HashCodeUtil.hash(result, this.anexoCq04C458);
        result = HashCodeUtil.hash(result, this.anexoCq04C459);
        result = HashCodeUtil.hash(result, this.anexoCq04C460);
        result = HashCodeUtil.hash(result, this.anexoCq04C461);
        return result;
    }
}

