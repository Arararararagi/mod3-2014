/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoC.qQuadro02";
    public static final String ANEXOCQ02C03_LINK = "aAnexoC.qQuadro02.fanexoCq02C03";
    public static final String ANEXOCQ02C03 = "anexoCq02C03";
    private Long anexoCq02C03;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoCq02C03() {
        return this.anexoCq02C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq02C03(Long anexoCq02C03) {
        Long oldValue = this.anexoCq02C03;
        this.anexoCq02C03 = anexoCq02C03;
        this.firePropertyChange("anexoCq02C03", oldValue, this.anexoCq02C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq02C03);
        return result;
    }
}

