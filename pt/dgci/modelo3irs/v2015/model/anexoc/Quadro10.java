/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10Base;

public class Quadro10
extends Quadro10Base {
    public Long getSumC1() {
        long soma = 0;
        if (this.getAnexoCq10C1001() != null) {
            soma+=this.getAnexoCq10C1001().longValue();
        }
        if (this.getAnexoCq10C1002() != null) {
            soma+=this.getAnexoCq10C1002().longValue();
        }
        if (this.getAnexoCq10C1003() != null) {
            soma+=this.getAnexoCq10C1003().longValue();
        }
        if (this.getAnexoCq10C1004() != null) {
            soma+=this.getAnexoCq10C1004().longValue();
        }
        if (this.getAnexoCq10C1005() != null) {
            soma+=this.getAnexoCq10C1005().longValue();
        }
        if (this.getAnexoCq10C1006() != null) {
            soma+=this.getAnexoCq10C1006().longValue();
        }
        if (this.getAnexoCq10C1007() != null) {
            soma+=this.getAnexoCq10C1007().longValue();
        }
        return soma;
    }
}

