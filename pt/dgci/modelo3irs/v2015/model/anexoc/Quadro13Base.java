/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro13Base
extends QuadroModel {
    public static final String QUADRO13_LINK = "aAnexoC.qQuadro13";
    public static final String ANEXOCQ13C1301_LINK = "aAnexoC.qQuadro13.fanexoCq13C1301";
    public static final String ANEXOCQ13C1301 = "anexoCq13C1301";
    private Long anexoCq13C1301;
    public static final String ANEXOCQ13C1302_LINK = "aAnexoC.qQuadro13.fanexoCq13C1302";
    public static final String ANEXOCQ13C1302 = "anexoCq13C1302";
    private Long anexoCq13C1302;
    public static final String ANEXOCQ13C1303_LINK = "aAnexoC.qQuadro13.fanexoCq13C1303";
    public static final String ANEXOCQ13C1303 = "anexoCq13C1303";
    private Long anexoCq13C1303;
    public static final String ANEXOCQ13C1304_LINK = "aAnexoC.qQuadro13.fanexoCq13C1304";
    public static final String ANEXOCQ13C1304 = "anexoCq13C1304";
    private Long anexoCq13C1304;
    public static final String ANEXOCQ13C1305_LINK = "aAnexoC.qQuadro13.fanexoCq13C1305";
    public static final String ANEXOCQ13C1305 = "anexoCq13C1305";
    private Long anexoCq13C1305;
    public static final String ANEXOCQ13C1306_LINK = "aAnexoC.qQuadro13.fanexoCq13C1306";
    public static final String ANEXOCQ13C1306 = "anexoCq13C1306";
    private Long anexoCq13C1306;
    public static final String ANEXOCQ13C1307_LINK = "aAnexoC.qQuadro13.fanexoCq13C1307";
    public static final String ANEXOCQ13C1307 = "anexoCq13C1307";
    private Long anexoCq13C1307;
    public static final String ANEXOCQ13C1308_LINK = "aAnexoC.qQuadro13.fanexoCq13C1308";
    public static final String ANEXOCQ13C1308 = "anexoCq13C1308";
    private Long anexoCq13C1308;
    public static final String ANEXOCQ13C1309_LINK = "aAnexoC.qQuadro13.fanexoCq13C1309";
    public static final String ANEXOCQ13C1309 = "anexoCq13C1309";
    private Long anexoCq13C1309;
    public static final String ANEXOCQ13C1310_LINK = "aAnexoC.qQuadro13.fanexoCq13C1310";
    public static final String ANEXOCQ13C1310 = "anexoCq13C1310";
    private Long anexoCq13C1310;
    public static final String ANEXOCQ13C1311_LINK = "aAnexoC.qQuadro13.fanexoCq13C1311";
    public static final String ANEXOCQ13C1311 = "anexoCq13C1311";
    private Long anexoCq13C1311;
    public static final String ANEXOCQ13C1312_LINK = "aAnexoC.qQuadro13.fanexoCq13C1312";
    public static final String ANEXOCQ13C1312 = "anexoCq13C1312";
    private Long anexoCq13C1312;
    public static final String ANEXOCQ13C1313_LINK = "aAnexoC.qQuadro13.fanexoCq13C1313";
    public static final String ANEXOCQ13C1313 = "anexoCq13C1313";
    private Long anexoCq13C1313;
    public static final String ANEXOCQ13C1314_LINK = "aAnexoC.qQuadro13.fanexoCq13C1314";
    public static final String ANEXOCQ13C1314 = "anexoCq13C1314";
    private Long anexoCq13C1314;
    public static final String ANEXOCQ13C1315_LINK = "aAnexoC.qQuadro13.fanexoCq13C1315";
    public static final String ANEXOCQ13C1315 = "anexoCq13C1315";
    private Long anexoCq13C1315;
    public static final String ANEXOCQ13C1316_LINK = "aAnexoC.qQuadro13.fanexoCq13C1316";
    public static final String ANEXOCQ13C1316 = "anexoCq13C1316";
    private Long anexoCq13C1316;
    public static final String ANEXOCQ13C1317_LINK = "aAnexoC.qQuadro13.fanexoCq13C1317";
    public static final String ANEXOCQ13C1317 = "anexoCq13C1317";
    private Long anexoCq13C1317;
    public static final String ANEXOCQ13C1318_LINK = "aAnexoC.qQuadro13.fanexoCq13C1318";
    public static final String ANEXOCQ13C1318 = "anexoCq13C1318";
    private Long anexoCq13C1318;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1301() {
        return this.anexoCq13C1301;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1301(Long anexoCq13C1301) {
        Long oldValue = this.anexoCq13C1301;
        this.anexoCq13C1301 = anexoCq13C1301;
        this.firePropertyChange("anexoCq13C1301", oldValue, this.anexoCq13C1301);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1302() {
        return this.anexoCq13C1302;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1302(Long anexoCq13C1302) {
        Long oldValue = this.anexoCq13C1302;
        this.anexoCq13C1302 = anexoCq13C1302;
        this.firePropertyChange("anexoCq13C1302", oldValue, this.anexoCq13C1302);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1303() {
        return this.anexoCq13C1303;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1303(Long anexoCq13C1303) {
        Long oldValue = this.anexoCq13C1303;
        this.anexoCq13C1303 = anexoCq13C1303;
        this.firePropertyChange("anexoCq13C1303", oldValue, this.anexoCq13C1303);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1304() {
        return this.anexoCq13C1304;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1304(Long anexoCq13C1304) {
        Long oldValue = this.anexoCq13C1304;
        this.anexoCq13C1304 = anexoCq13C1304;
        this.firePropertyChange("anexoCq13C1304", oldValue, this.anexoCq13C1304);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1305() {
        return this.anexoCq13C1305;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1305(Long anexoCq13C1305) {
        Long oldValue = this.anexoCq13C1305;
        this.anexoCq13C1305 = anexoCq13C1305;
        this.firePropertyChange("anexoCq13C1305", oldValue, this.anexoCq13C1305);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1306() {
        return this.anexoCq13C1306;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1306(Long anexoCq13C1306) {
        Long oldValue = this.anexoCq13C1306;
        this.anexoCq13C1306 = anexoCq13C1306;
        this.firePropertyChange("anexoCq13C1306", oldValue, this.anexoCq13C1306);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1307() {
        return this.anexoCq13C1307;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1307(Long anexoCq13C1307) {
        Long oldValue = this.anexoCq13C1307;
        this.anexoCq13C1307 = anexoCq13C1307;
        this.firePropertyChange("anexoCq13C1307", oldValue, this.anexoCq13C1307);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1308() {
        return this.anexoCq13C1308;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1308(Long anexoCq13C1308) {
        Long oldValue = this.anexoCq13C1308;
        this.anexoCq13C1308 = anexoCq13C1308;
        this.firePropertyChange("anexoCq13C1308", oldValue, this.anexoCq13C1308);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1309() {
        return this.anexoCq13C1309;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1309(Long anexoCq13C1309) {
        Long oldValue = this.anexoCq13C1309;
        this.anexoCq13C1309 = anexoCq13C1309;
        this.firePropertyChange("anexoCq13C1309", oldValue, this.anexoCq13C1309);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1310() {
        return this.anexoCq13C1310;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1310(Long anexoCq13C1310) {
        Long oldValue = this.anexoCq13C1310;
        this.anexoCq13C1310 = anexoCq13C1310;
        this.firePropertyChange("anexoCq13C1310", oldValue, this.anexoCq13C1310);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1311() {
        return this.anexoCq13C1311;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1311(Long anexoCq13C1311) {
        Long oldValue = this.anexoCq13C1311;
        this.anexoCq13C1311 = anexoCq13C1311;
        this.firePropertyChange("anexoCq13C1311", oldValue, this.anexoCq13C1311);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1312() {
        return this.anexoCq13C1312;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1312(Long anexoCq13C1312) {
        Long oldValue = this.anexoCq13C1312;
        this.anexoCq13C1312 = anexoCq13C1312;
        this.firePropertyChange("anexoCq13C1312", oldValue, this.anexoCq13C1312);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1313() {
        return this.anexoCq13C1313;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1313(Long anexoCq13C1313) {
        Long oldValue = this.anexoCq13C1313;
        this.anexoCq13C1313 = anexoCq13C1313;
        this.firePropertyChange("anexoCq13C1313", oldValue, this.anexoCq13C1313);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1314() {
        return this.anexoCq13C1314;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1314(Long anexoCq13C1314) {
        Long oldValue = this.anexoCq13C1314;
        this.anexoCq13C1314 = anexoCq13C1314;
        this.firePropertyChange("anexoCq13C1314", oldValue, this.anexoCq13C1314);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1315() {
        return this.anexoCq13C1315;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1315(Long anexoCq13C1315) {
        Long oldValue = this.anexoCq13C1315;
        this.anexoCq13C1315 = anexoCq13C1315;
        this.firePropertyChange("anexoCq13C1315", oldValue, this.anexoCq13C1315);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1316() {
        return this.anexoCq13C1316;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1316(Long anexoCq13C1316) {
        Long oldValue = this.anexoCq13C1316;
        this.anexoCq13C1316 = anexoCq13C1316;
        this.firePropertyChange("anexoCq13C1316", oldValue, this.anexoCq13C1316);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1317() {
        return this.anexoCq13C1317;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1317(Long anexoCq13C1317) {
        Long oldValue = this.anexoCq13C1317;
        this.anexoCq13C1317 = anexoCq13C1317;
        this.firePropertyChange("anexoCq13C1317", oldValue, this.anexoCq13C1317);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq13C1318() {
        return this.anexoCq13C1318;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq13C1318(Long anexoCq13C1318) {
        Long oldValue = this.anexoCq13C1318;
        this.anexoCq13C1318 = anexoCq13C1318;
        this.firePropertyChange("anexoCq13C1318", oldValue, this.anexoCq13C1318);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq13C1301);
        result = HashCodeUtil.hash(result, this.anexoCq13C1302);
        result = HashCodeUtil.hash(result, this.anexoCq13C1303);
        result = HashCodeUtil.hash(result, this.anexoCq13C1304);
        result = HashCodeUtil.hash(result, this.anexoCq13C1305);
        result = HashCodeUtil.hash(result, this.anexoCq13C1306);
        result = HashCodeUtil.hash(result, this.anexoCq13C1307);
        result = HashCodeUtil.hash(result, this.anexoCq13C1308);
        result = HashCodeUtil.hash(result, this.anexoCq13C1309);
        result = HashCodeUtil.hash(result, this.anexoCq13C1310);
        result = HashCodeUtil.hash(result, this.anexoCq13C1311);
        result = HashCodeUtil.hash(result, this.anexoCq13C1312);
        result = HashCodeUtil.hash(result, this.anexoCq13C1313);
        result = HashCodeUtil.hash(result, this.anexoCq13C1314);
        result = HashCodeUtil.hash(result, this.anexoCq13C1315);
        result = HashCodeUtil.hash(result, this.anexoCq13C1316);
        result = HashCodeUtil.hash(result, this.anexoCq13C1317);
        result = HashCodeUtil.hash(result, this.anexoCq13C1318);
        return result;
    }
}

