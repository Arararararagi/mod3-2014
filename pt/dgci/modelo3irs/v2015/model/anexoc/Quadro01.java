/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01Base;

public class Quadro01
extends Quadro01Base {
    public boolean isAnexoCq01B1Selected() {
        return this.getAnexoCq01B1() == null ? false : this.getAnexoCq01B1();
    }

    public boolean isAnexoCq01B2Selected() {
        return this.getAnexoCq01B2() == null ? false : this.getAnexoCq01B2();
    }
}

