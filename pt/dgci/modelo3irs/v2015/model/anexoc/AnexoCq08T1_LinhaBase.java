/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoCq08T1_LinhaBase
extends Model {
    public static final String NIF_LINK = "aAnexoC.qQuadro08.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;
    public static final String VALOR_LINK = "aAnexoC.qQuadro08.fvalor";
    public static final String VALOR = "valor";
    private Long valor;

    public AnexoCq08T1_LinhaBase(Long nIF, Long valor) {
        this.nIF = nIF;
        this.valor = valor;
    }

    public AnexoCq08T1_LinhaBase() {
        this.nIF = null;
        this.valor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValor() {
        return this.valor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValor(Long valor) {
        Long oldValue = this.valor;
        this.valor = valor;
        this.firePropertyChange("valor", oldValue, this.valor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.valor);
        return result;
    }

    public static enum Property {
        NIF("nIF", 2),
        VALOR("valor", 3);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

