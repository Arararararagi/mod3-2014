/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro11Base
extends QuadroModel {
    public static final String QUADRO11_LINK = "aAnexoC.qQuadro11";
    public static final String ANEXOCQ11C1101_LINK = "aAnexoC.qQuadro11.fanexoCq11C1101";
    public static final String ANEXOCQ11C1101 = "anexoCq11C1101";
    private Long anexoCq11C1101;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq11C1101() {
        return this.anexoCq11C1101;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq11C1101(Long anexoCq11C1101) {
        Long oldValue = this.anexoCq11C1101;
        this.anexoCq11C1101 = anexoCq11C1101;
        this.firePropertyChange("anexoCq11C1101", oldValue, this.anexoCq11C1101);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq11C1101);
        return result;
    }
}

