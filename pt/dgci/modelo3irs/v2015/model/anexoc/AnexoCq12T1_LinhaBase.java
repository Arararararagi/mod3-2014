/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoCq12T1_LinhaBase
extends Model {
    public static final String NIPCDASENTIDADES_LINK = "aAnexoC.qQuadro12.fnIPCdasEntidades";
    public static final String NIPCDASENTIDADES = "nIPCdasEntidades";
    private Long nIPCdasEntidades;
    public static final String SUBSIDIODETINADOAEXPLORACAO_LINK = "aAnexoC.qQuadro12.fsubsidioDetinadoaExploracao";
    public static final String SUBSIDIODETINADOAEXPLORACAO = "subsidioDetinadoaExploracao";
    private Long subsidioDetinadoaExploracao;
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON_LINK = "aAnexoC.qQuadro12.fsubsidiosNaoDestinadosExploracaoN";
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON = "subsidiosNaoDestinadosExploracaoN";
    private Long subsidiosNaoDestinadosExploracaoN;
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON1_LINK = "aAnexoC.qQuadro12.fsubsidiosNaoDestinadosExploracaoN1";
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON1 = "subsidiosNaoDestinadosExploracaoN1";
    private Long subsidiosNaoDestinadosExploracaoN1;
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON2_LINK = "aAnexoC.qQuadro12.fsubsidiosNaoDestinadosExploracaoN2";
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON2 = "subsidiosNaoDestinadosExploracaoN2";
    private Long subsidiosNaoDestinadosExploracaoN2;
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON3_LINK = "aAnexoC.qQuadro12.fsubsidiosNaoDestinadosExploracaoN3";
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON3 = "subsidiosNaoDestinadosExploracaoN3";
    private Long subsidiosNaoDestinadosExploracaoN3;
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON4_LINK = "aAnexoC.qQuadro12.fsubsidiosNaoDestinadosExploracaoN4";
    public static final String SUBSIDIOSNAODESTINADOSEXPLORACAON4 = "subsidiosNaoDestinadosExploracaoN4";
    private Long subsidiosNaoDestinadosExploracaoN4;

    public AnexoCq12T1_LinhaBase(Long nIPCdasEntidades, Long subsidioDetinadoaExploracao, Long subsidiosNaoDestinadosExploracaoN, Long subsidiosNaoDestinadosExploracaoN1, Long subsidiosNaoDestinadosExploracaoN2, Long subsidiosNaoDestinadosExploracaoN3, Long subsidiosNaoDestinadosExploracaoN4) {
        this.nIPCdasEntidades = nIPCdasEntidades;
        this.subsidioDetinadoaExploracao = subsidioDetinadoaExploracao;
        this.subsidiosNaoDestinadosExploracaoN = subsidiosNaoDestinadosExploracaoN;
        this.subsidiosNaoDestinadosExploracaoN1 = subsidiosNaoDestinadosExploracaoN1;
        this.subsidiosNaoDestinadosExploracaoN2 = subsidiosNaoDestinadosExploracaoN2;
        this.subsidiosNaoDestinadosExploracaoN3 = subsidiosNaoDestinadosExploracaoN3;
        this.subsidiosNaoDestinadosExploracaoN4 = subsidiosNaoDestinadosExploracaoN4;
    }

    public AnexoCq12T1_LinhaBase() {
        this.nIPCdasEntidades = null;
        this.subsidioDetinadoaExploracao = null;
        this.subsidiosNaoDestinadosExploracaoN = null;
        this.subsidiosNaoDestinadosExploracaoN1 = null;
        this.subsidiosNaoDestinadosExploracaoN2 = null;
        this.subsidiosNaoDestinadosExploracaoN3 = null;
        this.subsidiosNaoDestinadosExploracaoN4 = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIPCdasEntidades() {
        return this.nIPCdasEntidades;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIPCdasEntidades(Long nIPCdasEntidades) {
        Long oldValue = this.nIPCdasEntidades;
        this.nIPCdasEntidades = nIPCdasEntidades;
        this.firePropertyChange("nIPCdasEntidades", oldValue, this.nIPCdasEntidades);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getSubsidioDetinadoaExploracao() {
        return this.subsidioDetinadoaExploracao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setSubsidioDetinadoaExploracao(Long subsidioDetinadoaExploracao) {
        Long oldValue = this.subsidioDetinadoaExploracao;
        this.subsidioDetinadoaExploracao = subsidioDetinadoaExploracao;
        this.firePropertyChange("subsidioDetinadoaExploracao", oldValue, this.subsidioDetinadoaExploracao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getSubsidiosNaoDestinadosExploracaoN() {
        return this.subsidiosNaoDestinadosExploracaoN;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setSubsidiosNaoDestinadosExploracaoN(Long subsidiosNaoDestinadosExploracaoN) {
        Long oldValue = this.subsidiosNaoDestinadosExploracaoN;
        this.subsidiosNaoDestinadosExploracaoN = subsidiosNaoDestinadosExploracaoN;
        this.firePropertyChange("subsidiosNaoDestinadosExploracaoN", oldValue, this.subsidiosNaoDestinadosExploracaoN);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getSubsidiosNaoDestinadosExploracaoN1() {
        return this.subsidiosNaoDestinadosExploracaoN1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setSubsidiosNaoDestinadosExploracaoN1(Long subsidiosNaoDestinadosExploracaoN1) {
        Long oldValue = this.subsidiosNaoDestinadosExploracaoN1;
        this.subsidiosNaoDestinadosExploracaoN1 = subsidiosNaoDestinadosExploracaoN1;
        this.firePropertyChange("subsidiosNaoDestinadosExploracaoN1", oldValue, this.subsidiosNaoDestinadosExploracaoN1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getSubsidiosNaoDestinadosExploracaoN2() {
        return this.subsidiosNaoDestinadosExploracaoN2;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setSubsidiosNaoDestinadosExploracaoN2(Long subsidiosNaoDestinadosExploracaoN2) {
        Long oldValue = this.subsidiosNaoDestinadosExploracaoN2;
        this.subsidiosNaoDestinadosExploracaoN2 = subsidiosNaoDestinadosExploracaoN2;
        this.firePropertyChange("subsidiosNaoDestinadosExploracaoN2", oldValue, this.subsidiosNaoDestinadosExploracaoN2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getSubsidiosNaoDestinadosExploracaoN3() {
        return this.subsidiosNaoDestinadosExploracaoN3;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setSubsidiosNaoDestinadosExploracaoN3(Long subsidiosNaoDestinadosExploracaoN3) {
        Long oldValue = this.subsidiosNaoDestinadosExploracaoN3;
        this.subsidiosNaoDestinadosExploracaoN3 = subsidiosNaoDestinadosExploracaoN3;
        this.firePropertyChange("subsidiosNaoDestinadosExploracaoN3", oldValue, this.subsidiosNaoDestinadosExploracaoN3);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getSubsidiosNaoDestinadosExploracaoN4() {
        return this.subsidiosNaoDestinadosExploracaoN4;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setSubsidiosNaoDestinadosExploracaoN4(Long subsidiosNaoDestinadosExploracaoN4) {
        Long oldValue = this.subsidiosNaoDestinadosExploracaoN4;
        this.subsidiosNaoDestinadosExploracaoN4 = subsidiosNaoDestinadosExploracaoN4;
        this.firePropertyChange("subsidiosNaoDestinadosExploracaoN4", oldValue, this.subsidiosNaoDestinadosExploracaoN4);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIPCdasEntidades);
        result = HashCodeUtil.hash(result, this.subsidioDetinadoaExploracao);
        result = HashCodeUtil.hash(result, this.subsidiosNaoDestinadosExploracaoN);
        result = HashCodeUtil.hash(result, this.subsidiosNaoDestinadosExploracaoN1);
        result = HashCodeUtil.hash(result, this.subsidiosNaoDestinadosExploracaoN2);
        result = HashCodeUtil.hash(result, this.subsidiosNaoDestinadosExploracaoN3);
        result = HashCodeUtil.hash(result, this.subsidiosNaoDestinadosExploracaoN4);
        return result;
    }

    public static enum Property {
        NIPCDASENTIDADES("nIPCdasEntidades", 2),
        SUBSIDIODETINADOAEXPLORACAO("subsidioDetinadoaExploracao", 3),
        SUBSIDIOSNAODESTINADOSEXPLORACAON("subsidiosNaoDestinadosExploracaoN", 4),
        SUBSIDIOSNAODESTINADOSEXPLORACAON1("subsidiosNaoDestinadosExploracaoN1", 5),
        SUBSIDIOSNAODESTINADOSEXPLORACAON2("subsidiosNaoDestinadosExploracaoN2", 6),
        SUBSIDIOSNAODESTINADOSEXPLORACAON3("subsidiosNaoDestinadosExploracaoN3", 7),
        SUBSIDIOSNAODESTINADOSEXPLORACAON4("subsidiosNaoDestinadosExploracaoN4", 8);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

