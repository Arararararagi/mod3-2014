/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro10;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro11;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro13;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro14;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro15;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro16;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro17;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.util.HashCodeUtil;

@Min(value=0)
@Max(value=9)
public class AnexoCModelBase
extends AnexoModel {
    public static final String ANEXOC_LINK = "aAnexoC";

    public AnexoCModelBase(FormKey key, boolean addQuadrosToModel) {
        super(key);
        if (addQuadrosToModel) {
            this.addQuadro(new Quadro01());
            this.addQuadro(new Quadro02());
            this.addQuadro(new Quadro03());
            this.addQuadro(new Quadro04());
            this.addQuadro(new Quadro05());
            this.addQuadro(new Quadro06());
            this.addQuadro(new Quadro07());
            this.addQuadro(new Quadro08());
            this.addQuadro(new Quadro09());
            this.addQuadro(new Quadro10());
            this.addQuadro(new Quadro11());
            this.addQuadro(new Quadro12());
            this.addQuadro(new Quadro13());
            this.addQuadro(new Quadro14());
            this.addQuadro(new Quadro15());
            this.addQuadro(new Quadro16());
            this.addQuadro(new Quadro17());
        }
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro01.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro02.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro03.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro04.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro05.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro06.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro07.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro08.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro09.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro10.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro11.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro12.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro13.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro14.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro15.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro16.class.getSimpleName()));
        result = HashCodeUtil.hash(result, this.getQuadro(Quadro17.class.getSimpleName()));
        return result;
    }
}

