/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro17Base
extends QuadroModel {
    public static final String QUADRO17_LINK = "aAnexoC.qQuadro17";
    public static final String ANEXOCQ17C1701_LINK = "aAnexoC.qQuadro17.fanexoCq17C1701";
    public static final String ANEXOCQ17C1701 = "anexoCq17C1701";
    private Long anexoCq17C1701;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoCq17C1701() {
        return this.anexoCq17C1701;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq17C1701(Long anexoCq17C1701) {
        Long oldValue = this.anexoCq17C1701;
        this.anexoCq17C1701 = anexoCq17C1701;
        this.firePropertyChange("anexoCq17C1701", oldValue, this.anexoCq17C1701);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq17C1701);
        return result;
    }
}

