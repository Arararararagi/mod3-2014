/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoC.qQuadro05";
    public static final String ANEXOCQ05C501_LINK = "aAnexoC.qQuadro05.fanexoCq05C501";
    public static final String ANEXOCQ05C501 = "anexoCq05C501";
    private Long anexoCq05C501;
    public static final String ANEXOCQ05C502_LINK = "aAnexoC.qQuadro05.fanexoCq05C502";
    public static final String ANEXOCQ05C502 = "anexoCq05C502";
    private Long anexoCq05C502;
    public static final String ANEXOCQ05C503_LINK = "aAnexoC.qQuadro05.fanexoCq05C503";
    public static final String ANEXOCQ05C503 = "anexoCq05C503";
    private Long anexoCq05C503;
    public static final String ANEXOCQ05C504_LINK = "aAnexoC.qQuadro05.fanexoCq05C504";
    public static final String ANEXOCQ05C504 = "anexoCq05C504";
    private Long anexoCq05C504;
    public static final String ANEXOCQ05C505_LINK = "aAnexoC.qQuadro05.fanexoCq05C505";
    public static final String ANEXOCQ05C505 = "anexoCq05C505";
    private Long anexoCq05C505;
    public static final String ANEXOCQ05C506_LINK = "aAnexoC.qQuadro05.fanexoCq05C506";
    public static final String ANEXOCQ05C506 = "anexoCq05C506";
    private Long anexoCq05C506;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq05C501() {
        return this.anexoCq05C501;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq05C501(Long anexoCq05C501) {
        Long oldValue = this.anexoCq05C501;
        this.anexoCq05C501 = anexoCq05C501;
        this.firePropertyChange("anexoCq05C501", oldValue, this.anexoCq05C501);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq05C502() {
        return this.anexoCq05C502;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq05C502(Long anexoCq05C502) {
        Long oldValue = this.anexoCq05C502;
        this.anexoCq05C502 = anexoCq05C502;
        this.firePropertyChange("anexoCq05C502", oldValue, this.anexoCq05C502);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq05C503() {
        return this.anexoCq05C503;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq05C503(Long anexoCq05C503) {
        Long oldValue = this.anexoCq05C503;
        this.anexoCq05C503 = anexoCq05C503;
        this.firePropertyChange("anexoCq05C503", oldValue, this.anexoCq05C503);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq05C504() {
        return this.anexoCq05C504;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq05C504(Long anexoCq05C504) {
        Long oldValue = this.anexoCq05C504;
        this.anexoCq05C504 = anexoCq05C504;
        this.firePropertyChange("anexoCq05C504", oldValue, this.anexoCq05C504);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq05C505() {
        return this.anexoCq05C505;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq05C505(Long anexoCq05C505) {
        Long oldValue = this.anexoCq05C505;
        this.anexoCq05C505 = anexoCq05C505;
        this.firePropertyChange("anexoCq05C505", oldValue, this.anexoCq05C505);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq05C506() {
        return this.anexoCq05C506;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq05C506(Long anexoCq05C506) {
        Long oldValue = this.anexoCq05C506;
        this.anexoCq05C506 = anexoCq05C506;
        this.firePropertyChange("anexoCq05C506", oldValue, this.anexoCq05C506);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq05C501);
        result = HashCodeUtil.hash(result, this.anexoCq05C502);
        result = HashCodeUtil.hash(result, this.anexoCq05C503);
        result = HashCodeUtil.hash(result, this.anexoCq05C504);
        result = HashCodeUtil.hash(result, this.anexoCq05C505);
        result = HashCodeUtil.hash(result, this.anexoCq05C506);
        return result;
    }
}

