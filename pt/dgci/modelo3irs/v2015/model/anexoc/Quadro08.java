/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro08Base;

public class Quadro08
extends Quadro08Base {
    public Long getSomaRetencoesTabela() {
        EventList<AnexoCq08T1_Linha> linhas = this.getAnexoCq08T1();
        long soma = 0;
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoCq08T1_Linha linhaSomar = linhas.get(i);
            if (linhaSomar.getValor() == null) continue;
            soma+=linhaSomar.getValor().longValue();
        }
        return soma;
    }
}

