/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;
import pt.opensoft.util.Date;

public class Quadro16Base
extends QuadroModel {
    public static final String QUADRO16_LINK = "aAnexoC.qQuadro16";
    public static final String ANEXOCQ16B1OP1_LINK = "aAnexoC.qQuadro16.fanexoCq16B1OP1";
    public static final String ANEXOCQ16B1OP1_VALUE = "1";
    public static final String ANEXOCQ16B1OP2_LINK = "aAnexoC.qQuadro16.fanexoCq16B1OP2";
    public static final String ANEXOCQ16B1OP2_VALUE = "2";
    public static final String ANEXOCQ16B1 = "anexoCq16B1";
    private String anexoCq16B1;
    public static final String ANEXOCQ16C3_LINK = "aAnexoC.qQuadro16.fanexoCq16C3";
    public static final String ANEXOCQ16C3 = "anexoCq16C3";
    private Date anexoCq16C3;
    public static final String ANEXOCQ16B4_LINK = "aAnexoC.qQuadro16.fanexoCq16B4";
    public static final String ANEXOCQ16B4 = "anexoCq16B4";
    private Boolean anexoCq16B4;

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoCq16B1() {
        return this.anexoCq16B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq16B1(String anexoCq16B1) {
        String oldValue = this.anexoCq16B1;
        this.anexoCq16B1 = anexoCq16B1;
        this.firePropertyChange("anexoCq16B1", oldValue, this.anexoCq16B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Date getAnexoCq16C3() {
        return this.anexoCq16C3;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq16C3(Date anexoCq16C3) {
        Date oldValue = this.anexoCq16C3;
        this.anexoCq16C3 = anexoCq16C3;
        this.firePropertyChange("anexoCq16C3", oldValue, this.anexoCq16C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoCq16B4() {
        return this.anexoCq16B4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoCq16B4(Boolean anexoCq16B4) {
        Boolean oldValue = this.anexoCq16B4;
        this.anexoCq16B4 = anexoCq16B4;
        this.firePropertyChange("anexoCq16B4", oldValue, this.anexoCq16B4);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq16B1);
        result = HashCodeUtil.hash(result, this.anexoCq16C3);
        result = HashCodeUtil.hash(result, this.anexoCq16B4);
        return result;
    }
}

