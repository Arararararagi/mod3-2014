/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq08T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro08Base
extends QuadroModel {
    public static final String QUADRO08_LINK = "aAnexoC.qQuadro08";
    public static final String ANEXOCQ08C801_LINK = "aAnexoC.qQuadro08.fanexoCq08C801";
    public static final String ANEXOCQ08C801 = "anexoCq08C801";
    private Long anexoCq08C801;
    public static final String ANEXOCQ08C802_LINK = "aAnexoC.qQuadro08.fanexoCq08C802";
    public static final String ANEXOCQ08C802 = "anexoCq08C802";
    private Long anexoCq08C802;
    public static final String ANEXOCQ08C803_LINK = "aAnexoC.qQuadro08.fanexoCq08C803";
    public static final String ANEXOCQ08C803 = "anexoCq08C803";
    private Long anexoCq08C803;
    public static final String ANEXOCQ08C804_LINK = "aAnexoC.qQuadro08.fanexoCq08C804";
    public static final String ANEXOCQ08C804 = "anexoCq08C804";
    private Long anexoCq08C804;
    public static final String ANEXOCQ08C805_LINK = "aAnexoC.qQuadro08.fanexoCq08C805";
    public static final String ANEXOCQ08C805 = "anexoCq08C805";
    private Long anexoCq08C805;
    public static final String ANEXOCQ08T1_LINK = "aAnexoC.qQuadro08.tanexoCq08T1";
    private EventList<AnexoCq08T1_Linha> anexoCq08T1 = new BasicEventList<AnexoCq08T1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq08C801() {
        return this.anexoCq08C801;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq08C801(Long anexoCq08C801) {
        Long oldValue = this.anexoCq08C801;
        this.anexoCq08C801 = anexoCq08C801;
        this.firePropertyChange("anexoCq08C801", oldValue, this.anexoCq08C801);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq08C802() {
        return this.anexoCq08C802;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq08C802(Long anexoCq08C802) {
        Long oldValue = this.anexoCq08C802;
        this.anexoCq08C802 = anexoCq08C802;
        this.firePropertyChange("anexoCq08C802", oldValue, this.anexoCq08C802);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq08C803() {
        return this.anexoCq08C803;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq08C803(Long anexoCq08C803) {
        Long oldValue = this.anexoCq08C803;
        this.anexoCq08C803 = anexoCq08C803;
        this.firePropertyChange("anexoCq08C803", oldValue, this.anexoCq08C803);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq08C804() {
        return this.anexoCq08C804;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq08C804(Long anexoCq08C804) {
        Long oldValue = this.anexoCq08C804;
        this.anexoCq08C804 = anexoCq08C804;
        this.firePropertyChange("anexoCq08C804", oldValue, this.anexoCq08C804);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq08C805() {
        return this.anexoCq08C805;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq08C805(Long anexoCq08C805) {
        Long oldValue = this.anexoCq08C805;
        this.anexoCq08C805 = anexoCq08C805;
        this.firePropertyChange("anexoCq08C805", oldValue, this.anexoCq08C805);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoCq08T1_Linha> getAnexoCq08T1() {
        return this.anexoCq08T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoCq08T1(EventList<AnexoCq08T1_Linha> anexoCq08T1) {
        this.anexoCq08T1 = anexoCq08T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq08C801);
        result = HashCodeUtil.hash(result, this.anexoCq08C802);
        result = HashCodeUtil.hash(result, this.anexoCq08C803);
        result = HashCodeUtil.hash(result, this.anexoCq08C804);
        result = HashCodeUtil.hash(result, this.anexoCq08C805);
        result = HashCodeUtil.hash(result, this.anexoCq08T1);
        return result;
    }
}

