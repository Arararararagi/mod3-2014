/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro12Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro12
extends Quadro12Base {
    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1201()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1203()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1205()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1210()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1211()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1212()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1202()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1204()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1206()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1207()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1208()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoCq12C1209()) && this.getAnexoCq12T1().isEmpty();
    }

    public Long getSomaValoresT1() {
        EventList<AnexoCq12T1_Linha> linhas = this.getAnexoCq12T1();
        long soma = 0;
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoCq12T1_Linha linhaSomar = linhas.get(i);
            if (linhaSomar.getSubsidioDetinadoaExploracao() == null) continue;
            soma+=linhaSomar.getSubsidioDetinadoaExploracao().longValue();
        }
        return soma;
    }
}

