/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_LinhaBase;

public class AnexoCq12T1_Linha
extends AnexoCq12T1_LinhaBase {
    public AnexoCq12T1_Linha() {
    }

    public AnexoCq12T1_Linha(Long nIPCdasEntidades, Long subsidioDestinadoaExploracao, Long subsidiosNaoDestinadosExploracaoN, Long subsidiosNaoDestinadosExploracaoN1, Long subsidiosNaoDestinadosExploracaoN2, Long subsidiosNaoDestinadosExploracaoN3, Long subsidiosNaoDestinadosExploracaoN4) {
        super(nIPCdasEntidades, subsidioDestinadoaExploracao, subsidiosNaoDestinadosExploracaoN, subsidiosNaoDestinadosExploracaoN1, subsidiosNaoDestinadosExploracaoN2, subsidiosNaoDestinadosExploracaoN3, subsidiosNaoDestinadosExploracaoN4);
    }
}

