/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq12T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro12Base
extends QuadroModel {
    public static final String QUADRO12_LINK = "aAnexoC.qQuadro12";
    public static final String ANEXOCQ12C1201_LINK = "aAnexoC.qQuadro12.fanexoCq12C1201";
    public static final String ANEXOCQ12C1201 = "anexoCq12C1201";
    private Long anexoCq12C1201;
    public static final String ANEXOCQ12C1210_LINK = "aAnexoC.qQuadro12.fanexoCq12C1210";
    public static final String ANEXOCQ12C1210 = "anexoCq12C1210";
    private Long anexoCq12C1210;
    public static final String ANEXOCQ12C1202_LINK = "aAnexoC.qQuadro12.fanexoCq12C1202";
    public static final String ANEXOCQ12C1202 = "anexoCq12C1202";
    private Long anexoCq12C1202;
    public static final String ANEXOCQ12C1203_LINK = "aAnexoC.qQuadro12.fanexoCq12C1203";
    public static final String ANEXOCQ12C1203 = "anexoCq12C1203";
    private Long anexoCq12C1203;
    public static final String ANEXOCQ12C1211_LINK = "aAnexoC.qQuadro12.fanexoCq12C1211";
    public static final String ANEXOCQ12C1211 = "anexoCq12C1211";
    private Long anexoCq12C1211;
    public static final String ANEXOCQ12C1204_LINK = "aAnexoC.qQuadro12.fanexoCq12C1204";
    public static final String ANEXOCQ12C1204 = "anexoCq12C1204";
    private Long anexoCq12C1204;
    public static final String ANEXOCQ12C1205_LINK = "aAnexoC.qQuadro12.fanexoCq12C1205";
    public static final String ANEXOCQ12C1205 = "anexoCq12C1205";
    private Long anexoCq12C1205;
    public static final String ANEXOCQ12C1212_LINK = "aAnexoC.qQuadro12.fanexoCq12C1212";
    public static final String ANEXOCQ12C1212 = "anexoCq12C1212";
    private Long anexoCq12C1212;
    public static final String ANEXOCQ12C1206_LINK = "aAnexoC.qQuadro12.fanexoCq12C1206";
    public static final String ANEXOCQ12C1206 = "anexoCq12C1206";
    private Long anexoCq12C1206;
    public static final String ANEXOCQ12C1207_LINK = "aAnexoC.qQuadro12.fanexoCq12C1207";
    public static final String ANEXOCQ12C1207 = "anexoCq12C1207";
    private Long anexoCq12C1207;
    public static final String ANEXOCQ12C1208_LINK = "aAnexoC.qQuadro12.fanexoCq12C1208";
    public static final String ANEXOCQ12C1208 = "anexoCq12C1208";
    private Long anexoCq12C1208;
    public static final String ANEXOCQ12C1209_LINK = "aAnexoC.qQuadro12.fanexoCq12C1209";
    public static final String ANEXOCQ12C1209 = "anexoCq12C1209";
    private Long anexoCq12C1209;
    public static final String ANEXOCQ12T1_LINK = "aAnexoC.qQuadro12.tanexoCq12T1";
    private EventList<AnexoCq12T1_Linha> anexoCq12T1 = new BasicEventList<AnexoCq12T1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1201() {
        return this.anexoCq12C1201;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1201(Long anexoCq12C1201) {
        Long oldValue = this.anexoCq12C1201;
        this.anexoCq12C1201 = anexoCq12C1201;
        this.firePropertyChange("anexoCq12C1201", oldValue, this.anexoCq12C1201);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1210() {
        return this.anexoCq12C1210;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1210(Long anexoCq12C1210) {
        Long oldValue = this.anexoCq12C1210;
        this.anexoCq12C1210 = anexoCq12C1210;
        this.firePropertyChange("anexoCq12C1210", oldValue, this.anexoCq12C1210);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1202() {
        return this.anexoCq12C1202;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1202(Long anexoCq12C1202) {
        Long oldValue = this.anexoCq12C1202;
        this.anexoCq12C1202 = anexoCq12C1202;
        this.firePropertyChange("anexoCq12C1202", oldValue, this.anexoCq12C1202);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1203() {
        return this.anexoCq12C1203;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1203(Long anexoCq12C1203) {
        Long oldValue = this.anexoCq12C1203;
        this.anexoCq12C1203 = anexoCq12C1203;
        this.firePropertyChange("anexoCq12C1203", oldValue, this.anexoCq12C1203);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1211() {
        return this.anexoCq12C1211;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1211(Long anexoCq12C1211) {
        Long oldValue = this.anexoCq12C1211;
        this.anexoCq12C1211 = anexoCq12C1211;
        this.firePropertyChange("anexoCq12C1211", oldValue, this.anexoCq12C1211);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1204() {
        return this.anexoCq12C1204;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1204(Long anexoCq12C1204) {
        Long oldValue = this.anexoCq12C1204;
        this.anexoCq12C1204 = anexoCq12C1204;
        this.firePropertyChange("anexoCq12C1204", oldValue, this.anexoCq12C1204);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1205() {
        return this.anexoCq12C1205;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1205(Long anexoCq12C1205) {
        Long oldValue = this.anexoCq12C1205;
        this.anexoCq12C1205 = anexoCq12C1205;
        this.firePropertyChange("anexoCq12C1205", oldValue, this.anexoCq12C1205);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1212() {
        return this.anexoCq12C1212;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1212(Long anexoCq12C1212) {
        Long oldValue = this.anexoCq12C1212;
        this.anexoCq12C1212 = anexoCq12C1212;
        this.firePropertyChange("anexoCq12C1212", oldValue, this.anexoCq12C1212);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1206() {
        return this.anexoCq12C1206;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1206(Long anexoCq12C1206) {
        Long oldValue = this.anexoCq12C1206;
        this.anexoCq12C1206 = anexoCq12C1206;
        this.firePropertyChange("anexoCq12C1206", oldValue, this.anexoCq12C1206);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1207() {
        return this.anexoCq12C1207;
    }

    public void setAnexoCq12C1207Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq12C1207")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq12C1207(newValue+=(this.getAnexoCq12C1201() == null ? 0 : this.getAnexoCq12C1201()) + (this.getAnexoCq12C1210() == null ? 0 : this.getAnexoCq12C1210()) + (this.getAnexoCq12C1202() == null ? 0 : this.getAnexoCq12C1202()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1207(Long anexoCq12C1207) {
        Long oldValue = this.anexoCq12C1207;
        this.anexoCq12C1207 = anexoCq12C1207;
        this.firePropertyChange("anexoCq12C1207", oldValue, this.anexoCq12C1207);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1208() {
        return this.anexoCq12C1208;
    }

    public void setAnexoCq12C1208Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq12C1208")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq12C1208(newValue+=(this.getAnexoCq12C1203() == null ? 0 : this.getAnexoCq12C1203()) + (this.getAnexoCq12C1211() == null ? 0 : this.getAnexoCq12C1211()) + (this.getAnexoCq12C1204() == null ? 0 : this.getAnexoCq12C1204()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1208(Long anexoCq12C1208) {
        Long oldValue = this.anexoCq12C1208;
        this.anexoCq12C1208 = anexoCq12C1208;
        this.firePropertyChange("anexoCq12C1208", oldValue, this.anexoCq12C1208);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq12C1209() {
        return this.anexoCq12C1209;
    }

    public void setAnexoCq12C1209Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq12C1209")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq12C1209(newValue+=(this.getAnexoCq12C1205() == null ? 0 : this.getAnexoCq12C1205()) + (this.getAnexoCq12C1212() == null ? 0 : this.getAnexoCq12C1212()) + (this.getAnexoCq12C1206() == null ? 0 : this.getAnexoCq12C1206()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq12C1209(Long anexoCq12C1209) {
        Long oldValue = this.anexoCq12C1209;
        this.anexoCq12C1209 = anexoCq12C1209;
        this.firePropertyChange("anexoCq12C1209", oldValue, this.anexoCq12C1209);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoCq12T1_Linha> getAnexoCq12T1() {
        return this.anexoCq12T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoCq12T1(EventList<AnexoCq12T1_Linha> anexoCq12T1) {
        this.anexoCq12T1 = anexoCq12T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq12C1201);
        result = HashCodeUtil.hash(result, this.anexoCq12C1210);
        result = HashCodeUtil.hash(result, this.anexoCq12C1202);
        result = HashCodeUtil.hash(result, this.anexoCq12C1203);
        result = HashCodeUtil.hash(result, this.anexoCq12C1211);
        result = HashCodeUtil.hash(result, this.anexoCq12C1204);
        result = HashCodeUtil.hash(result, this.anexoCq12C1205);
        result = HashCodeUtil.hash(result, this.anexoCq12C1212);
        result = HashCodeUtil.hash(result, this.anexoCq12C1206);
        result = HashCodeUtil.hash(result, this.anexoCq12C1207);
        result = HashCodeUtil.hash(result, this.anexoCq12C1208);
        result = HashCodeUtil.hash(result, this.anexoCq12C1209);
        result = HashCodeUtil.hash(result, this.anexoCq12T1);
        return result;
    }
}

