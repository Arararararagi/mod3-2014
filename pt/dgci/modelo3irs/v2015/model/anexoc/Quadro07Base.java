/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoC.qQuadro07";
    public static final String ANEXOCQ07C701_LINK = "aAnexoC.qQuadro07.fanexoCq07C701";
    public static final String ANEXOCQ07C701 = "anexoCq07C701";
    private Long anexoCq07C701;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq07C701() {
        return this.anexoCq07C701;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq07C701(Long anexoCq07C701) {
        Long oldValue = this.anexoCq07C701;
        this.anexoCq07C701 = anexoCq07C701;
        this.firePropertyChange("anexoCq07C701", oldValue, this.anexoCq07C701);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq07C701);
        return result;
    }
}

