/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.Quadro03Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro03
extends Quadro03Base {
    public Long getNifTitular() {
        if (this.getAnexoCq03C06() != null && Modelo3IRSValidatorUtil.isNIFValid(this.getAnexoCq03C06())) {
            return this.getAnexoCq03C06();
        }
        if (this.getAnexoCq03C07() != null && Modelo3IRSValidatorUtil.isNIFValid(this.getAnexoCq03C07())) {
            return this.getAnexoCq03C07();
        }
        return new Long(0);
    }
}

