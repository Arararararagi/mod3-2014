/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCq14T1_LinhaBase;

public class AnexoCq14T1_Linha
extends AnexoCq14T1_LinhaBase {
    public AnexoCq14T1_Linha() {
    }

    public AnexoCq14T1_Linha(String freguesia, String tipoPredio, Long artigo, String fraccao, Long valorVenda, Long campoQ4, String art139CIRC) {
        super(freguesia, tipoPredio, artigo, fraccao, valorVenda, campoQ4, art139CIRC);
    }

    public static String getLink(int numLinha) {
        return "aAnexoC.qQuadro14.tanexoCq14T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoCq14T1_LinhaBase.Property column) {
        return AnexoCq14T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

