/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro15Base
extends QuadroModel {
    public static final String QUADRO15_LINK = "aAnexoC.qQuadro15";
    public static final String ANEXOCQ15C1501_LINK = "aAnexoC.qQuadro15.fanexoCq15C1501";
    public static final String ANEXOCQ15C1501 = "anexoCq15C1501";
    private Long anexoCq15C1501;
    public static final String ANEXOCQ15C1502_LINK = "aAnexoC.qQuadro15.fanexoCq15C1502";
    public static final String ANEXOCQ15C1502 = "anexoCq15C1502";
    private Long anexoCq15C1502;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq15C1501() {
        return this.anexoCq15C1501;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq15C1501(Long anexoCq15C1501) {
        Long oldValue = this.anexoCq15C1501;
        this.anexoCq15C1501 = anexoCq15C1501;
        this.firePropertyChange("anexoCq15C1501", oldValue, this.anexoCq15C1501);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq15C1502() {
        return this.anexoCq15C1502;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq15C1502(Long anexoCq15C1502) {
        Long oldValue = this.anexoCq15C1502;
        this.anexoCq15C1502 = anexoCq15C1502;
        this.firePropertyChange("anexoCq15C1502", oldValue, this.anexoCq15C1502);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq15C1501);
        result = HashCodeUtil.hash(result, this.anexoCq15C1502);
        return result;
    }
}

