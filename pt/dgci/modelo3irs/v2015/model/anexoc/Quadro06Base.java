/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoc;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoC.qQuadro06";
    public static final String ANEXOCQ06C601_LINK = "aAnexoC.qQuadro06.fanexoCq06C601";
    public static final String ANEXOCQ06C601 = "anexoCq06C601";
    private Long anexoCq06C601;
    public static final String ANEXOCQ06C602_LINK = "aAnexoC.qQuadro06.fanexoCq06C602";
    public static final String ANEXOCQ06C602 = "anexoCq06C602";
    private Long anexoCq06C602;
    public static final String ANEXOCQ06C603_LINK = "aAnexoC.qQuadro06.fanexoCq06C603";
    public static final String ANEXOCQ06C603 = "anexoCq06C603";
    private Long anexoCq06C603;
    public static final String ANEXOCQ06C604_LINK = "aAnexoC.qQuadro06.fanexoCq06C604";
    public static final String ANEXOCQ06C604 = "anexoCq06C604";
    private Long anexoCq06C604;
    public static final String ANEXOCQ06C605_LINK = "aAnexoC.qQuadro06.fanexoCq06C605";
    public static final String ANEXOCQ06C605 = "anexoCq06C605";
    private Long anexoCq06C605;
    public static final String ANEXOCQ06C606_LINK = "aAnexoC.qQuadro06.fanexoCq06C606";
    public static final String ANEXOCQ06C606 = "anexoCq06C606";
    private Long anexoCq06C606;
    public static final String ANEXOCQ06C607_LINK = "aAnexoC.qQuadro06.fanexoCq06C607";
    public static final String ANEXOCQ06C607 = "anexoCq06C607";
    private Long anexoCq06C607;
    public static final String ANEXOCQ06C608_LINK = "aAnexoC.qQuadro06.fanexoCq06C608";
    public static final String ANEXOCQ06C608 = "anexoCq06C608";
    private Long anexoCq06C608;
    public static final String ANEXOCQ06C609_LINK = "aAnexoC.qQuadro06.fanexoCq06C609";
    public static final String ANEXOCQ06C609 = "anexoCq06C609";
    private Long anexoCq06C609;
    public static final String ANEXOCQ06C610_LINK = "aAnexoC.qQuadro06.fanexoCq06C610";
    public static final String ANEXOCQ06C610 = "anexoCq06C610";
    private Long anexoCq06C610;
    public static final String ANEXOCQ06C611_LINK = "aAnexoC.qQuadro06.fanexoCq06C611";
    public static final String ANEXOCQ06C611 = "anexoCq06C611";
    private Long anexoCq06C611;
    public static final String ANEXOCQ06C612_LINK = "aAnexoC.qQuadro06.fanexoCq06C612";
    public static final String ANEXOCQ06C612 = "anexoCq06C612";
    private Long anexoCq06C612;
    public static final String ANEXOCQ06C613_LINK = "aAnexoC.qQuadro06.fanexoCq06C613";
    public static final String ANEXOCQ06C613 = "anexoCq06C613";
    private Long anexoCq06C613;
    public static final String ANEXOCQ06C614_LINK = "aAnexoC.qQuadro06.fanexoCq06C614";
    public static final String ANEXOCQ06C614 = "anexoCq06C614";
    private Long anexoCq06C614;
    public static final String ANEXOCQ06C615_LINK = "aAnexoC.qQuadro06.fanexoCq06C615";
    public static final String ANEXOCQ06C615 = "anexoCq06C615";
    private Long anexoCq06C615;
    public static final String ANEXOCQ06C616_LINK = "aAnexoC.qQuadro06.fanexoCq06C616";
    public static final String ANEXOCQ06C616 = "anexoCq06C616";
    private Long anexoCq06C616;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C601() {
        return this.anexoCq06C601;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C601(Long anexoCq06C601) {
        Long oldValue = this.anexoCq06C601;
        this.anexoCq06C601 = anexoCq06C601;
        this.firePropertyChange("anexoCq06C601", oldValue, this.anexoCq06C601);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C602() {
        return this.anexoCq06C602;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C602(Long anexoCq06C602) {
        Long oldValue = this.anexoCq06C602;
        this.anexoCq06C602 = anexoCq06C602;
        this.firePropertyChange("anexoCq06C602", oldValue, this.anexoCq06C602);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C603() {
        return this.anexoCq06C603;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C603(Long anexoCq06C603) {
        Long oldValue = this.anexoCq06C603;
        this.anexoCq06C603 = anexoCq06C603;
        this.firePropertyChange("anexoCq06C603", oldValue, this.anexoCq06C603);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C604() {
        return this.anexoCq06C604;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C604(Long anexoCq06C604) {
        Long oldValue = this.anexoCq06C604;
        this.anexoCq06C604 = anexoCq06C604;
        this.firePropertyChange("anexoCq06C604", oldValue, this.anexoCq06C604);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C605() {
        return this.anexoCq06C605;
    }

    public void setAnexoCq06C605Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq06C605")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq06C605(newValue+=(this.getAnexoCq06C601() == null ? 0 : this.getAnexoCq06C601()) + (this.getAnexoCq06C602() == null ? 0 : this.getAnexoCq06C602()) + (this.getAnexoCq06C603() == null ? 0 : this.getAnexoCq06C603()) + (this.getAnexoCq06C604() == null ? 0 : this.getAnexoCq06C604()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C605(Long anexoCq06C605) {
        Long oldValue = this.anexoCq06C605;
        this.anexoCq06C605 = anexoCq06C605;
        this.firePropertyChange("anexoCq06C605", oldValue, this.anexoCq06C605);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C606() {
        return this.anexoCq06C606;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C606(Long anexoCq06C606) {
        Long oldValue = this.anexoCq06C606;
        this.anexoCq06C606 = anexoCq06C606;
        this.firePropertyChange("anexoCq06C606", oldValue, this.anexoCq06C606);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C607() {
        return this.anexoCq06C607;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C607(Long anexoCq06C607) {
        Long oldValue = this.anexoCq06C607;
        this.anexoCq06C607 = anexoCq06C607;
        this.firePropertyChange("anexoCq06C607", oldValue, this.anexoCq06C607);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C608() {
        return this.anexoCq06C608;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C608(Long anexoCq06C608) {
        Long oldValue = this.anexoCq06C608;
        this.anexoCq06C608 = anexoCq06C608;
        this.firePropertyChange("anexoCq06C608", oldValue, this.anexoCq06C608);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C609() {
        return this.anexoCq06C609;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C609(Long anexoCq06C609) {
        Long oldValue = this.anexoCq06C609;
        this.anexoCq06C609 = anexoCq06C609;
        this.firePropertyChange("anexoCq06C609", oldValue, this.anexoCq06C609);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C610() {
        return this.anexoCq06C610;
    }

    public void setAnexoCq06C610Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq06C610")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq06C610(newValue+=(this.getAnexoCq06C606() == null ? 0 : this.getAnexoCq06C606()) + (this.getAnexoCq06C607() == null ? 0 : this.getAnexoCq06C607()) + (this.getAnexoCq06C608() == null ? 0 : this.getAnexoCq06C608()) + (this.getAnexoCq06C609() == null ? 0 : this.getAnexoCq06C609()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C610(Long anexoCq06C610) {
        Long oldValue = this.anexoCq06C610;
        this.anexoCq06C610 = anexoCq06C610;
        this.firePropertyChange("anexoCq06C610", oldValue, this.anexoCq06C610);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C611() {
        return this.anexoCq06C611;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C611(Long anexoCq06C611) {
        Long oldValue = this.anexoCq06C611;
        this.anexoCq06C611 = anexoCq06C611;
        this.firePropertyChange("anexoCq06C611", oldValue, this.anexoCq06C611);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C612() {
        return this.anexoCq06C612;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C612(Long anexoCq06C612) {
        Long oldValue = this.anexoCq06C612;
        this.anexoCq06C612 = anexoCq06C612;
        this.firePropertyChange("anexoCq06C612", oldValue, this.anexoCq06C612);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C613() {
        return this.anexoCq06C613;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C613(Long anexoCq06C613) {
        Long oldValue = this.anexoCq06C613;
        this.anexoCq06C613 = anexoCq06C613;
        this.firePropertyChange("anexoCq06C613", oldValue, this.anexoCq06C613);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C614() {
        return this.anexoCq06C614;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C614(Long anexoCq06C614) {
        Long oldValue = this.anexoCq06C614;
        this.anexoCq06C614 = anexoCq06C614;
        this.firePropertyChange("anexoCq06C614", oldValue, this.anexoCq06C614);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C615() {
        return this.anexoCq06C615;
    }

    public void setAnexoCq06C615Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoCq06C615")) {
            return;
        }
        long newValue = 0;
        this.setAnexoCq06C615(newValue+=(this.getAnexoCq06C611() == null ? 0 : this.getAnexoCq06C611()) + (this.getAnexoCq06C612() == null ? 0 : this.getAnexoCq06C612()) + (this.getAnexoCq06C613() == null ? 0 : this.getAnexoCq06C613()) + (this.getAnexoCq06C614() == null ? 0 : this.getAnexoCq06C614()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C615(Long anexoCq06C615) {
        Long oldValue = this.anexoCq06C615;
        this.anexoCq06C615 = anexoCq06C615;
        this.firePropertyChange("anexoCq06C615", oldValue, this.anexoCq06C615);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoCq06C616() {
        return this.anexoCq06C616;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoCq06C616(Long anexoCq06C616) {
        Long oldValue = this.anexoCq06C616;
        this.anexoCq06C616 = anexoCq06C616;
        this.firePropertyChange("anexoCq06C616", oldValue, this.anexoCq06C616);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoCq06C601);
        result = HashCodeUtil.hash(result, this.anexoCq06C602);
        result = HashCodeUtil.hash(result, this.anexoCq06C603);
        result = HashCodeUtil.hash(result, this.anexoCq06C604);
        result = HashCodeUtil.hash(result, this.anexoCq06C605);
        result = HashCodeUtil.hash(result, this.anexoCq06C606);
        result = HashCodeUtil.hash(result, this.anexoCq06C607);
        result = HashCodeUtil.hash(result, this.anexoCq06C608);
        result = HashCodeUtil.hash(result, this.anexoCq06C609);
        result = HashCodeUtil.hash(result, this.anexoCq06C610);
        result = HashCodeUtil.hash(result, this.anexoCq06C611);
        result = HashCodeUtil.hash(result, this.anexoCq06C612);
        result = HashCodeUtil.hash(result, this.anexoCq06C613);
        result = HashCodeUtil.hash(result, this.anexoCq06C614);
        result = HashCodeUtil.hash(result, this.anexoCq06C615);
        result = HashCodeUtil.hash(result, this.anexoCq06C616);
        return result;
    }
}

