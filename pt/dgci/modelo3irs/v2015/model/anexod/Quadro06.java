/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.dgci.modelo3irs.v2015.model.anexod.Quadro06Base;

public class Quadro06
extends Quadro06Base {
    private static final long serialVersionUID = 3634640613470758928L;

    public boolean isEmpty() {
        return (this.getAnexoDq06C601() == null || this.getAnexoDq06C601() == 0) && (this.getAnexoDq06C602() == null || this.getAnexoDq06C602() == 0);
    }
}

