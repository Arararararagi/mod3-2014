/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_LinhaBase;

public class AnexoDq04T1_Linha
extends AnexoDq04T1_LinhaBase {
    private static final long serialVersionUID = -4641802944297865556L;

    public AnexoDq04T1_Linha() {
    }

    public AnexoDq04T1_Linha(Long nIF, Long imputacao, Long rendimentos, Long retencao) {
        super(nIF, imputacao, rendimentos, retencao);
    }

    public boolean isEmpty() {
        if ((this.getNIF() == null || this.getNIF() == 0) && (this.getImputacao() == null || this.getImputacao() == 0) && (this.getRendimentos() == null || this.getRendimentos() == 0) && (this.getRetencao() == null || this.getRetencao() == 0)) {
            return true;
        }
        return false;
    }

    public static String getLink(int lineIndex, AnexoDq04T1_LinhaBase.Property column) {
        return "aAnexoD.qQuadro04.tanexoDq04T1.l" + lineIndex + ".c" + column.getIndex();
    }
}

