/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoDq04T2_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoDq04T2_Linha>,
WritableTableFormat<AnexoDq04T2_Linha> {
    @Override
    public boolean isEditable(AnexoDq04T2_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoDq04T2_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCampoQ4() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoDQ4", line.getCampoQ4());
            }
            case 2: {
                if (line == null || line.getCodigoDoPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxD", line.getCodigoDoPais());
            }
            case 3: {
                return line.getMontanteDoRendimento();
            }
            case 4: {
                return line.getValor();
            }
        }
        return null;
    }

    @Override
    public AnexoDq04T2_Linha setColumnValue(AnexoDq04T2_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCampoQ4((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setCodigoDoPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setMontanteDoRendimento((Long)value);
                return line;
            }
            case 4: {
                line.setValor((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "N\u00famero do Campo do Quadro 4";
            }
            case 2: {
                return "C\u00f3digo do Pa\u00eds";
            }
            case 3: {
                return "Montante do Rendimento";
            }
            case 4: {
                return "Valor";
            }
        }
        return null;
    }
}

