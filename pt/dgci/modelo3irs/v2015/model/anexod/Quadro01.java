/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01Base;

public class Quadro01
extends Quadro01Base {
    private static final long serialVersionUID = 3358304838192424094L;

    public boolean isEmpty() {
        return (this.getAnexoDq01B1() == null || this.getAnexoDq01B1() == false) && (this.getAnexoDq01B2() == null || this.getAnexoDq01B2() == false);
    }

    public boolean isAnexoDq01B1Selected() {
        return this.getAnexoDq01B1() == null ? false : this.getAnexoDq01B1();
    }

    public boolean isAnexoDq01B2Selected() {
        return this.getAnexoDq01B2() == null ? false : this.getAnexoDq01B2();
    }
}

