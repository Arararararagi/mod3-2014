/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro01Base
extends QuadroModel {
    public static final String QUADRO01_LINK = "aAnexoD.qQuadro01";
    public static final String ANEXODQ01B1_LINK = "aAnexoD.qQuadro01.fanexoDq01B1";
    public static final String ANEXODQ01B1 = "anexoDq01B1";
    private Boolean anexoDq01B1;
    public static final String ANEXODQ01B2_LINK = "aAnexoD.qQuadro01.fanexoDq01B2";
    public static final String ANEXODQ01B2 = "anexoDq01B2";
    private Boolean anexoDq01B2;

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoDq01B1() {
        return this.anexoDq01B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq01B1(Boolean anexoDq01B1) {
        Boolean oldValue = this.anexoDq01B1;
        this.anexoDq01B1 = anexoDq01B1;
        this.firePropertyChange("anexoDq01B1", oldValue, this.anexoDq01B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoDq01B2() {
        return this.anexoDq01B2;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq01B2(Boolean anexoDq01B2) {
        Boolean oldValue = this.anexoDq01B2;
        this.anexoDq01B2 = anexoDq01B2;
        this.firePropertyChange("anexoDq01B2", oldValue, this.anexoDq01B2);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq01B1);
        result = HashCodeUtil.hash(result, this.anexoDq01B2);
        return result;
    }
}

