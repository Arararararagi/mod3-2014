/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_LinhaBase;

public class AnexoDq04T2_Linha
extends AnexoDq04T2_LinhaBase {
    public AnexoDq04T2_Linha() {
    }

    public boolean isEmpty() {
        if ((this.getCampoQ4() == null || this.getCampoQ4() == 0) && (this.getCodigoDoPais() == null || this.getCodigoDoPais() == 0) && (this.getMontanteDoRendimento() == null || this.getMontanteDoRendimento() == 0) && (this.getValor() == null || this.getValor() == 0)) {
            return true;
        }
        return false;
    }

    public AnexoDq04T2_Linha(Long campoQ4, Long codigoDoPais, Long montanteDoRendimento, Long valor) {
        super(campoQ4, codigoDoPais, montanteDoRendimento, valor);
    }

    public static String getLink(int line) {
        return "aAnexoD.qQuadro04.tanexoDq04T2.l" + line;
    }
}

