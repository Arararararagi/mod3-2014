/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoD.qQuadro04";
    public static final String ANEXODQ04C401_LINK = "aAnexoD.qQuadro04.fanexoDq04C401";
    public static final String ANEXODQ04C401 = "anexoDq04C401";
    private Long anexoDq04C401;
    public static final String ANEXODQ04C402_LINK = "aAnexoD.qQuadro04.fanexoDq04C402";
    public static final String ANEXODQ04C402 = "anexoDq04C402";
    private Long anexoDq04C402;
    public static final String ANEXODQ04C403_LINK = "aAnexoD.qQuadro04.fanexoDq04C403";
    public static final String ANEXODQ04C403 = "anexoDq04C403";
    private Long anexoDq04C403;
    public static final String ANEXODQ04C431_LINK = "aAnexoD.qQuadro04.fanexoDq04C431";
    public static final String ANEXODQ04C431 = "anexoDq04C431";
    private Long anexoDq04C431;
    public static final String ANEXODQ04C432_LINK = "aAnexoD.qQuadro04.fanexoDq04C432";
    public static final String ANEXODQ04C432 = "anexoDq04C432";
    private Long anexoDq04C432;
    public static final String ANEXODQ04C401A_LINK = "aAnexoD.qQuadro04.fanexoDq04C401a";
    public static final String ANEXODQ04C401A = "anexoDq04C401a";
    private Long anexoDq04C401a;
    public static final String ANEXODQ04C402A_LINK = "aAnexoD.qQuadro04.fanexoDq04C402a";
    public static final String ANEXODQ04C402A = "anexoDq04C402a";
    private Long anexoDq04C402a;
    public static final String ANEXODQ04C403A_LINK = "aAnexoD.qQuadro04.fanexoDq04C403a";
    public static final String ANEXODQ04C403A = "anexoDq04C403a";
    private Long anexoDq04C403a;
    public static final String ANEXODQ04C431A_LINK = "aAnexoD.qQuadro04.fanexoDq04C431a";
    public static final String ANEXODQ04C431A = "anexoDq04C431a";
    private Long anexoDq04C431a;
    public static final String ANEXODQ04C432A_LINK = "aAnexoD.qQuadro04.fanexoDq04C432a";
    public static final String ANEXODQ04C432A = "anexoDq04C432a";
    private Long anexoDq04C432a;
    public static final String ANEXODQ04C480A_LINK = "aAnexoD.qQuadro04.fanexoDq04C480a";
    public static final String ANEXODQ04C480A = "anexoDq04C480a";
    private Long anexoDq04C480a;
    public static final String ANEXODQ04C481A_LINK = "aAnexoD.qQuadro04.fanexoDq04C481a";
    public static final String ANEXODQ04C481A = "anexoDq04C481a";
    private Long anexoDq04C481a;
    public static final String ANEXODQ04C401B_LINK = "aAnexoD.qQuadro04.fanexoDq04C401b";
    public static final String ANEXODQ04C401B = "anexoDq04C401b";
    private Long anexoDq04C401b;
    public static final String ANEXODQ04C402B_LINK = "aAnexoD.qQuadro04.fanexoDq04C402b";
    public static final String ANEXODQ04C402B = "anexoDq04C402b";
    private Long anexoDq04C402b;
    public static final String ANEXODQ04C403B_LINK = "aAnexoD.qQuadro04.fanexoDq04C403b";
    public static final String ANEXODQ04C403B = "anexoDq04C403b";
    private Long anexoDq04C403b;
    public static final String ANEXODQ04C431B_LINK = "aAnexoD.qQuadro04.fanexoDq04C431b";
    public static final String ANEXODQ04C431B = "anexoDq04C431b";
    private Long anexoDq04C431b;
    public static final String ANEXODQ04C432B_LINK = "aAnexoD.qQuadro04.fanexoDq04C432b";
    public static final String ANEXODQ04C432B = "anexoDq04C432b";
    private Long anexoDq04C432b;
    public static final String ANEXODQ04C480B_LINK = "aAnexoD.qQuadro04.fanexoDq04C480b";
    public static final String ANEXODQ04C480B = "anexoDq04C480b";
    private Long anexoDq04C480b;
    public static final String ANEXODQ04C481B_LINK = "aAnexoD.qQuadro04.fanexoDq04C481b";
    public static final String ANEXODQ04C481B = "anexoDq04C481b";
    private Long anexoDq04C481b;
    public static final String ANEXODQ04C401C_LINK = "aAnexoD.qQuadro04.fanexoDq04C401c";
    public static final String ANEXODQ04C401C = "anexoDq04C401c";
    private Long anexoDq04C401c;
    public static final String ANEXODQ04C402C_LINK = "aAnexoD.qQuadro04.fanexoDq04C402c";
    public static final String ANEXODQ04C402C = "anexoDq04C402c";
    private Long anexoDq04C402c;
    public static final String ANEXODQ04C403C_LINK = "aAnexoD.qQuadro04.fanexoDq04C403c";
    public static final String ANEXODQ04C403C = "anexoDq04C403c";
    private Long anexoDq04C403c;
    public static final String ANEXODQ04C431C_LINK = "aAnexoD.qQuadro04.fanexoDq04C431c";
    public static final String ANEXODQ04C431C = "anexoDq04C431c";
    private Long anexoDq04C431c;
    public static final String ANEXODQ04C432C_LINK = "aAnexoD.qQuadro04.fanexoDq04C432c";
    public static final String ANEXODQ04C432C = "anexoDq04C432c";
    private Long anexoDq04C432c;
    public static final String ANEXODQ04C401D_LINK = "aAnexoD.qQuadro04.fanexoDq04C401d";
    public static final String ANEXODQ04C401D = "anexoDq04C401d";
    private Long anexoDq04C401d;
    public static final String ANEXODQ04C402D_LINK = "aAnexoD.qQuadro04.fanexoDq04C402d";
    public static final String ANEXODQ04C402D = "anexoDq04C402d";
    private Long anexoDq04C402d;
    public static final String ANEXODQ04C403D_LINK = "aAnexoD.qQuadro04.fanexoDq04C403d";
    public static final String ANEXODQ04C403D = "anexoDq04C403d";
    private Long anexoDq04C403d;
    public static final String ANEXODQ04C431D_LINK = "aAnexoD.qQuadro04.fanexoDq04C431d";
    public static final String ANEXODQ04C431D = "anexoDq04C431d";
    private Long anexoDq04C431d;
    public static final String ANEXODQ04C432D_LINK = "aAnexoD.qQuadro04.fanexoDq04C432d";
    public static final String ANEXODQ04C432D = "anexoDq04C432d";
    private Long anexoDq04C432d;
    public static final String ANEXODQ04C3_LINK = "aAnexoD.qQuadro04.fanexoDq04C3";
    public static final String ANEXODQ04C3 = "anexoDq04C3";
    private Long anexoDq04C3;
    public static final String ANEXODQ04C401E_LINK = "aAnexoD.qQuadro04.fanexoDq04C401e";
    public static final String ANEXODQ04C401E = "anexoDq04C401e";
    private Long anexoDq04C401e;
    public static final String ANEXODQ04C402E_LINK = "aAnexoD.qQuadro04.fanexoDq04C402e";
    public static final String ANEXODQ04C402E = "anexoDq04C402e";
    private Long anexoDq04C402e;
    public static final String ANEXODQ04C403E_LINK = "aAnexoD.qQuadro04.fanexoDq04C403e";
    public static final String ANEXODQ04C403E = "anexoDq04C403e";
    private Long anexoDq04C403e;
    public static final String ANEXODQ04C431E_LINK = "aAnexoD.qQuadro04.fanexoDq04C431e";
    public static final String ANEXODQ04C431E = "anexoDq04C431e";
    private Long anexoDq04C431e;
    public static final String ANEXODQ04C432E_LINK = "aAnexoD.qQuadro04.fanexoDq04C432e";
    public static final String ANEXODQ04C432E = "anexoDq04C432e";
    private Long anexoDq04C432e;
    public static final String ANEXODQ04C4_LINK = "aAnexoD.qQuadro04.fanexoDq04C4";
    public static final String ANEXODQ04C4 = "anexoDq04C4";
    private Long anexoDq04C4;
    public static final String ANEXODQ04T1_LINK = "aAnexoD.qQuadro04.tanexoDq04T1";
    private EventList<AnexoDq04T1_Linha> anexoDq04T1 = new BasicEventList<AnexoDq04T1_Linha>();
    public static final String ANEXODQ04C1_LINK = "aAnexoD.qQuadro04.fanexoDq04C1";
    public static final String ANEXODQ04C1 = "anexoDq04C1";
    private Long anexoDq04C1;
    public static final String ANEXODQ04C2_LINK = "aAnexoD.qQuadro04.fanexoDq04C2";
    public static final String ANEXODQ04C2 = "anexoDq04C2";
    private Long anexoDq04C2;
    public static final String ANEXODQ04T2_LINK = "aAnexoD.qQuadro04.tanexoDq04T2";
    private EventList<AnexoDq04T2_Linha> anexoDq04T2 = new BasicEventList<AnexoDq04T2_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C401() {
        return this.anexoDq04C401;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C401(Long anexoDq04C401) {
        Long oldValue = this.anexoDq04C401;
        this.anexoDq04C401 = anexoDq04C401;
        this.firePropertyChange("anexoDq04C401", oldValue, this.anexoDq04C401);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C402() {
        return this.anexoDq04C402;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C402(Long anexoDq04C402) {
        Long oldValue = this.anexoDq04C402;
        this.anexoDq04C402 = anexoDq04C402;
        this.firePropertyChange("anexoDq04C402", oldValue, this.anexoDq04C402);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C403() {
        return this.anexoDq04C403;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C403(Long anexoDq04C403) {
        Long oldValue = this.anexoDq04C403;
        this.anexoDq04C403 = anexoDq04C403;
        this.firePropertyChange("anexoDq04C403", oldValue, this.anexoDq04C403);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C431() {
        return this.anexoDq04C431;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C431(Long anexoDq04C431) {
        Long oldValue = this.anexoDq04C431;
        this.anexoDq04C431 = anexoDq04C431;
        this.firePropertyChange("anexoDq04C431", oldValue, this.anexoDq04C431);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C432() {
        return this.anexoDq04C432;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C432(Long anexoDq04C432) {
        Long oldValue = this.anexoDq04C432;
        this.anexoDq04C432 = anexoDq04C432;
        this.firePropertyChange("anexoDq04C432", oldValue, this.anexoDq04C432);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C401a() {
        return this.anexoDq04C401a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C401a(Long anexoDq04C401a) {
        Long oldValue = this.anexoDq04C401a;
        this.anexoDq04C401a = anexoDq04C401a;
        this.firePropertyChange("anexoDq04C401a", oldValue, this.anexoDq04C401a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C402a() {
        return this.anexoDq04C402a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C402a(Long anexoDq04C402a) {
        Long oldValue = this.anexoDq04C402a;
        this.anexoDq04C402a = anexoDq04C402a;
        this.firePropertyChange("anexoDq04C402a", oldValue, this.anexoDq04C402a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C403a() {
        return this.anexoDq04C403a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C403a(Long anexoDq04C403a) {
        Long oldValue = this.anexoDq04C403a;
        this.anexoDq04C403a = anexoDq04C403a;
        this.firePropertyChange("anexoDq04C403a", oldValue, this.anexoDq04C403a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C431a() {
        return this.anexoDq04C431a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C431a(Long anexoDq04C431a) {
        Long oldValue = this.anexoDq04C431a;
        this.anexoDq04C431a = anexoDq04C431a;
        this.firePropertyChange("anexoDq04C431a", oldValue, this.anexoDq04C431a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C432a() {
        return this.anexoDq04C432a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C432a(Long anexoDq04C432a) {
        Long oldValue = this.anexoDq04C432a;
        this.anexoDq04C432a = anexoDq04C432a;
        this.firePropertyChange("anexoDq04C432a", oldValue, this.anexoDq04C432a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C480a() {
        return this.anexoDq04C480a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C480a(Long anexoDq04C480a) {
        Long oldValue = this.anexoDq04C480a;
        this.anexoDq04C480a = anexoDq04C480a;
        this.firePropertyChange("anexoDq04C480a", oldValue, this.anexoDq04C480a);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq04C481a() {
        return this.anexoDq04C481a;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq04C481a(Long anexoDq04C481a) {
        Long oldValue = this.anexoDq04C481a;
        this.anexoDq04C481a = anexoDq04C481a;
        this.firePropertyChange("anexoDq04C481a", oldValue, this.anexoDq04C481a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C401b() {
        return this.anexoDq04C401b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C401b(Long anexoDq04C401b) {
        Long oldValue = this.anexoDq04C401b;
        this.anexoDq04C401b = anexoDq04C401b;
        this.firePropertyChange("anexoDq04C401b", oldValue, this.anexoDq04C401b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C402b() {
        return this.anexoDq04C402b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C402b(Long anexoDq04C402b) {
        Long oldValue = this.anexoDq04C402b;
        this.anexoDq04C402b = anexoDq04C402b;
        this.firePropertyChange("anexoDq04C402b", oldValue, this.anexoDq04C402b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C403b() {
        return this.anexoDq04C403b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C403b(Long anexoDq04C403b) {
        Long oldValue = this.anexoDq04C403b;
        this.anexoDq04C403b = anexoDq04C403b;
        this.firePropertyChange("anexoDq04C403b", oldValue, this.anexoDq04C403b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C431b() {
        return this.anexoDq04C431b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C431b(Long anexoDq04C431b) {
        Long oldValue = this.anexoDq04C431b;
        this.anexoDq04C431b = anexoDq04C431b;
        this.firePropertyChange("anexoDq04C431b", oldValue, this.anexoDq04C431b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C432b() {
        return this.anexoDq04C432b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C432b(Long anexoDq04C432b) {
        Long oldValue = this.anexoDq04C432b;
        this.anexoDq04C432b = anexoDq04C432b;
        this.firePropertyChange("anexoDq04C432b", oldValue, this.anexoDq04C432b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C480b() {
        return this.anexoDq04C480b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C480b(Long anexoDq04C480b) {
        Long oldValue = this.anexoDq04C480b;
        this.anexoDq04C480b = anexoDq04C480b;
        this.firePropertyChange("anexoDq04C480b", oldValue, this.anexoDq04C480b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C481b() {
        return this.anexoDq04C481b;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C481b(Long anexoDq04C481b) {
        Long oldValue = this.anexoDq04C481b;
        this.anexoDq04C481b = anexoDq04C481b;
        this.firePropertyChange("anexoDq04C481b", oldValue, this.anexoDq04C481b);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C401c() {
        return this.anexoDq04C401c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C401c(Long anexoDq04C401c) {
        Long oldValue = this.anexoDq04C401c;
        this.anexoDq04C401c = anexoDq04C401c;
        this.firePropertyChange("anexoDq04C401c", oldValue, this.anexoDq04C401c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C402c() {
        return this.anexoDq04C402c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C402c(Long anexoDq04C402c) {
        Long oldValue = this.anexoDq04C402c;
        this.anexoDq04C402c = anexoDq04C402c;
        this.firePropertyChange("anexoDq04C402c", oldValue, this.anexoDq04C402c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C403c() {
        return this.anexoDq04C403c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C403c(Long anexoDq04C403c) {
        Long oldValue = this.anexoDq04C403c;
        this.anexoDq04C403c = anexoDq04C403c;
        this.firePropertyChange("anexoDq04C403c", oldValue, this.anexoDq04C403c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C431c() {
        return this.anexoDq04C431c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C431c(Long anexoDq04C431c) {
        Long oldValue = this.anexoDq04C431c;
        this.anexoDq04C431c = anexoDq04C431c;
        this.firePropertyChange("anexoDq04C431c", oldValue, this.anexoDq04C431c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C432c() {
        return this.anexoDq04C432c;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C432c(Long anexoDq04C432c) {
        Long oldValue = this.anexoDq04C432c;
        this.anexoDq04C432c = anexoDq04C432c;
        this.firePropertyChange("anexoDq04C432c", oldValue, this.anexoDq04C432c);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C401d() {
        return this.anexoDq04C401d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C401d(Long anexoDq04C401d) {
        Long oldValue = this.anexoDq04C401d;
        this.anexoDq04C401d = anexoDq04C401d;
        this.firePropertyChange("anexoDq04C401d", oldValue, this.anexoDq04C401d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C402d() {
        return this.anexoDq04C402d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C402d(Long anexoDq04C402d) {
        Long oldValue = this.anexoDq04C402d;
        this.anexoDq04C402d = anexoDq04C402d;
        this.firePropertyChange("anexoDq04C402d", oldValue, this.anexoDq04C402d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C403d() {
        return this.anexoDq04C403d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C403d(Long anexoDq04C403d) {
        Long oldValue = this.anexoDq04C403d;
        this.anexoDq04C403d = anexoDq04C403d;
        this.firePropertyChange("anexoDq04C403d", oldValue, this.anexoDq04C403d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C431d() {
        return this.anexoDq04C431d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C431d(Long anexoDq04C431d) {
        Long oldValue = this.anexoDq04C431d;
        this.anexoDq04C431d = anexoDq04C431d;
        this.firePropertyChange("anexoDq04C431d", oldValue, this.anexoDq04C431d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C432d() {
        return this.anexoDq04C432d;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C432d(Long anexoDq04C432d) {
        Long oldValue = this.anexoDq04C432d;
        this.anexoDq04C432d = anexoDq04C432d;
        this.firePropertyChange("anexoDq04C432d", oldValue, this.anexoDq04C432d);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C3() {
        return this.anexoDq04C3;
    }

    public void setAnexoDq04C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoDq04C3")) {
            return;
        }
        long newValue = 0;
        this.setAnexoDq04C3(newValue+=(this.getAnexoDq04C401d() == null ? 0 : this.getAnexoDq04C401d()) + (this.getAnexoDq04C402d() == null ? 0 : this.getAnexoDq04C402d()) + (this.getAnexoDq04C403d() == null ? 0 : this.getAnexoDq04C403d()) + (this.getAnexoDq04C431d() == null ? 0 : this.getAnexoDq04C431d()) + (this.getAnexoDq04C432d() == null ? 0 : this.getAnexoDq04C432d()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C3(Long anexoDq04C3) {
        Long oldValue = this.anexoDq04C3;
        this.anexoDq04C3 = anexoDq04C3;
        this.firePropertyChange("anexoDq04C3", oldValue, this.anexoDq04C3);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C401e() {
        return this.anexoDq04C401e;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C401e(Long anexoDq04C401e) {
        Long oldValue = this.anexoDq04C401e;
        this.anexoDq04C401e = anexoDq04C401e;
        this.firePropertyChange("anexoDq04C401e", oldValue, this.anexoDq04C401e);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C402e() {
        return this.anexoDq04C402e;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C402e(Long anexoDq04C402e) {
        Long oldValue = this.anexoDq04C402e;
        this.anexoDq04C402e = anexoDq04C402e;
        this.firePropertyChange("anexoDq04C402e", oldValue, this.anexoDq04C402e);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C403e() {
        return this.anexoDq04C403e;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C403e(Long anexoDq04C403e) {
        Long oldValue = this.anexoDq04C403e;
        this.anexoDq04C403e = anexoDq04C403e;
        this.firePropertyChange("anexoDq04C403e", oldValue, this.anexoDq04C403e);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C431e() {
        return this.anexoDq04C431e;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C431e(Long anexoDq04C431e) {
        Long oldValue = this.anexoDq04C431e;
        this.anexoDq04C431e = anexoDq04C431e;
        this.firePropertyChange("anexoDq04C431e", oldValue, this.anexoDq04C431e);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C432e() {
        return this.anexoDq04C432e;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C432e(Long anexoDq04C432e) {
        Long oldValue = this.anexoDq04C432e;
        this.anexoDq04C432e = anexoDq04C432e;
        this.firePropertyChange("anexoDq04C432e", oldValue, this.anexoDq04C432e);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C4() {
        return this.anexoDq04C4;
    }

    public void setAnexoDq04C4Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoDq04C4")) {
            return;
        }
        long newValue = 0;
        this.setAnexoDq04C4(newValue+=(this.getAnexoDq04C401e() == null ? 0 : this.getAnexoDq04C401e()) + (this.getAnexoDq04C402e() == null ? 0 : this.getAnexoDq04C402e()) + (this.getAnexoDq04C403e() == null ? 0 : this.getAnexoDq04C403e()) + (this.getAnexoDq04C431e() == null ? 0 : this.getAnexoDq04C431e()) + (this.getAnexoDq04C432e() == null ? 0 : this.getAnexoDq04C432e()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C4(Long anexoDq04C4) {
        Long oldValue = this.anexoDq04C4;
        this.anexoDq04C4 = anexoDq04C4;
        this.firePropertyChange("anexoDq04C4", oldValue, this.anexoDq04C4);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoDq04T1_Linha> getAnexoDq04T1() {
        return this.anexoDq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoDq04T1(EventList<AnexoDq04T1_Linha> anexoDq04T1) {
        this.anexoDq04T1 = anexoDq04T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C1() {
        return this.anexoDq04C1;
    }

    public void setAnexoDq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoDq04C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoDq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoDq04T1().get(i).getRendimentos() == null ? 0 : this.getAnexoDq04T1().get(i).getRendimentos();
        }
        this.setAnexoDq04C1(newValue+=temporaryValue0 + (this.getAnexoDq04C401b() == null ? 0 : this.getAnexoDq04C401b()) + (this.getAnexoDq04C402b() == null ? 0 : this.getAnexoDq04C402b()) + (this.getAnexoDq04C403b() == null ? 0 : this.getAnexoDq04C403b()) + (this.getAnexoDq04C431b() == null ? 0 : this.getAnexoDq04C431b()) + (this.getAnexoDq04C432b() == null ? 0 : this.getAnexoDq04C432b()) + (this.getAnexoDq04C480b() == null ? 0 : this.getAnexoDq04C480b()) + (this.getAnexoDq04C481b() == null ? 0 : this.getAnexoDq04C481b()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C1(Long anexoDq04C1) {
        Long oldValue = this.anexoDq04C1;
        this.anexoDq04C1 = anexoDq04C1;
        this.firePropertyChange("anexoDq04C1", oldValue, this.anexoDq04C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq04C2() {
        return this.anexoDq04C2;
    }

    public void setAnexoDq04C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoDq04C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoDq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoDq04T1().get(i).getRetencao() == null ? 0 : this.getAnexoDq04T1().get(i).getRetencao();
        }
        this.setAnexoDq04C2(newValue+=temporaryValue0 + (this.getAnexoDq04C401c() == null ? 0 : this.getAnexoDq04C401c()) + (this.getAnexoDq04C402c() == null ? 0 : this.getAnexoDq04C402c()) + (this.getAnexoDq04C403c() == null ? 0 : this.getAnexoDq04C403c()) + (this.getAnexoDq04C431c() == null ? 0 : this.getAnexoDq04C431c()) + (this.getAnexoDq04C432c() == null ? 0 : this.getAnexoDq04C432c()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq04C2(Long anexoDq04C2) {
        Long oldValue = this.anexoDq04C2;
        this.anexoDq04C2 = anexoDq04C2;
        this.firePropertyChange("anexoDq04C2", oldValue, this.anexoDq04C2);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoDq04T2_Linha> getAnexoDq04T2() {
        return this.anexoDq04T2;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoDq04T2(EventList<AnexoDq04T2_Linha> anexoDq04T2) {
        this.anexoDq04T2 = anexoDq04T2;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq04C401);
        result = HashCodeUtil.hash(result, this.anexoDq04C402);
        result = HashCodeUtil.hash(result, this.anexoDq04C403);
        result = HashCodeUtil.hash(result, this.anexoDq04C431);
        result = HashCodeUtil.hash(result, this.anexoDq04C432);
        result = HashCodeUtil.hash(result, this.anexoDq04C401a);
        result = HashCodeUtil.hash(result, this.anexoDq04C402a);
        result = HashCodeUtil.hash(result, this.anexoDq04C403a);
        result = HashCodeUtil.hash(result, this.anexoDq04C431a);
        result = HashCodeUtil.hash(result, this.anexoDq04C432a);
        result = HashCodeUtil.hash(result, this.anexoDq04C480a);
        result = HashCodeUtil.hash(result, this.anexoDq04C481a);
        result = HashCodeUtil.hash(result, this.anexoDq04C401b);
        result = HashCodeUtil.hash(result, this.anexoDq04C402b);
        result = HashCodeUtil.hash(result, this.anexoDq04C403b);
        result = HashCodeUtil.hash(result, this.anexoDq04C431b);
        result = HashCodeUtil.hash(result, this.anexoDq04C432b);
        result = HashCodeUtil.hash(result, this.anexoDq04C480b);
        result = HashCodeUtil.hash(result, this.anexoDq04C481b);
        result = HashCodeUtil.hash(result, this.anexoDq04C401c);
        result = HashCodeUtil.hash(result, this.anexoDq04C402c);
        result = HashCodeUtil.hash(result, this.anexoDq04C403c);
        result = HashCodeUtil.hash(result, this.anexoDq04C431c);
        result = HashCodeUtil.hash(result, this.anexoDq04C432c);
        result = HashCodeUtil.hash(result, this.anexoDq04C401d);
        result = HashCodeUtil.hash(result, this.anexoDq04C402d);
        result = HashCodeUtil.hash(result, this.anexoDq04C403d);
        result = HashCodeUtil.hash(result, this.anexoDq04C431d);
        result = HashCodeUtil.hash(result, this.anexoDq04C432d);
        result = HashCodeUtil.hash(result, this.anexoDq04C3);
        result = HashCodeUtil.hash(result, this.anexoDq04C401e);
        result = HashCodeUtil.hash(result, this.anexoDq04C402e);
        result = HashCodeUtil.hash(result, this.anexoDq04C403e);
        result = HashCodeUtil.hash(result, this.anexoDq04C431e);
        result = HashCodeUtil.hash(result, this.anexoDq04C432e);
        result = HashCodeUtil.hash(result, this.anexoDq04C4);
        result = HashCodeUtil.hash(result, this.anexoDq04T1);
        result = HashCodeUtil.hash(result, this.anexoDq04C1);
        result = HashCodeUtil.hash(result, this.anexoDq04C2);
        result = HashCodeUtil.hash(result, this.anexoDq04T2);
        return result;
    }
}

