/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;

public class AnexoDq04T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoDq04T1_Linha>,
WritableTableFormat<AnexoDq04T1_Linha> {
    @Override
    public boolean isEditable(AnexoDq04T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoDq04T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNIF();
            }
            case 2: {
                return line.getImputacao();
            }
            case 3: {
                return line.getRendimentos();
            }
            case 4: {
                return line.getRetencao();
            }
        }
        return null;
    }

    @Override
    public AnexoDq04T1_Linha setColumnValue(AnexoDq04T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNIF((Long)value);
                return line;
            }
            case 2: {
                line.setImputacao((Long)value);
                return line;
            }
            case 3: {
                line.setRendimentos((Long)value);
                return line;
            }
            case 4: {
                line.setRetencao((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF Entidade Imputadora";
            }
            case 2: {
                return "% de Imputa\u00e7\u00e3o";
            }
            case 3: {
                return "Lucro/Preju\u00edzo - Rendimentos L\u00edquidos Imputados";
            }
            case 4: {
                return "Reten\u00e7\u00e3o na Fonte";
            }
        }
        return null;
    }
}

