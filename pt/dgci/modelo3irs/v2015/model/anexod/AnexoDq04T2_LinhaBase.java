/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoDq04T2_LinhaBase
extends Model {
    public static final String CAMPOQ4_LINK = "aAnexoD.qQuadro04.fcampoQ4";
    public static final String CAMPOQ4 = "campoQ4";
    private Long campoQ4;
    public static final String CODIGODOPAIS_LINK = "aAnexoD.qQuadro04.fcodigoDoPais";
    public static final String CODIGODOPAIS = "codigoDoPais";
    private Long codigoDoPais;
    public static final String MONTANTEDORENDIMENTO_LINK = "aAnexoD.qQuadro04.fmontanteDoRendimento";
    public static final String MONTANTEDORENDIMENTO = "montanteDoRendimento";
    private Long montanteDoRendimento;
    public static final String VALOR_LINK = "aAnexoD.qQuadro04.fvalor";
    public static final String VALOR = "valor";
    private Long valor;

    public AnexoDq04T2_LinhaBase(Long campoQ4, Long codigoDoPais, Long montanteDoRendimento, Long valor) {
        this.campoQ4 = campoQ4;
        this.codigoDoPais = codigoDoPais;
        this.montanteDoRendimento = montanteDoRendimento;
        this.valor = valor;
    }

    public AnexoDq04T2_LinhaBase() {
        this.campoQ4 = null;
        this.codigoDoPais = null;
        this.montanteDoRendimento = null;
        this.valor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoDQ4")
    public Long getCampoQ4() {
        return this.campoQ4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoQ4(Long campoQ4) {
        Long oldValue = this.campoQ4;
        this.campoQ4 = campoQ4;
        this.firePropertyChange("campoQ4", oldValue, this.campoQ4);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxD")
    public Long getCodigoDoPais() {
        return this.codigoDoPais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodigoDoPais(Long codigoDoPais) {
        Long oldValue = this.codigoDoPais;
        this.codigoDoPais = codigoDoPais;
        this.firePropertyChange("codigoDoPais", oldValue, this.codigoDoPais);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getMontanteDoRendimento() {
        return this.montanteDoRendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setMontanteDoRendimento(Long montanteDoRendimento) {
        Long oldValue = this.montanteDoRendimento;
        this.montanteDoRendimento = montanteDoRendimento;
        this.firePropertyChange("montanteDoRendimento", oldValue, this.montanteDoRendimento);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValor() {
        return this.valor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValor(Long valor) {
        Long oldValue = this.valor;
        this.valor = valor;
        this.firePropertyChange("valor", oldValue, this.valor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.campoQ4);
        result = HashCodeUtil.hash(result, this.codigoDoPais);
        result = HashCodeUtil.hash(result, this.montanteDoRendimento);
        result = HashCodeUtil.hash(result, this.valor);
        return result;
    }

    public static enum Property {
        CAMPOQ4("campoQ4", 2),
        CODIGODOPAIS("codigoDoPais", 3),
        MONTANTEDORENDIMENTO("montanteDoRendimento", 4),
        VALOR("valor", 5);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

