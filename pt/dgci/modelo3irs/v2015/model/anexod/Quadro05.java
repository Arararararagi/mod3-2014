/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.dgci.modelo3irs.v2015.model.anexod.Quadro05Base;

public class Quadro05
extends Quadro05Base {
    private static final long serialVersionUID = -1209829594979601101L;

    public boolean isEmpty() {
        return (this.getAnexoDq05C501() == null || this.getAnexoDq05C501() == 0) && (this.getAnexoDq05C502() == null || this.getAnexoDq05C502() == 0) && (this.getAnexoDq05C503() == null || this.getAnexoDq05C503() == 0) && (this.getAnexoDq05C504() == null || this.getAnexoDq05C504() == 0) && (this.getAnexoDq05C505() == null || this.getAnexoDq05C505() == 0) && (this.getAnexoDq05C506() == null || this.getAnexoDq05C506() == 0) && (this.getAnexoDq05C507() == null || this.getAnexoDq05C507() == 0) && (this.getAnexoDq05C508() == null || this.getAnexoDq05C508() == 0);
    }

    public long getMateriaColectavel() {
        long materiaColectavel = this.getAnexoDq05C501() != null ? this.getAnexoDq05C501() : 0;
        return materiaColectavel+=this.getAnexoDq05C505() != null ? this.getAnexoDq05C505() : 0;
    }

    public long getLucro() {
        long lucro = this.getAnexoDq05C503() != null ? this.getAnexoDq05C503() : 0;
        return lucro+=this.getAnexoDq05C507() != null ? this.getAnexoDq05C507() : 0;
    }

    public long getPrejuizo() {
        long prejuizo = this.getAnexoDq05C502() != null ? this.getAnexoDq05C502() : 0;
        return prejuizo+=this.getAnexoDq05C506() != null ? this.getAnexoDq05C506() : 0;
    }

    public long getRendimentoIliquido() {
        long rendimento = this.getAnexoDq05C504() != null ? this.getAnexoDq05C504() : 0;
        return rendimento+=this.getAnexoDq05C508() != null ? this.getAnexoDq05C508() : 0;
    }
}

