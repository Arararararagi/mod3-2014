/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoD.qQuadro02";
    public static final String ANEXODQ02C03_LINK = "aAnexoD.qQuadro02.fanexoDq02C03";
    public static final String ANEXODQ02C03 = "anexoDq02C03";
    private Long anexoDq02C03;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoDq02C03() {
        return this.anexoDq02C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq02C03(Long anexoDq02C03) {
        Long oldValue = this.anexoDq02C03;
        this.anexoDq02C03 = anexoDq02C03;
        this.firePropertyChange("anexoDq02C03", oldValue, this.anexoDq02C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq02C03);
        return result;
    }
}

