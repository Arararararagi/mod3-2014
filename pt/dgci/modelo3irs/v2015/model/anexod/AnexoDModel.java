/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.dgci.modelo3irs.v2015.model.OwnableAnexo;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModelBase;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro08;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=9)
public class AnexoDModel
extends AnexoDModelBase
implements OwnableAnexo {
    private static final long serialVersionUID = -8111052497711407677L;

    public AnexoDModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
        this.postCreate();
    }

    protected void postCreate() {
        Quadro03 quadro03Model = this.getQuadro03();
        if (quadro03Model != null) {
            quadro03Model.setAnexoDq03C06(Long.parseLong(this.formKey.getSubId()));
        }
    }

    public Long getAnexoDTitular() {
        return this.getQuadro03().getAnexoDq03C06();
    }

    @Override
    public boolean isOwnedBy(Long nif) {
        return nif != null && nif.equals(this.getQuadro03().getAnexoDq03C06());
    }

    public String getLink(String linkWithoutSubId) {
        return linkWithoutSubId.replaceFirst("aAnexoD", "aAnexoD|" + this.getFormKey().getSubId());
    }

    public boolean isEmpty() {
        return this.getQuadro04().isEmpty() && this.getQuadro05().isEmpty() && this.getQuadro06().isEmpty() && this.getQuadro07().isEmpty();
    }

    public Quadro01 getQuadro01() {
        return (Quadro01)this.getQuadro(Quadro01.class.getSimpleName());
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public Quadro07 getQuadro07() {
        return (Quadro07)this.getQuadro(Quadro07.class.getSimpleName());
    }

    public Quadro08 getQuadro08() {
        return (Quadro08)this.getQuadro(Quadro08.class.getSimpleName());
    }
}

