/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoD.qQuadro03";
    public static final String ANEXODQ03C04_LINK = "aAnexoD.qQuadro03.fanexoDq03C04";
    public static final String ANEXODQ03C04 = "anexoDq03C04";
    private Long anexoDq03C04;
    public static final String ANEXODQ03C05_LINK = "aAnexoD.qQuadro03.fanexoDq03C05";
    public static final String ANEXODQ03C05 = "anexoDq03C05";
    private Long anexoDq03C05;
    public static final String ANEXODQ03C06_LINK = "aAnexoD.qQuadro03.fanexoDq03C06";
    public static final String ANEXODQ03C06 = "anexoDq03C06";
    private Long anexoDq03C06;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq03C04() {
        return this.anexoDq03C04;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq03C04(Long anexoDq03C04) {
        Long oldValue = this.anexoDq03C04;
        this.anexoDq03C04 = anexoDq03C04;
        this.firePropertyChange("anexoDq03C04", oldValue, this.anexoDq03C04);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq03C05() {
        return this.anexoDq03C05;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq03C05(Long anexoDq03C05) {
        Long oldValue = this.anexoDq03C05;
        this.anexoDq03C05 = anexoDq03C05;
        this.firePropertyChange("anexoDq03C05", oldValue, this.anexoDq03C05);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq03C06() {
        return this.anexoDq03C06;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq03C06(Long anexoDq03C06) {
        Long oldValue = this.anexoDq03C06;
        this.anexoDq03C06 = anexoDq03C06;
        this.firePropertyChange("anexoDq03C06", oldValue, this.anexoDq03C06);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq03C04);
        result = HashCodeUtil.hash(result, this.anexoDq03C05);
        result = HashCodeUtil.hash(result, this.anexoDq03C06);
        return result;
    }
}

