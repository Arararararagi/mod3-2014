/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoDq04T1_LinhaBase
extends Model {
    public static final String NIF_LINK = "aAnexoD.qQuadro04.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;
    public static final String IMPUTACAO_LINK = "aAnexoD.qQuadro04.fimputacao";
    public static final String IMPUTACAO = "imputacao";
    private Long imputacao;
    public static final String RENDIMENTOS_LINK = "aAnexoD.qQuadro04.frendimentos";
    public static final String RENDIMENTOS = "rendimentos";
    private Long rendimentos;
    public static final String RETENCAO_LINK = "aAnexoD.qQuadro04.fretencao";
    public static final String RETENCAO = "retencao";
    private Long retencao;

    public AnexoDq04T1_LinhaBase(Long nIF, Long imputacao, Long rendimentos, Long retencao) {
        this.nIF = nIF;
        this.imputacao = imputacao;
        this.rendimentos = rendimentos;
        this.retencao = retencao;
    }

    public AnexoDq04T1_LinhaBase() {
        this.nIF = null;
        this.imputacao = null;
        this.rendimentos = null;
        this.retencao = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getImputacao() {
        return this.imputacao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setImputacao(Long imputacao) {
        Long oldValue = this.imputacao;
        this.imputacao = imputacao;
        this.firePropertyChange("imputacao", oldValue, this.imputacao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentos() {
        return this.rendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentos(Long rendimentos) {
        Long oldValue = this.rendimentos;
        this.rendimentos = rendimentos;
        this.firePropertyChange("rendimentos", oldValue, this.rendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencao() {
        return this.retencao;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencao(Long retencao) {
        Long oldValue = this.retencao;
        this.retencao = retencao;
        this.firePropertyChange("retencao", oldValue, this.retencao);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.imputacao);
        result = HashCodeUtil.hash(result, this.rendimentos);
        result = HashCodeUtil.hash(result, this.retencao);
        return result;
    }

    public static enum Property {
        NIF("nIF", 2),
        IMPUTACAO("imputacao", 3),
        RENDIMENTOS("rendimentos", 4),
        RETENCAO("retencao", 5);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

