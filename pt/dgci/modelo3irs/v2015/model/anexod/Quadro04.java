/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexod.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro04
extends Quadro04Base {
    private static final long serialVersionUID = -154630434720676203L;
    private long materiaColectavel = 0;
    private long lucro;
    private long prejuizo;
    private long rendimentoLiquidoPositivo = 0;

    public boolean isEmpty() {
        return (this.getAnexoDq04C401() == null || this.getAnexoDq04C401() == 0) && (this.getAnexoDq04C401a() == null || this.getAnexoDq04C401a() == 0) && (this.getAnexoDq04C401b() == null || this.getAnexoDq04C401b() == 0) && (this.getAnexoDq04C401c() == null || this.getAnexoDq04C401c() == 0) && (this.getAnexoDq04C401d() == null || this.getAnexoDq04C401d() == 0) && (this.getAnexoDq04C401e() == null || this.getAnexoDq04C401e() == 0) && (this.getAnexoDq04C402() == null || this.getAnexoDq04C402() == 0) && (this.getAnexoDq04C402a() == null || this.getAnexoDq04C402a() == 0) && (this.getAnexoDq04C402b() == null || this.getAnexoDq04C402b() == 0) && (this.getAnexoDq04C402c() == null || this.getAnexoDq04C402c() == 0) && (this.getAnexoDq04C402d() == null || this.getAnexoDq04C402d() == 0) && (this.getAnexoDq04C402e() == null || this.getAnexoDq04C402e() == 0) && (this.getAnexoDq04C403() == null || this.getAnexoDq04C403() == 0) && (this.getAnexoDq04C403a() == null || this.getAnexoDq04C403a() == 0) && (this.getAnexoDq04C403b() == null || this.getAnexoDq04C403b() == 0) && (this.getAnexoDq04C403c() == null || this.getAnexoDq04C403c() == 0) && (this.getAnexoDq04C403d() == null || this.getAnexoDq04C403d() == 0) && (this.getAnexoDq04C403e() == null || this.getAnexoDq04C403e() == 0) && (this.getAnexoDq04C431() == null || this.getAnexoDq04C431() == 0) && (this.getAnexoDq04C431a() == null || this.getAnexoDq04C431a() == 0) && (this.getAnexoDq04C431b() == null || this.getAnexoDq04C431b() == 0) && (this.getAnexoDq04C431c() == null || this.getAnexoDq04C431c() == 0) && (this.getAnexoDq04C431d() == null || this.getAnexoDq04C431d() == 0) && (this.getAnexoDq04C431e() == null || this.getAnexoDq04C431e() == 0) && (this.getAnexoDq04C432() == null || this.getAnexoDq04C432() == 0) && (this.getAnexoDq04C432a() == null || this.getAnexoDq04C432a() == 0) && (this.getAnexoDq04C432b() == null || this.getAnexoDq04C432b() == 0) && (this.getAnexoDq04C432c() == null || this.getAnexoDq04C432c() == 0) && (this.getAnexoDq04C432d() == null || this.getAnexoDq04C432d() == 0) && (this.getAnexoDq04C432e() == null || this.getAnexoDq04C432e() == 0) && (this.getAnexoDq04C480a() == null || this.getAnexoDq04C480a() == 0) && (this.getAnexoDq04C480b() == null || this.getAnexoDq04C480b() == 0) && (this.getAnexoDq04C481a() == null || this.getAnexoDq04C481a() == 0) && (this.getAnexoDq04C481b() == null || this.getAnexoDq04C481b() == 0) && this.tabela_anexoDq04T1_isEmpty() && this.tabela_anexoDq04T2_isEmpty();
    }

    public boolean tabela_anexoDq04T1_isEmpty() {
        if (this.getAnexoDq04T1().isEmpty()) {
            return true;
        }
        for (AnexoDq04T1_Linha linha : this.getAnexoDq04T1()) {
            if (linha.isEmpty()) continue;
            return false;
        }
        return true;
    }

    public boolean tabela_anexoDq04T2_isEmpty() {
        if (this.getAnexoDq04T2().isEmpty()) {
            return true;
        }
        for (AnexoDq04T2_Linha linha : this.getAnexoDq04T2()) {
            if (linha.isEmpty()) continue;
            return false;
        }
        return true;
    }

    public long getMateriaColectavel() {
        this.calculaMateriaColectavel();
        return this.materiaColectavel;
    }

    private void calculaMateriaColectavel() {
        this.materiaColectavel = Modelo3IRSValidatorUtil.getMaxFromValues(this.getAnexoDq04C401b(), this.getAnexoDq04C401d());
        this.materiaColectavel+=Modelo3IRSValidatorUtil.getMaxFromValues(this.getAnexoDq04C402b(), this.getAnexoDq04C402d());
        this.materiaColectavel+=Modelo3IRSValidatorUtil.getMaxFromValues(this.getAnexoDq04C403b(), this.getAnexoDq04C403d());
    }

    public boolean hasAdiantamentosMateriaColectavel() {
        return !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C401d()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C402d()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C403d());
    }

    private void calculaLucroPrejuizo() {
        this.lucro = 0;
        this.prejuizo = 0;
        long maxC431 = Modelo3IRSValidatorUtil.getMaxFromValuesIgnoreZero(this.getAnexoDq04C431b(), this.getAnexoDq04C431d());
        if (maxC431 > 0) {
            this.lucro = maxC431;
        } else {
            this.prejuizo = maxC431;
        }
        long maxC432 = Modelo3IRSValidatorUtil.getMaxFromValuesIgnoreZero(this.getAnexoDq04C432b(), this.getAnexoDq04C432d());
        if (maxC432 > 0) {
            this.lucro+=maxC432;
        } else {
            this.prejuizo+=maxC432;
        }
        EventList<AnexoDq04T1_Linha> linhas = this.getAnexoDq04T1();
        for (AnexoDq04T1_Linha linha : linhas) {
            if (linha.getRendimentos() == null) continue;
            if (linha.getRendimentos() > 0) {
                this.lucro+=linha.getRendimentos().longValue();
                continue;
            }
            this.prejuizo+=linha.getRendimentos().longValue();
        }
    }

    public long getLucro() {
        this.calculaLucroPrejuizo();
        return this.lucro;
    }

    public long getPrejuizo() {
        this.calculaLucroPrejuizo();
        return this.prejuizo;
    }

    public boolean hasAdiantamentosLucroPrejuizo() {
        return !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C431d()) || !Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C432d());
    }

    public long getRendimentoLiquidoPositivo() {
        this.calculaRendimentoLiquidoPositivo();
        return this.rendimentoLiquidoPositivo;
    }

    private void calculaRendimentoLiquidoPositivo() {
        this.rendimentoLiquidoPositivo = this.getAnexoDq04C401b() != null && this.getAnexoDq04C401b() > 0 ? this.getAnexoDq04C401b() : 0;
        this.rendimentoLiquidoPositivo+=this.getAnexoDq04C402b() != null && this.getAnexoDq04C402b() > 0 ? this.getAnexoDq04C402b() : 0;
        this.rendimentoLiquidoPositivo+=this.getAnexoDq04C403b() != null && this.getAnexoDq04C403b() > 0 ? this.getAnexoDq04C403b() : 0;
        this.rendimentoLiquidoPositivo+=this.getAnexoDq04C431b() != null && this.getAnexoDq04C431b() > 0 ? this.getAnexoDq04C431b() : 0;
        this.rendimentoLiquidoPositivo+=this.getAnexoDq04C432b() != null && this.getAnexoDq04C432b() > 0 ? this.getAnexoDq04C432b() : 0;
        EventList<AnexoDq04T1_Linha> linhas = this.getAnexoDq04T1();
        for (AnexoDq04T1_Linha linha : linhas) {
            if (linha.getRendimentos() == null || linha.getRendimentos() <= 0) continue;
            this.rendimentoLiquidoPositivo+=linha.getRendimentos().longValue();
        }
    }

    public long getRendimentosLiquidosImputadosHerancaIndivisa(long nifEntidadeImputadora) {
        long result = 0;
        EventList<AnexoDq04T1_Linha> linhas = this.getAnexoDq04T1();
        for (AnexoDq04T1_Linha linha : linhas) {
            if (linha.getNIF() == null || linha.getNIF() != nifEntidadeImputadora) continue;
            result+=linha.getRendimentos() != null ? linha.getRendimentos() : 0;
        }
        return result;
    }

    public long getPercentImputacao(long nifEntidadeImputadora) {
        long result = 0;
        EventList<AnexoDq04T1_Linha> linhas = this.getAnexoDq04T1();
        for (AnexoDq04T1_Linha linha : linhas) {
            if (linha.getNIF() == null || linha.getNIF() != nifEntidadeImputadora) continue;
            result+=linha.getImputacao() != null ? linha.getImputacao() : 0;
        }
        return result;
    }

    public long getRetencoesHerancaIndivisa(long nifEntidadeImputadora) {
        long result = 0;
        EventList<AnexoDq04T1_Linha> linhas = this.getAnexoDq04T1();
        for (AnexoDq04T1_Linha linha : linhas) {
            if (linha.getNIF() == null || linha.getNIF() != nifEntidadeImputadora) continue;
            result+=linha.getRetencao() != null ? linha.getRetencao() : 0;
        }
        return result;
    }

    public AnexoDq04T1_Linha getLinhaByEntidadeImputadora(Long nif) {
        if (this.getAnexoDq04T1() != null) {
            for (AnexoDq04T1_Linha current : this.getAnexoDq04T1()) {
                if (current.getNIF() == null || nif == null || !current.getNIF().equals(nif)) continue;
                return current;
            }
        }
        return null;
    }

    public boolean isMontanteRendimentoCampoPreenchido(Long campoQ4) {
        if (campoQ4 == null) {
            return false;
        }
        long campo = campoQ4;
        if (!(campo != 401 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C401()))) {
            return true;
        }
        if (!(campo != 402 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C402()))) {
            return true;
        }
        if (!(campo != 403 || Modelo3IRSValidatorUtil.isEmptyOrZero(this.getAnexoDq04C403()))) {
            return true;
        }
        return false;
    }

    public void setAnexoDq04C1Formula_WithoutOverwriteBehavior() {
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoDq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoDq04T1().get(i).getRendimentos() == null ? 0 : this.getAnexoDq04T1().get(i).getRendimentos();
        }
        this.setAnexoDq04C1(newValue+=temporaryValue0 + (this.getAnexoDq04C401b() == null ? 0 : this.getAnexoDq04C401b()) + (this.getAnexoDq04C402b() == null ? 0 : this.getAnexoDq04C402b()) + (this.getAnexoDq04C403b() == null ? 0 : this.getAnexoDq04C403b()) + (this.getAnexoDq04C431b() == null ? 0 : this.getAnexoDq04C431b()) + (this.getAnexoDq04C432b() == null ? 0 : this.getAnexoDq04C432b()) + (this.getAnexoDq04C480b() == null ? 0 : this.getAnexoDq04C480b()) + (this.getAnexoDq04C481b() == null ? 0 : this.getAnexoDq04C481b()));
    }
}

