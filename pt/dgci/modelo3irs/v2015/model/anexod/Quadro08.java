/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.dgci.modelo3irs.v2015.model.anexod.Quadro08Base;

public class Quadro08
extends Quadro08Base {
    private static final long serialVersionUID = 8615074159074796641L;

    public boolean isEmpty() {
        return this.getAnexoDq08C801() == null || this.getAnexoDq08C801() == 0;
    }
}

