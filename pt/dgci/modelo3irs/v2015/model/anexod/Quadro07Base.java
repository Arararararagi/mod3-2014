/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoD.qQuadro07";
    public static final String ANEXODQ07C701_LINK = "aAnexoD.qQuadro07.fanexoDq07C701";
    public static final String ANEXODQ07C701 = "anexoDq07C701";
    private Long anexoDq07C701;
    public static final String ANEXODQ07C702_LINK = "aAnexoD.qQuadro07.fanexoDq07C702";
    public static final String ANEXODQ07C702 = "anexoDq07C702";
    private Long anexoDq07C702;
    public static final String ANEXODQ07C703_LINK = "aAnexoD.qQuadro07.fanexoDq07C703";
    public static final String ANEXODQ07C703 = "anexoDq07C703";
    private Long anexoDq07C703;
    public static final String ANEXODQ07C704_LINK = "aAnexoD.qQuadro07.fanexoDq07C704";
    public static final String ANEXODQ07C704 = "anexoDq07C704";
    private Long anexoDq07C704;
    public static final String ANEXODQ07C705_LINK = "aAnexoD.qQuadro07.fanexoDq07C705";
    public static final String ANEXODQ07C705 = "anexoDq07C705";
    private Long anexoDq07C705;
    public static final String ANEXODQ07C706_LINK = "aAnexoD.qQuadro07.fanexoDq07C706";
    public static final String ANEXODQ07C706 = "anexoDq07C706";
    private Long anexoDq07C706;
    public static final String ANEXODQ07C707_LINK = "aAnexoD.qQuadro07.fanexoDq07C707";
    public static final String ANEXODQ07C707 = "anexoDq07C707";
    private Long anexoDq07C707;
    public static final String ANEXODQ07C708_LINK = "aAnexoD.qQuadro07.fanexoDq07C708";
    public static final String ANEXODQ07C708 = "anexoDq07C708";
    private Long anexoDq07C708;
    public static final String ANEXODQ07C709_LINK = "aAnexoD.qQuadro07.fanexoDq07C709";
    public static final String ANEXODQ07C709 = "anexoDq07C709";
    private Long anexoDq07C709;
    public static final String ANEXODQ07C710_LINK = "aAnexoD.qQuadro07.fanexoDq07C710";
    public static final String ANEXODQ07C710 = "anexoDq07C710";
    private Long anexoDq07C710;
    public static final String ANEXODQ07C711_LINK = "aAnexoD.qQuadro07.fanexoDq07C711";
    public static final String ANEXODQ07C711 = "anexoDq07C711";
    private Long anexoDq07C711;
    public static final String ANEXODQ07C712_LINK = "aAnexoD.qQuadro07.fanexoDq07C712";
    public static final String ANEXODQ07C712 = "anexoDq07C712";
    private Long anexoDq07C712;
    public static final String ANEXODQ07C713_LINK = "aAnexoD.qQuadro07.fanexoDq07C713";
    public static final String ANEXODQ07C713 = "anexoDq07C713";
    private Long anexoDq07C713;
    public static final String ANEXODQ07C714_LINK = "aAnexoD.qQuadro07.fanexoDq07C714";
    public static final String ANEXODQ07C714 = "anexoDq07C714";
    private Long anexoDq07C714;
    public static final String ANEXODQ07C715_LINK = "aAnexoD.qQuadro07.fanexoDq07C715";
    public static final String ANEXODQ07C715 = "anexoDq07C715";
    private Long anexoDq07C715;
    public static final String ANEXODQ07C716_LINK = "aAnexoD.qQuadro07.fanexoDq07C716";
    public static final String ANEXODQ07C716 = "anexoDq07C716";
    private Long anexoDq07C716;
    public static final String ANEXODQ07C717_LINK = "aAnexoD.qQuadro07.fanexoDq07C717";
    public static final String ANEXODQ07C717 = "anexoDq07C717";
    private Long anexoDq07C717;
    public static final String ANEXODQ07C718_LINK = "aAnexoD.qQuadro07.fanexoDq07C718";
    public static final String ANEXODQ07C718 = "anexoDq07C718";
    private Long anexoDq07C718;
    public static final String ANEXODQ07C719_LINK = "aAnexoD.qQuadro07.fanexoDq07C719";
    public static final String ANEXODQ07C719 = "anexoDq07C719";
    private Long anexoDq07C719;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq07C701() {
        return this.anexoDq07C701;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq07C701(Long anexoDq07C701) {
        Long oldValue = this.anexoDq07C701;
        this.anexoDq07C701 = anexoDq07C701;
        this.firePropertyChange("anexoDq07C701", oldValue, this.anexoDq07C701);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq07C702() {
        return this.anexoDq07C702;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq07C702(Long anexoDq07C702) {
        Long oldValue = this.anexoDq07C702;
        this.anexoDq07C702 = anexoDq07C702;
        this.firePropertyChange("anexoDq07C702", oldValue, this.anexoDq07C702);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq07C703() {
        return this.anexoDq07C703;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq07C703(Long anexoDq07C703) {
        Long oldValue = this.anexoDq07C703;
        this.anexoDq07C703 = anexoDq07C703;
        this.firePropertyChange("anexoDq07C703", oldValue, this.anexoDq07C703);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq07C704() {
        return this.anexoDq07C704;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq07C704(Long anexoDq07C704) {
        Long oldValue = this.anexoDq07C704;
        this.anexoDq07C704 = anexoDq07C704;
        this.firePropertyChange("anexoDq07C704", oldValue, this.anexoDq07C704);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq07C705() {
        return this.anexoDq07C705;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq07C705(Long anexoDq07C705) {
        Long oldValue = this.anexoDq07C705;
        this.anexoDq07C705 = anexoDq07C705;
        this.firePropertyChange("anexoDq07C705", oldValue, this.anexoDq07C705);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq07C706() {
        return this.anexoDq07C706;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq07C706(Long anexoDq07C706) {
        Long oldValue = this.anexoDq07C706;
        this.anexoDq07C706 = anexoDq07C706;
        this.firePropertyChange("anexoDq07C706", oldValue, this.anexoDq07C706);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoDq07C707() {
        return this.anexoDq07C707;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoDq07C707(Long anexoDq07C707) {
        Long oldValue = this.anexoDq07C707;
        this.anexoDq07C707 = anexoDq07C707;
        this.firePropertyChange("anexoDq07C707", oldValue, this.anexoDq07C707);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C708() {
        return this.anexoDq07C708;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C708(Long anexoDq07C708) {
        Long oldValue = this.anexoDq07C708;
        this.anexoDq07C708 = anexoDq07C708;
        this.firePropertyChange("anexoDq07C708", oldValue, this.anexoDq07C708);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C709() {
        return this.anexoDq07C709;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C709(Long anexoDq07C709) {
        Long oldValue = this.anexoDq07C709;
        this.anexoDq07C709 = anexoDq07C709;
        this.firePropertyChange("anexoDq07C709", oldValue, this.anexoDq07C709);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C710() {
        return this.anexoDq07C710;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C710(Long anexoDq07C710) {
        Long oldValue = this.anexoDq07C710;
        this.anexoDq07C710 = anexoDq07C710;
        this.firePropertyChange("anexoDq07C710", oldValue, this.anexoDq07C710);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C711() {
        return this.anexoDq07C711;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C711(Long anexoDq07C711) {
        Long oldValue = this.anexoDq07C711;
        this.anexoDq07C711 = anexoDq07C711;
        this.firePropertyChange("anexoDq07C711", oldValue, this.anexoDq07C711);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C712() {
        return this.anexoDq07C712;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C712(Long anexoDq07C712) {
        Long oldValue = this.anexoDq07C712;
        this.anexoDq07C712 = anexoDq07C712;
        this.firePropertyChange("anexoDq07C712", oldValue, this.anexoDq07C712);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C713() {
        return this.anexoDq07C713;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C713(Long anexoDq07C713) {
        Long oldValue = this.anexoDq07C713;
        this.anexoDq07C713 = anexoDq07C713;
        this.firePropertyChange("anexoDq07C713", oldValue, this.anexoDq07C713);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C714() {
        return this.anexoDq07C714;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C714(Long anexoDq07C714) {
        Long oldValue = this.anexoDq07C714;
        this.anexoDq07C714 = anexoDq07C714;
        this.firePropertyChange("anexoDq07C714", oldValue, this.anexoDq07C714);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C715() {
        return this.anexoDq07C715;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C715(Long anexoDq07C715) {
        Long oldValue = this.anexoDq07C715;
        this.anexoDq07C715 = anexoDq07C715;
        this.firePropertyChange("anexoDq07C715", oldValue, this.anexoDq07C715);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C716() {
        return this.anexoDq07C716;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C716(Long anexoDq07C716) {
        Long oldValue = this.anexoDq07C716;
        this.anexoDq07C716 = anexoDq07C716;
        this.firePropertyChange("anexoDq07C716", oldValue, this.anexoDq07C716);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C717() {
        return this.anexoDq07C717;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C717(Long anexoDq07C717) {
        Long oldValue = this.anexoDq07C717;
        this.anexoDq07C717 = anexoDq07C717;
        this.firePropertyChange("anexoDq07C717", oldValue, this.anexoDq07C717);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C718() {
        return this.anexoDq07C718;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C718(Long anexoDq07C718) {
        Long oldValue = this.anexoDq07C718;
        this.anexoDq07C718 = anexoDq07C718;
        this.firePropertyChange("anexoDq07C718", oldValue, this.anexoDq07C718);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq07C719() {
        return this.anexoDq07C719;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq07C719(Long anexoDq07C719) {
        Long oldValue = this.anexoDq07C719;
        this.anexoDq07C719 = anexoDq07C719;
        this.firePropertyChange("anexoDq07C719", oldValue, this.anexoDq07C719);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq07C701);
        result = HashCodeUtil.hash(result, this.anexoDq07C702);
        result = HashCodeUtil.hash(result, this.anexoDq07C703);
        result = HashCodeUtil.hash(result, this.anexoDq07C704);
        result = HashCodeUtil.hash(result, this.anexoDq07C705);
        result = HashCodeUtil.hash(result, this.anexoDq07C706);
        result = HashCodeUtil.hash(result, this.anexoDq07C707);
        result = HashCodeUtil.hash(result, this.anexoDq07C708);
        result = HashCodeUtil.hash(result, this.anexoDq07C709);
        result = HashCodeUtil.hash(result, this.anexoDq07C710);
        result = HashCodeUtil.hash(result, this.anexoDq07C711);
        result = HashCodeUtil.hash(result, this.anexoDq07C712);
        result = HashCodeUtil.hash(result, this.anexoDq07C713);
        result = HashCodeUtil.hash(result, this.anexoDq07C714);
        result = HashCodeUtil.hash(result, this.anexoDq07C715);
        result = HashCodeUtil.hash(result, this.anexoDq07C716);
        result = HashCodeUtil.hash(result, this.anexoDq07C717);
        result = HashCodeUtil.hash(result, this.anexoDq07C718);
        result = HashCodeUtil.hash(result, this.anexoDq07C719);
        return result;
    }
}

