/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoD.qQuadro05";
    public static final String ANEXODQ05C501_LINK = "aAnexoD.qQuadro05.fanexoDq05C501";
    public static final String ANEXODQ05C501 = "anexoDq05C501";
    private Long anexoDq05C501;
    public static final String ANEXODQ05C502_LINK = "aAnexoD.qQuadro05.fanexoDq05C502";
    public static final String ANEXODQ05C502 = "anexoDq05C502";
    private Long anexoDq05C502;
    public static final String ANEXODQ05C503_LINK = "aAnexoD.qQuadro05.fanexoDq05C503";
    public static final String ANEXODQ05C503 = "anexoDq05C503";
    private Long anexoDq05C503;
    public static final String ANEXODQ05C504_LINK = "aAnexoD.qQuadro05.fanexoDq05C504";
    public static final String ANEXODQ05C504 = "anexoDq05C504";
    private Long anexoDq05C504;
    public static final String ANEXODQ05C505_LINK = "aAnexoD.qQuadro05.fanexoDq05C505";
    public static final String ANEXODQ05C505 = "anexoDq05C505";
    private Long anexoDq05C505;
    public static final String ANEXODQ05C506_LINK = "aAnexoD.qQuadro05.fanexoDq05C506";
    public static final String ANEXODQ05C506 = "anexoDq05C506";
    private Long anexoDq05C506;
    public static final String ANEXODQ05C507_LINK = "aAnexoD.qQuadro05.fanexoDq05C507";
    public static final String ANEXODQ05C507 = "anexoDq05C507";
    private Long anexoDq05C507;
    public static final String ANEXODQ05C508_LINK = "aAnexoD.qQuadro05.fanexoDq05C508";
    public static final String ANEXODQ05C508 = "anexoDq05C508";
    private Long anexoDq05C508;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C501() {
        return this.anexoDq05C501;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C501(Long anexoDq05C501) {
        Long oldValue = this.anexoDq05C501;
        this.anexoDq05C501 = anexoDq05C501;
        this.firePropertyChange("anexoDq05C501", oldValue, this.anexoDq05C501);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C502() {
        return this.anexoDq05C502;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C502(Long anexoDq05C502) {
        Long oldValue = this.anexoDq05C502;
        this.anexoDq05C502 = anexoDq05C502;
        this.firePropertyChange("anexoDq05C502", oldValue, this.anexoDq05C502);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C503() {
        return this.anexoDq05C503;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C503(Long anexoDq05C503) {
        Long oldValue = this.anexoDq05C503;
        this.anexoDq05C503 = anexoDq05C503;
        this.firePropertyChange("anexoDq05C503", oldValue, this.anexoDq05C503);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C504() {
        return this.anexoDq05C504;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C504(Long anexoDq05C504) {
        Long oldValue = this.anexoDq05C504;
        this.anexoDq05C504 = anexoDq05C504;
        this.firePropertyChange("anexoDq05C504", oldValue, this.anexoDq05C504);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C505() {
        return this.anexoDq05C505;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C505(Long anexoDq05C505) {
        Long oldValue = this.anexoDq05C505;
        this.anexoDq05C505 = anexoDq05C505;
        this.firePropertyChange("anexoDq05C505", oldValue, this.anexoDq05C505);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C506() {
        return this.anexoDq05C506;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C506(Long anexoDq05C506) {
        Long oldValue = this.anexoDq05C506;
        this.anexoDq05C506 = anexoDq05C506;
        this.firePropertyChange("anexoDq05C506", oldValue, this.anexoDq05C506);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C507() {
        return this.anexoDq05C507;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C507(Long anexoDq05C507) {
        Long oldValue = this.anexoDq05C507;
        this.anexoDq05C507 = anexoDq05C507;
        this.firePropertyChange("anexoDq05C507", oldValue, this.anexoDq05C507);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq05C508() {
        return this.anexoDq05C508;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq05C508(Long anexoDq05C508) {
        Long oldValue = this.anexoDq05C508;
        this.anexoDq05C508 = anexoDq05C508;
        this.firePropertyChange("anexoDq05C508", oldValue, this.anexoDq05C508);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq05C501);
        result = HashCodeUtil.hash(result, this.anexoDq05C502);
        result = HashCodeUtil.hash(result, this.anexoDq05C503);
        result = HashCodeUtil.hash(result, this.anexoDq05C504);
        result = HashCodeUtil.hash(result, this.anexoDq05C505);
        result = HashCodeUtil.hash(result, this.anexoDq05C506);
        result = HashCodeUtil.hash(result, this.anexoDq05C507);
        result = HashCodeUtil.hash(result, this.anexoDq05C508);
        return result;
    }
}

