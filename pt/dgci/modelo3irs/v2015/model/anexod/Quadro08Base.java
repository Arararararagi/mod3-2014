/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro08Base
extends QuadroModel {
    public static final String QUADRO08_LINK = "aAnexoD.qQuadro08";
    public static final String ANEXODQ08C801_LINK = "aAnexoD.qQuadro08.fanexoDq08C801";
    public static final String ANEXODQ08C801 = "anexoDq08C801";
    private Long anexoDq08C801;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq08C801() {
        return this.anexoDq08C801;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq08C801(Long anexoDq08C801) {
        Long oldValue = this.anexoDq08C801;
        this.anexoDq08C801 = anexoDq08C801;
        this.firePropertyChange("anexoDq08C801", oldValue, this.anexoDq08C801);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq08C801);
        return result;
    }
}

