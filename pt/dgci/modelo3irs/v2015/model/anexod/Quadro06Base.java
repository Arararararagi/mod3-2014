/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexod;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoD.qQuadro06";
    public static final String ANEXODQ06C601_LINK = "aAnexoD.qQuadro06.fanexoDq06C601";
    public static final String ANEXODQ06C601 = "anexoDq06C601";
    private Long anexoDq06C601;
    public static final String ANEXODQ06C602_LINK = "aAnexoD.qQuadro06.fanexoDq06C602";
    public static final String ANEXODQ06C602 = "anexoDq06C602";
    private Long anexoDq06C602;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq06C601() {
        return this.anexoDq06C601;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq06C601(Long anexoDq06C601) {
        Long oldValue = this.anexoDq06C601;
        this.anexoDq06C601 = anexoDq06C601;
        this.firePropertyChange("anexoDq06C601", oldValue, this.anexoDq06C601);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoDq06C602() {
        return this.anexoDq06C602;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoDq06C602(Long anexoDq06C602) {
        Long oldValue = this.anexoDq06C602;
        this.anexoDq06C602 = anexoDq06C602;
        this.firePropertyChange("anexoDq06C602", oldValue, this.anexoDq06C602);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoDq06C601);
        result = HashCodeUtil.hash(result, this.anexoDq06C602);
        return result;
    }
}

