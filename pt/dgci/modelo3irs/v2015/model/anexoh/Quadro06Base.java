/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoH.qQuadro06";
    public static final String ANEXOHQ06C601_LINK = "aAnexoH.qQuadro06.fanexoHq06C601";
    public static final String ANEXOHQ06C601 = "anexoHq06C601";
    private Long anexoHq06C601;
    public static final String ANEXOHQ06C602_LINK = "aAnexoH.qQuadro06.fanexoHq06C602";
    public static final String ANEXOHQ06C602 = "anexoHq06C602";
    private Long anexoHq06C602;
    public static final String ANEXOHQ06C603_LINK = "aAnexoH.qQuadro06.fanexoHq06C603";
    public static final String ANEXOHQ06C603 = "anexoHq06C603";
    private Long anexoHq06C603;
    public static final String ANEXOHQ06C1_LINK = "aAnexoH.qQuadro06.fanexoHq06C1";
    public static final String ANEXOHQ06C1 = "anexoHq06C1";
    private Long anexoHq06C1;
    public static final String ANEXOHQ06T1_LINK = "aAnexoH.qQuadro06.tanexoHq06T1";
    private EventList<AnexoHq06T1_Linha> anexoHq06T1 = new BasicEventList<AnexoHq06T1_Linha>();

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq06C601() {
        return this.anexoHq06C601;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq06C601(Long anexoHq06C601) {
        Long oldValue = this.anexoHq06C601;
        this.anexoHq06C601 = anexoHq06C601;
        this.firePropertyChange("anexoHq06C601", oldValue, this.anexoHq06C601);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq06C602() {
        return this.anexoHq06C602;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq06C602(Long anexoHq06C602) {
        Long oldValue = this.anexoHq06C602;
        this.anexoHq06C602 = anexoHq06C602;
        this.firePropertyChange("anexoHq06C602", oldValue, this.anexoHq06C602);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq06C603() {
        return this.anexoHq06C603;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq06C603(Long anexoHq06C603) {
        Long oldValue = this.anexoHq06C603;
        this.anexoHq06C603 = anexoHq06C603;
        this.firePropertyChange("anexoHq06C603", oldValue, this.anexoHq06C603);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq06C1() {
        return this.anexoHq06C1;
    }

    public void setAnexoHq06C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoHq06C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoHq06C1(newValue+=(this.getAnexoHq06C601() == null ? 0 : this.getAnexoHq06C601()) + (this.getAnexoHq06C602() == null ? 0 : this.getAnexoHq06C602()) + (this.getAnexoHq06C603() == null ? 0 : this.getAnexoHq06C603()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq06C1(Long anexoHq06C1) {
        Long oldValue = this.anexoHq06C1;
        this.anexoHq06C1 = anexoHq06C1;
        this.firePropertyChange("anexoHq06C1", oldValue, this.anexoHq06C1);
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoHq06T1_Linha> getAnexoHq06T1() {
        return this.anexoHq06T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoHq06T1(EventList<AnexoHq06T1_Linha> anexoHq06T1) {
        this.anexoHq06T1 = anexoHq06T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq06C601);
        result = HashCodeUtil.hash(result, this.anexoHq06C602);
        result = HashCodeUtil.hash(result, this.anexoHq06C603);
        result = HashCodeUtil.hash(result, this.anexoHq06C1);
        result = HashCodeUtil.hash(result, this.anexoHq06T1);
        return result;
    }
}

