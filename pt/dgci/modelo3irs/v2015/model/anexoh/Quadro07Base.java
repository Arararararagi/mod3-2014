/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoH.qQuadro07";
    public static final String ANEXOHQ07T7_LINK = "aAnexoH.qQuadro07.tanexoHq07T7";
    private EventList<AnexoHq07T7_Linha> anexoHq07T7 = new BasicEventList<AnexoHq07T7_Linha>();
    public static final String ANEXOHQ07C1_LINK = "aAnexoH.qQuadro07.fanexoHq07C1";
    public static final String ANEXOHQ07C1 = "anexoHq07C1";
    private Long anexoHq07C1;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoHq07T7_Linha> getAnexoHq07T7() {
        return this.anexoHq07T7;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoHq07T7(EventList<AnexoHq07T7_Linha> anexoHq07T7) {
        this.anexoHq07T7 = anexoHq07T7;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq07C1() {
        return this.anexoHq07C1;
    }

    public void setAnexoHq07C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoHq07C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoHq07T7().size(); ++i) {
            temporaryValue0+=this.getAnexoHq07T7().get(i).getImportanciaAplicada() == null ? 0 : this.getAnexoHq07T7().get(i).getImportanciaAplicada();
        }
        this.setAnexoHq07C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq07C1(Long anexoHq07C1) {
        Long oldValue = this.anexoHq07C1;
        this.anexoHq07C1 = anexoHq07C1;
        this.firePropertyChange("anexoHq07C1", oldValue, this.anexoHq07C1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq07T7);
        result = HashCodeUtil.hash(result, this.anexoHq07C1);
        return result;
    }
}

