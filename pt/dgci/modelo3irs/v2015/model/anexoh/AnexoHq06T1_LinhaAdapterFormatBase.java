/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;

public class AnexoHq06T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoHq06T1_Linha>,
WritableTableFormat<AnexoHq06T1_Linha> {
    @Override
    public boolean isEditable(AnexoHq06T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoHq06T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getNifBeneficiarios();
            }
            case 2: {
                return line.getValor();
            }
        }
        return null;
    }

    @Override
    public AnexoHq06T1_Linha setColumnValue(AnexoHq06T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setNifBeneficiarios((Long)value);
                return line;
            }
            case 2: {
                line.setValor((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "NIF dos Benefici\u00e1rios das Pens\u00f5es";
            }
            case 2: {
                return "Valor";
            }
        }
        return null;
    }
}

