/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro10Base
extends QuadroModel {
    public static final String QUADRO10_LINK = "aAnexoH.qQuadro10";
    public static final String ANEXOHQ10C1001_LINK = "aAnexoH.qQuadro10.fanexoHq10C1001";
    public static final String ANEXOHQ10C1001 = "anexoHq10C1001";
    private Long anexoHq10C1001;
    public static final String ANEXOHQ10C1001A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1001a";
    public static final String ANEXOHQ10C1001A = "anexoHq10C1001a";
    private Long anexoHq10C1001a;
    public static final String ANEXOHQ10C1002_LINK = "aAnexoH.qQuadro10.fanexoHq10C1002";
    public static final String ANEXOHQ10C1002 = "anexoHq10C1002";
    private Long anexoHq10C1002;
    public static final String ANEXOHQ10C1002A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1002a";
    public static final String ANEXOHQ10C1002A = "anexoHq10C1002a";
    private Long anexoHq10C1002a;
    public static final String ANEXOHQ10C1003_LINK = "aAnexoH.qQuadro10.fanexoHq10C1003";
    public static final String ANEXOHQ10C1003 = "anexoHq10C1003";
    private Long anexoHq10C1003;
    public static final String ANEXOHQ10C1003A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1003a";
    public static final String ANEXOHQ10C1003A = "anexoHq10C1003a";
    private Long anexoHq10C1003a;
    public static final String ANEXOHQ10C1004_LINK = "aAnexoH.qQuadro10.fanexoHq10C1004";
    public static final String ANEXOHQ10C1004 = "anexoHq10C1004";
    private Long anexoHq10C1004;
    public static final String ANEXOHQ10C1004A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1004a";
    public static final String ANEXOHQ10C1004A = "anexoHq10C1004a";
    private Long anexoHq10C1004a;
    public static final String ANEXOHQ10C1005_LINK = "aAnexoH.qQuadro10.fanexoHq10C1005";
    public static final String ANEXOHQ10C1005 = "anexoHq10C1005";
    private Long anexoHq10C1005;
    public static final String ANEXOHQ10C1005A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1005a";
    public static final String ANEXOHQ10C1005A = "anexoHq10C1005a";
    private Long anexoHq10C1005a;
    public static final String ANEXOHQ10C1006_LINK = "aAnexoH.qQuadro10.fanexoHq10C1006";
    public static final String ANEXOHQ10C1006 = "anexoHq10C1006";
    private Long anexoHq10C1006;
    public static final String ANEXOHQ10C1006A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1006a";
    public static final String ANEXOHQ10C1006A = "anexoHq10C1006a";
    private Long anexoHq10C1006a;
    public static final String ANEXOHQ10C1007_LINK = "aAnexoH.qQuadro10.fanexoHq10C1007";
    public static final String ANEXOHQ10C1007 = "anexoHq10C1007";
    private Long anexoHq10C1007;
    public static final String ANEXOHQ10C1007A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1007a";
    public static final String ANEXOHQ10C1007A = "anexoHq10C1007a";
    private Long anexoHq10C1007a;
    public static final String ANEXOHQ10C1008_LINK = "aAnexoH.qQuadro10.fanexoHq10C1008";
    public static final String ANEXOHQ10C1008 = "anexoHq10C1008";
    private Long anexoHq10C1008;
    public static final String ANEXOHQ10C1008A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1008a";
    public static final String ANEXOHQ10C1008A = "anexoHq10C1008a";
    private Long anexoHq10C1008a;
    public static final String ANEXOHQ10C1009_LINK = "aAnexoH.qQuadro10.fanexoHq10C1009";
    public static final String ANEXOHQ10C1009 = "anexoHq10C1009";
    private Long anexoHq10C1009;
    public static final String ANEXOHQ10C1_LINK = "aAnexoH.qQuadro10.fanexoHq10C1";
    public static final String ANEXOHQ10C1 = "anexoHq10C1";
    private Long anexoHq10C1;
    public static final String ANEXOHQ10C1A_LINK = "aAnexoH.qQuadro10.fanexoHq10C1a";
    public static final String ANEXOHQ10C1A = "anexoHq10C1a";
    private Long anexoHq10C1a;

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1001() {
        return this.anexoHq10C1001;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1001(Long anexoHq10C1001) {
        Long oldValue = this.anexoHq10C1001;
        this.anexoHq10C1001 = anexoHq10C1001;
        this.firePropertyChange("anexoHq10C1001", oldValue, this.anexoHq10C1001);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1001a() {
        return this.anexoHq10C1001a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1001a(Long anexoHq10C1001a) {
        Long oldValue = this.anexoHq10C1001a;
        this.anexoHq10C1001a = anexoHq10C1001a;
        this.firePropertyChange("anexoHq10C1001a", oldValue, this.anexoHq10C1001a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1002() {
        return this.anexoHq10C1002;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1002(Long anexoHq10C1002) {
        Long oldValue = this.anexoHq10C1002;
        this.anexoHq10C1002 = anexoHq10C1002;
        this.firePropertyChange("anexoHq10C1002", oldValue, this.anexoHq10C1002);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1002a() {
        return this.anexoHq10C1002a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1002a(Long anexoHq10C1002a) {
        Long oldValue = this.anexoHq10C1002a;
        this.anexoHq10C1002a = anexoHq10C1002a;
        this.firePropertyChange("anexoHq10C1002a", oldValue, this.anexoHq10C1002a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1003() {
        return this.anexoHq10C1003;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1003(Long anexoHq10C1003) {
        Long oldValue = this.anexoHq10C1003;
        this.anexoHq10C1003 = anexoHq10C1003;
        this.firePropertyChange("anexoHq10C1003", oldValue, this.anexoHq10C1003);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1003a() {
        return this.anexoHq10C1003a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1003a(Long anexoHq10C1003a) {
        Long oldValue = this.anexoHq10C1003a;
        this.anexoHq10C1003a = anexoHq10C1003a;
        this.firePropertyChange("anexoHq10C1003a", oldValue, this.anexoHq10C1003a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1004() {
        return this.anexoHq10C1004;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1004(Long anexoHq10C1004) {
        Long oldValue = this.anexoHq10C1004;
        this.anexoHq10C1004 = anexoHq10C1004;
        this.firePropertyChange("anexoHq10C1004", oldValue, this.anexoHq10C1004);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1004a() {
        return this.anexoHq10C1004a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1004a(Long anexoHq10C1004a) {
        Long oldValue = this.anexoHq10C1004a;
        this.anexoHq10C1004a = anexoHq10C1004a;
        this.firePropertyChange("anexoHq10C1004a", oldValue, this.anexoHq10C1004a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1005() {
        return this.anexoHq10C1005;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1005(Long anexoHq10C1005) {
        Long oldValue = this.anexoHq10C1005;
        this.anexoHq10C1005 = anexoHq10C1005;
        this.firePropertyChange("anexoHq10C1005", oldValue, this.anexoHq10C1005);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1005a() {
        return this.anexoHq10C1005a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1005a(Long anexoHq10C1005a) {
        Long oldValue = this.anexoHq10C1005a;
        this.anexoHq10C1005a = anexoHq10C1005a;
        this.firePropertyChange("anexoHq10C1005a", oldValue, this.anexoHq10C1005a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1006() {
        return this.anexoHq10C1006;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1006(Long anexoHq10C1006) {
        Long oldValue = this.anexoHq10C1006;
        this.anexoHq10C1006 = anexoHq10C1006;
        this.firePropertyChange("anexoHq10C1006", oldValue, this.anexoHq10C1006);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1006a() {
        return this.anexoHq10C1006a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1006a(Long anexoHq10C1006a) {
        Long oldValue = this.anexoHq10C1006a;
        this.anexoHq10C1006a = anexoHq10C1006a;
        this.firePropertyChange("anexoHq10C1006a", oldValue, this.anexoHq10C1006a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1007() {
        return this.anexoHq10C1007;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1007(Long anexoHq10C1007) {
        Long oldValue = this.anexoHq10C1007;
        this.anexoHq10C1007 = anexoHq10C1007;
        this.firePropertyChange("anexoHq10C1007", oldValue, this.anexoHq10C1007);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1007a() {
        return this.anexoHq10C1007a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1007a(Long anexoHq10C1007a) {
        Long oldValue = this.anexoHq10C1007a;
        this.anexoHq10C1007a = anexoHq10C1007a;
        this.firePropertyChange("anexoHq10C1007a", oldValue, this.anexoHq10C1007a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1008() {
        return this.anexoHq10C1008;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1008(Long anexoHq10C1008) {
        Long oldValue = this.anexoHq10C1008;
        this.anexoHq10C1008 = anexoHq10C1008;
        this.firePropertyChange("anexoHq10C1008", oldValue, this.anexoHq10C1008);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1008a() {
        return this.anexoHq10C1008a;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1008a(Long anexoHq10C1008a) {
        Long oldValue = this.anexoHq10C1008a;
        this.anexoHq10C1008a = anexoHq10C1008a;
        this.firePropertyChange("anexoHq10C1008a", oldValue, this.anexoHq10C1008a);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1009() {
        return this.anexoHq10C1009;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1009(Long anexoHq10C1009) {
        Long oldValue = this.anexoHq10C1009;
        this.anexoHq10C1009 = anexoHq10C1009;
        this.firePropertyChange("anexoHq10C1009", oldValue, this.anexoHq10C1009);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1() {
        return this.anexoHq10C1;
    }

    public void setAnexoHq10C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoHq10C1")) {
            return;
        }
        long newValue = 0;
        this.setAnexoHq10C1(newValue+=(this.getAnexoHq10C1001() == null ? 0 : this.getAnexoHq10C1001()) + (this.getAnexoHq10C1002() == null ? 0 : this.getAnexoHq10C1002()) + (this.getAnexoHq10C1003() == null ? 0 : this.getAnexoHq10C1003()) + (this.getAnexoHq10C1004() == null ? 0 : this.getAnexoHq10C1004()) + (this.getAnexoHq10C1005() == null ? 0 : this.getAnexoHq10C1005()) + (this.getAnexoHq10C1006() == null ? 0 : this.getAnexoHq10C1006()) + (this.getAnexoHq10C1007() == null ? 0 : this.getAnexoHq10C1007()) + (this.getAnexoHq10C1008() == null ? 0 : this.getAnexoHq10C1008()) + (this.getAnexoHq10C1009() == null ? 0 : this.getAnexoHq10C1009()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1(Long anexoHq10C1) {
        Long oldValue = this.anexoHq10C1;
        this.anexoHq10C1 = anexoHq10C1;
        this.firePropertyChange("anexoHq10C1", oldValue, this.anexoHq10C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq10C1a() {
        return this.anexoHq10C1a;
    }

    public void setAnexoHq10C1aFormula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoHq10C1a")) {
            return;
        }
        long newValue = 0;
        this.setAnexoHq10C1a(newValue+=(this.getAnexoHq10C1001a() == null ? 0 : this.getAnexoHq10C1001a()) + (this.getAnexoHq10C1002a() == null ? 0 : this.getAnexoHq10C1002a()) + (this.getAnexoHq10C1003a() == null ? 0 : this.getAnexoHq10C1003a()) + (this.getAnexoHq10C1004a() == null ? 0 : this.getAnexoHq10C1004a()) + (this.getAnexoHq10C1005a() == null ? 0 : this.getAnexoHq10C1005a()) + (this.getAnexoHq10C1006a() == null ? 0 : this.getAnexoHq10C1006a()) + (this.getAnexoHq10C1007a() == null ? 0 : this.getAnexoHq10C1007a()) + (this.getAnexoHq10C1008a() == null ? 0 : this.getAnexoHq10C1008a()));
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq10C1a(Long anexoHq10C1a) {
        Long oldValue = this.anexoHq10C1a;
        this.anexoHq10C1a = anexoHq10C1a;
        this.firePropertyChange("anexoHq10C1a", oldValue, this.anexoHq10C1a);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq10C1001);
        result = HashCodeUtil.hash(result, this.anexoHq10C1001a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1002);
        result = HashCodeUtil.hash(result, this.anexoHq10C1002a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1003);
        result = HashCodeUtil.hash(result, this.anexoHq10C1003a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1004);
        result = HashCodeUtil.hash(result, this.anexoHq10C1004a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1005);
        result = HashCodeUtil.hash(result, this.anexoHq10C1005a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1006);
        result = HashCodeUtil.hash(result, this.anexoHq10C1006a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1007);
        result = HashCodeUtil.hash(result, this.anexoHq10C1007a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1008);
        result = HashCodeUtil.hash(result, this.anexoHq10C1008a);
        result = HashCodeUtil.hash(result, this.anexoHq10C1009);
        result = HashCodeUtil.hash(result, this.anexoHq10C1);
        result = HashCodeUtil.hash(result, this.anexoHq10C1a);
        return result;
    }
}

