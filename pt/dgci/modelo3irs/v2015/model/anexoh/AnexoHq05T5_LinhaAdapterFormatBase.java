/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoHq05T5_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoHq05T5_Linha>,
WritableTableFormat<AnexoHq05T5_Linha> {
    @Override
    public boolean isEditable(AnexoHq05T5_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoHq05T5_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepEFalecidos", line.getTitular());
            }
            case 2: {
                return line.getMontanteRendimento();
            }
        }
        return null;
    }

    @Override
    public AnexoHq05T5_Linha setColumnValue(AnexoHq05T5_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setMontanteRendimento((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
            case 2: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Titular";
            }
            case 2: {
                return "Montante do Rendimento";
            }
        }
        return null;
    }
}

