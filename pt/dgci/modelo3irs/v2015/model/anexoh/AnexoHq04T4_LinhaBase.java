/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoHq04T4_LinhaBase
extends Model {
    public static final String CODRENDIMENTOS_LINK = "aAnexoH.qQuadro04.fcodRendimentos";
    public static final String CODRENDIMENTOS = "codRendimentos";
    private Long codRendimentos;
    public static final String TITULAR_LINK = "aAnexoH.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String RENDIMENTOSILIQUIDOS_LINK = "aAnexoH.qQuadro04.frendimentosIliquidos";
    public static final String RENDIMENTOSILIQUIDOS = "rendimentosIliquidos";
    private Long rendimentosIliquidos;
    public static final String RENTENCAOIRS_LINK = "aAnexoH.qQuadro04.frentencaoIRS";
    public static final String RENTENCAOIRS = "rentencaoIRS";
    private Long rentencaoIRS;
    public static final String NIFPORTUGUES_LINK = "aAnexoH.qQuadro04.fnifPortugues";
    public static final String NIFPORTUGUES = "nifPortugues";
    private Long nifPortugues;
    public static final String PAIS_LINK = "aAnexoH.qQuadro04.fpais";
    public static final String PAIS = "pais";
    private Long pais;
    public static final String NUMEROFISCALUE_LINK = "aAnexoH.qQuadro04.fnumeroFiscalUE";
    public static final String NUMEROFISCALUE = "numeroFiscalUE";
    private String numeroFiscalUE;

    public AnexoHq04T4_LinhaBase(Long codRendimentos, String titular, Long rendimentosIliquidos, Long rentencaoIRS, Long nifPortugues, Long pais, String numeroFiscalUE) {
        this.codRendimentos = codRendimentos;
        this.titular = titular;
        this.rendimentosIliquidos = rendimentosIliquidos;
        this.rentencaoIRS = rentencaoIRS;
        this.nifPortugues = nifPortugues;
        this.pais = pais;
        this.numeroFiscalUE = numeroFiscalUE;
    }

    public AnexoHq04T4_LinhaBase() {
        this.codRendimentos = null;
        this.titular = null;
        this.rendimentosIliquidos = null;
        this.rentencaoIRS = null;
        this.nifPortugues = null;
        this.pais = null;
        this.numeroFiscalUE = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoHQ4")
    public Long getCodRendimentos() {
        return this.codRendimentos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodRendimentos(Long codRendimentos) {
        Long oldValue = this.codRendimentos;
        this.codRendimentos = codRendimentos;
        this.firePropertyChange("codRendimentos", oldValue, this.codRendimentos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimentosIliquidos() {
        return this.rendimentosIliquidos;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimentosIliquidos(Long rendimentosIliquidos) {
        Long oldValue = this.rendimentosIliquidos;
        this.rendimentosIliquidos = rendimentosIliquidos;
        this.firePropertyChange("rendimentosIliquidos", oldValue, this.rendimentosIliquidos);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRentencaoIRS() {
        return this.rentencaoIRS;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRentencaoIRS(Long rentencaoIRS) {
        Long oldValue = this.rentencaoIRS;
        this.rentencaoIRS = rentencaoIRS;
        this.firePropertyChange("rentencaoIRS", oldValue, this.rentencaoIRS);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifPortugues() {
        return this.nifPortugues;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifPortugues(Long nifPortugues) {
        Long oldValue = this.nifPortugues;
        this.nifPortugues = nifPortugues;
        this.firePropertyChange("nifPortugues", oldValue, this.nifPortugues);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxH_Q04")
    public Long getPais() {
        return this.pais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setPais(Long pais) {
        Long oldValue = this.pais;
        this.pais = pais;
        this.firePropertyChange("pais", oldValue, this.pais);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getNumeroFiscalUE() {
        return this.numeroFiscalUE;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNumeroFiscalUE(String numeroFiscalUE) {
        String oldValue = this.numeroFiscalUE;
        this.numeroFiscalUE = numeroFiscalUE;
        this.firePropertyChange("numeroFiscalUE", oldValue, this.numeroFiscalUE);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.codRendimentos);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.rendimentosIliquidos);
        result = HashCodeUtil.hash(result, this.rentencaoIRS);
        result = HashCodeUtil.hash(result, this.nifPortugues);
        result = HashCodeUtil.hash(result, this.pais);
        result = HashCodeUtil.hash(result, this.numeroFiscalUE);
        return result;
    }

    public static enum Property {
        CODRENDIMENTOS("codRendimentos", 2),
        TITULAR("titular", 3),
        RENDIMENTOSILIQUIDOS("rendimentosIliquidos", 4),
        RENTENCAOIRS("rentencaoIRS", 5),
        NIFPORTUGUES("nifPortugues", 6),
        PAIS("pais", 7),
        NUMEROFISCALUE("numeroFiscalUE", 8);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

