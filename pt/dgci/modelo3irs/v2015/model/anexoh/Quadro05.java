/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro05Base;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;

public class Quadro05
extends Quadro05Base {
    public boolean isEmpty() {
        return this.getAnexoHq05T5().isEmpty();
    }

    public long getTotalRendimentos() {
        long total = 0;
        EventList<AnexoHq05T5_Linha> linhas = this.getAnexoHq05T5();
        for (AnexoHq05T5_Linha linha : linhas) {
            total+=linha.getMontanteRendimento() != null ? linha.getMontanteRendimento() : 0;
        }
        return total;
    }

    public Long getRendimentoByNif(Long nifTitular, RostoModel rostoModel) {
        EventList<AnexoHq05T5_Linha> linhas = this.getAnexoHq05T5();
        if (linhas.isEmpty()) {
            return null;
        }
        for (AnexoHq05T5_Linha linha : linhas) {
            if (linha.getTitular() == null || rostoModel.getNIFTitular(linha.getTitular()) == null) continue;
            Long nif = rostoModel.getNIFTitular(linha.getTitular());
            if (nifTitular.longValue() != nif.longValue()) continue;
            return linha.getMontanteRendimento();
        }
        return null;
    }

    public String getLinkRendimentoByNif(Long nifTitular, RostoModel rostoModel) {
        EventList<AnexoHq05T5_Linha> linhas = this.getAnexoHq05T5();
        if (linhas.isEmpty()) {
            return null;
        }
        for (int i = 0; i < linhas.size(); ++i) {
            AnexoHq05T5_Linha linha = linhas.get(i);
            if (linha.getTitular() == null || rostoModel.getNIFTitular(linha.getTitular()) == null) continue;
            Long nif = rostoModel.getNIFTitular(linha.getTitular());
            if (nifTitular.longValue() != nif.longValue()) continue;
            String linkCurrentLine = "aAnexoH.qQuadro05.tanexoHq05T5.l" + (i + 1);
            String linkMontanteRendimento = linkCurrentLine + ".c3";
            return linkMontanteRendimento;
        }
        return null;
    }
}

