/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoH.qQuadro05";
    public static final String ANEXOHQ05T5_LINK = "aAnexoH.qQuadro05.tanexoHq05T5";
    private EventList<AnexoHq05T5_Linha> anexoHq05T5 = new BasicEventList<AnexoHq05T5_Linha>();
    public static final String ANEXOHQ05C1_LINK = "aAnexoH.qQuadro05.fanexoHq05C1";
    public static final String ANEXOHQ05C1 = "anexoHq05C1";
    private Long anexoHq05C1;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoHq05T5_Linha> getAnexoHq05T5() {
        return this.anexoHq05T5;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoHq05T5(EventList<AnexoHq05T5_Linha> anexoHq05T5) {
        this.anexoHq05T5 = anexoHq05T5;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq05C1() {
        return this.anexoHq05C1;
    }

    public void setAnexoHq05C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoHq05C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoHq05T5().size(); ++i) {
            temporaryValue0+=this.getAnexoHq05T5().get(i).getMontanteRendimento() == null ? 0 : this.getAnexoHq05T5().get(i).getMontanteRendimento();
        }
        this.setAnexoHq05C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq05C1(Long anexoHq05C1) {
        Long oldValue = this.anexoHq05C1;
        this.anexoHq05C1 = anexoHq05C1;
        this.firePropertyChange("anexoHq05C1", oldValue, this.anexoHq05C1);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq05T5);
        result = HashCodeUtil.hash(result, this.anexoHq05C1);
        return result;
    }
}

