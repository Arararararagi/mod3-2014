/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro10Base;

public class Quadro10
extends Quadro10Base {
    public boolean isEmpty() {
        return (this.getAnexoHq10C1() == null || this.getAnexoHq10C1() == 0) && (this.getAnexoHq10C1a() == null || this.getAnexoHq10C1a() == 0);
    }
}

