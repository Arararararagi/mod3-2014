/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro08Base;

public class Quadro08
extends Quadro08Base {
    private static final long serialVersionUID = 4523761330425954821L;

    public boolean existsAtLeastOneCodigo(long[] codigos) {
        for (AnexoHq08T814_Linha anexoHq08T814Linha : this.getAnexoHq08T814()) {
            if (anexoHq08T814Linha.getCodigo() == null) continue;
            for (long codigo : codigos) {
                if (anexoHq08T814Linha.getCodigo() != codigo) continue;
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return this.getAnexoHq08T1().isEmpty() && this.getAnexoHq08T814().isEmpty();
    }

    public static String getLinkRowForANEXOHQ08T1_LINK(int linhaPos) {
        return "aAnexoH.qQuadro08.tanexoHq08T1.l" + linhaPos;
    }

    public static String getLinkCellForANEXOHQ08T1_LINK(int linhaPos, AnexoHq08T1_LinhaBase.Property property) {
        int index = property.getIndex();
        return Quadro08.getLinkRowForANEXOHQ08T1_LINK(linhaPos) + ".c" + index;
    }
}

