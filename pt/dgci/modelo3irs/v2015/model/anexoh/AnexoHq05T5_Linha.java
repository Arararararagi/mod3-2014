/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq05T5_LinhaBase;

public class AnexoHq05T5_Linha
extends AnexoHq05T5_LinhaBase {
    public AnexoHq05T5_Linha() {
    }

    @Deprecated
    public AnexoHq05T5_Linha(Long codigo, String titular, Long montanteRendimento) {
        super(titular, montanteRendimento);
    }

    public AnexoHq05T5_Linha(String titular, Long montanteRendimento) {
        super(titular, montanteRendimento);
    }

    public static String getLink(int numLinha) {
        return "aAnexoH.qQuadro05.tanexoHq05T5.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoHq05T5_LinhaBase.Property column) {
        return "aAnexoH.qQuadro05.tanexoHq05T5.l" + numLinha + ".c" + column.getIndex();
    }
}

