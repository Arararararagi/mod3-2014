/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoHq08T814_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoHq08T814_Linha>,
WritableTableFormat<AnexoHq08T814_Linha> {
    @Override
    public boolean isEditable(AnexoHq08T814_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoHq08T814_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCodigo() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_BeneficiosAnexoHQ8", line.getCodigo());
            }
            case 2: {
                return line.getFreguesia();
            }
            case 3: {
                if (line == null || line.getTipoPredio() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_TipoPredio_UrbanoOmisso", line.getTipoPredio());
            }
            case 4: {
                return line.getArtigo();
            }
            case 5: {
                return line.getFraccao();
            }
            case 6: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepSPCEFalecidos", line.getTitular());
            }
            case 7: {
                if (line == null || line.getHabitacaoPermanenteArrendada() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_PermanenteArrendada", line.getHabitacaoPermanenteArrendada());
            }
            case 8: {
                return line.getNIFArrendatario();
            }
            case 9: {
                if (line == null || line.getClassificacaoA() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_SimNao", line.getClassificacaoA());
            }
        }
        return null;
    }

    @Override
    public AnexoHq08T814_Linha setColumnValue(AnexoHq08T814_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCodigo((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setFreguesia((String)value);
                return line;
            }
            case 3: {
                if (value != null) {
                    line.setTipoPredio((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 4: {
                line.setArtigo((Long)value);
                return line;
            }
            case 5: {
                line.setFraccao((String)value);
                return line;
            }
            case 6: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 7: {
                if (value != null) {
                    line.setHabitacaoPermanenteArrendada((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 8: {
                line.setNIFArrendatario((Long)value);
                return line;
            }
            case 9: {
                if (value != null) {
                    line.setClassificacaoA((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return String.class;
            }
            case 6: {
                return String.class;
            }
            case 7: {
                return String.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 10;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "C\u00f3digo";
            }
            case 2: {
                return "Freguesia";
            }
            case 3: {
                return "Tipo";
            }
            case 4: {
                return "Artigo";
            }
            case 5: {
                return "Fra\u00e7\u00e3o";
            }
            case 6: {
                return "Titular";
            }
            case 7: {
                return "Habita\u00e7\u00e3o Permanente/Arrendada";
            }
            case 8: {
                return "NIF do Arrendat\u00e1rio/locador";
            }
            case 9: {
                return "Classifica\u00e7\u00e3o A / A+";
            }
        }
        return null;
    }
}

