/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoHq08T1_LinhaBase
extends Model {
    public static final String NIF_LINK = "aAnexoH.qQuadro08.fnIF";
    public static final String NIF = "nIF";
    private String nIF;
    public static final String ANEXOHQ08C801_LINK = "aAnexoH.qQuadro08.fanexoHq08C801";
    public static final String ANEXOHQ08C801 = "anexoHq08C801";
    private Long anexoHq08C801;
    public static final String ANEXOHQ08C802_LINK = "aAnexoH.qQuadro08.fanexoHq08C802";
    public static final String ANEXOHQ08C802 = "anexoHq08C802";
    private Long anexoHq08C802;
    public static final String ANEXOHQ08C803_LINK = "aAnexoH.qQuadro08.fanexoHq08C803";
    public static final String ANEXOHQ08C803 = "anexoHq08C803";
    private Long anexoHq08C803;

    public AnexoHq08T1_LinhaBase(String nIF, Long anexoHq08C801, Long anexoHq08C802, Long anexoHq08C803) {
        this.nIF = nIF;
        this.anexoHq08C801 = anexoHq08C801;
        this.anexoHq08C802 = anexoHq08C802;
        this.anexoHq08C803 = anexoHq08C803;
    }

    public AnexoHq08T1_LinhaBase() {
        this.nIF = null;
        this.anexoHq08C801 = null;
        this.anexoHq08C802 = null;
        this.anexoHq08C803 = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepAfiAscEFalecidos")
    public String getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(String nIF) {
        String oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq08C801() {
        return this.anexoHq08C801;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq08C801(Long anexoHq08C801) {
        Long oldValue = this.anexoHq08C801;
        this.anexoHq08C801 = anexoHq08C801;
        this.firePropertyChange("anexoHq08C801", oldValue, this.anexoHq08C801);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq08C802() {
        return this.anexoHq08C802;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq08C802(Long anexoHq08C802) {
        Long oldValue = this.anexoHq08C802;
        this.anexoHq08C802 = anexoHq08C802;
        this.firePropertyChange("anexoHq08C802", oldValue, this.anexoHq08C802);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq08C803() {
        return this.anexoHq08C803;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq08C803(Long anexoHq08C803) {
        Long oldValue = this.anexoHq08C803;
        this.anexoHq08C803 = anexoHq08C803;
        this.firePropertyChange("anexoHq08C803", oldValue, this.anexoHq08C803);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.anexoHq08C801);
        result = HashCodeUtil.hash(result, this.anexoHq08C802);
        result = HashCodeUtil.hash(result, this.anexoHq08C803);
        return result;
    }

    public static enum Property {
        NIF("nIF", 2),
        ANEXOHQ08C801("anexoHq08C801", 3),
        ANEXOHQ08C802("anexoHq08C802", 4),
        ANEXOHQ08C803("anexoHq08C803", 5);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

