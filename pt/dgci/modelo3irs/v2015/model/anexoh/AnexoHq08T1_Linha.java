/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_LinhaBase;

public class AnexoHq08T1_Linha
extends AnexoHq08T1_LinhaBase {
    public AnexoHq08T1_Linha() {
    }

    public AnexoHq08T1_Linha(String nIF, Long anexoHq08C801, Long anexoHq08C802, Long anexoHq08C803) {
        super(nIF, anexoHq08C801, anexoHq08C802, anexoHq08C803);
    }

    public static String getLink(int numLinha) {
        return "aAnexoH.qQuadro08.tanexoHq08T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoHq08T1_LinhaBase.Property column) {
        return "aAnexoH.qQuadro08.tanexoHq08T1.l" + numLinha + ".c" + column.getIndex();
    }
}

