/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_LinhaBase;

public class AnexoHq06T1_Linha
extends AnexoHq06T1_LinhaBase {
    public AnexoHq06T1_Linha() {
    }

    public AnexoHq06T1_Linha(Long nifBeneficiarios, Long valor) {
        super(nifBeneficiarios, valor);
    }

    public static String getLink(int numLinha) {
        return "aAnexoH.qQuadro06.tanexoHq06T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoHq06T1_LinhaBase.Property column) {
        return "aAnexoH.qQuadro06.tanexoHq06T1.l" + numLinha + ".c" + column.getIndex();
    }
}

