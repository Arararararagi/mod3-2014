/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoHq06T1_LinhaBase
extends Model {
    public static final String NIFBENEFICIARIOS_LINK = "aAnexoH.qQuadro06.fnifBeneficiarios";
    public static final String NIFBENEFICIARIOS = "nifBeneficiarios";
    private Long nifBeneficiarios;
    public static final String VALOR_LINK = "aAnexoH.qQuadro06.fvalor";
    public static final String VALOR = "valor";
    private Long valor;

    public AnexoHq06T1_LinhaBase(Long nifBeneficiarios, Long valor) {
        this.nifBeneficiarios = nifBeneficiarios;
        this.valor = valor;
    }

    public AnexoHq06T1_LinhaBase() {
        this.nifBeneficiarios = null;
        this.valor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifBeneficiarios() {
        return this.nifBeneficiarios;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifBeneficiarios(Long nifBeneficiarios) {
        Long oldValue = this.nifBeneficiarios;
        this.nifBeneficiarios = nifBeneficiarios;
        this.firePropertyChange("nifBeneficiarios", oldValue, this.nifBeneficiarios);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getValor() {
        return this.valor;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setValor(Long valor) {
        Long oldValue = this.valor;
        this.valor = valor;
        this.firePropertyChange("valor", oldValue, this.valor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nifBeneficiarios);
        result = HashCodeUtil.hash(result, this.valor);
        return result;
    }

    public static enum Property {
        NIFBENEFICIARIOS("nifBeneficiarios", 2),
        VALOR("valor", 3);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

