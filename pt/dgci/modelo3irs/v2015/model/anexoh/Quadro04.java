/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro04Base;

public class Quadro04
extends Quadro04Base {
    public long getTotalRendimentosIliquidosPreenchidos() {
        long totalRendimentosIliquidosPreenchidos = 0;
        for (AnexoHq04T4_Linha anexoHq04T4Linha : this.getAnexoHq04T4()) {
            if (anexoHq04T4Linha.getRendimentosIliquidos() == null) continue;
            totalRendimentosIliquidosPreenchidos+=anexoHq04T4Linha.getRendimentosIliquidos().longValue();
        }
        return totalRendimentosIliquidosPreenchidos;
    }

    public long getTotalRetencaoIRSPreenchido() {
        long totalRetencaoIRSPreenchido = 0;
        for (AnexoHq04T4_Linha anexoHq04T4Linha : this.getAnexoHq04T4()) {
            if (anexoHq04T4Linha.getRentencaoIRS() == null) continue;
            totalRetencaoIRSPreenchido+=anexoHq04T4Linha.getRentencaoIRS().longValue();
        }
        return totalRetencaoIRSPreenchido;
    }

    public boolean isEmpty() {
        return this.getAnexoHq04T4().isEmpty();
    }
}

