/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoHq04T4_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoHq04T4_Linha>,
WritableTableFormat<AnexoHq04T4_Linha> {
    @Override
    public boolean isEditable(AnexoHq04T4_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoHq04T4_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCodRendimentos() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoHQ4", line.getCodRendimentos());
            }
            case 2: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepEFalecidos", line.getTitular());
            }
            case 3: {
                return line.getRendimentosIliquidos();
            }
            case 4: {
                return line.getRentencaoIRS();
            }
            case 5: {
                return line.getNifPortugues();
            }
            case 6: {
                if (line == null || line.getPais() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_Pais_AnxH_Q04", line.getPais());
            }
            case 7: {
                return line.getNumeroFiscalUE();
            }
        }
        return null;
    }

    @Override
    public AnexoHq04T4_Linha setColumnValue(AnexoHq04T4_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCodRendimentos((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 3: {
                line.setRendimentosIliquidos((Long)value);
                return line;
            }
            case 4: {
                line.setRentencaoIRS((Long)value);
                return line;
            }
            case 5: {
                line.setNifPortugues((Long)value);
                return line;
            }
            case 6: {
                if (value != null) {
                    line.setPais((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 7: {
                line.setNumeroFiscalUE((String)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
            case 7: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "C\u00f3digo do Rendimento";
            }
            case 2: {
                return "Titular";
            }
            case 3: {
                return "Rendimentos";
            }
            case 4: {
                return "Reten\u00e7\u00e3o de IRS";
            }
            case 5: {
                return "NIF/NIPC Portugu\u00eas";
            }
            case 6: {
                return "Pa\u00eds";
            }
            case 7: {
                return "N\u00famero Fiscal(UE ou EEE)";
            }
        }
        return null;
    }
}

