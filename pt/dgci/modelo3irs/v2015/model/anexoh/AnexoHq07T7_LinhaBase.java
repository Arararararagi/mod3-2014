/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoHq07T7_LinhaBase
extends Model {
    public static final String CODBENEFICIO_LINK = "aAnexoH.qQuadro07.fcodBeneficio";
    public static final String CODBENEFICIO = "codBeneficio";
    private Long codBeneficio;
    public static final String TITULAR_LINK = "aAnexoH.qQuadro07.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String IMPORTANCIAAPLICADA_LINK = "aAnexoH.qQuadro07.fimportanciaAplicada";
    public static final String IMPORTANCIAAPLICADA = "importanciaAplicada";
    private Long importanciaAplicada;
    public static final String NIFNIPCPORTUGUES_LINK = "aAnexoH.qQuadro07.fnifNipcPortugues";
    public static final String NIFNIPCPORTUGUES = "nifNipcPortugues";
    private Long nifNipcPortugues;
    public static final String PAIS_LINK = "aAnexoH.qQuadro07.fpais";
    public static final String PAIS = "pais";
    private Long pais;
    public static final String NUMEROFISCALUE_LINK = "aAnexoH.qQuadro07.fnumeroFiscalUE";
    public static final String NUMEROFISCALUE = "numeroFiscalUE";
    private String numeroFiscalUE;

    public AnexoHq07T7_LinhaBase(Long codBeneficio, String titular, Long importanciaAplicada, Long nifNipcPortugues, Long pais, String numeroFiscalUE) {
        this.codBeneficio = codBeneficio;
        this.titular = titular;
        this.importanciaAplicada = importanciaAplicada;
        this.nifNipcPortugues = nifNipcPortugues;
        this.pais = pais;
        this.numeroFiscalUE = numeroFiscalUE;
    }

    public AnexoHq07T7_LinhaBase() {
        this.codBeneficio = null;
        this.titular = null;
        this.importanciaAplicada = null;
        this.nifNipcPortugues = null;
        this.pais = null;
        this.numeroFiscalUE = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Beneficios")
    public Long getCodBeneficio() {
        return this.codBeneficio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodBeneficio(Long codBeneficio) {
        Long oldValue = this.codBeneficio;
        this.codBeneficio = codBeneficio;
        this.firePropertyChange("codBeneficio", oldValue, this.codBeneficio);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepAfiAscSPCEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getImportanciaAplicada() {
        return this.importanciaAplicada;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setImportanciaAplicada(Long importanciaAplicada) {
        Long oldValue = this.importanciaAplicada;
        this.importanciaAplicada = importanciaAplicada;
        this.firePropertyChange("importanciaAplicada", oldValue, this.importanciaAplicada);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifNipcPortugues() {
        return this.nifNipcPortugues;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifNipcPortugues(Long nifNipcPortugues) {
        Long oldValue = this.nifNipcPortugues;
        this.nifNipcPortugues = nifNipcPortugues;
        this.firePropertyChange("nifNipcPortugues", oldValue, this.nifNipcPortugues);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxH_Q04")
    public Long getPais() {
        return this.pais;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setPais(Long pais) {
        Long oldValue = this.pais;
        this.pais = pais;
        this.firePropertyChange("pais", oldValue, this.pais);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getNumeroFiscalUE() {
        return this.numeroFiscalUE;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNumeroFiscalUE(String numeroFiscalUE) {
        String oldValue = this.numeroFiscalUE;
        this.numeroFiscalUE = numeroFiscalUE;
        this.firePropertyChange("numeroFiscalUE", oldValue, this.numeroFiscalUE);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.codBeneficio);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.importanciaAplicada);
        result = HashCodeUtil.hash(result, this.nifNipcPortugues);
        result = HashCodeUtil.hash(result, this.pais);
        result = HashCodeUtil.hash(result, this.numeroFiscalUE);
        return result;
    }

    public static enum Property {
        CODBENEFICIO("codBeneficio", 2),
        TITULAR("titular", 3),
        IMPORTANCIAAPLICADA("importanciaAplicada", 4),
        NIFNIPCPORTUGUES("nifNipcPortugues", 5),
        PAIS("pais", 6),
        NUMEROFISCALUE("numeroFiscalUE", 7);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

