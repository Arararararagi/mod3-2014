/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro09Base
extends QuadroModel {
    public static final String QUADRO09_LINK = "aAnexoH.qQuadro09";
    public static final String ANEXOHQ09B1OP1_LINK = "aAnexoH.qQuadro09.fanexoHq09B1OP1";
    public static final String ANEXOHQ09B1OP1_VALUE = "1";
    public static final String ANEXOHQ09B1OP2_LINK = "aAnexoH.qQuadro09.fanexoHq09B1OP2";
    public static final String ANEXOHQ09B1OP2_VALUE = "2";
    public static final String ANEXOHQ09B1 = "anexoHq09B1";
    private String anexoHq09B1;
    public static final String ANEXOHQ09C901_LINK = "aAnexoH.qQuadro09.fanexoHq09C901";
    public static final String ANEXOHQ09C901 = "anexoHq09C901";
    private Long anexoHq09C901;
    public static final String ANEXOHQ09C901IRS_LINK = "aAnexoH.qQuadro09.fanexoHq09C901IRS";
    public static final String ANEXOHQ09C901IRS = "anexoHq09C901IRS";
    private Boolean anexoHq09C901IRS;
    public static final String ANEXOHQ09C901IVA_LINK = "aAnexoH.qQuadro09.fanexoHq09C901IVA";
    public static final String ANEXOHQ09C901IVA = "anexoHq09C901IVA";
    private Boolean anexoHq09C901IVA;

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoHq09B1() {
        return this.anexoHq09B1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq09B1(String anexoHq09B1) {
        String oldValue = this.anexoHq09B1;
        this.anexoHq09B1 = anexoHq09B1;
        this.firePropertyChange("anexoHq09B1", oldValue, this.anexoHq09B1);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoHq09C901() {
        return this.anexoHq09C901;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq09C901(Long anexoHq09C901) {
        Long oldValue = this.anexoHq09C901;
        this.anexoHq09C901 = anexoHq09C901;
        this.firePropertyChange("anexoHq09C901", oldValue, this.anexoHq09C901);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoHq09C901IRS() {
        return this.anexoHq09C901IRS;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq09C901IRS(Boolean anexoHq09C901IRS) {
        Boolean oldValue = this.anexoHq09C901IRS;
        this.anexoHq09C901IRS = anexoHq09C901IRS;
        this.firePropertyChange("anexoHq09C901IRS", oldValue, this.anexoHq09C901IRS);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Boolean getAnexoHq09C901IVA() {
        return this.anexoHq09C901IVA;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq09C901IVA(Boolean anexoHq09C901IVA) {
        Boolean oldValue = this.anexoHq09C901IVA;
        this.anexoHq09C901IVA = anexoHq09C901IVA;
        this.firePropertyChange("anexoHq09C901IVA", oldValue, this.anexoHq09C901IVA);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq09B1);
        result = HashCodeUtil.hash(result, this.anexoHq09C901);
        result = HashCodeUtil.hash(result, this.anexoHq09C901IRS);
        result = HashCodeUtil.hash(result, this.anexoHq09C901IVA);
        return result;
    }
}

