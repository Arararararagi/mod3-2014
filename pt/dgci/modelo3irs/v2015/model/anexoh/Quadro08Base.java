/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro08Base
extends QuadroModel {
    public static final String QUADRO08_LINK = "aAnexoH.qQuadro08";
    public static final String ANEXOHQ08T1_LINK = "aAnexoH.qQuadro08.tanexoHq08T1";
    private EventList<AnexoHq08T1_Linha> anexoHq08T1 = new BasicEventList<AnexoHq08T1_Linha>();
    public static final String ANEXOHQ08T814_LINK = "aAnexoH.qQuadro08.tanexoHq08T814";
    private EventList<AnexoHq08T814_Linha> anexoHq08T814 = new BasicEventList<AnexoHq08T814_Linha>();
    public static final String ANEXOHQ08C814_LINK = "aAnexoH.qQuadro08.fanexoHq08C814";
    public static final String ANEXOHQ08C814 = "anexoHq08C814";
    private Long anexoHq08C814;
    public static final String ANEXOHQ08C81501_LINK = "aAnexoH.qQuadro08.fanexoHq08C81501";
    public static final String ANEXOHQ08C81501 = "anexoHq08C81501";
    private Long anexoHq08C81501;
    public static final String ANEXOHQ08C81502_LINK = "aAnexoH.qQuadro08.fanexoHq08C81502";
    public static final String ANEXOHQ08C81502 = "anexoHq08C81502";
    private Long anexoHq08C81502;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoHq08T1_Linha> getAnexoHq08T1() {
        return this.anexoHq08T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoHq08T1(EventList<AnexoHq08T1_Linha> anexoHq08T1) {
        this.anexoHq08T1 = anexoHq08T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoHq08T814_Linha> getAnexoHq08T814() {
        return this.anexoHq08T814;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoHq08T814(EventList<AnexoHq08T814_Linha> anexoHq08T814) {
        this.anexoHq08T814 = anexoHq08T814;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Pais_AnxH")
    public Long getAnexoHq08C814() {
        return this.anexoHq08C814;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq08C814(Long anexoHq08C814) {
        Long oldValue = this.anexoHq08C814;
        this.anexoHq08C814 = anexoHq08C814;
        this.firePropertyChange("anexoHq08C814", oldValue, this.anexoHq08C814);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq08C81501() {
        return this.anexoHq08C81501;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq08C81501(Long anexoHq08C81501) {
        Long oldValue = this.anexoHq08C81501;
        this.anexoHq08C81501 = anexoHq08C81501;
        this.firePropertyChange("anexoHq08C81501", oldValue, this.anexoHq08C81501);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq08C81502() {
        return this.anexoHq08C81502;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq08C81502(Long anexoHq08C81502) {
        Long oldValue = this.anexoHq08C81502;
        this.anexoHq08C81502 = anexoHq08C81502;
        this.firePropertyChange("anexoHq08C81502", oldValue, this.anexoHq08C81502);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq08T1);
        result = HashCodeUtil.hash(result, this.anexoHq08T814);
        result = HashCodeUtil.hash(result, this.anexoHq08C814);
        result = HashCodeUtil.hash(result, this.anexoHq08C81501);
        result = HashCodeUtil.hash(result, this.anexoHq08C81502);
        return result;
    }
}

