/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_LinhaBase;

public class AnexoHq04T4_Linha
extends AnexoHq04T4_LinhaBase {
    public AnexoHq04T4_Linha() {
    }

    public AnexoHq04T4_Linha(Long codRendimentos, String titular, Long rendimentosIliquidos, Long rentencaoIRS, Long nifEntidadePagadora, Long pais, String numeroFiscalUE) {
        super(codRendimentos, titular, rendimentosIliquidos, rentencaoIRS, nifEntidadePagadora, pais, numeroFiscalUE);
    }

    public static String getLink(int numLinha) {
        return "aAnexoH.qQuadro04.tanexoHq04T4.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoHq04T4_LinhaBase.Property column) {
        return "aAnexoH.qQuadro04.tanexoHq04T4.l" + numLinha + ".c" + column.getIndex();
    }
}

