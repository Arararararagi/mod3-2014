/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_LinhaBase;

public class AnexoHq08T814_Linha
extends AnexoHq08T814_LinhaBase {
    public AnexoHq08T814_Linha() {
    }

    public AnexoHq08T814_Linha(Long codigo, String freguesia, String tipoPredio, Long artigo, String fraccao, String titular, String habitacaoPermanenteArrendada, Long nIFArrendatario, String classificacaoA) {
        super(codigo, freguesia, tipoPredio, artigo, fraccao, titular, habitacaoPermanenteArrendada, nIFArrendatario, classificacaoA);
    }

    public static String getLink(int numLinha) {
        return "aAnexoH.qQuadro08.tanexoHq08T814.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoHq08T814_LinhaBase.Property column) {
        return "aAnexoH.qQuadro08.tanexoHq08T814.l" + numLinha + ".c" + column.getIndex();
    }
}

