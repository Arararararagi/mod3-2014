/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoH.qQuadro04";
    public static final String ANEXOHQ04T4_LINK = "aAnexoH.qQuadro04.tanexoHq04T4";
    private EventList<AnexoHq04T4_Linha> anexoHq04T4 = new BasicEventList<AnexoHq04T4_Linha>();
    public static final String ANEXOHQ04C1_LINK = "aAnexoH.qQuadro04.fanexoHq04C1";
    public static final String ANEXOHQ04C1 = "anexoHq04C1";
    private Long anexoHq04C1;
    public static final String ANEXOHQ04C2_LINK = "aAnexoH.qQuadro04.fanexoHq04C2";
    public static final String ANEXOHQ04C2 = "anexoHq04C2";
    private Long anexoHq04C2;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoHq04T4_Linha> getAnexoHq04T4() {
        return this.anexoHq04T4;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoHq04T4(EventList<AnexoHq04T4_Linha> anexoHq04T4) {
        this.anexoHq04T4 = anexoHq04T4;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq04C1() {
        return this.anexoHq04C1;
    }

    public void setAnexoHq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoHq04C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoHq04T4().size(); ++i) {
            temporaryValue0+=this.getAnexoHq04T4().get(i).getRendimentosIliquidos() == null ? 0 : this.getAnexoHq04T4().get(i).getRendimentosIliquidos();
        }
        this.setAnexoHq04C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq04C1(Long anexoHq04C1) {
        Long oldValue = this.anexoHq04C1;
        this.anexoHq04C1 = anexoHq04C1;
        this.firePropertyChange("anexoHq04C1", oldValue, this.anexoHq04C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoHq04C2() {
        return this.anexoHq04C2;
    }

    public void setAnexoHq04C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoHq04C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoHq04T4().size(); ++i) {
            temporaryValue0+=this.getAnexoHq04T4().get(i).getRentencaoIRS() == null ? 0 : this.getAnexoHq04T4().get(i).getRentencaoIRS();
        }
        this.setAnexoHq04C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoHq04C2(Long anexoHq04C2) {
        Long oldValue = this.anexoHq04C2;
        this.anexoHq04C2 = anexoHq04C2;
        this.firePropertyChange("anexoHq04C2", oldValue, this.anexoHq04C2);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq04T4);
        result = HashCodeUtil.hash(result, this.anexoHq04C1);
        result = HashCodeUtil.hash(result, this.anexoHq04C2);
        return result;
    }
}

