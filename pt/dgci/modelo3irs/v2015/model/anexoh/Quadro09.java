/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro09Base;

public class Quadro09
extends Quadro09Base {
    public boolean isAnexoHq09B1OP1Selected() {
        return this.getAnexoHq09B1() != null && this.getAnexoHq09B1().equals("1");
    }

    public boolean isAnexoHq09B1OP2Selected() {
        return this.getAnexoHq09B1() != null && this.getAnexoHq09B1().equals("2");
    }

    public boolean isEmpty() {
        return this.getAnexoHq09B1() == null && this.getAnexoHq09C901() == null && this.getAnexoHq09C901IRS() == null && this.getAnexoHq09C901IVA() == null;
    }
}

