/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoHq05T5_LinhaBase
extends Model {
    public static final String TITULAR_LINK = "aAnexoH.qQuadro05.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String MONTANTERENDIMENTO_LINK = "aAnexoH.qQuadro05.fmontanteRendimento";
    public static final String MONTANTERENDIMENTO = "montanteRendimento";
    private Long montanteRendimento;

    public AnexoHq05T5_LinhaBase(String titular, Long montanteRendimento) {
        this.titular = titular;
        this.montanteRendimento = montanteRendimento;
    }

    public AnexoHq05T5_LinhaBase() {
        this.titular = null;
        this.montanteRendimento = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getMontanteRendimento() {
        return this.montanteRendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setMontanteRendimento(Long montanteRendimento) {
        Long oldValue = this.montanteRendimento;
        this.montanteRendimento = montanteRendimento;
        this.firePropertyChange("montanteRendimento", oldValue, this.montanteRendimento);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.montanteRendimento);
        return result;
    }

    public static enum Property {
        TITULAR("titular", 2),
        MONTANTERENDIMENTO("montanteRendimento", 3);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

