/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoH.qQuadro02";
    public static final String ANEXOHQ02C01_LINK = "aAnexoH.qQuadro02.fanexoHq02C01";
    public static final String ANEXOHQ02C01 = "anexoHq02C01";
    private Long anexoHq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoHq02C01() {
        return this.anexoHq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq02C01(Long anexoHq02C01) {
        Long oldValue = this.anexoHq02C01;
        this.anexoHq02C01 = anexoHq02C01;
        this.firePropertyChange("anexoHq02C01", oldValue, this.anexoHq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq02C01);
        return result;
    }
}

