/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro07Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro07
extends Quadro07Base {
    public boolean existsAtLeastOneCodigoBeneficio(long[] codigosBeneficio) {
        for (AnexoHq07T7_Linha anexoHq04T7Linha : this.getAnexoHq07T7()) {
            if (anexoHq04T7Linha.getCodBeneficio() == null) continue;
            for (long codigoBeneficio : codigosBeneficio) {
                if (anexoHq04T7Linha.getCodBeneficio() != codigoBeneficio) continue;
                return true;
            }
        }
        return false;
    }

    public boolean existsAtLeastOneCodigoBeneficioTitular(long[] codigosBeneficio, String titular) {
        for (AnexoHq07T7_Linha anexoHq04T7Linha : this.getAnexoHq07T7()) {
            if (anexoHq04T7Linha.getCodBeneficio() == null || !Modelo3IRSValidatorUtil.equals(anexoHq04T7Linha.getTitular(), titular)) continue;
            for (long codigoBeneficio : codigosBeneficio) {
                if (anexoHq04T7Linha.getCodBeneficio() != codigoBeneficio) continue;
                return true;
            }
        }
        return false;
    }

    public boolean existsAtLeastOneCodigoBeneficioTitular(long[] codigosBeneficio, String[] titulares) {
        for (AnexoHq07T7_Linha anexoHq04T7Linha : this.getAnexoHq07T7()) {
            if (anexoHq04T7Linha.getCodBeneficio() == null) continue;
            for (String titular : titulares) {
                if (!Modelo3IRSValidatorUtil.equals(anexoHq04T7Linha.getTitular(), titular)) continue;
                for (long codigoBeneficio : codigosBeneficio) {
                    if (anexoHq04T7Linha.getCodBeneficio() != codigoBeneficio) continue;
                    return true;
                }
            }
        }
        return false;
    }

    public long getTotalImportanciaAplicadaPreenchida() {
        long totalImportanciaAplicada = 0;
        for (AnexoHq07T7_Linha anexoHq04T7Linha : this.getAnexoHq07T7()) {
            if (anexoHq04T7Linha.getImportanciaAplicada() == null) continue;
            totalImportanciaAplicada+=anexoHq04T7Linha.getImportanciaAplicada().longValue();
        }
        return totalImportanciaAplicada;
    }

    public long getTotalImportanciaAplicadaByCodigoBeneficio(long codBeneficio) {
        long totalImportanciaAplicada = 0;
        for (AnexoHq07T7_Linha anexoHq04T7Linha : this.getAnexoHq07T7()) {
            if (anexoHq04T7Linha.getImportanciaAplicada() == null || anexoHq04T7Linha.getCodBeneficio() == null || anexoHq04T7Linha.getCodBeneficio() != codBeneficio) continue;
            totalImportanciaAplicada+=anexoHq04T7Linha.getImportanciaAplicada().longValue();
        }
        return totalImportanciaAplicada;
    }

    public boolean existsNif(long[] nifs) {
        for (AnexoHq07T7_Linha anexoHq04T7Linha : this.getAnexoHq07T7()) {
            if (anexoHq04T7Linha.getNifNipcPortugues() == null) continue;
            for (long nif : nifs) {
                if (anexoHq04T7Linha.getNifNipcPortugues() != nif) continue;
                return true;
            }
        }
        return false;
    }

    public long getNOcorrenciasByCodigoBenificio(long codBeneficio) {
        long ocorrencias = 0;
        for (AnexoHq07T7_Linha anexoHq04T7Linha : this.getAnexoHq07T7()) {
            if (anexoHq04T7Linha.getCodBeneficio() == null || anexoHq04T7Linha.getCodBeneficio() != codBeneficio) continue;
            ++ocorrencias;
        }
        return ocorrencias;
    }

    public boolean isEmpty() {
        return this.getAnexoHq07T7().isEmpty();
    }
}

