/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoHq08T814_LinhaBase
extends Model {
    public static final String CODIGO_LINK = "aAnexoH.qQuadro08.fcodigo";
    public static final String CODIGO = "codigo";
    private Long codigo;
    public static final String FREGUESIA_LINK = "aAnexoH.qQuadro08.ffreguesia";
    public static final String FREGUESIA = "freguesia";
    private String freguesia;
    public static final String TIPOPREDIO_LINK = "aAnexoH.qQuadro08.ftipoPredio";
    public static final String TIPOPREDIO = "tipoPredio";
    private String tipoPredio;
    public static final String ARTIGO_LINK = "aAnexoH.qQuadro08.fartigo";
    public static final String ARTIGO = "artigo";
    private Long artigo;
    public static final String FRACCAO_LINK = "aAnexoH.qQuadro08.ffraccao";
    public static final String FRACCAO = "fraccao";
    private String fraccao;
    public static final String TITULAR_LINK = "aAnexoH.qQuadro08.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String HABITACAOPERMANENTEARRENDADA_LINK = "aAnexoH.qQuadro08.fhabitacaoPermanenteArrendada";
    public static final String HABITACAOPERMANENTEARRENDADA = "habitacaoPermanenteArrendada";
    private String habitacaoPermanenteArrendada;
    public static final String NIFARRENDATARIO_LINK = "aAnexoH.qQuadro08.fnIFArrendatario";
    public static final String NIFARRENDATARIO = "nIFArrendatario";
    private Long nIFArrendatario;
    public static final String CLASSIFICACAOA_LINK = "aAnexoH.qQuadro08.fclassificacaoA";
    public static final String CLASSIFICACAOA = "classificacaoA";
    private String classificacaoA;

    public AnexoHq08T814_LinhaBase(Long codigo, String freguesia, String tipoPredio, Long artigo, String fraccao, String titular, String habitacaoPermanenteArrendada, Long nIFArrendatario, String classificacaoA) {
        this.codigo = codigo;
        this.freguesia = freguesia;
        this.tipoPredio = tipoPredio;
        this.artigo = artigo;
        this.fraccao = fraccao;
        this.titular = titular;
        this.habitacaoPermanenteArrendada = habitacaoPermanenteArrendada;
        this.nIFArrendatario = nIFArrendatario;
        this.classificacaoA = classificacaoA;
    }

    public AnexoHq08T814_LinhaBase() {
        this.codigo = null;
        this.freguesia = null;
        this.tipoPredio = null;
        this.artigo = null;
        this.fraccao = null;
        this.titular = null;
        this.habitacaoPermanenteArrendada = null;
        this.nIFArrendatario = null;
        this.classificacaoA = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_BeneficiosAnexoHQ8")
    public Long getCodigo() {
        return this.codigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCodigo(Long codigo) {
        Long oldValue = this.codigo;
        this.codigo = codigo;
        this.firePropertyChange("codigo", oldValue, this.codigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFreguesia() {
        return this.freguesia;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFreguesia(String freguesia) {
        String oldValue = this.freguesia;
        this.freguesia = freguesia;
        this.firePropertyChange("freguesia", oldValue, this.freguesia);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio_UrbanoOmisso")
    public String getTipoPredio() {
        return this.tipoPredio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTipoPredio(String tipoPredio) {
        String oldValue = this.tipoPredio;
        this.tipoPredio = tipoPredio;
        this.firePropertyChange("tipoPredio", oldValue, this.tipoPredio);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getArtigo() {
        return this.artigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setArtigo(Long artigo) {
        Long oldValue = this.artigo;
        this.artigo = artigo;
        this.firePropertyChange("artigo", oldValue, this.artigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFraccao() {
        return this.fraccao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFraccao(String fraccao) {
        String oldValue = this.fraccao;
        this.fraccao = fraccao;
        this.firePropertyChange("fraccao", oldValue, this.fraccao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComDepSPCEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_PermanenteArrendada")
    public String getHabitacaoPermanenteArrendada() {
        return this.habitacaoPermanenteArrendada;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setHabitacaoPermanenteArrendada(String habitacaoPermanenteArrendada) {
        String oldValue = this.habitacaoPermanenteArrendada;
        this.habitacaoPermanenteArrendada = habitacaoPermanenteArrendada;
        this.firePropertyChange("habitacaoPermanenteArrendada", oldValue, this.habitacaoPermanenteArrendada);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIFArrendatario() {
        return this.nIFArrendatario;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIFArrendatario(Long nIFArrendatario) {
        Long oldValue = this.nIFArrendatario;
        this.nIFArrendatario = nIFArrendatario;
        this.firePropertyChange("nIFArrendatario", oldValue, this.nIFArrendatario);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_SimNao")
    public String getClassificacaoA() {
        return this.classificacaoA;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setClassificacaoA(String classificacaoA) {
        String oldValue = this.classificacaoA;
        this.classificacaoA = classificacaoA;
        this.firePropertyChange("classificacaoA", oldValue, this.classificacaoA);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.codigo);
        result = HashCodeUtil.hash(result, this.freguesia);
        result = HashCodeUtil.hash(result, this.tipoPredio);
        result = HashCodeUtil.hash(result, this.artigo);
        result = HashCodeUtil.hash(result, this.fraccao);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.habitacaoPermanenteArrendada);
        result = HashCodeUtil.hash(result, this.nIFArrendatario);
        result = HashCodeUtil.hash(result, this.classificacaoA);
        return result;
    }

    public static enum Property {
        CODIGO("codigo", 2),
        FREGUESIA("freguesia", 3),
        TIPOPREDIO("tipoPredio", 4),
        ARTIGO("artigo", 5),
        FRACCAO("fraccao", 6),
        TITULAR("titular", 7),
        HABITACAOPERMANENTEARRENDADA("habitacaoPermanenteArrendada", 8),
        NIFARRENDATARIO("nIFArrendatario", 9),
        CLASSIFICACAOA("classificacaoA", 10);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

