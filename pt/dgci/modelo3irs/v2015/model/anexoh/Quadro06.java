/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro06Base;

public class Quadro06
extends Quadro06Base {
    private static final long serialVersionUID = -1753680772236837338L;

    public long getValorTotalPensoesPreenchido() {
        long valorTotalPensoes = 0;
        for (AnexoHq06T1_Linha anexoHq06T1_Linha : this.getAnexoHq06T1()) {
            if (anexoHq06T1_Linha.getValor() == null) continue;
            valorTotalPensoes+=anexoHq06T1_Linha.getValor().longValue();
        }
        return valorTotalPensoes;
    }

    public boolean hasAnexoHq06T1Valor() {
        for (AnexoHq06T1_Linha anexoHq06T1_Linha : this.getAnexoHq06T1()) {
            if (anexoHq06T1_Linha.getValor() == null) continue;
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        return (this.getAnexoHq06C1() == null || this.getAnexoHq06C1() == 0) && this.getAnexoHq06T1().isEmpty();
    }
}

