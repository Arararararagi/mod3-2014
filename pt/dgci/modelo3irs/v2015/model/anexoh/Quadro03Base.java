/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoH.qQuadro03";
    public static final String ANEXOHQ03C02_LINK = "aAnexoH.qQuadro03.fanexoHq03C02";
    public static final String ANEXOHQ03C02 = "anexoHq03C02";
    private Long anexoHq03C02;
    public static final String ANEXOHQ03C03_LINK = "aAnexoH.qQuadro03.fanexoHq03C03";
    public static final String ANEXOHQ03C03 = "anexoHq03C03";
    private Long anexoHq03C03;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoHq03C02() {
        return this.anexoHq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq03C02(Long anexoHq03C02) {
        Long oldValue = this.anexoHq03C02;
        this.anexoHq03C02 = anexoHq03C02;
        this.firePropertyChange("anexoHq03C02", oldValue, this.anexoHq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoHq03C03() {
        return this.anexoHq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoHq03C03(Long anexoHq03C03) {
        Long oldValue = this.anexoHq03C03;
        this.anexoHq03C03 = anexoHq03C03;
        this.firePropertyChange("anexoHq03C03", oldValue, this.anexoHq03C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoHq03C02);
        result = HashCodeUtil.hash(result, this.anexoHq03C03);
        return result;
    }
}

