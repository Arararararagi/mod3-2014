/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_LinhaBase;

public class AnexoHq07T7_Linha
extends AnexoHq07T7_LinhaBase {
    public AnexoHq07T7_Linha() {
    }

    public AnexoHq07T7_Linha(Long codBeneficio, String titular, Long importanciaAplicada, Long nifNipcPortugues, Long pais, String numeroFiscalUE) {
        super(codBeneficio, titular, importanciaAplicada, nifNipcPortugues, pais, numeroFiscalUE);
    }

    public static String getLink(int numLinha) {
        return "aAnexoH.qQuadro07.tanexoHq07T7.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoHq07T7_LinhaBase.Property column) {
        return "aAnexoH.qQuadro07.tanexoHq07T7.l" + numLinha + ".c" + column.getIndex();
    }
}

