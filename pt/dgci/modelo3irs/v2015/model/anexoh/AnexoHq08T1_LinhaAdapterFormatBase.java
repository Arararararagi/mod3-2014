/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexoh;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoHq08T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoHq08T1_Linha>,
WritableTableFormat<AnexoHq08T1_Linha> {
    @Override
    public boolean isEditable(AnexoHq08T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoHq08T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getNIF() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComDepAfiAscEFalecidos", line.getNIF());
            }
            case 2: {
                return line.getAnexoHq08C801();
            }
            case 3: {
                return line.getAnexoHq08C802();
            }
            case 4: {
                return line.getAnexoHq08C803();
            }
        }
        return null;
    }

    @Override
    public AnexoHq08T1_Linha setColumnValue(AnexoHq08T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setNIF((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setAnexoHq08C801((Long)value);
                return line;
            }
            case 3: {
                line.setAnexoHq08C802((Long)value);
                return line;
            }
            case 4: {
                line.setAnexoHq08C803((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Benefici\u00e1rio da Despesa";
            }
            case 2: {
                return "Sa\u00fade (isento IVA ou taxa reduzida)";
            }
            case 3: {
                return "Sa\u00fade (c/IVA a taxa normal)";
            }
            case 4: {
                return "Educa\u00e7\u00e3o";
            }
        }
        return null;
    }
}

