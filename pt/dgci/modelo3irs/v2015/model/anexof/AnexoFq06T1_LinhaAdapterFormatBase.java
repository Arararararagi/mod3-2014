/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoFq06T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoFq06T1_Linha>,
WritableTableFormat<AnexoFq06T1_Linha> {
    @Override
    public boolean isEditable(AnexoFq06T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoFq06T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos", line.getTitular());
            }
            case 2: {
                return line.getRendaRecebida();
            }
            case 3: {
                return line.getRetencoes();
            }
            case 4: {
                return line.getNifSublocatario();
            }
            case 5: {
                return line.getRendaPaga();
            }
            case 6: {
                return line.getNifSenhorio();
            }
        }
        return null;
    }

    @Override
    public AnexoFq06T1_Linha setColumnValue(AnexoFq06T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setRendaRecebida((Long)value);
                return line;
            }
            case 3: {
                line.setRetencoes((Long)value);
                return line;
            }
            case 4: {
                line.setNifSublocatario((Long)value);
                return line;
            }
            case 5: {
                line.setRendaPaga((Long)value);
                return line;
            }
            case 6: {
                line.setNifSenhorio((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return String.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return Long.class;
            }
            case 6: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Titular";
            }
            case 2: {
                return "Renda Recebida (valor il\u00edquido)";
            }
            case 3: {
                return "Reten\u00e7\u00f5es de IRS";
            }
            case 4: {
                return "Sublocat\u00e1rio (NIF)";
            }
            case 5: {
                return "Renda Paga ao Senhorio";
            }
            case 6: {
                return "Senhorio (NIF)";
            }
        }
        return null;
    }
}

