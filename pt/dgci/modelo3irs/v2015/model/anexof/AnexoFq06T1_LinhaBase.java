/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoFq06T1_LinhaBase
extends Model {
    public static final String TITULAR_LINK = "aAnexoF.qQuadro06.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String RENDARECEBIDA_LINK = "aAnexoF.qQuadro06.frendaRecebida";
    public static final String RENDARECEBIDA = "rendaRecebida";
    private Long rendaRecebida;
    public static final String RETENCOES_LINK = "aAnexoF.qQuadro06.fretencoes";
    public static final String RETENCOES = "retencoes";
    private Long retencoes;
    public static final String NIFSUBLOCATARIO_LINK = "aAnexoF.qQuadro06.fnifSublocatario";
    public static final String NIFSUBLOCATARIO = "nifSublocatario";
    private Long nifSublocatario;
    public static final String RENDAPAGA_LINK = "aAnexoF.qQuadro06.frendaPaga";
    public static final String RENDAPAGA = "rendaPaga";
    private Long rendaPaga;
    public static final String NIFSENHORIO_LINK = "aAnexoF.qQuadro06.fnifSenhorio";
    public static final String NIFSENHORIO = "nifSenhorio";
    private Long nifSenhorio;

    public AnexoFq06T1_LinhaBase(String titular, Long rendaRecebida, Long retencoes, Long nifSublocatario, Long rendaPaga, Long nifSenhorio) {
        this.titular = titular;
        this.rendaRecebida = rendaRecebida;
        this.retencoes = retencoes;
        this.nifSublocatario = nifSublocatario;
        this.rendaPaga = rendaPaga;
        this.nifSenhorio = nifSenhorio;
    }

    public AnexoFq06T1_LinhaBase() {
        this.titular = null;
        this.rendaRecebida = null;
        this.retencoes = null;
        this.nifSublocatario = null;
        this.rendaPaga = null;
        this.nifSenhorio = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendaRecebida() {
        return this.rendaRecebida;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendaRecebida(Long rendaRecebida) {
        Long oldValue = this.rendaRecebida;
        this.rendaRecebida = rendaRecebida;
        this.firePropertyChange("rendaRecebida", oldValue, this.rendaRecebida);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencoes() {
        return this.retencoes;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencoes(Long retencoes) {
        Long oldValue = this.retencoes;
        this.retencoes = retencoes;
        this.firePropertyChange("retencoes", oldValue, this.retencoes);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifSublocatario() {
        return this.nifSublocatario;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifSublocatario(Long nifSublocatario) {
        Long oldValue = this.nifSublocatario;
        this.nifSublocatario = nifSublocatario;
        this.firePropertyChange("nifSublocatario", oldValue, this.nifSublocatario);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendaPaga() {
        return this.rendaPaga;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendaPaga(Long rendaPaga) {
        Long oldValue = this.rendaPaga;
        this.rendaPaga = rendaPaga;
        this.firePropertyChange("rendaPaga", oldValue, this.rendaPaga);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNifSenhorio() {
        return this.nifSenhorio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNifSenhorio(Long nifSenhorio) {
        Long oldValue = this.nifSenhorio;
        this.nifSenhorio = nifSenhorio;
        this.firePropertyChange("nifSenhorio", oldValue, this.nifSenhorio);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.rendaRecebida);
        result = HashCodeUtil.hash(result, this.retencoes);
        result = HashCodeUtil.hash(result, this.nifSublocatario);
        result = HashCodeUtil.hash(result, this.rendaPaga);
        result = HashCodeUtil.hash(result, this.nifSenhorio);
        return result;
    }

    public static enum Property {
        TITULAR("titular", 2),
        RENDARECEBIDA("rendaRecebida", 3),
        RETENCOES("retencoes", 4),
        NIFSUBLOCATARIO("nifSublocatario", 5),
        RENDAPAGA("rendaPaga", 6),
        NIFSENHORIO("nifSenhorio", 7);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

