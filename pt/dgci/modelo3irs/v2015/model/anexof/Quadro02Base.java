/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro02Base
extends QuadroModel {
    public static final String QUADRO02_LINK = "aAnexoF.qQuadro02";
    public static final String ANEXOFQ02C01_LINK = "aAnexoF.qQuadro02.fanexoFq02C01";
    public static final String ANEXOFQ02C01 = "anexoFq02C01";
    private Long anexoFq02C01;

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_Anos")
    public Long getAnexoFq02C01() {
        return this.anexoFq02C01;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoFq02C01(Long anexoFq02C01) {
        Long oldValue = this.anexoFq02C01;
        this.anexoFq02C01 = anexoFq02C01;
        this.firePropertyChange("anexoFq02C01", oldValue, this.anexoFq02C01);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoFq02C01);
        return result;
    }
}

