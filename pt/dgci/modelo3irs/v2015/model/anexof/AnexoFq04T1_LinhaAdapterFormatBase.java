/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoFq04T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoFq04T1_Linha>,
WritableTableFormat<AnexoFq04T1_Linha> {
    @Override
    public boolean isEditable(AnexoFq04T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoFq04T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getNLinha() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoFQ4", line.getNLinha());
            }
            case 2: {
                return line.getFreguesia();
            }
            case 3: {
                if (line == null || line.getTipoPredio() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_TipoPredio", line.getTipoPredio());
            }
            case 4: {
                return line.getArtigo();
            }
            case 5: {
                return line.getFraccao();
            }
            case 6: {
                if (line == null || line.getTitular() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_RostoTitularesComCDepEFalecidos", line.getTitular());
            }
            case 7: {
                return line.getQuotaParte();
            }
            case 8: {
                return line.getRendas();
            }
            case 9: {
                return line.getRetencoes();
            }
            case 10: {
                return line.getNIF();
            }
            case 11: {
                return line.getDespesas();
            }
        }
        return null;
    }

    @Override
    public AnexoFq04T1_Linha setColumnValue(AnexoFq04T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setNLinha((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setFreguesia((String)value);
                return line;
            }
            case 3: {
                if (value != null) {
                    line.setTipoPredio((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 4: {
                line.setArtigo((Long)value);
                return line;
            }
            case 5: {
                line.setFraccao((String)value);
                return line;
            }
            case 6: {
                if (value != null) {
                    line.setTitular((String)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 7: {
                line.setQuotaParte((Long)value);
                return line;
            }
            case 8: {
                line.setRendas((Long)value);
                return line;
            }
            case 9: {
                line.setRetencoes((Long)value);
                return line;
            }
            case 10: {
                line.setNIF((Long)value);
                return line;
            }
            case 11: {
                line.setDespesas((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return String.class;
            }
            case 3: {
                return String.class;
            }
            case 4: {
                return Long.class;
            }
            case 5: {
                return String.class;
            }
            case 6: {
                return String.class;
            }
            case 7: {
                return Long.class;
            }
            case 8: {
                return Long.class;
            }
            case 9: {
                return Long.class;
            }
            case 10: {
                return Long.class;
            }
            case 11: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 12;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "N\u00ba da Linha";
            }
            case 2: {
                return "Freguesia (c\u00f3digo)";
            }
            case 3: {
                return "Tipo";
            }
            case 4: {
                return "Artigo";
            }
            case 5: {
                return "Fra\u00e7\u00e3o / Sec\u00e7\u00e3o";
            }
            case 6: {
                return "Titular";
            }
            case 7: {
                return "Quota-Parte %";
            }
            case 8: {
                return "Rendas (Rendimento il\u00edquido)";
            }
            case 9: {
                return "Reten\u00e7\u00f5es na Fonte de IRS";
            }
            case 10: {
                return "NIF do Arrendat\u00e1rio";
            }
            case 11: {
                return "Despesas";
            }
        }
        return null;
    }
}

