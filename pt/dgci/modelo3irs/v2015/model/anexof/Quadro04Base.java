/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro04Base
extends QuadroModel {
    public static final String QUADRO04_LINK = "aAnexoF.qQuadro04";
    public static final String ANEXOFQ04T1_LINK = "aAnexoF.qQuadro04.tanexoFq04T1";
    private EventList<AnexoFq04T1_Linha> anexoFq04T1 = new BasicEventList<AnexoFq04T1_Linha>();
    public static final String ANEXOFQ04C1_LINK = "aAnexoF.qQuadro04.fanexoFq04C1";
    public static final String ANEXOFQ04C1 = "anexoFq04C1";
    private Long anexoFq04C1;
    public static final String ANEXOFQ04C2_LINK = "aAnexoF.qQuadro04.fanexoFq04C2";
    public static final String ANEXOFQ04C2 = "anexoFq04C2";
    private Long anexoFq04C2;
    public static final String ANEXOFQ04C3_LINK = "aAnexoF.qQuadro04.fanexoFq04C3";
    public static final String ANEXOFQ04C3 = "anexoFq04C3";
    private Long anexoFq04C3;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoFq04T1_Linha> getAnexoFq04T1() {
        return this.anexoFq04T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoFq04T1(EventList<AnexoFq04T1_Linha> anexoFq04T1) {
        this.anexoFq04T1 = anexoFq04T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoFq04C1() {
        return this.anexoFq04C1;
    }

    public void setAnexoFq04C1Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoFq04C1")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoFq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoFq04T1().get(i).getRendas() == null ? 0 : this.getAnexoFq04T1().get(i).getRendas();
        }
        this.setAnexoFq04C1(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoFq04C1(Long anexoFq04C1) {
        Long oldValue = this.anexoFq04C1;
        this.anexoFq04C1 = anexoFq04C1;
        this.firePropertyChange("anexoFq04C1", oldValue, this.anexoFq04C1);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoFq04C2() {
        return this.anexoFq04C2;
    }

    public void setAnexoFq04C2Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoFq04C2")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoFq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoFq04T1().get(i).getRetencoes() == null ? 0 : this.getAnexoFq04T1().get(i).getRetencoes();
        }
        this.setAnexoFq04C2(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoFq04C2(Long anexoFq04C2) {
        Long oldValue = this.anexoFq04C2;
        this.anexoFq04C2 = anexoFq04C2;
        this.firePropertyChange("anexoFq04C2", oldValue, this.anexoFq04C2);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getAnexoFq04C3() {
        return this.anexoFq04C3;
    }

    public void setAnexoFq04C3Formula(Long value) {
        if (!OverwriteBehaviorManager.getInstance().isToDoFormula("anexoFq04C3")) {
            return;
        }
        long newValue = 0;
        long temporaryValue0 = 0;
        for (int i = 0; i < this.getAnexoFq04T1().size(); ++i) {
            temporaryValue0+=this.getAnexoFq04T1().get(i).getDespesas() == null ? 0 : this.getAnexoFq04T1().get(i).getDespesas();
        }
        this.setAnexoFq04C3(newValue+=temporaryValue0);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setAnexoFq04C3(Long anexoFq04C3) {
        Long oldValue = this.anexoFq04C3;
        this.anexoFq04C3 = anexoFq04C3;
        this.firePropertyChange("anexoFq04C3", oldValue, this.anexoFq04C3);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoFq04T1);
        result = HashCodeUtil.hash(result, this.anexoFq04C1);
        result = HashCodeUtil.hash(result, this.anexoFq04C2);
        result = HashCodeUtil.hash(result, this.anexoFq04C3);
        return result;
    }
}

