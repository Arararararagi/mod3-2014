/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06Base;

public class Quadro06
extends Quadro06Base {
    public long getRetencoesByTitular(String titular) {
        long result = 0;
        EventList<AnexoFq06T1_Linha> linhas = this.getAnexoFq06T1();
        for (AnexoFq06T1_Linha linha : linhas) {
            if (linha.getRetencoes() == null || linha.getTitular() == null || !linha.getTitular().equals(titular)) continue;
            result+=linha.getRetencoes().longValue();
        }
        return result;
    }

    public boolean isEmpty() {
        if (this.getAnexoFq06T1().isEmpty()) {
            return true;
        }
        EventList<AnexoFq06T1_Linha> linhas = this.getAnexoFq06T1();
        for (AnexoFq06T1_Linha linha : linhas) {
            if (linha.isEmpty()) continue;
            return false;
        }
        return true;
    }
}

