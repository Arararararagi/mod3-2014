/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class AnexoFq07T1_Linha
extends AnexoFq07T1_LinhaBase {
    public AnexoFq07T1_Linha() {
    }

    public AnexoFq07T1_Linha(Long campoQ4, Long rendimento, Long nanos) {
        super(campoQ4, rendimento, nanos);
    }

    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getCampoQ4()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getRendimento()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getNanos());
    }

    public static String getLink(int numLinha) {
        return "aAnexoF.qQuadro07.tanexoFq07T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoFq07T1_LinhaBase.Property column) {
        return AnexoFq07T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

