/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoFq05T1_LinhaBase
extends Model {
    public static final String CAMPOSQUADRO4_LINK = "aAnexoF.qQuadro05.fcamposQuadro4";
    public static final String CAMPOSQUADRO4 = "camposQuadro4";
    private Long camposQuadro4;

    public AnexoFq05T1_LinhaBase(Long camposQuadro4) {
        this.camposQuadro4 = camposQuadro4;
    }

    public AnexoFq05T1_LinhaBase() {
        this.camposQuadro4 = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoFQ4")
    public Long getCamposQuadro4() {
        return this.camposQuadro4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCamposQuadro4(Long camposQuadro4) {
        Long oldValue = this.camposQuadro4;
        this.camposQuadro4 = camposQuadro4;
        this.firePropertyChange("camposQuadro4", oldValue, this.camposQuadro4);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.camposQuadro4);
        return result;
    }

    public static enum Property {
        CAMPOSQUADRO4("camposQuadro4", 2);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

