/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoFq04T1_LinhaBase
extends Model {
    public static final String NLINHA_LINK = "aAnexoF.qQuadro04.fnLinha";
    public static final String NLINHA = "nLinha";
    private Long nLinha;
    public static final String FREGUESIA_LINK = "aAnexoF.qQuadro04.ffreguesia";
    public static final String FREGUESIA = "freguesia";
    private String freguesia;
    public static final String TIPOPREDIO_LINK = "aAnexoF.qQuadro04.ftipoPredio";
    public static final String TIPOPREDIO = "tipoPredio";
    private String tipoPredio;
    public static final String ARTIGO_LINK = "aAnexoF.qQuadro04.fartigo";
    public static final String ARTIGO = "artigo";
    private Long artigo;
    public static final String FRACCAO_LINK = "aAnexoF.qQuadro04.ffraccao";
    public static final String FRACCAO = "fraccao";
    private String fraccao;
    public static final String TITULAR_LINK = "aAnexoF.qQuadro04.ftitular";
    public static final String TITULAR = "titular";
    private String titular;
    public static final String QUOTAPARTE_LINK = "aAnexoF.qQuadro04.fquotaParte";
    public static final String QUOTAPARTE = "quotaParte";
    private Long quotaParte;
    public static final String RENDAS_LINK = "aAnexoF.qQuadro04.frendas";
    public static final String RENDAS = "rendas";
    private Long rendas;
    public static final String RETENCOES_LINK = "aAnexoF.qQuadro04.fretencoes";
    public static final String RETENCOES = "retencoes";
    private Long retencoes;
    public static final String NIF_LINK = "aAnexoF.qQuadro04.fnIF";
    public static final String NIF = "nIF";
    private Long nIF;
    public static final String DESPESAS_LINK = "aAnexoF.qQuadro04.fdespesas";
    public static final String DESPESAS = "despesas";
    private Long despesas;

    public AnexoFq04T1_LinhaBase(Long nLinha, String freguesia, String tipoPredio, Long artigo, String fraccao, String titular, Long quotaParte, Long rendas, Long retencoes, Long nIF, Long despesas) {
        this.nLinha = nLinha;
        this.freguesia = freguesia;
        this.tipoPredio = tipoPredio;
        this.artigo = artigo;
        this.fraccao = fraccao;
        this.titular = titular;
        this.quotaParte = quotaParte;
        this.rendas = rendas;
        this.retencoes = retencoes;
        this.nIF = nIF;
        this.despesas = despesas;
    }

    public AnexoFq04T1_LinhaBase() {
        this.nLinha = null;
        this.freguesia = null;
        this.tipoPredio = null;
        this.artigo = null;
        this.fraccao = null;
        this.titular = null;
        this.quotaParte = null;
        this.rendas = null;
        this.retencoes = null;
        this.nIF = null;
        this.despesas = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoFQ4")
    public Long getNLinha() {
        return this.nLinha;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNLinha(Long nLinha) {
        Long oldValue = this.nLinha;
        this.nLinha = nLinha;
        this.firePropertyChange("nLinha", oldValue, this.nLinha);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFreguesia() {
        return this.freguesia;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFreguesia(String freguesia) {
        String oldValue = this.freguesia;
        this.freguesia = freguesia;
        this.firePropertyChange("freguesia", oldValue, this.freguesia);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_TipoPredio")
    public String getTipoPredio() {
        return this.tipoPredio;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTipoPredio(String tipoPredio) {
        String oldValue = this.tipoPredio;
        this.tipoPredio = tipoPredio;
        this.firePropertyChange("tipoPredio", oldValue, this.tipoPredio);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getArtigo() {
        return this.artigo;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setArtigo(Long artigo) {
        Long oldValue = this.artigo;
        this.artigo = artigo;
        this.firePropertyChange("artigo", oldValue, this.artigo);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getFraccao() {
        return this.fraccao;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setFraccao(String fraccao) {
        String oldValue = this.fraccao;
        this.fraccao = fraccao;
        this.firePropertyChange("fraccao", oldValue, this.fraccao);
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_RostoTitularesComCDepEFalecidos")
    public String getTitular() {
        return this.titular;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setTitular(String titular) {
        String oldValue = this.titular;
        this.titular = titular;
        this.firePropertyChange("titular", oldValue, this.titular);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getQuotaParte() {
        return this.quotaParte;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setQuotaParte(Long quotaParte) {
        Long oldValue = this.quotaParte;
        this.quotaParte = quotaParte;
        this.firePropertyChange("quotaParte", oldValue, this.quotaParte);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendas() {
        return this.rendas;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendas(Long rendas) {
        Long oldValue = this.rendas;
        this.rendas = rendas;
        this.firePropertyChange("rendas", oldValue, this.rendas);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRetencoes() {
        return this.retencoes;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRetencoes(Long retencoes) {
        Long oldValue = this.retencoes;
        this.retencoes = retencoes;
        this.firePropertyChange("retencoes", oldValue, this.retencoes);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNIF() {
        return this.nIF;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNIF(Long nIF) {
        Long oldValue = this.nIF;
        this.nIF = nIF;
        this.firePropertyChange("nIF", oldValue, this.nIF);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getDespesas() {
        return this.despesas;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setDespesas(Long despesas) {
        Long oldValue = this.despesas;
        this.despesas = despesas;
        this.firePropertyChange("despesas", oldValue, this.despesas);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.nLinha);
        result = HashCodeUtil.hash(result, this.freguesia);
        result = HashCodeUtil.hash(result, this.tipoPredio);
        result = HashCodeUtil.hash(result, this.artigo);
        result = HashCodeUtil.hash(result, this.fraccao);
        result = HashCodeUtil.hash(result, this.titular);
        result = HashCodeUtil.hash(result, this.quotaParte);
        result = HashCodeUtil.hash(result, this.rendas);
        result = HashCodeUtil.hash(result, this.retencoes);
        result = HashCodeUtil.hash(result, this.nIF);
        result = HashCodeUtil.hash(result, this.despesas);
        return result;
    }

    public static enum Property {
        NLINHA("nLinha", 2),
        FREGUESIA("freguesia", 3),
        TIPOPREDIO("tipoPredio", 4),
        ARTIGO("artigo", 5),
        FRACCAO("fraccao", 6),
        TITULAR("titular", 7),
        QUOTAPARTE("quotaParte", 8),
        RENDAS("rendas", 9),
        RETENCOES("retencoes", 10),
        NIF("nIF", 11),
        DESPESAS("despesas", 12);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

