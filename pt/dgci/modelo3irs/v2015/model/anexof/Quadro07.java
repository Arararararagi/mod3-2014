/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro07Base;

public class Quadro07
extends Quadro07Base {
    public long getTotalNAnos() {
        long result = 0;
        for (AnexoFq07T1_Linha linha : this.getAnexoFq07T1()) {
            result+=linha.getNanos() != null ? linha.getNanos() : 0;
        }
        return result;
    }

    public long getTotalRendimentos() {
        long result = 0;
        for (AnexoFq07T1_Linha linha : this.getAnexoFq07T1()) {
            result+=linha.getRendimento() != null ? linha.getRendimento() : 0;
        }
        return result;
    }

    public boolean isEmpty() {
        if (this.getAnexoFq07T1().isEmpty()) {
            return true;
        }
        EventList<AnexoFq07T1_Linha> linhas = this.getAnexoFq07T1();
        for (AnexoFq07T1_Linha linha : linhas) {
            if (linha.isEmpty()) continue;
            return false;
        }
        return true;
    }
}

