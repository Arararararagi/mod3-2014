/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro05Base
extends QuadroModel {
    public static final String QUADRO05_LINK = "aAnexoF.qQuadro05";
    public static final String ANEXOFQ05T1_LINK = "aAnexoF.qQuadro05.tanexoFq05T1";
    private EventList<AnexoFq05T1_Linha> anexoFq05T1 = new BasicEventList<AnexoFq05T1_Linha>();
    public static final String ANEXOFQ05B1OPSIM_LINK = "aAnexoF.qQuadro05.fanexoFq05B1OPSim";
    public static final String ANEXOFQ05B1OPSIM_VALUE = "S";
    public static final String ANEXOFQ05B1OPNAO_LINK = "aAnexoF.qQuadro05.fanexoFq05B1OPNao";
    public static final String ANEXOFQ05B1OPNAO_VALUE = "N";
    public static final String ANEXOFQ05B1OP = "anexoFq05B1OP";
    private String anexoFq05B1OP;
    public static final String ANEXOFQ05B2OPSIM_LINK = "aAnexoF.qQuadro05.fanexoFq05B2OPSim";
    public static final String ANEXOFQ05B2OPSIM_VALUE = "S";
    public static final String ANEXOFQ05B2OPNAO_LINK = "aAnexoF.qQuadro05.fanexoFq05B2OPNao";
    public static final String ANEXOFQ05B2OPNAO_VALUE = "N";
    public static final String ANEXOFQ05B2OP = "anexoFq05B2OP";
    private String anexoFq05B2OP;
    public static final String ANEXOFQ05B3OPSIM_LINK = "aAnexoF.qQuadro05.fanexoFq05B3OPSim";
    public static final String ANEXOFQ05B3OPSIM_VALUE = "S";
    public static final String ANEXOFQ05B3OPNAO_LINK = "aAnexoF.qQuadro05.fanexoFq05B3OPNao";
    public static final String ANEXOFQ05B3OPNAO_VALUE = "N";
    public static final String ANEXOFQ05B3OP = "anexoFq05B3OP";
    private String anexoFq05B3OP;

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoFq05T1_Linha> getAnexoFq05T1() {
        return this.anexoFq05T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoFq05T1(EventList<AnexoFq05T1_Linha> anexoFq05T1) {
        this.anexoFq05T1 = anexoFq05T1;
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoFq05B1OP() {
        return this.anexoFq05B1OP;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoFq05B1OP(String anexoFq05B1OP) {
        String oldValue = this.anexoFq05B1OP;
        this.anexoFq05B1OP = anexoFq05B1OP;
        this.firePropertyChange("anexoFq05B1OP", oldValue, this.anexoFq05B1OP);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoFq05B2OP() {
        return this.anexoFq05B2OP;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoFq05B2OP(String anexoFq05B2OP) {
        String oldValue = this.anexoFq05B2OP;
        this.anexoFq05B2OP = anexoFq05B2OP;
        this.firePropertyChange("anexoFq05B2OP", oldValue, this.anexoFq05B2OP);
    }

    @Type(value=Type.TYPE.CAMPO)
    public String getAnexoFq05B3OP() {
        return this.anexoFq05B3OP;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoFq05B3OP(String anexoFq05B3OP) {
        String oldValue = this.anexoFq05B3OP;
        this.anexoFq05B3OP = anexoFq05B3OP;
        this.firePropertyChange("anexoFq05B3OP", oldValue, this.anexoFq05B3OP);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoFq05T1);
        result = HashCodeUtil.hash(result, this.anexoFq05B1OP);
        result = HashCodeUtil.hash(result, this.anexoFq05B2OP);
        result = HashCodeUtil.hash(result, this.anexoFq05B3OP);
        return result;
    }
}

