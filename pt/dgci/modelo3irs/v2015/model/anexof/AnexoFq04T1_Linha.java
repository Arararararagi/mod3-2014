/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class AnexoFq04T1_Linha
extends AnexoFq04T1_LinhaBase {
    public AnexoFq04T1_Linha() {
    }

    public AnexoFq04T1_Linha(Long nLinha, String freguesia, String tipoPredio, Long artigo, String fraccao, String titular, Long quotaParte, Long rendas, Long retencoes, Long nIF, Long despesas) {
        super(nLinha, freguesia, tipoPredio, artigo, fraccao, titular, quotaParte, rendas, retencoes, nIF, despesas);
    }

    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getNLinha()) && StringUtil.isEmpty(this.getFreguesia()) && StringUtil.isEmpty(this.getTipoPredio()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getArtigo()) && StringUtil.isEmpty(this.getFraccao()) && StringUtil.isEmpty(this.getTitular()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getQuotaParte()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getRendas()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getRetencoes()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getNIF()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getDespesas());
    }

    public static String getLink(int numLinha) {
        return "aAnexoF.qQuadro04.tanexoFq04T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoFq04T1_LinhaBase.Property column) {
        return AnexoFq04T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

