/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_Linha;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;

public class AnexoFq07T1_LinhaAdapterFormatBase
implements AdvancedTableFormat<AnexoFq07T1_Linha>,
WritableTableFormat<AnexoFq07T1_Linha> {
    @Override
    public boolean isEditable(AnexoFq07T1_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return true;
        }
        return true;
    }

    @Override
    public Object getColumnValue(AnexoFq07T1_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (line == null || line.getCampoQ4() == null) {
                    return null;
                }
                return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI("Cat_M3V2015_AnexoFQ4", line.getCampoQ4());
            }
            case 2: {
                return line.getRendimento();
            }
            case 3: {
                return line.getNanos();
            }
        }
        return null;
    }

    @Override
    public AnexoFq07T1_Linha setColumnValue(AnexoFq07T1_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                if (value != null) {
                    line.setCampoQ4((Long)((ICatalogItem)value).getValue());
                }
                return line;
            }
            case 2: {
                line.setRendimento((Long)value);
                return line;
            }
            case 3: {
                line.setNanos((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
            case 2: {
                return Long.class;
            }
            case 3: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Campo Q4";
            }
            case 2: {
                return "Rendimento";
            }
            case 3: {
                return "N.\u00ba Anos";
            }
        }
        return null;
    }
}

