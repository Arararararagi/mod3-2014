/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.EventList;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro05Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro05
extends Quadro05Base {
    public boolean isEmpty() {
        if (this.getAnexoFq05T1().isEmpty()) {
            return true;
        }
        EventList<AnexoFq05T1_Linha> linhas = this.getAnexoFq05T1();
        for (AnexoFq05T1_Linha linha : linhas) {
            if (linha.isEmpty()) continue;
            return false;
        }
        return true;
    }

    public List<Long> getNlinhaList() {
        ArrayList<Long> list = new ArrayList<Long>();
        EventList<AnexoFq05T1_Linha> linhas = this.getAnexoFq05T1();
        for (AnexoFq05T1_Linha linha : linhas) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getCamposQuadro4()) || list.contains(linha.getCamposQuadro4())) continue;
            list.add(linha.getCamposQuadro4());
        }
        return list;
    }
}

