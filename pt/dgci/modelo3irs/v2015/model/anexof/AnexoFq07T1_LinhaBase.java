/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Catalog;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class AnexoFq07T1_LinhaBase
extends Model {
    public static final String CAMPOQ4_LINK = "aAnexoF.qQuadro07.fcampoQ4";
    public static final String CAMPOQ4 = "campoQ4";
    private Long campoQ4;
    public static final String RENDIMENTO_LINK = "aAnexoF.qQuadro07.frendimento";
    public static final String RENDIMENTO = "rendimento";
    private Long rendimento;
    public static final String NANOS_LINK = "aAnexoF.qQuadro07.fnanos";
    public static final String NANOS = "nanos";
    private Long nanos;

    public AnexoFq07T1_LinhaBase(Long campoQ4, Long rendimento, Long nanos) {
        this.campoQ4 = campoQ4;
        this.rendimento = rendimento;
        this.nanos = nanos;
    }

    public AnexoFq07T1_LinhaBase() {
        this.campoQ4 = null;
        this.rendimento = null;
        this.nanos = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Catalog(value="Cat_M3V2015_AnexoFQ4")
    public Long getCampoQ4() {
        return this.campoQ4;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setCampoQ4(Long campoQ4) {
        Long oldValue = this.campoQ4;
        this.campoQ4 = campoQ4;
        this.firePropertyChange("campoQ4", oldValue, this.campoQ4);
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public Long getRendimento() {
        return this.rendimento;
    }

    @Type(value=Type.TYPE.CAMPO)
    @FractionDigits(value=2)
    public void setRendimento(Long rendimento) {
        Long oldValue = this.rendimento;
        this.rendimento = rendimento;
        this.firePropertyChange("rendimento", oldValue, this.rendimento);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getNanos() {
        return this.nanos;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setNanos(Long nanos) {
        Long oldValue = this.nanos;
        this.nanos = nanos;
        this.firePropertyChange("nanos", oldValue, this.nanos);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.campoQ4);
        result = HashCodeUtil.hash(result, this.rendimento);
        result = HashCodeUtil.hash(result, this.nanos);
        return result;
    }

    public static enum Property {
        CAMPOQ4("campoQ4", 2),
        RENDIMENTO("rendimento", 3),
        NANOS("nanos", 4);
        
        private final String name;
        private final int index;

        private Property(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
    }

}

