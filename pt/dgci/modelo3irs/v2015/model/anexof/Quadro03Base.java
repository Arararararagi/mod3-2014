/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro03Base
extends QuadroModel {
    public static final String QUADRO03_LINK = "aAnexoF.qQuadro03";
    public static final String ANEXOFQ03C02_LINK = "aAnexoF.qQuadro03.fanexoFq03C02";
    public static final String ANEXOFQ03C02 = "anexoFq03C02";
    private Long anexoFq03C02;
    public static final String ANEXOFQ03C03_LINK = "aAnexoF.qQuadro03.fanexoFq03C03";
    public static final String ANEXOFQ03C03 = "anexoFq03C03";
    private Long anexoFq03C03;

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoFq03C02() {
        return this.anexoFq03C02;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoFq03C02(Long anexoFq03C02) {
        Long oldValue = this.anexoFq03C02;
        this.anexoFq03C02 = anexoFq03C02;
        this.firePropertyChange("anexoFq03C02", oldValue, this.anexoFq03C02);
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getAnexoFq03C03() {
        return this.anexoFq03C03;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setAnexoFq03C03(Long anexoFq03C03) {
        Long oldValue = this.anexoFq03C03;
        this.anexoFq03C03 = anexoFq03C03;
        this.firePropertyChange("anexoFq03C03", oldValue, this.anexoFq03C03);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoFq03C02);
        result = HashCodeUtil.hash(result, this.anexoFq03C03);
        return result;
    }
}

