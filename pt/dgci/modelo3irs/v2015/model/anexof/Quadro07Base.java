/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro07Base
extends QuadroModel {
    public static final String QUADRO07_LINK = "aAnexoF.qQuadro07";
    public static final String ANEXOFQ07T1_LINK = "aAnexoF.qQuadro07.tanexoFq07T1";
    private EventList<AnexoFq07T1_Linha> anexoFq07T1 = new BasicEventList<AnexoFq07T1_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoFq07T1_Linha> getAnexoFq07T1() {
        return this.anexoFq07T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoFq07T1(EventList<AnexoFq07T1_Linha> anexoFq07T1) {
        this.anexoFq07T1 = anexoFq07T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoFq07T1);
        return result;
    }
}

