/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class AnexoFq05T1_Linha
extends AnexoFq05T1_LinhaBase {
    public AnexoFq05T1_Linha() {
    }

    public AnexoFq05T1_Linha(Long camposQuadro4) {
        super(camposQuadro4);
    }

    public boolean isEmpty() {
        return Modelo3IRSValidatorUtil.isEmptyOrZero(this.getCamposQuadro4());
    }

    public static String getLink(int numLinha) {
        return "aAnexoF.qQuadro05.tanexoFq05T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoFq05T1_LinhaBase.Property column) {
        return AnexoFq05T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

