/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class Quadro06Base
extends QuadroModel {
    public static final String QUADRO06_LINK = "aAnexoF.qQuadro06";
    public static final String ANEXOFQ06T1_LINK = "aAnexoF.qQuadro06.tanexoFq06T1";
    private EventList<AnexoFq06T1_Linha> anexoFq06T1 = new BasicEventList<AnexoFq06T1_Linha>();

    @Type(value=Type.TYPE.TABELA)
    public EventList<AnexoFq06T1_Linha> getAnexoFq06T1() {
        return this.anexoFq06T1;
    }

    @Type(value=Type.TYPE.TABELA)
    public void setAnexoFq06T1(EventList<AnexoFq06T1_Linha> anexoFq06T1) {
        this.anexoFq06T1 = anexoFq06T1;
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.anexoFq06T1);
        return result;
    }
}

