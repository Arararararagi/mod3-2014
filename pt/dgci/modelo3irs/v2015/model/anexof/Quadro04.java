/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import ca.odell.glazedlists.EventList;
import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04Base;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;

public class Quadro04
extends Quadro04Base {
    public boolean isEmptyOfRenda() {
        if (this.getAnexoFq04T1().isEmpty()) {
            return true;
        }
        EventList<AnexoFq04T1_Linha> linhas = this.getAnexoFq04T1();
        for (AnexoFq04T1_Linha linha : linhas) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getRendas())) continue;
            return false;
        }
        return true;
    }

    public List<Long> getNlinhaList() {
        ArrayList<Long> list = new ArrayList<Long>();
        EventList<AnexoFq04T1_Linha> linhas = this.getAnexoFq04T1();
        for (AnexoFq04T1_Linha linha : linhas) {
            if (Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNLinha()) || list.contains(linha.getNLinha())) continue;
            list.add(linha.getNLinha());
        }
        return list;
    }

    public long getRendasByNLinha(long nLinha) {
        EventList<AnexoFq04T1_Linha> linhas = this.getAnexoFq04T1();
        for (AnexoFq04T1_Linha linha : linhas) {
            if (linha.getNLinha() == null || !linha.getNLinha().equals(nLinha)) continue;
            return linha.getRendas() != null ? linha.getRendas() : 0;
        }
        return 0;
    }

    public boolean existsNLinha(long nLinha) {
        EventList<AnexoFq04T1_Linha> linhas = this.getAnexoFq04T1();
        for (AnexoFq04T1_Linha linha : linhas) {
            if (linha.getNLinha() == null || !linha.getNLinha().equals(nLinha)) continue;
            return true;
        }
        return false;
    }

    public long getRetencoesByTitular(String titular) {
        long result = 0;
        EventList<AnexoFq04T1_Linha> linhas = this.getAnexoFq04T1();
        for (AnexoFq04T1_Linha linha : linhas) {
            if (linha.getRetencoes() == null || linha.getTitular() == null || !linha.getTitular().equals(titular)) continue;
            result+=linha.getRetencoes().longValue();
        }
        return result;
    }
}

