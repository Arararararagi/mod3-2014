/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.util.StringUtil;

public class AnexoFq06T1_Linha
extends AnexoFq06T1_LinhaBase {
    public AnexoFq06T1_Linha() {
    }

    public AnexoFq06T1_Linha(String titular, Long rendaRecebida, Long retencoes, Long nifSublocatario, Long rendaPaga, Long nifSenhorio) {
        super(titular, rendaRecebida, retencoes, nifSublocatario, rendaPaga, nifSenhorio);
    }

    public boolean isEmpty() {
        return StringUtil.isEmpty(this.getTitular()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getRendaRecebida()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getRetencoes()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getNifSublocatario()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getRendaPaga()) && Modelo3IRSValidatorUtil.isEmptyOrZero(this.getNifSenhorio());
    }

    public static String getLink(int numLinha) {
        return "aAnexoF.qQuadro06.tanexoFq06T1.l" + numLinha;
    }

    public static String getLink(int numLinha, AnexoFq06T1_LinhaBase.Property column) {
        return AnexoFq06T1_Linha.getLink(numLinha) + ".c" + column.getIndex();
    }
}

