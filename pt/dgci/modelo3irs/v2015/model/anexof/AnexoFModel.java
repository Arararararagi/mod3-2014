/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.model.anexof;

import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModelBase;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro02;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro03;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexof.Quadro07;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;

@Min(value=0)
@Max(value=1)
public class AnexoFModel
extends AnexoFModelBase {
    public AnexoFModel(FormKey key, boolean addQuadrosToModel) {
        super(key, addQuadrosToModel);
    }

    public Quadro02 getQuadro02() {
        return (Quadro02)this.getQuadro(Quadro02.class.getSimpleName());
    }

    public Quadro03 getQuadro03() {
        return (Quadro03)this.getQuadro(Quadro03.class.getSimpleName());
    }

    public Quadro04 getQuadro04() {
        return (Quadro04)this.getQuadro(Quadro04.class.getSimpleName());
    }

    public Quadro05 getQuadro05() {
        return (Quadro05)this.getQuadro(Quadro05.class.getSimpleName());
    }

    public Quadro06 getQuadro06() {
        return (Quadro06)this.getQuadro(Quadro06.class.getSimpleName());
    }

    public Quadro07 getQuadro07() {
        return (Quadro07)this.getQuadro(Quadro07.class.getSimpleName());
    }

    public boolean isEmpty() {
        return this.getQuadro04().isEmptyOfRenda() && this.getQuadro05().isEmpty() && this.getQuadro06().isEmpty() && this.getQuadro07().isEmpty();
    }
}

