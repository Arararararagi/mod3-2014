/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.menus;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedMenuItem;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;

public class AdminMenu
extends JMenu {
    private static final long serialVersionUID = -8305689137878752166L;
    protected EnhancedAction writeDeclarationAction = ActionsTaxClientManager.getAction("WriteDeclarationAction");
    protected EnhancedAction readDeclarationAction = ActionsTaxClientManager.getAction("ReadDeclarationAction");
    protected EnhancedAction simulateDebugAction = ActionsTaxClientManager.getAction("SimulateDebugAction");

    public AdminMenu() {
        super("Admin");
        this.addActionsToMenu();
    }

    protected void addActionsToMenu() {
        if (this.readDeclarationAction != null) {
            this.add(new EnhancedMenuItem(this.readDeclarationAction));
        }
        if (this.writeDeclarationAction != null) {
            this.add(new EnhancedMenuItem(this.writeDeclarationAction));
        }
        if (this.simulateDebugAction != null) {
            this.add(new EnhancedMenuItem(this.simulateDebugAction));
        }
    }
}

