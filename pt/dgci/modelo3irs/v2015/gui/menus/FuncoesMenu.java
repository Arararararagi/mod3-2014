/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.menus;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import pt.dgci.modelo3irs.v2015.actions.ActionsModelo3IRSManager;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedMenuItem;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.StringUtil;

public class FuncoesMenu
extends JMenu {
    private static final long serialVersionUID = -8305689137878752166L;
    protected EnhancedAction validateAction;
    protected EnhancedAction simulateAction;
    protected EnhancedAction submitAction;
    protected EnhancedAction substituirAction;

    public FuncoesMenu() {
        super("Fun\u00e7\u00f5es");
        this.setMnemonic('u');
        this.validateAction = ActionsTaxClientManager.getAction("Validar");
        this.simulateAction = ActionsTaxClientManager.getAction("Simular");
        this.submitAction = ActionsTaxClientManager.getAction("Submeter");
        if (Session.isApplet() && StringUtil.isEmpty(Session.getApplet().getParameter("appletOffline"))) {
            this.substituirAction = ActionsModelo3IRSManager.getAction("Substituir");
        }
        this.addActionsToMenu();
    }

    protected void addActionsToMenu() {
        this.add(new EnhancedMenuItem(this.validateAction));
        if (this.simulateAction != null) {
            this.add(new EnhancedMenuItem(this.simulateAction));
        }
        if (this.submitAction != null) {
            this.add(new EnhancedMenuItem(this.submitAction));
        }
        if (this.substituirAction != null) {
            this.add(new EnhancedMenuItem(this.substituirAction));
        }
    }

    public EnhancedAction getValidateAction() {
        return this.validateAction;
    }

    public EnhancedAction getSimulateAction() {
        return this.simulateAction;
    }

    public EnhancedAction getSubmitAction() {
        return this.submitAction;
    }

    public EnhancedAction getSubstituirAction() {
        return this.substituirAction;
    }
}

