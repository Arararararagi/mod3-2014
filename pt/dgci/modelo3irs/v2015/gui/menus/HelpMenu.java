/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.menus;

import java.awt.Component;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedMenuItem;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;

public class HelpMenu
extends JMenu {
    private static final long serialVersionUID = -7029420245827874779L;
    protected EnhancedAction ajudaPreenchimentoAction;
    protected EnhancedAction ajudPorTemasAction;
    protected EnhancedAction facilitadorAction;
    protected EnhancedAction aboutAction;
    protected EnhancedAction dadosVersaoAction;

    public HelpMenu() {
        super("Ajuda");
        this.setMnemonic('a');
        this.ajudaPreenchimentoAction = ActionsTaxClientManager.getAction("AjudaPreenchimento");
        this.ajudPorTemasAction = ActionsTaxClientManager.getAction("Ajuda por Temas");
        this.facilitadorAction = ActionsTaxClientManager.getAction("Wizard");
        this.aboutAction = ActionsTaxClientManager.getAction("AcercaDe");
        this.dadosVersaoAction = ActionsTaxClientManager.getAction("Dados da Versao");
        this.addActionsToMenu();
    }

    protected void addActionsToMenu() {
        this.add(new EnhancedMenuItem(this.ajudaPreenchimentoAction));
        this.add(new EnhancedMenuItem(this.ajudPorTemasAction));
        this.add(new EnhancedMenuItem(this.facilitadorAction));
        this.add(new JSeparator());
        this.add(new EnhancedMenuItem(this.aboutAction));
        this.add(new EnhancedMenuItem(this.dadosVersaoAction));
    }
}

