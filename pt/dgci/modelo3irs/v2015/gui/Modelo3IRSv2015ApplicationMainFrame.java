/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Application;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.actions.ActionsModelo3IRSManager;
import pt.dgci.modelo3irs.v2015.actions.AddAnexoBPopupMenu;
import pt.dgci.modelo3irs.v2015.actions.AddAnexoCPopupMenu;
import pt.dgci.modelo3irs.v2015.actions.AddAnexoIPopupMenu;
import pt.dgci.modelo3irs.v2015.actions.AddAnexoJPopupMenu;
import pt.dgci.modelo3irs.v2015.actions.AddAnexoSSPopupMenu;
import pt.dgci.modelo3irs.v2015.gui.SimuladorPageDisplayer;
import pt.dgci.modelo3irs.v2015.gui.ThemeHelpDisplayer;
import pt.dgci.modelo3irs.v2015.gui.menus.AdminMenu;
import pt.dgci.modelo3irs.v2015.gui.menus.FuncoesMenu;
import pt.dgci.modelo3irs.v2015.gui.menus.HelpMenu;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoi.AnexoIModel;
import pt.dgci.modelo3irs.v2015.model.anexoj.AnexoJModel;
import pt.dgci.modelo3irs.v2015.model.anexol.AnexoLModel;
import pt.dgci.modelo3irs.v2015.model.anexoss.AnexoSSModel;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedToolBar;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.structural.add.AddAnexoAction;
import pt.opensoft.taxclient.actions.structural.add.single.impl.AddSingleAnexoAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.JTabbedCardLayoutPanel;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.help.HelpWizardDisplayer;
import pt.opensoft.taxclient.ui.help.impl.DefaultHelpDisplayer;
import pt.opensoft.taxclient.ui.help.impl.DefaultHelpWizardDisplayer;
import pt.opensoft.taxclient.ui.layout.Skeleton;
import pt.opensoft.taxclient.ui.layout.impl.DefaultSkeleton;
import pt.opensoft.taxclient.ui.menus.ClassicMenuBar;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.multi.impl.DefaultMultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.ui.navigation.impl.DefaultNavigationDisplayer;
import pt.opensoft.taxclient.ui.structure.StructureControllerDisplayer;
import pt.opensoft.taxclient.ui.structure.impl.DefaultStructureControllerDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;
import pt.opensoft.taxclient.ui.validation.impl.TitledValidationDisplayer;
import pt.opensoft.taxclient.ui.validation.impl.TitledWarningsDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class Modelo3IRSv2015ApplicationMainFrame
extends MainFrame {
    private static final long serialVersionUID = -7447521442958102235L;
    private Modelo3IRSv2015Application app;
    private static final String warningsMessage = "Verifique as situa\u00e7\u00f5es a seguir referidas e caso n\u00e3o identifique nenhuma incorrec\u00e7\u00e3o volte a submeter a declara\u00e7\u00e3o.";
    private static final String warningsFooter = "Os alertas exibidos n\u00e3o impedem a submiss\u00e3o da declara\u00e7\u00e3o tal como se encontra para o que dever\u00e1 utilizar novamente o bot\u00e3o submeter e assinalar que tomou o conhecimento dos mesmos, n\u00e3o pretendendo alterar os elementos declarados.";

    public Modelo3IRSv2015ApplicationMainFrame(DeclaracaoModel declaracaoModel, NavigationModel navigationModel) {
        super(declaracaoModel, navigationModel);
    }

    @Override
    public void initTopRibbon() {
    }

    @Override
    public void initMenuBar() {
        ClassicMenuBar classicMenuBar = new ClassicMenuBar();
        classicMenuBar.initFileMenu();
        classicMenuBar.initEditMenu();
        classicMenuBar.add(new FuncoesMenu());
        classicMenuBar.add(new HelpMenu());
        if (Modelo3IRSv2015Parameters.instance().isAdminActionsEnabled()) {
            classicMenuBar.add(new AdminMenu());
        }
        classicMenuBar.add(Box.createHorizontalGlue());
        this.menuBar = classicMenuBar;
    }

    @Override
    public void initTopMenuToolBar() {
        ListHashMap<String, EnhancedAction> actionsToolbar = new ListHashMap<String, EnhancedAction>();
        EnhancedAction[] rightAlignedActions = new EnhancedAction[2];
        actionsToolbar.put("Novo", ActionsTaxClientManager.getAction("Novo"));
        actionsToolbar.put("Abrir", ActionsTaxClientManager.getAction("Abrir"));
        actionsToolbar.put("Gravar", ActionsTaxClientManager.getAction("Gravar"));
        actionsToolbar.put("Imprimir", ActionsTaxClientManager.getAction("Imprimir"));
        actionsToolbar.put("Validar", ActionsTaxClientManager.getAction("Validar"));
        actionsToolbar.put("Simular", ActionsTaxClientManager.getAction("Simular"));
        actionsToolbar.put("Submeter", ActionsTaxClientManager.getAction("Submeter"));
        if (Session.isApplet() && StringUtil.isEmpty(Session.getApplet().getParameter("appletOffline"))) {
            actionsToolbar.put("Substituir", ActionsModelo3IRSManager.getAction("Substituir"));
        }
        actionsToolbar.put("Wizard", ActionsTaxClientManager.getAction("Wizard"));
        actionsToolbar.put("AjudaPreenchimento", ActionsTaxClientManager.getAction("AjudaPreenchimento"));
        actionsToolbar.put("Ajuda por Temas", ActionsTaxClientManager.getAction("Ajuda por Temas"));
        rightAlignedActions[0] = ActionsTaxClientManager.getAction("Pesquisar");
        rightAlignedActions[1] = ActionsTaxClientManager.getAction("AbrirPF");
        this.topMenuToolBar = new EnhancedToolBar(actionsToolbar, rightAlignedActions, 3);
        this.add((Component)this.topMenuToolBar, "North");
    }

    @Override
    protected void initMultiPurposeDisplay() {
        this.multiPurposeDisplayer = this.createMultiPurposeDisplayer();
        this.skeleton.pluginMultiPurposeDisplayer((JComponent)this.multiPurposeDisplayer);
        this.multiPurposeDisplayer.hideMultiPurposeDisplayer();
    }

    @Override
    protected DeclarationDisplayer createDeclarationDisplayer() {
        JTabbedCardLayoutPanel tabbedCardLayout = new JTabbedCardLayoutPanel(Session.getApplicationName());
        Session.getCurrentDeclaracao().addPropertyChangeListener("selectedQuadro", tabbedCardLayout);
        return tabbedCardLayout;
    }

    @Override
    protected NavigationDisplayer createNavigationDisplayer() {
        return new DefaultNavigationDisplayer();
    }

    @Override
    protected MultiPurposeDisplayer createMultiPurposeDisplayer() {
        DefaultMultiPurposeDisplayer purposeDisplayer = new DefaultMultiPurposeDisplayer(true);
        purposeDisplayer.setValidationDisplayer(new TitledValidationDisplayer("errorsModelo3IRSv2010.css", this.declaracaoModel));
        purposeDisplayer.setHelpWizardDisplayer(new DefaultHelpWizardDisplayer("/ajuda/wizard.htm", "errorsModelo3IRSv2010.css", "pt.dgci.modelo3irs.v2015.model", this.declaracaoModel));
        ThemeHelpDisplayer ajudaTemas = new ThemeHelpDisplayer("/ajuda/temas.htm", "/ajuda/ajudaPF.css");
        purposeDisplayer.addGenericCard("Ajuda por Temas", ajudaTemas);
        purposeDisplayer.setHelpDisplayer(new DefaultHelpDisplayer(true, true));
        SimuladorPageDisplayer simulador = new SimuladorPageDisplayer("/ajuda/simulador.htm", "/ajuda/simulador.css");
        purposeDisplayer.addGenericCard("Simular", simulador);
        DefaultHelpDisplayer searchDisplayer = new DefaultHelpDisplayer(true, true, IconFactory.getIconBig(GUIParameters.ICON_PESQUISA_AJUDA));
        purposeDisplayer.addGenericCard("Pesquisar", searchDisplayer);
        Pair<String, String> pair = new Pair<String, String>("", "");
        purposeDisplayer.setWarningDisplayer(new TitledWarningsDisplayer("errorsModelo3IRSv2010.css", this.declaracaoModel, pair, "Verifique as situa\u00e7\u00f5es a seguir referidas e caso n\u00e3o identifique nenhuma incorrec\u00e7\u00e3o volte a submeter a declara\u00e7\u00e3o.", "Os alertas exibidos n\u00e3o impedem a submiss\u00e3o da declara\u00e7\u00e3o tal como se encontra para o que dever\u00e1 utilizar novamente o bot\u00e3o submeter e assinalar que tomou o conhecimento dos mesmos, n\u00e3o pretendendo alterar os elementos declarados.", "Alertas da declara\u00e7\u00e3o", "Fechar painel de alertas"));
        return purposeDisplayer;
    }

    @Override
    protected Skeleton createSkeleton() {
        return new DefaultSkeleton();
    }

    @Override
    protected String getStatusBarText() {
        return "Vers\u00e3o: " + Modelo3IRSv2015Parameters.instance().getAppVersion() + " (" + Modelo3IRSv2015Parameters.instance().getAppVersionDate() + ")";
    }

    @Override
    protected StructureControllerDisplayer createStructureControllerDisplayer() {
        return new DefaultStructureControllerDisplayer();
    }

    @Override
    protected List<AddAnexoAction> createAddActions(DeclaracaoModel declaracaoModel) {
        ArrayList<AddAnexoAction> actions = new ArrayList<AddAnexoAction>();
        actions.add(new AddSingleAnexoAction(AnexoAModel.class, declaracaoModel));
        if (Modelo3IRSv2015Parameters.instance().isFase2()) {
            actions.add(new AddAnexoBPopupMenu(AnexoBModel.class, declaracaoModel, "Introduza o n\u00famero fiscal do Titular do Rendimento para este anexo: "));
            actions.add(new AddAnexoCPopupMenu(AnexoCModel.class, declaracaoModel, "Introduza o n\u00famero fiscal do Titular do Rendimento para este anexo: "));
            actions.add(new AddAnexoJPopupMenu(AnexoDModel.class, declaracaoModel, "Introduza o n\u00famero fiscal do Titular do Rendimento para este anexo: "));
            actions.add(new AddSingleAnexoAction(AnexoEModel.class, declaracaoModel));
            actions.add(new AddSingleAnexoAction(AnexoFModel.class, declaracaoModel));
            actions.add(new AddSingleAnexoAction(AnexoGModel.class, declaracaoModel));
            actions.add(new AddSingleAnexoAction(AnexoG1Model.class, declaracaoModel));
        }
        actions.add(new AddSingleAnexoAction(AnexoHModel.class, declaracaoModel));
        if (Modelo3IRSv2015Parameters.instance().isFase2()) {
            actions.add(new AddAnexoIPopupMenu(AnexoIModel.class, declaracaoModel, "Introduza o NIPC da Heran\u00e7a Indivisa: "));
        }
        actions.add(new AddAnexoJPopupMenu(AnexoJModel.class, declaracaoModel, "Introduza o n\u00famero fiscal do Titular do Rendimento para este anexo: "));
        actions.add(new AddAnexoJPopupMenu(AnexoLModel.class, declaracaoModel, "Introduza o n\u00famero fiscal do Titular do Rendimento para este anexo: "));
        if (Modelo3IRSv2015Parameters.instance().isFase2() && Modelo3IRSv2015Parameters.instance().isNET()) {
            actions.add(new AddAnexoSSPopupMenu(AnexoSSModel.class, declaracaoModel, "Introduza o n\u00famero fiscal do Titular do Rendimento para este anexo: "));
        }
        return actions;
    }

    public void setApp(Modelo3IRSv2015Application app) {
        this.app = app;
    }

    public Modelo3IRSv2015Application getApp() {
        return this.app;
    }
}

