/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.util.SubmitTaxUtils;
import pt.opensoft.util.StringUtil;

public class PrePreenchimentoReceiptDialog {
    private static final String NO_PP_RECEIPT_FRASE = "N\u00e3o existe informa\u00e7\u00e3o para pr\u00e9-preencher a sua declara\u00e7\u00e3o.";
    private static final String PP_LOAD_OK_MESSAGE_0 = "N\u00e3o foi poss\u00edvel converter a sua declara\u00e7\u00e3o, pelo que os dados que apresentamos prov\u00eam dos elementos fornecidos pelas suas Entidades Pagadoras.";
    private static final String PP_LOAD_OK_MESSAGE_1 = "A sua declara\u00e7\u00e3o foi pr\u00e9-preenchida com os elementos fornecidos pelas suas Entidades Pagadoras.";
    private static final String PP_LOAD_OK_MESSAGE_2 = "No caso de verificar alguma incorrec\u00e7\u00e3o, \u00e9 de sua responsabilidade proceder \u00e0 respectiva rectifica\u00e7\u00e3o (acrescentar, alterar ou eliminar).";
    private static final String ANEXOF_PP_LOAD_CONFIRMATION_MESSAGE = "O Quadro 4 do Anexo F pode ser pr\u00e9-preenchido com os dados referentes \u00e0 identifica\u00e7\u00e3o matricial dos pr\u00e9dios dos sujeitos passivos, constantes da declara\u00e7\u00e3o do ano anterior, os quais podem ser alterados.<br>Pretende este pr\u00e9-preenchimento?";
    private static final String TITLE_GET_DECL_RECEIPT_PROD = "Dados da Declara\u00e7\u00e3o Modelo 3 de IRS";
    private static final String RECEIPT_SPA = "Sujeito Passivo A";
    private static final String RECEIPT_SPB = "Sujeito Passivo B";
    private static final String RECEIPT_STATUS = "Estado Actual da Declara\u00e7\u00e3o";
    private static final String RECEIPT_YEAR = "Ano do Exerc\u00edcio";
    private Component parentComponent;
    private JPanel prePreenchimentoReceiptPanel;
    private boolean hasAnexoF;
    private int keepAnexoF;

    public PrePreenchimentoReceiptDialog(Component parentComponent, String exercicio, String nifA, String nifB, String status, boolean hasAnexoF) {
        this(parentComponent, exercicio, nifA, nifB, status, false, false, hasAnexoF);
    }

    public PrePreenchimentoReceiptDialog(Component parentComponent, String exercicio, String nifA, String nifB, String status, boolean isPPConversaoImpossivel, boolean showSubstituirMsg, boolean hasAnexoF) {
        this.parentComponent = parentComponent;
        this.hasAnexoF = hasAnexoF;
        JPanel panel = new JPanel(new GridLayout(3 + (!StringUtil.isEmpty(nifB) ? 1 : 0), 2, 0, 10));
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;Sujeito Passivo A: </b></html>"));
        panel.add(new JLabel(nifA));
        if (!StringUtil.isEmpty(nifB)) {
            panel.add(new JLabel("<html><b>&nbsp;&nbsp;Sujeito Passivo B: </b></html>"));
            panel.add(new JLabel(nifB));
        }
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;Ano do Exerc\u00edcio: </b></html>"));
        panel.add(new JLabel(exercicio));
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;Estado Actual da Declara\u00e7\u00e3o: </b></html>"));
        panel.add(new JLabel(Modelo3IRSv2015Parameters.DECL_STATUS.getDescr(status)));
        JPanel tips = new JPanel(new GridLayout(3, 1));
        if (isPPConversaoImpossivel) {
            tips.add(SubmitTaxUtils.htmlJLabel("N\u00e3o foi poss\u00edvel converter a sua declara\u00e7\u00e3o, pelo que os dados que apresentamos prov\u00eam dos elementos fornecidos pelas suas Entidades Pagadoras.", true, 75));
            tips.add(SubmitTaxUtils.htmlJLabel("No caso de verificar alguma incorrec\u00e7\u00e3o, \u00e9 de sua responsabilidade proceder \u00e0 respectiva rectifica\u00e7\u00e3o (acrescentar, alterar ou eliminar).", true, 75));
        } else {
            if (Modelo3IRSv2015Parameters.DECL_STATUS.COM_PRE_PREENCHIMENTO.equalsDeclStatus(status)) {
                tips.add(SubmitTaxUtils.htmlJLabel("A sua declara\u00e7\u00e3o foi pr\u00e9-preenchida com os elementos fornecidos pelas suas Entidades Pagadoras.", true, 75));
                tips.add(SubmitTaxUtils.htmlJLabel("No caso de verificar alguma incorrec\u00e7\u00e3o, \u00e9 de sua responsabilidade proceder \u00e0 respectiva rectifica\u00e7\u00e3o (acrescentar, alterar ou eliminar).", true, 75));
                if (hasAnexoF) {
                    tips.add(SubmitTaxUtils.htmlJLabel("O Quadro 4 do Anexo F pode ser pr\u00e9-preenchido com os dados referentes \u00e0 identifica\u00e7\u00e3o matricial dos pr\u00e9dios dos sujeitos passivos, constantes da declara\u00e7\u00e3o do ano anterior, os quais podem ser alterados.<br>Pretende este pr\u00e9-preenchimento?", true, 75));
                }
            }
            if (!(Modelo3IRSv2015Parameters.DECL_STATUS.COM_PRE_PREENCHIMENTO.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.INIT.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.EM_PREENCH.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.PRE_SUBMETIDA.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.SUBMETIDA.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.ACEITE.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.COM_ERROS.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.ENVIADA.equalsDeclStatus(status) || Modelo3IRSv2015Parameters.DECL_STATUS.ANULADA.equalsDeclStatus(status))) {
                tips.add(SubmitTaxUtils.htmlJLabel("N\u00e3o existe informa\u00e7\u00e3o para pr\u00e9-preencher a sua declara\u00e7\u00e3o.", true, 75));
            }
        }
        if (showSubstituirMsg) {
            JPanel substituirInfoPanel = new JPanel();
            substituirInfoPanel.setLayout(new FlowLayout(3, 2, 2));
            substituirInfoPanel.add(SubmitTaxUtils.htmlJLabel("Se pretende alterar a sua declara\u00e7\u00e3o utilize o bot\u00e3o <b>Substituir</b>", false, 75));
            substituirInfoPanel.add(new JLabel(IconFactory.getIconSmall("Submeter.png")));
            tips.add(substituirInfoPanel);
        }
        this.prePreenchimentoReceiptPanel = new JPanel(new BorderLayout(0, 20));
        this.prePreenchimentoReceiptPanel.add((Component)panel, "North");
        this.prePreenchimentoReceiptPanel.add((Component)tips, "Center");
    }

    public void showDialog() {
        String initialValue;
        Object[] options;
        if (this.hasAnexoF) {
            options = new Object[]{"Sim", "N\u00e3o"};
            initialValue = "Sim";
        } else {
            options = new Object[]{"Fechar"};
            initialValue = "Fechar";
        }
        this.keepAnexoF = JOptionPane.showOptionDialog(this.parentComponent, this.prePreenchimentoReceiptPanel, "Dados da Declara\u00e7\u00e3o Modelo 3 de IRS", -1, 1, null, options, initialValue);
    }

    public boolean keepAnexoF() {
        return this.keepAnexoF == 0;
    }
}

