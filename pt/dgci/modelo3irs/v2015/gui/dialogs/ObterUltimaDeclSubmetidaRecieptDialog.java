/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.util.SubmitTaxUtils;
import pt.opensoft.util.StringUtil;

public class ObterUltimaDeclSubmetidaRecieptDialog {
    private static final String TITLE_GET_DECL_RECEIPT_PROD = "Dados da Declara\u00e7\u00e3o Modelo 3 de IRS";
    private static final String RECEIPT_SPA = "Sujeito Passivo A";
    private static final String RECEIPT_SPB = "Sujeito Passivo B";
    private static final String RECEIPT_STATUS = "Estado Actual da Declara\u00e7\u00e3o";
    private static final String RECEIPT_YEAR = "Ano do Exerc\u00edcio";
    private Component parentComponent;
    private JPanel prePreenchimentoReceiptPanel;

    public ObterUltimaDeclSubmetidaRecieptDialog(Component parentComponent, String exercicio, String nifA, String nifB, String status) {
        this(parentComponent, exercicio, nifA, nifB, status, false, false);
    }

    public ObterUltimaDeclSubmetidaRecieptDialog(Component parentComponent, String exercicio, String nifA, String nifB, String status, boolean isPPConversaoImpossivel, boolean showSubstituirMsg) {
        this.parentComponent = parentComponent;
        JPanel panel = new JPanel(new GridLayout(3 + (!StringUtil.isEmpty(nifB) ? 1 : 0), 2, 0, 10));
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;Sujeito Passivo A: </b></html>"));
        panel.add(new JLabel(nifA));
        if (!StringUtil.isEmpty(nifB)) {
            panel.add(new JLabel("<html><b>&nbsp;&nbsp;Sujeito Passivo B: </b></html>"));
            panel.add(new JLabel(nifB));
        }
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;Ano do Exerc\u00edcio: </b></html>"));
        panel.add(new JLabel(exercicio));
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;Estado Actual da Declara\u00e7\u00e3o: </b></html>"));
        panel.add(new JLabel(Modelo3IRSv2015Parameters.DECL_STATUS.getDescr(status)));
        JPanel tips = new JPanel(new GridLayout(3, 1));
        if (showSubstituirMsg) {
            JPanel substituirInfoPanel = new JPanel();
            substituirInfoPanel.setLayout(new FlowLayout(3, 2, 2));
            substituirInfoPanel.add(SubmitTaxUtils.htmlJLabel("Se pretende alterar a sua declara\u00e7\u00e3o utilize o bot\u00e3o <b>Substituir</b>", false, 75));
            substituirInfoPanel.add(new JLabel(IconFactory.getIconSmall("Submeter.png")));
            tips.add(substituirInfoPanel);
        }
        this.prePreenchimentoReceiptPanel = new JPanel(new BorderLayout(0, 20));
        this.prePreenchimentoReceiptPanel.add((Component)panel, "North");
        this.prePreenchimentoReceiptPanel.add((Component)tips, "Center");
    }

    public void showDialog() {
        JOptionPane.showOptionDialog(this.parentComponent, this.prePreenchimentoReceiptPanel, "Dados da Declara\u00e7\u00e3o Modelo 3 de IRS", -1, 1, null, new Object[]{"Fechar"}, "Fechar");
    }
}

