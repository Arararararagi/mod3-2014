/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.util.HashMap;
import java.util.Map;
import pt.opensoft.util.StringUtil;

public class PrePreenchimentoResponse {
    private int errorCode;
    private Map optionalFields;
    private String modeloXML;
    private String message;
    private String status;

    public PrePreenchimentoResponse(int errorCode) {
        this(errorCode, null, null, null, null);
    }

    public PrePreenchimentoResponse(int errorCode, String modeloXML) {
        this(errorCode, modeloXML, null, null, null);
    }

    public PrePreenchimentoResponse(int errorCode, String modeloXML, String message) {
        this(errorCode, modeloXML, message, null, null);
    }

    public PrePreenchimentoResponse(int errorCode, String modeloXML, String message, String status) {
        this(errorCode, modeloXML, message, status, null);
    }

    public PrePreenchimentoResponse(int errorCode, String modeloXML, String message, String status, Map optionalFields) {
        this.errorCode = errorCode;
        this.modeloXML = modeloXML;
        this.message = message;
        this.optionalFields = optionalFields;
        this.status = StringUtil.isEmpty(status) ? null : status;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getModeloXML() {
        return this.modeloXML;
    }

    public void setModeloXML(String modeloXML) {
        this.modeloXML = modeloXML;
    }

    public Map getOptionalFields() {
        return this.optionalFields;
    }

    public void addOptionalField(String key, String value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("args are null");
        }
        if (this.optionalFields == null) {
            this.optionalFields = new HashMap();
        }
        this.optionalFields.put(key, value);
    }

    public String getStatus() {
        return this.status;
    }
}

