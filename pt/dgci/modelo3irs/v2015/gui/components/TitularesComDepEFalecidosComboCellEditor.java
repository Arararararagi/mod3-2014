/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.components;

import java.util.List;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.gui.components.CatalogUtil;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesComboCellEditorBase;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.util.TitularesEnum;

public class TitularesComDepEFalecidosComboCellEditor
extends TitularesComboCellEditorBase {
    private static final long serialVersionUID = -1250928519324628025L;

    public TitularesComDepEFalecidosComboCellEditor(RostoModel rosto, Cat_M3V2015_RostoTitulares catalogo) {
        super(rosto, catalogo);
    }

    @Override
    protected List<TitularesEnum> getTitulares() {
        return CatalogUtil.buildTitularesComDepEFalecidosCatalog();
    }

    @Override
    protected String getCatalogName() {
        return "Cat_M3V2015_RostoTitularesComDepEFalecidos";
    }
}

