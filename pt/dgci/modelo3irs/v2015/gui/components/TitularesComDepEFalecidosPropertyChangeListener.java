/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.components;

import com.jgoodies.binding.list.SelectionInList;
import java.util.List;
import javax.swing.JComboBox;
import pt.dgci.modelo3irs.v2015.gui.components.CatalogUtil;
import pt.dgci.modelo3irs.v2015.gui.components.TitularesPropertyChangeListenerBase;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.util.TitularesEnum;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class TitularesComDepEFalecidosPropertyChangeListener
extends TitularesPropertyChangeListenerBase {
    public TitularesComDepEFalecidosPropertyChangeListener(RostoModel rosto, JComboBox comboBox) {
        super(rosto, comboBox);
    }

    public TitularesComDepEFalecidosPropertyChangeListener(RostoModel rosto, JComboBox comboBox, SelectionInList<ICatalogItem> selectionInListC1) {
        super(rosto, comboBox, selectionInListC1);
    }

    @Override
    protected List<TitularesEnum> getTitulares() {
        return CatalogUtil.buildTitularesComDepEFalecidosCatalog();
    }

    @Override
    protected String getCatalogName() {
        return "Cat_M3V2015_RostoTitularesComDepEFalecidos";
    }
}

