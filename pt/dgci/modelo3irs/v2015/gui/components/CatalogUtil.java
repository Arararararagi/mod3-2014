/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.components;

import java.util.ArrayList;
import java.util.List;
import pt.dgci.modelo3irs.v2015.util.TitularesEnum;

public class CatalogUtil {
    public static List<TitularesEnum> buildTitularesComCDepEFalecidosCatalog() {
        ArrayList<TitularesEnum> titulares = new ArrayList<TitularesEnum>();
        titulares.add(TitularesEnum.SPAB);
        titulares.add(TitularesEnum.SPC);
        titulares.add(TitularesEnum.D);
        titulares.add(TitularesEnum.DD);
        titulares.add(TitularesEnum.DG);
        titulares.add(TitularesEnum.F);
        return titulares;
    }

    public static List<TitularesEnum> buildTitularesComDepAfiAscSPCEFalecidosCatalog() {
        ArrayList<TitularesEnum> titulares = new ArrayList<TitularesEnum>();
        titulares.add(TitularesEnum.SPAB);
        titulares.add(TitularesEnum.SPC);
        titulares.add(TitularesEnum.D);
        titulares.add(TitularesEnum.DD);
        titulares.add(TitularesEnum.DG);
        titulares.add(TitularesEnum.AF);
        titulares.add(TitularesEnum.AS);
        titulares.add(TitularesEnum.AC);
        titulares.add(TitularesEnum.F);
        return titulares;
    }

    public static List<TitularesEnum> buildTitularesComDepAfiAscEFalecidosCatalog() {
        ArrayList<TitularesEnum> titulares = new ArrayList<TitularesEnum>();
        titulares.add(TitularesEnum.SPAB);
        titulares.add(TitularesEnum.D);
        titulares.add(TitularesEnum.DD);
        titulares.add(TitularesEnum.DG);
        titulares.add(TitularesEnum.AF);
        titulares.add(TitularesEnum.AS);
        titulares.add(TitularesEnum.AC);
        titulares.add(TitularesEnum.F);
        return titulares;
    }

    public static List<TitularesEnum> buildTitularesComDepEFalecidosCatalog() {
        ArrayList<TitularesEnum> titulares = new ArrayList<TitularesEnum>();
        titulares.add(TitularesEnum.SPAB);
        titulares.add(TitularesEnum.D);
        titulares.add(TitularesEnum.DD);
        titulares.add(TitularesEnum.DG);
        titulares.add(TitularesEnum.F);
        return titulares;
    }

    public static List<TitularesEnum> buildTitularesComDepSPCEFalecidosCatalog() {
        ArrayList<TitularesEnum> titulares = new ArrayList<TitularesEnum>();
        titulares.add(TitularesEnum.SPAB);
        titulares.add(TitularesEnum.SPC);
        titulares.add(TitularesEnum.D);
        titulares.add(TitularesEnum.DD);
        titulares.add(TitularesEnum.DG);
        titulares.add(TitularesEnum.F);
        return titulares;
    }
}

