/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.components;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.swing.EventComboBoxModel;
import java.awt.Component;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.util.TitularesEnum;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public abstract class TitularesComboCellEditorBase
extends AbstractCellEditor
implements TableCellEditor {
    private static final long serialVersionUID = -1250928519324628025L;
    private RostoModel rosto;
    private JComboBox comboBoxTitulares = new JComboBox();

    public TitularesComboCellEditorBase(RostoModel rosto, Cat_M3V2015_RostoTitulares catalogo) {
        this.rosto = rosto;
        this.update(null, catalogo, true, 0, 0);
    }

    @Override
    public Object getCellEditorValue() {
        if (!(this.comboBoxTitulares.getSelectedItem() == null || StringUtil.isEmpty(String.valueOf(((Cat_M3V2015_RostoTitulares)this.comboBoxTitulares.getSelectedItem()).getValue())))) {
            return (Cat_M3V2015_RostoTitulares)this.comboBoxTitulares.getSelectedItem();
        }
        return null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return this.update(table, value, isSelected, row, column);
    }

    private Component update(JTable table, Object value, boolean isSelected, int row, int column) {
        ListHashMap titularesList = (ListHashMap)CatalogManager.getInstance().getCatalog(this.getCatalogName());
        titularesList.clear();
        Cat_M3V2015_RostoTitulares titularSelected = (Cat_M3V2015_RostoTitulares)value;
        String selectedKey = "";
        if (titularSelected != null) {
            selectedKey = (String)titularSelected.getValue();
        }
        List<TitularesEnum> titulares = this.getTitulares();
        List<Cat_M3V2015_RostoTitulares> titularesAsCatalogo = this.rosto.getTitularesAsCatalogo(titulares);
        for (Cat_M3V2015_RostoTitulares Cat_M3V2015_RostoTitulares : titularesAsCatalogo) {
            titularesList.put(Cat_M3V2015_RostoTitulares.getValue(), Cat_M3V2015_RostoTitulares);
            if (Cat_M3V2015_RostoTitulares.getValue() == null || !Cat_M3V2015_RostoTitulares.getValue().equals(selectedKey)) continue;
            titularSelected = Cat_M3V2015_RostoTitulares;
        }
        BasicEventList eventList = new BasicEventList();
        eventList.addAll(CatalogManager.getInstance().getCatalog(this.getCatalogName()).elements());
        this.comboBoxTitulares.setModel(new EventComboBoxModel(eventList));
        this.comboBoxTitulares.setSelectedItem(titularSelected);
        return this.comboBoxTitulares;
    }

    protected abstract List<TitularesEnum> getTitulares();

    protected abstract String getCatalogName();
}

