/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui.components;

import ca.odell.glazedlists.BasicEventList;
import com.jgoodies.binding.list.SelectionInList;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;
import javax.swing.JComboBox;
import pt.dgci.modelo3irs.v2015.catalogs.Cat_M3V2015_RostoTitulares;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.util.TitularesEnum;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.util.ListMap;

public abstract class TitularesPropertyChangeListenerBase
implements PropertyChangeListener {
    protected RostoModel rosto;
    protected JComboBox comboBox;
    protected SelectionInList<ICatalogItem> selected;

    public TitularesPropertyChangeListenerBase(RostoModel rosto, JComboBox comboBox) {
        this.rosto = rosto;
        this.comboBox = comboBox;
        this.propertyChange(null);
    }

    public TitularesPropertyChangeListenerBase(RostoModel rosto, JComboBox comboBox, SelectionInList<ICatalogItem> selectionInListC1) {
        this.rosto = rosto;
        this.comboBox = comboBox;
        this.selected = selectionInListC1;
        this.propertyChange(null);
    }

    @Override
    public void propertyChange(PropertyChangeEvent arg0) {
        ListMap map = CatalogManager.getInstance().getCatalog(this.getCatalogName());
        map.clear();
        List<TitularesEnum> titulares = this.getTitulares();
        List<Cat_M3V2015_RostoTitulares> titularesAsCatalogo = this.rosto.getTitularesAsCatalogo(titulares);
        if (this.selected != null) {
            this.selected.getList().clear();
        }
        for (Cat_M3V2015_RostoTitulares Cat_M3V2015_RostoTitulares : titularesAsCatalogo) {
            map.put(Cat_M3V2015_RostoTitulares.getValue(), Cat_M3V2015_RostoTitulares);
            if (this.selected == null || this.selected.getList().contains(Cat_M3V2015_RostoTitulares)) continue;
            this.selected.getList().add(Cat_M3V2015_RostoTitulares);
        }
        BasicEventList eventList = new BasicEventList();
        eventList.addAll(CatalogManager.getInstance().getCatalog(this.getCatalogName()).elements());
    }

    protected abstract List<TitularesEnum> getTitulares();

    protected abstract String getCatalogName();
}

