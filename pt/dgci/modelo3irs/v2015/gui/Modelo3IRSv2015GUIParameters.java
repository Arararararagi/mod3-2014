/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.io.InputStream;
import pt.opensoft.util.SimpleParameters;

public class Modelo3IRSv2015GUIParameters
extends SimpleParameters {
    public static final String DEFAULT_ICON_AJUDA = "ajudaPreenchimento.png";
    public static final String DEFAULT_ICON_SUBMETER = "Submeter.png";
    public static final String DEFAULT_ICON_NOVO = "new.png";
    public static final String DEFAULT_ICON_ABRIR = "open.png";
    public static final String DEFAULT_ICON_GRAVAR = "save.png";
    public static final String DEFAULT_ICON_IMPRIMIR = "print.png";
    public static final String DEFAULT_ICON_VALIDAR = "Validar.png";
    public static final String DEFAULT_ICON_LAF = "Theme.png";
    public static final String DEFAULT_ICON_APP_MENU = "AppMenuButton.png";
    public static final String DEFAULT_ICON_HELP_BROWSER = "HelpBrowser.png";
    public static final String DEFAULT_ICON_APP_LOGO = "Modelo3IRSv2015_Logo.png";
    public static final String DEFAULT_ICON_CALC = "xcalc.gif";
    public static String ICON_AJUDA;
    public static String ICON_SUBMETER;
    public static String ICON_NOVO;
    public static String ICON_ABRIR;
    public static String ICON_GRAVAR;
    public static String ICON_IMPRIMIR;
    public static String ICON_VALIDAR;
    public static String ICON_LAF;
    public static String ICON_APP_MENU;
    public static String ICON_HELP_BROWSER;
    public static String ICON_APP_LOGO;
    private static Modelo3IRSv2015GUIParameters instance;

    public static Modelo3IRSv2015GUIParameters instance() {
        return instance;
    }

    public static void load(String parametersFileName) {
        InputStream paramInput = Modelo3IRSv2015GUIParameters.class.getResourceAsStream(parametersFileName);
        instance = new Modelo3IRSv2015GUIParameters(paramInput);
    }

    public Modelo3IRSv2015GUIParameters(InputStream paramInput) {
        this.read(paramInput);
        ICON_NOVO = this.getString("app.icon.Novo", "new.png");
        ICON_ABRIR = this.getString("app.icon.Abrir", "open.png");
        ICON_GRAVAR = this.getString("app.icon.Gravar", "save.png");
        ICON_IMPRIMIR = this.getString("app.icon.Imprimir", "print.png");
        ICON_AJUDA = this.getString("app.icon.Ajuda", "ajudaPreenchimento.png");
        ICON_VALIDAR = this.getString("app.icon.Validar", "Validar.png");
        ICON_SUBMETER = this.getString("app.icon.Submeter", "Submeter.png");
        ICON_LAF = this.getString("app.icon.LAF", "Theme.png");
        ICON_HELP_BROWSER = this.getString("app.icon.Help.Browser", "HelpBrowser.png");
        ICON_APP_LOGO = this.getString("app.icon.AppLogo", "Modelo3IRSv2015_Logo.png");
    }
}

