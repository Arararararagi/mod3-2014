/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.net.MalformedURLException;
import java.net.URL;
import pt.dgci.modelo3irs.v2015.actions.util.PrePreenchimentoSubmitInfo;
import pt.dgci.modelo3irs.v2015.gui.PrePreenchimentoHTTPConnection;
import pt.dgci.modelo3irs.v2015.gui.PrePreenchimentoResponse;
import pt.dgci.modelo3irs.v2015.gui.PrePreenchimentoTaxThread;

public class PrePreenchimentoTaxThreadHttpConnection
extends PrePreenchimentoTaxThread {
    public PrePreenchimentoTaxThreadHttpConnection(String url, PrePreenchimentoSubmitInfo submitRequest) {
        super(url, submitRequest);
    }

    @Override
    public void initSubmitTax() throws MalformedURLException {
        this.submitTax = new PrePreenchimentoHTTPConnection(new URL(this.url));
    }

    @Override
    public String getStatusDescr() {
        if (this.submitTax == null) {
            return "Comunicando com o servidor...";
        }
        return this.submitTax.getStateDescriptionOfHttpPost();
    }

    public PrePreenchimentoResponse getPrePreenchimentoResponse() {
        return this.submitTax.getPrePreenchimentoResponse();
    }
}

