/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.net.MalformedURLException;
import pt.dgci.modelo3irs.v2015.actions.util.PrePreenchimentoSubmitInfo;
import pt.dgci.modelo3irs.v2015.gui.PrePreenchimentoHTTPConnection;

public abstract class PrePreenchimentoTaxThread
extends Thread
implements Runnable {
    private Exception exception = null;
    private boolean completed = false;
    private boolean sucess = false;
    protected PrePreenchimentoHTTPConnection submitTax;
    protected String url;
    private PrePreenchimentoSubmitInfo submitRequest;

    public PrePreenchimentoTaxThread(String url, PrePreenchimentoSubmitInfo submitRequest) {
        this.url = url;
        this.submitRequest = submitRequest;
    }

    @Override
    public void run() {
        try {
            this.initSubmitTax();
            this.submitTax.submit(this.submitRequest);
            this.sucess = true;
        }
        catch (Exception e) {
            this.exception = e;
        }
        finally {
            this.completed = true;
        }
    }

    public abstract void initSubmitTax() throws MalformedURLException;

    public boolean isSucessfull() {
        return this.sucess;
    }

    public boolean isCompleted() {
        return this.completed;
    }

    public Exception getException() {
        return this.exception;
    }

    public abstract String getStatusDescr();
}

