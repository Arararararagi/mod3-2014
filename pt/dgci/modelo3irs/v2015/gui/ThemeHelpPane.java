/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.util.SimpleLog;

public class ThemeHelpPane
extends ScrollableHtmlPane
implements HyperlinkListener,
TextListener {
    private static final long serialVersionUID = -1975872212437250807L;
    private String _anchor;

    public ThemeHelpPane(String initialText) {
        this(".", null, initialText);
    }

    public ThemeHelpPane(String baseURL, String page, String initialText) {
        this(baseURL, page, initialText, null);
    }

    public ThemeHelpPane(String baseURL, String page, String initialText, String anchor) {
        this.setEditable(false);
        this.setContentType("text/html");
        this.addHyperlinkListener(this);
        this.setText(initialText);
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            URL url = e.getURL();
            this._anchor = url.getRef();
            if (this._anchor.equals("")) {
                try {
                    url = new URL(url.toString().substring(0, url.toString().length() - 1));
                }
                catch (MalformedURLException e1) {
                    throw new RuntimeException(e1);
                }
            }
            try {
                this.setPage(url);
            }
            catch (IOException exception) {
                throw new RuntimeException(exception);
            }
        }
    }

    @Override
    public void textValueChanged(TextEvent arg0) {
        SimpleLog.log("Entered AWTEvent " + arg0.toString());
    }
}

