/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import pt.dgci.modelo3irs.v2015.actions.util.ObterUltimaDeclSubmetidaSubmitInfo;
import pt.dgci.modelo3irs.v2015.gui.ObterUltimaDeclSubmetidaResponse;
import pt.opensoft.crypt.Password;
import pt.opensoft.http.HttpResponse;
import pt.opensoft.http.JavaHttpConnection;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.app.ProxyParameters;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.util.WebServicesUtil;
import pt.opensoft.text.Base64;
import pt.opensoft.text.Base64Decoder;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.XmlBuilder;

public class ObterDeclSubmetidaHttpConnection {
    protected ObterUltimaDeclSubmetidaResponse obterUltimaDeclSubmetidaResponse;
    public static final String SUJ_PASS_A_ID = "SPA";
    public static final String SUJ_PASS_B_ID = "SPB";
    public static final String IRS_PASS_A = "PassA";
    public static final String IRS_PASS_B = "PassB";
    public static final String TOC_ID = "TOC";
    public static final String IRS_PASS_TOC = "PassTOC";
    public static final int RESPONSE_CODE_OK = 0;
    public static final int RESPONSE_CODE_ERROR_INVALID_REQUEST = 96;
    public static final int RESPONSE_CODE_EXCEPTION = 97;
    public static final int RESPONSE_CODE_UNAUTHORIZED = 98;
    public static final int RESPONSE_CODE_NOK = 99;
    protected static final String RESPONSE_PARAMETER_CODE = "code";
    protected static final String RESPONSE_PARAMETER_MESSAGE = "message";
    protected static final String RESPONSE_PARAMETER_ERROR = "error";
    protected static final String RESPONSE_PARAMETER_WARNING = "warning";
    protected static final String RESPONSE_PARAMETER_RECEIPT = "receipt";
    protected static final String RESPONSE_PARAMETER_OPTIONAL = "optional";
    protected static final String RESPONSE_PARAMETER_NIFS = "nif";
    protected static final String RESPONSE_RECEIPT_DATE = "date";
    protected static final String RESPONSE_RECEIPT_YEAR = "year";
    protected static final String RESPONSE_RECEIPT_DECLID = "declid";
    protected static final String RESPONSE_NIF_NAME = "name";
    protected static final String RESPONSE_NIF_VALUE = "nif";
    protected static final String RESPONSE_OPTIONAL_NAME = "key";
    protected static final String RESPONSE_OPTIONAL_VALUE = "value";
    protected static final String RESPONSE_PARAMETER_STATUS = "status";
    protected static final String RESPONSE_PARAMETER_ERROR_REFERENCE = "reference";
    protected static final String RESPONSE_ERROR_CODE_ATTRIBUTE = "code";
    protected static final String RESPONSE_ERROR_FIELDREF_ATTRIBUTE = "field";
    public static final String HTTP_STATE_IDLE = "Comunicando com o servidor...";
    protected static final String HTTP_STATE_SEND = "A enviar dados...";
    protected static final String HTTP_STATE_RECEIVE = "A receber resposta do servidor...";
    protected static final String HTTP_STATE_FINISH = "Comunica\u00e7\u00e3o terminada...";
    protected URL url;
    protected JavaHttpConnection httpConnection;
    protected static final int TIMEOUT = ProxyParameters.getConnectionTimeout();
    protected static final String DECL_TAG = "decl";
    private static final String CHECK_TAG = "checkwarnings";
    private static final String POST_NAME = "tax";
    private static final String DE_SYSTEM_NAME = "taxUser";
    private static final String DE_SYSTEM_PASS = "y5jeC$maQe";
    private static final String SYSTEM_NAME_TAG = "systemName";
    private static final String SYSTEM_PASS_TAG = "systemPass";
    private static final String APP_VERSION_TAG = "app_version";
    private static final String DECL_VERSION_TAG = "declVersion";
    private static final String IRS_EXERC = "Exercicio";
    private boolean completed;

    public ObterDeclSubmetidaHttpConnection(URL url) {
        if (url == null) {
            throw new IllegalArgumentException("url cannot be null");
        }
        this.url = url;
        this.initializeConnection();
    }

    public boolean addRequestParametersToWebservice(ObterUltimaDeclSubmetidaSubmitInfo submitInfo) {
        PortalTaxClientRevampedSubmitUser submitUser = submitInfo.getSubmitUser();
        if (submitInfo.hasAppVersion()) {
            this.addParameter("app_version", submitInfo.getAppVersion());
        }
        this.addParameter("checkwarnings", String.valueOf(submitInfo.isCheckWarnings()));
        this.addParameter("SPA", submitUser.getNifA());
        this.addParameter("nifA", submitUser.getNifA());
        this.addParameter("passwordA", Password.encryptWithPaddingLength(submitUser.getPasswordA()));
        if (!StringUtil.isEmpty(submitUser.getNifB())) {
            this.addParameter("SPB", submitUser.getNifB());
            this.addParameter("nifB", submitUser.getNifB());
            this.addParameter("passwordB", Password.encryptWithPaddingLength(submitUser.getPasswordB()));
        }
        for (Pair optionalField : submitInfo.getOptionalFields()) {
            this.addParameter((String)optionalField.getFirst(), (String)optionalField.getSecond());
        }
        this.addParameter("ws_version", "1.0");
        this.addParameter("Exercicio", String.valueOf(submitInfo.getExercicio()));
        return true;
    }

    public final void initializeConnection() throws IllegalArgumentException {
        this.httpConnection = new JavaHttpConnection(this.url);
        this.httpConnection.setTimeout(TIMEOUT);
        try {
            if (ProxyParameters.getUseProxy() && ProxyParameters.isProxySet()) {
                this.httpConnection.setProxy(ProxyParameters.getProxyHost(), Integer.parseInt(ProxyParameters.getProxyPort()));
            }
        }
        catch (NumberFormatException e) {
            throw new IllegalArgumentException("Proxy Port is not a valid number.", e);
        }
    }

    public ValidationResult parseErrorsFromResponse(Element rootElement) throws JDOMException {
        List errorList = rootElement.getChildren("error");
        ValidationResult result = new ValidationResult();
        for (Element error : errorList) {
            String errorCode = error.getAttributeValue("code");
            String fieldReference = error.getAttributeValue("field");
            List links = error.getChildren("reference");
            String[] fieldLinks = new String[links.size()];
            for (int i = 0; i < links.size(); ++i) {
                String link;
                fieldLinks[i] = link = ((Element)links.get(i)).getText();
            }
            result.add(new DeclValidationMessage(errorCode, fieldReference, fieldLinks));
        }
        return result;
    }

    public Map parseOptionalFromResponse(Element rootElement) throws JDOMException {
        List optionalFieldsList = rootElement.getChildren("optional");
        if (!(optionalFieldsList == null || optionalFieldsList.isEmpty())) {
            HashMap<String, String> parseMap = new HashMap<String, String>();
            int j = optionalFieldsList.size();
            for (int i = 0; i < j; ++i) {
                Element optional = (Element)optionalFieldsList.get(i);
                String key = optional.getAttributeValue("key");
                String value = optional.getAttributeValue("value");
                if (StringUtil.isEmpty(key) || StringUtil.isEmpty(value)) {
                    throw new JDOMException("Optional field has key ou value empty");
                }
                parseMap.put(key, value);
            }
            return parseMap;
        }
        return null;
    }

    public List parseWarningsFromResponse(Element rootElement) throws JDOMException {
        List warningList = rootElement.getChildren("warning");
        if (!(warningList == null || warningList.isEmpty())) {
            ArrayList<String> parseList = new ArrayList<String>();
            int j = warningList.size();
            for (int i = 0; i < j; ++i) {
                Element warning = (Element)warningList.get(i);
                String msg = warning.getText();
                if (StringUtil.isEmpty(msg)) {
                    throw new JDOMException("Warning message is empty!");
                }
                parseList.add(Base64.decode(msg));
            }
            return parseList;
        }
        return null;
    }

    public void receiveSubmitResponse(Document response) throws JDOMException, IOException {
        String message;
        Element element = response.getRootElement();
        if (element == null) {
            throw new JDOMException("Root Element \u00e9 vazio!");
        }
        int code = Integer.parseInt(element.getAttributeValue("code"));
        String string = message = StringUtil.isEmpty(String.valueOf(element.getAttributeValue("message"))) ? "" : new String(Base64Decoder.decodeBuffer(String.valueOf(element.getAttributeValue("message"))));
        if (code == 0) {
            Element modelo = (Element)element.getChildren("modeloXML").get(0);
            String modeloXML = WebServicesUtil.getZipDecodedData(modelo.getTextTrim());
            String status = element.getAttributeValue("status");
            this.obterUltimaDeclSubmetidaResponse = new ObterUltimaDeclSubmetidaResponse(code, modeloXML, message, status);
        } else {
            this.obterUltimaDeclSubmetidaResponse = new ObterUltimaDeclSubmetidaResponse(code, null, message);
        }
    }

    public void submit(ObterUltimaDeclSubmetidaSubmitInfo submitInfo) {
        try {
            boolean parametersAdded = this.addRequestParametersToWebservice(submitInfo);
            if (!parametersAdded) {
                throw new IOException("Error occured when adding parameters to the request");
            }
            HttpResponse response = this.httpConnection.send();
            if (response == null) {
                throw new IOException("Response is null!");
            }
            XmlBuilder xmlBuilder = new XmlBuilder();
            List saxParseExceptions = xmlBuilder.parse(new StringReader(response.getBody().replaceAll("\r", "").replaceAll("\n", "").trim()));
            if (!saxParseExceptions.isEmpty()) {
                throw new JDOMException("There were errors while parsing response" + saxParseExceptions.get(0).toString());
            }
            Document result = xmlBuilder.getDocument();
            if (result == null) {
                throw new JDOMException("Document in response is null!");
            }
            this.receiveSubmitResponse(result);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected int getStateOfHttpPost() {
        return this.httpConnection.getCommunicationThreadStatus();
    }

    public String getStateDescriptionOfHttpPost() {
        switch (this.getStateOfHttpPost()) {
            case 0: {
                return "Comunicando com o servidor...";
            }
            case 2: {
                return "A receber resposta do servidor...";
            }
            case 1: {
                return "A enviar dados...";
            }
            case 3: {
                return "Comunica\u00e7\u00e3o terminada...";
            }
        }
        throw new IllegalStateException("HTTP Connection has invalid state");
    }

    protected void addParameter(String name, String value) {
        this.httpConnection.addParameter(name, WebServicesUtil.encode(value));
    }

    public ObterUltimaDeclSubmetidaResponse getPrePreenchimentoResponse() {
        return this.obterUltimaDeclSubmetidaResponse;
    }
}

