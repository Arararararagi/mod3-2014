/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.net.MalformedURLException;
import java.net.URL;
import pt.dgci.modelo3irs.v2015.actions.util.ObterUltimaDeclSubmetidaSubmitInfo;
import pt.dgci.modelo3irs.v2015.gui.ObterDeclSubmetidaHttpConnection;
import pt.dgci.modelo3irs.v2015.gui.ObterUltimaDeclSubmetidaResponse;
import pt.dgci.modelo3irs.v2015.gui.ObterUltimaDeclTaxThread;

public class ObterUltimaDeclTaxThreadHttpConnection
extends ObterUltimaDeclTaxThread {
    public ObterUltimaDeclTaxThreadHttpConnection(String url, ObterUltimaDeclSubmetidaSubmitInfo submitRequest) {
        super(url, submitRequest);
    }

    @Override
    public void initSubmitTax() throws MalformedURLException {
        this.submitTax = new ObterDeclSubmetidaHttpConnection(new URL(this.url));
    }

    @Override
    public String getStatusDescr() {
        if (this.submitTax == null) {
            return "Comunicando com o servidor...";
        }
        return this.submitTax.getStateDescriptionOfHttpPost();
    }

    public ObterUltimaDeclSubmetidaResponse getPrePreenchimentoResponse() {
        return this.submitTax.getPrePreenchimentoResponse();
    }
}

