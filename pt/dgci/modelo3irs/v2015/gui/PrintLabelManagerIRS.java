/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.io.InputStream;
import pt.dgci.modelo3irs.v2015.print.util.LabelParameters;

public class PrintLabelManagerIRS {
    protected LabelParameters properties;

    public String getCampo(String quadro, String campo) {
        String result = this.properties.getLabelFor(quadro, campo);
        if (result == null) {
            result = this.properties.getBaseLabelFor(quadro, campo);
        }
        return result;
    }

    public String getCampoLabel(String quadro, String campo) {
        return this.properties.getLabelFor(quadro, campo);
    }

    public String getCampoBaseLabel(String quadro, String campo) {
        return this.properties.getBaseLabelFor(quadro, campo);
    }

    public String getSimpleLabel(String quadro, String campo) {
        return this.properties.getSimpleLabelFor(quadro, campo);
    }

    public String getCampoFullDescr(String quadro, String campo) {
        String result = this.properties.getBaseLabelFor(quadro, campo) != null ? this.properties.getBaseLabelFor(quadro, campo) : "";
        result = result + (this.properties.getBaseLabelFor(quadro, campo) != null && this.properties.getLabelFor(quadro, campo) != null ? " - " : "");
        result = result + (this.properties.getLabelFor(quadro, campo) != null ? this.properties.getLabelFor(quadro, campo) : "");
        return result;
    }

    public String getCampoNumber(String quadro, String campo) {
        return this.properties.getNumForCampo(quadro, campo);
    }

    public String getQuadroNumber(String quadro) {
        return this.properties.getNumForQuadro(quadro);
    }

    public String getQuadroTitle(String quadro) {
        return this.properties.getTitleForQuadro(quadro);
    }

    public void getResource(String resource) {
        String propertiesFile = resource + ".properties";
        String localPropertiesFile = resource + ".local.properties";
        this.properties = new LabelParameters();
        this.properties.read(this.getClass().getResourceAsStream(propertiesFile));
        try {
            this.properties.read(this.getClass().getResourceAsStream(localPropertiesFile));
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    public String getString(String key) {
        return this.properties.getString(key);
    }
}

