/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.awt.Color;
import java.awt.Component;
import java.io.IOException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.dgci.modelo3irs.v2015.gui.ThemeHelpPane;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.TitledPanel2;
import pt.opensoft.swing.Util;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.taxclient.actions.util.CloseBottomPanelAction;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class ThemeHelpDisplayer
extends TitledPanel2
implements HelpDisplayer {
    private static final long serialVersionUID = -1975872212437250807L;
    private ThemeHelpPane scrollableHtmlPane;
    private String htmlFile;

    public ThemeHelpDisplayer(String htmlFile, String cssFile) {
        super(IconFactory.getIconBig(GUIParameters.ICON_AJUDA_TEMAS), "Ajuda por temas", (EnhancedAction)new CloseBottomPanelAction("Fechar painel de ajuda"), new JSearchTextField(new ThemeHelpPane(htmlFile, "", null)), GUIParameters.TITLED_PANEL_TITLE_COLOR, null);
        this.htmlFile = htmlFile;
        this.scrollableHtmlPane = (ThemeHelpPane)this.searchTextField.getSearchableTextComponent();
        this.scrollableHtmlPane.setEditable(false);
        this.scrollableHtmlPane.setContentType("text/html");
        StyleSheet css = new StyleSheet();
        if (cssFile != null) {
            css.importStyleSheet(Util.class.getResource(cssFile));
        }
        HTMLEditorKit edtKit = new HTMLEditorKit();
        edtKit.setStyleSheet(css);
        this.scrollableHtmlPane.setEditorKit(edtKit);
        this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
        JScrollPane scrollPane = new JScrollPane(this.scrollableHtmlPane);
        scrollPane.getVerticalScrollBar().setUnitIncrement(scrollPane.getVerticalScrollBar().getBlockIncrement());
        this.add(scrollPane);
    }

    @Override
    public void updateHelp() throws IOException {
        if (Session.getMainFrame().getMultiPurposeDisplayer().isDisplayerVisible()) {
            URL url = Util.class.getResource(this.htmlFile);
            SimpleLog.log("showing html: pageLocation = " + url);
            this.scrollableHtmlPane.setPage(url);
        }
    }

    @Override
    public void updateSearchHelp(String text) throws Exception {
        throw new RuntimeException("Not implemented");
    }

    @Override
    protected Color getTextForeground(boolean selected) {
        Color c = UIManager.getColor("TitledPanel3Erros.foreground");
        if (c != null) {
            return c;
        }
        return super.getTextForeground(selected);
    }
}

