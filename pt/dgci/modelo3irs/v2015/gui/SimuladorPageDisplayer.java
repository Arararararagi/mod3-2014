/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.dgci.modelo3irs.v2015.actions.util.AdditionalInfoAction;
import pt.dgci.modelo3irs.v2015.actions.util.PrintBottomPanelAction;
import pt.dgci.modelo3irs.v2015.actions.util.ShowSimulationDataAction;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.GenericTitledPanel;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.Util;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.taxclient.actions.util.CloseBottomPanelAction;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class SimuladorPageDisplayer
extends GenericTitledPanel
implements HelpDisplayer {
    private static final long serialVersionUID = 515907891737199657L;
    private static final String INFO = "/simulador/info.html";
    protected ScrollableHtmlPane scrollableHtmlPane;
    protected String htmlFile;
    private Map<String, String> values;
    private static final String DEFAULT_SIM_PRINT_MSG = "<br>A liquida&ccedil;&atilde;o das Declara&ccedil;&otilde;es Modelo 3 de IRS &eacute; efectuada por um programa espec&iacute;fico; a simula&ccedil;&atilde;o que efectuou n&atilde;o contempla todas as situa&ccedil;&otilde;es declarativas, pelo que os resultados obtidos podem apresentar ligeiras diferen&ccedil;as, relativamente &agrave; liquida&ccedil;&atilde;o a emitir pelo sistema de liquida&ccedil;&atilde;o central, nomeadamente a dedu&ccedil;&atilde;o do benef&iacute;cio fiscal do IVA suportado.";
    private static final PrintBottomPanelAction printAction = new PrintBottomPanelAction("Imprimir Simula\u00e7\u00e3o", "<br>A liquida&ccedil;&atilde;o das Declara&ccedil;&otilde;es Modelo 3 de IRS &eacute; efectuada por um programa espec&iacute;fico; a simula&ccedil;&atilde;o que efectuou n&atilde;o contempla todas as situa&ccedil;&otilde;es declarativas, pelo que os resultados obtidos podem apresentar ligeiras diferen&ccedil;as, relativamente &agrave; liquida&ccedil;&atilde;o a emitir pelo sistema de liquida&ccedil;&atilde;o central, nomeadamente a dedu&ccedil;&atilde;o do benef&iacute;cio fiscal do IVA suportado.");
    public static final AdditionalInfoAction additionalInfoAction = new AdditionalInfoAction("Informa\u00e7\u00e3o adicional");
    public static final ShowSimulationDataAction simulationDataAction = new ShowSimulationDataAction("Informa\u00e7\u00e3o da Simula\u00e7\u00e3o", new EnhancedAction[0]);

    public SimuladorPageDisplayer(String htmlFile, String cssFile) {
        super(IconFactory.getIconBig(GUIParameters.ICON_SIMULAR), "Simulador", Arrays.asList(simulationDataAction, additionalInfoAction, printAction, new CloseBottomPanelAction("Fechar simulador")), null, GUIParameters.TITLED_PANEL_TITLE_COLOR, null, null);
        this.htmlFile = htmlFile;
        this.scrollableHtmlPane = new ScrollableHtmlPane();
        this.scrollableHtmlPane.setEditable(false);
        this.scrollableHtmlPane.setContentType("text/html");
        StyleSheet css = new StyleSheet();
        if (cssFile != null) {
            css.importStyleSheet(Util.class.getResource(cssFile));
        }
        HTMLEditorKit edtKit = new HTMLEditorKit();
        edtKit.setStyleSheet(css);
        this.scrollableHtmlPane.setEditorKit(edtKit);
        this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
        this.changeFont();
        JScrollPane scrollPane = new JScrollPane(this.scrollableHtmlPane);
        scrollPane.getVerticalScrollBar().setUnitIncrement(scrollPane.getVerticalScrollBar().getBlockIncrement());
        this.add(scrollPane);
    }

    @Override
    public void updateHelp() throws Exception {
        if (Session.getMainFrame().getMultiPurposeDisplayer().isDisplayerVisible()) {
            String helpText;
            String pageLocation = this.htmlFile;
            try {
                SimpleLog.log("Showing html: pageLocation = " + pageLocation);
                helpText = Util.getHtmlText(this.scrollableHtmlPane, pageLocation);
                if (this.values == null) {
                    throw new IOException();
                }
                for (Map.Entry<String, String> entry : this.values.entrySet()) {
                    helpText = StringUtil.replace(helpText, "#" + entry.getKey() + "#", entry.getValue());
                }
            }
            catch (IOException e) {
                helpText = "<p><i>P\u00e1gina n\u00e3o dispon\u00edvel</i></p></html>";
            }
            this.scrollableHtmlPane.setText(helpText);
            this.getAccessibleContext().setAccessibleDescription(helpText);
            this.changeFont();
            printAction.setPrintHTML(helpText.replaceAll("<!-- @PRINT_MSG@ -->", "<br>A liquida&ccedil;&atilde;o das Declara&ccedil;&otilde;es Modelo 3 de IRS &eacute; efectuada por um programa espec&iacute;fico; a simula&ccedil;&atilde;o que efectuou n&atilde;o contempla todas as situa&ccedil;&otilde;es declarativas, pelo que os resultados obtidos podem apresentar ligeiras diferen&ccedil;as, relativamente &agrave; liquida&ccedil;&atilde;o a emitir pelo sistema de liquida&ccedil;&atilde;o central, nomeadamente a dedu&ccedil;&atilde;o do benef&iacute;cio fiscal do IVA suportado."));
            this.toggleAction(simulationDataAction, false);
            this.toggleAction(additionalInfoAction, true);
        }
    }

    public void showAdditionalHelp() {
        if (Session.getMainFrame().getMultiPurposeDisplayer().isDisplayerVisible()) {
            String helpText;
            try {
                SimpleLog.log("Showing html: pageLocation = /simulador/info.html");
                helpText = Util.getHtmlText(this.scrollableHtmlPane, "/simulador/info.html");
            }
            catch (IOException e) {
                helpText = "<p><i>P\u00e1gina n\u00e3o dispon\u00edvel</i></p></html>";
            }
            this.scrollableHtmlPane.setText(helpText);
            this.getAccessibleContext().setAccessibleDescription(helpText);
            this.changeFont();
        }
    }

    public void setMap(Map<String, String> values) {
        this.values = values;
    }

    public void toggleAction(EnhancedAction action, boolean visible) {
        for (Component component : this.actionBar.getComponents()) {
            JButton button = (JButton)component;
            if (!button.getAction().equals(action)) continue;
            button.setVisible(visible);
        }
    }

    @Override
    public void updateSearchHelp(String text) throws Exception {
        throw new RuntimeException("Not implemented");
    }

    protected void changeFont() {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)this.scrollableHtmlPane.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }
}

