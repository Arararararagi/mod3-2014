/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.simulador.util;

import ca.odell.glazedlists.EventList;
import calculo.business.Modelo3;
import calculo.common.util.Mod3IO;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Parameters;
import pt.dgci.modelo3irs.v2015.model.Modelo3IRSv2015Model;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAModel;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4A_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq04T4B_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.AnexoAq05T5_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro04;
import pt.dgci.modelo3irs.v2015.model.anexoa.Quadro05;
import pt.dgci.modelo3irs.v2015.model.anexob.AnexoBModel;
import pt.dgci.modelo3irs.v2015.model.anexob.Quadro01;
import pt.dgci.modelo3irs.v2015.model.anexoc.AnexoCModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDModel;
import pt.dgci.modelo3irs.v2015.model.anexod.AnexoDq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEModel;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoe.AnexoEq04T2_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFModel;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq05T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq06T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexof.AnexoFq07T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGModel;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04AT1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq04T1_LinhaBase;
import pt.dgci.modelo3irs.v2015.model.anexog.AnexoGq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro06;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro07;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro08;
import pt.dgci.modelo3irs.v2015.model.anexog.Quadro09;
import pt.dgci.modelo3irs.v2015.model.anexog1.AnexoG1Model;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHModel;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq04T4_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq07T7_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T1_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.AnexoHq08T814_Linha;
import pt.dgci.modelo3irs.v2015.model.anexoh.Quadro10;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro02;
import pt.dgci.modelo3irs.v2015.model.rosto.Quadro03;
import pt.dgci.modelo3irs.v2015.model.rosto.RostoModel;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq03DT1_Linha;
import pt.dgci.modelo3irs.v2015.model.rosto.Rostoq07CT1_Linha;
import pt.dgci.modelo3irs.v2015.simulador.util.SimulatorHashtable;
import pt.dgci.modelo3irs.v2015.validator.util.Modelo3IRSValidatorUtil;
import pt.opensoft.field.EuroField;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.Date;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class SimulateUtil {
    private Modelo3IRSv2015Model model;
    private static final Double DOUBLE_ZERO = 0.0;
    private Hashtable<String, Object> simulatorHashtable;
    private long somaRendas = 0;
    private long somaDespesas = 0;
    private long somaRetencoes = 0;

    public SimulateUtil(Modelo3IRSv2015Model model) {
        this.model = model;
    }

    public boolean isReembolso(int idadeSPA, int idadeSPB, int idadeSPF, int idadeD1, int idadeD2, int idadeD3, int nDep, int nDepGC) {
        if (this.model == null) {
            return false;
        }
        Hashtable<String, Object> values = this.fillHashtable(idadeSPA, idadeSPB, idadeSPF, idadeD1, idadeD2, idadeD3, nDep, nDepGC);
        Mod3IO mod3io = new Mod3IO(values);
        Modelo3 simCalc = mod3io.hashToMod3();
        simCalc.efectuaSimulacao();
        long valImpostoApurado = this.convertToCents(simCalc.getValImpostoApurado());
        return valImpostoApurado < -998;
    }

    public Hashtable<String, Object> getSimulatorHashtable() {
        return this.simulatorHashtable;
    }

    public Map<String, String> efectuaSimulacao(int idadeSPA, int idadeSPB, int idadeSPF, int idadeD1, int idadeD2, int idadeD3, int nDep, int nDepGC) {
        if (this.model == null) {
            return null;
        }
        this.simulatorHashtable = this.fillHashtable(idadeSPA, idadeSPB, idadeSPF, idadeD1, idadeD2, idadeD3, nDep, nDepGC);
        Mod3IO mod3io = new Mod3IO(this.simulatorHashtable);
        Modelo3 simCalc = mod3io.hashToMod3();
        simCalc.efectuaSimulacao();
        if (Modelo3IRSv2015Parameters.instance().getTestSimHt() != null) {
            try {
                FileInputStream fileinputstream = new FileInputStream(Modelo3IRSv2015Parameters.instance().getTestSimHt());
                ObjectInputStream objectinputstream = new ObjectInputStream(fileinputstream);
                Hashtable fileValues = (Hashtable)objectinputstream.readObject();
                fileinputstream.close();
                new HtReader().printDifferences(this.simulatorHashtable, fileValues);
            }
            catch (IOException e) {
                SimpleLog.log("ERRO A LER DO FICHEIRO.", e);
            }
            catch (ClassNotFoundException e) {
                SimpleLog.log("ERRO A INSTACIAR A HASHTABLE.", e);
            }
        }
        HashMap<String, String> valuesSimul = new HashMap<String, String>();
        valuesSimul.put("RendGlobal", this.formatAmmount(simCalc.getValRendimGlobal()));
        valuesSimul.put("ImpRendIsentos", this.formatAmmount(simCalc.getValorColRendIsent()));
        valuesSimul.put("DeducoesEspecif", this.formatAmmount(simCalc.getValDedEspecificas()));
        valuesSimul.put("TaxaAdicional", this.formatAmmount(simCalc.getValTaxaAdicional()));
        valuesSimul.put("PerdasRecuperar", this.formatAmmount(simCalc.getValPerdasRecup()));
        valuesSimul.put("ImpTributAutonomas", this.formatAmmount(simCalc.getValImpTribAutonoma()));
        valuesSimul.put("Abatimentos", this.formatAmmount(simCalc.getValAbatimentos()));
        valuesSimul.put("ColectaTotal", this.formatAmmount(simCalc.getValColectaTotal()));
        valuesSimul.put("RendDeterminacaoTaxas", this.formatAmmount(simCalc.getValRendimDetTaxas()));
        valuesSimul.put("DeducoesColecta", this.formatAmmount(simCalc.getValDeduColecta()));
        valuesSimul.put("CoeficienteConjugal", this.formatAmmount(simCalc.getValCoefConjugal()));
        valuesSimul.put("AcrescimosColecta", this.formatAmmount(simCalc.getValAcrescColecta()));
        valuesSimul.put("Taxa", this.formatAmmount(simCalc.getValTaxaImposto()));
        valuesSimul.put("Importancia", this.formatAmmount(simCalc.getValImportApurada()));
        valuesSimul.put("ColectaLiquida", this.formatAmmount(simCalc.getValColectaLiquida()));
        valuesSimul.put("PagamentosPorConta", this.formatAmmount(simCalc.getValPagamentoConta()));
        valuesSimul.put("ParcelaAbater", this.formatAmmount(simCalc.getValParcelaAbater()));
        valuesSimul.put("RetencoesFonte", this.formatAmmount(simCalc.getValRetencoesFonte()));
        valuesSimul.put("RendColectavel", this.formatAmmount(simCalc.getValRendimColectavel()));
        valuesSimul.put("BeneficioMunicipal", this.formatAmmount(simCalc.getValBenFiscalSF()));
        valuesSimul.put("QuocienteRendAnt", this.formatAmmount(simCalc.getValQuoRendAnter()));
        valuesSimul.put("RendIsentos", this.formatAmmount(simCalc.getValRendimIsentos()));
        valuesSimul.put("ImpRendAnosAnt", this.formatAmmount(simCalc.getValImpostAnosAnt()));
        valuesSimul.put("ValStRendimento", this.formatAmmount(simCalc.getValStRendimento()));
        valuesSimul.put("ValStRendimentoXTaxa", this.formatAmmount(simCalc.getValStRendimentoXTaxa()));
        valuesSimul.put("ValStDeducoes", this.formatAmmount(simCalc.getValStDeducoes()));
        valuesSimul.put("ValStRetencoes", this.formatAmmount(simCalc.getValStRetencoes()));
        valuesSimul.put("ValStColecta", this.formatAmmount(simCalc.getValStColecta()));
        long valImpostoApurado = this.convertToCents(simCalc.getValImpostoApurado());
        if (valImpostoApurado >= -998 && valImpostoApurado <= 2493) {
            valImpostoApurado = 0;
            valuesSimul.put("Texto", "C\u00e1lculo sem imposto");
        } else if (valImpostoApurado < -998) {
            valImpostoApurado = Math.abs(valImpostoApurado);
            valuesSimul.put("Texto", "Valor a receber");
        } else {
            valuesSimul.put("Texto", "Valor a pagar");
        }
        String calculoImposto = this.formatAmmount(valImpostoApurado);
        calculoImposto = calculoImposto.equals("0") ? "--" : new EuroField("field", calculoImposto).format();
        valuesSimul.put("CalculoImposto", calculoImposto);
        return valuesSimul;
    }

    public String isSimulationPossible() {
        Set<String> dependentesComInvalidez;
        int maxDependentesComRendimentosEOuInvalidez;
        AnexoHModel anexoH;
        if (!Modelo3IRSv2015Parameters.instance().IsSimulatePossible()) {
            return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel";
        }
        RostoModel rosto = this.model.getRosto();
        if (rosto.getQuadro02().getQ02C02() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro02().getQ02C02()))) {
            return "Por favor indique o Ano dos Rendimentos.";
        }
        if (rosto.getQuadro02().getQ02C02() == 2001 || rosto.getQuadro02().getQ02C02() == 2002) {
            return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para o ano " + rosto.getQuadro02().getQ02C02() + ".";
        }
        Set<String> dependentesComRendimentos = SimulateUtil.getDependentesComRendimentos(this.model);
        int countDependentes = this.countDependenteRendimentosInvalidez(dependentesComRendimentos, dependentesComInvalidez = SimulateUtil.getDependentesComInvalidez(this.model));
        if (countDependentes > (maxDependentesComRendimentosEOuInvalidez = 3)) {
            return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para declara\u00e7\u00f5es com mais do que 3 dependentes com rendimentos e/ou grau de invalidez.";
        }
        if (rosto.getQuadro05().isQ05B3OP8Selected() || rosto.getQuadro05().isQ05B3OP9Selected()) {
            return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para o tratamento dos rendimentos dos contribuintes n\u00e3o residentes<br> quando estes optem pelas taxas do artigo 68\u00ba do CIRS (n\u00ba 8 do artigo 72\u00ba do CIRS) ou pelas regras do artigo 17\u00ba A do CIRS.";
        }
        AnexoAModel anexoA = this.model.getAnexoA();
        if (anexoA != null) {
            for (AnexoAq04T4B_Linha linhaQ4B : anexoA.getQuadro04().getAnexoAq04T4B()) {
                if (linhaQ4B.getCodDespesa() != 412) continue;
                return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para despesas de valoriza\u00e7\u00e3o profissional de Ju\u00edzes.";
            }
        }
        if (Modelo3IRSv2015Parameters.instance().isFase2()) {
            List<AnexoCModel> anexosC;
            List<AnexoModel> anexosD;
            AnexoGModel anexoG;
            List<AnexoBModel> anexosB = this.model.getAnexoB();
            if (anexosB != null) {
                for (AnexoBModel anexoB : anexosB) {
                    if (anexoB.getQuadro06().getAnexoBq06C1() != null && anexoB.getQuadro06().getAnexoBq06C1() > 0) {
                        return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para acr\u00e9scimos ao rendimento da categoria B.";
                    }
                    if (anexoB.getQuadro08().getAnexoBq08C801() == null) continue;
                    return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para acr\u00e9scimos ao rendimento da categoria B.";
                }
            }
            if ((anexosC = this.model.getAnexoC()) != null) {
                for (AnexoCModel anexoC : anexosC) {
                    if (anexoC.getQuadro01().getAnexoCq01B2() != null && anexoC.getQuadro01().getAnexoCq01B2().booleanValue()) {
                        return " A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para rendimentos agr\u00edcolas do Anexo C.";
                    }
                    if (anexoC.getQuadro09().getAnexoCq09C901() == null) continue;
                    return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para preju\u00edzos fiscais a deduzir em caso de sucess\u00e3o por morte.";
                }
            }
            if ((anexosD = this.model.getAnexosD()) != null) {
                for (AnexoDModel anexoD : anexosD) {
                    if (anexoD.getQuadro07().getAnexoDq07C701() == null) continue;
                    return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para preju\u00edzos fiscais a deduzir em caso de sucess\u00e3o por morte.";
                }
            }
            if ((anexoG = this.model.getAnexoG()) != null && anexoG.getQuadro04().getAnexoGq04T1().size() > 4) {
                return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para Anexos G com mais do que 4 linhas no Quadro 4.";
            }
            List<AnexoModel> anexosI = this.model.getAnexosI();
            if (!(Modelo3IRSv2015Parameters.instance().IsSimulatePossibleForFase2() || (anexosB == null || anexosB.size() <= 0) && (anexosC == null || anexosC.size() <= 0) && (anexosD == null || anexosD.size() <= 0) && this.model.getAnexoE() == null && this.model.getAnexoF() == null && anexoG == null && this.model.getAnexoG1() == null && (anexosI == null || anexosI.size() <= 0))) {
                return "A simula\u00e7\u00e3o ainda n\u00e3o est\u00e1 dispon\u00edvel para declara\u00e7\u00f5es da 2\u00aa fase de entrega.";
            }
            AnexoEModel anexoEModel = this.model.getAnexoE();
            if (anexoEModel != null && anexoEModel.getQuadro04().getAnexoEq04B1() != null && anexoEModel.getQuadro04().getAnexoEq04B1().equals("2")) {
                return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para rendimentos da categoria E, quando \u00e9 efectuada a op\u00e7\u00e3o pelo n\u00e3o englobamento.";
            }
        }
        if ((anexoH = this.model.getAnexoH()) != null) {
            if (anexoH.getQuadro05().getAnexoHq05C1() != null && anexoH.getQuadro05().getAnexoHq05C1() > 0) {
                return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para rendimentos da propriedade intelectual isentos parcialmente.";
            }
            if (anexoH.getQuadro10().getAnexoHq10C1009() != null && anexoH.getQuadro10().getAnexoHq10C1009() > 0) {
                return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para acr\u00e9scimos pelo imcumprimento do disposto na alinea b) do art. 4\u00ba do Decreto Legislativo Regional n\u00ba 5/2000/M, de 28 de Fevereiro.";
            }
        }
        if (!this.model.getAnexosJ().isEmpty()) {
            return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para rendimentos auferidos no estrangeiro.";
        }
        if (!this.model.getAnexosL().isEmpty()) {
            return "A simula\u00e7\u00e3o n\u00e3o est\u00e1 dispon\u00edvel para residentes n\u00e3o habituais.";
        }
        return null;
    }

    private int countDependenteRendimentosInvalidez(Set<String> dependentesComRendimentos, Set<String> dependentesComInvalidez) {
        HashSet<String> collection = new HashSet<String>();
        if (dependentesComRendimentos != null) {
            collection.addAll(dependentesComRendimentos);
        }
        if (dependentesComInvalidez != null) {
            collection.addAll(dependentesComInvalidez);
        }
        return collection.size();
    }

    private RostoModel doRosto(int idadeD1, int idadeD2, int idadeD3, SimulatorHashtable<String, Object> values, List<String> nifs) {
        RostoModel rosto = this.model.getRosto();
        if (!(rosto.getQuadro02().getQ02C02() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro02().getQ02C02())))) {
            Integer anoSimulacao = rosto.getQuadro02().getQ02C02() == 2014 ? Integer.valueOf(14) : (rosto.getQuadro02().getQ02C02() == 2013 ? Integer.valueOf(13) : (rosto.getQuadro02().getQ02C02() == 2012 ? Integer.valueOf(12) : (rosto.getQuadro02().getQ02C02() == 2011 ? Integer.valueOf(11) : (rosto.getQuadro02().getQ02C02() == 2010 ? Integer.valueOf(10) : (rosto.getQuadro02().getQ02C02() == 2009 ? Integer.valueOf(9) : (rosto.getQuadro02().getQ02C02() == 2008 ? Integer.valueOf(8) : (rosto.getQuadro02().getQ02C02() == 2007 ? Integer.valueOf(1) : (rosto.getQuadro02().getQ02C02() == 2006 ? Integer.valueOf(2) : (rosto.getQuadro02().getQ02C02() == 2005 ? Integer.valueOf(3) : (rosto.getQuadro02().getQ02C02() == 2004 ? Integer.valueOf(4) : (rosto.getQuadro02().getQ02C02() == 2003 ? Integer.valueOf(5) : (rosto.getQuadro02().getQ02C02() == 2002 ? Integer.valueOf(6) : (rosto.getQuadro02().getQ02C02() == 2001 ? Integer.valueOf(7) : Integer.valueOf(12))))))))))))));
            values.put("AnoSim", anoSimulacao);
        }
        values.put("Residencia", Character.valueOf(!StringUtil.isEmpty(rosto.getQuadro05().getQ05B1()) ? rosto.getQuadro05().getQ05B1().charAt(0) : ' '));
        values.put("ServFin", !StringUtil.isEmpty(String.valueOf(rosto.getQuadro01().getQ01C01())) ? StringUtil.padZeros(String.valueOf(rosto.getQuadro01().getQ01C01()), 4) : "");
        values.put("EstadoCivil", Character.valueOf(rosto.getQuadro06().getQ06B1() != null ? rosto.getQuadro06().getQ06B1().charAt(0) : ' '));
        values.put("NDepNDefic", new Integer(rosto.getQuadro03().getCountDependentesNaoDeficientes()));
        values.put("NDepDeficientes", new Integer(rosto.getQuadro03().getCountDependentesDeficientes()));
        int nDepNDef = 0;
        int nDepDef = 0;
        int depGConjDeficiencientesComRendimentos = 0;
        Set<String> dependentesGuardaConjunta = SimulateUtil.getTitularesComRendimentos(this.model, "DG");
        if (rosto.getQuadro03().getRostoq03DT1() != null) {
            EventList<Rostoq03DT1_Linha> dependentesGConjunta = rosto.getQuadro03().getRostoq03DT1();
            String titular = null;
            for (int i = 0; i < dependentesGConjunta.size(); ++i) {
                boolean isDeficiente;
                Rostoq03DT1_Linha rostoq03DT1_Linha = dependentesGConjunta.get(i);
                if (rostoq03DT1_Linha == null) continue;
                boolean bl = isDeficiente = !Modelo3IRSValidatorUtil.isEmptyOrZero(rostoq03DT1_Linha.getDeficienteGrau());
                if (isDeficiente) {
                    ++nDepDef;
                } else {
                    ++nDepNDef;
                }
                titular = "DG" + (i + 1);
                if (!dependentesGuardaConjunta.contains(titular) && !isDeficiente) continue;
                ++depGConjDeficiencientesComRendimentos;
            }
        }
        values.put("NumDepGCNDef", nDepNDef);
        values.put("NumDepGCDef", nDepDef);
        values.put("DefFASPA", rosto.getQuadro03().getQ03B03() != null ? rosto.getQuadro03().getQ03B03() : false);
        values.put("GuardaConjuntaD1", depGConjDeficiencientesComRendimentos >= 1);
        values.put("GuardaConjuntaD2", depGConjDeficiencientesComRendimentos >= 2);
        values.put("GuardaConjuntaD3", depGConjDeficiencientesComRendimentos >= 3);
        values.put("IdadeD1", idadeD1);
        values.put("IdadeD2", idadeD2);
        values.put("IdadeD3", idadeD3);
        values.put("Nafilhados", rosto.getQuadro07().getRostoq07CT1() != null ? rosto.getQuadro07().getRostoq07CT1().size() : 0);
        int numAscendentesNDef = 0;
        int numAscendentesDef = 0;
        if (!(rosto.getQuadro07().getQ07C01() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C01())) || rosto.getQuadro07().getQ07C01a() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C01a())) || rosto.getQuadro07().getQ07C01() < 60)) {
            ++numAscendentesDef;
        } else if (!(rosto.getQuadro07().getQ07C01() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C01())))) {
            ++numAscendentesNDef;
        }
        if (!(rosto.getQuadro07().getQ07C02() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C02())) || rosto.getQuadro07().getQ07C02a() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C02a())) || rosto.getQuadro07().getQ07C02() < 60)) {
            ++numAscendentesDef;
        } else if (!(rosto.getQuadro07().getQ07C02() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C02())))) {
            ++numAscendentesNDef;
        }
        if (!(rosto.getQuadro07().getQ07C03() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C03())) || rosto.getQuadro07().getQ07C03a() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C03a())) || rosto.getQuadro07().getQ07C03() < 60)) {
            ++numAscendentesDef;
        } else if (!(rosto.getQuadro07().getQ07C03() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C03())))) {
            ++numAscendentesNDef;
        }
        if (!(rosto.getQuadro07().getQ07C04() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C04())) || rosto.getQuadro07().getQ07C04a() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C04a())) || rosto.getQuadro07().getQ07C04() < 60)) {
            ++numAscendentesDef;
        } else if (!(rosto.getQuadro07().getQ07C04() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C04())))) {
            ++numAscendentesNDef;
        }
        values.put("NAscNDef", numAscendentesNDef);
        values.put("NAscDef", numAscendentesDef);
        values.put("Nascendentes", numAscendentesNDef + numAscendentesDef);
        Long obito = rosto.getQuadro07().getQ07C1();
        if (obito == null) {
            values.put("Obito", Character.valueOf('0'));
        } else {
            values.put("Obito", Character.valueOf('1'));
        }
        values.put("EnglobE", Character.valueOf('1'));
        values.put("JuntaDocs", Character.valueOf('1'));
        values.put("idContSPA", "SPA");
        values.put("idContSPB", "SPB");
        values.put("idContSPF", "SPF");
        values.put("idContD1", "D1");
        values.put("idContD2", "D2");
        values.put("idContD3", "D3");
        if (!(rosto.getQuadro03().getQ03CDD1() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD1())))) {
            nifs.add(String.valueOf(rosto.getQuadro03().getQ03CDD1()));
        }
        if (!(rosto.getQuadro03().getQ03CDD2() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD2())))) {
            nifs.add(String.valueOf(rosto.getQuadro03().getQ03CDD2()));
        }
        if (!(rosto.getQuadro03().getQ03CDD3() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD3())))) {
            nifs.add(String.valueOf(rosto.getQuadro03().getQ03CDD3()));
        }
        if (!(rosto.getQuadro03().getRostoq03DT1() == null || rosto.getQuadro03().getRostoq03DT1().isEmpty())) {
            for (Rostoq03DT1_Linha linha : rosto.getQuadro03().getRostoq03DT1()) {
                if (linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau())) continue;
                nifs.add(String.valueOf(linha.getNIF()));
            }
        }
        if (!(rosto.getQuadro03().getQ03CD1() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CD1())))) {
            nifs.add(String.valueOf(rosto.getQuadro03().getQ03CD1()));
        }
        if (!(rosto.getQuadro03().getQ03CD2() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CD2())))) {
            nifs.add(String.valueOf(rosto.getQuadro03().getQ03CD2()));
        }
        if (!(rosto.getQuadro03().getQ03CD3() == null || StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CD3())))) {
            nifs.add(String.valueOf(rosto.getQuadro03().getQ03CD3()));
        }
        return rosto;
    }

    private Hashtable<String, Object> fillHashtable(int idadeSpA, int idadeSpB, int idadeSPF, int idadeD1, int idadeD2, int idadeD3, int nDep, int nDepGC) {
        int contpre_SPA;
        void SPF_401To403;
        void SPB_404To407;
        void D1_401To403Anos;
        void D1_401To403;
        int pripag_SPB;
        void D2_404To407Anos;
        void D2_404To407;
        void SPA_401To403;
        void SPF_401To403Anos;
        void SPA_404To407Anos;
        void D3_404To407;
        void SPB_401To403;
        AnexoHModel i$;
        int contpre_SPB;
        void D1_404To407Anos;
        int pripag_SPA;
        void SPF_404To407;
        void SPA_404To407;
        String titular;
        void D1_Soma413;
        void SPA_401To403Anos;
        void SPB_404To407Anos;
        void D3_401To403;
        void D3_404To407Anos;
        void D2_401To403Anos;
        void D2_401To403;
        void D3_Soma413;
        void D3_401To403Anos;
        void SPF_404To407Anos;
        void D1_404To407;
        void anexoH;
        void D2_Soma413;
        void SPB_401To403Anos;
        List<AnexoBModel> anexosB;
        List<AnexoDModel> anexosD;
        List<AnexoCModel> anexosC;
        SimulatorHashtable<String, Object> values = new SimulatorHashtable<String, Object>();
        ArrayList<String> nifs = new ArrayList<String>();
        RostoModel rosto = this.doRosto(idadeD1, idadeD2, idadeD3, values, nifs);
        long nifD1 = 0;
        long nifD2 = 0;
        long nifD3 = 0;
        AnexoAModel anexoA = this.model.getAnexoA();
        if (anexoA != null) {
            for (AnexoAq04T4A_Linha linha4A : anexoA.getQuadro04().getAnexoAq04T4A()) {
                if (!linha4A.getTitular().startsWith("D")) continue;
                if (nifD1 == 0) {
                    nifD1 = rosto.getNIFTitular(linha4A.getTitular());
                    nifs.remove(String.valueOf(nifD1));
                    continue;
                }
                if (nifD2 == 0 && nifD1 != rosto.getNIFTitular(linha4A.getTitular())) {
                    nifD2 = rosto.getNIFTitular(linha4A.getTitular());
                    nifs.remove(String.valueOf(nifD2));
                    continue;
                }
                if (nifD3 != 0 || nifD1 == rosto.getNIFTitular(linha4A.getTitular()) || nifD2 == rosto.getNIFTitular(linha4A.getTitular())) continue;
                nifD3 = rosto.getNIFTitular(linha4A.getTitular());
                nifs.remove(String.valueOf(nifD3));
                break;
            }
        }
        if (!ListUtil.isEmpty(anexosB = this.model.getAnexoB())) {
            for (AnexoBModel anexoB : anexosB) {
                String titular2 = rosto.getTitularByNIF(Long.parseLong(anexoB.getFormKey().getSubId()));
                if (titular2 == null || !titular2.startsWith("D")) continue;
                if (nifD1 == 0) {
                    nifD1 = rosto.getNIFTitular(titular2);
                    nifs.remove(String.valueOf(nifD1));
                    continue;
                }
                if (nifD2 == 0 && nifD1 != rosto.getNIFTitular(titular2)) {
                    nifD2 = rosto.getNIFTitular(titular2);
                    nifs.remove(String.valueOf(nifD2));
                    continue;
                }
                if (nifD3 != 0 || nifD1 == rosto.getNIFTitular(titular2) || nifD2 == rosto.getNIFTitular(titular2)) continue;
                nifD3 = rosto.getNIFTitular(titular2);
                nifs.remove(String.valueOf(nifD3));
                break;
            }
        }
        if (!ListUtil.isEmpty(anexosC = this.model.getAnexoC())) {
            for (AnexoCModel anexoC : anexosC) {
                String titular3 = rosto.getTitularByNIF(Long.parseLong(anexoC.getFormKey().getSubId()));
                if (titular3 == null || !titular3.startsWith("D")) continue;
                if (nifD1 == 0) {
                    nifD1 = rosto.getNIFTitular(titular3);
                    nifs.remove(String.valueOf(nifD1));
                    continue;
                }
                if (nifD2 == 0 && nifD1 != rosto.getNIFTitular(titular3)) {
                    nifD2 = rosto.getNIFTitular(titular3);
                    nifs.remove(String.valueOf(nifD2));
                    continue;
                }
                if (nifD3 != 0 || nifD1 == rosto.getNIFTitular(titular3) || nifD2 == rosto.getNIFTitular(titular3)) continue;
                nifD3 = rosto.getNIFTitular(titular3);
                nifs.remove(String.valueOf(nifD3));
                break;
            }
        }
        if (!ListUtil.isEmpty(anexosD = this.model.getAnexoD())) {
            for (AnexoDModel anexoD : anexosD) {
                String titular4 = rosto.getTitularByNIF(Long.parseLong(anexoD.getFormKey().getSubId()));
                if (titular4 == null || !titular4.startsWith("D")) continue;
                if (nifD1 == 0) {
                    nifD1 = rosto.getNIFTitular(titular4);
                    nifs.remove(String.valueOf(nifD1));
                    continue;
                }
                if (nifD2 == 0 && nifD1 != rosto.getNIFTitular(titular4)) {
                    nifD2 = rosto.getNIFTitular(titular4);
                    nifs.remove(String.valueOf(nifD2));
                    continue;
                }
                if (nifD3 != 0 || nifD1 == rosto.getNIFTitular(titular4) || nifD2 == rosto.getNIFTitular(titular4)) continue;
                nifD3 = rosto.getNIFTitular(titular4);
                nifs.remove(String.valueOf(nifD3));
                break;
            }
        }
        for (String nifStr : nifs) {
            if (nifD1 == 0) {
                nifD1 = Long.parseLong(nifStr);
                continue;
            }
            if (nifD2 == 0) {
                nifD2 = Long.parseLong(nifStr);
                continue;
            }
            if (nifD3 != 0) continue;
            nifD3 = Long.parseLong(nifStr);
        }
        short grauInvD1 = SimulateUtil.getGrauInvalidezFromNif(rosto, nifD1);
        short grauInvD2 = SimulateUtil.getGrauInvalidezFromNif(rosto, nifD2);
        short grauInvD3 = SimulateUtil.getGrauInvalidezFromNif(rosto, nifD3);
        Character grauDefiSPA = SimulateUtil.fromPercentToLetter('0', rosto.getQuadro03().getQ03C03a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03C03a())) ? rosto.getQuadro03().getQ03C03a().shortValue() : 0);
        Character grauDefiSPB = SimulateUtil.fromPercentToLetter('-', rosto.getQuadro03().getQ03C04a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03C04a())) ? rosto.getQuadro03().getQ03C04a().shortValue() : 0);
        Character grauDefiSPF = SimulateUtil.fromPercentToLetter('-', rosto.getQuadro07().getQ07C1a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro07().getQ07C1a())) ? rosto.getQuadro07().getQ07C1a().shortValue() : 0);
        Character grauDefiD1 = SimulateUtil.fromPercentToLetter('-', grauInvD1 != -1 ? grauInvD1 : 0);
        Character grauDefiD2 = SimulateUtil.fromPercentToLetter('-', grauInvD2 != -1 ? grauInvD2 : 0);
        Character grauDefiD3 = SimulateUtil.fromPercentToLetter('-', grauInvD3 != -1 ? grauInvD3 : 0);
        values.put("GrauDefiSPA", grauDefiSPA);
        values.put("GrauDefiSPB", grauDefiSPB);
        values.put("GrauDefiSPF", grauDefiSPF);
        values.put("GrauDefiD1", grauDefiD1);
        values.put("GrauDefiD2", grauDefiD2);
        values.put("GrauDefiD3", grauDefiD3);
        values.put("GrauDefiSPAAux", grauDefiSPA.toString());
        values.put("GrauDefiSPBAux", grauDefiSPB.toString());
        values.put("GrauDefiSPFAux", grauDefiSPF.toString());
        values.put("GrauDefiD1Aux", grauDefiD1.toString());
        values.put("GrauDefiD2Aux", grauDefiD2.toString());
        values.put("GrauDefiD3Aux", grauDefiD3.toString());
        values.put("DefFASPA", rosto.getQuadro03().getQ03B03() != null ? rosto.getQuadro03().getQ03B03() : false);
        values.put("DefFASPB", rosto.getQuadro03().getQ03B04() != null ? rosto.getQuadro03().getQ03B04() : false);
        values.put("DefFASPF", rosto.getQuadro07().getQ07C1b() != null ? rosto.getQuadro07().getQ07C1b() : false);
        values.put("NDep03", new Integer(nDep));
        values.put("NumDepGC03", nDepGC);
        values.put("AcrPatNaoJust1Rend", 0.0);
        values.put("AcrPatNaoJust1Ret", 0.0);
        values.put("AcrPatNaoJust3Rend", 0.0);
        values.put("AcrPatNaoJust3Ret", 0.0);
        AnexoGModel anexoG = this.model.getAnexoG();
        AnexoFModel anexoF = this.model.getAnexoF();
        AnexoEModel anexoE = this.model.getAnexoE();
        this.doAnexoB(values, rosto, nifD1, nifD2, nifD3);
        this.doAnexoC(values, rosto, nifD1, nifD2, nifD3);
        this.doAnexoD(values, rosto, nifD1, nifD2, nifD3);
        this.doAnexoE(values, anexoE);
        Map<Long, Object> anexoFQ05 = this.doAnexoF(values, anexoF);
        this.doAnexoG(values, anexoG, anexoF, anexoFQ05);
        long rendBTotal_SPA = 0;
        long rendBTotal_SPB = 0;
        long rendBTotal_SPF = 0;
        long rendBTotal_D1 = 0;
        long rendBTotal_D2 = 0;
        long rendBTotal_D3 = 0;
        long rendB401_SPA = 0;
        long rendB401_SPB = 0;
        long rendB401_SPF = 0;
        long rendB401_D1 = 0;
        long rendB401_D2 = 0;
        long rendB401_D3 = 0;
        long rendB404_SPA = 0;
        long rendB404_SPB = 0;
        long rendB404_SPF = 0;
        long rendB404_D1 = 0;
        long rendB404_D2 = 0;
        long rendB404_D3 = 0;
        long rendB406_SPA = 0;
        long rendB406_SPB = 0;
        long rendB406_SPF = 0;
        long rendB406_D1 = 0;
        long rendB406_D2 = 0;
        long rendB406_D3 = 0;
        long rendB407_SPA = 0;
        long rendB407_SPB = 0;
        long rendB407_SPF = 0;
        long rendB407_D1 = 0;
        long rendB407_D2 = 0;
        long rendB407_D3 = 0;
        long Grat402_SPA = 0;
        long Grat402_SPB = 0;
        long Grat402_SPF = 0;
        long Grat402_D1 = 0;
        long Grat402_D2 = 0;
        long Grat402_D3 = 0;
        long ret401402_SPA = 0;
        long ret401402_SPB = 0;
        long ret401402_SPF = 0;
        long ret401402_D1 = 0;
        long ret401402_D2 = 0;
        long ret401402_D3 = 0;
        long retSobTaxa401402_SPA = 0;
        long retSobTaxa401402_SPB = 0;
        long retSobTaxa401402_SPF = 0;
        long retSobTaxa401402_D1 = 0;
        long retSobTaxa401402_D2 = 0;
        long retSobTaxa401402_D3 = 0;
        long contr401_SPA = 0;
        long contr401_SPB = 0;
        long contr401_SPF = 0;
        long contr401_D1 = 0;
        long contr401_D2 = 0;
        long contr401408_D3 = 0;
        long ret404405407_SPA = 0;
        long ret404405407_SPB = 0;
        long ret404405407_SPF = 0;
        long ret404405407_D1 = 0;
        long ret404405407_D2 = 0;
        long ret404405407_D3 = 0;
        long retSobTaxa404405407_SPA = 0;
        long retSobTaxa404405407_SPB = 0;
        long retSobTaxa404405407_SPF = 0;
        long retSobTaxa404405407_D1 = 0;
        long retSobTaxa404405407_D2 = 0;
        long retSobTaxa404405407_D3 = 0;
        long contr404405407_SPA = 0;
        long contr404405407_SPB = 0;
        long contr404405407_SPF = 0;
        long contr404405407_D1 = 0;
        long contr404405407_D2 = 0;
        long contr404405407_D3 = 0;
        long rendB408_SPA = 0;
        long rendB408_SPB = 0;
        long ret408_SPA = 0;
        long ret408_SPB = 0;
        long contr408_SPA = 0;
        long contr408_SPB = 0;
        long rendB403_SPA = 0;
        long rendB403_SPB = 0;
        long rendB403_SPF = 0;
        long rendB403_D1 = 0;
        long rendB403_D2 = 0;
        long rendB403_D3 = 0;
        long rendSobrev405_SPA = 0;
        long rendSobrev405_SPB = 0;
        long rendSobrev405_SPF = 0;
        long rendSobrev405_D1 = 0;
        long rendSobrev405_D2 = 0;
        long rendSobrev405_D3 = 0;
        if (anexoA != null) {
            for (AnexoAq04T4A_Linha linha4A : anexoA.getQuadro04().getAnexoAq04T4A()) {
                long codRendimentos;
                long rendimentos = linha4A.getRendimentos() != null ? linha4A.getRendimentos() : 0;
                long retencoes = linha4A.getRetencoes() != null ? linha4A.getRetencoes() : 0;
                long retSobretaxa = linha4A.getRetSobretaxa() != null ? linha4A.getRetSobretaxa() : 0;
                long contribuicoes = linha4A.getContribuicoes() != null ? linha4A.getContribuicoes() : 0;
                String titular5 = linha4A.getTitular() != null ? linha4A.getTitular() : "";
                long l = codRendimentos = linha4A.getCodRendimentos() != null ? linha4A.getCodRendimentos() : 0;
                if (titular5.startsWith("A")) {
                    if (codRendimentos == 401 || codRendimentos == 402) {
                        if (codRendimentos == 401) {
                            rendB401_SPA+=rendimentos;
                            contr401_SPA+=contribuicoes;
                        } else if (codRendimentos == 402) {
                            Grat402_SPA+=rendimentos;
                        }
                        ret401402_SPA+=retencoes;
                        retSobTaxa401402_SPA+=retSobretaxa;
                    } else if (codRendimentos == 404) {
                        rendB404_SPA+=rendimentos;
                        ret404405407_SPA+=retencoes;
                        retSobTaxa404405407_SPA+=retSobretaxa;
                        contr404405407_SPA+=contribuicoes;
                    } else if (codRendimentos == 405) {
                        rendSobrev405_SPA+=rendimentos;
                        ret404405407_SPA+=retencoes;
                        retSobTaxa404405407_SPA+=retSobretaxa;
                        contr404405407_SPA+=contribuicoes;
                    } else if (codRendimentos == 406) {
                        rendB406_SPA+=rendimentos;
                    } else if (codRendimentos == 407) {
                        rendB407_SPA+=rendimentos;
                        ret404405407_SPA+=retencoes;
                        retSobTaxa404405407_SPA+=retSobretaxa;
                        contr404405407_SPA+=contribuicoes;
                    } else if (codRendimentos == 408) {
                        rendB408_SPA+=rendimentos;
                        ret408_SPA+=retencoes;
                        contr408_SPA+=contribuicoes;
                    } else if (codRendimentos == 403) {
                        rendB403_SPA+=rendimentos;
                    }
                    rendBTotal_SPA+=rendimentos;
                    continue;
                }
                if (titular5.startsWith("B")) {
                    if (codRendimentos == 401 || codRendimentos == 402) {
                        if (codRendimentos == 401) {
                            rendB401_SPB+=rendimentos;
                            contr401_SPB+=contribuicoes;
                        } else if (codRendimentos == 402) {
                            Grat402_SPB+=rendimentos;
                        }
                        ret401402_SPB+=retencoes;
                        retSobTaxa401402_SPB+=retSobretaxa;
                    } else if (codRendimentos == 404) {
                        rendB404_SPB+=rendimentos;
                        ret404405407_SPB+=retencoes;
                        retSobTaxa404405407_SPB+=retSobretaxa;
                        contr404405407_SPB+=contribuicoes;
                    } else if (codRendimentos == 405) {
                        rendSobrev405_SPB+=rendimentos;
                        ret404405407_SPB+=retencoes;
                        retSobTaxa404405407_SPB+=retSobretaxa;
                        contr404405407_SPB+=contribuicoes;
                    } else if (codRendimentos == 406) {
                        rendB406_SPB+=rendimentos;
                    } else if (codRendimentos == 407) {
                        rendB407_SPB+=rendimentos;
                        ret404405407_SPB+=retencoes;
                        retSobTaxa404405407_SPB+=retSobretaxa;
                        contr404405407_SPB+=contribuicoes;
                    } else if (codRendimentos == 408) {
                        rendB408_SPB+=rendimentos;
                        ret408_SPB+=retencoes;
                        contr408_SPB+=contribuicoes;
                    } else if (codRendimentos == 403) {
                        rendB403_SPB+=rendimentos;
                    }
                    rendBTotal_SPB+=rendimentos;
                    continue;
                }
                if (titular5.startsWith("F")) {
                    if (codRendimentos == 401 || codRendimentos == 402) {
                        if (codRendimentos == 401) {
                            rendB401_SPF+=rendimentos;
                            contr401_SPF+=contribuicoes;
                        } else if (codRendimentos == 402) {
                            Grat402_SPF+=rendimentos;
                        }
                        ret401402_SPF+=retencoes;
                        retSobTaxa401402_SPF+=retSobretaxa;
                    } else if (codRendimentos == 404) {
                        rendB404_SPF+=rendimentos;
                        ret404405407_SPF+=retencoes;
                        retSobTaxa404405407_SPF+=retSobretaxa;
                        contr404405407_SPF+=contribuicoes;
                    } else if (codRendimentos == 405) {
                        rendSobrev405_SPF+=rendimentos;
                        ret404405407_SPF+=retencoes;
                        retSobTaxa404405407_SPF+=retSobretaxa;
                        contr404405407_SPF+=contribuicoes;
                    } else if (codRendimentos == 406) {
                        rendB406_SPF+=rendimentos;
                    } else if (codRendimentos == 407) {
                        rendB407_SPF+=rendimentos;
                        ret404405407_SPF+=retencoes;
                        retSobTaxa404405407_SPF+=retSobretaxa;
                        contr404405407_SPF+=contribuicoes;
                    } else if (codRendimentos == 403) {
                        rendB403_SPF+=rendimentos;
                    }
                    rendBTotal_SPF+=rendimentos;
                    continue;
                }
                if (nifD1 == rosto.getNIFTitular(titular5)) {
                    if (codRendimentos == 401 || codRendimentos == 402) {
                        if (codRendimentos == 401) {
                            rendB401_D1+=rendimentos;
                            contr401_D1+=contribuicoes;
                        } else if (codRendimentos == 402) {
                            Grat402_D1+=rendimentos;
                        }
                        ret401402_D1+=retencoes;
                        retSobTaxa401402_D1+=retSobretaxa;
                    } else if (codRendimentos == 404) {
                        rendB404_D1+=rendimentos;
                        ret404405407_D1+=retencoes;
                        retSobTaxa404405407_D1+=retSobretaxa;
                        contr404405407_D1+=contribuicoes;
                    } else if (codRendimentos == 405) {
                        rendSobrev405_D1+=rendimentos;
                        ret404405407_D1+=retencoes;
                        retSobTaxa404405407_D1+=retSobretaxa;
                        contr404405407_D1+=contribuicoes;
                    } else if (codRendimentos == 406) {
                        rendB406_D1+=rendimentos;
                    } else if (codRendimentos == 407) {
                        rendB407_D1+=rendimentos;
                        ret404405407_D1+=retencoes;
                        retSobTaxa404405407_D1+=retSobretaxa;
                        contr404405407_D1+=contribuicoes;
                    } else if (codRendimentos == 403) {
                        rendB403_D1+=rendimentos;
                    }
                    rendBTotal_D1+=rendimentos;
                    continue;
                }
                if (nifD2 == rosto.getNIFTitular(titular5)) {
                    if (codRendimentos == 401 || codRendimentos == 402) {
                        if (codRendimentos == 401) {
                            rendB401_D2+=rendimentos;
                            contr401_D2+=contribuicoes;
                        } else if (codRendimentos == 402) {
                            Grat402_D2+=rendimentos;
                        }
                        ret401402_D2+=retencoes;
                        retSobTaxa401402_D2+=retSobretaxa;
                    } else if (codRendimentos == 404) {
                        rendB404_D2+=rendimentos;
                        ret404405407_D2+=retencoes;
                        retSobTaxa404405407_D2+=retSobretaxa;
                        contr404405407_D2+=contribuicoes;
                    } else if (codRendimentos == 405) {
                        rendSobrev405_D2+=rendimentos;
                        ret404405407_D2+=retencoes;
                        retSobTaxa404405407_D2+=retSobretaxa;
                        contr404405407_D2+=contribuicoes;
                    } else if (codRendimentos == 406) {
                        rendB406_D2+=rendimentos;
                    } else if (codRendimentos == 407) {
                        rendB407_D2+=rendimentos;
                        ret404405407_D2+=retencoes;
                        retSobTaxa404405407_D2+=retSobretaxa;
                        contr404405407_D2+=contribuicoes;
                    } else if (codRendimentos == 403) {
                        rendB403_D2+=rendimentos;
                    }
                    rendBTotal_D2+=rendimentos;
                    continue;
                }
                if (nifD3 != rosto.getNIFTitular(titular5)) continue;
                if (codRendimentos == 401 || codRendimentos == 402) {
                    if (codRendimentos == 401) {
                        rendB401_D3+=rendimentos;
                        contr401408_D3+=contribuicoes;
                    } else if (codRendimentos == 402) {
                        Grat402_D3+=rendimentos;
                    }
                    ret401402_D3+=retencoes;
                    retSobTaxa401402_D3+=retSobretaxa;
                } else if (codRendimentos == 404) {
                    rendB404_D3+=rendimentos;
                    ret404405407_D3+=retencoes;
                    retSobTaxa404405407_D3+=retSobretaxa;
                    contr404405407_D3+=contribuicoes;
                } else if (codRendimentos == 405) {
                    rendSobrev405_D3+=rendimentos;
                    ret404405407_D3+=retencoes;
                    retSobTaxa404405407_D3+=retSobretaxa;
                    contr404405407_D3+=contribuicoes;
                } else if (codRendimentos == 406) {
                    rendB406_D3+=rendimentos;
                } else if (codRendimentos == 407) {
                    rendB407_D3+=rendimentos;
                    ret404405407_D3+=retencoes;
                    retSobTaxa404405407_D3+=retSobretaxa;
                    contr404405407_D3+=contribuicoes;
                } else if (codRendimentos == 408) {
                    contr401408_D3+=contribuicoes;
                } else if (codRendimentos == 403) {
                    rendB403_D3+=rendimentos;
                }
                rendBTotal_D3+=rendimentos;
            }
        }
        values.put("TemRendSPA", anexoA != null && rendBTotal_SPA > 0);
        values.put("TemRendSPB", anexoA != null && rendBTotal_SPB > 0);
        values.put("TemRendSPF", anexoA != null && rendBTotal_SPF > 0);
        values.put("TemRendD1", anexoA != null && rendBTotal_D1 > 0);
        values.put("TemRendD2", anexoA != null && rendBTotal_D2 > 0);
        values.put("TemRendD3", anexoA != null && rendBTotal_D3 > 0);
        values.put("RBASPA", anexoA != null ? (double)rendB401_SPA / 100.0 : 0.0);
        values.put("RBASPB", anexoA != null ? (double)rendB401_SPB / 100.0 : 0.0);
        values.put("RBASPF", anexoA != null ? (double)rendB401_SPF / 100.0 : 0.0);
        values.put("RBAD1", anexoA != null ? (double)rendB401_D1 / 100.0 : 0.0);
        values.put("RBAD2", anexoA != null ? (double)rendB401_D2 / 100.0 : 0.0);
        values.put("RBAD3", anexoA != null ? (double)rendB401_D3 / 100.0 : 0.0);
        values.put("GratASPA", anexoA != null ? (double)Grat402_SPA / 100.0 : 0.0);
        values.put("GratASPB", anexoA != null ? (double)Grat402_SPB / 100.0 : 0.0);
        values.put("GratASPF", anexoA != null ? (double)Grat402_SPF / 100.0 : 0.0);
        values.put("GratAD1", anexoA != null ? (double)Grat402_D1 / 100.0 : 0.0);
        values.put("GratAD2", anexoA != null ? (double)Grat402_D2 / 100.0 : 0.0);
        values.put("GratAD3", anexoA != null ? (double)Grat402_D3 / 100.0 : 0.0);
        values.put("RFASPA", anexoA != null ? (double)ret401402_SPA / 100.0 : 0.0);
        values.put("RFASPB", anexoA != null ? (double)ret401402_SPB / 100.0 : 0.0);
        values.put("RFASPF", anexoA != null ? (double)ret401402_SPF / 100.0 : 0.0);
        values.put("RFAD1", anexoA != null ? (double)ret401402_D1 / 100.0 : 0.0);
        values.put("RFAD2", anexoA != null ? (double)ret401402_D2 / 100.0 : 0.0);
        values.put("RFAD3", anexoA != null ? (double)ret401402_D3 / 100.0 : 0.0);
        values.put("RFSTASPA", anexoA != null ? (double)retSobTaxa401402_SPA / 100.0 : 0.0);
        values.put("RFSTASPB", anexoA != null ? (double)retSobTaxa401402_SPB / 100.0 : 0.0);
        values.put("RFSTASPF", anexoA != null ? (double)retSobTaxa401402_SPF / 100.0 : 0.0);
        values.put("RFSTAD1", anexoA != null ? (double)retSobTaxa401402_D1 / 100.0 : 0.0);
        values.put("RFSTAD2", anexoA != null ? (double)retSobTaxa401402_D2 / 100.0 : 0.0);
        values.put("RFSTAD3", anexoA != null ? (double)retSobTaxa401402_D3 / 100.0 : 0.0);
        values.put("CObrigASPA", anexoA != null ? (double)contr401_SPA / 100.0 : 0.0);
        values.put("CObrigASPB", anexoA != null ? (double)contr401_SPB / 100.0 : 0.0);
        values.put("CObrigASPF", anexoA != null ? (double)contr401_SPF / 100.0 : 0.0);
        values.put("CObrigAD1", anexoA != null ? (double)contr401_D1 / 100.0 : 0.0);
        values.put("CObrigAD2", anexoA != null ? (double)contr401_D2 / 100.0 : 0.0);
        values.put("CObrigAD3", anexoA != null ? (double)contr401408_D3 / 100.0 : 0.0);
        values.put("PSobrevSPA", anexoA != null ? (double)rendSobrev405_SPA / 100.0 : 0.0);
        values.put("PSobrevSPB", anexoA != null ? (double)rendSobrev405_SPB / 100.0 : 0.0);
        values.put("PSobrevSPF", anexoA != null ? (double)rendSobrev405_SPF / 100.0 : 0.0);
        values.put("PSobrevD1", anexoA != null ? (double)rendSobrev405_D1 / 100.0 : 0.0);
        values.put("PSobrevD2", anexoA != null ? (double)rendSobrev405_D2 / 100.0 : 0.0);
        values.put("PSobrevD3", anexoA != null ? (double)rendSobrev405_D3 / 100.0 : 0.0);
        long SPA_Soma410 = 0;
        long SPB_Soma410 = 0;
        long SPF_Soma410 = 0;
        long D1_Soma410 = 0;
        long D2_Soma410 = 0;
        long D3_Soma410 = 0;
        long SPA_Soma409 = 0;
        long SPB_Soma409 = 0;
        long SPF_Soma409 = 0;
        long D1_Soma409 = 0;
        long D2_Soma409 = 0;
        long D3_Soma409 = 0;
        long SPA_Soma411 = 0;
        long SPB_Soma411 = 0;
        long SPF_Soma411 = 0;
        long D1_Soma411 = 0;
        long D2_Soma411 = 0;
        long D3_Soma411 = 0;
        long SPA_Soma413 = 0;
        long SPB_Soma413 = 0;
        long SPF_Soma413 = 0;
        long l = 0;
        long l2 = 0;
        long l3 = 0;
        if (anexoA != null) {
            void i$2;
            Iterator<AnexoAq04T4B_Linha> iterator = anexoA.getQuadro04().getAnexoAq04T4B().iterator();
            while (i$2.hasNext()) {
                void linha4B;
                void valor;
                String string;
                void codDespesa;
                AnexoAq04T4B_Linha anexoAq04T4B_Linha = (AnexoAq04T4B_Linha)i$2.next();
                long l4 = linha4B.getValor() != null ? linha4B.getValor() : 0;
                long l5 = linha4B.getCodDespesa() != null ? linha4B.getCodDespesa() : 0;
                String string2 = string = linha4B.getTitular() != null ? linha4B.getTitular() : "";
                if (titular.startsWith("A")) {
                    if (codDespesa == 410) {
                        SPA_Soma410+=valor;
                        continue;
                    }
                    if (codDespesa == 409) {
                        SPA_Soma409+=valor;
                        continue;
                    }
                    if (codDespesa == 411) {
                        SPA_Soma411+=valor;
                        continue;
                    }
                    if (codDespesa != 413) continue;
                    SPA_Soma413+=valor;
                    continue;
                }
                if (titular.startsWith("B")) {
                    if (codDespesa == 410) {
                        SPB_Soma410+=valor;
                        continue;
                    }
                    if (codDespesa == 409) {
                        SPB_Soma409+=valor;
                        continue;
                    }
                    if (codDespesa == 411) {
                        SPB_Soma411+=valor;
                        continue;
                    }
                    if (codDespesa != 413) continue;
                    SPB_Soma413+=valor;
                    continue;
                }
                if (titular.startsWith("F")) {
                    if (codDespesa == 410) {
                        SPF_Soma410+=valor;
                        continue;
                    }
                    if (codDespesa == 409) {
                        SPF_Soma409+=valor;
                        continue;
                    }
                    if (codDespesa == 411) {
                        SPF_Soma411+=valor;
                        continue;
                    }
                    if (codDespesa != 413) continue;
                    SPF_Soma413+=valor;
                    continue;
                }
                if (nifD1 == rosto.getNIFTitular(titular)) {
                    if (codDespesa == 410) {
                        D1_Soma410+=valor;
                        continue;
                    }
                    if (codDespesa == 409) {
                        D1_Soma409+=valor;
                        continue;
                    }
                    if (codDespesa == 411) {
                        D1_Soma411+=valor;
                        continue;
                    }
                    if (codDespesa != 413) continue;
                    D1_Soma413+=valor;
                    continue;
                }
                if (nifD2 == rosto.getNIFTitular(titular)) {
                    if (codDespesa == 410) {
                        D2_Soma410+=valor;
                        continue;
                    }
                    if (codDespesa == 409) {
                        D2_Soma409+=valor;
                        continue;
                    }
                    if (codDespesa == 411) {
                        D2_Soma411+=valor;
                        continue;
                    }
                    if (codDespesa != 413) continue;
                    D2_Soma413+=valor;
                    continue;
                }
                if (nifD3 != rosto.getNIFTitular(titular)) continue;
                if (codDespesa == 410) {
                    D3_Soma410+=valor;
                    continue;
                }
                if (codDespesa == 409) {
                    D3_Soma409+=valor;
                    continue;
                }
                if (codDespesa == 411) {
                    D3_Soma411+=valor;
                    continue;
                }
                if (codDespesa != 413) continue;
                D3_Soma413+=valor;
            }
        }
        values.put("SeguDesgASPA", anexoA != null ? (double)SPA_Soma413 / 100.0 : 0.0);
        values.put("SeguDesgASPB", anexoA != null ? (double)SPB_Soma413 / 100.0 : 0.0);
        values.put("SeguDesgASPF", anexoA != null ? (double)SPF_Soma413 / 100.0 : 0.0);
        values.put("SeguDesgAD1", anexoA != null ? D1_Soma413 / 100.0 : 0.0);
        values.put("SeguDesgAD2", anexoA != null ? D2_Soma413 / 100.0 : 0.0);
        values.put("SeguDesgAD3", anexoA != null ? D3_Soma413 / 100.0 : 0.0);
        values.put("IndmnASPA", anexoA != null ? (double)SPA_Soma410 / 100.0 : 0.0);
        values.put("IndmnASPB", anexoA != null ? (double)SPB_Soma410 / 100.0 : 0.0);
        values.put("IndmnASPF", anexoA != null ? (double)SPF_Soma410 / 100.0 : 0.0);
        values.put("IndmnAD1", anexoA != null ? (double)D1_Soma410 / 100.0 : 0.0);
        values.put("IndmnAD2", anexoA != null ? (double)D2_Soma410 / 100.0 : 0.0);
        values.put("IndmnAD3", anexoA != null ? (double)D3_Soma410 / 100.0 : 0.0);
        values.put("QuotizASPA", anexoA != null ? (double)SPA_Soma409 / 100.0 : 0.0);
        values.put("QuotizASPB", anexoA != null ? (double)SPB_Soma409 / 100.0 : 0.0);
        values.put("QuotizASPF", anexoA != null ? (double)SPF_Soma409 / 100.0 : 0.0);
        values.put("QuotizAD1", anexoA != null ? (double)D1_Soma409 / 100.0 : 0.0);
        values.put("QuotizAD2", anexoA != null ? (double)D2_Soma409 / 100.0 : 0.0);
        values.put("QuotizAD3", anexoA != null ? (double)D3_Soma409 / 100.0 : 0.0);
        values.put("QuotizOPASPA", anexoA != null ? (double)SPA_Soma411 / 100.0 : 0.0);
        values.put("QuotizOPASPB", anexoA != null ? (double)SPB_Soma411 / 100.0 : 0.0);
        values.put("QuotizOPASPF", anexoA != null ? (double)SPF_Soma411 / 100.0 : 0.0);
        values.put("QuotizOPAD1", anexoA != null ? (double)D1_Soma411 / 100.0 : 0.0);
        values.put("QuotizOPAD2", anexoA != null ? (double)D2_Soma411 / 100.0 : 0.0);
        values.put("QuotizOPAD3", anexoA != null ? (double)D3_Soma411 / 100.0 : 0.0);
        values.put("RBHSPA", anexoA != null ? (double)rendB404_SPA / 100.0 : 0.0);
        values.put("RBHSPB", anexoA != null ? (double)rendB404_SPB / 100.0 : 0.0);
        values.put("RBHSPF", anexoA != null ? (double)rendB404_SPF / 100.0 : 0.0);
        values.put("RBHD1", anexoA != null ? (double)rendB404_D1 / 100.0 : 0.0);
        values.put("RBHD2", anexoA != null ? (double)rendB404_D2 / 100.0 : 0.0);
        values.put("RBHD3", anexoA != null ? (double)rendB404_D3 / 100.0 : 0.0);
        values.put("PAlimHSPA", anexoA != null ? (double)rendB406_SPA / 100.0 : 0.0);
        values.put("PAlimHSPB", anexoA != null ? (double)rendB406_SPB / 100.0 : 0.0);
        values.put("PAlimHSPF", anexoA != null ? (double)rendB406_SPF / 100.0 : 0.0);
        values.put("PAlimHD1", anexoA != null ? (double)rendB406_D1 / 100.0 : 0.0);
        values.put("PAlimHD2", anexoA != null ? (double)rendB406_D2 / 100.0 : 0.0);
        values.put("PAlimHD3", anexoA != null ? (double)rendB406_D3 / 100.0 : 0.0);
        values.put("RVitHSPA", anexoA != null ? (double)rendB407_SPA / 100.0 : 0.0);
        values.put("RVitHSPB", anexoA != null ? (double)rendB407_SPB / 100.0 : 0.0);
        values.put("RVitHSPF", anexoA != null ? (double)rendB407_SPF / 100.0 : 0.0);
        values.put("RVitHD1", anexoA != null ? (double)rendB407_D1 / 100.0 : 0.0);
        values.put("RVitHD2", anexoA != null ? (double)rendB407_D2 / 100.0 : 0.0);
        values.put("RVitHD3", anexoA != null ? (double)rendB407_D3 / 100.0 : 0.0);
        values.put("RFHSPA", anexoA != null ? (double)ret404405407_SPA / 100.0 : 0.0);
        values.put("RFHSPB", anexoA != null ? (double)ret404405407_SPB / 100.0 : 0.0);
        values.put("RFHSPF", anexoA != null ? (double)ret404405407_SPF / 100.0 : 0.0);
        values.put("RFHD1", anexoA != null ? (double)ret404405407_D1 / 100.0 : 0.0);
        values.put("RFHD2", anexoA != null ? (double)ret404405407_D2 / 100.0 : 0.0);
        values.put("RFHD3", anexoA != null ? (double)ret404405407_D3 / 100.0 : 0.0);
        values.put("RFSTHSPA", anexoA != null ? (double)retSobTaxa404405407_SPA / 100.0 : 0.0);
        values.put("RFSTHSPB", anexoA != null ? (double)retSobTaxa404405407_SPB / 100.0 : 0.0);
        values.put("RFSTHSPF", anexoA != null ? (double)retSobTaxa404405407_SPF / 100.0 : 0.0);
        values.put("RFSTHD1", anexoA != null ? (double)retSobTaxa404405407_D1 / 100.0 : 0.0);
        values.put("RFSTHD2", anexoA != null ? (double)retSobTaxa404405407_D2 / 100.0 : 0.0);
        values.put("RFSTHD3", anexoA != null ? (double)retSobTaxa404405407_D3 / 100.0 : 0.0);
        values.put("CObrigHSPA", DOUBLE_ZERO);
        values.put("CObrigHSPB", DOUBLE_ZERO);
        values.put("CObrigHSPF", DOUBLE_ZERO);
        values.put("CObrigHD1", DOUBLE_ZERO);
        values.put("CObrigHD2", DOUBLE_ZERO);
        values.put("CObrigHD3", DOUBLE_ZERO);
        values.put("QuotizHSPA", DOUBLE_ZERO);
        values.put("QuotizHSPB", DOUBLE_ZERO);
        values.put("QuotizHSPF", DOUBLE_ZERO);
        values.put("QuotizHD1", DOUBLE_ZERO);
        values.put("QuotizHD2", DOUBLE_ZERO);
        values.put("QuotizHD3", DOUBLE_ZERO);
        values.put("ContribHSPA", anexoA != null ? (double)contr404405407_SPA / 100.0 : 0.0);
        values.put("ContribHSPB", anexoA != null ? (double)contr404405407_SPB / 100.0 : 0.0);
        values.put("ContribHSPF", anexoA != null ? (double)contr404405407_SPF / 100.0 : 0.0);
        values.put("ContribHD1", anexoA != null ? (double)contr404405407_D1 / 100.0 : 0.0);
        values.put("ContribHD2", anexoA != null ? (double)contr404405407_D2 / 100.0 : 0.0);
        values.put("ContribHD3", anexoA != null ? (double)contr404405407_D3 / 100.0 : 0.0);
        boolean i$2 = false;
        boolean linha4B = false;
        boolean valor = false;
        boolean bl = false;
        if (anexoA != null) {
            void i$3;
            Iterator<AnexoAq04T4A_Linha> codDespesa = anexoA.getQuadro04().getAnexoAq04T4A().iterator();
            while (i$3.hasNext()) {
                void linhaQ04A;
                AnexoAq04T4A_Linha anexoAq04T4A_Linha = (AnexoAq04T4A_Linha)i$3.next();
                String string = titular = linhaQ04A.getTitular() != null ? linhaQ04A.getTitular() : "";
                if (titular.startsWith("A") && contpre_SPA == 0 && pripag_SPA == 0) {
                    contpre_SPA = linhaQ04A.getDataContratoPreReforma() != null ? linhaQ04A.getDataContratoPreReforma().getYear() : 0;
                    pripag_SPA = linhaQ04A.getDataPrimeiroPagamento() != null ? linhaQ04A.getDataPrimeiroPagamento().getYear() : 0;
                    continue;
                }
                if (!titular.startsWith("B") || contpre_SPB != false || pripag_SPB != false) continue;
                contpre_SPB = linhaQ04A.getDataContratoPreReforma() != null ? linhaQ04A.getDataContratoPreReforma().getYear() : 0;
                pripag_SPB = linhaQ04A.getDataPrimeiroPagamento() != null ? linhaQ04A.getDataPrimeiroPagamento().getYear() : 0;
            }
        }
        values.put("valrendBrutoPreSPA", anexoA != null ? (double)rendB408_SPA / 100.0 : 0.0);
        values.put("valrendBrutoPreSPB", anexoA != null ? (double)rendB408_SPB / 100.0 : 0.0);
        values.put("valcontObrigPreSPA", anexoA != null ? (double)contr408_SPA / 100.0 : 0.0);
        values.put("valcontObrigPreSPB", anexoA != null ? (double)contr408_SPB / 100.0 : 0.0);
        values.put("valretFontePreSPA", anexoA != null ? (double)ret408_SPA / 100.0 : 0.0);
        values.put("valretFontePreSPB", anexoA != null ? (double)ret408_SPB / 100.0 : 0.0);
        values.put("valdataContPreSPA", contpre_SPA);
        values.put("valdataContPreSPB", contpre_SPB);
        values.put("valdataPriPagPreSPA", pripag_SPA);
        values.put("valdataPriPagPreSPB", pripag_SPB);
        long i$3 = 0;
        long titular6 = 0;
        long l6 = 0;
        long l7 = 0;
        long l8 = 0;
        long l9 = 0;
        boolean bl2 = false;
        boolean bl3 = false;
        boolean bl4 = false;
        boolean bl5 = false;
        boolean bl6 = false;
        boolean bl7 = false;
        long l10 = 0;
        long l11 = 0;
        long l12 = 0;
        long l13 = 0;
        long l14 = 0;
        long l15 = 0;
        boolean bl8 = false;
        boolean bl9 = false;
        boolean bl10 = false;
        boolean bl11 = false;
        boolean bl12 = false;
        boolean bl13 = false;
        if (anexoA != null) {
            Iterator<AnexoAq05T5_Linha> iterator = anexoA.getQuadro05().getAnexoAq05T5().iterator();
            while (i$.hasNext()) {
                long l16;
                void linhaQ5;
                void anos;
                void titular7;
                void rendimentos;
                void codRendimentos;
                AnexoAq05T5_Linha anexoAq05T5_Linha = (AnexoAq05T5_Linha)i$.next();
                String string = linhaQ5.getTitular() != null ? linhaQ5.getTitular() : "";
                long l17 = linhaQ5.getCodRendimentos() != null ? linhaQ5.getCodRendimentos() : 0;
                int n = linhaQ5.getNanos() != null ? linhaQ5.getNanos().intValue() : 0;
                long l18 = l16 = linhaQ5.getRendimentos() != null ? linhaQ5.getRendimentos() : 0;
                if (titular7.startsWith("A")) {
                    if (codRendimentos == 404 || codRendimentos == 405 || codRendimentos == 406 || codRendimentos == 407) {
                        SPA_404To407+=rendimentos;
                        SPA_404To407Anos+=anos;
                        continue;
                    }
                    if (codRendimentos != 401 && codRendimentos != 402 && codRendimentos != 403) continue;
                    SPA_401To403+=rendimentos;
                    SPA_401To403Anos+=anos;
                    continue;
                }
                if (titular7.startsWith("B")) {
                    if (codRendimentos == 404 || codRendimentos == 405 || codRendimentos == 406 || codRendimentos == 407) {
                        SPB_404To407+=rendimentos;
                        SPB_404To407Anos+=anos;
                        continue;
                    }
                    if (codRendimentos != 401 && codRendimentos != 402 && codRendimentos != 403) continue;
                    SPB_401To403+=rendimentos;
                    SPB_401To403Anos+=anos;
                    continue;
                }
                if (titular7.startsWith("F")) {
                    if (codRendimentos == 404 || codRendimentos == 405 || codRendimentos == 406 || codRendimentos == 407) {
                        SPF_404To407+=rendimentos;
                        SPF_404To407Anos+=anos;
                        continue;
                    }
                    if (codRendimentos != 401 && codRendimentos != 402 && codRendimentos != 403) continue;
                    SPF_401To403+=rendimentos;
                    SPF_401To403Anos+=anos;
                    continue;
                }
                if (nifD1 == rosto.getNIFTitular((String)titular7)) {
                    if (codRendimentos == 404 || codRendimentos == 405 || codRendimentos == 406 || codRendimentos == 407) {
                        D1_404To407+=rendimentos;
                        D1_404To407Anos+=anos;
                        continue;
                    }
                    if (codRendimentos != 401 && codRendimentos != 402 && codRendimentos != 403) continue;
                    D1_401To403+=rendimentos;
                    D1_401To403Anos+=anos;
                    continue;
                }
                if (nifD2 == rosto.getNIFTitular((String)titular7)) {
                    if (codRendimentos == 404 || codRendimentos == 405 || codRendimentos == 406 || codRendimentos == 407) {
                        D2_404To407+=rendimentos;
                        D2_404To407Anos+=anos;
                        continue;
                    }
                    if (codRendimentos != 401 && codRendimentos != 402 && codRendimentos != 403) continue;
                    D2_401To403+=rendimentos;
                    D2_401To403Anos+=anos;
                    continue;
                }
                if (nifD3 != rosto.getNIFTitular((String)titular7)) continue;
                if (codRendimentos == 404 || codRendimentos == 405 || codRendimentos == 406 || codRendimentos == 407) {
                    D3_404To407+=rendimentos;
                    D3_404To407Anos+=anos;
                    continue;
                }
                if (codRendimentos != 401 && codRendimentos != 402 && codRendimentos != 403) continue;
                D3_401To403+=rendimentos;
                D3_401To403Anos+=anos;
            }
        }
        values.put("valrendAntHSPA", anexoA != null ? SPA_404To407 / 100.0 : 0.0);
        values.put("valrendAntHSPB", anexoA != null ? SPB_404To407 / 100.0 : 0.0);
        values.put("valrendAntHSPF", anexoA != null ? SPF_404To407 / 100.0 : 0.0);
        values.put("valrendAntHDP1", anexoA != null ? D1_404To407 / 100.0 : 0.0);
        values.put("valrendAntHDP2", anexoA != null ? D2_404To407 / 100.0 : 0.0);
        values.put("valrendAntHDP3", anexoA != null ? D3_404To407 / 100.0 : 0.0);
        values.put("anosrendAntHSPA", (int)SPA_404To407Anos);
        values.put("anosrendAntHSPB", (int)SPB_404To407Anos);
        values.put("anosrendAntHSPF", (int)SPF_404To407Anos);
        values.put("anosrendAntHDP1", (int)D1_404To407Anos);
        values.put("anosrendAntHDP2", (int)D2_404To407Anos);
        values.put("anosrendAntHDP3", (int)D3_404To407Anos);
        values.put("valrendAntASPA", anexoA != null ? SPA_401To403 / 100.0 : 0.0);
        values.put("valrendAntASPB", anexoA != null ? SPB_401To403 / 100.0 : 0.0);
        values.put("valrendAntASPF", anexoA != null ? SPF_401To403 / 100.0 : 0.0);
        values.put("valrendAntADP1", anexoA != null ? D1_401To403 / 100.0 : 0.0);
        values.put("valrendAntADP2", anexoA != null ? D2_401To403 / 100.0 : 0.0);
        values.put("valrendAntADP3", anexoA != null ? D3_401To403 / 100.0 : 0.0);
        values.put("anosrendAntASPA", (int)SPA_401To403Anos);
        values.put("anosrendAntASPB", (int)SPB_401To403Anos);
        values.put("anosrendAntASPF", (int)SPF_401To403Anos);
        values.put("anosrendAntADP1", (int)D1_401To403Anos);
        values.put("anosrendAntADP2", (int)D2_401To403Anos);
        values.put("anosrendAntADP3", (int)D3_401To403Anos);
        values.put("RendDespSPA", anexoA != null ? (double)rendB403_SPA / 100.0 : 0.0);
        values.put("RendDespSPB", anexoA != null ? (double)rendB403_SPB / 100.0 : 0.0);
        values.put("RendDespSPF", anexoA != null ? (double)rendB403_SPF / 100.0 : 0.0);
        values.put("RendDespD1", anexoA != null ? (double)rendB403_D1 / 100.0 : 0.0);
        values.put("RendDespD2", anexoA != null ? (double)rendB403_D2 / 100.0 : 0.0);
        values.put("RendDespD3", anexoA != null ? (double)rendB403_D3 / 100.0 : 0.0);
        i$ = this.model.getAnexoH();
        this.doAnexoH(idadeSpA, idadeSpB, idadeSPF, values, rosto, (AnexoHModel)anexoH);
        this.putDefaultValues(values);
        return values;
    }

    private void doAnexoH(int idadeSpA, int idadeSpB, int idadeSPF, SimulatorHashtable<String, Object> values, RostoModel rosto, AnexoHModel anexoH) {
        HashMap<String, Double> anexoHt4defaultValues = new HashMap<String, Double>();
        for (int i = 1; i <= 10; ++i) {
            String index = StringUtil.padZeros(i, 2);
            anexoHt4defaultValues.put("cod4" + index + "SPA", DOUBLE_ZERO);
            anexoHt4defaultValues.put("ret4" + index + "SPA", DOUBLE_ZERO);
            anexoHt4defaultValues.put("cod4" + index + "SPB", DOUBLE_ZERO);
            anexoHt4defaultValues.put("ret4" + index + "SPB", DOUBLE_ZERO);
            anexoHt4defaultValues.put("cod4" + index + "D1", DOUBLE_ZERO);
            anexoHt4defaultValues.put("ret4" + index + "D1", DOUBLE_ZERO);
            anexoHt4defaultValues.put("cod4" + index + "D2", DOUBLE_ZERO);
            anexoHt4defaultValues.put("ret4" + index + "D2", DOUBLE_ZERO);
            anexoHt4defaultValues.put("cod4" + index + "D3", DOUBLE_ZERO);
            anexoHt4defaultValues.put("ret4" + index + "D3", DOUBLE_ZERO);
        }
        if (anexoH != null && anexoH.getQuadro04().getAnexoHq04T4() != null) {
            for (AnexoHq04T4_Linha linha : anexoH.getQuadro04().getAnexoHq04T4()) {
                String codRendimento;
                Long rendimentos = linha.getRendimentosIliquidos() != null ? linha.getRendimentosIliquidos() : 0;
                Long retencoes = linha.getRentencaoIRS() != null ? linha.getRentencaoIRS() : 0;
                String string = codRendimento = linha.getCodRendimentos() != null ? String.valueOf(linha.getCodRendimentos()) : "";
                String titular = linha.getTitular();
                if (StringUtil.isEmpty(titular)) continue;
                if ("A".equals(titular)) {
                    titular = "SPA";
                } else if ("B".equals(titular)) {
                    titular = "SPB";
                }
                String codKey = "cod" + codRendimento + titular;
                Double OldCodValue = (Double)values.remove(codKey);
                if (OldCodValue == null) {
                    OldCodValue = (double)DOUBLE_ZERO;
                }
                values.put(codKey, (double)rendimentos.longValue() / 100.0 + OldCodValue);
                String retKey = "ret" + codRendimento + titular;
                Double OldRetValue = (Double)values.remove(retKey);
                if (OldRetValue == null) {
                    OldRetValue = (double)DOUBLE_ZERO;
                }
                values.put(retKey, (double)retencoes.longValue() / 100.0 + OldRetValue);
                anexoHt4defaultValues.remove(codKey);
                anexoHt4defaultValues.remove(retKey);
            }
        }
        for (Map.Entry entry : anexoHt4defaultValues.entrySet()) {
            values.put((String)entry.getKey(), entry.getValue());
        }
        values.put("AbatPensObrig", anexoH != null && anexoH.getQuadro06().getAnexoHq06C601() != null ? (double)anexoH.getQuadro06().getAnexoHq06C601().longValue() / 100.0 : 0.0);
        values.put("AbatImpRendas", anexoH != null && anexoH.getQuadro06().getAnexoHq06C602() != null ? (double)anexoH.getQuadro06().getAnexoHq06C602().longValue() / 100.0 : 0.0);
        values.put("AbatAquisImov", anexoH != null && anexoH.getQuadro06().getAnexoHq06C603() != null ? (double)anexoH.getQuadro06().getAnexoHq06C603().longValue() / 100.0 : 0.0);
        long SPA = rosto.getQuadro03().getQ03C03() != null ? rosto.getQuadro03().getQ03C03() : 0;
        long SPB = rosto.getQuadro03().getQ03C04() != null ? rosto.getQuadro03().getQ03C04() : 0;
        long CF = rosto.getQuadro07().getQ07C1() != null ? rosto.getQuadro07().getQ07C1() : 0;
        long soma712 = 0;
        long soma713 = 0;
        long soma714 = 0;
        long soma715 = 0;
        long soma716 = 0;
        long soma701_711 = 0;
        long soma702 = 0;
        long soma705 = 0;
        long soma706_DGC = 0;
        long soma706_Outros = 0;
        long soma707 = 0;
        long soma708 = 0;
        long soma709 = 0;
        long soma710 = 0;
        long soma705_SPA = 0;
        long soma705_SPB = 0;
        long soma701_SPA = 0;
        long soma701_SPB = 0;
        long soma701_CF = 0;
        long soma711_SPA = 0;
        long soma711_SPB = 0;
        long soma711_CF = 0;
        long soma733_SPA = 0;
        long soma733_SPB = 0;
        long soma733_CF = 0;
        long soma717 = 0;
        long soma718 = 0;
        long soma719 = 0;
        long soma720 = 0;
        long soma721 = 0;
        long soma722 = 0;
        long soma723 = 0;
        long soma724 = 0;
        long soma725 = 0;
        long soma726 = 0;
        long soma727 = 0;
        long soma728 = 0;
        long soma729 = 0;
        long soma730_DGC = 0;
        long soma730_Outros = 0;
        long soma731 = 0;
        long soma732 = 0;
        long soma734 = 0;
        long soma735 = 0;
        long soma736_DGC = 0;
        long soma736_Outros = 0;
        long soma737_DGC = 0;
        long soma737_Outros = 0;
        long soma737_SPeCF = 0;
        long soma738 = 0;
        long soma739 = 0;
        long soma740 = 0;
        long soma741 = 0;
        long soma742_DGC = 0;
        long soma742_Outros = 0;
        long soma743 = 0;
        HashSet<String> set730_DGC = new HashSet<String>();
        HashSet<String> set730_D = new HashSet<String>();
        if (anexoH != null) {
            for (AnexoHq07T7_Linha linha : anexoH.getQuadro07().getAnexoHq07T7()) {
                long importanciaAplicada;
                int codBeneficio = linha.getCodBeneficio() != null ? linha.getCodBeneficio().intValue() : 0;
                String titular = linha.getTitular() != null ? linha.getTitular() : "";
                long l = importanciaAplicada = linha.getImportanciaAplicada() != null ? linha.getImportanciaAplicada() : 0;
                if (codBeneficio == 0) continue;
                switch (codBeneficio) {
                    case 701: {
                        soma701_711+=importanciaAplicada;
                        if (rosto.getNIFTitular(titular) == SPA) {
                            soma701_SPA+=importanciaAplicada;
                        }
                        if (rosto.getNIFTitular(titular) == SPB) {
                            soma701_SPB+=importanciaAplicada;
                        }
                        if (rosto.getNIFTitular(titular) != CF) break;
                        soma701_CF+=importanciaAplicada;
                        break;
                    }
                    case 702: {
                        soma702+=importanciaAplicada;
                        break;
                    }
                    case 705: {
                        soma705+=importanciaAplicada;
                        if (rosto.getNIFTitular(titular) == SPA) {
                            soma705_SPA+=importanciaAplicada;
                        }
                        if (rosto.getNIFTitular(titular) != SPB) break;
                        soma705_SPB+=importanciaAplicada;
                        break;
                    }
                    case 706: {
                        if (Quadro03.isDependenteEmGuardaConjunta(titular) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(titular)) {
                            soma706_DGC+=importanciaAplicada;
                            break;
                        }
                        if ((!Quadro03.isSujeitoPassivoA(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03())) && (!Quadro03.isSujeitoPassivoB(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!Quadro03.isSujeitoPassivoC(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03()) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isConjugeFalecido(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro07().getQ07C1())) && (!Quadro03.isDependenteNaoDeficiente(titular) || !rosto.getQuadro03().existsInDependentesNaoDeficientes(titular)) && (!Quadro03.isDependenteDeficiente(titular) || !rosto.getQuadro03().existsInDependentesDeficientes(titular))) break;
                        soma706_Outros+=importanciaAplicada;
                        break;
                    }
                    case 707: {
                        soma707+=importanciaAplicada;
                        break;
                    }
                    case 708: {
                        soma708+=importanciaAplicada;
                        break;
                    }
                    case 709: {
                        soma709+=importanciaAplicada;
                        break;
                    }
                    case 710: {
                        soma710+=importanciaAplicada;
                        break;
                    }
                    case 711: {
                        soma701_711+=importanciaAplicada;
                        if (rosto.getNIFTitular(titular) == SPA) {
                            soma711_SPA+=importanciaAplicada;
                        }
                        if (rosto.getNIFTitular(titular) == SPB) {
                            soma711_SPB+=importanciaAplicada;
                        }
                        if (rosto.getNIFTitular(titular) != CF) break;
                        soma711_CF+=importanciaAplicada;
                        break;
                    }
                    case 712: {
                        soma712+=importanciaAplicada;
                        break;
                    }
                    case 713: {
                        soma713+=importanciaAplicada;
                        break;
                    }
                    case 714: {
                        soma714+=importanciaAplicada;
                        break;
                    }
                    case 715: {
                        soma715+=importanciaAplicada;
                        break;
                    }
                    case 716: {
                        soma716+=importanciaAplicada;
                        break;
                    }
                    case 717: {
                        soma717+=importanciaAplicada;
                        break;
                    }
                    case 718: {
                        soma718+=importanciaAplicada;
                        break;
                    }
                    case 719: {
                        soma719+=importanciaAplicada;
                        break;
                    }
                    case 720: {
                        soma720+=importanciaAplicada;
                        break;
                    }
                    case 721: {
                        soma721+=importanciaAplicada;
                        break;
                    }
                    case 722: {
                        soma722+=importanciaAplicada;
                        break;
                    }
                    case 723: {
                        soma723+=importanciaAplicada;
                        break;
                    }
                    case 724: {
                        soma724+=importanciaAplicada;
                        break;
                    }
                    case 725: {
                        soma725+=importanciaAplicada;
                        break;
                    }
                    case 726: {
                        soma726+=importanciaAplicada;
                        break;
                    }
                    case 727: {
                        soma727+=importanciaAplicada;
                        break;
                    }
                    case 728: {
                        soma728+=importanciaAplicada;
                        break;
                    }
                    case 729: {
                        soma729+=importanciaAplicada;
                        break;
                    }
                    case 730: {
                        if (Quadro03.isDependenteEmGuardaConjunta(titular) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(titular)) {
                            soma730_DGC+=importanciaAplicada;
                            if (set730_DGC.contains(titular)) break;
                            set730_DGC.add(titular);
                            break;
                        }
                        if (Quadro03.isSujeitoPassivoA(titular) && !Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03()) || Quadro03.isSujeitoPassivoB(titular) && !Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04()) || Quadro03.isSujeitoPassivoC(titular) && !Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03()) && !Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04()) || pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isConjugeFalecido(titular) && !Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro07().getQ07C1()) || Quadro03.isDependenteNaoDeficiente(titular) && rosto.getQuadro03().existsInDependentesNaoDeficientes(titular) || Quadro03.isDependenteDeficiente(titular) && rosto.getQuadro03().existsInDependentesDeficientes(titular)) {
                            soma730_Outros+=importanciaAplicada;
                        }
                        if ((!Quadro03.isDependenteNaoDeficiente(titular) || !rosto.getQuadro03().existsInDependentesNaoDeficientes(titular)) && (!Quadro03.isDependenteDeficiente(titular) || !rosto.getQuadro03().existsInDependentesDeficientes(titular) || set730_D.contains(titular))) break;
                        set730_D.add(titular);
                        break;
                    }
                    case 731: {
                        soma731+=importanciaAplicada;
                        break;
                    }
                    case 732: {
                        soma732+=importanciaAplicada;
                        break;
                    }
                    case 733: {
                        if (rosto.getNIFTitular(titular) == SPA) {
                            soma733_SPA+=importanciaAplicada;
                        }
                        if (rosto.getNIFTitular(titular) == SPB) {
                            soma733_SPB+=importanciaAplicada;
                        }
                        if (rosto.getNIFTitular(titular) != CF) break;
                        soma733_CF+=importanciaAplicada;
                        break;
                    }
                    case 734: {
                        soma734+=importanciaAplicada;
                        break;
                    }
                    case 735: {
                        soma735+=importanciaAplicada;
                        break;
                    }
                    case 736: {
                        if (Quadro03.isDependenteEmGuardaConjunta(titular) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(titular)) {
                            soma736_DGC+=importanciaAplicada;
                            break;
                        }
                        if ((!Quadro03.isSujeitoPassivoA(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03())) && (!Quadro03.isSujeitoPassivoB(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!Quadro03.isSujeitoPassivoC(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03()) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isConjugeFalecido(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro07().getQ07C1())) && (!Quadro03.isDependenteNaoDeficiente(titular) || !rosto.getQuadro03().existsInDependentesNaoDeficientes(titular)) && (!Quadro03.isDependenteDeficiente(titular) || !rosto.getQuadro03().existsInDependentesDeficientes(titular))) break;
                        soma736_Outros+=importanciaAplicada;
                        break;
                    }
                    case 737: {
                        if (Quadro03.isDependenteEmGuardaConjunta(titular) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(titular)) {
                            soma737_DGC+=importanciaAplicada;
                            break;
                        }
                        if (Quadro03.isDependenteNaoDeficiente(titular) && rosto.getQuadro03().existsInDependentesNaoDeficientes(titular) || Quadro03.isDependenteDeficiente(titular) && rosto.getQuadro03().existsInDependentesDeficientes(titular) || pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isAscendentesEColaterais(titular) && rosto.getQuadro07().existsInAscendentesEColaterais(titular)) {
                            soma737_Outros+=importanciaAplicada;
                            break;
                        }
                        if ((!Quadro03.isSujeitoPassivoA(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03())) && (!Quadro03.isSujeitoPassivoB(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!Quadro03.isSujeitoPassivoC(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03()) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isConjugeFalecido(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro07().getQ07C1()))) break;
                        soma737_SPeCF+=importanciaAplicada;
                        break;
                    }
                    case 738: {
                        soma738+=importanciaAplicada;
                        break;
                    }
                    case 739: {
                        soma739+=importanciaAplicada;
                        break;
                    }
                    case 740: {
                        soma740+=importanciaAplicada;
                        break;
                    }
                    case 741: {
                        soma741+=importanciaAplicada;
                        break;
                    }
                    case 742: {
                        if (Quadro03.isDependenteEmGuardaConjunta(titular) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(titular)) {
                            soma742_DGC+=importanciaAplicada;
                            break;
                        }
                        if ((!Quadro03.isSujeitoPassivoA(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03())) && (!Quadro03.isSujeitoPassivoB(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!Quadro03.isSujeitoPassivoC(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03()) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isConjugeFalecido(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro07().getQ07C1())) && (!Quadro03.isDependenteNaoDeficiente(titular) || !rosto.getQuadro03().existsInDependentesNaoDeficientes(titular)) && (!Quadro03.isDependenteDeficiente(titular) || !rosto.getQuadro03().existsInDependentesDeficientes(titular))) break;
                        soma742_Outros+=importanciaAplicada;
                        break;
                    }
                    case 743: {
                        soma743+=importanciaAplicada;
                    }
                }
            }
        }
        values.put("IdadSPA", idadeSpA);
        values.put("IdadSPB", idadeSpB);
        values.put("BenIVAServ", (double)soma712 / 100.0);
        values.put("AbatDonatPub", (double)soma713 / 100.0);
        values.put("AbatDonatOut", (double)soma714 / 100.0);
        values.put("AbatJurosImov", (double)soma731 / 100.0);
        values.put("AbatEnergRen", (double)soma738 / 100.0);
        values.put("ObrasMelhTermico", (double)soma739 / 100.0);
        values.put("VeiculosNaoPoluentes", (double)soma740 / 100.0);
        values.put("RendasLocFinan", (double)soma741 / 100.0);
        values.put("ContDefVelh", (double)soma743 / 100.0);
        values.put("AbatSegVida", (double)soma729 / 100.0);
        values.put("AbatDonatIgrCatolica", (double)soma715 / 100.0);
        values.put("AbatDonatRelig", (double)soma716 / 100.0);
        values.put("BenPPRPPE", (double)soma701_711 / 100.0);
        values.put("BenPPH", (double)soma702 / 100.0);
        values.put("BenOPVPubl", DOUBLE_ZERO);
        values.put("BenOPVTrab", DOUBLE_ZERO);
        values.put("BenPPA", (double)soma705 / 100.0);
        values.put("BenComput", (double)soma708 / 100.0);
        values.put("BenCooperati", (double)soma709 / 100.0);
        values.put("BenCooperado", (double)soma710 / 100.0);
        values.put("MeceCient", (double)soma717 / 100.0);
        values.put("MeceCult", (double)soma718 / 100.0);
        values.put("ContPluriA", (double)soma719 / 100.0);
        values.put("MeceSoci", (double)soma720 / 100.0);
        values.put("MeceSocEsp", (double)soma721 / 100.0);
        values.put("MeceFamil", (double)soma722 / 100.0);
        values.put("MeceSocInf", (double)soma723 / 100.0);
        values.put("EstMecCient", (double)soma724 / 100.0);
        values.put("EstMeceCult", (double)soma725 / 100.0);
        values.put("EstContPluriA", (double)soma726 / 100.0);
        values.put("EstMeceSoci", (double)soma727 / 100.0);
        values.put("EstMeceFamil", (double)soma728 / 100.0);
        values.put("ComCentRep", (double)soma734 / 100.0);
        values.put("ComCentRepPluri", (double)soma735 / 100.0);
        values.put("PPASPA", (double)soma705_SPA / 100.0);
        values.put("PPASPB", (double)soma705_SPB / 100.0);
        values.put("PPRSPA", (double)soma701_SPA / 100.0);
        values.put("PPRSPB", (double)soma701_SPB / 100.0);
        values.put("PPESPA", (double)soma711_SPA / 100.0);
        values.put("PPESPB", (double)soma711_SPB / 100.0);
        values.put("PPRSPF", (double)soma701_CF / 100.0);
        values.put("PPESPF", (double)soma711_CF / 100.0);
        values.put("PPRPPEidadeSPF", idadeSPF);
        values.put("AbatRendHabP", (double)soma732 / 100.0);
        values.put("RPCSPA", (double)soma733_SPA / 100.0);
        values.put("RPCSPB", (double)soma733_SPB / 100.0);
        values.put("RPCCF", (double)soma733_CF / 100.0);
        values.put("EncargosLaresGC", (double)soma737_DGC / 100.0);
        values.put("EncargosLaresOut", (double)(soma737_Outros + soma737_SPeCF) / 100.0);
        values.put("EncargosLaresSP", 0.0);
        values.put("PremiosSegRiSaudeGC", (double)soma730_DGC / 100.0);
        values.put("PremiosSegRiSaudeOut", (double)soma730_Outros / 100.0);
        values.put("NumDepSegRiSaudeGC", set730_DGC.size());
        values.put("NumDepSegRiSaudeOut", set730_D.size());
        values.put("ReabImoveisArrendGC", (double)soma736_DGC / 100.0);
        values.put("ReabImoveisArrendOut", (double)soma736_Outros / 100.0);
        values.put("DespEducSPDdefGC", (double)soma706_DGC / 100.0);
        values.put("DespEducSPDdefOut", (double)soma706_Outros / 100.0);
        values.put("PSegurosSPDdefGC", (double)soma742_DGC / 100.0);
        values.put("PSegurosSPDdefOut", (double)soma742_Outros / 100.0);
        values.put("AbatEncLares", (double)(soma737_DGC + soma737_Outros + soma737_SPeCF) / 100.0);
        values.put("AbatSegSaude", (double)(soma730_DGC + soma730_Outros) / 100.0);
        values.put("AbatEncarReabImoveis", (double)(soma736_DGC + soma736_Outros) / 100.0);
        values.put("BenDespEducD", (double)(soma706_DGC + soma706_Outros) / 100.0);
        values.put("BenSegurosD", (double)(soma742_DGC + soma742_Outros + soma707) / 100.0);
        long soma801_Outros = 0;
        long soma801_DeDD = 0;
        long soma801_DGC = 0;
        HashSet<String> set801_DeDD = new HashSet<String>();
        HashSet<String> set801_DGC = new HashSet<String>();
        long soma802_Outros = 0;
        long soma802_DeDD = 0;
        long soma802_DGC = 0;
        HashSet<String> set802_DeDD = new HashSet<String>();
        HashSet<String> set802_DGC = new HashSet<String>();
        long soma803_SP = 0;
        long soma803_Outros = 0;
        long soma803_DeDD = 0;
        long soma803_DGC = 0;
        HashSet<String> set803_DeDD = new HashSet<String>();
        HashSet<String> set803_DGC = new HashSet<String>();
        if (anexoH != null && anexoH.getQuadro08().getAnexoHq08T1() != null) {
            for (AnexoHq08T1_Linha anexoHq08T1Linha : anexoH.getQuadro08().getAnexoHq08T1()) {
                long c803;
                String titular = anexoHq08T1Linha.getNIF();
                long c801 = anexoHq08T1Linha.getAnexoHq08C801() != null ? anexoHq08T1Linha.getAnexoHq08C801() : 0;
                long c802 = anexoHq08T1Linha.getAnexoHq08C802() != null ? anexoHq08T1Linha.getAnexoHq08C802() : 0;
                long l = c803 = anexoHq08T1Linha.getAnexoHq08C803() != null ? anexoHq08T1Linha.getAnexoHq08C803() : 0;
                if (titular == null) continue;
                if (Quadro03.isDependenteDeficiente(titular) && rosto.getQuadro03().existsInDependentesDeficientes(titular) || Quadro03.isDependenteNaoDeficiente(titular) && rosto.getQuadro03().existsInDependentesNaoDeficientes(titular)) {
                    soma801_DeDD+=c801;
                    soma802_DeDD+=c802;
                    soma803_DeDD+=c803;
                    if (!(set801_DeDD.contains(titular) || c801 <= 0)) {
                        set801_DeDD.add(titular);
                    }
                    if (!(set802_DeDD.contains(titular) || c802 <= 0)) {
                        set802_DeDD.add(titular);
                    }
                    if (set803_DeDD.contains(titular) || c803 <= 0) continue;
                    set803_DeDD.add(titular);
                    continue;
                }
                if (Quadro03.isDependenteEmGuardaConjunta(titular) && rosto.getQuadro03().existsInDependentesEmGuardaConjunta(titular)) {
                    soma801_DGC+=c801;
                    soma802_DGC+=c802;
                    soma803_DGC+=c803;
                    if (!(set801_DGC.contains(titular) || c801 <= 0)) {
                        set801_DGC.add(titular);
                    }
                    if (!(set802_DGC.contains(titular) || c802 <= 0)) {
                        set802_DGC.add(titular);
                    }
                    if (set803_DGC.contains(titular) || c803 <= 0) continue;
                    set803_DGC.add(titular);
                    continue;
                }
                soma801_Outros+=c801;
                soma802_Outros+=c802;
                soma803_Outros+=c803;
                if ((!Quadro03.isSujeitoPassivoA(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03())) && (!Quadro03.isSujeitoPassivoB(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!Quadro03.isSujeitoPassivoC(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C03()) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro03().getQ03C04())) && (!pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isConjugeFalecido(titular) || Modelo3IRSValidatorUtil.isEmptyOrZero(rosto.getQuadro07().getQ07C1())) && (!pt.dgci.modelo3irs.v2015.model.rosto.Quadro07.isAfilhadosCivisEmComunhao(titular) || !rosto.getQuadro07().existsInAfilhadosCivisEmComunhao(titular))) continue;
                soma803_SP+=c803;
            }
        }
        values.put("DespesasEducSP", (double)soma803_SP / 100.0);
        values.put("NumDepDespEduc", set803_DeDD.size());
        values.put("DespesasEducDep", (double)soma803_DeDD / 100.0);
        values.put("NumDepGCDespEduc", set803_DGC.size());
        values.put("DespesasEducDepGC", (double)soma803_DGC / 100.0);
        values.put("NumDepDespSaude5", set801_DeDD.size());
        values.put("DespesasSaude5Dep", (double)soma801_DeDD / 100.0);
        values.put("NumDepGCDespSaude5", set801_DGC.size());
        values.put("DespesasSaude5GC", (double)soma801_DGC / 100.0);
        values.put("DespesasSaude5SP", (double)soma801_Outros / 100.0);
        values.put("NumDepDespSaudeOut", set802_DeDD.size());
        values.put("DespesasSaudeOutDep", (double)soma802_DeDD / 100.0);
        values.put("NumDepGCDespSaudeOut", set802_DGC.size());
        values.put("DespesasSaudeOutDepGC", (double)soma802_DGC / 100.0);
        values.put("DespesasSaudeOutSP", (double)soma802_Outros / 100.0);
        values.put("AbatDespSaude5", (double)(soma801_DeDD + soma801_DGC + soma801_Outros) / 100.0);
        values.put("AbatDespSaudOu", (double)(soma802_DeDD + soma802_DGC + soma802_Outros) / 100.0);
        values.put("AbatDespEducND", (double)(soma803_DeDD + soma803_DGC + soma803_Outros) / 100.0);
        values.put("AbatEquipComp", DOUBLE_ZERO);
        values.put("DEduSujPass", (double)soma803_SP / 100.0);
        values.put("DEduDepend", (double)(soma803_DeDD + soma803_DGC) / 100.0);
        values.put("NumDepDesp", set803_DeDD.size() + set803_DGC.size());
        boolean classifA = false;
        boolean classifReabImoveis = false;
        if (!(anexoH == null || anexoH.getQuadro08().getAnexoHq08T814().isEmpty())) {
            AnexoHq08T814_Linha linha814 = anexoH.getQuadro08().getAnexoHq08T814().get(0);
            if (!(linha814.getCodigo() == null && linha814.getFreguesia() == null && StringUtil.isEmpty(linha814.getTipoPredio()) && linha814.getArtigo() == null && StringUtil.isEmpty(linha814.getFraccao()) && StringUtil.isEmpty(linha814.getTitular()) && StringUtil.isEmpty(linha814.getHabitacaoPermanenteArrendada()) && linha814.getNIFArrendatario() == null)) {
                classifA = !StringUtil.isEmpty(linha814.getClassificacaoA()) && linha814.getClassificacaoA().equals("S");
                classifReabImoveis = linha814.getCodigo() != null && linha814.getCodigo() == 736;
            }
            for (int k = 1; k < anexoH.getQuadro08().getAnexoHq08T814().size(); ++k) {
                linha814 = anexoH.getQuadro08().getAnexoHq08T814().get(k);
                if (linha814.getCodigo() == null && linha814.getFreguesia() == null && StringUtil.isEmpty(linha814.getTipoPredio()) && linha814.getArtigo() == null && StringUtil.isEmpty(linha814.getFraccao()) && StringUtil.isEmpty(linha814.getTitular()) && StringUtil.isEmpty(linha814.getHabitacaoPermanenteArrendada()) && linha814.getNIFArrendatario() == null) continue;
                classifA = classifA && !StringUtil.isEmpty(linha814.getClassificacaoA()) && linha814.getClassificacaoA().equals("S");
                classifReabImoveis = classifReabImoveis && linha814.getCodigo() != null && linha814.getCodigo() == 736;
            }
        }
        values.put("ClassifA", classifA);
        values.put("ClassifReabImoveis", classifReabImoveis);
        values.put("AcrSegurosRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1001a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1001a().longValue() / 100.0 : 0.0);
        values.put("AcrSegurosCol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1001() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1001().longValue() / 100.0 : 0.0);
        values.put("AcrPPRRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1002a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1002a().longValue() / 100.0 : 0.0);
        values.put("AcrPPRCol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1002() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1002().longValue() / 100.0 : 0.0);
        values.put("AcrPPALRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1003a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1003a().longValue() / 100.0 : 0.0);
        values.put("AcrPPALCol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1003() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1003().longValue() / 100.0 : 0.0);
        values.put("AcrPPAIRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1004a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1004a().longValue() / 100.0 : 0.0);
        values.put("AcrPPAICol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1004() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1004().longValue() / 100.0 : 0.0);
        values.put("AcrPPHRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1005a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1005a().longValue() / 100.0 : 0.0);
        values.put("AcrPPHCol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1005() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1005().longValue() / 100.0 : 0.0);
        values.put("AcrCoopRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1006a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1006a().longValue() / 100.0 : 0.0);
        values.put("AcrCoopCol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1006() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1006().longValue() / 100.0 : 0.0);
        values.put("AcrInobsRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1007a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1007a().longValue() / 100.0 : 0.0);
        values.put("AcrInobsCol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1007() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1007().longValue() / 100.0 : 0.0);
        values.put("AcrCondomRend", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1008a() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1008a().longValue() / 100.0 : 0.0);
        values.put("AcrCondomCol", anexoH != null && anexoH.getQuadro10().getAnexoHq10C1008() != null ? (double)anexoH.getQuadro10().getAnexoHq10C1008().longValue() / 100.0 : 0.0);
    }

    private void doAnexoG(SimulatorHashtable<String, Object> values, AnexoGModel anexoG, AnexoFModel anexoF, Map<Long, Object> anexoFQ05) {
        int i;
        HashMap<Long, Object> anexoGQ04A = new HashMap<Long, Object>();
        if (anexoG != null) {
            for (AnexoGq04AT1_Linha anexoGq04AT1 : anexoG.getQuadro04().getAnexoGq04AT1()) {
                anexoGQ04A.put(anexoGq04AT1.getCamposQuadro4(), null);
            }
            if (anexoF != null) {
                for (AnexoFq04T1_Linha linha : anexoF.getQuadro04().getAnexoFq04T1()) {
                    if (anexoFQ05.containsKey(linha.getNLinha())) continue;
                    if (linha.getRendas() != null) {
                        this.somaRendas+=linha.getRendas().longValue();
                    }
                    if (linha.getDespesas() != null) {
                        this.somaDespesas+=linha.getDespesas().longValue();
                    }
                    if (linha.getRetencoes() == null) continue;
                    this.somaRetencoes+=linha.getRetencoes().longValue();
                }
            }
        }
        Iterator<AnexoGq04AT1_Linha> iterator = anexoG != null && anexoG.getQuadro04().getAnexoGq04T1() != null ? anexoG.getQuadro04().getAnexoGq04T1().iterator() : null;
        int nbensNotIn = 1;
        int nbensIn = 1;
        while (iterator != null && iterator.hasNext()) {
            AnexoGq04T1_Linha linhaT4 = (AnexoGq04T1_Linha)iterator.next();
            if (!anexoGQ04A.containsKey(linhaT4.getNLinha())) {
                values.put("DataReaBem" + nbensNotIn, linhaT4 != null && linhaT4.getAnoRealizacao() != null && linhaT4.getMesRealizacao() != null ? StringUtil.padZeros(linhaT4.getAnoRealizacao(), 4) + "-" + StringUtil.padZeros(linhaT4.getMesRealizacao(), 2) : "");
                values.put("DataAquBem" + nbensNotIn, linhaT4 != null && linhaT4.getAnoAquisicao() != null && linhaT4.getMesAquisicao() != null ? StringUtil.padZeros(linhaT4.getAnoAquisicao(), 4) + "-" + StringUtil.padZeros(linhaT4.getMesAquisicao(), 2) : "");
                values.put("ValReaBem" + nbensNotIn, linhaT4 != null && linhaT4.getValorRealizacao() != null ? (double)linhaT4.getValorRealizacao().longValue() / 100.0 : 0.0);
                values.put("ValAquBem" + nbensNotIn, linhaT4 != null && linhaT4.getValorAquisicao() != null ? (double)linhaT4.getValorAquisicao().longValue() / 100.0 : 0.0);
                values.put("ValEncBem" + nbensNotIn, linhaT4 != null && linhaT4.getDespesas() != null ? (double)linhaT4.getDespesas().longValue() / 100.0 : 0.0);
                nbensNotIn = (short)(nbensNotIn + 1);
                continue;
            }
            values.put("DataReaBemRec" + nbensIn, linhaT4 != null && linhaT4.getAnoRealizacao() != null && linhaT4.getMesRealizacao() != null ? StringUtil.padZeros(linhaT4.getAnoRealizacao(), 4) + "-" + StringUtil.padZeros(linhaT4.getMesRealizacao(), 2) : "");
            values.put("DataAquBemRec" + nbensIn, linhaT4 != null && linhaT4.getAnoAquisicao() != null && linhaT4.getMesAquisicao() != null ? StringUtil.padZeros(linhaT4.getAnoAquisicao(), 4) + "-" + StringUtil.padZeros(linhaT4.getMesAquisicao(), 2) : "");
            values.put("ValReaBemRec" + nbensIn, linhaT4 != null && linhaT4.getValorRealizacao() != null ? (double)linhaT4.getValorRealizacao().longValue() / 100.0 : 0.0);
            values.put("ValAquBemRec" + nbensIn, linhaT4 != null && linhaT4.getValorAquisicao() != null ? (double)linhaT4.getValorAquisicao().longValue() / 100.0 : 0.0);
            values.put("ValEncBemRec" + nbensIn, linhaT4 != null && linhaT4.getDespesas() != null ? (double)linhaT4.getDespesas().longValue() / 100.0 : 0.0);
            nbensIn = (short)(nbensIn + 1);
        }
        for (i = nbensNotIn; i < 5; ++i) {
            values.put("DataReaBem" + i, "");
            values.put("DataAquBem" + i, "");
            values.put("ValReaBem" + i, DOUBLE_ZERO);
            values.put("ValAquBem" + i, DOUBLE_ZERO);
            values.put("ValEncBem" + i, DOUBLE_ZERO);
        }
        for (i = nbensIn; i < 5; ++i) {
            values.put("DataReaBemRec" + i, "");
            values.put("DataAquBemRec" + i, "");
            values.put("ValReaBemRec" + i, DOUBLE_ZERO);
            values.put("ValAquBemRec" + i, DOUBLE_ZERO);
            values.put("ValEncBemRec" + i, DOUBLE_ZERO);
        }
        values.put("EnglobG", Character.valueOf((char)(anexoG != null && anexoG.getQuadro09().getAnexoGq09B1() != null && anexoG.getQuadro09().getAnexoGq09B1().equals("2") ? 48 : 49)));
        long soma_R = 0;
        long soma_A = 0;
        long soma_Desp = 0;
        AnexoGq04T1_LinhaBase linhaT4 = anexoG != null && anexoG.getQuadro04().getAnexoGq04T1() != null && anexoG.getQuadro04().getAnexoGq04T1().size() > 0 ? anexoG.getQuadro04().getAnexoGq04T1().get(0) : null;
        soma_R = linhaT4 != null && linhaT4.getValorRealizacao() != null ? linhaT4.getValorRealizacao() : 0;
        soma_A = linhaT4 != null && linhaT4.getValorAquisicao() != null ? linhaT4.getValorAquisicao() : 0;
        soma_Desp = linhaT4 != null && linhaT4.getDespesas() != null ? linhaT4.getDespesas() : 0;
        linhaT4 = anexoG != null && anexoG.getQuadro04().getAnexoGq04T1() != null && anexoG.getQuadro04().getAnexoGq04T1().size() > 1 ? anexoG.getQuadro04().getAnexoGq04T1().get(1) : null;
        soma_R+=linhaT4 != null && linhaT4.getValorRealizacao() != null ? linhaT4.getValorRealizacao() : 0;
        soma_A+=linhaT4 != null && linhaT4.getValorAquisicao() != null ? linhaT4.getValorAquisicao() : 0;
        soma_Desp+=linhaT4 != null && linhaT4.getDespesas() != null ? linhaT4.getDespesas() : 0;
        linhaT4 = anexoG != null && anexoG.getQuadro04().getAnexoGq04T1() != null && anexoG.getQuadro04().getAnexoGq04T1().size() > 2 ? anexoG.getQuadro04().getAnexoGq04T1().get(2) : null;
        soma_R+=linhaT4 != null && linhaT4.getValorRealizacao() != null ? linhaT4.getValorRealizacao() : 0;
        soma_A+=linhaT4 != null && linhaT4.getValorAquisicao() != null ? linhaT4.getValorAquisicao() : 0;
        soma_Desp+=linhaT4 != null && linhaT4.getDespesas() != null ? linhaT4.getDespesas() : 0;
        linhaT4 = anexoG != null && anexoG.getQuadro04().getAnexoGq04T1() != null && anexoG.getQuadro04().getAnexoGq04T1().size() > 3 ? anexoG.getQuadro04().getAnexoGq04T1().get(3) : null;
        values.put("MVTABImov", (double)(soma_A+=linhaT4 != null && linhaT4.getValorAquisicao() != null ? linhaT4.getValorAquisicao() : 0) / 100.0);
        values.put("MVTRBImov", (double)(soma_R+=linhaT4 != null && linhaT4.getValorRealizacao() != null ? linhaT4.getValorRealizacao() : 0) / 100.0);
        values.put("MVTDBImov", (double)(soma_Desp+=linhaT4 != null && linhaT4.getDespesas() != null ? linhaT4.getDespesas() : 0) / 100.0);
        values.put("MVTRPInte", anexoG != null && anexoG.getQuadro06().getAnexoGq06C1c() != null ? (double)anexoG.getQuadro06().getAnexoGq06C1c().longValue() / 100.0 : 0.0);
        values.put("MVTAPInte", anexoG != null && anexoG.getQuadro06().getAnexoGq06C1d() != null ? (double)anexoG.getQuadro06().getAnexoGq06C1d().longValue() / 100.0 : 0.0);
        values.put("MVTDPInte", anexoG != null && anexoG.getQuadro06().getAnexoGq06C1e() != null ? (double)anexoG.getQuadro06().getAnexoGq06C1e().longValue() / 100.0 : 0.0);
        values.put("MVTRPCont", anexoG != null && anexoG.getQuadro07().getAnexoGq07C1c() != null ? (double)anexoG.getQuadro07().getAnexoGq07C1c().longValue() / 100.0 : 0.0);
        values.put("MVTAPCont", anexoG != null && anexoG.getQuadro07().getAnexoGq07C1d() != null ? (double)anexoG.getQuadro07().getAnexoGq07C1d().longValue() / 100.0 : 0.0);
        long somaD = 0;
        long somaA = 0;
        long somaR = 0;
        if (anexoG != null) {
            for (AnexoGq08T1_Linha linha : anexoG.getQuadro08().getAnexoGq08T1()) {
                if (linha.getValorRealizacao() != null) {
                    somaR+=linha.getValorRealizacao().longValue();
                }
                if (linha.getValorAquisicao() != null) {
                    somaA+=linha.getValorAquisicao().longValue();
                }
                if (linha.getDespesas() == null) continue;
                somaD+=linha.getDespesas().longValue();
            }
        }
        values.put("MVTRPartS", (double)somaR / 100.0);
        values.put("MVTAPartS", (double)somaA / 100.0);
        values.put("MVTDPartS", (double)somaD / 100.0);
        long soma = 0;
        if (anexoG != null && anexoG.getQuadro09().getAnexoGq09C901b() != null) {
            soma = anexoG.getQuadro09().getAnexoGq09C901b();
        }
        if (anexoG != null && anexoG.getQuadro09().getAnexoGq09C902b() != null) {
            soma+=anexoG.getQuadro09().getAnexoGq09C902b().longValue();
        }
        if (anexoG != null && anexoG.getQuadro09().getAnexoGq09C903b() != null) {
            soma+=anexoG.getQuadro09().getAnexoGq09C903b().longValue();
        }
        values.put("MVTFinanD", (double)soma / 100.0);
        values.put("OFoutros", anexoG != null && anexoG.getQuadro09().getAnexoGq09C901b() != null ? (double)anexoG.getQuadro09().getAnexoGq09C901b().longValue() / 100.0 : 0.0);
        values.put("OFi4", anexoG != null && anexoG.getQuadro09().getAnexoGq09C902b() != null ? (double)anexoG.getQuadro09().getAnexoGq09C902b().longValue() / 100.0 : 0.0);
        values.put("OFcertif", anexoG != null && anexoG.getQuadro09().getAnexoGq09C903b() != null ? (double)anexoG.getQuadro09().getAnexoGq09C903b().longValue() / 100.0 : 0.0);
        values.put("IndemDanos", anexoG != null && anexoG.getQuadro10().getAnexoGq10C1001b() != null ? (double)anexoG.getQuadro10().getAnexoGq10C1001b().longValue() / 100.0 : 0.0);
        values.put("IndemDanosRF", anexoG != null && anexoG.getQuadro10().getAnexoGq10C1001c() != null ? (double)anexoG.getQuadro10().getAnexoGq10C1001c().longValue() / 100.0 : 0.0);
        values.put("ImpNConc", anexoG != null && anexoG.getQuadro10().getAnexoGq10C1002b() != null ? (double)anexoG.getQuadro10().getAnexoGq10C1002b().longValue() / 100.0 : 0.0);
        values.put("ImpNConcRF", anexoG != null && anexoG.getQuadro10().getAnexoGq10C1002c() != null ? (double)anexoG.getQuadro10().getAnexoGq10C1002c().longValue() / 100.0 : 0.0);
        values.put("MVTIncPat", anexoG != null && anexoG.getQuadro10().getAnexoGq10C1b() != null ? (double)anexoG.getQuadro10().getAnexoGq10C1b().longValue() / 100.0 : 0.0);
        values.put("MVTIncPatRF", anexoG != null && anexoG.getQuadro10().getAnexoGq10C1c() != null ? (double)anexoG.getQuadro10().getAnexoGq10C1c().longValue() / 100.0 : 0.0);
        values.put("DividEmp", anexoG != null && anexoG.getQuadro05().getAnexoGq05C505() != null ? (double)anexoG.getQuadro05().getAnexoGq05C505().longValue() / 100.0 : 0.0);
        values.put("ReinvParc", anexoG != null && anexoG.getQuadro05().getAnexoGq05C506() != null ? (double)anexoG.getQuadro05().getAnexoGq05C506().longValue() / 100.0 : 0.0);
        if (anexoG != null && anexoG.getQuadro04().getAnexoGq04B1OP() != null) {
            values.put("EnglobImvG", Character.valueOf(anexoG.getQuadro04().getAnexoGq04B1OP().equals("S") ? '1' : '0'));
        } else {
            values.put("EnglobImvG", Character.valueOf('0'));
        }
        values.put("NumBemAlienado", String.valueOf(anexoG != null && anexoG.getQuadro05().getAnexoGq05C502() != null ? anexoG.getQuadro04().getSimulatorLineIndexByNLinha(anexoG.getQuadro05().getAnexoGq05C502()) : 0));
    }

    private Map<Long, Object> doAnexoF(SimulatorHashtable<String, Object> values, AnexoFModel anexoF) {
        long somaRendasImoveis = 0;
        long somaDespesasImoveis = 0;
        long somaRetencoesImoveis = 0;
        long somaRendaRecebida_Q6 = 0;
        long somaRetencaoIRS_Q6 = 0;
        long somaRendaPaga_Q6 = 0;
        long somaRendimentosQ7 = 0;
        int somaNAnosQ7 = 0;
        HashMap<Long, Object> anexoFQ05 = new HashMap<Long, Object>();
        if (anexoF != null) {
            for (AnexoFq05T1_Linha anexoFq05T1Linha : anexoF.getQuadro05().getAnexoFq05T1()) {
                anexoFQ05.put(anexoFq05T1Linha.getCamposQuadro4(), null);
            }
            for (AnexoFq04T1_Linha linha : anexoF.getQuadro04().getAnexoFq04T1()) {
                if (!anexoFQ05.containsKey(linha.getNLinha())) {
                    if (linha.getRendas() != null) {
                        this.somaRendas+=linha.getRendas().longValue();
                    }
                    if (linha.getDespesas() != null) {
                        this.somaDespesas+=linha.getDespesas().longValue();
                    }
                    if (linha.getRetencoes() == null) continue;
                    this.somaRetencoes+=linha.getRetencoes().longValue();
                    continue;
                }
                if (linha.getRendas() != null) {
                    somaRendasImoveis+=linha.getRendas().longValue();
                }
                if (linha.getDespesas() != null) {
                    somaDespesasImoveis+=linha.getDespesas().longValue();
                }
                if (linha.getRetencoes() == null) continue;
                somaRetencoesImoveis+=linha.getRetencoes().longValue();
            }
            EventList<AnexoFq06T1_Linha> anexoFq06T1Linhas = anexoF.getQuadro06().getAnexoFq06T1();
            for (AnexoFq06T1_Linha anexoFq06T1Linha : anexoFq06T1Linhas) {
                if (anexoFq06T1Linha.getRendaRecebida() != null) {
                    somaRendaRecebida_Q6+=anexoFq06T1Linha.getRendaRecebida().longValue();
                }
                if (anexoFq06T1Linha.getRetencoes() != null) {
                    somaRetencaoIRS_Q6+=anexoFq06T1Linha.getRetencoes().longValue();
                }
                if (anexoFq06T1Linha.getRendaPaga() == null) continue;
                somaRendaPaga_Q6+=anexoFq06T1Linha.getRendaPaga().longValue();
            }
            EventList<AnexoFq07T1_Linha> anexoFq07T1Linhas = anexoF.getQuadro07().getAnexoFq07T1();
            for (AnexoFq07T1_Linha anexoFq07T1Linha : anexoFq07T1Linhas) {
                if (anexoFq07T1Linha.getRendimento() != null) {
                    somaRendimentosQ7+=anexoFq07T1Linha.getRendimento().longValue();
                }
                if (anexoFq07T1Linha.getNanos() == null) continue;
                somaNAnosQ7 = (int)((long)somaNAnosQ7 + anexoFq07T1Linha.getNanos());
            }
        }
        values.put("PredTotRend", (double)this.somaRendas / 100.0);
        values.put("PredManut", Double.valueOf(this.somaDespesas) / 100.0);
        values.put("PredConserv", DOUBLE_ZERO);
        values.put("TaxasAutarq", DOUBLE_ZERO);
        values.put("PredCAutarq", DOUBLE_ZERO);
        values.put("PredCondomi", DOUBLE_ZERO);
        values.put("PredRF", (double)(this.somaRetencoes + somaRetencaoIRS_Q6) / 100.0);
        values.put("RendAnosAntcatF", Double.valueOf(somaRendimentosQ7) / 100.0);
        values.put("NAnoscatF", somaNAnosQ7);
        if (anexoF != null && anexoF.getQuadro05().getAnexoFq05B3OP() != null) {
            values.put("OpcaoEnglobCatF", anexoF.getQuadro05().getAnexoFq05B3OP().equals("S"));
        } else {
            values.put("OpcaoEnglobCatF", false);
        }
        values.put("PredPerdas", DOUBLE_ZERO);
        values.put("RendRecebSub", (double)somaRendaRecebida_Q6 / 100.0);
        values.put("RendPagaSenhor", (double)somaRendaPaga_Q6 / 100.0);
        values.put("TotalRendasImv", (double)somaRendasImoveis / 100.0);
        values.put("DespManutencaoImv", Double.valueOf(somaDespesasImoveis) / 100.0);
        values.put("DespConservacaoImv", DOUBLE_ZERO);
        values.put("TaxasAutarqImv", DOUBLE_ZERO);
        values.put("ContribAutarqImv", DOUBLE_ZERO);
        values.put("DespCondominioImv", DOUBLE_ZERO);
        values.put("RetFonteFImv", Double.valueOf(somaRetencoesImoveis) / 100.0);
        if (anexoF != null && anexoF.getQuadro05().getAnexoFq05B3OP() != null) {
            values.put("EnglobamentoF", Character.valueOf(anexoF.getQuadro05().getAnexoFq05B3OP().equals("S") ? '1' : '0'));
        } else {
            values.put("EnglobamentoF", Character.valueOf('0'));
        }
        return anexoFQ05;
    }

    private void doAnexoE(SimulatorHashtable<String, Object> values, AnexoEModel anexoE) {
        long somaRendA = 0;
        long somaRendB = 0;
        long somaRetA = 0;
        long somaRetB = 0;
        long somaRendE6 = 0;
        if (anexoE != null && anexoE.getQuadro04().getAnexoEq04T1() != null) {
            for (AnexoEq04T1_Linha line : anexoE.getQuadro04().getAnexoEq04T1()) {
                if (line.getRendimentos() != null) {
                    somaRendA+=line.getRendimentos().longValue();
                }
                if (line.getRetencoes() == null) continue;
                somaRetA+=line.getRetencoes().longValue();
            }
        }
        if (anexoE != null && anexoE.getQuadro04().getAnexoEq04T2() != null) {
            for (AnexoEq04T2_Linha line : anexoE.getQuadro04().getAnexoEq04T2()) {
                if (line.getRendimentos() != null) {
                    somaRendB+=line.getRendimentos().longValue();
                }
                if (line.getRetencoes() != null) {
                    somaRetB+=line.getRetencoes().longValue();
                }
                if (anexoE.getQuadro04().getAnexoEq04B1() == null || !anexoE.getQuadro04().getAnexoEq04B1().equals("2") || !"E6".equals(line.getCodRendimentos()) || line.getRendimentos() == null) continue;
                somaRendE6+=line.getRendimentos().longValue();
            }
        }
        values.put("CapJurDep", DOUBLE_ZERO);
        values.put("CapJurPrem", DOUBLE_ZERO);
        values.put("CapJurSupr", DOUBLE_ZERO);
        values.put("CapLucros", DOUBLE_ZERO);
        values.put("CapResgPPA", DOUBLE_ZERO);
        values.put("CapResgFPR", DOUBLE_ZERO);
        values.put("CapOutCap1", (double)somaRendA / 100.0);
        values.put("CapOutCap2", (double)somaRendB / 100.0);
        values.put("CapRF", (double)(somaRetA + somaRetB) / 100.0);
        values.put("RendFundCapRisco", (double)somaRendE6 / 100.0);
    }

    private void doAnexoD(SimulatorHashtable<String, Object> values, RostoModel rosto, long nifD1, long nifD2, long nifD3) {
        long nif = rosto.getQuadro03().getQ03C03() != null ? rosto.getQuadro03().getQ03C03() : 0;
        AnexoDModel anexoD = nif != 0 ? (AnexoDModel)this.model.getAnexo(new FormKey(AnexoDModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendDSPA", Character.valueOf('2'));
        values.put("AgriVSProfDSPA", anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejMatColProDSPA = 0;
        long lucroPrejMatColAgrDSPA = 0;
        long lucroPrejRetFonteDSPA = 0;
        long ajustamentosAgrSPA = 0;
        long ajustamentosProSPA = 0;
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDSPA = (anexoD.getQuadro05().getAnexoDq05C501() != null ? anexoD.getQuadro05().getAnexoDq05C501() : 0) + (anexoD.getQuadro05().getAnexoDq05C503() != null ? anexoD.getQuadro05().getAnexoDq05C503() : 0) - (anexoD.getQuadro05().getAnexoDq05C502() != null ? anexoD.getQuadro05().getAnexoDq05C502() : 0);
            lucroPrejMatColAgrDSPA = (anexoD.getQuadro05().getAnexoDq05C504() != null ? anexoD.getQuadro05().getAnexoDq05C504() : 0) + (anexoD.getQuadro05().getAnexoDq05C506() != null ? anexoD.getQuadro05().getAnexoDq05C506() : 0) - (anexoD.getQuadro05().getAnexoDq05C505() != null ? anexoD.getQuadro05().getAnexoDq05C505() : 0);
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            lucroPrejMatColAgrDSPA+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColAgrDSPA+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColAgrDSPA+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColAgrDSPA+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColAgrDSPA+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColAgrDSPA+=linha.getRendimentos().longValue();
            }
            ajustamentosAgrSPA+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosAgrSPA+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosAgrSPA+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosAgrSPA+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosAgrSPA+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDSPA+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColProDSPA+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColProDSPA+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColProDSPA+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColProDSPA+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColProDSPA+=linha.getRendimentos().longValue();
            }
            ajustamentosProSPA+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosProSPA+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosProSPA+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosProSPA+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosProSPA+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        }
        if (anexoD != null) {
            lucroPrejRetFonteDSPA+=anexoD.getQuadro04().getAnexoDq04C401c() != null ? anexoD.getQuadro04().getAnexoDq04C401c() : 0;
            lucroPrejRetFonteDSPA+=anexoD.getQuadro04().getAnexoDq04C402c() != null ? anexoD.getQuadro04().getAnexoDq04C402c() : 0;
            lucroPrejRetFonteDSPA+=anexoD.getQuadro04().getAnexoDq04C403c() != null ? anexoD.getQuadro04().getAnexoDq04C403c() : 0;
            lucroPrejRetFonteDSPA+=anexoD.getQuadro04().getAnexoDq04C431c() != null ? anexoD.getQuadro04().getAnexoDq04C431c() : 0;
            lucroPrejRetFonteDSPA+=anexoD.getQuadro04().getAnexoDq04C432c() != null ? anexoD.getQuadro04().getAnexoDq04C432c() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRetencao() == null) continue;
                lucroPrejRetFonteDSPA+=linha.getRetencao().longValue();
            }
        }
        values.put("LucroPrejProDSPA", (double)lucroPrejMatColProDSPA / 100.0);
        values.put("LucroPrejAgrDSPA", (double)lucroPrejMatColAgrDSPA / 100.0);
        values.put("RetFonteDSPA", (double)lucroPrejRetFonteDSPA / 100.0);
        values.put("AjustamentosPSPA", (double)ajustamentosProSPA / 100.0);
        values.put("AjustamentosASPA", (double)ajustamentosAgrSPA / 100.0);
        values.put("PagamenContaDSPA", anexoD != null && anexoD.getQuadro06().getAnexoDq06C601() != null ? (double)anexoD.getQuadro06().getAnexoDq06C601().longValue() / 100.0 : 0.0);
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            values.put("RendLiqImpPSPA", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosPSPA", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpPSPA", DOUBLE_ZERO);
            values.put("AdContLucrosPSPA", DOUBLE_ZERO);
        }
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            values.put("RendLiqImpASPA", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosASPA", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpASPA", DOUBLE_ZERO);
            values.put("AdContLucrosASPA", DOUBLE_ZERO);
        }
        nif = rosto.getQuadro03().getQ03C04() != null ? rosto.getQuadro03().getQ03C04() : 0;
        anexoD = nif != 0 ? (AnexoDModel)this.model.getAnexo(new FormKey(AnexoDModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendDSPB", Character.valueOf('2'));
        values.put("AgriVSProfDSPB", anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejMatColProDSPB = 0;
        long lucroPrejMatColAgrDSPB = 0;
        long lucroPrejRetFonteDSPB = 0;
        long ajustamentosAgrSPB = 0;
        long ajustamentosProSPB = 0;
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDSPB = (anexoD.getQuadro05().getAnexoDq05C501() != null ? anexoD.getQuadro05().getAnexoDq05C501() : 0) + (anexoD.getQuadro05().getAnexoDq05C503() != null ? anexoD.getQuadro05().getAnexoDq05C503() : 0) - (anexoD.getQuadro05().getAnexoDq05C502() != null ? anexoD.getQuadro05().getAnexoDq05C502() : 0);
            lucroPrejMatColAgrDSPB = (anexoD.getQuadro05().getAnexoDq05C504() != null ? anexoD.getQuadro05().getAnexoDq05C504() : 0) + (anexoD.getQuadro05().getAnexoDq05C506() != null ? anexoD.getQuadro05().getAnexoDq05C506() : 0) - (anexoD.getQuadro05().getAnexoDq05C505() != null ? anexoD.getQuadro05().getAnexoDq05C505() : 0);
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            lucroPrejMatColAgrDSPB+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColAgrDSPB+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColAgrDSPB+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColAgrDSPB+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColAgrDSPB+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColAgrDSPB+=linha.getRendimentos().longValue();
            }
            ajustamentosAgrSPB+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosAgrSPB+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosAgrSPB+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosAgrSPB+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosAgrSPB+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDSPB+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColProDSPB+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColProDSPB+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColProDSPB+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColProDSPB+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColProDSPB+=linha.getRendimentos().longValue();
            }
            ajustamentosProSPB+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosProSPB+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosProSPB+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosProSPB+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosProSPB+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        }
        if (anexoD != null) {
            lucroPrejRetFonteDSPB+=anexoD.getQuadro04().getAnexoDq04C401c() != null ? anexoD.getQuadro04().getAnexoDq04C401c() : 0;
            lucroPrejRetFonteDSPB+=anexoD.getQuadro04().getAnexoDq04C402c() != null ? anexoD.getQuadro04().getAnexoDq04C402c() : 0;
            lucroPrejRetFonteDSPB+=anexoD.getQuadro04().getAnexoDq04C403c() != null ? anexoD.getQuadro04().getAnexoDq04C403c() : 0;
            lucroPrejRetFonteDSPB+=anexoD.getQuadro04().getAnexoDq04C431c() != null ? anexoD.getQuadro04().getAnexoDq04C431c() : 0;
            lucroPrejRetFonteDSPB+=anexoD.getQuadro04().getAnexoDq04C432c() != null ? anexoD.getQuadro04().getAnexoDq04C432c() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRetencao() == null) continue;
                lucroPrejRetFonteDSPB+=linha.getRetencao().longValue();
            }
        }
        values.put("LucroPrejProDSPB", (double)lucroPrejMatColProDSPB / 100.0);
        values.put("LucroPrejAgrDSPB", (double)lucroPrejMatColAgrDSPB / 100.0);
        values.put("RetFonteDSPB", (double)lucroPrejRetFonteDSPB / 100.0);
        values.put("AjustamentosPSPB", (double)ajustamentosProSPB / 100.0);
        values.put("AjustamentosASPB", (double)ajustamentosAgrSPB / 100.0);
        values.put("PagamenContaDSPB", anexoD != null && anexoD.getQuadro06().getAnexoDq06C601() != null ? (double)anexoD.getQuadro06().getAnexoDq06C601().longValue() / 100.0 : 0.0);
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            values.put("RendLiqImpPSPB", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosPSPB", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpPSPB", DOUBLE_ZERO);
            values.put("AdContLucrosPSPB", DOUBLE_ZERO);
        }
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            values.put("RendLiqImpASPB", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosASPB", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpASPB", DOUBLE_ZERO);
            values.put("AdContLucrosASPB", DOUBLE_ZERO);
        }
        nif = nifD1;
        anexoD = nif != 0 ? (AnexoDModel)this.model.getAnexo(new FormKey(AnexoDModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendDD1", Character.valueOf('2'));
        values.put("AgriVSProfDD1", anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejMatColProDD1 = 0;
        long lucroPrejMatColAgrDD1 = 0;
        long lucroPrejRetFonteDD1 = 0;
        long ajustamentosAgrD1 = 0;
        long ajustamentosProD1 = 0;
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDD1 = (anexoD.getQuadro05().getAnexoDq05C501() != null ? anexoD.getQuadro05().getAnexoDq05C501() : 0) + (anexoD.getQuadro05().getAnexoDq05C503() != null ? anexoD.getQuadro05().getAnexoDq05C503() : 0) - (anexoD.getQuadro05().getAnexoDq05C502() != null ? anexoD.getQuadro05().getAnexoDq05C502() : 0);
            lucroPrejMatColAgrDD1 = (anexoD.getQuadro05().getAnexoDq05C504() != null ? anexoD.getQuadro05().getAnexoDq05C504() : 0) + (anexoD.getQuadro05().getAnexoDq05C506() != null ? anexoD.getQuadro05().getAnexoDq05C506() : 0) - (anexoD.getQuadro05().getAnexoDq05C505() != null ? anexoD.getQuadro05().getAnexoDq05C505() : 0);
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            lucroPrejMatColAgrDD1+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColAgrDD1+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColAgrDD1+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColAgrDD1+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColAgrDD1+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColAgrDD1+=linha.getRendimentos().longValue();
            }
            ajustamentosAgrD1+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosAgrD1+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosAgrD1+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosAgrD1+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosAgrD1+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDD1+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColProDD1+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColProDD1+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColProDD1+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColProDD1+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColProDD1+=linha.getRendimentos().longValue();
            }
            ajustamentosProD1+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosProD1+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosProD1+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosProD1+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosProD1+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        }
        if (anexoD != null) {
            lucroPrejRetFonteDD1+=anexoD.getQuadro04().getAnexoDq04C401c() != null ? anexoD.getQuadro04().getAnexoDq04C401c() : 0;
            lucroPrejRetFonteDD1+=anexoD.getQuadro04().getAnexoDq04C402c() != null ? anexoD.getQuadro04().getAnexoDq04C402c() : 0;
            lucroPrejRetFonteDD1+=anexoD.getQuadro04().getAnexoDq04C403c() != null ? anexoD.getQuadro04().getAnexoDq04C403c() : 0;
            lucroPrejRetFonteDD1+=anexoD.getQuadro04().getAnexoDq04C431c() != null ? anexoD.getQuadro04().getAnexoDq04C431c() : 0;
            lucroPrejRetFonteDD1+=anexoD.getQuadro04().getAnexoDq04C432c() != null ? anexoD.getQuadro04().getAnexoDq04C432c() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRetencao() == null) continue;
                lucroPrejRetFonteDD1+=linha.getRetencao().longValue();
            }
        }
        values.put("LucroPrejProDD1", (double)lucroPrejMatColProDD1 / 100.0);
        values.put("LucroPrejAgrDD1", (double)lucroPrejMatColAgrDD1 / 100.0);
        values.put("RetFonteDD1", (double)lucroPrejRetFonteDD1 / 100.0);
        values.put("AjustamentosPD1", (double)ajustamentosProD1 / 100.0);
        values.put("AjustamentosAD1", (double)ajustamentosAgrD1 / 100.0);
        values.put("PagamenContaDD1", anexoD != null && anexoD.getQuadro06().getAnexoDq06C601() != null ? (double)anexoD.getQuadro06().getAnexoDq06C601().longValue() / 100.0 : 0.0);
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            values.put("RendLiqImpPD1", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosPD1", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpPD1", DOUBLE_ZERO);
            values.put("AdContLucrosPD1", DOUBLE_ZERO);
        }
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            values.put("RendLiqImpAD1", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosAD1", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpAD1", DOUBLE_ZERO);
            values.put("AdContLucrosAD1", DOUBLE_ZERO);
        }
        nif = nifD2;
        anexoD = nif != 0 ? (AnexoDModel)this.model.getAnexo(new FormKey(AnexoDModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendDD2", Character.valueOf('2'));
        values.put("AgriVSProfDD2", anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejMatColProDD2 = 0;
        long lucroPrejMatColAgrDD2 = 0;
        long lucroPrejRetFonteDD2 = 0;
        long ajustamentosAgrD2 = 0;
        long ajustamentosProD2 = 0;
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDD2 = (anexoD.getQuadro05().getAnexoDq05C501() != null ? anexoD.getQuadro05().getAnexoDq05C501() : 0) + (anexoD.getQuadro05().getAnexoDq05C503() != null ? anexoD.getQuadro05().getAnexoDq05C503() : 0) - (anexoD.getQuadro05().getAnexoDq05C502() != null ? anexoD.getQuadro05().getAnexoDq05C502() : 0);
            lucroPrejMatColAgrDD2 = (anexoD.getQuadro05().getAnexoDq05C504() != null ? anexoD.getQuadro05().getAnexoDq05C504() : 0) + (anexoD.getQuadro05().getAnexoDq05C506() != null ? anexoD.getQuadro05().getAnexoDq05C506() : 0) - (anexoD.getQuadro05().getAnexoDq05C505() != null ? anexoD.getQuadro05().getAnexoDq05C505() : 0);
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            lucroPrejMatColAgrDD2+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColAgrDD2+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColAgrDD2+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColAgrDD2+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColAgrDD2+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColAgrDD2+=linha.getRendimentos().longValue();
            }
            ajustamentosAgrD2+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosAgrD2+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosAgrD2+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosAgrD2+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosAgrD2+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDD2+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColProDD2+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColProDD2+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColProDD2+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColProDD2+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColProDD2+=linha.getRendimentos().longValue();
            }
            ajustamentosProD2+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosProD2+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosProD2+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosProD2+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosProD2+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        }
        if (anexoD != null) {
            lucroPrejRetFonteDD2+=anexoD.getQuadro04().getAnexoDq04C401c() != null ? anexoD.getQuadro04().getAnexoDq04C401c() : 0;
            lucroPrejRetFonteDD2+=anexoD.getQuadro04().getAnexoDq04C402c() != null ? anexoD.getQuadro04().getAnexoDq04C402c() : 0;
            lucroPrejRetFonteDD2+=anexoD.getQuadro04().getAnexoDq04C403c() != null ? anexoD.getQuadro04().getAnexoDq04C403c() : 0;
            lucroPrejRetFonteDD2+=anexoD.getQuadro04().getAnexoDq04C431c() != null ? anexoD.getQuadro04().getAnexoDq04C431c() : 0;
            lucroPrejRetFonteDD2+=anexoD.getQuadro04().getAnexoDq04C432c() != null ? anexoD.getQuadro04().getAnexoDq04C432c() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRetencao() == null) continue;
                lucroPrejRetFonteDD2+=linha.getRetencao().longValue();
            }
        }
        values.put("LucroPrejProDD2", (double)lucroPrejMatColProDD2 / 100.0);
        values.put("LucroPrejAgrDD2", (double)lucroPrejMatColAgrDD2 / 100.0);
        values.put("RetFonteDD2", (double)lucroPrejRetFonteDD2 / 100.0);
        values.put("AjustamentosPD2", (double)ajustamentosProD2 / 100.0);
        values.put("AjustamentosAD2", (double)ajustamentosAgrD2 / 100.0);
        values.put("PagamenContaDD2", anexoD != null && anexoD.getQuadro06().getAnexoDq06C601() != null ? (double)anexoD.getQuadro06().getAnexoDq06C601().longValue() / 100.0 : 0.0);
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            values.put("RendLiqImpPD2", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosPD2", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpPD2", DOUBLE_ZERO);
            values.put("AdContLucrosPD2", DOUBLE_ZERO);
        }
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            values.put("RendLiqImpAD2", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosAD2", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpAD2", DOUBLE_ZERO);
            values.put("AdContLucrosAD2", DOUBLE_ZERO);
        }
        nif = nifD3;
        anexoD = nif != 0 ? (AnexoDModel)this.model.getAnexo(new FormKey(AnexoDModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendDD3", Character.valueOf('2'));
        values.put("AgriVSProfDD3", anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejMatColProDD3 = 0;
        long lucroPrejMatColAgrDD3 = 0;
        long lucroPrejRetFonteDD3 = 0;
        long ajustamentosAgrD3 = 0;
        long ajustamentosProD3 = 0;
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected() && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDD3 = (anexoD.getQuadro05().getAnexoDq05C501() != null ? anexoD.getQuadro05().getAnexoDq05C501() : 0) + (anexoD.getQuadro05().getAnexoDq05C503() != null ? anexoD.getQuadro05().getAnexoDq05C503() : 0) - (anexoD.getQuadro05().getAnexoDq05C502() != null ? anexoD.getQuadro05().getAnexoDq05C502() : 0);
            lucroPrejMatColAgrDD3 = (anexoD.getQuadro05().getAnexoDq05C504() != null ? anexoD.getQuadro05().getAnexoDq05C504() : 0) + (anexoD.getQuadro05().getAnexoDq05C506() != null ? anexoD.getQuadro05().getAnexoDq05C506() : 0) - (anexoD.getQuadro05().getAnexoDq05C505() != null ? anexoD.getQuadro05().getAnexoDq05C505() : 0);
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            lucroPrejMatColAgrDD3+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColAgrDD3+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColAgrDD3+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColAgrDD3+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColAgrDD3+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColAgrDD3+=linha.getRendimentos().longValue();
            }
            ajustamentosAgrD3+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosAgrD3+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosAgrD3+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosAgrD3+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosAgrD3+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        } else if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            lucroPrejMatColProDD3+=anexoD.getQuadro04().getAnexoDq04C401b() != null ? anexoD.getQuadro04().getAnexoDq04C401b() : 0;
            lucroPrejMatColProDD3+=anexoD.getQuadro04().getAnexoDq04C402b() != null ? anexoD.getQuadro04().getAnexoDq04C402b() : 0;
            lucroPrejMatColProDD3+=anexoD.getQuadro04().getAnexoDq04C403b() != null ? anexoD.getQuadro04().getAnexoDq04C403b() : 0;
            lucroPrejMatColProDD3+=anexoD.getQuadro04().getAnexoDq04C431b() != null ? anexoD.getQuadro04().getAnexoDq04C431b() : 0;
            lucroPrejMatColProDD3+=anexoD.getQuadro04().getAnexoDq04C432b() != null ? anexoD.getQuadro04().getAnexoDq04C432b() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRendimentos() == null) continue;
                lucroPrejMatColProDD3+=linha.getRendimentos().longValue();
            }
            ajustamentosProD3+=anexoD.getQuadro04().getAnexoDq04C401e() != null ? anexoD.getQuadro04().getAnexoDq04C401e() : 0;
            ajustamentosProD3+=anexoD.getQuadro04().getAnexoDq04C402e() != null ? anexoD.getQuadro04().getAnexoDq04C402e() : 0;
            ajustamentosProD3+=anexoD.getQuadro04().getAnexoDq04C403e() != null ? anexoD.getQuadro04().getAnexoDq04C403e() : 0;
            ajustamentosProD3+=anexoD.getQuadro04().getAnexoDq04C431e() != null ? anexoD.getQuadro04().getAnexoDq04C431e() : 0;
            ajustamentosProD3+=anexoD.getQuadro04().getAnexoDq04C432e() != null ? anexoD.getQuadro04().getAnexoDq04C432e() : 0;
        }
        if (anexoD != null) {
            lucroPrejRetFonteDD3+=anexoD.getQuadro04().getAnexoDq04C401c() != null ? anexoD.getQuadro04().getAnexoDq04C401c() : 0;
            lucroPrejRetFonteDD3+=anexoD.getQuadro04().getAnexoDq04C402c() != null ? anexoD.getQuadro04().getAnexoDq04C402c() : 0;
            lucroPrejRetFonteDD3+=anexoD.getQuadro04().getAnexoDq04C403c() != null ? anexoD.getQuadro04().getAnexoDq04C403c() : 0;
            lucroPrejRetFonteDD3+=anexoD.getQuadro04().getAnexoDq04C431c() != null ? anexoD.getQuadro04().getAnexoDq04C431c() : 0;
            lucroPrejRetFonteDD3+=anexoD.getQuadro04().getAnexoDq04C432c() != null ? anexoD.getQuadro04().getAnexoDq04C432c() : 0;
            for (AnexoDq04T1_Linha linha : anexoD.getQuadro04().getAnexoDq04T1()) {
                if (linha.getRetencao() == null) continue;
                lucroPrejRetFonteDD3+=linha.getRetencao().longValue();
            }
        }
        values.put("LucroPrejProDD3", (double)lucroPrejMatColProDD3 / 100.0);
        values.put("LucroPrejAgrDD3", (double)lucroPrejMatColAgrDD3 / 100.0);
        values.put("RetFonteDD3", (double)lucroPrejRetFonteDD3 / 100.0);
        values.put("AjustamentosPD3", (double)ajustamentosProD3 / 100.0);
        values.put("AjustamentosAD3", (double)ajustamentosAgrD3 / 100.0);
        values.put("PagamenContaDD3", anexoD != null && anexoD.getQuadro06().getAnexoDq06C601() != null ? (double)anexoD.getQuadro06().getAnexoDq06C601().longValue() / 100.0 : 0.0);
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B1Selected()) {
            values.put("RendLiqImpPD3", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosPD3", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpPD3", DOUBLE_ZERO);
            values.put("AdContLucrosPD3", DOUBLE_ZERO);
        }
        if (anexoD != null && anexoD.getQuadro01().isAnexoDq01B2Selected()) {
            values.put("RendLiqImpAD3", anexoD.getQuadro04().getAnexoDq04C1() != null ? (double)anexoD.getQuadro04().getAnexoDq04C1().longValue() / 100.0 : 0.0);
            values.put("AdContLucrosAD3", anexoD.getQuadro04().getAnexoDq04C3() != null ? (double)anexoD.getQuadro04().getAnexoDq04C3().longValue() / 100.0 : 0.0);
        } else {
            values.put("RendLiqImpAD3", DOUBLE_ZERO);
            values.put("AdContLucrosAD3", DOUBLE_ZERO);
        }
    }

    private void doAnexoC(SimulatorHashtable<String, Object> values, RostoModel rosto, long nifD1, long nifD2, long nifD3) {
        long nif = rosto.getQuadro03().getQ03C03() != null ? rosto.getQuadro03().getQ03C03() : 0;
        AnexoCModel anexoC = nif != 0 ? (AnexoCModel)this.model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendCSPA", Character.valueOf('2'));
        values.put("AgriVSProfCSPA", anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejProCSPA = 0;
        long lucroPrejAgrCSPA = 0;
        long lucroPrejActFinCSPA = 0;
        if (anexoC != null) {
            lucroPrejActFinCSPA = (anexoC.getQuadro05().getAnexoCq05C506() != null ? anexoC.getQuadro05().getAnexoCq05C506() : 0) - (anexoC.getQuadro05().getAnexoCq05C505() != null ? anexoC.getQuadro05().getAnexoCq05C505() : 0);
        }
        if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCSPA = (anexoC.getQuadro05().getAnexoCq05C503() != null ? anexoC.getQuadro05().getAnexoCq05C503() : 0) - (anexoC.getQuadro05().getAnexoCq05C501() != null ? anexoC.getQuadro05().getAnexoCq05C501() : 0);
            lucroPrejAgrCSPA = (anexoC.getQuadro05().getAnexoCq05C504() != null ? anexoC.getQuadro05().getAnexoCq05C504() : 0) - (anexoC.getQuadro05().getAnexoCq05C502() != null ? anexoC.getQuadro05().getAnexoCq05C502() : 0);
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected()) {
            lucroPrejAgrCSPA = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejAgrCSPA-=lucroPrejActFinCSPA;
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCSPA = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejProCSPA-=lucroPrejActFinCSPA;
        }
        values.put("LucroPrejProCSPA", (double)lucroPrejProCSPA / 100.0);
        values.put("LucroPrejAgrCSPA", (double)lucroPrejAgrCSPA / 100.0);
        values.put("LucroPrejActFinCSPA", (double)lucroPrejActFinCSPA / 100.0);
        values.put("RetFonteCSPA", anexoC != null && anexoC.getQuadro08().getAnexoCq08C802() != null ? (double)anexoC.getQuadro08().getAnexoCq08C802().longValue() / 100.0 : 0.0);
        values.put("PagamenContaCSPA", anexoC != null && anexoC.getQuadro08().getAnexoCq08C803() != null ? (double)anexoC.getQuadro08().getAnexoCq08C803().longValue() / 100.0 : 0.0);
        values.put("RendAufDefCSPA", anexoC != null && anexoC.getQuadro04().getAnexoCq04C403() != null ? (double)anexoC.getQuadro04().getAnexoCq04C403().longValue() / 100.0 : 0.0);
        values.put("DespConfTASPA", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1001() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1001().longValue() / 100.0) : null));
        values.put("DespRepreTASPA", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1002() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1002().longValue() / 100.0 : 0.0);
        values.put("EncargViaturTASPA", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1003() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1003().longValue() / 100.0 : 0.0);
        values.put("ImpNaoResiTASPA", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1004() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1004().longValue() / 100.0) : null));
        values.put("AjudasCustoTASPA", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1005() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1005().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMenorTASPA", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1006() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1006().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMaiorTASPA", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1007() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1007().longValue() / 100.0 : 0.0);
        nif = rosto.getQuadro03().getQ03C04() != null ? rosto.getQuadro03().getQ03C04() : 0;
        anexoC = nif != 0 ? (AnexoCModel)this.model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendCSPB", Character.valueOf('2'));
        values.put("AgriVSProfCSPB", anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejProCSPB = 0;
        long lucroPrejAgrCSPB = 0;
        long lucroPrejActFinCSPB = 0;
        if (anexoC != null) {
            lucroPrejActFinCSPB = (anexoC.getQuadro05().getAnexoCq05C506() != null ? anexoC.getQuadro05().getAnexoCq05C506() : 0) - (anexoC.getQuadro05().getAnexoCq05C505() != null ? anexoC.getQuadro05().getAnexoCq05C505() : 0);
        }
        if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCSPB = (anexoC.getQuadro05().getAnexoCq05C503() != null ? anexoC.getQuadro05().getAnexoCq05C503() : 0) - (anexoC.getQuadro05().getAnexoCq05C501() != null ? anexoC.getQuadro05().getAnexoCq05C501() : 0);
            lucroPrejAgrCSPB = (anexoC.getQuadro05().getAnexoCq05C504() != null ? anexoC.getQuadro05().getAnexoCq05C504() : 0) - (anexoC.getQuadro05().getAnexoCq05C502() != null ? anexoC.getQuadro05().getAnexoCq05C502() : 0);
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected()) {
            lucroPrejAgrCSPB = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejAgrCSPB-=lucroPrejActFinCSPB;
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCSPB = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejProCSPB-=lucroPrejActFinCSPB;
        }
        values.put("LucroPrejProCSPB", (double)lucroPrejProCSPB / 100.0);
        values.put("LucroPrejAgrCSPB", (double)lucroPrejAgrCSPB / 100.0);
        values.put("LucroPrejActFinCSPB", (double)lucroPrejActFinCSPB / 100.0);
        values.put("RetFonteCSPB", anexoC != null && anexoC.getQuadro08().getAnexoCq08C802() != null ? (double)anexoC.getQuadro08().getAnexoCq08C802().longValue() / 100.0 : 0.0);
        values.put("PagamenContaCSPB", anexoC != null && anexoC.getQuadro08().getAnexoCq08C803() != null ? (double)anexoC.getQuadro08().getAnexoCq08C803().longValue() / 100.0 : 0.0);
        values.put("RendAufDefCSPB", anexoC != null && anexoC.getQuadro04().getAnexoCq04C403() != null ? (double)anexoC.getQuadro04().getAnexoCq04C403().longValue() / 100.0 : 0.0);
        values.put("DespConfTASPB", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1001() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1001().longValue() / 100.0) : null));
        values.put("DespRepreTASPB", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1002() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1002().longValue() / 100.0 : 0.0);
        values.put("EncargViaturTASPB", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1003() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1003().longValue() / 100.0 : 0.0);
        values.put("ImpNaoResiTASPB", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1004() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1004().longValue() / 100.0) : null));
        values.put("AjudasCustoTASPB", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1005() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1005().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMenorTASPB", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1006() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1006().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMaiorTASPB", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1007() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1007().longValue() / 100.0 : 0.0);
        nif = nifD1;
        anexoC = nif != 0 ? (AnexoCModel)this.model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendCD1", Character.valueOf('2'));
        values.put("AgriVSProfCD1", anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejProCD1 = 0;
        long lucroPrejAgrCD1 = 0;
        long lucroPrejActFinCD1 = 0;
        if (anexoC != null) {
            lucroPrejActFinCD1 = (anexoC.getQuadro05().getAnexoCq05C506() != null ? anexoC.getQuadro05().getAnexoCq05C506() : 0) - (anexoC.getQuadro05().getAnexoCq05C505() != null ? anexoC.getQuadro05().getAnexoCq05C505() : 0);
        }
        if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCD1 = (anexoC.getQuadro05().getAnexoCq05C503() != null ? anexoC.getQuadro05().getAnexoCq05C503() : 0) - (anexoC.getQuadro05().getAnexoCq05C501() != null ? anexoC.getQuadro05().getAnexoCq05C501() : 0);
            lucroPrejAgrCD1 = (anexoC.getQuadro05().getAnexoCq05C504() != null ? anexoC.getQuadro05().getAnexoCq05C504() : 0) - (anexoC.getQuadro05().getAnexoCq05C502() != null ? anexoC.getQuadro05().getAnexoCq05C502() : 0);
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected()) {
            lucroPrejAgrCD1 = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejAgrCD1-=lucroPrejActFinCD1;
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCD1 = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejProCD1-=lucroPrejActFinCD1;
        }
        values.put("LucroPrejProCD1", (double)lucroPrejProCD1 / 100.0);
        values.put("LucroPrejAgrCD1", (double)lucroPrejAgrCD1 / 100.0);
        values.put("LucroPrejActFinCD1", (double)lucroPrejActFinCD1 / 100.0);
        values.put("RetFonteCD1", anexoC != null && anexoC.getQuadro08().getAnexoCq08C802() != null ? (double)anexoC.getQuadro08().getAnexoCq08C802().longValue() / 100.0 : 0.0);
        values.put("PagamenContaCD1", anexoC != null && anexoC.getQuadro08().getAnexoCq08C803() != null ? (double)anexoC.getQuadro08().getAnexoCq08C803().longValue() / 100.0 : 0.0);
        values.put("RendAufDefCD1", anexoC != null && anexoC.getQuadro04().getAnexoCq04C403() != null ? (double)anexoC.getQuadro04().getAnexoCq04C403().longValue() / 100.0 : 0.0);
        values.put("DespConfTAD1", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1001() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1001().longValue() / 100.0) : null));
        values.put("DespRepreTAD1", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1002() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1002().longValue() / 100.0 : 0.0);
        values.put("EncargViaturTAD1", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1003() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1003().longValue() / 100.0 : 0.0);
        values.put("ImpNaoResiTAD1", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1004() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1004().longValue() / 100.0) : null));
        values.put("AjudasCustoTAD1", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1005() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1005().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMenorTAD1", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1006() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1006().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMaiorTAD1", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1007() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1007().longValue() / 100.0 : 0.0);
        nif = nifD2;
        anexoC = nif != 0 ? (AnexoCModel)this.model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendCD2", Character.valueOf('2'));
        values.put("AgriVSProfCD2", anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejProCD2 = 0;
        long lucroPrejAgrCD2 = 0;
        long lucroPrejActFinCD2 = 0;
        if (anexoC != null) {
            lucroPrejActFinCD2 = (anexoC.getQuadro05().getAnexoCq05C506() != null ? anexoC.getQuadro05().getAnexoCq05C506() : 0) - (anexoC.getQuadro05().getAnexoCq05C505() != null ? anexoC.getQuadro05().getAnexoCq05C505() : 0);
        }
        if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCD2 = (anexoC.getQuadro05().getAnexoCq05C503() != null ? anexoC.getQuadro05().getAnexoCq05C503() : 0) - (anexoC.getQuadro05().getAnexoCq05C501() != null ? anexoC.getQuadro05().getAnexoCq05C501() : 0);
            lucroPrejAgrCD2 = (anexoC.getQuadro05().getAnexoCq05C504() != null ? anexoC.getQuadro05().getAnexoCq05C504() : 0) - (anexoC.getQuadro05().getAnexoCq05C502() != null ? anexoC.getQuadro05().getAnexoCq05C502() : 0);
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected()) {
            lucroPrejAgrCD2 = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejAgrCD2-=lucroPrejActFinCD2;
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCD2 = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejProCD2-=lucroPrejActFinCD2;
        }
        values.put("LucroPrejProCD2", (double)lucroPrejProCD2 / 100.0);
        values.put("LucroPrejAgrCD2", (double)lucroPrejAgrCD2 / 100.0);
        values.put("LucroPrejActFinCD2", (double)lucroPrejActFinCD2 / 100.0);
        values.put("RetFonteCD2", anexoC != null && anexoC.getQuadro08().getAnexoCq08C802() != null ? (double)anexoC.getQuadro08().getAnexoCq08C802().longValue() / 100.0 : 0.0);
        values.put("PagamenContaCD2", anexoC != null && anexoC.getQuadro08().getAnexoCq08C803() != null ? (double)anexoC.getQuadro08().getAnexoCq08C803().longValue() / 100.0 : 0.0);
        values.put("RendAufDefCD2", anexoC != null && anexoC.getQuadro04().getAnexoCq04C403() != null ? (double)anexoC.getQuadro04().getAnexoCq04C403().longValue() / 100.0 : 0.0);
        values.put("AjudasCustoTAD2", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1005() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1005().longValue() / 100.0 : 0.0);
        values.put("DespRepreTAD2", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1002() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1002().longValue() / 100.0 : 0.0);
        values.put("EncargViaturTAD2", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1003() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1003().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMenorTAD2", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1006() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1006().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMaiorTAD2", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1007() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1007().longValue() / 100.0 : 0.0);
        nif = nifD3;
        anexoC = nif != 0 ? (AnexoCModel)this.model.getAnexo(new FormKey(AnexoCModel.class, String.valueOf(nif))) : null;
        values.put("TipoRendCD3", Character.valueOf('2'));
        values.put("AgriVSProfCD3", anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() ? 2.0 : 1.0);
        long lucroPrejProCD3 = 0;
        long lucroPrejAgrCD3 = 0;
        long lucroPrejActFinCD3 = 0;
        if (anexoC != null) {
            lucroPrejActFinCD3 = (anexoC.getQuadro05().getAnexoCq05C506() != null ? anexoC.getQuadro05().getAnexoCq05C506() : 0) - (anexoC.getQuadro05().getAnexoCq05C505() != null ? anexoC.getQuadro05().getAnexoCq05C505() : 0);
        }
        if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected() && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCD3 = (anexoC.getQuadro05().getAnexoCq05C503() != null ? anexoC.getQuadro05().getAnexoCq05C503() : 0) - (anexoC.getQuadro05().getAnexoCq05C501() != null ? anexoC.getQuadro05().getAnexoCq05C501() : 0);
            lucroPrejAgrCD3 = (anexoC.getQuadro05().getAnexoCq05C504() != null ? anexoC.getQuadro05().getAnexoCq05C504() : 0) - (anexoC.getQuadro05().getAnexoCq05C502() != null ? anexoC.getQuadro05().getAnexoCq05C502() : 0);
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B2Selected()) {
            lucroPrejAgrCD3 = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejAgrCD3-=lucroPrejActFinCD3;
        } else if (anexoC != null && anexoC.getQuadro01().isAnexoCq01B1Selected()) {
            lucroPrejProCD3 = (anexoC.getQuadro04().getAnexoCq04C460() != null ? anexoC.getQuadro04().getAnexoCq04C460() : 0) - (anexoC.getQuadro04().getAnexoCq04C459() != null ? anexoC.getQuadro04().getAnexoCq04C459() : 0);
            lucroPrejProCD3-=lucroPrejActFinCD3;
        }
        values.put("LucroPrejProCD3", (double)lucroPrejProCD3 / 100.0);
        values.put("LucroPrejAgrCD3", (double)lucroPrejAgrCD3 / 100.0);
        values.put("LucroPrejActFinCD3", (double)lucroPrejActFinCD3 / 100.0);
        values.put("RetFonteCD3", anexoC != null && anexoC.getQuadro08().getAnexoCq08C802() != null ? (double)anexoC.getQuadro08().getAnexoCq08C802().longValue() / 100.0 : 0.0);
        values.put("PagamenContaCD3", anexoC != null && anexoC.getQuadro08().getAnexoCq08C803() != null ? (double)anexoC.getQuadro08().getAnexoCq08C803().longValue() / 100.0 : 0.0);
        values.put("RendAufDefCD3", anexoC != null && anexoC.getQuadro04().getAnexoCq04C403() != null ? (double)anexoC.getQuadro04().getAnexoCq04C403().longValue() / 100.0 : 0.0);
        values.put("DespConfTAD3", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1001() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1001().longValue() / 100.0) : null));
        values.put("DespRepreTAD3", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1002() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1002().longValue() / 100.0 : 0.0);
        values.put("EncargViaturTAD3", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1003() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1003().longValue() / 100.0 : 0.0);
        values.put("ImpNaoResiTAD3", (anexoC != null && anexoC.getQuadro10().getAnexoCq10C1004() != null ? Double.valueOf((double)anexoC.getQuadro10().getAnexoCq10C1004().longValue() / 100.0) : null));
        values.put("AjudasCustoTAD3", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1005() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1005().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMenorTAD3", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1006() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1006().longValue() / 100.0 : 0.0);
        values.put("EncargAquisMaiorTAD3", anexoC != null && anexoC.getQuadro10().getAnexoCq10C1007() != null ? (double)anexoC.getQuadro10().getAnexoCq10C1007().longValue() / 100.0 : 0.0);
    }

    private void doAnexoB(SimulatorHashtable<String, Object> values, RostoModel rosto, long nifD1, long nifD2, long nifD3) {
        long nif = rosto.getQuadro03().getQ03C03() != null ? rosto.getQuadro03().getQ03C03() : 0;
        AnexoBModel anexoB = nif != 0 ? (AnexoBModel)this.model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(nif))) : null;
        values.put("NaturRendBSPA", Character.valueOf(anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? anexoB.getQuadro01().getAnexoBq01B1().charAt(0) : '1'));
        values.put("SimpVSIsolBSPA", anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? Double.valueOf(anexoB.getQuadro01().getAnexoBq01B1()) : 1.0);
        values.put("TipoRendBSPA", Character.valueOf('2'));
        values.put("AgriVSProfBSPA", anexoB != null && anexoB.getQuadro01().isAnexoBq01B4Selected() ? 2.0 : 1.0);
        values.put("VendasMercadoPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C401() != null ? (double)anexoB.getQuadro04().getAnexoBq04C401().longValue() / 100.0 : 0.0);
        values.put("ServicosHotPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C402() != null ? (double)anexoB.getQuadro04().getAnexoBq04C402().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C403() != null ? (double)anexoB.getQuadro04().getAnexoBq04C403().longValue() / 100.0 : 0.0);
        values.put("PropriedadeIntPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C404() != null ? (double)anexoB.getQuadro04().getAnexoBq04C404().longValue() / 100.0 : 0.0);
        values.put("RendimActFinPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C405() != null ? (double)anexoB.getQuadro04().getAnexoBq04C405().longValue() / 100.0 : 0.0);
        values.put("ServPrestSocPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C420() != null ? (double)anexoB.getQuadro04().getAnexoBq04C420().longValue() / 100.0 : 0.0);
        values.put("MicroProdElecPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C423() != null ? (double)anexoB.getQuadro04().getAnexoBq04C423().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C424() != null ? (double)anexoB.getQuadro04().getAnexoBq04C424().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C425() != null ? (double)anexoB.getQuadro04().getAnexoBq04C425().longValue() / 100.0 : 0.0);
        values.put("RendimPBSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C1() != null ? (double)anexoB.getQuadro04().getAnexoBq04C1().longValue() / 100.0 : 0.0);
        values.put("CustosExistPBSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null ? (double)anexoB.getQuadro04().getAnexoBq04C406().longValue() / 100.0 : 0.0);
        values.put("VendasMercadoASPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C409() != null ? (double)anexoB.getQuadro04().getAnexoBq04C409().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosASPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C410() != null ? (double)anexoB.getQuadro04().getAnexoBq04C410().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplASPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C411() != null ? (double)anexoB.getQuadro04().getAnexoBq04C411().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosASPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C426() != null ? (double)anexoB.getQuadro04().getAnexoBq04C426().longValue() / 100.0 : 0.0);
        values.put("RendimABSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C3() != null ? (double)anexoB.getQuadro04().getAnexoBq04C3().longValue() / 100.0 : 0.0);
        values.put("CustosExistABSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null ? (double)anexoB.getQuadro04().getAnexoBq04C413().longValue() / 100.0 : 0.0);
        values.put("TributacaoCatASPA", anexoB != null && anexoB.getQuadro04().isAnexoBq04B2OPSelected() ? 1 : 0);
        values.put("RendPredImpSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C421() != null ? (double)anexoB.getQuadro04().getAnexoBq04C421().longValue() / 100.0 : 0.0);
        values.put("RendCapImpSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C422() != null ? (double)anexoB.getQuadro04().getAnexoBq04C422().longValue() / 100.0 : 0.0);
        values.put("RendimCIRSPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C440() != null ? (double)anexoB.getQuadro04().getAnexoBq04C440().longValue() / 100.0 : 0.0);
        values.put("PropIndustrialPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C441() != null ? (double)anexoB.getQuadro04().getAnexoBq04C441().longValue() / 100.0 : 0.0);
        values.put("SaldoPositivoPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C442() != null ? (double)anexoB.getQuadro04().getAnexoBq04C442().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBPSPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C443() != null ? (double)anexoB.getQuadro04().getAnexoBq04C443().longValue() / 100.0 : 0.0);
        values.put("PrestServicosASPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C444() != null ? (double)anexoB.getQuadro04().getAnexoBq04C444().longValue() / 100.0 : 0.0);
        values.put("RendDiversosBASPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C445() != null ? (double)anexoB.getQuadro04().getAnexoBq04C445().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBASPA", anexoB != null && anexoB.getQuadro04().getAnexoBq04C446() != null ? (double)anexoB.getQuadro04().getAnexoBq04C446().longValue() / 100.0 : 0.0);
        long soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C406().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C901().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C902().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C903().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C907().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C908().longValue();
        }
        values.put("DespesaPBSPA", (double)soma / 100.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C413().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C910().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C911().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C912().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C916().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C917().longValue();
        }
        values.put("DespesaABSPA", (double)soma / 100.0);
        values.put("RetFonteBSPA", anexoB != null && anexoB.getQuadro07().getAnexoBq07C702() != null ? (double)anexoB.getQuadro07().getAnexoBq07C702().longValue() / 100.0 : 0.0);
        values.put("PagamenContaBSPA", anexoB != null && anexoB.getQuadro07().getAnexoBq07C703() != null ? (double)anexoB.getQuadro07().getAnexoBq07C703().longValue() / 100.0 : 0.0);
        values.put("EncViaturaPBSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null ? (double)anexoB.getQuadro09().getAnexoBq09C901().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalPBSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null ? (double)anexoB.getQuadro09().getAnexoBq09C902().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoPBSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null ? (double)anexoB.getQuadro09().getAnexoBq09C903().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesPBSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null ? (double)anexoB.getQuadro09().getAnexoBq09C907().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgPBSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null ? (double)anexoB.getQuadro09().getAnexoBq09C908().longValue() / 100.0 : 0.0);
        values.put("EncViaturaABSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null ? (double)anexoB.getQuadro09().getAnexoBq09C910().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalABSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null ? (double)anexoB.getQuadro09().getAnexoBq09C911().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoABSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null ? (double)anexoB.getQuadro09().getAnexoBq09C912().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesABSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null ? (double)anexoB.getQuadro09().getAnexoBq09C916().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgABSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null ? (double)anexoB.getQuadro09().getAnexoBq09C917().longValue() / 100.0 : 0.0);
        values.put("DespConfTASPA", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1001() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1001().longValue() / 100.0) : null));
        values.put("ImpNaoResiTASPA", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1002() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1002().longValue() / 100.0) : null));
        values.put("ContObrSegSocPSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C904() != null ? (double)anexoB.getQuadro09().getAnexoBq09C904().longValue() / 100.0 : 0.0);
        values.put("QuotSindPSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C905() != null ? (double)anexoB.getQuadro09().getAnexoBq09C905().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfPSPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C906() != null ? (double)anexoB.getQuadro09().getAnexoBq09C906().longValue() / 100.0 : 0.0);
        values.put("ContObrSegSocASPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C913() != null ? (double)anexoB.getQuadro09().getAnexoBq09C913().longValue() / 100.0 : 0.0);
        values.put("QuotSindASPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C914() != null ? (double)anexoB.getQuadro09().getAnexoBq09C914().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfASPA", anexoB != null && anexoB.getQuadro09().getAnexoBq09C915() != null ? (double)anexoB.getQuadro09().getAnexoBq09C915().longValue() / 100.0 : 0.0);
        nif = rosto.getQuadro03().getQ03C04() != null ? rosto.getQuadro03().getQ03C04() : 0;
        anexoB = nif != 0 ? (AnexoBModel)this.model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(nif))) : null;
        values.put("NaturRendBSPB", Character.valueOf(anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? anexoB.getQuadro01().getAnexoBq01B1().charAt(0) : '1'));
        values.put("SimpVSIsolBSPB", anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? Double.valueOf(anexoB.getQuadro01().getAnexoBq01B1()) : 1.0);
        values.put("TipoRendBSPB", Character.valueOf('2'));
        values.put("AgriVSProfBSPB", anexoB != null && anexoB.getQuadro01().isAnexoBq01B4Selected() ? 2.0 : 1.0);
        values.put("VendasMercadoPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C401() != null ? (double)anexoB.getQuadro04().getAnexoBq04C401().longValue() / 100.0 : 0.0);
        values.put("ServicosHotPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C402() != null ? (double)anexoB.getQuadro04().getAnexoBq04C402().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C403() != null ? (double)anexoB.getQuadro04().getAnexoBq04C403().longValue() / 100.0 : 0.0);
        values.put("PropriedadeIntPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C404() != null ? (double)anexoB.getQuadro04().getAnexoBq04C404().longValue() / 100.0 : 0.0);
        values.put("RendimActFinPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C405() != null ? (double)anexoB.getQuadro04().getAnexoBq04C405().longValue() / 100.0 : 0.0);
        values.put("ServPrestSocPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C420() != null ? (double)anexoB.getQuadro04().getAnexoBq04C420().longValue() / 100.0 : 0.0);
        values.put("MicroProdElecPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C423() != null ? (double)anexoB.getQuadro04().getAnexoBq04C423().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C424() != null ? (double)anexoB.getQuadro04().getAnexoBq04C424().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C425() != null ? (double)anexoB.getQuadro04().getAnexoBq04C425().longValue() / 100.0 : 0.0);
        values.put("RendimPBSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C1() != null ? (double)anexoB.getQuadro04().getAnexoBq04C1().longValue() / 100.0 : 0.0);
        values.put("CustosExistPBSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null ? (double)anexoB.getQuadro04().getAnexoBq04C406().longValue() / 100.0 : 0.0);
        values.put("VendasMercadoASPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C409() != null ? (double)anexoB.getQuadro04().getAnexoBq04C409().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosASPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C410() != null ? (double)anexoB.getQuadro04().getAnexoBq04C410().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplASPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C411() != null ? (double)anexoB.getQuadro04().getAnexoBq04C411().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosASPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C426() != null ? (double)anexoB.getQuadro04().getAnexoBq04C426().longValue() / 100.0 : 0.0);
        values.put("RendimABSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C3() != null ? (double)anexoB.getQuadro04().getAnexoBq04C3().longValue() / 100.0 : 0.0);
        values.put("CustosExistABSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null ? (double)anexoB.getQuadro04().getAnexoBq04C413().longValue() / 100.0 : 0.0);
        values.put("TributacaoCatASPB", anexoB != null && anexoB.getQuadro04().isAnexoBq04B2OPSelected() ? 1 : 0);
        values.put("RendPredImpSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C421() != null ? (double)anexoB.getQuadro04().getAnexoBq04C421().longValue() / 100.0 : 0.0);
        values.put("RendCapImpSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C422() != null ? (double)anexoB.getQuadro04().getAnexoBq04C422().longValue() / 100.0 : 0.0);
        values.put("RendimCIRSPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C440() != null ? (double)anexoB.getQuadro04().getAnexoBq04C440().longValue() / 100.0 : 0.0);
        values.put("PropIndustrialPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C441() != null ? (double)anexoB.getQuadro04().getAnexoBq04C441().longValue() / 100.0 : 0.0);
        values.put("SaldoPositivoPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C442() != null ? (double)anexoB.getQuadro04().getAnexoBq04C442().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBPSPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C443() != null ? (double)anexoB.getQuadro04().getAnexoBq04C443().longValue() / 100.0 : 0.0);
        values.put("PrestServicosASPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C444() != null ? (double)anexoB.getQuadro04().getAnexoBq04C444().longValue() / 100.0 : 0.0);
        values.put("RendDiversosBASPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C445() != null ? (double)anexoB.getQuadro04().getAnexoBq04C445().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBASPB", anexoB != null && anexoB.getQuadro04().getAnexoBq04C446() != null ? (double)anexoB.getQuadro04().getAnexoBq04C446().longValue() / 100.0 : 0.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C406().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C901().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C902().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C903().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C907().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C908().longValue();
        }
        values.put("DespesaPBSPB", (double)soma / 100.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C413().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C910().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C911().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C912().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C916().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C917().longValue();
        }
        values.put("DespesaABSPB", (double)soma / 100.0);
        values.put("RetFonteBSPB", anexoB != null && anexoB.getQuadro07().getAnexoBq07C702() != null ? (double)anexoB.getQuadro07().getAnexoBq07C702().longValue() / 100.0 : 0.0);
        values.put("PagamenContaBSPB", anexoB != null && anexoB.getQuadro07().getAnexoBq07C703() != null ? (double)anexoB.getQuadro07().getAnexoBq07C703().longValue() / 100.0 : 0.0);
        values.put("EncViaturaPBSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null ? (double)anexoB.getQuadro09().getAnexoBq09C901().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalPBSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null ? (double)anexoB.getQuadro09().getAnexoBq09C902().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoPBSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null ? (double)anexoB.getQuadro09().getAnexoBq09C903().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesPBSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null ? (double)anexoB.getQuadro09().getAnexoBq09C907().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgPBSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null ? (double)anexoB.getQuadro09().getAnexoBq09C908().longValue() / 100.0 : 0.0);
        values.put("EncViaturaABSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null ? (double)anexoB.getQuadro09().getAnexoBq09C910().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalABSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null ? (double)anexoB.getQuadro09().getAnexoBq09C911().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoABSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null ? (double)anexoB.getQuadro09().getAnexoBq09C912().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesABSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null ? (double)anexoB.getQuadro09().getAnexoBq09C916().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgABSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null ? (double)anexoB.getQuadro09().getAnexoBq09C917().longValue() / 100.0 : 0.0);
        values.put("DespConfTASPB", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1001() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1001().longValue() / 100.0) : null));
        values.put("ImpNaoResiTASPB", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1002() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1002().longValue() / 100.0) : null));
        values.put("ContObrSegSocPSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C904() != null ? (double)anexoB.getQuadro09().getAnexoBq09C904().longValue() / 100.0 : 0.0);
        values.put("QuotSindPSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C905() != null ? (double)anexoB.getQuadro09().getAnexoBq09C905().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfPSPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C906() != null ? (double)anexoB.getQuadro09().getAnexoBq09C906().longValue() / 100.0 : 0.0);
        values.put("ContObrSegSocASPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C913() != null ? (double)anexoB.getQuadro09().getAnexoBq09C913().longValue() / 100.0 : 0.0);
        values.put("QuotSindASPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C914() != null ? (double)anexoB.getQuadro09().getAnexoBq09C914().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfASPB", anexoB != null && anexoB.getQuadro09().getAnexoBq09C915() != null ? (double)anexoB.getQuadro09().getAnexoBq09C915().longValue() / 100.0 : 0.0);
        nif = nifD1;
        anexoB = nif != 0 ? (AnexoBModel)this.model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(nif))) : null;
        values.put("NaturRendBD1", Character.valueOf(anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? anexoB.getQuadro01().getAnexoBq01B1().charAt(0) : '1'));
        values.put("SimpVSIsolBD1", anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? Double.valueOf(anexoB.getQuadro01().getAnexoBq01B1()) : 1.0);
        values.put("TipoRendBD1", Character.valueOf('2'));
        values.put("AgriVSProfBD1", anexoB != null && anexoB.getQuadro01().isAnexoBq01B4Selected() ? 2.0 : 1.0);
        values.put("VendasMercadoPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C401() != null ? (double)anexoB.getQuadro04().getAnexoBq04C401().longValue() / 100.0 : 0.0);
        values.put("ServicosHotPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C402() != null ? (double)anexoB.getQuadro04().getAnexoBq04C402().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C403() != null ? (double)anexoB.getQuadro04().getAnexoBq04C403().longValue() / 100.0 : 0.0);
        values.put("PropriedadeIntPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C404() != null ? (double)anexoB.getQuadro04().getAnexoBq04C404().longValue() / 100.0 : 0.0);
        values.put("RendimActFinPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C405() != null ? (double)anexoB.getQuadro04().getAnexoBq04C405().longValue() / 100.0 : 0.0);
        values.put("ServPrestSocPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C420() != null ? (double)anexoB.getQuadro04().getAnexoBq04C420().longValue() / 100.0 : 0.0);
        values.put("MicroProdElecPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C423() != null ? (double)anexoB.getQuadro04().getAnexoBq04C423().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C424() != null ? (double)anexoB.getQuadro04().getAnexoBq04C424().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C425() != null ? (double)anexoB.getQuadro04().getAnexoBq04C425().longValue() / 100.0 : 0.0);
        values.put("RendimPBD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C1() != null ? (double)anexoB.getQuadro04().getAnexoBq04C1().longValue() / 100.0 : 0.0);
        values.put("CustosExistPBD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null ? (double)anexoB.getQuadro04().getAnexoBq04C406().longValue() / 100.0 : 0.0);
        values.put("VendasMercadoAD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C409() != null ? (double)anexoB.getQuadro04().getAnexoBq04C409().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosAD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C410() != null ? (double)anexoB.getQuadro04().getAnexoBq04C410().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplAD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C411() != null ? (double)anexoB.getQuadro04().getAnexoBq04C411().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosAD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C426() != null ? (double)anexoB.getQuadro04().getAnexoBq04C426().longValue() / 100.0 : 0.0);
        values.put("RendimABD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C3() != null ? (double)anexoB.getQuadro04().getAnexoBq04C3().longValue() / 100.0 : 0.0);
        values.put("CustosExistABD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null ? (double)anexoB.getQuadro04().getAnexoBq04C413().longValue() / 100.0 : 0.0);
        values.put("TributacaoCatAD1", anexoB != null && anexoB.getQuadro04().isAnexoBq04B2OPSelected() ? 1 : 0);
        values.put("RendPredImpD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C421() != null ? (double)anexoB.getQuadro04().getAnexoBq04C421().longValue() / 100.0 : 0.0);
        values.put("RendCapImpD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C422() != null ? (double)anexoB.getQuadro04().getAnexoBq04C422().longValue() / 100.0 : 0.0);
        values.put("RendimCIRSPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C440() != null ? (double)anexoB.getQuadro04().getAnexoBq04C440().longValue() / 100.0 : 0.0);
        values.put("PropIndustrialPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C441() != null ? (double)anexoB.getQuadro04().getAnexoBq04C441().longValue() / 100.0 : 0.0);
        values.put("SaldoPositivoPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C442() != null ? (double)anexoB.getQuadro04().getAnexoBq04C442().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBPD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C443() != null ? (double)anexoB.getQuadro04().getAnexoBq04C443().longValue() / 100.0 : 0.0);
        values.put("PrestServicosAD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C444() != null ? (double)anexoB.getQuadro04().getAnexoBq04C444().longValue() / 100.0 : 0.0);
        values.put("RendDiversosBAD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C445() != null ? (double)anexoB.getQuadro04().getAnexoBq04C445().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBAD1", anexoB != null && anexoB.getQuadro04().getAnexoBq04C446() != null ? (double)anexoB.getQuadro04().getAnexoBq04C446().longValue() / 100.0 : 0.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C406().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C901().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C902().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C903().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C907().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C908().longValue();
        }
        values.put("DespesaPBD1", (double)soma / 100.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C413().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C910().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C911().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C912().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C916().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C917().longValue();
        }
        values.put("DespesaABD1", (double)soma / 100.0);
        values.put("RetFonteBD1", anexoB != null && anexoB.getQuadro07().getAnexoBq07C702() != null ? (double)anexoB.getQuadro07().getAnexoBq07C702().longValue() / 100.0 : 0.0);
        values.put("PagamenContaBD1", anexoB != null && anexoB.getQuadro07().getAnexoBq07C703() != null ? (double)anexoB.getQuadro07().getAnexoBq07C703().longValue() / 100.0 : 0.0);
        values.put("EncViaturaPBD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null ? (double)anexoB.getQuadro09().getAnexoBq09C901().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalPBD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null ? (double)anexoB.getQuadro09().getAnexoBq09C902().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoPBD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null ? (double)anexoB.getQuadro09().getAnexoBq09C903().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesPBD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null ? (double)anexoB.getQuadro09().getAnexoBq09C907().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgPBD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null ? (double)anexoB.getQuadro09().getAnexoBq09C908().longValue() / 100.0 : 0.0);
        values.put("EncViaturaABD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null ? (double)anexoB.getQuadro09().getAnexoBq09C910().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalABD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null ? (double)anexoB.getQuadro09().getAnexoBq09C911().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoABD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null ? (double)anexoB.getQuadro09().getAnexoBq09C912().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesABD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null ? (double)anexoB.getQuadro09().getAnexoBq09C916().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgABD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null ? (double)anexoB.getQuadro09().getAnexoBq09C917().longValue() / 100.0 : 0.0);
        values.put("DespConfTAD1", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1001() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1001().longValue() / 100.0) : null));
        values.put("ImpNaoResiTAD1", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1002() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1002().longValue() / 100.0) : null));
        values.put("ContObrSegSocPD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C904() != null ? (double)anexoB.getQuadro09().getAnexoBq09C904().longValue() / 100.0 : 0.0);
        values.put("QuotSindPD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C905() != null ? (double)anexoB.getQuadro09().getAnexoBq09C905().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfPD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C906() != null ? (double)anexoB.getQuadro09().getAnexoBq09C906().longValue() / 100.0 : 0.0);
        values.put("ContObrSegSocAD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C913() != null ? (double)anexoB.getQuadro09().getAnexoBq09C913().longValue() / 100.0 : 0.0);
        values.put("QuotSindAD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C914() != null ? (double)anexoB.getQuadro09().getAnexoBq09C914().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfAD1", anexoB != null && anexoB.getQuadro09().getAnexoBq09C915() != null ? (double)anexoB.getQuadro09().getAnexoBq09C915().longValue() / 100.0 : 0.0);
        nif = nifD2;
        anexoB = nif != 0 ? (AnexoBModel)this.model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(nif))) : null;
        values.put("NaturRendBD2", Character.valueOf(anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? anexoB.getQuadro01().getAnexoBq01B1().charAt(0) : '1'));
        values.put("SimpVSIsolBD2", anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? Double.valueOf(anexoB.getQuadro01().getAnexoBq01B1()) : 1.0);
        values.put("TipoRendBD2", Character.valueOf('2'));
        values.put("AgriVSProfBD2", anexoB != null && anexoB.getQuadro01().isAnexoBq01B4Selected() ? 2.0 : 1.0);
        values.put("VendasMercadoPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C401() != null ? (double)anexoB.getQuadro04().getAnexoBq04C401().longValue() / 100.0 : 0.0);
        values.put("ServicosHotPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C402() != null ? (double)anexoB.getQuadro04().getAnexoBq04C402().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C403() != null ? (double)anexoB.getQuadro04().getAnexoBq04C403().longValue() / 100.0 : 0.0);
        values.put("PropriedadeIntPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C404() != null ? (double)anexoB.getQuadro04().getAnexoBq04C404().longValue() / 100.0 : 0.0);
        values.put("RendimActFinPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C405() != null ? (double)anexoB.getQuadro04().getAnexoBq04C405().longValue() / 100.0 : 0.0);
        values.put("ServPrestSocPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C420() != null ? (double)anexoB.getQuadro04().getAnexoBq04C420().longValue() / 100.0 : 0.0);
        values.put("MicroProdElecPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C423() != null ? (double)anexoB.getQuadro04().getAnexoBq04C423().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C424() != null ? (double)anexoB.getQuadro04().getAnexoBq04C424().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C425() != null ? (double)anexoB.getQuadro04().getAnexoBq04C425().longValue() / 100.0 : 0.0);
        values.put("RendimPBD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C1() != null ? (double)anexoB.getQuadro04().getAnexoBq04C1().longValue() / 100.0 : 0.0);
        values.put("CustosExistPBD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null ? (double)anexoB.getQuadro04().getAnexoBq04C406().longValue() / 100.0 : 0.0);
        values.put("VendasMercadoAD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C409() != null ? (double)anexoB.getQuadro04().getAnexoBq04C409().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosAD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C410() != null ? (double)anexoB.getQuadro04().getAnexoBq04C410().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplAD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C411() != null ? (double)anexoB.getQuadro04().getAnexoBq04C411().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosAD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C426() != null ? (double)anexoB.getQuadro04().getAnexoBq04C426().longValue() / 100.0 : 0.0);
        values.put("RendimABD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C3() != null ? (double)anexoB.getQuadro04().getAnexoBq04C3().longValue() / 100.0 : 0.0);
        values.put("CustosExistABD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null ? (double)anexoB.getQuadro04().getAnexoBq04C413().longValue() / 100.0 : 0.0);
        values.put("TributacaoCatAD2", anexoB != null && anexoB.getQuadro04().isAnexoBq04B2OPSelected() ? 1 : 0);
        values.put("RendPredImpD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C421() != null ? (double)anexoB.getQuadro04().getAnexoBq04C421().longValue() / 100.0 : 0.0);
        values.put("RendCapImpD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C422() != null ? (double)anexoB.getQuadro04().getAnexoBq04C422().longValue() / 100.0 : 0.0);
        values.put("RendimCIRSPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C440() != null ? (double)anexoB.getQuadro04().getAnexoBq04C440().longValue() / 100.0 : 0.0);
        values.put("PropIndustrialPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C441() != null ? (double)anexoB.getQuadro04().getAnexoBq04C441().longValue() / 100.0 : 0.0);
        values.put("SaldoPositivoPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C442() != null ? (double)anexoB.getQuadro04().getAnexoBq04C442().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBPD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C443() != null ? (double)anexoB.getQuadro04().getAnexoBq04C443().longValue() / 100.0 : 0.0);
        values.put("PrestServicosAD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C444() != null ? (double)anexoB.getQuadro04().getAnexoBq04C444().longValue() / 100.0 : 0.0);
        values.put("RendDiversosBAD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C445() != null ? (double)anexoB.getQuadro04().getAnexoBq04C445().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBAD2", anexoB != null && anexoB.getQuadro04().getAnexoBq04C446() != null ? (double)anexoB.getQuadro04().getAnexoBq04C446().longValue() / 100.0 : 0.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C406().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C901().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C902().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C903().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C907().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C908().longValue();
        }
        values.put("DespesaPBD2", (double)soma / 100.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C413().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C910().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C911().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C912().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C916().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C917().longValue();
        }
        values.put("DespesaABD2", (double)soma / 100.0);
        values.put("RetFonteBD2", anexoB != null && anexoB.getQuadro07().getAnexoBq07C702() != null ? (double)anexoB.getQuadro07().getAnexoBq07C702().longValue() / 100.0 : 0.0);
        values.put("PagamenContaBD2", anexoB != null && anexoB.getQuadro07().getAnexoBq07C703() != null ? (double)anexoB.getQuadro07().getAnexoBq07C703().longValue() / 100.0 : 0.0);
        values.put("EncViaturaPBD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null ? (double)anexoB.getQuadro09().getAnexoBq09C901().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalPBD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null ? (double)anexoB.getQuadro09().getAnexoBq09C902().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoPBD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null ? (double)anexoB.getQuadro09().getAnexoBq09C903().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesPBD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null ? (double)anexoB.getQuadro09().getAnexoBq09C907().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgPBD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null ? (double)anexoB.getQuadro09().getAnexoBq09C908().longValue() / 100.0 : 0.0);
        values.put("EncViaturaABD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null ? (double)anexoB.getQuadro09().getAnexoBq09C910().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalABD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null ? (double)anexoB.getQuadro09().getAnexoBq09C911().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoABD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null ? (double)anexoB.getQuadro09().getAnexoBq09C912().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesABD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null ? (double)anexoB.getQuadro09().getAnexoBq09C916().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgABD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null ? (double)anexoB.getQuadro09().getAnexoBq09C917().longValue() / 100.0 : 0.0);
        values.put("DespConfTAD2", anexoB != null && anexoB.getQuadro10().getAnexoBq10C1001() != null ? (double)anexoB.getQuadro10().getAnexoBq10C1001().longValue() / 100.0 : 0.0);
        values.put("ImpNaoResiTAD2", anexoB != null && anexoB.getQuadro10().getAnexoBq10C1002() != null ? (double)anexoB.getQuadro10().getAnexoBq10C1002().longValue() / 100.0 : 0.0);
        values.put("ContObrSegSocPD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C904() != null ? (double)anexoB.getQuadro09().getAnexoBq09C904().longValue() / 100.0 : 0.0);
        values.put("QuotSindPD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C905() != null ? (double)anexoB.getQuadro09().getAnexoBq09C905().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfPD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C906() != null ? (double)anexoB.getQuadro09().getAnexoBq09C906().longValue() / 100.0 : 0.0);
        values.put("ContObrSegSocAD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C913() != null ? (double)anexoB.getQuadro09().getAnexoBq09C913().longValue() / 100.0 : 0.0);
        values.put("QuotSindAD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C914() != null ? (double)anexoB.getQuadro09().getAnexoBq09C914().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfAD2", anexoB != null && anexoB.getQuadro09().getAnexoBq09C915() != null ? (double)anexoB.getQuadro09().getAnexoBq09C915().longValue() / 100.0 : 0.0);
        nif = nifD3;
        anexoB = nif != 0 ? (AnexoBModel)this.model.getAnexo(new FormKey(AnexoBModel.class, String.valueOf(nif))) : null;
        values.put("NaturRendBD3", Character.valueOf(anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? anexoB.getQuadro01().getAnexoBq01B1().charAt(0) : '1'));
        values.put("SimpVSIsolBD3", anexoB != null && anexoB.getQuadro01().getAnexoBq01B1() != null ? Double.valueOf(anexoB.getQuadro01().getAnexoBq01B1()) : 1.0);
        values.put("TipoRendBD3", Character.valueOf('2'));
        values.put("AgriVSProfBD3", anexoB != null && anexoB.getQuadro01().isAnexoBq01B4Selected() ? 2.0 : 1.0);
        values.put("VendasMercadoPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C401() != null ? (double)anexoB.getQuadro04().getAnexoBq04C401().longValue() / 100.0 : 0.0);
        values.put("ServicosHotPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C402() != null ? (double)anexoB.getQuadro04().getAnexoBq04C402().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C403() != null ? (double)anexoB.getQuadro04().getAnexoBq04C403().longValue() / 100.0 : 0.0);
        values.put("PropriedadeIntPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C404() != null ? (double)anexoB.getQuadro04().getAnexoBq04C404().longValue() / 100.0 : 0.0);
        values.put("RendimActFinPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C405() != null ? (double)anexoB.getQuadro04().getAnexoBq04C405().longValue() / 100.0 : 0.0);
        values.put("ServPrestSocPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C420() != null ? (double)anexoB.getQuadro04().getAnexoBq04C420().longValue() / 100.0 : 0.0);
        values.put("MicroProdElecPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C423() != null ? (double)anexoB.getQuadro04().getAnexoBq04C423().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C424() != null ? (double)anexoB.getQuadro04().getAnexoBq04C424().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C425() != null ? (double)anexoB.getQuadro04().getAnexoBq04C425().longValue() / 100.0 : 0.0);
        values.put("RendimPBD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C1() != null ? (double)anexoB.getQuadro04().getAnexoBq04C1().longValue() / 100.0 : 0.0);
        values.put("CustosExistPBD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null ? (double)anexoB.getQuadro04().getAnexoBq04C406().longValue() / 100.0 : 0.0);
        values.put("VendasMercadoAD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C409() != null ? (double)anexoB.getQuadro04().getAnexoBq04C409().longValue() / 100.0 : 0.0);
        values.put("OutrosServicosAD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C410() != null ? (double)anexoB.getQuadro04().getAnexoBq04C410().longValue() / 100.0 : 0.0);
        values.put("SubsidiosExplAD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C411() != null ? (double)anexoB.getQuadro04().getAnexoBq04C411().longValue() / 100.0 : 0.0);
        values.put("OutrosSubsidiosAD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C426() != null ? (double)anexoB.getQuadro04().getAnexoBq04C426().longValue() / 100.0 : 0.0);
        values.put("RendimABD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C3() != null ? (double)anexoB.getQuadro04().getAnexoBq04C3().longValue() / 100.0 : 0.0);
        values.put("CustosExistABD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null ? (double)anexoB.getQuadro04().getAnexoBq04C413().longValue() / 100.0 : 0.0);
        values.put("TributacaoCatAD3", anexoB != null && anexoB.getQuadro04().isAnexoBq04B2OPSelected() ? 1 : 0);
        values.put("RendPredImpD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C421() != null ? (double)anexoB.getQuadro04().getAnexoBq04C421().longValue() / 100.0 : 0.0);
        values.put("RendCapImpD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C422() != null ? (double)anexoB.getQuadro04().getAnexoBq04C422().longValue() / 100.0 : 0.0);
        values.put("RendimCIRSPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C440() != null ? (double)anexoB.getQuadro04().getAnexoBq04C440().longValue() / 100.0 : 0.0);
        values.put("PropIndustrialPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C441() != null ? (double)anexoB.getQuadro04().getAnexoBq04C441().longValue() / 100.0 : 0.0);
        values.put("SaldoPositivoPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C442() != null ? (double)anexoB.getQuadro04().getAnexoBq04C442().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBPD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C443() != null ? (double)anexoB.getQuadro04().getAnexoBq04C443().longValue() / 100.0 : 0.0);
        values.put("PrestServicosAD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C444() != null ? (double)anexoB.getQuadro04().getAnexoBq04C444().longValue() / 100.0 : 0.0);
        values.put("RendDiversosBAD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C445() != null ? (double)anexoB.getQuadro04().getAnexoBq04C445().longValue() / 100.0 : 0.0);
        values.put("OutrosCatBAD3", anexoB != null && anexoB.getQuadro04().getAnexoBq04C446() != null ? (double)anexoB.getQuadro04().getAnexoBq04C446().longValue() / 100.0 : 0.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C406() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C406().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C901().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C902().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C903().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C907().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C908().longValue();
        }
        values.put("DespesaPBD3", (double)soma / 100.0);
        soma = 0;
        if (anexoB != null && anexoB.getQuadro04().getAnexoBq04C413() != null) {
            soma+=anexoB.getQuadro04().getAnexoBq04C413().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C910().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C911().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C912().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C916().longValue();
        }
        if (anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null) {
            soma+=anexoB.getQuadro09().getAnexoBq09C917().longValue();
        }
        values.put("DespesaABD3", (double)soma / 100.0);
        values.put("RetFonteBD3", anexoB != null && anexoB.getQuadro07().getAnexoBq07C702() != null ? (double)anexoB.getQuadro07().getAnexoBq07C702().longValue() / 100.0 : 0.0);
        values.put("PagamenContaBD3", anexoB != null && anexoB.getQuadro07().getAnexoBq07C703() != null ? (double)anexoB.getQuadro07().getAnexoBq07C703().longValue() / 100.0 : 0.0);
        values.put("EncViaturaPBD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C901() != null ? (double)anexoB.getQuadro09().getAnexoBq09C901().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalPBD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C902() != null ? (double)anexoB.getQuadro09().getAnexoBq09C902().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoPBD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C903() != null ? (double)anexoB.getQuadro09().getAnexoBq09C903().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesPBD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C907() != null ? (double)anexoB.getQuadro09().getAnexoBq09C907().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgPBD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C908() != null ? (double)anexoB.getQuadro09().getAnexoBq09C908().longValue() / 100.0 : 0.0);
        values.put("EncViaturaABD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C910() != null ? (double)anexoB.getQuadro09().getAnexoBq09C910().longValue() / 100.0 : 0.0);
        values.put("ValProfissionalABD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C911() != null ? (double)anexoB.getQuadro09().getAnexoBq09C911().longValue() / 100.0 : 0.0);
        values.put("DespRepresentacaoABD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C912() != null ? (double)anexoB.getQuadro09().getAnexoBq09C912().longValue() / 100.0 : 0.0);
        values.put("DeslocacoesABD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C916() != null ? (double)anexoB.getQuadro09().getAnexoBq09C916().longValue() / 100.0 : 0.0);
        values.put("OutrosEncgABD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C917() != null ? (double)anexoB.getQuadro09().getAnexoBq09C917().longValue() / 100.0 : 0.0);
        values.put("DespConfTAD3", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1001() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1001().longValue() / 100.0) : null));
        values.put("ImpNaoResiTAD3", (anexoB != null && anexoB.getQuadro10().getAnexoBq10C1002() != null ? Double.valueOf((double)anexoB.getQuadro10().getAnexoBq10C1002().longValue() / 100.0) : null));
        values.put("ContObrSegSocPD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C904() != null ? (double)anexoB.getQuadro09().getAnexoBq09C904().longValue() / 100.0 : 0.0);
        values.put("QuotSindPD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C905() != null ? (double)anexoB.getQuadro09().getAnexoBq09C905().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfPD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C906() != null ? (double)anexoB.getQuadro09().getAnexoBq09C906().longValue() / 100.0 : 0.0);
        values.put("ContObrSegSocAD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C913() != null ? (double)anexoB.getQuadro09().getAnexoBq09C913().longValue() / 100.0 : 0.0);
        values.put("QuotSindAD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C914() != null ? (double)anexoB.getQuadro09().getAnexoBq09C914().longValue() / 100.0 : 0.0);
        values.put("QuotOrdProfAD3", anexoB != null && anexoB.getQuadro09().getAnexoBq09C915() != null ? (double)anexoB.getQuadro09().getAnexoBq09C915().longValue() / 100.0 : 0.0);
    }

    private void putDefaultValues(SimulatorHashtable<String, Object> values) {
        values.putDefaultValue("RFSTASPA", DOUBLE_ZERO);
        values.putDefaultValue("RFSTASPB", DOUBLE_ZERO);
        values.putDefaultValue("RFSTASPF", DOUBLE_ZERO);
        values.putDefaultValue("RFSTAD1", DOUBLE_ZERO);
        values.putDefaultValue("RFSTAD2", DOUBLE_ZERO);
        values.putDefaultValue("RFSTAD3", DOUBLE_ZERO);
        values.putDefaultValue("RFSTHSPA", DOUBLE_ZERO);
        values.putDefaultValue("RFSTHSPB", DOUBLE_ZERO);
        values.putDefaultValue("RFSTHSPF", DOUBLE_ZERO);
        values.putDefaultValue("RFSTHD1", DOUBLE_ZERO);
        values.putDefaultValue("RFSTHD2", DOUBLE_ZERO);
        values.putDefaultValue("RFSTHD3", DOUBLE_ZERO);
        values.putDefaultValue("RendAufDefCD1", DOUBLE_ZERO);
        values.putDefaultValue("RendAufDefCD2", DOUBLE_ZERO);
        values.putDefaultValue("RendAufDefCD3", DOUBLE_ZERO);
        values.putDefaultValue("RendAufDefCSPA", DOUBLE_ZERO);
        values.putDefaultValue("RendAufDefCSPB", DOUBLE_ZERO);
        values.putDefaultValue("RendPagaSenhor", DOUBLE_ZERO);
        values.putDefaultValue("RendRecebSub", DOUBLE_ZERO);
        values.putDefaultValue("TributacaoCatAD1", DOUBLE_ZERO);
        values.putDefaultValue("TributacaoCatAD2", DOUBLE_ZERO);
        values.putDefaultValue("TributacaoCatAD3", DOUBLE_ZERO);
        values.putDefaultValue("TributacaoCatASPA", DOUBLE_ZERO);
        values.putDefaultValue("TributacaoCatASPB", DOUBLE_ZERO);
        values.putDefaultValue("DividEmp", DOUBLE_ZERO);
        values.putDefaultValue("ReinvParc", DOUBLE_ZERO);
        values.putDefaultValue("DespConfTASPA", DOUBLE_ZERO);
        values.putDefaultValue("DespRepreTASPA", DOUBLE_ZERO);
        values.putDefaultValue("EncargViaturTASPA", DOUBLE_ZERO);
        values.putDefaultValue("ImpNaoResiTASPA", DOUBLE_ZERO);
        values.putDefaultValue("AjudasCustoTASPA", DOUBLE_ZERO);
        values.putDefaultValue("DespConfTASPB", DOUBLE_ZERO);
        values.putDefaultValue("DespRepreTASPB", DOUBLE_ZERO);
        values.putDefaultValue("EncargViaturTASPB", DOUBLE_ZERO);
        values.putDefaultValue("ImpNaoResiTASPB", DOUBLE_ZERO);
        values.putDefaultValue("AjudasCustoTASPB", DOUBLE_ZERO);
        values.putDefaultValue("DespConfTAD1", DOUBLE_ZERO);
        values.putDefaultValue("AjudasCustoTAD1", DOUBLE_ZERO);
        values.putDefaultValue("AjudasCustoTAD2", DOUBLE_ZERO);
        values.putDefaultValue("AjudasCustoTAD3", DOUBLE_ZERO);
        values.putDefaultValue("DespConfTAD2", DOUBLE_ZERO);
        values.putDefaultValue("DespConfTAD3", DOUBLE_ZERO);
        values.putDefaultValue("DespRepreTAD1", DOUBLE_ZERO);
        values.putDefaultValue("DespRepreTAD2", DOUBLE_ZERO);
        values.putDefaultValue("DespRepreTAD3", DOUBLE_ZERO);
        values.putDefaultValue("EncargViaturTAD1", DOUBLE_ZERO);
        values.putDefaultValue("EncargViaturTAD2", DOUBLE_ZERO);
        values.putDefaultValue("EncargViaturTAD3", DOUBLE_ZERO);
        values.putDefaultValue("ImpNaoResiTAD1", DOUBLE_ZERO);
        values.putDefaultValue("ImpNaoResiTAD2", DOUBLE_ZERO);
        values.putDefaultValue("ImpNaoResiTAD3", DOUBLE_ZERO);
        values.putDefaultValue("NaturRendBSPA", Character.valueOf('1'));
        values.putDefaultValue("SimpVSIsolBSPA", 1.0);
        values.putDefaultValue("TipoRendBSPA", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfBSPA", 1.0);
        values.putDefaultValue("VendasMercadoPSPA", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotPSPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosPSPA", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntPSPA", DOUBLE_ZERO);
        values.putDefaultValue("RendimActFinPSPA", DOUBLE_ZERO);
        values.putDefaultValue("ServPrestSocPSPA", DOUBLE_ZERO);
        values.putDefaultValue("MicroProdElecPSPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosPSPA", DOUBLE_ZERO);
        values.putDefaultValue("RendimPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoASPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosASPA", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplASPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosASPA", DOUBLE_ZERO);
        values.putDefaultValue("RendimABSPA", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistABSPA", DOUBLE_ZERO);
        values.putDefaultValue("DespesaPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("DespesaABSPA", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteBSPA", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaBSPA", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgPBSPA", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaABSPA", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalABSPA", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoABSPA", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesABSPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgABSPA", DOUBLE_ZERO);
        values.putDefaultValue("NaturRendBSPB", Character.valueOf('1'));
        values.putDefaultValue("SimpVSIsolBSPB", 1.0);
        values.putDefaultValue("TipoRendBSPB", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfBSPB", 1.0);
        values.putDefaultValue("VendasMercadoPSPB", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotPSPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosPSPB", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntPSPB", DOUBLE_ZERO);
        values.putDefaultValue("RendimActFinPSPB", DOUBLE_ZERO);
        values.putDefaultValue("ServPrestSocPSPB", DOUBLE_ZERO);
        values.putDefaultValue("MicroProdElecPSPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosPSPB", DOUBLE_ZERO);
        values.putDefaultValue("RendimPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoASPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosASPB", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplASPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosASPB", DOUBLE_ZERO);
        values.putDefaultValue("RendimABSPB", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistABSPB", DOUBLE_ZERO);
        values.putDefaultValue("DespesaPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("DespesaABSPB", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteBSPB", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaBSPB", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgPBSPB", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaABSPB", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalABSPB", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoABSPB", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesABSPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgABSPB", DOUBLE_ZERO);
        values.putDefaultValue("NaturRendBD1", Character.valueOf('1'));
        values.putDefaultValue("SimpVSIsolBD1", 1.0);
        values.putDefaultValue("TipoRendBD1", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfBD1", 1.0);
        values.putDefaultValue("VendasMercadoPD1", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotPD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosPD1", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntPD1", DOUBLE_ZERO);
        values.putDefaultValue("RendimActFinPD1", DOUBLE_ZERO);
        values.putDefaultValue("ServPrestSocPD1", DOUBLE_ZERO);
        values.putDefaultValue("MicroProdElecPD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosPD1", DOUBLE_ZERO);
        values.putDefaultValue("RendimPBD1", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistPBD1", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoAD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosAD1", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplAD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosAD1", DOUBLE_ZERO);
        values.putDefaultValue("RendimABD1", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistABD1", DOUBLE_ZERO);
        values.putDefaultValue("DespesaPBD1", DOUBLE_ZERO);
        values.putDefaultValue("DespesaABD1", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteBD1", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaBD1", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaPBD1", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalPBD1", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoPBD1", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesPBD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgPBD1", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaABD1", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalABD1", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoABD1", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesABD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgABD1", DOUBLE_ZERO);
        values.putDefaultValue("NaturRendBD2", Character.valueOf('1'));
        values.putDefaultValue("SimpVSIsolBD2", 1.0);
        values.putDefaultValue("TipoRendBD2", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfBD2", 1.0);
        values.putDefaultValue("VendasMercadoPD2", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotPD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosPD2", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntPD2", DOUBLE_ZERO);
        values.putDefaultValue("RendimActFinPD2", DOUBLE_ZERO);
        values.putDefaultValue("ServPrestSocPD2", DOUBLE_ZERO);
        values.putDefaultValue("MicroProdElecPD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosPD2", DOUBLE_ZERO);
        values.putDefaultValue("RendimPBD2", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistPBD2", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoAD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosAD2", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplAD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosAD2", DOUBLE_ZERO);
        values.putDefaultValue("RendimABD2", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistABD2", DOUBLE_ZERO);
        values.putDefaultValue("DespesaPBD2", DOUBLE_ZERO);
        values.putDefaultValue("DespesaABD2", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteBD2", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaBD2", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaPBD2", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalPBD2", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoPBD2", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesPBD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgPBD2", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaABD2", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalABD2", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoABD2", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesABD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgABD2", DOUBLE_ZERO);
        values.putDefaultValue("NaturRendBD3", Character.valueOf('1'));
        values.putDefaultValue("SimpVSIsolBD3", 1.0);
        values.putDefaultValue("TipoRendBD3", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfBD3", 1.0);
        values.putDefaultValue("VendasMercadoPD3", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotPD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosPD3", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntPD3", DOUBLE_ZERO);
        values.putDefaultValue("RendimActFinPD3", DOUBLE_ZERO);
        values.putDefaultValue("ServPrestSocPD3", DOUBLE_ZERO);
        values.putDefaultValue("MicroProdElecPD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosPD3", DOUBLE_ZERO);
        values.putDefaultValue("RendimPBD3", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistPBD3", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoAD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosAD3", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplAD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosSubsidiosAD3", DOUBLE_ZERO);
        values.putDefaultValue("RendimABD3", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistABD3", DOUBLE_ZERO);
        values.putDefaultValue("DespesaPBD3", DOUBLE_ZERO);
        values.putDefaultValue("DespesaABD3", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteBD3", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaBD3", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaPBD3", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalPBD3", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoPBD3", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesPBD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgPBD3", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaABD3", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalABD3", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoABD3", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesABD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgABD3", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendCSPA", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfCSPA", 1.0);
        values.putDefaultValue("LucroPrejProCSPA", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrCSPA", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejActFinCSPA", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteCSPA", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaCSPA", DOUBLE_ZERO);
        values.putDefaultValue("AgriVSProfCSPB", 1.0);
        values.putDefaultValue("TipoRendCSPB", Character.valueOf('2'));
        values.putDefaultValue("LucroPrejProCSPB", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrCSPB", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejActFinCSPB", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteCSPB", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaCSPB", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendCD1", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfCD1", 1.0);
        values.putDefaultValue("LucroPrejProCD1", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrCD1", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejActFinCD1", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteCD1", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaCD1", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendCD2", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfCD2", 1.0);
        values.putDefaultValue("LucroPrejProCD2", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrCD2", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejActFinCD2", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteCD2", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaCD2", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendCD3", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfCD3", 1.0);
        values.putDefaultValue("LucroPrejProCD3", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrCD3", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejActFinCD3", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteCD3", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaCD3", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendDSPA", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfDSPA", 1.0);
        values.putDefaultValue("LucroPrejProDSPA", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrDSPA", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteDSPA", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaDSPA", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendDSPB", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfDSPB", 1.0);
        values.putDefaultValue("LucroPrejProDSPB", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrDSPB", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteDSPB", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaDSPB", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendDD1", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfDD1", 1.0);
        values.putDefaultValue("LucroPrejProDD1", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrDD1", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteDD1", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaDD1", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendDD2", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfDD2", 1.0);
        values.putDefaultValue("LucroPrejProDD2", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrDD2", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteDD2", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaDD2", DOUBLE_ZERO);
        values.putDefaultValue("TipoRendDD3", Character.valueOf('2'));
        values.putDefaultValue("AgriVSProfDD3", 1.0);
        values.putDefaultValue("LucroPrejProDD3", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejAgrDD3", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteDD3", DOUBLE_ZERO);
        values.putDefaultValue("PagamenContaDD3", DOUBLE_ZERO);
        values.putDefaultValue("CapJurSupr", DOUBLE_ZERO);
        values.putDefaultValue("CapLucros", DOUBLE_ZERO);
        values.putDefaultValue("CapResgFPR", DOUBLE_ZERO);
        values.putDefaultValue("CapResgPPA", DOUBLE_ZERO);
        values.putDefaultValue("CapJurPrem", DOUBLE_ZERO);
        values.putDefaultValue("CapJurDep", DOUBLE_ZERO);
        values.putDefaultValue("CapOutCap1", DOUBLE_ZERO);
        values.putDefaultValue("CapOutCap2", DOUBLE_ZERO);
        values.putDefaultValue("CapRF", DOUBLE_ZERO);
        values.putDefaultValue("PredTotRend", DOUBLE_ZERO);
        values.putDefaultValue("PredRF", DOUBLE_ZERO);
        values.putDefaultValue("PredManut", DOUBLE_ZERO);
        values.putDefaultValue("PredConserv", DOUBLE_ZERO);
        values.putDefaultValue("TaxasAutarq", DOUBLE_ZERO);
        values.putDefaultValue("PredCAutarq", DOUBLE_ZERO);
        values.putDefaultValue("PredCondomi", DOUBLE_ZERO);
        values.putDefaultValue("PredPerdas", DOUBLE_ZERO);
        values.putDefaultValue("PredRustic", DOUBLE_ZERO);
        values.putDefaultValue("EnglobG", Character.valueOf('1'));
        values.putDefaultValue("DataReaBem1", "");
        values.putDefaultValue("DataAquBem1", "");
        values.putDefaultValue("ValReaBem1", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBem1", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBem1", DOUBLE_ZERO);
        values.putDefaultValue("DataReaBem2", "");
        values.putDefaultValue("DataAquBem2", "");
        values.putDefaultValue("ValReaBem2", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBem2", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBem2", DOUBLE_ZERO);
        values.putDefaultValue("DataReaBem3", "");
        values.putDefaultValue("DataAquBem3", "");
        values.putDefaultValue("ValReaBem3", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBem3", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBem3", DOUBLE_ZERO);
        values.putDefaultValue("DataReaBem4", "");
        values.putDefaultValue("DataAquBem4", "");
        values.putDefaultValue("ValReaBem4", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBem4", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBem4", DOUBLE_ZERO);
        values.putDefaultValue("MVTABImov", DOUBLE_ZERO);
        values.putDefaultValue("MVTRBImov", DOUBLE_ZERO);
        values.putDefaultValue("MVTDBImov", DOUBLE_ZERO);
        values.putDefaultValue("MVTRPInte", DOUBLE_ZERO);
        values.putDefaultValue("MVTAPInte", DOUBLE_ZERO);
        values.putDefaultValue("MVTDPInte", DOUBLE_ZERO);
        values.putDefaultValue("MVTRPCont", DOUBLE_ZERO);
        values.putDefaultValue("MVTAPCont", DOUBLE_ZERO);
        values.putDefaultValue("MVTRPartS", DOUBLE_ZERO);
        values.putDefaultValue("MVTAPartS", DOUBLE_ZERO);
        values.putDefaultValue("MVTDPartS", DOUBLE_ZERO);
        values.putDefaultValue("MVTFinanD", DOUBLE_ZERO);
        values.putDefaultValue("OFoutros", DOUBLE_ZERO);
        values.putDefaultValue("OFi4", DOUBLE_ZERO);
        values.putDefaultValue("OFcertif", DOUBLE_ZERO);
        values.putDefaultValue("IndemDanos", DOUBLE_ZERO);
        values.putDefaultValue("IndemDanosRF", DOUBLE_ZERO);
        values.putDefaultValue("ImpNConc", DOUBLE_ZERO);
        values.putDefaultValue("ImpNConcRF", DOUBLE_ZERO);
        values.putDefaultValue("MVTIncPat", DOUBLE_ZERO);
        values.putDefaultValue("MVTIncPatRF", DOUBLE_ZERO);
        values.putDefaultValue("MVPerdas", DOUBLE_ZERO);
        values.putDefaultValue("ContPlPeASPA", DOUBLE_ZERO);
        values.putDefaultValue("ContPlPeASPB", DOUBLE_ZERO);
        values.putDefaultValue("ContPlPeASPF", DOUBLE_ZERO);
        values.putDefaultValue("ContPlPeAD1", DOUBLE_ZERO);
        values.putDefaultValue("ContPlPeAD2", DOUBLE_ZERO);
        values.putDefaultValue("ContPlPeAD3", DOUBLE_ZERO);
        values.putDefaultValue("ValReaBemRec1", DOUBLE_ZERO);
        values.putDefaultValue("ValReaBemRec2", DOUBLE_ZERO);
        values.putDefaultValue("ValReaBemRec3", DOUBLE_ZERO);
        values.putDefaultValue("ValReaBemRec4", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBemRec1", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBemRec2", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBemRec3", DOUBLE_ZERO);
        values.putDefaultValue("ValAquBemRec4", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBemRec1", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBemRec2", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBemRec3", DOUBLE_ZERO);
        values.putDefaultValue("ValEncBemRec4", DOUBLE_ZERO);
        values.putDefaultValue("DataReaBemRec1", "");
        values.putDefaultValue("DataReaBemRec2", "");
        values.putDefaultValue("DataReaBemRec3", "");
        values.putDefaultValue("DataReaBemRec4", "");
        values.putDefaultValue("DataAquBemRec1", "");
        values.putDefaultValue("DataAquBemRec2", "");
        values.putDefaultValue("DataAquBemRec3", "");
        values.putDefaultValue("DataAquBemRec4", "");
        values.putDefaultValue("ContObrSegSocPSPA", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocPSPB", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocPD1", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocPD2", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocPD3", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindPSPA", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindPSPB", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindPD1", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindPD2", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindPD3", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfPSPA", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfPSPB", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfPD1", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfPD2", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfPD3", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocASPA", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocASPB", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocAD1", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocAD2", DOUBLE_ZERO);
        values.putDefaultValue("ContObrSegSocAD3", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindASPA", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindASPB", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindAD1", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindAD2", DOUBLE_ZERO);
        values.putDefaultValue("QuotSindAD3", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfASPA", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfASPB", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfAD1", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfAD2", DOUBLE_ZERO);
        values.putDefaultValue("QuotOrdProfAD3", DOUBLE_ZERO);
        values.putDefaultValue("RendPredImpSPA", DOUBLE_ZERO);
        values.putDefaultValue("RendPredImpSPB", DOUBLE_ZERO);
        values.putDefaultValue("RendPredImpD1", DOUBLE_ZERO);
        values.putDefaultValue("RendPredImpD2", DOUBLE_ZERO);
        values.putDefaultValue("RendPredImpD3", DOUBLE_ZERO);
        values.putDefaultValue("RendCapImpSPA", DOUBLE_ZERO);
        values.putDefaultValue("RendCapImpSPB", DOUBLE_ZERO);
        values.putDefaultValue("RendCapImpD1", DOUBLE_ZERO);
        values.putDefaultValue("RendCapImpD2", DOUBLE_ZERO);
        values.putDefaultValue("RendCapImpD3", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpPSPA", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpPSPB", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpPD1", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpPD2", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpPD3", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosPSPA", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosPSPB", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosPD1", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosPD2", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosPD3", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpASPA", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpASPB", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpAD1", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpAD2", DOUBLE_ZERO);
        values.putDefaultValue("RendLiqImpAD3", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosASPA", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosASPB", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosAD1", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosAD2", DOUBLE_ZERO);
        values.putDefaultValue("AdContLucrosAD3", DOUBLE_ZERO);
        values.putDefaultValue("RendFundCapRisco", DOUBLE_ZERO);
        values.putDefaultValue("TotalRendasImv", DOUBLE_ZERO);
        values.putDefaultValue("DespManutencaoImv", DOUBLE_ZERO);
        values.putDefaultValue("DespConservacaoImv", DOUBLE_ZERO);
        values.putDefaultValue("TaxasAutarqImv", DOUBLE_ZERO);
        values.putDefaultValue("ContribAutarqImv", DOUBLE_ZERO);
        values.putDefaultValue("DespCondominioImv", DOUBLE_ZERO);
        values.putDefaultValue("RetFonteFImv", DOUBLE_ZERO);
        values.putDefaultValue("EnglobamentoF", Character.valueOf('1'));
        values.putDefaultValue("EnglobImvG", Character.valueOf('1'));
        values.putDefaultValue("AjustamentosPSPA", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosPSPB", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosPD1", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosPD2", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosPD3", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosASPA", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosASPB", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosAD1", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosAD2", DOUBLE_ZERO);
        values.putDefaultValue("AjustamentosAD3", DOUBLE_ZERO);
        values.putDefaultValue("RendAnosAntcatF", DOUBLE_ZERO);
        values.putDefaultValue("NAnoscatF", 0);
        values.putDefaultValue("EqEnergRenovaveis", DOUBLE_ZERO);
        values.putDefaultValue("AbatEnergRen", DOUBLE_ZERO);
        values.putDefaultValue("ObrasMelhTermico", DOUBLE_ZERO);
        values.putDefaultValue("VeiculosNaoPoluentes", DOUBLE_ZERO);
        values.putDefaultValue("RendasLocFinan", DOUBLE_ZERO);
        values.putDefaultValue("ContDefVelh", DOUBLE_ZERO);
        values.putDefaultValue("NumBemAlienado", "0");
        values.putDefaultValue("DefFAD1", false);
        values.putDefaultValue("DefFAD2", false);
        values.putDefaultValue("DefFAD3", false);
        values.putDefaultValue("AbatAconsJur", (double)DOUBLE_ZERO);
        values.putDefaultValue("CapRendAcco", DOUBLE_ZERO);
        values.putDefaultValue("CapCredImp", DOUBLE_ZERO);
        values.putDefaultValue("MVMaisVal", DOUBLE_ZERO);
        values.putDefaultValue("MVMenosVal", DOUBLE_ZERO);
        values.putDefaultValue("MVIRSimp", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoBSPA", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoBSPB", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoBD1", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoBD2", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoBD3", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejCSPA", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejCSPB", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejCD1", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejCD2", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejCD3", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoCSPA", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoCSPB", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoCD1", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoCD2", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoCD3", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejDSPA", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejDSPB", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejDD1", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejDD2", DOUBLE_ZERO);
        values.putDefaultValue("LucroPrejDD3", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoDSPA", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoDSPB", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoDD1", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoDD2", DOUBLE_ZERO);
        values.putDefaultValue("CredImpostoDD3", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoSPA", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoSPB", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoD1", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoD2", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoD3", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotSPA", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotSPB", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotD1", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotD2", DOUBLE_ZERO);
        values.putDefaultValue("ServicosHotD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosSPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosSPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosD3", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplSPA", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplSPB", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplD1", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplD2", DOUBLE_ZERO);
        values.putDefaultValue("SubsidiosExplD3", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntSPA", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntSPB", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntD1", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntD2", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntD3", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoRASPA", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoRASPB", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoRAD1", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoRAD2", DOUBLE_ZERO);
        values.putDefaultValue("VendasMercadoRAD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosRASPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosRASPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosRAD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosRAD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosServicosRAD3", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntRASPA", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntRASPB", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntRAD1", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntRAD2", DOUBLE_ZERO);
        values.putDefaultValue("PropriedadeIntRAD3", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistRASPA", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistRASPB", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistRAD1", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistRAD2", DOUBLE_ZERO);
        values.putDefaultValue("CustosExistRAD3", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaRASPA", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaRASPB", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaRAD1", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaRAD2", DOUBLE_ZERO);
        values.putDefaultValue("EncViaturaRAD3", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalRASPA", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalRASPB", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalRAD1", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalRAD2", DOUBLE_ZERO);
        values.putDefaultValue("ValProfissionalRAD3", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoRASPA", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoRASPB", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoRAD1", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoRAD2", DOUBLE_ZERO);
        values.putDefaultValue("DespRepresentacaoRAD3", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesRASPA", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesRASPB", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesRAD1", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesRAD2", DOUBLE_ZERO);
        values.putDefaultValue("DeslocacoesRAD3", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgRASPA", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgRASPB", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgRAD1", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgRAD2", DOUBLE_ZERO);
        values.putDefaultValue("OutrosEncgRAD3", DOUBLE_ZERO);
        values.putDefaultValue("AcrescPatrim", DOUBLE_ZERO);
        values.putDefaultValue("art62n1", DOUBLE_ZERO);
        values.putDefaultValue("art62n2", DOUBLE_ZERO);
    }

    private static short getGrauInvalidezFromNif(RostoModel rosto, long nif) {
        long SujA = rosto.getQuadro03().getQ03C03() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03C03())) ? rosto.getQuadro03().getQ03C03() : 0;
        short GrauI_SujA = rosto.getQuadro03().getQ03C03a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03C03a())) ? rosto.getQuadro03().getQ03C03a().shortValue() : 0;
        long SujB = rosto.getQuadro03().getQ03C04() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03C04())) ? rosto.getQuadro03().getQ03C04() : 0;
        short GrauI_SujB = rosto.getQuadro03().getQ03C04a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03C04a())) ? rosto.getQuadro03().getQ03C04a().shortValue() : 0;
        long DD1 = rosto.getQuadro03().getQ03CDD1() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD1())) ? rosto.getQuadro03().getQ03CDD1() : 0;
        short GrauI_DD1 = rosto.getQuadro03().getQ03CDD1a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD1a())) ? rosto.getQuadro03().getQ03CDD1a().shortValue() : 0;
        long DD2 = rosto.getQuadro03().getQ03CDD2() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD2())) ? rosto.getQuadro03().getQ03CDD2() : 0;
        short GrauI_DD2 = rosto.getQuadro03().getQ03CDD2a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD2a())) ? rosto.getQuadro03().getQ03CDD2a().shortValue() : 0;
        long DD3 = rosto.getQuadro03().getQ03CDD3() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD3())) ? rosto.getQuadro03().getQ03CDD3() : 0;
        short GrauI_DD3 = rosto.getQuadro03().getQ03CDD3a() != null && !StringUtil.isEmpty(String.valueOf(rosto.getQuadro03().getQ03CDD3a())) ? rosto.getQuadro03().getQ03CDD3a().shortValue() : 0;
        boolean isDGC = false;
        short GrauI_DGC = 0;
        if (!(rosto.getQuadro03().getRostoq03DT1() == null || rosto.getQuadro03().getRostoq03DT1().isEmpty())) {
            for (Rostoq03DT1_Linha linha : rosto.getQuadro03().getRostoq03DT1()) {
                if (linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getNIF()) || !linha.getNIF().equals(nif)) continue;
                isDGC = true;
                GrauI_DGC = !Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau()) ? linha.getDeficienteGrau().shortValue() : 0;
                break;
            }
        }
        if (SujA == nif) {
            return GrauI_SujA;
        }
        if (SujB == nif) {
            return GrauI_SujB;
        }
        if (DD1 == nif) {
            return GrauI_DD1;
        }
        if (DD2 == nif) {
            return GrauI_DD2;
        }
        if (DD3 == nif) {
            return GrauI_DD3;
        }
        if (isDGC) {
            return GrauI_DGC;
        }
        return 0;
    }

    private static Character fromPercentToLetter(char defaultChar, short number) {
        if (number == 0) {
            return Character.valueOf(defaultChar);
        }
        if (number > 89) {
            return Character.valueOf('2');
        }
        if (number >= 60) {
            return Character.valueOf('1');
        }
        return Character.valueOf('0');
    }

    private String formatAmmount(double valueD) {
        return this.formatAmmount(this.convertToCents(valueD));
    }

    private String formatAmmount(long value) {
        if (value < 100 && value >= 0) {
            return (value < 10 ? "0,0" : "0,") + value;
        }
        return new EuroField("euro", value).format();
    }

    private long convertToCents(double value) {
        return new BigDecimal(value).movePointRight(2).setScale(0, 4).longValue();
    }

    private static Set<String> getDependentesComInvalidez(Modelo3IRSv2015Model model) {
        RostoModel rosto = model.getRosto();
        Quadro03 rostoQuadro03 = rosto.getQuadro03();
        HashSet<String> dependentesComInvalidez = new HashSet<String>();
        if (rostoQuadro03 != null) {
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD1())) {
                dependentesComInvalidez.add("DD1");
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD2())) {
                dependentesComInvalidez.add("DD2");
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD3())) {
                dependentesComInvalidez.add("DD3");
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD4())) {
                dependentesComInvalidez.add("DD4");
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD5())) {
                dependentesComInvalidez.add("DD5");
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD6())) {
                dependentesComInvalidez.add("DD6");
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD7())) {
                dependentesComInvalidez.add("DD7");
            }
            if (!Modelo3IRSValidatorUtil.isEmptyOrZero(rostoQuadro03.getQ03CDD8())) {
                dependentesComInvalidez.add("DD8");
            }
            if (!(rosto.getQuadro03().getRostoq03DT1() == null || rosto.getQuadro03().getRostoq03DT1().isEmpty())) {
                for (int i = 0; i < rosto.getQuadro03().getRostoq03DT1().size(); ++i) {
                    Rostoq03DT1_Linha linha = rosto.getQuadro03().getRostoq03DT1().get(i);
                    if (linha == null || Modelo3IRSValidatorUtil.isEmptyOrZero(linha.getDeficienteGrau())) continue;
                    dependentesComInvalidez.add("DG" + (i + 1));
                }
            }
        }
        return dependentesComInvalidez;
    }

    private static Set<String> getDependentesComRendimentos(Modelo3IRSv2015Model model) {
        String prefixoTitular = "D";
        return SimulateUtil.getTitularesComRendimentos(model, prefixoTitular);
    }

    private static Set<String> getTitularesComRendimentos(Modelo3IRSv2015Model model, String prefixoTitular) {
        List<AnexoCModel> anexosC;
        List<AnexoBModel> anexosB;
        List<AnexoDModel> anexosD;
        RostoModel rosto = model.getRosto();
        HashSet<String> dependentesComRendimentos = new HashSet<String>();
        AnexoAModel anexoA = model.getAnexoA();
        if (anexoA != null) {
            for (AnexoAq04T4A_Linha linha4A : anexoA.getQuadro04().getAnexoAq04T4A()) {
                String titular = linha4A.getTitular();
                if (titular == null || !titular.startsWith(prefixoTitular)) continue;
                dependentesComRendimentos.add(titular);
            }
        }
        if (!ListUtil.isEmpty(anexosB = model.getAnexoB())) {
            for (AnexoBModel anexoB : anexosB) {
                String titular = rosto.getTitularByNIF(Long.parseLong(anexoB.getFormKey().getSubId()));
                if (titular == null || !titular.startsWith(prefixoTitular)) continue;
                dependentesComRendimentos.add(titular);
            }
        }
        if (!ListUtil.isEmpty(anexosC = model.getAnexoC())) {
            for (AnexoCModel anexoC : anexosC) {
                String titular = rosto.getTitularByNIF(Long.parseLong(anexoC.getFormKey().getSubId()));
                if (titular == null || !titular.startsWith(prefixoTitular)) continue;
                dependentesComRendimentos.add(titular);
            }
        }
        if (!ListUtil.isEmpty(anexosD = model.getAnexoD())) {
            for (AnexoDModel anexoD : anexosD) {
                String titular = rosto.getTitularByNIF(Long.parseLong(anexoD.getFormKey().getSubId()));
                if (titular == null || !titular.startsWith(prefixoTitular)) continue;
                dependentesComRendimentos.add(titular);
            }
        }
        return dependentesComRendimentos;
    }

    private boolean isEmptyValue(Object value) {
        boolean isEmptyValue;
        try {
            isEmptyValue = value instanceof Double && (Double)value == 0.0 || value instanceof String && Double.parseDouble(((String)value).replace((CharSequence)",", (CharSequence)".")) == 0.0;
        }
        catch (NumberFormatException nfe) {
            isEmptyValue = StringUtil.isEmpty((String)value);
        }
        return isEmptyValue;
    }

    public class HtReader {
        public void printDifferences(Map ht1, Map ht2) {
            Object value;
            Object key;
            TreeMap sht1 = new TreeMap(ht1);
            TreeMap sht2 = new TreeMap(ht2);
            StringBuffer buf = new StringBuffer();
            buf.append("{\n");
            for (Map.Entry e2 : sht1.entrySet()) {
                key = e2.getKey();
                value = e2.getValue();
                if (!(ht2.get(key) != null || SimulateUtil.this.isEmptyValue(value))) {
                    buf.append("Online: ").append(key).append("=").append(value).append("; Offline: null\n");
                    continue;
                }
                if (ht2.get(key) == null) continue;
                if (!(ht2.get(key).equals(value) || value instanceof Integer && ht2.get(key) instanceof Double && ((Integer)value).intValue() == ((Double)ht2.get(key)).intValue())) {
                    buf.append("Online: ").append(key).append("=").append(value).append("; Offline: ").append(key).append("=").append(ht2.get(key)).append("\n");
                }
                sht2.remove(key);
            }
            if (!sht2.isEmpty()) {
                for (Map.Entry e2 : sht2.entrySet()) {
                    key = e2.getKey();
                    value = e2.getValue();
                    if (SimulateUtil.this.isEmptyValue(value)) continue;
                    buf.append("Online: null;   Offline: ").append(key).append("=").append(value).append("\n");
                }
            }
            buf.append("}");
            SimpleLog.log(buf.toString());
        }
    }

}

