/*
 * Decompiled with CFR 0_102.
 */
package pt.dgci.modelo3irs.v2015.simulador.util;

import java.util.Hashtable;

public class SimulatorHashtable<K, V>
extends Hashtable<K, V> {
    private static final long serialVersionUID = -98135570255136485L;

    @Override
    public V put(K key, V value) {
        if (value != null) {
            return super.put(key, value);
        }
        return null;
    }

    public V putDefaultValue(K key, V value) {
        Object valueInHT = this.get(key);
        if (valueInHT != null) {
            return valueInHT;
        }
        return this.put(key, value);
    }
}

