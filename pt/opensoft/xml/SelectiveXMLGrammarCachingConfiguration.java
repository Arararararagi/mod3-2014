/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.xml;

import java.io.IOException;
import org.apache.xerces.impl.XMLErrorReporter;
import org.apache.xerces.impl.dtd.DTDGrammar;
import org.apache.xerces.impl.dtd.XMLDTDLoader;
import org.apache.xerces.impl.xs.SchemaGrammar;
import org.apache.xerces.impl.xs.XMLSchemaLoader;
import org.apache.xerces.impl.xs.XSMessageFormatter;
import org.apache.xerces.parsers.XMLGrammarCachingConfiguration;
import org.apache.xerces.util.MessageFormatter;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.util.SynchronizedSymbolTable;
import org.apache.xerces.xni.XNIException;
import org.apache.xerces.xni.grammars.Grammar;
import org.apache.xerces.xni.grammars.XMLGrammarPool;
import org.apache.xerces.xni.parser.XMLComponentManager;
import org.apache.xerces.xni.parser.XMLConfigurationException;
import org.apache.xerces.xni.parser.XMLEntityResolver;
import org.apache.xerces.xni.parser.XMLInputSource;
import pt.opensoft.xml.SelectiveXMLGrammarPoolImpl;

public class SelectiveXMLGrammarCachingConfiguration
extends XMLGrammarCachingConfiguration {
    public static final int BIG_PRIME = 2039;
    protected static final SynchronizedSymbolTable fStaticSymbolTable = new SynchronizedSymbolTable(2039);
    protected static final SelectiveXMLGrammarPoolImpl fStaticGrammarPool = new SelectiveXMLGrammarPoolImpl(System.getProperty("opensoft.cacheable-namespaces"));
    protected static final String SCHEMA_FULL_CHECKING = "http://apache.org/xml/features/validation/schema-full-checking";
    protected XMLSchemaLoader fSchemaLoader;
    protected XMLDTDLoader fDTDLoader;

    public SelectiveXMLGrammarCachingConfiguration() {
        this(fStaticSymbolTable, fStaticGrammarPool, null);
    }

    public SelectiveXMLGrammarCachingConfiguration(SymbolTable symboltable) {
        this(symboltable, fStaticGrammarPool, null);
    }

    public SelectiveXMLGrammarCachingConfiguration(SymbolTable symboltable, XMLGrammarPool xmlgrammarpool) {
        this(symboltable, xmlgrammarpool, null);
    }

    public SelectiveXMLGrammarCachingConfiguration(SymbolTable symboltable, XMLGrammarPool xmlgrammarpool, XMLComponentManager xmlcomponentmanager) {
        super(symboltable, xmlgrammarpool, xmlcomponentmanager);
        this.fSchemaLoader = new XMLSchemaLoader(this.fSymbolTable);
        this.fSchemaLoader.setProperty("http://apache.org/xml/properties/internal/grammar-pool", this.fGrammarPool);
        this.fDTDLoader = new XMLDTDLoader(this.fSymbolTable, this.fGrammarPool);
    }

    @Override
    public void lockGrammarPool() {
        this.fGrammarPool.lockPool();
    }

    @Override
    public void clearGrammarPool() {
        this.fGrammarPool.clear();
    }

    @Override
    public void unlockGrammarPool() {
        this.fGrammarPool.unlockPool();
    }

    @Override
    public Grammar parseGrammar(String s, String s1) throws XNIException, IOException {
        XMLInputSource xmlinputsource = new XMLInputSource(null, s1, null);
        return this.parseGrammar(s, xmlinputsource);
    }

    @Override
    public Grammar parseGrammar(String s, XMLInputSource xmlinputsource) throws XNIException, IOException {
        if (s.equals("http://www.w3.org/2001/XMLSchema")) {
            return this.parseXMLSchema(xmlinputsource);
        }
        if (s.equals("http://www.w3.org/TR/REC-xml")) {
            return this.parseDTD(xmlinputsource);
        }
        return null;
    }

    @Override
    protected void checkFeature(String s) throws XMLConfigurationException {
        super.checkFeature(s);
    }

    @Override
    protected void checkProperty(String s) throws XMLConfigurationException {
        super.checkProperty(s);
    }

    SchemaGrammar parseXMLSchema(XMLInputSource xmlinputsource) throws IOException {
        XMLEntityResolver xmlentityresolver = this.getEntityResolver();
        if (xmlentityresolver != null) {
            this.fSchemaLoader.setEntityResolver(xmlentityresolver);
        }
        if (this.fErrorReporter.getMessageFormatter("http://www.w3.org/TR/xml-schema-1") == null) {
            this.fErrorReporter.putMessageFormatter("http://www.w3.org/TR/xml-schema-1", new XSMessageFormatter());
        }
        this.fSchemaLoader.setProperty("http://apache.org/xml/properties/internal/error-reporter", this.fErrorReporter);
        String s = "http://apache.org/xml/properties/";
        String s1 = s + "schema/external-schemaLocation";
        this.fSchemaLoader.setProperty(s1, this.getProperty(s1));
        s1 = s + "schema/external-noNamespaceSchemaLocation";
        this.fSchemaLoader.setProperty(s1, this.getProperty(s1));
        s1 = "http://java.sun.com/xml/jaxp/properties/schemaSource";
        this.fSchemaLoader.setProperty(s1, this.getProperty(s1));
        this.fSchemaLoader.setFeature("http://apache.org/xml/features/validation/schema-full-checking", this.getFeature("http://apache.org/xml/features/validation/schema-full-checking"));
        SchemaGrammar schemagrammar = (SchemaGrammar)this.fSchemaLoader.loadGrammar(xmlinputsource);
        if (schemagrammar != null) {
            this.fGrammarPool.cacheGrammars("http://www.w3.org/2001/XMLSchema", new Grammar[]{schemagrammar});
        }
        return schemagrammar;
    }

    DTDGrammar parseDTD(XMLInputSource xmlinputsource) throws IOException {
        XMLEntityResolver xmlentityresolver = this.getEntityResolver();
        if (xmlentityresolver != null) {
            this.fDTDLoader.setEntityResolver(xmlentityresolver);
        }
        this.fDTDLoader.setProperty("http://apache.org/xml/properties/internal/error-reporter", this.fErrorReporter);
        DTDGrammar dtdgrammar = (DTDGrammar)this.fDTDLoader.loadGrammar(xmlinputsource);
        if (dtdgrammar != null) {
            this.fGrammarPool.cacheGrammars("http://www.w3.org/TR/REC-xml", new Grammar[]{dtdgrammar});
        }
        return dtdgrammar;
    }
}

