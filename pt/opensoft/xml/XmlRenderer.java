/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.xml;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import pt.opensoft.field.Getter;
import pt.opensoft.field.Getter118;
import pt.opensoft.field.NamedGetter;
import pt.opensoft.field.NamedGetter118;
import pt.opensoft.text.Appender;
import pt.opensoft.text.DefaultRenderer;
import pt.opensoft.text.Renderer;

public class XmlRenderer
extends DefaultRenderer {
    public static final String XML_VERSION = "<?xml version='1.0' encoding='ISO-8859-1'?>";

    public static String cdata(String value) {
        return new StringBuffer(value.length() + 12).append("<![CDATA[").append(value).append("]]>").toString();
    }

    public XmlRenderer() {
    }

    protected XmlRenderer(Appender Appender) {
        super(Appender);
    }

    public XmlRenderer(String str) {
        super(str);
    }

    public XmlRenderer(int size) {
        super(size);
    }

    public void init() {
        this.append("<?xml version='1.0' encoding='ISO-8859-1'?>").newLine().newLine();
    }

    public XmlRenderer startTag(String name) {
        return this.startTag(name, null, true);
    }

    public XmlRenderer startTag(String name, boolean newLine) {
        return this.startTag(name, null, newLine);
    }

    public XmlRenderer startTag(String name, String attributes) {
        return this.startTag(name, attributes, true);
    }

    public XmlRenderer startTag(String name, String attributes, boolean newLine) {
        this.append('<').append(name);
        if (attributes != null) {
            this.append(' ').append(attributes);
        }
        this.append('>');
        if (newLine) {
            this.newLine();
        }
        return this;
    }

    public XmlRenderer endTag(String name) {
        return this.endTag(name, true);
    }

    public XmlRenderer endTag(String name, boolean newLine) {
        this.append("</").append(name).append('>');
        if (newLine) {
            this.newLine();
        }
        return this;
    }

    public XmlRenderer tag(String name) {
        this.append('<').append(name).append("/>").newLine();
        return this;
    }

    public XmlRenderer tag(String name, boolean value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, byte value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, char value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, short value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, int value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, long value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, float value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, double value) {
        return this.tag(name, String.valueOf(value));
    }

    public XmlRenderer tag(String name, Object value) {
        return this.tag(name, value.toString());
    }

    public XmlRenderer tag(String name, String value) {
        this.startTag(name, false);
        this.text(value, false);
        this.endTag(name);
        return this;
    }

    public XmlRenderer tag(String name, String attributes, boolean value) {
        return this.tag(name, attributes, String.valueOf(value), false);
    }

    public XmlRenderer tag(String name, String attributes, byte value) {
        return this.tag(name, attributes, String.valueOf(value));
    }

    public XmlRenderer tag(String name, String attributes, char value) {
        return this.tag(name, attributes, String.valueOf(value));
    }

    public XmlRenderer tag(String name, String attributes, short value) {
        return this.tag(name, attributes, String.valueOf(value));
    }

    public XmlRenderer tag(String name, String attributes, int value) {
        return this.tag(name, attributes, String.valueOf(value));
    }

    public XmlRenderer tag(String name, String attributes, long value) {
        return this.tag(name, attributes, String.valueOf(value));
    }

    public XmlRenderer tag(String name, String attributes, float value) {
        return this.tag(name, attributes, String.valueOf(value));
    }

    public XmlRenderer tag(String name, String attributes, double value) {
        return this.tag(name, attributes, String.valueOf(value));
    }

    public XmlRenderer tag(String name, String attributes, Object value) {
        return this.tag(name, attributes, value.toString());
    }

    public XmlRenderer tag(String name, String attributes, String text) {
        return this.tag(name, attributes, text, false);
    }

    public XmlRenderer tag(String name, String attributes, String text, boolean newLine) {
        this.startTag(name, attributes, newLine);
        this.text(text, newLine);
        return this.endTag(name, true);
    }

    public Appender append(Getter getter) {
        if (getter.isEmpty()) {
            return this;
        }
        this.startTag(getter.getName(), false);
        super.append(getter);
        this.endTag(getter.getName(), true);
        return this;
    }

    public Appender append(NamedGetter getter) {
        if (getter.isEmpty()) {
            return this;
        }
        this.startTag(getter.getName(), true);
        super.append(getter);
        this.endTag(getter.getName(), true);
        return this;
    }

    public XmlRenderer text(Document document) throws IOException {
        XMLOutputter serializer = new XMLOutputter("  ", true);
        serializer.setEncoding("ISO-8859-1");
        StringWriter stringWriter = new StringWriter();
        serializer.output(document, (Writer)stringWriter);
        this.text(stringWriter.getBuffer().toString());
        return this;
    }
}

