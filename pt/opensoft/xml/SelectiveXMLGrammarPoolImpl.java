/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.xml;

import org.apache.xerces.util.XMLGrammarPoolImpl;
import org.apache.xerces.xni.grammars.Grammar;
import org.apache.xerces.xni.grammars.XMLGrammarDescription;

public class SelectiveXMLGrammarPoolImpl
extends XMLGrammarPoolImpl {
    protected String cacheableNamespaces;

    public SelectiveXMLGrammarPoolImpl(String cacheableNamespaces) {
        this.cacheableNamespaces = cacheableNamespaces;
    }

    @Override
    public void cacheGrammars(String s, Grammar[] agrammar) {
        if (!this.fPoolIsLocked) {
            for (int i = 0; i < agrammar.length; ++i) {
                Grammar grammar = agrammar[i];
                if (this.cacheableNamespaces == null || grammar.getGrammarDescription().getNamespace() == null || this.cacheableNamespaces.indexOf(grammar.getGrammarDescription().getNamespace()) == -1) continue;
                this.putGrammar(grammar);
            }
        }
    }
}

