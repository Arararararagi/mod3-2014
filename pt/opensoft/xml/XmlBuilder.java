/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;
import pt.opensoft.io.FileUtil;
import pt.opensoft.io.ResourceLoader;
import pt.opensoft.io.ResourceNotFoundException;
import pt.opensoft.logging.Logger;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.UnicodeReader;

public class XmlBuilder
implements ErrorHandler {
    public static final String DEFAULT_ENCODING = "ISO8859_1";
    private static final int DEFAULT_MAX_ERRORS = 30;
    protected SAXBuilder _builder;
    protected ArrayList _errors = new ArrayList(30);
    protected int _maxErrors = 30;
    protected Document _jdomDocument;
    protected Logger _logger;

    public XmlBuilder(String saxParser, boolean validate) {
        final String absoluteLocalPath = FileUtil.getNormalizedPath(new File("").getAbsolutePath());
        this._builder = saxParser == null ? new SAXBuilder(validate) : new SAXBuilder(saxParser, validate);
        this._builder.setErrorHandler(this);
        this._builder.setEntityResolver(new EntityResolver(){

            @Override
            public InputSource resolveEntity(String s, String systemId) throws ResourceNotFoundException {
                if (systemId.startsWith("file:")) {
                    systemId = systemId.substring(5);
                    systemId = StringUtil.replace(systemId, absoluteLocalPath, "");
                    systemId = StringUtil.trimLeft(systemId, '/');
                }
                InputSource inputSource = new InputSource(ResourceLoader.getResourceStream(systemId));
                inputSource.setSystemId(systemId);
                inputSource.setEncoding("ISO-8859-1");
                return inputSource;
            }
        });
    }

    public XmlBuilder(boolean validate) {
        this(validate ? "org.apache.xerces.parsers.SAXParser" : null, validate);
    }

    public XmlBuilder() {
        this(false);
    }

    public void setLogger(Logger logger) {
        this._logger = logger;
    }

    public void setSchema(String schemas, boolean useNamespace) {
        this.setSchema(schemas, useNamespace, true);
    }

    public void setSchema(String schemas, boolean useNamespace, boolean schema_full_checking) {
        schemas = FileUtil.getNormalizedPath(schemas);
        this._builder.setFeature("http://xml.org/sax/features/validation", true);
        this._builder.setFeature("http://apache.org/xml/features/validation/schema", true);
        this._builder.setFeature("http://apache.org/xml/features/validation/schema-full-checking", schema_full_checking);
        if (!useNamespace) {
            this._builder.setProperty("http://apache.org/xml/properties/schema/external-noNamespaceSchemaLocation", schemas);
        } else {
            this._builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation", schemas);
        }
    }

    public void setMaxErrors(int maxErrors) {
        this._maxErrors = maxErrors;
    }

    public Document getDocument() {
        return this._jdomDocument;
    }

    public List parse(InputStream in) throws JDOMException, IOException {
        return this.parse(new InputSource(new UnicodeReader(in, "UTF-8")));
    }

    public List parse(Reader reader) throws JDOMException {
        return this.parse(new InputSource(reader));
    }

    public List parse(InputSource source) throws JDOMException {
        long start;
        block3 : {
            start = System.currentTimeMillis();
            try {
                this._jdomDocument = this._builder.build(source);
            }
            catch (JDOMException e) {
                if (e.getCause() != null && e.getCause() instanceof SAXParseException) break block3;
                throw e;
            }
        }
        if (this._logger != null) {
            this._logger.finest("XMLBuilder: parse time->" + (System.currentTimeMillis() - start) + " ms");
        }
        return this._errors;
    }

    @Override
    public void error(SAXParseException e) {
        if (this._errors.size() < this._maxErrors) {
            this._errors.add(e);
        }
    }

    @Override
    public void fatalError(SAXParseException e) {
        if (this._errors.size() < this._maxErrors) {
            this._errors.add(e);
        }
    }

    @Override
    public void warning(SAXParseException e) {
        if (this._logger != null) {
            this._logger.finest("SAXParseWarning:" + e);
        }
    }

}

