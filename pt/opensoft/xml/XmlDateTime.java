/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.xml;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pt.opensoft.util.DateTime;

public class XmlDateTime
implements Comparable {
    private static final String GMT = "GMT";
    private static final int TIMEZONE_EQUALIZER = 60000;
    protected long msSinceEpoch = 0;
    protected boolean dateOnly = false;
    protected boolean timeOnly = false;
    protected Integer timezoneShift = null;
    public static final Pattern dateTimePattern = Pattern.compile("-?(\\d\\d\\d\\d)\\-(\\d\\d)\\-(\\d\\d)[Tt](\\d\\d):(\\d\\d):(\\d\\d)(\\.(\\d+))?([Zz]|((\\+|\\-)(\\d\\d):(\\d\\d)))?");
    public static final Pattern datePattern = Pattern.compile("-?(\\d\\d\\d\\d)\\-(\\d\\d)\\-(\\d\\d)([Zz]|((\\+|\\-)(\\d\\d):(\\d\\d)))?");
    public static final Pattern dateTimeChoicePattern = Pattern.compile("-?(\\d\\d\\d\\d)\\-(\\d\\d)\\-(\\d\\d)([Tt](\\d\\d):(\\d\\d):(\\d\\d)(\\.(\\d+))?)?([Zz]|((\\+|\\-)(\\d\\d):(\\d\\d)))?");
    public static final Pattern timePattern = Pattern.compile("(\\d\\d):(\\d\\d):(\\d\\d)(\\.(\\d+))?");
    private static final SimpleDateFormat dateTimeFormat822 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.getDefault());

    public XmlDateTime() {
    }

    public XmlDateTime(long msSinceEpoch) {
        this.msSinceEpoch = msSinceEpoch;
    }

    public XmlDateTime(DateTime dateTime) {
        this.msSinceEpoch = dateTime.getTime();
    }

    public XmlDateTime(long msSinceEpoch, int timezoneShift) {
        this.msSinceEpoch = msSinceEpoch;
        this.timezoneShift = new Integer(timezoneShift);
    }

    public XmlDateTime(DateTime dateTime, TimeZone timezone) {
        this.msSinceEpoch = dateTime.getTime();
        this.timezoneShift = new Integer(timezone.getOffset(dateTime.getTime()) / 60000);
    }

    public static XmlDateTime now() {
        return new XmlDateTime(new DateTime(new Date()), TimeZone.getTimeZone("GMT"));
    }

    public long getMsSinceEpoch() {
        return this.msSinceEpoch;
    }

    public void setMsSinceEpoch(long ms) {
        this.msSinceEpoch = ms;
    }

    public boolean isDateOnly() {
        return this.dateOnly;
    }

    public void setIsDateOnly(boolean b) {
        this.dateOnly = b;
    }

    public boolean isTimeOnly() {
        return this.timeOnly;
    }

    public void setIsTimeOnly(boolean b) {
        this.timeOnly = b;
    }

    public Integer getTimezoneShift() {
        return this.timezoneShift;
    }

    public void setTimezoneShift(Integer shift) {
        this.timezoneShift = shift;
    }

    public int hashCode() {
        return new Long(this.msSinceEpoch).hashCode();
    }

    public boolean equals(Object o) {
        if (o instanceof XmlDateTime) {
            XmlDateTime other = (XmlDateTime)o;
            return this.msSinceEpoch == other.msSinceEpoch && (this.timezoneShift == null && other.timezoneShift == null || this.timezoneShift != null && this.timezoneShift.equals(other.timezoneShift));
        }
        if (o instanceof Date) {
            return this.msSinceEpoch == ((Date)o).getTime();
        }
        return false;
    }

    public int compareTo(Object o) {
        if (o instanceof XmlDateTime) {
            return new Long(this.msSinceEpoch).compareTo(new Long(((XmlDateTime)o).msSinceEpoch));
        }
        if (o instanceof Date) {
            return new Long(this.msSinceEpoch).compareTo(new Long(((Date)o).getTime()));
        }
        throw new RuntimeException("Invalid type.");
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        GregorianCalendar dateTime = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        long localTime = this.msSinceEpoch;
        if (this.timezoneShift != null) {
            localTime+=this.timezoneShift.longValue() * 60000;
        }
        dateTime.setTimeInMillis(localTime);
        try {
            XmlDateTime.appendInt(sb, dateTime.get(1), 4);
            sb.append('-');
            XmlDateTime.appendInt(sb, dateTime.get(2) + 1, 2);
            sb.append('-');
            XmlDateTime.appendInt(sb, dateTime.get(5), 2);
            if (!this.dateOnly) {
                sb.append('T');
                XmlDateTime.appendInt(sb, dateTime.get(11), 2);
                sb.append(':');
                XmlDateTime.appendInt(sb, dateTime.get(12), 2);
                sb.append(':');
                XmlDateTime.appendInt(sb, dateTime.get(13), 2);
                if (dateTime.isSet(14)) {
                    sb.append('.');
                    XmlDateTime.appendInt(sb, dateTime.get(14), 3);
                }
            }
            if (this.timezoneShift != null) {
                if (this.timezoneShift == 0) {
                    sb.append('Z');
                } else {
                    int absTzShift = this.timezoneShift;
                    if (this.timezoneShift > 0) {
                        sb.append('+');
                    } else {
                        sb.append('-');
                        absTzShift = - absTzShift;
                    }
                    int tzHours = absTzShift / 60;
                    int tzMinutes = absTzShift % 60;
                    XmlDateTime.appendInt(sb, tzHours, 2);
                    sb.append(':');
                    XmlDateTime.appendInt(sb, tzMinutes, 2);
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    public String toStringRfc822() {
        if (!this.dateOnly) {
            SimpleDateFormat simpleDateFormat = dateTimeFormat822;
            synchronized (simpleDateFormat) {
                return dateTimeFormat822.format(new Date(this.msSinceEpoch));
            }
        }
        return null;
    }

    public String toUiString() {
        StringBuffer sb = new StringBuffer();
        GregorianCalendar dateTime = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        long localTime = this.msSinceEpoch;
        if (this.timezoneShift != null) {
            localTime+=this.timezoneShift.longValue() * 60000;
        }
        dateTime.setTimeInMillis(localTime);
        try {
            XmlDateTime.appendInt(sb, dateTime.get(1), 4);
            sb.append('-');
            XmlDateTime.appendInt(sb, dateTime.get(2) + 1, 2);
            sb.append('-');
            XmlDateTime.appendInt(sb, dateTime.get(5), 2);
            if (!this.dateOnly) {
                sb.append(' ');
                XmlDateTime.appendInt(sb, dateTime.get(11), 2);
                sb.append(':');
                XmlDateTime.appendInt(sb, dateTime.get(12), 2);
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    public DateTime getDateTime() {
        long localTime = this.msSinceEpoch;
        if (this.timezoneShift != null) {
            localTime+=this.timezoneShift.longValue() * 60000;
        }
        return new DateTime(localTime);
    }

    public static XmlDateTime parseDateTime(String str) throws NumberFormatException {
        Matcher m = dateTimePattern.matcher((CharSequence)str);
        if (!m.matches()) {
            throw new NumberFormatException("Invalid date/time format.");
        }
        XmlDateTime ret = new XmlDateTime();
        ret.dateOnly = false;
        if (m.group(9) != null) {
            if (m.group(9).equalsIgnoreCase("Z")) {
                ret.timezoneShift = new Integer(0);
            } else {
                ret.timezoneShift = new Integer(Integer.valueOf(m.group(12)) * 60 + Integer.valueOf(m.group(13)));
                if (m.group(11).equals("-")) {
                    ret.timezoneShift = new Integer(- ret.timezoneShift.intValue());
                }
            }
        }
        GregorianCalendar dateTime = new GregorianCalendar();
        dateTime.clear();
        dateTime.set(Integer.valueOf(m.group(1)), Integer.valueOf(m.group(2)) - 1, Integer.valueOf(m.group(3)), Integer.valueOf(m.group(4)), Integer.valueOf(m.group(5)), Integer.valueOf(m.group(6)));
        if (m.group(8) != null && m.group(8).length() > 0) {
            BigDecimal bd = new BigDecimal("0." + m.group(8));
            dateTime.set(14, bd.movePointRight(3).intValue());
        }
        ret.msSinceEpoch = dateTime.getTimeInMillis();
        if (ret.timezoneShift != null) {
            ret.msSinceEpoch-=(long)(ret.timezoneShift * 60000);
        }
        return ret;
    }

    public static XmlDateTime parseDate(String str) throws NumberFormatException {
        Matcher m = datePattern.matcher((CharSequence)str);
        if (!m.matches()) {
            throw new NumberFormatException("Invalid date format.");
        }
        XmlDateTime ret = new XmlDateTime();
        ret.dateOnly = true;
        if (m.group(4) != null) {
            if (m.group(4).equalsIgnoreCase("Z")) {
                ret.timezoneShift = new Integer(0);
            } else {
                ret.timezoneShift = new Integer(Integer.valueOf(m.group(7)) * 60 + Integer.valueOf(m.group(8)));
                if (m.group(6).equals("-")) {
                    ret.timezoneShift = new Integer(- ret.timezoneShift.intValue());
                }
            }
        }
        GregorianCalendar dateTime = new GregorianCalendar();
        dateTime.clear();
        dateTime.set(Integer.valueOf(m.group(1)), Integer.valueOf(m.group(2)) - 1, Integer.valueOf(m.group(3)), 0, 0, 0);
        ret.msSinceEpoch = dateTime.getTimeInMillis();
        if (ret.timezoneShift != null) {
            ret.msSinceEpoch-=(long)(ret.timezoneShift * 60000);
        }
        return ret;
    }

    public static XmlDateTime parseTime(String str) throws NumberFormatException {
        Matcher m = timePattern.matcher((CharSequence)str);
        if (!m.matches()) {
            throw new NumberFormatException("Invalid date format.");
        }
        XmlDateTime ret = new XmlDateTime();
        ret.timeOnly = true;
        GregorianCalendar dateTime = new GregorianCalendar();
        dateTime.clear();
        dateTime.set(11, Integer.valueOf(m.group(1)));
        dateTime.set(12, Integer.valueOf(m.group(2)));
        dateTime.set(13, Integer.valueOf(m.group(3)));
        ret.msSinceEpoch = dateTime.getTimeInMillis();
        if (ret.timezoneShift != null) {
            ret.msSinceEpoch-=(long)(ret.timezoneShift * 60000);
        }
        return ret;
    }

    public static XmlDateTime parseDateTimeChoice(String value) throws NumberFormatException {
        try {
            return XmlDateTime.parseDateTime(value);
        }
        catch (NumberFormatException e) {
            NumberFormatException exception = e;
            try {
                return XmlDateTime.parseDate(value);
            }
            catch (NumberFormatException e) {
                exception = e;
                try {
                    return XmlDateTime.parseTime(value);
                }
                catch (NumberFormatException e) {
                    exception = e;
                    throw exception;
                }
            }
        }
    }

    private static void appendInt(StringBuffer sb, int num, int numDigits) {
        if (num < 0) {
            sb.append('-');
            num = - num;
        }
        char[] digits = new char[numDigits];
        for (int digit = numDigits - 1; digit >= 0; --digit) {
            digits[digit] = (char)(48 + num % 10);
            num/=10;
        }
        sb.append(digits);
    }

    static {
        dateTimeFormat822.setTimeZone(TimeZone.getTimeZone("GMT"));
    }
}

