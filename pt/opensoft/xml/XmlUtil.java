/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.xerces.parsers.SAXParser;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import pt.opensoft.io.ResourceLoader;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.Transformer;

public class XmlUtil {
    public static final String CRIMSON_SAX_PARSER = "org.apache.crimson.parser.XMLReaderImpl";
    public static final String XERCES_SAX_PARSER = "org.apache.xerces.parsers.SAXParser";
    private static final String DEFAULT_INDENT = "  ";
    public static final String DEFAULT_ENCODING = "ISO8859_1";
    private static final Map mappings = XmlUtil.fillMappings();

    public static Document readXml(File file) throws Exception {
        InputStream in = ResourceLoader.getResourceStream(file);
        try {
            Document document = XmlUtil.readXml(in);
            return document;
        }
        finally {
            in.close();
        }
    }

    public static Document readXml(InputStream in) throws Exception {
        InputStreamReader reader = new InputStreamReader(in, "ISO8859_1");
        try {
            Document document = XmlUtil.readXml(reader);
            return document;
        }
        finally {
            reader.close();
        }
    }

    public static Document readXml(Reader reader) throws Exception {
        SAXBuilder builder = new SAXBuilder();
        Document document = builder.build(reader);
        return document;
    }

    public static String toString(Document document) {
        return XmlUtil.toString(document, "  ", true);
    }

    public static String toString(Document document, String indent, boolean newLines) {
        StringWriter writer = new StringWriter();
        try {
            try {
                XmlUtil.writeTo(document, (Writer)writer, indent, newLines);
            }
            finally {
                writer.close();
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return writer.toString();
    }

    public static String toString(Element element) {
        return XmlUtil.toString(element, "  ", true);
    }

    public static String toString(Element element, String indent, boolean newLines) {
        StringWriter writer = new StringWriter();
        try {
            try {
                XmlUtil.writeTo(element, (Writer)writer, indent, newLines);
            }
            finally {
                writer.close();
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return writer.toString();
    }

    public static void writeTo(Element element, OutputStream out) throws IOException {
        XmlUtil.writeTo(element, (Writer)new OutputStreamWriter(out, "ISO8859_1"), "  ", true);
    }

    public static void writeTo(Element element, OutputStream out, String indent, boolean newLines) throws IOException {
        XmlUtil.writeTo(element, (Writer)new OutputStreamWriter(out, "ISO8859_1"), indent, newLines);
    }

    public static void writeTo(Element element, Writer writer) throws IOException {
        XmlUtil.writeTo(element, writer, "  ", true);
    }

    public static void writeTo(Element element, Writer writer, String indent, boolean newLines) throws IOException {
        XMLOutputter out = new XMLOutputter(indent, newLines);
        out.output(element, writer);
    }

    public static void writeTo(Document document, OutputStream out) throws IOException {
        XmlUtil.writeTo(document, (Writer)new OutputStreamWriter(out, "ISO8859_1"));
    }

    public static void writeTo(Document document, OutputStream out, String indent, boolean newLines) throws IOException {
        XmlUtil.writeTo(document, (Writer)new OutputStreamWriter(out, "ISO8859_1"), indent, newLines);
    }

    public static void writeTo(Document document, Writer writer) throws IOException {
        XmlUtil.writeTo(document, writer, "  ", true);
    }

    public static void writeTo(Document document, Writer writer, String indent, boolean newLines) throws IOException {
        XMLOutputter out = new XMLOutputter(indent, newLines);
        out.output(document, writer);
    }

    public static XMLReader getSaxParser(String parserName) {
        try {
            Class parserClass = Class.forName(parserName);
            return (XMLReader)ClassUtil.newInstance(parserClass);
        }
        catch (ClassNotFoundException e) {
            return new SAXParser();
        }
    }

    public static String getXmlErrorsAsString(List errors) {
        return "Xml errors: [" + ListUtil.collect(errors, new Transformer(){

            public Object transform(Object obj) {
                SAXParseException saxParseException = (SAXParseException)obj;
                return saxParseException.getLocalizedMessage() + ";";
            }
        }) + "]";
    }

    public static String escapeElementEntities(String entityText) {
        int i;
        if (entityText == null) {
            return entityText;
        }
        StringBuffer buff = new StringBuffer();
        char[] block = entityText.toCharArray();
        String stEntity = null;
        int last = 0;
        for (i = 0; i < block.length; ++i) {
            switch (block[i]) {
                case '<': {
                    stEntity = "&lt;";
                    break;
                }
                case '>': {
                    stEntity = "&gt;";
                    break;
                }
                case '&': {
                    stEntity = "&amp;";
                }
            }
            if (stEntity == null) continue;
            buff.append(block, last, i - last);
            buff.append(stEntity);
            stEntity = null;
            last = i + 1;
        }
        if (last < block.length) {
            buff.append(block, last, i - last);
        }
        return buff.toString();
    }

    public static String standardize(String string) throws IOException {
        StringBuffer standardizedStringBuffer = new StringBuffer("");
        StringReader isr = new StringReader(string);
        int character = isr.read();
        while (character != -1) {
            Integer characterInt = new Integer(character);
            if (mappings.containsKey(characterInt)) {
                standardizedStringBuffer.append((String)mappings.get(characterInt));
            } else {
                standardizedStringBuffer.append((char)character);
            }
            character = isr.read();
        }
        return standardizedStringBuffer.toString();
    }

    private static Map fillMappings() {
        HashMap<Integer, String> result = new HashMap<Integer, String>();
        result.put(new Integer(8364), "Euro");
        result.put(new Integer(8218), "'");
        result.put(new Integer(402), " ");
        result.put(new Integer(8222), "\"");
        result.put(new Integer(8230), "...");
        result.put(new Integer(8224), " ");
        result.put(new Integer(8225), " ");
        result.put(new Integer(710), "^");
        result.put(new Integer(8240), "%o");
        result.put(new Integer(352), " ");
        result.put(new Integer(8249), "&lt;");
        result.put(new Integer(338), " ");
        result.put(new Integer(381), " ");
        result.put(new Integer(8216), "'");
        result.put(new Integer(8217), "'");
        result.put(new Integer(8220), "\"");
        result.put(new Integer(8221), "\"");
        result.put(new Integer(8226), "-");
        result.put(new Integer(8211), "-");
        result.put(new Integer(8212), "-");
        result.put(new Integer(732), "~");
        result.put(new Integer(8482), "TM");
        result.put(new Integer(353), " ");
        result.put(new Integer(8250), "&gt;");
        result.put(new Integer(339), " ");
        result.put(new Integer(382), " ");
        result.put(new Integer(376), " ");
        result.put(new Integer(161), "!");
        result.put(new Integer(162), " ");
        result.put(new Integer(163), "Pound");
        result.put(new Integer(164), " ");
        result.put(new Integer(165), " ");
        result.put(new Integer(166), "|");
        result.put(new Integer(167), " ");
        result.put(new Integer(168), " ");
        result.put(new Integer(169), "Copyright");
        result.put(new Integer(170), "a");
        result.put(new Integer(171), "&lt;&lt;");
        result.put(new Integer(172), " ");
        result.put(new Integer(173), " ");
        result.put(new Integer(174), " ");
        result.put(new Integer(175), "-");
        result.put(new Integer(176), "o");
        result.put(new Integer(177), "+/-");
        result.put(new Integer(178), "2");
        result.put(new Integer(179), "3");
        result.put(new Integer(180), "'");
        result.put(new Integer(181), " ");
        result.put(new Integer(182), " ");
        result.put(new Integer(183), "-");
        result.put(new Integer(184), ",");
        result.put(new Integer(185), "1");
        result.put(new Integer(187), "&gt;&gt;");
        result.put(new Integer(188), "1/4");
        result.put(new Integer(189), "1/2");
        result.put(new Integer(190), "3/4");
        result.put(new Integer(191), "?");
        result.put(new Integer(196), "A");
        result.put(new Integer(197), "A");
        result.put(new Integer(198), "AE");
        result.put(new Integer(203), "E");
        result.put(new Integer(207), "I");
        result.put(new Integer(208), "D");
        result.put(new Integer(214), "O");
        result.put(new Integer(215), "x");
        result.put(new Integer(216), "0");
        result.put(new Integer(220), "U");
        result.put(new Integer(221), "'Y");
        result.put(new Integer(222), " ");
        result.put(new Integer(223), "B");
        result.put(new Integer(228), "a");
        result.put(new Integer(229), "a");
        result.put(new Integer(230), "ae");
        result.put(new Integer(235), "e");
        result.put(new Integer(239), "i");
        result.put(new Integer(240), " ");
        result.put(new Integer(246), "o");
        result.put(new Integer(247), "/");
        result.put(new Integer(248), "0");
        result.put(new Integer(252), "u");
        result.put(new Integer(253), "'y");
        result.put(new Integer(254), " ");
        result.put(new Integer(255), "y");
        return result;
    }

}

