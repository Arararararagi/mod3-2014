/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Dimension;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.NavigationFilter;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.base.filters.RightAlignMaskDocumentController;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;

public class JMoneyTextField
extends JTextField {
    private static final long serialVersionUID = 8730281001620115704L;
    public static final String EURO = "\u20ac";
    public static final String BIG_MASK = "#.###.###.###.###.###,## \u20ac";
    public static final String BIG_MASK_WITHOUT_PRECISION = "#.###.###.###.###.###";
    protected RightAlignMaskDocumentController controller;
    protected int precision = 2;
    protected boolean startNegative;

    public JMoneyTextField() {
        int initialColumns = 12;
        this.controller = new RightAlignMaskDocumentController("#.###.###.###.###.###,## \u20ac", "0,00 \u20ac", " \u20ac", this, initialColumns, false);
        ((AbstractDocument)this.getDocument()).setDocumentFilter(this.controller.getDocumentFilter());
        this.setNavigationFilter(this.controller.getNavigationFilter());
        this.setColumns(initialColumns);
        this.setHorizontalAlignment(4);
        this.setMinimumSize(this.getPreferredSize());
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public JMoneyTextField(int precision) {
        this.setPrecision(precision);
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public void setPrecision(int precision) {
        this.precision = precision;
        this.configureComponent();
    }

    public int getPrecision() {
        return this.precision;
    }

    private void configureComponent() {
        StringBuffer mask = new StringBuffer("#.###.###.###.###.###");
        StringBuffer placeHolders = new StringBuffer("");
        if (this.precision > 0) {
            mask.append(",");
            placeHolders.append("0,");
            do {
                mask.append("#");
                placeHolders.append("0");
                --this.precision;
            } while (this.precision > 0);
        }
        mask.append(" ").append("\u20ac");
        placeHolders.append(" ").append("\u20ac");
        this.controller = new RightAlignMaskDocumentController(mask.toString(), placeHolders.toString(), " \u20ac", this, mask.length() - 2, false);
        ((AbstractDocument)this.getDocument()).setDocumentFilter(this.controller.getDocumentFilter());
        this.setNavigationFilter(this.controller.getNavigationFilter());
        this.setColumns(mask.length() - 1);
        this.setHorizontalAlignment(4);
        this.setMinimumSize(this.getPreferredSize());
    }

    public void allowNegative(boolean startNegative) {
        this.startNegative = startNegative;
        this.controller.allowPrefix("-", "-", startNegative);
        super.setColumns(this.getColumns());
    }

    public boolean getAllowNegative() {
        return this.startNegative;
    }

    public void setAllowNegative(boolean startNegative) {
        this.allowNegative(startNegative);
    }

    @Override
    public void setColumns(int s) {
        this.controller.setSize(s);
        super.setColumns(this.getColumnsWithPunctuation(s));
        this.setMinimumSize(this.getPreferredSize());
    }

    protected int getColumnsWithPunctuation(int numColumns) {
        return numColumns + (this.controller.hasPrefix() ? this.controller.prefixSize() : 0);
    }

    @Override
    public String getText() {
        String result = super.getText().replace((CharSequence)"\u20ac", (CharSequence)"");
        result = result.replace((CharSequence)" ", (CharSequence)"");
        return result;
    }

    public String getValue() {
        return this.controller.getPureContent();
    }

    @Override
    public void copy() {
        JTextField jTextField = new JTextField(this.controller.getPureContent(this.getSelectionStart(), this.getSelectionEnd()));
        jTextField.selectAll();
        jTextField.copy();
    }

    public void setValue(String s) {
        if (s == null) {
            return;
        }
        this.controller.setPureContent(s);
    }

    @Override
    protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e, final int condition, final boolean pressed) {
        if (this.hasFocus()) {
            return super.processKeyBinding(ks, e, condition, pressed);
        }
        this.requestFocus();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                JMoneyTextField.this.processKeyBinding(ks, e, condition, pressed);
            }
        });
        return true;
    }

}

