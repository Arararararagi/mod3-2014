/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusListener;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.NavigationFilter;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.base.filters.DocumentController;
import pt.opensoft.swing.base.filters.LeftAlignMaskDocumentController;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;
import pt.opensoft.util.InvalidDate;
import pt.opensoft.util.StringUtil;

public class JDateTextField
extends JTextField {
    private static final long serialVersionUID = 3424442949671410418L;
    DocumentController controller;
    public static final String DATE_MASK = "####/##/##";
    public static final String DATE_PLACEHOLDER = "____/__/__";
    public static final String DATE_FORMAT = "yyyy/MM/dd";

    public JDateTextField() {
        this.controller = new LeftAlignMaskDocumentController(this.getDateMask(), this.getDatePlaceHolder(), this, 8);
        ((AbstractDocument)this.getDocument()).setDocumentFilter(this.controller.getDocumentFilter());
        this.setNavigationFilter(this.controller.getNavigationFilter());
        this.setColumns(this.getDefaultColumns());
        this.setHorizontalAlignment(4);
        this.setMinimumSize(this.getPreferredSize());
        this.setInputVerifier(new InputVerifier(){

            @Override
            public boolean verify(JComponent input) {
                try {
                    if (!JDateTextField.this.controller.getPureContent().equals("")) {
                        JDateTextField.this.setForeground(UIManager.getColor("TextField.color"));
                    }
                }
                catch (InvalidDate e) {
                    String s = JDateTextField.this.controller.getPureContent();
                    JDateTextField.this.controller.setPureContent("");
                    DialogFactory.instance().showErrorDialog("Data inv\u00e1lida.");
                    JDateTextField.this.controller.setPureContent(s);
                    JDateTextField.this.setForeground(Color.RED);
                    JDateTextField.this.repaint();
                    return false;
                }
                return true;
            }
        });
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    protected int getDefaultColumns() {
        return 6;
    }

    protected String getDateMask() {
        return "####/##/##";
    }

    protected String getDatePlaceHolder() {
        return "____/__/__";
    }

    public String getValue() {
        return this.controller.getPureContent();
    }

    public void setValue(String s) {
        this.controller.setPureContent(s);
    }

    @Override
    public String getText() {
        return super.getText();
    }

    @Override
    public void setText(String text) {
        String textChanged = StringUtil.removeChars(text, "/");
        super.setText(textChanged);
    }

    @Override
    public void setColumns(int columns) {
        super.setColumns(columns + 2);
    }

    @Override
    public void copy() {
        JTextField jTextField = new JTextField(this.controller.getPureContent());
        jTextField.selectAll();
        jTextField.copy();
    }

}

