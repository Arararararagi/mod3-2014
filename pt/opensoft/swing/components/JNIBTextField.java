/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Color;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.UIManager;
import pt.opensoft.field.NibValidator;
import pt.opensoft.swing.base.filters.LeftAlignMaskDocumentController;
import pt.opensoft.swing.components.JMaskTextField;
import pt.opensoft.util.StringUtil;

public class JNIBTextField
extends JMaskTextField {
    private static final long serialVersionUID = -3977363225881650993L;
    public static final String NIB_MASK = "####.####.###########.##";
    public static final String NIB_PLACEHOLDER = "____.____.___________.__";

    public JNIBTextField() {
        super("####.####.###########.##", "____.____.___________.__", true);
        this.setHorizontalAlignment(4);
        this.setInputVerifier(new InputVerifier(){

            @Override
            public boolean verify(JComponent input) {
                String nib = ((LeftAlignMaskDocumentController)JNIBTextField.this.controller).getPureContent();
                JNIBTextField.this.setForeground(UIManager.getColor("TextField.color"));
                if (!(StringUtil.isEmpty(nib) || NibValidator.validate(nib))) {
                    String s = ((LeftAlignMaskDocumentController)JNIBTextField.this.controller).getPureContent();
                    ((LeftAlignMaskDocumentController)JNIBTextField.this.controller).setPureContent("");
                    ((LeftAlignMaskDocumentController)JNIBTextField.this.controller).setPureContent(s.replace((CharSequence)".", (CharSequence)""));
                    JNIBTextField.this.setForeground(Color.RED);
                    JNIBTextField.this.repaint();
                    return true;
                }
                return true;
            }
        });
    }

}

