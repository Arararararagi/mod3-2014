/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components.popup;

import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.JDialog;
import pt.opensoft.swing.components.JButtonSimpleTextField;

public abstract class SimpleGenericSettablePopup<K extends JButtonSimpleTextField>
extends JDialog {
    private static final long serialVersionUID = -1892841459934140666L;
    protected K buttonField;

    public SimpleGenericSettablePopup() {
        this.setVisible(true);
    }

    public SimpleGenericSettablePopup(Frame owner) {
        super(owner);
    }

    public SimpleGenericSettablePopup(Dialog owner) {
        super(owner);
    }

    public abstract Object getValue();

    public void updateCaller() {
        this.buttonField.setValue(this.getValue());
    }

    public K getButtonField() {
        return this.buttonField;
    }

    public void setButtonField(K buttonField) {
        this.buttonField = buttonField;
    }
}

