/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataListener;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class JEditableComboBox
extends JComboBox {
    private static final long serialVersionUID = -8169312129897519459L;
    private boolean exactMatchSearch = true;
    private static FocusListener nonEditableClearPatternOnFocus = new FocusAdapter(){

        @Override
        public void focusGained(FocusEvent e) {
            JEditableComboBox cbox = (JEditableComboBox)e.getComponent();
            cbox.clearNonEditableSearchPattern();
        }

        @Override
        public void focusLost(FocusEvent e) {
            JEditableComboBox cbox = (JEditableComboBox)e.getComponent();
            cbox.clearNonEditableSearchPattern();
        }
    };
    private static SearchCriteriaInfo DEFAULT_SEARCH_IMPL = new SearchCriteriaInfo(){

        @Override
        public String getStringToSearchIn(Object element) {
            if (element == null) {
                return "";
            }
            ICatalogItem o = (ICatalogItem)element;
            return o.getValue() == null ? "" : o.getValue().toString();
        }

        @Override
        public String getStringToShowOnTextField(Object element) {
            if (element == null) {
                return "";
            }
            ICatalogItem o = (ICatalogItem)element;
            return o == null ? "" : o.toString().trim();
        }

        @Override
        public boolean isElementForNullValue(Object element) {
            if (element == null) {
                return false;
            }
            ICatalogItem o = (ICatalogItem)element;
            return o.getValue() == null;
        }
    };
    private EditorUpdaterComboBoxModel editableModel;
    private Object invalidEntry;
    private SearchCriteriaInfo searchCriteriaInfo = DEFAULT_SEARCH_IMPL;
    private JTextField editorTextField;
    private AutoKeySelection nonEditKeyboardSelection;
    List<KeyEvent> pendingEvents = new ArrayList<KeyEvent>();
    boolean isDispatching;

    public JEditableComboBox() {
        this.setEditable(true);
        this.nonEditKeyboardSelection = new AutoKeySelection();
        this.editorTextField = (JTextField)this.getEditor().getEditorComponent();
        this.getEditor().getEditorComponent().addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent e) {
                JEditableComboBox.this.setSelectedItem(JEditableComboBox.this.editorTextField.getText());
            }
        });
        this.addFocusListener(nonEditableClearPatternOnFocus);
        ((JTextField)this.getEditor().getEditorComponent()).addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                JEditableComboBox.this.setSelectedItem(JEditableComboBox.this.editorTextField.getText());
            }
        });
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public void updateEditorTextField() {
        this.editorTextField.setText(this.searchCriteriaInfo.getStringToShowOnTextField(this.editableModel.getSelectedItem()));
    }

    public void clearNonEditableSearchPattern() {
        if (this.nonEditKeyboardSelection == null) {
            return;
        }
        this.nonEditKeyboardSelection.clearPattern();
    }

    @Override
    public void setEditable(boolean editable) {
        super.setEditable(editable);
        if (!(editable || this.getKeySelectionManager() == this.nonEditKeyboardSelection)) {
            this.setKeySelectionManager(this.nonEditKeyboardSelection);
        }
    }

    public void setModel(ComboBoxModel aModel) {
        this.editableModel = new EditorUpdaterComboBoxModel(aModel, this.exactMatchSearch);
        super.setModel(this.editableModel);
    }

    public SearchCriteriaInfo getSearchElement() {
        return this.searchCriteriaInfo;
    }

    public void setSearchElement(SearchCriteriaInfo searchElement) {
        this.searchCriteriaInfo = searchElement;
    }

    public Object getInvalidEntry() {
        return this.invalidEntry;
    }

    public void setInvalidEntry(Object invalidEntry) {
        this.invalidEntry = invalidEntry;
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (this.isEditable() && this.getEditor() != null && this.getEditor().getEditorComponent() != null) {
            this.getEditor().getEditorComponent().setBackground(bg);
        }
    }

    public void setExactMatchSearch(boolean exactMatchSearch) {
        this.exactMatchSearch = exactMatchSearch;
    }

    public boolean isExactMatchSearch() {
        return this.exactMatchSearch;
    }

    @Override
    protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
        boolean retValue = super.processKeyBinding(ks, e, condition, pressed);
        if (!(retValue || this.editor == null)) {
            if (this.isStartingCellEdit(e)) {
                this.pendingEvents.add(e);
            } else if (this.pendingEvents.size() == 2) {
                this.pendingEvents.add(e);
                this.isDispatching = true;
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        for (KeyEvent event : JEditableComboBox.this.pendingEvents) {
                            JEditableComboBox.this.editor.getEditorComponent().dispatchEvent(event);
                        }
                        JEditableComboBox.this.pendingEvents.clear();
                        JEditableComboBox.this.isDispatching = false;
                    }
                });
            }
        }
        return retValue;
    }

    private boolean isStartingCellEdit(KeyEvent e) {
        if (this.isDispatching) {
            return false;
        }
        JTable table = (JTable)SwingUtilities.getAncestorOfClass(JTable.class, this);
        boolean isOwned = table != null && !Boolean.FALSE.equals((Boolean)table.getClientProperty("JTable.autoStartsEdit"));
        return isOwned && e.getComponent() == table;
    }

    private class AutoKeySelection
    implements JComboBox.KeySelectionManager {
        private StringBuffer patterntToSearch;

        private AutoKeySelection() {
            this.patterntToSearch = new StringBuffer();
        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {
            if (JEditableComboBox.this.isDisplayable()) {
                JEditableComboBox.this.setPopupVisible(false);
            }
            if (aKey == '\b') {
                if (this.patterntToSearch.length() > 0) {
                    this.patterntToSearch.deleteCharAt(this.patterntToSearch.length() - 1);
                }
            } else {
                this.patterntToSearch.append(aKey);
            }
            if (JEditableComboBox.this.editableModel == null) {
                return JEditableComboBox.this.getSelectedIndex();
            }
            if (JEditableComboBox.this.isDisplayable()) {
                JEditableComboBox.this.setPopupVisible(true);
            }
            return JEditableComboBox.this.editableModel.findItemPosThatContainsPattern(this.patterntToSearch.toString());
        }

        private void clearPattern() {
            this.patterntToSearch.setLength(0);
        }
    }

    private class EditorUpdaterComboBoxModel
    extends AbstractListModel
    implements ComboBoxModel {
        private static final long serialVersionUID = -7087035105310617061L;
        private ComboBoxModel originalModel;
        private boolean exactMatch;

        public EditorUpdaterComboBoxModel(ComboBoxModel model, boolean exactMatch) {
            this.originalModel = model;
            this.exactMatch = exactMatch;
        }

        @Override
        public void addListDataListener(ListDataListener l) {
            this.originalModel.addListDataListener(l);
        }

        @Override
        public Object getElementAt(int index) {
            return this.originalModel.getElementAt(index);
        }

        @Override
        public Object getSelectedItem() {
            return this.originalModel.getSelectedItem();
        }

        @Override
        public int getSize() {
            return this.originalModel.getSize();
        }

        @Override
        public void removeListDataListener(ListDataListener l) {
            this.originalModel.removeListDataListener(l);
        }

        @Override
        public void setSelectedItem(Object anItem) {
            if (anItem == null) {
                Object nullObject = this.findNullValueInModel();
                this.originalModel.setSelectedItem(nullObject);
                JEditableComboBox.this.updateEditorTextField();
                return;
            }
            if (!(anItem instanceof String)) {
                this.originalModel.setSelectedItem(anItem);
                JEditableComboBox.this.updateEditorTextField();
                return;
            }
            if (this.getSelectedItem() != null && this.getSelectedItem().toString().trim().equalsIgnoreCase(((String)anItem).trim())) {
                JEditableComboBox.this.updateEditorTextField();
                return;
            }
            if (((String)anItem).trim().isEmpty()) {
                Object nullObject = this.findNullValueInModel();
                this.originalModel.setSelectedItem(nullObject);
                JEditableComboBox.this.updateEditorTextField();
                return;
            }
            Object elm = null;
            elm = this.exactMatch ? this.findExactElementInModel((String)anItem) : this.findFirstElementInModelWithPattern((String)anItem);
            if (elm == null) {
                this.originalModel.setSelectedItem(JEditableComboBox.this.invalidEntry);
                JEditableComboBox.this.updateEditorTextField();
            } else {
                this.originalModel.setSelectedItem(elm);
                JEditableComboBox.this.updateEditorTextField();
            }
        }

        private Object findNullValueInModel() {
            int n = this.originalModel.getSize();
            for (int i = 0; i < n; ++i) {
                Object currentItem = this.originalModel.getElementAt(i);
                if (currentItem == null || !JEditableComboBox.this.searchCriteriaInfo.isElementForNullValue(currentItem)) continue;
                return currentItem;
            }
            return null;
        }

        private Object findExactElementInModel(String toFind) {
            int n = this.originalModel.getSize();
            for (int i = 0; i < n; ++i) {
                Object currentItem = this.originalModel.getElementAt(i);
                if (currentItem == null || !this.findStringInModelElement(currentItem).equalsIgnoreCase(toFind)) continue;
                return currentItem;
            }
            return null;
        }

        private Object findFirstElementInModelWithPattern(String toFind) {
            int n = this.originalModel.getSize();
            for (int i = 0; i < n; ++i) {
                Object currentItem = this.originalModel.getElementAt(i);
                if (currentItem == null || !this.isElementStartsWithpattern(this.findStringInModelElement(currentItem), toFind)) continue;
                return currentItem;
            }
            return null;
        }

        private String findStringInModelElement(Object modelElement) {
            if (JEditableComboBox.this.searchCriteriaInfo == null) {
                return modelElement.toString();
            }
            return JEditableComboBox.this.searchCriteriaInfo.getStringToSearchIn(modelElement);
        }

        private boolean isElementStartsWithpattern(String str1, String pattern) {
            return str1.toUpperCase().startsWith(pattern.toUpperCase().trim());
        }

        private int findItemPosThatContainsPattern(String toFind) {
            int n = this.originalModel.getSize();
            for (int i = 0; i < n; ++i) {
                Object currentItem = this.originalModel.getElementAt(i);
                if (currentItem == null || !this.isPatternInElement(currentItem.toString(), toFind)) continue;
                return i;
            }
            return -1;
        }

        private boolean isPatternInElement(String str1, String pattern) {
            return str1.toUpperCase().contains((CharSequence)pattern.toUpperCase());
        }
    }

    public static interface SearchCriteriaInfo {
        public String getStringToSearchIn(Object var1);

        public boolean isElementForNullValue(Object var1);

        public String getStringToShowOnTextField(Object var1);
    }

}

