/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.UIManager;

public class JRemoveLineIconableButton
extends JButton {
    private static final long serialVersionUID = 3254789736551998793L;

    public JRemoveLineIconableButton() {
        Icon addLineIcon = UIManager.getIcon("Button.removeLine.icon");
        if (addLineIcon != null) {
            this.setIcon(addLineIcon);
        }
    }
}

