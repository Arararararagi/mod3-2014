/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.components.JIconTextField;
import pt.opensoft.swing.listeners.SearchTextListener;

public class JSearchTextField
extends JIconTextField {
    private static final long serialVersionUID = -3993549067365775550L;
    protected JTextComponent searchableTextComponent;
    protected SearchTextListener searchListener;

    public JSearchTextField(JTextComponent searchableTextComponent) {
        this(searchableTextComponent, SearchTextListener.DEFAULT_HILIT_COLOR, SearchTextListener.DEFAULT_HILIT_ALL_COLOR, Color.PINK, null);
    }

    public JSearchTextField(JTextComponent searchableTextComponent, Color hilitColor, Color hilitAllColor, Color errorBackground, Color errorForeground) {
        this.searchableTextComponent = searchableTextComponent;
        this.searchListener = new SearchTextListener(this, this.searchableTextComponent, hilitColor, hilitAllColor);
        this.searchListener.setErrorBackground(errorBackground);
        this.searchListener.setErrorForeground(errorForeground);
        this.getDocument().addDocumentListener(this.searchListener);
        this.registerKeyboardAction(this.searchListener, KeyStroke.getKeyStroke(10, 0), 0);
        this.searchableTextComponent.getDocument().addDocumentListener(this.searchListener);
        this.searchableTextComponent.addPropertyChangeListener("document", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt != null && evt.getNewValue() != null) {
                    ((Document)evt.getNewValue()).addDocumentListener(JSearchTextField.this.searchListener);
                }
            }
        });
        this.setLeftIcon(IconFactory.getIconSmall("preview.png"));
        this.setEmptyText(null);
    }

    public JTextComponent getSearchableTextComponent() {
        return this.searchableTextComponent;
    }

    public boolean isCaseSensitive() {
        return this.searchListener.isCaseSensitive();
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.searchListener.setCaseSensitive(caseSensitive);
    }

    public boolean isAsciiSensitive() {
        return this.searchListener.isAsciiSensitive();
    }

    public void setAsciiSensitive(boolean asciiSensitive) {
        this.searchListener.setAsciiSensitive(asciiSensitive);
    }

}

