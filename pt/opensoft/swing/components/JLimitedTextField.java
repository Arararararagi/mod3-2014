/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.event.FocusListener;
import javax.swing.JTextField;
import javax.swing.text.Document;
import pt.opensoft.swing.FixedNumericDocument;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;

public class JLimitedTextField
extends JTextField {
    private static final long serialVersionUID = 3524850049163076517L;
    private int _maxLength = 0;
    private boolean _onlyNumeric = false;

    public JLimitedTextField() {
        super(new FixedNumericDocument(0, false), null, 0);
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public JLimitedTextField(int columns, int maxLength, boolean onlyNumeric) {
        super(new FixedNumericDocument(maxLength, onlyNumeric), null, columns);
        this._maxLength = maxLength;
        this._onlyNumeric = onlyNumeric;
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public void setMaxLength(int maxLength) {
        this._maxLength = maxLength;
        this.setDocument(new FixedNumericDocument(this._maxLength, this._onlyNumeric));
    }

    public void setOnlyNumeric(boolean onlyNumeric) {
        this._onlyNumeric = onlyNumeric;
        this.setDocument(new FixedNumericDocument(this._maxLength, this._onlyNumeric));
    }

    public int getMaxLength() {
        return this._maxLength;
    }

    public boolean isOnlyNumeric() {
        return this._onlyNumeric;
    }

    public void setAutomaticFocusTransferEnabled(boolean isAutomaticFocusTransferEnabled) {
        ((FixedNumericDocument)this.getDocument()).setAutomaticFocusTransferEnabled(isAutomaticFocusTransferEnabled);
    }
}

