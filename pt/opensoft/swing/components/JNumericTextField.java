/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.NavigationFilter;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.base.filters.RightAlignMaskDocumentController;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;
import pt.opensoft.util.StringUtil;

public class JNumericTextField
extends JTextField
implements FocusListener,
DocumentListener {
    private static final long serialVersionUID = -6985868812579045985L;
    public static final String SUFIX = "";
    public static final String BIG_MASK_WITHOUT_PRECISION = "#.###.###.###.###.### ";
    public static final String BIG_MASK_WITHOUT_PRECISION_AND_DOTS = "################ ";
    protected RightAlignMaskDocumentController controller;
    private String _lastValue;
    private int columns;
    private int decimalPlaces;
    boolean useDots;

    public JNumericTextField() {
        this(12, 2);
    }

    public JNumericTextField(int columns, int decimalPlaces) {
        this(columns, decimalPlaces, false);
    }

    public JNumericTextField(int columns, int decimalPlaces, boolean useDots) {
        this.columns = columns;
        this.decimalPlaces = decimalPlaces;
        this.useDots = useDots;
        this.configureComponent();
        this.addFocusListener(this);
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    private void configureComponent() {
        int tempDecimalPlaces = this.decimalPlaces;
        StringBuffer mask = new StringBuffer("################ ");
        StringBuffer placeHolders = new StringBuffer("");
        if (tempDecimalPlaces > 0) {
            mask.delete(mask.length() - tempDecimalPlaces, mask.length());
            mask.append(",");
            placeHolders.append("0,");
            do {
                mask.append("#");
                placeHolders.append("0");
            } while (--tempDecimalPlaces > 0);
        }
        tempDecimalPlaces = this.decimalPlaces;
        if (this.useDots) {
            int position = mask.length() - tempDecimalPlaces;
            while (position - 3 > 1) {
                mask.replace(position - 1, position-=4, ".");
            }
        }
        mask.append("");
        placeHolders.append("");
        this.controller = new RightAlignMaskDocumentController(mask.toString(), placeHolders.toString(), "", this, mask.length() - 2, false);
        ((AbstractDocument)this.getDocument()).setDocumentFilter(this.controller.getDocumentFilter());
        this.setNavigationFilter(this.controller.getNavigationFilter());
        this.setColumns(Math.min(this.columns, mask.length() - 1));
        this.setHorizontalAlignment(4);
        this.setInputVerifier(new NumericInputVerifier(mask.length()));
    }

    public void allowNegative(boolean startNegative) {
        this.controller.allowPrefix("-", "-", startNegative);
        this.setColumns(this.getColumns());
    }

    @Override
    public void setColumns(int s) {
        this.columns = s;
        this.controller.setSize(s);
        super.setColumns(this.getColumnsWithPunctuation(s));
    }

    @Override
    public int getColumns() {
        return this.columns;
    }

    private int getColumnsWithPunctuation(int numColumns) {
        return numColumns + (this.controller.hasPrefix() ? this.controller.prefixSize() : 0);
    }

    public String getValue() {
        return this.controller.getPureContent();
    }

    @Override
    public void copy() {
        JTextField jTextField = new JTextField(this.controller.getPureContent(this.getSelectionStart(), this.getSelectionEnd()));
        jTextField.selectAll();
        jTextField.copy();
    }

    public void setValue(String s) {
        if (s == null) {
            return;
        }
        this.controller.setPureContent(s);
    }

    public int getDecimalPlaces() {
        return this.decimalPlaces;
    }

    public void setDecimalPlaces(int decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
        this.configureComponent();
    }

    public boolean isUseDots() {
        return this.useDots;
    }

    public void setUseDots(boolean useDots) {
        this.useDots = useDots;
        this.configureComponent();
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        this.firePropertyChange("value", this._lastValue, this.getValue());
        this._lastValue = this.getValue();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.firePropertyChange("value", this._lastValue, this.getValue());
        this._lastValue = this.getValue();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.firePropertyChange("value", this._lastValue, this.getValue());
        this._lastValue = this.getValue();
    }

    class NumericInputVerifier
    extends InputVerifier {
        int maxSize;

        public NumericInputVerifier(int maxSize) {
            this.maxSize = maxSize;
        }

        @Override
        public boolean verify(JComponent input) {
            String number = JNumericTextField.this.controller.getPureContent();
            if (!StringUtil.isEmpty(number)) {
                JNumericTextField.this.setForeground(UIManager.getColor("TextField.color"));
            }
            if (number.length() > this.maxSize) {
                String s = JNumericTextField.this.controller.getPureContent();
                JNumericTextField.this.controller.setPureContent("");
                JNumericTextField.this.controller.setPureContent(s);
                JNumericTextField.this.setForeground(Color.RED);
                JNumericTextField.this.repaint();
                return false;
            }
            return true;
        }
    }

}

