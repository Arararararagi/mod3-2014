/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.UIManager;

public class JAddLineIconableButton
extends JButton {
    private static final long serialVersionUID = 3254789736551998793L;

    public JAddLineIconableButton() {
        Icon addLineIcon = UIManager.getIcon("Button.addLine.icon");
        if (addLineIcon != null) {
            this.setIcon(addLineIcon);
        }
    }
}

