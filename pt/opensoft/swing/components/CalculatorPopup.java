/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.ObservableElementList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Iterator;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import pt.dgci.modelo3irs.v2015.Modelo3IRSv2015Application;
import pt.dgci.modelo3irs.v2015.gui.Modelo3IRSv2015ApplicationMainFrame;
import pt.opensoft.swing.BrowserControl;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.components.CalculatorButtonField;
import pt.opensoft.swing.components.JAddLineIconableButton;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.JRemoveLineIconableButton;
import pt.opensoft.swing.components.PrintCalculatorRenderer;
import pt.opensoft.swing.components.popup.SimpleGenericSettablePopup;
import pt.opensoft.swing.editor.JMoneyCellEditor;
import pt.opensoft.swing.model.calculadora.TabelaCalculadora_Linha;
import pt.opensoft.swing.model.calculadora.TabelaCalculadora_LinhaAdapterFormat;
import pt.opensoft.swing.renderer.JMoneyCellRenderer;
import pt.opensoft.swing.renderer.RowIndexTableCellRenderer;
import pt.opensoft.taxclient.util.Session;

public class CalculatorPopup
extends SimpleGenericSettablePopup<CalculatorButtonField> {
    private static final long serialVersionUID = 5122565237318300630L;
    protected Long value;
    protected EventList<TabelaCalculadora_Linha> linhasCalculadora = new BasicEventList<TabelaCalculadora_Linha>();
    private JLabel dialogTitleLabel;
    private JLabel label2;
    private JTextField nLinhas;
    private JButton button4;
    private JScrollPane scrollPane1;
    protected JTable calculatorEntriesTable;
    private JToolBar toolBar2;
    private JAddLineIconableButton button1;
    private JRemoveLineIconableButton button3;
    private JLabel label1;
    private JMoneyTextField somatorio;
    private JToolBar toolBar1;
    private JButton button2;

    public CalculatorPopup() {
        super(((Modelo3IRSv2015ApplicationMainFrame)Session.getMainFrame()).getApp());
        this.initComponents();
        this.initTableModel();
        this.button2.setIcon(IconFactory.getIconSmall("Check.gif"));
        this.dialogTitleLabel.setIcon(IconFactory.getIconSmall("xcalc.gif"));
        this.bindListeners();
        this.adicionarValorAction(null);
    }

    public EventList<TabelaCalculadora_Linha> getLinhasCalculadora() {
        return this.linhasCalculadora;
    }

    @Override
    public void setVisible(boolean isVisible) {
        if (this.calculatorEntriesTable.getRowCount() > 0) {
            this.focusOnLastCell();
        }
        super.setVisible(isVisible);
    }

    private void focusOnLastCell() {
        this.calculatorEntriesTable.editingCanceled(null);
        if (this.calculatorEntriesTable.editCellAt(this.linhasCalculadora.size() - 1, 1)) {
            this.calculatorEntriesTable.changeSelection(this.linhasCalculadora.size() - 1, 1, true, true);
            this.calculatorEntriesTable.requestFocus();
            this.calculatorEntriesTable.setSelectionMode(0);
        }
    }

    private void initTableModel() {
        this.calculatorEntriesTable.setColumnSelectionAllowed(true);
        this.calculatorEntriesTable.setRowSelectionAllowed(true);
        ObservableElementList.Connector tableConnectorLinhasCalculadora = GlazedLists.beanConnector(TabelaCalculadora_Linha.class);
        ObservableElementList<TabelaCalculadora_Linha> tableElementListCampoTabela_Linha = new ObservableElementList<TabelaCalculadora_Linha>(this.linhasCalculadora, tableConnectorLinhasCalculadora);
        this.calculatorEntriesTable.setModel(new EventTableModel<TabelaCalculadora_Linha>(tableElementListCampoTabela_Linha, new TabelaCalculadora_LinhaAdapterFormat()));
        this.calculatorEntriesTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        this.calculatorEntriesTable.getTableHeader().setReorderingAllowed(false);
        this.calculatorEntriesTable.getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());
        this.calculatorEntriesTable.getColumnModel().getColumn(0).setMaxWidth(22);
        JMoneyCellEditor moneyCellEditor = new JMoneyCellEditor();
        moneyCellEditor.getComponent().addKeyListener(new CalculatorKeyHandler(this));
        this.calculatorEntriesTable.getColumnModel().getColumn(1).setCellEditor(moneyCellEditor);
        this.calculatorEntriesTable.getColumnModel().getColumn(1).setCellRenderer(new JMoneyCellRenderer());
        this.linhasCalculadora.addListEventListener(new ListEventListener(){

            public void listChanged(ListEvent listChanges) {
                try {
                    CalculatorPopup.this.updateValor();
                    CalculatorPopup.this.somatorio.setText(String.valueOf(CalculatorPopup.this.value));
                    CalculatorPopup.this.nLinhas.setText(String.valueOf(CalculatorPopup.this.linhasCalculadora.size()));
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private void bindListeners() {
        this.addKeyListener(new CalculatorKeyHandler(this));
        this.button2.addKeyListener(new CalculatorKeyHandler(this));
        this.calculatorEntriesTable.addKeyListener(new CalculatorKeyHandler(this));
        this.dialogTitleLabel.addKeyListener(new CalculatorKeyHandler(this));
        this.label2.addKeyListener(new CalculatorKeyHandler(this));
        this.nLinhas.addKeyListener(new CalculatorKeyHandler(this));
        this.scrollPane1.addKeyListener(new CalculatorKeyHandler(this));
        this.toolBar2.addKeyListener(new CalculatorKeyHandler(this));
        this.button1.addKeyListener(new CalculatorKeyHandler(this));
        this.button3.addKeyListener(new CalculatorKeyHandler(this));
        this.label1.addKeyListener(new CalculatorKeyHandler(this));
        this.somatorio.addKeyListener(new CalculatorKeyHandler(this));
        this.toolBar1.addKeyListener(new CalculatorKeyHandler(this));
    }

    public CalculatorPopup(Frame owner) {
        super(owner);
        this.initComponents();
    }

    public CalculatorPopup(Dialog owner) {
        super(owner);
        this.initComponents();
    }

    private void updateValor() {
        this.value = 0;
        Iterator<TabelaCalculadora_Linha> i$ = this.linhasCalculadora.iterator();
        while (i$.hasNext()) {
            TabelaCalculadora_Linha entry;
            this.value = this.value + ((entry = i$.next()).getValor() != null ? entry.getValor() : 0);
        }
    }

    public void gravarValor(ActionEvent e) {
        this.updateValor();
        this.updateCaller();
        this.setVisible(false);
    }

    public JTable getCalculatorEntriesTable() {
        return this.calculatorEntriesTable;
    }

    public void adicionarValorAction(ActionEvent e) {
        this.linhasCalculadora.add(new TabelaCalculadora_Linha());
        if (this.calculatorEntriesTable.editCellAt(this.linhasCalculadora.size() - 1, 1)) {
            this.focusOnLastCell();
        }
    }

    public void removeAllLines() {
        if (this.calculatorEntriesTable.editCellAt(0, 0)) {
            this.calculatorEntriesTable.changeSelection(0, 0, true, true);
            this.calculatorEntriesTable.requestFocus();
        }
        this.linhasCalculadora.clear();
    }

    public void removerValorAction(ActionEvent e) {
        int selectedRow;
        int n = selectedRow = this.calculatorEntriesTable.getSelectedRow() != -1 ? this.calculatorEntriesTable.getSelectedRow() : this.calculatorEntriesTable.getRowCount() - 1;
        if (selectedRow != -1) {
            this.linhasCalculadora.remove(selectedRow);
            this.focusOnLastCell();
            if (this.linhasCalculadora.size() == 0) {
                this.adicionarValorAction(null);
            }
        }
    }

    public JLabel getDialogTitleLabel() {
        return this.dialogTitleLabel;
    }

    private void ImprimirCalculadoraAction(ActionEvent e) throws Exception {
        if (this.linhasCalculadora != null) {
            try {
                File tempFile = File.createTempFile("print", ".pdf", new File(System.getProperty("user.home")));
                FileOutputStream os = new FileOutputStream(tempFile);
                PrintCalculatorRenderer renderer = new PrintCalculatorRenderer(os);
                tempFile.deleteOnExit();
                renderer.render(this.dialogTitleLabel.getText(), this.linhasCalculadora, this.value);
                BrowserControl.displayURL("file:///" + tempFile.getCanonicalPath());
            }
            catch (Exception e1) {
                throw new RuntimeException(e1);
            }
        }
    }

    private void initComponents() {
        this.dialogTitleLabel = new JLabel();
        this.label2 = new JLabel();
        this.nLinhas = new JTextField();
        this.button4 = new JButton();
        this.scrollPane1 = new JScrollPane();
        this.calculatorEntriesTable = new JTable();
        this.toolBar2 = new JToolBar();
        this.button1 = new JAddLineIconableButton();
        this.button3 = new JRemoveLineIconableButton();
        this.label1 = new JLabel();
        this.somatorio = new JMoneyTextField();
        this.toolBar1 = new JToolBar();
        this.button2 = new JButton();
        CellConstraints cc = new CellConstraints();
        this.setTitle("Calculadora");
        this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new FormLayout("2*(default, $lcgap), 31dlu, $lcgap, 42dlu, $lcgap, default, 2*($lcgap, default:grow), $lcgap", "default, $lgap, fill:min, 5*($lgap, default)"));
        this.dialogTitleLabel.setFont(this.dialogTitleLabel.getFont().deriveFont(this.dialogTitleLabel.getFont().getStyle() | 1));
        contentPane.add((Component)this.dialogTitleLabel, cc.xywh(3, 3, 11, 1));
        this.label2.setText("N\u00ba de Linhas");
        contentPane.add((Component)this.label2, cc.xy(3, 7));
        this.nLinhas.setEditable(false);
        contentPane.add((Component)this.nLinhas, cc.xy(5, 7));
        this.button4.setText("Imprimir");
        this.button4.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    CalculatorPopup.this.ImprimirCalculadoraAction(e);
                }
                catch (Exception e1) {
                    throw new RuntimeException(e1);
                }
            }
        });
        contentPane.add((Component)this.button4, cc.xy(13, 7));
        this.scrollPane1.setViewportView(this.calculatorEntriesTable);
        contentPane.add((Component)this.scrollPane1, cc.xywh(3, 9, 12, 1));
        this.toolBar2.setFloatable(false);
        this.toolBar2.setRollover(true);
        this.button1.setText("Adicionar Valor");
        this.button1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                CalculatorPopup.this.adicionarValorAction(e);
            }
        });
        this.toolBar2.add(this.button1);
        this.toolBar2.addSeparator(new Dimension(20, 0));
        this.button3.setText("Remover Valor");
        this.button3.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                CalculatorPopup.this.removerValorAction(e);
            }
        });
        this.toolBar2.add(this.button3);
        contentPane.add((Component)this.toolBar2, cc.xywh(1, 11, 14, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        this.label1.setText("Total:");
        contentPane.add((Component)this.label1, cc.xywh(3, 13, 1, 1, CellConstraints.LEFT, CellConstraints.DEFAULT));
        contentPane.add((Component)this.somatorio, cc.xywh(5, 13, 3, 1));
        this.toolBar1.setFloatable(false);
        this.toolBar1.setRollover(true);
        this.button2.setText("Transferir Valor");
        this.button2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                CalculatorPopup.this.gravarValor(e);
            }
        });
        this.toolBar1.add(this.button2);
        contentPane.add((Component)this.toolBar1, cc.xywh(11, 13, 3, 1));
        this.pack();
        this.setLocationRelativeTo(this.getOwner());
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    static class CalculatorKeyHandler
    implements KeyListener {
        private CalculatorPopup panel;

        public CalculatorKeyHandler(CalculatorPopup panel) {
            this.panel = panel;
        }

        @Override
        public void keyPressed(KeyEvent e) {
            int code = e.getKeyCode();
            if (code == 10) {
                this.panel.gravarValor(null);
            } else if (code == 27) {
                this.panel.dispose();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            char keyChar = e.getKeyChar();
            if (keyChar == '+') {
                this.panel.adicionarValorAction(null);
            } else if (keyChar == '-') {
                this.panel.removerValorAction(null);
            } else {
                int row = this.panel.getCalculatorEntriesTable().getSelectedRow();
                if (this.panel.getCalculatorEntriesTable().editCellAt(row, 1)) {
                    this.panel.getCalculatorEntriesTable().getEditorComponent().requestFocus();
                }
            }
        }

        @Override
        public void keyTyped(KeyEvent arg0) {
        }
    }

}

