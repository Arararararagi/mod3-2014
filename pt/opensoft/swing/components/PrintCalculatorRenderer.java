/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import ca.odell.glazedlists.EventList;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.Iterator;
import pt.opensoft.field.EuroField;
import pt.opensoft.pdf.DynamicPDFRenderer;
import pt.opensoft.swing.model.calculadora.TabelaCalculadora_Linha;

public class PrintCalculatorRenderer
extends DynamicPDFRenderer {
    public static final float SPACING = 1.0f;
    private EventList<TabelaCalculadora_Linha> linhasCalculadora;
    private String title;
    private Font headerFont = new Font();
    private Long valorTotal;

    public PrintCalculatorRenderer(OutputStream stream) throws FileNotFoundException, DocumentException {
        super(stream);
        this.headerFont.setSize(20.0f);
    }

    public void render(String title, EventList<TabelaCalculadora_Linha> linhasCalculadora, Long valorTotal) throws Exception {
        this.linhasCalculadora = linhasCalculadora;
        this.title = title;
        this.valorTotal = valorTotal;
        this.openDocument();
        this.buildDocument();
        this.closeDocument();
    }

    private void buildDocument() throws Exception {
        float[] colWith = new float[]{3.0f, 4.0f, 3.0f};
        this.emptySpace();
        this.newLine();
        this.emptySpace();
        this.newLine();
        this.emptySpace();
        this.newLine();
        this.emptySpace();
        this.startTable(colWith);
        this.startCell();
        this.endCell();
        this.startCell(1, 1, 1, 5, 0, true);
        this.field("Calculadora", this.headerFont);
        this.endCell();
        this.startCell();
        this.endCell();
        this.endTable();
        this.newLine();
        this.emptySpace();
        this.startTable(colWith);
        this.startCell();
        this.endCell();
        this.startCell(1, 1, 1, 5, 15, true);
        this.field("C\u00e1lculos efectuados para o campo " + this.title);
        this.endCell();
        this.startCell();
        this.endCell();
        this.endTable();
        this.newLine();
        this.emptySpace();
        this.startTable(colWith);
        this.startCell();
        this.endCell();
        this.startCell(1, 1, 1, 5, 15, true);
        this.field("N\u00ba de linhas", this.linhasCalculadora.size());
        this.endCell();
        this.startCell();
        this.endCell();
        this.endTable();
        this.startTable(colWith);
        this.startCell();
        this.endCell();
        this.startCell(1, 1, 1, 5, 0);
        this.field("Valores");
        this.endCell();
        this.startCell();
        this.endCell();
        this.endTable();
        Iterator<TabelaCalculadora_Linha> it = this.linhasCalculadora.iterator();
        EuroField euroFormat = new EuroField("euroFormat");
        while (it.hasNext()) {
            Long value = it.next().getValor();
            if (value == null) {
                value = new Long(0);
            }
            euroFormat.setValue(value);
            euroFormat.setPrecision(2);
            this.setValue(euroFormat.format());
        }
        float[] colWith2 = new float[]{3.0f, 0.1f, 3.9f, 3.0f};
        this.startTable(colWith2, true);
        this.startCell();
        this.endCell();
        this.startCell();
        this.endCell();
        this.startCell(1, 1, 2, 5, 0, true);
        euroFormat.setValue(this.valorTotal);
        euroFormat.setPrecision(2);
        this.field("Total", euroFormat.format() + "\u20ac");
        this.endCell();
        this.startCell();
        this.endCell();
        this.endTable();
    }

    private void setValue(String value) throws DocumentException {
        float[] colWith = new float[]{3.0f, 0.2f, 3.8f, 3.0f};
        this.startTable(colWith, true);
        this.startCell();
        this.endCell();
        this.startCell(1, 1, 0, 5, 0, true);
        this.field("\u20ac");
        this.endCell();
        this.startCell(1, 1, 2, 5, 15, true);
        this.field(value);
        this.endCell();
        this.startCell();
        this.endCell();
        this.endTable();
    }
}

