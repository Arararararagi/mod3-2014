/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import ca.odell.glazedlists.EventList;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.components.CalculatorPopup;
import pt.opensoft.swing.components.JButtonSimpleTextField;
import pt.opensoft.swing.components.JMoneyTextField;
import pt.opensoft.swing.components.popup.SimpleGenericSettablePopup;
import pt.opensoft.swing.model.calculadora.TabelaCalculadora_Linha;
import pt.opensoft.util.StringUtil;

public class CalculatorButtonField
extends JButtonSimpleTextField<JMoneyTextField, CalculatorPopup> {
    private static final long serialVersionUID = 433273879164132334L;
    private String dialogTitle;

    public CalculatorButtonField() {
        super(new JMoneyTextField());
        this.setJButtonDialog(CalculatorPopup.class);
        this.button.setText("");
        this.button.setBackground(UIManager.getColor("Button.Calculator.background"));
        this.button.setIcon(IconFactory.getIconSmall("xcalc.gif"));
        this.button.setPreferredSize(new Dimension(20, 20));
        this.button.setMinimumSize(new Dimension(10, 10));
        this.button.setMaximumSize(new Dimension(20, 20));
        this.button.setToolTipText("Calculadora");
        this.button.setFocusable(false);
        this.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent arg0) {
                CalculatorButtonField.this.getTextComponent().requestFocus();
            }

            @Override
            public void focusLost(FocusEvent arg0) {
            }
        });
    }

    @Override
    public void setAffectableFields(CalculatorPopup popupComponent) {
        long valueToSet;
        long popupValue = popupComponent.getValue() == null ? 0 : (Long)popupComponent.getValue();
        long l = valueToSet = StringUtil.isEmpty(((JMoneyTextField)this.textComponent).getValue()) ? 0 : Long.parseLong(((JMoneyTextField)this.textComponent).getValue());
        if (popupValue != valueToSet) {
            TabelaCalculadora_Linha linha = new TabelaCalculadora_Linha();
            linha.setValor(valueToSet);
            popupComponent.getLinhasCalculadora().add(linha);
            for (int i = popupComponent.getLinhasCalculadora().size() - 2; i >= 0; --i) {
                popupComponent.getLinhasCalculadora().remove(i);
            }
        }
        popupComponent.getDialogTitleLabel().setText(this.dialogTitle);
    }

    public String getDialogTitle() {
        return this.dialogTitle;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    public JButton getButton() {
        return this.button;
    }

}

