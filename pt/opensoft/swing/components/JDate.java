/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import com.toedter.calendar.IDateEditor;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import pt.opensoft.swing.components.IBindableUIComponent;
import pt.opensoft.util.StringUtil;

public class JDate
extends JDateChooser
implements IBindableUIComponent {
    private static final long serialVersionUID = -832175712425991256L;
    public static final String DATE = "date";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    private String dateValueStr;
    private boolean editable;

    public JDate() {
        this.setDateFormatString("yyyy-MM-dd");
        this.addPropertyChangeListener("date", new DatePropertyChangeListener());
        ((JTextFieldDateEditor)this.dateEditor.getUiComponent()).addPropertyChangeListener("background", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (!JDate.this.getBackground().equals((Color)evt.getNewValue())) {
                    JDate.this.setBackground((Color)evt.getNewValue());
                }
            }
        });
    }

    @Override
    public Object getValue() {
        return this.dateValueStr;
    }

    @Override
    public void setValue(Object value) {
        this.dateValueStr = (String)value;
        if (!StringUtil.isEmpty((String)value)) {
            this.setDate(new pt.opensoft.util.Date((String)value));
        } else {
            this.setDate(null);
        }
    }

    private String dateToString(Date dateToConvert) {
        if (dateToConvert == null) {
            return null;
        }
        pt.opensoft.util.Date date = new pt.opensoft.util.Date(dateToConvert);
        date.setFormat("yyyy-MM-dd");
        return date.format();
    }

    public boolean isDateSelected() {
        return this.dateSelected;
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (this.dateEditor != null && this.dateEditor.getUiComponent() != null) {
            this.dateEditor.getUiComponent().setBackground(bg);
        }
    }

    @Override
    public synchronized void addMouseListener(MouseListener l) {
        super.addMouseListener(l);
        if (this.dateEditor != null && this.dateEditor.getUiComponent() != null) {
            this.dateEditor.getUiComponent().addMouseListener(l);
        }
    }

    @Override
    public void setCursor(Cursor cursor) {
        super.setCursor(cursor);
        if (this.dateEditor != null && this.dateEditor.getUiComponent() != null) {
            this.dateEditor.getUiComponent().setCursor(cursor);
        }
    }

    public void setEditable(boolean editable) {
        if (editable != this.editable && this.dateEditor != null && this.dateEditor.getUiComponent() != null) {
            ((JTextFieldDateEditor)this.dateEditor.getUiComponent()).setEditable(editable);
            this.dateEditor.getUiComponent().setFocusable(editable);
            this.calendarButton.setEnabled(editable);
            this.calendarButton.setFocusable(editable);
            boolean oldValue = this.editable;
            this.editable = editable;
            this.firePropertyChange("editable", oldValue, this.editable);
        }
    }

    public boolean getEditable() {
        return this.editable;
    }

    public JPopupMenu getPopupMenu() {
        return this.popup;
    }

    class DatePropertyChangeListener
    implements PropertyChangeListener {
        DatePropertyChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals("date")) {
                JDate.this.firePropertyChange("value", JDate.this.dateToString((Date)evt.getOldValue()), JDate.this.dateToString((Date)evt.getNewValue()));
            }
        }
    }

}

