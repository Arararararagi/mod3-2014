/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import pt.opensoft.field.NifValidator;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.base.filters.LimitedSizeDocumentFilter;
import pt.opensoft.swing.base.filters.NumericDocumentFilter;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;
import pt.opensoft.util.StringUtil;

public class JNIFTextField
extends JTextField
implements FocusListener {
    private static final long serialVersionUID = -4607502149939901526L;
    private static final int NIF_SIZE = 9;
    private String _lastValue;
    private LimitedSizeDocumentFilter limitedSizeDocumentFilter = new LimitedSizeDocumentFilter(9);

    public JNIFTextField() {
        ((AbstractDocument)this.getDocument()).setDocumentFilter(new NumericDocumentFilter(this.limitedSizeDocumentFilter));
        this.setHorizontalAlignment(4);
        this.setMinimumSize(this.getPreferredSize());
        this.setInputVerifier(new InputVerifier(){

            @Override
            public boolean verify(JComponent input) {
                String nif = JNIFTextField.this.getValue();
                if (!StringUtil.isEmpty(nif)) {
                    JNIFTextField.this.setForeground(UIManager.getColor("TextField.color"));
                }
                if (!NifValidator.validate(nif)) {
                    String s = JNIFTextField.this.getText();
                    JNIFTextField.this.setText("");
                    JNIFTextField.this.setText(s);
                    JNIFTextField.this.setForeground(Color.RED);
                    JNIFTextField.this.repaint();
                    return true;
                }
                return true;
            }
        });
        this.addFocusListener(this);
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public String getValue() {
        return this.getText();
    }

    @Override
    public String getText() {
        if (!StringUtil.isEmpty(super.getText())) {
            this.setForeground(UIManager.getColor("TextField.color"));
        }
        if (!NifValidator.validate(super.getText())) {
            this.setForeground(Color.RED);
            this.repaint();
        }
        return super.getText();
    }

    public void setValue(String value) {
        if (value == null) {
            return;
        }
        this.setText(value);
    }

    @Override
    public void setText(String t) {
        super.setText(t);
        if (!StringUtil.isEmpty(super.getText())) {
            this.setForeground(UIManager.getColor("TextField.color"));
        }
        if (!NifValidator.validate(super.getText())) {
            this.setForeground(Color.RED);
            this.repaint();
        }
    }

    @Override
    public void copy() {
        JTextField jTextField = new JTextField(this.getText());
        jTextField.selectAll();
        jTextField.copy();
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        this.firePropertyChange("value", this._lastValue, this.getValue());
        this._lastValue = this.getValue();
    }

    public void setAutomaticFocusTransferEnabled(boolean isAutomaticFocusTransferEnabled) {
        this.limitedSizeDocumentFilter.setAutomaticFocusTransferEnabled(isAutomaticFocusTransferEnabled);
    }

}

