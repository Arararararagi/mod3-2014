/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import pt.opensoft.swing.components.IBindableUIComponent;
import pt.opensoft.swing.components.popup.SimpleGenericSettablePopup;
import pt.opensoft.util.StringUtil;

public abstract class JButtonSimpleTextField<K extends JTextField, P extends SimpleGenericSettablePopup>
extends JPanel
implements IBindableUIComponent {
    private static final long serialVersionUID = 4863655441041609914L;
    private static final String DEFAULT_BUTTON_LABEL = "...";
    private static final Boolean DEFAULT_EDITABLE_STATE = true;
    protected static final int DEFAULT_BUTTON_WIDTH = 32;
    protected static final int DEFAULT_BUTTON_HEIGHT = 16;
    protected K textComponent;
    protected JButton button;
    protected Class<P> dialog;
    protected P popupComponent;
    protected Boolean editable;
    protected Boolean canceled;

    public JButtonSimpleTextField(K componentToUse) {
        this.textComponent = componentToUse;
        this.textComponent.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                String value = (String)JButtonSimpleTextField.this.getValue();
                JButtonSimpleTextField.this.firePropertyChange("value", null, value);
            }
        });
        this.textComponent.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent arg0) {
                String value = (String)JButtonSimpleTextField.this.getValue();
                JButtonSimpleTextField.this.firePropertyChange("value", null, value);
            }
        });
        this.initComponents();
        this.groupComponents();
    }

    private void groupComponents() {
        CellConstraints cc = new CellConstraints();
        FormLayout f = new FormLayout("default:grow, default, default", "default:grow");
        this.setLayout(f);
        this.add((Component)this.textComponent, cc.xy(1, 1));
        this.add((Component)this.button, cc.xy(3, 1));
    }

    private void initComponents() {
        this.button = new JButton("...");
        this.button.setAlignmentY(0.5f);
        this.button.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                JButtonSimpleTextField.this.editable = false;
                JButtonSimpleTextField.this.canceled = false;
                JButtonSimpleTextField.this.openPopup();
            }
        });
    }

    private void openPopup() {
        try {
            if (this.popupComponent == null) {
                this.popupComponent = (SimpleGenericSettablePopup)this.dialog.newInstance();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }
        this.popupComponent.setButtonField((JButtonSimpleTextField)this);
        this.setAffectableFields(this.popupComponent);
        this.popupComponent.setVisible(true);
    }

    public abstract void setAffectableFields(P var1);

    public JTextField getTextComponent() {
        return this.textComponent;
    }

    public void setJButtonDialog(Class<P> dialog) {
        this.dialog = dialog;
    }

    @Override
    public Object getValue() {
        return this.textComponent.getText();
    }

    @Override
    public void setValue(Object value) {
        String textValue = String.valueOf(value);
        if (!StringUtil.isEmpty(textValue)) {
            this.textComponent.setText(textValue);
        } else {
            this.textComponent.setText("");
        }
        this.firePropertyChange("value", null, textValue);
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
    }

    public boolean isEditable() {
        return this.editable != null ? this.editable : DEFAULT_EDITABLE_STATE;
    }

    public boolean isCanceled() {
        return this.canceled;
    }

}

