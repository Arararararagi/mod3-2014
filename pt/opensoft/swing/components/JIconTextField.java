/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.Icon;
import javax.swing.JTextField;
import javax.swing.UIManager;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;

public class JIconTextField
extends JTextField {
    private static final long serialVersionUID = 788407382294916988L;
    private Icon leftIcon;
    private Icon rightIcon;
    private String emptyText;
    private Insets insets;

    public JIconTextField() {
        this.insets = super.getInsets();
        this.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                JIconTextField.this.repaint();
            }

            @Override
            public void focusLost(FocusEvent e) {
                JIconTextField.this.repaint();
            }
        });
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public void setLeftIcon(Icon leftIcon) {
        this.leftIcon = leftIcon;
    }

    public Icon getLeftIcon() {
        return this.leftIcon;
    }

    public void setRightIcon(Icon rightIcon) {
        this.rightIcon = rightIcon;
    }

    public Icon getRightIcon() {
        return this.rightIcon;
    }

    public String getEmptyText() {
        return this.emptyText;
    }

    public void setEmptyText(String emptyText) {
        this.emptyText = emptyText;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Insets parentInserts = super.getInsets();
        int textLeftInsert = 2;
        if (this.leftIcon != null) {
            int iconWidth = this.leftIcon.getIconWidth();
            int iconHeight = this.leftIcon.getIconHeight();
            int x = parentInserts.left + 5;
            int y = (this.getHeight() - iconHeight) / 2;
            textLeftInsert = x + iconWidth + 2;
            this.leftIcon.paintIcon(this, g, x, y);
        }
        int textRightInsert = 2;
        if (this.rightIcon != null) {
            int iconWidth = this.rightIcon.getIconWidth();
            int iconHeight = this.rightIcon.getIconHeight();
            int x = this.getWidth() - iconWidth - parentInserts.right - 5;
            int y = (this.getHeight() - iconHeight) / 2;
            textRightInsert = parentInserts.right + iconWidth + 2;
            this.rightIcon.paintIcon(this, g, x, y);
        }
        this.insets.set(parentInserts.top, parentInserts.left + textLeftInsert, parentInserts.bottom, parentInserts.right + textRightInsert);
        if (this.emptyText != null && !this.hasFocus() && this.getText().equals("")) {
            int height = this.getHeight();
            Color prevColor = g.getColor();
            g.setColor(UIManager.getColor("textInactiveText"));
            int h = g.getFontMetrics().getHeight();
            int textBottom = (height - h) / 2 + h - 4;
            int x = this.getInsets().left;
            Graphics2D g2d = (Graphics2D)g;
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.drawString(this.emptyText, x, textBottom);
            g.setColor(prevColor);
        }
    }

    @Override
    public Insets getInsets() {
        return this.insets;
    }

}

