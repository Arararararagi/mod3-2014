/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.NavigationFilter;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.base.filters.LeftAlignMaskDocumentController;
import pt.opensoft.swing.base.filters.RightAlignMaskDocumentController;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;

public class JMaskTextField
extends JTextField
implements FocusListener,
DocumentListener {
    private static final long serialVersionUID = -3822067689774062697L;
    public static final String SUFIX = "";
    Object controller;
    String mask = null;
    private String _lastValue;
    private boolean isNIB;

    public JMaskTextField(String mask, String placeholders) {
        this(mask, placeholders, false);
    }

    public JMaskTextField(String mask, String placeholders, boolean isNIB) {
        this.mask = mask;
        this.isNIB = isNIB;
        if (isNIB) {
            this.controller = new LeftAlignMaskDocumentController(mask, placeholders, this, mask.length());
            ((AbstractDocument)this.getDocument()).setDocumentFilter(((LeftAlignMaskDocumentController)this.controller).getDocumentFilter());
            this.setNavigationFilter(((LeftAlignMaskDocumentController)this.controller).getNavigationFilter());
            this.setColumns(mask.replace((CharSequence)".", (CharSequence)"").length());
            this.setHorizontalAlignment(2);
            this.setMinimumSize(this.getPreferredSize());
        } else {
            if (placeholders.length() > 16) {
                throw new RuntimeException("O tamanho da m\u00e1scara excede o limite m\u00e1ximo permitido. (actual: " + placeholders.length() + ", max: 16");
            }
            this.controller = new RightAlignMaskDocumentController(mask, placeholders, "", this, placeholders.length(), false);
            ((AbstractDocument)this.getDocument()).setDocumentFilter(((RightAlignMaskDocumentController)this.controller).getDocumentFilter());
            this.setNavigationFilter(((RightAlignMaskDocumentController)this.controller).getNavigationFilter());
            this.setColumns(placeholders.length());
            this.setHorizontalAlignment(2);
        }
        this.addFocusListener(this);
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public String getValue() {
        String out = "";
        out = this.isNIB ? ((LeftAlignMaskDocumentController)this.controller).getPureContent() : ((RightAlignMaskDocumentController)this.controller).getPureContent();
        return out;
    }

    public void setValue(String s) {
        if (this.isNIB) {
            ((LeftAlignMaskDocumentController)this.controller).setPureContent(s.replace((CharSequence)".", (CharSequence)""));
        } else {
            ((RightAlignMaskDocumentController)this.controller).setPureContent(s);
        }
    }

    @Override
    public void setText(String s) {
        if (this.isNIB) {
            super.setText(s.replace((CharSequence)".", (CharSequence)""));
        } else {
            super.setText(s);
        }
    }

    @Override
    public String getText() {
        if (this.isNIB) {
            return this.getValue().replace((CharSequence)".", (CharSequence)"");
        }
        return this.getValue();
    }

    @Override
    public void setColumns(int s) {
        if (this.isNIB) {
            ((LeftAlignMaskDocumentController)this.controller).setSize(s);
        } else {
            ((RightAlignMaskDocumentController)this.controller).setSize(s);
        }
        super.setColumns(this.mask.length());
    }

    @Override
    public void copy() {
        JTextField jTextField = this.isNIB ? new JTextField(((LeftAlignMaskDocumentController)this.controller).getPureContent()) : new JTextField(((RightAlignMaskDocumentController)this.controller).getPureContent());
        jTextField.selectAll();
        jTextField.copy();
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        this.firePropertyChange("value", this._lastValue, this.getValue());
        this._lastValue = this.getValue();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.firePropertyChange("value", this._lastValue, this.getValue());
        this._lastValue = this.getValue();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.firePropertyChange("value", this._lastValue, this.getValue());
        this._lastValue = this.getValue();
    }
}

