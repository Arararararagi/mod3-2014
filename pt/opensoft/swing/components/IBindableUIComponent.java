/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

public interface IBindableUIComponent {
    public static final String PROPERTYNAME_VALUE = "value";

    public Object getValue();

    public void setValue(Object var1);
}

