/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Dimension;
import java.awt.event.FocusListener;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.NavigationFilter;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.base.filters.DocumentController;
import pt.opensoft.swing.base.filters.RightAlignMaskDocumentController;
import pt.opensoft.swing.listeners.SelectTextOnFocusField;

public class JPercentTextField
extends JTextField {
    private static final long serialVersionUID = 1067123962617983348L;
    DocumentController controller;

    public JPercentTextField() {
        this.controller = new RightAlignMaskDocumentController("### %", " %", " %", this, 3, false);
        ((RightAlignMaskDocumentController)this.controller).setMaxNumber(100);
        ((AbstractDocument)this.getDocument()).setDocumentFilter(this.controller.getDocumentFilter());
        this.setNavigationFilter(this.controller.getNavigationFilter());
        this.setColumns(6);
        this.setHorizontalAlignment(4);
        this.setMinimumSize(this.getPreferredSize());
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public JPercentTextField(int precision) {
        StringBuffer mask = new StringBuffer("###");
        StringBuffer placeHolders = new StringBuffer("");
        long maxNumber = (long)Math.pow(10.0, 2 + precision);
        if (precision > 0) {
            int length = precision;
            mask.append(",");
            placeHolders.append("0,");
            do {
                mask.append("#");
                placeHolders.append("0");
            } while (--length > 0);
        }
        mask.append(" %");
        placeHolders.append(" %");
        this.controller = new RightAlignMaskDocumentController(mask.toString(), placeHolders.toString(), " %", this, 3 + precision, false);
        ((RightAlignMaskDocumentController)this.controller).setMaxNumber(maxNumber);
        ((AbstractDocument)this.getDocument()).setDocumentFilter(this.controller.getDocumentFilter());
        this.setNavigationFilter(this.controller.getNavigationFilter());
        this.setColumns(mask.length() - 1);
        this.setHorizontalAlignment(4);
        this.setMinimumSize(this.getPreferredSize());
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    public JPercentTextField(int precision, int maxNumber) {
        StringBuffer mask = new StringBuffer("###");
        StringBuffer placeHolders = new StringBuffer("");
        if (precision > 0) {
            int length = precision;
            mask.append(",");
            placeHolders.append("0,");
            do {
                mask.append("#");
                placeHolders.append("0");
            } while (--length > 0);
        }
        mask.append(" %");
        placeHolders.append(" %");
        this.controller = new RightAlignMaskDocumentController(mask.toString(), placeHolders.toString(), " %", this, 3 + precision, false);
        if (maxNumber != -1) {
            ((RightAlignMaskDocumentController)this.controller).setMaxNumber(maxNumber);
        }
        ((AbstractDocument)this.getDocument()).setDocumentFilter(this.controller.getDocumentFilter());
        this.setNavigationFilter(this.controller.getNavigationFilter());
        this.setColumns(mask.length() - 1);
        this.setHorizontalAlignment(4);
        this.setMinimumSize(this.getPreferredSize());
        if (GUIParameters.selectTextOnFocusGained()) {
            this.addFocusListener(SelectTextOnFocusField.getListener());
        }
    }

    @Override
    public String getText() {
        return super.getText().replaceAll("[%, \" \"]", "");
    }

    public String getValue() {
        return this.controller.getPureContent();
    }

    public void setValue(String s) {
        this.controller.setPureContent(s);
    }

    @Override
    public void copy() {
        JTextField jTextField = new JTextField(this.controller.getPureContent());
        jTextField.selectAll();
        jTextField.copy();
    }

    public void setAutomaticFocusTransferEnabled(boolean isAutomaticFocusTransferEnabled) {
        this.controller.setAutomaticFocusTransferEnabled(isAutomaticFocusTransferEnabled);
    }
}

