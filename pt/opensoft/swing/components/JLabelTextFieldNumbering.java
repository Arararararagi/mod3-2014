/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.components;

import java.awt.Color;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class JLabelTextFieldNumbering
extends JLabel {
    private static final long serialVersionUID = 7187719620937046705L;

    public JLabelTextFieldNumbering() {
        this.initializeLookAndFeelProperties();
    }

    public JLabelTextFieldNumbering(String arg0) {
        super(arg0);
        this.initializeLookAndFeelProperties();
    }

    public JLabelTextFieldNumbering(Icon arg0) {
        super(arg0);
        this.initializeLookAndFeelProperties();
    }

    public JLabelTextFieldNumbering(String arg0, int arg1) {
        super(arg0, arg1);
        this.initializeLookAndFeelProperties();
    }

    public JLabelTextFieldNumbering(Icon arg0, int arg1) {
        super(arg0, arg1);
        this.initializeLookAndFeelProperties();
    }

    public JLabelTextFieldNumbering(String arg0, Icon arg1, int arg2) {
        super(arg0, arg1, arg2);
        this.initializeLookAndFeelProperties();
    }

    private void initializeLookAndFeelProperties() {
        Color backgroundColor = UIManager.getColor("FieldLabel.background");
        Border labelBorder = UIManager.getBorder("FieldLabel.border");
        if (backgroundColor != null) {
            this.setOpaque(true);
            this.setBackground(backgroundColor);
        }
        if (labelBorder != null) {
            this.setBorder(labelBorder);
        }
    }
}

