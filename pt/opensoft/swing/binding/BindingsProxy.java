/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.swing.EventComboBoxModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.toedter.calendar.JDateChooser;
import java.beans.IntrospectionException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.text.JTextComponent;
import pt.opensoft.swing.binding.DateTimeToStringConverter;
import pt.opensoft.swing.binding.DateToStringConverter;
import pt.opensoft.swing.binding.IntegerToStringConverter;
import pt.opensoft.swing.binding.LongToStringConverter;
import pt.opensoft.swing.binding.ValueModelProxy;
import pt.opensoft.swing.binding.map.ICatalogToStringConverter;
import pt.opensoft.swing.components.IBindableUIComponent;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.swing.components.JEditableComboBox;
import pt.opensoft.swing.components.JNumericTextField;
import pt.opensoft.swing.model.catalogs.CatalogValueModel;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.swing.renderer.ComplexCatalogCellRenderer;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.ListMap;

public class BindingsProxy {
    private static HashMap<IBindableUIComponent, PropertyConnector> connectors = new HashMap();

    public static void bind(JComponent component, BeanAdapter<? extends Model> model, String propertyName) {
        BindingsProxy.bind(component, model, propertyName, true);
    }

    public static void bind(JComboBox component, BeanAdapter<? extends Model> model, String propertyName, ListModel aditionalModel) {
        try {
            ValueModel valueModel = ValueModelProxy.getModel(model, propertyName);
            BindingsProxy.bindJComboBox(component, new SelectionInList<Object>(aditionalModel, valueModel));
        }
        catch (IntrospectionException e) {
            throw new RuntimeException("Property was not found on BeanAdapter " + e);
        }
    }

    public static SelectionInList<ICatalogItem> bindAndReturn(JComboBox component, BeanAdapter<? extends Model> model, String propertyName, ListMap aditionalModel) {
        ICatalogToStringConverter adapterModel = new ICatalogToStringConverter(model.getValueModel(propertyName), aditionalModel);
        SelectionInList<ICatalogItem> selectionInList = new SelectionInList<ICatalogItem>(aditionalModel.elements(), (ValueModel)adapterModel);
        component.setRenderer(new ComplexCatalogCellRenderer());
        Bindings.bind(component, selectionInList);
        return selectionInList;
    }

    public static void bind(JComboBox component, BeanAdapter<? extends Model> model, String propertyName, ListMap catalog) {
        ICatalogToStringConverter adapterModel = new ICatalogToStringConverter(model.getValueModel(propertyName), catalog);
        BasicEventList eList = new BasicEventList();
        eList.addAll(catalog.elements());
        component.setModel(new ComboBoxAdapter(new EventComboBoxModel(eList), (ValueModel)adapterModel));
        component.setRenderer(new ComplexCatalogCellRenderer());
    }

    public static void bind(JEditableComboBox component, BeanAdapter<? extends Model> model, String propertyName, ListMap catalog, ICatalogItem invalidItem, boolean isEditable) {
        ICatalogToStringConverter adapterModel = new ICatalogToStringConverter(model.getValueModel(propertyName), catalog);
        SelectionInList selectionInList = new SelectionInList(catalog.elements(), (ValueModel)adapterModel);
        component.setRenderer(new ComplexCatalogCellRenderer());
        component.setEditable(isEditable);
        component.setInvalidEntry(invalidItem);
        Bindings.bind(component, selectionInList);
    }

    public static void bind(JComponent component, BeanAdapter<? extends Model> model, String propertyName, boolean commitOnFocusLost) {
        try {
            ValueModel valueModel = ValueModelProxy.getModel(model, propertyName);
            if (valueModel == null) {
                throw new RuntimeException("Property was not found on BeanAdapter: " + propertyName);
            }
            if (component instanceof JTextComponent) {
                BindingsProxy.bindJTextComponent((JTextComponent)component, valueModel, commitOnFocusLost);
            }
            if (component instanceof JCheckBox) {
                BindingsProxy.bindJCheckBox((JCheckBox)component, valueModel);
            }
            if (component instanceof JRadioButton) {
                BindingsProxy.bindJRadioButton((JRadioButton)component, valueModel);
            }
            if (component instanceof IBindableUIComponent) {
                BindingsProxy.bind((IBindableUIComponent)component, valueModel);
            }
            ValidationComponentUtils.setMessageKey(component, propertyName);
        }
        catch (IntrospectionException e) {
            throw new RuntimeException("Property was not found on BeanAdapter " + e);
        }
    }

    private static void bind(IBindableUIComponent component, ValueModel valueModel) {
        PropertyConnector oldConnector = connectors.get(component);
        if (oldConnector != null) {
            oldConnector.release();
        }
        component.setValue(null);
        PropertyConnector connector = PropertyConnector.connect(valueModel, "value", component, "value");
        connector.updateProperty2();
        connectors.put(component, connector);
    }

    public static void unbind(IBindableUIComponent component) {
        PropertyConnector oldConnector = connectors.get(component);
        if (oldConnector != null) {
            oldConnector.release();
            connectors.remove(component);
        }
        if (component instanceof JDateChooser) {
            ((JDateChooser)component).cleanup();
        }
    }

    public static void bindChoice(JRadioButton component, BeanAdapter<? extends Model> model, String propertyName, Object value) {
        try {
            ValueModel valueModel = ValueModelProxy.getModel(model, propertyName);
            Bindings.bind(component, valueModel, value);
        }
        catch (IntrospectionException e) {
            throw new RuntimeException("Property was not found on BeanAdapter " + e);
        }
    }

    private static void bindJRadioButton(JRadioButton component, ValueModel valueModel) {
        Bindings.bind(component, valueModel, (Object)true);
    }

    private static void bindJCheckBox(JCheckBox component, ValueModel valueModel) {
        Bindings.bind(component, valueModel);
    }

    private static void bindJComboBox(JComboBox comboBoxComponent, SelectionInList<Object> selection) {
        CatalogValueModel testModel = new CatalogValueModel(selection.getSelectionHolder(), selection);
        comboBoxComponent.setRenderer(new ComplexCatalogCellRenderer());
        Bindings.bind(comboBoxComponent, selection);
    }

    private static void bindJTextComponent(JTextComponent textComponent, ValueModel valueModel, boolean commitOnFocusLost) {
        ValueModel tipifiedValueModel = valueModel;
        if (valueModel.getValue() instanceof Integer) {
            tipifiedValueModel = new IntegerToStringConverter(valueModel);
        }
        if (valueModel.getValue() instanceof Long) {
            tipifiedValueModel = new LongToStringConverter(valueModel);
        }
        if (valueModel instanceof DateToStringConverter && textComponent instanceof JDateTextField) {
            ((DateToStringConverter)tipifiedValueModel).setFormat("yyyy/MM/dd");
        }
        if (valueModel.getValue() instanceof DateTime) {
            tipifiedValueModel = new DateTimeToStringConverter(valueModel, "yyyy-MM-dd HH:mm:ss");
        }
        if (textComponent instanceof JNumericTextField) {
            Bindings.bind((JTextField)textComponent, tipifiedValueModel, commitOnFocusLost);
        } else if (textComponent instanceof JTextField) {
            Bindings.bind((JTextField)textComponent, tipifiedValueModel, commitOnFocusLost);
        } else {
            Bindings.bind((JTextArea)textComponent, tipifiedValueModel, commitOnFocusLost);
        }
    }
}

