/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import com.jgoodies.binding.beans.BeanUtils;
import com.jgoodies.binding.beans.PropertyAccessException;
import com.jgoodies.binding.beans.PropertyNotBindableException;
import com.jgoodies.binding.beans.PropertyNotFoundException;
import com.jgoodies.binding.beans.PropertyUnboundException;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;

public final class UnidireccionalPropertyConnector {
    private final Object bean1;
    private final Object bean2;
    private final String property1Name;
    private final String property2Name;
    private final PropertyChangeListener property1ChangeHandler;
    private final PropertyDescriptor property1Descriptor;
    private final PropertyDescriptor property2Descriptor;

    public UnidireccionalPropertyConnector(Object bean1, String property1Name, Object bean2, String property2Name) {
        boolean canWriteProperty2;
        if (bean1 == null) {
            throw new NullPointerException("Bean1 must not be null.");
        }
        if (bean2 == null) {
            throw new NullPointerException("Bean2 must not be null.");
        }
        if (property1Name == null) {
            throw new NullPointerException("PropertyName1 must not be null.");
        }
        if (property2Name == null) {
            throw new NullPointerException("PropertyName2 must not be null.");
        }
        if (bean1 == bean2 && property1Name.equals(property2Name)) {
            throw new IllegalArgumentException("Cannot connect a bean property to itself on the same bean.");
        }
        this.bean1 = bean1;
        this.bean2 = bean2;
        this.property1Name = property1Name;
        this.property2Name = property2Name;
        this.property1Descriptor = this.getPropertyDescriptor(bean1, property1Name);
        this.property2Descriptor = this.getPropertyDescriptor(bean2, property2Name);
        boolean canWriteProperty1 = this.property1Descriptor.getWriteMethod() != null;
        boolean bl = canWriteProperty2 = this.property2Descriptor.getWriteMethod() != null;
        if (!(canWriteProperty1 || canWriteProperty2)) {
            throw new IllegalArgumentException("Cannot connect two read-only properties.");
        }
        this.property1ChangeHandler = new PropertyChangeHandler(bean1, this.property1Descriptor, bean2, this.property2Descriptor);
        if (canWriteProperty2) {
            this.addPropertyChangeHandler(bean1, this.property1ChangeHandler);
        }
    }

    public static void connect(Object bean1, String property1Name, Object bean2, String property2Name) {
        new UnidireccionalPropertyConnector(bean1, property1Name, bean2, property2Name);
    }

    public Object getBean1() {
        return this.bean1;
    }

    public Object getBean2() {
        return this.bean2;
    }

    public String getProperty1Name() {
        return this.property1Name;
    }

    public String getProperty2Name() {
        return this.property2Name;
    }

    public void updateProperty1() {
        Object property2Value = this.getValue(this.bean2, this.property2Descriptor);
        this.setValueSilently(this.bean1, this.property1Descriptor, property2Value);
    }

    public void updateProperty2() {
        Object property1Value = this.getValue(this.bean1, this.property1Descriptor);
        this.setValueSilently(this.bean2, this.property2Descriptor, property1Value);
    }

    public void release() {
        this.removePropertyChangeHandler(this.bean1, this.property1ChangeHandler);
    }

    private void addPropertyChangeHandler(Object bean, PropertyChangeListener listener) {
        if (bean == null) {
            return;
        }
        Class beanClass = bean.getClass();
        if (!BeanUtils.supportsBoundProperties(beanClass)) {
            throw new PropertyUnboundException("Bound properties unsupported by bean class=" + beanClass + "\nThe PropertyConnector can only connect bound properties; " + "i. e. the Bean class must provide a pair of methods:" + "\npublic void addPropertyChangeListener(PropertyChangeListener x);" + "\npublic void removePropertyChangeListener(PropertyChangeListener x);");
        }
        try {
            Method multicastPCLAdder = BeanUtils.getPCLAdder(beanClass);
            multicastPCLAdder.invoke(bean, listener);
        }
        catch (InvocationTargetException e) {
            throw new PropertyNotBindableException("Due to an InvocationTargetException we failed to add a multicast PropertyChangeListener to bean: " + bean, e);
        }
        catch (IllegalAccessException e) {
            throw new PropertyNotBindableException("Due to an IllegalAccessException we failed to add a multicast PropertyChangeListener to bean: " + bean, e);
        }
    }

    private void removePropertyChangeHandler(Object bean, PropertyChangeListener listener) {
        if (bean == null) {
            return;
        }
        Class beanClass = bean.getClass();
        try {
            Method multicastPCLRemover = BeanUtils.getPCLRemover(beanClass);
            if (multicastPCLRemover == null) {
                throw new PropertyUnboundException("Could not find the method to remove a multicast PropertyChangeListener from bean:" + bean);
            }
            multicastPCLRemover.invoke(bean, listener);
        }
        catch (InvocationTargetException e) {
            throw new PropertyNotBindableException("Due to an InvocationTargetException we failed to remove a multicast PropertyChangeListener from bean: " + bean, e);
        }
        catch (IllegalAccessException e) {
            throw new PropertyNotBindableException("Due to an IllegalAccessException we failed to remove a multicast PropertyChangeListener from bean: " + bean, e);
        }
    }

    private Object getValue(Object bean, PropertyDescriptor propertyDescriptor) {
        Method getter = propertyDescriptor.getReadMethod();
        if (getter == null) {
            throw new UnsupportedOperationException("The adapted property '" + propertyDescriptor.getName() + "' is write-only.");
        }
        try {
            return getter.invoke(bean, null);
        }
        catch (InvocationTargetException e) {
            throw PropertyAccessException.createReadAccessException(bean, propertyDescriptor, e);
        }
        catch (IllegalAccessException e) {
            throw PropertyAccessException.createReadAccessException(bean, propertyDescriptor, e);
        }
    }

    private void setValue(Object bean, PropertyDescriptor propertyDescriptor, Object newValue) {
        Method setter = propertyDescriptor.getWriteMethod();
        if (setter == null) {
            throw new UnsupportedOperationException("Property '" + propertyDescriptor.getName() + "' is read-only.");
        }
        try {
            setter.invoke(bean, newValue);
        }
        catch (InvocationTargetException e) {
            throw PropertyAccessException.createWriteAccessException(bean, newValue, propertyDescriptor, e);
        }
        catch (IllegalAccessException e) {
            throw PropertyAccessException.createWriteAccessException(bean, newValue, propertyDescriptor, e);
        }
        catch (IllegalArgumentException e) {
            throw PropertyAccessException.createWriteAccessException(bean, newValue, propertyDescriptor, e);
        }
    }

    private void setValueSilently(Object bean, PropertyDescriptor propertyDescriptor, Object newValue) {
        this.removePropertyChangeHandler(this.bean1, this.property1ChangeHandler);
        this.setValue(bean, propertyDescriptor, newValue);
        this.addPropertyChangeHandler(this.bean1, this.property1ChangeHandler);
    }

    private PropertyDescriptor getPropertyDescriptor(Object bean, String propertyName) {
        try {
            return BeanUtils.getPropertyDescriptor(bean.getClass(), propertyName);
        }
        catch (IntrospectionException e) {
            throw new PropertyNotFoundException(propertyName, bean, (Throwable)e);
        }
    }

    private class PropertyChangeHandler
    implements PropertyChangeListener {
        private final Object sourceBean;
        private final PropertyDescriptor sourcePropertyDescriptor;
        private final Object targetBean;
        private final PropertyDescriptor targetPropertyDescriptor;

        private PropertyChangeHandler(Object sourceBean, PropertyDescriptor sourcePropertyDescriptor, Object targetBean, PropertyDescriptor targetPropertyDescriptor) {
            this.sourceBean = sourceBean;
            this.sourcePropertyDescriptor = sourcePropertyDescriptor;
            this.targetBean = targetBean;
            this.targetPropertyDescriptor = targetPropertyDescriptor;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String sourcePropertyName = this.sourcePropertyDescriptor.getName();
            Object newValue = evt.getNewValue();
            if (newValue == null) {
                newValue = UnidireccionalPropertyConnector.this.getValue(this.sourceBean, this.sourcePropertyDescriptor);
            }
            if (sourcePropertyName.equals(evt.getPropertyName())) {
                UnidireccionalPropertyConnector.this.setValueSilently(this.targetBean, this.targetPropertyDescriptor, newValue);
            } else if (evt.getPropertyName() == null) {
                UnidireccionalPropertyConnector.this.setValueSilently(this.targetBean, this.targetPropertyDescriptor, newValue);
            }
        }
    }

}

