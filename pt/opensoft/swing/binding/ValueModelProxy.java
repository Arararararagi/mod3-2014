/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.value.ValueModel;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import pt.opensoft.swing.binding.DateTimeToStringConverter;
import pt.opensoft.swing.binding.DateToStringConverter;
import pt.opensoft.swing.binding.IntegerToStringConverter;
import pt.opensoft.swing.binding.LongToStringConverter;
import pt.opensoft.util.Date;
import pt.opensoft.util.DateTime;

public class ValueModelProxy {
    public static ValueModel getModel(BeanAdapter<? extends Model> beanModel, String propertyName) throws IntrospectionException {
        Class beanClass = beanModel.getBean().getClass();
        BeanAdapter.SimplePropertyAdapter originalModel = beanModel.getValueModel(propertyName);
        Class clazz = null;
        BeanInfo info = Introspector.getBeanInfo(beanClass);
        for (int i = 0; i < info.getPropertyDescriptors().length; ++i) {
            PropertyDescriptor descriptor = info.getPropertyDescriptors()[i];
            if (descriptor.getName().equals(propertyName) && (clazz = descriptor.getPropertyType()) != Object.class) break;
        }
        return ValueModelProxy.getCorrespondingValueModel(clazz, originalModel);
    }

    protected static ValueModel getCorrespondingValueModel(Class clazz, ValueModel model) {
        if (clazz == null) {
            return null;
        }
        if (clazz == String.class) {
            return model;
        }
        if (clazz == Long.class) {
            return new LongToStringConverter(model);
        }
        if (clazz == Integer.class) {
            return new IntegerToStringConverter(model);
        }
        if (clazz == DateTime.class) {
            return new DateTimeToStringConverter(model, "yyyy-MM-dd HH:mm:ss");
        }
        if (clazz == Date.class) {
            return new DateToStringConverter(model, "yyyy-MM-dd");
        }
        return model;
    }
}

