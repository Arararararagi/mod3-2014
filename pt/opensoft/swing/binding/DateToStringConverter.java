/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import com.jgoodies.binding.value.AbstractConverter;
import com.jgoodies.binding.value.ValueModel;
import pt.opensoft.logging.Logger;
import pt.opensoft.util.Date;
import pt.opensoft.util.InvalidDate;

public class DateToStringConverter
extends AbstractConverter {
    private static final long serialVersionUID = -1324593675422251224L;
    private String _format;

    public DateToStringConverter(ValueModel dateSubject, String format) {
        super(dateSubject);
        this._format = format;
    }

    @Override
    public Object convertFromSubject(Object subjectValue) {
        Date dateTime = (Date)subjectValue;
        if (dateTime != null) {
            return dateTime.format(this._format);
        }
        return "";
    }

    @Override
    public void setValue(Object newValue) {
        if (newValue == null) {
            this.subject.setValue(null);
            return;
        }
        if (!(newValue instanceof String)) {
            throw new ClassCastException("The new value must be a string.");
        }
        String newString = (String)newValue;
        try {
            this.subject.setValue(new Date(newString, this._format));
        }
        catch (InvalidDate e) {
            Logger.getDefault().info("Tried to parse an invalid date: " + e.getMessage());
            this.subject.setValue(null);
        }
    }

    public void setFormat(String dateFormat) {
        this._format = dateFormat;
    }
}

