/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import com.jgoodies.binding.value.AbstractConverter;
import com.jgoodies.binding.value.ValueModel;
import pt.opensoft.util.NumberUtil;

public class LongToStringConverter
extends AbstractConverter {
    private static final long serialVersionUID = 1;

    public LongToStringConverter(ValueModel longSubject) {
        super(longSubject);
    }

    @Override
    public Object convertFromSubject(Object subjectValue) {
        Long longVal = (Long)subjectValue;
        if (longVal != null) {
            return longVal.toString();
        }
        return "";
    }

    @Override
    public void setValue(Object newValue) {
        if (newValue == null) {
            this.subject.setValue(null);
        }
        if (!(newValue instanceof String)) {
            throw new ClassCastException("The new value must be a string.");
        }
        try {
            this.subject.setValue(new Long((String)newValue));
        }
        catch (Exception e) {
            try {
                String newString = ((String)newValue).replaceAll("\\.", "");
                newString = newString.replace((CharSequence)" ", (CharSequence)"");
                long testIfItIsADecimal = Long.parseLong(NumberUtil.fromXSDNumericType(newString, 2, ','));
                this.subject.setValue(testIfItIsADecimal);
            }
            catch (Exception e2) {
                this.subject.setValue(null);
            }
        }
    }
}

