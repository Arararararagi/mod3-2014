/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import com.jgoodies.binding.value.AbstractConverter;
import com.jgoodies.binding.value.ValueModel;

public class IntegerToStringConverter
extends AbstractConverter {
    private static final long serialVersionUID = 1;

    public IntegerToStringConverter(ValueModel integerModel) {
        super(integerModel);
    }

    @Override
    public Object convertFromSubject(Object subjectValue) {
        Integer intVal = (Integer)subjectValue;
        if (intVal != null) {
            return intVal.toString();
        }
        return "";
    }

    @Override
    public void setValue(Object newValue) {
        if (newValue == null) {
            this.subject.setValue(null);
        }
        if (!(newValue instanceof String)) {
            throw new ClassCastException("The new value must be a string.");
        }
        String newString = (String)newValue;
        try {
            this.subject.setValue(new Integer(newString));
        }
        catch (Exception e) {
            this.subject.setValue(null);
        }
    }
}

