/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding.map;

import com.jgoodies.binding.value.AbstractConverter;
import com.jgoodies.binding.value.ValueModel;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.StringUtil;

public class ICatalogToStringConverter
extends AbstractConverter {
    private static final long serialVersionUID = 1;
    private ListMap catalog;

    public ICatalogToStringConverter(ValueModel subject, ListMap catalog) {
        super(subject);
        this.catalog = catalog;
    }

    public ICatalogToStringConverter(ValueModel subject) {
        super(subject);
    }

    @Override
    public Object convertFromSubject(Object subjectValue) {
        if (subjectValue == null) {
            return null;
        }
        if (this.catalog != null) {
            return this.catalog.get(subjectValue);
        }
        return null;
    }

    @Override
    public void setValue(Object newValue) {
        if (newValue != null) {
            Object catalogValue = ((ICatalogItem)newValue).getValue();
            if (!StringUtil.isEmpty(String.valueOf(catalogValue), true)) {
                this.subject.setValue(catalogValue);
            } else {
                this.subject.setValue(null);
            }
        }
    }
}

