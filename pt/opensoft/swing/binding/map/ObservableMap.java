/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding.map;

import java.util.Map;
import pt.opensoft.swing.model.MapModel;

public interface ObservableMap<K, V>
extends Map<K, V>,
MapModel {
}

