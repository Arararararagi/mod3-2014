/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.list.ObservableList;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class DynamicArrayListModel<E>
extends ArrayList<E>
implements ObservableList<E>,
PropertyChangeListener {
    private static final long serialVersionUID = 2971460712775185375L;
    protected EventListenerList listenerList = new EventListenerList();

    public DynamicArrayListModel() {
        this(10);
    }

    public DynamicArrayListModel(int initialCapacity) {
        super(initialCapacity);
    }

    public DynamicArrayListModel(Collection c) {
        super(c);
    }

    @Override
    public void add(int index, E element) {
        super.add(index, element);
        this.fireIntervalAdded(index, index);
        if (element instanceof Model) {
            ((Model)element).addPropertyChangeListener(this);
        }
    }

    @Override
    public boolean add(E o) {
        int newIndex = this.size();
        super.add(o);
        this.fireIntervalAdded(newIndex, newIndex);
        if (o instanceof Model) {
            ((Model)o).addPropertyChangeListener(this);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        boolean changed = super.addAll(index, c);
        if (changed) {
            int lastIndex = index + c.size() - 1;
            this.fireIntervalAdded(index, lastIndex);
        }
        return changed;
    }

    @Override
    public boolean addAll(Collection c) {
        int firstIndex = this.size();
        boolean changed = super.addAll(c);
        if (changed) {
            int lastIndex = firstIndex + c.size() - 1;
            this.fireIntervalAdded(firstIndex, lastIndex);
        }
        return changed;
    }

    @Override
    public void clear() {
        if (this.isEmpty()) {
            return;
        }
        int oldLastIndex = this.size() - 1;
        super.clear();
        this.fireIntervalRemoved(0, oldLastIndex);
    }

    @Override
    public E remove(int index) {
        Object removedElement = super.remove(index);
        this.fireIntervalRemoved(index, index);
        if (removedElement instanceof Model) {
            ((Model)removedElement).removePropertyChangeListener(this);
        }
        return removedElement;
    }

    @Override
    public boolean remove(Object o) {
        boolean contained;
        int index = this.indexOf(o);
        boolean bl = contained = index != -1;
        if (contained) {
            this.remove(index);
        }
        if (o instanceof Model) {
            ((Model)o).removePropertyChangeListener(this);
        }
        return contained;
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
        this.fireIntervalRemoved(fromIndex, toIndex - 1);
    }

    @Override
    public E set(int index, E element) {
        E previousElement = super.set(index, element);
        this.fireContentsChanged(index, index);
        return previousElement;
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        this.listenerList.add(ListDataListener.class, l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        this.listenerList.remove(ListDataListener.class, l);
    }

    public Object getElementAt(int index) {
        return this.get(index);
    }

    @Override
    public int getSize() {
        return this.size();
    }

    public void fireContentsChanged(int index) {
        this.fireContentsChanged(index, index);
    }

    public ListDataListener[] getListDataListeners() {
        return (ListDataListener[])this.listenerList.getListeners((Class)ListDataListener.class);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.fireContentsChanged(this.indexOf(evt.getSource()));
    }

    private void fireContentsChanged(int index0, int index1) {
        Object[] listeners = this.listenerList.getListenerList();
        ListDataEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 0, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).contentsChanged(e);
        }
    }

    private void fireIntervalAdded(int index0, int index1) {
        Object[] listeners = this.listenerList.getListenerList();
        ListDataEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 1, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).intervalAdded(e);
        }
    }

    private void fireIntervalRemoved(int index0, int index1) {
        Object[] listeners = this.listenerList.getListenerList();
        ListDataEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 2, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).intervalRemoved(e);
        }
    }
}

