/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.binding;

import com.jgoodies.binding.beans.Model;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.event.EventListenerList;
import pt.opensoft.swing.binding.map.ObservableMap;
import pt.opensoft.swing.event.MapDataEvent;
import pt.opensoft.swing.event.MapDataListener;

public class DeclaracaoMapModel<K, V>
extends LinkedHashMap<K, V>
implements ObservableMap<K, V>,
PropertyChangeListener {
    private static final long serialVersionUID = 9023332504230185941L;
    protected EventListenerList listenerList = new EventListenerList();

    public DeclaracaoMapModel() {
        this(10);
    }

    public DeclaracaoMapModel(int initialCapacity) {
        super(initialCapacity);
    }

    public DeclaracaoMapModel(Map<K, V> m) {
        super(m);
    }

    @Override
    public void clear() {
        this.clear(null);
    }

    public void clear(Object caller) {
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != MapDataListener.class) continue;
            ((MapDataListener)listeners[i + 1]).startedClear();
        }
        ArrayList keys = new ArrayList(this.keySet());
        for (Object key : keys) {
            this.remove(caller, key);
        }
        for (int i2 = listeners.length - 2; i2 >= 0; i2-=2) {
            if (listeners[i2] != MapDataListener.class) continue;
            ((MapDataListener)listeners[i2 + 1]).finishedClear();
        }
        super.clear();
    }

    @Override
    public V put(K key, V value) {
        return this.put(null, key, value);
    }

    public V put(Object caller, K key, V value) {
        V previous = super.put(key, value);
        if (value instanceof Model) {
            ((Model)value).addPropertyChangeListener(this);
        }
        if (previous != null) {
            this.fireContentChanged(caller, key, previous);
        } else {
            this.fireObjectPut(caller, key, value);
        }
        return previous;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        this.putAll(null, m);
    }

    public void putAll(Object caller, Map<? extends K, ? extends V> m) {
        for (K key : m.keySet()) {
            Object old = this.get(key);
            V newObj = m.get(key);
            if (old != null) {
                super.put(key, newObj);
                if (newObj instanceof Model) {
                    ((Model)newObj).addPropertyChangeListener(this);
                }
                this.fireContentChanged(caller, key, old);
                continue;
            }
            super.put(key, newObj);
            if (newObj instanceof Model) {
                ((Model)newObj).addPropertyChangeListener(this);
            }
            this.fireObjectPut(caller, key, newObj);
        }
    }

    @Override
    public V remove(Object key) {
        return this.remove(this, key);
    }

    public V remove(Object caller, Object key) {
        Object previous = super.remove(key);
        this.fireObjectRemoved(caller, key, previous);
        if (previous instanceof Model) {
            Model model = (Model)previous;
            PropertyChangeListener[] propertyChangeListeners = model.getPropertyChangeListeners();
            for (int i = 0; i < propertyChangeListeners.length; ++i) {
                model.removePropertyChangeListener(propertyChangeListeners[i]);
            }
        }
        return previous;
    }

    @Override
    public void addMapDataListener(MapDataListener mapDataListener) {
        for (MapDataListener listener : (MapDataListener[])this.listenerList.getListeners((Class)MapDataListener.class)) {
            if (!listener.equals(mapDataListener)) continue;
            return;
        }
        this.listenerList.add(MapDataListener.class, mapDataListener);
    }

    @Override
    public void removeMapDataListener(MapDataListener mapDataListener) {
        this.listenerList.remove(MapDataListener.class, mapDataListener);
    }

    public void removeAllMapDataListener(Class<?> mapDataListenerClass) {
        Object[] allListeners;
        for (Object object : allListeners = this.listenerList.getListenerList()) {
            if (!mapDataListenerClass.isAssignableFrom(object.getClass())) continue;
            this.listenerList.remove(MapDataListener.class, (MapDataListener)object);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        ArrayList entries = new ArrayList(this.entrySet());
        Object value = evt.getSource();
        for (Map.Entry entry : entries) {
            if (!entry.getValue().equals(value)) continue;
            this.fireContentChanged(null, entry.getKey(), entry.getValue());
            break;
        }
    }

    private void fireContentChanged(Object firestarter, Object key, Object value) {
        if (this.listenerList.getListenerCount() > 0) {
            MapDataEvent objectChangedEvent = new MapDataEvent(firestarter != null ? firestarter : this, 0, key, value);
            Object[] listeners = this.listenerList.getListenerList();
            for (int i = listeners.length - 2; i >= 0; i-=2) {
                if (listeners[i] != MapDataListener.class) continue;
                ((MapDataListener)listeners[i + 1]).contentChanged(objectChangedEvent);
            }
        }
    }

    private void fireObjectPut(Object firestarter, Object key, Object value) {
        if (this.listenerList.getListenerCount() > 0) {
            MapDataEvent objectPutEvent = new MapDataEvent(firestarter != null ? firestarter : this, 1, key, value);
            Object[] listeners = this.listenerList.getListenerList();
            for (int i = listeners.length - 2; i >= 0; i-=2) {
                if (listeners[i] != MapDataListener.class) continue;
                ((MapDataListener)listeners[i + 1]).objectPut(objectPutEvent);
            }
        }
    }

    private void fireObjectRemoved(Object firestarter, Object key, Object value) {
        if (this.listenerList.getListenerCount() > 0) {
            MapDataEvent objectRemovedEvent = new MapDataEvent(firestarter != null ? firestarter : this, 2, key, value);
            Object[] listeners = this.listenerList.getListenerList();
            for (int i = listeners.length - 2; i >= 0; i-=2) {
                if (listeners[i] != MapDataListener.class) continue;
                ((MapDataListener)listeners[i + 1]).objectRemoved(objectRemovedEvent);
            }
        }
    }
}

