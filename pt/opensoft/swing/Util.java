/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JToolTip;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;
import pt.opensoft.util.StringUtil;

public class Util {
    private static Map cacheHTML = new HashMap();

    public static void loadAndShowHtml(JEditorPane pane, String pageLocation, String anchor) throws IOException {
        Util.loadAndShowHtml(pane, pageLocation, anchor, false);
    }

    public static void loadAndShowHtml(JEditorPane pane, String pageLocation, final String anchor, boolean withCache) throws IOException {
        String content = Util.getHtmlText(pane, pageLocation, withCache);
        pane.setText(content);
        pane.requestFocus();
        final JEditorPane paneFinal = pane;
        if (anchor != null) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    paneFinal.scrollToReference(anchor);
                }
            });
        }
    }

    public static String getHtmlText(JEditorPane pane, String pageLocation) throws IOException {
        return Util.getHtmlText(pane, pageLocation, false);
    }

    public static String getHtmlText(JEditorPane pane, String pageLocation, boolean withCache) throws IOException {
        String htmlText;
        URL url = Util.class.getResource(pageLocation);
        if (withCache && cacheHTML.containsKey(pageLocation)) {
            htmlText = (String)cacheHTML.get(pageLocation);
        } else {
            StringBuffer content;
            InputStream pageInput = Util.class.getResourceAsStream(pageLocation);
            if (pageInput == null) {
                throw new IOException("Page not found in classpath: " + pageLocation);
            }
            content = new StringBuffer();
            try {
                InputStreamReader reader = new InputStreamReader(pageInput, "ISO-8859-1");
                try {
                    char[] buffer = new char[128];
                    int read = -1;
                    while ((read = reader.read(buffer, 0, buffer.length)) != -1) {
                        content.append(buffer, 0, read);
                    }
                }
                finally {
                    reader.close();
                }
            }
            finally {
                pageInput.close();
            }
            htmlText = content.toString();
            if (withCache) {
                cacheHTML.put(pageLocation, htmlText);
            }
        }
        ((HTMLDocument)pane.getDocument()).setBase(url);
        return htmlText;
    }

    public static String getHtmlText(String pageLocation) throws IOException {
        StringBuffer content;
        InputStream pageInput = Util.class.getResourceAsStream(pageLocation);
        if (pageInput == null) {
            throw new IOException("Page not found in classpath: " + pageLocation);
        }
        content = new StringBuffer();
        try {
            InputStreamReader reader = new InputStreamReader(pageInput, "ISO-8859-1");
            try {
                char[] buffer = new char[128];
                int read = -1;
                while ((read = reader.read(buffer, 0, buffer.length)) != -1) {
                    content.append(buffer, 0, read);
                }
            }
            finally {
                reader.close();
            }
        }
        finally {
            pageInput.close();
        }
        String htmlText = content.toString();
        return htmlText;
    }

    public static Rectangle getBoundsRelativeToContainer(JComponent component, JPanel container) {
        Rectangle bounds = component.getBounds(null);
        Container current = component;
        while (current.getParent() != null && current.getParent() != container) {
            int x = bounds.getLocation().x;
            int y = bounds.getLocation().y;
            Point parentLocation = current.getParent().getBounds().getLocation();
            bounds.setLocation(x + parentLocation.x, y + parentLocation.y);
            current = current.getParent();
        }
        bounds.setSize(current.getSize());
        return bounds;
    }

    public static void printUIDefaults() {
        TreeMap ht = new TreeMap(UIManager.getLookAndFeel().getDefaults());
        for (Object key : ht.keySet()) {
        }
        HashMap ht2 = new HashMap(UIManager.getDefaults());
        for (Object key2 : ht2.keySet()) {
        }
    }

    public static DisplayMode getScreenDisplayMode() {
        return Util.getScreenDisplayMode(0);
    }

    public static DisplayMode getScreenDisplayMode(int i) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        GraphicsDevice gd = gs[i];
        return gd.getDisplayMode();
    }

    public static String breakLines(String str, String sep, String newLine, int width) {
        StringBuffer res = new StringBuffer();
        int size = str.length();
        int beginIndex = 0;
        int endIndex = 0;
        while (size - endIndex > width) {
            int lIndex = str.lastIndexOf(sep, beginIndex + width);
            int rIndex = str.indexOf(sep, beginIndex + width);
            int n = lIndex == -1 ? rIndex : (rIndex == -1 ? lIndex : (endIndex = width - lIndex < rIndex - width ? lIndex : rIndex));
            if (endIndex != -1 && endIndex > beginIndex) {
                res.append(str.substring(beginIndex, endIndex)).append(newLine);
                beginIndex = endIndex;
                continue;
            }
            size = endIndex = beginIndex;
        }
        return res.append(str.substring(endIndex)).toString();
    }

    public static String formatTooltipText(String tip) {
        int screenWidth;
        try {
            screenWidth = Util.getScreenDisplayMode().getWidth();
        }
        catch (InternalError ie) {
            screenWidth = 800;
        }
        JToolTip jtip = new JToolTip();
        int mWidth = jtip.getFontMetrics(jtip.getFont()).charWidth('m');
        int width = screenWidth / mWidth;
        return "<HTML>" + Util.breakLines(tip, " ", "<BR>", width) + "<HTML>";
    }

    public static int getPreferredRowHeight(JTable table, int rowIndex, int margin) {
        int height = table.getRowHeight();
        for (int c = 0; c < table.getColumnCount(); ++c) {
            TableCellRenderer renderer = table.getCellRenderer(rowIndex, c);
            Component comp = table.prepareRenderer(renderer, rowIndex, c);
            int h = comp.getPreferredSize().height + 2 * margin;
            height = Math.max(height, h);
        }
        return height;
    }

    public static JComponent findComponentByName(JComponent root, String name) {
        if (root == null || StringUtil.isEmpty(name)) {
            return null;
        }
        if (root.getName() != null && root.getName().equals(name)) {
            return root;
        }
        Component[] components = root.getComponents();
        for (int i = 0; i < components.length; ++i) {
            JComponent jComponent;
            JComponent componentByName;
            if (!(components[i] instanceof JComponent) || (componentByName = Util.findComponentByName(jComponent = (JComponent)components[i], name)) == null) continue;
            return componentByName;
        }
        return null;
    }

}

