/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.Image;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import pt.opensoft.swing.BlankIcon;

public class IconFactory {
    private static Icon BLANK_ICON = new BlankIcon();
    private static String DEFAULT_ICONS_PATH = "/icons/";
    private static String DEFAULT_ICONS_SMALL_PATH = "/icons-small/";

    private IconFactory() {
    }

    public static Image getImage(String iconName) {
        return IconFactory.getImage(DEFAULT_ICONS_PATH, iconName);
    }

    public static Image getImage(String path, String iconName) {
        return new ImageIcon(IconFactory.class.getResource(path + iconName)).getImage();
    }

    public static Icon getIconSmall(String iconName) {
        return IconFactory.getIconSmall(DEFAULT_ICONS_SMALL_PATH, iconName);
    }

    public static Icon getIconSmall(String path, String iconName) {
        return new ImageIcon(IconFactory.class.getResource(path + iconName));
    }

    public static Icon getIconBig(String iconName) {
        return IconFactory.getIconBig(DEFAULT_ICONS_PATH, iconName);
    }

    public static Icon getIconBig(String path, String iconName) {
        return new ImageIcon(IconFactory.class.getResource(path + iconName));
    }

    public static Icon getBlankIcon() {
        return BLANK_ICON;
    }
}

