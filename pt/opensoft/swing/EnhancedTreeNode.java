/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import javax.swing.tree.DefaultMutableTreeNode;

public class EnhancedTreeNode
extends DefaultMutableTreeNode {
    protected String _name;
    protected String _tooltip;

    public EnhancedTreeNode(String name) {
        this._name = name;
        this._tooltip = name;
    }

    public EnhancedTreeNode(String name, String tooltip) {
        this._name = name;
        this._tooltip = tooltip;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getTooltip() {
        return this._tooltip;
    }

    public void setTooltip(String tooltip) {
        this._tooltip = tooltip;
    }

    @Override
    public String toString() {
        return this._name;
    }
}

