/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.dependencies;

import java.awt.Color;
import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;
import pt.opensoft.swing.dependencies.FastGradientPaintContext;

public class FastGradientPaint
implements Paint {
    int startColor;
    int endColor;
    boolean isVertical;

    public FastGradientPaint(Color sc, Color ec, boolean isV) {
        this.startColor = sc.getRGB();
        this.endColor = ec.getRGB();
        this.isVertical = isV;
    }

    @Override
    public synchronized PaintContext createContext(ColorModel cm, Rectangle r, Rectangle2D r2d, AffineTransform xform, RenderingHints hints) {
        return new FastGradientPaintContext(cm, r, this.startColor, this.endColor, this.isVertical);
    }

    @Override
    public int getTransparency() {
        return ((this.startColor & this.endColor) >> 24 & 255) == 255 ? 1 : 3;
    }
}

