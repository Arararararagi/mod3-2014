/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.dependencies;

import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.WeakHashMap;

class FastGradientPaintContext
implements PaintContext {
    private static WeakHashMap gradientCache = new WeakHashMap();
    private static LinkedList recentInfos = new LinkedList();
    GradientInfo info;
    int parallelDevicePos;
    Gradient gradient;

    FastGradientPaintContext(ColorModel cm, Rectangle r, int sc, int ec, boolean ver) {
        Object o;
        this.info = new GradientInfo();
        this.info.model = ((sc & ec) >> 24 & 255) != 255 ? ColorModel.getRGBdefault() : cm;
        this.info.startColor = sc;
        this.info.endColor = ec;
        this.info.isVertical = ver;
        if (this.info.isVertical) {
            this.parallelDevicePos = r.y;
            this.info.parallelLength = r.height;
        } else {
            this.parallelDevicePos = r.x;
            this.info.parallelLength = r.width;
        }
        recentInfos.remove(this.info);
        recentInfos.add(0, this.info);
        if (recentInfos.size() > 16) {
            recentInfos.removeLast();
        }
        if ((o = gradientCache.get(this.info)) != null) {
            o = ((WeakReference)o).get();
        }
        if (o != null) {
            this.gradient = (Gradient)o;
        } else {
            this.gradient = new Gradient(this.info);
            gradientCache.put(this.info, new WeakReference<Gradient>(this.gradient));
        }
    }

    @Override
    public void dispose() {
        this.gradient.dispose();
    }

    @Override
    public ColorModel getColorModel() {
        return this.info.model;
    }

    @Override
    public synchronized Raster getRaster(int x, int y, int w, int h) {
        if (this.info.isVertical) {
            return this.gradient.getRaster(y - this.parallelDevicePos, w, h);
        }
        return this.gradient.getRaster(x - this.parallelDevicePos, h, w);
    }

    private class Gradient {
        GradientInfo info;
        int perpendicularLength;
        WritableRaster raster;
        HashMap childRasterCache;

        Gradient(GradientInfo i) {
            this.perpendicularLength = 0;
            this.info = i;
        }

        private Raster getRaster(int parallelPos, int perpendicularLength, int parallelLength) {
            Integer key;
            Object o;
            if (this.raster == null || this.perpendicularLength < perpendicularLength) {
                this.createRaster(perpendicularLength);
            }
            if ((o = this.childRasterCache.get(key = new Integer(parallelPos))) != null) {
                return (Raster)o;
            }
            if (parallelPos < this.raster.getMinY()) {
                parallelPos = this.raster.getMinY();
            }
            Raster r = this.info.isVertical ? this.raster.createChild(0, parallelPos, this.perpendicularLength, this.info.parallelLength - parallelPos, 0, 0, null) : this.raster.createChild(parallelPos, 0, this.info.parallelLength - parallelPos, this.perpendicularLength, 0, 0, null);
            this.childRasterCache.put(key, r);
            return r;
        }

        public void dispose() {
        }

        private void createRaster(int perpendicularLength) {
            int gradientHeight;
            int gradientWidth;
            if (this.info.isVertical) {
                gradientHeight = this.info.parallelLength;
                gradientWidth = this.perpendicularLength = perpendicularLength;
            } else {
                gradientWidth = this.info.parallelLength;
                gradientHeight = this.perpendicularLength = perpendicularLength;
            }
            int sa = this.info.startColor >> 24 & 255;
            int sr = this.info.startColor >> 16 & 255;
            int sg = this.info.startColor >> 8 & 255;
            int sb = this.info.startColor & 255;
            int da = (this.info.endColor >> 24 & 255) - sa;
            int dr = (this.info.endColor >> 16 & 255) - sr;
            int dg = (this.info.endColor >> 8 & 255) - sg;
            int db = (this.info.endColor & 255) - sb;
            this.raster = this.info.model.createCompatibleWritableRaster(gradientWidth, gradientHeight);
            Object c = null;
            int pl = this.info.parallelLength;
            for (int i = 0; i < pl; ++i) {
                c = this.info.model.getDataElements(sa + i * da / pl << 24 | sr + i * dr / pl << 16 | sg + i * dg / pl << 8 | sb + i * db / pl, c);
                for (int j = 0; j < perpendicularLength; ++j) {
                    if (this.info.isVertical) {
                        this.raster.setDataElements(j, i, c);
                        continue;
                    }
                    this.raster.setDataElements(i, j, c);
                }
            }
            this.childRasterCache = new HashMap();
        }
    }

    private class GradientInfo {
        ColorModel model;
        int parallelLength;
        int startColor;
        int endColor;
        boolean isVertical;

        private GradientInfo() {
        }

        public boolean equals(Object o) {
            if (!(o instanceof GradientInfo)) {
                return false;
            }
            GradientInfo gi = (GradientInfo)o;
            if (gi.model.equals(this.model) && gi.parallelLength == this.parallelLength && gi.startColor == this.startColor && gi.endColor == this.endColor && gi.isVertical == this.isVertical) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return this.parallelLength;
        }

        public String toString() {
            return "Direction:" + (this.isVertical ? "ver" : "hor") + ", Length: " + Integer.toString(this.parallelLength) + ", Color1: " + Integer.toString(this.startColor, 16) + ", Color2: " + Integer.toString(this.endColor, 16);
        }
    }

}

