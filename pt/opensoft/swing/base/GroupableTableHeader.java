/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base;

import java.util.Enumeration;
import java.util.Vector;
import javax.swing.plaf.TableHeaderUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.opensoft.swing.base.ColumnGroup;
import pt.opensoft.swing.base.GroupableTableHeaderUI;

public class GroupableTableHeader
extends JTableHeader {
    protected Vector columnGroups = null;

    public GroupableTableHeader(TableColumnModel model) {
        super(model);
        this.setUI(new GroupableTableHeaderUI());
        this.setReorderingAllowed(false);
    }

    @Override
    public void setReorderingAllowed(boolean b) {
        this.reorderingAllowed = false;
    }

    public void addColumnGroup(ColumnGroup g) {
        if (this.columnGroups == null) {
            this.columnGroups = new Vector();
        }
        this.columnGroups.addElement(g);
    }

    public Enumeration getColumnGroups(TableColumn col) {
        if (this.columnGroups == null) {
            return null;
        }
        Enumeration enume = this.columnGroups.elements();
        while (enume.hasMoreElements()) {
            ColumnGroup cGroup = (ColumnGroup)enume.nextElement();
            Vector v_ret = cGroup.getColumnGroups(col, new Vector());
            if (v_ret == null) continue;
            return v_ret.elements();
        }
        return null;
    }

    public void setColumnMargin() {
        if (this.columnGroups == null) {
            return;
        }
        int columnMargin = this.getColumnModel().getColumnMargin();
        Enumeration enume = this.columnGroups.elements();
        while (enume.hasMoreElements()) {
            ColumnGroup cGroup = (ColumnGroup)enume.nextElement();
            cGroup.setColumnMargin(columnMargin);
        }
    }
}

