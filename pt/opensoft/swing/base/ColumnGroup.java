/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class ColumnGroup {
    protected TableCellRenderer renderer;
    protected Vector v;
    protected String text;
    protected int margin = 0;

    public ColumnGroup(String text) {
        this(null, text);
    }

    public ColumnGroup(TableCellRenderer renderer, String text) {
        this.renderer = renderer == null ? new DefaultTableCellRenderer(){

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                JTableHeader header = table.getTableHeader();
                if (header != null) {
                    this.setForeground(header.getForeground());
                    this.setBackground(header.getBackground());
                    this.setFont(header.getFont());
                }
                this.setHorizontalAlignment(0);
                this.setText(value == null ? "" : value.toString());
                this.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
                return this;
            }
        } : renderer;
        this.text = text;
        this.v = new Vector();
    }

    public void add(Object obj) {
        if (obj == null) {
            return;
        }
        this.v.addElement(obj);
    }

    public Vector getColumnGroups(TableColumn c, Vector g) {
        g.addElement(this);
        if (this.v.contains(c)) {
            return g;
        }
        Enumeration enume = this.v.elements();
        while (enume.hasMoreElements()) {
            Vector groups;
            Object obj = enume.nextElement();
            if (!(obj instanceof ColumnGroup) || (groups = ((ColumnGroup)obj).getColumnGroups(c, (Vector)g.clone())) == null) continue;
            return groups;
        }
        return null;
    }

    public TableCellRenderer getHeaderRenderer() {
        return this.renderer;
    }

    public void setHeaderRenderer(TableCellRenderer renderer) {
        if (renderer != null) {
            this.renderer = renderer;
        }
    }

    public Object getHeaderValue() {
        return this.text;
    }

    public Dimension getSize(JTable table) {
        Component comp = this.renderer.getTableCellRendererComponent(table, this.getHeaderValue(), false, false, -1, -1);
        int height = comp.getPreferredSize().height;
        int width = 0;
        Enumeration enume = this.v.elements();
        while (enume.hasMoreElements()) {
            Object obj = enume.nextElement();
            if (obj instanceof TableColumn) {
                TableColumn aColumn = (TableColumn)obj;
                width+=aColumn.getWidth();
                width+=this.margin;
                continue;
            }
            width+=((ColumnGroup)obj).getSize((JTable)table).width;
        }
        return new Dimension(width, height);
    }

    public void setColumnMargin(int margin) {
        this.margin = margin;
        Enumeration enume = this.v.elements();
        while (enume.hasMoreElements()) {
            Object obj = enume.nextElement();
            if (!(obj instanceof ColumnGroup)) continue;
            ((ColumnGroup)obj).setColumnMargin(margin);
        }
    }

}

