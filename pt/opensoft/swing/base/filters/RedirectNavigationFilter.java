/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import javax.swing.text.NavigationFilter;
import javax.swing.text.Position;
import pt.opensoft.swing.base.filters.DocumentController;

public class RedirectNavigationFilter
extends NavigationFilter {
    DocumentController controller;

    public RedirectNavigationFilter(DocumentController controller) {
        this.controller = controller;
    }

    @Override
    public void setDot(NavigationFilter.FilterBypass filterBypass, int i, Position.Bias bias) {
        this.controller.setDot(filterBypass, i, bias);
    }

    @Override
    public void moveDot(NavigationFilter.FilterBypass filterBypass, int i, Position.Bias bias) {
        this.controller.moveDot(filterBypass, i, bias);
    }
}

