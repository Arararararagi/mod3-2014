/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import pt.opensoft.swing.base.filters.AutomaticFocusTransferFilter;
import pt.opensoft.swing.base.filters.DocumentController;

public class RedirectDocumentFilter
extends AutomaticFocusTransferFilter {
    DocumentController controller;

    public RedirectDocumentFilter(DocumentController controller) {
        this.controller = controller;
    }

    @Override
    public void insertString(DocumentFilter.FilterBypass filterBypass, int i, String string, AttributeSet attributeSet) throws BadLocationException {
        this.controller.insertString(filterBypass, i, string, attributeSet);
    }

    @Override
    public void replace(DocumentFilter.FilterBypass filterBypass, int i, int i1, String string, AttributeSet attributeSet) throws BadLocationException {
        this.controller.replace(filterBypass, i, i1, string, attributeSet);
        if (this.isAutomaticFocusTransferEnabled && filterBypass.getDocument().getLength() - this.controller.getSize() + 1 >= this.controller.getSize()) {
            this.transferFocusToNextComponent();
        }
    }

    @Override
    public void remove(DocumentFilter.FilterBypass filterBypass, int pos0, int len) throws BadLocationException {
        this.controller.remove(filterBypass, pos0, len);
    }
}

