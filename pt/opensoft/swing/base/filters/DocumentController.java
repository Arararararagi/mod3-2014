/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.NavigationFilter;
import javax.swing.text.Position;

public interface DocumentController {
    public DocumentFilter getDocumentFilter();

    public NavigationFilter getNavigationFilter();

    public String getPureContent();

    public String getPureContent(int var1, int var2);

    public void setPureContent(String var1);

    public void setSize(int var1);

    public int getSize();

    public void insertString(DocumentFilter.FilterBypass var1, int var2, String var3, AttributeSet var4) throws BadLocationException;

    public void replace(DocumentFilter.FilterBypass var1, int var2, int var3, String var4, AttributeSet var5) throws BadLocationException;

    public void remove(DocumentFilter.FilterBypass var1, int var2, int var3) throws BadLocationException;

    public void refreshContent(DocumentFilter.FilterBypass var1);

    public void setDot(NavigationFilter.FilterBypass var1, int var2, Position.Bias var3);

    public void moveDot(NavigationFilter.FilterBypass var1, int var2, Position.Bias var3);

    public void setAutomaticFocusTransferEnabled(boolean var1);
}

