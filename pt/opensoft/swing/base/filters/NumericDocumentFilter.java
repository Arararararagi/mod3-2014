/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class NumericDocumentFilter
extends DocumentFilter {
    DocumentFilter next;

    public NumericDocumentFilter() {
        this(null);
    }

    public NumericDocumentFilter(DocumentFilter next) {
        this.next = next;
    }

    @Override
    public void insertString(DocumentFilter.FilterBypass filterBypass, int i, String string, AttributeSet attributeSet) throws BadLocationException {
        char[] chars = string.toCharArray();
        for (int j = 0; j < chars.length; ++j) {
            if (Character.isDigit(chars[j])) continue;
            return;
        }
        if (this.next == null) {
            filterBypass.insertString(i, string, attributeSet);
        } else {
            this.next.insertString(filterBypass, i, string, attributeSet);
        }
    }

    @Override
    public void replace(DocumentFilter.FilterBypass filterBypass, int i, int i1, String string, AttributeSet attributeSet) throws BadLocationException {
        char[] chars = string.toCharArray();
        for (int j = 0; j < chars.length; ++j) {
            if (Character.isDigit(chars[j])) continue;
            return;
        }
        if (this.next == null) {
            filterBypass.replace(i, i1, string, attributeSet);
        } else {
            this.next.replace(filterBypass, i, i1, string, attributeSet);
        }
    }
}

