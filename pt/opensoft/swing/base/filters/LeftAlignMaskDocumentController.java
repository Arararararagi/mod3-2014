/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.NavigationFilter;
import javax.swing.text.Position;
import pt.opensoft.swing.base.filters.DocumentController;
import pt.opensoft.swing.base.filters.RedirectDocumentFilter;
import pt.opensoft.swing.base.filters.RedirectNavigationFilter;

public class LeftAlignMaskDocumentController
implements DocumentController {
    char[] mask;
    char[] placeholders;
    protected int size;
    protected StringBuffer content = new StringBuffer();
    protected StringBuffer pureContent = new StringBuffer();
    protected JTextComponent component;
    RedirectDocumentFilter documentFilter;
    RedirectNavigationFilter navigationFilter;
    private int previousDot = 0;

    public LeftAlignMaskDocumentController(String mask, String placeholders, JTextComponent component, int size) {
        this.mask = mask.toCharArray();
        this.placeholders = placeholders.toCharArray();
        this.component = component;
        this.size = size;
        this.documentFilter = new RedirectDocumentFilter(this);
        this.navigationFilter = new RedirectNavigationFilter(this);
    }

    @Override
    public DocumentFilter getDocumentFilter() {
        return this.documentFilter;
    }

    @Override
    public NavigationFilter getNavigationFilter() {
        return this.navigationFilter;
    }

    @Override
    public String getPureContent() {
        return this.pureContent.toString();
    }

    @Override
    public String getPureContent(int selectionStart, int selectionEnd) {
        int p0 = this.m2cpos(selectionStart);
        int p1 = this.m2cpos(selectionEnd);
        if (p1 > p0) {
            p1 = p1 < this.pureContent.length() ? p1 : this.pureContent.length();
            return this.pureContent.substring(p0, p1);
        }
        return "";
    }

    @Override
    public void setPureContent(String s) {
        this.component.setText(s);
    }

    @Override
    public void setSize(int s) {
        this.size = s;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    protected int m2cpos(int mpos) {
        int cpos = 0;
        for (int i = 0; i < mpos; ++i) {
            if (this.mask[i] != '#') continue;
            ++cpos;
        }
        return cpos;
    }

    protected int c2mpos(int cpos) {
        int mpos;
        int i = 0;
        for (mpos = 0; i < cpos && mpos < this.mask.length; ++mpos) {
            if (this.mask[mpos] != '#') continue;
            ++i;
        }
        return mpos;
    }

    @Override
    public void insertString(DocumentFilter.FilterBypass filterBypass, int i, String string, AttributeSet attributeSet) throws BadLocationException {
        this.component.setForeground(UIManager.getColor("TextField.color"));
        if (this.pureContent.length() + string.length() > this.size) {
            return;
        }
        char[] strCh = string.toCharArray();
        boolean valid = true;
        for (int j = 0; j < strCh.length; ++j) {
            char c = strCh[j];
            if (Character.isDigit(c)) continue;
            valid = false;
            break;
        }
        if (valid) {
            int mpos = i;
            int cpos = this.m2cpos(mpos);
            if (cpos >= this.pureContent.length()) {
                cpos = this.pureContent.length();
                this.pureContent.append(string);
            } else {
                this.pureContent.insert(cpos, string);
            }
            this.refreshContent(filterBypass);
            this.component.setCaretPosition(this.c2mpos(cpos + 1));
        }
    }

    @Override
    public void replace(DocumentFilter.FilterBypass filterBypass, int i, int i1, String string, AttributeSet attributeSet) throws BadLocationException {
        this.remove(filterBypass, i, i1);
        this.insertString(filterBypass, i, string, attributeSet);
    }

    @Override
    public void remove(DocumentFilter.FilterBypass filterBypass, int pos0, int len) throws BadLocationException {
        int cpos0 = this.m2cpos(pos0);
        int cpos1 = this.m2cpos(pos0 + len);
        if (len > 0 && cpos0 == cpos1) {
            ++cpos1;
        }
        if (cpos0 > 0 && cpos1 > this.pureContent.length()) {
            cpos0 = this.pureContent.length();
        }
        if (cpos1 > 0 && cpos1 > this.pureContent.length()) {
            cpos1 = this.pureContent.length();
        }
        this.pureContent.delete(cpos0, cpos1);
        this.refreshContent(filterBypass);
        this.component.setCaretPosition(this.c2mpos(cpos0));
    }

    @Override
    public void refreshContent(DocumentFilter.FilterBypass filterBypass) {
        char[] pureChars = this.pureContent.toString().toCharArray();
        int contentPos = 0;
        this.content = new StringBuffer();
        for (int maskPos = 0; maskPos < this.mask.length; ++maskPos) {
            if (this.mask[maskPos] == '#') {
                if (contentPos < pureChars.length) {
                    this.content.append(pureChars[contentPos++]);
                    continue;
                }
                this.content.append(this.placeholders[maskPos]);
                continue;
            }
            this.content.append(this.mask[maskPos]);
        }
        try {
            filterBypass.replace(0, filterBypass.getDocument().getLength(), this.content.toString(), null);
        }
        catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setDot(NavigationFilter.FilterBypass filterBypass, int i, Position.Bias bias) {
        if (i == 0 || this.mask[this.mask.length - (this.content.length() - i) - 1] == '#') {
            this.previousDot = i;
            filterBypass.setDot(i, bias);
        } else if (i > 0) {
            if (i <= this.previousDot) {
                this.previousDot = i;
                this.setDot(filterBypass, i - 1, bias);
            } else {
                this.previousDot = i;
                if (i < this.content.length() && this.mask[this.mask.length - (this.content.length() - i)] == '#') {
                    filterBypass.setDot(i + 1, bias);
                } else {
                    this.setDot(filterBypass, i - 1, bias);
                }
            }
        } else {
            filterBypass.setDot(0, bias);
        }
    }

    @Override
    public void moveDot(NavigationFilter.FilterBypass filterBypass, int i, Position.Bias bias) {
        filterBypass.moveDot(i, bias);
    }

    @Override
    public void setAutomaticFocusTransferEnabled(boolean isAutomaticFocusTransferEnabled) {
        this.documentFilter.setAutomaticFocusTransferEnabled(isAutomaticFocusTransferEnabled);
    }
}

