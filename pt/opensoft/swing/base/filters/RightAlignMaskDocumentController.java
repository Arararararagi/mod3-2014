/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.NavigationFilter;
import javax.swing.text.Position;
import pt.opensoft.swing.base.filters.DocumentController;
import pt.opensoft.swing.base.filters.RedirectDocumentFilter;
import pt.opensoft.swing.base.filters.RedirectNavigationFilter;

public class RightAlignMaskDocumentController
implements DocumentController {
    char[] mask;
    char[] placeholders;
    String emptyChars;
    int size;
    StringBuffer content = new StringBuffer();
    StringBuffer pureContent = new StringBuffer();
    JTextComponent component;
    boolean acceptsleadingzeros;
    long maxNumber;
    boolean prefixActive;
    String trigger;
    String prefix = null;
    RedirectDocumentFilter documentFilter;
    RedirectNavigationFilter navigationFilter;
    protected int previousDot = 0;

    public RightAlignMaskDocumentController(String mask, String placeholders, String emptyChars, JTextComponent component, int size, boolean acceptsleadingzeros) {
        this.mask = mask.toCharArray();
        this.placeholders = placeholders.toCharArray();
        this.emptyChars = emptyChars;
        this.component = component;
        this.size = size;
        this.documentFilter = new RedirectDocumentFilter(this);
        this.navigationFilter = new RedirectNavigationFilter(this);
        this.acceptsleadingzeros = acceptsleadingzeros;
    }

    public void setMaxNumber(long max) {
        this.maxNumber = max;
    }

    public void allowPrefix(String trigger, String prefix, boolean active) {
        this.trigger = trigger;
        this.prefix = prefix;
        this.prefixActive = active;
    }

    public boolean hasPrefix() {
        return this.prefix != null;
    }

    public int prefixSize() {
        return this.hasPrefix() ? this.prefix.length() : 0;
    }

    @Override
    public DocumentFilter getDocumentFilter() {
        return this.documentFilter;
    }

    @Override
    public NavigationFilter getNavigationFilter() {
        return this.navigationFilter;
    }

    @Override
    public String getPureContent() {
        return (this.prefix != null && this.prefixActive && this.pureContent.length() > 0 ? this.prefix : "") + this.pureContent.toString();
    }

    @Override
    public String getPureContent(int selectionStart, int selectionEnd) {
        int imp0 = this.content.length() - selectionStart;
        imp0 = imp0 < 0 ? 0 : imp0;
        int imp1 = this.content.length() - selectionEnd;
        imp1 = imp1 < 0 ? 0 : imp1;
        int icp0 = this.im2cpos(imp0);
        int icp1 = this.im2cpos(imp1);
        int p0 = this.pureContent.length() - icp0;
        p0 = p0 < 0 ? 0 : p0;
        int p1 = this.pureContent.length() - icp1;
        int n = p1 = p1 < 0 ? 0 : p1;
        if (p1 > p0 && this.pureContent.length() > 0) {
            p1 = p1 < this.pureContent.length() ? p1 : this.pureContent.length();
            return (this.prefix != null && p0 == 0 && this.prefixActive ? this.prefix : "") + this.pureContent.substring(p0, p1);
        }
        return "";
    }

    @Override
    public void setPureContent(String s) {
        this.component.setText(s);
    }

    @Override
    public void setSize(int s) {
        this.size = s;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    protected int im2cpos(int impos) {
        int icpos = 0;
        int i = this.mask.length - 1;
        while (this.mask.length - i <= impos) {
            if (this.mask[i] == '#') {
                ++icpos;
            }
            --i;
        }
        return icpos;
    }

    @Override
    public void insertString(DocumentFilter.FilterBypass filterBypass, int i, String string, AttributeSet attributeSet) throws BadLocationException {
        if (this.prefix != null) {
            if (this.trigger.equals(string) && this.pureContent.length() > 0) {
                this.prefixActive = !this.prefixActive;
                this.refreshContent(filterBypass);
                this.component.setCaretPosition(i + (i != 0 ? (this.prefixActive ? 1 : -1) : 0));
                return;
            }
            if (this.trigger.equals(string)) {
                this.prefixActive = false;
            } else if (string != null && string.indexOf(this.trigger) == 0) {
                string = string.substring(this.trigger.length(), string.length());
            }
        }
        if (this.pureContent.length() + string.length() > this.size) {
            return;
        }
        char[] strCh = string.toCharArray();
        boolean valid = true;
        for (int j = 0; j < strCh.length; ++j) {
            char c = strCh[j];
            if (Character.isDigit(c)) continue;
            valid = false;
            break;
        }
        if (valid) {
            int mpos = i;
            int impos = this.content.length() - mpos;
            impos = impos < 0 ? 0 : impos;
            int icpos = this.im2cpos(impos);
            int cpos = this.pureContent.length() - icpos;
            cpos = cpos < 0 ? 0 : cpos;
            String previous = this.pureContent.toString();
            this.pureContent.insert(cpos, string);
            if (this.maxNumber > 0 && this.pureContent.length() > 0 && Long.parseLong(this.pureContent.toString()) > this.maxNumber) {
                this.pureContent = new StringBuffer(previous);
            }
            this.refreshContent(filterBypass);
            this.component.setCaretPosition(this.content.length() - impos);
        }
    }

    @Override
    public void replace(DocumentFilter.FilterBypass filterBypass, int i, int i1, String string, AttributeSet attributeSet) throws BadLocationException {
        boolean oldPrefix = this.prefixActive;
        this.remove(filterBypass, i, i1);
        this.prefixActive = !(this.pureContent.length() != 0 || string == null || this.prefix == null || string.equals(this.prefix)) ? string.indexOf(this.prefix) == 0 : oldPrefix;
        this.insertString(filterBypass, i + i1, string, attributeSet);
    }

    @Override
    public void remove(DocumentFilter.FilterBypass filterBypass, int pos0, int len) throws BadLocationException {
        int ipos0 = this.content.length() - pos0;
        int ipos1 = this.content.length() - (pos0 + len);
        int icpos0 = this.im2cpos(ipos0);
        int icpos1 = this.im2cpos(ipos1);
        int cpos0 = this.pureContent.length() - icpos0;
        int cpos1 = this.pureContent.length() - icpos1;
        cpos0 = cpos0 < 0 ? 0 : cpos0;
        int n = cpos1 = cpos1 < 0 ? 0 : cpos1;
        if (len > 0 && cpos0 == cpos1) {
            ++cpos1;
            if (--ipos1 < 0) {
                ipos1 = 0;
            }
        }
        this.pureContent.delete(cpos0, cpos1);
        if (this.pureContent.length() == 0) {
            this.prefixActive = false;
        }
        this.refreshContent(filterBypass);
        this.component.setCaretPosition(this.content.length() - ipos1 < 0 ? 0 : this.content.length() - ipos1);
    }

    @Override
    public void refreshContent(DocumentFilter.FilterBypass filterBypass) {
        char[] pureChars;
        if (!this.acceptsleadingzeros) {
            try {
                if (this.pureContent.length() > 0) {
                    long number = Long.parseLong(this.pureContent.toString());
                    this.pureContent = new StringBuffer(Long.toString(number));
                }
            }
            catch (NumberFormatException e) {
                this.pureContent = new StringBuffer("");
                e.printStackTrace();
            }
        }
        if ((pureChars = this.pureContent.toString().toCharArray()).length == 0) {
            this.content = new StringBuffer(this.emptyChars);
        } else {
            this.content = new StringBuffer();
            int purePos = pureChars.length - 1;
            int maskPos = this.mask.length - 1;
            int plhlPos = this.placeholders.length - 1;
            while (maskPos >= 0) {
                if (purePos >= 0) {
                    if (this.mask[maskPos] == '#') {
                        this.content.insert(0, pureChars[purePos]);
                        --purePos;
                    } else {
                        this.content.insert(0, this.mask[maskPos]);
                    }
                } else if (plhlPos >= 0) {
                    this.content.insert(0, this.placeholders[plhlPos]);
                }
                --maskPos;
                --plhlPos;
            }
        }
        if (this.prefix != null && this.prefixActive) {
            this.content.insert(0, this.prefix);
        }
        try {
            filterBypass.replace(0, filterBypass.getDocument().getLength(), this.content.toString(), null);
        }
        catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setDot(NavigationFilter.FilterBypass filterBypass, int i, Position.Bias bias) {
        if (i == 0 || this.mask[this.mask.length - (this.content.length() - i) - 1] == '#') {
            this.previousDot = i;
            filterBypass.setDot(i, bias);
        } else if (i > 0) {
            if (i <= this.previousDot) {
                this.previousDot = i;
                this.setDot(filterBypass, i - 1, bias);
            } else {
                this.previousDot = i;
                if (i < this.content.length() && this.mask[this.mask.length - (this.content.length() - i)] == '#') {
                    filterBypass.setDot(i + 1, bias);
                } else {
                    this.setDot(filterBypass, i - 1, bias);
                }
            }
        } else {
            filterBypass.setDot(0, bias);
        }
    }

    @Override
    public void moveDot(NavigationFilter.FilterBypass filterBypass, int i, Position.Bias bias) {
        filterBypass.moveDot(i, bias);
    }

    @Override
    public void setAutomaticFocusTransferEnabled(boolean isAutomaticFocusTransferEnabled) {
        this.documentFilter.setAutomaticFocusTransferEnabled(isAutomaticFocusTransferEnabled);
    }
}

