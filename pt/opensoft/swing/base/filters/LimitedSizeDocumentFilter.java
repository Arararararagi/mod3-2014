/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import pt.opensoft.swing.base.filters.AutomaticFocusTransferFilter;

public class LimitedSizeDocumentFilter
extends AutomaticFocusTransferFilter {
    DocumentFilter next;
    int maxSize;

    public LimitedSizeDocumentFilter(int maxSize) {
        this(null, maxSize);
    }

    public LimitedSizeDocumentFilter(DocumentFilter next, int maxSize) {
        this.next = next;
        this.maxSize = maxSize;
    }

    @Override
    public void insertString(DocumentFilter.FilterBypass filterBypass, int i, String string, AttributeSet attributeSet) throws BadLocationException {
        if (filterBypass.getDocument().getLength() + string.length() > this.maxSize) {
            return;
        }
        if (this.next == null) {
            filterBypass.insertString(i, string, attributeSet);
        } else {
            this.next.insertString(filterBypass, i, string, attributeSet);
        }
    }

    @Override
    public void replace(DocumentFilter.FilterBypass filterBypass, int i, int i1, String string, AttributeSet attributeSet) throws BadLocationException {
        if (filterBypass.getDocument().getLength() + string.length() - i1 > this.maxSize) {
            return;
        }
        if (this.next == null) {
            filterBypass.replace(i, i1, string, attributeSet);
        } else {
            this.next.replace(filterBypass, i, i1, string, attributeSet);
        }
        if (this.isAutomaticFocusTransferEnabled && filterBypass.getDocument().getLength() >= this.maxSize) {
            this.transferFocusToNextComponent();
        }
    }
}

