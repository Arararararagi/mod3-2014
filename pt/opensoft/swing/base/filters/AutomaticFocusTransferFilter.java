/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import javax.swing.FocusManager;
import javax.swing.text.DocumentFilter;

public class AutomaticFocusTransferFilter
extends DocumentFilter {
    protected boolean isAutomaticFocusTransferEnabled;

    public void setAutomaticFocusTransferEnabled(boolean isAutomaticFocusTransferEnabled) {
        this.isAutomaticFocusTransferEnabled = isAutomaticFocusTransferEnabled;
    }

    public void transferFocusToNextComponent() {
        if (this.isAutomaticFocusTransferEnabled) {
            FocusManager.getCurrentManager().focusNextComponent();
        }
    }
}

