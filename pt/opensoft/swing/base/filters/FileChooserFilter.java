/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.base.filters;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class FileChooserFilter
extends FileFilter {
    public static final FileChooserFilter PDF = new FileChooserFilter("Ficheiros PDF", ".pdf");
    public static final FileChooserFilter XML = new FileChooserFilter("Ficheiros XML", ".xml");
    public static final FileChooserFilter EXCEL = new FileChooserFilter("Ficheiros XLS", ".xls");
    public static final FileChooserFilter WORD = new FileChooserFilter("Documentos Word", ".doc", ".docx");
    public static final FileChooserFilter IMAGE = new FileChooserFilter("Ficheiros de Imagem", ".jpg", ".jpeg", ".gif", ".png", ".bmp");
    public static final FileChooserFilter PFX_CERT = new FileChooserFilter("Certificados PFX - Personal Information Exchange", ".pfx");
    private String fullDescription;
    private String[] allowedExtensions;

    private /* varargs */ FileChooserFilter(String description, String ... allowedExtensions) {
        this.fullDescription = this.getFullDescription(description, allowedExtensions);
        this.allowedExtensions = allowedExtensions;
    }

    private /* varargs */ String getFullDescription(String description, String ... allowedExtensions) {
        StringBuffer fullDescriptionBuff = new StringBuffer(description);
        for (int i = 0; i < allowedExtensions.length; ++i) {
            fullDescriptionBuff.append(i == 0 ? " (" : (i > 0 ? ", " : ""));
            fullDescriptionBuff.append("*" + allowedExtensions[i]);
            fullDescriptionBuff.append(i == allowedExtensions.length - 1 ? ")" : "");
        }
        return fullDescriptionBuff.toString();
    }

    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        for (String allowedExtension : this.allowedExtensions) {
            if (!file.getName().endsWith(allowedExtension) && (file.getName() == null || !file.getName().endsWith(allowedExtension.toUpperCase()))) continue;
            return true;
        }
        return false;
    }

    @Override
    public String getDescription() {
        return this.fullDescription;
    }
}

