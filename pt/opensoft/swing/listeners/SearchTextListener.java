/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.listeners;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.util.StringUtil;

public class SearchTextListener
implements DocumentListener,
ActionListener {
    public static final Color DEFAULT_HILIT_COLOR = new Color(255, 150, 50);
    public static final Color DEFAULT_HILIT_ALL_COLOR = new Color(255, 255, 150);
    protected JTextField searchTextField;
    protected JTextComponent searchInTextComponent;
    protected Color searchTextFieldBackground;
    protected Color searchTextFieldForeground;
    protected Color errorBackground;
    protected Color errorForeground;
    protected Highlighter hilit;
    protected Highlighter.HighlightPainter painter;
    protected Highlighter.HighlightPainter allPainter;
    protected boolean caseSensitive;
    protected boolean asciiSensitive;
    private int currentIndex;

    public SearchTextListener(JSearchTextField searchTextField, JTextComponent searchableTextComponent) {
        this(searchTextField, searchableTextComponent, DEFAULT_HILIT_COLOR, DEFAULT_HILIT_ALL_COLOR);
    }

    public SearchTextListener(JSearchTextField searchTextField, JTextComponent searchableTextComponent, Color hilitColor, Color hilitAllColor) {
        this.searchTextField = searchTextField;
        this.searchInTextComponent = searchableTextComponent;
        this.searchTextFieldBackground = searchTextField.getBackground();
        this.searchTextFieldForeground = searchTextField.getForeground();
        this.caseSensitive = false;
        this.asciiSensitive = false;
        this.hilit = new DefaultHighlighter();
        this.painter = new DefaultHighlighter.DefaultHighlightPainter(hilitColor);
        this.allPainter = hilitAllColor != null ? new DefaultHighlighter.DefaultHighlightPainter(hilitAllColor) : null;
        searchableTextComponent.setHighlighter(this.hilit);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.currentIndex = 0;
        this.search();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.currentIndex = 0;
        this.search();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ++this.currentIndex;
        this.search();
    }

    public void search() {
        this.hilit.removeAllHighlights();
        String s = this.toSearchFormat(this.searchTextField.getText());
        if (s.length() <= 0) {
            this.searchTextField.setBackground(this.searchTextFieldBackground);
            this.searchTextField.setForeground(this.searchTextFieldForeground);
            return;
        }
        try {
            int index;
            String content = this.toSearchFormat(this.searchInTextComponent.getDocument().getText(0, this.searchInTextComponent.getDocument().getLength()));
            this.currentIndex = index = content.indexOf(s, this.currentIndex);
            if (index < 0) {
                this.currentIndex = index = content.indexOf(s, 0);
            }
            if (index >= 0) {
                final int end = index + s.length();
                this.hilit.addHighlight(index, end, this.painter);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        SearchTextListener.this.searchInTextComponent.setCaretPosition(end);
                    }
                });
                this.searchTextField.setBackground(this.searchTextFieldBackground);
                this.searchTextField.setForeground(this.searchTextFieldForeground);
            } else {
                if (this.errorForeground != null) {
                    this.searchTextField.setForeground(this.errorForeground);
                }
                if (this.errorBackground != null) {
                    this.searchTextField.setBackground(this.errorBackground);
                }
            }
            if (this.allPainter != null) {
                index = content.indexOf(s, 0);
                int lastEnd = 0;
                while (index >= 0) {
                    int end = index + s.length();
                    this.hilit.addHighlight(lastEnd - s.length(), lastEnd+=end, this.allPainter);
                    content = content.substring(end);
                    index = content.indexOf(s, 0);
                }
            }
        }
        catch (BadLocationException e1) {
            // empty catch block
        }
    }

    private String toSearchFormat(String string) {
        String result = string;
        if (!this.caseSensitive) {
            result = result.toLowerCase();
        }
        if (!this.asciiSensitive) {
            result = StringUtil.toAscii(result);
        }
        return result;
    }

    public Color getErrorBackground() {
        return this.errorBackground;
    }

    public void setErrorBackground(Color errorBackground) {
        this.errorBackground = errorBackground;
    }

    public Color getErrorForeground() {
        return this.errorForeground;
    }

    public void setErrorForeground(Color errorForeground) {
        this.errorForeground = errorForeground;
    }

    public boolean isCaseSensitive() {
        return this.caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public boolean isAsciiSensitive() {
        return this.asciiSensitive;
    }

    public void setAsciiSensitive(boolean asciiSensitive) {
        this.asciiSensitive = asciiSensitive;
    }

}

