/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.listeners;

import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.text.JTextComponent;

public final class SelectTextOnFocusField {
    private static final FocusListener listener = new FocusAdapter(){

        @Override
        public void focusGained(FocusEvent e) {
            if (e.getComponent() instanceof JTextComponent) {
                JTextComponent textField = (JTextComponent)e.getComponent();
                textField.selectAll();
            } else if (e.getComponent() instanceof JComboBox) {
                JComboBox cbox = (JComboBox)e.getComponent();
                JTextComponent textField = (JTextComponent)cbox.getEditor().getEditorComponent();
                textField.selectAll();
            } else {
                throw new IllegalArgumentException("Listener doesn't implement behaviour for this type of component");
            }
        }
    };

    private SelectTextOnFocusField() {
    }

    public static FocusListener getListener() {
        return listener;
    }

}

