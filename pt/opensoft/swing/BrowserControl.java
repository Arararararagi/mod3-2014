/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.applet.AppletContext;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import pt.opensoft.swing.WorkingDirDetection;

public class BrowserControl {
    private static AppletContext _appletContext = null;
    private static final String WIN_ID = "Windows";
    private static final String WIN_PATH = "rundll32";
    private static final String WIN_FLAG = "url.dll,FileProtocolHandler";
    private static final String UNIX_PATH = "netscape";
    private static final String UNIX_FLAG = "-remote openURL";
    private static final String MAC_ID = "Mac OS";

    public static void initWithApplet(AppletContext appletContext) {
        _appletContext = appletContext;
    }

    public static void displayURL(String url) {
        block21 : {
            String cmd = null;
            try {
                Process p;
                if (BrowserControl.isWindowsPlatform()) {
                    String finalUrl;
                    boolean useShortCut = false;
                    if (url.startsWith("file:/")) {
                        if (url.charAt(6) != '/') {
                            url = "file://" + url.substring(6);
                        }
                        if (url.charAt(7) != '/') {
                            url = "file:///" + url.substring(7);
                        }
                        useShortCut = true;
                    }
                    if (url.toLowerCase().endsWith("html") || url.toLowerCase().endsWith("htm")) {
                        useShortCut = true;
                    }
                    if (useShortCut) {
                        File shortcut = File.createTempFile("OpenInBrowser", ".url", WorkingDirDetection.getWorkingDir());
                        shortcut = shortcut.getCanonicalFile();
                        shortcut.deleteOnExit();
                        PrintWriter out = new PrintWriter(new FileWriter(shortcut));
                        out.println("[InternetShortcut]");
                        out.println("URL=" + url);
                        out.close();
                        finalUrl = shortcut.getCanonicalPath();
                    } else {
                        finalUrl = url;
                    }
                    cmd = "rundll32 url.dll,FileProtocolHandler \"" + finalUrl + "\"";
                    Process p2 = Runtime.getRuntime().exec(cmd);
                    break block21;
                }
                if (BrowserControl.isMacPlatform()) {
                    try {
                        Class mrjFileUtilsClass = Class.forName("com.apple.mrj.MRJFileUtils");
                        Method openURL = mrjFileUtilsClass.getDeclaredMethod("openURL", String.class);
                        openURL.invoke(null, url);
                    }
                    catch (Exception ex) {}
                    break block21;
                }
                String browser = "netscape";
                try {
                    p = Runtime.getRuntime().exec("which firebird");
                    if (p.waitFor() == 0) {
                        browser = "firebird";
                    }
                    if ((p = Runtime.getRuntime().exec("which mozilla")).waitFor() == 0) {
                        browser = "mozilla";
                    }
                    if ((p = Runtime.getRuntime().exec("which opera")).waitFor() == 0) {
                        browser = "opera";
                    }
                }
                catch (InterruptedException e) {
                    // empty catch block
                }
                cmd = browser + " " + "-remote openURL" + "(" + url + ")";
                p = Runtime.getRuntime().exec(cmd);
                try {
                    int exitCode = p.waitFor();
                    if (exitCode != 0) {
                        cmd = "netscape " + url;
                        p = Runtime.getRuntime().exec(cmd);
                    }
                }
                catch (InterruptedException x) {
                    System.err.println("Error bringing up browser, cmd='" + cmd + "'");
                    System.err.println("Caught: " + x);
                }
            }
            catch (IOException x) {
                System.err.println("Could not invoke browser, command=" + cmd);
                System.err.println("Caught: " + x);
            }
        }
    }

    private static boolean isWindowsPlatform() {
        String os = System.getProperty("os.name");
        if (os != null && os.startsWith("Windows")) {
            return true;
        }
        return false;
    }

    private static boolean isMacPlatform() {
        String os = System.getProperty("os.name");
        if (os != null && os.startsWith("Mac OS")) {
            return true;
        }
        return false;
    }
}

