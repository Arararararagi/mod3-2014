/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import pt.opensoft.swing.EnhancedAction;

public class EnhancedButton
extends JButton {
    protected Border _activeBorder;
    protected Border _inactiveBorder;

    public EnhancedButton() {
        this.enableEvents(16);
    }

    public EnhancedButton(EnhancedAction action) {
        this.setAction(action);
        this.enableEvents(16);
    }

    @Deprecated
    public EnhancedButton(EnhancedAction a, boolean opaque) {
        Icon icon;
        super(a);
        this._activeBorder = new CompoundBorder(new EtchedBorder(1), new EmptyBorder(3, 3, 3, 3));
        this._inactiveBorder = new EmptyBorder(5, 5, 5, 5);
        this.setBorder(this._inactiveBorder);
        this.setToolTipText(a.getTooltip());
        this.setOpaque(opaque);
        Icon icon2 = icon = a != null ? (Icon)a.getValue("SmallIcon") : null;
        if (icon != null) {
            this.setText(null);
        }
        this.enableEvents(16);
    }

    public void setAction(EnhancedAction action) {
        Icon icon;
        super.setAction(action);
        if (action == null) {
            return;
        }
        this._activeBorder = new CompoundBorder(new EtchedBorder(1), new EmptyBorder(3, 3, 3, 3));
        this._inactiveBorder = action.showWithBorder() ? this._activeBorder : new EmptyBorder(5, 5, 5, 5);
        this.setBorder(this._inactiveBorder);
        this.setToolTipText(action.getTooltip());
        this.setOpaque(false);
        Icon icon2 = icon = action != null ? (Icon)action.getValue("SmallIcon") : null;
        if (!(icon == null || action.showWithName())) {
            this.setText(null);
        }
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        switch (e.getID()) {
            case 504: {
                this.setBorder(this._activeBorder);
                this.setCursor(Cursor.getPredefinedCursor(12));
                break;
            }
            case 505: {
                this.setBorder(this._inactiveBorder);
                this.setCursor(Cursor.getPredefinedCursor(0));
                break;
            }
        }
        super.processMouseEvent(e);
    }
}

