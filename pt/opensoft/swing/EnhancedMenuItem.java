/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.Refreshable;

public class EnhancedMenuItem
extends JMenuItem
implements Refreshable {
    public EnhancedMenuItem(EnhancedAction actionMenu) {
        super(actionMenu);
        if (actionMenu != null) {
            this.setAccelerator(actionMenu.getShortcut());
        }
    }

    public EnhancedMenuItem(EnhancedAction actionMenu, char mnemonic) {
        super(actionMenu);
        this.setMnemonic(mnemonic);
        if (actionMenu != null) {
            this.setAccelerator(actionMenu.getShortcut());
        }
    }

    @Override
    public void refreshEnabled() {
        this.setEnabled(this.getAction().isEnabled());
    }
}

