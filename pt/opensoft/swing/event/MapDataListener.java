/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.event;

import java.util.EventListener;
import pt.opensoft.swing.event.MapDataEvent;

public interface MapDataListener
extends EventListener {
    public void objectPut(MapDataEvent var1);

    public void objectRemoved(MapDataEvent var1);

    public void contentChanged(MapDataEvent var1);

    public void startedClear();

    public void finishedClear();
}

