/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.event;

import java.util.EventObject;

public class MapDataEvent
extends EventObject {
    public static final int CONTENT_CHANGED = 0;
    public static final int OBJECT_PUT = 1;
    public static final int OBJECT_REMOVED = 2;
    private int type;
    private Object key;
    private Object value;

    public int getType() {
        return this.type;
    }

    public Object getKey() {
        return this.key;
    }

    public Object getValue() {
        return this.value;
    }

    public MapDataEvent(Object source, int type, Object key, Object value) {
        super(source);
        this.type = type;
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[type=" + this.type + " key=" + this.key + "]";
    }
}

