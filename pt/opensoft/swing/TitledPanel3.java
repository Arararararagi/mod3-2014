/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.jgoodies.looks.LookUtils
 */
package pt.opensoft.swing;

import com.jgoodies.looks.LookUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.TitledPanel2;

public class TitledPanel3
extends TitledPanel2 {
    protected GradientPanel gradientPanel;
    protected boolean useGradient;
    protected Color textColor;

    public TitledPanel3(LayoutManager layoutManager) {
        super(layoutManager);
    }

    public TitledPanel3(Icon icon, String title, Color titleColor) {
        this(icon, title, null, null, titleColor, null, null, false, null, false);
    }

    public TitledPanel3(Icon icon, String title, Color titleColor, boolean useGradient) {
        this(icon, title, null, null, titleColor, null, null, useGradient, null, false);
    }

    public TitledPanel3(Icon icon, String title, Color titleColor, boolean useGradient, boolean alignTitleLeft) {
        this(icon, title, null, null, titleColor, null, null, useGradient, null, alignTitleLeft);
    }

    public TitledPanel3(Icon icon, String title, Color titleColor, Insets insets) {
        this(icon, title, null, null, titleColor, null, insets, false, null, false);
    }

    public TitledPanel3(Icon icon, String title, EnhancedAction closeAction, Color titleColor, JToolBar toolbar) {
        this(icon, title, null, closeAction, titleColor, toolbar, null, false, null, false);
    }

    public TitledPanel3(Icon icon, String title, EnhancedAction printAction, EnhancedAction closeAction, Color titleColor, JToolBar toolbar) {
        this(icon, title, printAction, closeAction, titleColor, toolbar, null, false, null, false);
    }

    public TitledPanel3(Icon icon, String title, Color textColor, EnhancedAction printAction, EnhancedAction closeAction, Color titleColor, JToolBar toolbar) {
        this(icon, title, printAction, closeAction, titleColor, toolbar, null, false, textColor, false);
    }

    public TitledPanel3(Icon icon, String title, EnhancedAction closeAction, Color titleColor, JToolBar toolbar, Insets insets) {
        this(icon, title, null, closeAction, titleColor, toolbar, insets, false, null, false);
    }

    public TitledPanel3(Icon icon, String title, EnhancedAction printAction, EnhancedAction closeAction, Color titleColor, JToolBar toolbar, Insets insets, boolean useGradient, Color textColor, boolean alignTitleLeft) {
        super(new BorderLayout());
        this.useGradient = useGradient;
        this.textColor = textColor;
        if (titleColor != null) {
            this.headerBackground = titleColor;
        }
        this.titleLabel = printAction != null || closeAction != null || alignTitleLeft ? new JLabel(title, icon, 10) : new JLabel(title, icon, 0);
        Font tituloFont = UIManager.getFont("Impressos.font");
        this.titleLabel.setFont(tituloFont != null ? tituloFont : new Font(this.titleLabel.getFont().getFontName(), 1, this.titleLabel.getFont().getSize() + 2));
        this.titleLabel.setToolTipText(title);
        JPanel top = this.buildHeader(this.titleLabel, printAction, closeAction, toolbar);
        this.add((Component)top, "North");
        if (insets != null) {
            this.setBorder(new CompoundBorder(new EmptyBorder(insets), new ShadowBorder()));
        } else {
            this.setBorder(new ShadowBorder());
        }
        this.updateHeader();
    }

    @Override
    protected JPanel buildHeader(JLabel label, EnhancedAction printAction, EnhancedAction closeAction, JToolBar toolbar) {
        this.gradientPanel = new GradientPanel(new BorderLayout(), this.getHeaderBackground(), this.useGradient);
        label.setOpaque(false);
        if (printAction != null || closeAction != null) {
            this.gradientPanel.add((Component)label, "West");
        } else {
            this.gradientPanel.add((Component)label, "North");
        }
        JComponent rightComponent = null;
        if (printAction != null && closeAction != null) {
            rightComponent = new JPanel(new FlowLayout(2));
            rightComponent.setOpaque(false);
            JButton printButton = this.getPrintButton(printAction);
            JButton closeButton = this.getCloseButton(closeAction);
            rightComponent.add(printButton);
            rightComponent.add(closeButton);
        } else if (printAction != null) {
            rightComponent = this.getPrintButton(printAction);
        } else if (closeAction != null) {
            rightComponent = this.getCloseButton(closeAction);
        }
        if (rightComponent != null) {
            this.gradientPanel.add((Component)rightComponent, "East");
        }
        this.headerPanel = new JPanel(new BorderLayout());
        this.headerPanel.add((Component)this.gradientPanel, "Center");
        if (toolbar != null) {
            this.headerPanel.add((Component)toolbar, "South");
        }
        this.headerPanel.setBorder(BorderFactory.createEmptyBorder());
        this.headerPanel.setOpaque(false);
        return this.headerPanel;
    }

    @Override
    protected void updateHeader() {
        this.gradientPanel.setBackground(this.getHeaderBackground());
        this.gradientPanel.setOpaque(true);
        this.titleLabel.setForeground(this.getTextForeground(true));
        if (this.textColor != null) {
            this.titleLabel.setForeground(this.textColor);
        }
        this.headerPanel.repaint();
    }

    @Override
    protected Color getHeaderBackground() {
        if (this.headerBackground != null) {
            return this.headerBackground;
        }
        Color c = UIManager.getColor("TitledPanel2.activeTitleBackground");
        if (c != null) {
            return c;
        }
        if (LookUtils.IS_LAF_WINDOWS_XP_ENABLED) {
            c = UIManager.getColor("InternalFrame.activeTitleGradient");
        }
        return c != null ? c : UIManager.getColor("InternalFrame.activeTitleBackground");
    }

    protected static class GradientPanel
    extends JPanel {
        private boolean useGradient;

        public GradientPanel(LayoutManager lm, Color background, boolean useGradient) {
            super(lm);
            this.setBackground(background);
            this.useGradient = useGradient;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (!this.isOpaque()) {
                return;
            }
            Color control = this.useGradient ? UIManager.getColor("control") : this.getBackground();
            int width = this.getWidth();
            int height = this.getHeight();
            Graphics2D g2 = (Graphics2D)g;
            Paint storedPaint = g2.getPaint();
            g2.setPaint(new GradientPaint(0.0f, 0.0f, this.getBackground(), (int)((double)width * 1.5), 0.0f, control));
            g2.fillRect(0, 0, width, height);
            g2.setPaint(storedPaint);
        }
    }

    public static class ShadowBorder
    extends AbstractBorder {
        private static final Insets INSETS = new Insets(1, 1, 3, 3);

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            Color shadow = UIManager.getColor("controlShadow");
            if (shadow == null) {
                shadow = Color.GRAY;
            }
            Color lightShadow = new Color(shadow.getRed(), shadow.getGreen(), shadow.getBlue(), 170);
            Color lighterShadow = new Color(shadow.getRed(), shadow.getGreen(), shadow.getBlue(), 70);
            g.translate(x, y);
            g.setColor(shadow);
            g.fillRect(0, 0, w - 3, 1);
            g.fillRect(0, 0, 1, h - 3);
            g.fillRect(w - 3, 1, 1, h - 3);
            g.fillRect(1, h - 3, w - 3, 1);
            g.setColor(lightShadow);
            g.fillRect(w - 3, 0, 1, 1);
            g.fillRect(0, h - 3, 1, 1);
            g.fillRect(w - 2, 1, 1, h - 3);
            g.fillRect(1, h - 2, w - 3, 1);
            g.setColor(lighterShadow);
            g.fillRect(w - 2, 0, 1, 1);
            g.fillRect(0, h - 2, 1, 1);
            g.fillRect(w - 2, h - 2, 1, 1);
            g.fillRect(w - 1, 1, 1, h - 2);
            g.fillRect(1, h - 1, w - 2, 1);
            g.translate(- x, - y);
        }
    }

}

