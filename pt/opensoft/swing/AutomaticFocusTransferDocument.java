/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import javax.swing.FocusManager;
import javax.swing.text.PlainDocument;

public class AutomaticFocusTransferDocument
extends PlainDocument {
    private static final long serialVersionUID = -8161986363809164894L;
    protected boolean isAutomaticFocusTransferEnabled;

    public void setAutomaticFocusTransferEnabled(boolean isAutomaticFocusTransferEnabled) {
        this.isAutomaticFocusTransferEnabled = isAutomaticFocusTransferEnabled;
    }

    public void transferFocusToNextComponent() {
        if (this.isAutomaticFocusTransferEnabled) {
            FocusManager.getCurrentManager().focusNextComponent();
        }
    }
}

