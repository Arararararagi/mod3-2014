/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.LayoutManager;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import pt.opensoft.swing.base.filters.FileChooserFilter;

public class DialogFactory {
    protected static DialogFactory _instance;
    protected static Component _parent;
    private JFileChooser _dlgFileChooser = new JFileChooser(new File("."));
    private Map<FileChooserFilter, JFileChooser> _filteredFileChoosers = new HashMap<FileChooserFilter, JFileChooser>();

    public static void init(Component parent) {
        _parent = parent;
        UIManager.put("FileChooser.openButtonText", "Abrir");
        UIManager.put("FileChooser.saveButtonText", "Gravar");
        UIManager.put("FileChooser.cancelButtonText", "Cancelar");
        UIManager.put("FileChooser.directoryOpenButtonText", "Abrir");
        UIManager.put("FileChooser.openButtonToolTipText", "Abrir ficheiro seleccionado");
        UIManager.put("FileChooser.saveButtonToolTipText", "Gravar ficheiro seleccionado");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Cancelar");
        UIManager.put("FileChooser.directoryOpenButtonToolTipText", "Abrir a pasta seleccionada");
        UIManager.put("FileChooser.upFolderToolTipText", "N\u00edvel acima");
        UIManager.put("FileChooser.homeFolderToolTipText", "");
        UIManager.put("FileChooser.newFolderToolTipText", "Nova pasta");
        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");
        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Detalhe");
        UIManager.put("FileChooser.saveButtonMnemonic", String.valueOf(103));
        UIManager.put("FileChooser.openButtonMnemonic", String.valueOf(97));
        UIManager.put("FileChooser.cancelButtonMnemonic", String.valueOf(99));
        UIManager.put("FileChooser.openDialogTitleText", "Abrir");
        UIManager.put("FileChooser.saveDialogTitleText", "Gravar");
        UIManager.put("FileChooser.saveInLabelText", "Gravar em:");
        UIManager.put("FileChooser.lookInLabelText", "Procurar em:");
        UIManager.put("FileChooser.lookInLabelMnemonic", String.valueOf(112));
        UIManager.put("FileChooser.fileNameLabelText", "Nome do ficheiro:");
        UIManager.put("FileChooser.fileNameLabelMnemonic", String.valueOf(110));
        UIManager.put("FileChooser.filesOfTypeLabelText", "Tipo do ficheiro:");
        UIManager.put("FileChooser.filesOfTypeLabelMnemonic", String.valueOf(116));
        UIManager.put("FileChooser.fileNameHeaderText", "Nome");
        UIManager.put("FileChooser.fileSizeHeaderText", "Tamanho");
        UIManager.put("FileChooser.fileTypeHeaderText", "Tipo");
        UIManager.put("FileChooser.fileDateHeaderText", "Data");
        UIManager.put("FileChooser.fileAttrHeaderText", "Atributos");
        UIManager.put("OptionPane.yesButtonText", "Sim");
        UIManager.put("OptionPane.noButtonText", "N\u00e3o");
        UIManager.put("OptionPane.cancelButtonText", "Cancelar");
        UIManager.put("OptionPane.yesButtonMnemonic", String.valueOf('s'));
        UIManager.put("OptionPane.titleText", "Seleccione uma op\u00e7\u00e3o");
        UIManager.put("ProgressMonitor.progressText", "Progresso...");
        _instance = new DialogFactory();
    }

    public static Component getDummyFrame() {
        return _parent;
    }

    public static synchronized DialogFactory instance() {
        if (_instance == null) {
            throw new RuntimeException("DialogFactory must be initialized first");
        }
        return _instance;
    }

    protected DialogFactory() {
    }

    public JFileChooser getFileChooser() {
        if (this._dlgFileChooser == null) {
            this._dlgFileChooser = new JFileChooser(new File("."));
            this._dlgFileChooser.setCurrentDirectory(this._dlgFileChooser.getFileSystemView().getDefaultDirectory());
        }
        return this._dlgFileChooser;
    }

    public JFileChooser getFilteredFileChooser(FileChooserFilter fileChooserFilter) {
        JFileChooser filteredFileChooser = this._filteredFileChoosers.get(fileChooserFilter);
        if (filteredFileChooser == null) {
            filteredFileChooser = new JFileChooser(new File("."));
            filteredFileChooser.setCurrentDirectory(filteredFileChooser.getFileSystemView().getDefaultDirectory());
            filteredFileChooser.setFileFilter(fileChooserFilter);
            this._filteredFileChoosers.put(fileChooserFilter, filteredFileChooser);
        }
        filteredFileChooser.setSelectedFile(new File(""));
        return filteredFileChooser;
    }

    public void showExceptionDialog(Exception ex) {
        JOptionPane.showMessageDialog(_parent, ex.getMessage(), "Erro grave", 0);
        ex.printStackTrace();
    }

    public boolean showYesNoDialog(Object message) {
        return JOptionPane.showConfirmDialog(_parent, message, UIManager.getString("OptionPane.titleText"), 0) == 0;
    }

    public boolean showYesNoDialog(Object message, Object textButtonYes, Object textButtonNo) {
        Object[] options = new Object[]{textButtonYes, textButtonNo};
        return JOptionPane.showOptionDialog(_parent, message, UIManager.getString("OptionPane.titleText"), 0, 3, null, options, null) == 0;
    }

    public boolean showOkCancelDialog(Object message, Object textButtonOk, Object textButtonCancel) {
        Object[] options = new Object[]{textButtonOk, textButtonCancel};
        return JOptionPane.showOptionDialog(_parent, message, UIManager.getString("OptionPane.titleText"), 0, 2, null, options, null) == 0;
    }

    public boolean showOkCancelDialog(Object message) {
        return JOptionPane.showConfirmDialog(_parent, message, UIManager.getString("Aviso"), 2, 2) == 0;
    }

    public boolean showConfirmationDialog(Object message) {
        return JOptionPane.showConfirmDialog(_parent, message) == 0;
    }

    public int showConfirmationDialog2(Object message) {
        return JOptionPane.showConfirmDialog(_parent, message);
    }

    public void showInformationDialog(Object message) {
        JOptionPane.showMessageDialog(_parent, message, "Informa\u00e7\u00e3o", 1);
    }

    public void showWarningDialog(Object message) {
        JOptionPane.showMessageDialog(_parent, message, "Aviso", 2);
    }

    public void showErrorDialog(Object message) {
        JOptionPane.showMessageDialog(_parent, message, "Erro", 0);
    }

    public void showMultipleErrorsDialog(String errorMessage, String errorList) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.add((Component)new JLabel(errorMessage), "North");
        JTextArea errorsArea = new JTextArea(errorList, 5, 60);
        errorsArea.setBorder(new EtchedBorder());
        JScrollPane scrollPane = new JScrollPane(errorsArea);
        panel.add((Component)scrollPane, "Center");
        this.showErrorDialog(panel);
    }

    public String showInputDialog(Object message) {
        return JOptionPane.showInputDialog(_parent, message);
    }

    public void showWaitCursor() {
        if (_parent != null) {
            _parent.setCursor(Cursor.getPredefinedCursor(3));
        }
    }

    public void dismissWaitCursor() {
        if (_parent != null) {
            _parent.setCursor(Cursor.getPredefinedCursor(0));
        }
    }
}

