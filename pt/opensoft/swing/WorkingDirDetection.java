/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.io.File;

public class WorkingDirDetection {
    private static final String VISTA_APP_WORKING_DIR = "AppData\\LocalLow\\";

    public static boolean isVistaRunning() {
        String osname = System.getProperty("os.name");
        String osversion = System.getProperty("os.version");
        if (osname == null || osversion == null) {
            return false;
        }
        return osname.toLowerCase().startsWith("win") && osversion.toLowerCase().startsWith("6.");
    }

    public static File getWorkingDir() {
        if (WorkingDirDetection.isVistaRunning()) {
            return new File(System.getProperty("user.home"), "AppData\\LocalLow\\");
        }
        return new File(System.getProperty("user.home"));
    }
}

