/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.jgoodies.looks.LookUtils
 */
package pt.opensoft.swing;

import com.jgoodies.looks.LookUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedButton;
import pt.opensoft.swing.components.JSearchTextField;

public class GenericTitledPanel
extends JPanel {
    protected JLabel titleLabel;
    protected GradientPanel gradientPanel;
    protected JPanel headerPanel;
    protected Color headerBackground;
    protected JToolBar actionBar;
    protected JSearchTextField searchTextField;

    public GenericTitledPanel(LayoutManager layoutManager) {
        super(layoutManager);
    }

    public GenericTitledPanel(Icon icon, String title, List<EnhancedAction> actions, JSearchTextField searchTextField, Color titleColor, JToolBar toolbar, Insets insets) {
        super(new BorderLayout());
        if (titleColor != null) {
            this.headerBackground = titleColor;
        }
        this.searchTextField = searchTextField;
        this.titleLabel = new JLabel(title, icon, 10);
        Font tituloFont = this.titleLabel.getFont();
        this.titleLabel.setFont(new Font(tituloFont.getFontName(), 1, tituloFont.getSize() + 2));
        this.titleLabel.setToolTipText(title);
        this.actionBar = new JToolBar();
        for (EnhancedAction action : actions) {
            this.actionBar.add(this.getButtonForAction(action));
        }
        this.actionBar.setOpaque(false);
        this.actionBar.setFloatable(false);
        JPanel top = this.buildHeader(this.titleLabel, this.actionBar, this.searchTextField, toolbar);
        this.add((Component)top, "North");
        if (insets != null) {
            this.setBorder(new CompoundBorder(new EmptyBorder(insets), new ShadowBorder()));
        } else {
            this.setBorder(new ShadowBorder());
        }
        this.updateHeader();
    }

    public Icon getFrameIcon() {
        return this.titleLabel.getIcon();
    }

    public void setFrameIcon(Icon newIcon) {
        Icon oldIcon = this.getFrameIcon();
        this.titleLabel.setIcon(newIcon);
        this.firePropertyChange("frameIcon", oldIcon, newIcon);
    }

    public String getTitle() {
        return this.titleLabel.getText();
    }

    public void setTitle(String newText) {
        String oldText = this.getTitle();
        this.titleLabel.setText(newText);
        this.firePropertyChange("title", oldText, newText);
    }

    public JToolBar getToolBar() {
        return this.headerPanel.getComponentCount() > 1 ? (JToolBar)this.headerPanel.getComponent(1) : null;
    }

    public void setToolBar(JToolBar newToolBar) {
        JToolBar oldToolBar = this.getToolBar();
        if (oldToolBar == newToolBar) {
            return;
        }
        if (oldToolBar != null) {
            this.headerPanel.remove(oldToolBar);
        }
        if (newToolBar != null) {
            newToolBar.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
            this.headerPanel.add((Component)newToolBar, "East");
        }
        this.updateHeader();
        this.firePropertyChange("toolBar", oldToolBar, newToolBar);
    }

    public Component getContent() {
        return this.hasContent() ? this.getComponent(1) : null;
    }

    public void addInternal(Component newContent, Object constraints) {
        Component oldContent = this.getContent();
        if (this.hasContent()) {
            this.remove(oldContent);
        }
        this.add(newContent, constraints);
        this.firePropertyChange("content", oldContent, newContent);
    }

    protected JPanel buildHeader(JLabel label, JToolBar actions, JSearchTextField searchTextField, JToolBar toolbar) {
        this.gradientPanel = new GradientPanel(new BorderLayout(), this.getHeaderBackground());
        label.setOpaque(false);
        this.gradientPanel.add((Component)label, "West");
        JPanel rightComponent = new JPanel(new FlowLayout(2));
        rightComponent.setOpaque(false);
        if (searchTextField != null) {
            rightComponent.add(searchTextField);
            searchTextField.setColumns(15);
            searchTextField.addAncestorListener(new AncestorListener(){

                @Override
                public void ancestorAdded(AncestorEvent ae) {
                    ae.getComponent().requestFocus();
                }

                @Override
                public void ancestorRemoved(AncestorEvent ae) {
                }

                @Override
                public void ancestorMoved(AncestorEvent ae) {
                }
            });
        }
        if (actions != null) {
            rightComponent.add(actions);
        }
        if (rightComponent != null) {
            this.gradientPanel.add((Component)rightComponent, "East");
        }
        this.gradientPanel.setBorder(BorderFactory.createEmptyBorder(3, 4, 3, 1));
        this.headerPanel = new JPanel(new BorderLayout());
        this.headerPanel.add((Component)this.gradientPanel, "Center");
        if (toolbar != null) {
            toolbar.setBorderPainted(false);
            this.headerPanel.add((Component)toolbar, "South");
        }
        this.headerPanel.setBorder(new RaisedHeaderBorder());
        this.headerPanel.setOpaque(false);
        return this.headerPanel;
    }

    protected boolean isCloseButtonOpaque() {
        return false;
    }

    protected boolean isPrintButtonOpaque() {
        return false;
    }

    protected JButton getButtonForAction(EnhancedAction action) {
        Color c;
        EnhancedButton button = new EnhancedButton(action, this.isCloseButtonOpaque());
        if (action.showWithName()) {
            button.setText(action.getText());
        }
        if ((c = UIManager.getColor("titledpanel.button.foreground")) != null) {
            button.setForeground(c);
        }
        this.getInputMap(2).put(action.getShortcut(), action.getText());
        this.getActionMap().put(action.getText(), action);
        return button;
    }

    protected void updateHeader() {
        this.gradientPanel.setBackground(this.getHeaderBackground());
        this.gradientPanel.setOpaque(true);
        this.titleLabel.setForeground(this.getTextForeground(true));
        this.headerPanel.repaint();
    }

    @Override
    public void updateUI() {
        super.updateUI();
        if (this.titleLabel != null) {
            this.updateHeader();
        }
    }

    private boolean hasContent() {
        return this.getComponentCount() > 1;
    }

    protected Color getTextForeground(boolean selected) {
        Color c = UIManager.getColor(selected ? "TitledPanel2.activeTitleForeground" : "TitledPanel2.inactiveTitleForeground");
        if (c != null) {
            return c;
        }
        return UIManager.getColor(selected ? "InternalFrame.activeTitleForeground" : "Label.foreground");
    }

    protected Color getHeaderBackground() {
        if (this.headerBackground != null) {
            return this.headerBackground;
        }
        Color c = UIManager.getColor("TitledPanel2.activeTitleBackground");
        if (c != null) {
            return c;
        }
        if (LookUtils.IS_LAF_WINDOWS_XP_ENABLED) {
            c = UIManager.getColor("InternalFrame.activeTitleGradient");
        }
        return c != null ? c : UIManager.getColor("InternalFrame.activeTitleBackground");
    }

    protected static class GradientPanel
    extends JPanel {
        public GradientPanel(LayoutManager lm, Color background) {
            super(lm);
            this.setBackground(background);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (!this.isOpaque()) {
                return;
            }
            Color control = UIManager.getColor("control");
            int width = this.getWidth();
            int height = this.getHeight();
            Graphics2D g2 = (Graphics2D)g;
            Paint storedPaint = g2.getPaint();
            g2.setPaint(new GradientPaint(0.0f, 0.0f, this.getBackground(), (int)((double)width * 1.5), 0.0f, control));
            g2.fillRect(0, 0, width, height);
            g2.setPaint(storedPaint);
        }
    }

    protected static class RaisedHeaderBorder
    extends AbstractBorder {
        private static final Insets INSETS = new Insets(1, 1, 1, 0);

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            g.setColor(UIManager.getColor("controlLtHighlight"));
            g.fillRect(0, 0, w, 1);
            g.fillRect(0, 1, 1, h - 1);
            g.setColor(UIManager.getColor("controlShadow"));
            g.fillRect(0, h - 1, w, 1);
            g.translate(- x, - y);
        }
    }

    public static class ShadowBorder
    extends AbstractBorder {
        private static final Insets INSETS = new Insets(1, 1, 3, 3);

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            Color shadow = UIManager.getColor("controlShadow");
            if (shadow == null) {
                shadow = Color.GRAY;
            }
            Color lightShadow = new Color(shadow.getRed(), shadow.getGreen(), shadow.getBlue(), 170);
            Color lighterShadow = new Color(shadow.getRed(), shadow.getGreen(), shadow.getBlue(), 70);
            g.translate(x, y);
            g.setColor(shadow);
            g.fillRect(0, 0, w - 3, 1);
            g.fillRect(0, 0, 1, h - 3);
            g.fillRect(w - 3, 1, 1, h - 3);
            g.fillRect(1, h - 3, w - 3, 1);
            g.setColor(lightShadow);
            g.fillRect(w - 3, 0, 1, 1);
            g.fillRect(0, h - 3, 1, 1);
            g.fillRect(w - 2, 1, 1, h - 3);
            g.fillRect(1, h - 2, w - 3, 1);
            g.setColor(lighterShadow);
            g.fillRect(w - 2, 0, 1, 1);
            g.fillRect(0, h - 2, 1, 1);
            g.fillRect(w - 2, h - 2, 1, 1);
            g.fillRect(w - 1, 1, 1, h - 2);
            g.fillRect(1, h - 1, w - 2, 1);
            g.translate(- x, - y);
        }
    }

}

