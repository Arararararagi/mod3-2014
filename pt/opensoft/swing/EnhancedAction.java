/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.TableCellEditor;
import pt.opensoft.swing.InvalidContextException;

public abstract class EnhancedAction
extends AbstractAction {
    protected String _text;
    protected String _tooltip;
    protected String _realTooltip;
    protected KeyStroke _shortcut;
    protected Icon _iconBig;
    protected JTextField _input;
    protected JButton _button;

    protected EnhancedAction(String name, Icon iconSmall, Icon iconBig, KeyStroke shortcut) {
        this(name, iconSmall, iconBig, name, shortcut);
    }

    protected EnhancedAction(String name, Icon iconSmall) {
        super(name, iconSmall);
    }

    protected EnhancedAction(String name, JTextField input, JButton button) {
        this(name, input, button, name);
    }

    protected EnhancedAction(String name, JTextField input, JButton button, String tooltip) {
        this.putValue("Name", input);
        this._text = name;
        this._tooltip = tooltip;
        this._input = input;
        this._button = button;
        this._realTooltip = tooltip;
    }

    protected EnhancedAction(String name, Icon iconSmall, Icon iconBig, String tooltip, KeyStroke shortcut) {
        super(name, iconSmall);
        this._text = name;
        this._tooltip = tooltip;
        this._shortcut = shortcut;
        this._iconBig = iconBig;
        this._realTooltip = tooltip;
        if (this._shortcut != null) {
            this._realTooltip = this._realTooltip + " (" + EnhancedAction.keyStroke2String(this._shortcut) + ")";
        }
    }

    public String getText() {
        return this._text;
    }

    public final String getTooltip() {
        return this._realTooltip;
    }

    public KeyStroke getShortcut() {
        return this._shortcut;
    }

    public Icon getIconBig() {
        return this._iconBig;
    }

    public boolean showInToolbar() {
        return true;
    }

    public boolean showWithName() {
        return true;
    }

    public boolean showWithBorder() {
        return false;
    }

    private static String keyStroke2String(KeyStroke keyStroke) {
        if (keyStroke.getModifiers() != 0) {
            return KeyEvent.getKeyModifiersText(keyStroke.getModifiers()) + "+" + KeyEvent.getKeyText(keyStroke.getKeyCode());
        }
        return KeyEvent.getKeyText(keyStroke.getKeyCode());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Component componentWithFocus = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (componentWithFocus == null) {
            return;
        }
        if (componentWithFocus instanceof JComponent && ((JComponent)componentWithFocus).getInputVerifier() != null && !((JComponent)componentWithFocus).getInputVerifier().verify(null)) {
            throw new InvalidContextException();
        }
        if (e == null) {
            return;
        }
        if (e.getModifiers() == 0) {
            if (componentWithFocus.getParent() != null && componentWithFocus.getParent() instanceof JTable) {
                JTable jTable = (JTable)componentWithFocus.getParent();
                if (jTable.getCellEditor() != null) {
                    jTable.getCellEditor().stopCellEditing();
                }
            } else {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        }
    }

    public JTextField get_input() {
        return this._input;
    }

    public void set_input(JTextField _input) {
        this._input = _input;
    }
}

