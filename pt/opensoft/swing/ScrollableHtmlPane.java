/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.io.IOException;
import java.net.URL;
import javax.swing.JEditorPane;

public class ScrollableHtmlPane
extends JEditorPane {
    public ScrollableHtmlPane() {
    }

    public ScrollableHtmlPane(String url) throws IOException {
        super(url);
    }

    public ScrollableHtmlPane(URL initialPage) throws IOException {
        super(initialPage);
    }

    public ScrollableHtmlPane(String type, String text) {
        super(type, text);
    }

    @Override
    public void setText(String t) {
        this.validate();
        super.setText(t);
        this.setCaretPosition(0);
    }
}

