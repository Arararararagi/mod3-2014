/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.util.List;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.SeparatorAction;
import pt.opensoft.swing.util.IRenderableAction;
import pt.opensoft.util.ListMap;

public class EnhancedToolBar
extends JToolBar {
    public EnhancedToolBar(String name, int orientation) {
        super(name, orientation);
    }

    public EnhancedToolBar(ListMap leftAlignedActions, EnhancedAction[] rightAlignedActions, int textOrientation) {
        this(leftAlignedActions, rightAlignedActions, 0, textOrientation);
    }

    public EnhancedToolBar(ListMap leftAlignedActions, EnhancedAction[] rightAlignedActions, int orientation, int textOrientation) {
        this(leftAlignedActions, rightAlignedActions, orientation, textOrientation, true, null, false);
    }

    public EnhancedToolBar(ListMap leftAlignedActions, EnhancedAction[] rightAlignedActions, int orientation, int textOrientation, boolean isRollover, Border border, boolean addSeparator) {
        JButton button;
        super("Standard", orientation);
        this.setFloatable(false);
        this.setRollover(isRollover);
        this.setBorder(border);
        for (EnhancedAction action : leftAlignedActions.elements()) {
            if (action instanceof SeparatorAction) {
                this.addSeparator();
                continue;
            }
            if (action == null || !action.showInToolbar()) continue;
            button = new JButton(action);
            if (this.showIcon()) {
                button.setIcon(action.getIconBig());
            }
            button.setHorizontalTextPosition(this.getHorizontalTextPosition(textOrientation));
            button.setVerticalTextPosition(textOrientation);
            if (action.showWithName()) {
                button.setText(action.getText());
            } else {
                button.setText(null);
            }
            button.setMargin(new Insets(2, 6, 2, 6));
            button.setToolTipText(action.getTooltip());
            this.add(button);
            if (!addSeparator) continue;
            this.addSeparator();
        }
        if (rightAlignedActions != null) {
            this.add(Box.createHorizontalGlue());
            for (int i = 0; i < rightAlignedActions.length; ++i) {
                EnhancedAction rightAlignedAction = rightAlignedActions[i];
                if (rightAlignedAction instanceof IRenderableAction) {
                    Font buttonFont;
                    JTextField input = rightAlignedAction._input;
                    input.setColumns(24);
                    JButton button2 = rightAlignedAction._button;
                    button2.setIcon(rightAlignedAction.getIconBig());
                    Color textColor = UIManager.getColor("pesquisaAjuda.textColor");
                    Border textBorder = UIManager.getBorder("TextField.border");
                    Font textFont = UIManager.getFont("TextField.font");
                    if (textColor != null) {
                        input.setForeground(textColor);
                    }
                    if (textBorder != null) {
                        input.setBorder(textBorder);
                    }
                    if (textFont != null) {
                        input.setFont(textFont);
                    }
                    input.setMaximumSize(new Dimension(148, 24));
                    input.setMinimumSize(new Dimension(54, 4));
                    button2.setMargin(new Insets(2, 6, 2, 6));
                    input.setToolTipText(rightAlignedAction.getTooltip());
                    button2.setToolTipText(rightAlignedAction.getTooltip());
                    this.add(input);
                    Color foregroundColor = UIManager.getColor("Button.Toolbar.foreground");
                    if (foregroundColor != null) {
                        button2.setForeground(foregroundColor);
                    }
                    if ((buttonFont = UIManager.getFont("Button.Toolbar.font")) != null) {
                        button2.setFont(buttonFont);
                    }
                    this.add(button2);
                    this.addSeparator();
                    continue;
                }
                if (!rightAlignedAction.showInToolbar()) continue;
                button = new JButton(rightAlignedAction);
                if (this.showIcon()) {
                    button.setIcon(rightAlignedAction.getIconBig());
                }
                button.setHorizontalTextPosition(this.getHorizontalTextPosition(textOrientation));
                button.setVerticalTextPosition(textOrientation);
                if (rightAlignedAction.showWithName()) {
                    button.setText(rightAlignedAction.getText());
                } else {
                    button.setText(null);
                }
                button.setMargin(new Insets(2, 6, 2, 6));
                button.setToolTipText(rightAlignedAction.getTooltip());
                this.add(button);
                if (!addSeparator) continue;
                this.addSeparator();
            }
        }
    }

    protected boolean showIcon() {
        return true;
    }

    protected int getHorizontalTextPosition(int textOrientation) {
        if (textOrientation == 0) {
            return 11;
        }
        return 0;
    }
}

