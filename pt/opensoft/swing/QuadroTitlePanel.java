/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class QuadroTitlePanel
extends JPanel {
    private static final long serialVersionUID = -5103056076179304695L;
    private String number;
    private String title;
    private JLabel numberLabel = new JLabel();
    private JLabel titleLabel = new JLabel();

    public QuadroTitlePanel() {
        this.initializeComponents();
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
        this.numberLabel.setText(number);
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
        this.titleLabel.setText(title);
    }

    protected void initializeComponents() {
        String horizontalAlignment;
        this.setLayout(new BorderLayout());
        this.numberLabel.setText(this.number);
        this.titleLabel.setText(this.title);
        Color numLabelForeground = UIManager.getColor("SubTitleNum.foregroundColor");
        Color numLabelBackground = UIManager.getColor("SubTitleNum.backgroundColor");
        Color descLabelForeground = UIManager.getColor("SubTitleDesc.foregroundColor");
        Color descLabelBackground = UIManager.getColor("SubTitleDesc.backgroundColor");
        if (numLabelBackground != null) {
            this.numberLabel.setBackground(numLabelBackground);
        }
        if (numLabelForeground != null) {
            this.numberLabel.setForeground(numLabelForeground);
        }
        if (descLabelBackground != null) {
            this.titleLabel.setBackground(descLabelBackground);
        }
        if (descLabelForeground != null) {
            this.titleLabel.setForeground(descLabelForeground);
        }
        Font fontNum = UIManager.getFont("SubTitle.fontNum");
        Font fontLabel = UIManager.getFont("SubTitle.fontLabel");
        if (fontNum != null) {
            this.numberLabel.setFont(fontNum);
        }
        if (fontLabel != null) {
            this.titleLabel.setFont(fontLabel);
        }
        String opaque = UIManager.getString("SubTitle.opaque");
        String numOpaque = UIManager.getString("SubTitleNum.opaque");
        String descOpaque = UIManager.getString("SubTitleDesc.opaque");
        if (opaque != null) {
            try {
                this.setOpaque(Boolean.parseBoolean(opaque));
            }
            catch (Exception e) {
                // empty catch block
            }
        }
        if (numOpaque != null) {
            try {
                this.numberLabel.setOpaque(Boolean.parseBoolean(numOpaque));
            }
            catch (Exception e) {
                // empty catch block
            }
        }
        if (descOpaque != null) {
            try {
                this.titleLabel.setOpaque(Boolean.parseBoolean(descOpaque));
            }
            catch (Exception e) {
                // empty catch block
            }
        }
        Border border = UIManager.getBorder("TableHeader.cellBorder");
        String numUseBorder = UIManager.getString("SubTitleNum.useBorder");
        String descUseBorder = UIManager.getString("SubTitleDesc.useBorder");
        Border cellBorder = UIManager.getBorder("TableHeader.cellBorder");
        if (border != null) {
            try {
                if (numUseBorder != null && Boolean.parseBoolean(numUseBorder) && cellBorder != null) {
                    this.numberLabel.setBorder(cellBorder);
                } else {
                    this.numberLabel.setBorder(new EmptyBorder(0, 5, 0, 5));
                }
            }
            catch (Exception e) {
                // empty catch block
            }
            try {
                if (descUseBorder != null && Boolean.parseBoolean(descUseBorder) && cellBorder != null) {
                    this.titleLabel.setBorder(cellBorder);
                } else {
                    this.titleLabel.setBorder(new EmptyBorder(0, 5, 0, 0));
                }
            }
            catch (Exception e) {
                // empty catch block
            }
        }
        if ((horizontalAlignment = UIManager.getString("SubTitleDesc.horizontalAlignment")) != null) {
            try {
                this.titleLabel.setHorizontalAlignment(Integer.parseInt(horizontalAlignment));
            }
            catch (Exception e) {
                // empty catch block
            }
        }
        this.add((Component)this.numberLabel, "West");
        this.numberLabel.setHorizontalTextPosition(0);
        this.add((Component)this.titleLabel, "Center");
    }
}

