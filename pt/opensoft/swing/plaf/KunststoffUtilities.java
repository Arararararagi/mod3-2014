/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.image.ColorModel;
import pt.opensoft.swing.plaf.ColorUIResource2;
import pt.opensoft.swing.plaf.FastGradientPaint;

public class KunststoffUtilities {
    public static Color getTranslucentColor(Color color, int alpha) {
        if (color == null) {
            return null;
        }
        if (alpha == 255) {
            return color;
        }
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha);
    }

    public static Color getTranslucentColorUIResource(Color color, int alpha) {
        if (color == null) {
            return null;
        }
        if (alpha == 255) {
            return color;
        }
        return new ColorUIResource2(color.getRed(), color.getGreen(), color.getBlue(), alpha);
    }

    public static void drawGradient(Graphics g, Color color1, Color color2, Rectangle rect, boolean isVertical) {
        Graphics2D g2D = (Graphics2D)g;
        FastGradientPaint gradient = new FastGradientPaint(color1, color2, isVertical);
        g2D.setPaint(gradient);
        g2D.fill(rect);
    }

    public static void drawGradient(Graphics g, Color color1, Color color2, Rectangle rect, Rectangle rect2, boolean isVertical) {
        if (isVertical) {
            Graphics2D g2D = (Graphics2D)g;
            GradientPaint gradient = new GradientPaint(0.0f, (float)rect.getY(), color1, 0.0f, (float)(rect.getHeight() + rect.getY()), color2);
            g2D.setPaint(gradient);
            g2D.fill(rect);
        } else {
            Graphics2D g2D = (Graphics2D)g;
            GradientPaint gradient = new GradientPaint((float)rect.getX(), 0.0f, color1, (float)(rect.getWidth() + rect.getX()), 0.0f, color2);
            g2D.setPaint(gradient);
            g2D.fill(rect);
        }
    }

    public static boolean isToolkitTrueColor(Component c) {
        int pixelsize = c.getToolkit().getColorModel().getPixelSize();
        return pixelsize >= 24;
    }
}

