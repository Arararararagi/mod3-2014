/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf.pf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;

class PFBumpBuffer {
    static final int IMAGE_SIZE = 64;
    static Dimension imageSize = new Dimension(64, 64);
    transient Image image;
    Color topColor;
    Color shadowColor;
    Color backColor;
    private GraphicsConfiguration gc;

    public PFBumpBuffer(GraphicsConfiguration gc, Color aTopColor, Color aShadowColor, Color aBackColor) {
        this.gc = gc;
        this.topColor = aTopColor;
        this.shadowColor = aShadowColor;
        this.backColor = aBackColor;
        this.createImage();
        this.fillBumpBuffer();
    }

    public boolean hasSameConfiguration(GraphicsConfiguration gc, Color aTopColor, Color aShadowColor, Color aBackColor) {
        if (this.gc != null ? !this.gc.equals(gc) : gc != null) {
            return false;
        }
        return this.topColor.equals(aTopColor) && this.shadowColor.equals(aShadowColor) && this.backColor.equals(aBackColor);
    }

    public Image getImage() {
        return this.image;
    }

    public Dimension getImageSize() {
        return imageSize;
    }

    private void fillBumpBuffer() {
        int y;
        int x;
        Graphics g = this.image.getGraphics();
        g.setColor(this.backColor);
        g.fillRect(0, 0, 64, 64);
        g.setColor(this.topColor);
        for (x = 0; x < 64; x+=4) {
            for (y = 0; y < 64; y+=4) {
                g.drawLine(x, y, x, y);
                g.drawLine(x + 2, y + 2, x + 2, y + 2);
            }
        }
        g.setColor(this.shadowColor);
        for (x = 0; x < 64; x+=4) {
            for (y = 0; y < 64; y+=4) {
                g.drawLine(x + 1, y + 1, x + 1, y + 1);
                g.drawLine(x + 3, y + 3, x + 3, y + 3);
            }
        }
        g.dispose();
    }

    private void createImage() {
        if (this.gc != null) {
            this.image = this.gc.createCompatibleImage(64, 64);
        } else {
            int[] cmap = new int[]{this.backColor.getRGB(), this.topColor.getRGB(), this.shadowColor.getRGB()};
            IndexColorModel icm = new IndexColorModel(8, 3, cmap, 0, false, -1, 0);
            this.image = new BufferedImage(64, 64, 13, icm);
        }
    }
}

