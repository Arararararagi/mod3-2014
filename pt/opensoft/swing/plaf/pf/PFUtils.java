/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf.pf;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import pt.opensoft.swing.plaf.pf.PFLookAndFeel;
import pt.opensoft.util.SimpleLog;

class PFUtils {
    PFUtils() {
    }

    static void drawFlush3DBorder(Graphics g, Rectangle r) {
        PFUtils.drawFlush3DBorder(g, r.x, r.y, r.width, r.height);
    }

    static void drawFlush3DBorder(Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        g.setColor(PFLookAndFeel.getControlDarkShadow());
        g.drawRect(0, 0, w - 2, h - 2);
        g.setColor(PFLookAndFeel.getControlHighlight());
        g.drawRect(1, 1, w - 2, h - 2);
        g.setColor(PFLookAndFeel.getControl());
        g.drawLine(0, h - 1, 1, h - 2);
        g.drawLine(w - 1, 0, w - 2, 1);
        g.translate(- x, - y);
    }

    static void drawOvalFlushBorder(Graphics g, Rectangle r) {
        PFUtils.drawOvalFlushBorder(g, r.x, r.y, r.width, r.height);
    }

    static void drawOvalFlushBorder(Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        g.setColor(PFLookAndFeel.getControlDarkShadow());
        g.drawRoundRect(0, 0, w, h, 11, 11);
        g.translate(- x, - y);
    }

    static void drawPressed3DBorder(Graphics g, Rectangle r) {
        PFUtils.drawPressed3DBorder(g, r.x, r.y, r.width, r.height);
    }

    static void drawDisabledBorder(Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        g.setColor(PFLookAndFeel.getControlShadow());
        g.drawRect(0, 0, w - 1, h - 1);
        g.translate(- x, - y);
    }

    static void drawPressed3DBorder(Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        PFUtils.drawFlush3DBorder(g, 0, 0, w, h);
        g.setColor(PFLookAndFeel.getControlShadow());
        g.drawLine(1, 1, 1, h - 2);
        g.drawLine(1, 1, w - 2, 1);
        g.translate(- x, - y);
    }

    static void drawDark3DBorder(Graphics g, Rectangle r) {
        PFUtils.drawDark3DBorder(g, r.x, r.y, r.width, r.height);
    }

    static void drawDark3DBorder(Graphics g, int x, int y, int w, int h) {
        g.translate(x, y);
        PFUtils.drawFlush3DBorder(g, 0, 0, w, h);
        g.setColor(PFLookAndFeel.getControl());
        g.drawLine(1, 1, 1, h - 2);
        g.drawLine(1, 1, w - 2, 1);
        g.setColor(PFLookAndFeel.getControlShadow());
        g.drawLine(1, h - 2, 1, h - 2);
        g.drawLine(w - 2, 1, w - 2, 1);
        g.translate(- x, - y);
    }

    static void drawButtonBorder(Graphics g, int x, int y, int w, int h, boolean active) {
        if (active) {
            PFUtils.drawActiveButtonBorder(g, x, y, w, h);
        } else {
            PFUtils.drawFlush3DBorder(g, x, y, w, h);
        }
    }

    static void drawActiveButtonBorder(Graphics g, int x, int y, int w, int h) {
        PFUtils.drawFlush3DBorder(g, x, y, w, h);
        g.setColor(PFLookAndFeel.getPrimaryControl());
        g.drawLine(x + 1, y + 1, x + 1, h - 3);
        g.drawLine(x + 1, y + 1, w - 3, x + 1);
        g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
        g.drawLine(x + 2, h - 2, w - 2, h - 2);
        g.drawLine(w - 2, y + 2, w - 2, h - 2);
    }

    static void drawDefaultButtonBorder(Graphics g, int x, int y, int w, int h, boolean active) {
        PFUtils.drawButtonBorder(g, x + 1, y + 1, w - 1, h - 1, active);
        g.translate(x, y);
        g.setColor(PFLookAndFeel.getControlDarkShadow());
        g.drawRect(0, 0, w - 3, h - 3);
        g.drawLine(w - 2, 0, w - 2, 0);
        g.drawLine(0, h - 2, 0, h - 2);
        g.translate(- x, - y);
    }

    static void drawDefaultButtonPressedBorder(Graphics g, int x, int y, int w, int h) {
        PFUtils.drawPressed3DBorder(g, x + 1, y + 1, w - 1, h - 1);
        g.translate(x, y);
        g.setColor(PFLookAndFeel.getControlDarkShadow());
        g.drawRect(0, 0, w - 3, h - 3);
        g.drawLine(w - 2, 0, w - 2, 0);
        g.drawLine(0, h - 2, 0, h - 2);
        g.setColor(PFLookAndFeel.getControl());
        g.drawLine(w - 1, 0, w - 1, 0);
        g.drawLine(0, h - 1, 0, h - 1);
        g.translate(- x, - y);
    }

    static boolean isLeftToRight(Component c) {
        return c.getComponentOrientation().isLeftToRight();
    }

    static int getInt(Object key, int defaultValue) {
        Object value = UIManager.get(key);
        if (value instanceof Integer) {
            return (Integer)value;
        }
        if (value instanceof String) {
            try {
                return Integer.parseInt((String)value);
            }
            catch (NumberFormatException nfe) {
                // empty catch block
            }
        }
        return defaultValue;
    }

    static ImageIcon loadRes(String fich) {
        try {
            return new ImageIcon(Toolkit.getDefaultToolkit().createImage(PFUtils.readStream(PFLookAndFeel.class.getResourceAsStream(fich))));
        }
        catch (Exception ex) {
            SimpleLog.log("N\u00e3o foi possivel carregar o recurso " + fich, ex);
            return null;
        }
    }

    static byte[] readStream(InputStream input) throws IOException {
        int read;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        byte[] buffer = new byte[256];
        while ((read = input.read(buffer, 0, 256)) != -1) {
            bytes.write(buffer, 0, read);
        }
        return bytes.toByteArray();
    }
}

