/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf.pf.theme;

import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.PlasticTheme;
import java.awt.Color;
import java.awt.Font;
import java.security.AccessController;
import java.security.PrivilegedAction;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;

public class PFTheme
extends PlasticTheme {
    private FontDelegate fontDelegate = new FontDelegate();
    protected ColorUIResource CINZENTO1 = new ColorUIResource(233, 240, 240);
    protected ColorUIResource VERDE = new ColorUIResource(181, 202, 28);
    protected ColorUIResource AZUL1 = new ColorUIResource(31, 157, 183);
    protected ColorUIResource AZUL2 = new ColorUIResource(98, 187, 205);
    protected ColorUIResource AZUL3 = new ColorUIResource(241, 248, 251);
    protected ColorUIResource AZUL_CAIXA_DIALOGO = new ColorUIResource(212, 237, 241);
    protected ColorUIResource AZUL_TABS = new ColorUIResource(199, 230, 237);
    protected ColorUIResource AZUL_BOTAO = new ColorUIResource(84, 180, 200);
    protected ColorUIResource REBORDO_JANELAS = new ColorUIResource(215, 227, 227);
    protected ColorUIResource COR_MENUS = new ColorUIResource(0, 116, 140);
    protected ColorUIResource COR_FONTE_PRINCIPAL = new ColorUIResource(15, 81, 95);
    protected ColorUIResource COR_FONTE_TITULO = new ColorUIResource(86, 133, 143);
    protected ColorUIResource COR_FONTE_INPUT = new ColorUIResource(3, 0, 0);
    protected ColorUIResource COR_FONTE_MENU_ITEM_DISABLED = new ColorUIResource(192, 192, 192);
    protected ColorUIResource CINZENTO_SOMBRA = new ColorUIResource(208, 215, 216);
    protected ColorUIResource CINZENTO_SOMBRA_DARKER = new ColorUIResource(178, 173, 144);
    protected ColorUIResource COR_TEXTO_BOTAO = new ColorUIResource(15, 81, 95);
    protected ColorUIResource COR_FUNDO_BOTAO_SELECCIONADO = new ColorUIResource(135, 196, 209);
    protected ColorUIResource COR_FUNDO_BOTAO_SELECCIONADO_MARTELO = new ColorUIResource(168, 255, 218);
    public static final int TAHOMA_REGULAR_10 = 0;
    public static final int TAHOMA_BOLD_10 = 1;
    public static final int TAHOMA_REGULAR_11 = 2;
    public static final int TAHOMA_BOLD_11 = 3;
    public static final int TAHOMA_BOLD_UNDERLINE_11 = 4;
    public static final int TAHOMA_BOLD_12 = 5;
    public static final int TAHOMA_BOLD_14 = 8;
    public static final int VERDANA_REGULAR_10 = 6;
    public static final int TREBUCHETMS_REGULAR_11 = 7;
    public static final int HELVETICA_REGULAR_10 = 9;
    public static final int ARIAL_REGULAR_10 = 10;
    private static final String[] fontNames = new String[]{"Tahoma", "Tahoma", "Tahoma", "Tahoma", "Tahoma", "Tahoma", "Verdana", "Trebuchet ms", "Tahoma", "Helvetica", "Arial"};
    private static final int[] fontStyles = new int[]{0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0};
    private static final int[] fontSizes = new int[]{10, 10, 11, 11, 11, 12, 10, 11, 14, 10, 10};
    private static final String[] defaultNames = new String[]{"swing.plaf.pf.TahomaBold10", "swing.plaf.pf.TahomaBold11", "swing.plaf.pf.TahomaBold12", "swing.plaf.pf.TahomaRegular10", "swing.plaf.pf.TahomaRegular11", "swing.plaf.pf.TahomaBoldUnderline11", "swing.plaf.pf.VerdanaRegular11", "swing.plaf.pf.TrebuchetmsRegular11", "swing.plaf.pf.TahomaBold14", "swing.plaf.pf.HelveticaRegular10", "swing.plaf.pf.ArialRegular10"};

    @Override
    public String getName() {
        return "PF Theme";
    }

    @Override
    protected ColorUIResource getPrimary1() {
        return this.REBORDO_JANELAS;
    }

    @Override
    public ColorUIResource getBlack() {
        return BLACK;
    }

    @Override
    public ColorUIResource getWhite() {
        return WHITE;
    }

    public ColorUIResource getRed() {
        return new ColorUIResource(Color.RED);
    }

    @Override
    protected ColorUIResource getPrimary2() {
        return this.AZUL3;
    }

    @Override
    protected ColorUIResource getPrimary3() {
        return this.AZUL1;
    }

    @Override
    protected ColorUIResource getSecondary1() {
        return this.REBORDO_JANELAS;
    }

    @Override
    protected ColorUIResource getSecondary2() {
        return this.AZUL_TABS;
    }

    @Override
    protected ColorUIResource getSecondary3() {
        return this.AZUL3;
    }

    public ColorUIResource getCorTextoBotao() {
        return this.COR_TEXTO_BOTAO;
    }

    public ColorUIResource getBorderPanelColor() {
        return this.CINZENTO_SOMBRA_DARKER;
    }

    public ColorUIResource getTituloAnexoBackgroundColor() {
        return this.AZUL1;
    }

    public ColorUIResource getTituloSimuladorForegroundColor() {
        return this.COR_MENUS;
    }

    public ColorUIResource getTituloSimuladorBackgroundColor() {
        return this.CINZENTO1;
    }

    public ColorUIResource getTituloErrosForegroundColor() {
        return this.COR_MENUS;
    }

    public ColorUIResource getTituloErrosBackgroundColor() {
        return this.CINZENTO1;
    }

    public ColorUIResource getTituloFacilitadorForegroundColor() {
        return this.COR_MENUS;
    }

    public ColorUIResource getTituloFacilitadorBackgroundColor() {
        return this.CINZENTO1;
    }

    public ColorUIResource getCorBotaoAlerta() {
        return this.AZUL_BOTAO;
    }

    @Override
    public ColorUIResource getMenuItemSelectedBackground() {
        return this.getPrimary2();
    }

    @Override
    public ColorUIResource getMenuItemSelectedForeground() {
        return this.getWhite();
    }

    @Override
    public ColorUIResource getMenuSelectedBackground() {
        return this.AZUL1;
    }

    public ColorUIResource getImpressosHeaderBackgroundColor() {
        return this.VERDE;
    }

    public ColorUIResource getButtonBackgroundColor() {
        return this.COR_FONTE_PRINCIPAL;
    }

    public ColorUIResource getButtonSelectedColor() {
        return this.COR_FUNDO_BOTAO_SELECCIONADO;
    }

    public ColorUIResource getButtonSelectedColor2Martelo() {
        return this.COR_FUNDO_BOTAO_SELECCIONADO_MARTELO;
    }

    @Override
    public ColorUIResource getFocusColor() {
        return PlasticLookAndFeel.useHighContrastFocusColors ? this.AZUL1 : super.getFocusColor();
    }

    @Override
    public ColorUIResource getDesktopColor() {
        return this.AZUL3;
    }

    @Override
    public ColorUIResource getControl() {
        return this.getWhite();
    }

    public ColorUIResource getSubTitleColor() {
        return this.getPrimary3();
    }

    public ColorUIResource getMenuItemDisabledColor() {
        return this.COR_FONTE_MENU_ITEM_DISABLED;
    }

    @Override
    public ColorUIResource getControlShadow() {
        return this.AZUL_TABS;
    }

    @Override
    public ColorUIResource getControlDarkShadow() {
        return this.REBORDO_JANELAS;
    }

    @Override
    public ColorUIResource getControlInfo() {
        return this.AZUL1;
    }

    @Override
    public ColorUIResource getControlHighlight() {
        return this.COR_FONTE_PRINCIPAL;
    }

    @Override
    public ColorUIResource getControlDisabled() {
        return this.CINZENTO_SOMBRA;
    }

    @Override
    public ColorUIResource getPrimaryControl() {
        return this.COR_MENUS;
    }

    @Override
    public ColorUIResource getPrimaryControlShadow() {
        return this.AZUL_TABS;
    }

    @Override
    public ColorUIResource getPrimaryControlDarkShadow() {
        return this.CINZENTO_SOMBRA;
    }

    @Override
    public ColorUIResource getPrimaryControlInfo() {
        return this.getWhite();
    }

    @Override
    public ColorUIResource getPrimaryControlHighlight() {
        return this.CINZENTO_SOMBRA;
    }

    @Override
    public ColorUIResource getSystemTextColor() {
        return this.COR_FONTE_PRINCIPAL;
    }

    @Override
    public ColorUIResource getControlTextColor() {
        return this.COR_FONTE_PRINCIPAL;
    }

    @Override
    public ColorUIResource getInactiveControlTextColor() {
        return this.COR_FONTE_PRINCIPAL;
    }

    @Override
    public ColorUIResource getInactiveSystemTextColor() {
        return this.COR_FONTE_PRINCIPAL;
    }

    @Override
    public ColorUIResource getUserTextColor() {
        return this.COR_FONTE_INPUT;
    }

    @Override
    public ColorUIResource getTextHighlightColor() {
        return this.AZUL1;
    }

    @Override
    public ColorUIResource getHighlightedTextColor() {
        return this.getWhite();
    }

    @Override
    public ColorUIResource getWindowBackground() {
        return this.getWhite();
    }

    @Override
    public ColorUIResource getWindowTitleForeground() {
        return this.getWhite();
    }

    @Override
    public ColorUIResource getWindowTitleBackground() {
        return this.CINZENTO_SOMBRA;
    }

    @Override
    public ColorUIResource getWindowTitleInactiveBackground() {
        return this.CINZENTO_SOMBRA;
    }

    @Override
    public ColorUIResource getWindowTitleInactiveForeground() {
        return this.CINZENTO_SOMBRA;
    }

    @Override
    public ColorUIResource getMenuBackground() {
        return this.CINZENTO1;
    }

    @Override
    public ColorUIResource getMenuForeground() {
        return this.COR_MENUS;
    }

    @Override
    public ColorUIResource getMenuSelectedForeground() {
        return this.getWhite();
    }

    @Override
    public ColorUIResource getMenuDisabledForeground() {
        return this.COR_FONTE_PRINCIPAL;
    }

    public ColorUIResource getMenuItemDisabledForeground() {
        return this.COR_FONTE_MENU_ITEM_DISABLED;
    }

    @Override
    public ColorUIResource getSeparatorBackground() {
        return this.AZUL1;
    }

    @Override
    public ColorUIResource getSeparatorForeground() {
        return this.getWhite();
    }

    @Override
    public ColorUIResource getAcceleratorForeground() {
        return this.COR_FONTE_TITULO;
    }

    @Override
    public ColorUIResource getAcceleratorSelectedForeground() {
        return this.COR_FONTE_TITULO;
    }

    public ColorUIResource getFieldNumberLabelBackground() {
        return this.CINZENTO_SOMBRA;
    }

    public ColorUIResource getCheckBoxDrawColor() {
        return new ColorUIResource(33, 161, 33);
    }

    @Override
    public void addCustomEntriesToTable(UIDefaults table) {
        super.addCustomEntriesToTable(table);
        Object[] uiDefaults = new Object[]{"ScrollBar.maxBumpsWidth", new Integer(30)};
        table.putDefaults(uiDefaults);
    }

    public static String getDefaultFontName(int key) {
        return fontNames[key];
    }

    public static int getDefaultFontSize(int key) {
        return fontSizes[key];
    }

    public static int getDefaultFontStyle(int key) {
        return fontStyles[key];
    }

    static String getDefaultPropertyName(int key) {
        return defaultNames[key];
    }

    public FontUIResource getTahomaBold10() {
        return this.getFont(1);
    }

    public FontUIResource getTahomaRegular10() {
        return this.getFont(0);
    }

    public FontUIResource getTahomaRegular11() {
        return this.getFont(2);
    }

    public FontUIResource getTahomaBold11() {
        return this.getFont(3);
    }

    public FontUIResource getTahomaBold14() {
        return this.getFont(8);
    }

    public FontUIResource getTahomaBoldUnderline11() {
        return this.getFont(4);
    }

    public FontUIResource getTahomaBold12() {
        return this.getFont(5);
    }

    public FontUIResource getVerdanaRegular10() {
        return this.getFont(6);
    }

    public FontUIResource getTrebuchetMS11() {
        return this.getFont(7);
    }

    public FontUIResource getHelvetica10() {
        return this.getFont(9);
    }

    public FontUIResource getArial10() {
        return this.getFont(10);
    }

    private FontUIResource getFont(int key) {
        return this.fontDelegate.getFont(key);
    }

    private static class FontDelegate {
        private static int[] defaultMapping = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        FontUIResource[] fonts = new FontUIResource[11];

        public FontUIResource getFont(int type) {
            if (this.fonts[type = defaultMapping[type]] == null) {
                Font f = this.getPrivilegedFont(type);
                if (f == null) {
                    f = new Font(PFTheme.getDefaultFontName(type), PFTheme.getDefaultFontStyle(type), PFTheme.getDefaultFontSize(type));
                }
                this.fonts[type] = new FontUIResource(f);
            }
            return this.fonts[type];
        }

        protected Font getPrivilegedFont(final int key) {
            return (Font)AccessController.doPrivileged(new PrivilegedAction(){

                public Object run() {
                    return Font.getFont(PFTheme.getDefaultPropertyName(key));
                }
            });
        }

    }

}

