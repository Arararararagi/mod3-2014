/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf.pf;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.text.JTextComponent;
import pt.opensoft.swing.plaf.pf.PFBumps;
import pt.opensoft.swing.plaf.pf.PFLookAndFeel;
import pt.opensoft.swing.plaf.pf.PFUtils;

public class PFBorders {
    private static Border frameNavigationSpacedBorder;
    private static Border buttonBorder;
    private static Border emptyButtonBorder;
    private static Border textBorder;
    private static Border textFieldBorder;
    private static Border comboBoxBorder;
    private static Border toggleButtonBorder;

    public static Border getFrameNavigationSpacedBorder() {
        if (frameNavigationSpacedBorder == null) {
            frameNavigationSpacedBorder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7), BorderFactory.createLineBorder(PFLookAndFeel.getControlDarkShadow()));
        }
        return frameNavigationSpacedBorder;
    }

    public static Border getFrameNavigationSpacedBorder(Color color) {
        if (frameNavigationSpacedBorder == null) {
            frameNavigationSpacedBorder = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7), BorderFactory.createLineBorder(color));
        }
        return frameNavigationSpacedBorder;
    }

    public static Border getButtonBorder() {
        if (buttonBorder == null) {
            buttonBorder = new BorderUIResource.CompoundBorderUIResource(new BasicBorders.MarginBorder(), new BasicBorders.MarginBorder());
        }
        return buttonBorder;
    }

    public static Border getEmptyButtonBorder() {
        if (emptyButtonBorder == null) {
            emptyButtonBorder = new EmptyButtonBorder();
        }
        return emptyButtonBorder;
    }

    public static Border getTextBorder() {
        if (textBorder == null) {
            textBorder = new BorderUIResource.CompoundBorderUIResource(new Flush3DBorder(), new BasicBorders.MarginBorder());
        }
        return textBorder;
    }

    public static Border getTextFieldBorder() {
        if (textFieldBorder == null) {
            textFieldBorder = new BorderUIResource.CompoundBorderUIResource(new ShadowBorder(), BorderFactory.createEmptyBorder(2, 2, 2, 2));
        }
        return textFieldBorder;
    }

    public static Border getComboBoxBorder() {
        if (comboBoxBorder == null) {
            comboBoxBorder = new BorderUIResource.CompoundBorderUIResource(new ShadowBorder(), new BasicBorders.MarginBorder());
        }
        return comboBoxBorder;
    }

    public static Border getToggleButtonBorder() {
        if (toggleButtonBorder == null) {
            toggleButtonBorder = new BorderUIResource.CompoundBorderUIResource(new ToggleButtonBorder(), new BasicBorders.MarginBorder());
        }
        return toggleButtonBorder;
    }

    public static Border getDesktopIconBorder() {
        return new BorderUIResource.CompoundBorderUIResource(new LineBorder(PFLookAndFeel.getControlDarkShadow(), 1), new MatteBorder(2, 2, 1, 2, PFLookAndFeel.getControl()));
    }

    public static class ButtonBorder
    extends AbstractBorder
    implements UIResource {
        protected static Insets borderInsets = new Insets(3, 3, 3, 3);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            AbstractButton button = (AbstractButton)c;
            ButtonModel model = button.getModel();
            if (model.isEnabled()) {
                boolean isDefault;
                boolean isPressed = model.isPressed() && model.isArmed();
                boolean bl = isDefault = button instanceof JButton && ((JButton)button).isDefaultButton();
                if (isPressed && isDefault) {
                    PFUtils.drawDefaultButtonPressedBorder(g, x, y, w, h);
                } else if (isPressed) {
                    PFUtils.drawPressed3DBorder(g, x, y, w, h);
                } else if (isDefault) {
                    PFUtils.drawDefaultButtonBorder(g, x, y, w, h, false);
                } else {
                    PFUtils.drawButtonBorder(g, x, y, w, h, false);
                }
            } else {
                PFUtils.drawDisabledBorder(g, x, y, w - 1, h - 1);
            }
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return borderInsets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = ButtonBorder.borderInsets.top;
            newInsets.left = ButtonBorder.borderInsets.left;
            newInsets.bottom = ButtonBorder.borderInsets.bottom;
            newInsets.right = ButtonBorder.borderInsets.right;
            return newInsets;
        }
    }

    static class DialogBorder
    extends AbstractBorder
    implements UIResource {
        private static final Insets insets = new Insets(5, 5, 5, 5);
        private static final int corner = 14;

        DialogBorder() {
        }

        protected Color getActiveBackground() {
            return PFLookAndFeel.getPrimaryControlDarkShadow();
        }

        protected Color getActiveHighlight() {
            return PFLookAndFeel.getPrimaryControlShadow();
        }

        protected Color getActiveShadow() {
            return PFLookAndFeel.getPrimaryControlInfo();
        }

        protected Color getInactiveBackground() {
            return PFLookAndFeel.getControlDarkShadow();
        }

        protected Color getInactiveHighlight() {
            return PFLookAndFeel.getControlShadow();
        }

        protected Color getInactiveShadow() {
            return PFLookAndFeel.getControlInfo();
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            Color background;
            Color shadow;
            Color highlight;
            Window window = SwingUtilities.getWindowAncestor(c);
            if (window != null && window.isActive()) {
                background = this.getActiveBackground();
                highlight = this.getActiveHighlight();
                shadow = this.getActiveShadow();
            } else {
                background = this.getInactiveBackground();
                highlight = this.getInactiveHighlight();
                shadow = this.getInactiveShadow();
            }
            g.setColor(background);
            g.drawLine(x + 1, y + 0, x + w - 2, y + 0);
            g.drawLine(x + 0, y + 1, x + 0, y + h - 2);
            g.drawLine(x + w - 1, y + 1, x + w - 1, y + h - 2);
            g.drawLine(x + 1, y + h - 1, x + w - 2, y + h - 1);
            for (int i = 1; i < 5; ++i) {
                g.drawRect(x + i, y + i, w - i * 2 - 1, h - i * 2 - 1);
            }
            if (window instanceof Dialog && ((Dialog)window).isResizable()) {
                g.setColor(highlight);
                g.drawLine(15, 3, w - 14, 3);
                g.drawLine(3, 15, 3, h - 14);
                g.drawLine(w - 2, 15, w - 2, h - 14);
                g.drawLine(15, h - 2, w - 14, h - 2);
                g.setColor(shadow);
                g.drawLine(14, 2, w - 14 - 1, 2);
                g.drawLine(2, 14, 2, h - 14 - 1);
                g.drawLine(w - 3, 14, w - 3, h - 14 - 1);
                g.drawLine(14, h - 3, w - 14 - 1, h - 3);
            }
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = DialogBorder.insets.top;
            newInsets.left = DialogBorder.insets.left;
            newInsets.bottom = DialogBorder.insets.bottom;
            newInsets.right = DialogBorder.insets.right;
            return newInsets;
        }
    }

    public static class EmptyButtonBorder
    extends AbstractBorder
    implements UIResource {
        protected static Insets borderInsets = new Insets(3, 3, 3, 3);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            AbstractButton button = (AbstractButton)c;
            ButtonModel model = button.getModel();
            Border emptyBorder = BorderFactory.createEmptyBorder(x, y, w, h);
            if (model.isEnabled()) {
                boolean isDefault;
                boolean isPressed = model.isPressed() && model.isArmed();
                boolean bl = isDefault = button instanceof JButton && ((JButton)button).isDefaultButton();
                if (isPressed && isDefault) {
                    PFUtils.drawDefaultButtonPressedBorder(g, x, y, w, h);
                } else if (isPressed) {
                    PFUtils.drawPressed3DBorder(g, x, y, w, h);
                } else if (isDefault) {
                    emptyBorder.paintBorder(c, g, x, y, w, h);
                } else if (model.isRollover()) {
                    PFUtils.drawPressed3DBorder(g, x, y, w, h);
                } else {
                    emptyBorder.paintBorder(c, g, x, y, w, h);
                }
            }
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return borderInsets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = EmptyButtonBorder.borderInsets.top;
            newInsets.left = EmptyButtonBorder.borderInsets.left;
            newInsets.bottom = EmptyButtonBorder.borderInsets.bottom;
            newInsets.right = EmptyButtonBorder.borderInsets.right;
            return newInsets;
        }
    }

    static class ErrorDialogBorder
    extends DialogBorder
    implements UIResource {
        ErrorDialogBorder() {
        }

        @Override
        protected Color getActiveBackground() {
            return UIManager.getColor("OptionPane.errorDialog.border.background");
        }
    }

    public static class Flush3DBorder
    extends AbstractBorder
    implements UIResource {
        private static final Insets insets = new Insets(2, 2, 2, 2);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            if (c.isEnabled()) {
                PFUtils.drawFlush3DBorder(g, x, y, w, h);
            } else {
                PFUtils.drawDisabledBorder(g, x, y, w, h);
            }
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = Flush3DBorder.insets.top;
            newInsets.left = Flush3DBorder.insets.left;
            newInsets.bottom = Flush3DBorder.insets.bottom;
            newInsets.right = Flush3DBorder.insets.right;
            return newInsets;
        }
    }

    public static class FrameBorder
    extends AbstractBorder
    implements UIResource {
        private static final Insets insets = new Insets(5, 5, 5, 5);
        private static final int corner = 14;

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            ColorUIResource highlight;
            ColorUIResource background;
            ColorUIResource shadow;
            Window window = SwingUtilities.getWindowAncestor(c);
            if (window != null && window.isActive()) {
                background = PFLookAndFeel.getPrimaryControlDarkShadow();
                highlight = PFLookAndFeel.getPrimaryControlShadow();
                shadow = PFLookAndFeel.getPrimaryControlInfo();
            } else {
                background = PFLookAndFeel.getControlDarkShadow();
                highlight = PFLookAndFeel.getControlShadow();
                shadow = PFLookAndFeel.getControlInfo();
            }
            g.setColor(background);
            g.drawLine(x + 1, y + 0, x + w - 2, y + 0);
            g.drawLine(x + 0, y + 1, x + 0, y + h - 2);
            g.drawLine(x + w - 1, y + 1, x + w - 1, y + h - 2);
            g.drawLine(x + 1, y + h - 1, x + w - 2, y + h - 1);
            for (int i = 1; i < 5; ++i) {
                g.drawRect(x + i, y + i, w - i * 2 - 1, h - i * 2 - 1);
            }
            if (window instanceof Frame && ((Frame)window).isResizable()) {
                g.setColor(highlight);
                g.drawLine(15, 3, w - 14, 3);
                g.drawLine(3, 15, 3, h - 14);
                g.drawLine(w - 2, 15, w - 2, h - 14);
                g.drawLine(15, h - 2, w - 14, h - 2);
                g.setColor(shadow);
                g.drawLine(14, 2, w - 14 - 1, 2);
                g.drawLine(2, 14, 2, h - 14 - 1);
                g.drawLine(w - 3, 14, w - 3, h - 14 - 1);
                g.drawLine(14, h - 3, w - 14 - 1, h - 3);
            }
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = FrameBorder.insets.top;
            newInsets.left = FrameBorder.insets.left;
            newInsets.bottom = FrameBorder.insets.bottom;
            newInsets.right = FrameBorder.insets.right;
            return newInsets;
        }
    }

    public static class InternalFrameBorder
    extends AbstractBorder
    implements UIResource {
        private static final Insets insets = new Insets(5, 5, 5, 5);
        private static final int corner = 14;

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            ColorUIResource background;
            ColorUIResource highlight;
            ColorUIResource shadow;
            if (c instanceof JInternalFrame && ((JInternalFrame)c).isSelected()) {
                background = PFLookAndFeel.getPrimaryControlDarkShadow();
                highlight = PFLookAndFeel.getPrimaryControlShadow();
                shadow = PFLookAndFeel.getPrimaryControlInfo();
            } else {
                background = PFLookAndFeel.getControlDarkShadow();
                highlight = PFLookAndFeel.getControlShadow();
                shadow = PFLookAndFeel.getControlInfo();
            }
            g.setColor(background);
            g.drawLine(1, 0, w - 2, 0);
            g.drawLine(0, 1, 0, h - 2);
            g.drawLine(w - 1, 1, w - 1, h - 2);
            g.drawLine(1, h - 1, w - 2, h - 1);
            for (int i = 1; i < 5; ++i) {
                g.drawRect(x + i, y + i, w - i * 2 - 1, h - i * 2 - 1);
            }
            if (c instanceof JInternalFrame && ((JInternalFrame)c).isResizable()) {
                g.setColor(highlight);
                g.drawLine(15, 3, w - 14, 3);
                g.drawLine(3, 15, 3, h - 14);
                g.drawLine(w - 2, 15, w - 2, h - 14);
                g.drawLine(15, h - 2, w - 14, h - 2);
                g.setColor(shadow);
                g.drawLine(14, 2, w - 14 - 1, 2);
                g.drawLine(2, 14, 2, h - 14 - 1);
                g.drawLine(w - 3, 14, w - 3, h - 14 - 1);
                g.drawLine(14, h - 3, w - 14 - 1, h - 3);
            }
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = InternalFrameBorder.insets.top;
            newInsets.left = InternalFrameBorder.insets.left;
            newInsets.bottom = InternalFrameBorder.insets.bottom;
            newInsets.right = InternalFrameBorder.insets.right;
            return newInsets;
        }
    }

    public static class MenuBarBorder
    extends AbstractBorder
    implements UIResource {
        protected static Insets borderInsets = new Insets(1, 0, 1, 0);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getControlShadow());
            g.drawLine(0, h - 1, w, h - 1);
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return borderInsets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = MenuBarBorder.borderInsets.top;
            newInsets.left = MenuBarBorder.borderInsets.left;
            newInsets.bottom = MenuBarBorder.borderInsets.bottom;
            newInsets.right = MenuBarBorder.borderInsets.right;
            return newInsets;
        }
    }

    public static class MenuItemBorder
    extends AbstractBorder
    implements UIResource {
        protected static Insets borderInsets = new Insets(2, 2, 2, 2);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            JMenuItem b = (JMenuItem)c;
            ButtonModel model = b.getModel();
            g.translate(x, y);
            if (c.getParent() instanceof JMenuBar) {
                if (model.isArmed() || model.isSelected()) {
                    g.setColor(PFLookAndFeel.getControlDarkShadow());
                    g.drawLine(0, 0, w - 2, 0);
                    g.drawLine(0, 0, 0, h - 1);
                    g.drawLine(w - 2, 2, w - 2, h - 1);
                    g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
                    g.drawLine(w - 1, 1, w - 1, h - 1);
                    g.setColor(PFLookAndFeel.getMenuBackground());
                    g.drawLine(w - 1, 0, w - 1, 0);
                }
            } else if (model.isArmed() || c instanceof JMenu && model.isSelected()) {
                g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
                g.drawLine(0, 0, w - 1, 0);
                g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
                g.drawLine(0, h - 1, w - 1, h - 1);
            } else {
                g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
                g.drawLine(0, 0, 0, h - 1);
            }
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return borderInsets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = MenuItemBorder.borderInsets.top;
            newInsets.left = MenuItemBorder.borderInsets.left;
            newInsets.bottom = MenuItemBorder.borderInsets.bottom;
            newInsets.right = MenuItemBorder.borderInsets.right;
            return newInsets;
        }
    }

    public static class OptionDialogBorder
    extends AbstractBorder
    implements UIResource {
        private static final Insets insets = new Insets(3, 3, 3, 3);
        int titleHeight = 0;

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            Color borderColor2;
            Object obj;
            g.translate(x, y);
            int messageType = -1;
            if (c instanceof JInternalFrame && (obj = ((JInternalFrame)c).getClientProperty("JInternalFrame.messageType")) != null && obj instanceof Integer) {
                messageType = (Integer)obj;
            }
            switch (messageType) {
                Color borderColor2;
                case 0: {
                    borderColor2 = UIManager.getColor("OptionPane.errorDialog.border.background");
                    break;
                }
                case 3: {
                    borderColor2 = UIManager.getColor("OptionPane.questionDialog.border.background");
                    break;
                }
                case 2: {
                    borderColor2 = UIManager.getColor("OptionPane.warningDialog.border.background");
                    break;
                }
                default: {
                    borderColor2 = PFLookAndFeel.getPrimaryControlDarkShadow();
                }
            }
            g.setColor(borderColor2);
            g.drawLine(1, 0, w - 2, 0);
            g.drawLine(0, 1, 0, h - 2);
            g.drawLine(w - 1, 1, w - 1, h - 2);
            g.drawLine(1, h - 1, w - 2, h - 1);
            for (int i = 1; i < 3; ++i) {
                g.drawRect(i, i, w - i * 2 - 1, h - i * 2 - 1);
            }
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = OptionDialogBorder.insets.top;
            newInsets.left = OptionDialogBorder.insets.left;
            newInsets.bottom = OptionDialogBorder.insets.bottom;
            newInsets.right = OptionDialogBorder.insets.right;
            return newInsets;
        }
    }

    public static class PaddedLineBorder
    extends LineBorder
    implements UIResource {
        private static final Insets insets = new Insets(20, 20, 20, 20);
        private static final int corner = 14;

        public PaddedLineBorder() {
            super(PFLookAndFeel.getPrimaryControlDarkShadow());
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = PaddedLineBorder.insets.top;
            newInsets.left = PaddedLineBorder.insets.left;
            newInsets.bottom = PaddedLineBorder.insets.bottom;
            newInsets.right = PaddedLineBorder.insets.right;
            return newInsets;
        }
    }

    public static class PaletteBorder
    extends AbstractBorder
    implements UIResource {
        private static final Insets insets = new Insets(1, 1, 1, 1);
        int titleHeight = 0;

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
            g.drawLine(0, 1, 0, h - 2);
            g.drawLine(1, h - 1, w - 2, h - 1);
            g.drawLine(w - 1, 1, w - 1, h - 2);
            g.drawLine(1, 0, w - 2, 0);
            g.drawRect(1, 1, w - 3, h - 3);
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = PaletteBorder.insets.top;
            newInsets.left = PaletteBorder.insets.left;
            newInsets.bottom = PaletteBorder.insets.bottom;
            newInsets.right = PaletteBorder.insets.right;
            return newInsets;
        }
    }

    public static class PopupMenuBorder
    extends AbstractBorder
    implements UIResource {
        protected static Insets borderInsets = new Insets(3, 1, 2, 1);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
            g.drawRect(0, 0, w - 1, h - 1);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.drawLine(1, 1, w - 2, 1);
            g.drawLine(1, 2, 1, 2);
            g.drawLine(1, h - 2, 1, h - 2);
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return borderInsets;
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = PopupMenuBorder.borderInsets.top;
            newInsets.left = PopupMenuBorder.borderInsets.left;
            newInsets.bottom = PopupMenuBorder.borderInsets.bottom;
            newInsets.right = PopupMenuBorder.borderInsets.right;
            return newInsets;
        }
    }

    static class QuestionDialogBorder
    extends DialogBorder
    implements UIResource {
        QuestionDialogBorder() {
        }

        @Override
        protected Color getActiveBackground() {
            return UIManager.getColor("OptionPane.questionDialog.border.background");
        }
    }

    public static class RolloverButtonBorder
    extends ButtonBorder {
        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            AbstractButton b = (AbstractButton)c;
            ButtonModel model = b.getModel();
            if (model.isRollover() && (!model.isPressed() || model.isArmed())) {
                super.paintBorder(c, g, x, y, w, h);
            }
        }
    }

    static class RolloverMarginBorder
    extends EmptyBorder {
        public RolloverMarginBorder() {
            super(3, 3, 3, 3);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return this.getBorderInsets(c, new Insets(0, 0, 0, 0));
        }

        @Override
        public Insets getBorderInsets(Component c, Insets insets) {
            Insets margin = null;
            if (c instanceof AbstractButton) {
                margin = ((AbstractButton)c).getMargin();
            }
            if (margin == null || margin instanceof UIResource) {
                insets.left = this.left;
                insets.top = this.top;
                insets.right = this.right;
                insets.bottom = this.bottom;
            } else {
                insets.left = margin.left;
                insets.top = margin.top;
                insets.right = margin.right;
                insets.bottom = margin.bottom;
            }
            return insets;
        }
    }

    public static class ScrollPaneBorder
    extends AbstractBorder
    implements UIResource {
        private static final Insets insets = new Insets(1, 1, 2, 2);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            JScrollPane scroll = (JScrollPane)c;
            JViewport colHeader = scroll.getColumnHeader();
            int colHeaderHeight = 0;
            if (colHeader != null) {
                colHeaderHeight = colHeader.getHeight();
            }
            JViewport rowHeader = scroll.getRowHeader();
            int rowHeaderWidth = 0;
            if (rowHeader != null) {
                rowHeaderWidth = rowHeader.getWidth();
            }
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getControlDarkShadow());
            g.drawRect(0, 0, w - 2, h - 2);
            g.setColor(PFLookAndFeel.getControlHighlight());
            g.drawLine(w - 1, 1, w - 1, h - 1);
            g.drawLine(1, h - 1, w - 1, h - 1);
            g.setColor(PFLookAndFeel.getControl());
            g.drawLine(w - 2, 2 + colHeaderHeight, w - 2, 2 + colHeaderHeight);
            g.drawLine(1 + rowHeaderWidth, h - 2, 1 + rowHeaderWidth, h - 2);
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }
    }

    public static class ShadowBorder
    extends AbstractBorder {
        private static final Insets INSETS = new Insets(1, 1, 3, 3);

        @Override
        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            Color shadow = UIManager.getColor("controlShadow");
            if (shadow == null) {
                shadow = Color.GRAY;
            }
            Color lightShadow = new Color(shadow.getRed(), shadow.getGreen(), shadow.getBlue(), 170);
            Color lighterShadow = new Color(shadow.getRed(), shadow.getGreen(), shadow.getBlue(), 70);
            g.translate(x, y);
            g.setColor(shadow);
            g.fillRect(0, 0, w - 3, 1);
            g.fillRect(0, 0, 1, h - 3);
            g.fillRect(w - 3, 1, 1, h - 3);
            g.fillRect(1, h - 3, w - 3, 1);
            g.setColor(lightShadow);
            g.fillRect(w - 3, 0, 1, 1);
            g.fillRect(0, h - 3, 1, 1);
            g.fillRect(w - 2, 1, 1, h - 3);
            g.fillRect(1, h - 2, w - 3, 1);
            g.setColor(lighterShadow);
            g.fillRect(w - 2, 0, 1, 1);
            g.fillRect(0, h - 2, 1, 1);
            g.fillRect(w - 2, h - 2, 1, 1);
            g.fillRect(w - 1, 1, 1, h - 2);
            g.fillRect(1, h - 1, w - 2, 1);
            g.translate(- x, - y);
        }
    }

    public static class TableBorder
    extends AbstractBorder {
        protected Insets editorBorderInsets = new Insets(2, 2, 2, 0);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return this.editorBorderInsets;
        }
    }

    public static class TableHeaderBorder
    extends AbstractBorder {
        protected Insets editorBorderInsets = new Insets(2, 2, 2, 0);

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getControlDarkShadow());
            g.drawLine(1, h - 1, w - 1, h - 1);
            g.drawLine(w - 1, h - 1, w - 1, 1);
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return this.editorBorderInsets;
        }
    }

    public static class TextFieldBorder
    extends Flush3DBorder {
        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            if (!(c instanceof JTextComponent)) {
                if (c.isEnabled()) {
                    PFUtils.drawFlush3DBorder(g, x, y, w, h);
                } else {
                    PFUtils.drawDisabledBorder(g, x, y, w, h);
                }
                return;
            }
            if (c.isEnabled() && ((JTextComponent)c).isEditable()) {
                PFUtils.drawFlush3DBorder(g, x, y, w, h);
            } else {
                PFUtils.drawDisabledBorder(g, x, y, w, h);
            }
        }
    }

    public static class ToggleButtonBorder
    extends ButtonBorder {
        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            AbstractButton button = (AbstractButton)c;
            ButtonModel model = button.getModel();
            if (!c.isEnabled()) {
                PFUtils.drawDisabledBorder(g, x, y, w - 1, h - 1);
            } else if (model.isPressed() && model.isArmed()) {
                PFUtils.drawPressed3DBorder(g, x, y, w, h);
            } else if (model.isSelected()) {
                PFUtils.drawDark3DBorder(g, x, y, w, h);
            } else {
                PFUtils.drawFlush3DBorder(g, x, y, w, h);
            }
        }
    }

    public static class ToolBarBorder
    extends AbstractBorder
    implements UIResource,
    SwingConstants {
        protected PFBumps bumps = new PFBumps(10, 10, PFLookAndFeel.getControlHighlight(), PFLookAndFeel.getControlDarkShadow(), PFLookAndFeel.getMenuBackground());

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.translate(x, y);
            if (((JToolBar)c).isFloatable()) {
                if (((JToolBar)c).getOrientation() == 0) {
                    this.bumps.setBumpArea(10, c.getSize().height - 4);
                    if (PFUtils.isLeftToRight(c)) {
                        this.bumps.paintIcon(c, g, 2, 2);
                    } else {
                        this.bumps.paintIcon(c, g, c.getBounds().width - 12, 2);
                    }
                } else {
                    this.bumps.setBumpArea(c.getSize().width - 4, 10);
                    this.bumps.paintIcon(c, g, 2, 2);
                }
            }
            g.translate(- x, - y);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return this.getBorderInsets(c, new Insets(0, 0, 0, 0));
        }

        @Override
        public Insets getBorderInsets(Component c, Insets newInsets) {
            Insets margin;
            newInsets.right = 2;
            newInsets.bottom = 2;
            newInsets.left = 2;
            newInsets.top = 2;
            if (((JToolBar)c).isFloatable()) {
                if (((JToolBar)c).getOrientation() == 0) {
                    if (c.getComponentOrientation().isLeftToRight()) {
                        newInsets.left = 16;
                    } else {
                        newInsets.right = 16;
                    }
                } else {
                    newInsets.top = 16;
                }
            }
            if ((margin = ((JToolBar)c).getMargin()) != null) {
                newInsets.left+=margin.left;
                newInsets.top+=margin.top;
                newInsets.right+=margin.right;
                newInsets.bottom+=margin.bottom;
            }
            return newInsets;
        }
    }

    static class WarningDialogBorder
    extends DialogBorder
    implements UIResource {
        WarningDialogBorder() {
        }

        @Override
        protected Color getActiveBackground() {
            return UIManager.getColor("OptionPane.warningDialog.border.background");
        }
    }

}

