/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf.pf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.UIResource;
import pt.opensoft.swing.plaf.pf.PFBumps;
import pt.opensoft.swing.plaf.pf.PFLookAndFeel;
import pt.opensoft.swing.plaf.pf.PFUtils;

public class PFIconFactory
implements Serializable {
    private static Icon fileChooserDetailViewIcon;
    private static Icon fileChooserHomeFolderIcon;
    private static Icon fileChooserListViewIcon;
    private static Icon fileChooserNewFolderIcon;
    private static Icon fileChooserUpFolderIcon;
    private static Icon internalFrameDefaultMenuIcon;
    private static Icon radioButtonIcon;
    private static Icon treeComputerIcon;
    private static Icon treeFloppyDriveIcon;
    private static Icon treeHardDriveIcon;
    private static Icon menuArrowIcon;
    private static Icon menuItemArrowIcon;
    private static Icon checkBoxMenuItemIcon;
    private static Icon radioButtonMenuItemIcon;
    private static Icon checkBoxIcon;
    public static final boolean DARK = false;
    public static final boolean LIGHT = true;
    private static final Dimension folderIcon16Size;
    private static final Dimension fileIcon16Size;
    private static final Dimension treeControlSize;
    private static final Dimension menuArrowIconSize;
    private static final Dimension menuCheckIconSize;

    public static Icon getFileChooserDetailViewIcon() {
        if (fileChooserDetailViewIcon == null) {
            fileChooserDetailViewIcon = new FileChooserDetailViewIcon();
        }
        return fileChooserDetailViewIcon;
    }

    public static Icon getFileChooserHomeFolderIcon() {
        if (fileChooserHomeFolderIcon == null) {
            fileChooserHomeFolderIcon = new FileChooserHomeFolderIcon();
        }
        return fileChooserHomeFolderIcon;
    }

    public static Icon getFileChooserListViewIcon() {
        if (fileChooserListViewIcon == null) {
            fileChooserListViewIcon = new FileChooserListViewIcon();
        }
        return fileChooserListViewIcon;
    }

    public static Icon getFileChooserNewFolderIcon() {
        if (fileChooserNewFolderIcon == null) {
            fileChooserNewFolderIcon = new FileChooserNewFolderIcon();
        }
        return fileChooserNewFolderIcon;
    }

    public static Icon getFileChooserUpFolderIcon() {
        if (fileChooserUpFolderIcon == null) {
            fileChooserUpFolderIcon = new FileChooserUpFolderIcon();
        }
        return fileChooserUpFolderIcon;
    }

    public static Icon getInternalFrameAltMaximizeIcon(int size) {
        return new InternalFrameAltMaximizeIcon(size);
    }

    public static Icon getInternalFrameCloseIcon(int size) {
        return new InternalFrameCloseIcon(size);
    }

    public static Icon getInternalFrameDefaultMenuIcon() {
        if (internalFrameDefaultMenuIcon == null) {
            internalFrameDefaultMenuIcon = new InternalFrameDefaultMenuIcon();
        }
        return internalFrameDefaultMenuIcon;
    }

    public static Icon getInternalFrameMaximizeIcon(int size) {
        return new InternalFrameMaximizeIcon(size);
    }

    public static Icon getInternalFrameMinimizeIcon(int size) {
        return new InternalFrameMinimizeIcon(size);
    }

    public static Icon getRadioButtonIcon() {
        if (radioButtonIcon == null) {
            radioButtonIcon = new RadioButtonIcon();
        }
        return radioButtonIcon;
    }

    public static Icon getCheckBoxIcon() {
        if (checkBoxIcon == null) {
            checkBoxIcon = new CheckBoxIcon();
        }
        return checkBoxIcon;
    }

    public static Icon getTreeComputerIcon() {
        if (treeComputerIcon == null) {
            treeComputerIcon = new TreeComputerIcon();
        }
        return treeComputerIcon;
    }

    public static Icon getTreeFloppyDriveIcon() {
        if (treeFloppyDriveIcon == null) {
            treeFloppyDriveIcon = new TreeFloppyDriveIcon();
        }
        return treeFloppyDriveIcon;
    }

    public static Icon getTreeFolderIcon() {
        return new TreeFolderIcon();
    }

    public static Icon getTreeHardDriveIcon() {
        if (treeHardDriveIcon == null) {
            treeHardDriveIcon = new TreeHardDriveIcon();
        }
        return treeHardDriveIcon;
    }

    public static Icon getTreeLeafIcon() {
        return new TreeLeafIcon();
    }

    public static Icon getTreeControlIcon(boolean isCollapsed) {
        return new TreeControlIcon(isCollapsed);
    }

    public static Icon getMenuArrowIcon() {
        if (menuArrowIcon == null) {
            menuArrowIcon = new MenuArrowIcon();
        }
        return menuArrowIcon;
    }

    public static Icon getMenuItemCheckIcon() {
        return null;
    }

    public static Icon getMenuItemArrowIcon() {
        if (menuItemArrowIcon == null) {
            menuItemArrowIcon = new MenuItemArrowIcon();
        }
        return menuItemArrowIcon;
    }

    public static Icon getCheckBoxMenuItemIcon() {
        if (checkBoxMenuItemIcon == null) {
            checkBoxMenuItemIcon = new CheckBoxMenuItemIcon();
        }
        return checkBoxMenuItemIcon;
    }

    public static Icon getRadioButtonMenuItemIcon() {
        if (radioButtonMenuItemIcon == null) {
            radioButtonMenuItemIcon = new RadioButtonMenuItemIcon();
        }
        return radioButtonMenuItemIcon;
    }

    public static Icon getHorizontalSliderThumbIcon() {
        return new HorizontalSliderThumbIcon();
    }

    public static Icon getVerticalSliderThumbIcon() {
        return new VerticalSliderThumbIcon();
    }

    static /* synthetic */ Dimension access$1300() {
        return folderIcon16Size;
    }

    static /* synthetic */ Dimension access$1400() {
        return fileIcon16Size;
    }

    static /* synthetic */ Dimension access$1500() {
        return treeControlSize;
    }

    static /* synthetic */ Dimension access$1600() {
        return menuArrowIconSize;
    }

    static /* synthetic */ Dimension access$1700() {
        return menuCheckIconSize;
    }

    static {
        folderIcon16Size = new Dimension(16, 16);
        fileIcon16Size = new Dimension(16, 16);
        treeControlSize = new Dimension(18, 18);
        menuArrowIconSize = new Dimension(4, 8);
        menuCheckIconSize = new Dimension(10, 10);
    }

    private static class CheckBoxIcon
    implements Icon,
    UIResource,
    Serializable {
        private int w = 12;
        private int h = 12;

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JCheckBox b = (JCheckBox)c;
            ButtonModel model = b.getModel();
            boolean isEnabled = model.isEnabled();
            boolean isOn = model.isSelected() || model.isPressed();
            c.setBackground(UIManager.getColor("CheckBox.background"));
            Icon icono = UIManager.getIcon("CheckBox.iconBase");
            icono.paintIcon(c, g, x, y);
            if (!isEnabled) {
                g.setColor(UIManager.getColor("CheckBox.disabled"));
                g.fillRect(x + 2, y + 2, 8, 8);
            } else if (isOn) {
                g.fillRect(x, y, 12, 12);
            }
            if (model.isRollover()) {
                g.setColor(UIManager.getColor("CheckBox.focus"));
                g.fillRect(x + 2, y + 2, 8, 8);
            }
            if (isOn) {
                icono = UIManager.getIcon("CheckBox.iconTick");
                icono.paintIcon(c, g, x, y);
            }
        }

        @Override
        public int getIconWidth() {
            return this.w;
        }

        @Override
        public int getIconHeight() {
            return this.h;
        }
    }

    private static class CheckBoxMenuItemIcon
    implements Icon,
    UIResource,
    Serializable {
        private CheckBoxMenuItemIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JMenuItem b = (JMenuItem)c;
            ButtonModel model = b.getModel();
            boolean isSelected = model.isSelected();
            boolean isEnabled = model.isEnabled();
            boolean isPressed = model.isPressed();
            boolean isArmed = model.isArmed();
            g.translate(x, y);
            if (isEnabled) {
                if (isPressed || isArmed) {
                    g.setColor(PFLookAndFeel.getControlInfo());
                    g.drawLine(0, 0, 8, 0);
                    g.drawLine(0, 0, 0, 8);
                    g.drawLine(8, 2, 8, 8);
                    g.drawLine(2, 8, 8, 8);
                    g.setColor(PFLookAndFeel.getPrimaryControl());
                    g.drawLine(1, 1, 7, 1);
                    g.drawLine(1, 1, 1, 7);
                    g.drawLine(9, 1, 9, 9);
                    g.drawLine(1, 9, 9, 9);
                } else {
                    g.setColor(PFLookAndFeel.getControlDarkShadow());
                    g.drawLine(0, 0, 8, 0);
                    g.drawLine(0, 0, 0, 8);
                    g.drawLine(8, 2, 8, 8);
                    g.drawLine(2, 8, 8, 8);
                    g.setColor(PFLookAndFeel.getControlHighlight());
                    g.drawLine(1, 1, 7, 1);
                    g.drawLine(1, 1, 1, 7);
                    g.drawLine(9, 1, 9, 9);
                    g.drawLine(1, 9, 9, 9);
                }
            } else {
                g.setColor(PFLookAndFeel.getMenuDisabledForeground());
                g.drawRect(0, 0, 8, 8);
            }
            if (isSelected) {
                if (isEnabled) {
                    if (model.isArmed() || c instanceof JMenu && model.isSelected()) {
                        g.setColor(PFLookAndFeel.getMenuSelectedForeground());
                    } else {
                        g.setColor(b.getForeground());
                    }
                } else {
                    g.setColor(PFLookAndFeel.getMenuDisabledForeground());
                }
                g.drawLine(2, 2, 2, 6);
                g.drawLine(3, 2, 3, 6);
                g.drawLine(4, 4, 8, 0);
                g.drawLine(4, 5, 9, 0);
            }
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return PFIconFactory.access$1700().width;
        }

        @Override
        public int getIconHeight() {
            return PFIconFactory.access$1700().height;
        }
    }

    private static class FileChooserDetailViewIcon
    implements Icon,
    UIResource,
    Serializable {
        private FileChooserDetailViewIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(2, 2, 5, 2);
            g.drawLine(2, 3, 2, 7);
            g.drawLine(3, 7, 6, 7);
            g.drawLine(6, 6, 6, 3);
            g.drawLine(2, 10, 5, 10);
            g.drawLine(2, 11, 2, 15);
            g.drawLine(3, 15, 6, 15);
            g.drawLine(6, 14, 6, 11);
            g.drawLine(8, 5, 15, 5);
            g.drawLine(8, 13, 15, 13);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.drawRect(3, 3, 2, 3);
            g.drawRect(3, 11, 2, 3);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.drawLine(4, 4, 4, 5);
            g.drawLine(4, 12, 4, 13);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 18;
        }

        @Override
        public int getIconHeight() {
            return 18;
        }
    }

    private static class FileChooserHomeFolderIcon
    implements Icon,
    UIResource,
    Serializable {
        private FileChooserHomeFolderIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(8, 1, 1, 8);
            g.drawLine(8, 1, 15, 8);
            g.drawLine(11, 2, 11, 3);
            g.drawLine(12, 2, 12, 4);
            g.drawLine(3, 7, 3, 15);
            g.drawLine(13, 7, 13, 15);
            g.drawLine(4, 15, 12, 15);
            g.drawLine(6, 9, 6, 14);
            g.drawLine(10, 9, 10, 14);
            g.drawLine(7, 9, 9, 9);
            g.setColor(PFLookAndFeel.getControlDarkShadow());
            g.fillRect(8, 2, 1, 1);
            g.fillRect(7, 3, 3, 1);
            g.fillRect(6, 4, 5, 1);
            g.fillRect(5, 5, 7, 1);
            g.fillRect(4, 6, 9, 2);
            g.drawLine(9, 12, 9, 12);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.drawLine(4, 8, 12, 8);
            g.fillRect(4, 9, 2, 6);
            g.fillRect(11, 9, 2, 6);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 18;
        }

        @Override
        public int getIconHeight() {
            return 18;
        }
    }

    private static class FileChooserListViewIcon
    implements Icon,
    UIResource,
    Serializable {
        private FileChooserListViewIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(2, 2, 5, 2);
            g.drawLine(2, 3, 2, 7);
            g.drawLine(3, 7, 6, 7);
            g.drawLine(6, 6, 6, 3);
            g.drawLine(10, 2, 13, 2);
            g.drawLine(10, 3, 10, 7);
            g.drawLine(11, 7, 14, 7);
            g.drawLine(14, 6, 14, 3);
            g.drawLine(2, 10, 5, 10);
            g.drawLine(2, 11, 2, 15);
            g.drawLine(3, 15, 6, 15);
            g.drawLine(6, 14, 6, 11);
            g.drawLine(10, 10, 13, 10);
            g.drawLine(10, 11, 10, 15);
            g.drawLine(11, 15, 14, 15);
            g.drawLine(14, 14, 14, 11);
            g.drawLine(8, 5, 8, 5);
            g.drawLine(16, 5, 16, 5);
            g.drawLine(8, 13, 8, 13);
            g.drawLine(16, 13, 16, 13);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.drawRect(3, 3, 2, 3);
            g.drawRect(11, 3, 2, 3);
            g.drawRect(3, 11, 2, 3);
            g.drawRect(11, 11, 2, 3);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.drawLine(4, 4, 4, 5);
            g.drawLine(12, 4, 12, 5);
            g.drawLine(4, 12, 4, 13);
            g.drawLine(12, 12, 12, 13);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 18;
        }

        @Override
        public int getIconHeight() {
            return 18;
        }
    }

    private static class FileChooserNewFolderIcon
    implements Icon,
    UIResource,
    Serializable {
        private FileChooserNewFolderIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.fillRect(3, 5, 12, 9);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(1, 6, 1, 14);
            g.drawLine(2, 14, 15, 14);
            g.drawLine(15, 13, 15, 5);
            g.drawLine(2, 5, 9, 5);
            g.drawLine(10, 6, 14, 6);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.drawLine(2, 6, 2, 13);
            g.drawLine(3, 6, 9, 6);
            g.drawLine(10, 7, 14, 7);
            g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
            g.drawLine(11, 3, 15, 3);
            g.drawLine(10, 4, 15, 4);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 18;
        }

        @Override
        public int getIconHeight() {
            return 18;
        }
    }

    private static class FileChooserUpFolderIcon
    implements Icon,
    UIResource,
    Serializable {
        private FileChooserUpFolderIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.fillRect(3, 5, 12, 9);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(1, 6, 1, 14);
            g.drawLine(2, 14, 15, 14);
            g.drawLine(15, 13, 15, 5);
            g.drawLine(2, 5, 9, 5);
            g.drawLine(10, 6, 14, 6);
            g.drawLine(8, 13, 8, 16);
            g.drawLine(8, 9, 8, 9);
            g.drawLine(7, 10, 9, 10);
            g.drawLine(6, 11, 10, 11);
            g.drawLine(5, 12, 11, 12);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.drawLine(2, 6, 2, 13);
            g.drawLine(3, 6, 9, 6);
            g.drawLine(10, 7, 14, 7);
            g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
            g.drawLine(11, 3, 15, 3);
            g.drawLine(10, 4, 15, 4);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 18;
        }

        @Override
        public int getIconHeight() {
            return 18;
        }
    }

    public static class FileIcon16
    implements Icon,
    Serializable {
        ImageCacher imageCacher;

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Image image;
            GraphicsConfiguration gc = c.getGraphicsConfiguration();
            if (this.imageCacher == null) {
                this.imageCacher = new ImageCacher();
            }
            if ((image = this.imageCacher.getImage(gc)) == null) {
                image = gc != null ? gc.createCompatibleImage(this.getIconWidth(), this.getIconHeight(), 2) : new BufferedImage(this.getIconWidth(), this.getIconHeight(), 2);
                Graphics imageG = image.getGraphics();
                this.paintMe(imageG);
                imageG.dispose();
                this.imageCacher.cacheImage(image, gc);
            }
            g.drawImage(image, x, y + this.getShift(), null);
        }

        private void paintMe(Graphics g) {
            int right = PFIconFactory.access$1400().width - 1;
            int bottom = PFIconFactory.access$1400().height - 1;
            g.setColor(PFLookAndFeel.getWindowBackground());
            g.fillRect(4, 2, 9, 12);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(2, 0, 2, bottom);
            g.drawLine(2, 0, right - 4, 0);
            g.drawLine(2, bottom, right - 1, bottom);
            g.drawLine(right - 1, 6, right - 1, bottom);
            g.drawLine(right - 6, 2, right - 2, 6);
            g.drawLine(right - 5, 1, right - 4, 1);
            g.drawLine(right - 3, 2, right - 3, 3);
            g.drawLine(right - 2, 4, right - 2, 5);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.drawLine(3, 1, 3, bottom - 1);
            g.drawLine(3, 1, right - 6, 1);
            g.drawLine(right - 2, 7, right - 2, bottom - 1);
            g.drawLine(right - 5, 2, right - 3, 4);
            g.drawLine(3, bottom - 1, right - 2, bottom - 1);
        }

        public int getShift() {
            return 0;
        }

        public int getAdditionalHeight() {
            return 0;
        }

        @Override
        public int getIconWidth() {
            return PFIconFactory.access$1400().width;
        }

        @Override
        public int getIconHeight() {
            return PFIconFactory.access$1400().height + this.getAdditionalHeight();
        }
    }

    public static class FolderIcon16
    implements Icon,
    Serializable {
        ImageCacher imageCacher;

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Image image;
            GraphicsConfiguration gc = c.getGraphicsConfiguration();
            if (this.imageCacher == null) {
                this.imageCacher = new ImageCacher();
            }
            if ((image = this.imageCacher.getImage(gc)) == null) {
                image = gc != null ? gc.createCompatibleImage(this.getIconWidth(), this.getIconHeight(), 2) : new BufferedImage(this.getIconWidth(), this.getIconHeight(), 2);
                Graphics imageG = image.getGraphics();
                this.paintMe(imageG);
                imageG.dispose();
                this.imageCacher.cacheImage(image, gc);
            }
            g.drawImage(image, x, y + this.getShift(), null);
        }

        private void paintMe(Graphics g) {
            int right = PFIconFactory.access$1300().width - 1;
            int bottom = PFIconFactory.access$1300().height - 1;
            g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
            g.drawLine(right - 5, 3, right, 3);
            g.drawLine(right - 6, 4, right, 4);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.fillRect(2, 7, 13, 8);
            g.setColor(PFLookAndFeel.getPrimaryControlShadow());
            g.drawLine(right - 6, 5, right - 1, 5);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(0, 6, 0, bottom);
            g.drawLine(1, 5, right - 7, 5);
            g.drawLine(right - 6, 6, right - 1, 6);
            g.drawLine(right, 5, right, bottom);
            g.drawLine(0, bottom, right, bottom);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.drawLine(1, 6, 1, bottom - 1);
            g.drawLine(1, 6, right - 7, 6);
            g.drawLine(right - 6, 7, right - 1, 7);
        }

        public int getShift() {
            return 0;
        }

        public int getAdditionalHeight() {
            return 0;
        }

        @Override
        public int getIconWidth() {
            return PFIconFactory.access$1300().width;
        }

        @Override
        public int getIconHeight() {
            return PFIconFactory.access$1300().height + this.getAdditionalHeight();
        }
    }

    private static class HorizontalSliderThumbIcon
    implements Icon,
    Serializable,
    UIResource {
        protected static PFBumps controlBumps;
        protected static PFBumps primaryBumps;

        public HorizontalSliderThumbIcon() {
            controlBumps = new PFBumps(10, 6, PFLookAndFeel.getControlHighlight(), PFLookAndFeel.getControlInfo(), PFLookAndFeel.getControl());
            primaryBumps = new PFBumps(10, 6, PFLookAndFeel.getPrimaryControl(), PFLookAndFeel.getPrimaryControlDarkShadow(), PFLookAndFeel.getPrimaryControlShadow());
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JSlider slider = (JSlider)c;
            g.translate(x, y);
            if (slider.hasFocus()) {
                g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            } else {
                g.setColor(slider.isEnabled() ? PFLookAndFeel.getPrimaryControlInfo() : PFLookAndFeel.getControlDarkShadow());
            }
            g.drawLine(1, 0, 13, 0);
            g.drawLine(0, 1, 0, 8);
            g.drawLine(14, 1, 14, 8);
            g.drawLine(1, 9, 7, 15);
            g.drawLine(7, 15, 14, 8);
            if (slider.hasFocus()) {
                g.setColor(c.getForeground());
            } else {
                g.setColor(PFLookAndFeel.getControl());
            }
            g.fillRect(1, 1, 13, 8);
            g.drawLine(2, 9, 12, 9);
            g.drawLine(3, 10, 11, 10);
            g.drawLine(4, 11, 10, 11);
            g.drawLine(5, 12, 9, 12);
            g.drawLine(6, 13, 8, 13);
            g.drawLine(7, 14, 7, 14);
            if (slider.isEnabled()) {
                if (slider.hasFocus()) {
                    primaryBumps.paintIcon(c, g, 2, 2);
                } else {
                    controlBumps.paintIcon(c, g, 2, 2);
                }
            }
            if (slider.isEnabled()) {
                g.setColor(slider.hasFocus() ? PFLookAndFeel.getPrimaryControl() : PFLookAndFeel.getControlHighlight());
                g.drawLine(1, 1, 13, 1);
                g.drawLine(1, 1, 1, 8);
            }
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 15;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }
    }

    static class ImageCacher {
        Vector images = new Vector(1, 1);
        ImageGcPair currentImageGcPair;

        ImageCacher() {
        }

        Image getImage(GraphicsConfiguration newGC) {
            if (!(this.currentImageGcPair != null && this.currentImageGcPair.hasSameConfiguration(newGC))) {
                Enumeration elements = this.images.elements();
                while (elements.hasMoreElements()) {
                    ImageGcPair imgGcPair = (ImageGcPair)elements.nextElement();
                    if (!imgGcPair.hasSameConfiguration(newGC)) continue;
                    this.currentImageGcPair = imgGcPair;
                    return imgGcPair.image;
                }
                return null;
            }
            return this.currentImageGcPair.image;
        }

        void cacheImage(Image image, GraphicsConfiguration gc) {
            ImageGcPair imgGcPair = new ImageGcPair(image, gc);
            this.images.addElement(imgGcPair);
            this.currentImageGcPair = imgGcPair;
        }

        class ImageGcPair {
            Image image;
            GraphicsConfiguration gc;

            ImageGcPair(Image image, GraphicsConfiguration gc) {
                this.image = image;
                this.gc = gc;
            }

            boolean hasSameConfiguration(GraphicsConfiguration newGC) {
                return newGC != null && newGC.equals(this.gc) || newGC == null && this.gc == null;
            }
        }

    }

    private static class InternalFrameAltMaximizeIcon
    implements Icon,
    UIResource,
    Serializable {
        int iconSize = 16;

        public InternalFrameAltMaximizeIcon(int size) {
            this.iconSize = size;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JButton parentButton = (JButton)c;
            ButtonModel buttonModel = parentButton.getModel();
            ColorUIResource backgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource internalBackgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource mainItemColor = PFLookAndFeel.getPrimaryControlDarkShadow();
            ColorUIResource darkHighlightColor = PFLookAndFeel.getBlack();
            ColorUIResource ulLightHighlightColor = PFLookAndFeel.getWhite();
            ColorUIResource lrLightHighlightColor = PFLookAndFeel.getWhite();
            if (parentButton.getClientProperty("paintActive") != Boolean.TRUE) {
                internalBackgroundColor = backgroundColor = PFLookAndFeel.getControl();
                mainItemColor = PFLookAndFeel.getControlDarkShadow();
                if (buttonModel.isPressed() && buttonModel.isArmed()) {
                    ulLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getControlShadow();
                    mainItemColor = darkHighlightColor;
                }
            } else if (buttonModel.isPressed() && buttonModel.isArmed()) {
                ulLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getPrimaryControlShadow();
                mainItemColor = darkHighlightColor;
            }
            g.translate(x, y);
            g.setColor(backgroundColor);
            g.fillRect(0, 0, this.iconSize, this.iconSize);
            g.setColor(internalBackgroundColor);
            g.fillRect(3, 6, this.iconSize - 9, this.iconSize - 9);
            g.setColor(darkHighlightColor);
            g.drawRect(1, 5, this.iconSize - 8, this.iconSize - 8);
            g.drawLine(1, this.iconSize - 2, 1, this.iconSize - 2);
            g.setColor(lrLightHighlightColor);
            g.drawRect(2, 6, this.iconSize - 7, this.iconSize - 7);
            g.setColor(ulLightHighlightColor);
            g.drawRect(3, 7, this.iconSize - 9, this.iconSize - 9);
            g.setColor(mainItemColor);
            g.drawRect(2, 6, this.iconSize - 8, this.iconSize - 8);
            g.setColor(ulLightHighlightColor);
            g.drawLine(this.iconSize - 6, 8, this.iconSize - 6, 8);
            g.drawLine(this.iconSize - 9, 6, this.iconSize - 7, 8);
            g.setColor(mainItemColor);
            g.drawLine(3, this.iconSize - 3, 3, this.iconSize - 3);
            g.setColor(darkHighlightColor);
            g.drawLine(this.iconSize - 6, 9, this.iconSize - 6, 9);
            g.setColor(backgroundColor);
            g.drawLine(this.iconSize - 9, 5, this.iconSize - 9, 5);
            g.setColor(mainItemColor);
            g.fillRect(this.iconSize - 7, 3, 3, 5);
            g.drawLine(this.iconSize - 6, 5, this.iconSize - 3, 2);
            g.drawLine(this.iconSize - 6, 6, this.iconSize - 2, 2);
            g.drawLine(this.iconSize - 6, 7, this.iconSize - 3, 7);
            g.setColor(darkHighlightColor);
            g.drawLine(this.iconSize - 8, 2, this.iconSize - 7, 2);
            g.drawLine(this.iconSize - 8, 3, this.iconSize - 8, 7);
            g.drawLine(this.iconSize - 6, 4, this.iconSize - 3, 1);
            g.drawLine(this.iconSize - 4, 6, this.iconSize - 3, 6);
            g.setColor(lrLightHighlightColor);
            g.drawLine(this.iconSize - 6, 3, this.iconSize - 6, 3);
            g.drawLine(this.iconSize - 4, 5, this.iconSize - 2, 3);
            g.drawLine(this.iconSize - 4, 8, this.iconSize - 3, 8);
            g.drawLine(this.iconSize - 2, 8, this.iconSize - 2, 7);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return this.iconSize;
        }

        @Override
        public int getIconHeight() {
            return this.iconSize;
        }
    }

    private static class InternalFrameCloseIcon
    implements Icon,
    UIResource,
    Serializable {
        int iconSize = 16;

        public InternalFrameCloseIcon(int size) {
            this.iconSize = size;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JButton parentButton = (JButton)c;
            ButtonModel buttonModel = parentButton.getModel();
            ColorUIResource backgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource internalBackgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource mainItemColor = PFLookAndFeel.getPrimaryControlDarkShadow();
            ColorUIResource darkHighlightColor = PFLookAndFeel.getBlack();
            ColorUIResource xLightHighlightColor = PFLookAndFeel.getWhite();
            ColorUIResource boxLightHighlightColor = PFLookAndFeel.getWhite();
            if (parentButton.getClientProperty("paintActive") != Boolean.TRUE) {
                internalBackgroundColor = backgroundColor = PFLookAndFeel.getControl();
                mainItemColor = PFLookAndFeel.getControlDarkShadow();
                if (buttonModel.isPressed() && buttonModel.isArmed()) {
                    xLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getControlShadow();
                    mainItemColor = darkHighlightColor;
                }
            } else if (buttonModel.isPressed() && buttonModel.isArmed()) {
                xLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getPrimaryControlShadow();
                mainItemColor = darkHighlightColor;
            }
            int oneHalf = this.iconSize / 2;
            g.translate(x, y);
            g.setColor(backgroundColor);
            g.fillRect(0, 0, this.iconSize, this.iconSize);
            g.setColor(internalBackgroundColor);
            g.fillRect(3, 3, this.iconSize - 6, this.iconSize - 6);
            g.setColor(darkHighlightColor);
            g.drawRect(1, 1, this.iconSize - 3, this.iconSize - 3);
            g.drawRect(2, 2, this.iconSize - 5, this.iconSize - 5);
            g.setColor(boxLightHighlightColor);
            g.drawRect(2, 2, this.iconSize - 3, this.iconSize - 3);
            g.setColor(mainItemColor);
            g.drawRect(2, 2, this.iconSize - 4, this.iconSize - 4);
            g.drawLine(3, this.iconSize - 3, 3, this.iconSize - 3);
            g.drawLine(this.iconSize - 3, 3, this.iconSize - 3, 3);
            g.setColor(darkHighlightColor);
            g.drawLine(4, 5, 5, 4);
            g.drawLine(4, this.iconSize - 6, this.iconSize - 6, 4);
            g.setColor(xLightHighlightColor);
            g.drawLine(6, this.iconSize - 5, this.iconSize - 5, 6);
            g.drawLine(oneHalf, oneHalf + 2, oneHalf + 2, oneHalf);
            g.drawLine(this.iconSize - 5, this.iconSize - 5, this.iconSize - 4, this.iconSize - 5);
            g.drawLine(this.iconSize - 5, this.iconSize - 4, this.iconSize - 5, this.iconSize - 4);
            g.setColor(mainItemColor);
            g.drawLine(5, 5, this.iconSize - 6, this.iconSize - 6);
            g.drawLine(6, 5, this.iconSize - 5, this.iconSize - 6);
            g.drawLine(5, 6, this.iconSize - 6, this.iconSize - 5);
            g.drawLine(5, this.iconSize - 5, this.iconSize - 5, 5);
            g.drawLine(5, this.iconSize - 6, this.iconSize - 6, 5);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return this.iconSize;
        }

        @Override
        public int getIconHeight() {
            return this.iconSize;
        }
    }

    private static class InternalFrameDefaultMenuIcon
    implements Icon,
    UIResource,
    Serializable {
        private InternalFrameDefaultMenuIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            ColorUIResource windowBodyColor = PFLookAndFeel.getWindowBackground();
            ColorUIResource titleColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource edgeColor = PFLookAndFeel.getPrimaryControlDarkShadow();
            g.translate(x, y);
            g.setColor(titleColor);
            g.fillRect(0, 0, 16, 16);
            g.setColor(windowBodyColor);
            g.fillRect(2, 6, 13, 9);
            g.drawLine(2, 2, 2, 2);
            g.drawLine(5, 2, 5, 2);
            g.drawLine(8, 2, 8, 2);
            g.drawLine(11, 2, 11, 2);
            g.setColor(edgeColor);
            g.drawRect(1, 1, 13, 13);
            g.drawLine(1, 0, 14, 0);
            g.drawLine(15, 1, 15, 14);
            g.drawLine(1, 15, 14, 15);
            g.drawLine(0, 1, 0, 14);
            g.drawLine(2, 5, 13, 5);
            g.drawLine(3, 3, 3, 3);
            g.drawLine(6, 3, 6, 3);
            g.drawLine(9, 3, 9, 3);
            g.drawLine(12, 3, 12, 3);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 16;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }
    }

    private static class InternalFrameMaximizeIcon
    implements Icon,
    UIResource,
    Serializable {
        protected int iconSize = 16;

        public InternalFrameMaximizeIcon(int size) {
            this.iconSize = size;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JButton parentButton = (JButton)c;
            ButtonModel buttonModel = parentButton.getModel();
            ColorUIResource backgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource internalBackgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource mainItemColor = PFLookAndFeel.getPrimaryControlDarkShadow();
            ColorUIResource darkHighlightColor = PFLookAndFeel.getBlack();
            ColorUIResource ulLightHighlightColor = PFLookAndFeel.getWhite();
            ColorUIResource lrLightHighlightColor = PFLookAndFeel.getWhite();
            if (parentButton.getClientProperty("paintActive") != Boolean.TRUE) {
                internalBackgroundColor = backgroundColor = PFLookAndFeel.getControl();
                mainItemColor = PFLookAndFeel.getControlDarkShadow();
                if (buttonModel.isPressed() && buttonModel.isArmed()) {
                    ulLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getControlShadow();
                    mainItemColor = darkHighlightColor;
                }
            } else if (buttonModel.isPressed() && buttonModel.isArmed()) {
                ulLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getPrimaryControlShadow();
                mainItemColor = darkHighlightColor;
            }
            g.translate(x, y);
            g.setColor(backgroundColor);
            g.fillRect(0, 0, this.iconSize, this.iconSize);
            g.setColor(internalBackgroundColor);
            g.fillRect(3, 7, this.iconSize - 10, this.iconSize - 10);
            g.setColor(ulLightHighlightColor);
            g.drawRect(3, 7, this.iconSize - 10, this.iconSize - 10);
            g.setColor(lrLightHighlightColor);
            g.drawRect(2, 6, this.iconSize - 7, this.iconSize - 7);
            g.setColor(darkHighlightColor);
            g.drawRect(1, 5, this.iconSize - 7, this.iconSize - 7);
            g.drawRect(2, 6, this.iconSize - 9, this.iconSize - 9);
            g.setColor(mainItemColor);
            g.drawRect(2, 6, this.iconSize - 8, this.iconSize - 8);
            g.setColor(darkHighlightColor);
            g.drawLine(3, this.iconSize - 5, this.iconSize - 9, 7);
            g.drawLine(this.iconSize - 6, 4, this.iconSize - 5, 3);
            g.drawLine(this.iconSize - 7, 1, this.iconSize - 7, 2);
            g.drawLine(this.iconSize - 6, 1, this.iconSize - 2, 1);
            g.setColor(ulLightHighlightColor);
            g.drawLine(5, this.iconSize - 4, this.iconSize - 8, 9);
            g.setColor(lrLightHighlightColor);
            g.drawLine(this.iconSize - 6, 3, this.iconSize - 4, 5);
            g.drawLine(this.iconSize - 4, 5, this.iconSize - 4, 6);
            g.drawLine(this.iconSize - 2, 7, this.iconSize - 1, 7);
            g.drawLine(this.iconSize - 1, 2, this.iconSize - 1, 6);
            g.setColor(mainItemColor);
            g.drawLine(3, this.iconSize - 4, this.iconSize - 3, 2);
            g.drawLine(3, this.iconSize - 3, this.iconSize - 2, 2);
            g.drawLine(4, this.iconSize - 3, 5, this.iconSize - 3);
            g.drawLine(this.iconSize - 7, 8, this.iconSize - 7, 9);
            g.drawLine(this.iconSize - 6, 2, this.iconSize - 4, 2);
            g.drawRect(this.iconSize - 3, 3, 1, 3);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return this.iconSize;
        }

        @Override
        public int getIconHeight() {
            return this.iconSize;
        }
    }

    private static class InternalFrameMinimizeIcon
    implements Icon,
    UIResource,
    Serializable {
        int iconSize = 16;

        public InternalFrameMinimizeIcon(int size) {
            this.iconSize = size;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JButton parentButton = (JButton)c;
            ButtonModel buttonModel = parentButton.getModel();
            ColorUIResource backgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource internalBackgroundColor = PFLookAndFeel.getPrimaryControl();
            ColorUIResource mainItemColor = PFLookAndFeel.getPrimaryControlDarkShadow();
            ColorUIResource darkHighlightColor = PFLookAndFeel.getBlack();
            ColorUIResource ulLightHighlightColor = PFLookAndFeel.getWhite();
            ColorUIResource lrLightHighlightColor = PFLookAndFeel.getWhite();
            if (parentButton.getClientProperty("paintActive") != Boolean.TRUE) {
                internalBackgroundColor = backgroundColor = PFLookAndFeel.getControl();
                mainItemColor = PFLookAndFeel.getControlDarkShadow();
                if (buttonModel.isPressed() && buttonModel.isArmed()) {
                    ulLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getControlShadow();
                    mainItemColor = darkHighlightColor;
                }
            } else if (buttonModel.isPressed() && buttonModel.isArmed()) {
                ulLightHighlightColor = internalBackgroundColor = PFLookAndFeel.getPrimaryControlShadow();
                mainItemColor = darkHighlightColor;
            }
            g.translate(x, y);
            g.setColor(backgroundColor);
            g.fillRect(0, 0, this.iconSize, this.iconSize);
            g.setColor(internalBackgroundColor);
            g.fillRect(4, 11, this.iconSize - 13, this.iconSize - 13);
            g.setColor(lrLightHighlightColor);
            g.drawRect(2, 10, this.iconSize - 10, this.iconSize - 11);
            g.setColor(ulLightHighlightColor);
            g.drawRect(3, 10, this.iconSize - 12, this.iconSize - 12);
            g.setColor(darkHighlightColor);
            g.drawRect(1, 8, this.iconSize - 10, this.iconSize - 10);
            g.drawRect(2, 9, this.iconSize - 12, this.iconSize - 12);
            g.setColor(mainItemColor);
            g.drawRect(2, 9, this.iconSize - 11, this.iconSize - 11);
            g.drawLine(this.iconSize - 10, 10, this.iconSize - 10, 10);
            g.drawLine(3, this.iconSize - 3, 3, this.iconSize - 3);
            g.setColor(mainItemColor);
            g.fillRect(this.iconSize - 7, 3, 3, 5);
            g.drawLine(this.iconSize - 6, 5, this.iconSize - 3, 2);
            g.drawLine(this.iconSize - 6, 6, this.iconSize - 2, 2);
            g.drawLine(this.iconSize - 6, 7, this.iconSize - 3, 7);
            g.setColor(darkHighlightColor);
            g.drawLine(this.iconSize - 8, 2, this.iconSize - 7, 2);
            g.drawLine(this.iconSize - 8, 3, this.iconSize - 8, 7);
            g.drawLine(this.iconSize - 6, 4, this.iconSize - 3, 1);
            g.drawLine(this.iconSize - 4, 6, this.iconSize - 3, 6);
            g.setColor(lrLightHighlightColor);
            g.drawLine(this.iconSize - 6, 3, this.iconSize - 6, 3);
            g.drawLine(this.iconSize - 4, 5, this.iconSize - 2, 3);
            g.drawLine(this.iconSize - 7, 8, this.iconSize - 3, 8);
            g.drawLine(this.iconSize - 2, 8, this.iconSize - 2, 7);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return this.iconSize;
        }

        @Override
        public int getIconHeight() {
            return this.iconSize;
        }
    }

    private static class MenuArrowIcon
    implements Icon,
    UIResource,
    Serializable {
        private MenuArrowIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JMenuItem b = (JMenuItem)c;
            ButtonModel model = b.getModel();
            g.translate(x, y);
            if (!model.isEnabled()) {
                g.setColor(PFLookAndFeel.getMenuDisabledForeground());
            } else if (model.isArmed() || c instanceof JMenu && model.isSelected()) {
                g.setColor(PFLookAndFeel.getMenuSelectedForeground());
            } else {
                g.setColor(b.getForeground());
            }
            if (PFUtils.isLeftToRight(b)) {
                g.drawLine(0, 0, 0, 7);
                g.drawLine(1, 1, 1, 6);
                g.drawLine(2, 2, 2, 5);
                g.drawLine(3, 3, 3, 4);
            } else {
                g.drawLine(4, 0, 4, 7);
                g.drawLine(3, 1, 3, 6);
                g.drawLine(2, 2, 2, 5);
                g.drawLine(1, 3, 1, 4);
            }
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return PFIconFactory.access$1600().width;
        }

        @Override
        public int getIconHeight() {
            return PFIconFactory.access$1600().height;
        }
    }

    private static class MenuItemArrowIcon
    implements Icon,
    UIResource,
    Serializable {
        private MenuItemArrowIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
        }

        @Override
        public int getIconWidth() {
            return PFIconFactory.access$1600().width;
        }

        @Override
        public int getIconHeight() {
            return PFIconFactory.access$1600().height;
        }
    }

    public static class PaletteCloseIcon
    implements Icon,
    UIResource,
    Serializable {
        int iconSize = 7;

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JButton parentButton = (JButton)c;
            ButtonModel buttonModel = parentButton.getModel();
            ColorUIResource highlight = PFLookAndFeel.getPrimaryControlHighlight();
            ColorUIResource shadow = PFLookAndFeel.getPrimaryControlInfo();
            ColorUIResource back = buttonModel.isPressed() && buttonModel.isArmed() ? shadow : PFLookAndFeel.getPrimaryControlDarkShadow();
            g.translate(x, y);
            g.setColor(back);
            g.drawLine(0, 1, 5, 6);
            g.drawLine(1, 0, 6, 5);
            g.drawLine(1, 1, 6, 6);
            g.drawLine(6, 1, 1, 6);
            g.drawLine(5, 0, 0, 5);
            g.drawLine(5, 1, 1, 5);
            g.setColor(highlight);
            g.drawLine(6, 2, 5, 3);
            g.drawLine(2, 6, 3, 5);
            g.drawLine(6, 6, 6, 6);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return this.iconSize;
        }

        @Override
        public int getIconHeight() {
            return this.iconSize;
        }
    }

    private static class RadioButtonIcon
    implements Icon,
    UIResource,
    Serializable {
        private int w = 12;
        private int h = 12;

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JRadioButton b = (JRadioButton)c;
            ButtonModel model = b.getModel();
            boolean isEnabled = model.isEnabled();
            boolean isOn = model.isSelected() || model.isPressed();
            c.setBackground(UIManager.getColor("RadioButton.background"));
            Icon icono = UIManager.getIcon("RadioButton.iconBase");
            icono.paintIcon(c, g, x, y);
            if (!isEnabled) {
                g.setColor(UIManager.getColor("RadioButton.disabled"));
                g.fillOval(x + 2, y + 2, 8, 8);
            } else if (isOn) {
                g.fillOval(x, y, 12, 12);
            }
            if (model.isRollover()) {
                g.setColor(UIManager.getColor("RadioButton.focus"));
                g.fillOval(x + 2, y + 2, 8, 8);
            }
            if (isOn) {
                icono = UIManager.getIcon("RadioButton.iconTick");
                icono.paintIcon(c, g, x, y);
            }
        }

        @Override
        public int getIconWidth() {
            return this.w;
        }

        @Override
        public int getIconHeight() {
            return this.h;
        }
    }

    private static class RadioButtonMenuItemIcon
    implements Icon,
    UIResource,
    Serializable {
        private RadioButtonMenuItemIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            JMenuItem b = (JMenuItem)c;
            ButtonModel model = b.getModel();
            boolean isSelected = model.isSelected();
            boolean isEnabled = model.isEnabled();
            boolean isPressed = model.isPressed();
            boolean isArmed = model.isArmed();
            g.translate(x, y);
            if (isEnabled) {
                if (isPressed || isArmed) {
                    g.setColor(PFLookAndFeel.getPrimaryControl());
                    g.drawLine(3, 1, 8, 1);
                    g.drawLine(2, 9, 7, 9);
                    g.drawLine(1, 3, 1, 8);
                    g.drawLine(9, 2, 9, 7);
                    g.drawLine(2, 2, 2, 2);
                    g.drawLine(8, 8, 8, 8);
                    g.setColor(PFLookAndFeel.getControlInfo());
                    g.drawLine(2, 0, 6, 0);
                    g.drawLine(2, 8, 6, 8);
                    g.drawLine(0, 2, 0, 6);
                    g.drawLine(8, 2, 8, 6);
                    g.drawLine(1, 1, 1, 1);
                    g.drawLine(7, 1, 7, 1);
                    g.drawLine(1, 7, 1, 7);
                    g.drawLine(7, 7, 7, 7);
                } else {
                    g.setColor(PFLookAndFeel.getControlHighlight());
                    g.drawLine(3, 1, 8, 1);
                    g.drawLine(2, 9, 7, 9);
                    g.drawLine(1, 3, 1, 8);
                    g.drawLine(9, 2, 9, 7);
                    g.drawLine(2, 2, 2, 2);
                    g.drawLine(8, 8, 8, 8);
                    g.setColor(PFLookAndFeel.getControlDarkShadow());
                    g.drawLine(2, 0, 6, 0);
                    g.drawLine(2, 8, 6, 8);
                    g.drawLine(0, 2, 0, 6);
                    g.drawLine(8, 2, 8, 6);
                    g.drawLine(1, 1, 1, 1);
                    g.drawLine(7, 1, 7, 1);
                    g.drawLine(1, 7, 1, 7);
                    g.drawLine(7, 7, 7, 7);
                }
            } else {
                g.setColor(PFLookAndFeel.getMenuDisabledForeground());
                g.drawLine(2, 0, 6, 0);
                g.drawLine(2, 8, 6, 8);
                g.drawLine(0, 2, 0, 6);
                g.drawLine(8, 2, 8, 6);
                g.drawLine(1, 1, 1, 1);
                g.drawLine(7, 1, 7, 1);
                g.drawLine(1, 7, 1, 7);
                g.drawLine(7, 7, 7, 7);
            }
            if (isSelected) {
                if (isEnabled) {
                    if (model.isArmed() || c instanceof JMenu && model.isSelected()) {
                        g.setColor(PFLookAndFeel.getMenuSelectedForeground());
                    } else {
                        g.setColor(b.getForeground());
                    }
                } else {
                    g.setColor(PFLookAndFeel.getMenuDisabledForeground());
                }
                g.drawLine(3, 2, 5, 2);
                g.drawLine(2, 3, 6, 3);
                g.drawLine(2, 4, 6, 4);
                g.drawLine(2, 5, 6, 5);
                g.drawLine(3, 6, 5, 6);
            }
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return PFIconFactory.access$1700().width;
        }

        @Override
        public int getIconHeight() {
            return PFIconFactory.access$1700().height;
        }
    }

    private static class TreeComputerIcon
    implements Icon,
    UIResource,
    Serializable {
        private TreeComputerIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.fillRect(5, 4, 6, 4);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(2, 2, 2, 8);
            g.drawLine(13, 2, 13, 8);
            g.drawLine(3, 1, 12, 1);
            g.drawLine(12, 9, 12, 9);
            g.drawLine(3, 9, 3, 9);
            g.drawLine(4, 4, 4, 7);
            g.drawLine(5, 3, 10, 3);
            g.drawLine(11, 4, 11, 7);
            g.drawLine(5, 8, 10, 8);
            g.drawLine(1, 10, 14, 10);
            g.drawLine(14, 10, 14, 14);
            g.drawLine(1, 14, 14, 14);
            g.drawLine(1, 10, 1, 14);
            g.setColor(PFLookAndFeel.getControlDarkShadow());
            g.drawLine(6, 12, 8, 12);
            g.drawLine(10, 12, 12, 12);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 16;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }
    }

    public static class TreeControlIcon
    implements Icon,
    Serializable {
        protected boolean isLight;
        ImageCacher imageCacher;
        transient boolean cachedOrientation = true;

        public TreeControlIcon(boolean isCollapsed) {
            this.isLight = isCollapsed;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Image image;
            GraphicsConfiguration gc = c.getGraphicsConfiguration();
            if (this.imageCacher == null) {
                this.imageCacher = new ImageCacher();
            }
            if ((image = this.imageCacher.getImage(gc)) == null || this.cachedOrientation != PFUtils.isLeftToRight(c)) {
                this.cachedOrientation = PFUtils.isLeftToRight(c);
                image = gc != null ? gc.createCompatibleImage(this.getIconWidth(), this.getIconHeight(), 2) : new BufferedImage(this.getIconWidth(), this.getIconHeight(), 2);
                Graphics imageG = image.getGraphics();
                this.paintMe(c, imageG, x, y);
                imageG.dispose();
                this.imageCacher.cacheImage(image, gc);
            }
            if (PFUtils.isLeftToRight(c)) {
                if (this.isLight) {
                    g.drawImage(image, x + 5, y + 3, x + 18, y + 13, 4, 3, 17, 13, null);
                } else {
                    g.drawImage(image, x + 5, y + 3, x + 18, y + 17, 4, 3, 17, 17, null);
                }
            } else if (this.isLight) {
                g.drawImage(image, x + 3, y + 3, x + 16, y + 13, 4, 3, 17, 13, null);
            } else {
                g.drawImage(image, x + 3, y + 3, x + 16, y + 17, 4, 3, 17, 17, null);
            }
        }

        public void paintMe(Component c, Graphics g, int x, int y) {
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            int xoff = PFUtils.isLeftToRight(c) ? 0 : 4;
            g.drawLine(xoff + 4, 6, xoff + 4, 9);
            g.drawLine(xoff + 5, 5, xoff + 5, 5);
            g.drawLine(xoff + 6, 4, xoff + 9, 4);
            g.drawLine(xoff + 10, 5, xoff + 10, 5);
            g.drawLine(xoff + 11, 6, xoff + 11, 9);
            g.drawLine(xoff + 10, 10, xoff + 10, 10);
            g.drawLine(xoff + 6, 11, xoff + 9, 11);
            g.drawLine(xoff + 5, 10, xoff + 5, 10);
            g.drawLine(xoff + 7, 7, xoff + 8, 7);
            g.drawLine(xoff + 7, 8, xoff + 8, 8);
            if (this.isLight) {
                if (PFUtils.isLeftToRight(c)) {
                    g.drawLine(12, 7, 15, 7);
                    g.drawLine(12, 8, 15, 8);
                } else {
                    g.drawLine(4, 7, 7, 7);
                    g.drawLine(4, 8, 7, 8);
                }
            } else {
                g.drawLine(xoff + 7, 12, xoff + 7, 15);
                g.drawLine(xoff + 8, 12, xoff + 8, 15);
            }
            g.setColor(PFLookAndFeel.getPrimaryControlDarkShadow());
            g.drawLine(xoff + 5, 6, xoff + 5, 9);
            g.drawLine(xoff + 6, 5, xoff + 9, 5);
            g.setColor(PFLookAndFeel.getPrimaryControlShadow());
            g.drawLine(xoff + 6, 6, xoff + 6, 6);
            g.drawLine(xoff + 9, 6, xoff + 9, 6);
            g.drawLine(xoff + 6, 9, xoff + 6, 9);
            g.drawLine(xoff + 10, 6, xoff + 10, 9);
            g.drawLine(xoff + 6, 10, xoff + 9, 10);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.drawLine(xoff + 6, 7, xoff + 6, 8);
            g.drawLine(xoff + 7, 6, xoff + 8, 6);
            g.drawLine(xoff + 9, 7, xoff + 9, 7);
            g.drawLine(xoff + 7, 9, xoff + 7, 9);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.drawLine(xoff + 8, 9, xoff + 9, 9);
            g.drawLine(xoff + 9, 8, xoff + 9, 8);
        }

        @Override
        public int getIconWidth() {
            return PFIconFactory.access$1500().width;
        }

        @Override
        public int getIconHeight() {
            return PFIconFactory.access$1500().height;
        }
    }

    private static class TreeFloppyDriveIcon
    implements Icon,
    UIResource,
    Serializable {
        private TreeFloppyDriveIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControl());
            g.fillRect(2, 2, 12, 12);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(1, 1, 13, 1);
            g.drawLine(14, 2, 14, 14);
            g.drawLine(1, 14, 14, 14);
            g.drawLine(1, 1, 1, 14);
            g.setColor(PFLookAndFeel.getControlDarkShadow());
            g.fillRect(5, 2, 6, 5);
            g.drawLine(4, 8, 11, 8);
            g.drawLine(3, 9, 3, 13);
            g.drawLine(12, 9, 12, 13);
            g.setColor(PFLookAndFeel.getPrimaryControlHighlight());
            g.fillRect(8, 3, 2, 3);
            g.fillRect(4, 9, 8, 5);
            g.setColor(PFLookAndFeel.getPrimaryControlShadow());
            g.drawLine(5, 10, 9, 10);
            g.drawLine(5, 12, 8, 12);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 16;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }
    }

    public static class TreeFolderIcon
    extends FolderIcon16 {
        @Override
        public int getShift() {
            return -1;
        }

        @Override
        public int getAdditionalHeight() {
            return 2;
        }
    }

    private static class TreeHardDriveIcon
    implements Icon,
    UIResource,
    Serializable {
        private TreeHardDriveIcon() {
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.translate(x, y);
            g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            g.drawLine(1, 4, 1, 5);
            g.drawLine(2, 3, 3, 3);
            g.drawLine(4, 2, 11, 2);
            g.drawLine(12, 3, 13, 3);
            g.drawLine(14, 4, 14, 5);
            g.drawLine(12, 6, 13, 6);
            g.drawLine(4, 7, 11, 7);
            g.drawLine(2, 6, 3, 6);
            g.drawLine(1, 7, 1, 8);
            g.drawLine(2, 9, 3, 9);
            g.drawLine(4, 10, 11, 10);
            g.drawLine(12, 9, 13, 9);
            g.drawLine(14, 7, 14, 8);
            g.drawLine(1, 10, 1, 11);
            g.drawLine(2, 12, 3, 12);
            g.drawLine(4, 13, 11, 13);
            g.drawLine(12, 12, 13, 12);
            g.drawLine(14, 10, 14, 11);
            g.setColor(PFLookAndFeel.getControlShadow());
            g.drawLine(7, 6, 7, 6);
            g.drawLine(9, 6, 9, 6);
            g.drawLine(10, 5, 10, 5);
            g.drawLine(11, 6, 11, 6);
            g.drawLine(12, 5, 13, 5);
            g.drawLine(13, 4, 13, 4);
            g.drawLine(7, 9, 7, 9);
            g.drawLine(9, 9, 9, 9);
            g.drawLine(10, 8, 10, 8);
            g.drawLine(11, 9, 11, 9);
            g.drawLine(12, 8, 13, 8);
            g.drawLine(13, 7, 13, 7);
            g.drawLine(7, 12, 7, 12);
            g.drawLine(9, 12, 9, 12);
            g.drawLine(10, 11, 10, 11);
            g.drawLine(11, 12, 11, 12);
            g.drawLine(12, 11, 13, 11);
            g.drawLine(13, 10, 13, 10);
            g.setColor(PFLookAndFeel.getControlHighlight());
            g.drawLine(4, 3, 5, 3);
            g.drawLine(7, 3, 9, 3);
            g.drawLine(11, 3, 11, 3);
            g.drawLine(2, 4, 6, 4);
            g.drawLine(8, 4, 8, 4);
            g.drawLine(2, 5, 3, 5);
            g.drawLine(4, 6, 4, 6);
            g.drawLine(2, 7, 3, 7);
            g.drawLine(2, 8, 3, 8);
            g.drawLine(4, 9, 4, 9);
            g.drawLine(2, 10, 3, 10);
            g.drawLine(2, 11, 3, 11);
            g.drawLine(4, 12, 4, 12);
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 16;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }
    }

    public static class TreeLeafIcon
    extends FileIcon16 {
        @Override
        public int getShift() {
            return 2;
        }

        @Override
        public int getAdditionalHeight() {
            return 4;
        }
    }

    private static class VerticalSliderThumbIcon
    implements Icon,
    Serializable,
    UIResource {
        protected static PFBumps controlBumps;
        protected static PFBumps primaryBumps;

        public VerticalSliderThumbIcon() {
            controlBumps = new PFBumps(6, 10, PFLookAndFeel.getControlHighlight(), PFLookAndFeel.getControlInfo(), PFLookAndFeel.getControl());
            primaryBumps = new PFBumps(6, 10, PFLookAndFeel.getPrimaryControl(), PFLookAndFeel.getPrimaryControlDarkShadow(), PFLookAndFeel.getPrimaryControlShadow());
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            int offset;
            JSlider slider = (JSlider)c;
            boolean leftToRight = PFUtils.isLeftToRight(slider);
            g.translate(x, y);
            if (slider.hasFocus()) {
                g.setColor(PFLookAndFeel.getPrimaryControlInfo());
            } else {
                g.setColor(slider.isEnabled() ? PFLookAndFeel.getPrimaryControlInfo() : PFLookAndFeel.getControlDarkShadow());
            }
            if (leftToRight) {
                g.drawLine(1, 0, 8, 0);
                g.drawLine(0, 1, 0, 13);
                g.drawLine(1, 14, 8, 14);
                g.drawLine(9, 1, 15, 7);
                g.drawLine(9, 13, 15, 7);
            } else {
                g.drawLine(7, 0, 14, 0);
                g.drawLine(15, 1, 15, 13);
                g.drawLine(7, 14, 14, 14);
                g.drawLine(0, 7, 6, 1);
                g.drawLine(0, 7, 6, 13);
            }
            if (slider.hasFocus()) {
                g.setColor(c.getForeground());
            } else {
                g.setColor(PFLookAndFeel.getControl());
            }
            if (leftToRight) {
                g.fillRect(1, 1, 8, 13);
                g.drawLine(9, 2, 9, 12);
                g.drawLine(10, 3, 10, 11);
                g.drawLine(11, 4, 11, 10);
                g.drawLine(12, 5, 12, 9);
                g.drawLine(13, 6, 13, 8);
                g.drawLine(14, 7, 14, 7);
            } else {
                g.fillRect(7, 1, 8, 13);
                g.drawLine(6, 3, 6, 12);
                g.drawLine(5, 4, 5, 11);
                g.drawLine(4, 5, 4, 10);
                g.drawLine(3, 6, 3, 9);
                g.drawLine(2, 7, 2, 8);
            }
            int n = offset = leftToRight ? 2 : 8;
            if (slider.isEnabled()) {
                if (slider.hasFocus()) {
                    primaryBumps.paintIcon(c, g, offset, 2);
                } else {
                    controlBumps.paintIcon(c, g, offset, 2);
                }
            }
            if (slider.isEnabled()) {
                g.setColor(slider.hasFocus() ? PFLookAndFeel.getPrimaryControl() : PFLookAndFeel.getControlHighlight());
                if (leftToRight) {
                    g.drawLine(1, 1, 8, 1);
                    g.drawLine(1, 1, 1, 13);
                } else {
                    g.drawLine(8, 1, 14, 1);
                    g.drawLine(1, 7, 7, 1);
                }
            }
            g.translate(- x, - y);
        }

        @Override
        public int getIconWidth() {
            return 16;
        }

        @Override
        public int getIconHeight() {
            return 15;
        }
    }

}

