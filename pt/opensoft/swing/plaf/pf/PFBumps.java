/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf.pf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.Icon;
import pt.opensoft.swing.plaf.pf.PFBumpBuffer;
import pt.opensoft.swing.plaf.pf.PFLookAndFeel;

class PFBumps
implements Icon {
    protected int xBumps;
    protected int yBumps;
    protected Color topColor;
    protected Color shadowColor;
    protected Color backColor;
    protected static Vector buffers = new Vector();
    protected PFBumpBuffer buffer;

    public PFBumps(Dimension bumpArea) {
        this(bumpArea.width, bumpArea.height);
    }

    public PFBumps(int width, int height) {
        this(width, height, PFLookAndFeel.getPrimaryControlHighlight(), PFLookAndFeel.getPrimaryControlDarkShadow(), PFLookAndFeel.getPrimaryControlShadow());
    }

    public PFBumps(int width, int height, Color newTopColor, Color newShadowColor, Color newBackColor) {
        this.setBumpArea(width, height);
        this.setBumpColors(newTopColor, newShadowColor, newBackColor);
    }

    private PFBumpBuffer getBuffer(GraphicsConfiguration gc, Color aTopColor, Color aShadowColor, Color aBackColor) {
        if (this.buffer != null && this.buffer.hasSameConfiguration(gc, aTopColor, aShadowColor, aBackColor)) {
            return this.buffer;
        }
        PFBumpBuffer result = null;
        Enumeration elements = buffers.elements();
        while (elements.hasMoreElements()) {
            PFBumpBuffer aBuffer = (PFBumpBuffer)elements.nextElement();
            if (!aBuffer.hasSameConfiguration(gc, aTopColor, aShadowColor, aBackColor)) continue;
            result = aBuffer;
            break;
        }
        if (result == null) {
            result = new PFBumpBuffer(gc, this.topColor, this.shadowColor, this.backColor);
            buffers.addElement(result);
        }
        return result;
    }

    public void setBumpArea(Dimension bumpArea) {
        this.setBumpArea(bumpArea.width, bumpArea.height);
    }

    public void setBumpArea(int width, int height) {
        this.xBumps = width / 2;
        this.yBumps = height / 2;
    }

    public void setBumpColors(Color newTopColor, Color newShadowColor, Color newBackColor) {
        this.topColor = newTopColor;
        this.shadowColor = newShadowColor;
        this.backColor = newBackColor;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        GraphicsConfiguration gc = g instanceof Graphics2D ? ((Graphics2D)g).getDeviceConfiguration() : null;
        this.buffer = this.getBuffer(gc, this.topColor, this.shadowColor, this.backColor);
        int bufferWidth = this.buffer.getImageSize().width;
        int bufferHeight = this.buffer.getImageSize().height;
        int iconWidth = this.getIconWidth();
        int iconHeight = this.getIconHeight();
        int x2 = x + iconWidth;
        int y2 = y + iconHeight;
        int savex = x;
        while (y < y2) {
            int h = Math.min(y2 - y, bufferHeight);
            for (x = savex; x < x2; x+=bufferWidth) {
                int w = Math.min(x2 - x, bufferWidth);
                g.drawImage(this.buffer.getImage(), x, y, x + w, y + h, 0, 0, w, h, null);
            }
            y+=bufferHeight;
        }
    }

    @Override
    public int getIconWidth() {
        return this.xBumps * 2;
    }

    @Override
    public int getIconHeight() {
        return this.yBumps * 2;
    }
}

