/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.plaf.KunststoffUtilities;

public class TabbedPaneUI
extends BasicTabbedPaneUI {
    private static final int SHADOW_WIDTH = 5;

    public static ComponentUI createUI(JComponent c) {
        return new TabbedPaneUI();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
    }

    @Override
    protected void paintTab(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect) {
        Rectangle gradientRect;
        super.paintTab(g, tabPlacement, rects, tabIndex, iconRect, textRect);
        Rectangle tabRect = rects[tabIndex];
        Color colorShadow = GUIParameters.COLOR_SHADOW;
        Color colorShadowFaded = GUIParameters.COLOR_SHADOW_FADED;
        Color colorReflection = GUIParameters.COLOR_REFLECTION;
        Color colorReflectionFaded = GUIParameters.COLOR_REFLECTION_FADED;
        int selectedIndex = this.tabPane.getSelectedIndex();
        if (this.lastTabInRun(this.tabPane.getTabCount(), this.selectedRun) != selectedIndex) {
            if (tabPlacement == 1 || tabPlacement == 3) {
                if (tabIndex == selectedIndex + 1) {
                    gradientRect = new Rectangle((int)tabRect.getX(), (int)tabRect.getY(), 5, (int)tabRect.getHeight());
                    KunststoffUtilities.drawGradient(g, colorShadow, colorShadowFaded, gradientRect, false);
                }
            } else if (tabIndex == selectedIndex + 1) {
                gradientRect = new Rectangle((int)tabRect.getX(), (int)tabRect.getY(), (int)tabRect.getWidth(), 5);
                KunststoffUtilities.drawGradient(g, colorShadow, colorShadowFaded, gradientRect, true);
            }
        }
        if (tabPlacement == 1) {
            gradientRect = new Rectangle((int)tabRect.getX(), (int)tabRect.getY(), (int)tabRect.getWidth(), 5);
            KunststoffUtilities.drawGradient(g, colorReflection, colorReflectionFaded, gradientRect, true);
        } else if (tabPlacement == 3) {
            if (tabIndex != selectedIndex) {
                gradientRect = new Rectangle((int)tabRect.getX(), (int)tabRect.getY(), (int)tabRect.getWidth(), 5);
                KunststoffUtilities.drawGradient(g, colorShadow, colorShadowFaded, gradientRect, true);
            }
        } else if (tabPlacement == 4 && tabIndex != selectedIndex) {
            gradientRect = new Rectangle((int)tabRect.getX(), (int)tabRect.getY(), 5, (int)tabRect.getHeight());
            KunststoffUtilities.drawGradient(g, colorShadow, colorShadowFaded, gradientRect, false);
        }
    }

    @Override
    protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
        if (isSelected) {
            g.setColor(UIManager.getColor("TabbedPane.selected"));
        } else {
            g.setColor(this.tabPane.getBackgroundAt(tabIndex));
        }
        switch (tabPlacement) {
            case 2: {
                g.fillRect(x + 1, y + 1, w - 2, h - 3);
                break;
            }
            case 4: {
                g.fillRect(x, y + 1, w - 2, h - 3);
                break;
            }
            case 3: {
                g.fillRect(x + 1, y, w - 3, h - 1);
                break;
            }
            default: {
                g.fillRect(x + 1, y + 1, w - 3, h - 1);
            }
        }
    }
}

