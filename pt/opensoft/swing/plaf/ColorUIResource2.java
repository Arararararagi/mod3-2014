/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.plaf;

import java.awt.Color;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;
import javax.swing.plaf.ColorUIResource;

public class ColorUIResource2
extends ColorUIResource {
    private Color myColor;

    public ColorUIResource2(Color c) {
        super(0, 0, 0);
        this.myColor = c;
    }

    public ColorUIResource2(int r, int g, int b) {
        super(0, 0, 0);
        this.myColor = new Color(r, g, b);
    }

    public ColorUIResource2(int r, int g, int b, int a) {
        super(0, 0, 0);
        this.myColor = new Color(r, g, b, a);
    }

    public ColorUIResource2(int rgb) {
        super(0, 0, 0);
        this.myColor = new Color(rgb);
    }

    public ColorUIResource2(int rgba, boolean hasalpha) {
        super(0, 0, 0);
        this.myColor = new Color(rgba, hasalpha);
    }

    public ColorUIResource2(float r, float g, float b) {
        super(0, 0, 0);
        this.myColor = new Color(r, g, b);
    }

    public ColorUIResource2(float r, float g, float b, float alpha) {
        super(0, 0, 0);
        this.myColor = new Color(r, g, b, alpha);
    }

    @Override
    public int getRed() {
        return this.myColor.getRed();
    }

    @Override
    public int getGreen() {
        return this.myColor.getGreen();
    }

    @Override
    public int getBlue() {
        return this.myColor.getBlue();
    }

    @Override
    public int getAlpha() {
        return this.myColor.getAlpha();
    }

    @Override
    public int getRGB() {
        return this.myColor.getRGB();
    }

    @Override
    public Color brighter() {
        return this.myColor.brighter();
    }

    @Override
    public Color darker() {
        return this.myColor.darker();
    }

    @Override
    public int hashCode() {
        return this.myColor.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return this.myColor.equals(obj);
    }

    @Override
    public String toString() {
        return this.myColor.toString();
    }

    @Override
    public float[] getRGBComponents(float[] compArray) {
        return this.myColor.getRGBComponents(compArray);
    }

    @Override
    public float[] getRGBColorComponents(float[] compArray) {
        return this.myColor.getRGBColorComponents(compArray);
    }

    @Override
    public float[] getComponents(float[] compArray) {
        return this.myColor.getComponents(compArray);
    }

    @Override
    public float[] getColorComponents(float[] compArray) {
        return this.myColor.getColorComponents(compArray);
    }

    @Override
    public float[] getComponents(ColorSpace cspace, float[] compArray) {
        return this.myColor.getComponents(cspace, compArray);
    }

    @Override
    public float[] getColorComponents(ColorSpace cspace, float[] compArray) {
        return this.myColor.getColorComponents(cspace, compArray);
    }

    @Override
    public ColorSpace getColorSpace() {
        return this.myColor.getColorSpace();
    }

    @Override
    public PaintContext createContext(ColorModel cm, Rectangle r, Rectangle2D r2d, AffineTransform xform, RenderingHints hints) {
        return this.myColor.createContext(cm, r, r2d, xform, hints);
    }

    @Override
    public int getTransparency() {
        return this.myColor.getTransparency();
    }
}

