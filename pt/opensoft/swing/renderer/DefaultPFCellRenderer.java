/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.renderer;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;

public class DefaultPFCellRenderer
extends DefaultTableCellRenderer {
    private static final long serialVersionUID = -2703529140162770819L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value == null) {
            this.setText(null);
        } else {
            this.setText(String.valueOf(value));
        }
        if (value instanceof Long) {
            this.setHorizontalAlignment(4);
        }
        if (isSelected && !hasFocus) {
            this.setForeground(table.getSelectionForeground());
            this.setBackground(table.getSelectionBackground());
        } else if (!(table.getSelectedRow() != row || hasFocus)) {
            this.setForeground(table.getSelectionForeground());
            this.setBackground(table.getSelectionBackground());
        } else {
            this.setForeground(table.getForeground());
            this.setBackground(table.getBackground());
        }
        this.setOpaque(true);
        if (hasFocus) {
            this.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        } else {
            this.setBorder(noFocusBorder);
        }
        return this;
    }
}

