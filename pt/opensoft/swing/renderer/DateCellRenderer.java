/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.renderer;

import java.awt.Color;
import java.awt.Component;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;

public class DateCellRenderer
extends DefaultTableCellRenderer {
    private static final long serialVersionUID = -1493866649300663300L;
    private Border noFocusBorder = BorderFactory.createEmptyBorder(1, 1, 1, 1);

    @Override
    public final Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        if (value != null) {
            this.setText(new pt.opensoft.util.Date((Date)value).formatDate());
        } else {
            this.setText(null);
        }
        if (isSelected && !hasFocus) {
            this.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else if (!(table.getSelectedRow() != row || hasFocus)) {
            this.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else {
            this.setForeground(table.getForeground());
            this.setBackground(table.getBackground());
        }
        if (hasFocus) {
            this.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        } else {
            this.setBorder(this.noFocusBorder);
        }
        return this;
    }
}

