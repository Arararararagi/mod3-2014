/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.renderer;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;

public class RowIndexTableCellRendererWithPrefix
extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 1;
    private String prefix;

    public RowIndexTableCellRendererWithPrefix(String prefix) {
        this.prefix = prefix;
        this.setHorizontalTextPosition(0);
        this.setHorizontalAlignment(0);
        if (UIManager.getColor("TableHeader.background") != null) {
            this.setBackground(UIManager.getColor("TableHeader.background"));
        }
        if (UIManager.getBorder("TableHeader.cellBorder") != null) {
            this.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        }
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean arg2, boolean arg3, int row, int column) {
        this.setText(this.prefix + Integer.toString(row + 1));
        return this;
    }
}

