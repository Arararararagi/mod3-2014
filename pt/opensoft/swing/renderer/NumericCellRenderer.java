/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.renderer;

import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import pt.opensoft.swing.components.JNumericTextField;

public class NumericCellRenderer
extends JNumericTextField
implements TableCellRenderer {
    private static final long serialVersionUID = 2392967862370115305L;
    private Border noFocusBorder = BorderFactory.createEmptyBorder(1, 1, 1, 1);

    public NumericCellRenderer() {
        super(17, 2);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setBackground(new JLabel().getBackground());
    }

    public NumericCellRenderer(int columns, int decimalplaces) {
        super(columns, decimalplaces);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setBackground(new JLabel().getBackground());
    }

    @Override
    public final Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        if (value != null) {
            this.setValue(String.valueOf(value));
        } else {
            this.setValue("");
        }
        if (isSelected && !hasFocus) {
            this.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else if (!(table.getSelectedRow() != row || hasFocus)) {
            this.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else {
            this.setForeground(table.getForeground());
            this.setBackground(table.getBackground());
        }
        if (hasFocus) {
            this.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        } else {
            this.setBorder(this.noFocusBorder);
        }
        return this;
    }
}

