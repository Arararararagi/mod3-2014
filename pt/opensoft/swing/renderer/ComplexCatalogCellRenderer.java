/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.renderer;

import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class ComplexCatalogCellRenderer
extends DefaultListCellRenderer
implements TableCellRenderer {
    private static final long serialVersionUID = -1583853677861537629L;
    private JLabel label = new JLabel();

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        ICatalogItem catalogItem = (ICatalogItem)value;
        if (value == null) {
            this.setText(null);
        } else {
            this.setText(catalogItem.getDescription());
            list.setToolTipText(catalogItem.getDescription());
        }
        if (isSelected) {
            this.setBackground(list.getSelectionBackground());
            this.setForeground(list.getSelectionForeground());
        } else {
            this.setBackground(list.getBackground());
            this.setForeground(list.getForeground());
        }
        return this;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        ICatalogItem catalogItem = (ICatalogItem)value;
        if (value == null) {
            this.label.setText(null);
        } else {
            this.label.setText(catalogItem.getDescription());
            this.label.setToolTipText(catalogItem.getDescription());
        }
        if (isSelected && !hasFocus) {
            this.label.setForeground(table.getSelectionForeground());
            this.label.setBackground(table.getSelectionBackground());
        } else if (!(table.getSelectedRow() != row || hasFocus)) {
            this.label.setForeground(table.getSelectionForeground());
            this.label.setBackground(table.getSelectionBackground());
        } else {
            this.label.setForeground(table.getForeground());
            this.label.setBackground(table.getBackground());
        }
        this.label.setOpaque(true);
        if (hasFocus) {
            this.label.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        } else {
            this.label.setBorder(noFocusBorder);
        }
        return this.label;
    }
}

