/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.renderer;

import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import pt.opensoft.swing.components.JPercentTextField;

public class JPercentCellRenderer
extends JPercentTextField
implements TableCellRenderer {
    private static final long serialVersionUID = 1;
    private Border noFocusBorder = BorderFactory.createEmptyBorder(1, 1, 1, 1);

    public JPercentCellRenderer() {
        this.setBorder(BorderFactory.createEmptyBorder());
    }

    public JPercentCellRenderer(int precision) {
        super(precision);
        this.setBorder(BorderFactory.createEmptyBorder());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        this.setValue(value == null ? "" : String.valueOf(value));
        if (isSelected && !hasFocus) {
            this.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else if (!(table.getSelectedRow() != row || hasFocus)) {
            this.setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else {
            this.setForeground(table.getForeground());
            this.setBackground(table.getBackground());
        }
        if (hasFocus) {
            this.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        } else {
            this.setBorder(this.noFocusBorder);
        }
        return this;
    }
}

