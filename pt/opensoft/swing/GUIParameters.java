/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.io.InputStream;
import java.util.StringTokenizer;
import pt.opensoft.swing.dependencies.ColorUIResource2;
import pt.opensoft.swing.dependencies.KunststoffUtilities;
import pt.opensoft.util.SimpleParameters;

public class GUIParameters
extends SimpleParameters {
    public static int DEFAULT_APP_INITIAL_WINDOW_SIZE_X = 800;
    public static int DEFAULT_APP_INITIAL_WINDOW_SIZE_Y = 600;
    public static Dimension APP_INITIAL_WINDOW_SIZE;
    public static final Color DEFAULT_TABLE_TITLE_BG_COLOR;
    public static final Color DEFAULT_NAVIGATION_TITLE_BG_COLOR;
    public static final Color DEFAULT_DECLARATION_TITLE_BG_COLOR;
    public static final Color DEFAULT_TABLE_TITLE_FG_COLOR;
    public static final float DEFAULT_TABLE_TITLE_FONT_SIZE = 12.0f;
    public static final int DEFAULT_TABLE_TITLE_HORIZONTAL_ALIGN = 0;
    public static Color TABLE_TITLE_BG_COLOR;
    public static Color TABLE_TITLE_FG_COLOR;
    public static Color NAVIGATION_TITLE_BG_COLOR;
    public static Color DECLARATION_TITLE_BG_COLOR;
    public static float TABLE_TITLE_FONT_SIZE;
    public static int TABLE_TITLE_HORIZONTAL_ALIGN;
    public static int TABLE_INDEX_COLUMN_WIDTH;
    public static final Color DEFAULT_TITLED_PANEL_TITLE_COLOR;
    public static Color TITLED_PANEL_TITLE_COLOR;
    public static final boolean DEFAULT_SHOW_PANEL_NAME = true;
    public static boolean SHOW_PANEL_NAME;
    public static final ColorUIResource2 DEFAULT_COLOR_SHADOW;
    public static final ColorUIResource2 DEFAULT_COLOR_REFLECTION;
    public static Color COLOR_SHADOW;
    public static Color COLOR_SHADOW_FADED;
    public static Color COLOR_REFLECTION;
    public static Color COLOR_REFLECTION_FADED;
    public static int DEFAULT_MIN_WIDTH;
    public static int DEFAULT_MIN_HEIGHT;
    public static int MIN_WIDTH;
    public static int MIN_HEIGHT;
    public static final boolean DEFAULT_SHOW_MENU = true;
    public static boolean SHOW_MENU;
    public static final boolean DEFAULT_SAVE_CHANGES_INFO = false;
    public static boolean SAVE_CHANGES_INFO;
    public static final boolean DEFAULT_SELECT_TEXT_ON_FOCUS_GAINED = false;
    public static final String DEFAULT_ICON_SUBMETER_ANIMACAO = "submit.gif";
    public static final String DEFAULT_ICON_NOVO = "Novo.png";
    public static final String DEFAULT_ICON_ABRIR = "Abrir.png";
    public static final String DEFAULT_ICON_GRAVAR = "Gravar.png";
    public static final String DEFAULT_ICON_IMPRIMIR = "Imprimir.png";
    public static final String DEFAULT_ICON_AJUDA_TEMAS = "Ajuda_Temas.png";
    public static final String DEFAULT_ICON_PESQUISA_AJUDA = "lupa.png";
    public static final String DEFAULT_ICON_SIMULAR = "Simular.png";
    public static final String DEFAULT_ICON_VALIDAR = "Validar.png";
    public static final String DEFAULT_ICON_ANEXAR = "attachment.png";
    public static final String DEFAULT_ICON_ACERCA = "Acerca.png";
    public static final String DEFAULT_ICON_AJUDA_CAMPOS = "Ajuda_Campos.png";
    public static final String DEFAULT_ICON_AJUDA_ONLINE = "Ajuda_Online.png";
    public static final String DEFAULT_ICON_SUBMETER = "Submeter.png";
    public static final String DEFAULT_ICON_WIZARD = "Wizard.png";
    public static final String DEFAULT_ICON_CUT = "Cut.png";
    public static final String DEFAULT_ICON_ERRO = "error.png";
    public static final String DEFAULT_ICON_INFO = "icon_info.png";
    public static final String DEFAULT_ICON_COPY = "Copy.png";
    public static final String DEFAULT_ICON_PASTE = "Paste.png";
    public static final String DEFAULT_ICON_CONFIGURAR = "Configurar.png";
    public static final String DEFAULT_ICON_NOVO_ANEXO = "NovoAnexo.png";
    public static final String DEFAULT_ICON_DELETE_ANEXO = "DeleteAnexo.png";
    public static final String DEFAULT_APPLICATION_ICON = "icon.gif";
    public static final String DEFAULT_LOGO_ICON = "Logo.png";
    public static final String DEFAULT_THEME_ICON = "Theme.png";
    public static final String DEFAULT_CHANGE_COLOR_ICON = "ChangeColour.png";
    public static final String DEFAULT_APP_MENU_BUTTON_ICON = "AppMenuButton.png";
    public static final String DEFAULT_HELP_BROWSER_ICON = "HelpBrowser.png";
    public static final String DEFAULT_ICON_FECHAR_PAINEL = "Down.gif";
    public static final String DEFAULT_ICON_CENTRAL_ERRORS = "central_errors.png";
    public static String ICON_FECHAR_PAINEL;
    public static String ICON_NOVO;
    public static String ICON_ABRIR;
    public static String ICON_ANEXAR;
    public static String ICON_GRAVAR;
    public static String ICON_IMPRIMIR;
    public static String ICON_AJUDA_TEMAS;
    public static String ICON_SIMULAR;
    public static String ICON_VALIDAR;
    public static String ICON_ACERCA;
    public static String ICON_AJUDA_CAMPOS;
    public static String ICON_PESQUISA_AJUDA;
    public static String ICON_AJUDA_ONLINE;
    public static String ICON_SUBMETER;
    public static String ICON_WIZARD;
    public static String ICON_CUT;
    public static String ICON_COPY;
    public static String ICON_PASTE;
    public static String ICON_CONFIGURAR;
    public static String ICON_NOVO_ANEXO;
    public static String ICON_DELETE_ANEXO;
    public static String APPLICATION_ICON;
    public static String ICON_LOGO;
    public static String ICON_THEME;
    public static String ICON_CHANGE_COLOR;
    public static String ICON_APP_MENU_BUTTON;
    public static String ICON_HELP_BROWSER;
    public static String ICON_ERRO;
    public static String ICON_INFO;
    public static String ICON_SUBMETER_ANIMACAO;
    public static String ICON_CENTRAL_ERRORS;
    private static GUIParameters instance;

    public static GUIParameters instance() {
        return instance;
    }

    public static void load(String parametersFileName) {
        InputStream paramInput = GUIParameters.class.getResourceAsStream(parametersFileName);
        instance = new GUIParameters(paramInput);
    }

    public static void load(InputStream paramInput) {
        instance = new GUIParameters(paramInput);
    }

    public GUIParameters(InputStream paramInput) {
        this.read(paramInput);
        TITLED_PANEL_TITLE_COLOR = this.getColor("titled.panel.title.color", DEFAULT_TITLED_PANEL_TITLE_COLOR);
        SHOW_PANEL_NAME = this.getBoolean("show.panel.name", true);
        APPLICATION_ICON = this.getString("app.icon", "icon.gif");
        MIN_WIDTH = this.getInt("app.gui.minwidth", DEFAULT_MIN_WIDTH);
        MIN_HEIGHT = this.getInt("app.gui.minheigth", DEFAULT_MIN_HEIGHT);
        APP_INITIAL_WINDOW_SIZE = new Dimension(this.getInt("gui.appInitialWindowSizeX", DEFAULT_APP_INITIAL_WINDOW_SIZE_X), this.getInt("gui.appInitialWindowSizeY", DEFAULT_APP_INITIAL_WINDOW_SIZE_Y));
        TABLE_TITLE_BG_COLOR = this.getColor("table.title.bg.color", DEFAULT_TABLE_TITLE_BG_COLOR);
        NAVIGATION_TITLE_BG_COLOR = this.getColor("navigation.title.bg.color", DEFAULT_NAVIGATION_TITLE_BG_COLOR);
        DECLARATION_TITLE_BG_COLOR = this.getColor("declaration.title.bg.color", DEFAULT_DECLARATION_TITLE_BG_COLOR);
        TABLE_TITLE_FG_COLOR = this.getColor("table.title.fg.color", Color.WHITE);
        TABLE_TITLE_FONT_SIZE = this.getFloat("table.title.font.size", 12.0f);
        TABLE_TITLE_HORIZONTAL_ALIGN = this.getInt("table.title.horizontal.align", 0);
        COLOR_SHADOW = this.getColorUI2("color.shadow", DEFAULT_COLOR_SHADOW);
        COLOR_SHADOW_FADED = KunststoffUtilities.getTranslucentColor(COLOR_SHADOW, 0);
        COLOR_REFLECTION = this.getColorUI2("color.shadow", DEFAULT_COLOR_REFLECTION);
        COLOR_REFLECTION_FADED = KunststoffUtilities.getTranslucentColor(COLOR_REFLECTION, 0);
        SHOW_MENU = this.getBoolean("show.menu", true);
        SAVE_CHANGES_INFO = this.getBoolean("save.changes.info", false);
        ICON_NOVO = this.getString("app.icon.Novo", "Novo.png");
        ICON_ABRIR = this.getString("app.icon.Abrir", "Abrir.png");
        ICON_ANEXAR = this.getString("app.icon.Anexar", "attachment.png");
        ICON_GRAVAR = this.getString("app.icon.Gravar", "Gravar.png");
        ICON_IMPRIMIR = this.getString("app.icon.Imprimir", "Imprimir.png");
        ICON_AJUDA_TEMAS = this.getString("app.icon.Acerca", "Acerca.png");
        ICON_AJUDA_TEMAS = this.getString("app.icon.Ajuda_Temas", "Ajuda_Temas.png");
        ICON_PESQUISA_AJUDA = this.getString("app.icon.Search_Ajuda", "lupa.png");
        ICON_SIMULAR = this.getString("app.icon.Simular", "Simular.png");
        ICON_VALIDAR = this.getString("app.icon.Validar", "Validar.png");
        ICON_AJUDA_CAMPOS = this.getString("app.icon.Ajuda_Campos", "Ajuda_Campos.png");
        ICON_AJUDA_ONLINE = this.getString("app.icon.Ajuda_Online", "Ajuda_Online.png");
        ICON_SUBMETER = this.getString("app.icon.Submeter", "Submeter.png");
        ICON_WIZARD = this.getString("app.icon.Wizard", "Wizard.png");
        ICON_CUT = this.getString("app.icon.Cut", "Cut.png");
        ICON_COPY = this.getString("app.icon.Copy", "Copy.png");
        ICON_PASTE = this.getString("app.icon.Paste", "Paste.png");
        ICON_CONFIGURAR = this.getString("app.icon.Configurar", "Configurar.png");
        ICON_NOVO_ANEXO = this.getString("app.icon.NovoAnexo", "NovoAnexo.png");
        ICON_DELETE_ANEXO = this.getString("app.icon.DeleteAnexo", "DeleteAnexo.png");
        ICON_INFO = this.getString("app.icon.info", "icon_info.png");
        ICON_ERRO = this.getString("app.icon.erro", "error.png");
        ICON_LOGO = this.getString("app.icon.logo", "Logo.png");
        ICON_THEME = this.getString("app.icon.theme", "Theme.png");
        ICON_CHANGE_COLOR = this.getString("app.icon.change.color", "ChangeColour.png");
        ICON_APP_MENU_BUTTON = this.getString("app.icon.app.menu.button", "AppMenuButton.png");
        ICON_HELP_BROWSER = this.getString("app.icon.help.browser", "HelpBrowser.png");
        ICON_FECHAR_PAINEL = this.getString("app.icon.fecharPainel", "Down.gif");
        ICON_SUBMETER_ANIMACAO = this.getString("app.icon.Submeter_animacao", "submit.gif");
        ICON_CENTRAL_ERRORS = this.getString("app.icon.central.errors", "central_errors.png");
        TABLE_INDEX_COLUMN_WIDTH = this.getInt("app.table.index.column.width", 23);
    }

    public final boolean getBoolean(String value, boolean defaultBoolean) {
        if (this.getValue(value) == null) {
            return defaultBoolean;
        }
        return Boolean.valueOf(this.getString(value, String.valueOf(defaultBoolean)));
    }

    public final int getInt(String value, int defaultInt) {
        if (this.getValue(value) == null) {
            return defaultInt;
        }
        return Integer.parseInt(this.getString(value, String.valueOf(defaultInt)));
    }

    public final float getFloat(String value, float defaultFloat) {
        if (this.getValue(value) == null) {
            return defaultFloat;
        }
        return Float.parseFloat(this.getString(value, String.valueOf(defaultFloat)));
    }

    public final Color getColor(String value, Color color) {
        if (this.getValue(value) == null) {
            return color;
        }
        return new Color(this.getIndexedInt(value, 0, color.getRed()), this.getIndexedInt(value, 1, color.getGreen()), this.getIndexedInt(value, 2, color.getBlue()));
    }

    public final ColorUIResource2 getColorUI2(String value, ColorUIResource2 color) {
        if (this.getValue(value) == null) {
            return color;
        }
        return new ColorUIResource2(this.getIndexedInt(value, 0, color.getRed()), this.getIndexedInt(value, 1, color.getGreen()), this.getIndexedInt(value, 2, color.getBlue()), this.getIndexedInt(value, 4, color.getAlpha()));
    }

    private final int getIndexedInt(String value, int idx, int defaultValue) {
        if (this.getValue(value) == null) {
            return defaultValue;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(this.getString(value), ",");
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (idx == 0) {
                return Integer.parseInt(token);
            }
            --idx;
        }
        throw new RuntimeException("Invalid index (" + idx + ") for string: " + value);
    }

    public static boolean selectTextOnFocusGained() {
        if (instance == null) {
            return false;
        }
        return instance.getBoolean("taxclient.selectTextOnFocusField", false);
    }

    public void loadAdditionalParameters(String parametersFilename) {
        InputStream paramInput = GUIParameters.class.getResourceAsStream(parametersFilename);
        this.read(paramInput);
    }

    static {
        DEFAULT_TABLE_TITLE_BG_COLOR = new Color(97, 97, 63);
        DEFAULT_NAVIGATION_TITLE_BG_COLOR = new Color(181, 202, 28);
        DEFAULT_DECLARATION_TITLE_BG_COLOR = new Color(31, 157, 183);
        DEFAULT_TABLE_TITLE_FG_COLOR = Color.WHITE;
        DEFAULT_TITLED_PANEL_TITLE_COLOR = new Color(201, 217, 234);
        DEFAULT_COLOR_SHADOW = new ColorUIResource2(0, 0, 0, 48);
        DEFAULT_COLOR_REFLECTION = new ColorUIResource2(255, 255, 255, 96);
        DEFAULT_MIN_WIDTH = 550;
        DEFAULT_MIN_HEIGHT = 400;
    }
}

