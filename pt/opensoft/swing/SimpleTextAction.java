/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.event.ActionEvent;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;

public class SimpleTextAction
extends TextAction {
    public SimpleTextAction(String name) {
        super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    public JTextComponent getFocused() {
        return this.getFocusedComponent();
    }
}

