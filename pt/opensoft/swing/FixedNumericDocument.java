/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing;

import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import pt.opensoft.swing.AutomaticFocusTransferDocument;

public class FixedNumericDocument
extends AutomaticFocusTransferDocument {
    private static final long serialVersionUID = 4596232608345574082L;
    private int maxLength = 9999;
    private boolean numericOnly;

    public FixedNumericDocument(int maxLen, boolean numOnly) {
        this.maxLength = maxLen;
        this.numericOnly = numOnly;
    }

    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
        if (this.getLength() + str.length() > this.maxLength) {
            Toolkit.getDefaultToolkit().beep();
            return;
        }
        try {
            if (this.numericOnly) {
                Long.parseLong(str);
            }
            super.insertString(offset, str, attr);
            if (this.isAutomaticFocusTransferEnabled && this.getLength() >= this.maxLength) {
                this.transferFocusToNextComponent();
            }
        }
        catch (NumberFormatException exp) {
            Toolkit.getDefaultToolkit().beep();
            return;
        }
    }
}

