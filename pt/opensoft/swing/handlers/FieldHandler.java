/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.handlers;

import java.util.Map;
import pt.opensoft.swing.handlers.FieldHandlerModel;

public abstract class FieldHandler {
    protected static final String PREFIX = "tmp";
    protected boolean lazyLoad;
    protected boolean serverSide;

    protected FieldHandler(boolean lazyLoad, boolean serverSide) {
        this.lazyLoad = lazyLoad;
        this.serverSide = serverSide;
    }

    public abstract FieldHandlerModel getValue(String var1) throws Exception;

    public abstract FieldHandlerModel getValueWithAttributes(String var1, Map<String, String> var2) throws Exception;

    public abstract String getFieldInfo(FieldHandlerModel var1) throws Exception;

    public abstract Map<String, String> getFieldAttributes(FieldHandlerModel var1) throws Exception;
}

