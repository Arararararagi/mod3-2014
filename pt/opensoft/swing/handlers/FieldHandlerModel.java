/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.handlers;

public abstract class FieldHandlerModel {
    protected String metaInfo;
    protected String handlerClass;

    public FieldHandlerModel(String metaInfo, String handlerClass) {
        this.handlerClass = handlerClass;
        this.metaInfo = metaInfo;
    }

    public String getHandlerClass() {
        return this.handlerClass;
    }

    public void setHandlerClass(String handlerClass) {
        this.handlerClass = handlerClass;
    }

    public String getMetaInfo() {
        return this.metaInfo;
    }

    public void setMetaInfo(String metaInfo) {
        this.metaInfo = metaInfo;
    }

    public abstract String trim();

    public abstract boolean equals(Object var1);

    public abstract String toString();
}

