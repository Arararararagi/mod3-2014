/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.bindingutil;

import com.jgoodies.binding.BindingUtils;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;

public final class RadioButtonCustomAdapter
extends JToggleButton.ToggleButtonModel {
    private static final long serialVersionUID = -1373907721743372939L;
    private final ValueModel subject;
    private final Object choice;

    public RadioButtonCustomAdapter(ValueModel subject, Object choice) {
        if (subject == null) {
            throw new IllegalArgumentException("The subject must not be null.");
        }
        this.subject = subject;
        this.choice = choice;
        subject.addValueChangeListener(new SubjectValueChangeHandler());
        this.updateSelectedState();
    }

    public RadioButtonCustomAdapter(Object bean, String propertyName, Object choice) {
        if (bean == null || propertyName == null) {
            throw new IllegalArgumentException("The subject must not be null.");
        }
        this.subject = new PropertyAdapter<Object>(bean, propertyName);
        this.choice = choice;
        ((Model)bean).addPropertyChangeListener(propertyName, new SubjectValueChangeHandler());
        this.updateSelectedState();
    }

    @Override
    public void setSelected(boolean b) {
        if (!b && this.isSelected()) {
            this.subject.setValue(null);
        } else {
            this.subject.setValue(this.choice);
        }
        this.updateSelectedState();
    }

    @Override
    public void setGroup(ButtonGroup group) {
        if (group != null) {
            throw new UnsupportedOperationException("You need not and must not use a ButtonGroup with a set of RadioButtonAdapters. These form a group by sharing the same subject ValueModel.");
        }
    }

    private void updateSelectedState() {
        boolean subjectHoldsChoiceValue = BindingUtils.equals(this.choice, this.subject.getValue());
        super.setSelected(subjectHoldsChoiceValue);
    }

    private final class SubjectValueChangeHandler
    implements PropertyChangeListener {
        private SubjectValueChangeHandler() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            RadioButtonCustomAdapter.this.updateSelectedState();
        }
    }

}

