/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.model.calculadora;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.opensoft.swing.model.calculadora.TabelaCalculadora_Linha;

public class TabelaCalculadora_LinhaAdapterFormatBase
implements AdvancedTableFormat<TabelaCalculadora_Linha>,
WritableTableFormat<TabelaCalculadora_Linha> {
    @Override
    public boolean isEditable(TabelaCalculadora_Linha baseObject, int columnIndex) {
        if (columnIndex == 0) {
            return false;
        }
        return true;
    }

    @Override
    public Object getColumnValue(TabelaCalculadora_Linha line, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return line.getValor();
            }
        }
        return null;
    }

    @Override
    public TabelaCalculadora_Linha setColumnValue(TabelaCalculadora_Linha line, Object value, int columnIndex) {
        switch (columnIndex) {
            case 1: {
                line.setValor((Long)value);
                return line;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 1: {
                return Long.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 1: {
                return "Valor";
            }
        }
        return null;
    }
}

