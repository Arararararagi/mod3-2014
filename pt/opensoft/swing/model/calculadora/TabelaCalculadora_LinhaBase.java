/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.model.calculadora;

import com.jgoodies.binding.beans.Model;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.HashCodeUtil;

public class TabelaCalculadora_LinhaBase
extends Model {
    public static final String VALOR_LINK = "aUmAnexo.qQuadro1.fvalor";
    public static final String VALOR = "valor";
    private Long valor;

    public TabelaCalculadora_LinhaBase(Long valor) {
        this.valor = valor;
    }

    public TabelaCalculadora_LinhaBase() {
        this.valor = null;
    }

    @Type(value=Type.TYPE.CAMPO)
    public Long getValor() {
        return this.valor;
    }

    @Type(value=Type.TYPE.CAMPO)
    public void setValor(Long valor) {
        Long oldValue = this.valor;
        this.valor = valor;
        this.firePropertyChange("valor", oldValue, this.valor);
    }

    public int hashCode() {
        int result = 23;
        result = HashCodeUtil.hash(result, this.valor);
        return result;
    }
}

