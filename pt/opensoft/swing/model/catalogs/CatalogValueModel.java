/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.model.catalogs;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import pt.opensoft.swing.model.catalogs.ICatalogItem;

public class CatalogValueModel
extends AbstractValueModel
implements PropertyChangeListener {
    private static final long serialVersionUID = -4239019833842620917L;
    SelectionInList<Object> selectionInList;
    ValueModel beanValueModel;

    public CatalogValueModel(ValueModel beanValueModel, SelectionInList<Object> selectionInList) {
        this.beanValueModel = beanValueModel;
        this.selectionInList = selectionInList;
        this.setSelectedIndex(beanValueModel.getValue());
        this.beanValueModel.addValueChangeListener(this);
    }

    @Override
    public Object getValue() {
        if (this.beanValueModel == null) {
            return null;
        }
        return this.beanValueModel.getValue();
    }

    @Override
    public void setValue(Object newValue) {
        this.beanValueModel.setValue(((ICatalogItem)newValue).getValue());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.selectionInList != null) {
            for (int i = 0; i < this.selectionInList.getList().size(); ++i) {
                ICatalogItem item = (ICatalogItem)this.selectionInList.getList().get(i);
                if (!item.getValue().equals(evt.getNewValue())) continue;
                this.selectionInList.setSelectionIndex(i);
                break;
            }
        }
    }

    private void setSelectedIndex(Object value) {
        if (this.selectionInList == null) {
            return;
        }
        if (value == null) {
            this.selectionInList.setSelectionIndex(-1);
        } else {
            for (int i = 0; i < this.selectionInList.getList().size(); ++i) {
                ICatalogItem item = (ICatalogItem)this.selectionInList.getList().get(i);
                if (!item.getValue().equals(value)) continue;
                this.selectionInList.setSelectionIndex(i);
                break;
            }
        }
    }
}

