/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.model.catalogs;

public interface ICatalogItem {
    public static final String VALUE_PROPERTY = "value";

    public String getDescription();

    public Object getValue();
}

