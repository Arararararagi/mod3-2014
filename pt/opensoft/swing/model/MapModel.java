/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.model;

import pt.opensoft.swing.event.MapDataListener;

public interface MapModel {
    public void addMapDataListener(MapDataListener var1);

    public void removeMapDataListener(MapDataListener var1);
}

