/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import pt.opensoft.swing.components.JNumericTextField;
import pt.opensoft.util.StringUtil;

public class NumericCellEditor
extends AbstractCellEditor
implements TableCellEditor {
    private static final long serialVersionUID = -1493866649300663300L;
    protected JComponent editorComponent;
    protected EditorDelegate delegate;
    protected int clickCountToStart = 0;

    public NumericCellEditor(int totalDigits, int fractionDigits) {
        this(new JNumericTextField(totalDigits, fractionDigits));
    }

    public NumericCellEditor() {
        this(new JNumericTextField(11, 2));
    }

    public NumericCellEditor(final JNumericTextField component) {
        this.editorComponent = component;
        this.editorComponent.setBorder(BorderFactory.createEmptyBorder());
        this.delegate = new EditorDelegate(){

            @Override
            public void setValue(Object value) {
                component.setValue(value != null ? String.valueOf(value) : "");
            }

            @Override
            public Object getCellEditorValue() {
                return component.getValue();
            }
        };
    }

    public Component getComponent() {
        return this.editorComponent;
    }

    public void setClickCountToStart(int count) {
        this.clickCountToStart = count;
    }

    public int getClickCountToStart() {
        return this.clickCountToStart;
    }

    @Override
    public Object getCellEditorValue() {
        if (!StringUtil.isEmpty((String)this.delegate.getCellEditorValue())) {
            return Long.valueOf((String)this.delegate.getCellEditorValue());
        }
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return this.delegate.isCellEditable(anEvent);
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return this.delegate.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return this.delegate.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        this.delegate.cancelCellEditing();
    }

    public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row) {
        String stringValue = tree.convertValueToText(value, isSelected, expanded, leaf, row, false);
        this.delegate.setValue(stringValue);
        return this.editorComponent;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.delegate.setValue(value);
        return this.editorComponent;
    }

    protected class EditorDelegate
    implements ActionListener,
    ItemListener,
    Serializable {
        protected Object value;

        protected EditorDelegate() {
        }

        public Object getCellEditorValue() {
            return this.value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public boolean isCellEditable(EventObject anEvent) {
            if (anEvent instanceof MouseEvent) {
                return ((MouseEvent)anEvent).getClickCount() >= NumericCellEditor.this.clickCountToStart;
            }
            return true;
        }

        public boolean shouldSelectCell(EventObject anEvent) {
            return true;
        }

        public boolean startCellEditing(EventObject anEvent) {
            return true;
        }

        public boolean stopCellEditing() {
            NumericCellEditor.this.fireEditingStopped();
            return true;
        }

        public void cancelCellEditing() {
            NumericCellEditor.this.fireEditingCanceled();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            NumericCellEditor.this.stopCellEditing();
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            NumericCellEditor.this.stopCellEditing();
        }
    }

}

