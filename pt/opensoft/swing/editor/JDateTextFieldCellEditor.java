/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import pt.opensoft.swing.components.JDateTextField;
import pt.opensoft.util.Date;
import pt.opensoft.util.InvalidDate;
import pt.opensoft.util.StringUtil;

public class JDateTextFieldCellEditor
extends AbstractCellEditor
implements TableCellEditor {
    private static final long serialVersionUID = 7378679566551797824L;
    protected int clickCountToStart = 1;
    protected JDateTextField editorComponent;
    protected EditorDelegate delegate;

    public JDateTextFieldCellEditor() {
        this(new JDateTextField());
    }

    public JDateTextFieldCellEditor(final JDateTextField component) {
        this.editorComponent = component;
        this.delegate = new EditorDelegate(){

            @Override
            public void setValue(Object value) {
                component.setValue(value != null ? ((Date)value).format("yyyyMMdd") : "");
            }

            @Override
            public Object getCellEditorValue() {
                return component.getValue();
            }
        };
    }

    public Component getComponent() {
        return this.editorComponent;
    }

    public void setClickCountToStart(int count) {
        this.clickCountToStart = count;
    }

    public int getClickCountToStart() {
        return this.clickCountToStart;
    }

    @Override
    public Object getCellEditorValue() {
        if (!StringUtil.isEmpty((String)this.delegate.getCellEditorValue())) {
            try {
                Date date = new Date((String)this.delegate.getCellEditorValue(), "yyyyMMdd");
                return date;
            }
            catch (InvalidDate e) {
                return null;
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return this.delegate.isCellEditable(anEvent);
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return this.delegate.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return this.delegate.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        this.delegate.cancelCellEditing();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.delegate.setValue(value);
        if (UIManager.getBorder("Table.focusCellHighlightBorder") != null) {
            this.editorComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        }
        return this.editorComponent;
    }

    protected class EditorDelegate
    implements ActionListener,
    ItemListener,
    Serializable {
        protected Object value;

        protected EditorDelegate() {
        }

        public Object getCellEditorValue() {
            return this.value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public boolean isCellEditable(EventObject anEvent) {
            if (anEvent instanceof MouseEvent) {
                return ((MouseEvent)anEvent).getClickCount() >= JDateTextFieldCellEditor.this.clickCountToStart;
            }
            return true;
        }

        public boolean shouldSelectCell(EventObject anEvent) {
            return true;
        }

        public boolean startCellEditing(EventObject anEvent) {
            return true;
        }

        public boolean stopCellEditing() {
            JDateTextFieldCellEditor.this.fireEditingStopped();
            return true;
        }

        public void cancelCellEditing() {
            JDateTextFieldCellEditor.this.fireEditingCanceled();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JDateTextFieldCellEditor.this.stopCellEditing();
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            JDateTextFieldCellEditor.this.stopCellEditing();
        }
    }

}

