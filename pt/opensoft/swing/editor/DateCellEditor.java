/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.editor;

import com.toedter.calendar.JDateChooser;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.TableCellEditor;
import pt.opensoft.logging.Level;
import pt.opensoft.logging.Logger;
import pt.opensoft.swing.components.JDate;
import pt.opensoft.util.Date;

public class DateCellEditor
extends AbstractCellEditor
implements TableCellEditor {
    private static final long serialVersionUID = -1493866649300663300L;
    protected JComponent editorComponent;
    protected EditorDelegate delegate;
    protected int clickCountToStart = 1;

    public DateCellEditor() {
        this(new JDate());
    }

    public DateCellEditor(final JDate component) {
        this.editorComponent = component;
        ((JDateChooser)this.editorComponent).setDateFormatString("yyyy-MM-dd");
        this.delegate = new EditorDelegate(){

            @Override
            public void setValue(Object value) {
                component.setDate(value != null ? (Date)value : null);
            }

            @Override
            public Object getCellEditorValue() {
                return component.getDate() != null ? new Date(component.getDate()) : null;
            }
        };
    }

    public Component getComponent() {
        return this.editorComponent;
    }

    public void setClickCountToStart(int count) {
        this.clickCountToStart = count;
    }

    public int getClickCountToStart() {
        return this.clickCountToStart;
    }

    @Override
    public Object getCellEditorValue() {
        return this.delegate.getCellEditorValue();
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return this.delegate.isCellEditable(anEvent);
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return this.delegate.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        if (((JDate)this.editorComponent).isDateSelected()) {
            Logger.getDefault().log(Level.DEBUG, "Stop cell editing because the user selected a date.");
            return this.delegate.stopCellEditing();
        }
        if (!((JDate)this.editorComponent).getPopupMenu().isVisible()) {
            return this.delegate.stopCellEditing();
        }
        return false;
    }

    @Override
    public void cancelCellEditing() {
        if (((JDate)this.editorComponent).isDateSelected()) {
            Logger.getDefault().log(Level.DEBUG, "Cancel cell editing because the user selected a date.");
            this.delegate.cancelCellEditing();
        }
    }

    public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row) {
        String stringValue = tree.convertValueToText(value, isSelected, expanded, leaf, row, false);
        this.delegate.setValue(stringValue);
        return this.editorComponent;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.delegate.setValue(value);
        return this.editorComponent;
    }

    protected class EditorDelegate
    implements ActionListener,
    ItemListener,
    Serializable {
        protected Object value;

        protected EditorDelegate() {
        }

        public Object getCellEditorValue() {
            return this.value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public boolean isCellEditable(EventObject anEvent) {
            if (anEvent instanceof MouseEvent) {
                return ((MouseEvent)anEvent).getClickCount() >= DateCellEditor.this.clickCountToStart;
            }
            return true;
        }

        public boolean shouldSelectCell(EventObject anEvent) {
            return true;
        }

        public boolean startCellEditing(EventObject anEvent) {
            return true;
        }

        public boolean stopCellEditing() {
            DateCellEditor.this.fireEditingStopped();
            return true;
        }

        public void cancelCellEditing() {
            DateCellEditor.this.fireEditingCanceled();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            DateCellEditor.this.stopCellEditing();
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            DateCellEditor.this.stopCellEditing();
        }
    }

}

