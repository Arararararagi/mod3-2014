/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.swing.editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import pt.opensoft.swing.components.JPercentTextField;
import pt.opensoft.util.StringUtil;

public class JPercentCellEditor
extends AbstractCellEditor
implements TableCellEditor {
    private static final long serialVersionUID = 7378679566551797824L;
    protected int clickCountToStart = 1;
    protected JPercentTextField editorComponent;
    protected EditorDelegate delegate;

    public JPercentCellEditor() {
        this(new JPercentTextField());
    }

    public JPercentCellEditor(int precision) {
        this(new JPercentTextField(precision));
    }

    public JPercentCellEditor(final JPercentTextField component) {
        this.editorComponent = component;
        this.delegate = new EditorDelegate(){
            private static final long serialVersionUID = 2650470592662992931L;

            @Override
            public void setValue(Object value) {
                component.setValue(value != null ? String.valueOf(value) : "");
            }

            @Override
            public Object getCellEditorValue() {
                return component.getValue();
            }
        };
    }

    public Component getComponent() {
        return this.editorComponent;
    }

    public void setClickCountToStart(int count) {
        this.clickCountToStart = count;
    }

    public int getClickCountToStart() {
        return this.clickCountToStart;
    }

    @Override
    public Object getCellEditorValue() {
        if (!StringUtil.isEmpty((String)this.delegate.getCellEditorValue())) {
            return Long.valueOf((String)this.delegate.getCellEditorValue());
        }
        return null;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return this.delegate.isCellEditable(anEvent);
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return this.delegate.shouldSelectCell(anEvent);
    }

    @Override
    public boolean stopCellEditing() {
        return this.delegate.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
        this.delegate.cancelCellEditing();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.delegate.setValue(value);
        if (UIManager.getBorder("Table.focusCellHighlightBorder") != null) {
            this.editorComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        }
        return this.editorComponent;
    }

    protected class EditorDelegate
    implements ActionListener,
    ItemListener,
    Serializable {
        private static final long serialVersionUID = -4120207353635695819L;
        protected Object value;

        protected EditorDelegate() {
        }

        public Object getCellEditorValue() {
            return this.value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public boolean isCellEditable(EventObject anEvent) {
            if (anEvent instanceof MouseEvent) {
                return ((MouseEvent)anEvent).getClickCount() >= JPercentCellEditor.this.clickCountToStart;
            }
            return true;
        }

        public boolean shouldSelectCell(EventObject anEvent) {
            return true;
        }

        public boolean startCellEditing(EventObject anEvent) {
            return true;
        }

        public boolean stopCellEditing() {
            JPercentCellEditor.this.fireEditingStopped();
            return true;
        }

        public void cancelCellEditing() {
            JPercentCellEditor.this.fireEditingCanceled();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JPercentCellEditor.this.stopCellEditing();
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            JPercentCellEditor.this.stopCellEditing();
        }
    }

}

