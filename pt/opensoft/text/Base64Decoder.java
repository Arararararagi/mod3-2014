/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import pt.opensoft.text.Appender;
import pt.opensoft.text.Base64;
import pt.opensoft.util.CharUtil;

public class Base64Decoder
extends Base64 {
    protected static final int[] DECODE = new int[]{64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64, 64, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64, 64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64};
    private boolean removeWhitespaces = false;

    public Base64Decoder() {
    }

    public Base64Decoder(boolean removeWhitespaces) {
        this.removeWhitespaces = removeWhitespaces;
    }

    public Base64Decoder(int size) {
        super(size);
    }

    public Base64Decoder(String str) {
        super(str.length() * 3 / 4);
        this.append(str);
    }

    public Base64Decoder(String str, boolean removeWhitespaces) {
        super(str.length() * 3 / 4);
        this.removeWhitespaces = removeWhitespaces;
        this.append(str);
    }

    public Base64Decoder(Object object) {
        super(object);
    }

    @Override
    public Appender append(char ch) {
        int b = ch;
        if (ch == 61) {
            return this;
        }
        if (this.removeWhitespaces && (CharUtil.isWhiteSpace((char)ch) || CharUtil.isNewLine((char)ch))) {
            return this;
        }
        if ((b = DECODE[b]) == 64) {
            throw new IllegalArgumentException("invalid base64 char '" + (char)ch + "'");
        }
        if (this.offset == 0) {
            this.buffer = (b & 63) << 2;
            this.offset = 6;
        } else if (this.offset == 6) {
            this.buffer|=(b & 48) >> 4;
            this.string.append((char)this.buffer);
            this.buffer = (b & 15) << 4;
            this.offset = 4;
        } else if (this.offset == 4) {
            this.buffer|=(b & 60) >> 2;
            this.string.append((char)this.buffer);
            this.buffer = (b & 3) << 6;
            this.offset = 2;
        } else {
            this.buffer|=b & 63;
            this.string.append((char)this.buffer);
            this.buffer = 0;
            this.offset = 0;
        }
        return this;
    }
}

