/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.oroinc.text.regex.MalformedPatternException
 *  com.oroinc.text.regex.MatchResult
 *  com.oroinc.text.regex.Pattern
 *  com.oroinc.text.regex.PatternMatcher
 *  com.oroinc.text.regex.Perl5Compiler
 *  com.oroinc.text.regex.Perl5Matcher
 *  com.oroinc.text.regex.Perl5Substitution
 *  com.oroinc.text.regex.Substitution
 *  com.oroinc.text.regex.Util
 */
package pt.opensoft.text;

import com.oroinc.text.regex.MalformedPatternException;
import com.oroinc.text.regex.MatchResult;
import com.oroinc.text.regex.Pattern;
import com.oroinc.text.regex.PatternMatcher;
import com.oroinc.text.regex.Perl5Compiler;
import com.oroinc.text.regex.Perl5Matcher;
import com.oroinc.text.regex.Perl5Substitution;
import com.oroinc.text.regex.Substitution;
import com.oroinc.text.regex.Util;
import java.util.Vector;

public class Regex {
    public static final int CASE_INSENSITIVE_MASK = 1;
    public static final int DEFAULT_MASK = 0;
    public static final int MULTILINE_MASK = 8;
    public static final int READ_ONLY_MASK = 32768;
    public static final int SINGLELINE_MASK = 16;
    public static final int SPLIT_ALL = 0;
    public static final int SUBSTITUTE_ALL = -1;
    protected String expression = null;
    protected Pattern pattern = null;
    protected PatternMatcher matcher = null;
    protected MatchResult result = null;

    public Regex(String expression) {
        this(expression, 0);
    }

    public Regex(String expression, int options) {
        this.expression = expression;
        Perl5Compiler compiler = new Perl5Compiler();
        try {
            this.pattern = compiler.compile(expression, options);
        }
        catch (MalformedPatternException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        this.matcher = new Perl5Matcher();
    }

    public String getPattern() {
        return this.pattern.getPattern();
    }

    public boolean contains(String string) {
        boolean contains = this.matcher.contains(string, this.pattern);
        this.result = this.matcher.getMatch();
        return contains;
    }

    public boolean matches(String string) {
        boolean matches = this.matcher.matches(string, this.pattern);
        this.result = this.matcher.getMatch();
        return matches;
    }

    public Vector split(String source) {
        return Util.split((PatternMatcher)this.matcher, (Pattern)this.pattern, (String)source);
    }

    public Vector split(String source, int limit) {
        return Util.split((PatternMatcher)this.matcher, (Pattern)this.pattern, (String)source, (int)limit);
    }

    public String getGroup(int group) {
        if (this.result == null) {
            return null;
        }
        return this.result.group(group);
    }

    public int groups() {
        if (this.result == null) {
            return -1;
        }
        return this.result.groups();
    }

    public String[] getGroups() {
        if (this.result == null) {
            return null;
        }
        String[] groups = new String[this.result.groups()];
        for (int i = 0; i < this.result.groups(); ++i) {
            groups[i] = this.result.group(i);
        }
        return groups;
    }

    public String substitute(String source, String subst) {
        Perl5Substitution perlSubst = new Perl5Substitution(subst);
        return Util.substitute((PatternMatcher)this.matcher, (Pattern)this.pattern, (Substitution)perlSubst, (String)source);
    }

    public String substitute(String source, String subst, int limit) {
        Perl5Substitution perlSubst = new Perl5Substitution(subst);
        return Util.substitute((PatternMatcher)this.matcher, (Pattern)this.pattern, (Substitution)perlSubst, (String)source, (int)limit);
    }

    public String substitute(String source, String subst, int options, int limit) {
        Perl5Substitution perlSubst = new Perl5Substitution(subst, options);
        return Util.substitute((PatternMatcher)this.matcher, (Pattern)this.pattern, (Substitution)perlSubst, (String)source, (int)limit);
    }

    public String toString() {
        return this.expression;
    }
}

