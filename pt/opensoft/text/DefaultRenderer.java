/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import java.io.File;
import java.io.IOException;
import pt.opensoft.io.FileUtil;
import pt.opensoft.text.AbstractAppender;
import pt.opensoft.text.Appender;
import pt.opensoft.text.LazyRenderer;
import pt.opensoft.text.Renderer;
import pt.opensoft.text.StringAppender;
import pt.opensoft.util.ByteUtil;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.SystemParameters;

public class DefaultRenderer
extends AbstractAppender
implements Renderer {
    public static File ROOT = new File(".");
    protected Appender appender;

    public static void setRoot(File root) {
        ROOT = root;
    }

    protected static String getPage(String filename) throws IOException {
        File file = new File(ROOT, filename);
        return FileUtil.readFile(file);
    }

    public DefaultRenderer() {
        this(new StringAppender(SystemParameters.FILE_BLOCK_SIZE));
    }

    protected DefaultRenderer(Appender appender) {
        this.appender = appender;
    }

    public DefaultRenderer(int size) {
        this(new StringAppender(size));
    }

    public DefaultRenderer(String text) {
        this(new StringAppender(text));
    }

    public void loadPage(String filename) throws IOException {
        File file = new File(ROOT, filename);
        FileUtil.readFile(file, (Appender)this);
    }

    public void loadTemplate(String filename) throws IOException {
        this.loadPage(filename);
        this.setInitial(this.toString());
    }

    public void clearTemplate() {
        this.setInitial(null);
        this.clear();
    }

    public Renderer includePage(String filename) throws IOException {
        String content = DefaultRenderer.getPage(filename);
        this.append(content);
        return this;
    }

    public Renderer includePage(String tag, String filename) throws IOException {
        String content = DefaultRenderer.getPage(filename);
        this.replace(tag, content);
        return this;
    }

    @Override
    public Appender append(char ch) {
        this.appender.append(ch);
        return this;
    }

    @Override
    public Appender append(char[] chars, int off, int len) {
        this.appender.append(chars, off, len);
        return this;
    }

    @Override
    public Appender append(String str) {
        this.appender.append(str);
        return this;
    }

    @Override
    public Appender append(String str, int off, int len) {
        this.appender.append(str, off, len);
        return this;
    }

    @Override
    public String clear() {
        return this.appender.clear();
    }

    @Override
    public int length() {
        return this.appender.length();
    }

    @Override
    public Appender newLine() {
        return this.appender.newLine();
    }

    @Override
    public Renderer replace(String search, boolean replace) {
        return this.replace(search, String.valueOf(replace));
    }

    @Override
    public Renderer replace(String search, byte replace) {
        return this.replace(search, ByteUtil.toChar(replace));
    }

    @Override
    public Renderer replace(String search, char replace) {
        return this.replace(search, String.valueOf(replace));
    }

    @Override
    public Renderer replace(String search, double replace) {
        return this.replace(search, String.valueOf(replace));
    }

    @Override
    public Renderer replace(String search, float replace) {
        return this.replace(search, String.valueOf(replace));
    }

    @Override
    public Renderer replace(String search, int replace) {
        return this.replace(search, String.valueOf(replace));
    }

    @Override
    public Renderer replace(String search, long replace) {
        return this.replace(search, String.valueOf(replace));
    }

    @Override
    public Renderer replace(String search, short replace) {
        return this.replace(search, String.valueOf(replace));
    }

    @Override
    public Renderer replace(String search, Object replace) {
        String str = replace != null ? replace.toString() : null;
        return this.replace(search, str);
    }

    @Override
    public Renderer replace(String search, String replace) {
        if (search == null) {
            return this;
        }
        if (this.appender instanceof Renderer) {
            return ((Renderer)this.appender).replace(search, replace);
        }
        String str = this.appender.toString();
        this.appender.clear();
        this.appender.append(StringUtil.replace(str, search, replace));
        return this;
    }

    @Override
    public String reset() {
        return this.appender.reset();
    }

    @Override
    protected void reset(int size) {
        if (this.appender == null) {
            return;
        }
        ((AbstractAppender)this.appender).reset(size);
    }

    @Override
    protected void reset(String initial) {
        if (this.appender == null) {
            return;
        }
        ((AbstractAppender)this.appender).reset(initial);
    }

    @Override
    protected void setInitial(String str) {
        if (this.appender == null) {
            return;
        }
        ((AbstractAppender)this.appender).setInitial(str);
    }

    public Renderer text(Object text) {
        return this.text(text.toString());
    }

    public Renderer text(String text) {
        return this.text(text, true);
    }

    public Renderer text(String text, boolean newLine) {
        this.append(text);
        if (newLine) {
            this.newLine();
        }
        return this;
    }

    public String toString() {
        if (this.appender == null) {
            return null;
        }
        return this.appender.toString();
    }

    public void setKeepTags(boolean keepTags) {
        if (!(this.appender instanceof LazyRenderer)) {
            throw new IllegalAccessError("not implemented!");
        }
        ((LazyRenderer)this.appender).setKeepTags(keepTags);
    }
}

