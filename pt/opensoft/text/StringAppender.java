/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import pt.opensoft.text.AbstractAppender;
import pt.opensoft.text.Appender;

public class StringAppender
extends AbstractAppender {
    protected StringBuffer string;

    public StringAppender() {
        super(2048);
    }

    public StringAppender(int size) {
        super(size);
    }

    public StringAppender(String str) {
        super(str);
    }

    public StringAppender(Object object) {
        super(object);
    }

    @Override
    public Appender append(char ch) {
        this.string.append(ch);
        return this;
    }

    @Override
    public Appender append(char[] chars, int off, int len) {
        this.string.append(chars, off, len);
        return this;
    }

    @Override
    public Appender append(String str) {
        this.string.append(str);
        return this;
    }

    @Override
    public int length() {
        return this.string.length();
    }

    @Override
    protected void reset(int size) {
        this.string = new StringBuffer(size);
    }

    public String toString() {
        return this.string.toString();
    }
}

