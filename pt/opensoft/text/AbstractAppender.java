/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import java.util.Enumeration;
import java.util.Vector;
import pt.opensoft.field.Getter118;
import pt.opensoft.field.NamedGetter118;
import pt.opensoft.text.Appender;
import pt.opensoft.util.ByteUtil;

public abstract class AbstractAppender
implements Appender {
    private String initial = null;
    private int initialSize = 2048;

    protected AbstractAppender() {
        this(2048);
    }

    protected AbstractAppender(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("size must be > 0");
        }
        this.initialSize = size;
        this.reset(size);
    }

    protected AbstractAppender(String str) {
        this(str.length());
        this.initial = str;
        this.reset(this.initial);
    }

    protected AbstractAppender(Object object) {
        this(object.toString());
    }

    @Override
    public Appender append(boolean value) {
        return this.append(String.valueOf(value));
    }

    @Override
    public Appender append(byte b) {
        return this.append(ByteUtil.toChar(b));
    }

    @Override
    public Appender append(byte[] bytes) {
        return this.append(bytes, 0, bytes.length);
    }

    @Override
    public Appender append(byte[] bytes, int off, int len) {
        return this.append(ByteUtil.toCharArray(bytes, off, len));
    }

    @Override
    public abstract Appender append(char var1);

    @Override
    public Appender append(char[] chars) {
        return this.append(chars, 0, chars.length);
    }

    @Override
    public Appender append(char[] chars, int off, int len) {
        for (int i = 0; i < len; ++i) {
            this.append(chars[i]);
        }
        return this;
    }

    @Override
    public Appender append(double value) {
        return this.append(String.valueOf(value));
    }

    @Override
    public Appender append(float value) {
        return this.append(String.valueOf(value));
    }

    public Appender append(Getter118 field) {
        if (field.isNull()) {
            return this;
        }
        return this.append(field.getValue());
    }

    @Override
    public Appender append(long value) {
        return this.append(String.valueOf(value));
    }

    @Override
    public Appender append(int i) {
        return this.append(String.valueOf(i));
    }

    public Appender append(Vector list) {
        Enumeration<E> en = list.elements();
        while (en.hasMoreElements()) {
            this.append(en.nextElement());
        }
        return this;
    }

    public Appender append(NamedGetter118 getter) {
        if (getter.isEmpty()) {
            return this;
        }
        return this.append(getter.getValuesVector());
    }

    @Override
    public Appender append(Object object) {
        if (object == null) {
            return this;
        }
        if (object instanceof String) {
            return this.append((String)object);
        }
        if (object instanceof Vector) {
            return this.append((Vector)object);
        }
        if (object instanceof Getter118) {
            return this.append((Getter118)object);
        }
        if (object instanceof NamedGetter118) {
            return this.append((NamedGetter118)object);
        }
        return this.append(object.toString());
    }

    @Override
    public Appender append(String str) {
        return this.append(str.toCharArray(), 0, str.length());
    }

    @Override
    public Appender append(String str, int off, int len) {
        char[] chars = new char[len];
        str.getChars(off, off + len, chars, 0);
        return this.append(chars, 0, chars.length);
    }

    @Override
    public Appender append(short value) {
        return this.append(String.valueOf(value));
    }

    @Override
    public String clear() {
        String value = this.toString();
        this.reset(this.initialSize);
        return value;
    }

    @Override
    public Appender newLine() {
        return this.append(LINE_SEPARATOR);
    }

    @Override
    public Appender repeat(boolean value, int times) {
        return this.repeat(String.valueOf(value), times);
    }

    @Override
    public Appender repeat(byte value, int times) {
        return this.repeat(ByteUtil.toChar(value), times);
    }

    @Override
    public Appender repeat(byte[] value, int times) {
        return this.repeat(ByteUtil.toCharArray(value), times);
    }

    @Override
    public Appender repeat(byte[] value, int off, int len, int times) {
        return this.repeat(ByteUtil.toCharArray(value, off, len), times);
    }

    @Override
    public Appender repeat(char value, int times) {
        char[] chars = new char[times];
        for (int i = 0; i < times; ++i) {
            chars[i] = value;
        }
        return this.append(chars);
    }

    @Override
    public Appender repeat(char[] value, int times) {
        for (int i = 0; i < times; ++i) {
            this.append(value);
        }
        return this;
    }

    @Override
    public Appender repeat(char[] value, int off, int len, int times) {
        for (int i = 0; i < times; ++i) {
            this.append(value, off, len);
        }
        return this;
    }

    @Override
    public Appender repeat(double value, int times) {
        return this.repeat(String.valueOf(value), times);
    }

    @Override
    public Appender repeat(float value, int times) {
        return this.repeat(String.valueOf(value), times);
    }

    @Override
    public Appender repeat(long value, int times) {
        return this.repeat(String.valueOf(value), times);
    }

    @Override
    public Appender repeat(int value, int times) {
        return this.repeat(String.valueOf(value), times);
    }

    @Override
    public Appender repeat(Object value, int times) {
        return this.repeat(value.toString(), times);
    }

    @Override
    public Appender repeat(short value, int times) {
        return this.repeat(String.valueOf(value), times);
    }

    @Override
    public Appender repeat(String str, int times) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < times; ++i) {
            this.append(chars);
        }
        return this;
    }

    @Override
    public Appender repeat(String str, int off, int len, int times) {
        char[] chars = new char[len];
        str.getChars(off, off + len, chars, 0);
        for (int i = 0; i < times; ++i) {
            this.append(chars);
        }
        return this;
    }

    @Override
    public String reset() {
        String value = this.toString();
        if (this.initial != null && this.initialSize < this.initial.length()) {
            this.initialSize = this.initial.length();
        }
        if (value != null && this.initialSize < value.length()) {
            this.initialSize = value.length();
        }
        this.reset(this.initial);
        return value;
    }

    protected abstract void reset(int var1);

    protected void reset(String initial) {
        int length = initial != null ? initial.length() : 0;
        this.reset(Math.max(this.initialSize, length));
        if (initial != null) {
            this.append(initial);
        }
    }

    protected void setInitial(String str) {
        this.initialSize = str != null ? str.length() : 0;
        this.initial = str;
    }
}

