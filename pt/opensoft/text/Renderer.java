/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import pt.opensoft.text.Appender;

public interface Renderer
extends Appender {
    public Renderer replace(String var1, boolean var2);

    public Renderer replace(String var1, byte var2);

    public Renderer replace(String var1, char var2);

    public Renderer replace(String var1, double var2);

    public Renderer replace(String var1, float var2);

    public Renderer replace(String var1, int var2);

    public Renderer replace(String var1, long var2);

    public Renderer replace(String var1, Object var2);

    public Renderer replace(String var1, short var2);

    public Renderer replace(String var1, String var2);
}

