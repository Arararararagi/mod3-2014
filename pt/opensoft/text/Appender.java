/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import pt.opensoft.util.SystemParameters;

public interface Appender {
    public static final int DEFAULT_SIZE = 2048;
    public static final String LINE_SEPARATOR = SystemParameters.LINE_SEPARATOR;

    public Appender append(boolean var1);

    public Appender append(byte var1);

    public Appender append(byte[] var1);

    public Appender append(byte[] var1, int var2, int var3);

    public Appender append(char var1);

    public Appender append(char[] var1);

    public Appender append(char[] var1, int var2, int var3);

    public Appender append(double var1);

    public Appender append(float var1);

    public Appender append(long var1);

    public Appender append(Object var1);

    public Appender append(int var1);

    public Appender append(short var1);

    public Appender append(String var1);

    public Appender append(String var1, int var2, int var3);

    public String clear();

    public int length();

    public Appender newLine();

    public Appender repeat(boolean var1, int var2);

    public Appender repeat(byte var1, int var2);

    public Appender repeat(byte[] var1, int var2);

    public Appender repeat(byte[] var1, int var2, int var3, int var4);

    public Appender repeat(char var1, int var2);

    public Appender repeat(char[] var1, int var2);

    public Appender repeat(char[] var1, int var2, int var3, int var4);

    public Appender repeat(double var1, int var3);

    public Appender repeat(float var1, int var2);

    public Appender repeat(long var1, int var3);

    public Appender repeat(Object var1, int var2);

    public Appender repeat(int var1, int var2);

    public Appender repeat(short var1, int var2);

    public Appender repeat(String var1, int var2);

    public Appender repeat(String var1, int var2, int var3, int var4);

    public String reset();
}

