/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import pt.opensoft.text.Appender;
import pt.opensoft.text.DefaultRenderer;
import pt.opensoft.text.Renderer;

public class LazyRenderer
extends DefaultRenderer {
    protected static final String DEFAULT_INIT_TAG = "${";
    protected static final String DEFAULT_END_TAG = "}";
    protected static final char DEFAULT_SPECIAL_TAG = '#';
    protected String initTag = "${";
    protected String endTag = "}";
    protected char specialTag = 35;
    protected Map tags;
    protected boolean keepTags = true;

    public LazyRenderer() {
    }

    public LazyRenderer(int size) {
        super(size);
    }

    public LazyRenderer(String filename) throws IOException {
        this.loadPage(filename);
    }

    @Override
    public void setKeepTags(boolean keepTags) {
        this.keepTags = keepTags;
    }

    @Override
    public Renderer replace(String tag, String value) {
        if (this.tags == null) {
            this.tags = new HashMap();
        }
        if (value == null) {
            this.tags.put(tag, "");
        } else {
            this.tags.put(tag, value);
        }
        return this;
    }

    @Override
    public String reset() {
        int init;
        String page = this.appender.clear();
        if (this.tags == null) {
            return page;
        }
        int pos = 0;
        int initSize = this.initTag.length();
        int endSize = this.endTag.length();
        while ((init = page.indexOf(this.initTag, pos)) != -1) {
            int end = page.indexOf(this.endTag, init + initSize);
            this.appender.append(page.substring(pos, init));
            String name = page.substring(init + initSize, end);
            boolean special = name.charAt(0) == this.specialTag;
            String value = special ? (String)this.tags.get(name.substring(1)) : (String)this.tags.get(name);
            if (value != null) {
                if (special) {
                    if (value.trim().length() != 0) {
                        this.appender.append("value=\"").append(value).append("\"");
                    }
                } else {
                    this.appender.append(value);
                }
            } else if (this.keepTags) {
                this.appender.append(page.substring(init, end + endSize));
            }
            pos = end + endSize;
        }
        this.appender.append(page.substring(pos));
        this.tags = null;
        return this.appender.reset();
    }

    public void setTags(String init, String end) {
        this.initTag = init;
        this.endTag = end;
    }
}

