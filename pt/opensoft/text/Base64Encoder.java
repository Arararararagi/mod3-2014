/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import pt.opensoft.text.Appender;
import pt.opensoft.text.Base64;

public class Base64Encoder
extends Base64 {
    protected static final char[] ENCODE = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    public Base64Encoder() {
    }

    public Base64Encoder(int size) {
        super(size);
    }

    public Base64Encoder(String str) {
        super(str.length() * 4 / 3 + 2);
        this.append(str);
    }

    public Base64Encoder(Object object) {
        super(object);
    }

    @Override
    public Appender append(char ch) {
        char b = ch;
        if (this.offset == 0) {
            int b1 = (b & 252) >> 2;
            this.buffer = (b & 3) << 4;
            this.offset = 2;
            this.string.append(ENCODE[b1]);
        } else if (this.offset == 2) {
            int b1 = this.buffer | (b & 240) >> 4;
            this.buffer = (b & 15) << 2;
            this.offset = 4;
            this.string.append(ENCODE[b1]);
        } else if (this.offset == 4) {
            int b1 = this.buffer | (b & 192) >> 6;
            int b2 = b & 63;
            this.buffer = 0;
            this.offset = 0;
            this.string.append(ENCODE[b1]);
            this.string.append(ENCODE[b2]);
        }
        return this;
    }

    protected String closing() {
        StringBuffer closing = new StringBuffer();
        if (this.offset == 2) {
            closing.append(ENCODE[this.buffer]);
            closing.append('=');
            closing.append('=');
        } else if (this.offset == 4) {
            closing.append(ENCODE[this.buffer]);
            closing.append('=');
        }
        return closing.length() == 0 ? null : closing.toString();
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer(this.string.length() + 2);
        buffer.append(this.string.toString());
        String closing = this.closing();
        if (closing != null) {
            buffer.append(closing);
        }
        return buffer.toString();
    }
}

