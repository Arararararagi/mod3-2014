/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.text;

import pt.opensoft.text.Appender;
import pt.opensoft.text.Base64Decoder;
import pt.opensoft.text.Base64Encoder;
import pt.opensoft.text.StringAppender;
import pt.opensoft.util.ByteUtil;
import pt.opensoft.util.StringUtil;

public abstract class Base64
extends StringAppender {
    protected static final char END_CHAR = '=';
    protected int buffer = 0;
    protected int offset = 0;

    protected Base64() {
    }

    public Base64(int size) {
        super(size);
    }

    public Base64(String str) {
        super(str.length());
        this.append(str);
    }

    public Base64(Object object) {
        super(object);
    }

    @Override
    public Appender append(char[] chars, int off, int len) {
        for (int i = 0; i < chars.length; ++i) {
            this.append(chars[i]);
        }
        return this;
    }

    @Override
    public Appender append(String str) {
        for (int i = 0; i < str.length(); ++i) {
            this.append(str.charAt(i));
        }
        return this;
    }

    public static String decode(String string) {
        return Base64.decode(string, false);
    }

    public static String decode(String string, boolean removeWhiteSpace) {
        Base64Decoder decoder = new Base64Decoder(string, removeWhiteSpace);
        return decoder.toString();
    }

    public static String encode(String string) {
        Base64Encoder encoder = new Base64Encoder(string);
        return encoder.toString();
    }

    public static byte[] decodeBuffer(String string) {
        return Base64.decodeBuffer(string, false);
    }

    public static byte[] decodeBuffer(String string, boolean removeWhiteSpace) {
        Base64Decoder decoder = new Base64Decoder(string, removeWhiteSpace);
        return StringUtil.toByteArray(decoder.toString());
    }

    public static String encodeBuffer(byte[] byteArray) {
        Base64Encoder encoder = new Base64Encoder(ByteUtil.toString(byteArray));
        return encoder.toString();
    }

    @Override
    public int length() {
        return this.toString().length();
    }
}

