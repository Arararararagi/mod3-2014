/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.logging;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Stack;
import pt.opensoft.logging.Formater;
import pt.opensoft.logging.Level;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.NamedObject;

public class Handler
extends NamedObject {
    protected static Handler DEFAULT = new Handler();
    protected PrintWriter consoleWriter;
    protected Formater formater = null;
    protected boolean logToConsole = true;

    public static Handler getDefault() {
        return DEFAULT;
    }

    public static Handler setDefault(String handler) {
        return Handler.setDefault(Handler.parse(handler));
    }

    public static Handler setDefault(Handler handler) {
        DEFAULT = handler;
        return DEFAULT;
    }

    public static Handler parse(String name) {
        if (name == null) {
            return Handler.getDefault();
        }
        if (name.equalsIgnoreCase("default")) {
            return Handler.getDefault();
        }
        if (name.indexOf(".") == -1) {
            name = "pt.opensoft.logging." + name;
        }
        return (Handler)ClassUtil.newInstance(name);
    }

    public Handler() {
        this("console", Formater.getDefault());
    }

    public Handler(String name, Formater formater) {
        this(name, formater, true);
    }

    public Handler(String name, Formater formater, boolean logToConsole) {
        super(name);
        this.setFormater(formater);
        this.setLogToConsole(logToConsole);
    }

    public Formater getFormater() {
        return this.formater;
    }

    public boolean isLoggingToConsole() {
        return this.logToConsole;
    }

    public void publish(Level level, DateTime date, long millis, Stack stack, Object msg, Throwable thrown) {
        String formated = this.formater.format(level, date, millis, stack, msg, thrown);
        this.print(formated);
    }

    protected void print(String formated) {
        if (this.logToConsole) {
            this.consoleWriter.println(formated);
        }
    }

    public void setFormater(Formater formater) {
        this.formater = formater;
    }

    public void setLogToConsole(boolean logToConsole) {
        this.logToConsole = logToConsole;
        if (this.logToConsole) {
            try {
                this.consoleWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter((OutputStream)System.out, "ISO8859_1")), true);
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        } else {
            this.consoleWriter = null;
        }
    }
}

