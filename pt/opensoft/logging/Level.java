/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.logging;

import pt.opensoft.util.NamedObject;

public class Level
extends NamedObject {
    public static final Level ALL = new Level("ALL", Integer.MAX_VALUE);
    public static final Level FINEST = new Level("FINEST", 7);
    public static final Level FINER = new Level("FINER", 6);
    public static final Level FINE = new Level("FINE", 5);
    public static final Level DEBUG = new Level("DEBUG", 4);
    public static final Level INFO = new Level("INFO", 3);
    public static final Level WARNING = new Level("WARNING", 2);
    public static final Level ERROR = new Level("ERROR", 1);
    public static final Level OFF = new Level("OFF", 0);
    protected static Level DEFAULT = INFO;
    protected static final Level[] LEVELS = new Level[]{OFF, ERROR, WARNING, INFO, DEBUG, FINE, FINER, FINEST};
    protected int level = 0;

    public static Level getDefault() {
        return DEFAULT;
    }

    public static Level setDefault(String level) {
        return Level.setDefault(Level.parse(level));
    }

    public static Level setDefault(Level level) {
        DEFAULT = level;
        return DEFAULT;
    }

    protected Level(String name, int level) {
        super(name);
        this.setLevel(level);
    }

    public int getLevel() {
        return this.level;
    }

    public static Level parse(String name) {
        if (name == null) {
            return Level.getDefault();
        }
        if (name.equalsIgnoreCase("default")) {
            return Level.getDefault();
        }
        Level level = INFO;
        for (int i = 0; i < LEVELS.length; ++i) {
            if (!name.equalsIgnoreCase(LEVELS[i].getName())) continue;
            level = LEVELS[i];
            break;
        }
        return level;
    }

    public void setLevel(int level) {
        if (level < 0) {
            throw new IllegalArgumentException("invalid level: " + level);
        }
        this.level = level;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}

