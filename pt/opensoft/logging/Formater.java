/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.logging;

import java.util.Stack;
import pt.opensoft.logging.Level;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.NamedObject;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.SystemParameters;

public class Formater
extends NamedObject {
    protected static Formater DEFAULT = new Formater();
    protected char leftBrace = 91;
    protected char rightBrace = 93;
    protected String braces = null;
    protected String dateFormat = null;
    protected int millisSize = 0;

    public static Formater getDefault() {
        return DEFAULT;
    }

    public static Formater parse(String name) {
        if (name == null) {
            return Formater.getDefault();
        }
        if (name.equalsIgnoreCase("default")) {
            return Formater.getDefault();
        }
        if (name.indexOf(".") == -1) {
            name = "pt.opensoft.logging." + name;
        }
        return (Formater)ClassUtil.newInstance(name);
    }

    public static Formater setDefault(String name) {
        return Formater.setDefault(Formater.parse(name));
    }

    public static Formater setDefault(Formater formater) {
        DEFAULT = formater;
        return DEFAULT;
    }

    public Formater() {
        this("default", "[]", "yyyy-MM-dd HH:mm:ss", 4);
    }

    public Formater(String name, String braces, String dateFormat, int millisSize) {
        super(name);
        this.setBraces(braces);
        this.setDateFormat(dateFormat);
        this.setMillisSize(millisSize);
    }

    public String format(Level level, DateTime date, long millis, Stack stack, Object msg, Throwable thrown) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(this.leftBrace);
        buffer.append(level.toString().charAt(0));
        buffer.append(this.rightBrace);
        buffer.append(' ');
        if (date != null) {
            buffer.append(this.leftBrace);
            buffer.append(date.toString());
            buffer.append(this.rightBrace);
            buffer.append(' ');
        }
        if (millis >= 0) {
            String m = Long.toString(millis);
            buffer.append(this.leftBrace);
            buffer.append(StringUtil.prependChars(m, ' ', this.millisSize));
            buffer.append(this.rightBrace);
            buffer.append(' ');
        } else {
            buffer.append(StringUtil.prependChars("", ' ', this.millisSize + 3));
        }
        if (stack != null && stack.size() > 0) {
            buffer.append(this.leftBrace);
            for (int i = 0; i < stack.size(); ++i) {
                if (i > 0) {
                    buffer.append(this.rightBrace).append(' ').append(this.leftBrace);
                }
                buffer.append(stack.elementAt(i));
            }
            buffer.append(this.rightBrace);
            buffer.append(' ');
        }
        if (msg != null) {
            buffer.append(msg.toString());
        }
        if (thrown != null) {
            if (msg != null) {
                buffer.append(SystemParameters.LINE_SEPARATOR);
            }
            buffer.append(StringUtil.toString(thrown));
        }
        return buffer.toString();
    }

    public String getBraces() {
        return this.braces;
    }

    public String getDateFormat() {
        return this.dateFormat;
    }

    public char getLeftBrace() {
        return this.leftBrace;
    }

    public char getRightBrace() {
        return this.rightBrace;
    }

    public int getMillisSize() {
        return this.millisSize;
    }

    public void setBraces(String braces) {
        this.leftBrace = braces.charAt(0);
        this.rightBrace = braces.charAt(1);
        this.braces = braces;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public void setMillisSize(int millisSize) {
        this.millisSize = millisSize;
    }
}

