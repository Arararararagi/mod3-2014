/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.logging;

import java.util.List;
import java.util.Stack;
import org.apache.commons.logging.Log;
import pt.opensoft.logging.Handler;
import pt.opensoft.logging.Level;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.NamedObject;

public class Logger
extends NamedObject
implements Log {
    public static final String NULL_STRING = "null";
    private static Logger DEFAULT = new Logger();
    protected Level level = null;
    protected Handler[] handlers = new Handler[0];
    protected Stack stack = null;

    public static Logger getDefault() {
        return DEFAULT;
    }

    public static Logger parse(String name) {
        if (name == null) {
            return Logger.getDefault();
        }
        if (name.equalsIgnoreCase("default")) {
            return Logger.getDefault();
        }
        if (!name.contains((CharSequence)".")) {
            name = "pt.opensoft.logging." + name;
        }
        return (Logger)ClassUtil.newInstance(name);
    }

    public static Logger setDefault(String name) {
        return Logger.setDefault(Logger.parse(name));
    }

    public static Logger setDefault(Logger logger) {
        DEFAULT = logger;
        return DEFAULT;
    }

    public static Logger getLogger(Class sourceClass) {
        return Logger.getLogger(sourceClass.getName());
    }

    public static Logger getLogger(String name) {
        return Logger.getDefault();
    }

    public Logger() {
        this("console", Level.getDefault(), Handler.getDefault());
    }

    public Logger(String name) {
        this("console", Level.getDefault(), Handler.getDefault());
        this.push(name);
    }

    protected Logger(Logger logger) {
        this(logger.getName(), logger.getLevel(), logger.getHandlers());
        if (logger.stack != null) {
            this.stack = (Stack)logger.stack.clone();
        }
    }

    protected Logger(String name, Level level, Handler handler) {
        super(name);
        this.setLevel(level);
        this.addHandler(handler);
    }

    protected Logger(String name, Level level, Handler[] handlers) {
        super(name);
        this.setLevel(level);
        this.handlers = handlers;
    }

    public void clear() {
        if (this.stack == null) {
            return;
        }
        this.stack.clear();
        this.stack = null;
    }

    public void all(Object msg) {
        this.log(Level.ALL, msg);
    }

    public void all(long millis, Object msg) {
        this.log(Level.ALL, millis, msg);
    }

    @Override
    public void debug(Object msg) {
        this.log(Level.DEBUG, msg);
    }

    public void debug(long millis, Object msg) {
        this.log(Level.DEBUG, millis, msg);
    }

    @Override
    public void error(Object msg) {
        this.log(Level.ERROR, msg);
    }

    public void error(long millis, Object msg) {
        this.log(Level.ERROR, millis, msg);
    }

    public void error(long millis, Throwable thrown) {
        this.log(Level.ERROR, millis, thrown);
    }

    @Override
    public void error(Object msg, Throwable thrown) {
        this.log(Level.ERROR, msg, thrown);
    }

    public void error(long millis, Object msg, Throwable thrown) {
        this.log(Level.ERROR, millis, msg, thrown);
    }

    public void fine(Object msg) {
        this.log(Level.FINE, msg);
    }

    public void fine(long millis, Object msg) {
        this.log(Level.FINE, millis, msg);
    }

    public void finer(Object msg) {
        this.log(Level.FINER, msg);
    }

    public void finer(long millis, Object msg) {
        this.log(Level.FINER, millis, msg);
    }

    public void finest(Object msg) {
        this.log(Level.FINEST, msg);
    }

    public void finest(long millis, Object msg) {
        this.log(Level.FINEST, millis, msg);
    }

    public Handler[] getHandlers() {
        return this.handlers;
    }

    public Level getLevel() {
        return this.level;
    }

    public synchronized Logger getWrapper() {
        return new Logger(this);
    }

    @Override
    public void info(Object msg) {
        this.log(Level.INFO, msg);
    }

    public void info(long millis, Object msg) {
        this.log(Level.INFO, millis, msg);
    }

    public boolean isOff() {
        return this.level.getLevel() == Level.OFF.getLevel();
    }

    @Override
    public boolean isDebugEnabled() {
        return this.level.getLevel() >= Level.DEBUG.getLevel();
    }

    @Override
    public boolean isErrorEnabled() {
        return this.level.getLevel() >= Level.ERROR.getLevel();
    }

    public boolean isFineEnabled() {
        return this.level.getLevel() >= Level.FINE.getLevel();
    }

    public boolean isFinerEnabled() {
        return this.level.getLevel() >= Level.FINER.getLevel();
    }

    public boolean isFinestEnabled() {
        return this.level.getLevel() >= Level.FINEST.getLevel();
    }

    @Override
    public boolean isInfoEnabled() {
        return this.level.getLevel() >= Level.INFO.getLevel();
    }

    @Override
    public boolean isWarnEnabled() {
        return this.level.getLevel() >= Level.WARNING.getLevel();
    }

    public void log(Level level, Object msg) {
        this.log(level, -1, msg, null);
    }

    public void log(Level level, long millis, Object msg) {
        this.log(level, millis, msg, null);
    }

    public void log(Level level, long millis, Throwable thrown) {
        this.log(level, millis, null, thrown);
    }

    public void log(Level level, Object msg, Throwable thrown) {
        this.log(level, -1, msg, thrown);
    }

    public synchronized void log(Level l, long millis, Object msg, Throwable thrown) {
        if (l.getLevel() > this.level.getLevel()) {
            return;
        }
        DateTime now = new DateTime();
        for (Handler handler : this.handlers) {
            handler.publish(l, now, millis, this.stack, msg, thrown);
        }
    }

    public Object pop() {
        if (this.stack == null) {
            return null;
        }
        return this.stack.pop();
    }

    public void push(String name, List<?> values) {
        this.push(name, ListUtil.toString(values, ":"));
    }

    public void push(String name, Object value) {
        int nameLength = name != null ? name.length() : "null".length();
        int valueLength = value != null ? value.toString().length() : "null".length();
        StringBuilder builder = new StringBuilder(nameLength + valueLength);
        builder.append(name);
        builder.append("=");
        builder.append(value);
        this.push(builder.toString());
    }

    public void push(Object object) {
        if (this.stack == null) {
            this.stack = new Stack();
        }
        this.stack.push(object);
    }

    public void addHandler(Handler handler) {
        Handler[] tempHandlers = new Handler[this.handlers.length + 1];
        System.arraycopy(this.handlers, 0, tempHandlers, 0, this.handlers.length);
        tempHandlers[this.handlers.length] = handler;
        this.handlers = tempHandlers;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public void warning(Object msg) {
        this.log(Level.WARNING, msg);
    }

    public void warning(long millis, Object msg) {
        this.log(Level.WARNING, millis, msg);
    }

    public void warning(Object msg, Throwable thrown) {
        this.log(Level.WARNING, msg, thrown);
    }

    public void warning(long millis, Object msg, Throwable thrown) {
        this.log(Level.WARNING, millis, msg, thrown);
    }

    @Override
    public boolean isFatalEnabled() {
        return this.isErrorEnabled();
    }

    @Override
    public boolean isTraceEnabled() {
        return this.isFineEnabled();
    }

    @Override
    public void trace(Object o) {
        this.log(Level.FINE, o);
    }

    @Override
    public void trace(Object o, Throwable throwable) {
        this.log(Level.FINE, o, throwable);
    }

    @Override
    public void debug(Object o, Throwable throwable) {
        this.log(Level.FINE, o, throwable);
    }

    @Override
    public void info(Object o, Throwable throwable) {
        this.log(Level.INFO, o, throwable);
    }

    @Override
    public void warn(Object o) {
        this.log(Level.WARNING, o);
    }

    @Override
    public void warn(Object o, Throwable throwable) {
        this.log(Level.WARNING, o, throwable);
    }

    @Override
    public void fatal(Object o) {
        this.log(Level.ERROR, o);
    }

    @Override
    public void fatal(Object o, Throwable throwable) {
        this.log(Level.ERROR, o, throwable);
    }
}

