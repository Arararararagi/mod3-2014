/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.Getter118;

public abstract class Setter118
extends Getter118 {
    protected boolean modified = false;

    protected Setter118(String name) {
        super(name);
    }

    protected Setter118(String name, boolean caseSensitive) {
        super(name, caseSensitive);
    }

    protected Setter118(String name, boolean value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, byte value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, char value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, double value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, float value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, int value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, long value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, Object value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter118(String name, short value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    public boolean isModified() {
        return this.modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public Object setValue(boolean value) {
        return this.setValue(new Boolean(value));
    }

    public Object setValue(byte value) {
        return this.setValue(new Byte(value));
    }

    public Object setValue(char value) {
        return this.setValue(new Character(value));
    }

    public Object setValue(double value) {
        return this.setValue(new Double(value));
    }

    public Object setValue(float value) {
        return this.setValue(new Float(value));
    }

    public Object setValue(int value) {
        return this.setValue(new Integer(value));
    }

    public Object setValue(long value) {
        return this.setValue(new Long(value));
    }

    public abstract Object setValue(Object var1);

    public Object setValue(short value) {
        return this.setValue(new Short(value));
    }
}

