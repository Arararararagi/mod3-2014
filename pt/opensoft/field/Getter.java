/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import pt.opensoft.field.Getter118;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.MapUtil;

public abstract class Getter
extends Getter118 {
    public static List getList(Object value) {
        return Getter.getList(value, null, ",");
    }

    public static List getList(Object value, String separator) {
        return Getter.getList(value, null, separator);
    }

    public static List getList(Object value, List defaultValue) {
        return Getter.getList(value, defaultValue, ",");
    }

    public static List getList(Object value, List defaultValue, String separator) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof List) {
            return (List)value;
        }
        if (value instanceof Collection) {
            return new ArrayList((Collection)value);
        }
        return ListUtil.toList(value.toString(), separator);
    }

    public static Map getMap(Object value) {
        return Getter.getMap(value, null, null, null);
    }

    public static Map getMap(Object value, String sep1) {
        return Getter.getMap(value, null, sep1, true);
    }

    public static Map getMap(Object value, String sep1, boolean first) {
        return Getter.getMap(value, null, sep1, first);
    }

    public static Map getMap(Object value, Map defaultValue, String sep1) {
        return Getter.getMap(value, defaultValue, sep1, true);
    }

    public static Map getMap(Object value, Map defaultValue, String sep1, boolean first) {
        if (first) {
            return Getter.getMap(value, defaultValue, sep1, null);
        }
        return Getter.getMap(value, defaultValue, null, sep1);
    }

    public static Map getMap(Object value, Map defaultValue, String sep1, String sep2) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Map) {
            return (Map)value;
        }
        return MapUtil.toMap(value.toString(), sep1, sep2);
    }

    protected Getter(String name) {
        this(name, false);
    }

    protected Getter(String name, boolean caseSensitive) {
        super(name, caseSensitive);
    }

    public List getList() {
        return Getter.getList(this.getValue());
    }

    public List getList(String separator) {
        return Getter.getList(this.getValue(), separator);
    }

    public List getList(List defaultValue) {
        return Getter.getList(this.getValue(), defaultValue);
    }

    public List getList(List defaultValue, String separator) {
        return Getter.getList(this.getValue(), defaultValue, separator);
    }

    public Map getMap() {
        return Getter.getMap(this.getValue());
    }

    public Map getMap(String sep1) {
        return Getter.getMap(this.getValue(), sep1);
    }

    public Map getMap(String sep1, boolean first) {
        return Getter.getMap(this.getValue(), sep1, first);
    }

    public Map getMap(Map defaultValue, String sep1) {
        return Getter.getMap(this.getValue(), defaultValue, sep1);
    }

    public Map getMap(Map defaultValue, String sep1, boolean first) {
        return Getter.getMap(this.getValue(), defaultValue, sep1, first);
    }

    public Map getMap(Map defaultValue, String sep1, String sep2) {
        return Getter.getMap(this.getValue(), defaultValue, sep1, sep2);
    }
}

