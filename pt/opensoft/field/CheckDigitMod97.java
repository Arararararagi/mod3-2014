/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.CheckDigit;

public class CheckDigitMod97
extends CheckDigit {
    private static final int[] WEIGHT = new int[]{91, 77, 95, 58, 64, 84, 86, 28, 61, 74, 85, 57, 93, 19, 31, 71, 75, 56, 25, 51, 73, 17, 89, 38, 62, 45, 53, 15, 50, 5, 49, 34, 81, 76, 27, 90, 9, 30, 3};
    protected static final int CHECK_DIGIT_SIZE = 2;

    public CheckDigitMod97(int position) {
        super(2, position);
    }

    @Override
    public int calculate(String num) {
        int sum = 0;
        if (num == null) {
            throw new IllegalArgumentException("Number is null.");
        }
        int initFactor = WEIGHT.length - num.length();
        for (int i = 0; i < num.length(); ++i) {
            int digit = num.charAt(i) - 48;
            sum+=digit * WEIGHT[i + initFactor];
        }
        return 97 - sum % 97 + 1;
    }
}

