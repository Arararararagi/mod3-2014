/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import java.math.BigDecimal;
import pt.opensoft.field.Field;

public class NumericField
extends Field {
    public NumericField(String name) {
        super(name);
    }

    public NumericField(String name, boolean value) {
        super(name, value);
    }

    public NumericField(String name, byte value) {
        super(name, value);
    }

    public NumericField(String name, char value) {
        super(name, value);
    }

    public NumericField(String name, double value) {
        super(name, value);
    }

    public NumericField(String name, float value) {
        super(name, value);
    }

    public NumericField(String name, int value) {
        super(name, value);
    }

    public NumericField(String name, long value) {
        super(name, value);
    }

    public NumericField(String name, Object value) {
        super(name, value);
    }

    public NumericField(String name, short value) {
        super(name, value);
    }

    @Override
    public Object setValue(boolean value) {
        return this.setValue(value ? 1 : 0);
    }

    @Override
    public Object setValue(char value) {
        return this.setValue(new Integer(String.valueOf(value)));
    }

    @Override
    public Object setValue(Object value) {
        if (value == null) {
            this.value = null;
            return null;
        }
        if (value instanceof Number) {
            return super.setValue(value);
        }
        if (value instanceof String) {
            return super.setValue(new BigDecimal((String)value));
        }
        return super.setValue(new BigDecimal(value.toString()));
    }
}

