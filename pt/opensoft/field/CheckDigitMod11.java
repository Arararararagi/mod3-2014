/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.CheckDigit;

public class CheckDigitMod11
extends CheckDigit {
    protected static final int CHECK_DIGIT_SIZE = 1;
    protected static final int DEFAULT_WEIGHT_SHIFT = 2;
    protected static final int DEFAULT_SUM_SHIFT = 11;
    protected static final int DEFAULT_SUM_FACTOR = -1;
    protected static final int DEFAULT_WEIGHT_PERIOD = 8;
    protected static final int DEFAULT_CHECK_DIGIT = 0;
    protected int weightShift = 2;
    protected int sumShift = 11;
    protected int sumFactor = -1;
    protected int defaultCheckDigit = 0;
    protected int weightPeriod = 8;

    public CheckDigitMod11(int position) {
        super(1, position);
    }

    public CheckDigitMod11(int position, int weightPeriod) {
        super(1, position);
        this.weightPeriod = weightPeriod;
    }

    public CheckDigitMod11(int position, int weightShift, int sumShift, int sumFactor, int defaultCheckDigit) {
        super(1, position);
        this.weightShift = weightShift;
        this.sumShift = sumShift;
        this.sumFactor = sumFactor;
        this.defaultCheckDigit = defaultCheckDigit;
    }

    @Override
    public int calculate(String num) {
        int sum = 0;
        int len = num.length();
        for (int i = 0; i < len; ++i) {
            int digit = num.charAt(len - i - 1) - 48;
            int weight = this.weightShift + i % this.weightPeriod;
            sum+=digit * weight;
        }
        int cd = this.sumShift + this.sumFactor * (sum % 11);
        if (cd == 10 || cd == 11) {
            return this.defaultCheckDigit;
        }
        return cd;
    }
}

