/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.CheckDigit;
import pt.opensoft.field.CheckDigitMod97;
import pt.opensoft.util.StringUtil;

public class NibValidator {
    protected static final int NIB_LENGTH = 21;
    protected static final int CHECK_DIGIT_LENGTH = 2;
    protected static final CheckDigit check = new CheckDigitMod97(19);

    public static int calculateCheckDigit(String num) {
        return check.calculate(num);
    }

    public static boolean validate(int nib) {
        return NibValidator.validate(String.valueOf(nib));
    }

    public static boolean validate(String nib) {
        if (nib == null) {
            return false;
        }
        if (nib.length() != 21) {
            return false;
        }
        if (!StringUtil.isNumeric(nib)) {
            return false;
        }
        if (StringUtil.remove(nib, "0").equals("")) {
            return false;
        }
        return check.validate(nib);
    }

    private NibValidator() {
    }
}

