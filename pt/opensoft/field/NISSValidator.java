/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.util.StringUtil;

public class NISSValidator {
    protected static final int NISS_LENGTH = 11;
    protected static final int CHECK_DIGIT_POSITION = 10;

    public static boolean validate(String niss) {
        return NISSValidator.validateSingular(niss);
    }

    public static boolean validateSingular(String niss) {
        if (!NISSValidator.validateAll(niss)) {
            return false;
        }
        return Integer.parseInt(String.valueOf(niss.charAt(0))) == 1;
    }

    public static boolean validateAll(String niss) {
        if (StringUtil.isEmpty(niss)) {
            return false;
        }
        if (niss.length() != 11) {
            return false;
        }
        if (!StringUtil.isNumeric(niss)) {
            return false;
        }
        if (StringUtil.remove(niss, "0").equals("")) {
            return false;
        }
        return NISSValidator.checkDigitNiss(niss);
    }

    public static boolean checkDigitNiss(String niss) {
        int[] array = new int[11];
        int[] valores = new int[]{29, 23, 19, 17, 13, 11, 7, 5, 3, 2};
        int total = 0;
        int check = 0;
        int digit = Integer.parseInt(String.valueOf(niss.charAt(10)));
        int i = 0;
        int j = 0;
        while (i < niss.length() - 1) {
            array[i] = Integer.parseInt(String.valueOf(niss.charAt(i))) * valores[j];
            total+=array[i];
            ++i;
            ++j;
        }
        int resto = total % 10;
        check = 9 - resto;
        if (check == digit) {
            return true;
        }
        return false;
    }
}

