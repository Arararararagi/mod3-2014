/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import java.io.File;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import pt.opensoft.util.CharUtil;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.HashtableUtil;
import pt.opensoft.util.NamedObject;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Time;
import pt.opensoft.util.VectorUtil;
import pt.opensoft.util.WrappedRuntimeException;

public abstract class Getter118
extends NamedObject {
    public static Boolean getBoolean(Object value) {
        return Getter118.getBoolean(value, null, null);
    }

    public static Boolean getBoolean(Object value, String trueValue) {
        return Getter118.getBoolean(value, null, trueValue);
    }

    public static Boolean getBoolean(Object value, boolean defaultValue) {
        return Getter118.getBoolean(value, defaultValue, null);
    }

    public static Boolean getBoolean(Object value, boolean defaultValue, String trueValue) {
        Boolean v = Getter118.getBoolean(value, null, trueValue);
        return v == null ? new Boolean(defaultValue) : v;
    }

    public static Boolean getBoolean(Object value, Boolean defaultValue) {
        return Getter118.getBoolean(value, defaultValue, null);
    }

    public static Boolean getBoolean(Object value, Boolean defaultValue, String trueValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Boolean) {
            return (Boolean)value;
        }
        if (trueValue != null) {
            return new Boolean(value.toString().trim().equalsIgnoreCase(trueValue));
        }
        return new Boolean(value.toString().trim());
    }

    public static boolean getBooleanValue(Object value) {
        return Getter118.getBooleanValue(value, null, null);
    }

    public static boolean getBooleanValue(Object value, String trueValue) {
        return Getter118.getBooleanValue(value, null, trueValue);
    }

    public static boolean getBooleanValue(Object value, boolean defaultValue) {
        return Getter118.getBooleanValue(value, defaultValue, null);
    }

    public static boolean getBooleanValue(Object value, boolean defaultValue, String trueValue) {
        Boolean v = Getter118.getBoolean(value, null, trueValue);
        return v == null ? defaultValue : v;
    }

    public static boolean getBooleanValue(Object value, Boolean defaultValue) {
        return Getter118.getBoolean(value, defaultValue);
    }

    public static boolean getBooleanValue(Object value, Boolean defaultValue, String trueValue) {
        return Getter118.getBoolean(value, defaultValue, trueValue);
    }

    public static Byte getByte(Object value) {
        return Getter118.getByte(value, null);
    }

    public static Byte getByte(Object value, byte defaultValue) {
        Byte v = Getter118.getByte(value);
        return v == null ? new Byte(defaultValue) : v;
    }

    public static Byte getByte(Object value, Byte defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Byte) {
            return (Byte)value;
        }
        if (value instanceof Character) {
            return new Byte(CharUtil.toByte(((Character)value).charValue()));
        }
        if (value instanceof Number) {
            return new Byte(((Number)value).byteValue());
        }
        return new Byte(value.toString().trim());
    }

    public static byte getByteValue(Object value) {
        return Getter118.getByte(value).byteValue();
    }

    public static byte getByteValue(Object value, byte defaultValue) {
        Byte v = Getter118.getByte(value);
        return v == null ? defaultValue : v.byteValue();
    }

    public static byte getByteValue(Object value, Byte defaultValue) {
        return Getter118.getByte(value, defaultValue).byteValue();
    }

    public static Character getCharacter(Object value) {
        return Getter118.getCharacter(value, null);
    }

    public static Character getCharacter(Object value, char defaultValue) {
        Character v = Getter118.getCharacter(value);
        return v == null ? new Character(defaultValue) : v;
    }

    public static Character getCharacter(Object value, Character defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Character) {
            return (Character)value;
        }
        return new Character(value.toString().charAt(0));
    }

    public static char getCharValue(Object value) {
        return Getter118.getCharacter(value).charValue();
    }

    public static char getCharValue(Object value, char defaultValue) {
        Character v = Getter118.getCharacter(value);
        return v == null ? defaultValue : v.charValue();
    }

    public static char getCharValue(Object value, Character defaultValue) {
        return Getter118.getCharacter(value, defaultValue).charValue();
    }

    public static Class getClazz(Object value) {
        return Getter118.getClazz(value, (Class)null);
    }

    public static Class getClazz(Object value, Class defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Class) {
            return (Class)value;
        }
        try {
            return Class.forName(value.toString());
        }
        catch (ClassNotFoundException e) {
            throw new WrappedRuntimeException(e);
        }
    }

    public static Class getClazz(Object value, String defaultValue) {
        Class v = Getter118.getClazz(value);
        try {
            return v == null ? Class.forName(defaultValue) : v;
        }
        catch (ClassNotFoundException e) {
            throw new WrappedRuntimeException(e);
        }
    }

    public static DateTime getDate(Object value) {
        return Getter118.getDateTime(value, "yyyy-MM-dd");
    }

    public static DateTime getDate(Object value, boolean lenient) {
        return Getter118.getDateTime(value, "yyyy-MM-dd", lenient);
    }

    public static DateTime getDate(Object value, DateTime defaultValue) {
        return Getter118.getDateTime(value, defaultValue, "yyyy-MM-dd");
    }

    public static DateTime getDate(Object value, DateTime defaultValue, boolean lenient) {
        return Getter118.getDateTime(value, defaultValue, "yyyy-MM-dd", lenient);
    }

    public static DateTime getDateTime(Object value) {
        return Getter118.getDateTime(value, null, null, false);
    }

    public static DateTime getDateTime(Object value, boolean lenient) {
        return Getter118.getDateTime(value, null, null, lenient);
    }

    public static DateTime getDateTime(Object value, String format) {
        return Getter118.getDateTime(value, null, format, false);
    }

    public static DateTime getDateTime(Object value, String format, boolean lenient) {
        return Getter118.getDateTime(value, null, format, lenient);
    }

    public static DateTime getDateTime(Object value, DateTime defaultValue) {
        return Getter118.getDateTime(value, defaultValue, null, false);
    }

    public static DateTime getDateTime(Object value, DateTime defaultValue, boolean lenient) {
        return Getter118.getDateTime(value, defaultValue, null, lenient);
    }

    public static DateTime getDateTime(Object value, DateTime defaultValue, String format) {
        return Getter118.getDateTime(value, defaultValue, format, false);
    }

    public static DateTime getDateTime(Object value, DateTime defaultValue, String format, boolean lenient) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof DateTime) {
            DateTime date = (DateTime)value;
            date.setFormat(format);
            date.setLenient(lenient);
            return date;
        }
        if (value instanceof Timestamp) {
            return new DateTime((Timestamp)value, format, lenient);
        }
        if (value instanceof Date) {
            return new DateTime((Date)value, format, lenient);
        }
        return new DateTime(value.toString(), format, lenient);
    }

    public static Double getDouble(Object value) {
        return Getter118.getDouble(value, null);
    }

    public static Double getDouble(Object value, double defaultValue) {
        Double v = Getter118.getDouble(value);
        return v == null ? new Double(defaultValue) : v;
    }

    public static Double getDouble(Object value, Double defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Double) {
            return (Double)value;
        }
        if (value instanceof Number) {
            return new Double(((Number)value).doubleValue());
        }
        return new Double(value.toString().trim());
    }

    public static double getDoubleValue(Object value) {
        return Getter118.getDouble(value);
    }

    public static double getDoubleValue(Object value, double defaultValue) {
        Double v = Getter118.getDouble(value);
        return v == null ? defaultValue : v;
    }

    public static double getDoubleValue(Object value, Double defaultValue) {
        return Getter118.getDouble(value, defaultValue);
    }

    public static Number getNumber(Object value) {
        return Getter118.getNumber(value, null);
    }

    public static Number getNumber(Object value, Number defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Number) {
            return (Number)value;
        }
        try {
            return NumberFormat.getInstance().parse(value.toString().trim());
        }
        catch (ParseException e) {
            throw new WrappedRuntimeException(e);
        }
    }

    public static File getFile(Object value) {
        return Getter118.getFile(value, (File)null);
    }

    public static File getFile(Object value, String defaultValue) {
        return Getter118.getFile(value, new File(defaultValue));
    }

    public static File getFile(Object value, File defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof File) {
            return (File)value;
        }
        return new File(value.toString());
    }

    public static Float getFloat(Object value) {
        return Getter118.getFloat(value, null);
    }

    public static Float getFloat(Object value, float defaultValue) {
        Float v = Getter118.getFloat(value);
        return v == null ? new Float(defaultValue) : v;
    }

    public static Float getFloat(Object value, Float defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Float) {
            return (Float)value;
        }
        if (value instanceof Number) {
            return new Float(((Number)value).floatValue());
        }
        return new Float(value.toString().trim());
    }

    public static float getFloatValue(Object value) {
        return Getter118.getFloat(value).floatValue();
    }

    public static float getFloatValue(Object value, float defaultValue) {
        Float v = Getter118.getFloat(value);
        return v == null ? defaultValue : v.floatValue();
    }

    public static float getFloatValue(Object value, Float defaultValue) {
        return Getter118.getFloat(value, defaultValue).floatValue();
    }

    public static Hashtable getHashtable(Object value) {
        return Getter118.getHashtable(value, null, null, null);
    }

    public static Hashtable getHashtable(Object value, String sep1) {
        return Getter118.getHashtable(value, null, sep1, true);
    }

    public static Hashtable getHashtable(Object value, String sep1, boolean first) {
        return Getter118.getHashtable(value, null, sep1, first);
    }

    public static Hashtable getHashtable(Object value, Hashtable defaultValue, String sep1) {
        return Getter118.getHashtable(value, defaultValue, sep1, true);
    }

    public static Hashtable getHashtable(Object value, Hashtable defaultValue, String sep1, boolean first) {
        if (first) {
            return Getter118.getHashtable(value, defaultValue, sep1, null);
        }
        return Getter118.getHashtable(value, defaultValue, null, sep1);
    }

    public static Hashtable getHashtable(Object value, Hashtable defaultValue, String sep1, String sep2) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Hashtable) {
            return (Hashtable)value;
        }
        return HashtableUtil.toHashtable(value.toString(), sep1, sep2);
    }

    public static Integer getInteger(Object value) {
        return Getter118.getInteger(value, null);
    }

    public static Integer getInteger(Object value, int defaultValue) {
        Integer v = Getter118.getInteger(value);
        return v == null ? new Integer(defaultValue) : v;
    }

    public static Integer getInteger(Object value, Integer defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Integer) {
            return (Integer)value;
        }
        if (value instanceof Number) {
            return new Integer(((Number)value).intValue());
        }
        return new Integer(value.toString().trim());
    }

    public static int getIntValue(Object value) {
        return Getter118.getInteger(value);
    }

    public static int getIntValue(Object value, int defaultValue) {
        Integer v = Getter118.getInteger(value);
        return v == null ? defaultValue : v;
    }

    public static int getIntValue(Object value, Integer defaultValue) {
        return Getter118.getInteger(value, defaultValue);
    }

    public static Object getInstance(Object value) {
        return Getter118.getInstance(value, null);
    }

    public static Object getInstance(Object value, Class defaultValue) {
        try {
            if (value == null) {
                if (defaultValue == null) {
                    return null;
                }
                return defaultValue.newInstance();
            }
            return Getter118.getClazz(value).newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static Long getLong(Object value) {
        return Getter118.getLong(value, null);
    }

    public static Long getLong(Object value, long defaultValue) {
        Long v = Getter118.getLong(value);
        return v == null ? new Long(defaultValue) : v;
    }

    public static Long getLong(Object value, Long defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Long) {
            return (Long)value;
        }
        if (value instanceof Number) {
            return new Long(((Number)value).longValue());
        }
        return new Long(value.toString().trim());
    }

    public static long getLongValue(Object value) {
        return Getter118.getLong(value);
    }

    public static long getLongValue(Object value, long defaultValue) {
        Long v = Getter118.getLong(value);
        return v == null ? defaultValue : v;
    }

    public static long getLongValue(Object value, Long defaultValue) {
        return Getter118.getLong(value, defaultValue);
    }

    public static Short getShort(Object value) {
        return Getter118.getShort(value, null);
    }

    public static Short getShort(Object value, short defaultValue) {
        Short v = Getter118.getShort(value);
        return v == null ? new Short(defaultValue) : v;
    }

    public static Short getShort(Object value, Short defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Short) {
            return (Short)value;
        }
        if (value instanceof Number) {
            return new Short(((Number)value).shortValue());
        }
        return new Short(value.toString().trim());
    }

    public static short getShortValue(Object value) {
        return Getter118.getShort(value);
    }

    public static short getShortValue(Object value, short defaultValue) {
        Short v = Getter118.getShort(value);
        return v == null ? defaultValue : v;
    }

    public static short getShortValue(Object value, Short defaultValue) {
        return Getter118.getShort(value, defaultValue);
    }

    public static String getString(Object value) {
        return Getter118.getString(value, null);
    }

    public static String getString(Object value, String defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof String) {
            return (String)value;
        }
        return value.toString();
    }

    public static DateTime getTime(Object value) {
        return Getter118.getDateTime(value, "HH:mm:ss");
    }

    public static DateTime getTime(Object value, boolean lenient) {
        return Getter118.getDateTime(value, "HH:mm:ss", lenient);
    }

    public static DateTime getTime(Object value, Time defaultValue) {
        return Getter118.getDateTime(value, (DateTime)defaultValue, "HH:mm:ss");
    }

    public static DateTime getTime(Object value, Time defaultValue, boolean lenient) {
        return Getter118.getDateTime(value, defaultValue, "HH:mm:ss", lenient);
    }

    public static Vector getVector(Object value) {
        return Getter118.getVector(value, null, ",");
    }

    public static Vector getVector(Object value, String separator) {
        return Getter118.getVector(value, null, separator);
    }

    public static Vector getVector(Object value, Vector defaultValue) {
        return Getter118.getVector(value, defaultValue, ",");
    }

    public static Vector getVector(Object value, Vector defaultValue, String separator) {
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof Vector) {
            return (Vector)value;
        }
        return VectorUtil.toVector(value.toString(), separator);
    }

    protected Getter118(String name) {
        super(name);
    }

    protected Getter118(String name, boolean caseSensitive) {
        super(name, caseSensitive);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Getter118)) {
            return super.equals(obj);
        }
        Object value = this.getValue();
        Object other = ((Getter118)obj).getValue();
        if (value == null && other != null) {
            return false;
        }
        if (value != null && other == null) {
            return false;
        }
        return value.equals(other);
    }

    public Boolean getBoolean() {
        return Getter118.getBoolean(this.getValue());
    }

    public Boolean getBoolean(String trueValue) {
        return Getter118.getBoolean(this.getValue(), trueValue);
    }

    public Boolean getBoolean(boolean defaultValue) {
        return Getter118.getBoolean(this.getValue(), defaultValue);
    }

    public Boolean getBoolean(boolean defaultValue, String trueValue) {
        return Getter118.getBoolean(this.getValue(), defaultValue, trueValue);
    }

    public Boolean getBoolean(Boolean defaultValue) {
        return Getter118.getBoolean(this.getValue(), defaultValue);
    }

    public Boolean getBoolean(Boolean defaultValue, String trueValue) {
        return Getter118.getBoolean(this.getValue(), defaultValue, trueValue);
    }

    public boolean getBooleanValue() {
        return Getter118.getBooleanValue(this.getValue());
    }

    public boolean getBooleanValue(String trueValue) {
        return Getter118.getBooleanValue(this.getValue(), trueValue);
    }

    public boolean getBooleanValue(boolean defaultValue) {
        return Getter118.getBooleanValue(this.getValue(), defaultValue);
    }

    public boolean getBooleanValue(boolean defaultValue, String trueValue) {
        return Getter118.getBooleanValue(this.getValue(), defaultValue, trueValue);
    }

    public Byte getByte() {
        return Getter118.getByte(this.getValue());
    }

    public Byte getByte(Byte defaultValue) {
        return Getter118.getByte(this.getValue(), defaultValue);
    }

    public byte getByteValue() {
        return Getter118.getByteValue(this.getValue());
    }

    public byte getByteValue(byte defaultValue) {
        return Getter118.getByteValue(this.getValue(), defaultValue);
    }

    public Character getCharacter() {
        return Getter118.getCharacter(this.getValue());
    }

    public Character getCharacter(Character defaultValue) {
        return Getter118.getCharacter(this.getValue(), defaultValue);
    }

    public char getCharValue() {
        return Getter118.getCharValue(this.getValue());
    }

    public char getCharValue(char defaultValue) {
        return Getter118.getCharValue(this.getValue(), defaultValue);
    }

    public Class getClazz() {
        return Getter118.getClazz(this.getValue());
    }

    public Class getClazz(Class defaultValue) {
        return Getter118.getClazz(this.getValue(), defaultValue);
    }

    public DateTime getDate() {
        return Getter118.getDate(this.getValue());
    }

    public DateTime getDate(DateTime defaultValue) {
        return Getter118.getDate(this.getValue(), defaultValue);
    }

    public DateTime getDateTime() {
        return Getter118.getDateTime(this.getValue());
    }

    public DateTime getDateTime(DateTime defaultValue) {
        return this.getDateTime(defaultValue, null);
    }

    public DateTime getDateTime(DateTime defaultValue, String format) {
        return Getter118.getDateTime(this.getValue(), defaultValue, format, false);
    }

    public DateTime getDateTime(DateTime defaultValue, String format, boolean lenient) {
        return Getter118.getDateTime(this.getValue(), defaultValue, format, lenient);
    }

    public Double getDouble() {
        return Getter118.getDouble(this.getValue());
    }

    public Double getDouble(Double defaultValue) {
        return Getter118.getDouble(this.getValue(), defaultValue);
    }

    public double getDoubleValue() {
        return Getter118.getDoubleValue(this.getValue());
    }

    public double getDoubleValue(double defaultValue) {
        return Getter118.getDoubleValue(this.getValue(), defaultValue);
    }

    public File getFile() {
        return Getter118.getFile(this.getValue());
    }

    public File getFile(String defaultValue) {
        return Getter118.getFile(this.getValue(), defaultValue);
    }

    public File getFile(File defaultValue) {
        return Getter118.getFile(this.getValue(), defaultValue);
    }

    public Float getFloat() {
        return Getter118.getFloat(this.getValue());
    }

    public Float getFloat(Float defaultValue) {
        return Getter118.getFloat(this.getValue(), defaultValue);
    }

    public float getFloatValue() {
        return Getter118.getFloatValue(this.getValue());
    }

    public float getFloatValue(float defaultValue) {
        return Getter118.getFloatValue(this.getValue(), defaultValue);
    }

    public Hashtable getHashtable() {
        return Getter118.getHashtable(this.getValue());
    }

    public Hashtable getHashtable(String sep1) {
        return Getter118.getHashtable(this.getValue(), sep1);
    }

    public Hashtable getHashtable(String sep1, boolean first) {
        return Getter118.getHashtable(this.getValue(), sep1, first);
    }

    public Hashtable getHashtable(Hashtable defaultValue, String sep1) {
        return Getter118.getHashtable(this.getValue(), defaultValue, sep1);
    }

    public Hashtable getHashtable(Hashtable defaultValue, String sep1, boolean first) {
        return Getter118.getHashtable(this.getValue(), defaultValue, sep1, first);
    }

    public Hashtable getHashtable(Hashtable defaultValue, String sep1, String sep2) {
        return Getter118.getHashtable(this.getValue(), defaultValue, sep1, sep2);
    }

    public int getInt() {
        return Getter118.getIntValue(this.getValue());
    }

    public int getInt(int defaultValue) {
        return Getter118.getIntValue(this.getValue(), defaultValue);
    }

    public Integer getInteger() {
        return Getter118.getInteger(this.getValue());
    }

    public Integer getInteger(Integer defaultValue) {
        return Getter118.getInteger(this.getValue(), defaultValue);
    }

    public int getIntValue() {
        return Getter118.getIntValue(this.getValue());
    }

    public int getIntValue(int defaultValue) {
        return Getter118.getIntValue(this.getValue(), defaultValue);
    }

    public Object getInstance() {
        return Getter118.getInstance(this.getValue(), null);
    }

    public Object getInstance(Class defaultValue) {
        return Getter118.getInstance(this.getValue(), defaultValue);
    }

    public Long getLong() {
        return Getter118.getLong(this.getValue());
    }

    public Long getLong(Long defaultValue) {
        return Getter118.getLong(this.getValue(), defaultValue);
    }

    public long getLongValue() {
        return Getter118.getLongValue(this.getValue());
    }

    public long getLongValue(long defaultValue) {
        return Getter118.getLongValue(this.getValue(), defaultValue);
    }

    public Short getShort() {
        return Getter118.getShort(this.getValue());
    }

    public Short getShort(Short defaultValue) {
        return Getter118.getShort(this.getValue(), defaultValue);
    }

    public short getShortValue() {
        return Getter118.getShortValue(this.getValue());
    }

    public short getShortValue(short defaultValue) {
        return Getter118.getShortValue(this.getValue(), defaultValue);
    }

    public String getString() {
        return Getter118.getString(this.getValue());
    }

    public String getString(String defaultValue) {
        return Getter118.getString(this.getValue(), defaultValue);
    }

    public DateTime getTime() {
        return Getter118.getTime(this.getValue());
    }

    public DateTime getTime(Time defaultValue) {
        return Getter118.getTime(this.getValue(), defaultValue);
    }

    public abstract Object getValue();

    public Object getValue(Object defaultValue) {
        Object value = this.getValue();
        return value == null ? defaultValue : value;
    }

    public Vector getVector() {
        return Getter118.getVector(this.getValue());
    }

    public Vector getVector(String separator) {
        return Getter118.getVector(this.getValue(), separator);
    }

    public Vector getVector(Vector defaultValue) {
        return Getter118.getVector(this.getValue(), defaultValue);
    }

    public Vector getVector(Vector defaultValue, String separator) {
        return Getter118.getVector(this.getValue(), defaultValue, separator);
    }

    public boolean isNull() {
        return this.getValue() == null;
    }

    public boolean isEmpty() {
        return this.isNull();
    }

    public boolean isNumeric() {
        Object value = this.getValue();
        if (value == null) {
            return false;
        }
        if (value instanceof Number) {
            return true;
        }
        if (value instanceof String) {
            return StringUtil.isNumeric((String)value);
        }
        return StringUtil.isNumeric(value.toString());
    }

    @Override
    public String toString() {
        return this.toString("=");
    }

    public String toString(String separator) {
        StringBuffer buffer = new StringBuffer();
        String name = this.getName();
        buffer.append(name).append(separator);
        if (name.toLowerCase().endsWith("password")) {
            buffer.append("*");
        } else {
            buffer.append(this.getString());
        }
        return buffer.toString();
    }
}

