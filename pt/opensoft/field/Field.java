/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import java.io.Serializable;
import pt.opensoft.field.Setter;

public class Field
extends Setter
implements Serializable {
    protected Object value;

    public Field(String name) {
        super(name, true);
    }

    public Field(String name, boolean value) {
        super(name, value, true);
    }

    public Field(String name, byte value) {
        super(name, value, true);
        this.setModified(false);
    }

    public Field(String name, char value) {
        super(name, value, true);
        this.setModified(false);
    }

    public Field(String name, double value) {
        super(name, value, true);
        this.setModified(false);
    }

    public Field(String name, float value) {
        super(name, value, true);
        this.setModified(false);
    }

    public Field(String name, int value) {
        super(name, value, true);
        this.setModified(false);
    }

    public Field(String name, long value) {
        super(name, value, true);
        this.setModified(false);
    }

    public Field(String name, Object value) {
        super(name, value, true);
        this.setModified(false);
    }

    public Field(String name, short value) {
        super(name, value, true);
        this.setModified(false);
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public Object setValue(Object value) {
        if (this.value == null) {
            this.setModified(value != null);
        }
        if (this.value != null) {
            this.setModified(value == null || !value.equals(this.value));
        }
        this.value = value;
        return this.value;
    }
}

