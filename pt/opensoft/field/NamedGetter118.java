/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import pt.opensoft.field.Getter118;
import pt.opensoft.text.Appender;
import pt.opensoft.text.Regex;
import pt.opensoft.text.StringAppender;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.NamedObject;
import pt.opensoft.util.Time;

public abstract class NamedGetter118
extends NamedObject {
    public NamedGetter118(String name) {
        super(name);
    }

    public NamedGetter118(String name, boolean caseSensitive) {
        super(name, caseSensitive);
    }

    public boolean containsName(String name) {
        Vector names = this.getNamesVector();
        if (names == null) {
            return false;
        }
        return names.contains(name);
    }

    public boolean containsValue(Object value) {
        Vector values = this.getValuesVector();
        if (values == null) {
            return false;
        }
        return values.contains(value);
    }

    public Boolean getBoolean(String name) {
        return Getter118.getBoolean(this.getValue(name));
    }

    public Boolean getBoolean(String name, String trueValue) {
        return Getter118.getBoolean(this.getValue(name), trueValue);
    }

    public Boolean getBoolean(String name, boolean defaultValue) {
        return Getter118.getBoolean(this.getValue(name), defaultValue);
    }

    public Boolean getBoolean(String name, boolean defaultValue, String trueValue) {
        return Getter118.getBoolean(this.getValue(name), defaultValue, trueValue);
    }

    public Boolean getBoolean(String name, Boolean defaultValue) {
        return Getter118.getBoolean(this.getValue(name), defaultValue);
    }

    public Boolean getBoolean(String name, Boolean defaultValue, String trueValue) {
        return Getter118.getBoolean(this.getValue(name), defaultValue, trueValue);
    }

    public boolean getBooleanValue(String name) {
        return Getter118.getBooleanValue(this.getValue(name));
    }

    public boolean getBooleanValue(String name, String trueValue) {
        return Getter118.getBooleanValue(this.getValue(name), trueValue);
    }

    public boolean getBooleanValue(String name, boolean defaultValue) {
        return Getter118.getBooleanValue(this.getValue(name), defaultValue);
    }

    public boolean getBooleanValue(String name, boolean defaultValue, String trueValue) {
        return Getter118.getBooleanValue(this.getValue(name), defaultValue, trueValue);
    }

    public boolean getBooleanValue(String name, Boolean defaultValue) {
        return Getter118.getBooleanValue(this.getValue(name), defaultValue);
    }

    public boolean getBooleanValue(String name, Boolean defaultValue, String trueValue) {
        return Getter118.getBooleanValue(this.getValue(name), defaultValue, trueValue);
    }

    public Byte getByte(String name) {
        return Getter118.getByte(this.getValue(name));
    }

    public Byte getByte(String name, byte defaultValue) {
        return Getter118.getByte(this.getValue(name), defaultValue);
    }

    public Byte getByte(String name, Byte defaultValue) {
        return Getter118.getByte(this.getValue(name), defaultValue);
    }

    public byte getByteValue(String name) {
        return Getter118.getByteValue(this.getValue(name));
    }

    public byte getByteValue(String name, byte defaultValue) {
        return Getter118.getByteValue(this.getValue(name), defaultValue);
    }

    public byte getByteValue(String name, Byte defaultValue) {
        return Getter118.getByteValue(this.getValue(name), defaultValue);
    }

    public char getChar(String name) {
        return Getter118.getCharValue(this.getValue(name));
    }

    public char getChar(String name, char defaultValue) {
        return Getter118.getCharValue(this.getValue(name), defaultValue);
    }

    public char getChar(String name, Character defaultValue) {
        return Getter118.getCharValue(this.getValue(name), defaultValue);
    }

    public Character getCharacter(String name) {
        return Getter118.getCharacter(this.getValue(name));
    }

    public Character getCharacter(String name, char defaultValue) {
        return Getter118.getCharacter(this.getValue(name), defaultValue);
    }

    public Character getCharacter(String name, Character defaultValue) {
        return Getter118.getCharacter(this.getValue(name), defaultValue);
    }

    public char getCharValue(String name) {
        return Getter118.getCharValue(this.getValue(name));
    }

    public char getCharValue(String name, char defaultValue) {
        return Getter118.getCharValue(this.getValue(name), defaultValue);
    }

    public char getCharValue(String name, Character defaultValue) {
        return Getter118.getCharValue(this.getValue(name), defaultValue);
    }

    public Class getClass(String name) {
        return Getter118.getClazz(this.getValue(name));
    }

    public Class getClass(String name, Class defaultValue) {
        return Getter118.getClazz(this.getValue(name), defaultValue);
    }

    public DateTime getDate(String name) {
        return this.getDateTime(name, "yyyy-MM-dd");
    }

    public DateTime getDate(String name, boolean lenient) {
        return this.getDateTime(name, "yyyy-MM-dd", lenient);
    }

    public DateTime getDate(String name, DateTime defaultValue) {
        return this.getDateTime(name, defaultValue, "yyyy-MM-dd");
    }

    public DateTime getDate(String name, DateTime defaultValue, String format) {
        return this.getDateTime(name, defaultValue, format);
    }

    public DateTime getDate(String name, DateTime defaultValue, boolean lenient) {
        return this.getDateTime(name, defaultValue, "yyyy-MM-dd", lenient);
    }

    public DateTime getDate(String name, DateTime defaultValue, boolean lenient, String format) {
        return this.getDateTime(name, defaultValue, format, lenient);
    }

    public DateTime getDateTime(String name) {
        return Getter118.getDateTime(this.getValue(name));
    }

    public DateTime getDateTime(String name, boolean lenient) {
        return Getter118.getDateTime(this.getValue(name), lenient);
    }

    public DateTime getDateTime(String name, String format) {
        return Getter118.getDateTime(this.getValue(name), format);
    }

    public DateTime getDateTime(String name, String format, boolean lenient) {
        return Getter118.getDateTime(this.getValue(name), format, lenient);
    }

    public DateTime getDateTime(String name, DateTime defaultValue) {
        return Getter118.getDateTime(this.getValue(name), defaultValue);
    }

    public DateTime getDateTime(String name, DateTime defaultValue, boolean lenient) {
        return Getter118.getDateTime(this.getValue(name), defaultValue, lenient);
    }

    public DateTime getDateTime(String name, DateTime defaultValue, String format) {
        return Getter118.getDateTime(this.getValue(name), defaultValue, format);
    }

    public DateTime getDateTime(String name, DateTime defaultValue, String format, boolean lenient) {
        return Getter118.getDateTime(this.getValue(name), defaultValue, format, lenient);
    }

    public Double getDouble(String name) {
        return Getter118.getDouble(this.getValue(name));
    }

    public Double getDouble(String name, double defaultValue) {
        return Getter118.getDouble(this.getValue(name), defaultValue);
    }

    public Double getDouble(String name, Double defaultValue) {
        return Getter118.getDouble(this.getValue(name), defaultValue);
    }

    public double getDoubleValue(String name) {
        return Getter118.getDoubleValue(this.getValue(name));
    }

    public double getDoubleValue(String name, double defaultValue) {
        return Getter118.getDoubleValue(this.getValue(name), defaultValue);
    }

    public double getDoubleValue(String name, Double defaultValue) {
        return Getter118.getDoubleValue(this.getValue(name), defaultValue);
    }

    public Number getNumber(String name, Number defaultValue) {
        return Getter118.getNumber(this.getValue(name), defaultValue);
    }

    public Number getNumber(String name) {
        return Getter118.getNumber(this.getValue(name));
    }

    public File getFile(String name) {
        return Getter118.getFile(this.getValue(name));
    }

    public File getFile(String name, String defaultValue) {
        return Getter118.getFile(this.getValue(name), defaultValue);
    }

    public File getFile(String name, File defaultValue) {
        return Getter118.getFile(this.getValue(name), defaultValue);
    }

    public Float getFloat(String name) {
        return Getter118.getFloat(this.getValue(name));
    }

    public Float getFloat(String name, float defaultValue) {
        return Getter118.getFloat(this.getValue(name), defaultValue);
    }

    public Float getFloat(String name, Float defaultValue) {
        return Getter118.getFloat(this.getValue(name), defaultValue);
    }

    public float getFloatValue(String name) {
        return Getter118.getFloatValue(this.getValue(name));
    }

    public float getFloatValue(String name, float defaultValue) {
        return Getter118.getFloatValue(this.getValue(name), defaultValue);
    }

    public float getFloatValue(String name, Float defaultValue) {
        return Getter118.getFloatValue(this.getValue(name), defaultValue);
    }

    public Hashtable getHashtable(String name) {
        return Getter118.getHashtable(this.getValue(name));
    }

    public Hashtable getHashtable(String name, String sep1) {
        return Getter118.getHashtable(this.getValue(name), sep1);
    }

    public Hashtable getHashtable(String name, String sep1, boolean first) {
        return Getter118.getHashtable(this.getValue(name), sep1, first);
    }

    public Hashtable getHashtable(String name, Hashtable defaultValue, String sep1) {
        return Getter118.getHashtable(this.getValue(name), defaultValue, sep1);
    }

    public Hashtable getHashtable(String name, Hashtable defaultValue, String sep1, boolean first) {
        return Getter118.getHashtable(this.getValue(name), defaultValue, sep1, first);
    }

    public Hashtable getHashtable(String name, Hashtable defaultValue, String sep1, String sep2) {
        return Getter118.getHashtable(this.getValue(name), defaultValue, sep1, sep2);
    }

    public int getInt(String name) {
        return Getter118.getIntValue(this.getValue(name));
    }

    public int getInt(String name, int defaultValue) {
        return Getter118.getIntValue(this.getValue(name), defaultValue);
    }

    public int getInt(String name, Integer defaultValue) {
        return Getter118.getIntValue(this.getValue(name), defaultValue);
    }

    public Integer getInteger(String name) {
        return Getter118.getInteger(this.getValue(name));
    }

    public Integer getInteger(String name, int defaultValue) {
        return Getter118.getInteger(this.getValue(name), defaultValue);
    }

    public Integer getInteger(String name, Integer defaultValue) {
        return Getter118.getInteger(this.getValue(name), defaultValue);
    }

    public int getIntValue(String name) {
        return Getter118.getIntValue(this.getValue(name));
    }

    public int getIntValue(String name, int defaultValue) {
        return Getter118.getIntValue(this.getValue(name), defaultValue);
    }

    public int getIntValue(String name, Integer defaultValue) {
        return Getter118.getIntValue(this.getValue(name), defaultValue);
    }

    public Object getInstance(String name) {
        return Getter118.getInstance(this.getValue(name), null);
    }

    public Object getInstance(String name, Class defaultValue) {
        return Getter118.getInstance(this.getValue(name), defaultValue);
    }

    public Long getLong(String name) {
        return Getter118.getLong(this.getValue(name));
    }

    public Long getLong(String name, long defaultValue) {
        return Getter118.getLong(this.getValue(name), defaultValue);
    }

    public Long getLong(String name, Long defaultValue) {
        return Getter118.getLong(this.getValue(name), defaultValue);
    }

    public long getLongValue(String name) {
        return Getter118.getLongValue(this.getValue(name));
    }

    public long getLongValue(String name, long defaultValue) {
        return Getter118.getLongValue(this.getValue(name), defaultValue);
    }

    public long getLongValue(String name, Long defaultValue) {
        return Getter118.getLongValue(this.getValue(name), defaultValue);
    }

    public abstract Vector getNamesVector();

    public Vector getNamesVector(String pattern) {
        Regex regex = new Regex(pattern);
        Vector names = this.getNamesVector();
        Vector<String> result = new Vector<String>(names.size());
        Enumeration<E> en = names.elements();
        while (en.hasMoreElements()) {
            String name = (String)en.nextElement();
            if (!regex.contains(name)) continue;
            result.addElement(name);
        }
        return result;
    }

    public Short getShort(String name) {
        return Getter118.getShort(this.getValue(name));
    }

    public Short getShort(String name, short defaultValue) {
        return Getter118.getShort(this.getValue(name), defaultValue);
    }

    public Short getShort(String name, Short defaultValue) {
        return Getter118.getShort(this.getValue(name), defaultValue);
    }

    public short getShortValue(String name) {
        return Getter118.getShortValue(this.getValue(name));
    }

    public short getShortValue(String name, short defaultValue) {
        return Getter118.getShortValue(this.getValue(name), defaultValue);
    }

    public short getShortValue(String name, Short defaultValue) {
        return Getter118.getShortValue(this.getValue(name), defaultValue);
    }

    public String getString(String name) {
        return Getter118.getString(this.getValue(name));
    }

    public String getString(String name, String defaultValue) {
        return Getter118.getString(this.getValue(name), defaultValue);
    }

    public DateTime getTime(String name) {
        return this.getDateTime(name, "HH:mm:ss");
    }

    public DateTime getTime(String name, boolean lenient) {
        return this.getDateTime(name, "HH:mm:ss", lenient);
    }

    public DateTime getTime(String name, Time defaultValue) {
        return this.getDateTime(name, (DateTime)defaultValue, "HH:mm:ss");
    }

    public DateTime getTime(String name, Time defaultValue, boolean lenient) {
        return this.getDateTime(name, defaultValue, "HH:mm:ss", lenient);
    }

    public abstract Object getValue(String var1);

    public Object getValue(String name, Object defaultValue) {
        Object v = this.getValue(name);
        return v == null ? defaultValue : v;
    }

    public Vector getValuesVector() {
        Vector names = this.getNamesVector();
        if (names == null || names.size() == 0) {
            return null;
        }
        Vector<Object> values = new Vector<Object>(names.size());
        Enumeration<E> en = values.elements();
        while (en.hasMoreElements()) {
            String name = (String)en.nextElement();
            Object value = this.getValue(name);
            values.addElement(value);
        }
        return values;
    }

    public Vector getVector(String name) {
        return Getter118.getVector(this.getValue(name));
    }

    public Vector getVector(String name, String separator) {
        return Getter118.getVector(this.getValue(name), separator);
    }

    public Vector getVector(String name, Vector defaultValue) {
        return Getter118.getVector(this.getValue(name), defaultValue);
    }

    public Vector getVector(String name, Vector defaultValue, String separator) {
        return Getter118.getVector(this.getValue(name), defaultValue, separator);
    }

    public boolean isEmpty() {
        Vector values = this.getValuesVector();
        if (values == null) {
            return true;
        }
        Enumeration<E> en = values.elements();
        while (en.hasMoreElements()) {
            E value = en.nextElement();
            if (!(value instanceof Getter118 || value == null)) {
                return true;
            }
            if (!(value instanceof Getter118) || ((Getter118)value).isNull()) continue;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return this.toString("=", ";");
    }

    public String toString(String nvSeparator, String separator) {
        Vector names = this.getNamesVector();
        if (names == null) {
            return "";
        }
        StringAppender buffer = new StringAppender();
        Enumeration<E> en = names.elements();
        while (en.hasMoreElements()) {
            String name = (String)en.nextElement();
            if (buffer.length() > 0) {
                buffer.append(separator);
            }
            buffer.append(name);
            buffer.append(nvSeparator);
            if (name.toLowerCase().endsWith("password")) {
                buffer.append("*");
                continue;
            }
            buffer.append(this.getString(name));
        }
        return buffer.toString();
    }
}

