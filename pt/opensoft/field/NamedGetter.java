/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import pt.opensoft.field.Getter;
import pt.opensoft.field.NamedGetter118;
import pt.opensoft.text.Appender;
import pt.opensoft.text.Regex;
import pt.opensoft.text.StringAppender;
import pt.opensoft.util.ListUtil;

public abstract class NamedGetter
extends NamedGetter118 {
    private static final List DEFAULT_FILTERED_PASSWORDS = Arrays.asList("password", "pass", "pwd", "senha");
    private static Comparator PASSWORDS_FILTER_COMPARATOR = new Comparator(){

        public int compare(Object keyword, Object key) {
            if (keyword == null || key == null) {
                return -1;
            }
            if (key.toString().toLowerCase().indexOf(keyword.toString().toLowerCase()) >= 0) {
                return 0;
            }
            return -1;
        }
    };

    protected NamedGetter(String name) {
        super(name);
    }

    protected NamedGetter(String name, boolean caseSensitive) {
        super(name, caseSensitive);
    }

    public List getList(String name) {
        return Getter.getList(this.getValue(name));
    }

    public List getList(String name, String separator) {
        return Getter.getList(this.getValue(name), separator);
    }

    public List getList(String name, List defaultValue) {
        return Getter.getList(this.getValue(name), defaultValue);
    }

    public List getList(String name, List defaultValue, String separator) {
        return Getter.getList(this.getValue(name), defaultValue, separator);
    }

    public Map getMap(String name) {
        return Getter.getMap(this.getValue(name));
    }

    public Map getMap(String name, String sep1) {
        return Getter.getMap(this.getValue(name), sep1);
    }

    public Map getMap(String name, String sep1, boolean first) {
        return Getter.getMap(this.getValue(name), sep1, first);
    }

    public Map getMap(String name, Map defaultValue, String sep1) {
        return Getter.getMap(this.getValue(name), defaultValue, sep1);
    }

    public Map getMap(String name, Map defaultValue, String sep1, boolean first) {
        return Getter.getMap(this.getValue(name), defaultValue, sep1, first);
    }

    public Map getMap(String name, Map defaultValue, String sep1, String sep2) {
        return Getter.getMap(this.getValue(name), defaultValue, sep1, sep2);
    }

    public abstract List getNames();

    public List getNames(String pattern) {
        Regex regex = new Regex(pattern);
        List names = this.getNames();
        ArrayList<String> result = new ArrayList<String>(names.size());
        for (String name : names) {
            if (!regex.contains(name)) continue;
            result.add(name);
        }
        return result;
    }

    public List getNamesSorted() {
        return this.getNamesSorted(true);
    }

    public List getNamesSorted(boolean ascending) {
        List names = this.getNames();
        Collections.sort(names);
        if (!ascending) {
            Collections.reverse(names);
        }
        return names;
    }

    @Override
    public Vector getNamesVector() {
        List names = this.getNames();
        if (names == null) {
            return null;
        }
        Vector<E> vector = new Vector<E>();
        for (E name : names) {
            vector.add(name);
        }
        return vector;
    }

    @Override
    public abstract Object getValue(String var1);

    public List getValues() {
        List names = this.getNames();
        if (names == null || names.size() == 0) {
            return null;
        }
        ArrayList<Object> values = new ArrayList<Object>(names.size());
        for (String name : values) {
            Object value = this.getValue(name);
            values.add(value);
        }
        return values;
    }

    @Override
    public String toString(String nvSeparator, String separator) {
        List names = this.getNamesSorted();
        if (names == null) {
            return "";
        }
        StringAppender buffer = new StringAppender();
        for (int i = 0; i < names.size(); ++i) {
            String name = (String)names.get(i);
            if (i > 0) {
                buffer.append(separator);
            }
            buffer.append(name);
            buffer.append(nvSeparator);
            String nameLower = name.toLowerCase();
            if (ListUtil.contains(DEFAULT_FILTERED_PASSWORDS, nameLower, PASSWORDS_FILTER_COMPARATOR)) {
                buffer.append("*");
                continue;
            }
            buffer.append(this.getString(name));
        }
        return buffer.toString();
    }

}

