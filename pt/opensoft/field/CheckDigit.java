/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.util.StringUtil;

public abstract class CheckDigit {
    protected int size = -1;
    protected int position = -1;

    protected CheckDigit(int size, int position) {
        if (size <= 0) {
            throw new IllegalArgumentException("size must be > 0");
        }
        this.size = size;
        if (position <= 0) {
            throw new IllegalArgumentException("size must be > 0");
        }
        this.position = position;
    }

    public abstract int calculate(String var1);

    public String format(String num) {
        return this.format(num, this.calculate(num));
    }

    public String format(String num, int checkDigit) {
        String chk = StringUtil.prependChars(String.valueOf(checkDigit), '0', this.size);
        return num.substring(0, this.position) + chk + num.substring(this.position);
    }

    public String getNumber(String str) {
        return str.substring(0, this.position) + str.substring(this.position + this.size);
    }

    public int getCheckDigit(String str) {
        return Integer.parseInt(str.substring(this.position, this.position + this.size));
    }

    public boolean validate(String num) {
        return this.validate(this.getNumber(num), this.getCheckDigit(num));
    }

    public boolean validate(String num, int checkDigit) {
        return checkDigit == this.calculate(num);
    }
}

