/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.ExactNumericField;
import pt.opensoft.math.BigNumber;

public class MoneyField
extends ExactNumericField {
    public MoneyField(String name) {
        super(name);
        this.value = new BigNumber(0);
    }

    public MoneyField(String name, char decimalSeparator) {
        super(name);
        this.value = new BigNumber(0, decimalSeparator);
    }

    public MoneyField(String name, char decimalSeparator, int precision) {
        this(name, decimalSeparator);
        this.setPrecision(precision);
    }

    public MoneyField(String name, String number, char decimalSeparator) {
        super(name);
        this.value = new BigNumber(number, decimalSeparator);
    }

    public MoneyField(String name, String number, int precision) {
        super(name);
        this.value = new BigNumber(number, precision);
    }

    @Override
    public void setPrecision(int precision) {
        ((BigNumber)this.value).setPrecision(precision);
    }

    @Override
    public Object setValue(int value) {
        return this.setValue((long)value);
    }

    @Override
    public Object setValue(long value) {
        ((BigNumber)this.value).setValue(value);
        return this.value;
    }

    @Override
    public Object setValue(Object value) {
        ((BigNumber)this.value).setValue(value.toString());
        return this.value;
    }

    public String toHtml() {
        return this.toString();
    }
}

