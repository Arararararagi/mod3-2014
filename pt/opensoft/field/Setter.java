/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.Setter118;

public abstract class Setter
extends Setter118 {
    protected Setter(String name) {
        super(name);
    }

    protected Setter(String name, boolean caseSensitive) {
        super(name, caseSensitive);
    }

    protected Setter(String name, boolean value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, byte value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, char value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, double value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, float value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, int value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, long value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, Object value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    protected Setter(String name, short value, boolean caseSensitive) {
        super(name, caseSensitive);
        this.setValue(value);
    }

    @Override
    public abstract Object setValue(Object var1);
}

