/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.CheckDigit;
import pt.opensoft.field.CheckDigitMod11;
import pt.opensoft.util.StringUtil;

public class NifValidator {
    protected static final int NIF_LENGTH = 9;
    protected static final int CHECK_DIGIT_LENGTH = 1;
    protected static final CheckDigit checker = new CheckDigitMod11(8);
    protected static final int MIN_NIF = 100000000;
    protected static final int MAX_NIF = 999999999;

    public static int calculateCheckDigit(String num) {
        return checker.calculate(num);
    }

    public static boolean validate(int nif) {
        if (nif < 100000000 || nif > 999999999) {
            return false;
        }
        return checker.validate(String.valueOf(nif));
    }

    public static boolean validate(long nif) {
        if (nif < 100000000 || nif > 999999999) {
            return false;
        }
        return checker.validate(String.valueOf(nif));
    }

    public static boolean validate(String nif) {
        if (nif == null) {
            return false;
        }
        if (nif.length() != 9) {
            return false;
        }
        if (!StringUtil.isNumeric(nif)) {
            return false;
        }
        if (nif.charAt(0) == '0') {
            return false;
        }
        return checker.validate(nif);
    }

    public static boolean validate(String nifToValidate, boolean isNIFWithSubUser) {
        if (isNIFWithSubUser) {
            if (StringUtil.isEmpty(nifToValidate)) {
                return false;
            }
            if (nifToValidate.indexOf("/") == -1) {
                return false;
            }
            String[] nifs = nifToValidate.split("/");
            if (nifs.length != 2) {
                return false;
            }
            String nif = nifs[0];
            String subUser = nifs[1];
            if (!NifValidator.validate(nif)) {
                return false;
            }
            if (!(StringUtil.isNumeric(subUser) && subUser.length() <= 4 && subUser.length() >= 1)) {
                return false;
            }
            return true;
        }
        return NifValidator.validate(nifToValidate);
    }

    public static boolean isSingular(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '1' || firstDigit == '2' || firstDigit == '3' || firstDigit == '4';
    }

    public static boolean isEmpNomeIndividual(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '1' || firstDigit == '2';
    }

    public static boolean isNaoResidenteSingular(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '4';
    }

    public static boolean isColectivo(String nif) {
        char[] digits = new char[]{nif.charAt(0), nif.charAt(1)};
        return digits[0] == '5' || digits[0] == '9' && digits[1] == '7' || digits[0] == '9' && digits[1] == '8';
    }

    public static boolean isNaoResidente(String nif) {
        char[] digits = new char[]{nif.charAt(0), nif.charAt(1)};
        return digits[0] == '9' && digits[1] == '8';
    }

    public static boolean isEstado(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '6';
    }

    public static boolean isEspecial(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '7';
    }

    public static boolean isCabecaCasalHerancaIndivisa(String nif) {
        char[] digits = new char[]{nif.charAt(0), nif.charAt(1)};
        return digits[0] == '7' && digits[1] == '0' || digits[0] == '7' && digits[1] == '4' || digits[0] == '9' && digits[1] == '0' || digits[0] == '9' && digits[1] == '1';
    }

    public static boolean isNaoResidenteRetencaoFonte(String nif) {
        char[] digits = new char[]{nif.charAt(0), nif.charAt(1)};
        return digits[0] == '7' && digits[1] == '1';
    }

    public static boolean isFundos(String nif) {
        char[] digits = new char[]{nif.charAt(0), nif.charAt(1)};
        return digits[0] == '7' && digits[1] == '2';
    }

    public static boolean isEspecialRegimeExpo98(String nif) {
        char[] digits = new char[]{nif.charAt(0), nif.charAt(1)};
        return digits[0] == '7' && digits[1] == '9';
    }

    public static boolean isDF(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '7';
    }

    public static boolean isNotario(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '7' || firstDigit == '6' || firstDigit == '5' || firstDigit == '2' || firstDigit == '1';
    }

    public static boolean isGrupoSociedade(String nif) {
        char firstDigit = nif.charAt(0);
        return firstDigit == '5';
    }

    public static boolean isEntidadesEspeciaisTributacao(String nif) {
        char firstDigit = nif.charAt(0);
        char secondDigit = nif.charAt(1);
        return firstDigit == '4' && secondDigit == '5';
    }

    public static boolean checkFirstDigit(String nif, char firstDigit) {
        return StringUtil.checkFirstDigit(nif, firstDigit);
    }

    public static boolean checkFirstDigit(String nif, String digits) {
        return StringUtil.checkFirstDigit(nif, digits);
    }

    public static boolean checkDigitAt(String nif, char digit, int position) {
        return StringUtil.checkCharactertAt(nif, digit, position);
    }

    private NifValidator() {
    }
}

