/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.NumericField;
import pt.opensoft.math.BigNumber;

public class ExactNumericField
extends NumericField {
    protected static final char THOUSAND_SEPARATOR = '.';
    protected static final char DECIMAL_SEPARATOR = ',';
    protected static final int DEFAULT_PRECISION = 2;

    public ExactNumericField(String name, String number, char decimalSeparator, int precision) {
        super(name);
        this.value = new BigNumber(number, decimalSeparator);
        this.setPrecision(precision);
    }

    public ExactNumericField(String name, long number, char decimalSeparator, int precision) {
        this(name, String.valueOf(number), decimalSeparator, precision);
    }

    public ExactNumericField(String name) {
        super(name);
    }

    public ExactNumericField(String name, long value) {
        this(name, value, ',', 2);
    }

    public ExactNumericField(String name, long value, int precision) {
        this(name, value, ',', precision);
    }

    public ExactNumericField(String name, String number, int precision) {
        this(name, number, ',', precision);
    }

    public ExactNumericField(String name, Object value) {
        super(name, value);
    }

    public ExactNumericField(String name, short value) {
        this(name, String.valueOf(value), 44);
    }

    public void setPrecision(int precision) {
        ((BigNumber)this.value).setPrecision(precision);
    }

    public int getPrecision() {
        return ((BigNumber)this.value).getPrecision();
    }

    public String format() {
        return ((BigNumber)this.value).format();
    }

    @Override
    public String toString() {
        return this.getValue().toString();
    }

    @Override
    public Object getValue() {
        return new Long(((BigNumber)this.value).getValue());
    }

    @Override
    public Object setValue(long value) {
        this.value = new BigNumber(value, ',');
        return this.value;
    }
}

