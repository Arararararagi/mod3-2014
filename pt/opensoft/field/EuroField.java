/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.MoneyField;
import pt.opensoft.math.BigNumber;
import pt.opensoft.util.NumberUtil;
import pt.opensoft.util.StringUtil;

public class EuroField
extends MoneyField {
    private static final double PTE_RATE = 200.482;
    private static final char EURO_DECIMAL_SEPARATOR = ',';
    private static final char EURO_THOUSAND_SEPARATOR = '.';
    private static final int EURO_PRECISION = 2;

    public EuroField(String name) {
        super(name, ',', 2);
    }

    public EuroField(String name, String centims) {
        this(name, centims, 2);
    }

    public EuroField(String name, long centims) {
        this(name, String.valueOf(centims));
    }

    public EuroField(String name, String centims, int precision) {
        super(name, EuroField.removeThousandSeparators(centims), ',');
        this.setPrecision(precision);
    }

    public long getCentims() {
        return ((BigNumber)this.value).getValue();
    }

    @Override
    public String toHtml() {
        return "&euro; " + this.toString();
    }

    @Override
    public String toString() {
        return this.format();
    }

    public Object setValue(String value) {
        Object ret = super.setValue(value);
        super.setPrecision(2);
        return ret;
    }

    public static boolean isValid(String euro) {
        if (euro == null || euro.trim().length() == 0) {
            return false;
        }
        int pos = (euro = EuroField.removeThousandSeparators(euro)).indexOf(44);
        if (pos == -1) {
            return false;
        }
        if (pos != euro.length() - 2 - 1) {
            return false;
        }
        if (!StringUtil.isNumeric(StringUtil.remove(euro, String.valueOf(',')))) {
            return false;
        }
        return true;
    }

    public static EuroField fromValidEuro(String name, String euro) {
        if ((euro = EuroField.removeThousandSeparators(euro)).lastIndexOf(",") == -1) {
            euro = euro + "00";
        }
        return new EuroField(name, StringUtil.remove(euro, String.valueOf(',')));
    }

    public static String removeThousandSeparators(String euro) {
        StringBuffer buffer = new StringBuffer(euro);
        int thou = buffer.toString().indexOf(46);
        while (thou != -1) {
            buffer.deleteCharAt(thou);
            thou = buffer.toString().indexOf(46);
        }
        return buffer.toString();
    }

    public String toFullLength() {
        return EuroField.toFullLength(this.getLongValue());
    }

    public static String toFullLength(long valor) {
        long centimos;
        if (valor == 0) {
            return "zero euros";
        }
        StringBuffer buffer = new StringBuffer();
        long euros = valor / 100;
        if (euros > 0) {
            buffer.append(NumberUtil.toFullLength(euros));
            buffer.append(euros > 1 ? " euros" : " euro");
        }
        if ((centimos = valor % 100) > 0) {
            if (euros > 0) {
                buffer.append(" e ");
            }
            buffer.append(NumberUtil.toFullLength(centimos));
            buffer.append(centimos > 1 ? " c\u00eantimos" : " c\u00eantimo");
        }
        return buffer.toString();
    }

    public long toPTE() {
        return Math.round(this.getDoubleValue() * 200.482);
    }

    public static String removeSeparators(String euro) {
        return EuroField.removeSeparators(euro, true);
    }

    public static String removeSeparators(String euro, boolean trim) {
        if (euro == null) {
            return null;
        }
        String euroStr = EuroField.removeThousandSeparators(euro);
        StringBuffer buffer = new StringBuffer(euroStr);
        int thou = buffer.toString().indexOf(44);
        while (thou != -1) {
            buffer.deleteCharAt(thou);
            thou = buffer.toString().indexOf(44);
        }
        if (trim) {
            return buffer.toString().trim();
        }
        return buffer.toString();
    }

    public static boolean isEmpty(String euro) {
        return euro == null || StringUtil.isEmpty(EuroField.removeSeparators(euro));
    }

    public static boolean isEmpty(String euro, boolean trim) {
        return euro == null || StringUtil.isEmpty(EuroField.removeSeparators(euro), trim);
    }
}

