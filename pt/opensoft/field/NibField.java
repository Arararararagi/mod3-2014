/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.NibValidator;
import pt.opensoft.field.NumericField;

public class NibField
extends NumericField {
    protected static final String DEFAULT_SEPARATOR = " ";

    public static int calculateCheckDigit(String num) {
        return NibValidator.calculateCheckDigit(num);
    }

    public static boolean validate(int nib) {
        return NibValidator.validate(String.valueOf(nib));
    }

    public static boolean validate(String nib) {
        return NibValidator.validate(nib);
    }

    public NibField(String name) {
        super(name);
    }

    public NibField(String name, long nib) {
        super(name);
        this.setValue(nib);
    }

    public String format() {
        return this.format(" ");
    }

    public String format(String separator) {
        String nib = this.getString();
        StringBuffer buffer = new StringBuffer();
        buffer.append(nib.substring(0, 4));
        buffer.append(separator);
        buffer.append(nib.substring(4, 8));
        buffer.append(separator);
        buffer.append(nib.substring(8, 19));
        buffer.append(separator);
        buffer.append(nib.substring(19, 21));
        return buffer.toString();
    }
}

