/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.field;

import pt.opensoft.field.NamedGetter118;

public abstract class NamedSetter118
extends NamedGetter118 {
    protected NamedSetter118() {
        super("NamedSetter");
    }

    protected NamedSetter118(String name) {
        super(name);
    }

    protected NamedSetter118(String name, boolean caseSensitive) {
        super(name, caseSensitive);
    }

    public abstract Object remove(String var1);

    public Object setValue(String name, boolean value) {
        return this.setValue(name, new Boolean(value));
    }

    public Object setValue(String name, byte value) {
        return this.setValue(name, new Byte(value));
    }

    public Object setValue(String name, char value) {
        return this.setValue(name, new Character(value));
    }

    public Object setValue(String name, double value) {
        return this.setValue(name, new Double(value));
    }

    public Object setValue(String name, float value) {
        return this.setValue(name, new Float(value));
    }

    public Object setValue(String name, int value) {
        return this.setValue(name, new Integer(value));
    }

    public Object setValue(String name, long value) {
        return this.setValue(name, new Long(value));
    }

    public abstract Object setValue(String var1, Object var2);

    public Object setValue(String name, short value) {
        return this.setValue(name, new Short(value));
    }
}

