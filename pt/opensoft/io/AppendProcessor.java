/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import pt.opensoft.io.LineProcessor;
import pt.opensoft.text.Appender;
import pt.opensoft.text.StringAppender;

public class AppendProcessor
implements LineProcessor {
    protected StringAppender appender = new StringAppender();

    @Override
    public String process(String line) {
        this.appender.append(line);
        this.appender.newLine();
        return line;
    }

    public String toString() {
        return this.appender.toString();
    }
}

