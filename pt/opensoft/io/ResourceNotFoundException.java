/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.IOException;

public class ResourceNotFoundException
extends IOException {
    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(String s) {
        super(s);
    }
}

