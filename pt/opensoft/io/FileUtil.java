/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.zip.CRC32;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import pt.opensoft.io.AppendProcessor;
import pt.opensoft.io.LineProcessor;
import pt.opensoft.io.ReadUtil;
import pt.opensoft.io.ResourceLoader;
import pt.opensoft.text.Appender;
import pt.opensoft.text.StringAppender;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.SystemParameters;

public class FileUtil {
    public static final int DEFAULT_BUFFER_SIZE = SystemParameters.FILE_BLOCK_SIZE;
    private static final String ZIP_FILE_EXTENSION = "zip";

    private FileUtil() {
    }

    public static File copy(File file, File dest) throws IOException {
        try {
            FileReader reader = new FileReader(file);
            try {
                FileOutputStream out = new FileOutputStream(dest);
                try {
                    OutputStreamWriter writer = new OutputStreamWriter(out);
                    try {
                        char[] buffer = new char[DEFAULT_BUFFER_SIZE * 10];
                        int read = reader.read(buffer);
                        while (read != -1) {
                            writer.write(buffer, 0, read);
                            read = reader.read(buffer);
                        }
                    }
                    finally {
                        try {
                            out.getFD().sync();
                        }
                        finally {
                            writer.close();
                        }
                    }
                }
                finally {
                    out.close();
                }
            }
            finally {
                reader.close();
            }
        }
        catch (IOException e) {
            dest.delete();
            throw e;
        }
        return dest;
    }

    public static String readFile(String filename) throws IOException {
        return FileUtil.readFile(new File(filename));
    }

    public static String readFile(File file) throws IOException {
        StringAppender appender = new StringAppender(DEFAULT_BUFFER_SIZE);
        FileUtil.readFile(file, (Appender)appender);
        return appender.toString();
    }

    public static String readFile(InputStream in) throws IOException {
        StringAppender appender = new StringAppender(DEFAULT_BUFFER_SIZE);
        FileUtil.readFile(in, (Appender)appender);
        return appender.toString();
    }

    public static void readFile(File file, Appender appender) throws IOException {
        InputStream in = ResourceLoader.getResourceStream(file);
        try {
            FileUtil.readFile(in, appender);
        }
        finally {
            in.close();
        }
    }

    public static void readFile(InputStream in, Appender appender) throws IOException {
        InputStreamReader reader = new InputStreamReader(in, "ISO-8859-1");
        try {
            char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            int read = -1;
            while ((read = reader.read(buffer, 0, buffer.length)) != -1) {
                appender.append(buffer, 0, read);
            }
        }
        finally {
            reader.close();
        }
    }

    public static byte[] readBinaryFile(File file) throws IOException {
        InputStream in = ResourceLoader.getResourceStream(file);
        try {
            byte[] arrby;
            ByteArrayOutputStream bOut = new ByteArrayOutputStream((int)file.length());
            try {
                byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                int read = -1;
                while ((read = in.read(buffer, 0, buffer.length)) != -1) {
                    bOut.write(buffer, 0, read);
                }
                arrby = bOut.toByteArray();
            }
            catch (Throwable var6_6) {
                bOut.close();
                throw var6_6;
            }
            bOut.close();
            return arrby;
        }
        finally {
            in.close();
        }
    }

    public static void writeBinaryFile(File file, byte[] content) throws IOException, FileNotFoundException {
        FileOutputStream out = new FileOutputStream(file);
        try {
            out.write(content);
        }
        finally {
            out.close();
        }
    }

    public static void process(File file, LineProcessor processor) throws IOException {
        File tmp = new File(file + ".tmp");
        try {
            FileReader freader = new FileReader(file);
            try {
                BufferedReader reader = new BufferedReader(freader);
                try {
                    FileWriter fwriter = new FileWriter(tmp);
                    try {
                        PrintWriter writer = new PrintWriter(fwriter, true);
                        try {
                            String line = null;
                            while ((line = reader.readLine()) != null) {
                                String processed = processor.process(line);
                                writer.println(processed);
                            }
                        }
                        finally {
                            writer.close();
                        }
                    }
                    finally {
                        fwriter.close();
                    }
                }
                finally {
                    reader.close();
                }
            }
            finally {
                freader.close();
            }
            if (!file.delete()) {
                throw new IOException("could not delete original file: " + file);
            }
        }
        finally {
            if (!tmp.delete()) {
                throw new IOException("could not delete tmp file: " + tmp);
            }
        }
    }

    public static String tail(File file, long bytes) throws Exception {
        AppendProcessor processor = new AppendProcessor();
        FileUtil.processTail(file, bytes, processor, true);
        return processor.toString();
    }

    public static void processTail(File file, long bytes, LineProcessor processor, boolean forever) throws Exception {
        long mark;
        if (bytes < 1) {
            throw new IllegalArgumentException("Invalid number of bytes: " + bytes);
        }
        if (file.length() < 1) {
            return;
        }
        long skip = file.length() - bytes;
        long l = mark = skip > 0 ? skip : 0;
        do {
            if (mark == file.length()) {
                Thread.sleep(1000);
                continue;
            }
            FileReader freader = new FileReader(file);
            try {
                BufferedReader reader = new BufferedReader(freader, DEFAULT_BUFFER_SIZE);
                try {
                    if (skip > 0) {
                        freader.skip(skip);
                        reader.readLine();
                    }
                    mark+=ReadUtil.process(reader, processor);
                }
                finally {
                    reader.close();
                }
            }
            finally {
                freader.close();
            }
            if (!forever) break;
        } while (true);
    }

    public static long getCRC32(File file) throws IOException {
        CRC32 crc32;
        FileInputStream in = new FileInputStream(file);
        crc32 = new CRC32();
        try {
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int read = -1;
            while ((read = in.read(buffer, 0, buffer.length)) != -1) {
                crc32.update(buffer, 0, read);
            }
        }
        finally {
            in.close();
        }
        return crc32.getValue();
    }

    public static String getDirectory(String path) {
        if (path == null) {
            return null;
        }
        if (path.trim().length() == 0) {
            return null;
        }
        String dir = path.replace('\\', '/');
        int pos = dir.lastIndexOf("/");
        if (pos == -1) {
            return null;
        }
        return dir.substring(0, pos);
    }

    public static String getFilename(String path) {
        if (path == null) {
            return null;
        }
        if (path.trim().length() == 0) {
            return null;
        }
        String filename = path.replace('\\', '/');
        int pos = filename.lastIndexOf("/");
        if (pos == -1) {
            return filename;
        }
        return filename.substring(pos + 1);
    }

    public static String getFilenameOnly(String path) {
        if (path == null) {
            return null;
        }
        String filename = FileUtil.getFilename(path);
        String extension = FileUtil.getExtension(path);
        if (extension == null || extension.equals("")) {
            return filename;
        }
        return filename.substring(0, filename.length() - extension.length() - 1);
    }

    public static String getNormalizedPath(String path) {
        if (path == null) {
            return null;
        }
        if (path.trim().length() == 0) {
            return null;
        }
        path = StringUtil.replace(path, "\\\\", "\\");
        path = path.replace('\\', '/');
        return path;
    }

    public static String getExtension(String fileName) {
        if (fileName == null) {
            return null;
        }
        int idx = fileName.lastIndexOf(".");
        if (idx == -1) {
            return "";
        }
        return fileName.substring(idx + 1, fileName.length());
    }

    public static boolean isZipFile(String fileName) {
        return FileUtil.checkFileExtension(fileName, "zip");
    }

    public static boolean checkFileExtension(String filename, String extension) {
        String ext = FileUtil.getExtension(filename);
        if (ext == null && extension == null) {
            return true;
        }
        return extension.equalsIgnoreCase(FileUtil.getExtension(filename));
    }

    public static boolean checkExtensionIn(String filename, String[] extensions) {
        if (filename == null) {
            return true;
        }
        for (int i = 0; i < extensions.length; ++i) {
            if (!FileUtil.checkFileExtension(filename, extensions[i])) continue;
            return true;
        }
        return false;
    }

    public static boolean checkExtensionIn(String filename, String extensions) {
        if (filename == null) {
            return true;
        }
        return extensions.toLowerCase().indexOf(FileUtil.getExtension(filename).toLowerCase()) != -1;
    }

    public static void writeToFile(InputStream in, File dest) throws IOException {
        try {
            FileOutputStream out = new FileOutputStream(dest);
            try {
                byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                int read = in.read(buffer);
                while (read != -1) {
                    out.write(buffer, 0, read);
                    read = in.read(buffer);
                }
            }
            finally {
                out.close();
            }
        }
        finally {
            in.close();
        }
    }

    public static File gzip(String filename) throws IOException {
        return FileUtil.gzip(new File(filename));
    }

    public static File gzip(File file) throws IOException {
        File result;
        result = new File(file.getPath() + ".gz");
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
            try {
                GZIPOutputStream out = new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(result), DEFAULT_BUFFER_SIZE));
                try {
                    byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                    int read = in.read(buffer);
                    while (read != -1) {
                        out.write(buffer, 0, read);
                        read = in.read(buffer);
                    }
                }
                finally {
                    out.close();
                }
            }
            finally {
                in.close();
            }
        }
        catch (IOException e) {
            result.delete();
            throw e;
        }
        file.delete();
        return result;
    }

    public static File gunzip(String filename) throws IOException {
        return FileUtil.gunzip(new File(filename));
    }

    private static File gunzip(File file) throws IOException {
        File result;
        if (!FileUtil.checkExtensionIn(file.getName(), "gz,tgz")) {
            throw new IOException("unrecognized extension " + FileUtil.getExtension(file.getName()));
        }
        result = new File(FileUtil.getDirectory(file.getPath()), FileUtil.getFilenameOnly(file.getPath()));
        try {
            GZIPInputStream in = new GZIPInputStream(new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE));
            try {
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(result), DEFAULT_BUFFER_SIZE);
                try {
                    byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                    int read = in.read(buffer);
                    while (read != -1) {
                        out.write(buffer, 0, read);
                        read = in.read(buffer);
                    }
                }
                finally {
                    out.close();
                }
            }
            finally {
                in.close();
            }
        }
        catch (IOException e) {
            result.delete();
            throw e;
        }
        return result;
    }

    public static boolean deleteDirectory(File file) {
        if (file.exists()) {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; ++i) {
                if (files[i].isDirectory()) {
                    FileUtil.deleteDirectory(files[i]);
                    continue;
                }
                files[i].delete();
            }
        }
        return file.delete();
    }
}

