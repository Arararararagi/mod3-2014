/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import pt.opensoft.io.FileUtil;
import pt.opensoft.io.LineProcessor;
import pt.opensoft.text.Appender;
import pt.opensoft.text.StringAppender;

public class ReadUtil {
    public static final int DEFAULT_BUFFER_SIZE = FileUtil.DEFAULT_BUFFER_SIZE;

    private ReadUtil() {
    }

    public static String read(InputStream in) throws IOException {
        StringAppender appender = new StringAppender();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int read = in.read(buffer);
        while (read != -1) {
            appender.append(buffer, 0, read);
            read = in.read(buffer);
        }
        return appender.toString();
    }

    public static long process(BufferedReader reader, LineProcessor processor) throws IOException {
        long read = 0;
        String line = reader.readLine();
        while (line != null) {
            processor.process(line);
            read+=(long)line.length();
            line = reader.readLine();
        }
        return read;
    }
}

