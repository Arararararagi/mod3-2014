/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.File;
import java.io.InputStream;
import java.util.Set;
import pt.opensoft.io.ResourceFactory;
import pt.opensoft.io.ResourceNotFoundException;

public class ClasspathResourceFactory
implements ResourceFactory {
    private Class _classThatLoadsResources;
    private boolean _alwaysUseAbsolutePaths;

    public ClasspathResourceFactory() {
        this(false, null);
    }

    public ClasspathResourceFactory(Class classThatLoadsResources) {
        this(false, classThatLoadsResources);
    }

    public ClasspathResourceFactory(boolean alwaysUseAbsolutePaths) {
        this(alwaysUseAbsolutePaths, null);
    }

    public ClasspathResourceFactory(boolean alwaysUseAbsolutePaths, Class classThatLoadsResources) {
        this._alwaysUseAbsolutePaths = alwaysUseAbsolutePaths;
        this._classThatLoadsResources = classThatLoadsResources == null ? this.getClass() : classThatLoadsResources;
    }

    @Override
    public InputStream getResourceStream(String source) throws ResourceNotFoundException {
        if (source.startsWith("./")) {
            source = source.substring(1);
        } else if (this._alwaysUseAbsolutePaths && !source.startsWith("/")) {
            source = "/" + source;
        }
        InputStream in = null;
        try {
            in = this._classThatLoadsResources.getResourceAsStream(source);
        }
        catch (Exception e) {
            throw new ResourceNotFoundException("[ResourceLoader] Resource " + source + " not found in classpath: " + e.getMessage());
        }
        if (in == null) {
            throw new ResourceNotFoundException("[ResourceLoader] Resource " + source + " not found in classpath.");
        }
        return in;
    }

    @Override
    public Set getResourcePaths(String path) throws ResourceNotFoundException {
        return null;
    }

    @Override
    public long lastModified(File file) {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj instanceof ClasspathResourceFactory) {
            return this.hashCode() == obj.hashCode();
        }
        return super.equals(obj);
    }

    public int hashCode() {
        return this.getClass().getName().hashCode();
    }
}

