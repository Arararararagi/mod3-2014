/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.StringUtil;

public class VariableSizeWriter
extends Writer {
    protected PrintWriter _writer = null;
    protected int _lineLength = 0;
    protected int _registerSize = 0;

    public VariableSizeWriter(Writer writer, Integer registerSize) {
        this(writer, (int)registerSize);
    }

    public VariableSizeWriter(Writer writer, int registerSize) {
        this._writer = writer instanceof PrintWriter ? (PrintWriter)writer : new PrintWriter(writer, true);
        this._registerSize = registerSize;
    }

    public VariableSizeWriter(OutputStream out, Integer registerSize) {
        this(out, (int)registerSize);
    }

    public VariableSizeWriter(OutputStream out, int registerSize) {
        this._writer = new PrintWriter(out, true);
        this._registerSize = registerSize;
    }

    public int getLineLength() {
        return this._lineLength;
    }

    public void writeString(String str, int size) throws IOException {
        if (this._lineLength >= this._registerSize) {
            throw new IOException("exceeded register size '" + this._lineLength + "' >= '" + this._registerSize + "'");
        }
        if (str == null) {
            str = "";
        }
        if (str.length() > size) {
            str = str.substring(0, size);
        }
        this._writer.print(StringUtil.padChars(str, ' ', size, false));
        this._lineLength+=size;
    }

    public void writeNumeric(String num, int size) throws IOException {
        int len;
        if (this._lineLength >= this._registerSize) {
            throw new IOException("exceeded register size '" + this._lineLength + "' >= '" + this._registerSize + "'");
        }
        if (num == null) {
            num = "";
        }
        if ((len = num.length()) > size) {
            num = num.substring(0, size);
        }
        this._writer.print(StringUtil.padChars(num, '0', size, true));
        this._lineLength+=size;
    }

    public void writeNumeric(Integer num, int size) throws IOException {
        this.writeNumeric(num.toString(), size);
    }

    public void writeNumeric(int num, int size) throws IOException {
        this.writeNumeric(new Integer(num), size);
    }

    public void writeNumeric(long num, int size) throws IOException {
        this.writeNumeric(String.valueOf(num), size);
    }

    public void writeNumeric(byte num, int size) throws IOException {
        this.writeNumeric(String.valueOf(num), size);
    }

    public void writeNumeric(short num, int size) throws IOException {
        this.writeNumeric(String.valueOf(num), size);
    }

    public void writeDate(DateTime date, String format) throws IOException {
        if (this._lineLength >= this._registerSize) {
            throw new IOException("exceeded register size '" + this._lineLength + "' >= '" + this._registerSize + "'");
        }
        String str = date != null ? date.format(format) : VariableSizeWriter.getEmptyDate(format);
        this._writer.print(str);
        this._lineLength+=str.length();
    }

    public static String getEmptyDate(String format) {
        String str = new String(format);
        str = StringUtil.replaceIgnoreCase(str, "y", "0");
        str = StringUtil.replaceIgnoreCase(str, "m", "0");
        str = StringUtil.replaceIgnoreCase(str, "d", "0");
        return str;
    }

    public void writeln() {
        this._writer.print(StringUtil.padChars(null, ' ', this._registerSize - this._lineLength, false));
        this._writer.println();
        this._lineLength = 0;
    }

    public void writeLine() {
        this._writer.println();
        ++this._lineLength;
    }

    public void writeAll(String str) {
        this._writer.print(str);
    }

    @Override
    public void flush() throws IOException {
        this._writer.flush();
    }

    @Override
    public void close() throws IOException {
        this._writer.close();
    }

    @Override
    public void write(char[] buffer, int off, int len) throws IOException {
        this._writer.write(buffer, off, len);
    }
}

