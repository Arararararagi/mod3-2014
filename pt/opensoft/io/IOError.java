/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.IOException;
import pt.opensoft.util.WrappedError;

public class IOError
extends WrappedError {
    public IOError(IOException thrown) {
        super(thrown);
    }
}

