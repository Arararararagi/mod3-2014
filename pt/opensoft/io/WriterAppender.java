/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import pt.opensoft.io.IOError;
import pt.opensoft.text.AbstractAppender;
import pt.opensoft.text.Appender;

public class WriterAppender
extends AbstractAppender {
    public static final String DEFAULT_ENCODING = "ISO8859_1";
    protected BufferedWriter writer;
    protected int length = 0;

    public WriterAppender(OutputStream out) {
        this(out, 2048);
    }

    public WriterAppender(OutputStream out, int size) {
        try {
            this.writer = new BufferedWriter(new OutputStreamWriter(out, "ISO8859_1"), size);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public WriterAppender(Writer writer) {
        this(writer, 2048);
    }

    public WriterAppender(Writer writer, int size) {
        this(new BufferedWriter(writer, size));
    }

    public WriterAppender(BufferedWriter writer) {
        this.writer = writer;
    }

    @Override
    public Appender append(char ch) {
        try {
            this.writer.write(ch);
        }
        catch (IOException e) {
            throw new IOError(e);
        }
        ++this.length;
        this.flush();
        return this;
    }

    @Override
    public Appender append(char[] chars, int off, int len) {
        try {
            this.writer.write(chars, off, len);
        }
        catch (IOException e) {
            throw new IOError(e);
        }
        this.length+=len;
        return this;
    }

    public void close() throws IOException {
        this.writer.close();
    }

    public void flush() {
        if (this.writer == null) {
            return;
        }
        try {
            this.writer.flush();
        }
        catch (IOException e) {
            throw new IOError(e);
        }
    }

    @Override
    public int length() {
        return this.length;
    }

    @Override
    protected void reset(int size) {
        this.flush();
    }

    @Override
    public Appender newLine() {
        super.newLine();
        this.flush();
        return this;
    }
}

