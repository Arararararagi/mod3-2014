/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.File;
import java.io.InputStream;
import java.util.Set;
import pt.opensoft.io.FileResourceFactory;
import pt.opensoft.io.FileUtil;
import pt.opensoft.io.ResourceFactory;
import pt.opensoft.io.ResourceNotFoundException;
import pt.opensoft.logging.Logger;
import pt.opensoft.util.FIFOSet;

public class ResourceLoader {
    private static Set resourceFactories = new FIFOSet();
    private static boolean debug = false;
    private static Logger logger;

    public static void addResourceFactory(ResourceFactory factory) {
        resourceFactories.add(factory);
    }

    public static void setDebug(boolean debug) {
        ResourceLoader.debug = debug;
        logger = Logger.getDefault();
    }

    public static void setDebug(boolean debug, Logger logger) {
        ResourceLoader.debug = debug;
        if (debug && logger == null) {
            throw new IllegalArgumentException("logger can't be null");
        }
        ResourceLoader.logger = logger;
    }

    private ResourceLoader() {
    }

    public static InputStream getResourceStream(File source) throws ResourceNotFoundException {
        return ResourceLoader.getResourceStream(source.getPath());
    }

    public static InputStream getResourceStream(String source) throws ResourceNotFoundException {
        if (debug) {
            logger.info("[ResourceLoader] Request resource: " + source);
        }
        if (source == null || source.length() == 0) {
            throw new ResourceNotFoundException("[ResourceLoader] Need to specify a file!");
        }
        source = FileUtil.getNormalizedPath(source);
        InputStream in = null;
        for (ResourceFactory resourceFactory : resourceFactories) {
            try {
                in = resourceFactory.getResourceStream(source);
                if (!debug) break;
                logger.info("[ResourceLoader] Found resource (breaking): " + source + " using " + resourceFactory);
                break;
            }
            catch (ResourceNotFoundException e) {
                if (!debug) continue;
                logger.info("[ResourceLoader] Tried and failed to find resource: " + source + " using " + resourceFactory);
                continue;
            }
        }
        if (in == null) {
            throw new ResourceNotFoundException("[ResourceLoader] Resource not found in any Resource Factory: " + source);
        }
        return in;
    }

    public static Set getResourcePaths(String path) throws ResourceNotFoundException {
        try {
            for (ResourceFactory resourceFactory : resourceFactories) {
                Set paths = resourceFactory.getResourcePaths(path);
                if (paths == null) continue;
                return paths;
            }
        }
        catch (Exception ex) {
            throw new ResourceNotFoundException("[ResourceLoader] Can't get resource paths for " + path + " not found in any resource factory: " + ex.getMessage());
        }
        throw new ResourceNotFoundException("[ResourceLoader] Can't get resource paths for " + path + " not found in any resource factory");
    }

    public static long lastModified(File file) {
        for (ResourceFactory resourceFactory : resourceFactories) {
            long lastModified = resourceFactory.lastModified(file);
            if (lastModified == 0) continue;
            return lastModified;
        }
        return 0;
    }

    static {
        ResourceLoader.addResourceFactory(new FileResourceFactory());
    }
}

