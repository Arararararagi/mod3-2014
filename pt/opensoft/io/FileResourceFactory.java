/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Set;
import pt.opensoft.io.ResourceFactory;
import pt.opensoft.io.ResourceNotFoundException;

public class FileResourceFactory
implements ResourceFactory {
    private File basedir;

    public FileResourceFactory() {
    }

    public FileResourceFactory(String basedir) {
        this.basedir = new File(basedir);
    }

    @Override
    public InputStream getResourceStream(String source) throws ResourceNotFoundException {
        try {
            File sourceFile = this.basedir != null ? new File(this.basedir, source) : new File(source);
            return new FileInputStream(sourceFile);
        }
        catch (FileNotFoundException e) {
            throw new ResourceNotFoundException("[ResourceLoader] Resource " + source + " not found in file system.");
        }
    }

    @Override
    public Set getResourcePaths(String path) throws ResourceNotFoundException {
        return null;
    }

    @Override
    public long lastModified(File file) {
        if (file == null) {
            return 0;
        }
        if (file.exists()) {
            return file.lastModified();
        }
        return 0;
    }

    public String toString() {
        return "FileResourceFactory";
    }

    public boolean equals(Object obj) {
        if (obj instanceof FileResourceFactory) {
            return this.hashCode() == obj.hashCode();
        }
        return super.equals(obj);
    }

    public int hashCode() {
        if (this.basedir == null) {
            return this.getClass().getName().hashCode();
        }
        return this.getClass().getName().hashCode() + this.basedir.getPath().hashCode();
    }
}

