/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.io;

import java.io.File;
import java.io.InputStream;
import java.util.Set;
import pt.opensoft.io.ResourceNotFoundException;

public interface ResourceFactory {
    public InputStream getResourceStream(String var1) throws ResourceNotFoundException;

    public Set getResourcePaths(String var1) throws ResourceNotFoundException;

    public long lastModified(File var1);
}

