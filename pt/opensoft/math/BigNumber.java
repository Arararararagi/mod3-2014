/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.math;

import pt.opensoft.math.InvalidNumberException;
import pt.opensoft.util.StringUtil;

public class BigNumber {
    protected static final char DECIMAL_SEPARATOR = ',';
    protected long _value;
    protected int _decimalPlaces;
    protected char _decimalSeparator = 44;
    protected boolean _explicitSignal = false;

    public BigNumber(long value) {
        this.setValue(value);
    }

    public BigNumber(long value, char decimalSeparator) {
        this.setValue(value);
        this._decimalSeparator = decimalSeparator;
    }

    public BigNumber(String number) {
        this.parse(number);
    }

    public BigNumber(String number, char decimalSeparator) {
        this._decimalSeparator = decimalSeparator;
        this.parse(number);
    }

    public void setExplicitSignal(boolean explicitSignal) {
        this._explicitSignal = explicitSignal;
    }

    public BigNumber(String number, int precision) {
        try {
            if (number != null && number.startsWith("+")) {
                number = number.substring(1);
            }
            this._value = Long.parseLong(number);
        }
        catch (NumberFormatException e) {
            throw new InvalidNumberException("Number is not valid:" + number);
        }
        this._decimalPlaces = precision;
    }

    protected void parse(String number) {
        int idx;
        String numberWithoutDecimalSeparator;
        if (null == number) {
            this._value = 0;
            this._decimalPlaces = 0;
            return;
        }
        if ((number = number.trim()).substring((idx = number.indexOf(this._decimalSeparator)) + 1).indexOf(this._decimalSeparator) != -1) {
            throw new InvalidNumberException("Number contains more than one decimal separator:" + number);
        }
        if (idx == number.length() - 1) {
            throw new InvalidNumberException("Number cannot end with a decimal separator:" + number);
        }
        if (idx != -1) {
            numberWithoutDecimalSeparator = number.substring(0, idx) + number.substring(idx + 1, number.length());
            this._decimalPlaces = number.length() - idx - 1;
        } else {
            numberWithoutDecimalSeparator = number;
            this._decimalPlaces = 0;
        }
        try {
            if (numberWithoutDecimalSeparator != null && numberWithoutDecimalSeparator.startsWith("+")) {
                numberWithoutDecimalSeparator = numberWithoutDecimalSeparator.substring(1);
            }
            this._value = Long.parseLong(numberWithoutDecimalSeparator);
        }
        catch (NumberFormatException e) {
            throw new InvalidNumberException("Number is not valid:" + number);
        }
    }

    public int getPrecision() {
        return this._decimalPlaces;
    }

    public long getValue() {
        return this._value;
    }

    public void setPrecision(int decimalPlaces) {
        this._decimalPlaces = decimalPlaces;
    }

    public void setValue(long value) {
        this._value = value;
    }

    public void setValue(String value) {
        this.parse(value);
    }

    public String toString() {
        return this.toString(this._decimalSeparator);
    }

    public String toString(char decimalSeparator) {
        if (this._decimalPlaces > 0) {
            String decimalPart = this.getDecimalPart().equals("") ? "0" : this.getDecimalPart();
            String integerPart = this.getIntegerPart().equals("") ? "0" : this.getIntegerPart();
            return integerPart + decimalSeparator + decimalPart;
        }
        return "" + this._value;
    }

    public String toXml() {
        return this.toString('.');
    }

    public String format() {
        String value = this.getIntegerPart();
        StringBuffer buffer = new StringBuffer();
        int currentIdx = value.length() - 1;
        int ternoIdx = 0;
        while (currentIdx > 2) {
            while (ternoIdx < 3) {
                buffer.append(value.charAt(currentIdx));
                ++ternoIdx;
                --currentIdx;
            }
            if (value.charAt(currentIdx) != '-' && value.charAt(currentIdx) != '+') {
                buffer.append(".");
            }
            ternoIdx = 0;
        }
        while (currentIdx >= 0) {
            buffer.append(value.charAt(currentIdx));
            --currentIdx;
        }
        String decimalPart = this.getDecimalPart();
        if (!decimalPart.equals("")) {
            return buffer.reverse().toString() + this._decimalSeparator + decimalPart;
        }
        return buffer.reverse().toString();
    }

    public String toFLV(int length, int precision) {
        return this.toFLV(length, precision, '\uffff');
    }

    public String toFLV(int length, int precision, char decimalSeparator) {
        String decimalPart = StringUtil.trimRight(this.getDecimalPart(), '0');
        if (decimalPart.length() > precision) {
            throw new InvalidNumberException("Trying to cut precision: some digits would be lost: " + this.toString());
        }
        String decimalPartFLV = StringUtil.appendChars(decimalPart, '0', precision);
        String flv = decimalSeparator != '\uffff' && !StringUtil.isEmpty(decimalPartFLV) ? this.getIntegerPart() + decimalSeparator + decimalPartFLV : this.getIntegerPart() + decimalPartFLV;
        if (flv.length() > length) {
            throw new InvalidNumberException("Trying to cut precision: some digits would be lost: " + this.toString());
        }
        if (flv.startsWith("-")) {
            return "-" + StringUtil.prependChars(flv.substring(1), '0', length - 1);
        }
        if (this._explicitSignal) {
            return "+" + StringUtil.prependChars(flv, '0', length - 1);
        }
        return StringUtil.prependChars(flv, '0', length);
    }

    public Double getDouble() {
        return new Double(this.toString('.'));
    }

    protected String getDecimalPart() {
        if (this._decimalPlaces == 0) {
            return "";
        }
        String numberWithoutDecimalSeparator = this._value < 0 ? "" + this._value * -1 : "" + this._value;
        numberWithoutDecimalSeparator = StringUtil.prependChars(numberWithoutDecimalSeparator, '0', this._decimalPlaces);
        numberWithoutDecimalSeparator = numberWithoutDecimalSeparator.substring(numberWithoutDecimalSeparator.length() - this._decimalPlaces);
        return numberWithoutDecimalSeparator;
    }

    protected String getIntegerPart() {
        String numberWithoutDecimalSeparator;
        String signal;
        if (this._value < 0) {
            numberWithoutDecimalSeparator = "" + this._value * -1;
            signal = "-";
        } else {
            numberWithoutDecimalSeparator = "" + this._value;
            signal = "";
        }
        numberWithoutDecimalSeparator = StringUtil.prependChars(numberWithoutDecimalSeparator, '0', this._decimalPlaces + 1);
        return signal + numberWithoutDecimalSeparator.substring(0, numberWithoutDecimalSeparator.length() - this._decimalPlaces);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BigNumber)) {
            return false;
        }
        BigNumber bigNumber = (BigNumber)o;
        if (this._decimalPlaces != bigNumber._decimalPlaces) {
            return false;
        }
        if (this._decimalSeparator != bigNumber._decimalSeparator) {
            return false;
        }
        if (this._value != bigNumber._value) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result = (int)(this._value ^ this._value >>> 32);
        result = 29 * result + this._decimalPlaces;
        result = 29 * result + this._decimalSeparator;
        return result;
    }
}

