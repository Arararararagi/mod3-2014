/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.math;

import java.util.ArrayList;
import java.util.List;

public class Prime {
    private Prime() {
    }

    public static boolean isPrime(long number) {
        if (number < 0) {
            throw new IllegalArgumentException("illegal negative number: " + number);
        }
        if (number == 0) {
            return false;
        }
        if (number == 1) {
            return true;
        }
        if (number == 2) {
            return true;
        }
        if (number % 2 == 0) {
            return false;
        }
        if (number == 3) {
            return true;
        }
        long max = (long)Math.ceil(Math.sqrt(number));
        for (long i = 3; i <= max; i+=2) {
            if (number % i != 0) continue;
            return false;
        }
        return true;
    }

    protected static List factors(long number) {
        ArrayList<Long> factors = new ArrayList<Long>();
        long max = (long)Math.ceil(Math.sqrt(number));
        for (long i = 2; number > 3 && i <= max; i+=2) {
            while (number % i == 0) {
                factors.add(new Long(i));
                number/=i;
            }
            if (i != 2) continue;
            --i;
        }
        if (number > 1) {
            factors.add(new Long(number));
        }
        if (number == 1 && factors.size() == 0) {
            factors.add(new Long(number));
        }
        if (number == 0) {
            factors.add(new Long(number));
        }
        return factors;
    }
}

