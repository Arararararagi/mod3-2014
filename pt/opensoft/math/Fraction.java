/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.math;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import pt.opensoft.math.Prime;

public class Fraction
extends Number
implements Comparable {
    public static final Fraction ZERO = new Fraction(0, 1);
    public static final Fraction ONE = new Fraction(1, 1);
    protected int _signal = 1;
    protected long _numerator = 0;
    protected long _nominator = 0;
    protected List _numerators = null;
    protected List _nominators = null;

    protected static List append(List a, List b) {
        if (b == null) {
            return a;
        }
        if (a == null) {
            a = new ArrayList<Long>(b.size());
        }
        for (int i = 0; i < b.size(); ++i) {
            Long prime = (Long)b.get(i);
            if (prime == 1) continue;
            a.add(prime);
        }
        return a;
    }

    protected static List copy(List list) {
        if (list == null) {
            return null;
        }
        if (list.size() == 0) {
            return new ArrayList();
        }
        ArrayList copy = new ArrayList(list.size());
        for (int i = 0; i < list.size(); ++i) {
            copy.add(list.get(i));
        }
        return copy;
    }

    protected static void reduce(List numerators, List nominators) {
        int i;
        Long prime;
        for (i = 0; i < numerators.size(); ++i) {
            prime = (Long)numerators.get(i);
            if (prime == 1) {
                numerators.remove(i);
                --i;
                continue;
            }
            if (!nominators.contains(prime)) continue;
            numerators.remove(i);
            --i;
            nominators.remove(prime);
        }
        for (i = 0; i < numerators.size(); ++i) {
            prime = (Long)numerators.get(i);
            if (prime != 1 || nominators.size() <= 1) continue;
            numerators.remove(i);
            --i;
        }
        for (i = 0; i < nominators.size(); ++i) {
            prime = (Long)nominators.get(i);
            if (prime != 1 || nominators.size() <= 1) continue;
            nominators.remove(i);
            --i;
        }
        if (numerators.size() == 0) {
            numerators.add(new Long(1));
        }
        if (nominators.size() == 0) {
            nominators.add(new Long(1));
        }
    }

    public Fraction(String fraction) {
        int pos = fraction.indexOf("/");
        if (pos == -1) {
            this.init(Long.parseLong(fraction), 1, false);
        } else {
            this.init(Long.parseLong(fraction.substring(0, pos)), Long.parseLong(fraction.substring(pos + 1)), false);
        }
    }

    public Fraction(long numerator, long nominator) {
        this.init(numerator, nominator, false);
    }

    protected void init(long numerator, long nominator, boolean reduce) {
        if (nominator == 0) {
            throw new ArithmeticException("division by 0: " + numerator + "/" + nominator);
        }
        this.setNumerator(numerator);
        this.setNominator(nominator);
        if (reduce) {
            this.reduce();
        }
    }

    protected void appendNumerators(List numerators) {
        Fraction.append(this.getNumerators(), numerators);
    }

    protected void appendNominators(List nominators) {
        Fraction.append(this.getNominators(), nominators);
    }

    public Fraction copy() {
        Fraction copy = new Fraction(this._numerator, this._nominator);
        copy._numerators = Fraction.copy(this._numerators);
        copy._nominators = Fraction.copy(this._nominators);
        return copy;
    }

    protected void setNumerator(long numerator) {
        if (this._numerator != numerator) {
            this._numerators = null;
        }
        if (numerator < 0) {
            this._signal*=-1;
        }
        this._numerator = Math.abs(numerator);
    }

    protected void setNominator(long nominator) {
        if (this._nominator != nominator) {
            this._nominators = null;
        }
        if (this._nominator < 0) {
            this._signal*=-1;
        }
        this._nominator = Math.abs(nominator);
    }

    public long getNumerator() {
        return this._numerator;
    }

    public long getNominator() {
        return this._nominator;
    }

    protected int getSignal() {
        return this._signal;
    }

    protected List getNumerators() {
        if (this._numerators == null) {
            this._numerators = Prime.factors(this._numerator);
        }
        return this._numerators;
    }

    protected List getNominators() {
        if (this._nominators == null) {
            this._nominators = Prime.factors(this._nominator);
        }
        return this._nominators;
    }

    public void factorize() {
        this.getNumerators();
        this.getNominators();
    }

    public void reduce() {
        Fraction.reduce(this.getNumerators(), this.getNominators());
        this.recalc();
    }

    protected void recalc() {
        List numerators = this.getNumerators();
        this._numerator = 1;
        for (int i = 0; i < numerators.size(); ++i) {
            this._numerator*=((Long)numerators.get(i)).longValue();
        }
        List nominators = this.getNominators();
        this._nominator = 1;
        for (int i2 = 0; i2 < nominators.size(); ++i2) {
            this._nominator*=((Long)nominators.get(i2)).longValue();
        }
    }

    public String toFactorizedString() {
        int i;
        Long prime;
        this.factorize();
        StringBuffer buffer = new StringBuffer("");
        if (this._signal < 0) {
            buffer.append("-");
        }
        if (this._numerators.size() > 1) {
            buffer.append("(");
        }
        for (i = 0; i < this._numerators.size(); ++i) {
            if (i > 0) {
                buffer.append(" * ");
            }
            prime = (Long)this._numerators.get(i);
            buffer.append(prime);
        }
        if (this._numerators.size() > 1) {
            buffer.append(")");
        }
        buffer.append(" / ");
        if (this._nominators.size() > 1) {
            buffer.append("(");
        }
        for (i = 0; i < this._nominators.size(); ++i) {
            if (i > 0) {
                buffer.append(" * ");
            }
            prime = (Long)this._nominators.get(i);
            buffer.append(prime);
        }
        if (this._nominators.size() > 1) {
            buffer.append(")");
        }
        return buffer.toString();
    }

    public Fraction add(Fraction fraction) {
        return this.add(fraction, 1);
    }

    protected Fraction add(Fraction fraction, int signal) {
        Fraction f1 = this.copy();
        Fraction f2 = fraction.copy();
        if (this.getNominator() != fraction.getNominator()) {
            f1.appendNumerators(fraction.getNominators());
            f1.appendNominators(fraction.getNominators());
            f1.recalc();
            f2.appendNumerators(this.getNominators());
            f2.recalc();
        }
        f1.setNumerator(f1.getNumerator() + (long)signal * f2.getNumerator());
        f1.reduce();
        return f1;
    }

    public Fraction subtract(Fraction fraction) {
        return this.add(fraction, -1);
    }

    public Fraction multiply(Fraction fraction) {
        Fraction result = this.copy();
        result.appendNumerators(fraction.getNumerators());
        result.appendNominators(fraction.getNominators());
        result.reduce();
        return result;
    }

    public Fraction divide(Fraction fraction) {
        Fraction result = this.copy();
        result.appendNumerators(fraction.getNominators());
        result.appendNominators(fraction.getNumerators());
        result.reduce();
        return result;
    }

    public boolean isOne() {
        return this.getNumerator() == this.getNominator();
    }

    public boolean isZero() {
        return this.getNumerator() == 0;
    }

    public boolean isPositive() {
        return this.getSignal() >= 0;
    }

    public boolean isNegative() {
        return this.getSignal() < 0;
    }

    @Override
    public byte byteValue() {
        return (byte)this.longValue();
    }

    @Override
    public double doubleValue() {
        return (double)this._signal * (double)this.getNumerator() / (double)this.getNominator();
    }

    @Override
    public float floatValue() {
        return (float)this._signal * (float)this.getNumerator() / (float)this.getNominator();
    }

    @Override
    public int intValue() {
        return (int)this.longValue();
    }

    @Override
    public long longValue() {
        return Math.round(this.doubleValue());
    }

    @Override
    public short shortValue() {
        return (short)this.longValue();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Fraction)) {
            return false;
        }
        Fraction fraction = (Fraction)obj;
        if (this.getNumerator() == 0) {
            return fraction.getNumerator() == 0;
        }
        if (this.getNominator() == fraction.getNominator()) {
            return this.getNumerator() == fraction.getNumerator();
        }
        Fraction f1 = this.copy();
        f1.reduce();
        Fraction f2 = fraction.copy();
        f2.reduce();
        return f1.getNominator() == f2.getNominator() && f1.getNumerator() == f2.getNumerator() && f1.getSignal() == f2.getSignal();
    }

    public int compareTo(Object obj) {
        Fraction fraction = this.subtract((Fraction)obj);
        double dif = fraction.doubleValue();
        if (dif > 0.0) {
            return 1;
        }
        if (dif < 0.0) {
            return -1;
        }
        return 0;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer("");
        if (this._signal < 0) {
            buffer.append("-");
        }
        buffer.append(this._numerator);
        buffer.append("/");
        buffer.append(this._nominator);
        return buffer.toString();
    }

    public int hashCode() {
        int result = this._signal;
        result = 29 * result + (int)(this._numerator ^ this._numerator >>> 32);
        result = 29 * result + (int)(this._nominator ^ this._nominator >>> 32);
        return result;
    }

    public static void main(String[] args) throws Throwable {
        Fraction result = new Fraction(args[1]);
        for (int i = 2; i < args.length; ++i) {
            Fraction fraction = new Fraction(args[i]);
            System.out.print(result + " " + args[0] + " " + fraction + " = ");
            if (args[0].equals("+")) {
                result = result.add(fraction);
            } else if (args[0].equals("-")) {
                result = result.subtract(fraction);
            } else if (args[0].equals("*")) {
                result = result.multiply(fraction);
            } else if (args[0].equals("/")) {
                result = result.divide(fraction);
            }
            System.out.println(result);
        }
        System.out.println();
        System.out.println(result + " = " + result.toFactorizedString());
    }
}

