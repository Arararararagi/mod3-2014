/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.math;

public class MathUtil {
    public static int min(int a, int b, int c) {
        int min = a;
        if (b < min) {
            min = b;
        }
        if (c < min) {
            min = c;
        }
        return min;
    }

    public static double ld(String source, String target) {
        int n = source.length();
        int m = target.length();
        if (n == 0) {
            return m;
        }
        if (m == 0) {
            return n;
        }
        int[] d = new int[m + 1];
        int j = 0;
        while (j <= m) {
            d[j] = j++;
        }
        char[] s_chars = source.toCharArray();
        char[] t_chars = target.toCharArray();
        for (int i = 1; i <= n; ++i) {
            char s_i = s_chars[i - 1];
            int old = i - 1;
            for (int j2 = 1; j2 <= m; ++j2) {
                char t_j = t_chars[j2 - 1];
                int cost = s_i == t_j ? 0 : 1;
                int dj = d[j2];
                d[j2] = MathUtil.min(d[j2] + 1, j2 == 1 ? i + 1 : d[j2 - 1] + 1, old + cost);
                old = dj;
            }
        }
        return d[m];
    }

    public static double round(double value, double factor) {
        return (double)Math.round(value * factor) / factor;
    }

    public static float round(float value, float factor) {
        return (float)Math.round(value * factor) / factor;
    }

    public static double truncate(double value, double factor) {
        return Math.floor(value * factor) / factor;
    }

    public static double toDouble(float value) {
        return new Double(Float.toString(value));
    }

    private MathUtil() {
    }
}

