/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.msg;

public class ValidateException
extends RuntimeException {
    protected String field = null;
    protected int code = -1;

    public ValidateException() {
    }

    public ValidateException(Throwable cause) {
        super(cause);
    }

    public ValidateException(String msg) {
        super(msg);
    }

    public ValidateException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ValidateException(String msg, int code) {
        super(msg);
        this.code = code;
    }

    public ValidateException(String msg, int code, Throwable cause) {
        super(msg, cause);
        this.code = code;
    }

    public ValidateException(String field, String msg) {
        super(msg);
        this.field = field;
    }

    public ValidateException(String field, String msg, Throwable cause) {
        super(msg, cause);
        this.field = field;
    }

    public ValidateException(String field, String msg, int code) {
        super(msg);
        this.field = field;
        this.code = code;
    }

    public ValidateException(String field, String msg, int code, Throwable cause) {
        super(msg, cause);
        this.field = field;
        this.code = code;
    }

    public String getField() {
        return this.field;
    }

    public int getCode() {
        return this.code;
    }
}

