/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import pt.opensoft.io.LineProcessor;

public class StopWords
implements LineProcessor {
    protected List list = null;
    protected String[] array = null;

    public StopWords(InputStream in) throws Exception {
        this.load(in);
    }

    public String add(String stopWord) {
        if (stopWord == null) {
            return null;
        }
        if ((stopWord = stopWord.trim().toLowerCase()).length() == 0) {
            return stopWord;
        }
        if (this.list == null) {
            this.list = new ArrayList();
        }
        if (this.list.contains(stopWord)) {
            return stopWord;
        }
        this.array = null;
        this.list.add(stopWord);
        return stopWord;
    }

    public String[] getStopWords() {
        if (this.array != null) {
            return this.array;
        }
        this.array = new String[this.list.size()];
        return this.list.toArray(this.array);
    }

    public String[] load(InputStream in) throws Exception {
        StopWords.read(in, this);
        return this.getStopWords();
    }

    @Override
    public String process(String line) {
        return this.add(line);
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        this.getStopWords();
        for (int i = 0; i < this.array.length; ++i) {
            String word = this.array[i];
            if (i > 0) {
                buffer.append(",");
            }
            buffer.append(word);
        }
        return buffer.toString();
    }

    public static void read(InputStream in, LineProcessor processor) throws IOException {
        try {
            InputStreamReader ireader = new InputStreamReader(in, "ISO-8859-1");
            BufferedReader reader = new BufferedReader(ireader);
            try {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    processor.process(line);
                }
            }
            finally {
                reader.close();
            }
        }
        finally {
            in.close();
        }
    }
}

