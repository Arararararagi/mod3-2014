/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.calendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pt.opensoft.util.DateTime;

public class NonWorkingDays {
    private static final List nonWorkingDays = new ArrayList(10);

    public static void add(pt.opensoft.util.Date day) {
        nonWorkingDays.add(day);
    }

    public static boolean isNonWorkingDay(DateTime date) {
        return NonWorkingDays.isNonWorkingDay(new pt.opensoft.util.Date(date));
    }

    public static boolean isNonWorkingDay(pt.opensoft.util.Date date) {
        int weekDay = date.getWeekDay();
        if (weekDay == 7 || weekDay == 1) {
            return true;
        }
        for (pt.opensoft.util.Date data : nonWorkingDays) {
            if (data.getTime() != date.getTime()) continue;
            return true;
        }
        return false;
    }

    private NonWorkingDays() {
    }

    static {
        nonWorkingDays.add(new pt.opensoft.util.Date(2003, 11, 24));
    }
}

