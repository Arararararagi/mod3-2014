/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.calendar;

import pt.opensoft.calendar.Holiday;
import pt.opensoft.util.DateTime;

public class FixedHoliday
extends Holiday {
    public static final Holiday ANO_NOVO = new FixedHoliday("Ano Novo", 0, 1);
    public static final Holiday DIA_DA_LIBERDADE = new FixedHoliday("Dia da Liberdade", 3, 25);
    public static final Holiday DIA_TRABALHADOR = new FixedHoliday("Dia do Trabalhador", 4, 1);
    public static final Holiday DIA_PORTUGAL = new FixedHoliday("Dia de Portugal", 5, 10);
    public static final Holiday ASSUMPCAO = new FixedHoliday("Assun\u00e7\u00e3o da Virgem Santa Maria", 7, 15);
    public static final Holiday INSTITUICAO_REPUBLICA = new FixedHoliday("Institui\u00e7\u00e3o da Rep\u00fablica", 9, 5);
    public static final Holiday TODOS_SANTOS = new FixedHoliday("Dia de Todos os Santos", 10, 1);
    public static final Holiday INDEPENDENCIA = new FixedHoliday("Restaura\u00e7\u00e3o da Independ\u00eancia", 11, 1);
    public static final Holiday IMACULADA_CONCEICAO = new FixedHoliday("Concep\u00e7\u00e3o Imaculada", 11, 8);
    public static final Holiday NATAL = new FixedHoliday("Natal", 11, 25);

    public FixedHoliday(String name, int month, int day) {
        super("PT", name, month, day);
    }

    @Override
    public boolean contains(String holidayRegion, DateTime date) {
        if (!("PT".equals(this.region) || this.region.equals(holidayRegion))) {
            return false;
        }
        if (date.getDay() != this.day) {
            return false;
        }
        if (date.getMonth() != this.month) {
            return false;
        }
        if (date.getYear() >= 2013 && date.getYear() < 2018) {
            if (this.day == INSTITUICAO_REPUBLICA.getDay() && this.month == INSTITUICAO_REPUBLICA.getMonth()) {
                return false;
            }
            if (this.day == TODOS_SANTOS.getDay() && this.month == TODOS_SANTOS.getMonth()) {
                return false;
            }
            if (this.day == INDEPENDENCIA.getDay() && this.month == INDEPENDENCIA.getMonth()) {
                return false;
            }
        }
        return true;
    }
}

