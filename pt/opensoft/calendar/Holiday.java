/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.calendar;

import pt.opensoft.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.Month;
import pt.opensoft.util.NamedObject;
import pt.opensoft.util.StringUtil;

public abstract class Holiday
extends NamedObject {
    public static final String PORTUGAL = "PT";
    protected String region;
    protected int month;
    protected int day;

    public Holiday(String region, String name, int month, int day) {
        super(name, true);
        this.region = region;
        this.setMonth(month);
        this.setDay(day);
    }

    public abstract boolean contains(String var1, DateTime var2);

    public Date getDate(int year) {
        return new Date(year, this.month, this.day);
    }

    public int getDay() {
        return this.day;
    }

    public String getID() {
        return StringUtil.padZeros(this.month + 1, 2) + StringUtil.padZeros(this.day, 2);
    }

    public int getMonth() {
        return this.month;
    }

    public String getRegion() {
        return this.region;
    }

    protected void setMonth(int month) {
        if (month < 0) {
            throw new IllegalArgumentException("month cannot be < 0:" + month);
        }
        if (month > 11) {
            throw new IllegalArgumentException("month cannot be > 11:" + month);
        }
        this.month = month;
    }

    protected void setDay(int day) {
        if (day < 1) {
            throw new IllegalArgumentException("day cannot be < 1");
        }
        if (this.month != 1 && day > Month.getDays(this.month)) {
            throw new IllegalArgumentException("day cannot be > " + Month.getDays(this.month) + " for " + Month.getName(this.month));
        }
        if (this.month == 1 && day > 29) {
            throw new IllegalArgumentException("day cannot be > 29 for " + Month.getName(1));
        }
        this.day = day;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(this.region).append(" - ");
        buffer.append(this.getName()).append(" = ");
        buffer.append(this.getID());
        return buffer.toString();
    }
}

