/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.calendar;

import pt.opensoft.calendar.Holiday;
import pt.opensoft.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.Month;
import pt.opensoft.util.Year;

public class MoveableHoliday
extends Holiday {
    public static final Holiday CARNAVAL = new MoveableHoliday("Carnaval", 1, 3);
    public static final Holiday SEXTA_SANTA = new MoveableHoliday("Sexta Feira Santa", 2, 20);
    public static final Holiday PASCOA = new MoveableHoliday("P\u00e1scoa", 2, 22);
    public static final Holiday CORPO_DEUS = new MoveableHoliday("Corpo de Deus", 4, 21);
    private static final int MIN_YEAR = 1582;
    private static final int MAX_YEAR = 2099;

    private static int calculateShift(int year) {
        int m;
        if (year < 1582) {
            throw new IllegalArgumentException("year cannot be < 1582");
        }
        if (year > 2099) {
            throw new IllegalArgumentException("year cannot be > 2099");
        }
        int n = year >= 1900 ? 24 : (m = year >= 1700 ? 23 : 22);
        int n2 = year >= 1900 ? 5 : (year >= 1800 ? 4 : 3);
        int a = year % 19;
        int b = year % 4;
        int c = year % 7;
        int d = (19 * a + m) % 30;
        int e = (2 * b + 4 * c + 6 * d + n2) % 7;
        return d + e;
    }

    public MoveableHoliday(String name, int month, int day) {
        super("PT", name, month, day);
    }

    @Override
    public boolean contains(String region, DateTime date) {
        int monthDays;
        int month;
        if (!("PT".equals(this.region) || this.region.equals(region))) {
            return false;
        }
        int _month = date.getMonth();
        if (_month < 1 || _month > 5) {
            return false;
        }
        int year = date.getYear();
        int day = this.day + MoveableHoliday.calculateShift(year);
        if (day > (monthDays = Month.getDays(year, month = this.month))) {
            day-=monthDays;
            ++month;
        } else if (month == 1 && Year.isLeapYear(year)) {
            ++day;
        }
        if (date.getDay() != day) {
            return false;
        }
        if (date.getMonth() != month) {
            return false;
        }
        if (date.getYear() >= 2013 && date.getYear() < 2018) {
            if (date.getMonth() == month && date.getDay() == day && this.getName().equals(CARNAVAL.getName())) {
                return false;
            }
            if (date.getMonth() == month && date.getDay() == day && this.getName().equals(CORPO_DEUS.getName())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Date getDate(int year) {
        int monthDays;
        int month;
        int day = this.day + MoveableHoliday.calculateShift(year);
        if (day > (monthDays = Month.getDays(year, month = this.month))) {
            day-=monthDays;
            ++month;
        }
        return new Date(year, month, day);
    }
}

