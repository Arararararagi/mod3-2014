/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.calendar;

import pt.opensoft.calendar.Holiday;
import pt.opensoft.util.DateTime;

public class RegionalHoliday
extends Holiday {
    public static final Holiday ABRANTES = new RegionalHoliday("1401", "Abrantes", 5, 14);
    public static final Holiday AGUEDA = new RegionalHoliday("0101", "\u00c1gueda", 5, 4);
    public static final Holiday ALBUFEIRA = new RegionalHoliday("0801", "Albufeira", 7, 20);
    public static final Holiday ALCACER_DO_SAL = new RegionalHoliday("1501", "\u00c1lcacer do Sal", 5, 24);
    public static final Holiday ALCOBACA = new RegionalHoliday("1001", "Alcoba\u00e7a", 7, 20);
    public static final Holiday ALJUSTREL = new RegionalHoliday("0201", "Aljustrel", 5, 13);
    public static final Holiday ALMADA = new RegionalHoliday("1503", "Almada", 5, 24);
    public static final Holiday AMADORA = new RegionalHoliday("1115", "Amadora", 8, 11);
    public static final Holiday AMARANTE = new RegionalHoliday("1301", "Amarante", 6, 8);
    public static final Holiday ANGRA_DO_HEROISMO = new RegionalHoliday("1901", "Angra do Hero\u00edsmo", 5, 24);
    public static final Holiday ARGANIL = new RegionalHoliday("0601", "Arganil", 8, 7);
    public static final Holiday AVEIRO = new RegionalHoliday("0105", "Aveiro", 4, 12);
    public static final Holiday BAIAO = new RegionalHoliday("1302", "Bai\u00e3o", 7, 24);
    public static final Holiday BARCELOS = new RegionalHoliday("0302", "Barcelos", 4, 3);
    public static final Holiday BARREIRO = new RegionalHoliday("1504", "Barreiro", 5, 28);
    public static final Holiday BEJA = new RegionalHoliday("0205", "Beja", 4, 27);
    public static final Holiday BOMBARRAL = new RegionalHoliday("1005", "Bombarral", 5, 29);
    public static final Holiday BRAGA = new RegionalHoliday("0303", "Braga", 5, 24);
    public static final Holiday BRAGANCA = new RegionalHoliday("0402", "Bragan\u00e7a", 7, 22);
    public static final Holiday CABECEIRAS_DE_BASTO = new RegionalHoliday("0304", "Cabeceiras de Basto", 8, 29);
    public static final Holiday CALDAS_DA_RAINHA = new RegionalHoliday("1006", "Caldas da Rainha", 4, 15);
    public static final Holiday CANTANHEDE = new RegionalHoliday("0602", "Cantanhede", 6, 25);
    public static final Holiday CASCAIS = new RegionalHoliday("1105", "Cascais", 5, 13);
    public static final Holiday CASTELO_BRANCO = new RegionalHoliday("0502", "Castelo Branco", 3, 17);
    public static final Holiday CELORICO_DA_BEIRA = new RegionalHoliday("0903", "Celorico da Beira", 4, 23);
    public static final Holiday CHAVES = new RegionalHoliday("1703", "Chaves", 6, 8);
    public static final Holiday COIMBRA = new RegionalHoliday("0603", "Coimbra", 6, 4);
    public static final Holiday CORUCHE = new RegionalHoliday("1409", "Coruche", 7, 17);
    public static final Holiday COVILHA = new RegionalHoliday("0503", "Covilh\u00e3", 9, 20);
    public static final Holiday ELVAS = new RegionalHoliday("1207", "Elvas", 0, 14);
    public static final Holiday ENTRONCAMENTO = new RegionalHoliday("1410", "Entroncamento", 10, 24);
    public static final Holiday ESPINHO = new RegionalHoliday("0107", "Espinho", 5, 16);
    public static final Holiday ESPOSENDE = new RegionalHoliday("0306", "Esposende", 7, 19);
    public static final Holiday ESTREMOZ = new RegionalHoliday("0704", "Estremoz", 4, 27);
    public static final Holiday EVORA = new RegionalHoliday("0705", "\u00c9vora", 5, 29);
    public static final Holiday FAFE = new RegionalHoliday("0307", "Fafe", 4, 16);
    public static final Holiday FARO = new RegionalHoliday("0805", "Faro", 8, 7);
    public static final Holiday FERREIRA_DO_ALENTEJO = new RegionalHoliday("0208", "Ferreira do Alentejo", 2, 5);
    public static final Holiday FIGUEIRA_DA_FOZ = new RegionalHoliday("0605", "Figueira da Foz", 5, 24);
    public static final Holiday FUNCHAL = new RegionalHoliday("2203", "Funchal", 7, 21);
    public static final Holiday FUNDAO = new RegionalHoliday("0504", "Fund\u00e3o", 8, 15);
    public static final Holiday GOLEGA = new RegionalHoliday("1412", "Goleg\u00e3", 4, 27);
    public static final Holiday GONDOMAR = new RegionalHoliday("1304", "Gondomar", 9, 8);
    public static final Holiday GRANDOLA = new RegionalHoliday("1505", "Gr\u00e2ndola", 9, 22);
    public static final Holiday GUARDA = new RegionalHoliday("0907", "Guarda", 10, 27);
    public static final Holiday GUIMARAES = new RegionalHoliday("0308", "Guimar\u00e3es", 5, 24);
    public static final Holiday IDANHA_A_NOVA = new RegionalHoliday("0505", "Idanha-a-Nova", 3, 30);
    public static final Holiday ILHAVO = new RegionalHoliday("0110", "\u00cdlhavo", 3, 16);
    public static final Holiday LAGOS = new RegionalHoliday("0807", "Lagos", 9, 27);
    public static final Holiday LAMEGO = new RegionalHoliday("1805", "Lamego", 8, 8);
    public static final Holiday LEIRIA = new RegionalHoliday("1009", "Leiria", 4, 22);
    public static final Holiday LISBOA = new RegionalHoliday("1106", "Lisboa", 5, 13);
    public static final Holiday LOULE = new RegionalHoliday("0808", "Loul\u00e9", 4, 27);
    public static final Holiday LOURES = new RegionalHoliday("1107", "Loures", 6, 26);
    public static final Holiday LOURINHA = new RegionalHoliday("1108", "Lourinh\u00e3", 5, 24);
    public static final Holiday MAIA = new RegionalHoliday("1306", "Maia", 6, 9);
    public static final Holiday MANGUALDE = new RegionalHoliday("1806", "Mangualde", 8, 18);
    public static final Holiday MARINHA_GRANDE = new RegionalHoliday("1010", "Marinha Grande", 4, 27);
    public static final Holiday MATOSINHOS = new RegionalHoliday("1308", "Matosinhos", 5, 5);
    public static final Holiday MEALHADA = new RegionalHoliday("0111", "Mealhada", 4, 27);
    public static final Holiday MERTOLA = new RegionalHoliday("0209", "M\u00e9rtola", 5, 24);
    public static final Holiday MIRANDA_DO_DOURO = new RegionalHoliday("0609", "Miranda do Douro", 6, 10);
    public static final Holiday MIRANDELA = new RegionalHoliday("0406", "Mirandela", 4, 25);
    public static final Holiday MOITA = new RegionalHoliday("1506", "Moita", 8, 11);
    public static final Holiday MONTIJO = new RegionalHoliday("1507", "Montijo", 5, 29);
    public static final Holiday MOURA = new RegionalHoliday("0210", "Moura", 5, 24);
    public static final Holiday NAZARE = new RegionalHoliday("1011", "Nazar\u00e9", 8, 8);
    public static final Holiday OBIDOS = new RegionalHoliday("1012", "\u00d3bidos", 0, 11);
    public static final Holiday ODEMIRA = new RegionalHoliday("0211", "Odemira", 8, 8);
    public static final Holiday OEIRAS = new RegionalHoliday("1110", "Oeiras", 5, 7);
    public static final Holiday OLHAO = new RegionalHoliday("0810", "Olh\u00e3o", 5, 16);
    public static final Holiday OLIVEIRA_DO_HOSPITAL = new RegionalHoliday("0611", "Oliveira do Hospital", 9, 7);
    public static final Holiday OURIQUE = new RegionalHoliday("0212", "Ourique", 8, 8);
    public static final Holiday OVAR = new RegionalHoliday("0115", "Ovar", 6, 25);
    public static final Holiday PACOS_DE_FERREIRA = new RegionalHoliday("1309", "Pa\u00e7os de Ferreira", 10, 6);
    public static final Holiday PALMELA = new RegionalHoliday("1508", "Palmela", 5, 1);
    public static final Holiday PENAFIEL = new RegionalHoliday("1311", "Penafiel", 10, 11);
    public static final Holiday PENAMACOR = new RegionalHoliday("0507", "Penamacor", 3, 16);
    public static final Holiday PENICHE = new RegionalHoliday("1014", "Peniche", 7, 6);
    public static final Holiday POMBAL = new RegionalHoliday("1015", "Pombal", 10, 11);
    public static final Holiday PONTA_DELGADA = new RegionalHoliday("2103", "Ponta Delgada", 3, 16);
    public static final Holiday PONTE_DE_LIMA = new RegionalHoliday("1607", "Ponte de Lima", 8, 20);
    public static final Holiday PORTALEGRE = new RegionalHoliday("1214", "Portalegre", 4, 23);
    public static final Holiday PORTIMAO = new RegionalHoliday("0811", "Portim\u00e3o", 11, 11);
    public static final Holiday PORTO = new RegionalHoliday("1312", "Porto", 5, 24);
    public static final Holiday POVOA_DO_VARZIM = new RegionalHoliday("1313", "P\u00f3voa do Varzim", 5, 29);
    public static final Holiday PROENCA_A_NOVA = new RegionalHoliday("0508", "Proen\u00e7a-A-Nova", 5, 13);
    public static final Holiday REGUENGOS_DE_MONSARAZ = new RegionalHoliday("0711", "Reguengos de Monsaraz", 5, 13);
    public static final Holiday RIO_MAIOR = new RegionalHoliday("1414", "Rio Maior", 10, 6);
    public static final Holiday SANTAREM = new RegionalHoliday("1416", "Santar\u00e9m", 2, 19);
    public static final Holiday SEIA = new RegionalHoliday("0912", "Seia", 6, 3);
    public static final Holiday SEIXAL = new RegionalHoliday("1510", "Seixal", 5, 29);
    public static final Holiday SESIMBRA = new RegionalHoliday("1511", "Sesimbra", 4, 4);
    public static final Holiday SETUBAL = new RegionalHoliday("1512", "Set\u00fabal", 8, 15);
    public static final Holiday SILVES = new RegionalHoliday("0813", "Silves", 8, 3);
    public static final Holiday SINES = new RegionalHoliday("1513", "Sines", 10, 24);
    public static final Holiday SINTRA = new RegionalHoliday("1111", "Sintra", 5, 29);
    public static final Holiday S_JOAO_DA_MADEIRA = new RegionalHoliday("0116", "S. Jo\u00e3o da Madeira", 9, 11);
    public static final Holiday STO_TIRSO = new RegionalHoliday("1314", "Sto. Tirso", 6, 11);
    public static final Holiday TAVIRA = new RegionalHoliday("0814", "Tavira", 5, 24);
    public static final Holiday TOMAR = new RegionalHoliday("1418", "Tomar", 2, 1);
    public static final Holiday TORRES_NOVAS = new RegionalHoliday("1419", "Torres Novas", 4, 27);
    public static final Holiday VALONGO = new RegionalHoliday("1315", "Valongo", 5, 24);
    public static final Holiday VILA_DO_CONDE = new RegionalHoliday("1316", "Vila do Conde", 5, 24);
    public static final Holiday VILA_FRANCA_DE_XIRA = new RegionalHoliday("1114", "Vila Franca de Xira", 4, 27);
    public static final Holiday VILA_NOVA_DE_FAMALICAO = new RegionalHoliday("0312", "Vila Nova de Famalic\u00e3o", 5, 13);
    public static final Holiday VILA_NOVA_DE_GAIA = new RegionalHoliday("1317", "Vila Nova de Gaia", 5, 24);
    public static final Holiday VILA_REAL = new RegionalHoliday("1714", "Vila Real", 5, 13);
    public static final Holiday VISEU = new RegionalHoliday("1823", "Viseu", 8, 21);

    public RegionalHoliday(String region, String name, int month, int day) {
        super(region, name, month, day);
    }

    @Override
    public boolean contains(String holidayRegion, DateTime date) {
        if ("PT".equals(holidayRegion)) {
            return false;
        }
        if (!this.region.equals(holidayRegion)) {
            return false;
        }
        if (date.getDay() != this.day) {
            return false;
        }
        if (date.getMonth() != this.month) {
            return false;
        }
        return true;
    }
}

