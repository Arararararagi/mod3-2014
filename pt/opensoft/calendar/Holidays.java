/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.calendar;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import pt.opensoft.calendar.FixedHoliday;
import pt.opensoft.calendar.Holiday;
import pt.opensoft.calendar.MoveableHoliday;
import pt.opensoft.calendar.RegionalHoliday;
import pt.opensoft.util.DateTime;

public class Holidays {
    private static final List holidays = new ArrayList(128);
    private static final Map fixedHolidays = new HashMap(128);
    private static final List moveableHolidays = new ArrayList(4);

    public static void excludeCarnival() {
        Iterator iterator = moveableHolidays.iterator();
        while (iterator.hasNext()) {
            MoveableHoliday holiday = (MoveableHoliday)iterator.next();
            if (!holiday.getName().equals(MoveableHoliday.CARNAVAL.getName())) continue;
            iterator.remove();
            holidays.remove(holiday);
            break;
        }
    }

    public static boolean isHoliday(String region, DateTime date) {
        return Holidays.getHolidays(region, date) != null;
    }

    public static final void add(Class clazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            if (!Modifier.isStatic(fields[i].getModifiers())) continue;
            if (Modifier.isPrivate(fields[i].getModifiers())) continue;
            Object field = null;
            try {
                field = fields[i].get(null);
            }
            catch (IllegalArgumentException e) {
                System.out.println("[Holidays] IllegalArgumentException: " + fields[i]);
                continue;
            }
            catch (IllegalAccessException e) {
                System.out.println("[Holidays] IllegalAccessException: " + fields[i]);
                continue;
            }
            if (!(field instanceof Holiday)) continue;
            Holidays.add((Holiday)field);
        }
    }

    public static final void add(Holiday holiday) {
        if (holiday instanceof MoveableHoliday) {
            moveableHolidays.add(holiday);
        } else {
            String id = holiday.getID();
            ArrayList<Holiday> list = (ArrayList<Holiday>)fixedHolidays.get(id);
            if (list == null) {
                list = new ArrayList<Holiday>(2);
                fixedHolidays.put(id, list);
            }
            list.add(holiday);
        }
        holidays.add(holiday);
    }

    public static Holiday getHoliday(String region, DateTime date) {
        List list = Holidays.getHolidays(region, date);
        if (list == null) {
            return null;
        }
        return (Holiday)list.get(0);
    }

    public static List getHolidays(String region, DateTime date) {
        int month = date.getMonth() + 1;
        int day = date.getDay();
        StringBuffer id = new StringBuffer(4);
        if (month < 10) {
            id.append("0");
        }
        id.append(month);
        if (day < 10) {
            id.append("0");
        }
        id.append(day);
        List fixed = (List)fixedHolidays.get(id.toString());
        ArrayList<Holiday> result = new ArrayList<Holiday>(2);
        if (fixed != null) {
            for (Holiday holiday : fixed) {
                if (!holiday.contains(region, date)) continue;
                result.add(holiday);
            }
        }
        for (Holiday holiday : moveableHolidays) {
            if (!holiday.contains(region, date)) continue;
            result.add(holiday);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result;
    }

    public static final List getHolidays() {
        return holidays;
    }

    private Holidays() {
    }

    static {
        Holidays.add(FixedHoliday.class);
        Holidays.add(MoveableHoliday.class);
        Holidays.add(RegionalHoliday.class);
    }
}

