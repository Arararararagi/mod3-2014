/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.pdf;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import pt.opensoft.field.EuroField;
import pt.opensoft.pdf.PDFHeaderFooterEvent;

public class DynamicPDFRenderer {
    private static final int FONT_TYPE = 1;
    public static final int ALIGN_UNDEFINED = -1;
    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_CENTER = 1;
    public static final int ALIGN_RIGHT = 2;
    public static final int ALIGN_JUSTIFIED = 3;
    public static final int ALIGN_TOP = 4;
    public static final int ALIGN_MIDDLE = 5;
    public static final int ALIGN_BOTTOM = 6;
    public static final int ALIGN_BASELINE = 7;
    public static final int UNDEFINED = -1;
    public static final int BORDER_NONE = 0;
    public static final int BORDER_TOP = 1;
    public static final int BORDER_BOTTOM = 2;
    public static final int BORDER_LEFT = 4;
    public static final int BORDER_RIGHT = 8;
    public static final int BORDER_BOX = 15;
    public static Font normalText = new Font(1, 9.0f);
    public static Font normalText8 = new Font(1, 8.0f);
    public static Font normalText7 = new Font(1, 7.0f);
    public static Font normalText6 = new Font(1, 6.0f);
    public static Font upperText = new Font(1, 6.0f);
    public static Font normalMonoSpaceText = new Font(0, 9.0f);
    public static Font boldText9 = new Font(1, 9.0f, 1);
    public static Font boldText8 = new Font(1, 8.0f, 1);
    public static Font boldText6 = new Font(1, 6.0f, 1);
    public static Font boldText5 = new Font(1, 5.0f, 1);
    public static Font alertText = new Font(1, 16.0f, 1, Color.red);
    public static Font tableColumnNameText = new Font(1, 9.0f, 1);
    protected static final float[] DEFAULT_MARGINS_SIZE = new float[]{10.0f, 10.0f, 10.0f, 10.0f};
    public static final int ALLOW_ALL = 1234567;
    public static final int ALLOW_READ = 16;
    public static final int ALLOW_PRINT = 2052;
    public static final int ALLOW_WRITE = 1320;
    public static final String EURO_SYMBOL = "\u20ac";
    public static final String SQUARE_METER = "squareMeter";
    public static final String EURO = "euro";
    public static final String INVISIBLE_CHAR = "\u2062";
    private EuroField euroFormat = new EuroField("euroFormat");
    protected boolean _splitCharacter;
    protected OutputStream _stream = null;
    protected PdfWriter _writer = null;
    protected Document _document = null;
    protected Phrase _phrase;
    protected Cell _cell;
    protected Table _table;

    public DynamicPDFRenderer(OutputStream stream) throws FileNotFoundException, DocumentException {
        this(stream, null, 1234567);
    }

    public DynamicPDFRenderer(OutputStream stream, String password, int permissions) throws FileNotFoundException, DocumentException {
        this._stream = stream;
        this.createDocument();
        this.initDocument(password, permissions);
    }

    public void openDocument() {
        this._document.open();
    }

    public void openDocument(Element header) {
        this.openDocument(header, false);
    }

    public void openDocument(Element header, boolean hasPageNumber) {
        this.setHeaderFooter(header, hasPageNumber);
        this.openDocument();
    }

    public void openDocument(Chunk header, int headerAlign, Chunk footer, int footerAlign) {
        this.setHeader(header, headerAlign);
        this.setFooter(footer, footerAlign);
        this.openDocument();
    }

    protected void initDocument(String password, int permissions) throws DocumentException {
        this._writer = PdfWriter.getInstance(this._document, this._stream);
        if (permissions != 1234567) {
            this._writer.setEncryption(false, null, password, permissions);
        }
        this._writer.setViewerPreferences(2048);
    }

    protected void createDocument() {
        float[] margins = DEFAULT_MARGINS_SIZE;
        this._document = new Document(PageSize.A4, margins[0], margins[1], margins[2], margins[3]);
    }

    public Document closeDocument() throws IOException {
        if (this._document != null) {
            Document.compress = false;
            this._document.close();
        }
        return this._document;
    }

    protected void setMargins(float leftMargin, float rightMargin, float topMargin, float bottomMargin) {
        this._document.setMargins(leftMargin, rightMargin, topMargin, bottomMargin);
    }

    public void setHeaderFooter(Element header, boolean hasPageNumber) {
        if (header != null) {
            this._writer.setPageEvent(new PDFHeaderFooterEvent(header, hasPageNumber));
        }
    }

    public void showPageNumber() {
        this._writer.setPageEvent(new PDFHeaderFooterEvent(true));
    }

    public void setHeader(Chunk header, int alignment) {
        if (header != null) {
            HeaderFooter _header = new HeaderFooter(new Phrase(header), false);
            _header.setAlignment(alignment);
            this._document.setHeader(_header);
        }
    }

    public void setFooter(Chunk footer, int alignment) {
        if (footer != null) {
            HeaderFooter _footer = new HeaderFooter(new Phrase(footer), false);
            _footer.setAlignment(alignment);
            this._document.setFooter(_footer);
        }
    }

    public void sectionHeader(Chunk chunk, Color color) throws DocumentException {
        this.sectionHeader(chunk, 1, 5.0f, color);
    }

    public void sectionHeader(Chunk chunk) throws DocumentException {
        this.sectionHeader(chunk, 1);
    }

    public void sectionHeader(Chunk chunk, int colSpan) throws DocumentException {
        this.sectionHeader(chunk, colSpan, 5.0f);
    }

    public void sectionHeader(Chunk chunk, int colSpan, float offset, Color color) throws DocumentException {
        Cell c = new Cell(chunk);
        c.setBorderWidth(1.0f);
        this.cell(c, colSpan, 1, 0, 4, 15, color);
    }

    public void sectionHeader(Chunk chunk, int colSpan, float offset) throws DocumentException {
        this._table.setOffset(offset);
        Cell c = new Cell(chunk);
        c.setBorderWidth(1.0f);
        this.cell(c, colSpan, 1, 0, 4, 15);
    }

    public void sectionHeader(Chunk chunk, int colSpan, float offset, float spacing) throws DocumentException {
        this._table.setOffset(offset);
        this._table.setSpacing(spacing);
        Cell c = new Cell(chunk);
        c.setBorderWidth(1.0f);
        c.setUseAscender(true);
        c.setUseDescender(true);
        this.cell(c, colSpan, 1, 0, 6, 15, true);
        this._table.setAlignment(1);
    }

    public Chunk newLine() {
        this.addChunk(Chunk.NEWLINE);
        return Chunk.NEWLINE;
    }

    public Chunk field(String text) {
        return this.field(text, normalText);
    }

    public Chunk field(String text, Font font) {
        Chunk chunk = new Chunk(text, font);
        this.addChunk(chunk);
        return chunk;
    }

    public void field(String name, long value) {
        this.field(name, String.valueOf(value));
    }

    public void field(String name, String value) {
        this.field(name, value, null);
    }

    public void field(byte[] image) throws IOException, BadElementException {
        this._cell.addElement(com.lowagie.text.Image.getInstance(image));
    }

    public void field(com.lowagie.text.Image image) throws BadElementException {
        this._cell.addElement(image);
    }

    public void field(Image image) throws IOException, BadElementException {
        this.field(com.lowagie.text.Image.getInstance(image, null));
    }

    public void euroField(long value) throws Exception {
        this.euroField(String.valueOf(value), boldText9);
    }

    public void euroField(String name, long value) throws Exception {
        this.euroFormat.setValue(value);
        this.field(name, this.euroFormat.toString(), "euro");
    }

    public void euroField(String value) throws Exception {
        this.euroField(value, normalText);
    }

    public void euroField(String value, Font font) throws Exception {
        this.euroSymbol(font);
        this.euroFormat.setValue(value);
        this.addChunk(new Chunk(this.euroFormat.toString(), font));
    }

    public void field(String name, String value, String metric) {
        this.addChunk(new Chunk(name + ": " + "\u2062", boldText9));
        if ("euro".equals(metric)) {
            Chunk chunkValue = new Chunk("\u20ac\u2062" + value + "\u2062" + " ", normalText);
            this.addChunk(chunkValue);
        } else if ("squareMeter".equals(metric)) {
            this.addChunk(new Chunk(value + "\u2062", normalText));
            this.addChunk(new Chunk("m", normalText));
            Chunk square = new Chunk("2", normalText6);
            square.setTextRise(5.0f);
            this.addChunk(square);
            this.addChunk(new Chunk("\u2062 ", normalText));
        } else {
            this.addChunk(new Chunk(value + "\u2062" + " ", normalText));
        }
    }

    public void setLineLeading(float leading) {
        this._phrase.setLeading(leading);
    }

    public void setCellSpacing(float spacing) {
        this._table.setSpacing(spacing);
    }

    public void euroSymbol(Font font) {
        Chunk meter = new Chunk("\u20ac", font);
        Chunk space = new Chunk(" ", font);
        this.addChunk(meter);
        this.addChunk(space);
    }

    protected void addChunk(Chunk c) {
        this._phrase.add(c);
    }

    public void add(Element element) throws DocumentException {
        this._document.add(element);
    }

    public void addImage(byte[] image) throws IOException, DocumentException {
        this.addImage(com.lowagie.text.Image.getInstance(image), 0);
    }

    public void addImage(byte[] image, int alignment) throws IOException, DocumentException {
        this.addImage(com.lowagie.text.Image.getInstance(image), alignment);
    }

    public void addImage(com.lowagie.text.Image image) throws DocumentException {
        this.addImage(image, 0);
    }

    public void addImage(com.lowagie.text.Image image, int alignment) throws DocumentException {
        image.setAlignment(alignment);
        this._document.add(image);
    }

    public void addImage(Image image) throws IOException, DocumentException {
        this.addImage(com.lowagie.text.Image.getInstance(image, null), 0);
    }

    public void addImage(Image image, int alignment) throws IOException, DocumentException {
        this.addImage(com.lowagie.text.Image.getInstance(image, null), alignment);
    }

    public void addParagraph(String text) throws DocumentException {
        this.addParagraph(text, new Font(2), 0, 0.0f, 0.0f);
    }

    public void addParagraph(String text, Font font) throws DocumentException {
        this.addParagraph(text, font, 0, 0.0f, 0.0f);
    }

    public void addParagraph(String text, Font font, int alignment) throws DocumentException {
        this.addParagraph(text, font, alignment, 0.0f, 0.0f);
    }

    public void addParagraph(String text, Font font, float identationLeft, float identationRight) throws DocumentException {
        this.addParagraph(text, font, 0, identationLeft, identationRight);
    }

    public void addParagraph(String text, Font font, int alignment, float identationLeft, float identationRight) throws DocumentException {
        Paragraph p = new Paragraph(text, font);
        p.setAlignment(alignment);
        p.setIndentationLeft(identationLeft);
        p.setIndentationRight(identationRight);
        this._document.add(p);
    }

    public void cell(Cell cell) {
        this.cell(cell, 1, 1, 0, 4, 0);
    }

    public void cell(Cell cell, int colSpan, int rowSpan, int horizAlign, int vertAlign, int border, Color color) {
        cell.setColspan(colSpan);
        cell.setRowspan(rowSpan);
        cell.setBorder(border);
        cell.setHorizontalAlignment(horizAlign);
        cell.setVerticalAlignment(vertAlign);
        cell.setBackgroundColor(color);
        cell.setUseAscender(false);
        cell.setUseDescender(false);
        this._table.addCell(cell);
    }

    public void cell(Cell cell, int colSpan, int rowSpan, int horizAlign, int vertAlign, int border) {
        cell.setColspan(colSpan);
        cell.setRowspan(rowSpan);
        cell.setBorder(border);
        cell.setHorizontalAlignment(horizAlign);
        cell.setVerticalAlignment(vertAlign);
        cell.setUseAscender(false);
        cell.setUseDescender(false);
        this._table.addCell(cell);
    }

    public void cell(Cell cell, int colSpan, int rowSpan, int horizAlign, int vertAlign, int border, boolean descAsc) {
        cell.setColspan(colSpan);
        cell.setRowspan(rowSpan);
        cell.setBorder(border);
        cell.setHorizontalAlignment(horizAlign);
        cell.setVerticalAlignment(vertAlign);
        cell.setUseAscender(descAsc);
        cell.setUseDescender(descAsc);
        this._table.addCell(cell);
    }

    public void startCell() throws BadElementException {
        this.startCell(0);
    }

    public void startCell(int border) throws BadElementException {
        this.startCell(1, border);
    }

    public void startCell(int colSpan, int border) throws BadElementException {
        this.startCell(colSpan, 1, border);
    }

    public void startCell(int colSpan, int rowSpan, int border) throws BadElementException {
        this.startCell(colSpan, rowSpan, 0, 4, border);
    }

    public void startCell(int colSpan, int rowSpan, int horizAlign, int vertAlign, int border) {
        this._cell = new Cell();
        this._phrase = new Phrase();
        this._cell.setColspan(colSpan);
        this._cell.setRowspan(rowSpan);
        this._cell.setBorder(border);
        this._cell.setHorizontalAlignment(horizAlign);
        this._cell.setVerticalAlignment(vertAlign);
    }

    public void startCell(int colSpan, int rowSpan, int horizAlign, int vertAlign, int border, boolean nowrap) {
        this._cell = new Cell();
        this._phrase = new Phrase();
        this._cell.setUseAscender(nowrap);
        this._cell.setUseDescender(false);
        this._cell.setColspan(colSpan);
        this._cell.setRowspan(rowSpan);
        this._cell.setBorder(border);
        this._cell.setHorizontalAlignment(horizAlign);
        this._cell.setVerticalAlignment(vertAlign);
    }

    public void startCell(int colSpan, int rowSpan, int horizAlign, int vertAlign, int border, boolean noDescenderAscender, float leading) {
        this._cell = new Cell();
        this._phrase = new Phrase();
        this._cell.setUseAscender(false);
        this._cell.setUseDescender(false);
        this._cell.setColspan(colSpan);
        this._cell.setRowspan(rowSpan);
        this._cell.setBorder(border);
        this._cell.setHorizontalAlignment(horizAlign);
        this._cell.setVerticalAlignment(vertAlign);
        this._cell.setLeading(leading);
    }

    public float getOffset() {
        if (this._table != null) {
            return this._table.getOffset();
        }
        return 0.0f;
    }

    public void setCellLeading(float leading) {
        this._cell.setLeading(leading);
    }

    public void setCellBGColor(Color color) {
        this._cell.setBackgroundColor(color);
    }

    public void emptyCell() throws BadElementException {
        this.cell(new Cell(true));
    }

    public void emptyCell(int border) throws BadElementException {
        this.cell(new Cell(true), 1, 1, 1, 5, border);
    }

    public void emptyCell(int colspan, int border) throws BadElementException {
        this.cell(new Cell(true), colspan, 1, 1, 5, border);
    }

    public void endCell() {
        if (this._phrase != null) {
            this._cell.add(this._phrase);
        } else {
            this._cell.add(new Paragraph(new Phrase()));
        }
        this._table.addCell(this._cell);
    }

    public Cell resetCell() {
        if (this._phrase != null) {
            this._cell.add(this._phrase);
        } else {
            this._cell.add(new Phrase());
        }
        return this._cell;
    }

    public void startTable() throws DocumentException {
        this.startTable(new float[0]);
    }

    public void startTable(boolean fitToPage) throws DocumentException {
        this.startTable(new float[0], 0.0f, Color.black, 0, 1.0f, fitToPage, 2.0f);
    }

    public void startTable(float[] colWith) throws DocumentException {
        this.startTable(colWith, 0.0f, Color.black, 0, 1.0f, true, 2.0f);
    }

    public void startTable(float[] colWith, boolean fitToPage) throws DocumentException {
        this.startTable(colWith, 0.0f, Color.black, 0, 1.0f, fitToPage, 2.0f);
    }

    public void startTable(float[] colWith, float offset) throws DocumentException {
        this.startTable(colWith, 0.0f, Color.black, 15, 1.0f, true, 2.0f, 2.0f);
    }

    public void startTable(float[] colWith, float borderWith, Color color, int defaultCellBorder, float defaultCellBorderWith, boolean fitToPage, float spacing) throws DocumentException {
        this.startTable(colWith, borderWith, color, defaultCellBorder, defaultCellBorderWith, fitToPage, spacing, 0.0f);
    }

    public void startTable(float[] colWith, float borderWith, Color color, int defaultCellBorder, float defaultCellBorderWith, boolean fitToPage, float spacing, float offset) throws DocumentException {
        this._table = new Table(colWith.length != 0 ? colWith.length : 1);
        this._table.setBorderWidth(borderWith);
        this._table.setBorderColor(color);
        this._table.setDefaultCellBorder(defaultCellBorder);
        this._table.setDefaultCellBorderWidth(defaultCellBorderWith);
        this._table.setTableFitsPage(fitToPage);
        this._table.setUseVariableBorders(true);
        this._table.setPadding(spacing);
        if (colWith.length != 0) {
            this._table.setWidths(colWith);
        }
    }

    public void setOffset(float offset) {
        if (this._table != null) {
            this._table.setOffset(offset);
        }
    }

    public void setTableBorder(int border, Color color, float width) {
        this.setTableBorder(border);
        this.setTableBorderColor(color);
        this.setTableBorderWidth(width);
    }

    public void setTableBorderWidth(float width) {
        this._table.setBorderWidth(width);
    }

    public void setTableBorderColor(Color color) {
        this._table.setBorderColor(color);
    }

    public void setTableBorder(int border) {
        this._table.setBorder(border);
    }

    public void setTableCellDefaults(Color backgroundColor, int border, Color borderColor, float borderWidth, float grayFill) {
        this.setTableCellDefaultBackgroundColor(backgroundColor);
        this.setTableCellDefaultBorder(border);
        this.setTableCellDefaultBorderColor(borderColor);
        this.setTableCellDefaultBorderWidth(borderWidth);
        this.setTableCellDefaultGrayFill(grayFill);
    }

    public void setTableCellDefaultGrayFill(float grayFill) {
        this._table.setDefaultCellGrayFill(grayFill);
    }

    public void setTableCellDefaultBorderWidth(float borderWidth) {
        this._table.setDefaultCellBorderWidth(borderWidth);
    }

    public void setTableCellDefaultBorderColor(Color borderColor) {
        this._table.setDefaultCellBorderColor(borderColor);
    }

    public void setTableCellDefaultBorder(int border) {
        this._table.setDefaultCellBorder(border);
    }

    public void setTableCellDefaultBackgroundColor(Color backgroundColor) {
        this._table.setDefaultCellBackgroundColor(backgroundColor);
    }

    public void endTable() throws DocumentException {
        this._document.add(this._table);
    }

    public void insertTable(Table table) {
        this._table.insertTable(table);
    }

    public Table resetTable() {
        return this._table;
    }

    protected void setTableFitsPage(boolean fit) {
        this._table.setTableFitsPage(fit);
    }

    protected void setCellsFitPage(boolean fit) {
        this._table.setCellsFitPage(fit);
    }

    protected void newPage() throws DocumentException {
        this._document.newPage();
    }

    protected void rotatePage() {
        this._document.setPageSize(this._document.getPageSize().rotate());
    }

    protected void emptySpace() throws DocumentException, BadElementException {
        this.startTable();
        this.startCell();
        this.emptyCell();
        this.endCell();
        this.endTable();
    }

    protected OutputStream getStream() {
        return this._stream;
    }
}

