/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;

public class PDFHeaderFooterEvent
extends PdfPageEventHelper {
    private Element header;
    private boolean hasPageNumber;
    protected PdfContentByte _content = null;
    protected PdfTemplate _template = null;
    protected HeaderFooter _footer = null;
    protected BaseFont _font = null;

    public PDFHeaderFooterEvent(Element header, boolean hasPageNumber) {
        this.header = header;
        this.hasPageNumber = hasPageNumber;
    }

    public PDFHeaderFooterEvent(boolean hasPageNumber) {
        this.hasPageNumber = hasPageNumber;
    }

    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        try {
            this._font = BaseFont.createFont("Helvetica", "Cp1252", false);
            this._content = writer.getDirectContent();
            this._template = this._content.createTemplate(15.0f, 6.0f);
        }
        catch (DocumentException de) {
        }
        catch (IOException ioe) {
            // empty catch block
        }
    }

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        if (this.header != null) {
            try {
                document.add(this.header);
            }
            catch (DocumentException de) {
                de.printStackTrace();
            }
        }
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        if (this.hasPageNumber) {
            int pageN = writer.getPageNumber();
            String text = "P\u00e1gina " + pageN + " de ";
            int yPos = (int)document.getPageSize().width() - 63;
            float len = this._font.getWidthPoint(text, 6.0f);
            this._content.beginText();
            this._content.setFontAndSize(this._font, 6.0f);
            this._content.setTextMatrix(yPos, 12.0f);
            this._content.showText(text);
            this._content.endText();
            this._content.addTemplate(this._template, (float)yPos + len, 12.0f);
        }
    }

    @Override
    public void onCloseDocument(PdfWriter writer, Document document) {
        this._template.beginText();
        this._template.setFontAndSize(this._font, 6.0f);
        this._template.showText(String.valueOf(writer.getPageNumber() - 1));
        this._template.endText();
    }
}

