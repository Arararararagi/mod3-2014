/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model.annotations;

@Retention(value=RetentionPolicy.RUNTIME)
public @interface Max {
    public static final int UNBOUNDED = -1;

    public int value() default 1;
}

