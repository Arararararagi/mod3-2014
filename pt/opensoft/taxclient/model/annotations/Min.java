/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model.annotations;

@Retention(value=RetentionPolicy.RUNTIME)
public @interface Min {
    public int value() default 0;
}

