/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model;

import com.jgoodies.binding.beans.Model;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.util.ServerValidationResult;

public abstract class DeclaracaoModel
extends Model {
    protected final DeclaracaoMapModel<FormKey, AnexoModel> anexos;
    protected final Map<String, Integer> contadorAnexos;
    public int lastHashCode;
    protected String declaracaoName;
    protected String declaracaoFilePath;
    protected int version;

    public DeclaracaoModel(String declaracaoName, int version) {
        this.setDeclaracaoName(declaracaoName);
        this.declaracaoFilePath = null;
        this.version = version;
        this.anexos = new DeclaracaoMapModel();
        this.contadorAnexos = new HashMap<String, Integer>();
        this.lastHashCode = this.hashCode();
    }

    protected abstract AnexoModel createAnexo(FormKey var1);

    public void addAnexo(Class<? extends AnexoModel> anexoClass) {
        this.addAnexo(anexoClass, null);
    }

    public void addAnexo(Class<? extends AnexoModel> anexoClass, String subID) {
        FormKey key = new FormKey(anexoClass, subID);
        AnexoModel anexo = this.createAnexo(key);
        this.addAnexo(anexo);
    }

    public void addAnexo(AnexoModel anexo) {
        int max;
        Max maxAnnotation = (Max)anexo.getClass().getAnnotation(Max.class);
        if (maxAnnotation != null && (max = maxAnnotation.value()) != -1 && this.countAnexosByType(anexo.formKey.getId()) >= max) {
            throw new RuntimeException("Reached Limit of Anexos of type: " + anexo.getClass().getSimpleName() + " - " + max);
        }
        if (!this.anexos.containsKey(anexo.getFormKey())) {
            Integer count = this.contadorAnexos.get(anexo.getFormKey().getId());
            if (count == null) {
                this.contadorAnexos.put(anexo.getFormKey().getId(), 1);
            } else {
                this.contadorAnexos.put(anexo.getFormKey().getId(), count + 1);
            }
        }
        this.anexos.put(this, anexo.formKey, anexo);
    }

    public AnexoModel removeAnexo(Class<? extends AnexoModel> anexoClass, String subID) {
        FormKey key = new FormKey(anexoClass, subID);
        return this.removeAnexo(key);
    }

    public AnexoModel removeAnexo(FormKey formKey) {
        if (this.anexos.containsKey(formKey)) {
            Integer count = this.contadorAnexos.get(formKey.getId());
            if (count == null) {
                throw new RuntimeException("Contador de anexos inconsistente com o modelo.");
            }
            this.contadorAnexos.put(formKey.getId(), count - 1);
        }
        return this.anexos.remove(formKey);
    }

    public AnexoModel getAnexo(Class<? extends AnexoModel> anexoClass) {
        return this.getAnexo(anexoClass, null);
    }

    public AnexoModel getAnexo(Class<? extends AnexoModel> anexoClass, String subId) {
        return this.getAnexo(new FormKey(anexoClass, subId));
    }

    public AnexoModel getAnexo(FormKey formKey) {
        return this.anexos.get(formKey);
    }

    public DeclaracaoMapModel<FormKey, AnexoModel> getAnexos() {
        return this.anexos;
    }

    public int countAnexosByType(String id) {
        return this.contadorAnexos.get(id) == null ? 0 : this.contadorAnexos.get(id);
    }

    public boolean anexoExistsInModel(Class<? extends AnexoModel> anexoClass, String subId) {
        FormKey key = new FormKey(anexoClass, subId);
        return this.anexos.containsKey(key);
    }

    public List<AnexoModel> getAllAnexosByType(Class<? extends AnexoModel> anexoModelClass) {
        Set<FormKey> set = this.anexos.keySet();
        Iterator<FormKey> iterator = set.iterator();
        ArrayList<AnexoModel> list = new ArrayList<AnexoModel>();
        String id = anexoModelClass.getSimpleName().substring(0, anexoModelClass.getSimpleName().indexOf("Model"));
        while (iterator.hasNext()) {
            FormKey key = iterator.next();
            if (!id.equals(key.getId())) continue;
            list.add(this.getAnexo(key));
        }
        return list;
    }

    public List<FormKey> getAllAnexosByType(String id) {
        Set<FormKey> set = this.anexos.keySet();
        Iterator<FormKey> iterator = set.iterator();
        ArrayList<FormKey> list = new ArrayList<FormKey>();
        while (iterator.hasNext()) {
            FormKey key = iterator.next();
            if (!id.equals(key.getId())) continue;
            list.add(key);
        }
        return list;
    }

    public void addDeclaracaoMapModelListener(MapDataListener mapDataListener) {
        this.anexos.addMapDataListener(mapDataListener);
    }

    public void removeDeclaracaoMapModelListener(MapDataListener mapDataListener) {
        this.anexos.removeMapDataListener(mapDataListener);
    }

    public void removeAllDeclaracaoMapModelListener(Class<? extends MapDataListener> mapDataListenerClass) {
        this.anexos.removeAllMapDataListener(mapDataListenerClass);
    }

    public String getDeclaracaoName() {
        return this.declaracaoName;
    }

    public void setDeclaracaoName(String declaracaoName) {
        this.declaracaoName = declaracaoName;
    }

    public String getDeclaracaoFilePath() {
        return this.declaracaoFilePath;
    }

    public void setDeclaracaoFilePath(String declaracaoFilePath) {
        this.declaracaoFilePath = declaracaoFilePath;
    }

    public boolean isDirty() {
        return this.hashCode() != this.lastHashCode;
    }

    public void resetDirty() {
        int currentHashCode = this.hashCode();
        if (this.lastHashCode != currentHashCode) {
            this.lastHashCode = currentHashCode;
        }
    }

    public int getVersion() {
        return this.version;
    }

    public void resetModel() {
        this.anexos.clear();
        this.contadorAnexos.clear();
        this.setDeclaracaoName(this.declaracaoName);
        this.declaracaoFilePath = null;
        ServerValidationResult.CENTRAL_ERRORS.clearValidationResult();
        this.resetDirty();
    }
}

