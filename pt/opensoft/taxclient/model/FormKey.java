/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model;

import pt.opensoft.taxclient.model.AnexoModel;

public class FormKey {
    protected String id;
    protected String subId;

    public FormKey(Class<? extends AnexoModel> anexoModelClass) {
        this(anexoModelClass, null);
    }

    public FormKey(Class<? extends AnexoModel> anexoModelClass, String subId) {
        if (anexoModelClass == null) {
            throw new IllegalArgumentException("'anexoModelClass' cannot be empty");
        }
        this.id = anexoModelClass.getSimpleName().substring(0, anexoModelClass.getSimpleName().indexOf("Model"));
        this.subId = subId;
    }

    public String getId() {
        return this.id;
    }

    public String getSubId() {
        return this.subId;
    }

    public String getUniqueId() {
        return this.getId().concat(this.getSubId() != null ? this.getSubId() : "");
    }

    public String toString() {
        return "FormKey: [" + this.getUniqueId() + "]";
    }

    public boolean equals(Object obj) {
        if (obj instanceof FormKey) {
            FormKey keyToCompare = (FormKey)obj;
            return this.id.equals(keyToCompare.getId()) && (this.subId == null || this.subId.equals(keyToCompare.getSubId()));
        }
        return false;
    }

    public int hashCode() {
        if (this.subId != null) {
            return this.id.hashCode() + this.subId.hashCode();
        }
        return this.id.hashCode();
    }
}

