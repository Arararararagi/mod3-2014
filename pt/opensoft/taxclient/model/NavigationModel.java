/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model;

import com.jgoodies.binding.beans.Model;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.EventListenerList;
import pt.opensoft.swing.event.MapDataEvent;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.util.Session;

public abstract class NavigationModel
extends Model
implements MapDataListener,
PropertyChangeListener {
    private boolean isClearing = false;
    private boolean isReading = false;
    protected EventListenerList listenerList = new EventListenerList();

    public NavigationModel() {
        DeclarationReader.addReadingListener(this);
    }

    public void unregisterListeners() {
        DeclarationReader.removeReadingListener(this);
    }

    protected abstract void insertNavigationItem(FormKey var1);

    protected abstract void removeNavigationItem(FormKey var1);

    protected abstract void removeAllNavigationItems();

    protected abstract void changeNavigationItem(FormKey var1);

    protected abstract void notifyDisplayerOfSelection(NavigationDisplayer var1, FormKey var2);

    @Override
    public void objectPut(MapDataEvent e) {
        if (!e.getSource().equals(this)) {
            Object key = e.getKey();
            this.insertNavigationItem((FormKey)key);
            if (!this.isReading) {
                this.notifyDisplayerOfSelection(Session.getMainFrame().getNavigationDisplayer(), (FormKey)key);
            }
        }
    }

    @Override
    public void objectRemoved(MapDataEvent e) {
        if (!(this.isClearing || e.getSource().equals(this))) {
            Object key = e.getKey();
            this.removeNavigationItem((FormKey)key);
        }
    }

    @Override
    public void contentChanged(MapDataEvent e) {
        if (!e.getSource().equals(this)) {
            Object key = e.getKey();
            this.changeNavigationItem((FormKey)key);
        }
    }

    @Override
    public void startedClear() {
        this.isClearing = true;
        this.removeAllNavigationItems();
    }

    @Override
    public void finishedClear() {
        this.isClearing = false;
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals("reading")) {
            this.isReading = (Boolean)event.getNewValue();
        }
    }
}

