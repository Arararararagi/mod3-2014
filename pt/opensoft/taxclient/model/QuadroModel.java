/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model;

import com.jgoodies.binding.beans.Model;

public abstract class QuadroModel
extends Model {
    protected String quadroName;

    public String getQuadroName() {
        return this.quadroName;
    }

    public void setQuadroName(String quadroName) {
        this.quadroName = quadroName;
    }

    public void reset() {
    }

    public void rebindMe() {
    }
}

