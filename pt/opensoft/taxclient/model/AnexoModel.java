/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model;

import com.jgoodies.binding.beans.Model;
import java.util.List;
import pt.opensoft.swing.binding.DynamicArrayListModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;

public abstract class AnexoModel
extends Model
implements Comparable<AnexoModel> {
    private static final long serialVersionUID = -1591130420377964954L;
    protected FormKey formKey;
    protected DynamicArrayListModel<QuadroModel> quadros;

    public AnexoModel() {
        this.quadros = new DynamicArrayListModel();
    }

    public AnexoModel(FormKey formKey) {
        this.formKey = formKey;
        this.quadros = new DynamicArrayListModel();
    }

    public boolean addQuadro(QuadroModel quadro) {
        return this.quadros.add(quadro);
    }

    public boolean removeQuadro(QuadroModel quadro) {
        return this.quadros.remove(quadro);
    }

    public QuadroModel removeQuadro(int index) {
        return this.quadros.remove(index);
    }

    public QuadroModel getQuadro(String name) {
        if (name != null) {
            for (QuadroModel quadroModel : this.quadros) {
                if ((quadroModel.getQuadroName() == null || !quadroModel.getQuadroName().equals(name)) && !quadroModel.getClass().getName().endsWith(name)) continue;
                return quadroModel;
            }
        }
        return null;
    }

    public void removeQuadro(String name) {
        if (name != null) {
            for (QuadroModel quadroModel : this.quadros) {
                if ((quadroModel.getQuadroName() == null || !quadroModel.getQuadroName().equals(name)) && !quadroModel.getClass().getName().endsWith(name)) continue;
                this.removeQuadro(quadroModel);
            }
        }
    }

    public FormKey getFormKey() {
        return this.formKey;
    }

    public List<QuadroModel> getQuadros() {
        return this.quadros;
    }

    @Override
    public int compareTo(AnexoModel anexoToCompare) {
        if (anexoToCompare == null) {
            throw new NullPointerException("anexoToCompare cannot be null!");
        }
        if (anexoToCompare.getFormKey() == null) {
            throw new NullPointerException("anexoToCompare has no FormKey!");
        }
        FormKey fKey = anexoToCompare.getFormKey();
        int keyComparison = this.getFormKey().getId().compareTo(fKey.getId());
        if (keyComparison != 0) {
            return keyComparison;
        }
        if (this.getFormKey().getSubId() == null && fKey.getSubId() == null) {
            return 0;
        }
        return this.getFormKey().getSubId().compareTo(fKey.getSubId());
    }
}

