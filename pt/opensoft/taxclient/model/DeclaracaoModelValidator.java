/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.Validator;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class DeclaracaoModelValidator
implements Validator<DeclaracaoModel> {
    protected ValidationResult declarationValidationResult;

    public ValidationResult getDeclarationValidationResult() {
        return this.declarationValidationResult;
    }

    public void setDeclarationValidationResult(ValidationResult declarationValidationResult) {
        this.declarationValidationResult = declarationValidationResult;
    }

    protected abstract ValidationResult validateDeclaration(DeclaracaoModel var1);

    @Override
    public ValidationResult validate(DeclaracaoModel validationTarget) {
        this.declarationValidationResult = this.validateDeclaration(validationTarget);
        return this.declarationValidationResult;
    }
}

