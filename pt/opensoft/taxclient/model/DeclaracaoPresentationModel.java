/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.model;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.event.EventListenerList;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;
import pt.opensoft.taxclient.ui.event.NavigationSelectionListener;
import pt.opensoft.taxclient.ui.event.SelectedListener;
import pt.opensoft.taxclient.ui.event.SelectionEvent;

public class DeclaracaoPresentationModel
extends PresentationModel<DeclaracaoModel> {
    private static final long serialVersionUID = 2181744895526851313L;
    protected List<SelectedListener> selectedListeners = new ArrayList<SelectedListener>();
    public static final String SELECTEDQUADRO = "selectedQuadro";
    private FormKey selectedAnexo;
    private String selectedQuadro;
    protected EventListenerList listenerList = new EventListenerList();

    public DeclaracaoPresentationModel(DeclaracaoModel declaracaoModel) {
        super(new ValueHolder(declaracaoModel, true));
    }

    public void addNavigationSelectionListener(NavigationSelectionListener navigationSelectionListener) {
        this.listenerList.add(NavigationSelectionListener.class, navigationSelectionListener);
    }

    public void removeNavigationSelectionListener(NavigationSelectionListener navigationListener) {
        this.listenerList.remove(NavigationSelectionListener.class, navigationListener);
    }

    public void addSelectionListener(SelectedListener selectionListener) {
        this.selectedListeners.add(selectionListener);
    }

    public void removeSelectionListener(SelectedListener selectionListener) {
        this.selectedListeners.remove(selectionListener);
    }

    protected void fireNavigationItemSelected(Object source, FormKey formKey) {
        if (this.listenerList.getListenerCount() > 0) {
            NavigationItemEvent navigationItemEvent = new NavigationItemEvent(source, NavigationItemEvent.NavigationItemEventType.SELECTED, formKey);
            Object[] listeners = this.listenerList.getListenerList();
            for (int i = listeners.length - 2; i >= 0; i-=2) {
                if (listeners[i] != NavigationSelectionListener.class) continue;
                ((NavigationSelectionListener)listeners[i + 1]).navigationItemSelected(navigationItemEvent);
            }
        }
    }

    protected void fireSelectedItem(Object source, FormKey formKey) {
        Iterator<SelectedListener> iterator = this.selectedListeners.iterator();
        while (iterator.hasNext()) {
            SelectionEvent selectionEvent = new SelectionEvent(source, formKey);
            iterator.next().selectedAnexoChanged(selectionEvent);
        }
    }

    public void setSelectedAnexo(Object caller, FormKey selectedAnexo) {
        if (this.selectedAnexo != null && this.selectedAnexo.equals(selectedAnexo)) {
            return;
        }
        this.selectedAnexo = selectedAnexo;
        this.fireSelectedItem(caller, selectedAnexo);
        AnexoModel anexo = ((DeclaracaoModel)this.getBean()).getAnexo(selectedAnexo);
        if (anexo.getQuadros().size() > 0) {
            this.selectedQuadro = anexo.getQuadros().get(0).getClass().getSimpleName();
        }
    }

    public void setSelectedRootAnexo(FormKey rootFormKey) {
        this.selectedAnexo = rootFormKey;
        this.fireSelectedItem(this, this.selectedAnexo);
        AnexoModel anexo = ((DeclaracaoModel)this.getBean()).getAnexo(this.selectedAnexo);
        if (anexo.getQuadros().size() > 0) {
            this.selectedQuadro = anexo.getQuadros().get(0).getClass().getSimpleName();
        }
    }

    public FormKey getSelectedAnexo() {
        return this.selectedAnexo;
    }

    public void setSelectedAnexo(FormKey selectedAnexo) {
        this.setSelectedAnexo(this, selectedAnexo);
    }

    public DeclaracaoModel getDeclaracaoModel() {
        return (DeclaracaoModel)this.getBean();
    }

    public String getSelectedQuadro() {
        return this.selectedQuadro;
    }

    public void setSelectedQuadro(String selectedQuadro) {
        String old = this.selectedQuadro;
        this.selectedQuadro = selectedQuadro;
        this.firePropertyChange("selectedQuadro", old, this.selectedQuadro);
    }
}

