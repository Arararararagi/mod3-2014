/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.file;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import org.xml.sax.SAXException;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.persistence.DeclarationWriter;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public abstract class TaxClientRevampedNewFileAction
extends EnhancedAction {
    private static final long serialVersionUID = -5052972085699346273L;

    public TaxClientRevampedNewFileAction() {
        super("Novo", IconFactory.getIconSmall(GUIParameters.ICON_NOVO), IconFactory.getIconBig(GUIParameters.ICON_NOVO), "Nova declara\u00e7\u00e3o", KeyStroke.getKeyStroke(78, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DeclaracaoModel currentDeclaracao = Session.getCurrentDeclaracao().getDeclaracaoModel();
        Session.getMainFrame().getMultiPurposeDisplayer().hideMultiPurposeDisplayer();
        if (currentDeclaracao.isDirty()) {
            String messageAsk = currentDeclaracao.getDeclaracaoFilePath() != null ? "Quer gravar as altera\u00e7\u00f5es efectuadas no ficheiro " + currentDeclaracao.getDeclaracaoFilePath() + "?" : "Quer gravar as altera\u00e7\u00f5es efectuadas?";
            int returnCode = DialogFactory.instance().showConfirmationDialog2(messageAsk);
            if (returnCode == 2 || returnCode == -1) {
                return;
            }
            if (returnCode == 0) {
                TaxClientRevampedSaveFileAction saveFileAction = (TaxClientRevampedSaveFileAction)ActionsTaxClientManager.getAction("Gravar");
                saveFileAction.actionPerformed(e);
                if (saveFileAction.wasCanceled()) {
                    return;
                }
            }
        }
        try {
            currentDeclaracao.resetModel();
            currentDeclaracao = this.inicializaDeclarationModel();
            DeclarationWriter dwriter = new DeclarationWriter();
            StringWriter strwriter = new StringWriter();
            dwriter.write(strwriter, currentDeclaracao, null, null, false, false);
            DeclarationReader.read(new StringReader(strwriter.toString()), Session.getCurrentDeclaracao().getDeclaracaoModel(), true, false);
            Session.getCurrentDeclaracao().getDeclaracaoModel().resetDirty();
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        catch (SAXException e1) {
            e1.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected abstract DeclaracaoModel inicializaDeclarationModel();
}

