/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.file;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.actions.util.DefaultSavingDialog;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.persistence.DeclarationWriter;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedSaveFileAsAction
extends EnhancedAction {
    private static final long serialVersionUID = -358394186009880616L;
    private boolean wasCanceled;
    private JDialog savingDialog = new DefaultSavingDialog();
    private boolean showDialogWhileSaving = true;

    public TaxClientRevampedSaveFileAsAction() {
        super("Gravar como... ", IconFactory.getIconSmall(GUIParameters.ICON_GRAVAR), IconFactory.getIconBig(GUIParameters.ICON_GRAVAR), KeyStroke.getKeyStroke(71, 3));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        File file;
        JFileChooser fileChooser = this.getJFileChooser();
        boolean fileChosen = false;
        do {
            int optionChosen;
            if ((optionChosen = fileChooser.showSaveDialog(DialogFactory.getDummyFrame())) == 1) {
                this.wasCanceled = true;
                return;
            }
            if (optionChosen != 0) {
                return;
            }
            file = fileChooser.getSelectedFile();
            if (file == null) {
                return;
            }
            if (file.exists()) {
                if (!DialogFactory.instance().showYesNoDialog("Confirme que pretende sobrepor o conte\u00fado do ficheiro " + file.getName())) continue;
                fileChosen = true;
                continue;
            }
            fileChosen = true;
        } while (!fileChosen);
        SaveAsWorker saveAsWorker = new SaveAsWorker(this.savingDialog, file);
        saveAsWorker.execute();
    }

    public boolean isWasCanceled() {
        return this.wasCanceled;
    }

    @Override
    public boolean showInToolbar() {
        return false;
    }

    protected JFileChooser getJFileChooser() {
        return DialogFactory.instance().getFileChooser();
    }

    public void setSavingDialog(JDialog savingDialog) {
        if (savingDialog != null) {
            this.savingDialog = savingDialog;
        }
    }

    public void setShowDialogWhileSaving(boolean showDialogWhileSaving) {
        this.showDialogWhileSaving = showDialogWhileSaving;
    }

    private class SaveAsWorker
    extends SwingWorker<Void, Void> {
        private final JDialog savingDialog;
        private final File file;

        public SaveAsWorker(JDialog savingDialog, File file) {
            this.savingDialog = savingDialog;
            this.file = file;
        }

        @Override
        protected Void doInBackground() throws Exception {
            if (TaxClientRevampedSaveFileAsAction.this.showDialogWhileSaving) {
                this.showSavingDialog();
            }
            DialogFactory.instance().showWaitCursor();
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(this.file);
                DeclarationWriter writer = new DeclarationWriter();
                writer.write(new OutputStreamWriter(fileOutputStream), Session.getCurrentDeclaracao().getDeclaracaoModel(), null, null, false, false);
                fileOutputStream.flush();
                fileOutputStream.close();
                Session.getCurrentDeclaracao().getDeclaracaoModel().setDeclaracaoFilePath(this.file.getAbsolutePath());
                Session.getCurrentDeclaracao().getDeclaracaoModel().resetDirty();
                if (TaxClientRevampedSaveFileAsAction.this.showDialogWhileSaving) {
                    this.savingDialog.dispose();
                }
                Toolkit.getDefaultToolkit().beep();
                DialogFactory.instance().showInformationDialog(DesignationManager.SAVE_FILE_SUCCESS_MESSAGE);
            }
            catch (Exception e1) {
                String message = "<html><h3>Ocorreu um problema na escrita do ficheiro.</h3>";
                DialogFactory.instance().showErrorDialog(message);
                e1.printStackTrace();
            }
            finally {
                if (this.savingDialog != null) {
                    this.savingDialog.dispose();
                }
                DialogFactory.instance().dismissWaitCursor();
            }
            return null;
        }

        @Override
        protected void done() {
            if (this.savingDialog != null) {
                this.savingDialog.dispose();
            }
            DialogFactory.instance().dismissWaitCursor();
        }

        private void showSavingDialog() {
            this.savingDialog.setLocationRelativeTo(null);
            this.savingDialog.pack();
            this.savingDialog.setVisible(true);
        }
    }

}

