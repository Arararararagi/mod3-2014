/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.file;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.InvalidContextException;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAsAction;
import pt.opensoft.taxclient.actions.util.DefaultSavingDialog;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.persistence.DeclarationWriter;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedSaveFileAction
extends EnhancedAction {
    private static final long serialVersionUID = 3199363764970693053L;
    protected TaxClientRevampedSaveFileAsAction saveFileAsAction = (TaxClientRevampedSaveFileAsAction)ActionsTaxClientManager.getAction("GravarComo");
    private JDialog savingDialog = new DefaultSavingDialog();
    private boolean showDialogWhileSaving = true;

    public TaxClientRevampedSaveFileAction() {
        super("Gravar", IconFactory.getIconSmall(GUIParameters.ICON_GRAVAR), IconFactory.getIconBig(GUIParameters.ICON_GRAVAR), KeyStroke.getKeyStroke(71, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DeclaracaoModel currentModel = Session.getCurrentDeclaracao().getDeclaracaoModel();
        if (currentModel.getDeclaracaoFilePath() == null) {
            this.saveFileAsAction.actionPerformed(e);
            return;
        }
        try {
            super.actionPerformed(e);
        }
        catch (InvalidContextException ex) {
            return;
        }
        try {
            File file = new File(currentModel.getDeclaracaoFilePath());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            SaveWorker saveWorker = new SaveWorker(this.savingDialog, fileOutputStream);
            saveWorker.execute();
        }
        catch (FileNotFoundException fnfe) {
            this.saveFileAsAction.actionPerformed(e);
        }
        catch (Exception ie) {
            String message = "<html><h3>Ocorreu um problema na escrita do ficheiro.</h3>";
            DialogFactory.instance().showErrorDialog(message);
            ie.printStackTrace();
        }
    }

    protected JFileChooser getJFileChooser() {
        return DialogFactory.instance().getFileChooser();
    }

    public boolean wasCanceled() {
        return this.saveFileAsAction.isWasCanceled();
    }

    public void declaracaoChanged() {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new RuntimeException("SaveFileAction.declaracaoChanged was called outside event dispatch thread");
        }
    }

    public void setSavingDialog(JDialog savingDialog) {
        if (savingDialog != null) {
            this.savingDialog = savingDialog;
        }
    }

    public void setShowDialogWhileSaving(boolean showDialogWhileSaving) {
        this.showDialogWhileSaving = showDialogWhileSaving;
    }

    private class SaveWorker
    extends SwingWorker<Void, Void> {
        private final JDialog savingDialog;
        private final FileOutputStream fileOutputStream;

        public SaveWorker(JDialog savingDialog, FileOutputStream fileOutputStream) {
            this.savingDialog = savingDialog;
            this.fileOutputStream = fileOutputStream;
        }

        @Override
        protected Void doInBackground() throws Exception {
            if (TaxClientRevampedSaveFileAction.this.showDialogWhileSaving) {
                this.showSavingDialog();
            }
            DialogFactory.instance().showWaitCursor();
            try {
                DeclarationWriter writer = new DeclarationWriter();
                writer.write(new OutputStreamWriter(this.fileOutputStream), Session.getCurrentDeclaracao().getDeclaracaoModel(), null, null, false, false);
                this.fileOutputStream.flush();
                this.fileOutputStream.close();
                Session.getCurrentDeclaracao().getDeclaracaoModel().resetDirty();
                if (TaxClientRevampedSaveFileAction.this.showDialogWhileSaving) {
                    this.savingDialog.dispose();
                }
                Toolkit.getDefaultToolkit().beep();
                DialogFactory.instance().showInformationDialog(DesignationManager.SAVE_FILE_SUCCESS_MESSAGE);
            }
            catch (Exception e1) {
                String message = "<html><h3>Ocorreu um problema na escrita do ficheiro.</h3>";
                DialogFactory.instance().showErrorDialog(message);
                e1.printStackTrace();
            }
            finally {
                if (this.savingDialog != null) {
                    this.savingDialog.dispose();
                }
                DialogFactory.instance().dismissWaitCursor();
            }
            return null;
        }

        @Override
        protected void done() {
            if (this.savingDialog != null) {
                this.savingDialog.dispose();
            }
            DialogFactory.instance().dismissWaitCursor();
        }

        private void showSavingDialog() {
            this.savingDialog.setLocationRelativeTo(null);
            this.savingDialog.pack();
            this.savingDialog.setVisible(true);
        }
    }

}

