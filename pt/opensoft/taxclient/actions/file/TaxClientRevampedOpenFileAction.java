/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.file;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.xml.sax.SAXException;
import pt.opensoft.logging.Logger;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedNewFileAction;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAction;
import pt.opensoft.taxclient.app.InvalidMessageException;
import pt.opensoft.taxclient.app.InvalidXmlFileException;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.persistence.exception.CorruptedFileException;
import pt.opensoft.taxclient.ui.event.SelectionEvent;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.TaxclientParameters;

public class TaxClientRevampedOpenFileAction
extends EnhancedAction {
    private static final long serialVersionUID = 6935200540091284230L;

    public TaxClientRevampedOpenFileAction() {
        super("Abrir", IconFactory.getIconSmall(GUIParameters.ICON_ABRIR), IconFactory.getIconBig(GUIParameters.ICON_ABRIR), "Abrir declara\u00e7\u00e3o", KeyStroke.getKeyStroke(65, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.actionPerformed(e, true);
    }

    public void actionPerformed(ActionEvent e, boolean verifyDirty) {
        JFileChooser fileChooser;
        final DeclaracaoModel _declaration = Session.getCurrentDeclaracao().getDeclaracaoModel();
        if (_declaration.isDirty() && verifyDirty) {
            String messageAsk = _declaration.getDeclaracaoFilePath() != null ? "Quer gravar as altera\u00e7\u00f5es efectuadas no ficheiro " + _declaration.getDeclaracaoFilePath() + "?" : "Quer gravar as altera\u00e7\u00f5es efectuadas?";
            int returnCode = DialogFactory.instance().showConfirmationDialog2(messageAsk);
            if (returnCode == 2 || returnCode == -1) {
                return;
            }
            if (returnCode == 0) {
                TaxClientRevampedSaveFileAction saveFileAction = (TaxClientRevampedSaveFileAction)ActionsTaxClientManager.getAction("Gravar");
                saveFileAction.actionPerformed(e);
                if (saveFileAction.wasCanceled()) {
                    return;
                }
            }
        }
        if ((fileChooser = this.getJFileChooser()).showOpenDialog(DialogFactory.getDummyFrame()) != 0) {
            return;
        }
        File file = fileChooser.getSelectedFile();
        if (!(file != null && file.isFile())) {
            DialogFactory.instance().showErrorDialog("O ficheiro seleccionado n\u00e3o existe");
            return;
        }
        long start = 0;
        try {
            try {
                DeclarationReader.setReading(true);
                DialogFactory.instance().showWaitCursor();
                start = System.currentTimeMillis();
                _declaration.resetModel();
                this.getFile(file);
            }
            catch (InvalidXmlFileException xe) {
                DeclarationReader.setReading(false);
                if (!xe.isNonFatal()) {
                    throw xe;
                }
                String message = "<html><h2>Ocorreram problemas na leitura do ficheiro.</h2><br> Poder\u00e1 n\u00e3o ter sido poss\u00edvel ler a totalidade da declara\u00e7\u00e3o.<br>Por favor, confirme que os dados est\u00e3o correctos.<br><br>Erros encontrados:</html>";
                DialogFactory.instance().showMultipleErrorsDialog(message, xe.getErrorListAsString());
                xe.printStackTrace();
            }
            catch (RuntimeException runtE) {
                DeclarationReader.setReading(false);
                _declaration.resetModel();
                throw runtE;
            }
            catch (Exception runtE) {
                DeclarationReader.setReading(false);
                _declaration.resetModel();
                throw runtE;
            }
            _declaration.setDeclaracaoFilePath(file.getAbsolutePath());
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    _declaration.resetDirty();
                }
            });
            Session.getMainFrame().getMultiPurposeDisplayer().hideMultiPurposeDisplayer();
            this.selectRootAnexo(_declaration);
        }
        catch (InvalidMessageException ex) {
            String message = "<html><h3>Ocorreu um problema na leitura do ficheiro.</h3>" + (ex.getRegisto() != null ? new StringBuilder().append("<br><b>Registo:</b> ").append(ex.getRegisto()).toString() : "") + (ex.getCampo() != null ? new StringBuilder().append("<br><b>Campo:</b> ").append(ex.getCampo()).toString() : "") + "<br><b>Erro:</b> " + ex.getMessage() + "</html>";
            DialogFactory.instance().showErrorDialog(message);
            ex.printStackTrace();
            ((TaxClientRevampedNewFileAction)ActionsTaxClientManager.getAction("Novo")).actionPerformed(null);
        }
        catch (InvalidXmlFileException xe) {
            String message = this.getInvalidXmlFileExceptionMessage(xe);
            DialogFactory.instance().showErrorDialog(message);
            xe.printStackTrace();
            ((TaxClientRevampedNewFileAction)ActionsTaxClientManager.getAction("Novo")).actionPerformed(null);
        }
        catch (FileNotFoundException e1) {
            String message = "<html><h3>Ocorreu um problema na leitura do ficheiro.</h3><br>O ficheiro indicado n\u00e3o existe.</html>";
            DialogFactory.instance().showErrorDialog(message);
            e1.printStackTrace();
        }
        catch (CorruptedFileException exception) {
            DialogFactory.instance().showErrorDialog(exception.getMessageForUser());
            exception.printStackTrace();
            ((TaxClientRevampedNewFileAction)ActionsTaxClientManager.getAction("Novo")).actionPerformed(null);
        }
        catch (Exception e1) {
            String message = "<html><h3>Ocorreu um problema na leitura do ficheiro.</h3></html>";
            DialogFactory.instance().showErrorDialog(message);
            e1.printStackTrace();
            ((TaxClientRevampedNewFileAction)ActionsTaxClientManager.getAction("Novo")).actionPerformed(null);
        }
        finally {
            long stop = System.currentTimeMillis();
            Logger.getDefault().debug("Took " + (stop - start) / 1000 + " sec");
            DialogFactory.instance().dismissWaitCursor();
        }
    }

    private void selectRootAnexo(DeclaracaoModel _declaration) {
        Set<FormKey> keySet = _declaration.getAnexos().keySet();
        Iterator<FormKey> iterator = keySet.iterator();
        boolean foundRoot = false;
        if (iterator.hasNext()) {
            FormKey firstKey = iterator.next();
            Session.getMainFrame().getDeclarationDisplayer().showAnexo(firstKey);
            Session.getMainFrame().getNavigationDisplayer().selectedAnexoChanged(new SelectionEvent(this, firstKey));
            if (firstKey.getUniqueId().equals(TaxclientParameters.FORM_ROOT_DISPLAY_NAME)) {
                foundRoot = true;
            }
        }
        if (!foundRoot) {
            while (iterator.hasNext()) {
                FormKey formKey = iterator.next();
                if (!formKey.getUniqueId().equals(TaxclientParameters.FORM_ROOT_DISPLAY_NAME)) continue;
                Session.getMainFrame().getDeclarationDisplayer().showAnexo(formKey);
                Session.getMainFrame().getNavigationDisplayer().selectedAnexoChanged(new SelectionEvent(this, formKey));
                break;
            }
        }
    }

    protected String getInvalidXmlFileExceptionMessage(InvalidXmlFileException xe) {
        return "<html><h3>Ocorreram problemas na leitura do ficheiro.</h3></html>";
    }

    private void getFile(File file) throws FileNotFoundException, IOException, SAXException {
        DeclarationReader.read(new FileReader(file), Session.getCurrentDeclaracao().getDeclaracaoModel(), true, false);
        Session.getCurrentDeclaracao().getDeclaracaoModel().setDeclaracaoFilePath(file.getCanonicalPath());
        Session.getCurrentDeclaracao().getDeclaracaoModel().resetDirty();
    }

    protected JFileChooser getJFileChooser() {
        return DialogFactory.instance().getFileChooser();
    }

}

