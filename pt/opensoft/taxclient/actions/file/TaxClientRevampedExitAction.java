/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.file;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedExitAction
extends EnhancedAction {
    private static final long serialVersionUID = -5057648479530034765L;

    public TaxClientRevampedExitAction() {
        super("Sair", IconFactory.getBlankIcon(), IconFactory.getBlankIcon(), null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Session.getCurrentDeclaracao().getDeclaracaoModel().isDirty()) {
            String messageAsk = Session.getCurrentDeclaracao().getDeclaracaoModel().getDeclaracaoFilePath() != null ? "Quer gravar as altera\u00e7\u00f5es efectuadas no ficheiro " + Session.getCurrentDeclaracao().getDeclaracaoModel().getDeclaracaoFilePath() + "?" : "Quer gravar as altera\u00e7\u00f5es efectuadas?";
            int returnCode = DialogFactory.instance().showConfirmationDialog2(messageAsk);
            if (returnCode == 2 || returnCode == -1) {
                return;
            }
            if (returnCode == 0) {
                TaxClientRevampedSaveFileAction saveFileAction = (TaxClientRevampedSaveFileAction)ActionsTaxClientManager.getAction("Gravar");
                saveFileAction.actionPerformed(e);
                if (saveFileAction.wasCanceled()) {
                    return;
                }
            }
        }
        System.exit(0);
    }

    @Override
    public boolean showInToolbar() {
        return false;
    }
}

