/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.swing.event.MapDataEvent;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.actions.structural.add.AddAnexoAction;
import pt.opensoft.taxclient.actions.structural.remove.RemoveAnexoAction;
import pt.opensoft.taxclient.actions.structural.remove.impl.DefaultRemoveAnexoAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class ActionsTaxClientManager
implements MapDataListener {
    public static final String NOVO = "Novo";
    public static final String ABRIR = "Abrir";
    public static final String GRAVAR = "Gravar";
    public static final String GRAVAR_COMO = "GravarComo";
    public static final String CONFIGURAR_PROXY = "ConfigurarProxy";
    public static final String IMPRIMIR = "Imprimir";
    public static final String SAIR = "Sair";
    public static final String AJUDA_PREENCHIMENTO = "AjudaPreenchimento";
    public static final String AJUDA_TEMAS = "AjudaTemas";
    public static final String WIZARD = "Wizard";
    public static final String ACERCA_DE = "AcercaDe";
    public static final String AJUDA_NAVEGACAO = "AjudaNavegacao";
    public static final String CORTAR = "Cortar";
    public static final String COPIAR = "Copiar";
    public static final String COLAR = "Colar";
    public static final String VALIDAR = "Validar";
    public static final String SIMULAR = "Simular";
    public static final String SUBMETER = "Submeter";
    public static final String PESQUISAR = "Pesquisar";
    public static final String ERROS_CENTRAIS = "Erros Centrais";
    public static final String REMOVER_ANEXO = "RemoverAnexo";
    public static final String EXIBIR_ANEXO_ANTERIOR = "Exibir Anexo anterior";
    public static final String EXIBIR_ANEXO_SEGUINTE = "Exibir Anexo seguinte";
    public static final String EXIBIR_QUADRO_ANTERIOR = "Exibir Quadro anterior";
    public static final String EXIBIR_QUADRO_SEGUINTE = "Exibir Quadro seguinte";
    private static Map<String, EnhancedAction> _actions;
    private static Map<String, AddAnexoAction> _addActions;
    private static Map<String, RemoveAnexoAction> _removeActions;
    private static DeclaracaoModel _declaracaoModel;
    private static ActionsTaxClientManager _instance;
    private boolean isClearing = false;

    public static void registerAction(String actionName, EnhancedAction action) {
        if (_actions == null) {
            _actions = new TreeMap<String, EnhancedAction>();
        }
        _actions.put(actionName, action);
    }

    protected ActionsTaxClientManager() {
    }

    public static synchronized ActionsTaxClientManager instance() {
        if (_instance == null) {
            throw new RuntimeException("ActionsTaxClientManager must be initialized first");
        }
        return _instance;
    }

    public static EnhancedAction getAction(String actionName) {
        if (_actions != null) {
            EnhancedAction enhancedAction = _actions.get(actionName);
            if (enhancedAction == null) {
                SimpleLog.log("Unregistered add action: " + actionName);
            }
            return enhancedAction;
        }
        SimpleLog.log("No add actions registered!");
        return null;
    }

    public static void initActionsTaxClientManager(DeclaracaoModel model) {
        _instance = new ActionsTaxClientManager();
        _addActions = new LinkedHashMap<String, AddAnexoAction>();
        _removeActions = new LinkedHashMap<String, RemoveAnexoAction>();
        _declaracaoModel = model;
        model.addDeclaracaoMapModelListener(_instance);
        Collection<AnexoModel> anexos = model.getAnexos().values();
        for (AnexoModel anexoModel : anexos) {
            Min min = (Min)anexoModel.getClass().getAnnotation(Min.class);
            Max max = (Max)anexoModel.getClass().getAnnotation(Max.class);
            if (min == null || max == null) continue;
            String id = anexoModel.getFormKey().getId();
            String subId = anexoModel.getFormKey().getSubId();
            _removeActions.put(ActionsTaxClientManager.generateRemoveActionsKey(id, subId), new DefaultRemoveAnexoAction(anexoModel.getClass(), subId, Session.getCurrentDeclaracao().getDeclaracaoModel()));
        }
    }

    public static void registerAddAction(String addActionName, AddAnexoAction action) {
        if (_addActions == null) {
            _addActions = new LinkedHashMap<String, AddAnexoAction>();
        }
        if (!Session.isEditable().booleanValue()) {
            action.setEnabled(false);
        }
        _addActions.put(addActionName, action);
    }

    public static AddAnexoAction getAddAction(String actionName) {
        if (_addActions != null) {
            AddAnexoAction addAction = _addActions.get(actionName);
            if (addAction == null) {
                SimpleLog.log("Unregistered add action: " + actionName);
            }
            return addAction;
        }
        SimpleLog.log("No add actions registered!");
        return null;
    }

    public static void registerAddActions(List<AddAnexoAction> actions) {
        if (_addActions == null) {
            _addActions = new LinkedHashMap<String, AddAnexoAction>();
        }
        for (AddAnexoAction addAnexoAction : actions) {
            ActionsTaxClientManager.registerAddAction(addAnexoAction.getId(), addAnexoAction);
        }
    }

    public static RemoveAnexoAction getRemoveAction(String id) {
        return ActionsTaxClientManager.getRemoveAction(id, null);
    }

    public static RemoveAnexoAction getRemoveAction(String id, String subId) {
        if (!Session.isEditable().booleanValue()) {
            return null;
        }
        Class clazz = null;
        Collection<AnexoModel> anexos = _declaracaoModel.getAnexos().values();
        for (AnexoModel anexoModel : anexos) {
            if (!anexoModel.getFormKey().getId().equals(id)) continue;
            clazz = anexoModel.getClass();
        }
        if (clazz == null) {
            throw new RuntimeException("Inexistent anexo: " + id);
        }
        if ((((Max)clazz.getAnnotation(Max.class)).value() > 1 || ((Max)clazz.getAnnotation(Max.class)).value() == -1) && StringUtil.isEmpty(subId)) {
            throw new RuntimeException("Tentou obter uma ac\u00e7\u00e3o de remo\u00e7\u00e3o de um anexo repetitivo, sem especificar o seu subId");
        }
        if (_removeActions == null) {
            _removeActions = new LinkedHashMap<String, RemoveAnexoAction>();
        }
        if (_removeActions.containsKey(ActionsTaxClientManager.generateRemoveActionsKey(id, subId))) {
            return _removeActions.get(ActionsTaxClientManager.generateRemoveActionsKey(id, subId));
        }
        DefaultRemoveAnexoAction action = new DefaultRemoveAnexoAction(clazz, subId, Session.getCurrentDeclaracao().getDeclaracaoModel());
        _removeActions.put(ActionsTaxClientManager.generateRemoveActionsKey(id, subId), action);
        return action;
    }

    public static List<RemoveAnexoAction> getAllEnabledRemoveActions() {
        Collection<RemoveAnexoAction> actions = _removeActions.values();
        ArrayList<RemoveAnexoAction> actionsEnabled = new ArrayList<RemoveAnexoAction>();
        for (RemoveAnexoAction removeAnexoAction : actions) {
            if (!removeAnexoAction.isEnabled() || !Session.isEditable().booleanValue()) continue;
            actionsEnabled.add(removeAnexoAction);
        }
        return actionsEnabled;
    }

    public static List<AddAnexoAction> getAllAddActions() {
        return new ArrayList<AddAnexoAction>(_addActions.values());
    }

    private static String generateRemoveActionsKey(String id, String subId) {
        return StringUtil.isEmpty(subId) ? id : id + '_' + subId;
    }

    @Override
    public void contentChanged(MapDataEvent e) {
    }

    @Override
    public void objectPut(MapDataEvent e) {
        Object key = e.getKey();
        String id = ((FormKey)key).getId();
        String subId = ((FormKey)key).getSubId();
        Class clazz = _declaracaoModel.getAnexo((FormKey)key).getClass();
        if (clazz.getAnnotation(Min.class) == null && clazz.getAnnotation(Max.class) == null) {
            return;
        }
        if (!_removeActions.containsKey(ActionsTaxClientManager.generateRemoveActionsKey(id, subId))) {
            _removeActions.put(ActionsTaxClientManager.generateRemoveActionsKey(id, subId), new DefaultRemoveAnexoAction(clazz, subId, Session.getCurrentDeclaracao().getDeclaracaoModel()));
        }
    }

    @Override
    public void objectRemoved(MapDataEvent e) {
        String removeActionKey;
        String subId;
        Object key;
        String id;
        RemoveAnexoAction removeAnexoAction;
        if (!(this.isClearing || (removeAnexoAction = _removeActions.get(removeActionKey = ActionsTaxClientManager.generateRemoveActionsKey(id = ((FormKey)(key = e.getKey())).getId(), subId = ((FormKey)key).getSubId()))) == null)) {
            removeAnexoAction.unregisterListeners();
            _removeActions.remove(removeActionKey);
        }
    }

    @Override
    public void startedClear() {
        this.isClearing = true;
        DeclarationReader.removeAllReadingListeners(RemoveAnexoAction.class);
        Session.getCurrentDeclaracao().getDeclaracaoModel().removeAllDeclaracaoMapModelListener(RemoveAnexoAction.class);
        _removeActions.clear();
    }

    @Override
    public void finishedClear() {
        this.isClearing = false;
    }
}

