/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.declaracao;

import com.jgoodies.validation.ValidationResult;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.KeyStroke;
import pt.opensoft.logging.Logger;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitDialog;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.gui.SubmitTaxThread;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.SubmitTaxUtils;

public abstract class TaxClientRevampedSubmitAction
extends EnhancedAction {
    private static final long serialVersionUID = -657352263659904281L;
    protected final JLabel statusCommunication_L = new JLabel();

    public TaxClientRevampedSubmitAction() {
        super("Submeter", IconFactory.getIconSmall(GUIParameters.ICON_SUBMETER), IconFactory.getIconBig(GUIParameters.ICON_SUBMETER), "Submeter Declara\u00e7\u00e3o", KeyStroke.getKeyStroke(120, 0));
        this.initSubmitDialogs();
    }

    protected abstract void initSubmitDialogs();

    protected abstract TaxClientRevampedSubmitDialog getSubmitDialog();

    public abstract String getSubmitUrl();

    public abstract String getAppVersion();

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            super.actionPerformed(e);
            if (Session.getDeclaracaoValidator().getDeclarationValidationResult() != null && Session.getDeclaracaoValidator().getDeclarationValidationResult().hasErrors()) {
                Session.getDeclaracaoValidator().setDeclarationValidationResult(new ValidationResult());
            }
            Session.getDeclaracaoValidator().validate(Session.getCurrentDeclaracao().getDeclaracaoModel());
            ValidationResult validationResult = Session.getDeclaracaoValidator().getDeclarationValidationResult();
            if (validationResult.hasErrors()) {
                this.handleDeclaracaoValidationErrors(validationResult);
                return;
            }
            this.submit(true);
        }
        catch (Exception ex) {
            Logger.getDefault().error("Erro na submiss\u00e3o: " + ex.getCause());
            throw new RuntimeException("Ocorreu um erro grave na submiss\u00e3o da declara\u00e7\u00e3o. ", ex);
        }
    }

    protected void handleDeclaracaoValidationErrors(ValidationResult validationResult) {
        DialogFactory.instance().showWarningDialog("<html><b>A declara\u00e7\u00e3o apresenta erros de preenchimento.</b><br>Por favor, corrija a declara\u00e7\u00e3o antes de proceder \u00e0 submiss\u00e3o.</html>");
        MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
        ValidationDisplayer validationDisplayer = multiPurposeDisplayer.getValidationDisplayer();
        validationDisplayer.fillErrorsList(validationResult);
        multiPurposeDisplayer.showValidationDisplayer();
    }

    protected void submit(boolean firstTime) throws Exception {
        try {
            this.getSubmitDialog().getFirstTimeDialog(firstTime);
        }
        catch (InterruptedException ex) {
            return;
        }
        if (!DialogFactory.instance().showYesNoDialog(SubmitTaxUtils.CONFIRM_SUBMISSION_MESSAGE)) {
            return;
        }
        this.submitTax(true);
    }

    protected abstract void submitTax(boolean var1) throws Exception;

    protected abstract TaxClientRevampedSubmitInfo initSubmitInfo() throws Exception;

    protected abstract void showResult(SubmitTaxThread var1);
}

