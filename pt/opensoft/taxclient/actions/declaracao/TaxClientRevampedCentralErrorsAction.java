/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.declaracao;

import com.jgoodies.validation.ValidationResult;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.CentralErrorsDisplayer;
import pt.opensoft.taxclient.util.ServerValidationResult;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedCentralErrorsAction
extends EnhancedAction {
    private static final long serialVersionUID = 3354855389635927691L;

    public TaxClientRevampedCentralErrorsAction() {
        super("Erros Centrais", IconFactory.getIconSmall(GUIParameters.ICON_CENTRAL_ERRORS), IconFactory.getIconBig(GUIParameters.ICON_CENTRAL_ERRORS), "Erros detetados na \u00faltima submiss\u00e3o", KeyStroke.getKeyStroke(117, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DeclaracaoModel model = Session.getCurrentDeclaracao().getDeclaracaoModel();
        MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
        CentralErrorsDisplayer centralErrorsDisplayer = multiPurposeDisplayer.getCentralErrorsDisplayer();
        centralErrorsDisplayer.fillErrorsList(ServerValidationResult.CENTRAL_ERRORS.getValidationResult());
        multiPurposeDisplayer.showCentralErrorsDisplayer();
    }
}

