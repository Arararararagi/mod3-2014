/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.declaracao;

import com.jgoodies.validation.ValidationResult;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedValidateAction
extends EnhancedAction {
    private static final long serialVersionUID = 3354855389635927691L;

    public TaxClientRevampedValidateAction() {
        super("Validar", IconFactory.getIconSmall(GUIParameters.ICON_VALIDAR), IconFactory.getIconBig(GUIParameters.ICON_VALIDAR), "Validar Declara\u00e7\u00e3o", KeyStroke.getKeyStroke(116, 0));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DeclaracaoModel model = Session.getCurrentDeclaracao().getDeclaracaoModel();
        ValidationResult result = Session.getDeclaracaoValidator().validate(model);
        if (result != null) {
            MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
            ValidationDisplayer validationDisplayer = multiPurposeDisplayer.getValidationDisplayer();
            validationDisplayer.fillErrorsList(result);
            multiPurposeDisplayer.showValidationDisplayer();
        }
    }
}

