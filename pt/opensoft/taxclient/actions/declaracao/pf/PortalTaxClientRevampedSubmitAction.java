/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  netscape.javascript.JSObject
 */
package pt.opensoft.taxclient.actions.declaracao.pf;

import com.jgoodies.validation.ValidationResult;
import java.applet.Applet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import netscape.javascript.JSObject;
import pt.opensoft.http.ConnectionTimeoutException;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.TaxClientRevampedApplet;
import pt.opensoft.taxclient.actions.declaracao.TaxClientRevampedSubmitAction;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedDadosToSubmit;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitDialog;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitDialog;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.gui.PortalTaxClientRevampedSubmitAuxDialogs;
import pt.opensoft.taxclient.gui.SubmitDialog;
import pt.opensoft.taxclient.gui.SubmitResponse;
import pt.opensoft.taxclient.gui.SubmitTaxThread;
import pt.opensoft.taxclient.gui.SubmitTaxThreadHttpConnection;
import pt.opensoft.taxclient.http.SubmitReceipt;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.SubmitTaxUtils;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.StringUtil;

public class PortalTaxClientRevampedSubmitAction
extends TaxClientRevampedSubmitAction {
    private static final long serialVersionUID = 3766498818362460567L;
    private static final int PROGRESS_REFRESH_TIME = 500;
    private static final int LOGIN_INVALIDO = 1;
    private static final int LOGIN_INVALIDO_PF = 98;
    private static final int VALIDACAO_IMPOSSIVEL = 2;
    private static final int ENTREGA_IMPOSSIVEL = 3;
    protected static final String DEFAULT_WARNING_MESSAGE = "<html>A valida\u00e7\u00e3o do pedido produziu mensagens.<br><br>Deseja continuar a submiss\u00e3o?</html>";
    protected static final int FECHAR_DIALOG = 0;
    private String url;
    private String appletUrl;
    private String appVersion;
    private PortalTaxClientRevampedSubmitDialog submitDialogs;
    private PortalTaxClientRevampedSubmitInfo submitInfo;
    private String warningMessage;
    private Timer timer;

    public PortalTaxClientRevampedSubmitAction(String url) {
        this(url, null, "<html>A valida\u00e7\u00e3o do pedido produziu mensagens.<br><br>Deseja continuar a submiss\u00e3o?</html>", null);
    }

    public PortalTaxClientRevampedSubmitAction(String url, String appletUrl) {
        this(url, appletUrl, "<html>A valida\u00e7\u00e3o do pedido produziu mensagens.<br><br>Deseja continuar a submiss\u00e3o?</html>", null);
    }

    public PortalTaxClientRevampedSubmitAction(String url, String appletUrl, String warningMessage) {
        this(url, appletUrl, warningMessage, null);
    }

    public PortalTaxClientRevampedSubmitAction(String url, String appletUrl, String warningMessage, String appVersion) {
        this.url = url;
        this.appletUrl = appletUrl;
        this.warningMessage = StringUtil.isEmpty(warningMessage) ? "<html>A valida\u00e7\u00e3o do pedido produziu mensagens.<br><br>Deseja continuar a submiss\u00e3o?</html>" : warningMessage;
        this.appVersion = appVersion;
    }

    @Override
    public String getAppVersion() {
        return this.appVersion;
    }

    @Override
    protected TaxClientRevampedSubmitDialog getSubmitDialog() {
        return this.submitDialogs;
    }

    @Override
    public String getSubmitUrl() {
        return this.url;
    }

    public String getAppletUrl() {
        return this.appletUrl;
    }

    @Override
    protected void initSubmitDialogs() {
        this.submitDialogs = new PortalTaxClientRevampedSubmitDialog();
    }

    public void setSubmitDialogs(PortalTaxClientRevampedSubmitDialog dialogs) {
        this.submitDialogs = dialogs;
    }

    @Override
    protected TaxClientRevampedSubmitInfo initSubmitInfo() throws Exception {
        this.submitInfo = new PortalTaxClientRevampedSubmitInfo(this.submitDialogs.getSubmitUser(), new TaxClientRevampedDadosToSubmit(Session.getCurrentDeclaracao().getDeclaracaoModel()));
        return this.submitInfo;
    }

    protected int showReceiptDialog(SubmitResponse submitResponse) {
        JPanel receiptPanel = this.submitDialogs.constructReceiptPanel(submitResponse);
        int option = JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), receiptPanel, "Dados da Declara\u00e7\u00e3o", -1, 1, null, new Object[]{"Fechar"}, "Fechar");
        return option;
    }

    protected Object[] getRedirectParameters(SubmitResponse submitResponse) {
        return new Object[0];
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    protected void showResult(SubmitTaxThread submitTaxThread) {
        if (submitTaxThread.isSucessfull()) {
            SubmitResponse submitResponse = ((SubmitTaxThreadHttpConnection)submitTaxThread).getSubmitResponse();
            int errorCode = submitResponse.getErrorCode();
            switch (errorCode) {
                case 0: {
                    this.handleResponseCodeOK(submitResponse);
                    return;
                }
                case 1: {
                    JPanel panelLoginInvalid = SubmitTaxUtils.constructErrorsPanel("Os dados de utilizador submetidos s\u00e3o inv\u00e1lidos.", submitResponse.getErrors());
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelLoginInvalid, "Falha na Submiss\u00e3o", 0);
                    return;
                }
                case 2: {
                    JPanel panelValImposs = SubmitTaxUtils.constructErrorsPanel("N\u00e3o foi poss\u00edvel validar os dados de Login.", submitResponse.getErrors());
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelValImposs, "Falha na Submiss\u00e3o", 0);
                    return;
                }
                case 3: {
                    JPanel panelEntImposs = SubmitTaxUtils.constructErrorsPanel("N\u00e3o foi poss\u00edvel substituir o formul\u00e1rio, o formul\u00e1rio anterior encontra-se no estado verifica\u00e7\u00e3o pedida.", submitResponse.getErrors());
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelEntImposs, "Falha na Submiss\u00e3o", 0);
                    return;
                }
                case 99: {
                    if (!(submitResponse.getValidationResult() == null || submitResponse.getValidationResult().isEmpty())) {
                        if (!submitResponse.getValidationResult().hasErrors()) return;
                        MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
                        ValidationDisplayer validationDisplayer = multiPurposeDisplayer.getValidationDisplayer();
                        validationDisplayer.fillErrorsList(submitResponse.getValidationResult());
                        multiPurposeDisplayer.showValidationDisplayer();
                        return;
                    }
                    if (submitResponse.getErrors() == null || submitResponse.getErrors().isEmpty()) throw new IllegalStateException("Code is NOK, but list of Errors are null");
                    SubmitDialog dialog = PortalTaxClientRevampedSubmitAuxDialogs.getErrorSubmitDialog(JOptionPane.getFrameForComponent(DialogFactory.getDummyFrame()), submitResponse.getErrors());
                    dialog.setVisible(true);
                    return;
                }
                case 98: {
                    JPanel panelUnauthorized = SubmitTaxUtils.constructErrorsPanel("Os dados de utilizador submetidos s\u00e3o inv\u00e1lidos.", submitResponse.getErrors());
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelUnauthorized, "Falha na Submiss\u00e3o", 0);
                    try {
                        this.submit(false);
                        return;
                    }
                    catch (Exception e) {
                        throw new IllegalStateException(e.getMessage());
                    }
                }
                case 95: {
                    this.handleResponseWithWarnings(submitResponse);
                    return;
                }
                case 96: {
                    JPanel panelInvalid = SubmitTaxUtils.constructErrorsPanel("Os dados submetidos s\u00e3o inv\u00e1lidos.", submitResponse.getErrors());
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelInvalid, "Falha na Submiss\u00e3o", 0);
                    return;
                }
                case 94: {
                    JPanel panelOutdated = SubmitTaxUtils.constructErrorsPanel("N\u00e3o foi poss\u00edvel submeter a sua declara\u00e7\u00e3o", submitResponse.getErrors());
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelOutdated, "Falha na Submiss\u00e3o", 0);
                    return;
                }
                case 97: {
                    JPanel panelException = SubmitTaxUtils.constructErrorsPanel("Sistema central indispon\u00edvel. Por favor tente mais tarde.", submitResponse.getErrors());
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelException, "Falha na Submiss\u00e3o", 0);
                    return;
                }
                case 93: {
                    List<String> informations = submitResponse.getInformations();
                    JPanel panelInfo = SubmitTaxUtils.constructInformationsPanel("Informa\u00e7\u00f5es de Submiss\u00e3o", informations);
                    JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelInfo, "Informa\u00e7\u00f5es da Submiss\u00e3o", 1);
                    return;
                }
            }
            this.handleUnknownCode(submitResponse);
            return;
        }
        if (submitTaxThread.getException() instanceof MalformedURLException) {
            throw new NullPointerException("Url Inv\u00e1lido [" + submitTaxThread.getException().toString() + "]");
        }
        String message = null;
        message = submitTaxThread.getException() instanceof ConnectionTimeoutException ? SubmitTaxUtils.CONNECTION_TIMEOUT_MESSAGE : (submitTaxThread.getException() instanceof IOException ? SubmitTaxUtils.IO_EXCEPTION_MESSAGE : SubmitTaxUtils.EXCEPTION_MESSAGE);
        DialogFactory.instance().showErrorDialog(message);
    }

    protected void handleResponseCodeOK(SubmitResponse submitResponse) {
        if (submitResponse.getSubmitReceipt() != null) {
            boolean close = false;
            while (!close) {
                int option = this.showReceiptDialog(submitResponse);
                close = option == 0;
                if (!close || !Session.isApplet() || StringUtil.isEmpty(TaxclientParameters.getRedirectAfterSubmission())) continue;
                TaxClientRevampedApplet applet = Session.getApplet();
                JSObject.getWindow((Applet)applet).call(TaxclientParameters.getRedirectAfterSubmission(), this.getRedirectParameters(submitResponse));
            }
        } else {
            throw new IllegalArgumentException("SubmitResponse invalido: Code - OK; Receipt - Null");
        }
    }

    protected void handleUnknownCode(SubmitResponse submitResponse) {
        JPanel panelDefaultException = SubmitTaxUtils.constructErrorsPanel("C\u00f3digo de Resposta desconhecido. Por favor tente mais tarde.", submitResponse.getErrors());
        JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), panelDefaultException, "Falha na Submiss\u00e3o", 0);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    protected void handleResponseWithWarnings(SubmitResponse submitResponse) {
        if (!(submitResponse.getValidationResult() == null || submitResponse.getValidationResult().isEmpty())) {
            if (!submitResponse.getValidationResult().hasMessages()) return;
            if (DialogFactory.instance().showYesNoDialog(this.warningMessage, new String("Sim"), new String("Mais informa\u00e7\u00f5es"))) {
                try {
                    this.submitTax(false);
                    return;
                }
                catch (Exception e) {
                    throw new IllegalStateException(e.toString());
                }
            }
            MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
            WarningDisplayer warningDisplayer = multiPurposeDisplayer.getWarningDisplayer();
            warningDisplayer.fillWarningsList(submitResponse.getValidationResult());
            multiPurposeDisplayer.showWarningDisplayer();
            return;
        }
        if (submitResponse.getWarnings() == null || submitResponse.getWarnings().isEmpty()) throw new IllegalStateException("Code is NOK, but list of Warnings are null");
        SubmitDialog dialog = PortalTaxClientRevampedSubmitAuxDialogs.getWarningSubmitDialog(JOptionPane.getFrameForComponent(DialogFactory.getDummyFrame()), submitResponse.getWarnings());
        dialog.setVisible(true);
        if (dialog.getActionPerfomed() != 2) return;
        try {
            this.submitTax(false);
            return;
        }
        catch (Exception e) {
            throw new IllegalStateException(e.toString());
        }
    }

    @Override
    protected void submitTax(boolean checkWarnings) throws Exception {
        String url = Session.isApplet() && this.getAppletUrl() != null ? this.getAppletUrl() : this.getSubmitUrl();
        PortalTaxClientRevampedSubmitInfo tcrsi = (PortalTaxClientRevampedSubmitInfo)this.initSubmitInfo();
        if (Session.isApplet()) {
            tcrsi.addOptionalField("isApplet", "true");
        }
        tcrsi.setAppVersion(this.getAppVersion());
        tcrsi.setCheckWarnings(checkWarnings);
        tcrsi.addOptionalField("javaVersion", System.getProperty("java.version"));
        final JDialog dialog = SubmitTaxUtils.submitDialog(this.statusCommunication_L);
        dialog.setVisible(true);
        DialogFactory.instance().showWaitCursor();
        final SubmitTaxThreadHttpConnection submitTaxThread = new SubmitTaxThreadHttpConnection(url, tcrsi);
        submitTaxThread.start();
        this.timer = new Timer(500, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (submitTaxThread.isCompleted()) {
                    PortalTaxClientRevampedSubmitAction.this.setEnabled(true);
                    DialogFactory.instance().dismissWaitCursor();
                    PortalTaxClientRevampedSubmitAction.this.timer.stop();
                    dialog.setVisible(false);
                    PortalTaxClientRevampedSubmitAction.this.showResult(submitTaxThread);
                } else {
                    PortalTaxClientRevampedSubmitAction.this.statusCommunication_L.setText(submitTaxThread.getStatusDescr());
                }
            }
        });
        this.timer.start();
        this.setEnabled(false);
    }

}

