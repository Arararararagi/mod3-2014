/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.help;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedHelpTemasAction
extends EnhancedAction {
    private static final long serialVersionUID = 3354855389635927691L;

    public TaxClientRevampedHelpTemasAction() {
        super("Ajuda", IconFactory.getIconSmall(GUIParameters.ICON_AJUDA_TEMAS), IconFactory.getIconBig(GUIParameters.ICON_AJUDA_TEMAS), "Ajuda por temas", KeyStroke.getKeyStroke(113, 0));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
            HelpDisplayer helpDisplayer = multiPurposeDisplayer.getHelpDisplayer();
            multiPurposeDisplayer.showHelpDisplayer();
            helpDisplayer.updateHelp();
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
    }
}

