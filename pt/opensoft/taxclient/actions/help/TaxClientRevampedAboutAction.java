/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.help;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.help.AboutInterface;
import pt.opensoft.taxclient.ui.help.AboutPanel;
import pt.opensoft.taxclient.util.Session;

public abstract class TaxClientRevampedAboutAction
extends EnhancedAction
implements AboutInterface {
    private static final long serialVersionUID = -1020720947414189289L;
    protected String applicationName;

    protected TaxClientRevampedAboutAction(String applicationName) {
        this(applicationName, IconFactory.getIconSmall(GUIParameters.ICON_AJUDA_CAMPOS), IconFactory.getIconBig(GUIParameters.ICON_AJUDA_CAMPOS));
    }

    protected TaxClientRevampedAboutAction(String applicationName, Icon iconSmall, Icon iconBig) {
        super("Acerca de ...", iconSmall, iconBig, "Acerca de ...", null);
        this.applicationName = applicationName;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JDialog dialog = this.createDialog(Session.getMainFrame(), "Acerca de ...", new AboutPanel(this.getAppVersion(), this.getAppVersionDate(), this.getAppName()));
        dialog.setVisible(true);
    }

    @Override
    public JDialog createDialog(Component parentComponent, String title, JPanel About) {
        JFrame frame = new JFrame();
        frame.setSize(400, 800);
        frame.pack();
        final JDialog dialog = new JDialog(frame, title, true);
        Container contentPane = dialog.getContentPane();
        dialog.setResizable(false);
        contentPane.setLayout(new BorderLayout());
        contentPane.add((Component)About, "Center");
        dialog.pack();
        dialog.setLocationRelativeTo(parentComponent);
        dialog.addWindowListener(new WindowAdapter(){
            boolean gotFocus;

            @Override
            public void windowClosing(WindowEvent we) {
            }

            @Override
            public void windowActivated(WindowEvent we) {
                if (!this.gotFocus) {
                    this.gotFocus = true;
                }
            }
        });
        dialog.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent event) {
                if (dialog.isVisible()) {
                    dialog.setVisible(false);
                    dialog.dispose();
                }
            }
        });
        return dialog;
    }

    protected abstract String getAppVersion();

    protected abstract String getAppVersionDate();

    protected String getAppName() {
        return this.applicationName;
    }

}

