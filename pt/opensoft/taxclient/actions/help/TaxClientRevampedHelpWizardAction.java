/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.help;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.help.HelpWizardDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedHelpWizardAction
extends EnhancedAction {
    private static final long serialVersionUID = 3354855389635927691L;

    public TaxClientRevampedHelpWizardAction() {
        super("Facilitador", IconFactory.getIconSmall(GUIParameters.ICON_WIZARD), IconFactory.getIconBig(GUIParameters.ICON_WIZARD), "Facilitador", KeyStroke.getKeyStroke(114, 0));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            MultiPurposeDisplayer multiPurposeDisplayer = Session.getMainFrame().getMultiPurposeDisplayer();
            HelpWizardDisplayer helpWizardDisplayer = multiPurposeDisplayer.getHelpWizardDisplayer();
            multiPurposeDisplayer.showHelpWizardDisplayer();
            helpWizardDisplayer.updateHelpWizard();
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
    }
}

