/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.help;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import javax.accessibility.AccessibleContext;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.Util;
import pt.opensoft.taxclient.util.Session;

public class TaxClientRevampedHelpKeyNavigationAction
extends EnhancedAction {
    private static final long serialVersionUID = -6860462040736183952L;

    public TaxClientRevampedHelpKeyNavigationAction() {
        super("Ajuda - Teclas de navega\u00e7\u00e3o", IconFactory.getIconSmall(GUIParameters.ICON_AJUDA_CAMPOS), IconFactory.getIconBig(GUIParameters.ICON_AJUDA_CAMPOS), KeyStroke.getKeyStroke(115, 0));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Window dialog = null;
        try {
            dialog = new KeyNavigationHelpDialog(Session.getApplicationFrame(), "Ajuda - Teclas de navega\u00e7\u00e3o", true);
            dialog.setVisible(true);
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
        finally {
            if (dialog != null) {
                dialog.dispose();
            }
        }
    }

    private class KeyNavigationHelpDialog
    extends JDialog {
        private static final long serialVersionUID = -7910748480094511226L;
        private ScrollableHtmlPane scrollableHtmlPane;
        private JScrollPane scrollPane;

        public KeyNavigationHelpDialog(Frame owner, String title, boolean modal) {
            super(owner, title, modal);
            this.getContentPane().setLayout(new BorderLayout(0, 10));
            this.initializeHelpPane();
            this.pack();
            this.setLocationRelativeTo(owner);
            this.getRootPane().registerKeyboardAction(new ActionListener(TaxClientRevampedHelpKeyNavigationAction.this){
                final /* synthetic */ TaxClientRevampedHelpKeyNavigationAction val$this$0;

                @Override
                public void actionPerformed(ActionEvent e) {
                    KeyNavigationHelpDialog.this.dispose();
                }
            }, KeyStroke.getKeyStroke(27, 0), 2);
        }

        private void initializeHelpPane() {
            String helpText;
            this.scrollableHtmlPane = new ScrollableHtmlPane();
            this.scrollableHtmlPane.setEditable(false);
            this.scrollableHtmlPane.setContentType("text/html");
            StyleSheet css = new StyleSheet();
            css.importStyleSheet(Util.class.getResource("/errorsPF.css"));
            HTMLEditorKit edtKit = new HTMLEditorKit();
            edtKit.setStyleSheet(css);
            this.scrollableHtmlPane.setEditorKit(edtKit);
            this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
            this.scrollPane = new JScrollPane(this.scrollableHtmlPane);
            this.scrollPane.getVerticalScrollBar().setUnitIncrement(this.scrollPane.getVerticalScrollBar().getBlockIncrement());
            this.getContentPane().add((Component)this.scrollPane, "Center");
            String pageLocation = "/ajuda/teclasNavegacao.html";
            try {
                helpText = Util.getHtmlText(this.scrollableHtmlPane, pageLocation);
            }
            catch (IOException e) {
                helpText = "<p><i>P\u00e1gina n\u00e3o dispon\u00edvel</i></p></html>";
            }
            this.scrollableHtmlPane.setText(helpText);
            this.getAccessibleContext().setAccessibleDescription(helpText);
            this.changeFont();
        }

        private void changeFont() {
            Font currentGlobalFont = UIManager.getFont("Label.font");
            StyledDocument doc = (StyledDocument)this.scrollableHtmlPane.getDocument();
            SimpleAttributeSet attr = new SimpleAttributeSet();
            StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
            StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
            doc.setCharacterAttributes(0, doc.getLength(), attr, false);
        }

    }

}

