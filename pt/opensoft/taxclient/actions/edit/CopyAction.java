/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.edit;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.SimpleTextAction;

public class CopyAction
extends EnhancedAction {
    private SimpleTextAction _textAction = new SimpleTextAction("Copiar");

    public CopyAction() {
        super("Copiar", IconFactory.getIconSmall(GUIParameters.ICON_COPY), IconFactory.getIconSmall(GUIParameters.ICON_COPY), KeyStroke.getKeyStroke(67, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent textComp = this._textAction.getFocused();
        if (textComp != null) {
            textComp.copy();
            textComp.requestFocus();
        }
    }
}

