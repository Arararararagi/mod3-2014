/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.edit;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.SimpleTextAction;

public class CutAction
extends EnhancedAction {
    private SimpleTextAction _textAction = new SimpleTextAction("Cortar");

    public CutAction() {
        super("Cortar", IconFactory.getIconSmall(GUIParameters.ICON_CUT), IconFactory.getIconSmall(GUIParameters.ICON_CUT), KeyStroke.getKeyStroke(88, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent textComp = this._textAction.getFocused();
        if (textComp != null) {
            textComp.selectAll();
            textComp.cut();
            textComp.requestFocus();
        }
    }
}

