/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.edit;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.SimpleTextAction;

public class PasteAction
extends EnhancedAction {
    private SimpleTextAction _textAction = new SimpleTextAction("Colar");

    public PasteAction() {
        super("Colar", IconFactory.getIconSmall(GUIParameters.ICON_PASTE), IconFactory.getIconSmall(GUIParameters.ICON_PASTE), KeyStroke.getKeyStroke(86, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this._textAction.getFocused() != null) {
            this._textAction.getFocused().paste();
        }
        this._textAction.getFocused().requestFocus();
    }
}

