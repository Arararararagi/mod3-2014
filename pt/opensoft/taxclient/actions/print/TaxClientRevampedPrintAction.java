/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.print;

import java.awt.event.ActionEvent;
import java.io.PrintStream;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;

public class TaxClientRevampedPrintAction
extends EnhancedAction {
    private static final long serialVersionUID = 3199363764970693053L;

    public TaxClientRevampedPrintAction() {
        super("Imprimir", IconFactory.getIconSmall(GUIParameters.ICON_IMPRIMIR), IconFactory.getIconBig(GUIParameters.ICON_IMPRIMIR), KeyStroke.getKeyStroke(80, 2));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Imprimir declara\u00e7\u00e3o - TODO");
    }
}

