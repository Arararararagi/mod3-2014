/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.usability;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.taxclient.model.DeclaracaoPresentationModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.util.Session;

public class ExibirAnexoAnteriorAction
extends EnhancedAction {
    private static final long serialVersionUID = -2921536636366426737L;

    public ExibirAnexoAnteriorAction() {
        super("Anterior anexo", (Icon)null, null, KeyStroke.getKeyStroke(38, 512));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        DeclaracaoPresentationModel currentDeclaracao = Session.getCurrentDeclaracao();
        if (currentDeclaracao == null) {
            return;
        }
        NavigationDisplayer navigationDisplayer = Session.getMainFrame().getNavigationDisplayer();
        FormKey nearestFormKey = navigationDisplayer.findPreviousAnexCircular(currentDeclaracao.getSelectedAnexo());
        if (nearestFormKey == null) {
            return;
        }
        Session.getCurrentDeclaracao().setSelectedAnexo(nearestFormKey);
    }
}

