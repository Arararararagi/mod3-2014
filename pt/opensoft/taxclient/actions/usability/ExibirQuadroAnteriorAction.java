/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.usability;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoPresentationModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.util.Session;

public class ExibirQuadroAnteriorAction
extends EnhancedAction {
    private static final long serialVersionUID = -7395007134309585706L;

    public ExibirQuadroAnteriorAction() {
        super("Quadro Anterior", (Icon)null, null, KeyStroke.getKeyStroke(37, 512));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        DeclaracaoPresentationModel currentDeclaracao = Session.getCurrentDeclaracao();
        if (currentDeclaracao == null) {
            return;
        }
        FormKey selectedAnexo = currentDeclaracao.getSelectedAnexo();
        if (selectedAnexo == null) {
            return;
        }
        AnexoModel anexo = ((DeclaracaoModel)currentDeclaracao.getBean()).getAnexo(selectedAnexo);
        if (anexo == null) {
            return;
        }
        List<QuadroModel> quadros = anexo.getQuadros();
        if (quadros == null) {
            return;
        }
        AnexoTabbedPane anexoTabbedPane = Session.getMainFrame().getDeclarationDisplayer().getAnexoTabbedPane(selectedAnexo);
        if (anexoTabbedPane == null) {
            return;
        }
        int selectedIndex = anexoTabbedPane.getSelectedIndex();
        int tabCount = anexoTabbedPane.getTabCount();
        int nextTab = selectedIndex - 1;
        if (nextTab < 0) {
            nextTab = tabCount - 1;
        }
        Session.getCurrentDeclaracao().setSelectedQuadro(quadros.get(nextTab).getClass().getSimpleName());
        anexoTabbedPane.requestFocusToAnexo();
    }
}

