/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import pt.opensoft.util.SimpleLog;

public class LookAndFeelChanger
implements ActionListener {
    protected JFrame frame;
    protected String lafClassName;
    protected boolean wasOriginallyDecoratedByOS;

    public static JMenuItem getMenuItem(JFrame frame, String lafName, String lafClassName) {
        JMenuItem result = new JMenuItem(lafName);
        result.addActionListener(new LookAndFeelChanger(frame, lafClassName));
        return result;
    }

    public LookAndFeelChanger(JFrame frame, String lafClassName) {
        this.frame = frame;
        this.lafClassName = lafClassName;
        this.wasOriginallyDecoratedByOS = frame != null && !frame.isUndecorated();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final boolean frameExists = this.frame != null;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    boolean wasDecoratedByOS = frameExists && !LookAndFeelChanger.this.frame.isUndecorated();
                    UIManager.setLookAndFeel(LookAndFeelChanger.this.lafClassName);
                    for (Window window : Window.getWindows()) {
                        SwingUtilities.updateComponentTreeUI(window);
                    }
                    boolean canBeDecoratedByLAF = UIManager.getLookAndFeel().getSupportsWindowDecorations();
                    if (canBeDecoratedByLAF == wasDecoratedByOS) {
                        boolean wasVisible;
                        boolean bl = wasVisible = frameExists && LookAndFeelChanger.this.frame.isVisible();
                        if (frameExists) {
                            LookAndFeelChanger.this.frame.setVisible(false);
                            LookAndFeelChanger.this.frame.dispose();
                            if (!canBeDecoratedByLAF || LookAndFeelChanger.this.wasOriginallyDecoratedByOS) {
                                LookAndFeelChanger.this.frame.setUndecorated(false);
                                LookAndFeelChanger.this.frame.getRootPane().setWindowDecorationStyle(0);
                            } else {
                                LookAndFeelChanger.this.frame.setUndecorated(true);
                                LookAndFeelChanger.this.frame.getRootPane().setWindowDecorationStyle(1);
                            }
                            LookAndFeelChanger.this.frame.setVisible(wasVisible);
                            wasDecoratedByOS = !LookAndFeelChanger.this.frame.isUndecorated();
                        }
                    }
                }
                catch (ClassNotFoundException cnfe) {
                    SimpleLog.log("LAF main class '" + LookAndFeelChanger.this.lafClassName + "' not found");
                }
                catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        });
    }

}

