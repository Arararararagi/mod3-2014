/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.app.ProxyParameters;

public class ConfigureProxyAction
extends EnhancedAction {
    private final JTextField proxyHost_TF = new JTextField(15);
    private final JTextField proxyPort_TF = new JTextField(8);
    private final JCheckBox defineProxy_CB = new JCheckBox();

    public ConfigureProxyAction() {
        super("Configurar Proxy HTTP...", IconFactory.getIconSmall(GUIParameters.ICON_CONFIGURAR), null, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            super.actionPerformed(e);
            this.getProxyValues();
        }
        catch (Exception excep) {
            return;
        }
    }

    private void getProxyValues() {
        boolean valid;
        JPanel panel = this.ConstructDialogPanel();
        String proxyHost = null;
        String proxyPort = null;
        do {
            valid = true;
            if (JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), panel, "Configura\u00e7\u00e3o de Proxy HTTP", -1, 3, null, new Object[]{"Aplicar", "Cancelar"}, "Aplicar") != 0) {
                throw new RuntimeException("User cancelled");
            }
            if (!this.defineProxy_CB.isSelected()) continue;
            proxyHost = this.proxyHost_TF.getText();
            if (proxyHost == null || proxyHost.trim().equals("")) {
                DialogFactory.instance().showErrorDialog("Ter\u00e1 que definir um nome para o Servidor de Proxy HTTP");
                valid = false;
            }
            if (!valid) continue;
            proxyPort = this.proxyPort_TF.getText();
            try {
                if (proxyPort == null || proxyHost.equals("")) {
                    DialogFactory.instance().showErrorDialog("Ter\u00e1 que definir um valor para o Porto do Servidor de Proxy HTTP");
                    valid = false;
                    continue;
                }
                Integer.parseInt(proxyPort);
                continue;
            }
            catch (NumberFormatException e) {
                DialogFactory.instance().showErrorDialog("Valor do porto do Servidor de Proxy HTTP inv\u00e1lido. Insira um n\u00famero inteiro v\u00e1lido!");
                valid = false;
            }
        } while (!valid);
        ProxyParameters.setProxyHost(proxyHost);
        ProxyParameters.setProxyPort(proxyPort);
        ProxyParameters.setUseProxy(this.defineProxy_CB.isSelected());
    }

    private JPanel ConstructDialogPanel() {
        JPanel panelProxyConfig = new JPanel(new GridLayout(3, 1, 2, 2));
        panelProxyConfig.add(new JLabel("Usar Proxy HTTP  "));
        this.defineProxy_CB.addActionListener(new ProxyCBActionListener());
        this.defineProxy_CB.setToolTipText("Para utilizar um Proxy HTTP nas defini\u00e7\u00f5es de liga\u00e7\u00e3o seleccionar esta caixa");
        panelProxyConfig.add(this.defineProxy_CB);
        panelProxyConfig.add(new JLabel("Servidor:"));
        this.proxyHost_TF.setToolTipText("Nome do Servidor de Proxy HTTP");
        panelProxyConfig.add(this.proxyHost_TF);
        panelProxyConfig.add(new JLabel("Porto:"));
        this.proxyPort_TF.setToolTipText("Porto do Servidor de Proxy HTTP");
        panelProxyConfig.add(this.proxyPort_TF);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ConfigureProxyAction.this.proxyHost_TF.requestFocus();
            }
        });
        return panelProxyConfig;
    }

    @Override
    public boolean showInToolbar() {
        return false;
    }

    private class ProxyCBActionListener
    implements ActionListener {
        public ProxyCBActionListener() {
            this.initProxySettings();
        }

        public void initProxySettings() {
            ConfigureProxyAction.this.defineProxy_CB.setSelected(ProxyParameters.getUseProxy());
            this.changeTextFieldsState(ProxyParameters.getUseProxy());
            if (ProxyParameters.isProxySet()) {
                this.setTextFields(ProxyParameters.getProxyHost(), ProxyParameters.getProxyPort());
            } else {
                this.setTextFields("", "");
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ProxyParameters.setUseProxy(ConfigureProxyAction.this.defineProxy_CB.isSelected());
            this.changeTextFieldsState(ConfigureProxyAction.this.defineProxy_CB.isSelected());
        }

        private void changeTextFieldsState(boolean state) {
            ConfigureProxyAction.this.proxyHost_TF.setEditable(state);
            ConfigureProxyAction.this.proxyPort_TF.setEditable(state);
        }

        private void setTextFields(String proxyHost, String proxyPort) {
            ConfigureProxyAction.this.proxyHost_TF.setText(proxyHost);
            ConfigureProxyAction.this.proxyPort_TF.setText(proxyPort);
        }
    }

}

