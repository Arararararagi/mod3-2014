/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.remove.impl;

import pt.opensoft.taxclient.actions.structural.add.util.InvalidStructuralActionReasons;
import pt.opensoft.taxclient.actions.structural.remove.RemoveAnexoAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class DefaultRemoveAnexoAction
extends RemoveAnexoAction {
    public DefaultRemoveAnexoAction(Class<? extends AnexoModel> clazz, String subId, DeclaracaoModel model) {
        super(clazz, subId, model);
    }

    @Override
    protected InvalidStructuralActionReasons validateRemovePossibility() {
        return null;
    }
}

