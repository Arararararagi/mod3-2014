/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.remove;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.Icon;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.event.MapDataEvent;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.TaxClientRevampedInitializer;
import pt.opensoft.taxclient.actions.structural.add.util.InvalidStructuralActionReasons;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.StringUtil;

public abstract class RemoveAnexoAction
extends EnhancedAction
implements MapDataListener,
PropertyChangeListener {
    private final Class<? extends AnexoModel> anexoModelClass;
    private final String actionSubId;
    private final String actionId;
    private final String designation;
    private final DeclaracaoModel model;
    private final int min;
    private final int max;
    private boolean isClearing = false;
    private boolean isReading = false;

    protected RemoveAnexoAction(Class<? extends AnexoModel> clazz, String subId, DeclaracaoModel model) {
        super(RemoveAnexoAction.getActionDesignation(clazz.getSimpleName(), subId), null);
        if (model == null) {
            throw new IllegalArgumentException("O modelo n\u00e3o pode ser null. Verificar o m\u00e9todo de cria\u00e7\u00e3o do modelo no " + TaxClientRevampedInitializer.class.getSimpleName());
        }
        this.anexoModelClass = clazz;
        this.actionId = this.anexoModelClass.getSimpleName().substring(0, this.anexoModelClass.getSimpleName().indexOf("Model"));
        this.actionSubId = subId;
        this.designation = RemoveAnexoAction.getActionDesignation(clazz.getSimpleName(), subId);
        this.min = ((Min)clazz.getAnnotation(Min.class)).value();
        this.max = ((Max)clazz.getAnnotation(Max.class)).value();
        this.model = model;
        if (model.anexoExistsInModel(this.anexoModelClass, subId) && model.countAnexosByType(this.actionId) > this.min) {
            this.setEnabled(true);
        } else {
            this.setEnabled(false);
        }
        model.addDeclaracaoMapModelListener(this);
        DeclarationReader.addReadingListener(this);
    }

    private static String getActionDesignation(String className, String subId) {
        String key = className.substring(0, className.indexOf("Model")).toLowerCase() + ".remove.action.designation";
        String value = DesignationManager.getInstance().getString(key);
        if (value == null) {
            throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
        }
        return RemoveAnexoAction.getActionDesignationAsString(value, subId);
    }

    protected static String getActionDesignationAsString(String value, String subId) {
        if (subId == null) {
            return value;
        }
        return value + " (" + subId + ")";
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        super.actionPerformed(arg0);
        if (this.allowRemove()) {
            InvalidStructuralActionReasons reasons = this.validateRemovePossibility();
            if (reasons == null || reasons.getReasons().size() == 0) {
                this.preRemoveAnexo(this.model);
                FormKey keyToRemove = new FormKey(this.anexoModelClass, this.getSubId());
                FormKey nearestKey = Session.getMainFrame().getNavigationDisplayer().getNearestFormKey(keyToRemove);
                this.getModel().removeAnexo(this.anexoModelClass, this.getSubId());
                this.selectNearestKey(nearestKey);
                this.postRemoveAnexo(this.model);
            } else {
                this.notifyInvalidRemoveAnexo(reasons);
            }
        }
    }

    private void selectNearestKey(FormKey nearestKey) {
        if (nearestKey == null) {
            DeclarationDisplayer declarationDisplayer = Session.getMainFrame().getDeclarationDisplayer();
            declarationDisplayer.showAnexo(null);
        }
        Session.getCurrentDeclaracao().setSelectedAnexo(nearestKey);
    }

    protected void preRemoveAnexo(DeclaracaoModel model) {
    }

    protected void postRemoveAnexo(DeclaracaoModel model) {
    }

    protected abstract InvalidStructuralActionReasons validateRemovePossibility();

    protected void notifyInvalidRemoveAnexo(InvalidStructuralActionReasons reasons) {
        StringBuffer sb = new StringBuffer();
        sb.append("<html>N\u00e3o \u00e9 poss\u00edvel remover o anexo " + this.designation + " uma vez que: <br><br>");
        Iterator<String> iterator = reasons.getReasons().iterator();
        while (iterator.hasNext()) {
            sb.append(" - " + iterator.next() + "<br><br>");
        }
        sb.append("</html>");
        DialogFactory.instance().showErrorDialog(sb.toString());
    }

    protected boolean allowRemove() {
        return DialogFactory.instance().showConfirmationDialog("Tem a certeza que quer apagar o anexo " + this.designation + "?");
    }

    private void refresh() {
        FormKey formKey = new FormKey(this.anexoModelClass, this.getSubId());
        if (this.model.anexoExistsInModel(this.anexoModelClass, this.getSubId())) {
            int numberOfAnexos = this.model.countAnexosByType(formKey.getId());
            if (numberOfAnexos > this.min) {
                this.setEnabled(true);
            } else {
                this.setEnabled(false);
            }
        }
    }

    public void unregisterListeners() {
        this.model.removeDeclaracaoMapModelListener(this);
        DeclarationReader.removeReadingListener(this);
    }

    @Override
    public void objectPut(MapDataEvent e) {
        Object key;
        String formKeyId;
        if (!this.isClearing && !this.isReading && (formKeyId = ((FormKey)(key = e.getKey())).getId()).equals(this.getId()) && this.model.anexoExistsInModel(this.anexoModelClass, this.actionSubId) && this.model.countAnexosByType(formKeyId) > this.min) {
            this.setEnabled(true);
        }
    }

    @Override
    public void objectRemoved(MapDataEvent e) {
        if (!(this.isClearing || this.isReading)) {
            Object key = e.getKey();
            String formKeyId = ((FormKey)key).getId();
            String subId = ((FormKey)key).getSubId();
            if (formKeyId.equals(this.getId())) {
                if (!StringUtil.isEmpty(subId)) {
                    if (subId.equals(this.actionSubId) || this.model.anexoExistsInModel(this.anexoModelClass, subId) && this.model.countAnexosByType(formKeyId) == this.min) {
                        this.setEnabled(false);
                    }
                } else if (this.min == 0 || this.model.anexoExistsInModel(this.anexoModelClass, subId) && this.model.countAnexosByType(formKeyId) == this.min) {
                    this.setEnabled(false);
                }
            }
        }
    }

    @Override
    public void startedClear() {
        this.isClearing = true;
    }

    @Override
    public void finishedClear() {
        this.isClearing = false;
        this.refresh();
    }

    @Override
    public void contentChanged(MapDataEvent e) {
    }

    @Override
    public void setEnabled(boolean newValue) {
        boolean isEditable = Session.isEditable().booleanValue();
        super.setEnabled(newValue && isEditable);
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals("reading")) {
            this.isReading = (Boolean)event.getNewValue();
            if (!this.isReading) {
                this.refresh();
            }
        }
    }

    public String getId() {
        return this.actionId;
    }

    public String getSubId() {
        return this.actionSubId;
    }

    public DeclaracaoModel getModel() {
        return this.model;
    }

    public int getMin() {
        return this.min;
    }

    public int getMax() {
        return this.max;
    }
}

