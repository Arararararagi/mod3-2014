/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural;

import java.util.List;
import javax.swing.JPanel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.Reflections;
import pt.opensoft.util.StringUtil;

public class BindingsManager {
    public static void quadroAdded(FormKey anexoKey) {
        AnexoModel anexo = Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(anexoKey);
        for (QuadroModel quadro : anexo.getQuadros()) {
            BindingsManager.bind(anexoKey, quadro);
        }
    }

    public static void quadroRemoved(FormKey key, AnexoModel anexo) {
        if (anexo == null) {
            return;
        }
        for (QuadroModel quadro : anexo.getQuadros()) {
            BindingsManager.unbind(key, quadro);
        }
    }

    public static void unbind(FormKey key, QuadroModel model) {
        AnexoTabbedPane anexoPanel = Session.getMainFrame().getDeclarationDisplayer().getAnexoTabbedPane(key);
        IBindablePanel panel = (IBindablePanel)anexoPanel.getPanelExtensionByModel(model.getClass().getSimpleName());
        if (panel != null) {
            panel.setModel(null, false);
        }
    }

    public static void bind(FormKey key, QuadroModel model) {
        AnexoTabbedPane anexoPanel = Session.getMainFrame().getDeclarationDisplayer().getAnexoTabbedPane(key);
        IBindablePanel panel = (IBindablePanel)anexoPanel.getPanelExtensionByModel(model.getClass().getSimpleName());
        panel.setModel(model, false);
        Reflections reflections = new Reflections();
        Reflections.MethodListReflections methods = reflections.getAllDeclaredMethods(model.getClass()).onlyPublic().withAnnotation(Type.class);
        for (int i = 0; i < methods.methods().length; ++i) {
            Method gotoMethod = methods.methods()[i];
            if (!((Type)gotoMethod.getAnnotation(Type.class)).value().equals((Object)Type.TYPE.QUADRO_REPETITIVO)) continue;
            String propertyName = StringUtil.remove(gotoMethod.getName(), "get");
            try {
                Method panelMethod = panel.getClass().getMethod("goto" + propertyName + "Action", Integer.TYPE);
                panelMethod.invoke(panel, 1);
                continue;
            }
            catch (SecurityException e) {
                e.printStackTrace();
                continue;
            }
            catch (NoSuchMethodException e) {
                e.printStackTrace();
                continue;
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
                continue;
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
                continue;
            }
            catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
}

