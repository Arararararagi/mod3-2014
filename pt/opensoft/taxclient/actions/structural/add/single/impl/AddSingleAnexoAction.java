/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.single.impl;

import pt.opensoft.taxclient.actions.structural.add.AddAnexoAction;
import pt.opensoft.taxclient.actions.structural.add.util.InvalidStructuralActionReasons;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AddSingleAnexoAction
extends AddAnexoAction {
    public AddSingleAnexoAction(Class<? extends AnexoModel> clazz, DeclaracaoModel model) {
        super(clazz, model);
        if (this.getMax() > 1) {
            throw new IllegalArgumentException("A multiplicidade do anexo " + this.getId() + " \u00e9 superior a 1. Verificar o maxOccurs do xsd e/ou o @max do modelo.");
        }
        this.setEnabled(model.countAnexosByType(this.getId()) == 0);
    }

    @Override
    protected void doAddAnexoAction(DeclaracaoModel model) {
        this.getModel().addAnexo(this.getAnexoModelClass());
    }

    @Override
    protected InvalidStructuralActionReasons validateAddPossibility() {
        return null;
    }
}

