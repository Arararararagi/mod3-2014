/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.multi;

import java.text.MessageFormat;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.actions.structural.add.AddAnexoAction;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoActionBean;
import pt.opensoft.taxclient.actions.structural.add.util.InvalidStructuralActionReasons;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class AddMultipleAnexoAction
extends AddAnexoAction {
    public AddMultipleAnexoAction(Class<? extends AnexoModel> clazz, DeclaracaoModel model) {
        super(clazz, model);
        if (this.getMax() != -1) {
            if (this.getMax() <= 1) {
                throw new IllegalArgumentException("A multiplicidade do anexo " + this.getId() + " \u00e9 igual ou inferior a 1. Verificar o maxOccurs do xsd e/ou o @max do modelo.");
            }
            int countAnexos = model.countAnexosByType(this.getId());
            if (countAnexos > this.getMax()) {
                throw new IllegalStateException("O n\u00famero de anexos " + this.getId() + " existentes no modelo \u00e9 superior ao m\u00e1ximo permitido. Verificar o n\u00famero de anexos que se est\u00e1 a adicionar no arranque da aplica\u00e7\u00e3o.");
            }
            this.setEnabled(countAnexos < this.getMax());
        } else {
            this.setEnabled(true);
        }
    }

    public abstract boolean validateSubId(DeclaracaoModel var1, String var2);

    public abstract AddMultipleAnexoActionBean obtainSubId(DeclaracaoModel var1);

    protected boolean validateUniqueness(DeclaracaoModel model, String subID) {
        if (model.getAnexo(this.getAnexoModelClass(), subID) != null) {
            DialogFactory.instance().showErrorDialog(DesignationManager.MULTIPLE_ANEXO_DUPLICATED_MESSAGE.format(new String[]{this.getId(), subID}));
            return false;
        }
        return true;
    }

    @Override
    protected void doAddAnexoAction(DeclaracaoModel model) {
        AddMultipleAnexoActionBean userOutput;
        while (!((userOutput = this.obtainSubId(this.getModel())).getType().equals((Object)AddMultipleAnexoActionBean.UserAction.CANCELLED) || this.validateSubId(this.getModel(), userOutput.getSubID()) && this.validateUniqueness(this.getModel(), userOutput.getSubID()))) {
        }
        if (!userOutput.getType().equals((Object)AddMultipleAnexoActionBean.UserAction.CANCELLED)) {
            this.getModel().addAnexo(this.getAnexoModelClass(), userOutput.getSubID());
        }
    }

    @Override
    protected InvalidStructuralActionReasons validateAddPossibility() {
        return null;
    }
}

