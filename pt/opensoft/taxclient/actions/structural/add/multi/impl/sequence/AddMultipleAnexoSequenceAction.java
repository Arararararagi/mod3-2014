/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.multi.impl.sequence;

import java.util.PriorityQueue;
import java.util.Queue;
import pt.opensoft.swing.event.MapDataEvent;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoAction;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoActionBean;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.util.StringUtil;

public abstract class AddMultipleAnexoSequenceAction
extends AddMultipleAnexoAction {
    private int counter;
    private Queue<Integer> holes = new PriorityQueue<Integer>();

    public AddMultipleAnexoSequenceAction(Class<? extends AnexoModel> clazz, DeclaracaoModel model) {
        super(clazz, model);
        this.counter = model.countAnexosByType(this.getId()) + 1;
    }

    @Override
    public void objectRemoved(MapDataEvent e) {
        super.objectRemoved(e);
        Object key = e.getKey();
        FormKey keyz = (FormKey)key;
        if (keyz.getId().equals(this.getId())) {
            String subId = ((FormKey)key).getSubId();
            this.holes.add(new Integer(subId));
        }
    }

    @Override
    public AddMultipleAnexoActionBean obtainSubId(DeclaracaoModel model) {
        AddMultipleAnexoActionBean answer = this.holes.size() > 0 ? new AddMultipleAnexoActionBean(StringUtil.padZeros(this.holes.remove().toString(), 2), AddMultipleAnexoActionBean.UserAction.OK) : new AddMultipleAnexoActionBean(StringUtil.padZeros(new Integer(this.counter++).toString(), 2), AddMultipleAnexoActionBean.UserAction.OK);
        return answer;
    }
}

