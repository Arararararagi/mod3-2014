/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.multi.impl.popup;

import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.actions.structural.add.multi.impl.popup.AddMultipleAnexoPopupAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.util.StringUtil;

public class AddAnexoPopupMenu
extends AddMultipleAnexoPopupAction {
    public AddAnexoPopupMenu(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model, question);
    }

    @Override
    public void postAddAnexo(DeclaracaoModel model) {
    }

    @Override
    public void preAddAnexo(DeclaracaoModel model) {
    }

    @Override
    public boolean validateSubId(DeclaracaoModel model, String subID) {
        if (StringUtil.isEmpty(subID)) {
            DialogFactory.instance().showErrorDialog("O campo n\u00e3o pode ser vazio.");
            return false;
        }
        if (subID.contains((CharSequence)".")) {
            DialogFactory.instance().showErrorDialog("O campo n\u00e3o pode conter o car\u00e1cter ponto (\".\").");
            return false;
        }
        if (subID.contains((CharSequence)"|")) {
            DialogFactory.instance().showErrorDialog("O campo n\u00e3o pode conter o car\u00e1cter \"|\".");
            return false;
        }
        return true;
    }
}

