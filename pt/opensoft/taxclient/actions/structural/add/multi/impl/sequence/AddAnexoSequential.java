/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.multi.impl.sequence;

import pt.opensoft.taxclient.actions.structural.add.multi.impl.sequence.AddMultipleAnexoSequenceAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public class AddAnexoSequential
extends AddMultipleAnexoSequenceAction {
    public AddAnexoSequential(Class<? extends AnexoModel> clazz, DeclaracaoModel model) {
        super(clazz, model);
    }

    @Override
    public void postAddAnexo(DeclaracaoModel model) {
    }

    @Override
    public void preAddAnexo(DeclaracaoModel model) {
    }

    @Override
    public boolean validateSubId(DeclaracaoModel model, String subID) {
        return true;
    }
}

