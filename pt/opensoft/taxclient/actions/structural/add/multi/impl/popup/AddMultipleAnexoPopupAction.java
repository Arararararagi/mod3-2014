/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.multi.impl.popup;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoAction;
import pt.opensoft.taxclient.actions.structural.add.multi.AddMultipleAnexoActionBean;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;

public abstract class AddMultipleAnexoPopupAction
extends AddMultipleAnexoAction {
    private final String question;

    public AddMultipleAnexoPopupAction(Class<? extends AnexoModel> clazz, DeclaracaoModel model, String question) {
        super(clazz, model);
        this.question = question;
    }

    @Override
    public AddMultipleAnexoActionBean obtainSubId(DeclaracaoModel model) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add((Component)new JLabel(this.question), "North");
        JTextField field = new JTextField();
        panel.add((Component)field, "Center");
        AddMultipleAnexoPopupAction.requestDeferredFocus(field);
        AddMultipleAnexoActionBean answer = JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), panel, "Novo Anexo", -1, 3, null, new Object[]{"OK", "Cancelar"}, "OK") != 0 ? new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.CANCELLED) : new AddMultipleAnexoActionBean(field.getText(), AddMultipleAnexoActionBean.UserAction.OK);
        return answer;
    }

    protected static void requestDeferredFocus(final JComponent component) {
        new Thread(new Runnable(){

            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                }
                catch (InterruptedException ie) {
                    // empty catch block
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        component.requestFocus();
                    }
                });
            }

        }).start();
    }

}

