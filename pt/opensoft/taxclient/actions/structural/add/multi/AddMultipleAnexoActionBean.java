/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.multi;

public class AddMultipleAnexoActionBean {
    private final UserAction type;
    private final String subID;

    public AddMultipleAnexoActionBean(String subID, UserAction type) {
        this.subID = subID;
        this.type = type;
    }

    public UserAction getType() {
        return this.type;
    }

    public String getSubID() {
        return this.subID;
    }

    public static enum UserAction {
        OK,
        CANCELLED;
        

        private UserAction() {
        }
    }

}

