/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.Icon;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.event.MapDataEvent;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.TaxClientRevampedInitializer;
import pt.opensoft.taxclient.actions.structural.add.util.InvalidStructuralActionReasons;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.model.annotations.Min;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.util.Session;

public abstract class AddAnexoAction
extends EnhancedAction
implements MapDataListener,
PropertyChangeListener {
    private final Class<? extends AnexoModel> anexoModelClass;
    private final DeclaracaoModel model;
    private final String id;
    private final int min;
    private final int max;
    private boolean isClearing = false;
    private boolean isReading = false;

    protected AddAnexoAction(Class<? extends AnexoModel> clazz, DeclaracaoModel model) {
        super(AddAnexoAction.getActionDesignation(clazz.getSimpleName()), null);
        if (model == null) {
            throw new IllegalArgumentException("O modelo n\u00e3o pode ser null. Verificar o m\u00e9todo de cria\u00e7\u00e3o do modelo no " + TaxClientRevampedInitializer.class.getSimpleName());
        }
        this.anexoModelClass = clazz;
        this.min = ((Min)clazz.getAnnotation(Min.class)).value();
        this.id = this.anexoModelClass.getSimpleName().substring(0, this.anexoModelClass.getSimpleName().indexOf("Model"));
        this.max = ((Max)clazz.getAnnotation(Max.class)).value();
        this.model = model;
        model.addDeclaracaoMapModelListener(this);
        DeclarationReader.addReadingListener(this);
    }

    protected static String getActionDesignation(String className) {
        String key = className.substring(0, className.indexOf("Model")).toLowerCase() + ".add.action.designation";
        String value = DesignationManager.getInstance().getString(key);
        if (value == null) {
            throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
        }
        return value;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        InvalidStructuralActionReasons reasons = this.validateAddPossibility();
        if (reasons == null || reasons.getReasons().size() == 0) {
            this.preAddAnexo(this.model);
            this.doAddAnexoAction(this.model);
            this.postAddAnexo(this.model);
        } else {
            this.notifyInvalidAddAnexo(reasons);
        }
    }

    protected void notifyInvalidAddAnexo(InvalidStructuralActionReasons reasons) {
        StringBuffer sb = new StringBuffer();
        sb.append("<html>N\u00e3o \u00e9 poss\u00edvel adicionar o anexo " + AddAnexoAction.getActionDesignation(this.getAnexoModelClass().getSimpleName()) + " uma vez que: <br><br>");
        Iterator<String> iterator = reasons.getReasons().iterator();
        while (iterator.hasNext()) {
            sb.append(" - " + iterator.next() + "<br><br>");
        }
        sb.append("</html>");
        DialogFactory.instance().showErrorDialog(sb.toString());
    }

    private void refresh() {
        if (this.getMax() == -1) {
            this.setEnabled(true);
        } else {
            int numberOfAnexos = this.model.countAnexosByType(this.id);
            this.setEnabled(numberOfAnexos < this.max);
        }
    }

    public void unregisterListeners() {
        this.model.removeDeclaracaoMapModelListener(this);
        DeclarationReader.removeReadingListener(this);
    }

    protected abstract void doAddAnexoAction(DeclaracaoModel var1);

    protected abstract InvalidStructuralActionReasons validateAddPossibility();

    protected void preAddAnexo(DeclaracaoModel model) {
    }

    protected void postAddAnexo(DeclaracaoModel model) {
    }

    @Override
    public void setEnabled(boolean newValue) {
        boolean isEditable = Session.isEditable().booleanValue();
        super.setEnabled(newValue && isEditable);
    }

    @Override
    public void objectPut(MapDataEvent e) {
        String eventId;
        Object key;
        if (!(this.isClearing || this.isReading || !(eventId = ((FormKey)(key = e.getKey())).getId()).equals(this.getId()) || this.getMax() == -1)) {
            this.setEnabled(this.model.countAnexosByType(this.getId()) < this.max);
        }
    }

    @Override
    public void objectRemoved(MapDataEvent e) {
        String eventId;
        Object key;
        if (!(this.isClearing || this.isReading || !(eventId = ((FormKey)(key = e.getKey())).getId()).equals(this.getId()) || this.getMax() == -1)) {
            this.setEnabled(this.model.countAnexosByType(this.getId()) < this.max);
        }
    }

    @Override
    public void startedClear() {
        this.isClearing = true;
    }

    @Override
    public void finishedClear() {
        this.isClearing = false;
        this.refresh();
    }

    @Override
    public void contentChanged(MapDataEvent e) {
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals("reading")) {
            this.isReading = (Boolean)event.getNewValue();
            if (!this.isReading) {
                this.refresh();
            }
        }
    }

    public String getId() {
        return this.id;
    }

    public Class<? extends AnexoModel> getAnexoModelClass() {
        return this.anexoModelClass;
    }

    public DeclaracaoModel getModel() {
        return this.model;
    }

    public int getMin() {
        return this.min;
    }

    public int getMax() {
        return this.max;
    }
}

