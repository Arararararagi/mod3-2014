/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.structural.add.util;

import java.util.ArrayList;
import java.util.List;

public class InvalidStructuralActionReasons {
    private List<String> errors = new ArrayList<String>();

    public void addReason(String reason) {
        this.errors.add(reason);
    }

    public List<String> getReasons() {
        return this.errors;
    }
}

