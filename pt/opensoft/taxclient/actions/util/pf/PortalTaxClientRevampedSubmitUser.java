/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util.pf;

import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitUser;

public class PortalTaxClientRevampedSubmitUser
extends TaxClientRevampedSubmitUser {
    public static final String LABEL_CONTRIBUINTE = "Contribuinte: ";
    public static final String LABEL_PWD_CONTRIBUINTE = "Password: ";
    protected String nifA;
    protected String nifB;
    protected String toc;
    protected String passwordA;
    protected String passwordB;
    protected String passwordToc;

    public PortalTaxClientRevampedSubmitUser(String nifA, String pwdA) {
        this.nifA = nifA;
        this.passwordA = pwdA;
    }

    public PortalTaxClientRevampedSubmitUser(String nifA, String passwordA, String toc, String passwordToc) {
        this.nifA = nifA;
        this.passwordA = passwordA;
        this.toc = toc;
        this.passwordToc = passwordToc;
    }

    public PortalTaxClientRevampedSubmitUser() {
    }

    public String getNifA() {
        return this.nifA;
    }

    public void setNifA(String nifA) {
        this.nifA = nifA;
    }

    public String getNifB() {
        return this.nifB;
    }

    public void setNifB(String nifB) {
        this.nifB = nifB;
    }

    public String getToc() {
        return this.toc;
    }

    public void setToc(String toc) {
        this.toc = toc;
    }

    public String getPasswordA() {
        return this.passwordA;
    }

    public void setPasswordA(String passwordA) {
        this.passwordA = passwordA;
    }

    public String getPasswordB() {
        return this.passwordB;
    }

    public void setPasswordB(String passwordB) {
        this.passwordB = passwordB;
    }

    public String getPasswordToc() {
        return this.passwordToc;
    }

    public void setPasswordToc(String passwordToc) {
        this.passwordToc = passwordToc;
    }

    @Override
    public void decodeUser() {
    }

    @Override
    public void encodeUser() {
    }
}

