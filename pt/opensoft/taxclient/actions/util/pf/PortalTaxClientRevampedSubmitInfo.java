/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util.pf;

import java.io.IOException;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedDadosToSubmit;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitUser;
import pt.opensoft.util.StringUtil;

public class PortalTaxClientRevampedSubmitInfo
extends TaxClientRevampedSubmitInfo {
    private PortalTaxClientRevampedSubmitUser submitUser;
    private TaxClientRevampedDadosToSubmit dados;

    public PortalTaxClientRevampedSubmitInfo(TaxClientRevampedSubmitUser submitUser, TaxClientRevampedDadosToSubmit dadosToSubmit) throws IOException {
        super(submitUser, dadosToSubmit);
    }

    @Override
    public void encodeInfo(TaxClientRevampedSubmitUser submitUser, TaxClientRevampedDadosToSubmit dadosToSubmit) throws IOException {
        submitUser.encodeUser();
        dadosToSubmit.encodeDados();
    }

    @Override
    public void initSubmitInfo(TaxClientRevampedSubmitUser submitUser, TaxClientRevampedDadosToSubmit dadosToSubmit) {
        this.submitUser = (PortalTaxClientRevampedSubmitUser)submitUser;
        this.dados = dadosToSubmit;
    }

    public PortalTaxClientRevampedSubmitUser getSubmitUser() {
        return this.submitUser;
    }

    public void setSubmitUser(PortalTaxClientRevampedSubmitUser submitUser) {
        this.submitUser = submitUser;
    }

    public TaxClientRevampedDadosToSubmit getDados() {
        return this.dados;
    }

    public void setDados(TaxClientRevampedDadosToSubmit dados) {
        this.dados = dados;
    }

    public boolean hasAppVersion() {
        return !StringUtil.isEmpty(this.getAppVersion());
    }
}

