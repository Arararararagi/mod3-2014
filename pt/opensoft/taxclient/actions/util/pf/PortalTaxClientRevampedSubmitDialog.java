/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util.pf;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitDialog;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.actions.util.pf.PFSubmitPanel;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.gui.SubmitResponse;
import pt.opensoft.taxclient.http.SubmitReceipt;
import pt.opensoft.taxclient.http.SubmitUser;
import pt.opensoft.taxclient.util.SubmitTaxUtils;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.StringUtil;

public class PortalTaxClientRevampedSubmitDialog
extends TaxClientRevampedSubmitDialog {
    public static final String TITLE_SUBMIT_RECEIPT = "Dados da Declara\u00e7\u00e3o";
    public static final String RECEIPT_FORMID = "  Identifica\u00e7\u00e3o da entrega:  ";
    public static final String RECEIPT_UTILIZADOR = "  Utilizador:  ";
    private static final String RECEIPT_FRASE_1 = "A declara\u00e7\u00e3o foi submetida com sucesso.";
    public static final String RECEIPT_DATE = "  Data e Hora de Recep\u00e7\u00e3o:  ";
    public static final String RECEIPT_DECLID = "  Identifica\u00e7\u00e3o da Declara\u00e7\u00e3o:  ";
    public static final String RECEIPT_YEAR = "  Ano do Exerc\u00edcio:  ";
    protected static final String TOC = "TOC";
    protected static final String SUBMIT_PANEL_TITLE = "Submiss\u00e3o de Declara\u00e7\u00e3o";
    private static final String LABEL_SUJEITO_PASSIVO = "Sujeito Passivo";
    protected static final String LABEL_TOC = "T\u00e9cnico Oficial de Contas";
    protected PortalTaxClientRevampedSubmitUser submitUser = new PortalTaxClientRevampedSubmitUser();
    protected PFSubmitPanel panel;

    public PortalTaxClientRevampedSubmitDialog() {
        this.panel = new PFSubmitPanel();
    }

    public void setPanel(PFSubmitPanel panel) {
        this.panel = panel;
    }

    public PortalTaxClientRevampedSubmitDialog(PFSubmitPanel panel) {
        this.panel = panel;
    }

    @Override
    public JPanel constructDialogPanel() {
        return this.panel;
    }

    @Override
    public void getFirstTimeDialog(boolean firstTime) throws InterruptedException {
        if (firstTime) {
            this.resetFinalFields();
        }
        PFSubmitPanel panel = (PFSubmitPanel)this.constructDialogPanel();
        boolean valid = false;
        do {
            if (JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), panel, "Submiss\u00e3o de Declara\u00e7\u00e3o", -1, 3, null, new Object[]{"Submeter", "Cancelar"}, "Submeter") != 0) {
                this.resetFinalFields();
                throw new InterruptedException("User cancelled");
            }
            valid = this.validateInputs();
            if (!valid) continue;
            this.buildSubmitUserObject(panel);
        } while (!valid);
    }

    @Override
    public TaxClientRevampedSubmitUser getSubmitUser() {
        return this.submitUser;
    }

    protected void buildSubmitUserObject(PFSubmitPanel panel) {
        this.submitUser = new PortalTaxClientRevampedSubmitUser(panel.getNifSujPass().getText(), new String(panel.getPassSujPass().getPassword()), panel.getNifTOC().getText(), new String(panel.getPassTOC().getPassword()));
    }

    @Override
    public void resetFinalFields() {
        this.panel.getNifSujPass().setText("");
        this.panel.getPassSujPass().setText("");
        this.panel.getFromTOC().setSelected(true);
        this.panel.alterarEstadoCamposTOC(this.panel.getFromTOC().isSelected());
        this.panel.getNifTOC().setText("");
        this.panel.getPassTOC().setText("");
    }

    public JPanel constructReceiptPanel(SubmitResponse response) {
        SubmitReceipt receipt = response.getSubmitReceipt();
        int numberUsers = receipt.getUsers() != null ? receipt.getUsers().size() : 0;
        JPanel panel = new JPanel(new GridLayout((StringUtil.isEmpty(receipt.getYear()) ? 2 : 3) + numberUsers, 2, 0, 10));
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        if (receipt.getUsers() != null) {
            for (SubmitUser submitUser : receipt.getUsers()) {
                panel.add(new JLabel("<html><b>&nbsp;&nbsp;" + this.getUserDescFromId(submitUser) + ": </b></html>"));
                panel.add(new JLabel(submitUser.getNif()));
            }
        }
        if (!StringUtil.isEmpty(receipt.getYear())) {
            panel.add(new JLabel("<html><b>&nbsp;&nbsp;  Ano do Exerc\u00edcio:  </b></html>"));
            panel.add(new JLabel(receipt.getYear()));
        }
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;  Identifica\u00e7\u00e3o da Declara\u00e7\u00e3o:  </b></html>"));
        panel.add(new JLabel(receipt.getDeclId()));
        panel.add(new JLabel("<html><b>&nbsp;&nbsp;  Data e Hora de Recep\u00e7\u00e3o:  </b></html>"));
        panel.add(new JLabel(receipt.getDate().format()));
        JPanel tips = new JPanel(new GridLayout(3, 1));
        tips.add(SubmitTaxUtils.htmlJLabel("A declara\u00e7\u00e3o foi submetida com sucesso.", true, 75));
        JPanel fullPanel = new JPanel(new BorderLayout(0, 20));
        fullPanel.add((Component)panel, "North");
        fullPanel.add((Component)tips, "Center");
        return fullPanel;
    }

    public String getUserDescFromId(SubmitUser submitUser) {
        if (submitUser.getId() != null && submitUser.getId().equals("TOC")) {
            return "T\u00e9cnico Oficial de Contas";
        }
        return "Sujeito Passivo";
    }

    @Override
    public boolean validateInputs() {
        return true;
    }

    public void printReceipt(SubmitResponse response) {
    }
}

