/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util.pf;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class PFSubmitPanel
extends JPanel {
    private JLabel label1;
    protected JTextField nifSujPass;
    private JLabel label2;
    protected JPasswordField passSujPass;
    private JComponent separator2;
    private JLabel label3;
    protected JCheckBox fromTOC;
    private JLabel nifTOCLabel;
    protected JTextField nifTOC;
    protected JLabel passTOCLabel;
    protected JPasswordField passTOC;

    public PFSubmitPanel() {
        this.initComponents(true);
        this.fromTOC.setEnabled(true);
    }

    public PFSubmitPanel(boolean addTOC) {
        this.initComponents(addTOC);
        this.fromTOC.setEnabled(true);
    }

    public JTextField getNifSujPass() {
        return this.nifSujPass;
    }

    public JPasswordField getPassSujPass() {
        return this.passSujPass;
    }

    public JCheckBox getFromTOC() {
        return this.fromTOC;
    }

    public JTextField getNifTOC() {
        return this.nifTOC;
    }

    public JPasswordField getPassTOC() {
        return this.passTOC;
    }

    private void fromTOCActionPerformed(ActionEvent e) {
        if (this.fromTOC.isSelected()) {
            this.alterarEstadoCamposTOC(true);
        } else {
            this.alterarEstadoCamposTOC(false);
        }
    }

    public void alterarEstadoCamposTOC(boolean estado) {
        this.nifTOC.setEditable(estado);
        this.nifTOC.setVisible(estado);
        this.passTOC.setEditable(estado);
        this.passTOC.setVisible(estado);
        this.nifTOCLabel.setVisible(estado);
        this.passTOCLabel.setVisible(estado);
        if (estado) {
            this.passSujPass.setEditable(false);
        } else {
            this.passSujPass.setEditable(true);
        }
    }

    private void initComponents(boolean addTOC) {
        DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
        this.label1 = new JLabel();
        this.nifSujPass = new JTextField();
        this.label2 = new JLabel();
        this.passSujPass = new JPasswordField();
        this.separator2 = compFactory.createSeparator("Submiss\u00e3o por TOC");
        this.label3 = new JLabel();
        this.fromTOC = new JCheckBox();
        this.nifTOCLabel = new JLabel();
        this.nifTOC = new JTextField();
        this.passTOCLabel = new JLabel();
        this.passTOC = new JPasswordField();
        CellConstraints cc = new CellConstraints();
        this.setBackground(Color.white);
        this.setLayout(new FormLayout("$rgap, $lcgap, 121dlu, $lcgap, 102dlu, $lcgap, $rgap", "$rgap, 10*($lgap, default), $lgap, $rgap"));
        this.label1.setText(this.getTextSujPass());
        this.add((Component)this.label1, cc.xy(3, 3));
        this.add((Component)this.nifSujPass, cc.xy(5, 3));
        this.label2.setText(this.getTextPassSujPass());
        this.add((Component)this.label2, cc.xy(3, 5));
        this.add((Component)this.passSujPass, cc.xy(5, 5));
        this.addSujPassB();
        if (addTOC) {
            this.add((Component)this.separator2, cc.xywh(3, 9, 3, 1));
            this.label3.setText("Declara\u00e7\u00e3o Entregue por TOC:");
            this.add((Component)this.label3, cc.xy(3, 13));
            this.fromTOC.setBackground(Color.white);
            this.fromTOC.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    PFSubmitPanel.this.fromTOCActionPerformed(e);
                }
            });
            this.add((Component)this.fromTOC, cc.xy(5, 13));
            this.nifTOCLabel.setText("NIF do TOC:");
            this.add((Component)this.nifTOCLabel, cc.xy(3, 17));
            this.add((Component)this.nifTOC, cc.xy(5, 17));
            this.passTOCLabel.setText("Senha do TOC:");
            this.add((Component)this.passTOCLabel, cc.xy(3, 19));
            this.add((Component)this.passTOC, cc.xy(5, 19));
        }
    }

    protected String getTextSujPass() {
        return "NIF do Sujeito Passivo:";
    }

    protected String getTextPassSujPass() {
        return "Senha do Sujeito Passivo:";
    }

    protected void addSujPassB() {
    }

}

