/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public class CloseBottomPanelAction
extends EnhancedAction {
    private static final long serialVersionUID = 7069012199969848271L;

    public CloseBottomPanelAction(String title) {
        this(title, IconFactory.getIconBig(GUIParameters.ICON_FECHAR_PAINEL), IconFactory.getIconBig(GUIParameters.ICON_FECHAR_PAINEL));
    }

    public CloseBottomPanelAction(String title, Icon iconSmall, Icon iconBig) {
        super(title, iconSmall, iconBig, KeyStroke.getKeyStroke(27, 0));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Session.getMainFrame().getMultiPurposeDisplayer().hideMultiPurposeDisplayer();
    }
}

