/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.util.StringUtil;

public class DefaultSavingDialog
extends JDialog {
    private String message;

    public DefaultSavingDialog() {
        this(DesignationManager.SAVING_FILE_MESSAGE);
    }

    public DefaultSavingDialog(String message) {
        if (StringUtil.isEmpty(message)) {
            message = DesignationManager.SAVING_FILE_MESSAGE;
        } else {
            this.message = message;
        }
        this.buildDialog();
    }

    private void buildDialog() {
        Dimension contentDimension = new Dimension(275, 50);
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout(5, 5));
        contentPane.setSize(contentDimension);
        contentPane.setMinimumSize(contentDimension);
        contentPane.add((Component)this.buildLabel(), "North");
        contentPane.add((Component)this.buildProgressBar(), "South");
        this.setSize(contentDimension);
        this.setMinimumSize(contentDimension);
        this.getContentPane().add((Component)contentPane, "Center");
        this.setResizable(false);
        this.setDefaultCloseOperation(0);
    }

    private JComponent buildLabel() {
        JLabel label = new JLabel(this.message);
        Box labelBox = Box.createHorizontalBox();
        labelBox.add(Box.createHorizontalGlue());
        labelBox.add(label);
        labelBox.add(Box.createHorizontalGlue());
        labelBox.setBorder(BorderFactory.createEmptyBorder(20, 5, 2, 5));
        return labelBox;
    }

    private JComponent buildProgressBar() {
        JProgressBar pb = new JProgressBar();
        Dimension pbDimension = new Dimension(175, 20);
        pb.setSize(pbDimension);
        pb.setMinimumSize(pbDimension);
        pb.setPreferredSize(pbDimension);
        pb.setMaximumSize(pbDimension);
        pb.setIndeterminate(true);
        Box pbBox = Box.createHorizontalBox();
        pbBox.add(Box.createHorizontalGlue());
        pbBox.add(pb);
        pbBox.add(Box.createHorizontalGlue());
        pbBox.setBorder(BorderFactory.createEmptyBorder(2, 5, 20, 5));
        return pbBox;
    }
}

