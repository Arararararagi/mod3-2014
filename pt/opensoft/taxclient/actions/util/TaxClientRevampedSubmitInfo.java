/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedDadosToSubmit;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitUser;
import pt.opensoft.util.Pair;

public abstract class TaxClientRevampedSubmitInfo {
    protected String appVersion;
    protected boolean checkWarnings;
    protected List<Pair> optionalFields;

    public TaxClientRevampedSubmitInfo(TaxClientRevampedSubmitUser submitUser, TaxClientRevampedDadosToSubmit dadosToSubmit) throws IOException {
        this.initSubmitInfo(submitUser, dadosToSubmit);
        this.encodeInfo(submitUser, dadosToSubmit);
        this.optionalFields = new ArrayList<Pair>();
        this.checkWarnings = true;
    }

    public abstract void initSubmitInfo(TaxClientRevampedSubmitUser var1, TaxClientRevampedDadosToSubmit var2);

    public abstract void encodeInfo(TaxClientRevampedSubmitUser var1, TaxClientRevampedDadosToSubmit var2) throws IOException;

    public TaxClientRevampedSubmitInfo getSubmitInfo() {
        return this;
    }

    public String getAppVersion() {
        return this.appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public boolean isCheckWarnings() {
        return this.checkWarnings;
    }

    public void setCheckWarnings(boolean checkWarnings) {
        this.checkWarnings = checkWarnings;
    }

    public List<Pair> getOptionalFields() {
        return this.optionalFields;
    }

    public void addOptionalField(String key, String value) {
        if (key == null || value == null) {
            throw new NullPointerException("args are null");
        }
        Pair<String, String> pair = new Pair<String, String>(key, value);
        this.optionalFields.add(pair);
    }
}

