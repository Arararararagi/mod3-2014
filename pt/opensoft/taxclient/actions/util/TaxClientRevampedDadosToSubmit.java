/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.persistence.DeclarationWriter;
import pt.opensoft.taxclient.util.WebServicesUtil;

public class TaxClientRevampedDadosToSubmit {
    private DeclaracaoModel model;
    private String dadosStr;

    public TaxClientRevampedDadosToSubmit(DeclaracaoModel model) throws Exception {
        this.setDados(model);
        this.setDadosStr();
    }

    public String encodeDados() throws IOException {
        this.dadosStr = WebServicesUtil.getZipEncodedData(this.dadosStr);
        return this.dadosStr;
    }

    public String decodedDados() throws IOException {
        return WebServicesUtil.getZipDecodedData(this.dadosStr);
    }

    public void setDados(DeclaracaoModel model) {
        this.model = model;
    }

    public DeclaracaoModel getModel() {
        return this.model;
    }

    public void setDadosStr() throws Exception {
        this.dadosStr = this.setDadosToStr();
    }

    public String getDadosStr() {
        return this.dadosStr;
    }

    public String setDadosToStr() throws Exception {
        DeclarationWriter dw = new DeclarationWriter();
        StringWriter sw = new StringWriter();
        dw.write(sw, this.getModel(), null, this.getExistingFields(), false, false);
        return sw.toString();
    }

    protected ArrayList<FieldHandlerModel> getExistingFields() throws Exception {
        return null;
    }
}

