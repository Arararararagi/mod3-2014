/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

import javax.swing.JPanel;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitUser;

public abstract class TaxClientRevampedSubmitDialog {
    public abstract void getFirstTimeDialog(boolean var1) throws InterruptedException;

    public abstract JPanel constructDialogPanel();

    public abstract TaxClientRevampedSubmitUser getSubmitUser();

    public abstract void resetFinalFields();

    public abstract boolean validateInputs();
}

