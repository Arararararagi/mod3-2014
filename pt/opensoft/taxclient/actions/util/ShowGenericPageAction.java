/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.multi.impl.DefaultMultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;

public class ShowGenericPageAction
extends EnhancedAction {
    private static final long serialVersionUID = 4467003468785011095L;
    private boolean maximize;

    public ShowGenericPageAction(String name, String iconName) {
        this(name, iconName, null, null, false);
    }

    public ShowGenericPageAction(String name, String iconName, boolean maximize) {
        this(name, iconName, null, null, maximize);
    }

    public ShowGenericPageAction(String name, String iconName, String tooltip, KeyStroke key) {
        this(name, iconName, tooltip, key, false);
    }

    public ShowGenericPageAction(String name, String iconName, String tooltip, KeyStroke key, boolean maximize) {
        super(name, IconFactory.getIconSmall(iconName), IconFactory.getIconBig(iconName), tooltip, key);
        this.maximize = maximize;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            DefaultMultiPurposeDisplayer multiPurposeDisplayer = (DefaultMultiPurposeDisplayer)Session.getMainFrame().getMultiPurposeDisplayer();
            if (multiPurposeDisplayer.isDisplayerMaximized()) {
                multiPurposeDisplayer.hideMultiPurposeDisplayer();
            } else {
                HelpDisplayer helpDisplayer = (HelpDisplayer)multiPurposeDisplayer.getGenericCard(this.getText());
                multiPurposeDisplayer.showGenericCard(this.getText());
                if (this.isMaximize()) {
                    multiPurposeDisplayer.maximizeMultiPurposeDisplayer();
                }
                helpDisplayer.updateHelp();
            }
        }
        catch (Exception e1) {
            throw new RuntimeException(e1);
        }
    }

    public boolean isMaximize() {
        return this.maximize;
    }

    public void setMaximize(boolean maximize) {
        this.maximize = maximize;
    }
}

