/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

public abstract class TaxClientRevampedSubmitUser {
    public TaxClientRevampedSubmitUser getSubmitUser() {
        return this;
    }

    public abstract void encodeUser();

    public abstract void decodeUser();
}

