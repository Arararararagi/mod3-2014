/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.actions.util;

import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import pt.opensoft.swing.BrowserControl;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.InvalidContextException;

public class OpenExternalPageAction
extends EnhancedAction {
    private static final long serialVersionUID = 1;
    private String url;

    public OpenExternalPageAction(String url, String name, Icon iconBig, String tooltip) {
        super(name, null, iconBig, tooltip, null);
        this.url = url;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        try {
            super.actionPerformed(e);
        }
        catch (InvalidContextException ex) {
            return;
        }
        BrowserControl.displayURL(this.url);
    }
}

