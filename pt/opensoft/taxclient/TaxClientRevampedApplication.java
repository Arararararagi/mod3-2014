/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient;

import java.awt.Component;
import java.awt.Container;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;
import pt.opensoft.taxclient.TaxClientRevampedInitializer;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoModelValidator;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.TaxclientParameters;

public abstract class TaxClientRevampedApplication
extends JFrame {
    private static final long serialVersionUID = 1;
    private TaxClientRevampedInitializer gui;

    public void start() throws HeadlessException {
        try {
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    try {
                        TaxClientRevampedApplication.this.preInit();
                        TaxClientRevampedApplication.this.getTaxClientRevampedGUI().initProperties();
                        TaxClientRevampedApplication.this.getTaxClientRevampedGUI().bootstrap();
                        TaxClientRevampedApplication.this.getTaxClientRevampedGUI().initLookAndFeel(TaxClientRevampedApplication.this);
                        DeclaracaoModel declaracaoModel = TaxClientRevampedApplication.this.getTaxClientRevampedGUI().createDeclaracaoModel();
                        TaxClientRevampedApplication.this.getTaxClientRevampedGUI().initDeclarationPresentationModel(declaracaoModel);
                        TaxClientRevampedApplication.this.getTaxClientRevampedGUI().registerDefaultTaxClientActions();
                        NavigationModel navigationModel = TaxClientRevampedApplication.this.getTaxClientRevampedGUI().createNavigationModel(declaracaoModel);
                        TaxClientRevampedApplication.this.initMainFrame(declaracaoModel, navigationModel);
                        Session.setApplicationFrame(TaxClientRevampedApplication.this.getMyself());
                        Session.setDeclaracaoValidator(TaxClientRevampedApplication.this.getTaxClientRevampedGUI().createDeclaracaoModelValidator());
                        TaxClientRevampedApplication.this.postInit();
                        declaracaoModel.resetDirty();
                    }
                    catch (Exception evtException) {
                        Session.getExceptionHandler().handle(evtException);
                    }
                }
            });
        }
        catch (Exception invocationException) {
            Session.getExceptionHandler().handle(invocationException);
        }
    }

    protected TaxClientRevampedApplication getMyself() {
        return this;
    }

    protected void initMainFrame(DeclaracaoModel declaracaoModel, NavigationModel navigationModel) throws UnsupportedLookAndFeelException, InterruptedException, InvocationTargetException {
        MainFrame mainFrame = this.getTaxClientRevampedGUI().createMainFrame(declaracaoModel, navigationModel);
        if (TaxclientParameters.INIT_USE_CLASSIC_MENU) {
            mainFrame.initMenuBar();
        }
        if (TaxclientParameters.INIT_USE_RIBBON_MENU) {
            mainFrame.initTopRibbon();
        }
        if (TaxclientParameters.INIT_USE_TOP_MENU) {
            mainFrame.initTopMenuToolBar();
        }
        this.setJMenuBar(mainFrame.getMenuBar());
        this.getContentPane().add(mainFrame);
        this.setTitle(this.gui.getApplicationName());
        Session.setApplicationName(this.gui.getApplicationName());
        this.getTaxClientRevampedGUI().addMainFrameToSession(mainFrame);
    }

    public void registerTaxClientRevampedGUI(TaxClientRevampedInitializer gui) {
        this.gui = gui;
    }

    public TaxClientRevampedInitializer getTaxClientRevampedGUI() {
        if (this.gui == null) {
            try {
                throw new Exception("No GUI registered. Please call registerTaxClientRevampedGUI in the preInit implemention of your application.");
            }
            catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        return this.gui;
    }

    protected abstract void preInit();

    protected abstract void postInit();

}

