/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.overwriteBehaviour;

import java.awt.Component;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.swing.text.JTextComponent;
import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorFileParser;

public class OverwriteBehaviorManager {
    private static final String FILENAME = "/overwriteBehavior.properties";
    private Map<String, OverwriteBehaviorFileParser.OverwriteBehaviorInfo> overwriteBehavior;
    private static OverwriteBehaviorManager instance = new OverwriteBehaviorManager();

    public static OverwriteBehaviorManager getInstance() {
        return instance;
    }

    public static void loadParameters() {
        InputStream paramInput = OverwriteBehaviorManager.class.getResourceAsStream("/overwriteBehavior.properties");
        if (paramInput == null) {
            OverwriteBehaviorManager.instance.overwriteBehavior = new HashMap<String, OverwriteBehaviorFileParser.OverwriteBehaviorInfo>();
        } else {
            try {
                OverwriteBehaviorManager.instance.overwriteBehavior = new OverwriteBehaviorFileParser().readFile(paramInput);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private OverwriteBehaviorManager() {
    }

    public Boolean getEditableBehavior(String fieldid) {
        OverwriteBehaviorFileParser.OverwriteBehaviorInfo overwriteBehaviorInfo = this.overwriteBehavior.get(fieldid);
        if (overwriteBehaviorInfo == null) {
            return null;
        }
        return overwriteBehaviorInfo.getEditableUI();
    }

    public Boolean getEnableBehavior(String fieldid) {
        OverwriteBehaviorFileParser.OverwriteBehaviorInfo overwriteBehaviorInfo = this.overwriteBehavior.get(fieldid);
        if (overwriteBehaviorInfo == null) {
            return null;
        }
        return overwriteBehaviorInfo.getEnableUI();
    }

    public Boolean getVisibleBehavior(String fieldid) {
        OverwriteBehaviorFileParser.OverwriteBehaviorInfo overwriteBehaviorInfo = this.overwriteBehavior.get(fieldid);
        if (overwriteBehaviorInfo == null) {
            return null;
        }
        return overwriteBehaviorInfo.getVisibleUI();
    }

    public Boolean getFormulaBehavior(String fieldid) {
        OverwriteBehaviorFileParser.OverwriteBehaviorInfo overwriteBehaviorInfo = this.overwriteBehavior.get(fieldid);
        if (overwriteBehaviorInfo == null) {
            return null;
        }
        return overwriteBehaviorInfo.getFormulaUI();
    }

    public boolean isToDoFormula(String fieldId) {
        Boolean doFormula = OverwriteBehaviorManager.getInstance().getFormulaBehavior(fieldId);
        return doFormula == null || doFormula != false;
    }

    public static void applyOverwriteBehavior(Component c, String fieldId) {
        OverwriteBehaviorFileParser.OverwriteBehaviorInfo overwriteBehaviorInfo = OverwriteBehaviorManager.getInstance().overwriteBehavior.get(fieldId);
        if (overwriteBehaviorInfo == null) {
            return;
        }
        if (overwriteBehaviorInfo.getVisibleUI() != null) {
            c.setVisible(overwriteBehaviorInfo.getVisibleUI());
        }
        if (overwriteBehaviorInfo.getFocusUI() != null) {
            c.setFocusable(overwriteBehaviorInfo.getFocusUI());
        }
        if (overwriteBehaviorInfo.getEditableUI() != null && c instanceof JTextComponent) {
            ((JTextComponent)c).setEditable(overwriteBehaviorInfo.getEditableUI());
        }
        if (overwriteBehaviorInfo.getEnableUI() != null) {
            c.setEnabled(overwriteBehaviorInfo.getEnableUI());
        }
    }
}

