/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.overwriteBehaviour;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import pt.opensoft.util.StringUtil;

public class OverwriteBehaviorFileParser {
    public Map<String, OverwriteBehaviorInfo> readFile(InputStream inStream) throws IOException {
        HashMap<String, OverwriteBehaviorInfo> res = new HashMap<String, OverwriteBehaviorInfo>();
        Properties prop = new Properties();
        prop.load(inStream);
        for (Map.Entry e : prop.entrySet()) {
            String fieldId = (String)e.getKey();
            String config = (String)e.getValue();
            if (StringUtil.isEmpty(config)) continue;
            String[] split = config.split(",");
            Boolean readOnlyInfo = null;
            Boolean formulaInfo = null;
            Boolean enableInfo = null;
            Boolean visibleInfo = null;
            Boolean focusInfo = null;
            for (String s : split) {
                s = StringUtil.trim(s);
                if (readOnlyInfo == null && (readOnlyInfo = this.isFormatEditableDefined(s)) != null || formulaInfo == null && (formulaInfo = this.isFormatFormulaDefined(s)) != null || enableInfo == null && (enableInfo = this.isFormatEnableDefined(s)) != null || visibleInfo == null && (visibleInfo = this.isFormatVisibleDefined(s)) != null) continue;
                if (focusInfo == null && (focusInfo = this.isFormatFocusDefined(s)) != null) continue;
                throw new IOException("A propriedade " + fieldId + " contem um valor inv\u00e1lido: " + s);
            }
            if (formulaInfo == null && readOnlyInfo == null && visibleInfo == null && enableInfo == null && focusInfo == null) {
                throw new IOException("A configura\u00e7\u00e3o da propriedade " + fieldId + " n\u00e3o est\u00e1 preenchida.");
            }
            res.put(fieldId, new OverwriteBehaviorInfo(readOnlyInfo, formulaInfo, enableInfo, visibleInfo, focusInfo));
        }
        return res;
    }

    private Boolean isFormatEditableDefined(String text) {
        if ("editableOn".equalsIgnoreCase(text)) {
            return Boolean.TRUE;
        }
        if ("editableOff".equalsIgnoreCase(text)) {
            return Boolean.FALSE;
        }
        return null;
    }

    private Boolean isFormatFormulaDefined(String text) {
        if ("formOn".equalsIgnoreCase(text)) {
            return Boolean.TRUE;
        }
        if ("formOff".equalsIgnoreCase(text)) {
            return Boolean.FALSE;
        }
        return null;
    }

    private Boolean isFormatEnableDefined(String text) {
        if ("enableOn".equalsIgnoreCase(text)) {
            return Boolean.TRUE;
        }
        if ("enableOff".equalsIgnoreCase(text)) {
            return Boolean.FALSE;
        }
        return null;
    }

    private Boolean isFormatVisibleDefined(String text) {
        if ("visibleOn".equalsIgnoreCase(text)) {
            return Boolean.TRUE;
        }
        if ("visibleOff".equalsIgnoreCase(text)) {
            return Boolean.FALSE;
        }
        return null;
    }

    private Boolean isFormatFocusDefined(String text) {
        if ("focusOn".equalsIgnoreCase(text)) {
            return Boolean.TRUE;
        }
        if ("focusOff".equalsIgnoreCase(text)) {
            return Boolean.FALSE;
        }
        return null;
    }

    public static class OverwriteBehaviorInfo {
        private Boolean editableUI;
        private Boolean formulaUI;
        private Boolean enableUI;
        private Boolean visibleUI;
        private Boolean focusUI;

        public OverwriteBehaviorInfo(Boolean editableUI, Boolean formulaUI, Boolean enableUI, Boolean visibleUI, Boolean focusUI) {
            this.editableUI = editableUI;
            this.formulaUI = formulaUI;
            this.enableUI = enableUI;
            this.visibleUI = visibleUI;
            this.focusUI = focusUI;
        }

        public Boolean getEditableUI() {
            return this.editableUI;
        }

        public Boolean getFormulaUI() {
            return this.formulaUI;
        }

        public Boolean getEnableUI() {
            return this.enableUI;
        }

        public Boolean getVisibleUI() {
            return this.visibleUI;
        }

        public Boolean getFocusUI() {
            return this.focusUI;
        }

        public String toString() {
            return String.format("editableUI: %s, formulaUI: %s, enableUI: %s, visibleUI: %s, focusUI: %s", this.editableUI, this.formulaUI, this.enableUI, this.visibleUI, this.focusUI);
        }
    }

}

