/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.menus;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedMenuItem;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;

public class FuncoesMenu
extends JMenu {
    protected EnhancedAction validateAction;
    protected EnhancedAction simulateAction;
    protected EnhancedAction submitAction;

    public FuncoesMenu() {
        super("Fun\u00e7\u00f5es");
        this.setMnemonic('u');
        this.validateAction = ActionsTaxClientManager.getAction("Validar");
        this.simulateAction = ActionsTaxClientManager.getAction("Simular");
        this.submitAction = ActionsTaxClientManager.getAction("Submeter");
        this.addActionsToMenu();
    }

    protected void addActionsToMenu() {
        this.add(new EnhancedMenuItem(this.validateAction));
        if (this.simulateAction != null) {
            this.add(new EnhancedMenuItem(this.simulateAction));
        }
        if (this.submitAction != null) {
            this.add(new EnhancedMenuItem(this.submitAction));
        }
    }

    public EnhancedAction getValidateAction() {
        return this.validateAction;
    }

    public EnhancedAction getSimulateAction() {
        return this.simulateAction;
    }

    public EnhancedAction getSubmitAction() {
        return this.submitAction;
    }
}

