/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.menus;

import org.jvnet.flamingo.ribbon.JRibbon;

public interface Ribbonizable {
    public JRibbon getRibbon();
}

