/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  org.jvnet.flamingo.common.AbstractCommandButton
 *  org.jvnet.flamingo.common.JCommandButton
 *  org.jvnet.flamingo.common.JCommandButton$CommandButtonKind
 *  org.jvnet.flamingo.common.RichTooltip
 *  org.jvnet.flamingo.common.icon.ImageWrapperResizableIcon
 *  org.jvnet.flamingo.common.icon.ResizableIcon
 *  org.jvnet.flamingo.ribbon.AbstractRibbonBand
 *  org.jvnet.flamingo.ribbon.JRibbonBand
 *  org.jvnet.flamingo.ribbon.RibbonApplicationMenu
 *  org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntrySecondary
 *  org.jvnet.flamingo.ribbon.RibbonElementPriority
 *  org.jvnet.flamingo.ribbon.RibbonTask
 *  org.jvnet.flamingo.ribbon.resize.CoreRibbonResizePolicies
 *  org.jvnet.flamingo.ribbon.ui.appmenu.JRibbonApplicationMenuButton
 *  org.jvnet.flamingo.utils.FlamingoUtilities
 */
package pt.opensoft.taxclient.ui.menus;

import com.jgoodies.binding.beans.PropertyConnector;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import org.jvnet.flamingo.common.AbstractCommandButton;
import org.jvnet.flamingo.common.JCommandButton;
import org.jvnet.flamingo.common.RichTooltip;
import org.jvnet.flamingo.common.icon.ImageWrapperResizableIcon;
import org.jvnet.flamingo.common.icon.ResizableIcon;
import org.jvnet.flamingo.ribbon.AbstractRibbonBand;
import org.jvnet.flamingo.ribbon.JRibbon;
import org.jvnet.flamingo.ribbon.JRibbonBand;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenu;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntryPrimary;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntrySecondary;
import org.jvnet.flamingo.ribbon.RibbonElementPriority;
import org.jvnet.flamingo.ribbon.RibbonTask;
import org.jvnet.flamingo.ribbon.resize.CoreRibbonResizePolicies;
import org.jvnet.flamingo.ribbon.ui.appmenu.JRibbonApplicationMenuButton;
import org.jvnet.flamingo.utils.FlamingoUtilities;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.ui.icons.DocumentNewSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.DocumentOpenSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.DocumentPrintPreviewSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.DocumentPrintSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.DocumentSaveAsSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.DocumentSaveSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.EditCutSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.EditPasteSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.LogOffSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.OfficeXSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.PrinterSVGResizableIcon;
import pt.opensoft.taxclient.ui.icons.TextHTMLSVGResizableIcon;
import pt.opensoft.taxclient.util.Session;

public class MenuRibbon
extends JRibbon {
    protected RibbonTask homeTask;
    protected JRibbonBand editBand;
    protected JRibbonBand themeBand;
    protected JRibbonBand helpBand;
    protected RibbonTask declaracaoTask;
    protected JRibbonBand declaracaoBand;

    public void initDeclaracaoTask() {
        this.declaracaoBand = this.getDeclaracaoBand();
        this.declaracaoTask = new RibbonTask("Declara\u00e7\u00e3o", new AbstractRibbonBand[]{this.declaracaoBand});
        this.addTask(this.declaracaoTask);
    }

    private JRibbonBand getDeclaracaoBand() {
        JRibbonBand declaracaoBand = new JRibbonBand("Declara\u00e7\u00e3o", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_SUBMETER), (Dimension)new Dimension(32, 32)));
        declaracaoBand.setExpandButtonKeyTip("FO");
        declaracaoBand.setCollapsedStateKeyTip("ZC");
        JCommandButton mainButton = new JCommandButton("Submeter", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_SUBMETER), (Dimension)new Dimension(32, 32)));
        mainButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("Submeter"));
        declaracaoBand.addCommandButton((AbstractCommandButton)mainButton, RibbonElementPriority.TOP);
        JCommandButton copyButton = new JCommandButton("Validar", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_VALIDAR), (Dimension)new Dimension(32, 32)));
        copyButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("Validar"));
        copyButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
        copyButton.setPopupKeyTip("C");
        declaracaoBand.addCommandButton((AbstractCommandButton)copyButton, RibbonElementPriority.MEDIUM);
        JCommandButton formatButton = new JCommandButton("Simular", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_SIMULAR), (Dimension)new Dimension(32, 32)));
        formatButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
        formatButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
        formatButton.setPopupKeyTip("FP");
        declaracaoBand.addCommandButton((AbstractCommandButton)formatButton, RibbonElementPriority.MEDIUM);
        declaracaoBand.setResizePolicies(CoreRibbonResizePolicies.getCorePoliciesRestrictive((JRibbonBand)declaracaoBand));
        return declaracaoBand;
    }

    public void initHomeTask() {
        this.editBand = this.getClipboardBand();
        this.themeBand = this.getThemeBand();
        this.homeTask = new RibbonTask("Edit", new AbstractRibbonBand[]{this.editBand, this.themeBand});
        this.helpBand = this.getHelpBand();
        this.addTask(this.homeTask);
    }

    private JRibbonBand getThemeBand() {
        JRibbonBand themeBand = new JRibbonBand("Tema", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_THEME), (Dimension)new Dimension(32, 32)));
        JCommandButton mainButton = new JCommandButton("Tema", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_THEME), (Dimension)new Dimension(32, 32)));
        mainButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        RichTooltip mainRichTooltip = new RichTooltip();
        mainRichTooltip.setTitle("Tema");
        mainRichTooltip.addDescriptionSection("Altera qual o aspecto da aplica\u00e7\u00e3o");
        mainButton.setActionRichTooltip(mainRichTooltip);
        mainButton.setPopupKeyTip("T");
        RichTooltip mainPopupRichTooltip = new RichTooltip();
        mainPopupRichTooltip.setTitle("Tema");
        mainPopupRichTooltip.addDescriptionSection("Clique aqui para mais op\u00e7\u00f5es relativas \u00e0 costumiza\u00e7\u00e3o da aplica\u00e7\u00e3o.");
        mainButton.setPopupRichTooltip(mainPopupRichTooltip);
        themeBand.addCommandButton((AbstractCommandButton)mainButton, RibbonElementPriority.TOP);
        JCommandButton colourButton = new JCommandButton("C\u00f4r", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_CHANGE_COLOR), (Dimension)new Dimension(32, 32)));
        colourButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
        colourButton.setPopupKeyTip("C");
        themeBand.addCommandButton((AbstractCommandButton)colourButton, RibbonElementPriority.MEDIUM);
        return themeBand;
    }

    private JRibbonBand getClipboardBand() {
        JRibbonBand clipboardBand = new JRibbonBand("Clipboard", (ResizableIcon)new EditPasteSVGResizableIcon());
        clipboardBand.setExpandButtonKeyTip("FO");
        clipboardBand.setCollapsedStateKeyTip("ZC");
        JCommandButton mainButton = new JCommandButton("Paste", (ResizableIcon)new EditPasteSVGResizableIcon());
        mainButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("Colar"));
        mainButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        RichTooltip mainRichTooltip = new RichTooltip();
        mainRichTooltip.setTitle("Paste");
        mainRichTooltip.addDescriptionSection("Paste the contents of the Clipboard");
        mainButton.setActionRichTooltip(mainRichTooltip);
        mainButton.setPopupKeyTip("V");
        RichTooltip mainPopupRichTooltip = new RichTooltip();
        mainPopupRichTooltip.setTitle("Paste");
        mainPopupRichTooltip.addDescriptionSection("Click here for more options such as pasting only the values or formatting");
        mainButton.setPopupRichTooltip(mainPopupRichTooltip);
        clipboardBand.addCommandButton((AbstractCommandButton)mainButton, RibbonElementPriority.TOP);
        JCommandButton cutButton = new JCommandButton("Cut", (ResizableIcon)new EditCutSVGResizableIcon());
        cutButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("Cortar"));
        cutButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        RichTooltip cutRichTooltip = new RichTooltip();
        cutRichTooltip.setTitle("Cut");
        cutRichTooltip.addDescriptionSection("Cut the selection from the document and put it on the Clipboard");
        cutButton.setActionRichTooltip(cutRichTooltip);
        cutButton.setPopupKeyTip("X");
        clipboardBand.addCommandButton((AbstractCommandButton)cutButton, RibbonElementPriority.MEDIUM);
        JCommandButton copyButton = new JCommandButton("Copy", (ResizableIcon)new EditPasteSVGResizableIcon());
        copyButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("Copiar"));
        copyButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
        copyButton.setPopupKeyTip("C");
        clipboardBand.addCommandButton((AbstractCommandButton)copyButton, RibbonElementPriority.MEDIUM);
        JCommandButton formatButton = new JCommandButton("Format", (ResizableIcon)new EditPasteSVGResizableIcon());
        formatButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
        formatButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
        formatButton.setPopupKeyTip("FP");
        clipboardBand.addCommandButton((AbstractCommandButton)formatButton, RibbonElementPriority.MEDIUM);
        clipboardBand.setResizePolicies(CoreRibbonResizePolicies.getCorePoliciesRestrictive((JRibbonBand)clipboardBand));
        return clipboardBand;
    }

    public void configureApplicationMenu() {
        RibbonApplicationMenuEntryPrimary amEntryNew = new RibbonApplicationMenuEntryPrimary((ResizableIcon)new DocumentNewSVGResizableIcon(), "New", new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        }, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntryNew.setActionKeyTip("N");
        RibbonApplicationMenuEntryPrimary amEntryOpen = new RibbonApplicationMenuEntryPrimary((ResizableIcon)new DocumentOpenSVGResizableIcon(), "Open", new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        }, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntryOpen.setActionKeyTip("O");
        RibbonApplicationMenuEntryPrimary amEntrySave = new RibbonApplicationMenuEntryPrimary((ResizableIcon)new DocumentSaveSVGResizableIcon(), "Save", new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        }, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntrySave.setEnabled(false);
        amEntrySave.setActionKeyTip("S");
        RibbonApplicationMenuEntryPrimary amEntrySaveAs = new RibbonApplicationMenuEntryPrimary((ResizableIcon)new DocumentSaveAsSVGResizableIcon(), "Save As", new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        }, JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        amEntrySaveAs.setActionKeyTip("A");
        amEntrySaveAs.setPopupKeyTip("F");
        RibbonApplicationMenuEntrySecondary amEntrySaveAsWord = new RibbonApplicationMenuEntrySecondary((ResizableIcon)new OfficeXSVGResizableIcon(), "Word Document", null, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntrySaveAsWord.setDescriptionText("Save the document in the default file format");
        amEntrySaveAsWord.setActionKeyTip("W");
        RibbonApplicationMenuEntrySecondary amEntrySaveAsHtml = new RibbonApplicationMenuEntrySecondary((ResizableIcon)new TextHTMLSVGResizableIcon(), "HTML Document", null, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntrySaveAsHtml.setDescriptionText("Publish a copy of the document as an HTML file");
        amEntrySaveAsHtml.setEnabled(false);
        amEntrySaveAsHtml.setActionKeyTip("H");
        RibbonApplicationMenuEntrySecondary amEntrySaveAsOtherFormats = new RibbonApplicationMenuEntrySecondary((ResizableIcon)new DocumentSaveAsSVGResizableIcon(), "Other Formats", null, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntrySaveAsOtherFormats.setDescriptionText("Open the Save As dialog box to select from all possible file types");
        amEntrySaveAsOtherFormats.setActionKeyTip("O");
        amEntrySaveAs.addSecondaryMenuGroup("Save a copy of the document", new RibbonApplicationMenuEntrySecondary[]{amEntrySaveAsWord, amEntrySaveAsHtml, amEntrySaveAsOtherFormats});
        RibbonApplicationMenuEntryPrimary amEntryPrint = new RibbonApplicationMenuEntryPrimary((ResizableIcon)new DocumentPrintSVGResizableIcon(), "Print", new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        }, JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        amEntryPrint.setActionKeyTip("P");
        amEntryPrint.setPopupKeyTip("W");
        RibbonApplicationMenuEntrySecondary amEntryPrintSelect = new RibbonApplicationMenuEntrySecondary((ResizableIcon)new PrinterSVGResizableIcon(), "Print", null, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntryPrintSelect.setDescriptionText("Select a printer, number of copies and other printing options before printing");
        amEntryPrintSelect.setActionKeyTip("P");
        RibbonApplicationMenuEntrySecondary amEntryPrintDefault = new RibbonApplicationMenuEntrySecondary((ResizableIcon)new DocumentPrintSVGResizableIcon(), "Quick Print", null, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntryPrintDefault.setDescriptionText("Send the document directly to the default printer without making changes");
        amEntryPrintDefault.setActionKeyTip("Q");
        RibbonApplicationMenuEntrySecondary amEntryPrintPreview = new RibbonApplicationMenuEntrySecondary((ResizableIcon)new DocumentPrintPreviewSVGResizableIcon(), "Print Preview", null, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntryPrintPreview.setDescriptionText("Preview and make changes to the pages before printing");
        amEntryPrintPreview.setActionKeyTip("V");
        amEntryPrint.addSecondaryMenuGroup("Preview and print the document", new RibbonApplicationMenuEntrySecondary[]{amEntryPrintSelect, amEntryPrintDefault, amEntryPrintPreview});
        RibbonApplicationMenuEntryPrimary amEntryExit = new RibbonApplicationMenuEntryPrimary((ResizableIcon)new LogOffSVGResizableIcon(), "Exit", new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        }, JCommandButton.CommandButtonKind.ACTION_ONLY);
        amEntryExit.setActionKeyTip("X");
        RibbonApplicationMenu applicationMenu = new RibbonApplicationMenu();
        applicationMenu.addMenuEntry(amEntryNew);
        applicationMenu.addMenuEntry(amEntryOpen);
        applicationMenu.addMenuEntry(amEntrySave);
        applicationMenu.addMenuEntry(amEntrySaveAs);
        applicationMenu.addMenuEntry(amEntryPrint);
        applicationMenu.addMenuEntry(amEntryExit);
        this.setApplicationMenu(applicationMenu);
        RichTooltip appMenuRichTooltip = new RichTooltip();
        appMenuRichTooltip.setTitle("Test App Button");
        appMenuRichTooltip.addDescriptionSection("Carregue aqui para ver tudo o que pode fazer com a sua declara\u00e7\u00e3o...");
        appMenuRichTooltip.setMainImage(IconFactory.getImage(GUIParameters.ICON_APP_MENU_BUTTON));
        appMenuRichTooltip.setFooterImage(IconFactory.getImage(GUIParameters.ICON_HELP_BROWSER));
        appMenuRichTooltip.addFooterSection("Press F1 for more help");
        this.setApplicationMenuRichTooltip(appMenuRichTooltip);
        this.setApplicationMenuKeyTip("F");
        JRibbonApplicationMenuButton applicationButton = FlamingoUtilities.getApplicationMenuButton((Component)this);
        if (applicationButton != null) {
            applicationButton.setIcon((ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_LOGO), (Dimension)new Dimension(16, 16)));
        }
    }

    private JRibbonBand getHelpBand() {
        JRibbonBand helpBand = new JRibbonBand("Ajuda", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_AJUDA_CAMPOS), (Dimension)new Dimension(32, 32)));
        JCommandButton camposButton = new JCommandButton("Ajuda", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_AJUDA_CAMPOS), (Dimension)new Dimension(32, 32)));
        camposButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("AjudaPreenchimento"));
        RichTooltip camposRichTooltip = new RichTooltip();
        camposRichTooltip.addDescriptionSection("Ajuda preenchimento");
        camposButton.setActionRichTooltip(camposRichTooltip);
        helpBand.addCommandButton((AbstractCommandButton)camposButton, RibbonElementPriority.TOP);
        JCommandButton temasButton = new JCommandButton("Ajuda", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_AJUDA_TEMAS), (Dimension)new Dimension(32, 32)));
        temasButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("AjudaTemas"));
        RichTooltip temasRichTooltip = new RichTooltip();
        temasRichTooltip.addDescriptionSection("Ajuda por temas");
        temasButton.setActionRichTooltip(temasRichTooltip);
        helpBand.addCommandButton((AbstractCommandButton)temasButton, RibbonElementPriority.TOP);
        JCommandButton aboutButton = new JCommandButton("Acerca de", (ResizableIcon)ImageWrapperResizableIcon.getIcon((Image)IconFactory.getImage(GUIParameters.ICON_ACERCA), (Dimension)new Dimension(32, 32)));
        PropertyConnector.connectAndUpdate(Session.isEditable(), (Object)aboutButton, "enabled");
        aboutButton.addActionListener((ActionListener)ActionsTaxClientManager.getAction("AcercaDe"));
        RichTooltip mainRichTooltip = new RichTooltip();
        mainRichTooltip.addDescriptionSection("Acerca de");
        aboutButton.setActionRichTooltip(mainRichTooltip);
        helpBand.addCommandButton((AbstractCommandButton)aboutButton, RibbonElementPriority.TOP);
        helpBand.setResizePolicies(CoreRibbonResizePolicies.getCorePoliciesRestrictive((JRibbonBand)helpBand));
        return helpBand;
    }

}

