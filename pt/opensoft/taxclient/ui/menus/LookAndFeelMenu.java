/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.menus;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import pt.opensoft.taxclient.actions.LookAndFeelChanger;

public class LookAndFeelMenu
extends JMenu {
    public LookAndFeelMenu() {
        super("Look & feel");
        JFrame frame = null;
        JMenu substanceMenus = new JMenu("Substance family");
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Business", "org.jvnet.substance.skin.SubstanceBusinessLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Business Black Steel", "org.jvnet.substance.skin.SubstanceBusinessBlackSteelLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Business Blue Steel", "org.jvnet.substance.skin.SubstanceBusinessBlueSteelLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Creme", "org.jvnet.substance.skin.SubstanceCremeLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Creme Coffee", "org.jvnet.substance.skin.SubstanceCremeCoffeeLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Moderate", "org.jvnet.substance.skin.SubstanceModerateLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Nebula", "org.jvnet.substance.skin.SubstanceNebulaLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Nebula Brick Wall", "org.jvnet.substance.skin.SubstanceNebulaBrickWallLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Office Silver 2007", "org.jvnet.substance.skin.SubstanceOfficeSilver2007LookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Sahara", "org.jvnet.substance.skin.SubstanceSaharaLookAndFeel"));
        substanceMenus.addSeparator();
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Field of Wheat", "org.jvnet.substance.skin.SubstanceFieldOfWheatLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Finding Nemo", "org.jvnet.substance.skin.SubstanceFindingNemoLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Green Magic", "org.jvnet.substance.skin.SubstanceGreenMagicLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Mango", "org.jvnet.substance.skin.SubstanceMangoLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Office Blue 2007", "org.jvnet.substance.skin.SubstanceOfficeBlue2007LookAndFeel"));
        substanceMenus.addSeparator();
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Challenger Deep", "org.jvnet.substance.skin.SubstanceChallengerDeepLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Emerald Dusk", "org.jvnet.substance.skin.SubstanceEmeraldDuskLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Magma", "org.jvnet.substance.skin.SubstanceMagmaLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Raven", "org.jvnet.substance.skin.SubstanceRavenLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Raven Graphite", "org.jvnet.substance.skin.SubstanceRavenGraphiteLookAndFeel"));
        substanceMenus.add(LookAndFeelChanger.getMenuItem(frame, "Raven Graphite Glass", "org.jvnet.substance.skin.SubstanceRavenGraphiteGlassLookAndFeel"));
        this.add(substanceMenus);
        this.addSeparator();
        JMenu coreLafMenus = new JMenu("Core LAFs");
        this.add(coreLafMenus);
        coreLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "System", UIManager.getSystemLookAndFeelClassName()));
        coreLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Metal", "javax.swing.plaf.metal.MetalLookAndFeel"));
        coreLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Windows", "com.sun.java.swing.plaf.windows.WindowsLookAndFeel"));
        coreLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Windows Classic", "com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel"));
        coreLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Motif", "com.sun.java.swing.plaf.motif.MotifLookAndFeel"));
        coreLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Nimbus", "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"));
        JMenu customLafMenus = new JMenu("Custom LAFs");
        this.add(customLafMenus);
        JMenu jgoodiesMenu = new JMenu("JGoodies family");
        customLafMenus.add(jgoodiesMenu);
        jgoodiesMenu.add(LookAndFeelChanger.getMenuItem(frame, "JGoodies Plastic", "com.jgoodies.looks.plastic.PlasticLookAndFeel"));
        jgoodiesMenu.add(LookAndFeelChanger.getMenuItem(frame, "JGoodies PlasticXP", "com.jgoodies.looks.plastic.PlasticXPLookAndFeel"));
        jgoodiesMenu.add(LookAndFeelChanger.getMenuItem(frame, "JGoodies Plastic3D", "com.jgoodies.looks.plastic.Plastic3DLookAndFeel"));
        jgoodiesMenu.add(LookAndFeelChanger.getMenuItem(frame, "JGoodies Windows", "com.jgoodies.looks.windows.WindowsLookAndFeel"));
        JMenu jtattooMenu = new JMenu("JTattoo family");
        customLafMenus.add(jtattooMenu);
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Acryl", "com.jtattoo.plaf.acryl.AcrylLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Aero", "com.jtattoo.plaf.aero.AeroLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Aluminium", "com.jtattoo.plaf.aluminium.AluminiumLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Bernstein", "com.jtattoo.plaf.bernstein.BernsteinLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Fast", "com.jtattoo.plaf.fast.FastLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo HiFi", "com.jtattoo.plaf.hifi.HiFiLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Luna", "com.jtattoo.plaf.luna.LunaLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo McWin", "com.jtattoo.plaf.mcwin.McWinLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Mint", "com.jtattoo.plaf.mint.MintLookAndFeel"));
        jtattooMenu.add(LookAndFeelChanger.getMenuItem(frame, "JTattoo Smart", "com.jtattoo.plaf.smart.SmartLookAndFeel"));
        JMenu syntheticaMenu = new JMenu("Synthetica family");
        customLafMenus.add(syntheticaMenu);
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica base", "de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica BlackMoon", "de.javasoft.plaf.synthetica.SyntheticaBlackMoonLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica BlackStar", "de.javasoft.plaf.synthetica.SyntheticaBlackStarLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica BlueIce", "de.javasoft.plaf.synthetica.SyntheticaBlueIceLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica BlueMoon", "de.javasoft.plaf.synthetica.SyntheticaBlueMoonLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica BlueSteel", "de.javasoft.plaf.synthetica.SyntheticaBlueSteelLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica GreenDream", "de.javasoft.plaf.synthetica.SyntheticaGreenDreamLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica MauveMetallic", "de.javasoft.plaf.synthetica.SyntheticaMauveMetallicLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica OrangeMetallic", "de.javasoft.plaf.synthetica.SyntheticaOrangeMetallicLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica SkyMetallic", "de.javasoft.plaf.synthetica.SyntheticaSkyMetallicLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica SilverMoon", "de.javasoft.plaf.synthetica.SyntheticaSilverMoonLookAndFeel"));
        syntheticaMenu.add(LookAndFeelChanger.getMenuItem(frame, "Synthetica WhiteVision", "de.javasoft.plaf.synthetica.SyntheticaWhiteVisionLookAndFeel"));
        JMenu officeMenu = new JMenu("Office family");
        customLafMenus.add(officeMenu);
        officeMenu.add(LookAndFeelChanger.getMenuItem(frame, "Office 2003", "org.fife.plaf.Office2003.Office2003LookAndFeel"));
        officeMenu.add(LookAndFeelChanger.getMenuItem(frame, "Office XP", "org.fife.plaf.OfficeXP.OfficeXPLookAndFeel"));
        officeMenu.add(LookAndFeelChanger.getMenuItem(frame, "Visual Studio 2005", "org.fife.plaf.VisualStudio2005.VisualStudio2005LookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "A03", "a03.swing.plaf.A03LookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Alloy", "com.incors.plaf.alloy.AlloyLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "EaSynth", "com.easynth.lookandfeel.EaSynthLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "FH", "com.shfarr.ui.plaf.fh.FhLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Hippo", "se.diod.hippo.plaf.HippoLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "InfoNode", "net.infonode.gui.laf.InfoNodeLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Kuntstoff", "com.incors.plaf.kunststoff.KunststoffLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Liquid", "com.birosoft.liquid.LiquidLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Lipstik", "com.lipstikLF.LipstikLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Metouia", "net.sourceforge.mlf.metouia.MetouiaLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Napkin", "net.sourceforge.napkinlaf.NapkinLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Nimbus", "org.jdesktop.swingx.plaf.nimbus.NimbusLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "NimROD", "com.nilo.plaf.nimrod.NimRODLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Oyoaha", "com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Pagosoft", "com.pagosoft.plaf.PgsLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Quaqua", "ch.randelshofer.quaqua.QuaquaLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Simple", "com.memoire.slaf.SlafLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Skin", "com.l2fprod.gui.plaf.skin.SkinLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Smooth Metal", "smooth.metal.SmoothLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Squareness", "net.beeger.squareness.SquarenessLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Tiny", "de.muntjak.tinylookandfeel.TinyLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Tonic", "com.digitprop.tonic.TonicLookAndFeel"));
        customLafMenus.add(LookAndFeelChanger.getMenuItem(frame, "Trendy", "com.Trendy.swing.plaf.TrendyLookAndFeel"));
    }
}

