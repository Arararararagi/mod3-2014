/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.menus;

import java.awt.Component;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import pt.opensoft.taxclient.ui.menus.EditMenu;
import pt.opensoft.taxclient.ui.menus.FileMenu;
import pt.opensoft.taxclient.ui.menus.FuncoesMenu;
import pt.opensoft.taxclient.ui.menus.LookAndFeelMenu;

public class ClassicMenuBar
extends JMenuBar {
    protected FileMenu fileMenu;
    protected EditMenu editMenu;
    protected FuncoesMenu declaracaoMenu;
    protected LookAndFeelMenu lookAndFeelMenu;

    public void initFileMenu() {
        this.setFileMenu(new FileMenu());
    }

    public void initEditMenu() {
        this.setEditMenu(new EditMenu());
    }

    public void initDeclaracaoMenu() {
        this.setDeclaracaoMenu(new FuncoesMenu());
    }

    public void initLookAndFeelMenu() {
        this.setLookAndFeelMenu(new LookAndFeelMenu());
    }

    public FileMenu getFileMenu() {
        return this.fileMenu;
    }

    public void setFileMenu(FileMenu fileMenu) {
        if (this.fileMenu != null) {
            this.remove(this.fileMenu);
        }
        this.fileMenu = fileMenu;
        this.add(this.fileMenu);
    }

    public EditMenu getEditMenu() {
        return this.editMenu;
    }

    public void setEditMenu(EditMenu editMenu) {
        if (this.editMenu != null) {
            this.remove(this.editMenu);
        }
        this.editMenu = editMenu;
        this.add(this.editMenu);
    }

    public FuncoesMenu getDeclaracaoMenu() {
        return this.declaracaoMenu;
    }

    public void setDeclaracaoMenu(FuncoesMenu declaracaoMenu) {
        if (this.declaracaoMenu != null) {
            this.remove(this.declaracaoMenu);
        }
        this.declaracaoMenu = declaracaoMenu;
        this.add(this.declaracaoMenu);
    }

    public LookAndFeelMenu getLookAndFeelMenu() {
        return this.lookAndFeelMenu;
    }

    public void setLookAndFeelMenu(LookAndFeelMenu lookAndFeelMenu) {
        if (this.lookAndFeelMenu != null) {
            this.remove(this.lookAndFeelMenu);
        }
        this.lookAndFeelMenu = lookAndFeelMenu;
        this.add(this.lookAndFeelMenu);
    }
}

