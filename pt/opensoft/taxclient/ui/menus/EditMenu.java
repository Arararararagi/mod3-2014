/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.menus;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedMenuItem;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;

public class EditMenu
extends JMenu {
    protected EnhancedAction cutAction;
    protected EnhancedAction copyAction;
    protected EnhancedAction pasteAction;

    public EditMenu() {
        super("Editar");
        this.setMnemonic('e');
        this.cutAction = ActionsTaxClientManager.getAction("Cortar");
        this.copyAction = ActionsTaxClientManager.getAction("Copiar");
        this.pasteAction = ActionsTaxClientManager.getAction("Colar");
        this.addActionsToMenu();
    }

    protected void addActionsToMenu() {
        this.add(new EnhancedMenuItem(this.cutAction));
        this.add(new EnhancedMenuItem(this.copyAction));
        this.add(new EnhancedMenuItem(this.pasteAction));
    }
}

