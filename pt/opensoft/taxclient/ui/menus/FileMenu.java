/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.menus;

import java.awt.Component;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedMenuItem;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.util.Session;

public class FileMenu
extends JMenu {
    protected EnhancedAction newFileAction;
    protected EnhancedAction openFileAction;
    protected EnhancedAction saveFileAction;
    protected EnhancedAction saveFileAsAction;
    protected EnhancedAction configuraProxyAction;
    protected EnhancedAction imprimirAction;
    protected EnhancedAction exitAction;

    public FileMenu() {
        super("Ficheiro");
        this.setMnemonic('f');
        this.newFileAction = ActionsTaxClientManager.getAction("Novo");
        this.openFileAction = ActionsTaxClientManager.getAction("Abrir");
        this.saveFileAction = ActionsTaxClientManager.getAction("Gravar");
        this.saveFileAsAction = ActionsTaxClientManager.getAction("GravarComo");
        this.configuraProxyAction = ActionsTaxClientManager.getAction("ConfigurarProxy");
        this.imprimirAction = ActionsTaxClientManager.getAction("Imprimir");
        this.exitAction = ActionsTaxClientManager.getAction("Sair");
        this.addActionsToMenu();
    }

    protected void addActionsToMenu() {
        this.add(new EnhancedMenuItem(this.newFileAction, 'n'));
        this.add(new JSeparator());
        this.add(new EnhancedMenuItem(this.openFileAction, 'a'));
        this.add(new JSeparator());
        this.add(new EnhancedMenuItem(this.saveFileAction, 'g'));
        this.add(new EnhancedMenuItem(this.saveFileAsAction));
        if (!(this.configuraProxyAction == null || Session.isApplet())) {
            this.add(new JSeparator());
            this.add(new EnhancedMenuItem(this.configuraProxyAction));
        }
        if (this.imprimirAction != null) {
            this.add(new JSeparator());
            this.add(new EnhancedMenuItem(this.imprimirAction));
        }
        if (!(this.exitAction == null || Session.isApplet())) {
            this.add(new JSeparator());
            EnhancedMenuItem itemSair = new EnhancedMenuItem(this.exitAction, 's');
            this.add(itemSair);
        }
    }
}

