/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.menus;

import java.awt.Component;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedMenuItem;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;

public class HelpMenu
extends JMenu {
    public HelpMenu() {
        super("Ajuda");
        this.setMnemonic('a');
        EnhancedAction helpAction = ActionsTaxClientManager.getAction("AjudaPreenchimento");
        EnhancedAction themesHelpAction = ActionsTaxClientManager.getAction("AjudaTemas");
        EnhancedAction wizardAction = ActionsTaxClientManager.getAction("Wizard");
        if (helpAction != null) {
            this.add(new EnhancedMenuItem(helpAction));
        }
        if (themesHelpAction != null) {
            this.add(new EnhancedMenuItem(themesHelpAction));
        }
        if (wizardAction != null) {
            this.add(new EnhancedMenuItem(wizardAction));
        }
        this.add(new JSeparator());
    }
}

