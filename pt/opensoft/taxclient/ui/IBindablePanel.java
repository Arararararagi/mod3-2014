/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui;

import pt.opensoft.taxclient.model.QuadroModel;

public interface IBindablePanel<K extends QuadroModel> {
    public void setModel(K var1, boolean var2);

    public K getModel();
}

