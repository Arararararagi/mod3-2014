/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  org.jvnet.flamingo.common.icon.ResizableIcon
 */
package pt.opensoft.taxclient.ui.icons;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.awt.Paint;
import java.awt.RadialGradientPaint;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import org.jvnet.flamingo.common.icon.ResizableIcon;

public class DocumentNewSVGResizableIcon
implements ResizableIcon {
    int width = DocumentNewSVGResizableIcon.getOrigWidth();
    int height = DocumentNewSVGResizableIcon.getOrigHeight();

    public static void paint(Graphics2D g) {
        AlphaComposite origAlphaComposite;
        Shape shape = null;
        Paint paint = null;
        BasicStroke stroke = null;
        float origAlpha = 1.0f;
        Composite origComposite = g.getComposite();
        if (origComposite instanceof AlphaComposite && (origAlphaComposite = (AlphaComposite)origComposite).getRule() == 3) {
            origAlpha = origAlphaComposite.getAlpha();
        }
        AffineTransform defaultTransform_ = g.getTransform();
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_0 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_0_0 = g.getTransform();
        g.transform(new AffineTransform(0.02165152f, 0.0f, 0.0f, 0.01485743f, 43.0076f, 42.68539f));
        g.setComposite(AlphaComposite.getInstance(3, 0.40206185f * origAlpha));
        AffineTransform defaultTransform__0_0_0_0 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new LinearGradientPaint(new Point2D.Double(302.8571472167969, 366.64788818359375), new Point2D.Double(302.8571472167969, 609.5050659179688), new float[]{0.0f, 0.5f, 1.0f}, new Color[]{new Color(0, 0, 0, 0), new Color(0, 0, 0, 255), new Color(0, 0, 0, 0)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(2.774389f, 0.0f, 0.0f, 1.969706f, -1892.179f, -872.8854f));
        shape = new Rectangle2D.Double(-1559.2523193359375, -150.6968536376953, 1339.633544921875, 478.357177734375);
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_0_0_0);
        g.setComposite(AlphaComposite.getInstance(3, 0.40206185f * origAlpha));
        AffineTransform defaultTransform__0_0_0_1 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new RadialGradientPaint(new Point2D.Double(605.7142944335938, 486.64788818359375), 117.14286f, new Point2D.Double(605.7142944335938, 486.64788818359375), new float[]{0.0f, 1.0f}, new Color[]{new Color(0, 0, 0, 255), new Color(0, 0, 0, 0)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(2.774389f, 0.0f, 0.0f, 1.969706f, -1891.633f, -872.8854f));
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(-219.61876, -150.68037);
        ((GeneralPath)shape).curveTo(-219.61876, -150.68037, -219.61876, 327.65042, -219.61876, 327.65042);
        ((GeneralPath)shape).curveTo(-76.74459, 328.55087, 125.78146, 220.48074, 125.78138, 88.45424);
        ((GeneralPath)shape).curveTo(125.78138, -43.572304, -33.655437, -150.68036, -219.61876, -150.68037);
        ((GeneralPath)shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_0_0_1);
        g.setComposite(AlphaComposite.getInstance(3, 0.40206185f * origAlpha));
        AffineTransform defaultTransform__0_0_0_2 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new RadialGradientPaint(new Point2D.Double(605.7142944335938, 486.64788818359375), 117.14286f, new Point2D.Double(605.7142944335938, 486.64788818359375), new float[]{0.0f, 1.0f}, new Color[]{new Color(0, 0, 0, 255), new Color(0, 0, 0, 0)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(-2.774389f, 0.0f, 0.0f, 1.969706f, 112.7623f, -872.8854f));
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(-1559.2523, -150.68037);
        ((GeneralPath)shape).curveTo(-1559.2523, -150.68037, -1559.2523, 327.65042, -1559.2523, 327.65042);
        ((GeneralPath)shape).curveTo(-1702.1265, 328.55087, -1904.6525, 220.48074, -1904.6525, 88.45424);
        ((GeneralPath)shape).curveTo(-1904.6525, -43.572304, -1745.2157, -150.68036, -1559.2523, -150.68037);
        ((GeneralPath)shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_0_0_2);
        g.setTransform(defaultTransform__0_0_0);
        g.setTransform(defaultTransform__0_0);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_0 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new RadialGradientPaint(new Point2D.Double(33.966678619384766, 35.736915588378906), 86.70845f, new Point2D.Double(33.966678619384766, 35.736915588378906), new float[]{0.0f, 1.0f}, new Color[]{new Color(250, 250, 250, 255), new Color(187, 187, 187, 255)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(0.960493f, 0.0f, 0.0f, 1.041132f, 0.0f, 0.0f));
        shape = new RoundRectangle2D.Double(6.60355281829834, 3.6464462280273438, 34.875, 40.920494079589844, 2.2980971336364746, 2.2980971336364746);
        g.setPaint(paint);
        g.fill(shape);
        paint = new RadialGradientPaint(new Point2D.Double(8.824419021606445, 3.7561285495758057), 37.751713f, new Point2D.Double(8.824419021606445, 3.7561285495758057), new float[]{0.0f, 1.0f}, new Color[]{new Color(163, 163, 163, 255), new Color(76, 76, 76, 255)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(0.968273f, 0.0f, 0.0f, 1.032767f, 3.353553f, 0.646447f));
        stroke = new BasicStroke(1.0f, 1, 1, 4.0f, null, 0.0f);
        shape = new RoundRectangle2D.Double(6.60355281829834, 3.6464462280273438, 34.875, 40.920494079589844, 2.2980971336364746, 2.2980971336364746);
        g.setPaint(paint);
        g.setStroke(stroke);
        g.draw(shape);
        g.setTransform(defaultTransform__0_1_0);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_1 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new RadialGradientPaint(new Point2D.Double(8.143556594848633, 7.26789665222168), 38.158695f, new Point2D.Double(8.143556594848633, 7.26789665222168), new float[]{0.0f, 1.0f}, new Color[]{new Color(255, 255, 255, 255), new Color(248, 248, 248, 255)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(0.968273f, 0.0f, 0.0f, 1.032767f, 3.353553f, 0.646447f));
        stroke = new BasicStroke(1.0f, 1, 1, 4.0f, null, 0.0f);
        shape = new RoundRectangle2D.Double(7.666053771972656, 4.583946228027344, 32.77588653564453, 38.94638442993164, 0.2980971336364746, 0.2980971336364746);
        g.setPaint(paint);
        g.setStroke(stroke);
        g.draw(shape);
        g.setTransform(defaultTransform__0_1_1);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_2 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.646447f, -0.03798933f));
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_2_0 = g.getTransform();
        g.transform(new AffineTransform(0.229703f, 0.0f, 0.0f, 0.229703f, 4.967081f, 4.244972f));
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_2_0_0 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new Color(255, 255, 255, 255);
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(23.428, 113.07);
        ((GeneralPath)shape).curveTo(23.428, 115.043, 21.828, 116.642, 19.855, 116.642);
        ((GeneralPath)shape).curveTo(17.881, 116.642, 16.282, 115.042, 16.282, 113.07);
        ((GeneralPath)shape).curveTo(16.282, 111.096, 17.882, 109.497, 19.855, 109.497);
        ((GeneralPath)shape).curveTo(21.828, 109.497, 23.428, 111.097, 23.428, 113.07);
        ((GeneralPath)shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_1_2_0_0);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_2_0_1 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new Color(255, 255, 255, 255);
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(23.428, 63.07);
        ((GeneralPath)shape).curveTo(23.428, 65.043, 21.828, 66.643, 19.855, 66.643);
        ((GeneralPath)shape).curveTo(17.881, 66.643, 16.282, 65.043, 16.282, 63.07);
        ((GeneralPath)shape).curveTo(16.282, 61.096, 17.882, 59.497, 19.855, 59.497);
        ((GeneralPath)shape).curveTo(21.828, 59.497, 23.428, 61.097, 23.428, 63.07);
        ((GeneralPath)shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_1_2_0_1);
        g.setTransform(defaultTransform__0_1_2_0);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_2_1 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new RadialGradientPaint(new Point2D.Double(20.892099380493164, 114.56839752197266), 5.256f, new Point2D.Double(20.892099380493164, 114.56839752197266), new float[]{0.0f, 1.0f}, new Color[]{new Color(240, 240, 240, 255), new Color(154, 154, 154, 255)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(0.229703f, 0.0f, 0.0f, 0.229703f, 4.613529f, 3.979808f));
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(9.995011, 29.952326);
        ((GeneralPath)shape).curveTo(9.995011, 30.40553, 9.627486, 30.772825, 9.174282, 30.772825);
        ((GeneralPath)shape).curveTo(8.720848, 30.772825, 8.353554, 30.4053, 8.353554, 29.952326);
        ((GeneralPath)shape).curveTo(8.353554, 29.498892, 8.721078, 29.131598, 9.174282, 29.131598);
        ((GeneralPath)shape).curveTo(9.627486, 29.131598, 9.995011, 29.499123, 9.995011, 29.952326);
        ((GeneralPath)shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_1_2_1);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_2_2 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new RadialGradientPaint(new Point2D.Double(20.892099380493164, 64.56790161132812), 5.257f, new Point2D.Double(20.892099380493164, 64.56790161132812), new float[]{0.0f, 1.0f}, new Color[]{new Color(240, 240, 240, 255), new Color(154, 154, 154, 255)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(0.229703f, 0.0f, 0.0f, 0.229703f, 4.613529f, 3.979808f));
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(9.995011, 18.467176);
        ((GeneralPath)shape).curveTo(9.995011, 18.92038, 9.627486, 19.287905, 9.174282, 19.287905);
        ((GeneralPath)shape).curveTo(8.720848, 19.287905, 8.353554, 18.92038, 8.353554, 18.467176);
        ((GeneralPath)shape).curveTo(8.353554, 18.013742, 8.721078, 17.646446, 9.174282, 17.646446);
        ((GeneralPath)shape).curveTo(9.627486, 17.646446, 9.995011, 18.013971, 9.995011, 18.467176);
        ((GeneralPath)shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_1_2_2);
        g.setTransform(defaultTransform__0_1_2);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_3 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new Color(0, 0, 0, 4);
        stroke = new BasicStroke(0.9885531f, 0, 0, 4.0f, null, 0.0f);
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(11.505723, 5.4942765);
        ((GeneralPath)shape).lineTo(11.505723, 43.400867);
        g.setPaint(paint);
        g.setStroke(stroke);
        g.draw(shape);
        g.setTransform(defaultTransform__0_1_3);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_1_4 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        paint = new Color(255, 255, 255, 52);
        stroke = new BasicStroke(1.0f, 0, 0, 4.0f, null, 0.0f);
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(12.5, 5.0205154);
        ((GeneralPath)shape).lineTo(12.5, 43.038227);
        g.setPaint(paint);
        g.setStroke(stroke);
        g.draw(shape);
        g.setTransform(defaultTransform__0_1_4);
        g.setTransform(defaultTransform__0_1);
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_2 = g.getTransform();
        g.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        g.setComposite(AlphaComposite.getInstance(3, 1.0f * origAlpha));
        AffineTransform defaultTransform__0_2_0 = g.getTransform();
        g.transform(new AffineTransform(0.783292f, 0.0f, 0.0f, 0.783292f, -6.340883f, -86.65168f));
        paint = new RadialGradientPaint(new Point2D.Double(55.0, 125.0), 14.375f, new Point2D.Double(55.0, 125.0), new float[]{0.0f, 0.5f, 1.0f}, new Color[]{new Color(255, 255, 255, 255), new Color(255, 245, 32, 227), new Color(255, 243, 0, 0)}, MultipleGradientPaint.CycleMethod.NO_CYCLE, MultipleGradientPaint.ColorSpaceType.SRGB, new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
        shape = new GeneralPath();
        ((GeneralPath)shape).moveTo(69.375, 125.0);
        ((GeneralPath)shape).curveTo(69.375, 132.93909, 62.939095, 139.375, 55.0, 139.375);
        ((GeneralPath)shape).curveTo(47.060905, 139.375, 40.625, 132.93909, 40.625, 125.0);
        ((GeneralPath)shape).curveTo(40.625, 117.060905, 47.060905, 110.625, 55.0, 110.625);
        ((GeneralPath)shape).curveTo(62.939095, 110.625, 69.375, 117.060905, 69.375, 125.0);
        ((GeneralPath)shape).closePath();
        g.setPaint(paint);
        g.fill(shape);
        g.setTransform(defaultTransform__0_2_0);
        g.setTransform(defaultTransform__0_2);
        g.setTransform(defaultTransform__0);
        g.setTransform(defaultTransform_);
    }

    public static int getOrigWidth() {
        return 47;
    }

    public static int getOrigHeight() {
        return 48;
    }

    public int getIconHeight() {
        return this.width;
    }

    public int getIconWidth() {
        return this.height;
    }

    public void setDimension(Dimension newDimension) {
        this.width = newDimension.width;
        this.height = newDimension.height;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2d = (Graphics2D)g.create();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.translate(x, y);
        double coef1 = (double)this.width / (double)DocumentNewSVGResizableIcon.getOrigWidth();
        double coef2 = (double)this.height / (double)DocumentNewSVGResizableIcon.getOrigHeight();
        double coef = Math.min(coef1, coef2);
        g2d.scale(coef, coef);
        DocumentNewSVGResizableIcon.paint(g2d);
        g2d.dispose();
    }
}

