/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.exception;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import javax.swing.JOptionPane;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.SwingWorker;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.SetSystemProxy;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.SimpleLog;

public class EventExceptionsHandler
implements Thread.UncaughtExceptionHandler {
    protected String errorId;

    public void handle(Throwable t) {
        this.handleException(t);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        this.handleException(e);
    }

    private void handleException(final Throwable t) {
        t.printStackTrace();
        int errorcode = Math.abs((EventExceptionsHandler.toString(t) + this.getAppVersion()).hashCode());
        long timestamp = new DateTime().getTime();
        this.errorId = "" + errorcode + "/" + timestamp;
        if (JOptionPane.showOptionDialog(DialogFactory.getDummyFrame(), "<html><h3>Ocorreu um erro inesperado.</h3><b>Pedimos desculpa pelo inc\u00f3modo, se o problema persistir reinicie a aplica\u00e7\u00e3o.</b><hr><br>Cri\u00e1mos um relat\u00f3rio que nos poder\u00e1 ajudar a determinar a causa do erro<br>e melhorar o comportamento da aplica\u00e7\u00e3o.<br>Este relat\u00f3rio \u00e9 an\u00f3nimo e ser\u00e1 tratado de forma confidencial.<br><br><p align=\"right\"><small>ID: " + this.errorId + "</small></p>" + "</html>", "Erro inesperado", -1, 0, null, new Object[]{"Enviar relat\u00f3rio", "Ignorar"}, "Enviar relat\u00f3rio") == 0) {
            new SwingWorker(){

                @Override
                public Object construct() {
                    try {
                        SetSystemProxy.setSystemProxy();
                        String url = TaxclientParameters.isProduction() ? TaxclientParameters.ERROR_REPORTING_PROD_URL : (TaxclientParameters.isQuality() ? TaxclientParameters.ERROR_REPORTING_QUA_URL : TaxclientParameters.ERROR_REPORTING_TESTES_URL);
                        HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
                        connection.setDoOutput(true);
                        connection.setRequestMethod("POST");
                        OutputStream out = connection.getOutputStream();
                        PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
                        try {
                            try {
                                String ipAddress = InetAddress.getLocalHost().getHostAddress();
                                writer.print("IP=");
                                writer.print(URLEncoder.encode(ipAddress, "ISO-8859-1"));
                                writer.print("&");
                            }
                            catch (Exception e) {
                                // empty catch block
                            }
                            writer.print(EventExceptionsHandler.this.getErrorReport(t));
                            writer.flush();
                            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                            reader.close();
                            JOptionPane.showMessageDialog(DialogFactory.getDummyFrame(), "<html><b>Relat\u00f3rio enviado com sucesso.</b><br><br>O relat\u00f3rio enviado \u00e9 an\u00f3nimo e ser\u00e1 tratado de forma confidencial.<br>Obrigado pela sua colabora\u00e7\u00e3o.<br><p align=\"right\"><small>ID: " + EventExceptionsHandler.this.errorId + "</small></p>" + "</html>", "Relat\u00f3rio enviado", 1);
                        }
                        finally {
                            writer.close();
                        }
                    }
                    catch (IOException e) {
                        SimpleLog.log("Falhou o envio do relat\u00f3rio de erro:" + e);
                        try {
                            FileWriter fWriter = new FileWriter("relatorio_erro_" + new DateTime().format("ddHHmmss") + ".txt");
                            fWriter.write(EventExceptionsHandler.this.getErrorReport(t));
                            fWriter.close();
                        }
                        catch (IOException e1) {
                            SimpleLog.log("Falhou a escrita do ficheiro de relat\u00f3rio:" + e1);
                        }
                    }
                    return null;
                }
            }.start();
        }
    }

    private String getErrorReport(Throwable t) throws UnsupportedEncodingException {
        return "OS=" + URLEncoder.encode(System.getProperty("os.name"), "ISO-8859-1") + "&" + "TotalMemory=" + URLEncoder.encode(Long.toString(Runtime.getRuntime().totalMemory()), "ISO-8859-1") + "&" + "FreeMemory=" + URLEncoder.encode(Long.toString(Runtime.getRuntime().freeMemory()), "ISO-8859-1") + "&" + "MaxMemory=" + URLEncoder.encode(Long.toString(Runtime.getRuntime().maxMemory()), "ISO-8859-1") + "&" + "AppId=" + URLEncoder.encode(this.getAppTitle(), "ISO-8859-1") + "&" + "AppVersion=" + URLEncoder.encode(this.getAppVersion(), "ISO-8859-1") + "&" + "AppInternalVersion=" + URLEncoder.encode(this.getAppInternalVersion(), "ISO-8859-1") + "&" + "ErrorMessage=" + URLEncoder.encode(t.getMessage() == null ? t.toString() : t.getMessage(), "ISO-8859-1") + "&" + "FullProperties=" + URLEncoder.encode(System.getProperties().toString(), "ISO-8859-1") + "&" + "StackTrace=" + URLEncoder.encode(EventExceptionsHandler.toString(t), "ISO-8859-1") + "&" + "ErrorId=" + URLEncoder.encode(this.errorId, "ISO-8859-1") + "&" + "IsApplet=" + Session.isApplet();
    }

    protected String getAppTitle() {
        return "TaxClient";
    }

    protected String getAppVersion() {
        return "1.0.0";
    }

    protected String getAppInternalVersion() {
        return "1.0.0.b01";
    }

    private static String toString(Throwable t) {
        try {
            StringWriter sw = new StringWriter();
            try {
                PrintWriter pw = new PrintWriter(sw);
                try {
                    t.printStackTrace(pw);
                }
                finally {
                    pw.close();
                }
            }
            finally {
                sw.close();
            }
            return sw.toString();
        }
        catch (Exception e) {
            return null;
        }
    }

}

