/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.io.IOException;
import java.net.URL;
import javax.accessibility.AccessibleContext;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.TitledPanel2;
import pt.opensoft.swing.Util;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class GenericPageDisplayer
extends TitledPanel2
implements HelpDisplayer {
    private static final long serialVersionUID = 1618795110759521403L;
    protected ScrollableHtmlPane scrollableHtmlPane;
    protected String htmlFile;

    public GenericPageDisplayer(String htmlFile, String cssFile) {
        super(new BorderLayout());
        this.htmlFile = htmlFile;
        this.scrollableHtmlPane = new ScrollableHtmlPane();
        this.scrollableHtmlPane.setEditable(false);
        this.scrollableHtmlPane.setContentType("text/html");
        StyleSheet css = new StyleSheet();
        if (cssFile != null) {
            css.importStyleSheet(Util.class.getResource(cssFile));
        }
        HTMLEditorKit edtKit = new HTMLEditorKit();
        edtKit.setStyleSheet(css);
        this.scrollableHtmlPane.setEditorKit(edtKit);
        this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
        this.changeFont();
        JScrollPane scrollPane = new JScrollPane(this.scrollableHtmlPane);
        scrollPane.getVerticalScrollBar().setUnitIncrement(scrollPane.getVerticalScrollBar().getBlockIncrement());
        this.add(scrollPane);
    }

    @Override
    public void updateHelp() throws Exception {
        if (Session.getMainFrame().getMultiPurposeDisplayer().isDisplayerVisible()) {
            String helpText;
            String pageLocation = this.htmlFile;
            try {
                SimpleLog.log("Showing html: pageLocation = " + pageLocation);
                helpText = Util.getHtmlText(this.scrollableHtmlPane, pageLocation);
            }
            catch (IOException e) {
                helpText = "<p><i>P\u00e1gina n\u00e3o dispon\u00edvel</i></p></html>";
            }
            this.scrollableHtmlPane.setText(helpText);
            this.getAccessibleContext().setAccessibleDescription(helpText);
            this.changeFont();
        }
    }

    @Override
    public void updateSearchHelp(String text) throws Exception {
        throw new RuntimeException("Not implemented");
    }

    protected void changeFont() {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)this.scrollableHtmlPane.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }
}

