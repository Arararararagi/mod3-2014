/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.util;

import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class PopupMenu
extends JPopupMenu {
    private final List<? extends AbstractAction> actionBeans;

    public PopupMenu(List<? extends AbstractAction> actionBeans) {
        this.actionBeans = actionBeans;
        for (AbstractAction abstractAction : actionBeans) {
            this.add(abstractAction);
        }
    }
}

