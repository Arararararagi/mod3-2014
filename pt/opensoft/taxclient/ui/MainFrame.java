/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import org.jvnet.flamingo.ribbon.JRibbon;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.EnhancedToolBar;
import pt.opensoft.swing.SeparatorAction;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.structural.add.AddAnexoAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.persistence.DeclarationReader;
import pt.opensoft.taxclient.ui.event.NavigationSelectionListener;
import pt.opensoft.taxclient.ui.event.SelectedListener;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.layout.Skeleton;
import pt.opensoft.taxclient.ui.layout.impl.DefaultSkeleton;
import pt.opensoft.taxclient.ui.menus.ClassicMenuBar;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.ui.structure.StructureControllerDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.ListMap;
import pt.opensoft.util.SimpleLog;

public abstract class MainFrame
extends JPanel {
    protected Skeleton skeleton;
    protected JPanel statusBar;
    protected JMenuBar menuBar;
    protected JRibbon topRibbon;
    protected JToolBar topMenuToolBar;
    protected DeclaracaoModel declaracaoModel;
    protected DeclarationDisplayer declarationDisplayer;
    protected NavigationModel navigationModel;
    protected NavigationDisplayer navigationDisplayer;
    protected StructureControllerDisplayer structureControllerDisplayer;
    protected MultiPurposeDisplayer multiPurposeDisplayer;

    public MainFrame(DeclaracaoModel declaracaoModel, NavigationModel navigationModel) {
        this.preInitSetup();
        this.setLayout(new BorderLayout());
        DialogFactory.init(this);
        this.skeleton = this.createSkeleton();
        if (this.skeleton == null) {
            SimpleLog.log("Warning: No skeleton found after createSkeleton() method. Going with a DefaultSkeleton.");
            this.skeleton = new DefaultSkeleton();
        }
        this.skeleton.build(this);
        this.initializeStatusBar();
        this.navigationModel = navigationModel;
        this.initNavigationDisplay();
        this.declaracaoModel = declaracaoModel;
        this.initDeclarationDisplay();
        this.initMultiPurposeDisplay();
        List<AddAnexoAction> addAnexoActionsList = this.createAddActions(declaracaoModel);
        ActionsTaxClientManager.initActionsTaxClientManager(declaracaoModel);
        ActionsTaxClientManager.registerAddActions(addAnexoActionsList);
        this.initStructureControllerDisplay();
        declaracaoModel.addDeclaracaoMapModelListener(navigationModel);
        this.declarationDisplayer.registerListeners();
        Session.getCurrentDeclaracao().addNavigationSelectionListener(this.declarationDisplayer);
        Session.getCurrentDeclaracao().addNavigationSelectionListener(this.navigationDisplayer);
        declaracaoModel.addDeclaracaoMapModelListener(this.declarationDisplayer);
        Session.getCurrentDeclaracao().addSelectionListener(this.declarationDisplayer);
        Session.getCurrentDeclaracao().addSelectionListener(this.navigationDisplayer);
        DeclarationReader.addReadingListener(this.declarationDisplayer);
        this.postInitSetup();
    }

    protected void initializeStatusBar() {
        this.statusBar = new JPanel(new GridLayout(1, 4, 4, 4));
        this.statusBar.setSize(1, 100);
        JLabel statusLabel = new JLabel();
        statusLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        statusLabel.setText(this.getStatusBarText());
        this.statusBar.add(statusLabel);
        this.add((Component)this.statusBar, "South");
    }

    protected abstract String getStatusBarText();

    protected void initDeclarationDisplay() {
        this.declarationDisplayer = this.createDeclarationDisplayer();
        if (this.declarationDisplayer == null) {
            throw new IllegalStateException("Declaration displayer cannot be null.");
        }
        if (!(this.declarationDisplayer instanceof JComponent)) {
            throw new ClassCastException("Declaration displayer must extend JComponent.");
        }
        this.declarationDisplayer.setModel(this.declaracaoModel);
        this.skeleton.pluginDeclarationDisplayer((JComponent)this.declarationDisplayer);
    }

    protected void initNavigationDisplay() {
        this.navigationDisplayer = this.createNavigationDisplayer();
        this.navigationDisplayer.setModel(this.navigationModel);
        if (this.navigationDisplayer == null) {
            throw new IllegalStateException("Navigation displayer cannot be null.");
        }
        if (!(this.navigationDisplayer instanceof JComponent)) {
            throw new ClassCastException("Navigation displayer must extend JComponent.");
        }
        this.skeleton.pluginNavigationDisplayer((JComponent)this.navigationDisplayer);
    }

    protected void initStructureControllerDisplay() {
        this.structureControllerDisplayer = this.createStructureControllerDisplayer();
        if (this.structureControllerDisplayer == null) {
            throw new IllegalStateException("Structure Controller Displayer cannot be null.");
        }
        if (!(this.structureControllerDisplayer instanceof JComponent)) {
            throw new ClassCastException("Structure Controller displayer must extend JComponent.");
        }
        this.skeleton.pluginStructureController((JComponent)this.structureControllerDisplayer);
    }

    protected abstract List<AddAnexoAction> createAddActions(DeclaracaoModel var1);

    protected void initMultiPurposeDisplay() {
        this.multiPurposeDisplayer = this.createMultiPurposeDisplayer();
        this.skeleton.pluginMultiPurposeDisplayer((JComponent)this.multiPurposeDisplayer);
        this.multiPurposeDisplayer.hideMultiPurposeDisplayer();
        HelpDisplayer helpDisplayer = this.multiPurposeDisplayer.createHelpDisplayer();
        this.multiPurposeDisplayer.setHelpDisplayer(helpDisplayer);
    }

    public void initMenuBar() {
        ClassicMenuBar classicMenuBar = new ClassicMenuBar();
        classicMenuBar.initFileMenu();
        classicMenuBar.initEditMenu();
        classicMenuBar.initDeclaracaoMenu();
        classicMenuBar.add(Box.createHorizontalGlue());
        this.menuBar = classicMenuBar;
    }

    public void initTopMenuToolBar() {
        ListHashMap<String, EnhancedAction> actionsToolbar = new ListHashMap<String, EnhancedAction>();
        actionsToolbar.put("Novo", ActionsTaxClientManager.getAction("Novo"));
        actionsToolbar.put("Abrir", ActionsTaxClientManager.getAction("Abrir"));
        actionsToolbar.put("Gravar", ActionsTaxClientManager.getAction("Gravar"));
        actionsToolbar.put("GravarComo", ActionsTaxClientManager.getAction("GravarComo"));
        actionsToolbar.put("ConfigurarProxy", ActionsTaxClientManager.getAction("ConfigurarProxy"));
        actionsToolbar.put("Imprimir", ActionsTaxClientManager.getAction("Imprimir"));
        actionsToolbar.put("Sair", ActionsTaxClientManager.getAction("Sair"));
        actionsToolbar.put("separator1", new SeparatorAction());
        actionsToolbar.put("Validar", ActionsTaxClientManager.getAction("Validar"));
        actionsToolbar.put("Simular", ActionsTaxClientManager.getAction("Simular"));
        actionsToolbar.put("Submeter", ActionsTaxClientManager.getAction("Submeter"));
        actionsToolbar.put("separator2", new SeparatorAction());
        this.topMenuToolBar = new EnhancedToolBar(actionsToolbar, new EnhancedAction[]{null}, 3);
        this.add((Component)this.topMenuToolBar, "North");
    }

    public abstract void initTopRibbon();

    protected abstract DeclarationDisplayer createDeclarationDisplayer();

    protected abstract NavigationDisplayer createNavigationDisplayer();

    protected abstract StructureControllerDisplayer createStructureControllerDisplayer();

    protected abstract MultiPurposeDisplayer createMultiPurposeDisplayer();

    protected abstract Skeleton createSkeleton();

    protected void preInitSetup() {
    }

    protected void postInitSetup() {
    }

    public JMenuBar getMenuBar() {
        return this.menuBar;
    }

    public JToolBar getTopMenuToolbarl() {
        return this.topMenuToolBar;
    }

    public JRibbon getTopRibbon() {
        return this.topRibbon;
    }

    public DeclarationDisplayer getDeclarationDisplayer() {
        return this.declarationDisplayer;
    }

    public NavigationDisplayer getNavigationDisplayer() {
        return this.navigationDisplayer;
    }

    public MultiPurposeDisplayer getMultiPurposeDisplayer() {
        return this.multiPurposeDisplayer;
    }

    public Skeleton getSkeleton() {
        return this.skeleton;
    }
}

