/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.layout;

import javax.swing.JComponent;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.layout.DummyPlug;

public abstract class Skeleton {
    public abstract void build(MainFrame var1);

    public void pluginDeclarationDisplayer(JComponent declarationDisplayer) {
        this.setupDeclarationDisplayer(declarationDisplayer);
    }

    protected abstract void setupDeclarationDisplayer(JComponent var1);

    public void pluginNavigationDisplayer(JComponent navigationDisplayer) {
        if (navigationDisplayer instanceof DummyPlug) {
            this.setupDummyNavigationDisplayer(navigationDisplayer);
        } else {
            this.setupNavigationDisplayer(navigationDisplayer);
        }
    }

    protected abstract void setupNavigationDisplayer(JComponent var1);

    protected abstract void setupDummyNavigationDisplayer(JComponent var1);

    public void pluginStructureController(JComponent structureController) {
        if (structureController instanceof DummyPlug) {
            this.setupDummyStructureController(structureController);
        } else {
            this.setupStructureController(structureController);
        }
    }

    protected abstract void setupStructureController(JComponent var1);

    protected abstract void setupDummyStructureController(JComponent var1);

    public void pluginMultiPurposeDisplayer(JComponent multiPurposeDisplayer) {
        if (multiPurposeDisplayer instanceof DummyPlug) {
            this.setupDummyMultiPurposeDisplayer(multiPurposeDisplayer);
        } else {
            this.setupMultiPurposeDisplayer(multiPurposeDisplayer);
        }
    }

    protected abstract void setupMultiPurposeDisplayer(JComponent var1);

    protected abstract void setupDummyMultiPurposeDisplayer(JComponent var1);
}

