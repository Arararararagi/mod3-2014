/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.layout.impl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import pt.opensoft.swing.EnhancedTreeNode;
import pt.opensoft.swing.TitledPanel3;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.layout.Skeleton;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.ui.navigation.impl.tree.AnexoTreeNode;

public class DefaultW3Skeleton
extends Skeleton
implements PropertyChangeListener,
TreeSelectionListener {
    protected JSplitPane outterSplitPane;
    protected JSplitPane topFrame;
    protected TitledPanel3 topNavigationFrame;
    protected JSplitPane innerBottomSplitPane;
    protected TitledPanel3 buttonsFrame;
    protected static final int BUTTON_DIVIDER_SIZE = 2;
    protected boolean readonly;

    public DefaultW3Skeleton(boolean readonly) {
        this.readonly = readonly;
    }

    @Override
    public void build(MainFrame mainFrame) {
        this.outterSplitPane = new JSplitPane();
        this.outterSplitPane.setOrientation(0);
        this.outterSplitPane.setDividerSize(0);
        this.outterSplitPane.setBorder(BorderFactory.createEmptyBorder());
        this.outterSplitPane.setResizeWeight(0.1);
        this.topFrame = new JSplitPane();
        this.topFrame.setOrientation(0);
        Dimension topFrameMinumumSize = !this.readonly ? new Dimension(50, 50) : new Dimension(40, 40);
        this.topNavigationFrame = new TitledPanel3(null, null, Color.gray, new Insets(0, 0, 0, 0));
        this.topNavigationFrame.setMinimumSize(topFrameMinumumSize);
        this.topNavigationFrame.setBorder(BorderFactory.createEmptyBorder());
        this.topFrame.setTopComponent(this.topNavigationFrame);
        this.topFrame.setDividerSize(0);
        this.topFrame.setResizeWeight(1.0);
        this.topFrame.setBorder(BorderFactory.createEmptyBorder());
        this.topFrame.setBackground(UIManager.getColor("Skeleton.w3.backgroundColor"));
        this.outterSplitPane.setTopComponent(this.topFrame);
        this.innerBottomSplitPane = new JSplitPane();
        this.innerBottomSplitPane.setOrientation(0);
        this.innerBottomSplitPane.setBorder(BorderFactory.createEmptyBorder());
        this.innerBottomSplitPane.setDividerSize(0);
        this.innerBottomSplitPane.setDividerLocation(0.5);
        this.outterSplitPane.setBottomComponent(this.innerBottomSplitPane);
        mainFrame.add((Component)this.outterSplitPane, "Center");
    }

    @Override
    protected void setupDeclarationDisplayer(JComponent declarationDisplayer) {
        declarationDisplayer.setBorder(BorderFactory.createEmptyBorder());
        this.innerBottomSplitPane.setLeftComponent(declarationDisplayer);
    }

    @Override
    protected void setupNavigationDisplayer(JComponent navigationDisplayer) {
        if (navigationDisplayer instanceof NavigationDisplayer) {
            ((NavigationDisplayer)navigationDisplayer).registerListener(this);
        }
        this.topNavigationFrame.add((Component)navigationDisplayer, "Center");
    }

    @Override
    protected void setupDummyNavigationDisplayer(JComponent navigationDisplayer) {
        this.topNavigationFrame.add((Component)navigationDisplayer, "Center");
        navigationDisplayer.setVisible(false);
    }

    @Override
    protected void setupDummyStructureController(JComponent structureController) {
        this.buttonsFrame.add((Component)structureController, "Center");
        structureController.setVisible(false);
    }

    @Override
    protected void setupStructureController(JComponent structureController) {
        structureController.setBorder(BorderFactory.createEmptyBorder());
        this.topFrame.setBottomComponent(structureController);
        Dimension minumumSize = !this.readonly ? new Dimension(30, 30) : new Dimension(0, 0);
        structureController.setMinimumSize(minumumSize);
        structureController.setVisible(true);
    }

    @Override
    protected void setupMultiPurposeDisplayer(JComponent multiPurposeDisplayer) {
        this.innerBottomSplitPane.setRightComponent(multiPurposeDisplayer);
        ((MultiPurposeDisplayer)multiPurposeDisplayer).addIsVisibleListener(this);
    }

    @Override
    protected void setupDummyMultiPurposeDisplayer(JComponent multiPurposeDisplayer) {
        this.innerBottomSplitPane.setRightComponent(multiPurposeDisplayer);
        multiPurposeDisplayer.setVisible(false);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("isVisible".equals(evt.getPropertyName()) && ((Boolean)evt.getNewValue()).booleanValue()) {
            this.innerBottomSplitPane.getBottomComponent().setVisible(true);
            this.innerBottomSplitPane.setDividerLocation(0.5);
        }
        if ("isMaximized".equals(evt.getPropertyName()) && ((Boolean)evt.getNewValue()).booleanValue()) {
            this.innerBottomSplitPane.getBottomComponent().setVisible(true);
            this.innerBottomSplitPane.setDividerLocation(0.0);
        }
    }

    @Override
    public void valueChanged(TreeSelectionEvent event) {
        AnexoTreeNode anexoTreeNode;
        FormKey selectedAnexo;
        if (event.getNewLeadSelectionPath() == null) {
            return;
        }
        EnhancedTreeNode enhancedTreeNode = (EnhancedTreeNode)event.getNewLeadSelectionPath().getLastPathComponent();
        if (enhancedTreeNode instanceof AnexoTreeNode && (selectedAnexo = (anexoTreeNode = (AnexoTreeNode)enhancedTreeNode).getFormKey()) != null) {
            String key = selectedAnexo.getId().toLowerCase() + ".navigation.designation";
            String keyAdditional = selectedAnexo.getId().toLowerCase() + ".declaration.designation";
            String nodeDesignation = DesignationManager.getInstance().getString(key);
            if (nodeDesignation == null) {
                throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
            }
            String subId = selectedAnexo.getSubId() == null ? "" : " (" + selectedAnexo.getSubId() + ")";
            String title = nodeDesignation + subId;
            String additional = DesignationManager.getInstance().getString(keyAdditional);
            title = title + (additional == null ? "" : new StringBuilder().append(" - ").append(additional).toString());
        }
    }
}

