/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.layout.impl;

import com.jgoodies.uif_lite.panel.SimpleInternalFrame;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.layout.Skeleton;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.TaxclientParameters;

public class FixedAnexosSkeleton
extends Skeleton
implements PropertyChangeListener {
    protected JSplitPane outterSplitPane;
    protected JSplitPane innerSplitPane;

    @Override
    public void build(MainFrame mainFrame) {
        this.outterSplitPane = new JSplitPane();
        this.outterSplitPane.setDividerSize(TaxclientParameters.MF_DIVIDER_SIZE);
        this.outterSplitPane.setDividerLocation(TaxclientParameters.MF_DIVIDER_LOCATION);
        this.innerSplitPane = new JSplitPane();
        this.innerSplitPane.setOrientation(0);
        this.innerSplitPane.setDividerSize(0);
        this.innerSplitPane.setDividerLocation(0.5);
        this.outterSplitPane.setRightComponent(this.innerSplitPane);
        mainFrame.add((Component)this.outterSplitPane, "Center");
    }

    @Override
    protected void setupDeclarationDisplayer(JComponent declarationDisplayer) {
        if (TaxclientParameters.MF_USE_FRAME_FOR_DECL_DISPLAY) {
            SimpleInternalFrame framedDeclarationDisplayer = new SimpleInternalFrame(null, TaxclientParameters.MF_NAV_FRAME_TITLE, null, declarationDisplayer);
            this.innerSplitPane.setLeftComponent(framedDeclarationDisplayer);
        } else {
            this.innerSplitPane.setLeftComponent(declarationDisplayer);
        }
    }

    @Override
    protected void setupNavigationDisplayer(JComponent navigationDisplayer) {
        if (TaxclientParameters.MF_USE_FRAME_FOR_DECL_DISPLAY) {
            SimpleInternalFrame framedNavigationDisplayer = new SimpleInternalFrame(null, TaxclientParameters.MF_NAV_FRAME_TITLE, null, navigationDisplayer);
            this.outterSplitPane.setLeftComponent(framedNavigationDisplayer);
        } else {
            this.outterSplitPane.setLeftComponent(navigationDisplayer);
        }
    }

    @Override
    protected void setupDummyNavigationDisplayer(JComponent navigationDisplayer) {
        this.outterSplitPane.setDividerSize(0);
        this.outterSplitPane.setDividerLocation(0);
        this.outterSplitPane.setLeftComponent(navigationDisplayer);
        navigationDisplayer.setVisible(false);
    }

    @Override
    protected void setupMultiPurposeDisplayer(JComponent multiPurposeDisplayer) {
        this.innerSplitPane.setRightComponent(multiPurposeDisplayer);
        ((MultiPurposeDisplayer)multiPurposeDisplayer).addIsVisibleListener(this);
    }

    @Override
    protected void setupDummyMultiPurposeDisplayer(JComponent multiPurposeDisplayer) {
        this.innerSplitPane.setRightComponent(multiPurposeDisplayer);
        multiPurposeDisplayer.setVisible(false);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("isVisible".equals(evt.getPropertyName()) && ((Boolean)evt.getNewValue()).booleanValue()) {
            this.innerSplitPane.getBottomComponent().setVisible(true);
            this.innerSplitPane.setDividerLocation(0.5);
        }
        if ("isMaximized".equals(evt.getPropertyName()) && ((Boolean)evt.getNewValue()).booleanValue()) {
            this.innerSplitPane.getBottomComponent().setVisible(true);
            this.innerSplitPane.setDividerLocation(0.0);
        }
    }

    @Override
    protected void setupDummyStructureController(JComponent structureController) {
    }

    @Override
    protected void setupStructureController(JComponent structureController) {
    }
}

