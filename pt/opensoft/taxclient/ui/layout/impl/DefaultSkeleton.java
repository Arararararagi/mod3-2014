/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.layout.impl;

import com.jgoodies.uif_lite.panel.SimpleInternalFrame;
import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import pt.opensoft.swing.EnhancedTreeNode;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.TitledPanel3;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.layout.Skeleton;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.ui.navigation.impl.tree.AnexoTreeNode;
import pt.opensoft.taxclient.util.TaxclientParameters;

public class DefaultSkeleton
extends Skeleton
implements PropertyChangeListener,
TreeSelectionListener {
    protected JSplitPane outterSplitPane;
    protected JPanel innerLeftPane;
    protected JSplitPane innerRightSplitPane;
    protected TitledPanel3 framedDeclarationDisplayer;

    @Override
    public void build(MainFrame mainFrame) {
        this.outterSplitPane = new JSplitPane();
        this.outterSplitPane.setDividerSize(TaxclientParameters.MF_DIVIDER_SIZE);
        this.outterSplitPane.setDividerLocation(TaxclientParameters.MF_DIVIDER_LOCATION);
        this.innerLeftPane = new SimpleInternalFrame();
        TitledPanel3 leftNavigationFrame = new TitledPanel3(null, TaxclientParameters.MF_NAV_FRAME_TITLE, GUIParameters.NAVIGATION_TITLE_BG_COLOR);
        leftNavigationFrame.add((Component)this.innerLeftPane, "Center");
        this.outterSplitPane.setLeftComponent(leftNavigationFrame);
        this.innerRightSplitPane = new JSplitPane();
        this.innerRightSplitPane.setOrientation(0);
        this.innerRightSplitPane.setDividerSize(0);
        this.innerRightSplitPane.setDividerLocation(0.5);
        this.outterSplitPane.setRightComponent(this.innerRightSplitPane);
        mainFrame.add((Component)this.outterSplitPane, "Center");
    }

    @Override
    protected void setupDeclarationDisplayer(JComponent declarationDisplayer) {
        if (TaxclientParameters.MF_USE_FRAME_FOR_DECL_DISPLAY) {
            this.framedDeclarationDisplayer = new TitledPanel3(null, TaxclientParameters.MF_DECL_FRAME_TITLE, GUIParameters.DECLARATION_TITLE_BG_COLOR);
            this.framedDeclarationDisplayer.add((Component)declarationDisplayer, "Center");
            this.innerRightSplitPane.setLeftComponent(this.framedDeclarationDisplayer);
        } else {
            this.innerRightSplitPane.setLeftComponent(declarationDisplayer);
        }
    }

    @Override
    protected void setupNavigationDisplayer(JComponent navigationDisplayer) {
        if (navigationDisplayer instanceof NavigationDisplayer) {
            ((NavigationDisplayer)navigationDisplayer).registerListener(this);
        }
        if (TaxclientParameters.MF_USE_FRAME_FOR_NAV_DISPLAY) {
            SimpleInternalFrame framedNavigationDisplayer = new SimpleInternalFrame(null, TaxclientParameters.MF_NAV_FRAME_TITLE, null, navigationDisplayer);
            this.innerLeftPane.add((Component)framedNavigationDisplayer, "Center");
        } else {
            this.innerLeftPane.add((Component)navigationDisplayer, "Center");
        }
    }

    @Override
    protected void setupDummyNavigationDisplayer(JComponent navigationDisplayer) {
        this.innerLeftPane.add((Component)navigationDisplayer, "Center");
        navigationDisplayer.setVisible(false);
    }

    @Override
    protected void setupDummyStructureController(JComponent structureController) {
        this.innerLeftPane.add((Component)structureController, "Center");
        structureController.setVisible(false);
    }

    protected String calculateStrutureControllerBorderLayoutPos() {
        return "North";
    }

    @Override
    protected void setupStructureController(JComponent structureController) {
        if (TaxclientParameters.MF_USE_FRAME_FOR_NAV_CONTROLLER) {
            SimpleInternalFrame framedStructureControllerDisplayer = new SimpleInternalFrame(null, TaxclientParameters.MF_NAV_FRAME_TITLE, null, structureController);
            this.innerLeftPane.add((Component)framedStructureControllerDisplayer, this.calculateStrutureControllerBorderLayoutPos());
        } else {
            this.innerLeftPane.add((Component)structureController, this.calculateStrutureControllerBorderLayoutPos());
        }
    }

    @Override
    protected void setupMultiPurposeDisplayer(JComponent multiPurposeDisplayer) {
        this.innerRightSplitPane.setRightComponent(multiPurposeDisplayer);
        ((MultiPurposeDisplayer)multiPurposeDisplayer).addIsVisibleListener(this);
    }

    @Override
    protected void setupDummyMultiPurposeDisplayer(JComponent multiPurposeDisplayer) {
        this.innerRightSplitPane.setRightComponent(multiPurposeDisplayer);
        multiPurposeDisplayer.setVisible(false);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("isVisible".equals(evt.getPropertyName()) && ((Boolean)evt.getNewValue()).booleanValue()) {
            this.innerRightSplitPane.getBottomComponent().setVisible(true);
            this.innerRightSplitPane.setDividerLocation(0.5);
        }
        if ("isMaximized".equals(evt.getPropertyName()) && ((Boolean)evt.getNewValue()).booleanValue()) {
            this.innerRightSplitPane.getBottomComponent().setVisible(true);
            this.innerRightSplitPane.setDividerLocation(0.0);
        }
    }

    @Override
    public void valueChanged(TreeSelectionEvent event) {
        AnexoTreeNode anexoTreeNode;
        FormKey selectedAnexo;
        if (event.getNewLeadSelectionPath() == null) {
            return;
        }
        EnhancedTreeNode enhancedTreeNode = (EnhancedTreeNode)event.getNewLeadSelectionPath().getLastPathComponent();
        if (enhancedTreeNode instanceof AnexoTreeNode && (selectedAnexo = (anexoTreeNode = (AnexoTreeNode)enhancedTreeNode).getFormKey()) != null) {
            String key = selectedAnexo.getId().toLowerCase() + ".navigation.designation";
            String keyAdditional = selectedAnexo.getId().toLowerCase() + ".declaration.designation";
            String nodeDesignation = DesignationManager.getInstance().getString(key);
            if (nodeDesignation == null) {
                throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
            }
            String subId = selectedAnexo.getSubId() == null ? "" : " (" + selectedAnexo.getSubId() + ")";
            String title = nodeDesignation + subId;
            String additional = DesignationManager.getInstance().getString(keyAdditional);
            title = title + (additional == null ? "" : new StringBuilder().append(" - ").append(additional).toString());
            this.framedDeclarationDisplayer.setTitle(title);
        }
    }
}

