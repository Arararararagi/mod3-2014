/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.layout.impl;

import com.jgoodies.uif_lite.panel.SimpleInternalFrame;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import pt.opensoft.swing.TitledPanel3;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.layout.impl.DefaultSkeleton;
import pt.opensoft.taxclient.util.TaxclientParameters;

public class W3UsabilitySkeleton
extends DefaultSkeleton {
    @Override
    public void build(MainFrame mainFrame) {
        this.outterSplitPane = new JSplitPane();
        this.outterSplitPane.setDividerSize(TaxclientParameters.MF_DIVIDER_SIZE);
        this.outterSplitPane.setDividerLocation(TaxclientParameters.MF_DIVIDER_LOCATION);
        this.innerLeftPane = new SimpleInternalFrame();
        this.innerLeftPane.setMinimumSize(new Dimension(this.innerLeftPane.getWidth(), 45));
        TitledPanel3 leftNavigationFrame = new TitledPanel3(null, TaxclientParameters.MF_NAV_FRAME_TITLE, UIManager.getColor("TitledPanel3.titleBackground"), false, true);
        leftNavigationFrame.add((Component)this.innerLeftPane, "Center");
        this.outterSplitPane.setLeftComponent(leftNavigationFrame);
        this.innerRightSplitPane = new JSplitPane();
        this.innerRightSplitPane.setOrientation(0);
        this.innerRightSplitPane.setDividerSize(0);
        this.innerRightSplitPane.setDividerLocation(0.5);
        this.outterSplitPane.setRightComponent(this.innerRightSplitPane);
        this.outterSplitPane.setOrientation(0);
        this.outterSplitPane.setDividerLocation(-1);
        mainFrame.add((Component)this.outterSplitPane, "Center");
    }

    @Override
    protected void setupDummyStructureController(JComponent structureController) {
        this.innerLeftPane.add((Component)structureController, this.calculateStrutureControllerBorderLayoutPos());
        structureController.setVisible(false);
    }

    @Override
    protected void setupDeclarationDisplayer(JComponent declarationDisplayer) {
        if (TaxclientParameters.MF_USE_FRAME_FOR_DECL_DISPLAY) {
            this.framedDeclarationDisplayer = new TitledPanel3(null, TaxclientParameters.MF_DECL_FRAME_TITLE, UIManager.getColor("TitledPanel3.titleBackground"), false, true);
            this.framedDeclarationDisplayer.add((Component)declarationDisplayer, "Center");
            this.innerRightSplitPane.setLeftComponent(this.framedDeclarationDisplayer);
        } else {
            this.innerRightSplitPane.setLeftComponent(declarationDisplayer);
        }
    }

    @Override
    protected String calculateStrutureControllerBorderLayoutPos() {
        return "West";
    }
}

