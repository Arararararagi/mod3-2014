/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.multi;

import java.beans.PropertyChangeListener;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.help.HelpWizardDisplayer;
import pt.opensoft.taxclient.ui.validation.CentralErrorsDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;

public interface MultiPurposeDisplayer {
    public void setWarningDisplayer(WarningDisplayer var1);

    public WarningDisplayer getWarningDisplayer();

    public void showWarningDisplayer();

    public void setHelpWizardDisplayer(HelpWizardDisplayer var1);

    public HelpWizardDisplayer getHelpWizardDisplayer();

    public void showHelpWizardDisplayer();

    public void setValidationDisplayer(ValidationDisplayer var1);

    public ValidationDisplayer getValidationDisplayer();

    public void showValidationDisplayer();

    public void setCentralErrorsDisplayer(CentralErrorsDisplayer var1);

    public CentralErrorsDisplayer getCentralErrorsDisplayer();

    public void showCentralErrorsDisplayer();

    public void setHelpDisplayer(HelpDisplayer var1);

    public HelpDisplayer createHelpDisplayer();

    public HelpDisplayer getHelpDisplayer();

    public void showHelpDisplayer();

    public void hideMultiPurposeDisplayer();

    public void showMultiPurposeDisplayer();

    public void maximizeMultiPurposeDisplayer();

    public void addIsVisibleListener(PropertyChangeListener var1);

    public void removeIsVisibleListener(PropertyChangeListener var1);

    public boolean isDisplayerVisible();
}

