/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.multi.impl;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.help.HelpWizardDisplayer;
import pt.opensoft.taxclient.ui.help.impl.DefaultHelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.CentralErrorsDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;

public class DefaultMultiPurposeDisplayer
extends JPanel
implements MultiPurposeDisplayer {
    private static final long serialVersionUID = -5632594931287245379L;
    protected static final String VALIDATION_CARD_NAME = "VALIDATION";
    protected static final String HELP_CARD_NAME = "HELP";
    protected static final String WARNING_CARD_NAME = "WARNING";
    protected static final String HELP_WIZARD_CARD_NAME = "HELP_WIZARD";
    protected static final String CENTRAL_ERRORS_NAME = "CENTRAL_ERRORS";
    protected CardLayout cardLayout;
    private JPanel cardLayoutOwner;
    protected Map<String, Component> genericCards;
    ValidationDisplayer validationDisplayer = null;
    HelpDisplayer helpDisplayer = null;
    WarningDisplayer warningDisplayer = null;
    HelpWizardDisplayer helpWizardDisplayer = null;
    CentralErrorsDisplayer centralErrorsDisplayer = null;
    private List<PropertyChangeListener> propertyChangeListeners = new ArrayList<PropertyChangeListener>();
    private boolean isVisible;
    private boolean isMaximized = false;
    protected JToolBar menuBar;

    public DefaultMultiPurposeDisplayer() {
        this(false);
    }

    public DefaultMultiPurposeDisplayer(boolean hideTitleBar) {
        this.setLayout(new BorderLayout());
        if (!hideTitleBar) {
            this.menuBar = this.createMenuBar();
            this.add((Component)this.menuBar, "First");
        }
        this.cardLayoutOwner = this.createPanel();
        this.add((Component)this.cardLayoutOwner, "Center");
        this.setBorder(BorderFactory.createEtchedBorder());
        this.genericCards = new HashMap<String, Component>();
    }

    protected JToolBar createMenuBar() {
        JToolBar toolbar = new JToolBar(0);
        toolbar.setFloatable(false);
        toolbar.add(Box.createHorizontalGlue());
        toolbar.add(this.createCloseButton());
        return toolbar;
    }

    protected JButton createCloseButton() {
        JButton closeButton = new JButton();
        closeButton.setText("Fechar");
        closeButton.setIcon(IconFactory.getIconBig("Down.gif"));
        closeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultMultiPurposeDisplayer.this.hideMultiPurposeDisplayer();
            }
        });
        return closeButton;
    }

    protected JPanel createPanel() {
        JPanel panel = new JPanel();
        this.cardLayout = new CardLayout();
        panel.setLayout(this.cardLayout);
        return panel;
    }

    @Override
    public void showMultiPurposeDisplayer() {
        boolean oldIsVisibleValue = this.isVisible;
        this.isVisible = true;
        this.isMaximized = false;
        this.setVisible(this.isVisible);
        for (PropertyChangeListener propertyChangeListener : this.propertyChangeListeners) {
            propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "isVisible", oldIsVisibleValue, this.isVisible));
        }
    }

    @Override
    public void hideMultiPurposeDisplayer() {
        boolean oldIsVisibleValue = this.isVisible;
        this.isVisible = false;
        this.isMaximized = false;
        this.setVisible(this.isVisible);
        for (PropertyChangeListener propertyChangeListener : this.propertyChangeListeners) {
            propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "isVisible", oldIsVisibleValue, this.isVisible));
        }
    }

    @Override
    public void maximizeMultiPurposeDisplayer() {
        this.isMaximized = true;
        this.setVisible(true);
        for (PropertyChangeListener propertyChangeListener : this.propertyChangeListeners) {
            propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "isMaximized", false, true));
        }
    }

    @Override
    public void addIsVisibleListener(PropertyChangeListener propertyChangeListener) {
        this.propertyChangeListeners.add(propertyChangeListener);
    }

    @Override
    public void removeIsVisibleListener(PropertyChangeListener propertyChangeListener) {
        this.propertyChangeListeners.remove(propertyChangeListener);
    }

    @Override
    public void setValidationDisplayer(ValidationDisplayer validationDisplayer) {
        if (validationDisplayer != null) {
            this.validationDisplayer = validationDisplayer;
            this.cardLayoutOwner.add((Component)((JComponent)validationDisplayer), "Center");
            this.cardLayout.addLayoutComponent((JComponent)validationDisplayer, "VALIDATION");
        }
    }

    @Override
    public ValidationDisplayer getValidationDisplayer() {
        return this.validationDisplayer;
    }

    @Override
    public void showValidationDisplayer() {
        this.showMultiPurposeDisplayer();
        this.cardLayout.show(this.cardLayoutOwner, "VALIDATION");
    }

    @Override
    public void setCentralErrorsDisplayer(CentralErrorsDisplayer centralErrorsDisplayer) {
        if (centralErrorsDisplayer != null) {
            this.centralErrorsDisplayer = centralErrorsDisplayer;
            this.cardLayoutOwner.add((Component)((JComponent)centralErrorsDisplayer), "Center");
            this.cardLayout.addLayoutComponent((JComponent)centralErrorsDisplayer, "CENTRAL_ERRORS");
        }
    }

    @Override
    public CentralErrorsDisplayer getCentralErrorsDisplayer() {
        return this.centralErrorsDisplayer;
    }

    @Override
    public void showCentralErrorsDisplayer() {
        this.showMultiPurposeDisplayer();
        this.cardLayout.show(this.cardLayoutOwner, "CENTRAL_ERRORS");
    }

    @Override
    public void setWarningDisplayer(WarningDisplayer warningDisplayer) {
        if (warningDisplayer != null) {
            this.warningDisplayer = warningDisplayer;
            this.cardLayoutOwner.add((Component)((JComponent)warningDisplayer), "Center");
            this.cardLayout.addLayoutComponent((JComponent)warningDisplayer, "WARNING");
        }
    }

    @Override
    public WarningDisplayer getWarningDisplayer() {
        return this.warningDisplayer;
    }

    @Override
    public void showWarningDisplayer() {
        this.showMultiPurposeDisplayer();
        this.cardLayout.show(this.cardLayoutOwner, "WARNING");
    }

    @Override
    public void setHelpWizardDisplayer(HelpWizardDisplayer helpWizardDisplayer) {
        if (helpWizardDisplayer != null) {
            this.helpWizardDisplayer = helpWizardDisplayer;
            this.cardLayoutOwner.add((Component)((JComponent)helpWizardDisplayer), "Center");
            this.cardLayout.addLayoutComponent((JComponent)helpWizardDisplayer, "HELP_WIZARD");
        }
    }

    @Override
    public HelpWizardDisplayer getHelpWizardDisplayer() {
        return this.helpWizardDisplayer;
    }

    @Override
    public void showHelpWizardDisplayer() {
        this.showMultiPurposeDisplayer();
        this.cardLayout.show(this.cardLayoutOwner, "HELP_WIZARD");
    }

    @Override
    public HelpDisplayer createHelpDisplayer() {
        DefaultHelpDisplayer defaultHelpDisplayer = new DefaultHelpDisplayer();
        return defaultHelpDisplayer;
    }

    @Override
    public void setHelpDisplayer(HelpDisplayer helpDisplayer) {
        if (helpDisplayer != null) {
            this.helpDisplayer = helpDisplayer;
            this.cardLayoutOwner.add((Component)helpDisplayer, "Center");
            this.cardLayout.addLayoutComponent((JComponent)helpDisplayer, "HELP");
        }
    }

    @Override
    public HelpDisplayer getHelpDisplayer() {
        return this.helpDisplayer;
    }

    @Override
    public void showHelpDisplayer() {
        this.showMultiPurposeDisplayer();
        this.cardLayout.show(this.cardLayoutOwner, "HELP");
    }

    public void addGenericCard(String cardName, Component card) {
        this.cardLayoutOwner.add(card, "Center");
        this.cardLayout.addLayoutComponent(card, cardName);
        this.genericCards.put(cardName, card);
    }

    public void showGenericCard(String cardName) {
        this.showMultiPurposeDisplayer();
        this.cardLayout.show(this.cardLayoutOwner, cardName);
    }

    public Component getGenericCard(String name) {
        return this.genericCards.get(name);
    }

    @Override
    public boolean isDisplayerVisible() {
        return this.isVisible;
    }

    public boolean isDisplayerMaximized() {
        return this.isMaximized;
    }

}

