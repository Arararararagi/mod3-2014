/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.multi.impl;

import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import javax.swing.JPanel;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.help.HelpWizardDisplayer;
import pt.opensoft.taxclient.ui.layout.DummyPlug;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.ui.validation.CentralErrorsDisplayer;
import pt.opensoft.taxclient.ui.validation.ValidationDisplayer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;

public class DummyMultiPurposeDisplayer
extends JPanel
implements MultiPurposeDisplayer,
DummyPlug {
    public DummyMultiPurposeDisplayer() {
        Dimension theZeroDimension = new Dimension(0, 0);
        this.setMaximumSize(theZeroDimension);
        this.setPreferredSize(theZeroDimension);
        this.setMinimumSize(theZeroDimension);
        this.setVisible(false);
    }

    @Override
    public void setHelpDisplayer(HelpDisplayer helpDisplayer) {
    }

    @Override
    public void setValidationDisplayer(ValidationDisplayer validationDisplayer) {
    }

    @Override
    public void showMultiPurposeDisplayer() {
    }

    @Override
    public void hideMultiPurposeDisplayer() {
    }

    @Override
    public void maximizeMultiPurposeDisplayer() {
    }

    @Override
    public HelpDisplayer getHelpDisplayer() {
        return null;
    }

    @Override
    public ValidationDisplayer getValidationDisplayer() {
        return null;
    }

    @Override
    public HelpDisplayer createHelpDisplayer() {
        return null;
    }

    @Override
    public void showHelpDisplayer() {
    }

    @Override
    public void showValidationDisplayer() {
    }

    @Override
    public void setCentralErrorsDisplayer(CentralErrorsDisplayer centralErrorsDisplayer) {
    }

    @Override
    public CentralErrorsDisplayer getCentralErrorsDisplayer() {
        return null;
    }

    @Override
    public void showCentralErrorsDisplayer() {
    }

    @Override
    public void addIsVisibleListener(PropertyChangeListener propertyChangeListener) {
    }

    @Override
    public void removeIsVisibleListener(PropertyChangeListener propertyChangeListener) {
    }

    @Override
    public boolean isDisplayerVisible() {
        return false;
    }

    @Override
    public WarningDisplayer getWarningDisplayer() {
        return null;
    }

    @Override
    public void setWarningDisplayer(WarningDisplayer warningDisplayer) {
    }

    @Override
    public void showWarningDisplayer() {
    }

    @Override
    public HelpWizardDisplayer getHelpWizardDisplayer() {
        return null;
    }

    @Override
    public void setHelpWizardDisplayer(HelpWizardDisplayer helpWizardDisplayer) {
    }

    @Override
    public void showHelpWizardDisplayer() {
    }
}

