/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation;

import com.jgoodies.validation.ValidationResult;

public interface WarningDisplayer {
    public void fillWarningsList(ValidationResult var1);
}

