/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.impl;

import pt.opensoft.taxclient.ui.forms.Path;

public class DeclValidationKey {
    private final DeclValidationType type;
    private final String anexo;
    private final String quadro;

    public DeclValidationKey(String path) {
        try {
            Path pat = Path.buildPathFromString(path);
            if (Path.isPathToDecl(pat)) {
                this.type = DeclValidationType.DECL;
                this.anexo = "";
                this.quadro = "";
            } else if (Path.isPathToAnexo(pat)) {
                this.type = DeclValidationType.ANEXO;
                this.anexo = Path.extractAnexo(pat);
                this.quadro = "";
            } else {
                this.type = DeclValidationType.QUADRO;
                this.anexo = Path.extractAnexo(pat);
                this.quadro = Path.extractQuadro(pat);
            }
        }
        catch (Exception e) {
            throw new IllegalArgumentException("A path " + path + " n\u00e3o \u00e9 v\u00e1lida. Verifique a sua constru\u00e7\u00e3o.");
        }
    }

    public DeclValidationType getType() {
        return this.type;
    }

    public String getAnexo() {
        return this.anexo;
    }

    public String getQuadro() {
        return this.quadro;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DeclValidationKey)) {
            return false;
        }
        DeclValidationKey obj2 = (DeclValidationKey)obj;
        return this.type.equals((Object)obj2.getType()) && this.anexo.equals(obj2.getAnexo()) && this.quadro.equals(obj2.getQuadro());
    }

    public int hashCode() {
        return this.type.hashCode() * this.anexo.hashCode() * this.quadro.hashCode();
    }

    public String toString() {
        return "type: " + (Object)this.type + " anexo: " + this.anexo + " quadro: " + this.quadro;
    }

    public static enum DeclValidationType {
        DECL,
        ANEXO,
        QUADRO;
        

        private DeclValidationType() {
        }
    }

}

