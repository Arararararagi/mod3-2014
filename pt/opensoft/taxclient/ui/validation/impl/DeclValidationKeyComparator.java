/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.validation.impl.DeclValidationKey;
import pt.opensoft.util.StringUtil;

public class DeclValidationKeyComparator
implements Comparator<DeclValidationKey> {
    private DeclaracaoModel model;
    private FormKey[] anexos;

    public DeclValidationKeyComparator(DeclaracaoModel model) {
        this.model = model;
        ArrayList<AnexoModel> anexoList = new ArrayList<AnexoModel>();
        Set<Map.Entry<FormKey, AnexoModel>> entrySet = this.model.getAnexos().entrySet();
        for (Map.Entry<FormKey, AnexoModel> entry : entrySet) {
            anexoList.add(entry.getValue());
        }
        Collections.sort(anexoList);
        this.anexos = new FormKey[this.model.getAnexos().size()];
        for (int i = 0; i < anexoList.size(); ++i) {
            this.anexos[i] = ((AnexoModel)anexoList.get(i)).getFormKey();
        }
    }

    @Override
    public int compare(DeclValidationKey o1, DeclValidationKey o2) {
        if (o1.getType().equals((Object)DeclValidationKey.DeclValidationType.DECL) && o2.getType().equals((Object)DeclValidationKey.DeclValidationType.DECL)) {
            return 0;
        }
        int typeCompare = this.compareTypes(o1.getType(), o2.getType());
        if (typeCompare == 0) {
            int idsCompare = this.compareAnexos(o1.getAnexo(), o2.getAnexo());
            if (idsCompare == 0) {
                return this.compareQuadros(o1.getAnexo(), o1.getQuadro(), o2.getQuadro());
            }
            return idsCompare;
        }
        return typeCompare;
    }

    private int compareTypes(DeclValidationKey.DeclValidationType node1Type, DeclValidationKey.DeclValidationType node2Type) {
        if (node1Type.equals((Object)DeclValidationKey.DeclValidationType.DECL)) {
            return -1;
        }
        if (node2Type.equals((Object)DeclValidationKey.DeclValidationType.DECL)) {
            return 1;
        }
        return 0;
    }

    private int compareAnexos(String node1, String node2) {
        String[] split = node1.split("\\|");
        String id1 = split[0];
        String subId1 = null;
        if (split.length > 1) {
            subId1 = split[1];
        }
        split = node2.split("\\|");
        String id2 = split[0];
        String subId2 = null;
        if (split.length > 1) {
            subId2 = split[1];
        }
        if (id1.equals(id2)) {
            if (StringUtil.isEmpty(subId1)) {
                if (StringUtil.isEmpty(subId2)) {
                    return 0;
                }
                return -1;
            }
            if (StringUtil.isEmpty(subId2)) {
                return 1;
            }
            return subId1.compareTo(subId2);
        }
        int idx1 = -1;
        int idx2 = -1;
        for (int i = 0; i < this.anexos.length && (idx1 == -1 || idx2 == -1); ++i) {
            FormKey anexo = this.anexos[i];
            if (anexo.getId().equals(id1)) {
                idx1 = i;
            }
            if (!anexo.getId().equals(id2)) continue;
            idx2 = i;
        }
        if (idx1 != -1 && idx2 != -1) {
            return idx1 - idx2;
        }
        return id1.compareTo(id2);
    }

    private int compareQuadros(String anexoId, String quadroId1, String quadroId2) {
        if (StringUtil.isEmpty(quadroId1)) {
            if (StringUtil.isEmpty(quadroId2)) {
                return 0;
            }
            return -1;
        }
        if (StringUtil.isEmpty(quadroId2)) {
            return 1;
        }
        String[] split = anexoId.split("\\|");
        String id = split[0];
        AnexoModel anexoModel = null;
        for (int i = 0; i < this.anexos.length && anexoModel == null; ++i) {
            FormKey formKey = this.anexos[i];
            if (!formKey.getId().equals(id)) continue;
            anexoModel = this.model.getAnexo(formKey);
        }
        if (anexoModel != null) {
            int idx = 0;
            int idx1 = -1;
            int idx2 = -1;
            Iterator<QuadroModel> iterator = anexoModel.getQuadros().iterator();
            while (iterator.hasNext() && (idx1 == -1 || idx2 == -1)) {
                QuadroModel quadroModel = iterator.next();
                if (quadroModel.getClass().getSimpleName().equals(quadroId1)) {
                    idx1 = idx;
                }
                if (quadroModel.getClass().getSimpleName().equals(quadroId2)) {
                    idx2 = idx;
                }
                ++idx;
            }
            return idx1 - idx2;
        }
        return quadroId1.compareTo(quadroId2);
    }
}

