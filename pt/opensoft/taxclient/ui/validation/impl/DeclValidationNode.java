/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.impl;

import java.util.ArrayList;
import java.util.List;
import pt.opensoft.taxclient.gui.DeclValidationMessage;

public class DeclValidationNode {
    private final DeclValidationNodeType type;
    private final String id;
    private List<DeclValidationMessage> errors;
    private List<DeclValidationNode> childs;

    public DeclValidationNode(DeclValidationNodeType type, String id) {
        this.type = type;
        this.id = id;
        this.errors = new ArrayList<DeclValidationMessage>();
        this.childs = new ArrayList<DeclValidationNode>();
    }

    public void addError(DeclValidationMessage error) {
        this.errors.add(error);
    }

    public void addChild(DeclValidationNode child) {
        this.childs.add(child);
    }

    public DeclValidationNodeType getType() {
        return this.type;
    }

    public String getId() {
        return this.id;
    }

    public List<DeclValidationMessage> getErrors() {
        return this.errors;
    }

    public List<DeclValidationNode> getChilds() {
        return this.childs;
    }

    public static enum DeclValidationNodeType {
        DECL,
        ANEXO,
        QUADRO;
        

        private DeclValidationNodeType() {
        }
    }

}

