/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.impl;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.validation.impl.DeclValidationKey;
import pt.opensoft.taxclient.ui.validation.impl.DefaultErrorsValidationRenderer;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class DefaultWarningsValidationRenderer
extends DefaultErrorsValidationRenderer {
    private static final String DEFAULT_WARNINGS_FOOTER = "Se submeter a Declara\u00e7\u00e3o tal como se encontra, a mesma poder\u00e1 ser seleccionada para posterior an\u00e1lise.";

    public DefaultWarningsValidationRenderer(DeclaracaoModel model) {
        super(model);
        this.NO_ERRORS_TITLE = DesignationManager.NO_WARNINGS_TITLE;
        this.ERRORS_TITLE = DesignationManager.WARNINGS_TITLE;
        this.DECL_ERRORS_TITLE = DesignationManager.DECL_WARNINGS_TITLE;
        this.ANEXO_ERRORS_TITLE = DesignationManager.ANEXO_WARNINGS_TITLE;
        this.QUADRO_ERRORS_TITLE = DesignationManager.QUADRO_WARNINGS_TITLE;
    }

    public String renderHTMLValidationWarnings(ValidationResult validationResult, Pair warningsTitles, String infoWarningTitle, String warningsFooter) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<html><body>");
        List<ValidationMessage> messages = validationResult.getMessages();
        if (messages.size() == 0) {
            buffer.append("<h1>" + StringUtil.toHtml(this.NO_ERRORS_TITLE) + "</h1>");
        } else {
            buffer.append("<h1>" + StringUtil.toHtml(this.ERRORS_TITLE.replace((CharSequence)"@VALUE", (CharSequence)Integer.toString(messages.size()))) + "</h1>");
            ValidationResult warnings = null;
            ValidationResult infoWarnings = null;
            if (!StringUtil.isEmpty(infoWarningTitle)) {
                warnings = new ValidationResult();
                infoWarnings = new ValidationResult();
                for (int k = 0; k < messages.size(); ++k) {
                    if (messages.get(k).severity() == Severity.WARNING) {
                        infoWarnings.add(messages.get(k));
                        continue;
                    }
                    if (messages.get(k).severity() != Severity.ERROR) continue;
                    warnings.add(messages.get(k));
                }
            }
            if (!(infoWarnings == null || infoWarnings.getMessages().size() == 0 || StringUtil.isEmpty(infoWarningTitle))) {
                this.append(infoWarnings.getMessages(), buffer, infoWarningTitle);
                if (warnings != null && warnings.getMessages().size() != 0 && warningsTitles != null) {
                    this.append(warnings.getMessages(), buffer, (String)warningsTitles.getSecond());
                    buffer.append("<h1>" + StringUtil.toHtml(StringUtil.isEmpty(warningsFooter) ? "Se submeter a Declara\u00e7\u00e3o tal como se encontra, a mesma poder\u00e1 ser seleccionada para posterior an\u00e1lise." : warningsFooter) + "</h1>");
                }
            } else if (warnings != null && warnings.getMessages().size() != 0 && warningsTitles != null) {
                this.append(warnings.getMessages(), buffer, (String)warningsTitles.getFirst());
                buffer.append("<h1>" + StringUtil.toHtml(StringUtil.isEmpty(warningsFooter) ? "Se submeter a Declara\u00e7\u00e3o tal como se encontra, a mesma poder\u00e1 ser seleccionada para posterior an\u00e1lise." : warningsFooter) + "</h1>");
            }
        }
        buffer.append("</body></html>");
        return buffer.toString();
    }

    private void append(List<ValidationMessage> messages, StringBuffer buffer, String title) {
        TreeMap<DeclValidationKey, List<DeclValidationMessage>> orderedMap = this.orderErrorList(messages);
        DeclValidationKey[] keyset = new DeclValidationKey[orderedMap.size()];
        keyset = orderedMap.keySet().toArray(keyset);
        buffer.append(this.renderDecl(keyset, orderedMap, title));
    }

    protected String renderDecl(DeclValidationKey[] keys, Map<DeclValidationKey, List<DeclValidationMessage>> map, String title) {
        Pair pair;
        StringBuffer buffer = new StringBuffer();
        int current = 0;
        buffer.append("<h2>" + StringUtil.toHtml(title) + "</h2>");
        buffer.append("<ul class=\"" + this.DECL_ERRORS_CSS_CLASS + "\" >");
        if (keys[current].getType().equals((Object)DeclValidationKey.DeclValidationType.DECL)) {
            List<DeclValidationMessage> warnings = map.get(keys[current]);
            for (DeclValidationMessage warning : warnings) {
                buffer.append(this.getErrorHtml(warning, this.DECL_ERROR_CSS_CLASS));
            }
            if (++current >= keys.length) {
                buffer.append("</ul>");
                return buffer.toString();
            }
        }
        buffer.append("<li class=\"" + this.DECL_ERRORS_ANEXOS_CSS_CLASS + "\">");
        do {
            pair = this.renderAnexo(current, keys, map);
            buffer.append((String)pair.getFirst());
        } while ((current = ((Integer)pair.getSecond()).intValue()) < keys.length);
        buffer.append("</li>");
        buffer.append("</ul>");
        return buffer.toString();
    }
}

