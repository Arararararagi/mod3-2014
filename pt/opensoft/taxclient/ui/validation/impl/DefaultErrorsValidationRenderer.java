/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.impl;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.validation.ErrorValidationRenderer;
import pt.opensoft.taxclient.ui.validation.impl.DeclValidationKey;
import pt.opensoft.taxclient.ui.validation.util.ErrorValidationRendererUtil;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class DefaultErrorsValidationRenderer
extends ErrorValidationRenderer {
    protected DeclaracaoModel model;
    protected String NO_ERRORS_TITLE = "A declara\u00e7\u00e3o n\u00e3o apresenta erros.";
    protected String ERRORS_TITLE = "Foram detectados @VALUE erros na Declara\u00e7\u00e3o";
    protected String DECL_ERRORS_TITLE = "Erros Gerais da Declara\u00e7\u00e3o:";
    protected String DECL_ERRORS_CSS_CLASS = "declerrors";
    protected String DECL_ERROR_CSS_CLASS = "declerror";
    protected String DECL_ERRORS_ANEXOS_CSS_CLASS = "declerrorsanexos";
    protected String ANEXO_ERRORS_TITLE = DesignationManager.ANEXO_ERRORS_TITLE;
    protected String DECL_ANEXOS_CSS_CLASS = "anexoerrors";
    protected String DECL_ANEXO_CSS_CLASS = "anexoerror";
    protected String ANEXOS_ERRORS_QUADROS_CSS_CLASS = "anexoserrorsquadros";
    protected String QUADRO_ERRORS_TITLE = DesignationManager.QUADRO_ERRORS_TITLE;
    protected String QUADRO_ERRORS_CSS_CLASS = "declerrors";
    protected String QUADRO_ERROR_CSS_CLASS = "quadroerror";

    public DefaultErrorsValidationRenderer(DeclaracaoModel model) {
        this.model = model;
    }

    @Override
    public String renderHTMLValidationErrors(ValidationResult validationResult) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<html><body>");
        List<ValidationMessage> errors = validationResult.getErrors();
        if (errors.size() == 0) {
            buffer.append("<h1>" + StringUtil.toHtml(this.NO_ERRORS_TITLE) + "</h1>");
        } else {
            TreeMap<DeclValidationKey, List<DeclValidationMessage>> orderedMap = this.orderErrorList(errors);
            buffer.append("<h1>" + StringUtil.toHtml(this.ERRORS_TITLE.replace((CharSequence)"@VALUE", (CharSequence)Integer.toString(errors.size()))) + "</h1>");
            DeclValidationKey[] keyset = new DeclValidationKey[orderedMap.size()];
            keyset = orderedMap.keySet().toArray(keyset);
            buffer.append(this.renderDecl(keyset, orderedMap));
        }
        buffer.append("</body></html>");
        return buffer.toString();
    }

    protected String renderDecl(DeclValidationKey[] keys, Map<DeclValidationKey, List<DeclValidationMessage>> map) {
        Pair pair;
        StringBuffer buffer = new StringBuffer();
        int current = 0;
        buffer.append("<h2>" + StringUtil.toHtml(this.DECL_ERRORS_TITLE) + "</h2>");
        buffer.append("<ul class=\"" + this.DECL_ERRORS_CSS_CLASS + "\" >");
        if (keys[current].getType().equals((Object)DeclValidationKey.DeclValidationType.DECL)) {
            List<DeclValidationMessage> errors = map.get(keys[current]);
            for (DeclValidationMessage error : errors) {
                buffer.append(this.getErrorHtml(error, this.DECL_ERROR_CSS_CLASS));
            }
            if (++current >= keys.length) {
                buffer.append("</ul>");
                return buffer.toString();
            }
        }
        buffer.append("<li class=\"" + this.DECL_ERRORS_ANEXOS_CSS_CLASS + "\">");
        do {
            pair = this.renderAnexo(current, keys, map);
            buffer.append((String)pair.getFirst());
        } while ((current = ((Integer)pair.getSecond()).intValue()) < keys.length);
        buffer.append("</li>");
        buffer.append("</ul>");
        return buffer.toString();
    }

    protected Pair renderAnexo(int firstKey, DeclValidationKey[] keys, Map<DeclValidationKey, List<DeclValidationMessage>> map) {
        StringBuffer buffer = new StringBuffer();
        int current = firstKey;
        String[] anexo = keys[firstKey].getAnexo().split("\\|");
        if (anexo.length > 1) {
            buffer.append("<h3>" + this.ANEXO_ERRORS_TITLE + DefaultErrorsValidationRenderer.getAnexoErrorDesignation(anexo[0], anexo[1]) + "</h3>");
        } else {
            buffer.append("<h3>" + this.ANEXO_ERRORS_TITLE + DefaultErrorsValidationRenderer.getAnexoErrorDesignation(anexo[0], null) + "</h3>");
        }
        buffer.append("<ul class=\"" + this.DECL_ANEXOS_CSS_CLASS + "\">");
        if (keys[current].getType().equals((Object)DeclValidationKey.DeclValidationType.ANEXO)) {
            List<DeclValidationMessage> errors = map.get(keys[current]);
            for (DeclValidationMessage error : errors) {
                buffer.append(this.getErrorHtml(error, this.DECL_ANEXO_CSS_CLASS));
            }
            if (!(++current < keys.length && keys[current].getAnexo().equals(keys[firstKey].getAnexo()))) {
                buffer.append("</ul>");
                return new Pair<String, Integer>(buffer.toString(), new Integer(current));
            }
        }
        buffer.append("<li class=\"" + this.ANEXOS_ERRORS_QUADROS_CSS_CLASS + "\">");
        do {
            buffer.append(this.renderQuadro(keys[firstKey].getAnexo(), keys[current].getQuadro(), map.get(keys[current])));
        } while (++current < keys.length && keys[current].getAnexo().equals(keys[firstKey].getAnexo()));
        buffer.append("</li>");
        buffer.append("</ul>");
        return new Pair<String, Integer>(buffer.toString(), new Integer(current));
    }

    protected String renderQuadro(String anexo, String quadro, List<DeclValidationMessage> errors) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<h4>" + this.QUADRO_ERRORS_TITLE + DefaultErrorsValidationRenderer.getQuadroErrorDesignation(anexo.split("\\|")[0], quadro) + "</h4>");
        buffer.append("<ul class=\"" + this.QUADRO_ERRORS_CSS_CLASS + "\">");
        for (DeclValidationMessage validationMessage : errors) {
            buffer.append(this.getErrorHtml(validationMessage, this.QUADRO_ERROR_CSS_CLASS));
        }
        buffer.append("</ul>");
        return buffer.toString();
    }

    protected String getErrorHtml(DeclValidationMessage error, String css) {
        return "<li class=\"" + css + "\">" + this.getErrorHtml(error) + "</li>";
    }

    protected TreeMap<DeclValidationKey, List<DeclValidationMessage>> orderErrorList(List<ValidationMessage> errors) {
        return ErrorValidationRendererUtil.orderErrorList(errors, this.model);
    }

    protected static String getAnexoErrorDesignation(String anexo, String subId) {
        String key = anexo.toLowerCase() + ".error.designation";
        String value = DesignationManager.getInstance().getString(key);
        if (value == null) {
            throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
        }
        return DefaultErrorsValidationRenderer.getAnexoErrorDesignationAsString(value, subId);
    }

    protected static String getQuadroErrorDesignation(String anexo, String quadro) {
        String key = anexo.toLowerCase() + "." + quadro.toLowerCase() + ".error.designation";
        String value = DesignationManager.getInstance().getString(key);
        if (value == null) {
            throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
        }
        return value;
    }

    protected static String getAnexoErrorDesignationAsString(String value, String subId) {
        if (subId == null) {
            return value;
        }
        return value + " (" + subId + ")";
    }
}

