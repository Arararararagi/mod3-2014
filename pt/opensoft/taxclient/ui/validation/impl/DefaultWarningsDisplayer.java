/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.impl;

import com.jgoodies.validation.ValidationResult;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.net.URL;
import javax.accessibility.AccessibleContext;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.Util;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.validation.ErrorValidationRenderer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;
import pt.opensoft.taxclient.ui.validation.impl.DefaultWarningsValidationRenderer;
import pt.opensoft.taxclient.util.Session;

public class DefaultWarningsDisplayer
extends JPanel
implements HyperlinkListener,
WarningDisplayer {
    private ScrollableHtmlPane scrollableHtmlPane;
    private JScrollPane scrollPane;
    private ErrorValidationRenderer renderer;

    public DefaultWarningsDisplayer(String stylesheetFile, DeclaracaoModel model) {
        super(new BorderLayout());
        this.renderer = new DefaultWarningsValidationRenderer(model);
        this.scrollableHtmlPane = new ScrollableHtmlPane();
        this.scrollableHtmlPane.setEditable(false);
        this.scrollableHtmlPane.setContentType("text/html");
        this.scrollableHtmlPane.addHyperlinkListener(this);
        StyleSheet css = new StyleSheet();
        css.importStyleSheet(Util.class.getResource("/" + stylesheetFile));
        HTMLEditorKit edtKit = new HTMLEditorKit();
        edtKit.setStyleSheet(css);
        this.scrollableHtmlPane.setEditorKit(edtKit);
        this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
        this.changeFont();
        this.scrollPane = new JScrollPane(this.scrollableHtmlPane);
        this.scrollPane.getVerticalScrollBar().setUnitIncrement(this.scrollPane.getVerticalScrollBar().getBlockIncrement());
        this.add(this.scrollPane);
    }

    @Override
    public void fillWarningsList(ValidationResult warningResult) {
        String html = this.renderer.renderHTMLValidationErrors(warningResult);
        this.scrollableHtmlPane.setText(html);
        this.getAccessibleContext().setAccessibleDescription(html);
        this.changeFont();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            String path = e.getDescription();
            Session.getMainFrame().getDeclarationDisplayer().goToPath(path);
        }
    }

    private void changeFont() {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)this.scrollableHtmlPane.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }
}

