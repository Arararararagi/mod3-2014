/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.impl;

import com.jgoodies.validation.ValidationResult;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.net.URL;
import javax.accessibility.AccessibleContext;
import javax.swing.Icon;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.TitledPanel2;
import pt.opensoft.swing.Util;
import pt.opensoft.taxclient.actions.util.CloseBottomPanelAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.validation.WarningDisplayer;
import pt.opensoft.taxclient.ui.validation.impl.DefaultWarningsValidationRenderer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class TitledWarningsDisplayer
extends TitledPanel2
implements HyperlinkListener,
WarningDisplayer {
    private static final long serialVersionUID = -382768400830389716L;
    private ScrollableHtmlPane scrollableHtmlPane;
    private JScrollPane scrollPane;
    private DefaultWarningsValidationRenderer renderer;
    private Pair _warningsTitles;
    private String _infoWarningsTitle;
    private String _warningsFooter;

    public TitledWarningsDisplayer(String stylesheetFile, DeclaracaoModel model) {
        this(stylesheetFile, model, null, null);
    }

    public TitledWarningsDisplayer(String stylesheetFile, DeclaracaoModel model, Pair warningsTitles, String infoWarningsTitle) {
        this(stylesheetFile, model, warningsTitles, infoWarningsTitle, "Avisos da declara\u00e7\u00e3o", "Fechar painel de avisos");
    }

    public TitledWarningsDisplayer(String stylesheetFile, DeclaracaoModel model, Pair warningsTitles, String infoWarningsTitle, String toolbarTitle, String closeButtonTitle) {
        this(stylesheetFile, model, warningsTitles, infoWarningsTitle, "", toolbarTitle, closeButtonTitle);
    }

    public TitledWarningsDisplayer(String stylesheetFile, DeclaracaoModel model, Pair warningsTitles, String infoWarningsTitle, String warningsFooter, String toolbarTitle, String closeButtonTitle) {
        super(IconFactory.getIconBig(GUIParameters.ICON_ERRO), toolbarTitle, new CloseBottomPanelAction(closeButtonTitle), GUIParameters.TITLED_PANEL_TITLE_COLOR, null);
        this._warningsTitles = warningsTitles;
        this._infoWarningsTitle = infoWarningsTitle;
        this._warningsFooter = warningsFooter;
        this.renderer = new DefaultWarningsValidationRenderer(model);
        this.scrollableHtmlPane = new ScrollableHtmlPane();
        this.scrollableHtmlPane.setEditable(false);
        this.scrollableHtmlPane.setContentType("text/html");
        this.scrollableHtmlPane.addHyperlinkListener(this);
        StyleSheet css = new StyleSheet();
        css.importStyleSheet(Util.class.getResource("/" + stylesheetFile));
        HTMLEditorKit edtKit = new HTMLEditorKit();
        edtKit.setStyleSheet(css);
        this.scrollableHtmlPane.setEditorKit(edtKit);
        this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
        this.changeFont();
        this.scrollPane = new JScrollPane(this.scrollableHtmlPane);
        this.scrollPane.getVerticalScrollBar().setUnitIncrement(this.scrollPane.getVerticalScrollBar().getBlockIncrement());
        this.add(this.scrollPane);
    }

    @Override
    public void fillWarningsList(ValidationResult validationResult) {
        String html = "";
        html = this._warningsTitles == null && StringUtil.isEmpty(this._infoWarningsTitle) ? this.renderer.renderHTMLValidationErrors(validationResult) : this.renderer.renderHTMLValidationWarnings(validationResult, this._warningsTitles, this._infoWarningsTitle, this._warningsFooter);
        this.scrollableHtmlPane.setText(html);
        this.getAccessibleContext().setAccessibleDescription(html);
        this.changeFont();
    }

    @Override
    protected Color getTextForeground(boolean selected) {
        Color c = UIManager.getColor("TitledPanel3Erros.foreground");
        if (c != null) {
            return c;
        }
        return UIManager.getColor(selected ? "InternalFrame.activeTitleForeground" : "Label.foreground");
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            String path = e.getDescription();
            Session.getMainFrame().getDeclarationDisplayer().goToPath(path);
        }
    }

    private void changeFont() {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)this.scrollableHtmlPane.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }
}

