/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation;

import com.jgoodies.validation.ValidationResult;
import pt.opensoft.taxclient.app.ErrorsParameters;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.ui.validation.util.ErrorValidationRendererUtil;
import pt.opensoft.util.StringUtil;

public abstract class ErrorValidationRenderer {
    public static final String LINE_TOKEN = "\\?\\d";
    public static final String START_LINK_PATTERN = "\\{\\d\\d";
    protected static ErrorsParameters erros;
    public static final int COUNTER_LENGTH = 2;
    public static final String START_LINK = "{";
    public static final String END_LINK = "}";
    public static final String TAG_REPLACE = "|R|";
    public static final String ERRO_FORMATO = "E_Formato";

    public static void setParameters(ErrorsParameters param) {
        erros = param;
    }

    protected String getErrorHtml(DeclValidationMessage error) {
        if (erros == null) {
            throw new RuntimeException("Os parametros da aplica\u00e7\u00e3o n\u00e3o foram inicializados correctamente. N\u00e3o \u00e9 poss\u00edvel obter as mensagens de erros.");
        }
        String msg = erros.getError("showErrorCode");
        String errorCode = "";
        if (msg != null && msg.equalsIgnoreCase("yes")) {
            errorCode = error.get_code() + " : ";
        }
        return errorCode + this.renderErrorHtml(error);
    }

    protected String renderErrorHtml(DeclValidationMessage error) {
        String msg = error.get_message();
        if (StringUtil.isEmpty(msg)) {
            msg = erros.getError(error.get_code());
        }
        if (msg == null) {
            throw new RuntimeException("O c\u00f3digo de erro " + error.get_code() + " n\u00e3o foi encontrado no ficheiro de properties.");
        }
        msg = StringUtil.toHtml(msg, true);
        msg = ErrorValidationRendererUtil.getMessageWithLinks(msg, error);
        return msg;
    }

    public abstract String renderHTMLValidationErrors(ValidationResult var1);
}

