/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation.util;

import com.jgoodies.validation.ValidationMessage;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.validation.impl.DeclValidationKey;
import pt.opensoft.taxclient.ui.validation.impl.DeclValidationKeyComparator;
import pt.opensoft.util.StringUtil;

public abstract class ErrorValidationRendererUtil {
    private static final String LINK_PATTERN = "\\{\\d\\d[^\\}]+\\}";
    public static final String LINE_TOKEN = "\\?\\d";
    public static final String START_LINK_PATTERN = "\\{\\d\\d";
    public static final int COUNTER_LENGTH = 2;
    public static final String START_LINK = "{";
    public static final String END_LINK = "}";
    public static final String TAG_REPLACE = "|R|";
    public static final String ERRO_FORMATO = "E_Formato";

    public static String getMessage(String msg, DeclValidationMessage error) {
        String[] replaces = error.get_replaces();
        msg = ErrorValidationRendererUtil.fillReplaces(msg, replaces);
        return ErrorValidationRendererUtil.removeLinks(msg);
    }

    public static String getMessageWithLinks(String msg, DeclValidationMessage error) {
        String[] replaces = error.get_replaces();
        msg = ErrorValidationRendererUtil.fillReplaces(msg, replaces);
        String[] links = error.get_links();
        return ErrorValidationRendererUtil.fillLinks(msg, links, error.get_code());
    }

    public static TreeMap<DeclValidationKey, List<DeclValidationMessage>> orderErrorList(List<ValidationMessage> errors, DeclaracaoModel model) {
        TreeMap<DeclValidationKey, List<DeclValidationMessage>> map = new TreeMap<DeclValidationKey, List<DeclValidationMessage>>(new DeclValidationKeyComparator(model));
        for (DeclValidationMessage validationMessage : errors) {
            List associatedErrors;
            DeclValidationKey key = new DeclValidationKey(validationMessage.get_fieldReference());
            if (map.containsKey(key)) {
                associatedErrors = map.get(key);
            } else {
                associatedErrors = new ArrayList();
                map.put(key, associatedErrors);
            }
            associatedErrors.add(validationMessage);
        }
        return map;
    }

    private static String fillReplaces(String msg, String[] replaces) {
        if (replaces != null) {
            int msgIndex;
            while ((msgIndex = msg.indexOf("|R|")) != -1) {
                try {
                    int replaceIndex = Integer.parseInt(msg.substring(msgIndex + "|R|".length(), msgIndex + "|R|".length() + 2));
                    if (replaceIndex >= replaces.length) {
                        throw new RuntimeException(new ParseException("Falta de parametros para substituir os replaces \n\t\t" + msg, msgIndex));
                    }
                    msg = StringUtil.replaceFirst(msg, msg.substring(msgIndex, msgIndex + "|R|".length() + 2), replaces[replaceIndex]);
                    continue;
                }
                catch (NumberFormatException nfe) {
                    throw new RuntimeException(new ParseException("Mensagem no formato errado\n\t\t" + msg, msgIndex));
                }
            }
        }
        return msg;
    }

    private static String fillLinks(String msg, String[] links, String errorCode) {
        if (links != null && links.length != 0) {
            int msgIndex;
            if (!Pattern.compile("\\{\\d\\d[^\\}]+\\}").matcher((CharSequence)msg).find()) {
                throw new RuntimeException("O erro " + errorCode + " est\u00e1 sintaticamente mal especificado no ficheiro de properties.");
            }
            msg = StringUtil.replace(msg, "}", "</a>");
            while ((msgIndex = msg.indexOf("{")) != -1) {
                try {
                    int lnkIndex = Integer.parseInt(msg.substring(msgIndex + "{".length(), msgIndex + "{".length() + 2));
                    if (lnkIndex >= links.length) {
                        throw new RuntimeException(new ParseException("Falta de parametros para substituir links \n\t\t" + msg, msgIndex));
                    }
                    msg = StringUtil.replaceFirst(msg, msg.substring(msgIndex, msgIndex + "{".length() + 2), "<a href=\"" + links[lnkIndex] + "\">");
                    continue;
                }
                catch (NumberFormatException nfe) {
                    throw new RuntimeException(new ParseException("Mensagem no formato errado\n\t\t" + msg, msgIndex));
                }
            }
        }
        return msg;
    }

    private static String removeLinks(String msg) {
        msg = msg.replaceAll("\\{\\d\\d", "");
        return StringUtil.replace(msg, "}", "");
    }
}

