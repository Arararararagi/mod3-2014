/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.validation;

import com.jgoodies.validation.ValidationResult;

public interface CentralErrorsDisplayer {
    public void fillErrorsList(ValidationResult var1);
}

