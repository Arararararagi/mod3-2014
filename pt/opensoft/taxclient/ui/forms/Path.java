/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.forms;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import pt.opensoft.util.StringUtil;

public class Path {
    public static final String SEPARATOR = ".";
    private static final String PATTERN_SEPARATOR = "\\.";
    public static final String SUBID_SEPARATOR = "|";
    private static final String SUB_QUADRO_ID_SEPARATOR = "|";
    private static final String PATTERN_ANEXO = (Object)Element.ANEXO + "[^\\.]+";
    private static final String PATTERN_SUBID = "[^\\.]+";
    private static final String PATTERN_SUB_QUADRO = "[^\\.]+";
    private static final String PATTERN_QUADRO = (Object)Element.QUADRO + "[^\\.]+";
    private static final String PATTERN_CAMPO = (Object)Element.CAMPO + "[^\\.]+";
    private static final String PATTERN_TABELA = (Object)Element.TABELA + "[^\\.]+";
    private static final String PATTERN_LINHA = (Object)Element.LINHA + "[^\\.]+";
    private static final String PATTERN_COLUNA = (Object)Element.COLUNA + "[^\\.]+";
    private static final String PATTERN_QUADRO_REPETITIVO = (Object)Element.QUADRO_REPETITIVO + "[^\\.]+";
    private static final String PATTERN_CAMPO_QUADRO_REPETITIVO = (Object)Element.CAMPO_QUADRO_REPETITIVO + "[^\\.]+";
    private static final String PATTERN_TABLE_PATH = PATTERN_TABELA + "(" + "\\." + PATTERN_LINHA + "(" + "\\." + PATTERN_COLUNA + ")?)?";
    private static final String PATTERN_QUADRO_REPETITIVO_PATH = PATTERN_QUADRO_REPETITIVO + "" + "|" + "[^\\.]+" + "(" + "\\." + PATTERN_CAMPO_QUADRO_REPETITIVO + ")?";
    private static final String PATTERN_PATH = "(" + PATTERN_ANEXO + "((" + "|" + "[^\\.]+" + ")?" + "\\." + PATTERN_QUADRO + "(" + "\\." + "(" + PATTERN_CAMPO + "|" + PATTERN_TABLE_PATH + "|" + PATTERN_QUADRO_REPETITIVO_PATH + "))?)?)?";
    private Path parent;
    private Path child;
    private String definition;
    private Element element;

    public Path(Element element, String definition) {
        this(null, null, element, definition);
    }

    public Path(Path child, Element element, String definition) {
        this(null, child, element, definition);
    }

    public Path(Path parent, Path child, Element element, String definition) {
        if (element == null) {
            throw new IllegalArgumentException("Element cannot be null.");
        }
        if (StringUtil.isEmpty(definition)) {
            throw new IllegalArgumentException("Definition cannot be null or empty.");
        }
        this.setParent(parent);
        this.setChild(child);
        this.element = element;
        this.definition = definition;
    }

    public static Path buildPathFromString(String pathAsString) {
        if (pathAsString == null) {
            return null;
        }
        if (!Pattern.matches(PATTERN_PATH, (CharSequence)pathAsString)) {
            throw new IllegalArgumentException("Argument does not match a path pattern.");
        }
        List<String> split = Arrays.asList(pathAsString.split("\\."));
        String beginning = split.get(0);
        Path path = null;
        Path root = null;
        for (String splinter : split) {
            if (splinter.startsWith(Element.ANEXO.toString())) {
                root = path = new Path(Element.ANEXO, beginning.replaceFirst(Element.ANEXO.toString(), ""));
            }
            if (splinter.startsWith(Element.QUADRO.toString())) {
                path.setChild(new Path(Element.QUADRO, splinter.replaceFirst(Element.QUADRO.toString(), "")));
                path = path.getChild();
                continue;
            }
            if (splinter.startsWith(Element.CAMPO.toString())) {
                path.setChild(new Path(Element.CAMPO, splinter.replaceFirst(Element.CAMPO.toString(), "")));
                path = path.getChild();
                continue;
            }
            if (splinter.startsWith(Element.TABELA.toString())) {
                path.setChild(new Path(Element.TABELA, splinter.replaceFirst(Element.TABELA.toString(), "")));
                path = path.getChild();
                continue;
            }
            if (splinter.startsWith(Element.COLUNA.toString())) {
                path.setChild(new Path(Element.COLUNA, splinter.replaceFirst(Element.COLUNA.toString(), "")));
                path = path.getChild();
                continue;
            }
            if (splinter.startsWith(Element.LINHA.toString())) {
                path.setChild(new Path(Element.LINHA, splinter.replaceFirst(Element.LINHA.toString(), "")));
                path = path.getChild();
                continue;
            }
            if (splinter.startsWith(Element.CAMPO_QUADRO_REPETITIVO.toString())) {
                path.setChild(new Path(Element.CAMPO_QUADRO_REPETITIVO, splinter.replaceFirst(Element.CAMPO_QUADRO_REPETITIVO.toString(), "")));
                path = path.getChild();
                continue;
            }
            if (!splinter.startsWith(Element.QUADRO_REPETITIVO.toString())) continue;
            path.setChild(new Path(Element.QUADRO_REPETITIVO, splinter.replaceFirst(Element.QUADRO_REPETITIVO.toString(), "")));
            path = path.getChild();
        }
        return root;
    }

    public static String getPathAsString(Path path) {
        if (path != null) {
            String pathAsString = (Object)path.getElement() + path.getDefinition();
            for (Path child = path.getChild(); child != null; child = child.getChild()) {
                pathAsString = pathAsString + "." + (Object)child.getElement() + child.getDefinition();
            }
            return pathAsString;
        }
        return null;
    }

    private static String extract(Path path, Element element) {
        if (path != null) {
            if (path.getElement().equals((Object)element)) {
                return path.definition;
            }
            if (path.getElement().getDepth() < element.getDepth()) {
                return Path.extract(path.getChild(), element);
            }
            if (path.getElement().getDepth() > element.getDepth()) {
                return Path.extract(path.getParent(), element);
            }
        }
        return null;
    }

    public static String extractAnexo(Path path) {
        return Path.extract(path, Element.ANEXO);
    }

    public static String extractQuadro(Path path) {
        return Path.extract(path, Element.QUADRO);
    }

    public static String extractCampo(Path path) {
        return Path.extract(path, Element.CAMPO);
    }

    public static String extractTabela(Path path) {
        return Path.extract(path, Element.TABELA);
    }

    public static String extractLinha(Path path) {
        return Path.extract(path, Element.LINHA);
    }

    public static String extractColuna(Path path) {
        return Path.extract(path, Element.COLUNA);
    }

    public static String extractQuadroRepetitivo(Path path) {
        return Path.extract(path, Element.QUADRO_REPETITIVO);
    }

    public static String extractCampoQuadroRepetitivo(Path path) {
        return Path.extract(path, Element.CAMPO_QUADRO_REPETITIVO);
    }

    public static boolean isPathToDecl(Path path) {
        return StringUtil.isEmpty(Path.extractAnexo(path));
    }

    public static boolean isPathToAnexo(Path path) {
        return !StringUtil.isEmpty(Path.extractAnexo(path)) && StringUtil.isEmpty(Path.extractQuadro(path));
    }

    public static boolean isPathToQuadro(Path path) {
        return !StringUtil.isEmpty(Path.extractAnexo(path)) && !StringUtil.isEmpty(Path.extractQuadro(path)) && StringUtil.isEmpty(Path.extractCampo(path)) && StringUtil.isEmpty(Path.extractTabela(path)) && StringUtil.isEmpty(Path.extractQuadroRepetitivo(path));
    }

    public static boolean isPathToCampo(Path path) {
        return !StringUtil.isEmpty(Path.extractAnexo(path)) && !StringUtil.isEmpty(Path.extractQuadro(path)) && !StringUtil.isEmpty(Path.extractCampo(path));
    }

    public static boolean isPathToTabela(Path path) {
        return !StringUtil.isEmpty(Path.extractAnexo(path)) && !StringUtil.isEmpty(Path.extractQuadro(path)) && !StringUtil.isEmpty(Path.extractTabela(path)) && StringUtil.isEmpty(Path.extractLinha(path));
    }

    public static boolean isPathToTabelaLinha(Path path) {
        return !StringUtil.isEmpty(Path.extractAnexo(path)) && !StringUtil.isEmpty(Path.extractQuadro(path)) && !StringUtil.isEmpty(Path.extractTabela(path)) && !StringUtil.isEmpty(Path.extractLinha(path)) && StringUtil.isEmpty(Path.extractColuna(path));
    }

    public static boolean isPathToTabelaColuna(Path path) {
        return !StringUtil.isEmpty(Path.extractAnexo(path)) && !StringUtil.isEmpty(Path.extractQuadro(path)) && !StringUtil.isEmpty(Path.extractTabela(path)) && !StringUtil.isEmpty(Path.extractLinha(path)) && !StringUtil.isEmpty(Path.extractColuna(path));
    }

    public static boolean isPathToCampoQuadroRepetitivo(Path path) {
        return !StringUtil.isEmpty(Path.extractAnexo(path)) && !StringUtil.isEmpty(Path.extractQuadro(path)) && !StringUtil.isEmpty(Path.extractQuadroRepetitivo(path)) && !StringUtil.isEmpty(Path.extractCampoQuadroRepetitivo(path));
    }

    public String toString() {
        return Path.getPathAsString(this);
    }

    public int hashCode() {
        int acum = this.element.hashCode() + this.definition.hashCode();
        for (Path path = this.getChild(); path != null; path = path.getChild()) {
            acum+=path.element.hashCode() + path.definition.hashCode();
        }
        return acum;
    }

    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Path) {
            Path pathToCompare = (Path)obj;
            return this.element.equals((Object)pathToCompare.getElement()) && this.definition.equals(pathToCompare.getDefinition());
        }
        return false;
    }

    public boolean isBeginning() {
        return this.parent == null;
    }

    public boolean isEnd() {
        return this.child == null;
    }

    public void setParent(Path parent) {
        if (parent != null) {
            if (this.equals(parent)) {
                throw new IllegalArgumentException("Path element cannot have himself as a parent");
            }
            if (this.element.getDepth() - 1 != parent.getElement().getDepth()) {
                throw new IllegalArgumentException("Invalid parent depth (" + parent.getElement().getDepth() + ") for path element depth (" + this.element.getDepth() + ")");
            }
        }
        this.parent = parent;
        if (parent != null && parent.getChild() != this) {
            parent.setChild(this);
        }
    }

    public Path getParent() {
        return this.parent;
    }

    public void setChild(Path child) {
        if (child != null) {
            if (this.equals(child)) {
                throw new IllegalArgumentException("Path element cannot have himself as a child");
            }
            if (this.element.getDepth() + 1 != child.getElement().getDepth()) {
                throw new IllegalArgumentException("Invalid child depth (" + child.getElement().getDepth() + ") for path element depth (" + this.element.getDepth() + ")");
            }
        }
        this.child = child;
        if (child != null && child.getParent() != this) {
            child.setParent(this);
        }
    }

    public Path getChild() {
        return this.child;
    }

    public String getDefinition() {
        return this.definition;
    }

    public Element getElement() {
        return this.element;
    }

    public static enum Element {
        ANEXO("a", 0),
        QUADRO("q", 1),
        CAMPO("f", 2),
        TABELA("t", 2),
        LINHA("l", 3),
        COLUNA("c", 4),
        QUADRO_REPETITIVO("r", 2),
        CAMPO_QUADRO_REPETITIVO("rf", 3);
        
        private final String element;
        private final int depth;

        private Element(String element, int depth) {
            this.element = element;
            this.depth = depth;
        }

        public String getElement() {
            return this.element;
        }

        public int getDepth() {
            return this.depth;
        }

        public String toString() {
            return this.element;
        }
    }

}

