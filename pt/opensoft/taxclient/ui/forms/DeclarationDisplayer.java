/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.forms;

import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.event.NavigationSelectionListener;
import pt.opensoft.taxclient.ui.event.SelectedListener;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;

public interface DeclarationDisplayer
extends MapDataListener,
NavigationSelectionListener,
SelectedListener,
PropertyChangeListener {
    public void setModel(DeclaracaoModel var1);

    public void registerListeners();

    public void showAnexo(FormKey var1);

    public JComponent getComponent(FormKey var1, String var2);

    public void goToPath(String var1);

    public AnexoTabbedPane getAnexoTabbedPane(FormKey var1);
}

