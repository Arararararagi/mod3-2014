/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.forms.impl.w3;

import java.awt.Color;
import javax.swing.Icon;
import javax.swing.UIManager;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.ui.forms.impl.JTabbedCardLayoutPanel;
import pt.opensoft.taxclient.ui.forms.impl.w3.W3UsabilityAnexoTabbedPane;

public class W3UsabilityJTabbedCardLayoutPanel
extends JTabbedCardLayoutPanel {
    private static final long serialVersionUID = 487705989727901295L;

    public W3UsabilityJTabbedCardLayoutPanel(String title) {
        super(null, title, UIManager.getColor("TitledPanel3.titleBackground"));
    }

    @Override
    protected AnexoTabbedPane createAnexoTabbedPane(String formKeyID) {
        return new W3UsabilityAnexoTabbedPane(formKeyID);
    }
}

