/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.forms.impl.w3;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.util.Pair;

public class W3UsabilityAnexoTabbedPane
extends AnexoTabbedPane {
    private static final long serialVersionUID = 5053277781077648278L;
    private static final CheckRadioBoxKeyListener CHECK_AND_RADIO_LISTENER = new CheckRadioBoxKeyListener();

    public W3UsabilityAnexoTabbedPane(String formKeyID) {
        super(formKeyID);
        W3UsabilityAnexoTabbedPane.changeTransversalKeys(this);
    }

    public static void changeTransversalKeys(JComponent comp) {
        Set<AWTKeyStroke> forwardKeys = comp.getFocusTraversalKeys(0);
        Set<AWTKeyStroke> backwardKeys = comp.getFocusTraversalKeys(1);
        HashSet<AWTKeyStroke> newForwardKeys = new HashSet<AWTKeyStroke>(forwardKeys);
        newForwardKeys.add(KeyStroke.getKeyStroke(10, 0));
        newForwardKeys.add(KeyStroke.getKeyStroke(10, 2));
        comp.setFocusTraversalKeys(0, newForwardKeys);
        HashSet<AWTKeyStroke> newBackwardKeys = new HashSet<AWTKeyStroke>(backwardKeys);
        newBackwardKeys.add(KeyStroke.getKeyStroke(10, 3));
        newBackwardKeys.add(KeyStroke.getKeyStroke(10, 1));
        comp.setFocusTraversalKeys(1, newBackwardKeys);
    }

    @Override
    public void doExtrActionsForAddFirstModel(JPanel quadroPanelExtension, JScrollPane scrollPane) {
        W3UsabilityAnexoTabbedPane.addFocusListenerToAllComponentsInContainer(this, new FocusManagerForComponents(quadroPanelExtension, null, this, this.tabIds, scrollPane), CHECK_AND_RADIO_LISTENER);
    }

    private static void addFocusListenerToAllComponentsInContainer(Container container, FocusListener focusListener, CheckRadioBoxKeyListener checkRadioKeyListener) {
        if (container.getComponentCount() == 0) {
            return;
        }
        for (int i = 0; i < container.getComponentCount(); ++i) {
            Component component = container.getComponent(i);
            if (component instanceof Container) {
                W3UsabilityAnexoTabbedPane.addFocusListenerToAllComponentsInContainer((Container)component, focusListener, checkRadioKeyListener);
            }
            component.removeFocusListener(focusListener);
            component.addFocusListener(focusListener);
            if (!(component instanceof JCheckBox) && !(component instanceof JRadioButton)) continue;
            component.removeKeyListener(checkRadioKeyListener);
            component.addKeyListener(checkRadioKeyListener);
        }
    }

    private static class CheckRadioBoxKeyListener
    implements KeyListener {
        private CheckRadioBoxKeyListener() {
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if (e.getKeyChar() == '*' && e.getComponent() instanceof JToggleButton) {
                ((JToggleButton)e.getComponent()).doClick();
                e.getComponent().transferFocus();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }
    }

    public static class FocusManagerForComponents
    implements FocusListener {
        private JTextField erro;
        private JTabbedPane tPane;
        private Map<String, Pair> tabIds;
        private JComponent currentPanel;
        private JScrollPane scrollPane;
        private Map<Component, Color> originalBackGroundColor;

        public FocusManagerForComponents(JComponent currentPanel, JTextField erro, JTabbedPane tPane, Map<String, Pair> tabIds, JScrollPane scrollPane) {
            this.currentPanel = currentPanel;
            this.erro = erro;
            this.tPane = tPane;
            this.tabIds = tabIds;
            this.scrollPane = scrollPane;
            this.originalBackGroundColor = new HashMap<Component, Color>();
        }

        @Override
        public void focusLost(FocusEvent evt) {
            Component toComponent = evt.getOppositeComponent();
            Component fromComponent = evt.getComponent();
            this.changeBackGroundToNormal(fromComponent);
            this.changeTabIfNeeded(toComponent, fromComponent);
        }

        @Override
        public void focusGained(FocusEvent evt) {
            Component toFocusComponent = evt.getComponent();
            this.changeBackGroundToFocus(toFocusComponent);
            if (toFocusComponent instanceof JComponent) {
                this.synchronizeScrollAndComponent((JComponent)toFocusComponent, this.scrollPane, (JPanel)this.currentPanel);
            }
        }

        private Container getContainerWithFocusTransversalPolicyOfComponent(Component comp) {
            if (comp == null) {
                return null;
            }
            Container rootAncestor = comp.getFocusCycleRootAncestor();
            while (!(rootAncestor == null || rootAncestor.isShowing() && rootAncestor.isFocusable() && rootAncestor.isEnabled())) {
                comp = rootAncestor;
                rootAncestor = comp.getFocusCycleRootAncestor();
            }
            if (rootAncestor != null) {
                return rootAncestor;
            }
            return null;
        }

        private FocusDir getFocusInTransversalDirection(Component focusGainedComp, Component focusLostComp) {
            if (focusGainedComp == null || focusLostComp == null) {
                return FocusDir.UNKNOWN;
            }
            Container containerWithFocusTransPolicy = this.getContainerWithFocusTransversalPolicyOfComponent(focusGainedComp);
            if (containerWithFocusTransPolicy == null) {
                return null;
            }
            if (containerWithFocusTransPolicy.getFocusTraversalPolicy().getComponentBefore(containerWithFocusTransPolicy, focusGainedComp) == focusLostComp) {
                return FocusDir.FORWARD;
            }
            if (containerWithFocusTransPolicy.getFocusTraversalPolicy().getComponentAfter(containerWithFocusTransPolicy, focusGainedComp) == focusLostComp) {
                return FocusDir.BACKWARD;
            }
            return null;
        }

        private void changeTabIfNeeded(Component toComponent, Component fromComponent) {
            Container focusCycleRootAncestor = fromComponent.getFocusCycleRootAncestor();
            if (focusCycleRootAncestor == null) {
                return;
            }
            FocusTraversalPolicy focusTraversalPolicy = focusCycleRootAncestor.getFocusTraversalPolicy();
            if (focusTraversalPolicy == null) {
                return;
            }
            Component firstFocusComponent = focusTraversalPolicy.getFirstComponent(this.currentPanel);
            Component lastFocusComponent = focusTraversalPolicy.getLastComponent(this.currentPanel);
            if (fromComponent != firstFocusComponent && fromComponent != lastFocusComponent) {
                return;
            }
            if (FocusManagerForComponents.existsComponentInContainerTree(this.currentPanel, toComponent)) {
                return;
            }
            FocusDir focusTransversalDirection = this.getFocusInTransversalDirection(toComponent, fromComponent);
            if (focusTransversalDirection != null && focusTransversalDirection != FocusDir.FORWARD && focusTransversalDirection != FocusDir.BACKWARD) {
                return;
            }
            boolean backwardDir = focusTransversalDirection == FocusDir.BACKWARD;
            boolean forwardDir = focusTransversalDirection == FocusDir.FORWARD;
            int currentTab = this.tPane.getSelectedIndex();
            if (forwardDir || backwardDir) {
                Container containerOfTab;
                Container focusCycleRootAncestorNextCont;
                int max;
                boolean found = false;
                if (found) {
                    this.erro.setText("enc erro");
                    this.erro.requestFocusInWindow();
                    return;
                }
                if (forwardDir) {
                    int nextTab = currentTab + 1;
                    max = this.tPane.getTabCount();
                    nextTab = nextTab >= max ? 0 : nextTab;
                    this.tPane.setSelectedIndex(nextTab);
                    containerOfTab = this.getContainerOfTab(nextTab, this.tabIds);
                    if (containerOfTab == null) {
                        return;
                    }
                    focusCycleRootAncestorNextCont = containerOfTab.getFocusCycleRootAncestor();
                    if (focusCycleRootAncestorNextCont == null) {
                        return;
                    }
                    Component firstComponent = focusCycleRootAncestorNextCont.getFocusTraversalPolicy().getFirstComponent(containerOfTab);
                    if (firstComponent == null) {
                        return;
                    }
                    firstComponent.requestFocusInWindow();
                }
                if (backwardDir) {
                    max = this.tPane.getTabCount();
                    int previousTab = currentTab - 1;
                    previousTab = previousTab < 0 ? max - 1 : previousTab;
                    this.tPane.setSelectedIndex(previousTab);
                    containerOfTab = this.getContainerOfTab(previousTab, this.tabIds);
                    if (containerOfTab == null) {
                        return;
                    }
                    focusCycleRootAncestorNextCont = containerOfTab.getFocusCycleRootAncestor();
                    if (focusCycleRootAncestorNextCont == null) {
                        return;
                    }
                    containerOfTab.setBackground(Color.BLACK);
                    Component lastComponent = focusCycleRootAncestorNextCont.getFocusTraversalPolicy().getLastComponent(containerOfTab);
                    if (lastComponent == null) {
                        return;
                    }
                    lastComponent.requestFocusInWindow();
                }
            }
        }

        private void changeBackGroundToFocus(Component c) {
            Color focusBackgroundColor = Color.ORANGE;
            if (c instanceof JTable) {
                JTable tb = (JTable)c;
                Color originalBCColor = this.getBackColorOfTable(tb);
                if (!focusBackgroundColor.equals(originalBCColor)) {
                    this.originalBackGroundColor.put(c, originalBCColor);
                    this.setBackColorOfTable(tb, focusBackgroundColor);
                }
            } else if (!focusBackgroundColor.equals(c.getBackground())) {
                this.originalBackGroundColor.put(c, c.getBackground());
                c.setBackground(focusBackgroundColor);
            }
        }

        private void changeBackGroundToNormal(Component c) {
            Color originalColor = this.originalBackGroundColor.remove(c);
            if (originalColor == null) {
                return;
            }
            if (c instanceof JTable) {
                JTable tb = (JTable)c;
                this.setBackColorOfTable(tb, originalColor);
            } else {
                c.setBackground(originalColor);
            }
        }

        private void setBackColorOfTable(JTable table, Color backgroundColor) {
            if (table.getParent() != null && table.getParent() instanceof JViewport) {
                table.getParent().setBackground(backgroundColor);
            }
        }

        private Color getBackColorOfTable(JTable table) {
            if (table.getParent() != null && table.getParent() instanceof JViewport) {
                return table.getParent().getBackground();
            }
            return Color.BLACK;
        }

        private void synchronizeScrollAndComponent(JComponent toFocusComp, JScrollPane scrollPane, JPanel currentPanel) {
            if (toFocusComp instanceof JTable) {
                JTable table = (JTable)toFocusComp;
                if (table.getParent() == null) {
                    return;
                }
                if (table.getParent().getParent() == null) {
                    return;
                }
                JScrollPane tableScrollPane = (JScrollPane)table.getParent().getParent();
                Rectangle rect = tableScrollPane.getBounds();
                tableScrollPane.scrollRectToVisible(new Rectangle(rect.x, 0, rect.width, 70));
                return;
            }
            JComponent focusedComponent = toFocusComp;
            Rectangle bounds = focusedComponent.getBounds();
            if (bounds == null) {
                return;
            }
            Container parentComp = focusedComponent.getParent();
            if (parentComp == null) {
                return;
            }
            if (!(parentComp instanceof JComponent)) {
                return;
            }
            ((JComponent)focusedComponent.getParent()).scrollRectToVisible(bounds);
        }

        private Container getContainerOfTab(Integer tabPos, Map<String, Pair> tabIds) {
            for (Pair p : tabIds.values()) {
                if (!p.getFirst().equals(tabPos)) continue;
                return this.retrievePureContainer(p.getSecond());
            }
            return null;
        }

        private Container retrievePureContainer(Object obj) {
            if (obj instanceof JScrollPane) {
                JScrollPane scroP = (JScrollPane)obj;
                Component view = scroP.getViewport().getView();
                if (view != null && view instanceof Container) {
                    return (Container)view;
                }
                return null;
            }
            if (obj instanceof Container) {
                return (Container)obj;
            }
            return null;
        }

        private static boolean existsComponentInContainerTree(Container container, Component compToFind) {
            if (container.getComponentCount() == 0) {
                return false;
            }
            for (int i = 0; i < container.getComponentCount(); ++i) {
                boolean res;
                Component component2 = container.getComponent(i);
                if (component2 instanceof Container && (res = FocusManagerForComponents.existsComponentInContainerTree((Container)component2, compToFind))) {
                    return true;
                }
                if (component2 != compToFind) continue;
                return true;
            }
            return false;
        }

        private static enum FocusDir {
            FORWARD,
            BACKWARD,
            UNKNOWN;
            

            private FocusDir() {
            }
        }

    }

}

