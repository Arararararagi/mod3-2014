/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.forms.impl;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.TitledPanel3;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.swing.event.MapDataEvent;
import pt.opensoft.swing.event.MapDataListener;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;
import pt.opensoft.taxclient.ui.event.SelectionEvent;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.Path;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.ui.forms.impl.JTabbedCardLayoutPathInterpreter;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.Pair;

public class JTabbedCardLayoutPanel
extends TitledPanel3
implements DeclarationDisplayer {
    private static final long serialVersionUID = -8464402914375551558L;
    protected static String EMPTY_CARD_NAME = "empty";
    protected FormKey selectedAnexo;
    protected CardLayout cardLayout = new CardLayout();
    protected DeclaracaoModel declaracaoModel;
    private JTabbedCardLayoutPathInterpreter pathInterpreter;
    private Map<String, AnexoTabbedPane> anexoTabbedPaneCache = new HashMap<String, AnexoTabbedPane>();
    private boolean isReading = false;
    private boolean isClearing = false;

    public JTabbedCardLayoutPanel(String title) {
        this(null, title, GUIParameters.TITLED_PANEL_TITLE_COLOR);
    }

    public JTabbedCardLayoutPanel(Icon icon, String title, Color titleColor) {
        this(icon, title, titleColor, false);
    }

    public JTabbedCardLayoutPanel(Icon icon, String title, Color titleColor, boolean alignLeft) {
        super(icon, title, titleColor, false, alignLeft);
        this.setLayout(this.cardLayout);
        this.pathInterpreter = new JTabbedCardLayoutPathInterpreter(null);
        this.add(this.createEmptyCard(), EMPTY_CARD_NAME);
    }

    protected Component createEmptyCard() {
        JPanel emptyPanel = new JPanel();
        emptyPanel.setName(EMPTY_CARD_NAME);
        return emptyPanel;
    }

    protected void addJTabbedPane(AnexoModel anexoModel, boolean skipBinding) {
        if (anexoModel == null) {
            throw new IllegalArgumentException("anexoModel cannot be null.");
        }
        String formKeyID = anexoModel.getFormKey().getId();
        if (this.anexoTabbedPaneCache.containsKey(formKeyID)) {
            AnexoTabbedPane anexoTabbedPane = this.anexoTabbedPaneCache.get(formKeyID);
            anexoTabbedPane.addAnexoModel(anexoModel, skipBinding);
        } else {
            AnexoTabbedPane anexoTabbedPane = this.createAnexoTabbedPane(formKeyID);
            anexoTabbedPane.addAnexoModel(anexoModel, skipBinding);
            anexoTabbedPane.addChangeListener(new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent e) {
                    try {
                        JTabbedPane tabSource = (JTabbedPane)e.getSource();
                        String tab = tabSource.getSelectedComponent().getName();
                        Session.getCurrentDeclaracao().setSelectedQuadro(tab);
                        if (Session.getMainFrame().getMultiPurposeDisplayer().getHelpDisplayer() != null) {
                            Session.getMainFrame().getMultiPurposeDisplayer().getHelpDisplayer().updateHelp();
                        }
                    }
                    catch (Exception e1) {
                        Session.getExceptionHandler().handle(e1);
                    }
                }
            });
            this.add((Component)anexoTabbedPane, anexoModel.getFormKey().getId());
            this.anexoTabbedPaneCache.put(formKeyID, anexoTabbedPane);
        }
        this.cardLayout.show(this, anexoModel.getFormKey().getId());
    }

    protected AnexoTabbedPane createAnexoTabbedPane(String formKeyID) {
        return new AnexoTabbedPane(formKeyID);
    }

    public void refresh() {
        Component[] cards;
        for (String formKeyID : this.anexoTabbedPaneCache.keySet()) {
            AnexoTabbedPane anexoTabbedPane = this.anexoTabbedPaneCache.get(formKeyID);
            anexoTabbedPane.purge();
        }
        this.anexoTabbedPaneCache.clear();
        for (Component card : cards = this.getComponents()) {
            if (EMPTY_CARD_NAME.equals(card.getName())) continue;
            this.remove(card);
        }
        boolean displayRoot = false;
        AnexoTabbedPane anexoTabbedPaneFocus = null;
        Collection<AnexoModel> anexos = this.declaracaoModel.getAnexos().values();
        for (AnexoModel anexoModel : anexos) {
            this.addJTabbedPane(anexoModel, true);
            if (!anexoModel.getFormKey().getUniqueId().equals(TaxclientParameters.FORM_ROOT_DISPLAY_NAME)) continue;
            displayRoot = true;
            AnexoTabbedPane anexoTabbedPane = this.anexoTabbedPaneCache.get(anexoModel.getFormKey().getId());
            anexoTabbedPane.showAnexoModel(anexoModel);
            anexoTabbedPaneFocus = anexoTabbedPane;
        }
        if (displayRoot) {
            this.cardLayout.show(this, TaxclientParameters.FORM_ROOT_DISPLAY_NAME);
            if (anexoTabbedPaneFocus != null) {
                anexoTabbedPaneFocus.requestFocusToAnexo();
            }
        } else {
            this.cardLayout.first(this);
        }
    }

    @Override
    public void setModel(DeclaracaoModel declaracaoModel) {
        this.declaracaoModel = declaracaoModel;
        this.declaracaoModel.addDeclaracaoMapModelListener(this);
        this.pathInterpreter.setDeclaracaoModel(declaracaoModel);
        this.refresh();
    }

    @Override
    public AnexoTabbedPane getAnexoTabbedPane(FormKey formKey) {
        return this.anexoTabbedPaneCache.get(formKey.getId());
    }

    @Override
    public void goToPath(String pathAsString) {
        Path path = Path.buildPathFromString(pathAsString);
        FormKey formKey = this.pathInterpreter.getAnexoKey(Path.extractAnexo(path));
        AnexoTabbedPane anexoTabbedPane = null;
        if (formKey != null) {
            anexoTabbedPane = this.anexoTabbedPaneCache.get(formKey.getId());
        }
        this.pathInterpreter.goToPath(path, anexoTabbedPane);
    }

    @Override
    public void registerListeners() {
    }

    @Override
    public JComponent getComponent(FormKey formKey, String componentPath) {
        return null;
    }

    @Override
    public void showAnexo(FormKey formKey) {
        if (formKey == null) {
            this.cardLayout.show(this, EMPTY_CARD_NAME);
        }
    }

    @Override
    public void objectPut(MapDataEvent e) {
        if (!(this.isReading || this.isClearing)) {
            FormKey formKey = (FormKey)e.getKey();
            AnexoTabbedPane anexoTabbedPane = this.anexoTabbedPaneCache.get(formKey.getId());
            if (anexoTabbedPane == null) {
                this.addJTabbedPane((AnexoModel)e.getValue(), false);
            }
            this.updateUI();
            AnexoTabbedPane addedAnexoTabbedPane = this.anexoTabbedPaneCache.get(formKey.getId());
            if (addedAnexoTabbedPane != null) {
                addedAnexoTabbedPane.requestFocusToAnexo();
            }
        }
    }

    @Override
    public void objectRemoved(MapDataEvent e) {
        FormKey formKey = (FormKey)e.getKey();
        AnexoTabbedPane anexoTabbedPane = this.anexoTabbedPaneCache.get(formKey.getId());
        if (anexoTabbedPane != null) {
            anexoTabbedPane.removeAnexoModel((AnexoModel)e.getValue());
        }
    }

    @Override
    public void contentChanged(MapDataEvent e) {
    }

    @Override
    public void startedClear() {
        this.isClearing = true;
        for (String formKeyID : this.anexoTabbedPaneCache.keySet()) {
            AnexoTabbedPane anexoTabbedPane = this.anexoTabbedPaneCache.get(formKeyID);
            anexoTabbedPane.purge();
        }
    }

    @Override
    public void finishedClear() {
        this.isClearing = false;
    }

    @Override
    public void navigationItemSelected(NavigationItemEvent navigationItemSelectedEvent) {
        if (!(this.isReading || this.isClearing)) {
            AnexoTabbedPane anexoTabbedPane;
            FormKey formKey = navigationItemSelectedEvent.getFormKey();
            if (formKey != null) {
                anexoTabbedPane = this.anexoTabbedPaneCache.get(formKey.getId());
                if (anexoTabbedPane != null) {
                    anexoTabbedPane.showAnexoModel(this.declaracaoModel.getAnexo(formKey));
                }
                this.cardLayout.show(this, formKey.getId());
            } else {
                this.cardLayout.show(this, EMPTY_CARD_NAME);
            }
            anexoTabbedPane = this.anexoTabbedPaneCache.get(formKey.getId());
            if (anexoTabbedPane != null) {
                anexoTabbedPane.requestFocusToAnexo();
            }
        }
    }

    @Override
    public void selectedAnexoChanged(SelectionEvent selectedEvent) {
        if (!(this.isReading || this.isClearing)) {
            AnexoTabbedPane anexoTabbedPane;
            String formKeyID = selectedEvent.getFormKey().getId();
            if (formKeyID != null) {
                anexoTabbedPane = this.anexoTabbedPaneCache.get(formKeyID);
                if (anexoTabbedPane != null) {
                    anexoTabbedPane.showAnexoModel(this.declaracaoModel.getAnexo(selectedEvent.getFormKey()));
                }
                this.cardLayout.show(this, formKeyID);
            } else {
                this.cardLayout.show(this, EMPTY_CARD_NAME);
            }
            this.updateUI();
            this.selectedAnexo = selectedEvent.getFormKey();
            try {
                JTabbedPane tabSource = this.anexoTabbedPaneCache.get(formKeyID);
                if (tabSource != null) {
                    String tab = tabSource.getSelectedComponent().getName();
                    Session.getCurrentDeclaracao().setSelectedQuadro(tab);
                    if (Session.getMainFrame().getMultiPurposeDisplayer().getHelpDisplayer() != null) {
                        Session.getMainFrame().getMultiPurposeDisplayer().getHelpDisplayer().updateHelp();
                    }
                }
            }
            catch (Exception e1) {
                Session.getExceptionHandler().handle(e1);
            }
            anexoTabbedPane = this.anexoTabbedPaneCache.get(formKeyID);
            if (anexoTabbedPane != null) {
                anexoTabbedPane.requestFocusToAnexo();
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals("reading")) {
            this.isReading = (Boolean)event.getNewValue();
            if (!this.isReading) {
                this.refresh();
            }
        } else if (event.getPropertyName().equals("selectedQuadro")) {
            if (this.selectedAnexo == null || this.anexoTabbedPaneCache.isEmpty() || event.getNewValue() == null) {
                return;
            }
            String quadro = (String)event.getNewValue();
            AnexoTabbedPane tabbedPane = this.anexoTabbedPaneCache.get(this.selectedAnexo.getId());
            Pair info = tabbedPane.getComponentInfoByName(quadro);
            int position = (Integer)info.getFirst();
            if (position != tabbedPane.getSelectedIndex() && position < tabbedPane.getTabCount()) {
                tabbedPane.setSelectedIndex(position);
            }
        }
    }

    public DeclaracaoModel getDeclaracaoModel() {
        return this.declaracaoModel;
    }

    public FormKey getSelectedAnexo() {
        return this.selectedAnexo;
    }

}

