/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.forms.impl;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.ui.IBindablePanel;
import pt.opensoft.util.Pair;

public class AnexoTabbedPane
extends JTabbedPane {
    private static final long serialVersionUID = -7350335346610375653L;
    private static final String MODEL_PKG = "model";
    private static final String UI_PKG = "ui";
    private static final String PANEL_EXTENSION_SUFFIX = "PanelExtension";
    private final String formKeyID;
    protected Map<String, Pair> tabIds = new HashMap<String, Pair>();
    private Map<Class<? extends QuadroModel>, IBindablePanel<QuadroModel>> quadroPanelExtensionsMap = null;

    public AnexoTabbedPane(String formKeyID) {
        this.formKeyID = formKeyID;
        this.addMouseListener(new OnMouse(this));
    }

    public void addAnexoModel(AnexoModel anexoModel, boolean skipBinding) {
        if (anexoModel == null) {
            throw new NullPointerException("Cannot add a null AnexoModel to an AnexoTabbedPane.");
        }
        FormKey formKey = anexoModel.getFormKey();
        if (!formKey.getId().equals(this.formKeyID)) {
            throw new RuntimeException("The FormKey ID from the AnexoModel (" + formKey.getId() + ") does not match the FormKey ID for the AnexoTabbedPane (" + this.formKeyID + ")");
        }
        if (this.quadroPanelExtensionsMap == null) {
            this.addFirstModel(anexoModel, skipBinding);
        } else {
            this.addSubsequentModel(anexoModel, skipBinding);
        }
    }

    private void addFirstModel(AnexoModel anexoModel, boolean skipBinding) {
        this.quadroPanelExtensionsMap = new HashMap<Class<? extends QuadroModel>, IBindablePanel<QuadroModel>>(anexoModel.getQuadros().size());
        String packageForModel = anexoModel.getClass().getPackage().getName();
        int indexOfModelPackageSnippet = packageForModel.lastIndexOf("model");
        String packageForUI = packageForModel.substring(0, indexOfModelPackageSnippet) + "ui" + packageForModel.substring(indexOfModelPackageSnippet + "model".length());
        for (QuadroModel quadroModel : anexoModel.getQuadros()) {
            try {
                Class quadroPanelExtensionClass = Class.forName(packageForUI + "." + quadroModel.getClass().getSimpleName() + "PanelExtension");
                Constructor constructor = quadroPanelExtensionClass.getConstructor(quadroModel.getClass(), Boolean.TYPE);
                JPanel quadroPanelExtension = (JPanel)constructor.newInstance(quadroModel, false);
                this.quadroPanelExtensionsMap.put(quadroModel.getClass(), (IBindablePanel)quadroPanelExtension);
                JScrollPane scrollPane = new JScrollPane(quadroPanelExtension);
                scrollPane.getVerticalScrollBar().setUnitIncrement(scrollPane.getVerticalScrollBar().getBlockIncrement());
                scrollPane.setName(quadroPanelExtensionClass.getSimpleName().replace((CharSequence)"PanelExtension", (CharSequence)""));
                scrollPane.setHorizontalScrollBarPolicy(30);
                scrollPane.setBorder(null);
                this.addTab(this.getDesignationForTabName(quadroModel.getClass().getSimpleName() + ".tab.designation"), scrollPane);
                this.doExtrActionsForAddFirstModel(quadroPanelExtension, scrollPane);
                continue;
            }
            catch (ClassNotFoundException e) {
                throw new RuntimeException("Could not find " + packageForUI + "." + quadroModel.getClass().getSimpleName() + "PanelExtension", e);
            }
            catch (IllegalArgumentException e) {
                throw new RuntimeException("Unable to invoke the binding method: " + e.getMessage(), e);
            }
            catch (InstantiationException e) {
                throw new RuntimeException("Unable to invoke the binding method: " + e.getMessage(), e);
            }
            catch (IllegalAccessException e) {
                throw new RuntimeException("Unable to invoke the binding method: " + e.getMessage(), e);
            }
            catch (InvocationTargetException e) {
                throw new RuntimeException("Unable to invoke the binding method: " + e.getMessage(), e);
            }
            catch (SecurityException e) {
                throw new RuntimeException("Unable to invoke the constructor method: " + e.getMessage(), e);
            }
            catch (NoSuchMethodException e) {
                throw new RuntimeException("Unable to invoke the constructor method: " + e.getMessage(), e);
            }
        }
    }

    public void doExtrActionsForAddFirstModel(JPanel quadroPanelExtension, JScrollPane scrollPane) {
    }

    private void addSubsequentModel(AnexoModel anexoModel, boolean skipBinding) {
        if (!skipBinding) {
            this.showAnexoModel(anexoModel);
        }
    }

    public void removeAnexoModel(AnexoModel anexoModel) {
        for (QuadroModel quadroModel : anexoModel.getQuadros()) {
            IBindablePanel<QuadroModel> quadroPanelExtension = this.quadroPanelExtensionsMap.get(quadroModel.getClass());
            if (quadroPanelExtension.getModel() == null) continue;
            quadroPanelExtension.setModel(null, false);
        }
    }

    public void showAnexoModel(AnexoModel anexoModel) {
        if (anexoModel == null) {
            throw new NullPointerException("Cannot show a null AnexoModel.");
        }
        for (QuadroModel quadroModel : anexoModel.getQuadros()) {
            IBindablePanel<QuadroModel> quadroPanelExtension = this.quadroPanelExtensionsMap.get(quadroModel.getClass());
            quadroPanelExtension.setModel(null, false);
            quadroPanelExtension.setModel(quadroModel, false);
        }
    }

    public void requestFocusToAnexo() {
        this.transferFocus();
    }

    public String getDesignationForTabName(String tab) {
        String designation = DesignationManager.getInstance().getString(tab);
        if (designation == null) {
            throw new RuntimeException("A propriedade " + tab + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
        }
        return designation;
    }

    public void purge() {
        for (Class<? extends QuadroModel> quadroModelClass : this.quadroPanelExtensionsMap.keySet()) {
            IBindablePanel<QuadroModel> quadroPanelExtension = this.quadroPanelExtensionsMap.get(quadroModelClass);
            if (quadroPanelExtension.getModel() == null) continue;
            quadroPanelExtension.setModel(null, false);
        }
    }

    @Override
    public void addTab(String title, Component component) {
        super.addTab(title, component);
        if (component.getName() == null) {
            throw new RuntimeException("Tentou adicionar a tab " + title + " sem lhe ter dado um nome");
        }
        this.tabIds.put(component.getName(), new Pair<Integer, Component>(new Integer(this.getTabCount() - 1), component));
    }

    @Override
    public void addTab(String title, Icon icon, Component component, String tip) {
        super.addTab(title, icon, component, tip);
        if (component.getName() == null) {
            throw new RuntimeException("Tentou adicionar a tab " + title + " sem lhe ter dado um nome");
        }
        this.tabIds.put(component.getName(), new Pair<Integer, Component>(this.getTabCount(), component));
    }

    public Pair getComponentInfoByName(String name) {
        return this.tabIds.get(name);
    }

    public JPanel getPanelExtensionByModel(String quadroModelSimpleClassName) {
        for (Class<? extends QuadroModel> quadroModelClass : this.quadroPanelExtensionsMap.keySet()) {
            if (!quadroModelClass.getSimpleName().equals(quadroModelSimpleClassName)) continue;
            return (JPanel)this.quadroPanelExtensionsMap.get(quadroModelClass);
        }
        return null;
    }

    public JPanel getPanelExtensionByModel(Class<? extends QuadroModel> quadroModelClass) {
        return (JPanel)this.quadroPanelExtensionsMap.get(quadroModelClass);
    }

    private static class OnMouse
    implements MouseListener {
        private AnexoTabbedPane anexoTabbedPane;

        public OnMouse(AnexoTabbedPane jtabbedPane) {
            this.anexoTabbedPane = jtabbedPane;
        }

        @Override
        public void mouseClicked(MouseEvent arg0) {
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
        }

        @Override
        public void mouseExited(MouseEvent arg0) {
        }

        @Override
        public void mousePressed(MouseEvent arg0) {
        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
            this.anexoTabbedPane.requestFocusToAnexo();
        }
    }

}

