/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.forms.impl;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.forms.Path;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.Pair;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class JTabbedCardLayoutPathInterpreter {
    private static final int COEFICIENTE_DO_CAGANCO_X = -100;
    private static final int COEFICIENTE_DO_CAGANCO_Y = -50;
    private DeclaracaoModel declaracaoModel;

    public JTabbedCardLayoutPathInterpreter(DeclaracaoModel declaracaoModel) {
        this.declaracaoModel = declaracaoModel;
    }

    public void setDeclaracaoModel(DeclaracaoModel declaracaoModel) {
        this.declaracaoModel = declaracaoModel;
    }

    public DeclaracaoModel getDeclaracaoModel() {
        return this.declaracaoModel;
    }

    public void goToPath(Path path, AnexoTabbedPane anexoTabbedPane) {
        if (Path.isPathToAnexo(path)) {
            this.goToAnexo(Path.extractAnexo(path));
        } else if (Path.isPathToQuadro(path)) {
            this.goToQuadro(Path.extractAnexo(path), Path.extractQuadro(path), anexoTabbedPane);
        } else if (Path.isPathToCampo(path)) {
            this.goToCampo(Path.extractAnexo(path), Path.extractQuadro(path), Path.extractCampo(path), anexoTabbedPane);
        } else if (Path.isPathToTabela(path)) {
            this.goToTabela(Path.extractAnexo(path), Path.extractQuadro(path), Path.extractTabela(path), anexoTabbedPane);
        } else if (Path.isPathToTabelaLinha(path)) {
            this.goToLinha(Path.extractAnexo(path), Path.extractQuadro(path), Path.extractTabela(path), Path.extractLinha(path), anexoTabbedPane);
        } else if (Path.isPathToTabelaColuna(path)) {
            this.goToColuna(Path.extractAnexo(path), Path.extractQuadro(path), Path.extractTabela(path), Path.extractLinha(path), Path.extractColuna(path), anexoTabbedPane);
        } else if (Path.isPathToCampoQuadroRepetitivo(path)) {
            this.goToCampoQuadroRepetitivo(Path.extractAnexo(path), Path.extractQuadro(path), Path.extractQuadroRepetitivo(path), Path.extractCampoQuadroRepetitivo(path), anexoTabbedPane);
        }
    }

    private FormKey goToAnexo(String anexo) {
        FormKey anexoKey = this.getAnexoKey(anexo);
        if (anexoKey == null) {
            return null;
        }
        Session.getCurrentDeclaracao().setSelectedAnexo(this, anexoKey);
        return anexoKey;
    }

    private JScrollPane goToQuadro(String anexo, String quadro, AnexoTabbedPane tabbedPane) {
        FormKey anexoKey = this.goToAnexo(anexo);
        if (anexoKey == null) {
            return null;
        }
        Pair info = tabbedPane.getComponentInfoByName(quadro);
        int position = (Integer)info.getFirst();
        tabbedPane.setSelectedIndex(position);
        return (JScrollPane)info.getSecond();
    }

    private void goToCampo(String anexo, String quadro, String campo, AnexoTabbedPane anexoTabbedPane) {
        JScrollPane scrollPane = this.goToQuadro(anexo, quadro, anexoTabbedPane);
        if (scrollPane == null) {
            return;
        }
        JPanel panel = this.getInnerPanel(scrollPane);
        this.goToCampoImpl(anexo, quadro, campo, scrollPane, panel);
    }

    private void goToCampoImpl(String anexo, String quadro, String campo, JScrollPane scrollPane, JPanel panel) {
        try {
            JComponent component = (JComponent)panel.getClass().getMethod("get" + StringUtil.capitalizeFirstCharacter(campo), new Class[0]).invoke(panel, new Object[0]);
            int x = component.getX();
            int y = component.getY();
            for (Container container = component.getParent(); container != scrollPane.getViewport(); container = container.getParent()) {
                x+=container.getX();
                y+=container.getY();
            }
            scrollPane.getHorizontalScrollBar().setValue(scrollPane.getHorizontalScrollBar().getValue() + x + -100);
            scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getValue() + y + -50);
            component.requestFocus();
        }
        catch (Exception e) {
            throw new RuntimeException("O campo " + campo + " n\u00e3o foi encontrado no painel do quadro " + quadro + " do anexo " + anexo);
        }
    }

    private JTable goToTabela(String anexo, String quadro, String tabela, AnexoTabbedPane anexoTabbedPane) {
        JScrollPane scrollPane = this.goToQuadro(anexo, quadro, anexoTabbedPane);
        if (scrollPane == null) {
            return null;
        }
        JPanel panel = this.getInnerPanel(scrollPane);
        JTable table = null;
        try {
            table = (JTable)panel.getClass().getMethod("get" + StringUtil.capitalizeFirstCharacter(tabela), new Class[0]).invoke(panel, new Object[0]);
            int x = table.getX();
            int y = table.getY();
            for (Container container = table.getParent(); container != scrollPane.getViewport(); container = container.getParent()) {
                x+=container.getX();
                y+=container.getY();
            }
            scrollPane.getHorizontalScrollBar().setValue(scrollPane.getHorizontalScrollBar().getValue() + x + -100);
            scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getValue() + y + -50);
            table.requestFocus();
        }
        catch (Exception e) {
            throw new RuntimeException("A tabela " + tabela + " n\u00e3o foi encontrada no painel do quadro " + quadro + " do anexo " + anexo);
        }
        return table;
    }

    private JPanel getInnerPanel(JScrollPane scrollPane) {
        int i;
        Container viewport = null;
        for (i = 0; i < scrollPane.getComponents().length; ++i) {
            if (!(scrollPane.getComponent(i) instanceof JViewport)) continue;
            viewport = (JViewport)scrollPane.getComponent(i);
            break;
        }
        if (viewport == null) {
            throw new RuntimeException("N\u00e3o foi encontrado o Viewport do JScrollPane " + scrollPane);
        }
        JPanel panel = null;
        for (i = 0; i < viewport.getComponents().length; ++i) {
            if (!(viewport.getComponent(i) instanceof JPanel)) continue;
            panel = (JPanel)viewport.getComponent(i);
            break;
        }
        if (panel == null) {
            throw new RuntimeException("N\u00e3o foi encontrado o painel do Viewport " + viewport);
        }
        return panel;
    }

    private void goToLinha(String anexo, String quadro, String tabela, String linha, AnexoTabbedPane anexoTabbedPane) {
        JTable table = this.goToTabela(anexo, quadro, tabela, anexoTabbedPane);
        if (table == null) {
            return;
        }
        int line = Integer.parseInt(linha) - 1;
        if (line < 0 || line >= table.getRowCount() || table.getRowCount() == 0) {
            SimpleLog.log("A linha " + line + " da tabela " + tabela + " do quadro " + quadro + " do anexo " + anexo + " n\u00e3o existe. Verifique o seu ind\u00edce.");
            return;
        }
        this.scrollToVisible(table, line, 0);
        table.setCellSelectionEnabled(true);
        table.setRowSelectionInterval(line, line);
        table.setColumnSelectionInterval(0, table.getColumnCount() - 1);
        table.changeSelection(line, table.getColumnCount(), false, false);
        table.requestFocus();
    }

    private void goToColuna(String anexo, String quadro, String tabela, String linha, String coluna, AnexoTabbedPane anexoTabbedPane) {
        JTable table = this.goToTabela(anexo, quadro, tabela, anexoTabbedPane);
        if (table == null) {
            return;
        }
        int line = Integer.parseInt(linha) - 1;
        int column = Integer.parseInt(coluna) - 1;
        if (line < 0 || line >= table.getRowCount() || table.getRowCount() == 0) {
            SimpleLog.log("A linha " + line + " da tabela " + tabela + " do quadro " + quadro + " do anexo " + anexo + " n\u00e3o existe. Verifique o seu ind\u00edce.");
            return;
        }
        if (column < 0 || column > table.getColumnCount()) {
            throw new RuntimeException("A coluna " + column + " da tabela " + tabela + " do quadro " + quadro + " do anexo " + anexo + " n\u00e3o existe. Verifique o seu ind\u00edce.");
        }
        this.scrollToVisible(table, line, column);
        table.setCellSelectionEnabled(true);
        table.setRowSelectionInterval(line, line);
        table.setColumnSelectionInterval(column, column);
        table.changeSelection(line, column, false, false);
        table.requestFocus();
    }

    private void goToCampoQuadroRepetitivo(String anexo, String quadro, String quadroRepetitivo, String campo, AnexoTabbedPane anexoTabbedPane) {
        String[] parts = quadroRepetitivo.split("\\|");
        String quadroRepetitivoName = parts[0];
        int index = Integer.parseInt(parts[1]);
        JScrollPane scrollPane = this.goToQuadro(anexo, quadro, anexoTabbedPane);
        if (scrollPane == null) {
            return;
        }
        JPanel panel = this.getInnerPanel(scrollPane);
        Method[] methods = panel.getClass().getMethods();
        for (int i = 0; i < methods.length; ++i) {
            Method method = methods[i];
            if (!method.getName().equals("goto" + quadroRepetitivoName + "Action")) continue;
            try {
                method.invoke(panel, index);
                continue;
            }
            catch (Exception e) {
                throw new RuntimeException("Unable to jump to index " + index + " on " + quadroRepetitivoName, e);
            }
        }
        this.goToCampoImpl(anexo, quadro, campo, scrollPane, panel);
    }

    private void scrollToVisible(JTable table, int linha, int coluna) {
        if (!(table.getParent() instanceof JViewport)) {
            return;
        }
        JViewport viewport = (JViewport)table.getParent();
        Rectangle rect = table.getCellRect(linha, coluna, true);
        Point pt = viewport.getViewPosition();
        rect.setLocation(rect.x - pt.x, rect.y - pt.y);
        viewport.scrollRectToVisible(rect);
    }

    protected FormKey getAnexoKey(String pathAsString) {
        Class modelClass;
        String[] split = pathAsString.split("\\|");
        String packageName = ClassUtil.getPackageName(this.declaracaoModel.getClass());
        packageName = packageName.substring(0, packageName.lastIndexOf(".model"));
        try {
            modelClass = ClassUtil.getClass(new StringBuilder().append(packageName).append(".model.").append(split[0]).append(".").toString().toLowerCase() + split[0] + "Model");
        }
        catch (Exception e) {
            throw new RuntimeException("O link " + pathAsString + " referencia uma classe de modelo desconhecida");
        }
        AnexoModel model = split.length == 1 ? Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(modelClass) : Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(modelClass, split[1]);
        if (model == null) {
            SimpleLog.log("O link " + pathAsString + " referencia um anexo n\u00e3o existente no modelo.");
            return null;
        }
        return model.getFormKey();
    }
}

