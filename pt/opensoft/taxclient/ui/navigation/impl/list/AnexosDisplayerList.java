/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.list;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.swing.EventListModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.FormKey;
import sun.swing.DefaultLookup;

public class AnexosDisplayerList<E>
extends JList {
    private static final long serialVersionUID = -7165518845139030450L;
    private static final int CELL_WIDTH = 150;
    private static final int CELL_HEIGHT = 16;
    private EventList<E> listModel;

    public AnexosDisplayerList(EventList<E> model, int numberOfLines) {
        this.setCellRenderer(new NavigationDisplayerListCellRenderer());
        this.setVisibleRowCount(numberOfLines);
        this.setLayoutOrientation(2);
        this.setFixedCellWidth(150);
        this.setFixedCellHeight(16);
        this.listModel = model;
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setBackground(UIManager.getColor("DefaultNavigationDisplayer.w3.backgroundColor"));
        EventListModel<E> listModel = new EventListModel<E>(model);
        super.setModel(listModel);
    }

    public void setSelectedItem(E anexoName) {
        int i = 0;
        for (E anexo : this.listModel) {
            if (anexo.equals(anexoName)) {
                this.setSelectedIndex(i);
                return;
            }
            ++i;
        }
    }

    public void removeNonExistant(List<E> anexos) {
        ArrayList<E> temp = new ArrayList<E>();
        for (E anexo : this.listModel) {
            if (anexos.contains(anexo)) continue;
            temp.add(anexo);
        }
        for (E anx : temp) {
            this.listModel.remove(anx);
        }
    }

    public void addNonExistant(List<E> anexos) {
        for (E anexo : anexos) {
            if (this.listModel.contains(anexo)) continue;
            this.listModel.add(anexo);
        }
    }

    public void setModelElems(List<E> anexos) {
        this.removeNonExistant(anexos);
        this.addNonExistant(anexos);
        Collections.sort(this.listModel, new Comparator<E>(){

            @Override
            public int compare(E o1, E o2) {
                if (((FormKey)o1).getId().equals("Rosto")) {
                    return -1;
                }
                if (((FormKey)o2).getId().equals("Rosto")) {
                    return 1;
                }
                return ((FormKey)o1).getId().compareTo(((FormKey)o2).getId());
            }
        });
        this.repaint();
    }

    public E getElementAtIndex(int index) {
        return this.listModel.get(index);
    }

    private String getListEntryDesignation(FormKey formKey, boolean includeDeclarationDesignation) {
        String key = formKey.getId().toLowerCase() + ".navigation.designation";
        String value = DesignationManager.getInstance().getString(key);
        if (value == null) {
            throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
        }
        if (includeDeclarationDesignation) {
            String declarationKey = formKey.getId().toLowerCase() + ".declaration.designation";
            String declarationValue = DesignationManager.getInstance().getString(declarationKey);
            if (declarationValue == null) {
                throw new RuntimeException("A propriedade " + declarationKey + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
            }
            return this.getListEntryDesignationAsString(value, formKey.getSubId()) + " - " + declarationValue;
        }
        return this.getListEntryDesignationAsString(value, formKey.getSubId());
    }

    private String getListEntryDesignationAsString(String value, String subId) {
        if (subId == null) {
            return value;
        }
        return value + " (" + subId + ")";
    }

    private class NavigationDisplayerListCellRenderer
    extends DefaultListCellRenderer {
        private static final long serialVersionUID = 8130542812929668665L;

        private NavigationDisplayerListCellRenderer() {
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            this.setComponentOrientation(list.getComponentOrientation());
            Color bg = null;
            Color fg = null;
            JList.DropLocation dropLocation = list.getDropLocation();
            if (!(dropLocation == null || dropLocation.isInsert() || dropLocation.getIndex() != index)) {
                bg = DefaultLookup.getColor(this, this.ui, "List.dropCellBackground");
                fg = DefaultLookup.getColor(this, this.ui, "List.dropCellForeground");
                isSelected = true;
            }
            if (isSelected) {
                this.setBackground(bg == null ? list.getSelectionBackground() : bg);
                this.setForeground(fg == null ? list.getSelectionForeground() : fg);
            } else {
                this.setBackground(list.getBackground());
                this.setForeground(list.getForeground());
            }
            this.setIcon(IconFactory.getIconSmall("FormNode.png"));
            this.setText(AnexosDisplayerList.this.getListEntryDesignation((FormKey)value, false));
            this.setEnabled(list.isEnabled());
            this.setFont(list.getFont());
            Border border = null;
            if (cellHasFocus) {
                if (isSelected) {
                    border = DefaultLookup.getBorder(this, this.ui, "List.focusSelectedCellHighlightBorder");
                }
                if (border == null) {
                    border = DefaultLookup.getBorder(this, this.ui, "List.focusCellHighlightBorder");
                }
            } else {
                border = this.getNoFocusBorder();
            }
            this.setBorder(border);
            return this;
        }

        private Border getNoFocusBorder() {
            Border border = DefaultLookup.getBorder(this, this.ui, "List.cellNoFocusBorder");
            if (System.getSecurityManager() != null) {
                if (border != null) {
                    return border;
                }
                return new EmptyBorder(1, 1, 1, 1);
            }
            if (border != null && (noFocusBorder == null || noFocusBorder == new EmptyBorder(1, 1, 1, 1))) {
                return border;
            }
            return noFocusBorder;
        }
    }

}

