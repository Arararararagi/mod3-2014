/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.table;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableModel;

public class AnexosDisplayerTable<E>
extends JTable {
    EventList<E> tableListModel;

    public AnexosDisplayerTable(EventList<E> model, TableFormat<? super E> tableFormat) {
        this.tableListModel = model;
        this.setBorder(BorderFactory.createEmptyBorder());
        EventTableModel<? super E> tableModel = new EventTableModel<E>(model, tableFormat);
        super.setModel(tableModel);
    }

    public void addAnexo(E anexo) {
        this.tableListModel.add(anexo);
    }

    public void removeAnexo(E anexo) {
        this.tableListModel.remove(anexo);
    }

    public void removeAnexo(int indexToRemove) {
        this.tableListModel.remove(indexToRemove);
    }

    public void setSelectedItem(E anexoName) {
        int i = 0;
        for (E anexo : this.tableListModel) {
            if (anexo.equals(anexoName)) {
                this.setRowSelectionInterval(i, i);
                return;
            }
            ++i;
        }
    }

    public void removeNonExistant(List<E> anexos) {
        for (E anexo : this.tableListModel) {
            if (anexos.contains(anexo)) continue;
            this.tableListModel.remove(anexo);
        }
    }

    public void addNonExistant(List<E> anexos) {
        for (E anexo : anexos) {
            if (this.tableListModel.contains(anexo)) continue;
            this.tableListModel.add(anexo);
        }
    }

    public void setModelElems(List<E> anexos) {
        this.removeNonExistant(anexos);
        this.addNonExistant(anexos);
        this.repaint();
    }

    public E getElemAtRow(int row) {
        return this.tableListModel.get(row);
    }
}

