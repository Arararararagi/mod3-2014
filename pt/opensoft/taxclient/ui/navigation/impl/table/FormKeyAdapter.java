/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.table;

import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import pt.opensoft.taxclient.model.FormKey;

public class FormKeyAdapter
implements AdvancedTableFormat<FormKey>,
WritableTableFormat<FormKey> {
    @Override
    public boolean isEditable(FormKey baseObject, int columnIndex) {
        return false;
    }

    @Override
    public Object getColumnValue(FormKey line, int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return line.getId();
            }
        }
        return null;
    }

    @Override
    public FormKey setColumnValue(FormKey line, Object value, int columnIndex) {
        return line;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public Comparator getColumnComparator(int column) {
        return null;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: {
                return "Impressos";
            }
        }
        return null;
    }
}

