/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.tree;

import com.jgoodies.uif_lite.panel.SimpleInternalFrame;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JSplitPane;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import pt.opensoft.swing.EnhancedTreeNode;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.ui.navigation.impl.tree.AnexoTreeNode;
import pt.opensoft.taxclient.ui.navigation.impl.tree.DeclaracaoTreeNode;
import pt.opensoft.taxclient.util.Session;

public class TaxNavigationTreeModel
extends NavigationModel
implements TreeModel,
TreeSelectionListener {
    protected DeclaracaoTreeNode root;
    protected List<TreeModelListener> treeModelListeners = new ArrayList<TreeModelListener>();

    public TaxNavigationTreeModel(DeclaracaoTreeNode root) {
        this.root = root;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        this.treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        this.treeModelListeners.remove(l);
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        if (e.getNewLeadSelectionPath() == null) {
            return;
        }
        EnhancedTreeNode enhancedTreeNode = (EnhancedTreeNode)e.getNewLeadSelectionPath().getLastPathComponent();
        if (!enhancedTreeNode.equals(this.root)) {
            AnexoTreeNode anexoTreeNode = (AnexoTreeNode)enhancedTreeNode;
            FormKey selectedAnexo = anexoTreeNode.getFormKey();
            Session.getCurrentDeclaracao().setSelectedAnexo(this, selectedAnexo);
            this.changeAnexoTitle(selectedAnexo.getId());
        }
    }

    private void changeAnexoTitle(String title) {
        block0 : for (Component outerComponent : Session.getMainFrame().getComponents()) {
            if (!(outerComponent instanceof JSplitPane)) continue;
            for (Component middleComponent : ((JSplitPane)outerComponent).getComponents()) {
                if (!(middleComponent instanceof JSplitPane)) continue;
                for (Component innerComponent : ((JSplitPane)middleComponent).getComponents()) {
                    if (!(innerComponent instanceof SimpleInternalFrame)) continue;
                    String key = title.toLowerCase() + ".navigation.designation";
                    String value = DesignationManager.getInstance().getString(key);
                    if (value == null) {
                        throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
                    }
                    ((SimpleInternalFrame)innerComponent).setTitle(value);
                    break block0;
                }
                break block0;
            }
            break;
        }
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        EnhancedTreeNode enhancedTreeNode = (EnhancedTreeNode)path.getLastPathComponent();
        enhancedTreeNode.setUserObject(newValue);
        if (enhancedTreeNode.equals(this.root)) {
            NavigationItemEvent navigationItemChangedEvent = new NavigationItemEvent(this, NavigationItemEvent.NavigationItemEventType.CHANGED, null);
        } else {
            AnexoTreeNode anexoTreeNode = (AnexoTreeNode)enhancedTreeNode;
            NavigationItemEvent navigationItemChangedEvent = new NavigationItemEvent(this, NavigationItemEvent.NavigationItemEventType.CHANGED, anexoTreeNode.getFormKey());
        }
    }

    @Override
    public Object getChild(Object parent, int index) {
        return ((EnhancedTreeNode)parent).getChildAt(index);
    }

    @Override
    public int getChildCount(Object parent) {
        return ((EnhancedTreeNode)parent).getChildCount();
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if (parent == null || child == null) {
            return -1;
        }
        return ((EnhancedTreeNode)parent).getIndex((EnhancedTreeNode)child);
    }

    @Override
    public Object getRoot() {
        return this.root;
    }

    @Override
    public boolean isLeaf(Object node) {
        return node instanceof AnexoTreeNode;
    }

    @Override
    protected void changeNavigationItem(FormKey formKey) {
    }

    @Override
    protected void insertNavigationItem(FormKey formKey) {
        AnexoModel anexoModel = Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(formKey);
        AnexoTreeNode anexoTreeNode = new AnexoTreeNode(anexoModel);
        int i = 0;
        int result = -1;
        if (this.root.getChildCount() > 0) {
            AnexoModel tempModel;
            FormKey fKey;
            do {
                fKey = ((AnexoTreeNode)this.root.getChildAt(i)).getFormKey();
            } while ((result = (tempModel = Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexo(fKey)).compareTo(anexoModel)) <= 0 && result <= 0 && ++i < this.root.getChildCount());
        }
        this.root.insert(anexoTreeNode, i);
        for (TreeModelListener listener : this.treeModelListeners) {
            listener.treeStructureChanged(new TreeModelEvent((Object)this, new TreeNode[]{this.root}, null, null));
        }
    }

    @Override
    protected void notifyDisplayerOfSelection(NavigationDisplayer displayer, FormKey formKey) {
        Object[] path = this.findChildNodePath(formKey);
        displayer.setSelectionNavigationItem(new TreePath(path));
    }

    @Override
    protected void removeNavigationItem(FormKey formKey) {
        Enumeration children = this.root.children();
        while (children.hasMoreElements()) {
            AnexoTreeNode node = (AnexoTreeNode)children.nextElement();
            if (!node.getFormKey().equals(formKey)) continue;
            this.root.remove(node);
            break;
        }
        for (TreeModelListener listener : this.treeModelListeners) {
            listener.treeStructureChanged(new TreeModelEvent((Object)this, new TreeNode[]{this.root}, null, null));
        }
    }

    @Override
    protected void removeAllNavigationItems() {
        this.root.removeAllChildren();
        for (TreeModelListener listener : this.treeModelListeners) {
            listener.treeStructureChanged(new TreeModelEvent((Object)this, new TreeNode[]{this.root}, null, null));
        }
    }

    public TreeNode[] findChildNodePath(FormKey key) {
        Enumeration children = this.root.children();
        while (children.hasMoreElements()) {
            AnexoTreeNode node = (AnexoTreeNode)children.nextElement();
            if (!node.getFormKey().equals(key)) continue;
            return node.getPath();
        }
        return null;
    }

    private class TreeModelListenerTranslator {
        private final TreeModelListener treeModelListener;

        public TreeModelListenerTranslator(TreeModelListener treeModelListener) {
            if (treeModelListener == null) {
                throw new IllegalArgumentException("treeModelListener cannot be null.");
            }
            this.treeModelListener = treeModelListener;
        }

        public TreeModelListener getTreeModelListener() {
            return this.treeModelListener;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof TreeModelListenerTranslator)) {
                return false;
            }
            TreeModelListenerTranslator objToCompare = (TreeModelListenerTranslator)obj;
            return this.treeModelListener.equals(objToCompare.getTreeModelListener());
        }

        public int hashCode() {
            return this.treeModelListener.hashCode();
        }
    }

}

