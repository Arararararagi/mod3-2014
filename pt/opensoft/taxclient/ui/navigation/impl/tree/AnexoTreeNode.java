/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.tree;

import pt.opensoft.swing.EnhancedTreeNode;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.FormKey;

public class AnexoTreeNode
extends EnhancedTreeNode {
    private FormKey formKey;

    public AnexoTreeNode(AnexoModel anexoModel) {
        this(anexoModel, AnexoTreeNode.getTreeNodeDesignation(anexoModel.getClass().getSimpleName(), anexoModel.getFormKey().getSubId()));
    }

    public AnexoTreeNode(AnexoModel anexoModel, String tooltip) {
        super(AnexoTreeNode.getTreeNodeDesignation(anexoModel.getClass().getSimpleName(), anexoModel.getFormKey().getSubId()), tooltip);
        this.setUserObject(anexoModel);
        this.formKey = anexoModel.getFormKey();
    }

    private static String getTreeNodeDesignation(String modelClass, String subId) {
        String key = modelClass.substring(0, modelClass.indexOf("Model")).toLowerCase() + ".navigation.designation";
        String value = DesignationManager.getInstance().getString(key);
        if (value == null) {
            throw new RuntimeException("A propriedade " + key + " n\u00e3o foi encontrada no ficheiro designation.properties. \u00c9 favor adicion\u00e1-la");
        }
        return AnexoTreeNode.getTreeNodeDesignationAsString(value, subId);
    }

    protected static String getTreeNodeDesignationAsString(String value, String subId) {
        if (subId == null) {
            return value;
        }
        return value + " (" + subId + ")";
    }

    public FormKey getFormKey() {
        return this.formKey;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AnexoTreeNode)) {
            return false;
        }
        return this.formKey.equals(((AnexoTreeNode)obj).getFormKey());
    }

    public int hashCode() {
        return this.formKey.hashCode();
    }
}

