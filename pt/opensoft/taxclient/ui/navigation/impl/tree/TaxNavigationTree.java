/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.tree;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.structural.remove.RemoveAnexoAction;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.navigation.impl.tree.AnexoTreeNode;
import pt.opensoft.taxclient.ui.navigation.impl.tree.TaxNavigationTreeModel;
import pt.opensoft.taxclient.util.TaxclientParameters;

public class TaxNavigationTree
extends JTree {
    private static final long serialVersionUID = 1072813327219334285L;

    public TaxNavigationTree() {
        this.setRootVisible(TaxclientParameters.SHOW_ANEXOS_NAVIGATION_ROOT);
        this.setShowsRootHandles(false);
        this.setEditable(false);
        this.setRowHeight(22);
        this.getSelectionModel().setSelectionMode(1);
        this.addMouseListener(new TaxNavigationTreeMouseListener());
    }

    @Override
    public void setModel(TreeModel newModel) {
        super.setModel(newModel);
        this.expandAll();
    }

    public void setModel(NavigationModel navigationModel) {
        this.setModel((TreeModel)navigationModel);
        this.addTreeSelectionListener((TaxNavigationTreeModel)navigationModel);
    }

    public void expandAll() {
        for (int i = 0; i < this.getRowCount(); ++i) {
            this.expandRow(i);
        }
    }

    public void treeNodesChanged(TreeModelEvent e) {
        this.expandAll();
    }

    public void treeNodesInserted(final TreeModelEvent e) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                TaxNavigationTree.this.expandAll();
                if (e.getTreePath() != null) {
                    TaxNavigationTree.this.setSelectionPath(e.getTreePath());
                }
            }
        });
    }

    public void treeNodesRemoved(TreeModelEvent e) {
        this.expandAll();
    }

    public void treeStructureChanged(TreeModelEvent e) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                TaxNavigationTree.this.expandAll();
            }
        });
    }

    private class ActionWrapper
    extends EnhancedAction {
        private final EnhancedAction wrappedAction;

        public ActionWrapper(String text, Icon icon, EnhancedAction wrappedAction) {
            super(text, icon);
            this.wrappedAction = wrappedAction;
            this.setEnabled(wrappedAction != null && wrappedAction.isEnabled());
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this.wrappedAction.actionPerformed(actionEvent);
        }
    }

    private class TaxNavigationTreeMouseListener
    extends MouseAdapter {
        private TaxNavigationTreeMouseListener() {
        }

        private void myPopupEvent(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();
            JTree tree = (JTree)e.getSource();
            TreePath path = tree.getPathForLocation(x, y);
            if (path == null) {
                return;
            }
            tree.setSelectionPath(path);
            if (path.getLastPathComponent() instanceof AnexoTreeNode) {
                AnexoTreeNode anexoTreeNode = (AnexoTreeNode)path.getLastPathComponent();
                FormKey formKey = anexoTreeNode.getFormKey();
                RemoveAnexoAction removeAction = ActionsTaxClientManager.getRemoveAction(formKey.getId(), formKey.getSubId());
                JPopupMenu popup = new JPopupMenu();
                Icon deleteAnexoIcon = IconFactory.getIconSmall("DeleteAnexo.png");
                ActionWrapper actionWrapper = new ActionWrapper(DesignationManager.REMOVE_ANEXO_POPUP_TEXT, deleteAnexoIcon, removeAction);
                popup.add(new JMenuItem(actionWrapper));
                popup.show(tree, x, y);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.isPopupTrigger()) {
                this.myPopupEvent(e);
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                this.myPopupEvent(e);
            }
        }
    }

}

