/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.tree;

import java.util.List;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import pt.opensoft.taxclient.ui.navigation.impl.NavActionsBean;
import pt.opensoft.taxclient.ui.navigation.impl.actions.NavAddAnexoAction;
import pt.opensoft.taxclient.ui.navigation.impl.tree.TaxNavigationTree;

public class NavTreePopupMenu
extends JPopupMenu
implements MenuListener {
    private final TYPE type;
    private final List<NavActionsBean> navActionBeans;

    public NavTreePopupMenu(TaxNavigationTree tree, List<NavActionsBean> navActionBeans, TYPE type) {
        this.type = type;
        this.navActionBeans = navActionBeans;
        for (NavActionsBean navActionsBean : navActionBeans) {
            this.add(new NavAddAnexoAction(tree, navActionsBean.getId(), navActionsBean.getMultiplicty()));
        }
    }

    @Override
    public void menuSelected(MenuEvent e) {
    }

    @Override
    public void menuCanceled(MenuEvent e) {
    }

    @Override
    public void menuDeselected(MenuEvent e) {
    }

    public static enum TYPE {
        ADD,
        REMOVE;
        

        private TYPE() {
        }
    }

}

