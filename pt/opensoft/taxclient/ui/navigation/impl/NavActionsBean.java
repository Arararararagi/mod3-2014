/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl;

public class NavActionsBean {
    public static final Integer UNBOUND = null;
    protected final String id;
    protected final Integer multiplicty;

    public NavActionsBean(String id, Integer multiplicty) {
        this.id = id;
        this.multiplicty = multiplicty;
    }

    public String getId() {
        return this.id;
    }

    public Integer getMultiplicty() {
        return this.multiplicty;
    }
}

