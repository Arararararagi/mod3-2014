/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;
import pt.opensoft.taxclient.ui.event.SelectionEvent;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.ui.navigation.impl.tree.AnexoTreeNode;
import pt.opensoft.taxclient.ui.navigation.impl.tree.DeclaracaoTreeNode;
import pt.opensoft.taxclient.ui.navigation.impl.tree.TaxNavigationTree;
import pt.opensoft.taxclient.ui.navigation.impl.tree.TaxNavigationTreeModel;

public class DefaultNavigationDisplayer
extends JPanel
implements NavigationDisplayer,
MouseListener,
TreeSelectionListener {
    private static final long serialVersionUID = 5311556361267026499L;
    protected TaxNavigationTree tree;
    private JScrollPane treeScrollPane;

    public DefaultNavigationDisplayer() {
        Border border;
        this.setLayout(new BorderLayout());
        this.initTree();
        if (this.tree != null) {
            this.treeScrollPane = new JScrollPane(this.tree);
            this.treeScrollPane.setBorder(BorderFactory.createEmptyBorder());
            this.add((Component)this.treeScrollPane, "Center");
            this.tree.addTreeSelectionListener(this);
        }
        if ((border = UIManager.getBorder("DefaultNavigationDisplayer.border")) != null) {
            super.setBorder(border);
        }
    }

    protected void initTree() {
        this.tree = new TaxNavigationTree();
    }

    @Override
    public void setModel(NavigationModel navigationModel) {
        this.tree.setModel(navigationModel);
    }

    @Override
    public void navigationItemSelected(NavigationItemEvent navigationItemSelectedEvent) {
    }

    @Override
    public FormKey getNearestFormKey(FormKey formKey) {
        DeclaracaoTreeNode root = (DeclaracaoTreeNode)this.tree.getModel().getRoot();
        int childCount = root.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            AnexoTreeNode child = (AnexoTreeNode)root.getChildAt(i);
            FormKey nodeFormKey = child.getFormKey();
            if (!nodeFormKey.equals(formKey)) continue;
            int previousIndex = i - 1;
            if (previousIndex < 0) {
                int nextIndex = i + 1;
                if (nextIndex >= childCount) continue;
                AnexoTreeNode nextNode = (AnexoTreeNode)root.getChildAt(nextIndex);
                return nextNode.getFormKey();
            }
            AnexoTreeNode previousNode = (AnexoTreeNode)root.getChildAt(previousIndex);
            return previousNode.getFormKey();
        }
        return null;
    }

    @Override
    public FormKey findNextAnexCircular(FormKey formKeySelectedAnex) {
        DeclaracaoTreeNode root = (DeclaracaoTreeNode)this.tree.getModel().getRoot();
        int childCount = root.getChildCount();
        if (childCount <= 1) {
            return null;
        }
        int foundPos = this.findChildPosOfAnexx(formKeySelectedAnex, root, childCount);
        if (foundPos < 0) {
            return null;
        }
        foundPos = foundPos + 1 >= childCount ? 0 : ++foundPos;
        AnexoTreeNode previousNode = (AnexoTreeNode)root.getChildAt(foundPos);
        return previousNode.getFormKey();
    }

    @Override
    public FormKey findPreviousAnexCircular(FormKey formKeySelectedAnex) {
        DeclaracaoTreeNode root = (DeclaracaoTreeNode)this.tree.getModel().getRoot();
        int childCount = root.getChildCount();
        if (childCount <= 1) {
            return null;
        }
        int foundPos = this.findChildPosOfAnexx(formKeySelectedAnex, root, childCount);
        if (foundPos < 0) {
            return null;
        }
        foundPos = foundPos - 1 < 0 ? childCount - 1 : --foundPos;
        AnexoTreeNode previousNode = (AnexoTreeNode)root.getChildAt(foundPos);
        return previousNode.getFormKey();
    }

    private int findChildPosOfAnexx(FormKey formKeySelectedAnex, DeclaracaoTreeNode root, int childCount) {
        int foundPos = -1;
        for (int i = 0; i < childCount; ++i) {
            AnexoTreeNode child = (AnexoTreeNode)root.getChildAt(i);
            FormKey nodeFormKey = child.getFormKey();
            if (!nodeFormKey.equals(formKeySelectedAnex)) continue;
            foundPos = i;
            break;
        }
        return foundPos;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        e.getX();
        e.getY();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void registerListener(EventListener listener) {
        if (listener instanceof TreeSelectionListener) {
            this.tree.addTreeSelectionListener((TreeSelectionListener)listener);
        }
    }

    @Override
    public void unregisterListener(EventListener listener) {
        if (listener instanceof TreeSelectionListener) {
            this.tree.removeTreeSelectionListener((TreeSelectionListener)listener);
        }
    }

    @Override
    public void setSelectionNavigationItem(Object path) {
        this.tree.setSelectionPath((TreePath)path);
    }

    @Override
    public void selectedAnexoChanged(SelectionEvent selectedEvent) {
        Object source = selectedEvent.getSource();
        if (source instanceof TaxNavigationTreeModel) {
            TaxNavigationTreeModel model = (TaxNavigationTreeModel)this.tree.getModel();
            TreeNode[] nodes = model.findChildNodePath(selectedEvent.getFormKey());
            if (nodes == null || nodes.length == 0) {
                return;
            }
            TreePath path = new TreePath(model.findChildNodePath(selectedEvent.getFormKey()));
            this.tree.scrollPathToVisible(path);
            return;
        }
        TaxNavigationTreeModel model = (TaxNavigationTreeModel)this.tree.getModel();
        TreeNode[] nodes = model.findChildNodePath(selectedEvent.getFormKey());
        if (nodes == null || nodes.length == 0) {
            return;
        }
        TreePath path = new TreePath(model.findChildNodePath(selectedEvent.getFormKey()));
        this.tree.getSelectionModel().clearSelection();
        this.tree.getSelectionModel().setSelectionPath(path);
        this.tree.setLeadSelectionPath(path);
        this.tree.scrollPathToVisible(path);
        this.tree.updateUI();
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
    }
}

