/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.actions;

import java.awt.event.ActionEvent;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.InvalidContextException;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.util.Session;

public class NavRemoveAnexoAction
extends EnhancedAction {
    public static final Integer UNBOUND = null;
    private final FormKey formKey;
    private final Integer multiplicity;

    public NavRemoveAnexoAction(FormKey formKey, Integer multiplicity) {
        super(formKey.getId(), null);
        this.formKey = formKey;
        this.multiplicity = multiplicity;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            super.actionPerformed(e);
        }
        catch (InvalidContextException ex) {
            return;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
            }
        });
    }

    @Override
    public boolean isEnabled() {
        if (this.multiplicity != null) {
            DeclaracaoMapModel<FormKey, AnexoModel> anexos = Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexos();
            int counter = 0;
            for (FormKey formKey : anexos.keySet()) {
                if (!formKey.getId().equals(this.formKey.getId())) continue;
                ++counter;
            }
            return this.multiplicity > counter;
        }
        return true;
    }

}

