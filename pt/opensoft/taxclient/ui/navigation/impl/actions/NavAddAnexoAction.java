/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl.actions;

import java.awt.event.ActionEvent;
import java.util.Set;
import javax.swing.Icon;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.InvalidContextException;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.navigation.impl.tree.TaxNavigationTree;
import pt.opensoft.taxclient.util.Session;

public class NavAddAnexoAction
extends EnhancedAction {
    public static final Integer UNBOUND = null;
    private final String id;
    private final Integer multiplicity;
    private final TaxNavigationTree tree;

    public NavAddAnexoAction(TaxNavigationTree tree, String id, Integer multiplicity) {
        super(id, null);
        this.id = id;
        this.tree = tree;
        this.multiplicity = multiplicity;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            super.actionPerformed(e);
        }
        catch (InvalidContextException ex) {
            return;
        }
    }

    @Override
    public boolean isEnabled() {
        if (this.multiplicity != null) {
            DeclaracaoMapModel<FormKey, AnexoModel> anexos = Session.getCurrentDeclaracao().getDeclaracaoModel().getAnexos();
            int counter = 0;
            for (FormKey formKey : anexos.keySet()) {
                if (!formKey.getId().equals(this.id)) continue;
                ++counter;
            }
            return this.multiplicity > counter;
        }
        return true;
    }
}

