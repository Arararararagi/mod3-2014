/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import javax.swing.JComponent;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;
import pt.opensoft.taxclient.ui.event.SelectedListener;
import pt.opensoft.taxclient.ui.event.SelectionEvent;
import pt.opensoft.taxclient.ui.layout.DummyPlug;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;

public class DummyNavigationDisplayer
extends JComponent
implements NavigationDisplayer,
DummyPlug {
    private static final long serialVersionUID = -6678558718986766909L;
    protected List<SelectedListener> selectedListeners;

    public DummyNavigationDisplayer() {
        Dimension theZeroDimension = new Dimension(0, 0);
        this.setMaximumSize(theZeroDimension);
        this.setPreferredSize(theZeroDimension);
        this.setMinimumSize(theZeroDimension);
        this.setVisible(false);
        this.selectedListeners = new ArrayList<SelectedListener>();
    }

    @Override
    public void registerListener(EventListener listener) {
        if (listener instanceof SelectedListener) {
            this.selectedListeners.add((SelectedListener)listener);
        }
    }

    @Override
    public void unregisterListener(EventListener listener) {
        if (listener instanceof SelectedListener) {
            this.selectedListeners.remove((SelectedListener)listener);
        }
    }

    @Override
    public void setModel(NavigationModel navigationModel) {
    }

    @Override
    public void navigationItemSelected(NavigationItemEvent navigationItemSelectedEvent) {
    }

    @Override
    public void setSelectionNavigationItem(Object path) {
    }

    @Override
    public void selectedAnexoChanged(SelectionEvent selectedEvent) {
        for (SelectedListener selectedListener : this.selectedListeners) {
            selectedListener.selectedAnexoChanged(selectedEvent);
        }
    }

    @Override
    public FormKey getNearestFormKey(FormKey formKey) {
        return null;
    }

    @Override
    public FormKey findNextAnexCircular(FormKey formKeySelectedAnex) {
        return null;
    }

    @Override
    public FormKey findPreviousAnexCircular(FormKey formKeySelectedAnex) {
        return null;
    }
}

