/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation.impl;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.gui.TableFormat;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;
import pt.opensoft.taxclient.ui.event.SelectionEvent;
import pt.opensoft.taxclient.ui.navigation.NavigationDisplayer;
import pt.opensoft.taxclient.ui.navigation.impl.table.AnexosDisplayerTable;
import pt.opensoft.taxclient.ui.navigation.impl.table.FormKeyAdapter;
import pt.opensoft.taxclient.ui.navigation.impl.tree.AnexoTreeNode;
import pt.opensoft.taxclient.ui.navigation.impl.tree.DeclaracaoTreeNode;
import pt.opensoft.taxclient.util.Session;

public class W3DefaultNavigationDisplayer
extends JPanel
implements NavigationDisplayer {
    private static final long serialVersionUID = -3123099424036602245L;
    AnexosDisplayerTable<FormKey> modelDisplayer;
    static NavigationModel navModel;

    public W3DefaultNavigationDisplayer() {
        Border border;
        this.setLayout(new BorderLayout());
        this.initTable();
        if (this.modelDisplayer != null) {
            JScrollPane tableScrollPane = new JScrollPane(this.modelDisplayer);
            tableScrollPane.setBorder(BorderFactory.createEmptyBorder());
            tableScrollPane.getViewport().setBackground(UIManager.getColor("DefaultNavigationDisplayer.w3.backgroundColor"));
            this.add((Component)tableScrollPane, "Center");
            this.modelDisplayer.addMouseListener((MouseListener)new MouseAdapter(){

                @Override
                public void mouseClicked(MouseEvent e) {
                    int row;
                    JTable target = (JTable)e.getSource();
                    FormKey anexo = (FormKey)((AnexosDisplayerTable)target).getElemAtRow(row = target.getSelectedRow());
                    if (anexo == null) {
                        return;
                    }
                    Session.getCurrentDeclaracao().setSelectedAnexo(anexo);
                }
            });
        }
        if ((border = UIManager.getBorder("DefaultNavigationDisplayer.border")) != null) {
            super.setBorder(border);
        } else {
            super.setBorder(BorderFactory.createEmptyBorder());
        }
    }

    private void initTable() {
        EventList anexos = GlazedLists.eventList(new LinkedList());
        this.modelDisplayer = new AnexosDisplayerTable(anexos, new FormKeyAdapter());
    }

    @Override
    public void registerListener(EventListener listener) {
    }

    @Override
    public void setModel(NavigationModel navigationModel) {
        TreeModel model = (TreeModel)navigationModel;
        navModel = navigationModel;
        DeclaracaoTreeNode root = (DeclaracaoTreeNode)model.getRoot();
        if (root.getChildCount() > 0) {
            ArrayList<FormKey> descriptors = new ArrayList<FormKey>();
            for (int i = 0; i < root.getChildCount(); ++i) {
                FormKey fKey = ((AnexoTreeNode)root.getChildAt(i)).getFormKey();
                descriptors.add(fKey);
            }
            this.modelDisplayer.setModelElems(descriptors);
        }
        model.addTreeModelListener(new TreeModelListener(){

            @Override
            public void treeNodesChanged(TreeModelEvent arg0) {
            }

            @Override
            public void treeNodesInserted(TreeModelEvent arg0) {
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent arg0) {
            }

            @Override
            public void treeStructureChanged(TreeModelEvent event) {
                DeclaracaoTreeNode root = (DeclaracaoTreeNode)event.getPath()[0];
                Enumeration children = root.children();
                ArrayList<FormKey> descriptors = new ArrayList<FormKey>();
                while (children.hasMoreElements()) {
                    FormKey fKey = ((AnexoTreeNode)children.nextElement()).getFormKey();
                    descriptors.add(fKey);
                }
                W3DefaultNavigationDisplayer.this.modelDisplayer.setModelElems(descriptors);
            }
        });
    }

    @Override
    public void setSelectionNavigationItem(Object path) {
        TreePath caminho = (TreePath)path;
        AnexoTreeNode no = (AnexoTreeNode)caminho.getLastPathComponent();
        this.modelDisplayer.setSelectedItem(no.getFormKey());
        Session.getCurrentDeclaracao().setSelectedAnexo(no.getFormKey());
    }

    @Override
    public void unregisterListener(EventListener listener) {
    }

    @Override
    public void selectedAnexoChanged(SelectionEvent selectedEvent) {
        this.modelDisplayer.setSelectedItem(selectedEvent.getFormKey());
    }

    @Override
    public void navigationItemSelected(NavigationItemEvent navigationItemSelectedEvent) {
    }

    @Override
    public FormKey getNearestFormKey(FormKey formKey) {
        TreeModel model = (TreeModel)navModel;
        DeclaracaoTreeNode root = (DeclaracaoTreeNode)model.getRoot();
        int childCount = root.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            AnexoTreeNode child = (AnexoTreeNode)root.getChildAt(i);
            FormKey nodeFormKey = child.getFormKey();
            if (!nodeFormKey.equals(formKey)) continue;
            int previousIndex = i - 1;
            if (previousIndex < 0) {
                int nextIndex = i + 1;
                if (nextIndex >= childCount) continue;
                AnexoTreeNode nextNode = (AnexoTreeNode)root.getChildAt(nextIndex);
                return nextNode.getFormKey();
            }
            AnexoTreeNode previousNode = (AnexoTreeNode)root.getChildAt(previousIndex);
            return previousNode.getFormKey();
        }
        return null;
    }

    @Override
    public FormKey findNextAnexCircular(FormKey formKeySelectedAnex) {
        TreeModel model = (TreeModel)navModel;
        DeclaracaoTreeNode root = (DeclaracaoTreeNode)model.getRoot();
        int childCount = root.getChildCount();
        if (childCount <= 1) {
            return null;
        }
        int foundPos = this.findChildPosOfAnexx(formKeySelectedAnex, root, childCount);
        if (foundPos < 0) {
            return null;
        }
        foundPos = foundPos + 1 >= childCount ? 0 : ++foundPos;
        AnexoTreeNode previousNode = (AnexoTreeNode)root.getChildAt(foundPos);
        return previousNode.getFormKey();
    }

    @Override
    public FormKey findPreviousAnexCircular(FormKey formKeySelectedAnex) {
        TreeModel model = (TreeModel)navModel;
        DeclaracaoTreeNode root = (DeclaracaoTreeNode)model.getRoot();
        int childCount = root.getChildCount();
        if (childCount <= 1) {
            return null;
        }
        int foundPos = this.findChildPosOfAnexx(formKeySelectedAnex, root, childCount);
        if (foundPos < 0) {
            return null;
        }
        foundPos = foundPos - 1 < 0 ? childCount - 1 : --foundPos;
        AnexoTreeNode previousNode = (AnexoTreeNode)root.getChildAt(foundPos);
        return previousNode.getFormKey();
    }

    private int findChildPosOfAnexx(FormKey formKeySelectedAnex, DeclaracaoTreeNode root, int childCount) {
        int foundPos = -1;
        for (int i = 0; i < childCount; ++i) {
            AnexoTreeNode child = (AnexoTreeNode)root.getChildAt(i);
            FormKey nodeFormKey = child.getFormKey();
            if (!nodeFormKey.equals(formKeySelectedAnex)) continue;
            foundPos = i;
            break;
        }
        return foundPos;
    }

}

