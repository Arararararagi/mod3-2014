/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.navigation;

import java.util.EventListener;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.event.NavigationSelectionListener;
import pt.opensoft.taxclient.ui.event.SelectedListener;

public interface NavigationDisplayer
extends NavigationSelectionListener,
SelectedListener {
    public void setModel(NavigationModel var1);

    public void registerListener(EventListener var1);

    public void unregisterListener(EventListener var1);

    public void setSelectionNavigationItem(Object var1);

    public FormKey getNearestFormKey(FormKey var1);

    public FormKey findNextAnexCircular(FormKey var1);

    public FormKey findPreviousAnexCircular(FormKey var1);
}

