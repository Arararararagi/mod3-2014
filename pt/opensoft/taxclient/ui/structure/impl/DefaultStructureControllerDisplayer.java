/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.structure.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.structural.remove.RemoveAnexoAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.structure.StructureControllerDisplayer;
import pt.opensoft.taxclient.ui.util.PopupMenu;
import pt.opensoft.taxclient.util.Session;

public class DefaultStructureControllerDisplayer
extends JPanel
implements StructureControllerDisplayer,
MouseListener {
    private static final long serialVersionUID = -6708033250965423097L;
    protected JToolBar toolbar;
    protected PopupMenu popupAddMenu;
    protected PopupMenu popupRemoveMenu;
    protected DeclaracaoModel declaracaoModel;

    public DefaultStructureControllerDisplayer() {
        this.setLayout(new BorderLayout());
        this.toolbar = new JToolBar(1);
        this.toolbar.setFloatable(false);
        this.toolbar.setRollover(true);
        this.toolbar.add(this.createAddButton());
        this.toolbar.add(this.createRemoveButton());
        if (this.toolbar != null) {
            this.add((Component)this.toolbar, "First");
        }
        this.declaracaoModel = Session.getCurrentDeclaracao().getDeclaracaoModel();
        this.popupAddMenu = new PopupMenu(ActionsTaxClientManager.getAllAddActions());
        this.popupRemoveMenu = new PopupMenu(ActionsTaxClientManager.getAllEnabledRemoveActions());
    }

    protected JButton createAddButton() {
        final JButton addButton = new JButton("Novo Anexo");
        addButton.setIcon(IconFactory.getIconSmall(GUIParameters.ICON_NOVO_ANEXO));
        addButton.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                DefaultStructureControllerDisplayer.this.popupAddMenu.show(addButton, x, y);
            }
        });
        return addButton;
    }

    protected JButton createRemoveButton() {
        final JButton removeButton = new JButton("Apagar Anexo");
        removeButton.setIcon(IconFactory.getIconSmall(GUIParameters.ICON_DELETE_ANEXO));
        removeButton.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                DefaultStructureControllerDisplayer.this.showRemoveAnexoMenu(removeButton, x, y);
            }
        });
        return removeButton;
    }

    protected void showRemoveAnexoMenu(JButton removeButton, int x, int y) {
        this.popupRemoveMenu = null;
        List<RemoveAnexoAction> list = ActionsTaxClientManager.getAllEnabledRemoveActions();
        if (list.size() == 0) {
            DialogFactory.instance().showInformationDialog("N\u00e3o existem anexos poss\u00edveis de serem removidos");
        } else {
            this.popupRemoveMenu = new PopupMenu(list);
            this.popupRemoveMenu.show(removeButton, x, y);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        e.getX();
        e.getY();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

}

