/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.structure.impl;

import java.awt.Dimension;
import javax.swing.JPanel;
import pt.opensoft.taxclient.ui.layout.DummyPlug;
import pt.opensoft.taxclient.ui.structure.StructureControllerDisplayer;

public class DummyStructureControllerDisplayer
extends JPanel
implements StructureControllerDisplayer,
DummyPlug {
    private static final long serialVersionUID = -1494265616443085682L;

    public DummyStructureControllerDisplayer() {
        Dimension theZeroDimension = new Dimension(0, 0);
        this.setMaximumSize(theZeroDimension);
        this.setPreferredSize(theZeroDimension);
        this.setMinimumSize(theZeroDimension);
        this.setVisible(false);
    }
}

