/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.structure.impl;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.KeyStroke;
import pt.opensoft.taxclient.ui.structure.impl.W3StructureControllerDisplayer;
import pt.opensoft.taxclient.ui.util.PopupMenu;

public class W3UsabilityStructureControllerDisplayer
extends W3StructureControllerDisplayer {
    private static final long serialVersionUID = 5896952224066341266L;
    private static final String REMOVER_ANEXO_SHORTCUT_KEY_ACTION = "RemoverAnexoShortcutKeyAction";
    private static final String ADICIONAR_ANEXO_SHORTCUT_KEY_ACTION = "AdicionarAnexoShortcutKeyAction";

    public W3UsabilityStructureControllerDisplayer(boolean readonly) {
        super(readonly);
        if (!readonly) {
            this.addButton.getInputMap(2).put(KeyStroke.getKeyStroke(65, 512), "AdicionarAnexoShortcutKeyAction");
            this.addButton.getInputMap(2).put(KeyStroke.getKeyStroke(65, 8192), "AdicionarAnexoShortcutKeyAction");
            this.addButton.getActionMap().put("AdicionarAnexoShortcutKeyAction", new AbstractAction(){
                private static final long serialVersionUID = 1;

                @Override
                public void actionPerformed(ActionEvent e) {
                    int x = 10;
                    int y = 10;
                    W3UsabilityStructureControllerDisplayer.this.popupAddMenu.show(W3UsabilityStructureControllerDisplayer.this.addButton, 10, 10);
                }
            });
            this.removeButton.getInputMap(2).put(KeyStroke.getKeyStroke(82, 512), "RemoverAnexoShortcutKeyAction");
            this.removeButton.getInputMap(2).put(KeyStroke.getKeyStroke(82, 8192), "RemoverAnexoShortcutKeyAction");
            this.removeButton.getActionMap().put("RemoverAnexoShortcutKeyAction", new AbstractAction(){
                private static final long serialVersionUID = 1;

                @Override
                public void actionPerformed(ActionEvent e) {
                    int x = 10;
                    int y = 10;
                    W3UsabilityStructureControllerDisplayer.this.showRemoveAnexoMenu(W3UsabilityStructureControllerDisplayer.this.removeButton, 10, 10);
                }
            });
        }
    }

}

