/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.structure.impl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.structural.remove.RemoveAnexoAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.structure.impl.DefaultStructureControllerDisplayer;
import pt.opensoft.taxclient.ui.util.PopupMenu;
import pt.opensoft.taxclient.util.Session;

public class W3StructureControllerDisplayer
extends DefaultStructureControllerDisplayer {
    protected boolean readonly;
    protected JButton addButton;
    protected JButton removeButton;

    public W3StructureControllerDisplayer(boolean readonly) {
        this.setLayout(new BorderLayout());
        this.readonly = readonly;
        if (!readonly) {
            this.toolbar = new JToolBar(0);
            this.toolbar.setFloatable(false);
            this.addButton = this.createAddButton();
            this.removeButton = this.createRemoveButton();
            this.toolbar.add(this.addButton);
            this.toolbar.add(this.removeButton);
            this.toolbar.setBackground(UIManager.getColor("Toolbar.Controller.Displayer.color"));
            if (this.toolbar != null) {
                this.add((Component)this.toolbar, "First");
            }
            this.declaracaoModel = Session.getCurrentDeclaracao().getDeclaracaoModel();
            this.popupAddMenu = new PopupMenu(ActionsTaxClientManager.getAllAddActions());
            this.popupRemoveMenu = new PopupMenu(ActionsTaxClientManager.getAllEnabledRemoveActions());
            Border border = UIManager.getBorder("W3StructureController.border");
            if (border != null) {
                super.setBorder(border);
            } else {
                super.setBorder(BorderFactory.createEmptyBorder());
            }
        } else {
            this.setMaximumSize(new Dimension(0, 0));
            this.setPreferredSize(new Dimension(0, 0));
            this.setMinimumSize(new Dimension(0, 0));
            this.setBackground(UIManager.getColor("Controller.w3.backgroundColor"));
        }
    }

    @Override
    protected JButton createAddButton() {
        final JButton addButton = new JButton("Novo Anexo");
        if (!this.readonly) {
            addButton.addMouseListener(new MouseAdapter(){

                @Override
                public void mouseReleased(MouseEvent e) {
                    int x = e.getX();
                    int y = e.getY();
                    W3StructureControllerDisplayer.this.popupAddMenu.show(addButton, x, y);
                }
            });
        }
        if (this.readonly) {
            addButton.setEnabled(false);
        }
        return addButton;
    }

    @Override
    protected JButton createRemoveButton() {
        final JButton removeButton = new JButton("Apagar Anexo");
        if (!this.readonly) {
            removeButton.addMouseListener(new MouseAdapter(){

                @Override
                public void mouseReleased(MouseEvent e) {
                    int x = e.getX();
                    int y = e.getY();
                    W3StructureControllerDisplayer.this.popupRemoveMenu = null;
                    List<RemoveAnexoAction> list = ActionsTaxClientManager.getAllEnabledRemoveActions();
                    if (list.size() == 0) {
                        DialogFactory.instance().showInformationDialog("N\u00e3o existem anexos poss\u00edveis de serem removidos");
                    } else {
                        W3StructureControllerDisplayer.this.popupRemoveMenu = new PopupMenu(list);
                        W3StructureControllerDisplayer.this.popupRemoveMenu.show(removeButton, x, y);
                    }
                }
            });
        }
        if (this.readonly) {
            removeButton.setEnabled(false);
        }
        return removeButton;
    }

}

