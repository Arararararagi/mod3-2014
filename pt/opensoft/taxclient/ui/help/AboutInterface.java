/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.help;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.JDialog;
import javax.swing.JPanel;

public interface AboutInterface {
    public void actionPerformed(ActionEvent var1);

    public JDialog createDialog(Component var1, String var2, JPanel var3);
}

