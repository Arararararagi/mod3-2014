/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.help.impl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.Icon;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.TitledPanel2;
import pt.opensoft.swing.Util;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.taxclient.actions.util.CloseBottomPanelAction;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.help.HelpDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.IndexerSearch;
import pt.opensoft.taxclient.util.ProcessResultadoXmlToHtml;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public class DefaultHelpDisplayer
extends TitledPanel2
implements HyperlinkListener,
HelpDisplayer {
    private static final long serialVersionUID = -7467325194896065428L;
    private ScrollableHtmlPane scrollableHtmlPane;
    private JScrollPane scrollPane;
    private List<String> _list_path_to_search;
    private String _path_stop_words;
    private boolean isSearchEnabled;

    public DefaultHelpDisplayer() {
        super(new BorderLayout());
        this.isSearchEnabled = false;
        this.initializeHelpPane();
    }

    public DefaultHelpDisplayer(boolean showTitleBar) {
        this(showTitleBar, IconFactory.getIconBig(GUIParameters.ICON_AJUDA_CAMPOS));
    }

    public DefaultHelpDisplayer(boolean showTitleBar, boolean isSearchEnabled) {
        this(showTitleBar, isSearchEnabled, IconFactory.getIconBig(GUIParameters.ICON_AJUDA_CAMPOS));
    }

    public DefaultHelpDisplayer(boolean showTitleBar, Icon iconBig) {
        this(showTitleBar, false, iconBig);
    }

    public DefaultHelpDisplayer(boolean showTitleBar, boolean isSearchEnabled, Icon iconBig) {
        super(iconBig, "Ajuda no preenchimento", (EnhancedAction)new CloseBottomPanelAction("Fechar painel de ajuda"), new JSearchTextField(new ScrollableHtmlPane()), GUIParameters.TITLED_PANEL_TITLE_COLOR, null);
        this.isSearchEnabled = isSearchEnabled;
        this.initializeHelpPane();
    }

    private void initializeHelpPane() {
        if (this.searchTextField != null) {
            this.searchTextField.setVisible(this.isSearchEnabled);
            this.scrollableHtmlPane = (ScrollableHtmlPane)this.searchTextField.getSearchableTextComponent();
        } else {
            this.scrollableHtmlPane = new ScrollableHtmlPane();
        }
        this.scrollableHtmlPane.setEditable(false);
        this.scrollableHtmlPane.setContentType("text/html");
        this.scrollableHtmlPane.addHyperlinkListener(this);
        StyleSheet css = new StyleSheet();
        css.importStyleSheet(Util.class.getResource("/errorsPF.css"));
        HTMLEditorKit edtKit = new HTMLEditorKit();
        edtKit.setStyleSheet(css);
        this.scrollableHtmlPane.setEditorKit(edtKit);
        this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
        this.changeFont();
        this.scrollPane = new JScrollPane(this.scrollableHtmlPane);
        this.scrollPane.getVerticalScrollBar().setUnitIncrement(this.scrollPane.getVerticalScrollBar().getBlockIncrement());
        this.addInternal(this.scrollPane, "Center");
    }

    @Override
    public void updateHelp() throws Exception {
        if (Session.getMainFrame().getMultiPurposeDisplayer().isDisplayerVisible()) {
            String helpText;
            String anexoName = Session.getCurrentDeclaracao().getSelectedAnexo() != null ? Session.getCurrentDeclaracao().getSelectedAnexo().getId() : "Rosto";
            String tabName = Session.getCurrentDeclaracao().getSelectedQuadro();
            String pageLocation = "/ajuda/" + anexoName + "/" + tabName + ".html";
            try {
                SimpleLog.log("Showing html: pageLocation = " + pageLocation);
                helpText = Util.getHtmlText(this.scrollableHtmlPane, pageLocation);
            }
            catch (IOException e) {
                helpText = "<p><i>P\u00e1gina n\u00e3o dispon\u00edvel</i></p></html>";
            }
            this.scrollableHtmlPane.setText(helpText);
            this.getAccessibleContext().setAccessibleDescription(helpText);
            this.changeFont();
        }
    }

    public void showSearchHelp(List<String> toSearch, String stopWords, String text) throws Exception {
        this._list_path_to_search = toSearch;
        this._path_stop_words = stopWords;
        this.updateSearchHelp(text);
    }

    @Override
    public void updateSearchHelp(String text) throws Exception {
        IndexerSearch idxSearch = new IndexerSearch(this._list_path_to_search, this._path_stop_words, text);
        org.jdom.Document resultadosPesquisa = idxSearch.processSearch();
        this.setTitle("" + idxSearch.getNumResultados() + " resultados para : " + text);
        ProcessResultadoXmlToHtml toHtml = new ProcessResultadoXmlToHtml(resultadosPesquisa);
        String fichHtml = toHtml.xmlToHtml();
        String helpText = "<html><p><i>P\u00e1gina n\u00e3o dispon\u00edvel</i></p></html>";
        System.out.println(fichHtml);
        try {
            SimpleLog.log("showing result html: pageLocation");
            this.scrollableHtmlPane.setText(fichHtml);
        }
        catch (Exception e) {
            this.scrollableHtmlPane.setText(helpText);
        }
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            String path = e.getDescription();
            Session.getMainFrame().getDeclarationDisplayer().goToPath(path);
        }
    }

    private void changeFont() {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)this.scrollableHtmlPane.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }

    @Override
    protected Color getTextForeground(boolean selected) {
        Color c = UIManager.getColor("TitledPanel3Erros.foreground");
        if (c != null) {
            return c;
        }
        return super.getTextForeground(selected);
    }
}

