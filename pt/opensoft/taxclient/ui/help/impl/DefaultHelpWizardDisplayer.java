/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.help.impl;

import ca.odell.glazedlists.EventList;
import com.jgoodies.binding.beans.Model;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.Icon;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import pt.opensoft.swing.EnhancedAction;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.TitledPanel2;
import pt.opensoft.swing.Util;
import pt.opensoft.swing.components.JSearchTextField;
import pt.opensoft.taxclient.actions.structural.add.single.impl.AddSingleAnexoAction;
import pt.opensoft.taxclient.actions.util.CloseBottomPanelAction;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.model.annotations.Max;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.Path;
import pt.opensoft.taxclient.ui.help.HelpWizardDisplayer;
import pt.opensoft.taxclient.ui.multi.MultiPurposeDisplayer;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;

public class DefaultHelpWizardDisplayer
extends TitledPanel2
implements HyperlinkListener,
HelpWizardDisplayer {
    private static final long serialVersionUID = -2776312995497026514L;
    private String pageLocation;
    private DeclaracaoModel declaracaoModel;
    private String modelPackage;
    private ScrollableHtmlPane scrollableHtmlPane;
    private JScrollPane scrollPane;

    public DefaultHelpWizardDisplayer(String pageLocation, String stylesheetFile, String modelPackage, DeclaracaoModel declaracaoModel) {
        super(IconFactory.getIconBig(GUIParameters.ICON_WIZARD), "Facilitador", (EnhancedAction)new CloseBottomPanelAction("Fechar facilitador"), new JSearchTextField(new ScrollableHtmlPane()), GUIParameters.TITLED_PANEL_TITLE_COLOR, null);
        this.declaracaoModel = declaracaoModel;
        this.modelPackage = modelPackage;
        this.pageLocation = pageLocation;
        this.scrollableHtmlPane = (ScrollableHtmlPane)this.searchTextField.getSearchableTextComponent();
        this.scrollableHtmlPane.setEditable(false);
        this.scrollableHtmlPane.setContentType("text/html");
        this.scrollableHtmlPane.addHyperlinkListener(this);
        StyleSheet css = new StyleSheet();
        css.importStyleSheet(Util.class.getResource("/" + stylesheetFile));
        HTMLEditorKit edtKit = new HTMLEditorKit();
        edtKit.setStyleSheet(css);
        this.scrollableHtmlPane.setEditorKit(edtKit);
        this.scrollableHtmlPane.setDocument(new HTMLDocument(css));
        this.changeFont();
        this.scrollPane = new JScrollPane(this.scrollableHtmlPane);
        this.scrollPane.getVerticalScrollBar().setUnitIncrement(this.scrollPane.getVerticalScrollBar().getBlockIncrement());
        this.add(this.scrollPane);
    }

    @Override
    public void updateHelpWizard() {
        if (Session.getMainFrame().getMultiPurposeDisplayer().isDisplayerVisible()) {
            String helpWizardText;
            try {
                SimpleLog.log("Showing html: pageLocation = " + this.pageLocation);
                helpWizardText = Util.getHtmlText(this.scrollableHtmlPane, this.pageLocation);
            }
            catch (IOException e) {
                helpWizardText = "<p><i>P\u00e1gina n\u00e3o dispon\u00edvel</i></p></html>";
            }
            this.scrollableHtmlPane.setText(helpWizardText);
            this.getAccessibleContext().setAccessibleDescription(helpWizardText);
            this.changeFont();
        }
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        try {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                QuadroModel quadroModel;
                String quadroStr;
                String helpWizardPathAsString = e.getDescription();
                HelpWizardPath helpWizardPath = HelpWizardPath.buildHelpWizardPathFromString(helpWizardPathAsString);
                Path path = helpWizardPath.getPath();
                String pathStr = path.toString();
                Object value = this.parseObject(helpWizardPath.getValor());
                String anexoStr = Path.extractAnexo(path);
                AnexoModel anexoModel = this.createAnexo(anexoStr);
                if (anexoModel != null && value != null && (quadroModel = anexoModel.getQuadro(quadroStr = Path.extractQuadro(path))) != null) {
                    String fieldStr = Path.extractCampo(path);
                    if (fieldStr != null) {
                        Method setFieldMethod = quadroModel.getClass().getMethod("set" + StringUtil.capitalizeFirstCharacter(fieldStr), value.getClass());
                        setFieldMethod.invoke(quadroModel, value);
                    } else {
                        String tableStr = Path.extractTabela(path);
                        if (tableStr != null) {
                            String columnStr = Path.extractColuna(path);
                            String columnValueStr = helpWizardPath.getColunaValor();
                            if (columnStr != null && columnValueStr != null) {
                                StringBuffer lineStrBuff = new StringBuffer();
                                this.createLineWithValue(quadroModel, anexoStr, quadroStr, tableStr, lineStrBuff, columnValueStr, value);
                                pathStr = "a" + anexoStr + ".q" + quadroStr + ".t" + tableStr + ".l" + lineStrBuff.toString() + ".c" + columnStr;
                            }
                        }
                    }
                }
                Session.getMainFrame().getDeclarationDisplayer().goToPath(pathStr);
            }
        }
        catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    private AnexoModel createAnexo(String anexoStr) throws ClassNotFoundException {
        if (anexoStr == null) {
            return null;
        }
        Class anexoClass = Class.forName(this.modelPackage + "." + anexoStr.toLowerCase() + "." + anexoStr + "Model");
        int max = ((Max)anexoClass.getAnnotation(Max.class)).value();
        if (max == -1 || max > 1) {
            throw new RuntimeException("Not Implemented!! Por enquanto o help wizard apenas aceita anexos singulares.");
        }
        if (this.declaracaoModel.getAllAnexosByType(anexoStr).isEmpty()) {
            new AddSingleAnexoAction(anexoClass, this.declaracaoModel).actionPerformed(null);
        }
        FormKey anexoFormKey = this.declaracaoModel.getAllAnexosByType(anexoStr).get(0);
        return this.declaracaoModel.getAnexo(anexoFormKey);
    }

    private Model createLineWithValue(QuadroModel quadroModel, String anexoStr, String quadroStr, String tableStr, StringBuffer lineStrBuff, String columnStr, Object value) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        Method getListMethod = quadroModel.getClass().getMethod("get" + StringUtil.capitalizeFirstCharacter(tableStr), new Class[0]);
        EventList linhas = (EventList)getListMethod.invoke(quadroModel, new Object[0]);
        Model resultLineModel = null;
        for (int i = 0; i < linhas.size(); ++i) {
            Model linha = (Model)linhas.get(i);
            String columnFieldStr = this.getPropertyNameByIndex(linha, Integer.parseInt(columnStr));
            Method getValueMethod = linha.getClass().getMethod("get" + StringUtil.capitalizeFirstCharacter(columnFieldStr), new Class[0]);
            Object currLinevalue = getValueMethod.invoke(linha, new Object[0]);
            if (currLinevalue == null || !currLinevalue.equals(value)) continue;
            resultLineModel = linha;
            lineStrBuff.delete(0, lineStrBuff.length());
            lineStrBuff.append(i + 1);
            break;
        }
        if (resultLineModel == null) {
            Class lineClass = Class.forName(this.modelPackage + "." + anexoStr.toLowerCase() + "." + StringUtil.capitalizeFirstCharacter(tableStr) + "_Linha");
            resultLineModel = (Model)lineClass.newInstance();
            linhas.add(resultLineModel);
            lineStrBuff.delete(0, lineStrBuff.length());
            lineStrBuff.append(linhas.size());
            String columnFieldStr = this.getPropertyNameByIndex(resultLineModel, Integer.parseInt(columnStr));
            Method setValueMethod = resultLineModel.getClass().getMethod("set" + StringUtil.capitalizeFirstCharacter(columnFieldStr), value.getClass());
            setValueMethod.invoke(resultLineModel, value);
        }
        return resultLineModel;
    }

    private String getPropertyNameByIndex(Model linhaModel, int index) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Class<?>[] linhaInnerClasses;
        Class propertyEnum = null;
        for (Class innerClass : linhaInnerClasses = Class.forName(linhaModel.getClass().getName() + "Base").getClasses()) {
            if (!innerClass.getSimpleName().equals("Property")) continue;
            propertyEnum = innerClass;
        }
        if (propertyEnum != null) {
            for (Class field : propertyEnum.getEnumConstants()) {
                Method getIndexMethod = field.getClass().getMethod("getIndex", new Class[0]);
                int currFieldIndex = (Integer)getIndexMethod.invoke(field, new Object[0]);
                if (currFieldIndex != index) continue;
                Method getFieldMethod = field.getClass().getMethod("getName", new Class[0]);
                return (String)getFieldMethod.invoke(field, new Object[0]);
            }
        }
        return null;
    }

    private Object parseObject(String value) {
        if (value == null || value.equals("null")) {
            return null;
        }
        if (StringUtil.isNumeric(value)) {
            return Long.parseLong(value);
        }
        if (StringUtil.in(value, new String[]{"true", "false"})) {
            return new Boolean(value);
        }
        return value;
    }

    private void changeFont() {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)this.scrollableHtmlPane.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }

    @Override
    protected Color getTextForeground(boolean selected) {
        Color c = UIManager.getColor("TitledPanel3Erros.foreground");
        if (c != null) {
            return c;
        }
        return UIManager.getColor(selected ? "InternalFrame.activeTitleForeground" : "Label.foreground");
    }

    private static class HelpWizardPath {
        protected Path path;
        protected String colunaValor;
        protected String valor;

        private HelpWizardPath(Path path, String colunaValor, String valor) {
            this.path = path;
            this.colunaValor = colunaValor;
            this.valor = valor;
        }

        public static HelpWizardPath buildHelpWizardPathFromString(String helpWizardPathAsString) {
            String[] splittedStr = helpWizardPathAsString.split("\\.v");
            String valor = splittedStr.length > 1 ? splittedStr[1] : null;
            splittedStr = splittedStr[0].split("\\.cv");
            String colunaValor = splittedStr.length > 1 ? splittedStr[1] : null;
            Path path = Path.buildPathFromString(splittedStr[0]);
            return new HelpWizardPath(path, colunaValor, valor);
        }

        public Path getPath() {
            return this.path;
        }

        public String getColunaValor() {
            return this.colunaValor;
        }

        public String getValor() {
            return this.valor;
        }
    }

}

