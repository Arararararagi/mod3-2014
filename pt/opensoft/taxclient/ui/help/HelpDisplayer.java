/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.help;

public interface HelpDisplayer {
    public void updateHelp() throws Exception;

    public void updateSearchHelp(String var1) throws Exception;
}

