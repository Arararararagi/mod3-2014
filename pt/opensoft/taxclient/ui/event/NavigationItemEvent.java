/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.event;

import java.util.EventObject;
import pt.opensoft.taxclient.model.FormKey;

public class NavigationItemEvent
extends EventObject {
    private static final long serialVersionUID = 7176922690310399396L;
    private final NavigationItemEventType type;
    private final FormKey formKey;

    public NavigationItemEvent(Object source, NavigationItemEventType type, FormKey formKey) {
        super(source);
        this.type = type;
        this.formKey = formKey;
    }

    public NavigationItemEventType getType() {
        return this.type;
    }

    public FormKey getFormKey() {
        return this.formKey;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[type=" + (Object)this.type + " formKey=" + this.formKey + "]";
    }

    public static enum NavigationItemEventType {
        CREATED,
        REMOVED,
        SELECTED,
        CHANGED;
        

        private NavigationItemEventType() {
        }
    }

}

