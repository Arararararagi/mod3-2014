/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.event;

import java.util.EventListener;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;

public interface NavigationSelectionListener
extends EventListener {
    public void navigationItemSelected(NavigationItemEvent var1);
}

