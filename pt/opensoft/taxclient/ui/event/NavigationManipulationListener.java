/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.event;

import java.util.EventListener;
import pt.opensoft.taxclient.ui.event.NavigationItemEvent;

public interface NavigationManipulationListener
extends EventListener {
    public void navigationItemRemoved(NavigationItemEvent var1);

    public void navigationItemAdded(NavigationItemEvent var1);

    public void navigationItemChanged(NavigationItemEvent var1);
}

