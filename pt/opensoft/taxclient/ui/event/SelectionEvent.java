/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.event;

import java.util.EventObject;
import pt.opensoft.taxclient.model.FormKey;

public class SelectionEvent
extends EventObject {
    private static final long serialVersionUID = 6972719618841254069L;
    private final FormKey formKey;

    public SelectionEvent(Object source, FormKey formKey) {
        super(source);
        this.formKey = formKey;
    }

    public FormKey getFormKey() {
        return this.formKey;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[formId=" + this.formKey + "]";
    }
}

