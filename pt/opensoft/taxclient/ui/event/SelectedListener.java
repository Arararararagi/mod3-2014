/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.ui.event;

import java.util.EventListener;
import pt.opensoft.taxclient.ui.event.SelectionEvent;

public interface SelectedListener
extends EventListener {
    public void selectedAnexoChanged(SelectionEvent var1);
}

