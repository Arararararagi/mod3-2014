/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence;

import ca.odell.glazedlists.EventList;
import com.jgoodies.binding.list.SelectionInList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.xml.sax.helpers.DefaultHandler;
import pt.opensoft.io.WriterAppender;
import pt.opensoft.swing.binding.DeclaracaoMapModel;
import pt.opensoft.swing.handlers.FieldHandler;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.ComplexField;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.SchemaNamespace;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.text.Appender;
import pt.opensoft.text.Base64Encoder;
import pt.opensoft.text.Renderer;
import pt.opensoft.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.NumberUtil;
import pt.opensoft.util.Reflections;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.XmlRenderer;
import pt.opensoft.xml.XmlUtil;

public class DeclarationWriter
extends DefaultHandler {
    public static final String XSI_NAMESPACE = "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
    public static final String WRITE_TABELA_EMPTY_LINES = "writeTabelaEmptyLines";
    private static final char DECIMAL_SEPARATOR = '.';
    private Boolean lazyLoad;
    private Boolean serverSide;
    private ArrayList<FieldHandlerModel> existingFields;
    private Map<Class, Method[]> methodsQuadrosCache;
    private Map<Class, Method[]> methodsTabelasCache;

    public void write(Writer writer, DeclaracaoModel model, Map attributes, ArrayList<FieldHandlerModel> existingFields, boolean lazyLoad, boolean serverSide) throws Exception {
        this.write(writer, model, attributes, existingFields, lazyLoad, serverSide, TaxclientParameters.isToWriteTabelaEmptyLines());
    }

    public void write(Writer writer, DeclaracaoModel model, Map attributes, ArrayList<FieldHandlerModel> existingFields, boolean lazyLoad, boolean serverSide, boolean writeEmptyLines) throws Exception {
        WriterAppender writerAppender = null;
        try {
            this.existingFields = existingFields;
            this.lazyLoad = lazyLoad;
            this.serverSide = serverSide;
            this.methodsQuadrosCache = new HashMap<Class, Method[]>();
            this.methodsTabelasCache = new HashMap<Class, Method[]>();
            writerAppender = new WriterAppender(writer);
            XmlStreamRenderer renderer = new XmlStreamRenderer(writerAppender);
            renderer.init();
            this.writeHeader(renderer, model, attributes);
            renderer.increaseDepth();
            ArrayList<AnexoModel> annexs = new ArrayList<AnexoModel>();
            annexs.addAll(model.getAnexos().values());
            for (AnexoModel anexoModel : annexs) {
                this.writeAnexo(renderer, anexoModel, writeEmptyLines);
            }
            renderer.decreaseDepth();
            this.writeFooter(renderer, model);
            renderer.reset();
            writerAppender.flush();
            writer.flush();
        }
        finally {
            writer.close();
            writerAppender.close();
            writerAppender = null;
        }
    }

    private void writeHeader(XmlStreamRenderer renderer, DeclaracaoModel model, Map attributes) {
        StringBuffer rootAttributes = new StringBuffer();
        SchemaNamespace schemaNamespace = (SchemaNamespace)model.getClass().getAnnotation(SchemaNamespace.class);
        rootAttributes.append("xmlns=\"").append(schemaNamespace.value()).append("\" ");
        rootAttributes.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"").append(" ");
        rootAttributes.append("xsi:schemaLocation=\"").append(schemaNamespace.value()).append(" ").append(model.getDeclaracaoName()).append(".xsd").append("\" ");
        rootAttributes.append("versao=\"").append(model.getVersion()).append("\" ");
        if (attributes != null) {
            for (Object key : attributes.keySet()) {
                rootAttributes.append(key).append("=\"").append(attributes.get(key)).append("\" ");
            }
        }
        renderer.startTag(model.getDeclaracaoName(), rootAttributes.toString(), true);
    }

    private void writeAnexo(XmlStreamRenderer renderer, AnexoModel anexoModel, boolean writeEmptyLines) throws Exception {
        String anexoName = anexoModel.getFormKey().getId();
        if (anexoModel.getFormKey().getSubId() != null) {
            renderer.startTag(anexoName, "id=\"" + anexoModel.getFormKey().getSubId() + "\"", true);
        } else {
            renderer.startTag(anexoName, true);
        }
        renderer.increaseDepth();
        List<QuadroModel> quadros = anexoModel.getQuadros();
        for (QuadroModel quadroModel : quadros) {
            this.writeQuadro(quadroModel, renderer, writeEmptyLines);
        }
        renderer.decreaseDepth();
        renderer.endTag(anexoName);
    }

    private void writeQuadro(QuadroModel quadroModel, XmlStreamRenderer renderer, boolean writeEmptyLines) throws Exception {
        Class quadroClass = quadroModel.getClass();
        renderer.startTag(quadroModel.getClass().getSimpleName().replace((CharSequence)"Model", (CharSequence)"").replace((CharSequence)"_", (CharSequence)"-"));
        renderer.increaseDepth();
        Method[] methods = this.methodsQuadrosCache.get(quadroClass);
        if (methods == null) {
            methods = new Reflections().getAllDeclaredMethods(quadroClass).startingWith("get").onlyPublic().withAnnotation(Type.class).methods();
            this.methodsQuadrosCache.put(quadroClass, methods);
        }
        block5 : for (Method method : methods) {
            Type type = (Type)method.getAnnotation(Type.class);
            switch (type.value()) {
                case CAMPO: {
                    this.writeSimpleField(renderer, method, quadroModel);
                    continue block5;
                }
                case TABELA: {
                    this.writeTableField(renderer, method, quadroModel, writeEmptyLines);
                    continue block5;
                }
                case QUADRO_REPETITIVO: {
                    this.writeQuadroRepetitivoField(renderer, method, quadroModel, writeEmptyLines);
                    continue block5;
                }
                default: {
                    throw new IllegalArgumentException("Don't know how to write this type of field:" + (Object)type.value());
                }
            }
        }
        renderer.decreaseDepth();
        renderer.endTag(quadroModel.getClass().getSimpleName().replace((CharSequence)"Model", (CharSequence)"").replace((CharSequence)"_", (CharSequence)"-"));
    }

    private boolean writeSimpleField(XmlStreamRenderer renderer, Method getter, Object model) throws Exception {
        Method attrGetter;
        Object returnValueObj;
        Object value = getter.invoke(model, new Object[0]);
        String valueStr = this.getValueForXml(value, getter);
        Map<String, String> attrMap = this.getAttributesForXml(value, getter);
        try {
            attrGetter = model.getClass().getMethod(getter.getName() + "_Attr", new Class[0]);
        }
        catch (NoSuchMethodException e) {
            attrGetter = null;
        }
        if (attrGetter != null && (returnValueObj = attrGetter.invoke(model, new Object[0])) != null) {
            Reflections.MethodListReflections methodList = new Reflections().getAllDeclaredMethods(returnValueObj.getClass()).onlyPublic();
            Method[] getters = methodList.startingWith("get").methods();
            Method[] gettersIs = methodList.startingWith("is").methods();
            attrMap.putAll(this.processAttributesFromMethods(getters, returnValueObj));
            attrMap.putAll(this.processAttributesFromMethods(gettersIs, returnValueObj));
        }
        String attributeString = this.writeMapAsAttributeString(attrMap);
        if (!(valueStr == null && StringUtil.isEmpty(attributeString))) {
            renderer.tag(getter.getName().replace((CharSequence)"get", (CharSequence)"").replace((CharSequence)"_", (CharSequence)"-"), attributeString, valueStr == null ? "" : XmlUtil.standardize(XmlUtil.escapeElementEntities(valueStr)));
            return true;
        }
        return false;
    }

    private Map<String, String> processAttributesFromMethods(Method[] methods, Object attrClass) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        HashMap<String, String> attributesMap = new HashMap<String, String>();
        for (int i = 0; i < methods.length; ++i) {
            Method method = methods[i];
            String attrName = method.getName().startsWith("get") ? method.getName().substring(3) : method.getName().substring(2);
            Object invocationResult = method.invoke(attrClass, new Object[0]);
            if (invocationResult == null) continue;
            String attrValue = String.valueOf(invocationResult);
            attributesMap.put(attrName, attrValue);
        }
        return attributesMap;
    }

    private Map<String, String> getAttributesForXml(Object value, Method getter) throws Exception {
        HashMap<String, String> attributesMap = new HashMap<String, String>();
        if (getter.getAnnotation(ComplexField.class) != null && value != null) {
            Map<String, String> maps;
            FieldHandler fileHandler;
            Class clazz = Class.forName(((ComplexField)getter.getAnnotation(ComplexField.class)).value(), true, this.getClass().getClassLoader());
            Class[] argsClass = new Class[]{Boolean.TYPE, Boolean.TYPE};
            Constructor constructor = clazz.getConstructor(argsClass);
            Object[] args = new Object[]{this.lazyLoad, this.serverSide};
            if (!this.serverSide.booleanValue() && this.existingFields != null && this.existingFields.contains((FieldHandlerModel)value)) {
                args[0] = Boolean.TRUE;
            }
            if ((maps = (fileHandler = (FieldHandler)constructor.newInstance(args)).getFieldAttributes((FieldHandlerModel)value)) != null) {
                attributesMap.putAll(maps);
            }
        }
        return attributesMap;
    }

    private String writeMapAsAttributeString(Map<String, String> attrMap) {
        if (attrMap == null || attrMap.isEmpty()) {
            return "";
        }
        StringBuffer attributes = new StringBuffer();
        attributes.append(" ");
        Iterator<String> iterator = attrMap.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String attribute = attrMap.get(key);
            attribute = XmlUtil.escapeElementEntities(attribute);
            attribute.replaceAll("\"", "&quot;");
            attributes.append(DeclarationWriter.firstToLower(key)).append("=\"").append(attribute).append("\"");
            if (!iterator.hasNext()) continue;
            attributes.append(" ");
        }
        return attributes.toString();
    }

    private static final String firstToUp(String str) {
        return new String(new StringBuilder().append("").append(str.charAt(0)).toString()).toUpperCase() + str.substring(1);
    }

    private static final String firstToLower(String str) {
        return new String(new StringBuilder().append("").append(str.charAt(0)).toString()).toLowerCase() + str.substring(1);
    }

    private boolean writeTableField(XmlStreamRenderer renderer, Method listGetter, Object quadroModel, boolean writeEmptyLines) throws Exception {
        Method attrGetter;
        Object returnValueObj;
        Object value = listGetter.invoke(quadroModel, new Object[0]);
        Map<String, String> attrMap = this.getAttributesForXml(value, listGetter);
        try {
            attrGetter = quadroModel.getClass().getMethod(listGetter.getName() + "_Attr", new Class[0]);
        }
        catch (NoSuchMethodException e) {
            attrGetter = null;
        }
        if (attrGetter != null && (returnValueObj = attrGetter.invoke(quadroModel, new Object[0])) != null) {
            Reflections.MethodListReflections methodList = new Reflections().getAllDeclaredMethods(returnValueObj.getClass()).onlyPublic();
            Method[] getters = methodList.startingWith("get").methods();
            Method[] gettersIs = methodList.startingWith("is").methods();
            attrMap.putAll(this.processAttributesFromMethods(getters, returnValueObj));
            gettersIs = new Reflections().getAllDeclaredMethods(returnValueObj.getClass()).onlyPublic().startingWith("is").methods();
            attrMap.putAll(this.processAttributesFromMethods(gettersIs, returnValueObj));
        }
        String attributeString = this.writeMapAsAttributeString(attrMap);
        renderer.startTag(listGetter.getName().replace((CharSequence)"get", (CharSequence)"").replace((CharSequence)"_LinhaModel", (CharSequence)"").replace((CharSequence)"_", (CharSequence)"-"), attributeString);
        renderer.increaseDepth();
        boolean instanceHasValues = false;
        EventList valuesList = (EventList)listGetter.invoke(quadroModel, new Object[0]);
        int currentLine = 1;
        for (Object lineInstance : valuesList) {
            Class lineClass = lineInstance.getClass();
            StringWriter stringWriter = new StringWriter();
            XmlStreamRenderer lineRenderer = new XmlStreamRenderer(new WriterAppender(stringWriter));
            lineRenderer.startTag(lineClass.getSimpleName().replace((CharSequence)"_", (CharSequence)"-"), "numero=\"" + currentLine++ + "\"");
            lineRenderer.increaseDepth();
            boolean lineInstanceHasValues = false;
            Method[] getters = this.methodsTabelasCache.get(lineClass);
            if (getters == null) {
                getters = new Reflections().getAllDeclaredMethods(lineClass).startingWith("get").onlyPublic().withAnnotation(Type.class).methods();
                this.methodsTabelasCache.put(lineClass, getters);
            }
            for (Method getter : getters) {
                Type type = (Type)getter.getAnnotation(Type.class);
                if (type.value().equals((Object)Type.TYPE.CAMPO)) {
                    if (!this.writeSimpleField(lineRenderer, getter, lineInstance)) continue;
                    lineInstanceHasValues = true;
                    continue;
                }
                throw new IllegalArgumentException("Tables are not allowed to be inside other tables");
            }
            lineRenderer.decreaseDepth();
            lineRenderer.endTag(lineClass.getSimpleName().replace((CharSequence)"_", (CharSequence)"-"));
            if (!lineInstanceHasValues && !writeEmptyLines) continue;
            renderer.appendXml(stringWriter.getBuffer().toString());
            instanceHasValues = true;
        }
        renderer.decreaseDepth();
        renderer.endTag(listGetter.getName().replace((CharSequence)"get", (CharSequence)"").replace((CharSequence)"_LinhaModel", (CharSequence)"").replace((CharSequence)"_", (CharSequence)"-"));
        return instanceHasValues;
    }

    private void writeQuadroRepetitivoField(XmlStreamRenderer renderer, Method listGetter, QuadroModel quadroModel, boolean writeEmptyLines) throws Exception {
        renderer.startTag(listGetter.getName().replace((CharSequence)"get", (CharSequence)"").replace((CharSequence)"ListModel", (CharSequence)"").replace((CharSequence)"_", (CharSequence)"-"));
        renderer.increaseDepth();
        SelectionInList valuesList = (SelectionInList)listGetter.invoke(quadroModel, new Object[0]);
        int currentLine = 1;
        Class lineClass = null;
        Method[] getters = null;
        if (valuesList.getSize() > 0) {
            lineClass = valuesList.getList().get(0).getClass();
            getters = new Reflections().getAllDeclaredMethods(lineClass).startingWith("get").onlyPublic().withAnnotation(Type.class).methods();
        }
        for (Object lineInstance : valuesList.getList()) {
            StringWriter stringWriter = new StringWriter();
            XmlStreamRenderer lineRenderer = new XmlStreamRenderer(new WriterAppender(stringWriter));
            lineRenderer.startTag(lineClass.getSimpleName().replace((CharSequence)"_", (CharSequence)"-"), "numero=\"" + currentLine++ + "\"");
            lineRenderer.increaseDepth();
            boolean lineInstanceHasValues = false;
            for (Method getter : getters) {
                Type type = (Type)getter.getAnnotation(Type.class);
                if (type.value().equals((Object)Type.TYPE.CAMPO)) {
                    if (!this.writeSimpleField(lineRenderer, getter, lineInstance)) continue;
                    lineInstanceHasValues = true;
                    continue;
                }
                if (type.value().equals((Object)Type.TYPE.TABELA)) {
                    if (!this.writeTableField(lineRenderer, getter, lineInstance, writeEmptyLines)) continue;
                    lineInstanceHasValues = true;
                    continue;
                }
                throw new IllegalArgumentException("Quadros repetitivos are not allowed to be inside other quadros repetitivos");
            }
            lineRenderer.decreaseDepth();
            lineRenderer.endTag(lineClass.getSimpleName().replace((CharSequence)"_", (CharSequence)"-"));
            if (!lineInstanceHasValues) continue;
            renderer.appendXml(stringWriter.getBuffer().toString());
        }
        renderer.decreaseDepth();
        renderer.endTag(listGetter.getName().replace((CharSequence)"get", (CharSequence)"").replace((CharSequence)"ListModel", (CharSequence)"").replace((CharSequence)"_", (CharSequence)"-"));
    }

    private String getValueForXml(Object value, Method getter) throws Exception {
        if (value == null) {
            return null;
        }
        if (value instanceof Date) {
            Date dateValue = (Date)value;
            return dateValue.format("yyyy-MM-dd");
        }
        if (value instanceof DateTime) {
            DateTime dateTimeValue = (DateTime)value;
            return dateTimeValue.format("yyyy-MM-dd'T'HH:mm:ss'Z'");
        }
        if (value instanceof byte[]) {
            return Base64Encoder.encodeBuffer((byte[])value);
        }
        if (value instanceof Number) {
            String fieldStringValue;
            if (getter.getAnnotation(FractionDigits.class) != null) {
                int fractionDigits = ((FractionDigits)getter.getAnnotation(FractionDigits.class)).value();
                fieldStringValue = NumberUtil.toXSDNumericType((Long)value, fractionDigits, '.');
            } else {
                fieldStringValue = String.valueOf(value);
            }
            return fieldStringValue;
        }
        if (getter.getAnnotation(ComplexField.class) != null) {
            Class clazz = Class.forName(((ComplexField)getter.getAnnotation(ComplexField.class)).value(), true, this.getClass().getClassLoader());
            Class[] argsClass = new Class[]{Boolean.TYPE, Boolean.TYPE};
            Constructor constructor = clazz.getConstructor(argsClass);
            Object[] args = new Object[]{this.lazyLoad, this.serverSide};
            if (!this.serverSide.booleanValue() && this.existingFields != null && this.existingFields.contains((FieldHandlerModel)value)) {
                args[0] = Boolean.TRUE;
            }
            FieldHandler fileHandler = (FieldHandler)constructor.newInstance(args);
            return fileHandler.getFieldInfo((FieldHandlerModel)value);
        }
        return value.toString();
    }

    private void writeFooter(XmlStreamRenderer renderer, DeclaracaoModel model) {
        renderer.endTag(model.getDeclaracaoName());
    }

    class XmlStreamRenderer
    extends XmlRenderer {
        private int currentDepth;

        public XmlStreamRenderer(WriterAppender writerAppender) {
            super(writerAppender);
            this.currentDepth = 0;
        }

        @Override
        public XmlRenderer startTag(String name, String attributes, boolean newLine) {
            this.append(StringUtil.padSpaces("", this.currentDepth * 2));
            return super.startTag(name, attributes, newLine);
        }

        @Override
        public XmlRenderer endTag(String name, boolean newLine) {
            this.append(StringUtil.padSpaces("", this.currentDepth * 2));
            return super.endTag(name, newLine);
        }

        @Override
        public XmlRenderer tag(String name) {
            this.append(StringUtil.padSpaces("", this.currentDepth * 2));
            return super.tag(name);
        }

        @Override
        public XmlRenderer tag(String name, String value) {
            this.append(StringUtil.padSpaces("", this.currentDepth * 2));
            super.startTag(name, false);
            super.text(value, false);
            super.endTag(name);
            return this;
        }

        @Override
        public XmlRenderer tag(String name, String attributes, String text, boolean newLine) {
            this.append(StringUtil.padSpaces("", this.currentDepth * 2));
            super.startTag(name, attributes, newLine);
            super.text(text, newLine);
            super.endTag(name, true);
            return this;
        }

        public void appendXml(String xml) {
            BufferedReader reader = new BufferedReader(new StringReader(xml));
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    this.append(StringUtil.padSpaces("", this.currentDepth * 2));
                    this.append(line);
                    this.newLine();
                }
            }
            catch (IOException e) {
                throw new RuntimeException("Couldn't append xml", e);
            }
        }

        public void increaseDepth() {
            ++this.currentDepth;
        }

        public void decreaseDepth() {
            --this.currentDepth;
        }
    }

}

