/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence;

import ca.odell.glazedlists.EventList;
import com.jgoodies.binding.list.SelectionInList;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import pt.opensoft.logging.Logger;
import pt.opensoft.swing.handlers.FieldHandler;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.app.InvalidMessageException;
import pt.opensoft.taxclient.model.AnexoModel;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.FormKey;
import pt.opensoft.taxclient.model.QuadroModel;
import pt.opensoft.taxclient.persistence.ComplexField;
import pt.opensoft.taxclient.persistence.FractionDigits;
import pt.opensoft.taxclient.persistence.Type;
import pt.opensoft.taxclient.persistence.exception.DuplicateAnexoIdentificationException;
import pt.opensoft.taxclient.persistence.exception.InvalidAnexoSubIdException;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.forms.DeclarationDisplayer;
import pt.opensoft.taxclient.ui.forms.impl.AnexoTabbedPane;
import pt.opensoft.taxclient.util.XmlProcessorHandler;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.NumberUtil;
import pt.opensoft.util.Reflections;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.XmlDateTime;

public class DeclarationReaderHandler
extends XmlProcessorHandler {
    private static final int DEPTH_TAX = 1;
    private static final int DEPTH_ANEXO = 2;
    private static final int DEPTH_QUADRO = 3;
    private static final int DEPTH_CAMPO_OR_TABELA_OR_QUADRO_REPETITIVO = 4;
    private static final int DEPTH_LINHA = 5;
    private static final int DEPTH_COLUNA_OR_CAMPO_QUADRO_REP_OR_TABELA_QUAD_REP = 6;
    private static final int DEPTH_LINHA_TAB_QUAD_REP = 7;
    private static final int DEPTH_COLUNA_TAB_QUAD_REP = 8;
    private static final char DECIMAL_SEPARATOR = '.';
    private Context context;
    private StringBuffer buffer;
    private String packageBase;
    private Boolean lazyLoad;
    private Boolean serverSide;
    private static final String SUB_ID_ANEXO_ATTRIBUTE = "id";
    private static final String VERSAO_ATTRIBUTE = "versao";

    public DeclarationReaderHandler(String packageName, DeclaracaoModel declaracaoModel, boolean stopChainOnError, boolean lazyLoad, boolean serverSide) {
        super(stopChainOnError);
        this.packageBase = packageName;
        this.context = new Context();
        this.context.setDeclaracao(declaracaoModel);
        this.buffer = new StringBuffer();
        this.lazyLoad = lazyLoad;
        this.serverSide = serverSide;
    }

    @Type(value=Type.TYPE.CAMPO)
    @Override
    protected boolean startElement(String uri, String localName, String name, Map attributes, int depth) throws Exception {
        String setterName = DeclarationReaderHandler.buildSetterMethodName(localName);
        String getterName = DeclarationReaderHandler.buildGetterMethodName(localName);
        String className = DeclarationReaderHandler.standardJavaClassName(localName);
        block4 : switch (depth) {
            case 1: {
                this.context.setAttributes(attributes);
                String versaoAttribute = (String)attributes.get("versao");
                if (versaoAttribute == null) {
                    throw new RuntimeException("Root tag is missing an attribute 'versao'. Please include this in your declaration schema.");
                }
                if (this.context.getDeclaracao().getVersion() != Integer.parseInt(versaoAttribute)) {
                    SimpleLog.log("The version of the file is not compatible to the version of the model. You'll probably want to instantiate a different model.");
                    throw new InvalidMessageException("Vers\u00e3o do ficheiro inv\u00e1lida para esta aplica\u00e7\u00e3o.");
                }
                if (this.context.getDeclaracao().getDeclaracaoName().equals(name)) break;
                SimpleLog.log("Root tag of the file is not compatible to the model. You'll probably want to instantiate a different model.");
                throw new InvalidMessageException("Formato do ficheiro inv\u00e1lido para esta aplica\u00e7\u00e3o.");
            }
            case 2: {
                FormKey key;
                this.context.setAttributes(attributes);
                Class[] intArgsClass = new Class[]{FormKey.class, Boolean.TYPE};
                Class modelClass = ClassUtil.getClass(new StringBuilder().append(this.packageBase).append(".model.").append(localName).append(".").toString().toLowerCase() + localName + "Model");
                Constructor argsConstructor = modelClass.getConstructor(intArgsClass);
                String subId = (String)attributes.get("id");
                if (subId == null) {
                    key = new FormKey(modelClass);
                } else {
                    if (subId.contains((CharSequence)".") || subId.contains((CharSequence)"|")) {
                        throw new InvalidAnexoSubIdException("SubId de anexo invalido! SubID: " + subId, subId);
                    }
                    key = new FormKey(modelClass, subId);
                }
                AnexoModel anexoModel = (AnexoModel)argsConstructor.newInstance(key, true);
                this.context.setAnexo(anexoModel);
                if (this.context.getDeclaracao().getAnexo(key) != null) {
                    throw new DuplicateAnexoIdentificationException("Leitura de anexo duplicado! ID: " + key.getId() + " SubID: " + key.getSubId(), key.getId(), key.getSubId());
                }
                this.context.getDeclaracao().addAnexo(this.context.getAnexo());
                break;
            }
            case 3: {
                Class quadroClass;
                this.context.setAttributes(attributes);
                try {
                    quadroClass = ClassUtil.getClass(this.packageBase + ".model." + this.context.getAnexo().getFormKey().getId().toLowerCase() + "." + className);
                }
                catch (Error e) {
                    throw new RuntimeException("Invalid form tag name:" + className);
                }
                if (this.context.getAnexo().getQuadro(quadroClass.getSimpleName()) != null) {
                    this.context.setQuadro(this.context.getAnexo().getQuadro(quadroClass.getSimpleName()));
                    break;
                }
                QuadroModel quadroModel = (QuadroModel)ClassUtil.newInstance(quadroClass);
                this.context.getAnexo().addQuadro(quadroModel);
                this.context.setQuadro(quadroModel);
                break;
            }
            case 4: {
                Method[] methods;
                this.context.setAttributes(attributes);
                this.context.setAttributeClassInstance(null);
                QuadroModel quadro = this.context.getQuadro();
                for (Method method : methods = quadro.getClass().getMethods()) {
                    if (!method.getName().trim().equals(setterName.trim())) continue;
                    if (method.getAnnotation(Type.class) == null) break block4;
                    Type.TYPE type = ((Type)method.getAnnotation(Type.class)).value();
                    String attributeMethodName = getterName + "_Attr";
                    try {
                        Method attrMethod = quadro.getClass().getMethod(attributeMethodName, new Class[0]);
                        this.context.setAttributeClassInstance(attrMethod.invoke(quadro, new Object[0]));
                    }
                    catch (NoSuchMethodException e) {
                        // empty catch block
                    }
                    this.context.setSetter(method);
                    Method getter = this.context.getQuadro().getClass().getMethod(getterName, new Class[0]);
                    switch (type) {
                        case CAMPO: {
                            this.buffer = new StringBuffer();
                            this.context.readingCampo = true;
                            this.context.readingTabela = false;
                            this.context.readingQuadroRepetitivo = false;
                            this.readAttributes();
                            break block4;
                        }
                        case TABELA: {
                            EventList tabela = (EventList)getter.invoke(this.context.getQuadro(), new Object[0]);
                            java.lang.reflect.Type tabelaLineType = getter.getGenericReturnType();
                            this.context.setTabela(tabela);
                            this.context.setTabelaLineType(tabelaLineType);
                            this.context.readingTabela = true;
                            this.context.readingCampo = false;
                            this.context.readingQuadroRepetitivo = false;
                            this.readAttributes();
                            break block4;
                        }
                        case QUADRO_REPETITIVO: {
                            SelectionInList quadroRepetitivoInstance = (SelectionInList)getter.invoke(this.context.getQuadro(), new Object[0]);
                            java.lang.reflect.Type quadroRepetitivoLineType = getter.getGenericReturnType();
                            this.context.setQuadroRepetitivo(quadroRepetitivoInstance);
                            this.context.setQuadroLineType(quadroRepetitivoLineType);
                            this.context.readingQuadroRepetitivo = true;
                            this.context.readingCampo = false;
                            this.context.readingTabela = false;
                        }
                    }
                    break block4;
                }
                break;
            }
            case 5: {
                this.context.setAttributes(attributes);
                if (this.context.readingTabela()) {
                    ParameterizedType tabelaLineType = (ParameterizedType)this.context.getTabelaLineType();
                    this.context.setTabelaLineInstance(((Class)tabelaLineType.getActualTypeArguments()[0]).newInstance());
                }
                if (!this.context.readingQuadroRepetitivo()) break;
                ParameterizedType quadroLineType = (ParameterizedType)this.context.getQuadroLineType();
                this.context.setQuadroLineInstance(((Class)quadroLineType.getActualTypeArguments()[0]).newInstance());
                break;
            }
            case 6: {
                Class setterType;
                this.context.setAttributes(attributes);
                this.buffer = new StringBuffer();
                if (this.context.readingTabela()) {
                    setterType = this.context.getTabelaLineInstance().getClass().getMethod(getterName, new Class[0]).getReturnType();
                    this.context.setSetterTabela(this.context.getTabelaLineInstance().getClass().getMethod(setterName, setterType));
                }
                if (!this.context.readingQuadroRepetitivo()) break;
                setterType = this.context.getQuadroLineInstance().getClass().getMethod(getterName, new Class[0]).getReturnType();
                this.context.setSetterQuadroRepetitivo(this.context.getQuadroLineInstance().getClass().getMethod(setterName, setterType));
                if (this.context.getSetterQuadroRepetitivo().getAnnotation(Type.class) == null) break;
                Type.TYPE type = ((Type)this.context.getSetterQuadroRepetitivo().getAnnotation(Type.class)).value();
                String attributeMethodName = getterName + "_Attr";
                switch (type) {
                    case CAMPO: {
                        try {
                            Method attrMethod = this.context.getQuadroLineInstance().getClass().getMethod(attributeMethodName, new Class[0]);
                            this.context.setAttributeClassInstance(attrMethod.invoke(this.context.getQuadroLineInstance(), new Object[0]));
                            this.readAttributes();
                        }
                        catch (NoSuchMethodException e) {}
                        break block4;
                    }
                    case TABELA: {
                        try {
                            Method attrMethod = this.context.getQuadroLineInstance().getClass().getMethod(attributeMethodName, new Class[0]);
                            this.context.setAttributeClassInstance(attrMethod.invoke(this.context.getQuadroLineInstance(), new Object[0]));
                            this.readAttributes();
                        }
                        catch (NoSuchMethodException e) {
                            // empty catch block
                        }
                        Method getter = this.context.getQuadroLineInstance().getClass().getMethod(getterName, new Class[0]);
                        this.context.readingTabelaQuadroRepetitivo = true;
                        this.context.setTabelaQuadroRepetitivo((List)getter.invoke(this.context.getQuadroLineInstance(), new Object[0]));
                        java.lang.reflect.Type quadroRepetitivoTabelaLineType = getter.getGenericReturnType();
                        this.context.setTabelaQuadroLineType(quadroRepetitivoTabelaLineType);
                    }
                }
                break;
            }
            case 7: {
                this.context.setAttributes(attributes);
                ParameterizedType tabelaLineType = (ParameterizedType)this.context.getTabelaQuadroLineType();
                this.context.setTabelaQuadroRepetitivoLineInstance(((Class)tabelaLineType.getActualTypeArguments()[0]).newInstance());
                break;
            }
            case 8: {
                Class setterType = this.context.getTabelaQuadroRepetitivoLineInstance().getClass().getMethod(getterName, new Class[0]).getReturnType();
                this.context.setSetterTabelaQuadroRepetitivo(this.context.getTabelaQuadroRepetitivoLineInstance().getClass().getMethod(setterName, setterType));
                this.context.setAttributes(attributes);
                this.buffer = new StringBuffer();
            }
        }
        return true;
    }

    @Override
    protected boolean endElement(String uri, String localName, String name, int depth) throws Exception {
        switch (depth) {
            case 1: {
                break;
            }
            case 2: {
                this.context.clearAnexo();
                break;
            }
            case 3: {
                this.context.clearQuadro();
                break;
            }
            case 4: {
                if (this.context.isReadingCampo()) {
                    String value = this.buffer.toString();
                    value = value.trim();
                    this.setFieldAuxiliary(value);
                } else if (this.context.readingQuadroRepetitivo()) {
                    MainFrame mainFrame;
                    Method getMainframeMethod;
                    Class sessionClass;
                    if (this.context.getQuadroRepetitivo().getList().isEmpty()) {
                        ParameterizedType quadroLineType = (ParameterizedType)this.context.getQuadroLineType();
                        this.context.setQuadroLineInstance(((Class)quadroLineType.getActualTypeArguments()[0]).newInstance());
                        this.context.getQuadroRepetitivo().getList().add(this.context.getQuadroLineInstance());
                    }
                    if (!(this.serverSide.booleanValue() || (mainFrame = (MainFrame)(getMainframeMethod = (sessionClass = ClassUtil.getClass("pt.opensoft.taxclient.util.Session")).getMethod("getMainFrame", new Class[0])).invoke(null, new Object[0])) == null)) {
                        AnexoTabbedPane anexoPane1 = mainFrame.getDeclarationDisplayer().getAnexoTabbedPane(this.context.getAnexo().getFormKey());
                        JPanel quadroPanelExtension1 = anexoPane1.getPanelExtensionByModel(this.context.getQuadro().getClass().getSimpleName());
                        Class lineClass = (Class)((ParameterizedType)this.context.getQuadroLineType()).getActualTypeArguments()[0];
                        Method methodQr = ClassUtil.getClass(this.packageBase + ".binding." + this.context.getAnexo().getFormKey().getId().toLowerCase() + "." + ClassUtil.getClassName(lineClass) + "Bindings").getMethod("doBindings", lineClass, quadroPanelExtension1.getClass());
                        methodQr.invoke(null, this.context.getQuadroLineInstance(), quadroPanelExtension1);
                        this.context.getQuadroRepetitivo().setSelectionIndex(this.context.getQuadroRepetitivo().getSize() - 1);
                    }
                    Method totPag = this.context.getQuadro().getClass().getMethod("setTotPag", Integer.TYPE);
                    totPag.invoke(this.context.getQuadro(), this.context.getQuadroRepetitivo().getList().size());
                    Method setPagActual = this.context.getQuadro().getClass().getMethod("setPagActual", Integer.TYPE);
                    setPagActual.invoke(this.context.getQuadro(), this.context.getQuadroRepetitivo().getList().size());
                }
                this.context.clearCampo();
                this.context.clearTabela();
                this.context.clearQuadroRepetitivo();
                break;
            }
            case 5: {
                if (this.context.readingTabela()) {
                    this.context.getTabela().add(this.context.getTabelaLineInstance());
                }
                if (!this.context.readingQuadroRepetitivo()) break;
                this.context.getQuadroRepetitivo().getList().add(this.context.getQuadroLineInstance());
                break;
            }
            case 6: {
                String value;
                if (this.context.readingTabela()) {
                    value = this.buffer.toString();
                    value = value.trim();
                    this.setFieldAuxiliary(value);
                }
                if (!this.context.readingQuadroRepetitivo()) break;
                value = this.buffer.toString();
                value = value.trim();
                if (this.context.readingTabelaQuadroRepetitivo) {
                    this.context.readingTabelaQuadroRepetitivo = false;
                    break;
                }
                this.setFieldAuxiliary(value);
                break;
            }
            case 7: {
                this.context.getTabelaQuadroRepetitivo().add(this.context.getTabelaQuadroRepetitivoLineInstance());
                break;
            }
            case 8: {
                String value = this.buffer.toString();
                value = value.trim();
                this.setFieldAuxiliary(value);
            }
        }
        return true;
    }

    private void readAttributes() throws Exception {
        if (this.context.getAttributeClassInstance() == null) {
            return;
        }
        Method[] methods = new Reflections().getAllDeclaredMethods(this.context.getAttributeClassInstance().getClass()).onlyPublic().methods();
        for (int i = 0; i < methods.length; ++i) {
            Method method = methods[i];
            String propertyName = method.getName().startsWith("get") ? method.getName().substring(3) : method.getName().substring(2);
            String setterName = DeclarationReaderHandler.buildSetterMethodName(propertyName);
            Method setterMethod = this.context.getAttributeClassInstance().getClass().getMethod(setterName, method.getReturnType());
            String value = this.context.getAttributes().get(DeclarationReaderHandler.firstToLower(propertyName));
            Class setterArgumentClass = setterMethod.getParameterTypes()[0];
            if (value == null) continue;
            this.invokeSetterFromString(setterArgumentClass, this.context.getAttributeClassInstance(), setterMethod, value);
        }
    }

    private static final String firstToLower(String str) {
        return new String(new StringBuilder().append("").append(str.charAt(0)).toString()).toLowerCase() + str.substring(1);
    }

    private void setFieldAuxiliary(String value) throws Exception {
        Object baseBean = null;
        Method setterMethod = null;
        if (this.context.isReadingCampo()) {
            baseBean = this.context.getQuadro();
            setterMethod = this.context.getSetter();
        } else if (this.context.readingTabela()) {
            baseBean = this.context.getTabelaLineInstance();
            setterMethod = this.context.getSetterTabela();
        } else if (this.context.readingQuadroRepetitivo()) {
            if (this.context.readingTabelaQuadroRepetitivo()) {
                baseBean = this.context.getTabelaQuadroRepetitivoLineInstance();
                setterMethod = this.context.getSetterTabelaQuadroRepetitivo();
            } else {
                baseBean = this.context.getQuadroLineInstance();
                setterMethod = this.context.getSetterQuadroRepetitivo();
            }
        }
        Class setterArgumentClass = setterMethod.getParameterTypes()[0];
        if (!StringUtil.isEmpty(value)) {
            this.invokeSetterFromString(setterArgumentClass, baseBean, setterMethod, value);
        }
    }

    private void invokeSetterFromString(Class setterArgumentClass, Object baseBean, Method setterMethod, String value) throws Exception {
        if (setterArgumentClass.isAssignableFrom(Boolean.class) || setterArgumentClass.isAssignableFrom(Boolean.TYPE)) {
            setterMethod.invoke(baseBean, Boolean.parseBoolean(value));
        } else if (setterArgumentClass.isAssignableFrom(DateTime.class)) {
            setterMethod.invoke(baseBean, XmlDateTime.parseDateTimeChoice(value).getDateTime());
        } else if (setterArgumentClass.isAssignableFrom(Date.class)) {
            setterMethod.invoke(baseBean, new Date(XmlDateTime.parseDateTimeChoice(value).getDateTime().format("yyyy-MM-dd")));
        } else if (setterMethod.getAnnotation(ComplexField.class) != null) {
            Class clazz = Class.forName(((ComplexField)setterMethod.getAnnotation(ComplexField.class)).value(), true, this.getClass().getClassLoader());
            Class[] argsClass = new Class[]{Boolean.TYPE, Boolean.TYPE};
            Constructor constructor = clazz.getConstructor(argsClass);
            Object[] args = new Object[]{this.lazyLoad, this.serverSide};
            FieldHandler fileHandler = (FieldHandler)constructor.newInstance(args);
            Map<String, String> fieldAttr = this.context.getAttributes();
            if (!(fieldAttr == null || fieldAttr.isEmpty())) {
                setterMethod.invoke(baseBean, fileHandler.getValueWithAttributes(value, fieldAttr));
            } else {
                setterMethod.invoke(baseBean, fileHandler.getValue(value));
            }
        } else if (setterArgumentClass.isAssignableFrom(String.class)) {
            setterMethod.invoke(baseBean, value);
        } else if (setterArgumentClass.isAssignableFrom(Integer.class)) {
            setterMethod.invoke(baseBean, Integer.parseInt(value));
        } else {
            if (setterMethod.getAnnotation(FractionDigits.class) != null) {
                int fractionDigits = ((FractionDigits)setterMethod.getAnnotation(FractionDigits.class)).value();
                value = NumberUtil.fromXSDNumericType(value, fractionDigits, '.');
            }
            setterMethod.invoke(baseBean, Long.parseLong(value));
        }
    }

    @Override
    protected boolean characters(char[] ch, int start, int length, int depth) throws Exception {
        switch (depth) {
            case 4: 
            case 6: 
            case 8: {
                if (this.buffer == null) break;
                this.buffer.append(ch, start, length);
            }
        }
        return true;
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        Logger.getDefault().error("ERROR ON READ!" + e.getMessage());
        throw new RuntimeException(e.getMessage() + " \n\n " + e.getCause());
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        Logger.getDefault().error("ERROR ON READ!" + e.getMessage());
        throw new RuntimeException(e.getMessage() + " \n\n " + e.getCause());
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        Logger.getDefault().warning("WARNING ON READ!" + e.getMessage());
    }

    public static String standardJavaClassName(String className) {
        return StringUtil.capitalizeFirstCharacter(className).replace((CharSequence)"-", (CharSequence)"_");
    }

    public static String standardJavaElementName(String element) {
        String tempElement = element.replace((CharSequence)"-", (CharSequence)"_");
        String firstChar = tempElement.substring(0, 1).toLowerCase();
        return firstChar + tempElement.substring(1);
    }

    public static String buildGetterMethodName(String element) {
        String elementWithoutHifen = element.replace((CharSequence)"-", (CharSequence)"_");
        return "get" + StringUtil.capitalizeFirstCharacter(elementWithoutHifen);
    }

    public static String buildSetterMethodName(String element) {
        String elementWithoutHifen = element.replace((CharSequence)"-", (CharSequence)"_");
        return "set" + StringUtil.capitalizeFirstCharacter(elementWithoutHifen);
    }

    private class Context {
        private DeclaracaoModel declaracao;
        private Map<String, String> attributes;
        private Method setter;
        private Method setterQuadroRepetitivo;
        private Method setterTabela;
        private Method setterTabelaQuadroRepetitivo;
        private Method setAttributes;
        private Object attributeClassInstance;
        private AnexoModel anexo;
        private QuadroModel quadro;
        private List tabela;
        private java.lang.reflect.Type tabelaLineType;
        private java.lang.reflect.Type quadroLineType;
        private java.lang.reflect.Type tabelaQuadroLineType;
        private Object tabelaLineInstance;
        private Object quadroLineInstance;
        private Object tabelaQuadroRepetitivoLineInstance;
        private SelectionInList quadroRepetitivo;
        private List tabelaQuadroRepetitivo;
        private boolean readingAnexo;
        private boolean readingQuadro;
        private boolean readingTabela;
        private boolean readingCampo;
        private boolean readingQuadroRepetitivo;
        private boolean readingTabelaQuadroRepetitivo;
        private boolean readingCampoQuadroRepetitivo;

        private Context() {
            this.declaracao = null;
            this.attributes = null;
            this.setter = null;
            this.setterQuadroRepetitivo = null;
            this.setterTabela = null;
            this.setterTabelaQuadroRepetitivo = null;
            this.setAttributes = null;
            this.attributeClassInstance = null;
            this.anexo = null;
            this.quadro = null;
            this.tabela = null;
            this.quadroRepetitivo = null;
            this.tabelaQuadroRepetitivo = null;
            this.readingAnexo = false;
            this.readingQuadro = false;
            this.readingTabela = false;
            this.readingCampo = false;
            this.readingQuadroRepetitivo = false;
            this.readingTabelaQuadroRepetitivo = false;
            this.readingCampoQuadroRepetitivo = false;
        }

        public Method getSetAttributes() {
            return this.setAttributes;
        }

        public void setSetAttributes(Method setAttributes) {
            this.setAttributes = setAttributes;
        }

        public Object getAttributeClassInstance() {
            return this.attributeClassInstance;
        }

        public void setAttributeClassInstance(Object attributeClassInstance) {
            this.attributeClassInstance = attributeClassInstance;
        }

        public Method getSetterTabelaQuadroRepetitivo() {
            return this.setterTabelaQuadroRepetitivo;
        }

        public void setSetterTabelaQuadroRepetitivo(Method setterTabelaQuadroRepetitivo) {
            this.setterTabelaQuadroRepetitivo = setterTabelaQuadroRepetitivo;
        }

        public java.lang.reflect.Type getTabelaQuadroLineType() {
            return this.tabelaQuadroLineType;
        }

        public void setTabelaQuadroLineType(java.lang.reflect.Type tabelaQuadroLineType) {
            this.tabelaQuadroLineType = tabelaQuadroLineType;
        }

        public Object getTabelaQuadroRepetitivoLineInstance() {
            return this.tabelaQuadroRepetitivoLineInstance;
        }

        public void setTabelaQuadroRepetitivoLineInstance(Object tabelaQuadroRepetitivoLineInstance) {
            this.tabelaQuadroRepetitivoLineInstance = tabelaQuadroRepetitivoLineInstance;
        }

        public Object getQuadroLineInstance() {
            return this.quadroLineInstance;
        }

        public void setQuadroLineInstance(Object quadroLineInstance) {
            this.quadroLineInstance = quadroLineInstance;
        }

        public java.lang.reflect.Type getQuadroLineType() {
            return this.quadroLineType;
        }

        public void setQuadroLineType(java.lang.reflect.Type quadroLineType) {
            this.quadroLineType = quadroLineType;
        }

        public Method getSetterTabela() {
            return this.setterTabela;
        }

        public void setSetterTabela(Method setterTabela) {
            this.setterTabela = setterTabela;
        }

        public java.lang.reflect.Type getTabelaLineType() {
            return this.tabelaLineType;
        }

        public void setTabelaLineType(java.lang.reflect.Type tabelaLineType) {
            this.tabelaLineType = tabelaLineType;
        }

        public Object getTabelaLineInstance() {
            return this.tabelaLineInstance;
        }

        public void setTabelaLineInstance(Object tabelaLineInstance) {
            this.tabelaLineInstance = tabelaLineInstance;
        }

        public Map<String, String> getAttributes() {
            return this.attributes;
        }

        public void setAttributes(Map<String, String> attributes) {
            this.attributes = attributes;
        }

        public void clearAttributes() {
            this.attributes = null;
        }

        public boolean hasAttributes() {
            return this.attributes != null && this.attributes.size() > 0;
        }

        public DeclaracaoModel getDeclaracao() {
            return this.declaracao;
        }

        public void setDeclaracao(DeclaracaoModel declaracao) {
            this.declaracao = declaracao;
        }

        public Method getSetter() {
            return this.setter;
        }

        public void clearSetter() {
            this.setter = null;
        }

        public void setSetter(Method setter) {
            this.setter = setter;
        }

        public Method getSetterQuadroRepetitivo() {
            return this.setterQuadroRepetitivo;
        }

        public void clearSetterQuadroRepetitivo() {
            this.setterQuadroRepetitivo = null;
        }

        public void setSetterQuadroRepetitivo(Method setterQuadroRepetitivo) {
            this.setterQuadroRepetitivo = setterQuadroRepetitivo;
        }

        public AnexoModel getAnexo() {
            return this.anexo;
        }

        public void setAnexo(AnexoModel anexo) {
            this.anexo = anexo;
            this.readingAnexo = true;
        }

        public void clearAnexo() {
            this.anexo = null;
            this.readingAnexo = false;
        }

        public boolean readingAnexo() {
            return this.readingAnexo;
        }

        public QuadroModel getQuadro() {
            return this.quadro;
        }

        public void setQuadro(QuadroModel quadro) {
            this.quadro = quadro;
            this.readingQuadro = true;
        }

        public void clearQuadro() {
            this.quadro = null;
            this.readingQuadro = false;
        }

        public boolean readingQuadro() {
            return this.readingQuadro;
        }

        public List getTabela() {
            return this.tabela;
        }

        public void setTabela(List tabela) {
            this.tabela = tabela;
            this.readingTabela = true;
        }

        public void clearTabela() {
            this.tabela = null;
            this.readingTabela = false;
        }

        public boolean readingTabela() {
            return this.readingTabela;
        }

        public SelectionInList getQuadroRepetitivo() {
            return this.quadroRepetitivo;
        }

        public void setQuadroRepetitivo(SelectionInList quadroRepetitivo) {
            this.quadroRepetitivo = quadroRepetitivo;
            this.readingQuadroRepetitivo = true;
        }

        public void clearQuadroRepetitivo() {
            this.quadroRepetitivo = null;
            this.quadroLineInstance = null;
            this.quadroLineType = null;
            this.readingQuadroRepetitivo = false;
        }

        public boolean readingQuadroRepetitivo() {
            return this.readingQuadroRepetitivo;
        }

        public List getTabelaQuadroRepetitivo() {
            return this.tabelaQuadroRepetitivo;
        }

        public void setTabelaQuadroRepetitivo(List tabelaQuadroRepetitivo) {
            this.tabelaQuadroRepetitivo = tabelaQuadroRepetitivo;
            this.readingTabelaQuadroRepetitivo = true;
        }

        public void clearTabelaQuadroRepetitivo() {
            this.tabela = null;
            this.readingTabelaQuadroRepetitivo = false;
        }

        public boolean readingTabelaQuadroRepetitivo() {
            return this.readingTabelaQuadroRepetitivo;
        }

        public void readingCampo() {
            this.readingCampo = true;
        }

        public void clearCampo() {
            this.readingCampo = false;
        }

        public boolean isReadingCampo() {
            return this.readingCampo;
        }

        public void readingCampoQuadroRepetitivo() {
            this.readingCampoQuadroRepetitivo = true;
        }

        public void clearCampoQuadroRepetitivo() {
            this.readingCampoQuadroRepetitivo = false;
        }

        public boolean isReadingCampoQuadroRepetitivo() {
            return this.readingCampoQuadroRepetitivo;
        }
    }

}

