/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.persistence.DeclarationReaderHandler;
import pt.opensoft.taxclient.util.XmlProcessor;
import pt.opensoft.taxclient.util.XmlProcessorHandler;
import pt.opensoft.taxclient.util.XmlProcessorServerSide;
import pt.opensoft.util.ClassUtil;

public class DeclarationReader {
    public static final String READING_PROPERTY = "reading";
    private static boolean reading = false;
    private static List<PropertyChangeListener> readingListeners = new ArrayList<PropertyChangeListener>();

    public static synchronized boolean isReading() {
        return reading;
    }

    public static synchronized void setReading(boolean reading) {
        if (DeclarationReader.reading == reading) {
            return;
        }
        boolean oldReadingValue = reading;
        DeclarationReader.reading = reading;
        for (PropertyChangeListener statusListener : readingListeners) {
            statusListener.propertyChange(new PropertyChangeEvent(DeclarationReader.class, "reading", oldReadingValue, reading));
        }
    }

    public static void addReadingListener(PropertyChangeListener statusListener) {
        if (!readingListeners.contains(statusListener)) {
            readingListeners.add(statusListener);
        }
    }

    public static void removeReadingListener(PropertyChangeListener statusListener) {
        readingListeners.remove(statusListener);
    }

    public static void removeAllReadingListeners(Class<? extends PropertyChangeListener> statusListenerClass) {
        Iterator<PropertyChangeListener> iterator = readingListeners.iterator();
        while (iterator.hasNext()) {
            PropertyChangeListener listener = iterator.next();
            if (!statusListenerClass.isAssignableFrom(listener.getClass())) continue;
            iterator.remove();
        }
    }

    public static void read(Reader reader, DeclaracaoModel declaracaoModel, boolean lazyLoad, boolean serverSide) throws IOException, SAXException {
        try {
            if (!serverSide) {
                DeclarationReader.setReading(true);
            }
            String packageName = ClassUtil.getPackageName(declaracaoModel.getClass());
            packageName = packageName.substring(0, packageName.lastIndexOf(".model"));
            DeclarationReaderHandler handler = new DeclarationReaderHandler(packageName, declaracaoModel, false, lazyLoad, serverSide);
            XmlProcessor xmlReader = new XmlProcessor("", "", handler);
            xmlReader.read(reader);
        }
        finally {
            if (!serverSide) {
                DeclarationReader.setReading(false);
            }
        }
    }

    public static void readServerSide(XMLReader xmlReader, Reader reader, DeclaracaoModel declaracaoModel, boolean lazyLoad, boolean serverSide) throws IOException, SAXException {
        String packageName = ClassUtil.getPackageName(declaracaoModel.getClass());
        packageName = packageName.substring(0, packageName.lastIndexOf(".model"));
        DeclarationReaderHandler handler = new DeclarationReaderHandler(packageName, declaracaoModel, false, lazyLoad, serverSide);
        XmlProcessorServerSide xmlProcessor = new XmlProcessorServerSide(handler, xmlReader, reader);
        xmlProcessor.read();
    }
}

