/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence.exception;

import pt.opensoft.taxclient.persistence.exception.CorruptedFileException;

public class DuplicateAnexoIdentificationException
extends CorruptedFileException {
    private static final long serialVersionUID = 3302199740479383437L;
    private String id;
    private String subId;

    public DuplicateAnexoIdentificationException(String message, String id, String subId) {
        super(message);
        this.id = id;
        this.subId = subId;
    }

    public String getId() {
        return this.id;
    }

    public String getSubId() {
        return this.subId;
    }

    @Override
    public String getMessageForUser() {
        return "<html>" + this.getDefaultMessageHeaderForUser() + "<p>Identificador de anexo duplicado: " + this.id + " " + this.subId + "</p></html>";
    }
}

