/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence.exception;

public abstract class CorruptedFileException
extends RuntimeException {
    private static final long serialVersionUID = 7175609303716085911L;

    public CorruptedFileException() {
    }

    public CorruptedFileException(String message) {
        super(message);
    }

    public CorruptedFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public CorruptedFileException(Throwable cause) {
        super(cause);
    }

    protected String getDefaultMessageHeaderForUser() {
        return "<h3>Ocorreu um problema na leitura do ficheiro.</h3>";
    }

    public abstract String getMessageForUser();
}

