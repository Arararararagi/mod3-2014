/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence.exception;

import pt.opensoft.taxclient.persistence.exception.CorruptedFileException;

public class InvalidAnexoSubIdException
extends CorruptedFileException {
    private static final long serialVersionUID = 2022764298453064155L;
    private String subId;

    public InvalidAnexoSubIdException(String message, String subId) {
        super(message);
        this.subId = subId;
    }

    public String getSubId() {
        return this.subId;
    }

    @Override
    public String getMessageForUser() {
        return "<html>" + this.getDefaultMessageHeaderForUser() + "<p>Identificador de anexo inv&aacute;lido: " + this.subId + "</p></html>";
    }
}

