/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence;

@Retention(value=RetentionPolicy.RUNTIME)
public @interface FractionDigits {
    public int value();
}

