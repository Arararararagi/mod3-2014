/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.persistence;

@Retention(value=RetentionPolicy.RUNTIME)
public @interface Type {
    public TYPE value();

    public static enum TYPE {
        CAMPO("campo"),
        TABELA("tabela"),
        QUADRO_REPETITIVO("quadro repetitivo");
        
        private String type;

        private TYPE(String type) {
            this.type = type;
        }

        public String getType() {
            return this.type;
        }
    }

}

