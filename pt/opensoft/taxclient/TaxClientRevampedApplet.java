/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient;

import java.awt.Component;
import java.awt.Container;
import java.awt.HeadlessException;
import javax.swing.JApplet;
import javax.swing.JMenuBar;
import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;
import pt.opensoft.taxclient.TaxClientRevampedInitializer;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoModelValidator;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.taxclient.util.TaxclientParameters;

public abstract class TaxClientRevampedApplet
extends JApplet {
    private static final long serialVersionUID = 1;
    private TaxClientRevampedInitializer gui;

    @Override
    public void start() throws HeadlessException {
        try {
            final TaxClientRevampedApplet applet = this;
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    try {
                        TaxClientRevampedApplet.this.preInit();
                        Session.setApplet(applet);
                        TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().initProperties();
                        TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().bootstrap();
                        TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().initLookAndFeel(TaxClientRevampedApplet.this);
                        DeclaracaoModel declaracaoModel = TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().createDeclaracaoModel();
                        TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().initDeclarationPresentationModel(declaracaoModel);
                        TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().registerDefaultTaxClientActions();
                        NavigationModel navigationModel = TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().createNavigationModel(declaracaoModel);
                        TaxClientRevampedApplet.this.initMainFrame(declaracaoModel, navigationModel);
                        Session.setDeclaracaoValidator(TaxClientRevampedApplet.this.getTaxClientRevampedInitializer().createDeclaracaoModelValidator());
                        TaxClientRevampedApplet.this.postInit();
                        declaracaoModel.resetDirty();
                    }
                    catch (Exception evtException) {
                        Session.getExceptionHandler().handle(evtException);
                    }
                }
            });
        }
        catch (Exception invocationException) {
            Session.getExceptionHandler().handle(invocationException);
        }
    }

    protected void initMainFrame(DeclaracaoModel declaracaoModel, NavigationModel navigationModel) throws UnsupportedLookAndFeelException, InterruptedException, InvocationTargetException {
        MainFrame mainFrame = this.getTaxClientRevampedInitializer().createMainFrame(declaracaoModel, navigationModel);
        if (TaxclientParameters.INIT_USE_CLASSIC_MENU) {
            mainFrame.initMenuBar();
        }
        if (TaxclientParameters.INIT_USE_RIBBON_MENU) {
            mainFrame.initTopRibbon();
        }
        if (TaxclientParameters.INIT_USE_TOP_MENU) {
            mainFrame.initTopMenuToolBar();
        }
        this.setJMenuBar(mainFrame.getMenuBar());
        this.getContentPane().add(mainFrame);
        this.getTaxClientRevampedInitializer().addMainFrameToSession(mainFrame);
    }

    public void registerTaxClientRevampedGUI(TaxClientRevampedInitializer gui) {
        this.gui = gui;
    }

    public TaxClientRevampedInitializer getTaxClientRevampedInitializer() {
        if (this.gui == null) {
            try {
                throw new Exception("No GUI registered. Please call registerTaxClientRevampedGUI in the preInit implemention of your applet.");
            }
            catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        return this.gui;
    }

    protected abstract void preInit();

    protected abstract void postInit();

}

