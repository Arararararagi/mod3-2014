/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import pt.opensoft.util.StringUtil;

public class ProxyParameters {
    private static final String USE_PROXY = "use.proxy";
    private static final String PROXY_HOST_PARAMETER = "proxy.host";
    private static final String PROXY_PORT_PARAMETER = "proxy.port";
    private static final String CONNECTION_TIMEOUT = "connection.timeout";
    private static final int DEFAULT_CONNECTION_TIMEOUT = 300000;
    private static final String PROXY_SETTINGS_FILENAME = "proxy.properties";
    private static Boolean useProxy = null;
    private static String proxyHost = null;
    private static String proxyPort = null;
    private static int connectionTimeout = 0;

    private static boolean existsParameter(String parameter) {
        return ProxyParameters.getValueFromPropertiesFile(parameter) != null;
    }

    public static boolean isProxySet() {
        return ProxyParameters.existsParameter("proxy.host") && ProxyParameters.existsParameter("proxy.port") || proxyHost != null && proxyPort != null;
    }

    public static String getProxyHost() {
        if (proxyHost == null) {
            proxyHost = ProxyParameters.getValueFromPropertiesFile("proxy.host");
        }
        return proxyHost;
    }

    public static boolean getUseProxy() {
        if (useProxy == null) {
            useProxy = new Boolean(ProxyParameters.getValueFromPropertiesFile("use.proxy"));
        }
        return useProxy;
    }

    public static void setUseProxy(boolean use) {
        useProxy = new Boolean(use);
        ProxyParameters.setValueFromPropertiesFile("use.proxy", useProxy.toString());
    }

    public static String getProxyPort() {
        if (proxyPort == null) {
            proxyPort = ProxyParameters.getValueFromPropertiesFile("proxy.port");
        }
        return proxyPort;
    }

    public static int getConnectionTimeout() {
        if (connectionTimeout == 0) {
            String value = ProxyParameters.getValueFromPropertiesFile("connection.timeout");
            connectionTimeout = value != null && StringUtil.isNumeric(value) ? Integer.parseInt(value) : 300000;
        }
        return connectionTimeout;
    }

    public static void setConnectionTimeout(int newConnectionTimeout) {
        if (newConnectionTimeout < 0) {
            throw new IllegalArgumentException("The connection timeout cannot be less than zero.");
        }
        connectionTimeout = newConnectionTimeout;
    }

    public static void setProxyHost(String value) {
        proxyHost = value;
        ProxyParameters.setValueFromPropertiesFile("proxy.host", proxyHost);
    }

    public static void setProxyPort(String value) {
        proxyPort = value;
        ProxyParameters.setValueFromPropertiesFile("proxy.port", proxyPort);
    }

    private static String getValueFromPropertiesFile(String key) {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("proxy.properties"));
            return properties.getProperty(key);
        }
        catch (IOException e) {
            return null;
        }
    }

    private static void setValueFromPropertiesFile(String parameter, String value) {
        try {
            File propertiesFile = new File("proxy.properties");
            if (!propertiesFile.exists()) {
                propertiesFile.createNewFile();
            }
            Properties properties = new Properties();
            properties.load(new FileInputStream(propertiesFile));
            properties.setProperty(parameter, value);
            properties.store(new FileOutputStream(propertiesFile), "Ficheiro de Configura\u00e7\u00f5es de Proxy");
        }
        catch (IOException e) {
            return;
        }
    }
}

