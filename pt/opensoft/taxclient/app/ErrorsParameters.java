/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.app;

import pt.opensoft.util.SimpleParameters;

public abstract class ErrorsParameters
extends SimpleParameters {
    public String getError(String code) {
        return this.getString(code, code);
    }
}

