/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.app;

import java.io.PrintStream;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pt.opensoft.taxclient.app.ErrorsParameters;
import pt.opensoft.text.Appender;
import pt.opensoft.text.StringAppender;
import pt.opensoft.util.StringUtil;

public class DeclError {
    protected String _code;
    protected String _fieldValue;
    protected String[] _links;
    protected String[] _replaces;
    protected int[] _lineNumbers;
    protected boolean isNewErrorType;
    private static final String NEW_LINK_PATTERN = "\\{[^\\|]+\\|[^\\|\\}]+\\}";
    private static final String OLD_LINK_PATTERN = "\\{\\d\\d[^\\}]+\\}";
    public static final String LINK_SEPARATOR = "\\|";
    public static final String LINE_TOKEN = "\\?\\d";
    protected static ErrorsParameters erros;
    public static final int COUNTER_LENGTH = 2;
    public static final String START_LINK = "{";
    public static final String END_LINK = "}";
    public static final String TAG_REPLACE = "|R|";
    public static final String ERRO_FORMATO = "E_Formato";

    public DeclError(String code) {
        this._code = code;
        this.isNewErrorType = true;
    }

    public DeclError(String code, String[] links) {
        this(code, links, null, null);
    }

    public DeclError(String code, String[] links, String[] replaces) {
        this(code, links, null, replaces);
    }

    public DeclError(String code, String[] links, String fieldValue) {
        this(code, links, fieldValue, null);
    }

    public DeclError(String code, String[] links, String fieldValue, String[] replaces) {
        if (links != null && links.length > 99) {
            throw new RuntimeException("A mensagem de erro n\u00e3o pode ter mais do que 99 links.");
        }
        if (replaces != null && replaces.length > 99) {
            throw new RuntimeException("A mensagem de erro n\u00e3o pode ter mais do que 99 replaces.");
        }
        this._code = code;
        this._links = links;
        this._replaces = replaces;
        this._fieldValue = fieldValue;
        this.isNewErrorType = false;
    }

    public static void setParameters(ErrorsParameters param) {
        erros = param;
    }

    public DeclError addErrorLineNumbers(int[] lineNumbers) {
        this._lineNumbers = lineNumbers;
        return this;
    }

    public String getHtml() {
        if (erros == null) {
            System.err.println("Chamada ao validate sem a devida inicializa\u00e7\u00e3o dos parametros da aplica\u00e7\u00e3o.\nContinuando sem as devidas mensagens de erro...");
            return this._code;
        }
        String msg = erros.getError("showErrorCode");
        String errorCode = "";
        if (msg != null && msg.equalsIgnoreCase("yes")) {
            errorCode = this._code + " : ";
        }
        if (this.isNewErrorType) {
            return errorCode + this.getNewErrorHtml();
        }
        return errorCode + this.getOldErrorHtml();
    }

    private String getNewErrorHtml() {
        String msg = erros.getError(this._code);
        if (Pattern.compile("\\{\\d\\d[^\\}]+\\}").matcher((CharSequence)msg).find()) {
            throw new RuntimeException("Wrong pattern in error " + this._code + " message: old pattern being used with the new error handler.");
        }
        msg = StringUtil.toHtml(msg, true);
        StringRegexTokenizer srt = new StringRegexTokenizer(msg, "\\{[^\\|]+\\|[^\\|\\}]+\\}");
        while (srt.hasMoreTokens()) {
            String nextToken = srt.nextToken();
            msg = StringUtil.replace(msg, nextToken, this.createLink(nextToken));
        }
        return msg;
    }

    private String createLink(String newLink) {
        String[] splitedLink = newLink.split("\\|");
        String link = StringUtil.remove(splitedLink[0], "{");
        String target = StringUtil.remove(splitedLink[1], "}");
        if (target.indexOf(63) != -1) {
            int lineNumberIndex = Integer.parseInt(target.substring(target.indexOf(63) + 1, target.indexOf(63) + 2));
            target = target.replaceFirst("\\?\\d", this.getLineNumber(lineNumberIndex));
        }
        StringAppender appender = new StringAppender();
        appender.append("<a href=\"");
        appender.append(target);
        appender.append("\">");
        appender.append(link);
        appender.append("</a>");
        return appender.toString();
    }

    private String getLineNumber(int lineNumber) {
        return Integer.toString(this._lineNumbers[lineNumber]);
    }

    private String getOldErrorHtml() {
        int msgIndex;
        String msg = erros.getError(this._code);
        if (Pattern.compile("\\{[^\\|]+\\|[^\\|\\}]+\\}").matcher((CharSequence)msg).find()) {
            throw new RuntimeException("Wrong pattern in error " + this._code + " message: new pattern being used with the old error handler.");
        }
        msg = StringUtil.toHtml(msg, true);
        if (this._replaces != null) {
            while ((msgIndex = msg.indexOf("|R|")) != -1) {
                try {
                    int replaceIndex = Integer.parseInt(msg.substring(msgIndex + "|R|".length(), msgIndex + "|R|".length() + 2));
                    if (replaceIndex >= this._replaces.length) {
                        throw new RuntimeException(new ParseException("Falta de parametros para substituir os replaces \n\t\t" + msg, msgIndex));
                    }
                    msg = StringUtil.replaceFirst(msg, msg.substring(msgIndex, msgIndex + "|R|".length() + 2), this._replaces[replaceIndex]);
                    continue;
                }
                catch (NumberFormatException nfe) {
                    throw new RuntimeException(new ParseException("Mensagem no formato errado\n\t\t" + msg, msgIndex));
                }
            }
        }
        if (this._links != null) {
            msg = StringUtil.replace(msg, "}", "</a>");
            while ((msgIndex = msg.indexOf("{")) != -1) {
                try {
                    int lnkIndex = Integer.parseInt(msg.substring(msgIndex + "{".length(), msgIndex + "{".length() + 2));
                    if (lnkIndex >= this._links.length) {
                        throw new RuntimeException(new ParseException("Falta de parametros para substituir links \n\t\t" + msg, msgIndex));
                    }
                    msg = StringUtil.replaceFirst(msg, msg.substring(msgIndex, msgIndex + "{".length() + 2), "<a href=\"" + this._links[lnkIndex] + "\">");
                    continue;
                }
                catch (NumberFormatException nfe) {
                    throw new RuntimeException(new ParseException("Mensagem no formato errado\n\t\t" + msg, msgIndex));
                }
            }
        }
        return msg;
    }

    public String getErrorCode() {
        return this._code;
    }

    public String getFieldValue() {
        return this._fieldValue;
    }

    public String[] getLinks() {
        return this._links;
    }

    public String[] getReplaces() {
        return this._replaces;
    }

    public boolean isNewErrorType() {
        return this.isNewErrorType;
    }

    public static class StringRegexTokenizer {
        private String str;
        private Pattern pattern;
        private Matcher matcher;
        private boolean nextTokenFound;

        public StringRegexTokenizer(String str, String pattern) {
            this.str = str;
            this.pattern = Pattern.compile(pattern);
            this.init();
        }

        public StringRegexTokenizer(String str, Pattern pattern) {
            this.str = str;
            this.pattern = pattern;
            this.init();
        }

        private void init() {
            this.matcher = this.pattern.matcher((CharSequence)this.str);
            this.nextTokenFound = false;
        }

        public boolean hasMoreTokens() {
            this.nextTokenFound = this.matcher.find();
            return this.nextTokenFound;
        }

        public String nextToken() {
            if (!(this.nextTokenFound || this.matcher.find())) {
                return null;
            }
            this.nextTokenFound = false;
            return this.matcher.group();
        }
    }

}

