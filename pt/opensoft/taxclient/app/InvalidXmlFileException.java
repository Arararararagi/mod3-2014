/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.app;

import java.util.List;
import org.xml.sax.SAXParseException;

public class InvalidXmlFileException
extends RuntimeException {
    private List _errors;
    private boolean _nonFatal = false;

    public InvalidXmlFileException(List errors, boolean nonFatal) {
        this._errors = errors;
        this._nonFatal = nonFatal;
    }

    public List getErrors() {
        return this._errors;
    }

    public boolean isNonFatal() {
        return this._nonFatal;
    }

    public String getErrorListAsString() {
        StringBuffer errors = new StringBuffer();
        for (SAXParseException exception : this._errors) {
            errors.append(exception.getMessage()).append("\n");
        }
        return errors.toString();
    }
}

