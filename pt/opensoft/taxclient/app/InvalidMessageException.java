/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.app;

public class InvalidMessageException
extends RuntimeException {
    protected String _registo;
    protected String _campo;

    public InvalidMessageException(String message) {
        super(message);
    }

    public InvalidMessageException(String message, String registo, String campo) {
        super(message);
        this._registo = registo;
        this._campo = campo;
    }

    public String getRegisto() {
        return this._registo;
    }

    public String getCampo() {
        return this._campo;
    }
}

