/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import pt.opensoft.taxclient.util.XmlProcessorHandler;

public class XmlProcessorServerSide
extends DefaultHandler {
    private XMLReader parser;
    private Reader xmlContentReader;
    private List errorsList = null;
    private XmlProcessorHandler handler = null;

    public XmlProcessorServerSide(XmlProcessorHandler handler, XMLReader reader, Reader xmlContent) {
        this.handler = handler;
        this.initParser(reader);
        this.xmlContentReader = xmlContent;
    }

    private void initParser(XMLReader reader) {
        this.parser = reader;
        this.parser.setContentHandler(this);
        this.parser.setErrorHandler(this);
    }

    public List read() throws IOException, SAXException {
        InputSource inputSource = new InputSource(this.xmlContentReader);
        inputSource.setEncoding("ISO-8859-1");
        this.errorsList = new ArrayList(0);
        this.parser.parse(inputSource);
        return this.errorsList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (this.handler != null) {
            try {
                this.handler.startElementCallback(uri, localName, qName, this.attributes2Map(attributes));
            }
            catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Ocorreu um erro na leitura do ficheiro.");
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (this.handler != null) {
            try {
                this.handler.endElementCallback(uri, localName, qName);
            }
            catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Ocorreu um erro na leitura do ficheiro.");
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.handler != null) {
            this.handler.charactersCallback(ch, start, length);
        }
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        if (this.handler != null) {
            this.handler.warning(e);
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        this.errorsList.add(e);
        if (this.handler != null) {
            this.handler.error(e);
        }
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    public List getErrors() {
        return this.errorsList;
    }

    private Map attributes2Map(Attributes attributes) {
        TreeMap<String, String> result = new TreeMap<String, String>();
        for (int index = 0; index < attributes.getLength(); ++index) {
            result.put(attributes.getQName(index), attributes.getValue(index));
        }
        return result;
    }
}

