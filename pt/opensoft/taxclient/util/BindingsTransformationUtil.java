/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  net.sf.saxon.tinytree.TinyNodeImpl
 */
package pt.opensoft.taxclient.util;

import java.io.File;
import java.util.List;
import java.util.Map;
import net.sf.saxon.tinytree.TinyNodeImpl;
import pt.opensoft.taxclient.util.BehavioursGeneratorUtil;
import pt.opensoft.taxclient.util.PropertyTransformationUtil;
import pt.opensoft.taxclient.util.XsltToJavaTypesUtil;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class BindingsTransformationUtil {
    private static StringBuffer columnModelAuxiliary = new StringBuffer();
    private static StringBuffer bindingsForEnabled = new StringBuffer();
    private static StringBuffer bindingsForOverwrite = new StringBuffer();

    public static String buildBindingsImports(String basePackage, String panelName, String upperPanel, String tableName) {
        String uiPanelName = StringUtil.isEmpty(upperPanel) ? panelName + "PanelExtension" : upperPanel + "PanelExtension";
        String imports = "import pt.opensoft.swing.binding.BindingsProxy;\nimport pt.opensoft.swing.binding.UnidireccionalPropertyConnector;\nimport com.jgoodies.binding.beans.BeanAdapter;\nimport com.jgoodies.binding.beans.PropertyConnector;\nimport ca.odell.glazedlists.GlazedLists;\nimport ca.odell.glazedlists.ObservableElementList;\nimport ca.odell.glazedlists.ObservableElementList.Connector;\nimport pt.opensoft.taxclient.gui.CatalogManager;\nimport ca.odell.glazedlists.swing.EventTableModel;\nimport pt.opensoft.swing.renderer.ComplexCatalogCellRenderer;\nimport pt.opensoft.swing.renderer.RowIndexTableCellRenderer;\nimport pt.opensoft.taxclient.gui.CatalogManager;\nimport pt.opensoft.swing.model.catalogs.ICatalogItem;\nimport pt.opensoft.taxclient.util.Session;\nimport ca.odell.glazedlists.BasicEventList;\nimport ca.odell.glazedlists.EventList;\nimport ca.odell.glazedlists.swing.EventComboBoxModel;\nimport javax.swing.JComboBox;\nimport javax.swing.DefaultCellEditor;\nimport pt.opensoft.taxclient.bindings.TableColumnBindingsProxy;\nimport pt.opensoft.swing.editor.*;\nimport pt.opensoft.swing.renderer.*;\nimport " + basePackage + ".ui." + tableName.toLowerCase() + "." + uiPanelName + ";\n" + "import " + basePackage + ".model." + tableName.toLowerCase() + ".*;\n" + "import pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;\n" + "\n";
        return imports;
    }

    public static String buildBindingsPackage(String basePackageName, String upperLevel) {
        return "package " + basePackageName.toLowerCase() + ".binding." + upperLevel.toLowerCase() + ";\n\n";
    }

    public static String buildClassDeclaration(String panelName) {
        panelName = PropertyTransformationUtil.standardJavaClassName(panelName);
        return "public class " + panelName + "{\n\n";
    }

    public static String buildBeanAdapterDeclaration(String panelName, String upperPanel) {
        panelName = PropertyTransformationUtil.standardJavaClassName(panelName);
        String panelExtName = PropertyTransformationUtil.standardJavaClassName((!StringUtil.isEmpty(upperPanel) ? upperPanel : panelName) + "PanelExtension");
        String decl = "\tprotected static BeanAdapter<" + panelName + "> beanModel = null;\n\t" + "protected static " + panelExtName + " panelExtension = null;\n";
        return decl;
    }

    public static String buildDoBindingsHeader(String panelName, String upperPanel, Boolean repetitivo) {
        panelName = panelName.replace((CharSequence)"-", (CharSequence)"_");
        if (upperPanel != null) {
            upperPanel = PropertyTransformationUtil.standardJavaClassName(upperPanel);
        }
        String uiPanelName = StringUtil.isEmpty(upperPanel) ? panelName + "PanelExtension" : upperPanel + "PanelExtension";
        return "\n\n\tpublic static void doBindings(" + panelName + " model, " + uiPanelName + " panel){\n";
    }

    public static String buildDoBindings(String panelName, String upperPanel, Boolean repetitivo) {
        panelName = panelName.replace((CharSequence)"-", (CharSequence)"_");
        if (upperPanel != null) {
            upperPanel = PropertyTransformationUtil.standardJavaClassName(upperPanel);
        }
        String uiPanelName = StringUtil.isEmpty(upperPanel) ? panelName + "PanelExtension" : upperPanel + "PanelExtension";
        String elsePartOfMethod = "";
        if (repetitivo.booleanValue() && StringUtil.isEmpty(upperPanel)) {
            elsePartOfMethod = elsePartOfMethod + "\t\tBindingsProxy.bind(panel.getPagActual(), beanModel, " + panelName + ".PAG_ACTUAL);\n" + "\t\tBindingsProxy.bind(panel.getTotPag(), beanModel, " + panelName + ".TOT_PAG);\n";
        }
        String method = "\tpublic static void doBindings(" + panelName + " model, " + uiPanelName + " panel){\n" + "\t\tif(model == null){\n" + "\t\t\tif(beanModel == null) {\n" + "\t\t\t\treturn;\n" + "\t\t\t}\n" + "\t\t\tbeanModel.release();\n" + "\t\t\tbeanModel.setBean(null);\n" + "\t\t\treturn;\n" + "\t\t}\n" + "\t\tif(panel == panelExtension){ // NOSONAR \n" + "\t\t\tif(beanModel.getBean() != model){\n" + "\t\t\t\tbeanModel.release();\n" + "\t\t\t\tbeanModel.setBean(null);\n" + "\t\t\t}\n" + "\t\t}\n" + "\t\tbeanModel = new BeanAdapter<" + panelName + ">(model, true);\n" + "\t\tpanelExtension = panel;\n";
        method = method + elsePartOfMethod;
        return method;
    }

    public static String buildPropertyBindChoice(String panelName, String propertyName, String propertyType, List<TinyNodeImpl> listOfChoices, List<TinyNodeImpl> listOfChoiceValues) {
        panelName = panelName.replace((CharSequence)"-", (CharSequence)"_");
        String output = "//creating the binding for a JRadioButton group\n";
        for (int i = 0; i < listOfChoices.size(); ++i) {
            String choiceIdSuffix = listOfChoices.get(i).getStringValue();
            String javaType = XsltToJavaTypesUtil.javaComplexTypes.get(propertyType);
            String objValue = javaType.equals(ClassUtil.getClassName(String.class)) ? "\"" + listOfChoiceValues.get(i).getStringValue() + "\"" : listOfChoiceValues.get(i).getStringValue();
            String uicomponentName = PropertyTransformationUtil.buildGetterMethodName(propertyName + choiceIdSuffix);
            output = output + "\t\tBindingsProxy.bindChoice(panel." + uicomponentName + "(), beanModel, " + panelName + "." + PropertyTransformationUtil.uppercaseElement(propertyName) + "," + objValue + ");\n";
        }
        return output;
    }

    public static String buildPropertyBind(String panelName, String propertyName, String propertyType) {
        return BindingsTransformationUtil.buildPropertyBind(panelName, propertyName, propertyType, null);
    }

    public static String buildPropertyBind(String panelName, String propertyName, String propertyType, Object bindOnFocusLostXPathEval) {
        boolean bindOnFocusLost = true;
        if (propertyType.equals("xs:date") || propertyType.equals("xs:dateTime") || bindOnFocusLostXPathEval != null && bindOnFocusLostXPathEval instanceof Boolean) {
            bindOnFocusLost = true;
        }
        panelName = panelName.replace((CharSequence)"-", (CharSequence)"_");
        String method = null;
        if (XsltToJavaTypesUtil.catalogs.contains(propertyType)) {
            method = "\t\tBindingsProxy.bind(panel." + PropertyTransformationUtil.buildGetterMethodName(propertyName) + "(), beanModel, " + panelName + "." + PropertyTransformationUtil.uppercaseElement(propertyName) + ", CatalogManager.getInstance().getCatalogForUI(\"" + propertyType + "\")," + " CatalogManager.getInstance().getCatalogInvalidItem(\"" + propertyType + "\")," + "Session.isComboBoxEditable().booleanValue());\n";
        } else if (XsltToJavaTypesUtil.radiosList.containsKey(propertyType)) {
            Pair p = XsltToJavaTypesUtil.radiosList.get(propertyType);
            List listOfChoices = (List)p.getFirst();
            List listOfChoiceValues = (List)p.getSecond();
            method = BindingsTransformationUtil.buildPropertyBindChoice(panelName, propertyName, propertyType, listOfChoices, listOfChoiceValues);
        } else {
            method = "\t\tBindingsProxy.bind(panel." + PropertyTransformationUtil.buildGetterMethodName(propertyName) + "(), beanModel, " + panelName + "." + PropertyTransformationUtil.uppercaseElement(propertyName) + ", " + bindOnFocusLost + ");\n";
        }
        return method;
    }

    public static String buildTableModelBinding(String tableName, String className) {
        String stdClassName = StringUtil.capitalizeFirstCharacter(className.replace((CharSequence)"-", (CharSequence)"_"));
        String connector = "\n\t\tConnector<" + stdClassName + "> tableConnector" + stdClassName + " = GlazedLists.beanConnector(" + stdClassName + ".class);\n";
        String observableList = "\t\tObservableElementList<" + stdClassName + "> tableElementList" + stdClassName + " = " + "new ObservableElementList<" + stdClassName + ">(beanModel.getBean()." + PropertyTransformationUtil.buildGetterMethodName(tableName) + "(), tableConnector" + stdClassName + ");\n";
        String binding = "\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(tableName) + "().setModel(new EventTableModel<" + stdClassName + ">(tableElementList" + stdClassName + ", new " + stdClassName + "AdapterFormat()));\n";
        binding = binding + "\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(tableName) + "().putClientProperty(\"terminateEditOnFocusLost\", Boolean.TRUE);\n";
        binding = binding + "\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(tableName) + "().getTableHeader().setReorderingAllowed(false);\n\t\t";
        return connector + observableList + binding;
    }

    public static String buildRebindMethod(String modelName, String upperPanel) {
        String stdModelName = PropertyTransformationUtil.standardJavaClassName(modelName);
        String method = "\n\tpublic static void rebind(" + stdModelName + " model){\n\t\t" + "if(beanModel != null){\n\t\t\tbeanModel.setBean(null);\n\t\t}\n\t\t" + "beanModel = null;\n\t\t" + (!StringUtil.isEmpty(upperPanel) ? "doBindings(model, panelExtension);\n\t" : "panelExtension.setModel(model, false);\n\t") + "}";
        return method;
    }

    public static void addCatalogColumn(String tableName, List<TinyNodeImpl> listOfTypes, boolean indexColumn) {
        String method = "";
        String tableNameStd = PropertyTransformationUtil.standardJavaElementName(tableName);
        if (indexColumn) {
            method = method + "panel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(0).setCellRenderer(new RowIndexTableCellRenderer());\n\t\t";
            method = method + "panel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(0).setMaxWidth(pt.opensoft.swing.GUIParameters.TABLE_INDEX_COLUMN_WIDTH);";
        }
        for (int i = 0; i < listOfTypes.size(); ++i) {
            int colIndex = indexColumn ? i + 1 : i;
            String xsdType = listOfTypes.get(i).getStringValue();
            if (XsltToJavaTypesUtil.catalogs.contains(xsdType)) {
                method = method + "\n\t\tTableColumnBindingsProxy.getInstance().getTableColumnBindings().doBindingForCatalogColumn(panel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "(), " + "\"" + xsdType + "\", " + colIndex + ");\n\t\t";
            }
            if (xsdType.equals("xs:dateTime")) {
                method = method + "\n\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellEditor(new DateTimeCellEditor());\n\t\t" + "panel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellRenderer(new DateTimeCellRenderer());\n\t\t";
            }
            if (xsdType.equals("xs:date")) {
                method = method + "\n\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellEditor(new DateCellEditor());\n\t\t" + "panel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellRenderer(new DateCellRenderer());\n\t\t";
            }
            if (xsdType.equals("Decimal2")) {
                method = method + "\n\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellEditor(new NumericCellEditor());\n\t\t" + "panel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellRenderer(new NumericCellRenderer());\n\t\t";
            }
            if (!xsdType.equals("ComplexFieldHandler")) continue;
            method = method + "\n\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellEditor(new FileChooserCellEditor());\n\t\t" + "panel." + PropertyTransformationUtil.buildGetterMethodName(tableNameStd) + "().getColumnModel().getColumn(" + colIndex + ").setCellRenderer(new FileChooserCellRenderer());\n\t\t";
        }
        columnModelAuxiliary.append(method);
    }

    public static String buildColumnRendering() {
        String toReturn = columnModelAuxiliary.toString();
        columnModelAuxiliary = new StringBuffer();
        return toReturn;
    }

    public static void addBindingForEnabled(String propertyName, String propertyType) {
        if (XsltToJavaTypesUtil.radiosList.containsKey(propertyType)) {
            Pair p = XsltToJavaTypesUtil.radiosList.get(propertyType);
            List listOfChoices = (List)p.getFirst();
            for (int i = 0; i < listOfChoices.size(); ++i) {
                String choiceIdSuffix = ((TinyNodeImpl)listOfChoices.get(i)).getStringValue();
                String uicomponentName = PropertyTransformationUtil.buildGetterMethodName(propertyName + choiceIdSuffix);
                bindingsForEnabled.append("\t\tpanel." + uicomponentName + "().setEnabled(Session.isEditable().booleanValue());\n");
            }
        } else {
            bindingsForEnabled.append("\t\tpanel." + PropertyTransformationUtil.buildGetterMethodName(propertyName) + "().setEnabled(Session.isEditable().booleanValue());\n");
        }
    }

    public static String buildBindingsForEnabled(String panelName) {
        panelName = panelName.replace((CharSequence)"-", (CharSequence)"_");
        String toReturn = new String();
        toReturn = "\n\n\tpublic static void doBindingsForEnabled(" + panelName + "PanelExtension panel){\n" + bindingsForEnabled.toString() + "\t}\n";
        bindingsForEnabled = new StringBuffer();
        return toReturn;
    }

    public static String buildDoExtraBindings(String panelName, String upperPanel) {
        panelName = panelName.replace((CharSequence)"-", (CharSequence)"_");
        String toReturn = new String();
        if (upperPanel != null) {
            upperPanel = PropertyTransformationUtil.standardJavaClassName(upperPanel);
        }
        String uiPanelName = StringUtil.isEmpty(upperPanel) ? panelName + "PanelExtension" : upperPanel + "PanelExtension";
        toReturn = "\n\n\tpublic static void doExtraBindings(" + uiPanelName + " panel, " + panelName + " model){\n" + "//do stuff here! \n" + "}\n\n";
        return toReturn;
    }

    public static String buildUnidireccionalPropertyConnectorsForBehaviour(String panelName, String result, String formula) {
        if (StringUtil.isEmpty(formula)) {
            return "";
        }
        List<String> operandList = BehavioursGeneratorUtil.parseOperandString(formula);
        String method = "";
        String resultName = PropertyTransformationUtil.standardJavaElementName(result + "Formula");
        for (String operand : operandList) {
            if (operand.indexOf(".") != -1) {
                List operandTokenList = ListUtil.toList(operand, ".");
                String tableName = PropertyTransformationUtil.buildGetterMethodName(((String)operandTokenList.get(0)).substring(1));
                method = method + "\t\tbeanModel.getBean()." + tableName + "().addListEventListener( new ca.odell.glazedlists.event.ListEventListener(){\n\t\t\t" + "public void listChanged(ca.odell.glazedlists.event.ListEvent listChanges) {\n\t\t\t\t" + "try {\n\t\t\t\t\t" + "beanModel.getBean()." + PropertyTransformationUtil.buildSetterMethodName(resultName) + "(null);\n\t\t\t\t" + "} catch (Exception e) {\n\t\t\t\t\t" + "pt.opensoft.util.SimpleLog.log(e.getMessage(), e);\n\t\t\t\t" + "}\n\t\t\t" + "}\n\t\t" + "});\n";
                continue;
            }
            String propertyName = PropertyTransformationUtil.standardJavaElementName(operand);
            method = method + "\t\tUnidireccionalPropertyConnector.connect(beanModel.getBean(), " + panelName + "." + PropertyTransformationUtil.uppercaseElement(propertyName) + ", beanModel.getBean(), \"" + resultName + "\");\n\t\t";
        }
        return method;
    }

    public static boolean fileExists(String uri) {
        File file = new File(uri);
        return file.exists();
    }

    public static String buildOverwriteBehaviour(String panelName) {
        panelName = panelName.replace((CharSequence)"-", (CharSequence)"_");
        String toReturn = new String();
        toReturn = "\n\n\tpublic static void doBindingsForOverwriteBehavior(" + panelName + "PanelExtension panel){\n" + bindingsForOverwrite.toString() + "\t}\n";
        bindingsForOverwrite = new StringBuffer();
        return toReturn;
    }

    public static void addBindingOverwriteBehaviour(String propertyName, String propertyType) {
        String d = "OverwriteBehaviorManager.applyOverwriteBehavior(";
        if (XsltToJavaTypesUtil.radiosList.containsKey(propertyType)) {
            Pair p = XsltToJavaTypesUtil.radiosList.get(propertyType);
            List listOfChoices = (List)p.getFirst();
            for (int i = 0; i < listOfChoices.size(); ++i) {
                String choiceIdSuffix = ((TinyNodeImpl)listOfChoices.get(i)).getStringValue();
                String uicomponentName = PropertyTransformationUtil.buildGetterMethodName(propertyName + choiceIdSuffix);
                String id = PropertyTransformationUtil.standardJavaElementName(propertyName + choiceIdSuffix);
                bindingsForOverwrite.append("\t\t" + d + "panel." + uicomponentName + "(),\"" + id + "\");\n");
            }
        } else {
            String id = PropertyTransformationUtil.standardJavaElementName(propertyName);
            bindingsForOverwrite.append("\t\t" + d + "panel." + PropertyTransformationUtil.buildGetterMethodName(propertyName) + "(),\"" + id + "\");\n");
        }
    }
}

