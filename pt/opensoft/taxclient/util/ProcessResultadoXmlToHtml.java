/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import pt.opensoft.io.FileUtil;

public class ProcessResultadoXmlToHtml {
    private static final String TEMPLATE_FILE_PATH = "/templateResultadoAjudaXMLToHTML.xsl";
    private Document resultado;

    public ProcessResultadoXmlToHtml(Document document) {
        this.resultado = document;
    }

    public String xmlToHtml() throws Exception {
        File tempHtml = new File("");
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(new StreamSource(this.getClass().getResourceAsStream("/templateResultadoAjudaXMLToHTML.xsl")));
            File tempXml = File.createTempFile("resultadoAjuda", ".xml");
            tempHtml = File.createTempFile("resultado", ".html");
            tempXml.deleteOnExit();
            tempHtml.deleteOnExit();
            FileWriter fileWriter = new FileWriter(tempXml);
            XMLOutputter outputter = new XMLOutputter("  ", true, "ISO-8859-1");
            outputter.output(this.resultado, (Writer)fileWriter);
            fileWriter.close();
            String path = tempXml.getPath();
            transformer.transform(new StreamSource(path), new StreamResult(new OutputStreamWriter((OutputStream)new FileOutputStream(tempHtml), "ISO-8859-1")));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return FileUtil.readFile(tempHtml);
    }
}

