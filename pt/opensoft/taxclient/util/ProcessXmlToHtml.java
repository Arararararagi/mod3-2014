/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import pt.opensoft.io.FileUtil;
import pt.opensoft.logging.Logger;
import pt.opensoft.util.ArrayUtil;
import pt.opensoft.util.SimpleLog;

public class ProcessXmlToHtml {
    public static void main(String[] args) throws TransformerConfigurationException {
        Logger.getDefault().info("Args: 0 - root dir for the help files (XML); 1 - path to transformation XSL");
        Logger.getDefault().debug("Arguments received: " + ArrayUtil.toString(args));
        Logger.getDefault().info("This has to be run on the root of the project (trunk)");
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer(new StreamSource(args[1]));
        File dir = new File(args[0]);
        File[] listDirs = dir.listFiles();
        for (int i = 0; i < listDirs.length; ++i) {
            File insideFile = listDirs[i];
            if (!insideFile.isDirectory()) continue;
            File[] files = insideFile.listFiles();
            for (int j = 0; j < files.length; ++j) {
                File file = files[j];
                String filename = file.getName();
                String extension = FileUtil.getExtension(filename);
                if (!extension.equals("xml")) continue;
                String path = file.getPath();
                String dirPath = FileUtil.getDirectory(path);
                try {
                    transformer.transform(new StreamSource(path), new StreamResult(new FileOutputStream(dirPath + "/" + FileUtil.getFilenameOnly(filename).replace((CharSequence)"input", (CharSequence)"") + ".html")));
                }
                catch (Exception e) {
                    System.err.println("Erro:" + e);
                }
                SimpleLog.log("Processed - " + dirPath);
            }
        }
    }
}

