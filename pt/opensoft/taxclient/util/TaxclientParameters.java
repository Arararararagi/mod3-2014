/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.InputStream;
import pt.opensoft.util.SimpleParameters;

public class TaxclientParameters
extends SimpleParameters {
    private static final String PARAMETERS_FILE_NAME = "/taxclient.properties";
    private static final TaxclientParameters instance = new TaxclientParameters();
    public static final String WRITE_TABELA_EMPTY_LINES = "writeTabelaEmptyLines";
    public static final boolean INIT_USE_RIBBON_MENU = Boolean.parseBoolean(instance.getString("init.use.ribbon.menu", "true"));
    public static final boolean INIT_USE_CLASSIC_MENU = Boolean.parseBoolean(instance.getString("init.use.classic.menu", "false"));
    public static final boolean INIT_USE_TOP_MENU = Boolean.parseBoolean(instance.getString("init.use.top.menu", "false"));
    public static final String MF_DECL_FRAME_TITLE = instance.getString("mf.detail.frame.title", "Anexos");
    public static final boolean MF_USE_FRAME_FOR_DECL_DISPLAY = Boolean.parseBoolean(instance.getString("mf.use.frame.for.decl.display", "true"));
    public static final boolean MF_USE_FRAME_FOR_NAV_CONTROLLER = Boolean.parseBoolean(instance.getString("mf.use.frame.for.nav.controller", "true"));
    public static final String MF_NAV_FRAME_TITLE = instance.getString("mf.detail.frame.title", "Impressos");
    public static final boolean MF_USE_FRAME_FOR_NAV_DISPLAY = Boolean.parseBoolean(instance.getString("mf.use.frame.for.nav.display", "true"));
    public static final int MF_DIVIDER_SIZE = Integer.parseInt(instance.getString("mf.divider.size", "5"));
    public static final int MF_DIVIDER_LOCATION = Integer.parseInt(instance.getString("mf.divider.position", "175"));
    public static final String FORM_ROOT_DISPLAY_NAME = instance.getString("form.root.display.name", "Rosto");
    private static boolean isProduction = Boolean.parseBoolean(instance.getString("application.isProduction", "true"));
    private static boolean isQuality = Boolean.parseBoolean(instance.getString("application.isQuality", "false"));
    public static final boolean ANEXOS_ALLOW_ADD_REMOVE = Boolean.parseBoolean(instance.getString("anexos_allow_add_remove", "true"));
    public static final boolean SHOW_ANEXOS_NAVIGATION_ROOT = Boolean.parseBoolean(instance.getString("show.anexos.navigation.root", "false"));
    public static final boolean WRITE_EMPTY_LINES = Boolean.parseBoolean(instance.getString("write.tabela.empty.lines", System.getProperty("writeTabelaEmptyLines")));
    public static final String ERROR_REPORTING_TESTES_URL = instance.getString("error.reporting.url.dev", "http://suapps212:19050/pt/errorReporting.action");
    public static final String ERROR_REPORTING_QUA_URL = instance.getString("error.reporting.url.qua", "http://suapps304:19050/pt/errorReporting.action");
    public static final String ERROR_REPORTING_PROD_URL = instance.getString("error.reporting.url", "https://www.portaldasfinancas.gov.pt/pt/errorReporting.action");

    protected TaxclientParameters() {
        InputStream paramInput = TaxclientParameters.class.getResourceAsStream("/taxclient.properties");
        this.read(paramInput);
    }

    public static boolean isProduction() {
        return isProduction;
    }

    public static void setProduction(boolean production) {
        isProduction = production;
        if (production) {
            isQuality = false;
        }
    }

    public static boolean isQuality() {
        return isQuality;
    }

    public static void setQuality(boolean quality) {
        isQuality = quality;
        if (quality) {
            isProduction = false;
        }
    }

    public static String getRedirectAfterSubmission() {
        return instance.getString("redirect.after.submission", "");
    }

    public static boolean isToWriteTabelaEmptyLines() {
        return WRITE_EMPTY_LINES;
    }
}

