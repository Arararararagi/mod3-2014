/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import pt.opensoft.text.Base64;
import pt.opensoft.util.ZipUtil;

public class WebServicesUtil {
    private static final String ENCODE = "UTF-8";

    public static String encode(String message) {
        try {
            return URLEncoder.encode(Base64.encode(message), "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static String decode(String message) {
        try {
            return Base64.decode(URLDecoder.decode(message, "UTF-8"), true);
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static String getZipEncodedData(String data) throws IOException {
        try {
            return URLEncoder.encode(Base64.encode(ZipUtil.deflate(data)), "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public static String getZipDecodedData(String data) throws IOException {
        try {
            return ZipUtil.inflate(Base64.decode(URLDecoder.decode(data, "UTF-8")));
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}

