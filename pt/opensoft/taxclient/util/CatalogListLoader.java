/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.InputStream;
import java.util.List;
import pt.opensoft.io.ClasspathResourceFactory;
import pt.opensoft.io.ResourceLoader;
import pt.opensoft.io.ResourceNotFoundException;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.taxclient.util.PropertiesCatalogLoader;
import pt.opensoft.taxclient.util.TxtCatalogLoader;
import pt.opensoft.taxclient.util.XmlCatalogLoader;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.ListMap;

public class CatalogListLoader {
    public static ListMap loadXmlCatalogListModel(Class<ICatalogItem> catalogItemClass, String catalog) {
        XmlCatalogLoader<ICatalogItem> loader = new XmlCatalogLoader<ICatalogItem>();
        ResourceLoader.addResourceFactory(new ClasspathResourceFactory());
        try {
            InputStream stream = ResourceLoader.getResourceStream("./" + catalog + ".xml");
            List<ICatalogItem> list = loader.loadCatalogFromXML(stream, catalogItemClass);
            ListHashMap<Object, ICatalogItem> listMap = new ListHashMap<Object, ICatalogItem>();
            for (ICatalogItem catalogItem : list) {
                listMap.put(catalogItem.getValue(), catalogItem);
            }
            return listMap;
        }
        catch (ResourceNotFoundException e) {
            throw new RuntimeException("Could not load Catalog: " + catalog);
        }
    }

    public static ListMap loadPropertiesCatalogListModel(Class<ICatalogItem> catalogItemClass, String catalog, String propertyNameSetterMethodName, String propertyValueSetterMethodName) {
        PropertiesCatalogLoader<ICatalogItem> loader = new PropertiesCatalogLoader<ICatalogItem>(propertyNameSetterMethodName, propertyValueSetterMethodName);
        ResourceLoader.addResourceFactory(new ClasspathResourceFactory());
        try {
            InputStream stream = ResourceLoader.getResourceStream("./" + catalog + ".properties");
            List<ICatalogItem> list = loader.loadCatalogFromProperties(stream, catalogItemClass);
            ListHashMap<Object, ICatalogItem> listMap = new ListHashMap<Object, ICatalogItem>();
            for (ICatalogItem catalogItem : list) {
                listMap.put(catalogItem.getValue(), catalogItem);
            }
            return listMap;
        }
        catch (ResourceNotFoundException e) {
            throw new RuntimeException("Could not load Catalog: " + catalog);
        }
    }

    public static ListMap loadTxtCatalogListModel(Class<ICatalogItem> catalogItemClass, String catalog, String txtNameSetterMethodName, String txtValueSetterMethodName) {
        TxtCatalogLoader<ICatalogItem> loader = new TxtCatalogLoader<ICatalogItem>(txtNameSetterMethodName, txtValueSetterMethodName);
        ResourceLoader.addResourceFactory(new ClasspathResourceFactory());
        try {
            InputStream stream = ResourceLoader.getResourceStream("./" + catalog + ".txt");
            List<ICatalogItem> list = loader.loadCatalogFromTxt(stream, catalogItemClass);
            ListHashMap<Object, ICatalogItem> listMap = new ListHashMap<Object, ICatalogItem>();
            for (ICatalogItem catalogItem : list) {
                listMap.put(catalogItem.getValue(), catalogItem);
            }
            return listMap;
        }
        catch (ResourceNotFoundException e) {
            throw new RuntimeException("Could not load Catalog: " + catalog);
        }
    }
}

