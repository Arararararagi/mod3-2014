/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.util.Properties;
import pt.opensoft.taxclient.app.ProxyParameters;

public class SetSystemProxy {
    public static void setSystemProxy() {
        try {
            if (ProxyParameters.getUseProxy() && ProxyParameters.isProxySet()) {
                SetSystemProxy.setProxy(ProxyParameters.getProxyHost(), Integer.parseInt(ProxyParameters.getProxyPort()));
            } else {
                SetSystemProxy.unsetProxy();
            }
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    private static void setProxy(String proxyName, int proxyPort) {
        String proxyPortString = String.valueOf(proxyPort);
        System.setProperty("http.proxyHost", proxyName);
        System.setProperty("http.proxyPort", proxyPortString);
        System.setProperty("http.proxySet", "true");
        System.setProperty("https.proxyHost", proxyName);
        System.setProperty("https.proxyPort", proxyPortString);
        System.setProperty("https.proxySet", "true");
    }

    private static void unsetProxy() {
        Properties systemProperties = System.getProperties();
        systemProperties.remove("http.proxyHost");
        systemProperties.remove("http.proxyPort");
        systemProperties.remove("http.proxySet");
        systemProperties.remove("https.proxyHost");
        systemProperties.remove("https.proxyPort");
        systemProperties.remove("https.proxySet");
    }
}

