/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  net.sf.saxon.tinytree.TinyNodeImpl
 */
package pt.opensoft.taxclient.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.saxon.tinytree.TinyNodeImpl;
import pt.opensoft.swing.handlers.FieldHandlerModel;
import pt.opensoft.taxclient.util.PropertyTransformationUtil;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class XsltToJavaTypesUtil {
    public static List<String> catalogs = new ArrayList<String>();
    public static Map<String, Pair> radiosList = new HashMap<String, Pair>();
    public static Map<String, String> javaComplexTypes = new HashMap<String, String>();
    public static Map<String, String> decimalValuesFracions = new HashMap<String, String>();
    public static Map<String, String> complexFieldHandlers = new HashMap<String, String>();

    public static void addToTypes(String complexTypeName, String baseType, List<TinyNodeImpl> listOfIds, List<TinyNodeImpl> listOfValues) {
        String javaType = PropertyTransformationUtil.getJavaTypeFromXSD(baseType);
        if (!(listOfIds == null || listOfIds.isEmpty())) {
            Pair<List<TinyNodeImpl>, List<TinyNodeImpl>> pair = new Pair<List<TinyNodeImpl>, List<TinyNodeImpl>>(listOfIds, listOfValues);
            radiosList.put(complexTypeName, pair);
        } else {
            catalogs.add(complexTypeName);
        }
        javaComplexTypes.put(complexTypeName, javaType);
    }

    public static void addAttributeGroupToTypes(String attributeGroupName) {
        javaComplexTypes.put(attributeGroupName, StringUtil.capitalizeFirstCharacter(attributeGroupName));
    }

    public static void handleDecimalType(String name, String precision) {
        javaComplexTypes.put(name, ClassUtil.getClassName(Long.class));
        decimalValuesFracions.put(name, precision);
    }

    public static void handleComplexFieldHandlerType(String name, String handlerType) {
        javaComplexTypes.put(name, ClassUtil.getClassName(FieldHandlerModel.class));
        complexFieldHandlers.put(name, handlerType);
    }
}

