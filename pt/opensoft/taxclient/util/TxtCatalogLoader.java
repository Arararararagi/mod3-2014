/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import pt.opensoft.taxclient.util.PropertyTransformationUtil;
import pt.opensoft.util.Parameters;

public class TxtCatalogLoader<T> {
    private String propertyNameSetterMethodName;
    private String propertyValueSetterMethodName;
    private T pojoClassCatalog;
    private Class<T> pojoClass;
    private List<T> catalogEntriesList;

    public TxtCatalogLoader() {
        this("Name", "Value");
    }

    public TxtCatalogLoader(String propertyNameSetterMethodName, String propertyValueSetterMethodName) {
        this.propertyNameSetterMethodName = propertyNameSetterMethodName;
        this.propertyValueSetterMethodName = propertyValueSetterMethodName;
    }

    public List<T> loadCatalogFromTxt(InputStream stream, Class<T> pojoClass) {
        this.pojoClass = pojoClass;
        this.catalogEntriesList = new ArrayList<T>();
        Parameters parameters = Parameters.load(pojoClass.getSimpleName(), stream);
        Vector vector = parameters.getNamesVector();
        for (String propertyName : vector) {
            try {
                this.pojoClassCatalog = pojoClass.newInstance();
                this.invokeSetter(this.propertyNameSetterMethodName, propertyName);
                this.invokeSetter(this.propertyValueSetterMethodName, parameters.getValue(propertyName));
                this.catalogEntriesList.add(this.pojoClassCatalog);
            }
            catch (SecurityException e) {
                e.printStackTrace();
            }
            catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
        return this.catalogEntriesList;
    }

    private void invokeSetter(String setterMethodName, Object setterMethodParameter) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String setterName = PropertyTransformationUtil.buildSetterMethodName(setterMethodName);
        Method setterMethod = this.pojoClass.getMethod(setterName, this.getMethodParameterClass(this.pojoClass, setterName));
        Class setterParameterClass = setterMethod.getParameterTypes()[0];
        if (setterMethodParameter.getClass() == setterParameterClass) {
            setterMethod.invoke(this.pojoClassCatalog, setterMethodParameter);
        } else if (!(setterMethodParameter == null || setterMethodParameter.equals("null"))) {
            Object castVal = setterParameterClass.getDeclaredConstructor(setterMethodParameter.getClass()).newInstance(setterMethodParameter);
            setterMethod.invoke(this.pojoClassCatalog, castVal);
        }
    }

    private Class getMethodParameterClass(Class clazz, String methodName) {
        Method[] methods = clazz.getMethods();
        for (int i = 0; i < methods.length; ++i) {
            if (!methods[i].getName().equals(methodName)) continue;
            return methods[i].getParameterTypes()[0];
        }
        return null;
    }
}

