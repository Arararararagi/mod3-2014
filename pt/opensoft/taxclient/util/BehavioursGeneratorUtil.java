/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import pt.opensoft.taxclient.util.PropertyTransformationUtil;
import pt.opensoft.util.CharUtil;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class BehavioursGeneratorUtil {
    protected static final String OPERATIONS = "+-*/()";

    public static String handleFormula(String result, String formula) {
        List resultTokenList = ListUtil.toList(result, ".");
        String fieldToken = (String)resultTokenList.get(resultTokenList.size() - 1);
        String resultPropertyName = PropertyTransformationUtil.standardJavaElementName(fieldToken);
        String setterExtraCode = "\n\t\tlong newValue = 0;\n";
        Pair calculationPair = BehavioursGeneratorUtil.parseFormula(formula);
        String actualFormula = (String)calculationPair.getFirst();
        List intermediateCalculations = (List)calculationPair.getSecond();
        for (int i = 0; i < intermediateCalculations.size(); ++i) {
            String calculation = (String)intermediateCalculations.get(i);
            String variableInit = "\n\tlong temporaryValue" + i + "=0;" + "\n\t\t";
            if (!((variableInit = variableInit + calculation).trim().endsWith("}") || variableInit.trim().endsWith(";"))) {
                variableInit = variableInit + ";";
            }
            variableInit = variableInit + "\n\n";
            setterExtraCode = setterExtraCode + "\t\t" + variableInit;
        }
        setterExtraCode = setterExtraCode + "\t\tnewValue+=" + actualFormula + ";\n\n";
        setterExtraCode = setterExtraCode + "\t\tthis." + PropertyTransformationUtil.buildSetterMethodName(resultPropertyName) + "(newValue);\n";
        return setterExtraCode;
    }

    public static String handleSumBehaviour(String result, String formula) {
        return BehavioursGeneratorUtil.handleFormula(result, formula.replace((CharSequence)"\n", (CharSequence)""));
    }

    public static Pair parseFormula(String initialFormula) {
        StringTokenizer tokenizer = new StringTokenizer(initialFormula, "+-*/()", true);
        StringBuffer formulaOutput = new StringBuffer();
        ArrayList<String> tableColumnSums = new ArrayList<String>();
        int i = 0;
        while (tokenizer.hasMoreTokens()) {
            String token = StringUtil.trim(tokenizer.nextToken());
            if (BehavioursGeneratorUtil.isOperator(token, "+-*/()")) {
                formulaOutput.append(token + "\n");
                continue;
            }
            if (BehavioursGeneratorUtil.isConstant(token)) {
                formulaOutput.append(token);
                continue;
            }
            if (BehavioursGeneratorUtil.isColumnSum(token)) {
                String tableSum = BehavioursGeneratorUtil.resolveColumnSum(token, "temporaryValue" + i);
                formulaOutput.append("temporaryValue" + i);
                tableColumnSums.add(tableSum);
                ++i;
                continue;
            }
            formulaOutput.append(BehavioursGeneratorUtil.resolveOperand(token));
        }
        Pair pair = new Pair(formulaOutput.toString(), tableColumnSums);
        return pair;
    }

    private static String resolveColumnSum(String token, String variableName) {
        String columnSum = "";
        List operandTokenList = ListUtil.toList(token, ".");
        if (operandTokenList.size() == 2) {
            String tableName = PropertyTransformationUtil.buildGetterMethodName(((String)operandTokenList.get(0)).substring(1));
            String columnName = PropertyTransformationUtil.buildGetterMethodName(((String)operandTokenList.get(1)).substring(1));
            columnSum = columnSum + "\t\tfor(int i = 0; i < this." + tableName + "().size(); i++){\n\t\t\t" + variableName + " += this." + tableName + "().get(i)." + columnName + "() == null ? 0 : this." + tableName + "().get(i)." + columnName + "();\n\t\t" + "}";
        }
        return columnSum;
    }

    private static boolean isColumnSum(String token) {
        List operandTokenList = ListUtil.toList(token, ".");
        if (operandTokenList.size() == 2) {
            return ((String)operandTokenList.get(0)).substring(0, 1).equals("t") && ((String)operandTokenList.get(1)).substring(0, 1).equals("c");
        }
        return false;
    }

    private static Object resolveOperand(String token) {
        return "(this." + PropertyTransformationUtil.buildGetterMethodName(token) + "() == null ? 0 : this." + PropertyTransformationUtil.buildGetterMethodName(token) + "())";
    }

    private static boolean isConstant(String token) {
        return BehavioursGeneratorUtil.isNumericWithDot(token);
    }

    private static boolean isOperator(String token, String tokens) {
        return tokens.contains((CharSequence)token);
    }

    private static boolean isNumericWithDot(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isNumeric(ch) || ch == '.') continue;
            return false;
        }
        return true;
    }

    public static List<String> parseOperandString(String formula) {
        return ListUtil.toList(formula, "+-*/()");
    }

    public static List<String> parseOperationString(String formula) {
        StringTokenizer tokenizer = new StringTokenizer(formula, "+-*/()", true);
        ArrayList<String> tokens = new ArrayList<String>();
        while (tokenizer.hasMoreTokens()) {
            if (!tokenizer.hasMoreTokens()) continue;
            String operand = tokenizer.nextToken();
            tokens.add(operand);
        }
        return tokens;
    }
}

