/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.util.Map;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public abstract class XmlProcessorHandler {
    private int depth = 0;
    private boolean failed = false;
    private boolean stopChainOnError = false;
    private XmlProcessorHandler handler = null;

    public XmlProcessorHandler(boolean stopChainOnError) {
        this(null, stopChainOnError);
    }

    public XmlProcessorHandler(XmlProcessorHandler handler, boolean stopChainOnError) {
        this.handler = handler;
        this.stopChainOnError = stopChainOnError;
    }

    public final boolean startElementCallback(String uri, String localName, String qName, Map attributes) throws Exception {
        ++this.depth;
        if (!this.failed) {
            try {
                if (!this.startElement(uri, localName, qName, attributes, this.depth)) {
                    this.failed();
                }
            }
            catch (Exception e) {
                this.failed();
                throw e;
            }
        }
        if (this.handler != null) {
            return this.handler.startElementCallback(uri, localName, qName, attributes);
        }
        return this.failed && this.stopChainOnError;
    }

    protected abstract boolean startElement(String var1, String var2, String var3, Map var4, int var5) throws Exception;

    public final boolean endElementCallback(String uri, String localName, String qName) throws Exception {
        if (!this.failed) {
            try {
                if (!this.endElement(uri, localName, qName, this.depth)) {
                    this.failed();
                }
            }
            catch (Exception e) {
                this.failed();
                throw e;
            }
        }
        if (this.handler != null) {
            return this.handler.endElementCallback(uri, localName, qName);
        }
        --this.depth;
        return this.failed && this.stopChainOnError;
    }

    protected abstract boolean endElement(String var1, String var2, String var3, int var4) throws Exception;

    public final boolean charactersCallback(char[] ch, int start, int length) {
        if (!this.failed) {
            try {
                if (!this.characters(ch, start, length, this.depth)) {
                    this.failed();
                }
            }
            catch (Exception e) {
                this.failed();
            }
        }
        if (this.handler != null) {
            return this.handler.charactersCallback(ch, start, length);
        }
        return this.failed && this.stopChainOnError;
    }

    protected abstract boolean characters(char[] var1, int var2, int var3, int var4) throws Exception;

    public abstract void error(SAXParseException var1) throws SAXException;

    public abstract void fatalError(SAXParseException var1) throws SAXException;

    public abstract void warning(SAXParseException var1) throws SAXException;

    private void failed() {
        this.failed = this.stopChainOnError;
    }
}

