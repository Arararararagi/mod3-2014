/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import com.jgoodies.validation.ValidationResult;

public enum ServerValidationResult {
    CENTRAL_ERRORS;
    
    private ValidationResult validationResult = new ValidationResult();

    private ServerValidationResult() {
    }

    public void setValidationResult(ValidationResult validationResult) {
        this.validationResult = validationResult;
    }

    public ValidationResult getValidationResult() {
        return this.validationResult;
    }

    public void clearValidationResult() {
        if (this.validationResult != null) {
            this.validationResult = new ValidationResult();
        }
    }
}

