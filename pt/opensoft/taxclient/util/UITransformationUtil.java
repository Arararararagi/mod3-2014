/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.File;
import pt.opensoft.taxclient.util.PropertyTransformationUtil;

public class UITransformationUtil {
    private static String addQuadroRepetitivo = "";

    public static String buildPanelExtensionAddLineAction(String baseName, String className) {
        String lineClassName = PropertyTransformationUtil.standardJavaClassName(className);
        String baseClassName = PropertyTransformationUtil.standardJavaClassName(baseName);
        String method = "\tprotected void addLine" + lineClassName + "ActionPerformed(ActionEvent e) { \n" + "\t\tif ((Boolean)Session.isEditable().getValue()){\n" + "\t\t\tthis.model." + PropertyTransformationUtil.buildGetterMethodName(baseClassName) + "().add(new " + lineClassName + "());\n" + "\t\t}\n" + "\t}\n";
        return method;
    }

    public static String buildPanelExtensionRemoveLineAction(String baseName, String className) {
        String lineClassName = PropertyTransformationUtil.standardJavaClassName(className);
        String baseClassName = PropertyTransformationUtil.standardJavaClassName(baseName);
        String propertyName = PropertyTransformationUtil.standardJavaElementName(baseName);
        String method = "\tprotected void removeLine" + lineClassName + "ActionPerformed(ActionEvent e) { \n" + "\t\tif ((Boolean)Session.isEditable().getValue()){\n" + "\t\t\tint selectedRow = (this." + propertyName + ".getSelectedRow() != -1 ? this." + propertyName + ".getSelectedRow() : this." + propertyName + ".getRowCount() - 1);\n" + "\t\t\tif(selectedRow != -1){\n" + "\t\t\t\tthis.model." + PropertyTransformationUtil.buildGetterMethodName(baseClassName) + "().remove(selectedRow);\n" + "\t\t\t}\n" + "\t\t}\n" + "\t}\n";
        return method;
    }

    public static String buildAddQuadroRepetitivo(String quadroName, String className) {
        String classModelName = PropertyTransformationUtil.standardJavaClassName(className);
        String classModelBindings = classModelName + "Bindings";
        String quadroNameStd = PropertyTransformationUtil.standardJavaClassName(quadroName);
        String methodListModelGetter = PropertyTransformationUtil.buildGetterMethodName(quadroNameStd) + "()";
        String method = "protected void add" + quadroNameStd + "Action(ActionEvent e){\n" + "\t\tif ((Boolean)Session.isEditable().getValue()){\n" + "\t\t\t" + classModelName + " tempModel = new " + classModelName + "();\n" + "\t\t\tthis.model." + methodListModelGetter + ".getList().add(tempModel);\n" + "\t\t\tthis.model." + methodListModelGetter + ".setSelectionIndex(this.model." + methodListModelGetter + ".getList().size()-1);\n" + "\t\t\tcall" + classModelBindings + "(this.model." + methodListModelGetter + ".getSelection());\n" + "\t\t\tthis.model.setTotPag(this.model." + methodListModelGetter + ".getList().size());\n" + "\t\t\tthis.model.setPagActual(this.model." + methodListModelGetter + ".getSelectionIndex() +1);\n" + "\t\t}\n" + "\t}\n\n";
        addQuadroRepetitivo = addQuadroRepetitivo + "\n\t\tadd" + quadroNameStd + "Action(null);\n";
        return method;
    }

    public static String buildQuadroRepetitivoBindingsCallImpl(String quadroName, String className) {
        String classModelName = PropertyTransformationUtil.standardJavaClassName(className);
        String classModelBindings = classModelName + "Bindings";
        String method = "protected void call" + classModelBindings + "(" + classModelName + " model){\n" + "\t\t\t" + classModelBindings + ".doBindings(model, this);\n" + "\t}\n\n";
        return method;
    }

    public static String buildQuadroRepetitivoBindingsCallAbstract(String quadroName, String className) {
        String classModelName = PropertyTransformationUtil.standardJavaClassName(className);
        String classModelBindings = classModelName + "Bindings";
        String method = "protected abstract void call" + classModelBindings + "(" + classModelName + " model);\n\n";
        return method;
    }

    public static String buildGotoIndexQuadroRepetitivo(String quadroName, String className) {
        String classModelName = PropertyTransformationUtil.standardJavaClassName(className);
        String classModelBindings = classModelName + "Bindings";
        String quadroNameStd = PropertyTransformationUtil.standardJavaClassName(quadroName);
        String methodListModelGetter = PropertyTransformationUtil.buildGetterMethodName(quadroNameStd) + "()";
        String method = "public void goto" + quadroNameStd + "Action(int index){\n" + "this.model.setPagActual(index);\n" + "if (this.model." + methodListModelGetter + ".getSize() > 0) " + "this.model." + methodListModelGetter + ".setSelectionIndex(index-1);\n" + "call" + classModelBindings + "(this.model." + methodListModelGetter + ".getSelection());\n" + "\t}\n\n";
        return method;
    }

    public static String buildRemoveQuadroRepetitivo(String quadroName, String className) {
        String classModelName = PropertyTransformationUtil.standardJavaClassName(className);
        String classModelBindings = classModelName + "Bindings";
        String quadroNameStd = PropertyTransformationUtil.standardJavaClassName(quadroName);
        String methodListModelGetter = PropertyTransformationUtil.buildGetterMethodName(quadroNameStd) + "()";
        String method = "\tprotected void remove" + quadroNameStd + "Action(ActionEvent e){\n\t\t" + "if ((Boolean)Session.isEditable().getValue()){\n\t\t\t" + "int count = this.model." + methodListModelGetter + ".getList().size();\n\t\t\t" + "int indexToRemove = this.model." + methodListModelGetter + ".getSelectionIndex();\n\t\t\t" + "if(count == 0){\n\t\t\t\t" + "return;\n\t\t\t" + "}\n\t\t\t" + "this.model." + methodListModelGetter + ".getList().remove(indexToRemove);\n\t\t\t" + "this.model.setTotPag(this.model.getTotPag()-1);\n\t\t\t" + "if(count == 1){\n\t\t\t\t" + "add" + quadroNameStd + "Action(e);\n\t\t\t\t" + "return;\n\t\t\t" + "}\n\t\t\t" + classModelName + " modelToBind;\n\t\t\t" + "if(indexToRemove == count-1){\n\t\t\t" + "modelToBind = this.model." + methodListModelGetter + ".getList().get(indexToRemove-1);\n\t\t\t" + "this.model." + methodListModelGetter + ".setSelectionIndex(indexToRemove-1);\n\t\t\t" + "this.model.setPagActual(indexToRemove);\n\t\t\t" + "} else {\n\t\t\t" + "modelToBind = this.model." + methodListModelGetter + ".getList().get(indexToRemove);\n\t\t\t" + "this.model." + methodListModelGetter + ".setSelectionIndex(indexToRemove);\n\t\t\t" + "this.model.setPagActual(indexToRemove+1);\n\t\t\t" + "}\n\t\t\t" + "call" + classModelBindings + "(modelToBind);\n\t\t\t" + "}\n\t\t" + "}\n\n";
        return method;
    }

    public static String buildSetModel(String panelModelName, String panelBindingsName) {
        String method = "\n\tpublic void setModel(" + PropertyTransformationUtil.standardJavaClassName(panelModelName) + " model){ \n" + "\t\tthis.model = model;\n" + "\t\t" + panelBindingsName + ".doBindings(model, this);\n" + "\t}";
        return method;
    }

    public static String buildNextQuadroRepetitivo(String baseName, String lineName) {
        String stdLineName = PropertyTransformationUtil.standardJavaClassName(lineName);
        String stdQuadroName = PropertyTransformationUtil.standardJavaClassName(baseName);
        String classModelBindings = stdLineName + "Bindings";
        String methodListModelGetter = PropertyTransformationUtil.buildGetterMethodName(stdQuadroName) + "()";
        String method = "\tprotected void next" + stdQuadroName + "Action(ActionEvent e){\n" + "\t\tint nextOne = this.model." + methodListModelGetter + ".getSelectionIndex() +1;\n" + "\t\tif(this.model." + methodListModelGetter + ".getList().size() > nextOne){\n" + "\t\tthis.model.setPagActual(nextOne + 1);\n" + "\t\t\tthis.model." + methodListModelGetter + ".setSelectionIndex(nextOne);\n" + "\t\t\tcall" + classModelBindings + "(model." + methodListModelGetter + ".getElementAt(nextOne));\n" + "\t\t}\n" + "\t}\n\n";
        return method;
    }

    public static String buildPreviousQuadroRepetitivo(String baseName, String lineName) {
        String standardJavaClassName = PropertyTransformationUtil.standardJavaClassName(lineName);
        String classModelBindings = standardJavaClassName + "Bindings";
        String methodListModelGetter = PropertyTransformationUtil.buildGetterMethodName(baseName) + "()";
        String method = "\tprotected void previous" + PropertyTransformationUtil.standardJavaClassName(baseName) + "Action(ActionEvent e){\n" + "\t\tint previousOne = this.model." + methodListModelGetter + ".getSelectionIndex() -1;\n" + "\t\tif(previousOne >= 0){\n" + "\t\t\tthis.model.setPagActual(previousOne + 1);\n" + "\t\t\tthis.model." + methodListModelGetter + ".setSelectionIndex(previousOne);\n" + "\t\t\tcall" + classModelBindings + "(model." + methodListModelGetter + ".getElementAt(previousOne));\n" + "\t\t}\n" + "\t}\n\n";
        return method;
    }

    public static String buildPanelExtensionQRAddLineAction(String qrName, String tableName, String className) {
        String methodQRName = PropertyTransformationUtil.buildGetterMethodName(qrName) + "()";
        String lineClassName = PropertyTransformationUtil.standardJavaClassName(className);
        String method = "\tprotected void addLine" + lineClassName + "ActionPerformed(ActionEvent e) { \n" + "\t\tif ((Boolean)Session.isEditable().getValue()){\n" + "\t\t\tthis.model." + methodQRName + ".getSelection()." + PropertyTransformationUtil.buildGetterMethodName(tableName) + "().add(new " + lineClassName + "());\n" + "\t\t}\n" + "\t}\n";
        return method;
    }

    public static String buildPanelExtensionQRRemoveLineAction(String qrName, String tableClassName, String className) {
        String stdClassName = PropertyTransformationUtil.standardJavaClassName(tableClassName);
        String methodListModelGetter = PropertyTransformationUtil.buildGetterMethodName(qrName) + "()";
        String stdTableName = PropertyTransformationUtil.standardJavaElementName(tableClassName);
        String lineClassName = PropertyTransformationUtil.standardJavaClassName(className);
        String method = "\tprotected void removeLine" + lineClassName + "ActionPerformed(ActionEvent e) { \n" + "\t\tif ((Boolean)Session.isEditable().getValue()){\n" + "\t\t\tint selectedRow = (this." + stdTableName + ".getSelectedRow() != -1 ? this." + stdTableName + ".getSelectedRow() : this." + stdTableName + ".getRowCount() - 1);\n" + "\t\t\tif(selectedRow != -1){\n" + "\t\t\tthis.model." + methodListModelGetter + ".getSelection()." + PropertyTransformationUtil.buildGetterMethodName(stdClassName) + "().remove(selectedRow);\n" + "\t\t\t}\n" + "\t\t}\n" + "\t}\n";
        return method;
    }

    public static String buildFirstQuadroCall() {
        String calls = addQuadroRepetitivo;
        addQuadroRepetitivo = "";
        return calls;
    }

    public static boolean fileExists(String uri) {
        File file = new File(uri);
        return file.exists();
    }

    public static String logthis(String message) {
        return message;
    }
}

