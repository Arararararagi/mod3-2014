/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  net.sf.saxon.tinytree.TinyNodeImpl
 */
package pt.opensoft.taxclient.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.sf.saxon.tinytree.TinyNodeImpl;
import pt.opensoft.taxclient.util.PropertyTransformationUtil;
import pt.opensoft.util.SimpleLog;

public class ComplexTypesClassGenerator {
    public static String generateClassBaseFromComplexType(String name, List<TinyNodeImpl> fields, List<TinyNodeImpl> types, String packagePath) {
        String baseName = PropertyTransformationUtil.standardJavaClassName(name + "Base");
        SimpleLog.log("GENERATING: " + name);
        ArrayList<String> fieldList = new ArrayList<String>(fields.size());
        ArrayList<String> typesList = new ArrayList<String>(types.size());
        for (TinyNodeImpl type : types) {
            typesList.add(type.getStringValue());
        }
        for (TinyNodeImpl field : fields) {
            fieldList.add(field.getStringValue());
        }
        packagePath.replaceAll("/", ".");
        packagePath = packagePath + ".catalogs";
        String pojo = "package " + packagePath + ";\n\nimport pt.opensoft.swing.model.catalogs.ICatalogItem;\n" + "import pt.opensoft.taxclient.persistence.Type;\n\n";
        pojo = pojo + "public class " + baseName + "{\n\n" + "\t" + PropertyTransformationUtil.buildEmptyConstructor(baseName) + "\n\n" + "\t" + PropertyTransformationUtil.buildConstructorWithFieldsByLists(baseName, fieldList, typesList) + "\n\n";
        for (int i = 0; i < fieldList.size(); ++i) {
            String element = fieldList.get(i);
            String type2 = typesList.get(i);
            pojo = pojo + PropertyTransformationUtil.buildPropertyDeclaration(element, type2) + "\n\n";
            pojo = pojo + PropertyTransformationUtil.buildGetter(element, type2) + "\n\n";
            pojo = pojo + PropertyTransformationUtil.buildSetter(name, element, type2, false) + "\n\n";
        }
        pojo = pojo + "}\n";
        return pojo;
    }

    public static String generateClassForUpperComplexType(String name, List<TinyNodeImpl> fields, List<TinyNodeImpl> types, String packagePath) {
        String baseName = PropertyTransformationUtil.standardJavaClassName(name + "Base");
        String bizName = PropertyTransformationUtil.standardJavaClassName(name);
        ArrayList<String> fieldList = new ArrayList<String>(fields.size());
        ArrayList<String> typesList = new ArrayList<String>(types.size());
        for (TinyNodeImpl type : types) {
            typesList.add(type.getStringValue());
        }
        for (TinyNodeImpl field : fields) {
            fieldList.add(field.getStringValue());
        }
        packagePath.replaceAll("/", ".");
        packagePath = packagePath + ".catalogs";
        String pojo = "package " + packagePath + ";\n\n" + "import pt.opensoft.swing.model.catalogs.ICatalogItem;\n\n";
        pojo = pojo + "public class " + bizName + " extends " + baseName + " implements ICatalogItem{\n\n" + PropertyTransformationUtil.buildConstructorWithFieldsByListsSuper(bizName, fieldList, typesList) + "\t@Override\n" + "\tpublic String getDescription(){\n" + "\t\treturn super.getDescricao();\n" + "\t\t //TODO: Auto-generated, make sure this works!\n" + "\t}\n\n";
        pojo = pojo + "public " + bizName + "(){\n\t\tsuper();\n}\n\n";
        pojo = pojo + "\t@Override\n\tpublic String getValue(){\n\t\treturn super.getCodigo();\n\t\t //TODO: Auto-generated, make sure this works!\n\t}\n\n";
        pojo = pojo + "\t@Override\n\tpublic String toString(){\n\t\treturn this.getDescription();\n\t\t //TODO: Auto-generated, make sure this works!\n\t}\n\n;}\n\n";
        return pojo;
    }

    public static boolean fileExists(String uri) {
        File file = new File(uri);
        return file.exists();
    }
}

