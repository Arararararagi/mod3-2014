/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import pt.opensoft.taxclient.util.PropertyTransformationUtil;
import pt.opensoft.util.StringUtil;

public class XmlCatalogLoader<T>
extends DefaultHandler {
    public static final String ROOT = "Catalog";
    private String tempVal;
    private T pojoClassCatalog;
    private Class<T> pojoClass;
    private String baseClassName;
    private List<T> catalogEntriesList;

    public List<T> loadCatalogFromXML(InputStream stream, Class<T> pojoClass) {
        this.pojoClass = pojoClass;
        this.catalogEntriesList = new ArrayList<T>();
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = saxParserFactory.newSAXParser();
            parser.parse(stream, (DefaultHandler)this);
        }
        catch (SAXException se) {
            se.printStackTrace();
        }
        catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return this.catalogEntriesList;
    }

    public List<T> loadCatalogFromXML(File xmlFile, Class<T> pojoClass) {
        this.pojoClass = pojoClass;
        this.catalogEntriesList = new ArrayList<T>();
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = saxParserFactory.newSAXParser();
            parser.parse(xmlFile, (DefaultHandler)this);
        }
        catch (SAXException se) {
            se.printStackTrace();
        }
        catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return this.catalogEntriesList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.tempVal = "";
        if (qName.equals("Catalog")) {
            this.baseClassName = attributes.getValue(0);
        } else if (qName.equals(this.baseClassName)) {
            try {
                this.pojoClassCatalog = this.pojoClass.newInstance();
            }
            catch (InstantiationException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        this.tempVal = !StringUtil.isEmpty(this.tempVal) ? this.tempVal + new String(ch, start, length) : new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (!qName.equals("Catalog")) {
            if (qName.equals(this.baseClassName)) {
                this.catalogEntriesList.add(this.pojoClassCatalog);
            } else {
                String propertyName = qName;
                try {
                    String setterName = PropertyTransformationUtil.buildSetterMethodName(propertyName);
                    Method setterMethod = this.pojoClass.getMethod(setterName, this.getMethodParameterClass(this.pojoClass, setterName));
                    Class setterParameterClass = setterMethod.getParameterTypes()[0];
                    if (this.tempVal.getClass() == setterParameterClass) {
                        setterMethod.invoke(this.pojoClassCatalog, this.tempVal);
                    } else if (!(this.tempVal == null || this.tempVal.equals("null"))) {
                        Object castVal = setterParameterClass.getDeclaredConstructor(this.tempVal.getClass()).newInstance(this.tempVal);
                        setterMethod.invoke(this.pojoClassCatalog, castVal);
                    }
                }
                catch (SecurityException e) {
                    e.printStackTrace();
                }
                catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                catch (InstantiationException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Class getMethodParameterClass(Class clazz, String methodName) {
        Method[] methods = clazz.getMethods();
        for (int i = 0; i < methods.length; ++i) {
            if (!methods[i].getName().equals(methodName)) continue;
            return methods[i].getParameterTypes()[0];
        }
        return null;
    }
}

