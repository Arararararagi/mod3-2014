/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import pt.opensoft.search.StopWords;
import pt.opensoft.taxclient.util.TaxclientParameters;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.XmlBuilder;

public class IndexerSearch {
    private List list_path_to_search;
    private String path_stop_words;
    private String textToSearch;
    private Document resultado;
    private int numResultados;
    private int numResultadosLimited;

    public IndexerSearch(List pathToSearch, String pathStopWords, String textToSearch) {
        this.textToSearch = textToSearch;
        this.list_path_to_search = pathToSearch;
        this.path_stop_words = pathStopWords;
        this.numResultados = 0;
        this.numResultadosLimited = 0;
    }

    public IndexerSearch(List pathToSearch, String pathStopWords) {
        this.numResultados = 0;
        this.numResultadosLimited = 0;
        this.list_path_to_search = pathToSearch;
        this.path_stop_words = pathStopWords;
    }

    public Document processSearch() throws Exception {
        String textoAProcurar = this.textToSearch;
        this.resultado = new Document();
        this.resultado.setRootElement(new Element("Resultado"));
        InputStream fis = this.getClass().getResourceAsStream(this.path_stop_words);
        StopWords stopWords = new StopWords(fis);
        String[] arrayStopWords = stopWords.getStopWords();
        String[] toSearch = textoAProcurar.split(" ");
        String textoAux = "";
        int numberStpWds = 0;
        int[] wordsToDel = new int[toSearch.length];
        for (int ts = 0; ts < toSearch.length; ++ts) {
            if (toSearch[ts].equals("")) {
                ++numberStpWds;
                wordsToDel[ts] = 1;
                continue;
            }
            for (int stp = 0; stp < arrayStopWords.length; ++stp) {
                if (!arrayStopWords[stp].equals(toSearch[ts])) continue;
                ++numberStpWds;
                wordsToDel[ts] = 1;
            }
        }
        if (numberStpWds == toSearch.length) {
            this.resultado.getRootElement().addContent(" ");
            Element italiqErro = new Element("iE");
            italiqErro.addContent("N\u00e3o foram encontrados nenhuns resultados com a palavra: ");
            this.resultado.getRootElement().addContent(italiqErro);
            Element highlight = new Element("highlight");
            highlight.addContent(textoAProcurar);
            this.resultado.getRootElement().addContent(highlight);
            return this.resultado;
        }
        for (int wtd = 0; wtd < wordsToDel.length; ++wtd) {
            if (wordsToDel[wtd] == 1) continue;
            textoAux = textoAux + toSearch[wtd] + " ";
        }
        textoAProcurar = textoAux;
        for (int j = 0; j < this.list_path_to_search.size(); ++j) {
            String pathResource = (String)this.list_path_to_search.get(j);
            InputStreamReader fileReader = new InputStreamReader(this.getClass().getResourceAsStream(pathResource), "ISO8859_1");
            XmlBuilder xmlBuilder = new XmlBuilder(false);
            xmlBuilder.parse(fileReader);
            Document document = xmlBuilder.getDocument();
            Element root = document.getRootElement();
            List quadros = root.getChildren();
            for (Element quadro : quadros) {
                List elementsQuadro = quadro.getChildren();
                for (int elementsQuadroCount = 0; elementsQuadroCount < elementsQuadro.size(); ++elementsQuadroCount) {
                    Element elementoQuadro = (Element)elementsQuadro.get(elementsQuadroCount);
                    List elementsCampo = elementoQuadro.getChildren();
                    for (int elementsCamposCount = 0; elementsCamposCount < elementsCampo.size(); ++elementsCamposCount) {
                        int aux;
                        Element elementoCampo = (Element)elementsCampo.get(elementsCamposCount);
                        double inKeys = -1.0;
                        if (elementoCampo.getName().equals("keyword")) {
                            inKeys = this.searchInKeyWords(textoAProcurar, elementoCampo.getAttributeValue("keys"));
                        }
                        String textoOndeProcurar = this.getAllText(elementoCampo);
                        List resultSearch = this.searchText(textoAProcurar, textoOndeProcurar);
                        String resultSearchText = (String)resultSearch.get(1);
                        double resultSearchSimilarity = Double.parseDouble((String)resultSearch.get(0));
                        elementoCampo.setText(resultSearchText);
                        textoOndeProcurar = elementoCampo.getText();
                        List resultSearchLimited = this.searchLimitedText(textoAProcurar, textoOndeProcurar);
                        String resultSearchLimitedText = (String)resultSearchLimited.get(1);
                        double resultSearchLimitedSimilarity = Double.parseDouble((String)resultSearchLimited.get(0));
                        elementoCampo.setText(resultSearchLimitedText);
                        double similarity = resultSearchSimilarity >= 0.75 || resultSearchLimitedSimilarity >= 0.75 ? 1.0 : -1.0;
                        Element rootElement = this.resultado.getRootElement();
                        Element campoElement = elementoCampo.getParent();
                        Element quadroElement = campoElement.getParent();
                        Element ajudaElement = quadroElement.getParent();
                        Element nextChild = rootElement;
                        elementoCampo = this.highlight(elementoCampo);
                        if (similarity != -1.0 || inKeys != -1.0) {
                            nextChild = !IndexerSearch.hasChildWithAttribute(nextChild, ajudaElement) ? IndexerSearch.createChildWithAttribute(nextChild, ajudaElement) : IndexerSearch.getChildByNameAndAttribute(nextChild.getChildren(ajudaElement.getName()), ajudaElement.getAttributeValue("id"));
                            nextChild = !IndexerSearch.hasChildWithAttribute(nextChild, quadroElement) ? IndexerSearch.createChildWithAttribute(nextChild, quadroElement) : IndexerSearch.getChildByNameAndAttribute(nextChild.getChildren(quadroElement.getName()), quadroElement.getAttributeValue("id"));
                            if (elementsQuadroCount > 0 && ((Element)elementsQuadro.get(elementsQuadroCount - 1)).getName().equals("link")) {
                                for (aux = elementsQuadroCount - 1; aux >= 0 && ((Element)elementsQuadro.get(aux)).getName().equals("link"); --aux) {
                                    IndexerSearch.createChildWithAttribute(nextChild, (Element)elementsQuadro.get(aux));
                                }
                            }
                            nextChild = !IndexerSearch.hasChildWithAttribute(nextChild, campoElement) ? IndexerSearch.createChildWithAttribute(nextChild, campoElement) : IndexerSearch.getChildByNameAndAttribute(nextChild.getChildren(campoElement.getName()), campoElement.getAttributeValue("id"));
                            if (elementsCamposCount > 0 && ((Element)elementsCampo.get(elementsCamposCount - 1)).getName().equals("link")) {
                                for (aux = elementsCamposCount - 1; aux >= 0 && ((Element)elementsCampo.get(aux)).getName().equals("link"); --aux) {
                                    IndexerSearch.createChildWithAttribute(nextChild, (Element)elementsCampo.get(aux));
                                }
                            } else if (elementsCamposCount > 0 && ((Element)elementsCampo.get(elementsCamposCount - 1)).getName().equals("titulo") && !elementoCampo.getName().equals("titulo")) {
                                IndexerSearch.createChildWithAttribute(nextChild, (Element)elementsCampo.get(elementsCamposCount - 1));
                            }
                            if (!elementoCampo.getName().equals("texto")) continue;
                            nextChild = IndexerSearch.createChildWithAttribute(nextChild, elementoCampo);
                            continue;
                        }
                        if (!IndexerSearch.hasChildWithAttribute(nextChild, ajudaElement) || !IndexerSearch.hasChildWithAttribute(nextChild = IndexerSearch.getChildByNameAndAttribute(nextChild.getChildren(ajudaElement.getName()), ajudaElement.getAttributeValue("id")), quadroElement)) continue;
                        nextChild = IndexerSearch.getChildByNameAndAttribute(nextChild.getChildren(quadroElement.getName()), quadroElement.getAttributeValue("id"));
                        if (elementsQuadroCount > 0 && ((Element)elementsQuadro.get(elementsQuadroCount - 1)).getName().equals("link")) {
                            for (aux = elementsQuadroCount - 1; aux >= 0 && ((Element)elementsQuadro.get(aux)).getName().equals("link"); --aux) {
                                IndexerSearch.createChildWithAttribute(nextChild, (Element)elementsQuadro.get(aux));
                            }
                        }
                        if (!IndexerSearch.hasChildWithAttribute(nextChild, campoElement)) continue;
                        nextChild = IndexerSearch.getChildByNameAndAttribute(nextChild.getChildren(campoElement.getName()), campoElement.getAttributeValue("id"));
                        if (elementsCamposCount > 0 && ((Element)elementsCampo.get(elementsCamposCount - 1)).getName().equals("link")) {
                            for (aux = elementsCamposCount - 1; aux >= 0 && ((Element)elementsCampo.get(aux)).getName().equals("link"); --aux) {
                                IndexerSearch.createChildWithAttribute(nextChild, (Element)elementsCampo.get(aux));
                            }
                        } else if (elementsCamposCount > 0 && ((Element)elementsCampo.get(elementsCamposCount - 1)).getName().equals("titulo") && !elementoCampo.getName().equals("titulo")) {
                            IndexerSearch.createChildWithAttribute(nextChild, (Element)elementsCampo.get(elementsCamposCount - 1));
                        }
                        if (!elementoCampo.getName().equals("texto")) continue;
                        nextChild = IndexerSearch.createChildWithAttribute(nextChild, elementoCampo);
                    }
                }
            }
        }
        this.numResultados+=this.numResultadosLimited;
        if (this.resultado.getRootElement().getChildren().isEmpty()) {
            this.resultado.getRootElement().addContent(" ");
            Element italiqErro = new Element("iE");
            italiqErro.addContent("N\u00e3o foram encontrados nenhuns resultados com a palavra: ");
            this.resultado.getRootElement().addContent(italiqErro);
            Element highlight = new Element("highlight");
            highlight.addContent(textoAProcurar);
            this.resultado.getRootElement().addContent(highlight);
            return this.resultado;
        }
        List ajudas = this.resultado.getRootElement().getChildren("Ajuda");
        this.ordenaResultado(ajudas);
        return this.resultado;
    }

    private String getAllText(Element elementoCampo) {
        List content = elementoCampo.getContent();
        StringBuffer textContent = new StringBuffer();
        boolean hasText = false;
        for (int i = 0; i < content.size(); ++i) {
            String text;
            Object obj = content.get(i);
            if (obj instanceof Text) {
                text = ((Text)obj).getText();
                text = StringUtil.replace(text, "\u20ac", "<euro>eur</euro>");
                textContent.append(text);
                hasText = true;
                continue;
            }
            if (!(obj instanceof Element)) continue;
            text = ((Element)obj).getText();
            text = StringUtil.replace(text, "\u20ac", "&#8364;");
            textContent.append("<" + ((Element)obj).getName() + ">");
            textContent.append(text);
            textContent.append("</" + ((Element)obj).getName() + ">");
            hasText = true;
        }
        if (!hasText) {
            return "";
        }
        return textContent.toString();
    }

    private double searchInKeyWords(String textoAProcurar, String attributeValue) {
        StringTokenizer palavra = new StringTokenizer(attributeValue, ",");
        double similarity_aux = 0.0;
        while (palavra.hasMoreTokens()) {
            double similarity = 0.0;
            String word = palavra.nextToken();
            String[] toSearch = textoAProcurar.split(" ");
            for (int i = 0; i < toSearch.length; ++i) {
                similarity = StringUtil.similarity(word, toSearch[i]);
                double d = similarity_aux = similarity_aux < similarity ? similarity : similarity_aux;
                if (similarity < 0.75) continue;
                ++this.numResultados;
            }
        }
        if (similarity_aux >= 0.75) {
            return similarity_aux;
        }
        return -1.0;
    }

    private void ordenaResultado(List ajudas) {
        int lastIdx = ajudas.size() - 1;
        Element last = (Element)ajudas.get(lastIdx);
        if (last.getAttributeValue("id").equals(TaxclientParameters.FORM_ROOT_DISPLAY_NAME)) {
            ajudas.remove(lastIdx);
            ajudas.add(0, last);
        }
    }

    private static Element getChildByNameAndAttribute(List children, String attributeValue) {
        for (Element child : children) {
            if (!child.getAttributeValue("id").equals(attributeValue)) continue;
            return child;
        }
        return null;
    }

    private static Element createChildWithAttribute(Element parent, Element child) {
        Element newChild = new Element(child.getName());
        if (child.getAttributeValue("id") != null) {
            newChild.setAttribute("id", child.getAttributeValue("id"));
        }
        if (newChild.getName().equals("Quadro")) {
            Element aux = child.getChild("Quadro-titulo");
            newChild.addContent((Element)aux.clone());
        } else if (newChild.getName().equals("texto") || newChild.getName().equals("titulo") || newChild.getName().equals("link")) {
            newChild = (Element)child.clone();
        }
        parent.addContent(newChild);
        return newChild;
    }

    private static boolean hasChildWithAttribute(Element parent, Element child) {
        List children = parent.getChildren();
        for (Element elementChild : children) {
            if (!elementChild.getName().equals(child.getName()) || !elementChild.getAttributeValue("id").equals(child.getAttributeValue("id"))) continue;
            return true;
        }
        return false;
    }

    private List searchLimited(String pattern, String text) {
        String[] patternArr = pattern.split(" ");
        String[] textArr = text.split(" ");
        int patternLength = patternArr.length;
        int textLength = textArr.length;
        ArrayList<String> toReturn = new ArrayList<String>(2);
        double similarityLimitada = 0.0;
        String toReturnStr = "";
        String startedBold = "<b>";
        String endedBold = "</b>";
        String startedBu = "<bu>";
        String endedBu = "</bu>";
        String startedU = "<u>";
        String endedU = "</u>";
        String startedTabelaPaises = "<tabelaPaises>";
        String endedTabelaPaises = "</tabelaPaises>";
        String startedTabela = "<tabela>";
        String endedTabela = "</tabela>";
        String startedLinha = "<linha>";
        String endedLinha = "</linha>";
        String startedTituloTabela = "<tituloTabela>";
        String endedTituloTabela = "</tituloTabela>";
        String startedColunaTitulo = "<colunaTitulo>";
        String endedColunaTitulo = "</colunaTitulo>";
        String startedColuna = "<coluna>";
        String endedColuna = "</coluna>";
        String startedLista = "<lista>";
        String endedLista = "</lista>";
        String startedOlista = "<olista>";
        String endedOlista = "</olista>";
        String startedEfin = "<efin>";
        String endedEfin = "</efin>";
        int i = 0;
        int j = 0;
        int[] result = new int[textLength];
        block0 : while (i < textLength - patternLength) {
            double similarity;
            String word;
            String wordAux = word = textArr[i];
            if (word.indexOf("<H>") == 0) {
                wordAux = word.substring(word.indexOf("<H>") + 3, word.indexOf("</H>"));
            }
            if ((similarity = StringUtil.similarity(patternArr[0], wordAux)) >= 0.75) {
                int iaux = i;
                for (j = 1; j < patternLength; ++j) {
                    wordAux = word = textArr[iaux + 1];
                    if (word.indexOf("<H>") == 0) {
                        wordAux = word.substring(word.indexOf("<H>") + 3, word.indexOf("</H>"));
                    }
                    if ((similarity = StringUtil.similarity(patternArr[j], word)) >= 0.75) {
                        result[iaux] = 1;
                        result[iaux + 1] = 1;
                        ++i;
                        ++this.numResultadosLimited;
                        similarityLimitada = 1.0;
                        continue;
                    }
                    wordAux = word = textArr[iaux + 2];
                    if (word.indexOf("<H>") == 0) {
                        wordAux = word.substring(word.indexOf("<H>") + 3, word.indexOf("</H>"));
                    }
                    if ((similarity = StringUtil.similarity(patternArr[j], word)) >= 0.75) {
                        result[iaux] = 1;
                        result[iaux + 2] = 1;
                        i+=2;
                        ++this.numResultadosLimited;
                        similarityLimitada = 1.0;
                        continue;
                    }
                    ++i;
                    continue block0;
                }
                continue;
            }
            ++i;
        }
        for (int k = 0; k < result.length; ++k) {
            if (result[k] == 1) {
                if (textArr[k].indexOf(startedBold) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedBold)) + " " + textArr[k].substring(textArr[k].indexOf(startedBold) + startedBold.length()) + "</H> " + startedBold;
                    continue;
                }
                if (textArr[k].indexOf(endedBold) != -1) {
                    toReturnStr = toReturnStr + endedBold + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedBold)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedBu) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedBu)) + " " + textArr[k].substring(textArr[k].indexOf(startedBu) + startedBu.length()) + "</H> " + startedBu;
                    continue;
                }
                if (textArr[k].indexOf(endedBu) != -1) {
                    toReturnStr = toReturnStr + endedBu + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedBu)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedU) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedU)) + " " + textArr[k].substring(textArr[k].indexOf(startedU) + startedU.length()) + "</H> " + startedU;
                    continue;
                }
                if (textArr[k].indexOf(endedU) != -1) {
                    toReturnStr = toReturnStr + endedU + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedU)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedTabelaPaises) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedTabelaPaises)) + " " + textArr[k].substring(textArr[k].indexOf(startedTabelaPaises) + startedTabelaPaises.length()) + "</H> " + startedTabelaPaises;
                    continue;
                }
                if (textArr[k].indexOf(endedTabelaPaises) != -1) {
                    toReturnStr = toReturnStr + endedTabelaPaises + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedTabelaPaises)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedTabela) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedTabela)) + " " + textArr[k].substring(textArr[k].indexOf(startedTabela) + startedTabela.length()) + "</H> " + startedTabela;
                    continue;
                }
                if (textArr[k].indexOf(endedTabela) != -1) {
                    toReturnStr = toReturnStr + endedTabela + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedTabela)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedLinha) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedLinha)) + " " + textArr[k].substring(textArr[k].indexOf(startedLinha) + startedLinha.length()) + "</H> " + startedLinha;
                    continue;
                }
                if (textArr[k].indexOf(endedLinha) != -1) {
                    toReturnStr = toReturnStr + endedLinha + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedLinha)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedTituloTabela) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedTituloTabela)) + " " + textArr[k].substring(textArr[k].indexOf(startedTituloTabela) + startedTituloTabela.length()) + "</H> " + startedTituloTabela;
                    continue;
                }
                if (textArr[k].indexOf(endedTituloTabela) != -1) {
                    toReturnStr = toReturnStr + endedTituloTabela + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedTituloTabela)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedColunaTitulo) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedColunaTitulo)) + " " + textArr[k].substring(textArr[k].indexOf(startedColunaTitulo) + startedColunaTitulo.length()) + "</H> " + startedColunaTitulo;
                    continue;
                }
                if (textArr[k].indexOf(endedColunaTitulo) != -1) {
                    toReturnStr = toReturnStr + endedColunaTitulo + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedColunaTitulo)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedColuna) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedColuna)) + " " + textArr[k].substring(textArr[k].indexOf(startedColuna) + startedColuna.length()) + "</H> " + startedColuna;
                    continue;
                }
                if (textArr[k].indexOf(endedColuna) != -1) {
                    toReturnStr = toReturnStr + endedColuna + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedColuna)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedLista) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedLista)) + " " + textArr[k].substring(textArr[k].indexOf(startedLista) + startedLista.length()) + "</H> " + startedLista;
                    continue;
                }
                if (textArr[k].indexOf(endedLista) != -1) {
                    toReturnStr = toReturnStr + endedLista + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedLista)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedOlista) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedOlista)) + " " + textArr[k].substring(textArr[k].indexOf(startedOlista) + startedOlista.length()) + "</H> " + startedOlista;
                    continue;
                }
                if (textArr[k].indexOf(endedOlista) != -1) {
                    toReturnStr = toReturnStr + endedOlista + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedOlista)) + "</H>";
                    continue;
                }
                if (textArr[k].indexOf(startedEfin) != -1) {
                    toReturnStr = toReturnStr + "<H>" + textArr[k].substring(0, textArr[k].indexOf(startedEfin)) + " " + textArr[k].substring(textArr[k].indexOf(startedEfin) + startedEfin.length()) + "</H> " + startedEfin;
                    continue;
                }
                if (textArr[k].indexOf(endedEfin) != -1) {
                    toReturnStr = toReturnStr + endedEfin + " <H>" + textArr[k].substring(0, textArr[k].indexOf(endedEfin)) + "</H>";
                    continue;
                }
                toReturnStr = toReturnStr + "<H>" + textArr[k] + "</H> ";
                continue;
            }
            toReturnStr = toReturnStr + textArr[k] + " ";
        }
        toReturn.add(0, Double.toString(similarityLimitada));
        toReturn.add(1, toReturnStr);
        return toReturn;
    }

    private List searchLimitedText(String textoAProcurar, String textoOndeProcurar) throws IOException {
        Object[] textToSearch = this.getInputToSearch(textoAProcurar);
        List pesquisaLimitada = (List)textToSearch[0];
        double similarityLimitada = 0.0;
        ArrayList<String> listToReturn = new ArrayList<String>(2);
        for (String strLimited : pesquisaLimitada) {
            List result = this.searchLimited(strLimited, textoOndeProcurar);
            textoOndeProcurar = (String)result.get(1);
            similarityLimitada = similarityLimitada > Double.parseDouble((String)result.get(0)) ? similarityLimitada : Double.parseDouble((String)result.get(0));
        }
        listToReturn.add(0, Double.toString(similarityLimitada));
        listToReturn.add(1, textoOndeProcurar);
        return listToReturn;
    }

    private List searchText(String textoAProcurar, String textoOndeProcurar) throws IOException {
        StringTokenizer palavra = new StringTokenizer(textoOndeProcurar);
        Object[] textToSearch = this.getInputToSearch(textoAProcurar);
        String pesquisaNormal = (String)textToSearch[1];
        String newText = "";
        ArrayList<String> listToReturn = new ArrayList<String>(2);
        double similarityReturn = 0.0;
        String startedBold = "<b>";
        String endedBold = "</b>";
        String startedBu = "<bu>";
        String endedBu = "</bu>";
        String startedU = "<u>";
        String endedU = "</u>";
        String startedTabelaPaises = "<tabelaPaises>";
        String endedTabelaPaises = "</tabelaPaises>";
        String startedTabela = "<tabela>";
        String endedTabela = "</tabela>";
        String startedLinha = "<linha>";
        String endedLinha = "</linha>";
        String startedTituloTabela = "<tituloTabela>";
        String endedTituloTabela = "</tituloTabela>";
        String startedColunaTitulo = "<colunaTitulo>";
        String endedColunaTitulo = "</colunaTitulo>";
        String startedColuna = "<coluna>";
        String endedColuna = "</coluna>";
        String startedLista = "<lista>";
        String endedLista = "</lista>";
        String startedOlista = "<olista>";
        String endedOlista = "</olista>";
        String startedEfin = "<efin>";
        String endedEfin = "</efin>";
        while (palavra.hasMoreTokens()) {
            double similarityNormal = 0.0;
            String word = palavra.nextToken();
            String[] toSearch = pesquisaNormal.split(" ");
            for (int i = 0; i < toSearch.length; ++i) {
                double sim2;
                double sim1;
                double similarity = 0.0;
                similarity = word.indexOf(startedBold) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedBold)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedBold) + startedBold.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedBold) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedBold)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedBold) + endedBold.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedBu) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedBu)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedBu) + startedBu.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedBu) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedBu)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedBu) + endedBu.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedU) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedU)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedU) + startedU.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedU) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedU)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedU) + endedU.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedTabelaPaises) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedTabelaPaises)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedTabelaPaises) + startedTabelaPaises.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedTabelaPaises) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedTabelaPaises)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedTabelaPaises) + endedTabelaPaises.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedTabela) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedTabela)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedTabela) + startedTabela.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedTabela) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedTabela)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedTabela) + endedTabela.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedLinha) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedLinha)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedLinha) + startedLinha.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedLinha) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedLinha)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedLinha) + endedLinha.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedTituloTabela) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedTituloTabela)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedTituloTabela) + startedTituloTabela.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedTituloTabela) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedTituloTabela)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedTituloTabela) + endedTituloTabela.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedColunaTitulo) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedColunaTitulo)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedColunaTitulo) + startedColunaTitulo.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedColunaTitulo) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedColunaTitulo)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedColunaTitulo) + endedColunaTitulo.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedColuna) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedColuna)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedColuna) + startedColuna.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedColuna) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedColuna)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedColuna) + endedColuna.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedLista) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedLista)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedLista) + startedLista.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedLista) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedLista)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedLista) + endedLista.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedOlista) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedOlista)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedOlista) + startedOlista.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedOlista) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedOlista)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedOlista) + endedOlista.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(startedEfin) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(startedEfin)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(startedEfin) + startedEfin.length()), toSearch[i])) ? sim1 : sim2) : (word.indexOf(endedEfin) != -1 ? ((sim1 = StringUtil.similarity(word.substring(0, word.indexOf(endedEfin)), toSearch[i])) > (sim2 = StringUtil.similarity(word.substring(word.indexOf(endedEfin) + endedEfin.length()), toSearch[i])) ? sim1 : sim2) : StringUtil.similarity(word, toSearch[i]))))))))))))))))))))))));
                if (similarity < 0.75) continue;
                similarityNormal = similarityNormal < similarity ? similarity : similarityNormal;
                ++this.numResultados;
            }
            if (similarityNormal >= 0.75) {
                newText = word.indexOf(startedBold) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedBold)) + " " + word.substring(word.indexOf(startedBold) + startedBold.length()) + "</H> " + startedBold : (word.indexOf(endedBold) != -1 ? newText + endedBold + " <H>" + word.substring(0, word.indexOf(endedBold)) + "</H>" : (word.indexOf(startedBu) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedBu)) + " " + word.substring(word.indexOf(startedBu) + startedBu.length()) + "</H> " + startedBu : (word.indexOf(endedBu) != -1 ? newText + endedBu + " <H>" + word.substring(0, word.indexOf(endedBu)) + "</H>" : (word.indexOf(startedU) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedU)) + " " + word.substring(word.indexOf(startedU) + startedU.length()) + "</H> " + startedU : (word.indexOf(endedU) != -1 ? newText + endedU + " <H>" + word.substring(0, word.indexOf(endedU)) + "</H>" : (word.indexOf(startedTabelaPaises) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedTabelaPaises)) + " " + word.substring(word.indexOf(startedTabelaPaises) + startedTabelaPaises.length()) + "</H> " + startedTabelaPaises : (word.indexOf(endedTabelaPaises) != -1 ? newText + endedTabelaPaises + " <H>" + word.substring(0, word.indexOf(endedTabelaPaises)) + "</H>" : (word.indexOf(startedTabela) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedTabela)) + " " + word.substring(word.indexOf(startedTabela) + startedTabela.length()) + "</H> " + startedTabela : (word.indexOf(endedTabela) != -1 ? newText + endedTabela + " <H>" + word.substring(0, word.indexOf(endedTabela)) + "</H>" : (word.indexOf(startedLinha) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedLinha)) + " " + word.substring(word.indexOf(startedLinha) + startedLinha.length()) + "</H> " + startedLinha : (word.indexOf(endedLinha) != -1 ? newText + endedLinha + " <H>" + word.substring(0, word.indexOf(endedLinha)) + "</H>" : (word.indexOf(startedTituloTabela) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedTituloTabela)) + " " + word.substring(word.indexOf(startedTituloTabela) + startedTituloTabela.length()) + "</H> " + startedTituloTabela : (word.indexOf(endedTituloTabela) != -1 ? newText + endedTituloTabela + " <H>" + word.substring(0, word.indexOf(endedTituloTabela)) + "</H>" : (word.indexOf(startedColunaTitulo) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedColunaTitulo)) + " " + word.substring(word.indexOf(startedColunaTitulo) + startedColunaTitulo.length()) + "</H> " + startedColunaTitulo : (word.indexOf(endedColunaTitulo) != -1 ? newText + endedColunaTitulo + " <H>" + word.substring(0, word.indexOf(endedColunaTitulo)) + "</H>" : (word.indexOf(startedColuna) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedColuna)) + " " + word.substring(word.indexOf(startedColuna) + startedColuna.length()) + "</H> " + startedColuna : (word.indexOf(endedColuna) != -1 ? newText + endedColuna + " <H>" + word.substring(0, word.indexOf(endedColuna)) + "</H>" : (word.indexOf(startedLista) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedLista)) + " " + word.substring(word.indexOf(startedLista) + startedLista.length()) + "</H> " + startedLista : (word.indexOf(endedLista) != -1 ? newText + endedLista + " <H>" + word.substring(0, word.indexOf(endedLista)) + "</H>" : (word.indexOf(startedOlista) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedOlista)) + " " + word.substring(word.indexOf(startedOlista) + startedOlista.length()) + "</H> " + startedOlista : (word.indexOf(endedOlista) != -1 ? newText + endedOlista + " <H>" + word.substring(0, word.indexOf(endedOlista)) + "</H>" : (word.indexOf(startedEfin) != -1 ? newText + "<H>" + word.substring(0, word.indexOf(startedEfin)) + " " + word.substring(word.indexOf(startedEfin) + startedEfin.length()) + "</H> " + startedEfin : (word.indexOf(endedEfin) != -1 ? newText + endedEfin + " <H>" + word.substring(0, word.indexOf(endedEfin)) + "</H>" : newText + "<H>" + word + "</H> ")))))))))))))))))))))));
                similarityReturn = 1.0;
                continue;
            }
            newText = newText + word + " ";
        }
        listToReturn.add(0, Double.toString(similarityReturn));
        listToReturn.add(1, newText);
        return listToReturn;
    }

    private Element highlight(Element element) {
        String textToHighlight = element.getText();
        element.setText("");
        String startedBold = "<b>";
        String startedHighlight = "<H>";
        String endedHighlight = "</H>";
        String endedBold = "</b>";
        String startedBu = "<bu>";
        String endedBu = "</bu>";
        String startedU = "<u>";
        String endedU = "</u>";
        String startedTabelaPaises = "<tabelaPaises>";
        String endedTabelaPaises = "</tabelaPaises>";
        String startedTabela = "<tabela>";
        String endedTabela = "</tabela>";
        String startedLinha = "<linha>";
        String endedLinha = "</linha>";
        String startedTituloTabela = "<tituloTabela>";
        String endedTituloTabela = "</tituloTabela>";
        String startedColunaTitulo = "<colunaTitulo>";
        String endedColunaTitulo = "</colunaTitulo>";
        String startedColuna = "<coluna>";
        String endedColuna = "</coluna>";
        String startedLista = "<lista>";
        String endedLista = "</lista>";
        String startedOlista = "<olista>";
        String endedOlista = "</olista>";
        String startedEfin = "<efin>";
        String endedEfin = "</efin>";
        StringTokenizer palavra = new StringTokenizer(textToHighlight);
        Element bu = new Element("bu");
        Element bold = new Element("b");
        Element u = new Element("u");
        Element tabelaPaises = new Element("tabelaPaises");
        Element tabela = new Element("tabela");
        Element linha = new Element("linha");
        Element tituloTabela = new Element("tituloTabela");
        Element colunaTitulo = new Element("colunaTitulo");
        Element coluna = new Element("coluna");
        Element lista = new Element("lista");
        Element olista = new Element("olista");
        Element efin = new Element("efin");
        Element highlight = new Element("highlight");
        while (palavra.hasMoreTokens()) {
            String word = palavra.nextToken();
            int indexBu = word.indexOf(startedBu);
            int indexHighlight = word.indexOf(startedHighlight);
            int indexEndHighlight = word.indexOf(endedHighlight);
            int indexEndBu = word.indexOf(endedBu);
            int indexBold = word.indexOf(startedBold);
            int indexEndBold = word.indexOf(endedBold);
            int indexU = word.indexOf(startedU);
            int indexEndU = word.indexOf(endedU);
            int indexTabelaPaises = word.indexOf(startedTabelaPaises);
            int indexEndTabelaPaises = word.indexOf(endedTabelaPaises);
            int indexTabela = word.indexOf(startedTabela);
            int indexEndTabela = word.indexOf(endedTabela);
            int indexLinha = word.indexOf(startedLinha);
            int indexEndLinha = word.indexOf(endedLinha);
            int indexTituloTabela = word.indexOf(startedTituloTabela);
            int indexEndTituloTabela = word.indexOf(endedTituloTabela);
            int indexColunaTitulo = word.indexOf(startedColunaTitulo);
            int indexEndColunaTitulo = word.indexOf(endedColunaTitulo);
            int indexColuna = word.indexOf(startedColuna);
            int indexEndColuna = word.indexOf(endedColuna);
            int indexLista = word.indexOf(startedLista);
            int indexEndLista = word.indexOf(endedLista);
            int indexOlista = word.indexOf(startedOlista);
            int indexEndOlista = word.indexOf(endedOlista);
            int indexEfin = word.indexOf(startedEfin);
            int indexEndEfin = word.indexOf(endedEfin);
            if (indexBu == -1 && indexBold == -1 && indexU == -1 && indexTabelaPaises == -1 && indexTabela == -1 && indexLinha == -1 && indexTituloTabela == -1 && indexColunaTitulo == -1 && indexColuna == -1 && indexLista == -1 && indexOlista == -1 && indexEfin == -1 && indexHighlight == -1) {
                if (indexEndHighlight != -1) {
                    highlight.addContent(word.substring(0, indexEndHighlight) + " ");
                    element.addContent(highlight);
                    highlight = new Element("highlight");
                    continue;
                }
                if (indexEndBu != -1) {
                    bu.addContent(word.substring(0, indexEndBu) + " ");
                    element.addContent(bu);
                    bu = new Element("bu");
                    continue;
                }
                if (indexEndBold != -1) {
                    bold.addContent(word.substring(0, indexEndBold) + " ");
                    element.addContent(bold);
                    bold = new Element("b");
                    continue;
                }
                if (indexEndU != -1) {
                    u.addContent(word.substring(0, indexEndU) + " ");
                    element.addContent(u);
                    u = new Element("u");
                    continue;
                }
                if (indexEndTabelaPaises != -1) {
                    tabelaPaises.addContent(word.substring(0, indexEndTabelaPaises) + " ");
                    element.addContent(tabelaPaises);
                    tabelaPaises = new Element("tabelaPaises");
                    continue;
                }
                if (indexEndTabela != -1) {
                    tabela.addContent(word.substring(0, indexEndTabela) + " ");
                    element.addContent(tabela);
                    tabela = new Element("tabela");
                    continue;
                }
                if (indexEndLinha != -1) {
                    linha.addContent(word.substring(0, indexEndLinha) + " ");
                    element.addContent(linha);
                    linha = new Element("linha");
                    continue;
                }
                if (indexEndTituloTabela != -1) {
                    tituloTabela.addContent(word.substring(0, indexEndTituloTabela) + " ");
                    element.addContent(tituloTabela);
                    tituloTabela = new Element("tituloTabela");
                    continue;
                }
                if (indexEndColunaTitulo != -1) {
                    colunaTitulo.addContent(word.substring(0, indexEndColunaTitulo) + " ");
                    element.addContent(colunaTitulo);
                    colunaTitulo = new Element("colunaTitulo");
                    continue;
                }
                if (indexEndColuna != -1) {
                    coluna.addContent(word.substring(0, indexEndColuna) + " ");
                    element.addContent(coluna);
                    coluna = new Element("coluna");
                    continue;
                }
                if (indexEndLista != -1) {
                    lista.addContent(word.substring(0, indexEndLista) + " ");
                    element.addContent(lista);
                    lista = new Element("lista");
                    continue;
                }
                if (indexEndOlista != -1) {
                    olista.addContent(word.substring(0, indexEndOlista) + " ");
                    element.addContent(olista);
                    olista = new Element("olista");
                    continue;
                }
                if (indexEndEfin != -1) {
                    efin.addContent(word.substring(0, indexEndEfin) + " ");
                    element.addContent(efin);
                    efin = new Element("efin");
                    continue;
                }
                if (highlight.getText().length() > 0) {
                    highlight.addContent(word + " ");
                    continue;
                }
                if (bu.getText().length() > 0) {
                    bu.addContent(word + " ");
                    continue;
                }
                if (bold.getText().length() > 0) {
                    bold.addContent(word + " ");
                    continue;
                }
                if (u.getText().length() > 0) {
                    u.addContent(word + " ");
                    continue;
                }
                if (tabelaPaises.getText().length() > 0) {
                    tabelaPaises.addContent(word + " ");
                    continue;
                }
                if (tabela.getText().length() > 0) {
                    tabela.addContent(word + " ");
                    continue;
                }
                if (linha.getText().length() > 0) {
                    linha.addContent(word + " ");
                    continue;
                }
                if (tituloTabela.getText().length() > 0) {
                    tituloTabela.addContent(word + " ");
                    continue;
                }
                if (colunaTitulo.getText().length() > 0) {
                    colunaTitulo.addContent(word + " ");
                    continue;
                }
                if (coluna.getText().length() > 0) {
                    coluna.addContent(word + " ");
                    continue;
                }
                if (lista.getText().length() > 0) {
                    lista.addContent(word + " ");
                    continue;
                }
                if (olista.getText().length() > 0) {
                    olista.addContent(word + " ");
                    continue;
                }
                if (efin.getText().length() > 0) {
                    efin.addContent(word + " ");
                    continue;
                }
                element.addContent(word + " ");
                continue;
            }
            if (indexHighlight != -1) {
                if (indexEndHighlight != -1) {
                    highlight.addContent(word.substring(indexHighlight + startedHighlight.length(), indexEndHighlight) + " ");
                    element.addContent(highlight);
                    highlight = new Element("highlight");
                    continue;
                }
                bu.addContent(word.substring(indexBu + startedBu.length()) + " ");
                continue;
            }
            if (indexBu != -1) {
                if (indexEndBu != -1) {
                    bu.addContent(word.substring(indexBu + startedBu.length(), indexEndBu) + " ");
                    element.addContent(bu);
                    bu = new Element("bu");
                    continue;
                }
                bu.addContent(word.substring(indexBu + startedBu.length()) + " ");
                continue;
            }
            if (indexBold != -1) {
                if (indexEndBold != -1) {
                    bold.addContent(word.substring(indexBold + startedBold.length(), indexEndBold) + " ");
                    element.addContent(bold);
                    bold = new Element("b");
                    continue;
                }
                bold.addContent(word.substring(indexBold + startedBold.length()) + " ");
                continue;
            }
            if (indexU != -1) {
                if (indexEndU != -1) {
                    u.addContent(word.substring(indexU + startedU.length(), indexEndU) + " ");
                    element.addContent(u);
                    u = new Element("u");
                    continue;
                }
                u.addContent(word.substring(indexU + startedU.length()) + " ");
                continue;
            }
            if (indexTabelaPaises != -1) {
                if (indexEndTabelaPaises != -1) {
                    tabelaPaises.addContent(word.substring(indexTabelaPaises + startedTabelaPaises.length(), indexEndTabelaPaises) + " ");
                    element.addContent(tabelaPaises);
                    tabelaPaises = new Element("tabelaPaises");
                    continue;
                }
                tabelaPaises.addContent(word.substring(indexTabelaPaises + startedTabelaPaises.length()) + " ");
                continue;
            }
            if (indexTabela != -1) {
                if (indexEndTabela != -1) {
                    tabela.addContent(word.substring(indexTabela + startedTabela.length(), indexEndTabela) + " ");
                    element.addContent(tabela);
                    tabela = new Element("tabela");
                    continue;
                }
                tabela.addContent(word.substring(indexTabela + startedTabela.length()) + " ");
                continue;
            }
            if (indexLinha != -1) {
                if (indexEndLinha != -1) {
                    linha.addContent(word.substring(indexLinha + startedLinha.length(), indexEndLinha) + " ");
                    element.addContent(linha);
                    linha = new Element("linha");
                    continue;
                }
                linha.addContent(word.substring(indexLinha + startedLinha.length()) + " ");
                continue;
            }
            if (indexTituloTabela != -1) {
                if (indexEndTituloTabela != -1) {
                    tituloTabela.addContent(word.substring(indexTituloTabela + startedTituloTabela.length(), indexEndTituloTabela) + " ");
                    element.addContent(tituloTabela);
                    tituloTabela = new Element("tituloTabela");
                    continue;
                }
                tituloTabela.addContent(word.substring(indexTituloTabela + startedTituloTabela.length()) + " ");
                continue;
            }
            if (indexColunaTitulo != -1) {
                if (indexEndColunaTitulo != -1) {
                    colunaTitulo.addContent(word.substring(indexColunaTitulo + startedColunaTitulo.length(), indexEndColunaTitulo) + " ");
                    element.addContent(colunaTitulo);
                    colunaTitulo = new Element("colunaTitulo");
                    continue;
                }
                colunaTitulo.addContent(word.substring(indexColunaTitulo + startedColunaTitulo.length()) + " ");
                continue;
            }
            if (indexColuna != -1) {
                if (indexEndColuna != -1) {
                    coluna.addContent(word.substring(indexColuna + startedColuna.length(), indexEndColuna) + " ");
                    element.addContent(coluna);
                    coluna = new Element("coluna");
                    continue;
                }
                coluna.addContent(word.substring(indexColuna + startedColuna.length()) + " ");
                continue;
            }
            if (indexLista != -1) {
                if (indexEndLista != -1) {
                    lista.addContent(word.substring(indexLista + startedLista.length(), indexEndLista) + " ");
                    element.addContent(lista);
                    lista = new Element("lista");
                    continue;
                }
                lista.addContent(word.substring(indexLista + startedLista.length()) + " ");
                continue;
            }
            if (indexOlista != -1) {
                if (indexEndOlista != -1) {
                    olista.addContent(word.substring(indexOlista + startedOlista.length(), indexEndOlista) + " ");
                    element.addContent(olista);
                    olista = new Element("olista");
                    continue;
                }
                olista.addContent(word.substring(indexOlista + startedOlista.length()) + " ");
                continue;
            }
            if (indexEfin == -1) continue;
            if (indexEndEfin != -1) {
                efin.addContent(word.substring(indexEfin + startedEfin.length(), indexEndEfin) + " ");
                element.addContent(efin);
                efin = new Element("efin");
                continue;
            }
            efin.addContent(word.substring(indexEfin + startedEfin.length()) + " ");
        }
        return element;
    }

    public Document getResultado() {
        return this.resultado;
    }

    public void setResultado(Document resultado) {
        this.resultado = resultado;
    }

    public String getTextToSearch() {
        return this.textToSearch;
    }

    public void setTextToSearch(String textToSearch) {
        this.textToSearch = textToSearch;
    }

    public int getNumResultados() {
        return this.numResultados;
    }

    public void setNumResultados(int numResultados) {
        this.numResultados = numResultados;
    }

    private Object[] getInputToSearch(String toSearch) throws IOException {
        StringReader reader = new StringReader(toSearch);
        ArrayList<String> results = new ArrayList<String>();
        String toSearchLimited = "";
        String toSearchNormal = "";
        boolean hasStarted = false;
        int readChar = reader.read();
        while (readChar != -1) {
            if (Character.toString((char)readChar).equals("\"")) {
                if (!hasStarted) {
                    hasStarted = true;
                    toSearchLimited = "";
                } else {
                    results.add(toSearchLimited);
                    hasStarted = false;
                }
            } else {
                toSearchLimited = toSearchLimited + (char)readChar;
                if (!hasStarted) {
                    toSearchNormal = toSearchNormal + (char)readChar;
                }
            }
            readChar = reader.read();
        }
        return new Object[]{results, toSearchNormal};
    }
}

