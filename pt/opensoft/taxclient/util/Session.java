/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import com.jgoodies.binding.value.ValueHolder;
import java.util.HashMap;
import java.util.Map;
import pt.opensoft.taxclient.TaxClientRevampedApplet;
import pt.opensoft.taxclient.TaxClientRevampedApplication;
import pt.opensoft.taxclient.model.DeclaracaoModelValidator;
import pt.opensoft.taxclient.model.DeclaracaoPresentationModel;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.exception.EventExceptionsHandler;

public class Session {
    private static String applicationName = null;
    private static MainFrame mainFrame = null;
    private static DeclaracaoPresentationModel currentDeclaracao = null;
    private static DeclaracaoModelValidator declaracaoValidator = null;
    private static ValueHolder isEditable = new ValueHolder(true);
    private static ValueHolder isComboBoxEditable = new ValueHolder(false);
    private static TaxClientRevampedApplet applet;
    private static TaxClientRevampedApplication applicationFrame;
    protected static EventExceptionsHandler exceptionHandler;
    private static Map<String, String> parameters;

    public static TaxClientRevampedApplication getApplicationFrame() {
        return applicationFrame;
    }

    public static void setApplicationFrame(TaxClientRevampedApplication applicationFrame) {
        Session.applicationFrame = applicationFrame;
    }

    public static EventExceptionsHandler getExceptionHandler() {
        if (exceptionHandler == null) {
            exceptionHandler = new EventExceptionsHandler();
        }
        return exceptionHandler;
    }

    public static void setExceptionHandler(EventExceptionsHandler exceptionHandler) {
        Session.exceptionHandler = exceptionHandler;
    }

    public static DeclaracaoPresentationModel getCurrentDeclaracao() {
        return currentDeclaracao;
    }

    public static void setCurrentDeclaracao(DeclaracaoPresentationModel currentDeclaracao) {
        Session.currentDeclaracao = currentDeclaracao;
    }

    public static boolean hasCurrentDeclaracao() {
        return currentDeclaracao != null;
    }

    public static MainFrame getMainFrame() {
        return mainFrame;
    }

    public static void setMainFrame(MainFrame mainFrame) {
        Session.mainFrame = mainFrame;
    }

    public static boolean hasCurrentMainFrame() {
        return mainFrame != null;
    }

    public static DeclaracaoModelValidator getDeclaracaoValidator() {
        return declaracaoValidator;
    }

    public static void setDeclaracaoValidator(DeclaracaoModelValidator declaracaoValidator) {
        Session.declaracaoValidator = declaracaoValidator;
    }

    public static String getApplicationName() {
        return applicationName;
    }

    public static void setApplicationName(String applicationName) {
        Session.applicationName = applicationName;
    }

    public static void setEditable(boolean isEditable) {
        Session.isEditable.setValue(isEditable);
    }

    public static ValueHolder isEditable() {
        return isEditable;
    }

    public static void setComboBoxEditable(boolean isEditable) {
        isComboBoxEditable.setValue(isEditable);
    }

    public static ValueHolder isComboBoxEditable() {
        return isComboBoxEditable;
    }

    public static String getParameter(String parameter) {
        if (applet != null && applet.getParameter(parameter) != null) {
            return applet.getParameter(parameter);
        }
        if (parameters == null) {
            return null;
        }
        return parameters.get(parameter);
    }

    public static void setParameter(String parameter, String value) {
        if (applet != null && applet.getParameter(parameter) != null) {
            throw new RuntimeException("Cannot set applet parameters!");
        }
        if (parameters == null) {
            parameters = new HashMap<String, String>();
        }
        parameters.put(parameter, value);
    }

    public static void setApplet(TaxClientRevampedApplet applet) {
        Session.applet = applet;
    }

    public static TaxClientRevampedApplet getApplet() {
        return applet;
    }

    public static boolean isApplet() {
        return applet != null;
    }
}

