/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.Iterator;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.IconFactory;
import pt.opensoft.taxclient.gui.DesignationManager;
import pt.opensoft.taxclient.util.Environment;
import pt.opensoft.util.StringUtil;

public class SubmitTaxUtils {
    private static final int LINE_LENGHT = 120;
    private static final String MESSAGE_BREAK_LINE = "<br>";
    public static final String DEFAULT_CONNECTION_TIMEOUT_MESSAGE = "<html><b>Ocorreu um erro na submiss\u00e3o da Declara\u00e7\u00e3o (Expirou o tempo de Comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>";
    public static final String DEFAULT_IO_EXCEPTION_MESSAGE = "<html><b>Ocorreu um erro na submiss\u00e3o da Declara\u00e7\u00e3o (Problemas de comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>";
    public static final String DEFAULT_EXCEPTION_MESSAGE = "<html><b>Ocorreu um erro na submiss\u00e3o da Declara\u00e7\u00e3o.</b><br>Por favor, tente novamente.</html>";
    private static final String DEFAULT_TAX_NOT_SUBMITED = "A declara\u00e7\u00e3o <b>n\u00e3o</b> foi submetida.";
    public static final String CONNECTION_TIMEOUT_MESSAGE = DesignationManager.getInstance().getString("connection.timeout.message", "<html><b>Ocorreu um erro na submiss\u00e3o da Declara\u00e7\u00e3o (Expirou o tempo de Comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>");
    public static final String IO_EXCEPTION_MESSAGE = DesignationManager.getInstance().getString("io.exception.message", "<html><b>Ocorreu um erro na submiss\u00e3o da Declara\u00e7\u00e3o (Problemas de comunica\u00e7\u00e3o).</b><br>Verifique se necessita de definir as propriedades de Proxy (no menu Ficheiro > Configurar Proxy HTTP)<br>Por favor, tente novamente.</html>");
    public static final String EXCEPTION_MESSAGE = DesignationManager.getInstance().getString("exception.message", "<html><b>Ocorreu um erro na submiss\u00e3o da Declara\u00e7\u00e3o.</b><br>Por favor, tente novamente.</html>");
    private static final String TAX_NOT_SUBMITED = DesignationManager.getInstance().getString("tax.not.submited", "A declara\u00e7\u00e3o <b>n\u00e3o</b> foi submetida.");
    private static final String DEFAULT_CONFIRM_SUBMISSION_MESSAGE = "<html>Este formul\u00e1rio vai substituir qualquer formul\u00e1rio previamente entregue. Deseja continuar?</html>";
    public static final String CONFIRM_SUBMISSION_MESSAGE = DesignationManager.getInstance().getString("confirm.submission.message", "<html>Este formul\u00e1rio vai substituir qualquer formul\u00e1rio previamente entregue. Deseja continuar?</html>");

    public static JPanel constructErrorsPanel(String msgTitle, List errors) {
        JPanel panel = new JPanel(new BorderLayout(5, 15));
        panel.add((Component)SubmitTaxUtils.htmlJLabel(msgTitle, true), "North");
        if (errors != null) {
            Iterator iter = errors.iterator();
            while (iter.hasNext()) {
                panel.add((Component)SubmitTaxUtils.htmlJLabel("&nbsp;&nbsp;&nbsp;-&nbsp;" + iter.next().toString(), false), "Center");
            }
        }
        panel.add((Component)SubmitTaxUtils.htmlJLabel(TAX_NOT_SUBMITED, false), "South");
        return panel;
    }

    public static JPanel constructErrorsPanel(String header, String message, String footer) {
        JPanel panel = new JPanel(new BorderLayout(5, 15));
        panel.add((Component)SubmitTaxUtils.htmlJLabel(header, true), "North");
        panel.add((Component)SubmitTaxUtils.htmlJLabel(message, false), "Center");
        panel.add((Component)SubmitTaxUtils.htmlJLabel(footer, false), "South");
        return panel;
    }

    public static JPanel constructInformationsPanel(String msgTitle, List informations) {
        JPanel panel = new JPanel(new BorderLayout(5, 15));
        panel.add((Component)SubmitTaxUtils.htmlJLabel(msgTitle, true), "North");
        JPanel informationPanel = new JPanel();
        informationPanel.setLayout(new BoxLayout(informationPanel, 1));
        if (informations != null) {
            Iterator iter = informations.iterator();
            while (iter.hasNext()) {
                informationPanel.add(SubmitTaxUtils.htmlJLabel(iter.next().toString(), false));
            }
        }
        panel.add((Component)informationPanel, "Center");
        return panel;
    }

    public static String breakSubmitTaxMessages(String message, int lineLength) {
        return StringUtil.breakMessage(message, lineLength, "<br>");
    }

    public static JDialog submitDialog(JLabel descriptiveLabel) {
        return SubmitTaxUtils.submitDialog(descriptiveLabel, true);
    }

    public static JDialog submitDialog(JLabel descriptiveLabel, boolean isProduction) {
        return SubmitTaxUtils.submitDialog(descriptiveLabel, isProduction ? Environment.PRD : Environment.DEV);
    }

    public static JDialog submitDialog(JLabel descriptiveLabel, Environment env) {
        JPanel panel = new JPanel(new GridLayout(2, 1, 0, 20));
        panel.add(new JLabel(IconFactory.getIconBig(GUIParameters.ICON_SUBMETER_ANIMACAO)));
        descriptiveLabel.setText("Comunicando com o servidor...");
        descriptiveLabel.setHorizontalAlignment(0);
        panel.add(descriptiveLabel);
        JOptionPane optionPane = new JOptionPane(panel, -1, 0, null, new Object[0], null);
        JDialog dialog = optionPane.createDialog(DialogFactory.getDummyFrame(), "");
        String title = env.toString().equals(Environment.PRD.toString()) ? "Em Submiss\u00e3o ..." : (env.toString().equals(Environment.QUA.toString()) ? "Em Submiss\u00e3o (qualidade) ..." : "Em Submiss\u00e3o (testes) ...");
        dialog.setTitle(title);
        dialog.setModal(false);
        dialog.setDefaultCloseOperation(0);
        return dialog;
    }

    public static JLabel htmlJLabel(String message, boolean bold, int lineLenght) {
        String startTag = "<html>";
        String endTag = "</html>";
        if (bold) {
            startTag = startTag + "<b>";
            endTag = "</b>" + endTag;
        }
        return new JLabel(startTag + SubmitTaxUtils.breakSubmitTaxMessages(message, lineLenght) + endTag);
    }

    public static JLabel htmlJLabel(String message, boolean bold) {
        return SubmitTaxUtils.htmlJLabel(message, bold, 120);
    }

    public static JLabel htmlJLabel(String message, boolean bold, int lineLenght, String color) {
        String startTag = "<html>";
        String endTag = "</html>";
        if (bold) {
            if (color != null) {
                startTag = startTag + "<b><font color=" + color + ">";
                endTag = "</font></b>" + endTag;
            } else {
                startTag = startTag + "<b>";
                endTag = "</b>" + endTag;
            }
        } else if (color != null) {
            startTag = startTag + "<font color=" + color + ">";
            endTag = "</font>" + endTag;
        }
        return new JLabel(startTag + SubmitTaxUtils.breakSubmitTaxMessages(message, lineLenght) + endTag);
    }

    public static JLabel htmlJLabel(String message, boolean bold, String color) {
        return SubmitTaxUtils.htmlJLabel(message, bold, 120, color);
    }
}

