/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  net.sf.saxon.tinytree.TinyNodeImpl
 */
package pt.opensoft.taxclient.util;

import com.jgoodies.binding.list.ArrayListModel;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.saxon.tinytree.TinyNodeImpl;
import pt.opensoft.logging.Logger;
import pt.opensoft.taxclient.util.BehavioursGeneratorUtil;
import pt.opensoft.taxclient.util.XsltToJavaTypesUtil;
import pt.opensoft.util.ClassUtil;
import pt.opensoft.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;

public class PropertyTransformationUtil {
    protected static Map<String, StringBuffer> bufferMap = new HashMap<String, StringBuffer>();
    protected static Map<String, List<String>> columnOperationsMap = new HashMap<String, List<String>>();
    protected static Map<String, List<String>> fieldNames = new HashMap<String, List<String>>();
    protected static String nomeAnexoCorrente;
    protected static String nomeQuadroCorrente;

    public static String getJavaTypeFromXSD(String xsdType) {
        if (xsdType == null) {
            return "";
        }
        if (xsdType.equalsIgnoreCase("xs:string")) {
            return ClassUtil.getClassName(String.class);
        }
        if (xsdType.equalsIgnoreCase("xs:int") || xsdType.equalsIgnoreCase("xs:integer")) {
            return ClassUtil.getClassName(Integer.class);
        }
        if (xsdType.equalsIgnoreCase("xs:long")) {
            return ClassUtil.getClassName(Long.class);
        }
        if (xsdType.equalsIgnoreCase("xs:boolean")) {
            return ClassUtil.getClassName(Boolean.class);
        }
        if (xsdType.equals("xs:date")) {
            return ClassUtil.getClassName(Date.class);
        }
        if (xsdType.equals("xs:dateTime")) {
            return ClassUtil.getClassName(DateTime.class);
        }
        if (xsdType.equalsIgnoreCase("xs:base64Binary")) {
            return ClassUtil.getClassName(String.class);
        }
        if (xsdType.equalsIgnoreCase("xs:decimal")) {
            return ClassUtil.getClassName(Long.class);
        }
        if (XsltToJavaTypesUtil.javaComplexTypes.containsKey(xsdType)) {
            return XsltToJavaTypesUtil.javaComplexTypes.get(xsdType);
        }
        return "";
    }

    public static String buildPackageDeclaration(String packagePath) {
        return "package " + packagePath + ";";
    }

    public static String standardJavaClassName(String className) {
        return StringUtil.capitalizeFirstCharacter(className).replace((CharSequence)"-", (CharSequence)"_");
    }

    public static String standardJavaElementName(String element) {
        String tempElement = element.replace((CharSequence)"-", (CharSequence)"_");
        String firstChar = tempElement.substring(0, 1).toLowerCase();
        return firstChar + tempElement.substring(1);
    }

    public static String buildGetterMethodName(String element) {
        String elementWithoutHifen = element.replace((CharSequence)"-", (CharSequence)"_");
        return "get" + StringUtil.capitalizeFirstCharacter(elementWithoutHifen);
    }

    public static String buildSetterMethodName(String element) {
        String elementWithoutHifen = element.replace((CharSequence)"-", (CharSequence)"_");
        return "set" + StringUtil.capitalizeFirstCharacter(elementWithoutHifen);
    }

    public static String buildGetter(String element, String xsdType, String appinfo) {
        if (element == null || xsdType == null) {
            throw new RuntimeException("Element " + element + " or  XsdType " + xsdType + " are null.");
        }
        String fraction = "";
        if (XsltToJavaTypesUtil.catalogs.contains(xsdType)) {
            fraction = fraction + "\n\t@Catalog(\"" + xsdType + "\")";
        }
        if (XsltToJavaTypesUtil.decimalValuesFracions.containsKey(xsdType)) {
            fraction = fraction + "\n\t@FractionDigits(" + XsltToJavaTypesUtil.decimalValuesFracions.get(xsdType) + ")";
        }
        String complexFieldHandler = "";
        if (XsltToJavaTypesUtil.complexFieldHandlers.containsKey(xsdType)) {
            complexFieldHandler = "\n\t@ComplexField(\"" + XsltToJavaTypesUtil.complexFieldHandlers.get(xsdType) + "\")";
        }
        return "\n\n\t" + appinfo + "\n\t@Type(Type.TYPE.CAMPO)" + fraction + complexFieldHandler + "\n\tpublic " + PropertyTransformationUtil.getJavaTypeFromXSD(xsdType) + " " + PropertyTransformationUtil.buildGetterMethodName(element) + "(){\n" + "\t\t" + "return this." + PropertyTransformationUtil.standardJavaElementName(element) + "; \n" + "\t" + "}";
    }

    public static String buildGetter(String element, String xsdType, String appinfo, boolean attribute) {
        if (element == null || xsdType == null) {
            throw new RuntimeException("Element " + element + " or  XsdType " + xsdType + " are null.");
        }
        String fraction = "";
        if (XsltToJavaTypesUtil.catalogs.contains(xsdType)) {
            fraction = fraction + "\n\t@Catalog(\"" + xsdType + "\")";
        }
        if (XsltToJavaTypesUtil.decimalValuesFracions.containsKey(xsdType)) {
            fraction = fraction + "\n\t@FractionDigits(" + XsltToJavaTypesUtil.decimalValuesFracions.get(xsdType) + ")";
        }
        String complexFieldHandler = "";
        if (XsltToJavaTypesUtil.complexFieldHandlers.containsKey(xsdType)) {
            complexFieldHandler = "\n\t@ComplexField(\"" + XsltToJavaTypesUtil.complexFieldHandlers.get(xsdType) + "\")";
        }
        String typeAnnotation = "";
        if (!attribute) {
            typeAnnotation = "\n\t@Type(Type.TYPE.CAMPO)";
        }
        return "\n\n\t" + appinfo + typeAnnotation + fraction + complexFieldHandler + "\n\tpublic " + PropertyTransformationUtil.getJavaTypeFromXSD(xsdType) + " " + PropertyTransformationUtil.buildGetterMethodName(element) + "(){\n" + "\t\t" + "return this." + PropertyTransformationUtil.standardJavaElementName(element) + "; \n" + "\t" + "}";
    }

    public static String buildFormula(String element, String xsdType, String formula) {
        if (StringUtil.isEmpty(formula)) {
            return "";
        }
        String method = "\n";
        String auxSetterName = PropertyTransformationUtil.buildSetterMethodName(element + "Formula");
        method = method + "\tpublic void " + auxSetterName + "(" + PropertyTransformationUtil.getJavaTypeFromXSD(xsdType) + " value){\n\t\t" + PropertyTransformationUtil.buildCheckFormula(PropertyTransformationUtil.getFileNameConstInClass(element)) + BehavioursGeneratorUtil.handleSumBehaviour(element, formula) + "}\n\n";
        return method;
    }

    private static String buildCheckFormula(String fieldId) {
        return String.format("if (!OverwriteBehaviorManager.getInstance(). isToDoFormula(%s))\n\t\t\treturn;\n\t\t", fieldId);
    }

    private static String getFileNameConstInClass(String element) {
        return PropertyTransformationUtil.standardJavaElementName(element).toUpperCase();
    }

    public static String buildGetter(String element, String xsdType) {
        if (element == null || xsdType == null) {
            throw new RuntimeException("Element " + element + " or  XsdType " + xsdType + " are null.");
        }
        return "\n\tpublic " + PropertyTransformationUtil.getJavaTypeFromXSD(xsdType) + " " + PropertyTransformationUtil.buildGetterMethodName(element) + "(){\n" + "\t\t" + "return this." + PropertyTransformationUtil.standardJavaElementName(element) + "; \n" + "\t" + "}";
    }

    public static String buildParameterGroupDeclaration(String panelName, String elementName, String parameterGroup) {
        if (StringUtil.isEmpty(parameterGroup)) {
            return "";
        }
        String elementNameModified = elementName + "_Attr";
        String propertyDeclaration = PropertyTransformationUtil.buildPropertyDeclaration(elementNameModified, parameterGroup);
        String getter = PropertyTransformationUtil.buildGetter(elementNameModified, parameterGroup, "", true);
        return "\n\n\t" + propertyDeclaration + "\n\n\t" + getter;
    }

    public static String buildSetter(String modelName, String element, String xsdType, boolean propertyChange) {
        if (element == null || xsdType == null) {
            throw new RuntimeException("Element " + element + " or  XsdType " + xsdType + " are null.");
        }
        String type = PropertyTransformationUtil.getJavaTypeFromXSD(xsdType);
        String javaName = PropertyTransformationUtil.standardJavaElementName(element);
        String savedOldValue = propertyChange ? "\t\t" + type + " oldValue = this." + javaName + "; \n" : "";
        String firePropertyChange = propertyChange ? "\t\tfirePropertyChange(" + PropertyTransformationUtil.standardJavaClassName(modelName) + "." + javaName.toUpperCase() + ", oldValue, this." + javaName + ");\n" : "";
        String fraction = "";
        if (XsltToJavaTypesUtil.decimalValuesFracions.containsKey(xsdType)) {
            fraction = "\n\t@FractionDigits(" + XsltToJavaTypesUtil.decimalValuesFracions.get(xsdType) + ")";
        }
        String complexFieldHandler = "";
        if (XsltToJavaTypesUtil.complexFieldHandlers.containsKey(xsdType)) {
            complexFieldHandler = "\n\t@ComplexField(\"" + XsltToJavaTypesUtil.complexFieldHandlers.get(xsdType) + "\")";
        }
        String setter = "\n\t@Type(Type.TYPE.CAMPO)" + fraction + complexFieldHandler + "\n\tpublic void " + PropertyTransformationUtil.buildSetterMethodName(element) + "(" + type + " " + javaName + ") { \n" + savedOldValue + "\t\t" + "this." + javaName + "=" + javaName + ";\n" + firePropertyChange + "\t" + "}";
        return setter;
    }

    public static String buildConstructorWithFields(String className, String fieldList, String typeList) {
        List listOfFields = ListUtil.toList(fieldList, " ");
        List listOfTypes = ListUtil.toList(typeList, " ");
        return PropertyTransformationUtil.buildConstructorWithFieldsByLists(className, listOfFields, listOfTypes);
    }

    public static String buildConstructorWithFieldsSuper(String className, String fieldList, String typeList) {
        List listOfFields = ListUtil.toList(fieldList, " ");
        List listOfTypes = ListUtil.toList(typeList, " ");
        return PropertyTransformationUtil.buildConstructorWithFieldsByListsSuper(className, listOfFields, listOfTypes);
    }

    public static String buildConstructorWithFieldsByLists(String className, List<String> listOfFields, List<String> listOfTypes) {
        ArrayListModel<String> auxiliaryAssignmentList = new ArrayListModel<String>();
        if (listOfTypes == null || listOfTypes.isEmpty()) {
            Logger.getDefault().error("THERE HAS BEEN AN ERROR: CATALOGS NOT HANDLED INSIDE TABLES");
            return "";
        }
        if (listOfFields.size() != listOfTypes.size()) {
            throw new RuntimeException("The ammount of fields and types does not match. Fields: " + listOfFields.size() + " Types: " + listOfTypes.size());
        }
        String constructor = "\n\t public " + PropertyTransformationUtil.standardJavaClassName(className) + "(";
        for (int i = 0; i < listOfFields.size(); ++i) {
            String field = PropertyTransformationUtil.standardJavaElementName(listOfFields.get(i));
            String type = PropertyTransformationUtil.getJavaTypeFromXSD(listOfTypes.get(i));
            String auxAssignment = "this." + field + "=" + field + ";";
            auxiliaryAssignmentList.add(auxAssignment);
            constructor = constructor + (i == 0 ? "" : ", ") + type + " " + field;
        }
        constructor = constructor + "){ \n";
        for (String assignment : auxiliaryAssignmentList) {
            constructor = constructor + "\t\t" + assignment + "\n";
        }
        constructor = constructor + "\t } \n";
        return constructor;
    }

    public static String buildConstructorWithFieldsByListsSuper(String className, List<String> listOfFields, List<String> listOfTypes) {
        if (listOfTypes == null || listOfTypes.isEmpty()) {
            Logger.getDefault().error("THERE HAS BEEN AN ERROR: CATALOGS NOT HANDLED INSIDE TABLES");
            return "";
        }
        if (listOfFields.size() != listOfTypes.size()) {
            throw new RuntimeException("The ammount of fields and types does not match. Fields: " + listOfFields.size() + " Types: " + listOfTypes.size());
        }
        String constructor = "\n\t public " + PropertyTransformationUtil.standardJavaClassName(className) + "(";
        String superConstructor = "\t\tsuper(";
        for (int i = 0; i < listOfFields.size(); ++i) {
            String field = PropertyTransformationUtil.standardJavaElementName(listOfFields.get(i));
            String type = PropertyTransformationUtil.getJavaTypeFromXSD(listOfTypes.get(i));
            superConstructor = superConstructor + (i == 0 ? "" : ", ") + field;
            constructor = constructor + (i == 0 ? "" : ", ") + type + " " + field;
        }
        superConstructor = superConstructor + ");\n";
        constructor = constructor + "){ \n";
        constructor = constructor + superConstructor;
        constructor = constructor + "\t } \n";
        return constructor;
    }

    public static String buildDefaultEmptyConstructor(String className, String fieldList) {
        List listOfFields = ListUtil.toList(fieldList, " ");
        String constructor = "\n\t public " + PropertyTransformationUtil.standardJavaClassName(className) + "() {\n";
        for (int i = 0; i < listOfFields.size(); ++i) {
            String field = PropertyTransformationUtil.standardJavaElementName((String)listOfFields.get(i));
            constructor = constructor + "\t\t this." + field + " = null;" + "\n";
        }
        constructor = constructor + "\t } \n";
        return constructor;
    }

    public static String buildQuadroConstructor(String className) {
        String constructor = "\n\t public " + PropertyTransformationUtil.standardJavaClassName(className) + "() {\n" + "\t\tsuper();\n" + "\t\tsetQuadroName(this.getClass().getSimpleName());\n" + "\t}\n";
        return constructor;
    }

    public static String uppercaseElement(String element) {
        return element.replace((CharSequence)"-", (CharSequence)"_").toUpperCase();
    }

    public static String buildPublicStaticStringProperty(String element) {
        if (element == null) {
            throw new RuntimeException("element cannot be null");
        }
        return "\t public static final String " + PropertyTransformationUtil.uppercaseElement(element) + " = " + StringUtil.addEnclosingQuotes(PropertyTransformationUtil.standardJavaElementName(element)) + ";";
    }

    public static String buildPropertyDeclaration(String element, String xsdType) {
        String definition = PropertyTransformationUtil.buildPropertyDeclarationJavaType(element, PropertyTransformationUtil.getJavaTypeFromXSD(xsdType));
        return definition;
    }

    public static String buildPropertyDeclarationJavaType(String element, String javaType) {
        if (element == null) {
            throw new RuntimeException("element cannot be null");
        }
        String standardJavaElementName = PropertyTransformationUtil.standardJavaElementName(element);
        String elementDecl = "\tprivate " + javaType + " " + standardJavaElementName;
        if (javaType.endsWith("Attr")) {
            elementDecl = elementDecl + " = new " + javaType + "()";
        }
        elementDecl = elementDecl + ";";
        return elementDecl;
    }

    public static void bufferFieldNameForHash(String elementName, String quadroName) {
        System.out.println("ELEMENTO: " + elementName + " - QUADRO: " + quadroName);
        String quadroClassName = PropertyTransformationUtil.standardJavaClassName(quadroName);
        String standardJavaElementName = PropertyTransformationUtil.standardJavaElementName(elementName);
        if (!fieldNames.containsKey(quadroClassName)) {
            ArrayList<String> fieldList = new ArrayList<String>();
            fieldList.add(standardJavaElementName);
            fieldNames.put(quadroClassName, fieldList);
        } else {
            fieldNames.get(quadroClassName).add(standardJavaElementName);
        }
    }

    public static String buildTableAdapterConstructor(String className, String columnNames) {
        String constructor = "\n\t public " + PropertyTransformationUtil.standardJavaClassName(className) + " (ListModel listModel) { \n";
        if (columnNames.endsWith("|")) {
            columnNames = columnNames.substring(0, columnNames.length() - 1);
        }
        String columnNameArgs = columnNames.trim().replace((CharSequence)"|", (CharSequence)"\", \"");
        constructor = constructor + "\t\t super(listModel, \"" + columnNameArgs + "\"); \n";
        constructor = constructor + "\t } \n\n";
        return constructor;
    }

    public static String buildIsCellEditableDefault(String className, boolean createIndex) {
        String method = "\t public boolean isEditable(" + PropertyTransformationUtil.standardJavaClassName(className) + " baseObject, int columnIndex){ \n" + (createIndex ? "\t\tif(columnIndex == 0) { return true; } \n\t" : "") + "\t\t return true; \n" + "\t } \n\n";
        return method;
    }

    public static String buildGetPositionAtDefault(String lineClassName, String fieldList, String typeList, boolean createIndex) {
        return PropertyTransformationUtil.buildGetPositionAtDefault(lineClassName, fieldList, "", typeList, createIndex);
    }

    public static String buildGetPositionAtDefault(String className, String fieldList, String suffix, String typeList, boolean createIndex) {
        List listOfFields = ListUtil.toList(fieldList, " ");
        List listOfTypes = ListUtil.toList(typeList, " ");
        String method = "\t @Override \n";
        method = method + "\t public Object getColumnValue(" + PropertyTransformationUtil.standardJavaClassName(className) + " line, int columnIndex) { \n";
        method = method + "\t\t switch (columnIndex) { \n";
        for (int i = 0; i < listOfFields.size(); ++i) {
            int indexCase = createIndex ? i + 1 : i;
            String xsdType = (String)listOfTypes.get(i);
            method = method + "\t\t\t case " + indexCase + ": \n";
            if (XsltToJavaTypesUtil.catalogs.contains(xsdType)) {
                method = method + "\t\t\t\t if (line == null || line." + PropertyTransformationUtil.buildGetterMethodName((String)listOfFields.get(i)) + suffix + "()" + "==null)\n";
                method = method + "\t\t\t\t \t return null;\n";
                method = method + "\t\t\t\t return CatalogManager.getInstance().convertCatalogValueToCatalogItemForUI(\"" + xsdType + "\", line." + PropertyTransformationUtil.buildGetterMethodName((String)listOfFields.get(i)) + suffix + "());\n";
                continue;
            }
            method = method + "\t\t\t\t return line." + PropertyTransformationUtil.buildGetterMethodName((String)listOfFields.get(i)) + suffix + "(); \n";
        }
        method = method + "\t\t\t default: \n";
        method = method + "\t\t\t\t break; \n";
        method = method + "\t\t } \n";
        method = method + "\t\t return null; \n";
        method = method + "\t } \n\n";
        return method;
    }

    public static String buildGetColumnCount(String fieldList, boolean createIndex) {
        List listOfFields = ListUtil.toList(fieldList, " ");
        String method = "\t @Override \n";
        int size = createIndex ? listOfFields.size() + 1 : listOfFields.size();
        method = method + "\t public int getColumnCount(){ \n\t\t return " + size + "; \n" + "\t}\n\n";
        return method;
    }

    public static String buildSetPositionAtDefault(String lineClassName, String fieldList, String typeList, boolean createIndex) {
        List listOfFields = ListUtil.toList(fieldList, " ");
        List listOfTypes = ListUtil.toList(typeList, " ");
        String method = "\t @Override \n";
        method = method + "\t public " + PropertyTransformationUtil.standardJavaClassName(lineClassName) + " setColumnValue(" + PropertyTransformationUtil.standardJavaClassName(lineClassName) + " line, Object value, int columnIndex) { \n";
        method = method + "\t\t switch (columnIndex) { \n";
        for (int i = 0; i < listOfFields.size(); ++i) {
            String xsdType = (String)listOfTypes.get(i);
            int indexCase = createIndex ? i + 1 : i;
            method = method + "\t\t\t case " + indexCase + ": \n";
            method = XsltToJavaTypesUtil.catalogs.contains(xsdType) ? method + "\t\t\t if(value != null)\n\t\t\t\tline." + PropertyTransformationUtil.buildSetterMethodName((String)listOfFields.get(i)) + "((" + PropertyTransformationUtil.getJavaTypeFromXSD((String)listOfTypes.get(i)) + ")((ICatalogItem) value).getValue() ); \n" : method + "\t\t\t line." + PropertyTransformationUtil.buildSetterMethodName((String)listOfFields.get(i)) + "((" + PropertyTransformationUtil.getJavaTypeFromXSD((String)listOfTypes.get(i)) + ") value); \n";
            if (columnOperationsMap.containsKey(listOfFields.get(i))) {
                List<String> resultsToWhichIContribute = columnOperationsMap.get(listOfFields.get(i));
                for (String result : resultsToWhichIContribute) {
                    method = method + "\nline." + PropertyTransformationUtil.buildSetterMethodName(new StringBuilder().append(result).append("Formula").toString()) + "((" + PropertyTransformationUtil.getJavaTypeFromXSD((String)listOfTypes.get(i)) + ")value);\n";
                }
            }
            method = method + "\t\t\t\t return line;\n";
        }
        method = method + "\t\t\t default: \n";
        method = method + "\t\t\t\t return null; \n";
        method = method + "\t\t } \n";
        method = method + "\t } \n\n";
        columnOperationsMap.clear();
        return method;
    }

    public static String buildGetColumnClassDefault(String typeList, boolean createIndex) {
        List listOfTypes = ListUtil.toList(typeList, " ");
        String method = "\t @Override \n";
        method = method + "\t public Class<?> getColumnClass(int columnIndex) { \n";
        method = method + "\t\t switch (columnIndex) { \n";
        for (int i = 0; i < listOfTypes.size(); ++i) {
            int indexCase = createIndex ? i + 1 : i;
            method = method + "\t\t\t case " + indexCase + ": \n";
            method = method + "\t\t\t\t return " + PropertyTransformationUtil.getJavaTypeFromXSD((String)listOfTypes.get(i)) + ".class; \n";
        }
        method = method + "\t\t\t default: \n";
        method = method + "\t\t\t\t return null; \n";
        method = method + "\t\t } \n";
        method = method + "\t } \n\n";
        return method;
    }

    public static String buildGetColumnsName(String columnNames, boolean createIndex) {
        String method = "\n\t public String getColumnName(int column) { \n";
        if (columnNames.endsWith("|")) {
            columnNames = columnNames.substring(0, columnNames.length() - 1);
        }
        List list = ListUtil.toList(columnNames, "|");
        method = method + "\t\tswitch(column) {\n";
        for (int i = 0; i < list.size(); ++i) {
            int indexCase = createIndex ? i + 1 : i;
            method = method + "\t\t\t case " + indexCase + ": \n";
            method = method + "\t\t\t\t return \"" + (String)list.get(i) + "\"; \n";
        }
        method = method + "\t\t\t default: \n";
        method = method + "\t\t\t\t return null; \n";
        method = method + "\t\t } \n";
        method = method + "\t } \n\n";
        return method;
    }

    public static String buildEmptyColumnComparator() {
        String method = "\t @Override \n\t @SuppressWarnings(\"unchecked\") \n\t public Comparator getColumnComparator(int column){ \n\t\t return null; \n\t } \n\n";
        return method;
    }

    public static String buildEventListDeclaration(String className, String lineName) {
        String stdClassName = PropertyTransformationUtil.standardJavaClassName(lineName);
        String declaration = "\n\t private EventList<" + stdClassName + "> " + PropertyTransformationUtil.standardJavaElementName(className) + " = new BasicEventList<" + stdClassName + ">(); \n\n";
        return declaration;
    }

    public static String buildAttrTableDeclaration(String className, String lineName, String attrName) {
        String stdClassName = PropertyTransformationUtil.standardJavaClassName(lineName);
        String declaration = "\n\t private EventList<" + stdClassName + "> " + PropertyTransformationUtil.standardJavaElementName(className) + " = new BasicEventList<" + stdClassName + ">(); \n\n";
        return declaration;
    }

    public static String buildEventListGetter(String className, String lineName) {
        String modelGetterName = PropertyTransformationUtil.buildGetterMethodName(className);
        String method = "\n\t@Type(Type.TYPE.TABELA)\n\tpublic EventList<" + PropertyTransformationUtil.standardJavaClassName(lineName) + "> " + modelGetterName + "(){ \n" + "\t\t return this." + PropertyTransformationUtil.standardJavaElementName(className) + ";\n" + "\t } \n\n";
        return method;
    }

    public static String buildEventListGetterAndAppInfo(String className, String lineName, String appInfo) {
        String modelGetterName = PropertyTransformationUtil.buildGetterMethodName(className);
        String method = "\n\n\t" + appInfo + "\n\t@Type(Type.TYPE.TABELA)\n\tpublic EventList<" + PropertyTransformationUtil.standardJavaClassName(lineName) + "> " + modelGetterName + "(){ \n" + "\t\t return this." + PropertyTransformationUtil.standardJavaElementName(className) + ";\n" + "\t } \n\n";
        return method;
    }

    public static String buildEventListSetter(String className, String lineName) {
        String modelElementName = PropertyTransformationUtil.standardJavaElementName(className);
        String modelSetterName = PropertyTransformationUtil.buildSetterMethodName(className);
        String method = "\n\t@Type(Type.TYPE.TABELA)\n\t public void " + modelSetterName + "(EventList<" + PropertyTransformationUtil.standardJavaClassName(lineName) + "> " + modelElementName + "){ \n" + "\t\t this." + modelElementName + " =" + modelElementName + "; \n" + "\t } \n\n";
        return method;
    }

    public static String buildQuadroRepetitivoDeclaration(String quadroName, String lineName) {
        String elementName = PropertyTransformationUtil.standardJavaElementName(quadroName);
        String lineClassName = PropertyTransformationUtil.standardJavaClassName(lineName);
        String contsString = "\tpublic static final String PAG_ACTUAL = \"pagActual\";\n\tpublic static final String TOT_PAG = \"totPag\";\n\tprivate int pagActual ;\n\tprivate int totPag;\n";
        String declaration = "\n\t@Type(Type.TYPE.QUADRO_REPETITIVO)\n\tprivate SelectionInList<" + PropertyTransformationUtil.standardJavaClassName(lineName) + "> " + elementName + " = new SelectionInList<" + lineClassName + ">((ListModel) new ArrayListModel<" + lineClassName + ">());\n\n";
        return contsString + declaration;
    }

    public static String buildQuadroRepetitivoGetter(String quadroName, String lineName) {
        String elementName = PropertyTransformationUtil.standardJavaElementName(quadroName);
        String lineClassName = PropertyTransformationUtil.standardJavaClassName(lineName);
        String method = "\n\t@Type(Type.TYPE.QUADRO_REPETITIVO)\n\tpublic SelectionInList<" + PropertyTransformationUtil.standardJavaClassName(lineClassName) + "> " + PropertyTransformationUtil.buildGetterMethodName(quadroName) + "(){\n" + "\t\treturn this." + elementName + ";\n" + "\t}";
        return method;
    }

    public static String buildQuadroRepetitivoSetter(String quadroName, String lineName) {
        String elementName = PropertyTransformationUtil.standardJavaElementName(quadroName);
        String method = "\n\t@Type(Type.TYPE.QUADRO_REPETITIVO)\n\tpublic void " + PropertyTransformationUtil.buildSetterMethodName(quadroName) + "(SelectionInList<" + PropertyTransformationUtil.standardJavaClassName(lineName) + "> " + elementName + "){\n" + "\t\tthis." + elementName + " = " + elementName + ";\n" + "\t}";
        return method;
    }

    public static String buildGetterSetterPagActual() {
        String getter = "\tpublic int getPagActual() {\n\t\treturn pagActual;\n\t}\n";
        String setter = "\tpublic void setPagActual(int value) {\n\t\tint oldValue = this.pagActual;\n\t\tthis.pagActual = value;\n\t\tfirePropertyChange(PAG_ACTUAL, oldValue, this.pagActual);\n\t}\n";
        return getter + setter;
    }

    public static String buildGetterSetterTotPag() {
        String getter = "\tpublic int getTotPag() {\n\t\treturn totPag;\n\t}\n";
        String setter = "\tpublic void setTotPag(int value) {\n\t\tint oldValue = this.totPag;\n\t\tthis.totPag = value;\n\t\tfirePropertyChange(TOT_PAG, oldValue, this.totPag);\n\t}\n";
        return getter + setter;
    }

    public static String buildEmptyConstructor(String name) {
        return "\tpublic " + PropertyTransformationUtil.standardJavaClassName(name) + "() {\n\t}\n\n";
    }

    public static void resetBufferMap() {
        bufferMap = new HashMap<String, StringBuffer>();
    }

    public static String buildCreateAnexo(List<TinyNodeImpl> anexos) {
        String method = "\n\tprotected AnexoModel createAnexo(FormKey formKey) {\n";
        for (TinyNodeImpl anexo : anexos) {
            String anexoModelName = anexo.getStringValue() + "Model";
            method = method + "\t\tif(\"" + anexo.getStringValue() + "\".equals(formKey.getId())){\n";
            method = method + "\t\t\treturn new " + anexoModelName + "(formKey, true);";
            method = method + "\t\t}\n";
        }
        method = method + "\t\treturn null;\n";
        method = method + "\t}\n";
        return method;
    }

    public static String buildDeclaracaoModelHashCode(List<TinyNodeImpl> anexos) {
        String method = "\n\tpublic int hashCode(){\n\t\tint result = HashCodeUtil.SEED;\n\t\t";
        for (TinyNodeImpl anexo : anexos) {
            String anexoModelName = anexo.getStringValue() + "Model";
            method = method + "result = HashCodeUtil.hash(result, this.getAllAnexosByType(" + anexoModelName + ".class));\n\t\t";
        }
        method = method + "return result;\n\t}\n";
        return method;
    }

    public static String buildCreateAnexoFormKeys(List<TinyNodeImpl> anexos) {
        String method = "";
        for (TinyNodeImpl anexo : anexos) {
            method = method + "\tpublic static final FormKey " + PropertyTransformationUtil.standardJavaClassName(anexo.getStringValue()).toUpperCase() + "_KEY = new FormKey(\"" + PropertyTransformationUtil.standardJavaClassName(anexo.getStringValue()) + "\");\n\t";
        }
        return method;
    }

    public static void mapColumnOperations(String result, String formula) {
        if (StringUtil.isEmpty(formula)) {
            return;
        }
        List<String> operandList = BehavioursGeneratorUtil.parseOperandString(formula);
        for (String operand : operandList) {
            List<String> participationList = columnOperationsMap.get(operand);
            if (participationList == null) {
                participationList = new ArrayList<String>();
                columnOperationsMap.put(operand, participationList);
            }
            participationList.add(result);
        }
    }

    public static boolean fileExists(String uri) {
        File file = new File(uri);
        return file.exists();
    }

    public static String generateImportsForQuadroModel(String packageStr) {
        String importString = "import ca.odell.glazedlists.BasicEventList;\nimport ca.odell.glazedlists.EventList;\nimport com.jgoodies.binding.list.SelectionInList;\nimport javax.swing.ListModel;\nimport com.jgoodies.binding.list.ArrayListModel;\nimport pt.opensoft.taxclient.model.QuadroModel;\nimport pt.opensoft.swing.handlers.FieldHandlerModel;\nimport pt.opensoft.taxclient.persistence.Catalog;\nimport pt.opensoft.taxclient.persistence.ComplexField;\nimport pt.opensoft.taxclient.persistence.FractionDigits;\nimport pt.opensoft.taxclient.persistence.Type;\nimport pt.opensoft.util.DateTime;\nimport pt.opensoft.util.Date;\nimport pt.opensoft.taxclient.util.HashCodeUtil;\nimport pt.opensoft.taxclient.overwriteBehaviour.OverwriteBehaviorManager;\nimport " + packageStr.toLowerCase() + ".*;\n\n";
        int last = packageStr.lastIndexOf(".");
        String modelPack = packageStr.substring(0, last);
        importString = importString + "import " + modelPack.toLowerCase() + ".*;" + "\n\n";
        return importString;
    }

    public static String createAnexoLink(String nomeDoAnexo) {
        nomeAnexoCorrente = "a" + PropertyTransformationUtil.standardJavaClassName(nomeDoAnexo);
        String link = "\n\tpublic static final String " + PropertyTransformationUtil.standardJavaClassName(nomeDoAnexo).toUpperCase() + "_LINK = \"" + nomeAnexoCorrente + "\";\n";
        return link;
    }

    public static String createQuadroLink(String nomeDoQuadro) {
        nomeQuadroCorrente = "q" + PropertyTransformationUtil.standardJavaClassName(nomeDoQuadro);
        String link = "\n\tpublic static final String " + nomeDoQuadro.toUpperCase() + "_LINK = \"" + nomeAnexoCorrente + "." + nomeQuadroCorrente + "\";\n";
        return link;
    }

    public static String createCampoLink(String nomeDoCampo, String elementType) {
        String link = "";
        nomeDoCampo = PropertyTransformationUtil.standardJavaElementName(nomeDoCampo);
        if (XsltToJavaTypesUtil.radiosList.containsKey(elementType)) {
            List idList = (List)XsltToJavaTypesUtil.radiosList.get(elementType).getFirst();
            List valueList = (List)XsltToJavaTypesUtil.radiosList.get(elementType).getSecond();
            String javaClass = PropertyTransformationUtil.getJavaTypeFromXSD(elementType);
            for (int i = 0; i < idList.size(); ++i) {
                String optionId = ((TinyNodeImpl)idList.get(i)).getStringValue();
                String value = ((TinyNodeImpl)valueList.get(i)).getStringValue();
                String tempNome = nomeDoCampo + optionId;
                link = link + "\n\tpublic static final String " + tempNome.toUpperCase() + "_LINK = \"" + nomeAnexoCorrente + "." + nomeQuadroCorrente + ".f" + tempNome + "\";\n";
                link = String.class.getSimpleName().equals(javaClass) ? link + "\n\tpublic static final " + javaClass + " " + tempNome.toUpperCase() + "_VALUE = \"" + value + "\";\n\n" : link + "\n\tpublic static final " + javaClass + " " + tempNome.toUpperCase() + "_VALUE = " + value + ";\n\n";
            }
        } else {
            link = link + "\n\tpublic static final String " + nomeDoCampo.toUpperCase() + "_LINK = \"" + nomeAnexoCorrente + "." + nomeQuadroCorrente + ".f" + nomeDoCampo + "\";\n";
        }
        return link;
    }

    public static String createTableLink(String nomeDaTabela) {
        nomeDaTabela = PropertyTransformationUtil.standardJavaElementName(nomeDaTabela);
        String link = "\n\tpublic static final String " + nomeDaTabela.toUpperCase() + "_LINK = \"" + nomeAnexoCorrente + "." + nomeQuadroCorrente + ".t" + nomeDaTabela + "\";\n";
        return link;
    }

    public static String generateHashCodeMethod(String quadroName) {
        String javaQuadroName = PropertyTransformationUtil.standardJavaClassName(quadroName);
        if (!fieldNames.containsKey(javaQuadroName)) {
            return "";
        }
        String hashCodeMethod = "\t@Override \n\tpublic int hashCode(){ \n\t\tint result = HashCodeUtil.SEED; \n\t\t";
        for (String field : fieldNames.get(javaQuadroName)) {
            hashCodeMethod = hashCodeMethod + "result = HashCodeUtil.hash( result," + field + "); \n\t\t";
        }
        hashCodeMethod = hashCodeMethod + "return result; \n\t}\n\n";
        fieldNames.remove(javaQuadroName);
        return hashCodeMethod;
    }

    public static String buildFieldEnum(String modelName, String fieldList, boolean createIndex) {
        List listOfFields = ListUtil.toList(fieldList, " ");
        String method = "\n\t public enum Property { \n";
        for (int i = 0; i < listOfFields.size(); ++i) {
            int indexCase = createIndex ? i + 1 : i;
            String field = ((String)listOfFields.get(i)).replace((CharSequence)"-", (CharSequence)"_");
            method = method + "\t\t " + field.toUpperCase() + "(" + PropertyTransformationUtil.standardJavaClassName(modelName) + "." + field.toUpperCase() + ", " + (indexCase + 1) + ")";
            method = method + (i != listOfFields.size() - 1 ? ",\n" : ";\n\n");
        }
        method = method + "\t\t private final String name;\n";
        method = method + "\t\t private final int index;\n\n";
        method = method + "\t\t Property(String name, int index){\n";
        method = method + "\t\t\t this.name = name;\n";
        method = method + "\t\t\t this.index = index;\n";
        method = method + "\t\t }\n";
        method = method + "\t\t public String getName() { return name; }\n";
        method = method + "\t\t public int getIndex() { return index; }\n";
        method = method + "\t }\n\n";
        return method;
    }
}

