/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import pt.opensoft.io.FileUtil;
import pt.opensoft.io.ResourceLoader;
import pt.opensoft.io.ResourceNotFoundException;
import pt.opensoft.taxclient.app.InvalidMessageException;
import pt.opensoft.taxclient.persistence.exception.CorruptedFileException;
import pt.opensoft.taxclient.util.XmlProcessorHandler;
import pt.opensoft.util.SimpleLog;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.SelectiveXMLGrammarCachingConfiguration;
import pt.opensoft.xml.XmlUtil;

public class XmlProcessor
extends DefaultHandler {
    private static final String FILE_START = "file:";
    private XMLReader parser;
    private List errorsList = null;
    private XmlProcessorHandler handler = null;
    private static boolean initializedOnce = false;

    public XmlProcessor(XmlProcessorHandler handler) {
        this.handler = handler;
        this.initParser();
        this.forceNoSchema();
    }

    public XmlProcessor(String schemasLocation, String schemaNamespace) {
        this(schemasLocation, schemaNamespace, null);
    }

    public XmlProcessor(String schemasLocation, String schemaNamespace, XmlProcessorHandler handler) {
        this.handler = handler;
        this.errorsList = new ArrayList(0);
        this.initParser();
        if (!StringUtil.isEmpty(schemasLocation)) {
            this.setEntityResolver(schemasLocation);
        }
        if (!StringUtil.isEmpty(schemaNamespace)) {
            this.forceSchema(schemaNamespace);
        }
    }

    private void initParser() {
        if (!initializedOnce) {
            System.setProperty("opensoft.cacheable-namespaces", "http://www.dgci.gov.pt/2007/IES-DA http://www.mj.gov.pt/2007/IES-DA");
            String className = new SelectiveXMLGrammarCachingConfiguration().getClass().getName();
            System.setProperty("org.apache.xerces.xni.parser.XMLParserConfiguration", className);
            initializedOnce = true;
        }
        this.parser = XmlUtil.getSaxParser("org.apache.xerces.parsers.SAXParser");
        this.parser.setContentHandler(this);
        this.parser.setErrorHandler(this);
    }

    private void setEntityResolver(final String schemasLocation) {
        this.parser.setEntityResolver(new EntityResolver(){

            @Override
            public InputSource resolveEntity(String s, String systemId) throws ResourceNotFoundException {
                if (systemId.startsWith("file:")) {
                    systemId = schemasLocation + FileUtil.getFilename(systemId.substring("file:".length()));
                }
                InputSource inputSource = new InputSource(ResourceLoader.getResourceStream(systemId));
                inputSource.setSystemId(systemId);
                inputSource.setEncoding("ISO-8859-1");
                return inputSource;
            }
        });
    }

    private void forceSchema(String schemaNamespace) {
        try {
            this.parser.setFeature("http://xml.org/sax/features/validation", true);
            this.parser.setFeature("http://apache.org/xml/features/validation/schema", true);
            this.parser.setFeature("http://apache.org/xml/features/validation/schema-full-checking", false);
            this.parser.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation", schemaNamespace);
        }
        catch (SAXNotRecognizedException e) {
            SimpleLog.log("Invalid property setting on xml parser.");
        }
        catch (SAXNotSupportedException e) {
            SimpleLog.log("Not supported property setting on xml parser.");
        }
    }

    private void forceNoSchema() {
        try {
            this.parser.setFeature("http://xml.org/sax/features/validation", false);
            this.parser.setFeature("http://apache.org/xml/features/validation/schema", false);
            this.parser.setFeature("http://apache.org/xml/features/validation/schema-full-checking", false);
        }
        catch (SAXNotRecognizedException e) {
            SimpleLog.log("Invalid property setting on xml parser.");
        }
        catch (SAXNotSupportedException e) {
            SimpleLog.log("Not supported property setting on xml parser.");
        }
    }

    public List read(Reader reader) throws IOException, SAXException {
        InputSource inputSource = new InputSource(reader);
        inputSource.setEncoding("ISO-8859-1");
        this.errorsList = new ArrayList(0);
        this.parser.parse(inputSource);
        return this.errorsList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (this.handler != null) {
            try {
                this.handler.startElementCallback(uri, localName, qName, this.attributes2Map(attributes));
            }
            catch (InvalidMessageException ime) {
                SimpleLog.log("Erro na leitura do ficheiro: " + ime.getMessage());
                throw ime;
            }
            catch (CorruptedFileException e) {
                e.printStackTrace();
                throw e;
            }
            catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Ocorreu um erro na leitura do ficheiro.");
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (this.handler != null) {
            try {
                this.handler.endElementCallback(uri, localName, qName);
            }
            catch (InvalidMessageException ime) {
                SimpleLog.log("Erro na leitura do ficheiro: " + ime.getMessage());
                throw ime;
            }
            catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Ocorreu um erro na leitura do ficheiro.");
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.handler != null) {
            this.handler.charactersCallback(ch, start, length);
        }
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        if (this.handler != null) {
            this.handler.warning(e);
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        this.errorsList.add(e);
        if (this.handler != null) {
            this.handler.error(e);
        }
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    public List getErrors() {
        return this.errorsList;
    }

    private Map attributes2Map(Attributes attributes) {
        TreeMap<String, String> result = new TreeMap<String, String>();
        for (int index = 0; index < attributes.getLength(); ++index) {
            result.put(attributes.getQName(index), attributes.getValue(index));
        }
        return result;
    }

}

