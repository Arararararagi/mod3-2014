/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import java.util.HashMap;
import java.util.Map;
import pt.opensoft.swing.model.catalogs.ICatalogItem;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.ListMap;

public class CatalogManager {
    private static CatalogManager instance = null;
    private Map<String, ListMap> catalogMap = new HashMap<String, ListMap>();
    private Map<String, ListMap> catalogMapForUI;
    private Map<String, ICatalogItem> catalogInvalidItem;

    private CatalogManager() {
    }

    public static CatalogManager getInstance() {
        if (instance == null) {
            instance = new CatalogManager();
        }
        return instance;
    }

    public void registerCatalogListForUI(String catalogName, ListMap catalogModel, ICatalogItem invalidCatalogItem) {
        if (this.catalogInvalidItem == null) {
            this.catalogInvalidItem = new HashMap<String, ICatalogItem>();
        }
        if (this.catalogMapForUI == null) {
            this.catalogMapForUI = new HashMap<String, ListMap>();
        }
        this.catalogInvalidItem.put(catalogName, invalidCatalogItem);
        ListHashMap uiInvalida = new ListHashMap();
        uiInvalida.addAll(catalogModel);
        uiInvalida.put(invalidCatalogItem.getValue(), invalidCatalogItem);
        this.catalogMapForUI.put(catalogName, uiInvalida);
    }

    public void registerCatalogListModel(String catalogName, ListMap catalogModel) {
        this.catalogMap.put(catalogName, catalogModel);
    }

    public ListMap getCatalog(String catalogName) {
        return this.catalogMap.get(catalogName);
    }

    public ListMap getCatalogForUI(String catalogName) {
        ListMap uiCatalog;
        if (this.catalogMapForUI != null && (uiCatalog = this.catalogMapForUI.get(catalogName)) != null) {
            return uiCatalog;
        }
        return this.catalogMap.get(catalogName);
    }

    public ICatalogItem getCatalogInvalidItem(String catalogName) {
        if (this.catalogInvalidItem == null) {
            return null;
        }
        return this.catalogInvalidItem.get(catalogName);
    }

    public ICatalogItem convertCatalogValueToCatalogItemForUI(String catalogName, Object valueToConvert) {
        ICatalogItem foundValue = (ICatalogItem)this.getCatalog(catalogName).get(valueToConvert);
        if (foundValue != null) {
            return foundValue;
        }
        return this.getCatalogInvalidItem(catalogName);
    }

    public void reloadCatalogForUIFromDataCatalog(String catalogName) {
        ICatalogItem invalidItem;
        if (this.catalogMapForUI == null) {
            return;
        }
        ListMap catalogForUI = this.catalogMapForUI.get(catalogName);
        if (catalogForUI == null) {
            return;
        }
        catalogForUI.clear();
        ListMap original = this.catalogMap.get(catalogName);
        if (original != null) {
            catalogForUI.addAll(original);
        }
        if ((invalidItem = this.catalogInvalidItem.get(catalogName)) != null) {
            catalogForUI.put(invalidItem.getValue(), invalidItem);
        }
    }
}

