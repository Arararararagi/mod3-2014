/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import pt.opensoft.taxclient.util.WebServicesUtil;
import pt.opensoft.util.StringUtil;

public class DeclValidationMessage
implements ValidationMessage {
    protected String _fieldReference;
    protected String _code;
    protected String _fieldValue;
    protected String[] _links;
    protected String[] _replaces;
    protected int[] _lineNumbers;
    protected Severity _severity;
    protected String message;

    public DeclValidationMessage(String code) {
        this(code, "");
    }

    public DeclValidationMessage(String code, Severity severity) {
        this(code, "", null, null, null, severity);
    }

    public DeclValidationMessage(String code, String fieldReference) {
        this(code, fieldReference, null);
    }

    public DeclValidationMessage(String code, String fieldReference, String[] links) {
        this(code, fieldReference, links, null, null, null);
    }

    public DeclValidationMessage(String code, String fieldReference, String[] links, Severity severity) {
        this(code, fieldReference, links, null, null, severity);
    }

    public DeclValidationMessage(String code, String fieldReference, String[] links, String[] replaces) {
        this(code, fieldReference, links, null, replaces);
    }

    public DeclValidationMessage(String code, String fieldReference, String[] links, String[] replaces, Severity severity) {
        this(code, fieldReference, links, null, replaces, severity);
    }

    public DeclValidationMessage(String code, String fieldReference, String[] links, String fieldValue) {
        this(code, fieldReference, links, fieldValue, null);
    }

    public DeclValidationMessage(String code, String fieldReference, String[] links, String fieldValue, String[] replaces) {
        this(code, fieldReference, links, fieldValue, replaces, null);
    }

    public DeclValidationMessage(String code, String fieldReference, String[] links, String fieldValue, String[] replaces, Severity severity) {
        if (links != null && links.length > 99) {
            throw new RuntimeException("A mensagem de erro n\u00e3o pode ter mais do que 99 links.");
        }
        if (replaces != null && replaces.length > 99) {
            throw new RuntimeException("A mensagem de erro n\u00e3o pode ter mais do que 99 replaces.");
        }
        this._fieldReference = fieldReference;
        this._code = code;
        this._links = links;
        this._replaces = replaces;
        this._fieldValue = fieldValue;
        this._severity = severity;
    }

    @Override
    public String formattedText() {
        return null;
    }

    @Override
    public Object key() {
        int index = this._fieldReference.lastIndexOf(46);
        if (index != -1) {
            return this._fieldReference.substring(index + 1);
        }
        return this._fieldReference;
    }

    @Override
    public Severity severity() {
        if (this._severity != null) {
            return this._severity;
        }
        return Severity.ERROR;
    }

    public void setSeverity(Severity severity) {
        this._severity = severity;
    }

    public String getLineNumber(int lineNumber) {
        return Integer.toString(this._lineNumbers[lineNumber]);
    }

    public String get_fieldReference() {
        return this._fieldReference;
    }

    public String get_code() {
        return this._code;
    }

    public String get_fieldValue() {
        return this._fieldValue;
    }

    public String[] get_links() {
        return this._links;
    }

    public String[] get_replaces() {
        return this._replaces;
    }

    public int[] get_lineNumbers() {
        return this._lineNumbers;
    }

    public String get_message() {
        return this.message;
    }

    public void set_message(String msg) {
        this.message = msg;
    }

    public void set_MessageAndEncodeIt(String msg) {
        if (!StringUtil.isEmpty(msg)) {
            this.message = WebServicesUtil.encode(msg);
        }
    }

    public void set_MessageAndDecodeIt(String msg) {
        if (!StringUtil.isEmpty(msg)) {
            this.message = WebServicesUtil.decode(msg);
        }
    }
}

