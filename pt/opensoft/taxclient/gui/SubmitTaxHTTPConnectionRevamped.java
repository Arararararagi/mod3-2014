/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import pt.opensoft.crypt.Password;
import pt.opensoft.http.HttpResponse;
import pt.opensoft.http.JavaHttpConnection;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedDadosToSubmit;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.actions.util.pf.PortalTaxClientRevampedSubmitUser;
import pt.opensoft.taxclient.app.ProxyParameters;
import pt.opensoft.taxclient.gui.AbstractSubmitTaxRevamped;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.gui.SubmitResponse;
import pt.opensoft.taxclient.http.SubmitReceipt;
import pt.opensoft.taxclient.http.SubmitUser;
import pt.opensoft.taxclient.util.WebServicesUtil;
import pt.opensoft.text.Base64;
import pt.opensoft.text.Base64Decoder;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.ListUtil;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.XmlBuilder;

public class SubmitTaxHTTPConnectionRevamped
extends AbstractSubmitTaxRevamped {
    protected SubmitResponse submitResponse;
    public static final String SUJ_PASS_A_ID = "SPA";
    public static final String SUJ_PASS_B_ID = "SPB";
    public static final String TOC_ID = "TOC";
    public static final int RESPONSE_CODE_OK = 0;
    public static final int RESPONSE_CODE_ERROR_INVALID_REQUEST = 96;
    public static final int RESPONSE_CODE_EXCEPTION = 97;
    public static final int RESPONSE_CODE_UNAUTHORIZED = 98;
    public static final int RESPONSE_CODE_NOK = 99;
    protected static final String RESPONSE_PARAMETER_CODE = "code";
    protected static final String RESPONSE_PARAMETER_ERROR = "error";
    protected static final String RESPONSE_PARAMETER_GENERIC_ERROR = "erros";
    protected static final String RESPONSE_PARAMETER_WARNING = "warning";
    protected static final String RESPONSE_PARAMETER_INFORMATION = "information";
    protected static final String RESPONSE_PARAMETER_RECEIPT = "receipt";
    protected static final String RESPONSE_PARAMETER_OPTIONAL = "optional";
    protected static final String RESPONSE_PARAMETER_NIFS = "nif";
    protected static final String RESPONSE_RECEIPT_DATE = "date";
    protected static final String RESPONSE_RECEIPT_YEAR = "year";
    protected static final String RESPONSE_RECEIPT_DECLID = "declid";
    protected static final String RESPONSE_NIF_NAME = "name";
    protected static final String RESPONSE_NIF_VALUE = "nif";
    protected static final String RESPONSE_OPTIONAL_NAME = "key";
    protected static final String RESPONSE_OPTIONAL_VALUE = "value";
    protected static final String RESPONSE_PARAMETER_ERROR_REFERENCE = "reference";
    protected static final String RESPONSE_PARAMETER_ERROR_REPLACE = "replace";
    protected static final String RESPONSE_ERROR_CODE_ATTRIBUTE = "code";
    protected static final String RESPONSE_ERROR_FIELDREF_ATTRIBUTE = "field";
    protected static final String RESPONSE_ERROR_SEVERITY_ATTRIBUTE = "severity";
    protected static final String RESPONSE_ERROR_MSG_ATTRIBUTE = "msg";
    public static final String HTTP_STATE_IDLE = "Comunicando com o servidor...";
    protected static final String HTTP_STATE_SEND = "A enviar Declara\u00e7\u00e3o...";
    protected static final String HTTP_STATE_RECEIVE = "A receber resposta do servidor...";
    protected static final String HTTP_STATE_FINISH = "Comunica\u00e7\u00e3o terminada...";
    protected URL url;
    protected JavaHttpConnection httpConnection;
    protected static final int TIMEOUT = ProxyParameters.getConnectionTimeout();
    protected static final String DECL_TAG = "decl";
    private static final String CHECK_TAG = "checkwarnings";
    private static final String POST_NAME = "tax";
    private static final String DE_SYSTEM_NAME = "taxUser";
    private static final String DE_SYSTEM_PASS = "y5jeC$maQe";
    private static final String SYSTEM_NAME_TAG = "systemName";
    private static final String SYSTEM_PASS_TAG = "systemPass";
    private static final String APP_VERSION_TAG = "app_version";
    private static final String DECL_VERSION_TAG = "declVersion";

    public SubmitTaxHTTPConnectionRevamped(URL url) {
        if (url == null) {
            throw new NullPointerException("url cannot be null");
        }
        this.url = url;
        this.initializeConnection();
    }

    public boolean addRequestParametersToWebservice(PortalTaxClientRevampedSubmitInfo submitInfo) {
        PortalTaxClientRevampedSubmitUser submitUser = submitInfo.getSubmitUser();
        this.addParameter("systemName", "taxUser");
        this.addParameter("systemPass", Password.encryptWithPaddingLength("y5jeC$maQe"));
        this.addParameter("decl", submitInfo.getDados().getDadosStr());
        if (submitInfo.hasAppVersion()) {
            this.addParameter("app_version", submitInfo.getAppVersion());
        }
        this.addParameter("checkwarnings", String.valueOf(submitInfo.isCheckWarnings()));
        this.addParameter("SPA", submitUser.getNifA());
        this.addParameter("nifA", submitUser.getNifA());
        this.addParameter("passwordA", submitUser.getPasswordA());
        if (!StringUtil.isEmpty(submitUser.getNifB())) {
            this.addParameter("SPB", submitUser.getNifB());
            this.addParameter("nifB", submitUser.getNifB());
            this.addParameter("passwordB", submitUser.getPasswordB());
        }
        if (!StringUtil.isEmpty(submitUser.getToc())) {
            this.addParameter("TOC", submitUser.getToc());
            this.addParameter("toc", submitUser.getToc());
            this.addParameter("passwordToc", submitUser.getPasswordToc());
        }
        for (Pair optionalField : submitInfo.getOptionalFields()) {
            this.addParameter((String)optionalField.getFirst(), (String)optionalField.getSecond());
        }
        this.addParameter("ws_version", "1.0");
        return true;
    }

    @Override
    public void initializeConnection() throws IllegalArgumentException {
        this.httpConnection = new JavaHttpConnection(this.url);
        this.httpConnection.setTimeout(TIMEOUT);
        try {
            if (ProxyParameters.getUseProxy() && ProxyParameters.isProxySet()) {
                this.httpConnection.setProxy(ProxyParameters.getProxyHost(), Integer.parseInt(ProxyParameters.getProxyPort()));
            }
        }
        catch (NumberFormatException e) {
            throw new IllegalArgumentException("Proxy Port is not a valid number.");
        }
    }

    public ValidationResult parseErrorsFromResponse(Element rootElement) throws JDOMException {
        List errorList = rootElement.getChildren("error");
        ValidationResult result = new ValidationResult();
        for (Element error : errorList) {
            String errorCode = error.getAttributeValue("code");
            String fieldReference = error.getAttributeValue("field");
            String severity = error.getAttributeValue("severity");
            String msg = error.getAttributeValue("msg");
            List links = error.getChildren("reference");
            String[] fieldLinks = new String[links.size()];
            for (int i = 0; i < links.size(); ++i) {
                String link;
                fieldLinks[i] = link = ((Element)links.get(i)).getText();
            }
            List replaceElems = error.getChildren("replace");
            String[] replaces = new String[replaceElems.size()];
            for (int i2 = 0; i2 < replaceElems.size(); ++i2) {
                String replace = ((Element)replaceElems.get(i2)).getText();
                replaces[i2] = !StringUtil.isEmpty(replace) ? new String(Base64Decoder.decodeBuffer(replace)) : "";
            }
            DeclValidationMessage declValidationMessage = severity != null ? new DeclValidationMessage(errorCode, fieldReference, fieldLinks, replaces, Severity.valueOf(severity)) : new DeclValidationMessage(errorCode, fieldReference, fieldLinks, replaces);
            declValidationMessage.set_MessageAndDecodeIt(msg);
            result.add(declValidationMessage);
        }
        return result;
    }

    public List<String> parseGenericErrorsFromResponse(Element rootElement) {
        ArrayList<String> genericErrorList = new ArrayList<String>();
        List errorList = rootElement.getChildren("erros");
        for (Element genericError : errorList) {
            List innerErrorsList = genericError.getChildren("error");
            for (Element innerError : innerErrorsList) {
                String msg = innerError.getChildText("msg");
                genericErrorList.add(!StringUtil.isEmpty(msg) ? new String(Base64Decoder.decodeBuffer(msg)) : "");
            }
        }
        return genericErrorList;
    }

    public String parseErrorMessageFromResponse() {
        throw new RuntimeException("Not implemented yet.");
    }

    public Map parseOptionalFromResponse(Element rootElement) throws JDOMException {
        List optionalFieldsList = rootElement.getChildren("optional");
        if (!(optionalFieldsList == null || optionalFieldsList.isEmpty())) {
            HashMap<String, String> parseMap = new HashMap<String, String>();
            int j = optionalFieldsList.size();
            for (int i = 0; i < j; ++i) {
                Element optional = (Element)optionalFieldsList.get(i);
                String key = optional.getAttributeValue("key");
                String value = optional.getAttributeValue("value");
                if (StringUtil.isEmpty(key) || StringUtil.isEmpty(value)) {
                    throw new JDOMException("Optional field has key ou value empty");
                }
                parseMap.put(key, value);
            }
            return parseMap;
        }
        return null;
    }

    public SubmitReceipt parseReceiptFromResponse(Element rootElement) throws JDOMException {
        List receiptList = rootElement.getChildren("receipt");
        if (!(receiptList == null || receiptList.isEmpty())) {
            if (receiptList.size() != 1) {
                throw new JDOMException("Resposta apenas deveria ter um comprovativo!! Tem " + receiptList.size() + " comprovativo.");
            }
            Element receiptElement = (Element)receiptList.get(0);
            SubmitReceipt submitReceipt = new SubmitReceipt();
            List nifs = receiptElement.getChildren("nif");
            for (Element nifElement : nifs) {
                submitReceipt.addUser(new SubmitUser(nifElement.getAttributeValue("name"), nifElement.getAttributeValue("nif"), null));
            }
            submitReceipt.setDate(new DateTime(receiptElement.getAttributeValue("date")));
            submitReceipt.setYear(receiptElement.getAttributeValue("year"));
            submitReceipt.setDeclId(receiptElement.getAttributeValue("declid"));
            return submitReceipt;
        }
        return null;
    }

    public List parseWarningsFromResponse(Element rootElement) throws JDOMException {
        List warningList = rootElement.getChildren("warning");
        if (!(warningList == null || warningList.isEmpty())) {
            ArrayList<String> parseList = new ArrayList<String>();
            int j = warningList.size();
            for (int i = 0; i < j; ++i) {
                Element warning = (Element)warningList.get(i);
                String msg = warning.getText();
                if (StringUtil.isEmpty(msg)) {
                    throw new JDOMException("Warning message is empty!");
                }
                parseList.add(Base64.decode(msg));
            }
            return parseList;
        }
        return null;
    }

    public List<String> parseInformationsFromResponse(Element rootElement) throws JDOMException {
        List informationsList = rootElement.getChildren("information");
        if (!(informationsList == null || informationsList.isEmpty())) {
            ArrayList<String> parseList = new ArrayList<String>();
            int j = informationsList.size();
            for (int i = 0; i < j; ++i) {
                Element information = (Element)informationsList.get(i);
                String msg = information.getText();
                if (StringUtil.isEmpty(msg)) {
                    throw new JDOMException("Information is empty!");
                }
                parseList.add(Base64.decode(msg));
            }
            return parseList;
        }
        return null;
    }

    public void receiveSubmitResponse(Document response) throws JDOMException {
        Element element = response.getRootElement();
        if (element == null) {
            throw new JDOMException("Root Element \u00e9 vazio!");
        }
        this.submitResponse = new SubmitResponse(Integer.parseInt(element.getAttributeValue("code")), this.parseReceiptFromResponse(element), this.parseErrorsFromResponse(element), this.parseWarningsFromResponse(element), this.parseOptionalFromResponse(element));
        List<String> informations = this.parseInformationsFromResponse(element);
        if (!ListUtil.isEmpty(informations)) {
            this.submitResponse.setInformations(informations);
        }
        if (this.submitResponse.getErrors() == null) {
            this.submitResponse.setErrors(this.parseGenericErrorsFromResponse(element));
        }
    }

    @Override
    public void submit(TaxClientRevampedSubmitInfo submitInfo) throws Exception {
        try {
            boolean parametersAdded = this.addRequestParametersToWebservice((PortalTaxClientRevampedSubmitInfo)submitInfo);
            if (!parametersAdded) {
                throw new IOException("Error occured when adding parameters to the request");
            }
            HttpResponse response = this.httpConnection.send();
            if (response == null) {
                throw new IOException("Response is null!");
            }
            XmlBuilder xmlBuilder = new XmlBuilder();
            List saxParseExceptions = xmlBuilder.parse(new StringReader(response.getBody().replaceAll("\r", "").replaceAll("\n", "").trim()));
            if (!saxParseExceptions.isEmpty()) {
                throw new JDOMException("There were errors while parsing response" + saxParseExceptions.get(0).toString());
            }
            Document result = xmlBuilder.getDocument();
            if (result == null) {
                throw new JDOMException("Document in response is null!");
            }
            this.receiveSubmitResponse(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    protected int getStateOfHttpPost() {
        return this.httpConnection.getCommunicationThreadStatus();
    }

    public String getStateDescriptionOfHttpPost() {
        switch (this.getStateOfHttpPost()) {
            case 0: {
                return "Comunicando com o servidor...";
            }
            case 2: {
                return "A receber resposta do servidor...";
            }
            case 1: {
                return "A enviar Declara\u00e7\u00e3o...";
            }
            case 3: {
                return "Comunica\u00e7\u00e3o terminada...";
            }
        }
        throw new IllegalStateException("HTTP Connection has invalid state");
    }

    protected void addParameter(String name, String value) {
        this.httpConnection.addParameter(name, WebServicesUtil.encode(value));
    }

    public SubmitResponse getSubmitResponse() {
        return this.submitResponse;
    }
}

