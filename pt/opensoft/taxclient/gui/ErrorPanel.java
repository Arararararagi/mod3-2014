/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import java.awt.Font;
import java.net.URL;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.swing.ScrollableHtmlPane;
import pt.opensoft.swing.Util;
import pt.opensoft.taxclient.gui.DeclValidationMessage;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.ArrayUtil;

public class ErrorPanel
extends ScrollableHtmlPane
implements HyperlinkListener {
    private static final long serialVersionUID = 7223232019251965908L;
    protected List _formSelectedListeners;
    private static final String BULLET = "<img src=\"" + GUIParameters.ICON_ERRO + "\" width=\"11\" height=\"11\"> &nbsp;&nbsp;";
    private static final URL BASE_URL = Util.class.getResource("/icons-small/" + GUIParameters.ICON_ERRO);

    public ErrorPanel() {
        this.setEditable(false);
        this.setContentType("text/html");
        this.addHyperlinkListener(this);
        ((HTMLDocument)this.getDocument()).setBase(BASE_URL);
        this.setText("<html>A declara\u00e7\u00e3o n\u00e3o apresenta erros.</html>");
        this.changeFont();
    }

    public void fillErrorsList(DeclaracaoModel model, ValidationResult result, int maxErros, MainFrame mainFrame) {
        String table_start_tag = "<table border=\"0\" width=\"100%\" cellpadding=\"1\" class=\"errors\">\n";
        String table_end_tag = "</table>\n";
        String start_row = "<tr>\n";
        String end_row = "</tr>\n";
        String anexo_start = "<td class=\"anexo\" colspan=\"2\">\n";
        String anexo_end = "</td>\n";
        String quadro_start = "<td class=\"quadro\" colspan=\"2\">\n";
        String quadro_end = "</td>\n";
        int numErros = result.getErrors().size();
        StringBuffer errorsList = new StringBuffer();
        errorsList.append("<html><table border=\"0\" width=\"100%\" cellpadding=\"1\" class=\"errors\"><tr><td colspan=\"2\">");
        if (numErros == 1) {
            errorsList.append("<b>Foi detectado 1 erro na declara\u00e7\u00e3o.</b>");
        } else {
            errorsList.append("<b>Foram detectados " + numErros + " erros na declara\u00e7\u00e3o.</b>");
        }
        if (numErros > maxErros) {
            errorsList.append("</td></tr><tr><td colspan=\"2\"><b>Apenas s\u00e3o mostrados os primeiros " + maxErros + " erros da declara\u00e7\u00e3o.</b>");
        }
        errorsList.append("</td></tr><tr></tr><tr><td colspan=\"2\" class=\"decl\">Erros da Declara\u00e7\u00e3o:</td></tr><tr><td width=\"15\">&nbsp;</td><td>");
        StringBuffer formErrorsList = new StringBuffer();
        String anexo = null;
        String field = null;
        String quadro = null;
        for (ValidationMessage message : result.getErrors()) {
            DeclValidationMessage validationMessage = (DeclValidationMessage)message;
            String[] tokens = ArrayUtil.toStringArray(validationMessage._fieldReference, ".");
            if (tokens.length > 0) {
                if (!tokens[0].equals(anexo)) {
                    if (anexo != null) {
                        formErrorsList.append(anexo_end);
                        errorsList.append(table_start_tag);
                        errorsList.append(formErrorsList);
                        quadro = null;
                    }
                    anexo = tokens[0];
                    errorsList.append(table_start_tag);
                    formErrorsList = new StringBuffer();
                    formErrorsList.append(anexo_start);
                    errorsList.append("Anexo : " + anexo);
                }
                if (tokens.length > 1) {
                    if (!tokens[1].equals(quadro)) {
                        quadro = tokens[1];
                        formErrorsList.append(start_row + quadro_start + "Erros do " + quadro + ":" + anexo_end + end_row);
                    }
                    if (tokens.length > 2) {
                        field = tokens[2];
                    }
                }
            }
            formErrorsList.append(start_row + "<td>" + BULLET + message.formattedText() + "</td>" + end_row);
        }
        formErrorsList.append(anexo_end);
        errorsList.append(table_start_tag);
        errorsList.append(formErrorsList);
        errorsList.append(table_end_tag + "</td></tr></table></html>");
        ((HTMLDocument)this.getDocument()).setBase(BASE_URL);
        this.setText(errorsList.toString());
        this.getAccessibleContext().setAccessibleDescription(errorsList.toString());
        this.changeFont();
    }

    public void fillErrorsList(DeclaracaoModel model, ValidationResult result, int maxErros) {
        this.fillErrorsList(model, result, maxErros, Session.getMainFrame());
    }

    private void changeFont() {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)this.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
    }
}

