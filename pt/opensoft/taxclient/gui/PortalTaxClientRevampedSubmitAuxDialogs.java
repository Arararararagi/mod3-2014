/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import java.awt.Frame;
import java.awt.HeadlessException;
import java.util.List;
import javax.swing.JFrame;
import pt.opensoft.swing.DialogFactory;
import pt.opensoft.taxclient.gui.SubmitDialog;

public class PortalTaxClientRevampedSubmitAuxDialogs
extends SubmitDialog {
    private static final long serialVersionUID = -2985430417318843031L;
    private static final String errorDialogTitle = "Submiss\u00e3o Falhou";
    private static final String errorDialogSubTitle = "Existem erros na declara\u00e7\u00e3o";
    private static final String errorFooter = "Por favor, corrija os erros e tente de novo.";
    private static final String errorToolTip = "Lista de erros detectados no servidor";
    private static final String warningDialogTitle = "Submiss\u00e3o da Declara\u00e7\u00e3o";
    private static final String warningDialogSubTitle = "De acordo com a informa\u00e7\u00e3o que possu\u00edmos:";
    private static final String warningFooter = "<html>Se submeter a declara\u00e7\u00e3o tal como se encontra, a mesma poder\u00e1 ser seleccionado para posterior an\u00e1lise.<br><br>Tem a certeza que deseja continuar com a submiss\u00e3o?</html>";
    private static final String warningToolTip = "Lista de avisos detectados no servidor";
    private static final String DEFAULT_TITLE = "Submiss\u00e3o Falhou";
    private static final String DEFAULT_SUB_TITLE = "Erros na Declara\u00e7\u00e3o";
    private static final String DEFAULT_HEADER = "Ocorreram os Seguintes Erros";
    private static final String DEFAULT_FOOTER = "Por favor, tente de novo";
    private static final String DEFAULT_TOOLTIP = "Lista de erros retornados pelo servidor";
    private static final String STYLE_FILE = "errorsPF.css";
    private static final int ERROR_WIDTH_SIZE = 650;
    private static final int ERROR_HEIGHT_SIZE = 350;
    private static final int WARNING_WIDTH_SIZE = 555;
    private static final int WARNING_HEIGHT_SIZE = 400;

    public PortalTaxClientRevampedSubmitAuxDialogs(List errors, int dialogType) throws HeadlessException {
        super((JFrame)DialogFactory.getDummyFrame(), dialogType, "Submiss\u00e3o Falhou", "Erros na Declara\u00e7\u00e3o", "Ocorreram os Seguintes Erros", "Por favor, tente de novo", PortalTaxClientRevampedSubmitAuxDialogs.convertToHtmlText(errors), "errorsPF.css", "Lista de erros retornados pelo servidor");
    }

    public static SubmitDialog getErrorSubmitDialog(Frame owner, List messages) {
        return new SubmitDialog(owner, 1, "Submiss\u00e3o Falhou", "Existem erros na declara\u00e7\u00e3o", null, "Por favor, corrija os erros e tente de novo.", PortalTaxClientRevampedSubmitAuxDialogs.convertToHtmlText(messages), "errorsPF.css", "Lista de erros detectados no servidor", 650, 350);
    }

    public static SubmitDialog getWarningSubmitDialog(Frame owner, List messages) {
        return new SubmitDialog(owner, 2, "Submiss\u00e3o da Declara\u00e7\u00e3o", "De acordo com a informa\u00e7\u00e3o que possu\u00edmos:", null, "<html>Se submeter a declara\u00e7\u00e3o tal como se encontra, a mesma poder\u00e1 ser seleccionado para posterior an\u00e1lise.<br><br>Tem a certeza que deseja continuar com a submiss\u00e3o?</html>", PortalTaxClientRevampedSubmitAuxDialogs.convertToHtmlText(messages), "errorsPF.css", "Lista de avisos detectados no servidor", 555, 400);
    }

    private static String convertToHtmlText(List errors) {
        if (errors == null || errors.isEmpty()) {
            return "";
        }
        StringBuffer buffer = new StringBuffer("<html><body>");
        for (String error : errors) {
            buffer.append("<h2>").append(error).append("</h2>");
        }
        buffer.append("</body></html>");
        return buffer.toString();
    }
}

