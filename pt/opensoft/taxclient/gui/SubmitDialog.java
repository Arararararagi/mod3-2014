/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;
import pt.opensoft.swing.Util;

public class SubmitDialog
extends JDialog
implements ActionListener {
    public static final int ERROR_DIALOG = 1;
    public static final int WARNING_DIALOG = 2;
    public static final int FECHAR_DIALOG = 1;
    public static final int CONTINUAR_DIALOG = 2;
    private static final String FECHAR_ACTION = "Cancelar";
    private static final String CONTINUAR_ACTION = "Continuar";
    private static final int DEFAULT_WIDTH_SIZE = 500;
    private static final int DEFAULT_HEIGHT_SIZE = 250;
    private static final URL BASE_URL = Util.class.getResource("/icons-small/error.png");
    private int actionPerfomed;

    public SubmitDialog(Frame owner, int dialogType, String dialogTitle, String dialogSubTitle, String header, String footer, String htmlText, String styleFile, String toolTip, int width, int height) throws HeadlessException {
        this(owner, dialogType, dialogTitle, dialogSubTitle, width, height);
        this.addBody(header, footer, htmlText, styleFile, toolTip);
    }

    public SubmitDialog(Frame owner, int dialogType, String dialogTitle, String dialogSubTitle, String header, String footer, String htmlText, String styleFile, String toolTip) throws HeadlessException {
        this(owner, dialogType, dialogTitle, dialogSubTitle, header, footer, htmlText, styleFile, toolTip, 500, 250);
    }

    private SubmitDialog(Frame owner, int dialogType, String dialogTitle, String dialogSubTitle, int width, int height) throws HeadlessException {
        super(owner);
        if (dialogType != 1 && dialogType != 2) {
            throw new IllegalArgumentException("Invalid dialog type");
        }
        this.setModal(true);
        this.setResizable(false);
        this.setTitle(dialogTitle);
        this.setSize(width, height);
        this.setLocationRelativeTo(owner);
        this.setResizable(true);
        this.getContentPane().setLayout(new BorderLayout(0, 10));
        this.getContentPane().add((Component)this.createTopToolbar(dialogType, dialogSubTitle), "North");
        this.getContentPane().add((Component)this.createBottomButtons(dialogType), "South");
        this.actionPerfomed = -1;
    }

    private void addBody(String header, String footer, String htmlText, String styleFile, String toolTip) {
        this.getContentPane().add((Component)this.createMessagePane(header, htmlText, styleFile, footer, toolTip), "Center");
    }

    private JToolBar createTopToolbar(int dialogType, String dialogSubTitle) {
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        JLabel jtitle = new JLabel(dialogSubTitle);
        this.setBiggerFont(jtitle, 2, true);
        toolBar.add(jtitle);
        toolBar.add(Box.createHorizontalGlue());
        return toolBar;
    }

    private JPanel createMessagePane(String header, String htmlText, String styleFile, String footer, String toolTip) {
        JPanel body = new JPanel(new BorderLayout(0, 10));
        if (header != null) {
            JLabel jheader = new JLabel(header);
            this.setBiggerFont(jheader, 0, true);
            body.add((Component)jheader, "North");
        }
        JEditorPane innerpane = new JEditorPane("text/html", "");
        StyleSheet css = new StyleSheet();
        css.importStyleSheet(Util.class.getResource(styleFile));
        innerpane.setDocument(new HTMLDocument(css));
        ((HTMLDocument)innerpane.getDocument()).setBase(BASE_URL);
        innerpane.setEditable(false);
        innerpane.setBorder(new LineBorder(UIManager.getColor("Label.color")));
        innerpane.setText(htmlText);
        this.changeFont(innerpane);
        innerpane.setCaretPosition(0);
        innerpane.setAutoscrolls(true);
        innerpane.setToolTipText(toolTip);
        JScrollPane scrollPane = new JScrollPane(innerpane);
        scrollPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        body.add((Component)scrollPane, "Center");
        JLabel jfooter = new JLabel(footer);
        this.setBiggerFont(jfooter, 0, true);
        body.add((Component)jfooter, "South");
        return body;
    }

    private JPanel createBottomButtons(int dialogType) {
        JPanel buttonPanel = new JPanel(new FlowLayout(1));
        if (dialogType == 2) {
            JButton continuarButton = new JButton("Continuar");
            continuarButton.addActionListener(this);
            buttonPanel.add(continuarButton);
        }
        JButton fecharButton = new JButton("Cancelar");
        fecharButton.addActionListener(this);
        buttonPanel.add(fecharButton);
        return buttonPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.actionPerfomed = e.getActionCommand().equals("Continuar") ? 2 : 1;
        this.dispose();
    }

    private void setBiggerFont(JLabel label, int increment, boolean bold) {
        Font tituloFont = label.getFont();
        if (bold) {
            label.setFont(new Font(tituloFont.getFontName(), 1, tituloFont.getSize() + increment));
        } else {
            label.setFont(new Font(tituloFont.getFontName(), 0, tituloFont.getSize() + increment));
        }
        label.setBorder(new EmptyBorder(5, 5, 5, 5));
    }

    public int getActionPerfomed() {
        return this.actionPerfomed;
    }

    private void changeFont(JEditorPane innerpane) {
        Font currentGlobalFont = UIManager.getFont("Label.font");
        StyledDocument doc = (StyledDocument)innerpane.getDocument();
        SimpleAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attr, currentGlobalFont.getFamily());
        StyleConstants.setFontSize(attr, currentGlobalFont.getSize());
        doc.setCharacterAttributes(0, doc.getLength(), attr, false);
    }
}

