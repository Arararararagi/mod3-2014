/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import com.jgoodies.validation.ValidationResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pt.opensoft.taxclient.http.SubmitReceipt;

public class SubmitResponse {
    private int errorCode;
    private List errors;
    private List warnings;
    private List infoWarnings;
    private SubmitReceipt submitReceipt;
    private Map optionalFields;
    private ValidationResult validationResult;
    private List<String> informations;

    public SubmitResponse(int errorCode) {
        this.errorCode = errorCode;
        this.errors = null;
        this.warnings = null;
        this.submitReceipt = null;
        this.optionalFields = null;
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt) {
        this.errorCode = errorCode;
        this.errors = null;
        this.warnings = null;
        this.submitReceipt = submitReceipt;
        this.optionalFields = null;
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt, ValidationResult result) {
        this.errorCode = errorCode;
        this.errors = null;
        this.warnings = null;
        this.submitReceipt = submitReceipt;
        this.validationResult = result;
        this.optionalFields = null;
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt, ValidationResult result, List warnings, Map optional) {
        this.errorCode = errorCode;
        this.errors = null;
        this.warnings = warnings;
        this.submitReceipt = submitReceipt;
        this.validationResult = result;
        this.optionalFields = optional;
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt, List errors, List warnings) {
        this.errorCode = errorCode;
        this.errors = errors;
        this.warnings = warnings;
        this.submitReceipt = submitReceipt;
        this.optionalFields = null;
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt, List errors, List warnings, Map optional) {
        this.errorCode = errorCode;
        this.errors = errors;
        this.warnings = warnings;
        this.submitReceipt = submitReceipt;
        this.optionalFields = optional;
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt, List errors, List warnings, List infoWarnings) {
        this(errorCode, submitReceipt, errors, warnings, infoWarnings, null);
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt, List errors, List warnings, List infoWarnings, Map optional) {
        this(errorCode, submitReceipt, errors, warnings, infoWarnings, optional, null);
    }

    public SubmitResponse(int errorCode, SubmitReceipt submitReceipt, List errors, List warnings, List infoWarnings, Map optional, List<String> informations) {
        this.errorCode = errorCode;
        this.submitReceipt = submitReceipt;
        this.errors = errors;
        this.warnings = warnings;
        this.infoWarnings = infoWarnings;
        this.optionalFields = optional;
        this.informations = informations;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public List getErrors() {
        return this.errors;
    }

    public void setErrors(List errors) {
        this.errors = errors;
    }

    public SubmitReceipt getSubmitReceipt() {
        return this.submitReceipt;
    }

    public void setSubmitReceipt(SubmitReceipt submitReceipt) {
        this.submitReceipt = submitReceipt;
    }

    public List getWarnings() {
        return this.warnings;
    }

    public void setWarnings(List warnings) {
        this.warnings = warnings;
    }

    public List getInfoWarnings() {
        return this.infoWarnings;
    }

    public void setInfoWarnings(List infoWarnings) {
        this.infoWarnings = infoWarnings;
    }

    public Map getOptionalFields() {
        return this.optionalFields;
    }

    public void addOptionalField(String key, String value) {
        if (key == null || value == null) {
            throw new NullPointerException("args are null");
        }
        if (this.optionalFields == null) {
            this.optionalFields = new HashMap();
        }
        this.optionalFields.put(key, value);
    }

    public ValidationResult getValidationResult() {
        return this.validationResult;
    }

    public void setValidationResult(ValidationResult validationResult) {
        this.validationResult = validationResult;
    }

    public List<String> getInformations() {
        return this.informations;
    }

    public void setInformations(List<String> informations) {
        this.informations = informations;
    }
}

