/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import java.io.InputStream;
import java.text.MessageFormat;
import pt.opensoft.util.SimpleParameters;

public class DesignationManager
extends SimpleParameters {
    private static DesignationManager instance = null;
    public static final String ANEXO_ERRORS_TITLE = DesignationManager.getInstance().getString("anexo.errors.title", "Erros do Anexo ");
    public static final String QUADRO_ERRORS_TITLE = DesignationManager.getInstance().getString("quadro.errors.title", "Erros do Quadro ");
    public static final String ANEXO_WARNINGS_TITLE = DesignationManager.getInstance().getString("anexo.warnings.title", "Avisos do Anexo ");
    public static final String QUADRO_WARNINGS_TITLE = DesignationManager.getInstance().getString("quadro.warnings.title", "Avisos do Quadro ");
    public static final String DECL_WARNINGS_TITLE = DesignationManager.getInstance().getString("decl.warnings.title", "Avisos Gerais da Declara\u00e7\u00e3o:");
    public static final String WARNINGS_TITLE = DesignationManager.getInstance().getString("warnings.title", "Foram detectados @VALUE avisos na Declara\u00e7\u00e3o");
    public static final String NO_WARNINGS_TITLE = DesignationManager.getInstance().getString("no.warnings.title", "A declara\u00e7\u00e3o n\u00e3o apresenta avisos.");
    public static final String ANEXO_CENTRAL_ERRORS_TITLE = DesignationManager.getInstance().getString("anexo.central.errors.title", "Erros do Anexo ");
    public static final String QUADRO_CENTRAL_ERRORS_TITLE = DesignationManager.getInstance().getString("quadro.central.errors.title", "Erros do Quadro ");
    public static final String SAVING_FILE_MESSAGE = DesignationManager.getInstance().getString("saving.file.message", "A gravar o ficheiro. Por favor, aguarde.");
    public static final String OPENING_FILE_MESSAGE = DesignationManager.getInstance().getString("opening.file.message", "A carregar o ficheiro. Por favor, aguarde.");
    public static final String SAVE_FILE_SUCCESS_MESSAGE = DesignationManager.getInstance().getString("save.file.success.message", "Ficheiro gravado com sucesso!");
    public static final MessageFormat MULTIPLE_ANEXO_DUPLICATED_MESSAGE = new MessageFormat(DesignationManager.getInstance().getString("multiple.anexo.duplicated.message", "O anexo {0}({1}) j\u00e1 existe na declara\u00e7\u00e3o."));
    public static final String REMOVE_ANEXO_POPUP_TEXT = DesignationManager.getInstance().getString("remove.anexo.popup.text", "Apagar Anexo");

    private DesignationManager() {
        this.read(this.getClass().getResourceAsStream("designation.properties"));
    }

    public static DesignationManager getInstance() {
        if (instance == null) {
            instance = new DesignationManager();
        }
        return instance;
    }
}

