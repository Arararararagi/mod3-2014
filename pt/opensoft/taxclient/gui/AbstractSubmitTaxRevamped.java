/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;

public abstract class AbstractSubmitTaxRevamped {
    public abstract void submit(TaxClientRevampedSubmitInfo var1) throws Exception;

    public abstract void initializeConnection() throws IllegalArgumentException;
}

