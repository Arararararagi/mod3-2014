/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import java.net.MalformedURLException;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.gui.SubmitTaxThread;

public class SubmitTaxThreadJAXWS
extends SubmitTaxThread {
    public SubmitTaxThreadJAXWS(String url, TaxClientRevampedSubmitInfo submitRequest) {
        super(url, submitRequest);
    }

    @Override
    public void initSubmitTax() throws MalformedURLException {
    }

    @Override
    public String getStatusDescr() {
        return null;
    }
}

