/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import java.net.MalformedURLException;
import java.net.URL;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.gui.AbstractSubmitTaxRevamped;
import pt.opensoft.taxclient.gui.SubmitResponse;
import pt.opensoft.taxclient.gui.SubmitTaxHTTPConnectionRevamped;
import pt.opensoft.taxclient.gui.SubmitTaxThread;

public class SubmitTaxThreadHttpConnection
extends SubmitTaxThread {
    public SubmitTaxThreadHttpConnection(String url, TaxClientRevampedSubmitInfo submitRequest) {
        super(url, submitRequest);
    }

    @Override
    public void initSubmitTax() throws MalformedURLException {
        this.submitTax = new SubmitTaxHTTPConnectionRevamped(new URL(this.url));
    }

    @Override
    public String getStatusDescr() {
        if (this.submitTax == null) {
            return "Comunicando com o servidor...";
        }
        return ((SubmitTaxHTTPConnectionRevamped)this.submitTax).getStateDescriptionOfHttpPost();
    }

    public SubmitResponse getSubmitResponse() {
        return ((SubmitTaxHTTPConnectionRevamped)this.submitTax).getSubmitResponse();
    }
}

