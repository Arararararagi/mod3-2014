/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.gui;

import java.net.MalformedURLException;
import pt.opensoft.taxclient.actions.util.TaxClientRevampedSubmitInfo;
import pt.opensoft.taxclient.gui.AbstractSubmitTaxRevamped;

public abstract class SubmitTaxThread
extends Thread
implements Runnable {
    private Exception exception = null;
    private boolean completed = false;
    private boolean sucess = false;
    protected AbstractSubmitTaxRevamped submitTax;
    protected String url;
    private TaxClientRevampedSubmitInfo submitRequest;

    public SubmitTaxThread(String url, TaxClientRevampedSubmitInfo submitRequest) {
        this.url = url;
        this.submitRequest = submitRequest;
    }

    @Override
    public void run() {
        try {
            this.initSubmitTax();
            this.submitTax.submit(this.submitRequest);
            this.sucess = true;
        }
        catch (Exception e) {
            this.exception = e;
        }
        finally {
            this.completed = true;
        }
    }

    public abstract void initSubmitTax() throws MalformedURLException;

    public boolean isSucessfull() {
        return this.sucess;
    }

    public boolean isCompleted() {
        return this.completed;
    }

    public Exception getException() {
        return this.exception;
    }

    public abstract String getStatusDescr();
}

