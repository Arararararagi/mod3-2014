/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import java.util.List;
import pt.opensoft.taxclient.http.GetDeclReceipt;

public class GetDeclResponse {
    private int errorCode;
    private List errors;
    private GetDeclReceipt getDeclReceipt;

    public GetDeclResponse(int errorCode) {
        this(errorCode, null, null);
    }

    public GetDeclResponse(int errorCode, GetDeclReceipt getDeclReceipt) {
        this(errorCode, getDeclReceipt, null);
    }

    public GetDeclResponse(int errorCode, GetDeclReceipt getDeclReceipt, List errors) {
        this.errorCode = errorCode;
        this.getDeclReceipt = getDeclReceipt;
        this.errors = errors;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public GetDeclReceipt getDeclReceipt() {
        return this.getDeclReceipt;
    }

    public void setDeclReceipt(GetDeclReceipt getDeclReceipt) {
        this.getDeclReceipt = getDeclReceipt;
    }

    public List getErrors() {
        return this.errors;
    }

    public void setErrors(List errors) {
        this.errors = errors;
    }
}

