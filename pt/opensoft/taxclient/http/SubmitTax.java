/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import pt.opensoft.crypt.Password;
import pt.opensoft.http.HttpResponse;
import pt.opensoft.http.JavaHttpConnection;
import pt.opensoft.taxclient.app.ProxyParameters;
import pt.opensoft.taxclient.gui.SubmitResponse;
import pt.opensoft.taxclient.http.SubmitReceipt;
import pt.opensoft.taxclient.http.SubmitRequest;
import pt.opensoft.taxclient.http.SubmitUser;
import pt.opensoft.taxclient.http.SubmitWebserviceRequest;
import pt.opensoft.text.Appender;
import pt.opensoft.text.Base64;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;
import pt.opensoft.xml.XmlBuilder;
import pt.opensoft.xml.XmlRenderer;

public class SubmitTax {
    public static final String SUJ_PASS_A_ID = "SPA";
    public static final String SUJ_PASS_B_ID = "SPB";
    public static final String TOC_ID = "TOC";
    public static final int RESPONSE_CODE_OK = 0;
    public static final int RESPONSE_CODE_WARNINGS = 95;
    public static final int RESPONSE_CODE_ERROR_INVALID_REQUEST = 96;
    public static final int RESPONSE_CODE_EXCEPTION = 97;
    public static final int RESPONSE_CODE_UNAUTHORIZED = 98;
    public static final int RESPONSE_CODE_NOK = 99;
    public static final int RESPONSE_CODE_OUTDATED_VERSION = 94;
    public static final int RESPONSE_CODE_INFO = 93;
    public static final int RESPONSE_CODE_NO_DATA = 92;
    public static final String HTTP_STATE_IDDLE = "Comunicando com o servidor...";
    private static final String HTTP_STATE_SEND = "A enviar Declara\u00e7\u00e3o...";
    private static final String HTTP_STATE_RECEIVE = "A receber resposta do servidor...";
    private static final String HTTP_STATE_FINISH = "Comunica\u00e7\u00e3o terminada...";
    private static final String ENCODE = "UTF-8";
    private static final int TIMEOUT = ProxyParameters.getConnectionTimeout();
    private URL url;
    private JavaHttpConnection httpConnection;
    private SubmitResponse submitResponse;
    private static final String ROOT_ELEMENT = "submit_request";
    private static final String DECL_TAG = "decl";
    private static final String CHECK_TAG = "checkwarnings";
    private static final String NIF_TAG = "nif";
    private static final String NIF_ATTRIBUTE_ID = "id";
    private static final String NIF_ATTRIBUTE_NIF = "nif";
    private static final String NIF_ATTRIBUTE_PASSWORD = "password";
    private static final String POST_NAME = "tax";
    private static final String DE_SYSTEM_NAME = "taxUser";
    private static final String DE_SYSTEM_PASS = "y5jeC$maQe";
    private static final String SYSTEM_NAME_TAG = "systemName";
    private static final String SYSTEM_PASS_TAG = "systemPass";
    private static final String APP_VERSION_TAG = "app_version";
    private static final String DECL_VERSION_TAG = "declVersion";
    private static final String USER_NIF_A_TAG = "nifA";
    private static final String USER_PASS_A_TAG = "passwordA";
    private static final String USER_NIF_B_TAG = "nifB";
    private static final String USER_PASS_B_TAG = "passwordB";
    private static final String USER_NIF_TOC_TAG = "nifTOC";
    private static final String USER_PASS_TOC_TAG = "passwordTOC";
    private static final String RESPONSE_PARAMETER_CODE = "code";
    private static final String RESPONSE_PARAMETER_ERROR = "error";
    private static final String RESPONSE_PARAMETER_WARNING = "warning";
    private static final String RESPONSE_PARAMETER_INFOWARNING = "infoWarning";
    private static final String RESPONSE_PARAMETER_INFORMATION = "information";
    private static final String RESPONSE_PARAMETER_RECEIPT = "receipt";
    private static final String RESPONSE_PARAMETER_OPTIONAL = "optional";
    private static final String RESPONSE_PARAMETER_NIFS = "nif";
    private static final String RESPONSE_RECEIPT_DATE = "date";
    private static final String RESPONSE_RECEIPT_YEAR = "year";
    private static final String RESPONSE_RECEIPT_DECLID = "declid";
    private static final String RESPONSE_NIF_NAME = "name";
    private static final String RESPONSE_NIF_VALUE = "nif";
    private static final String RESPONSE_OPTIONAL_NAME = "key";
    private static final String RESPONSE_OPTIONAL_VALUE = "value";

    public SubmitTax(URL url) {
        if (url == null) {
            throw new NullPointerException("url cannot be null");
        }
        this.url = url;
        this.initializeConnection();
    }

    public void submit(SubmitRequest submitRequest) throws Exception {
        try {
            boolean parametersAdded;
            boolean bl = parametersAdded = submitRequest instanceof SubmitWebserviceRequest ? this.addRequestParametersToWebservice((SubmitWebserviceRequest)submitRequest) : this.addRequestParameters(submitRequest);
            if (!parametersAdded) {
                throw new IOException("Error occured when adding parameters to the request");
            }
            HttpResponse response = this.httpConnection.send();
            if (response == null) {
                throw new IOException("Response is null!");
            }
            XmlBuilder xmlBuilder = new XmlBuilder();
            List saxParseExceptions = xmlBuilder.parse(new StringReader(response.getBody()));
            if (!saxParseExceptions.isEmpty()) {
                throw new JDOMException("There were errors while parsing response");
            }
            Document result = xmlBuilder.getDocument();
            if (result == null) {
                throw new JDOMException("Document in response is null!");
            }
            this.receiveSubmitResponse(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private boolean addRequestParameters(SubmitRequest submitRequest) {
        XmlRenderer xmlRenderer = new XmlRenderer("<?xml version='1.0' encoding='ISO-8859-1'?>");
        xmlRenderer.startTag("submit_request", false);
        xmlRenderer.tag("app_version", "value=\"" + submitRequest.getAppVersion() + "\"", false);
        xmlRenderer.startTag("decl", false);
        xmlRenderer.append(XmlRenderer.cdata(submitRequest.getDeclaracaoToRequest()));
        xmlRenderer.endTag("decl", false);
        for (SubmitUser submitUser : submitRequest.getUsers()) {
            xmlRenderer.tag("nif", "id=\"" + submitUser.getId() + "\" " + "nif" + "=\"" + submitUser.getNif() + "\" " + "password" + "=\"" + submitUser.getPassword() + "\"", false);
        }
        for (Pair optionalField : submitRequest.getOptionalFields()) {
            xmlRenderer.tag((String)optionalField.getFirst(), optionalField.getSecond());
        }
        xmlRenderer.tag("checkwarnings", "value=\"" + submitRequest.isCheckWarnings() + "\"", false);
        xmlRenderer.endTag("submit_request", false);
        this.addParameter("tax", xmlRenderer.toString());
        return true;
    }

    private boolean addRequestParametersToWebservice(SubmitWebserviceRequest submitRequest) {
        this.addParameter("systemName", "taxUser");
        this.addParameter("systemPass", Password.encryptWithPaddingLength("y5jeC$maQe"));
        this.addParameter("decl", submitRequest.getDeclaracaoToRequest());
        this.addParameter("declVersion", submitRequest.getDeclVersion());
        if (submitRequest.hasAppVersion()) {
            this.addParameter("app_version", submitRequest.getAppVersion());
        }
        this.addParameter("checkwarnings", String.valueOf(submitRequest.isCheckWarnings()));
        for (SubmitUser submitUser : submitRequest.getUsers()) {
            if (submitUser.getId().equals("SPA")) {
                this.addParameter("nifA", submitUser.getNif());
                this.addParameter("passwordA", submitUser.getPassword());
                continue;
            }
            if (submitUser.getId().equals("SPB")) {
                this.addParameter("nifB", submitUser.getNif());
                this.addParameter("passwordB", submitUser.getPassword());
                continue;
            }
            if (!submitUser.getId().equals("TOC")) continue;
            this.addParameter("nifTOC", submitUser.getNif());
            this.addParameter("passwordTOC", submitUser.getPassword());
        }
        for (Pair optionalField : submitRequest.getOptionalFields()) {
            this.addParameter((String)optionalField.getFirst(), (String)optionalField.getSecond());
        }
        return true;
    }

    private void receiveSubmitResponse(Document response) throws JDOMException {
        Element element = response.getRootElement();
        if (element == null) {
            throw new JDOMException("Root Element \u00e9 vazio!");
        }
        this.submitResponse = new SubmitResponse(Integer.parseInt(element.getAttributeValue("code")), this.parseReceiptFromResponse(element), this.parseErrorsFromResponse(element), this.parseWarningsFromResponse(element), this.parseInfoWarningsFromResponse(element), this.parseOptionalFromResponse(element), this.parseInformationsFromResponse(element));
    }

    private List parseErrorsFromResponse(Element rootElement) throws JDOMException {
        List errorList = rootElement.getChildren("error");
        if (!(errorList == null || errorList.isEmpty())) {
            ArrayList<String> parseList = new ArrayList<String>();
            int j = errorList.size();
            for (int i = 0; i < j; ++i) {
                Element error = (Element)errorList.get(i);
                String msg = error.getText();
                if (StringUtil.isEmpty(msg)) {
                    throw new JDOMException("Error has emtpy message!");
                }
                parseList.add(Base64.decode(msg));
            }
            return parseList;
        }
        return null;
    }

    private List parseWarningsFromResponse(Element rootElement) throws JDOMException {
        List warningList = rootElement.getChildren("warning");
        if (!(warningList == null || warningList.isEmpty())) {
            ArrayList<String> parseList = new ArrayList<String>();
            int j = warningList.size();
            for (int i = 0; i < j; ++i) {
                Element warning = (Element)warningList.get(i);
                String msg = warning.getText();
                if (StringUtil.isEmpty(msg)) {
                    throw new JDOMException("Warning message is empty!");
                }
                parseList.add(Base64.decode(msg));
            }
            return parseList;
        }
        return null;
    }

    private List parseInfoWarningsFromResponse(Element rootElement) throws JDOMException {
        List infoWarningList = rootElement.getChildren("infoWarning");
        if (!(infoWarningList == null || infoWarningList.isEmpty())) {
            ArrayList<String> parseList = new ArrayList<String>();
            int j = infoWarningList.size();
            for (int i = 0; i < j; ++i) {
                Element infoWarning = (Element)infoWarningList.get(i);
                String msg = infoWarning.getText();
                if (StringUtil.isEmpty(msg)) {
                    throw new JDOMException("Info warning message is empty!");
                }
                parseList.add(Base64.decode(msg));
            }
            return parseList;
        }
        return null;
    }

    private List<String> parseInformationsFromResponse(Element rootElement) throws JDOMException {
        List informationsList = rootElement.getChildren("information");
        if (!(informationsList == null || informationsList.isEmpty())) {
            ArrayList<String> parseList = new ArrayList<String>();
            int j = informationsList.size();
            for (int i = 0; i < j; ++i) {
                Element information = (Element)informationsList.get(i);
                String msg = information.getText();
                if (StringUtil.isEmpty(msg)) {
                    throw new JDOMException("Information is empty!");
                }
                parseList.add(Base64.decode(msg));
            }
            return parseList;
        }
        return null;
    }

    private SubmitReceipt parseReceiptFromResponse(Element rootElement) throws JDOMException {
        List receiptList = rootElement.getChildren("receipt");
        if (!(receiptList == null || receiptList.isEmpty())) {
            if (receiptList.size() != 1) {
                throw new JDOMException("Resposta apenas deveria ter um comprovativo!! Tem " + receiptList.size() + " comprovativo.");
            }
            Element receiptElement = (Element)receiptList.get(0);
            SubmitReceipt submitReceipt = new SubmitReceipt();
            submitReceipt.setDate(new DateTime(receiptElement.getAttributeValue("date")));
            submitReceipt.setYear(receiptElement.getAttributeValue("year"));
            submitReceipt.setDeclId(receiptElement.getAttributeValue("declid"));
            List nifs = receiptElement.getChildren("nif");
            for (Element nifElement : nifs) {
                submitReceipt.addUser(new SubmitUser(nifElement.getAttributeValue("name"), nifElement.getAttributeValue("nif"), null));
            }
            return submitReceipt;
        }
        return null;
    }

    private Map parseOptionalFromResponse(Element rootElement) throws JDOMException {
        List optionalFieldsList = rootElement.getChildren("optional");
        if (!(optionalFieldsList == null || optionalFieldsList.isEmpty())) {
            HashMap<String, String> parseMap = new HashMap<String, String>();
            int j = optionalFieldsList.size();
            for (int i = 0; i < j; ++i) {
                Element optional = (Element)optionalFieldsList.get(i);
                String key = optional.getAttributeValue("key");
                String value = optional.getAttributeValue("value");
                if (StringUtil.isEmpty(key) || StringUtil.isEmpty(value)) {
                    throw new JDOMException("Optional field has key ou value empty");
                }
                parseMap.put(key, value);
            }
            return parseMap;
        }
        return null;
    }

    public SubmitResponse getSubmitResponse() {
        return this.submitResponse;
    }

    private void initializeConnection() throws IllegalArgumentException {
        this.httpConnection = new JavaHttpConnection(this.url);
        this.httpConnection.setTimeout(TIMEOUT);
        try {
            if (ProxyParameters.getUseProxy() && ProxyParameters.isProxySet()) {
                this.httpConnection.setProxy(ProxyParameters.getProxyHost(), Integer.parseInt(ProxyParameters.getProxyPort()));
            }
        }
        catch (NumberFormatException e) {
            throw new IllegalArgumentException("Proxy Port is not a valid number.");
        }
    }

    private int getStateOfHttpPost() {
        return this.httpConnection.getCommunicationThreadStatus();
    }

    public String getStateDescriptionOfHttpPost() {
        switch (this.getStateOfHttpPost()) {
            case 0: {
                return "Comunicando com o servidor...";
            }
            case 2: {
                return "A receber resposta do servidor...";
            }
            case 1: {
                return "A enviar Declara\u00e7\u00e3o...";
            }
            case 3: {
                return "Comunica\u00e7\u00e3o terminada...";
            }
        }
        throw new IllegalStateException("HTTP Connection has invalid state");
    }

    private void addParameter(String name, String value) {
        this.httpConnection.addParameter(name, SubmitTax.encode(value));
    }

    private static String encode(String message) {
        try {
            return URLEncoder.encode(Base64.encode(message), "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}

