/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

public class GetDeclReceipt {
    private String declId;
    private String status;
    private String impressos;
    private String nifA;
    private String nomeA;
    private String nifB;
    private String nomeB;
    private String repFin;
    private String exerc;
    private String errosCentrais;

    public String getDeclId() {
        return this.declId;
    }

    public void setDeclId(String declId) {
        this.declId = declId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExerc() {
        return this.exerc;
    }

    public void setExerc(String exerc) {
        this.exerc = exerc;
    }

    public String getErrosCentrais() {
        return this.errosCentrais;
    }

    public void setErrosCentrais(String errosCentrais) {
        this.errosCentrais = errosCentrais;
    }

    public String getImpressos() {
        return this.impressos;
    }

    public void setImpressos(String impressos) {
        this.impressos = impressos;
    }

    public String getNifA() {
        return this.nifA;
    }

    public void setNifA(String nifA) {
        this.nifA = nifA;
    }

    public String getNifB() {
        return this.nifB;
    }

    public void setNifB(String nifB) {
        this.nifB = nifB;
    }

    public String getNomeA() {
        return this.nomeA;
    }

    public void setNomeA(String nomeA) {
        this.nomeA = nomeA;
    }

    public String getNomeB() {
        return this.nomeB;
    }

    public void setNomeB(String nomeB) {
        this.nomeB = nomeB;
    }

    public String getRepFin() {
        return this.repFin;
    }

    public void setRepFin(String repFin) {
        this.repFin = repFin;
    }
}

