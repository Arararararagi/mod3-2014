/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import pt.opensoft.crypt.Password;
import pt.opensoft.http.HttpResponse;
import pt.opensoft.http.JavaHttpConnection;
import pt.opensoft.taxclient.app.ProxyParameters;
import pt.opensoft.taxclient.http.GetDeclReceipt;
import pt.opensoft.taxclient.http.GetDeclRequest;
import pt.opensoft.taxclient.http.GetDeclResponse;
import pt.opensoft.taxclient.http.GetDeclUser;
import pt.opensoft.taxclient.http.GetDeclWebserviceRequest;
import pt.opensoft.text.Base64;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.ZipUtil;
import pt.opensoft.xml.XmlBuilder;
import pt.opensoft.xml.XmlRenderer;

public class GetDeclTax {
    public static final String SUJ_PASS_A_ID = "SPA";
    public static final String SUJ_PASS_B_ID = "SPB";
    public static final int RESPONSE_CODE_OK = 0;
    public static final int RESPONSE_CODE_ERROR_INVALID_REQUEST = 96;
    public static final int RESPONSE_CODE_EXCEPTION = 97;
    public static final int RESPONSE_CODE_UNAUTHORIZED = 98;
    public static final int RESPONSE_CODE_NOK = 99;
    public static final String HTTP_STATE_IDDLE = "Comunicando com o servidor...";
    private static final String HTTP_STATE_SEND = "A obter Declara\u00e7\u00e3o...";
    private static final String HTTP_STATE_RECEIVE = "A receber resposta do servidor...";
    private static final String HTTP_STATE_FINISH = "Comunica\u00e7\u00e3o terminada...";
    private static final String ENCODE = "UTF-8";
    private static final int TIMEOUT = ProxyParameters.getConnectionTimeout();
    private URL url;
    private JavaHttpConnection httpConnection;
    private GetDeclResponse getDeclResponse;
    private static final String ROOT_ELEMENT = "getDecl_request";
    private static final String NIF_TAG = "nif";
    private static final String NIF_ATTRIBUTE_ID = "id";
    private static final String NIF_ATTRIBUTE_NIF = "nif";
    private static final String NIF_ATTRIBUTE_PASSWORD = "password";
    private static final String POST_NAME = "tax";
    private static final String DE_SYSTEM_NAME = "taxUser";
    private static final String DE_SYSTEM_PASS = "y5jeC$maQe";
    private static final String SYSTEM_NAME_TAG = "systemName";
    private static final String SYSTEM_PASS_TAG = "systemPass";
    private static final String APP_VERSION_TAG = "app_version";
    private static final String DECL_VERSION_TAG = "declVersion";
    private static final String EXERCICIO_TAG = "exercicio";
    private static final String USER_NIF_A_TAG = "nifA";
    private static final String USER_PASS_A_TAG = "passwordA";
    private static final String USER_NIF_B_TAG = "nifB";
    private static final String USER_PASS_B_TAG = "passwordB";
    private static final String RESPONSE_PARAMETER_CODE = "code";
    private static final String RESPONSE_PARAMETER_RECEIPT = "receipt";
    private static final String RESPONSE_RECEIPT_DECLID = "declid";
    private static final String RESPONSE_RECEIPT_STATUS = "status";
    private static final String RESPONSE_RECEIPT_NIF_A = "nifA";
    private static final String RESPONSE_RECEIPT_NOME_A = "nomeA";
    private static final String RESPONSE_RECEIPT_NIF_B = "nifB";
    private static final String RESPONSE_RECEIPT_NOME_B = "nomeB";
    private static final String RESPONSE_RECEIPT_REPFIN = "repFin";
    private static final String RESPONSE_RECEIPT_EXERC = "exerc";
    private static final String RESPONSE_RECEIPT_IMPRESSOS = "impressos";
    private static final String RESPONSE_RECEIPT_ERROS = "errosCentrais";

    public GetDeclTax(URL url) {
        if (url == null) {
            throw new NullPointerException("url cannot be null");
        }
        this.url = url;
        this.initializeConnection();
    }

    public void getDecl(GetDeclRequest getDeclRequest) throws Exception {
        try {
            boolean parametersAdded;
            boolean bl = parametersAdded = getDeclRequest instanceof GetDeclWebserviceRequest ? this.addRequestParametersToWebservice((GetDeclWebserviceRequest)getDeclRequest) : this.addRequestParameters(getDeclRequest);
            if (!parametersAdded) {
                throw new IOException("Error occured when adding parameters to the request");
            }
            HttpResponse response = this.httpConnection.send();
            if (response == null) {
                throw new IOException("Response is null!");
            }
            XmlBuilder xmlBuilder = new XmlBuilder();
            List saxParseExceptions = xmlBuilder.parse(new StringReader(response.getBody()));
            if (!saxParseExceptions.isEmpty()) {
                throw new JDOMException("There were errors while parsing response");
            }
            Document result = xmlBuilder.getDocument();
            if (result == null) {
                throw new JDOMException("Document in response is null!");
            }
            this.receiveGetDeclResponse(result);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private boolean addRequestParameters(GetDeclRequest getDeclRequest) {
        XmlRenderer xmlRenderer = new XmlRenderer("<?xml version='1.0' encoding='ISO-8859-1'?>");
        xmlRenderer.startTag("getDecl_request", false);
        xmlRenderer.tag("app_version", "value=\"" + getDeclRequest.getAppVersion() + "\"", false);
        for (GetDeclUser getDeclUser : getDeclRequest.getUsers()) {
            xmlRenderer.tag("nif", "id=\"" + getDeclUser.getId() + "\" " + "nif" + "=\"" + getDeclUser.getNif() + "\" " + "password" + "=\"" + getDeclUser.getPassword() + "\"", false);
        }
        xmlRenderer.tag("app_version", "value=\"" + getDeclRequest.getAppVersion() + "\"", false);
        xmlRenderer.endTag("getDecl_request", false);
        this.addParameter("tax", xmlRenderer.toString());
        return true;
    }

    private boolean addRequestParametersToWebservice(GetDeclWebserviceRequest getDeclRequest) {
        this.addParameter("systemName", "taxUser");
        this.addParameter("systemPass", Password.encryptWithPaddingLength("y5jeC$maQe"));
        this.addParameter("declVersion", getDeclRequest.getDeclVersion());
        this.addParameter("exercicio", getDeclRequest.getExercicio());
        if (getDeclRequest.hasAppVersion()) {
            this.addParameter("app_version", getDeclRequest.getAppVersion());
        }
        for (GetDeclUser getDeclUser : getDeclRequest.getUsers()) {
            if (getDeclUser.getId().equals("SPA")) {
                this.addParameter("nifA", getDeclUser.getNif());
                this.addParameter("passwordA", getDeclUser.getPassword());
                continue;
            }
            if (!getDeclUser.getId().equals("SPB")) continue;
            this.addParameter("nifB", getDeclUser.getNif());
            this.addParameter("passwordB", getDeclUser.getPassword());
        }
        return true;
    }

    private void receiveGetDeclResponse(Document response) throws JDOMException, Exception {
        Element element = response.getRootElement();
        if (element == null) {
            throw new JDOMException("Root Element \u00e9 vazio!");
        }
        this.getDeclResponse = new GetDeclResponse(Integer.parseInt(element.getAttributeValue("code")), this.parseReceiptFromResponse(element));
    }

    private GetDeclReceipt parseReceiptFromResponse(Element rootElement) throws JDOMException, Exception {
        List receiptList = rootElement.getChildren("receipt");
        if (!(receiptList == null || receiptList.isEmpty())) {
            if (receiptList.size() != 1) {
                throw new JDOMException("Resposta apenas deveria ter um comprovativo!! Tem " + receiptList.size() + " comprovativo.");
            }
            Element receiptElement = (Element)receiptList.get(0);
            GetDeclReceipt getDeclReceipt = new GetDeclReceipt();
            getDeclReceipt.setDeclId(receiptElement.getAttributeValue("declid"));
            getDeclReceipt.setStatus(receiptElement.getAttributeValue("status"));
            getDeclReceipt.setNifA(receiptElement.getAttributeValue("nifA"));
            getDeclReceipt.setNomeA(receiptElement.getAttributeValue("nomeA"));
            getDeclReceipt.setNifB(receiptElement.getAttributeValue("nifB"));
            getDeclReceipt.setNomeB(receiptElement.getAttributeValue("nomeB"));
            getDeclReceipt.setRepFin(receiptElement.getAttributeValue("repFin"));
            getDeclReceipt.setExerc(receiptElement.getAttributeValue("exerc"));
            String impressos = StringUtil.isEmpty(receiptElement.getAttributeValue("impressos")) ? "" : ZipUtil.inflate(Base64.decode(receiptElement.getAttributeValue("impressos")));
            getDeclReceipt.setImpressos(impressos);
            String errosCentrais = StringUtil.isEmpty(receiptElement.getAttributeValue("errosCentrais")) ? "" : ZipUtil.inflate(Base64.decode(receiptElement.getAttributeValue("errosCentrais")));
            getDeclReceipt.setErrosCentrais(errosCentrais);
            return getDeclReceipt;
        }
        return null;
    }

    public GetDeclResponse getGetDeclResponse() {
        return this.getDeclResponse;
    }

    private void initializeConnection() throws IllegalArgumentException {
        this.httpConnection = new JavaHttpConnection(this.url);
        this.httpConnection.setTimeout(TIMEOUT);
        try {
            if (ProxyParameters.getUseProxy() && ProxyParameters.isProxySet()) {
                this.httpConnection.setProxy(ProxyParameters.getProxyHost(), Integer.parseInt(ProxyParameters.getProxyPort()));
            }
        }
        catch (NumberFormatException e) {
            throw new IllegalArgumentException("Proxy Port is not a valid number.");
        }
    }

    private int getStateOfHttpPost() {
        return this.httpConnection.getCommunicationThreadStatus();
    }

    public String getStateDescriptionOfHttpPost() {
        switch (this.getStateOfHttpPost()) {
            case 0: {
                return "Comunicando com o servidor...";
            }
            case 2: {
                return "A receber resposta do servidor...";
            }
            case 1: {
                return "A obter Declara\u00e7\u00e3o...";
            }
            case 3: {
                return "Comunica\u00e7\u00e3o terminada...";
            }
        }
        throw new IllegalStateException("HTTP Connection has invalid state");
    }

    private void addParameter(String name, String value) {
        this.httpConnection.addParameter(name, GetDeclTax.encode(value));
    }

    private static String encode(String message) {
        try {
            return URLEncoder.encode(Base64.encode(message), "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}

