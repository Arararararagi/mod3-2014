/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import java.util.ArrayList;
import java.util.List;
import pt.opensoft.taxclient.http.GetDeclUser;

public class GetDeclRequest {
    protected String appVersion;
    protected String exercicio;
    protected List users = new ArrayList();

    public String getAppVersion() {
        return this.appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getExercicio() {
        return this.exercicio;
    }

    public void setExercicio(String exercicio) {
        this.exercicio = exercicio;
    }

    public List getUsers() {
        return this.users;
    }

    public void addUser(GetDeclUser user) {
        if (user == null) {
            throw new NullPointerException("User \u00e9 null");
        }
        this.users.add(user);
    }
}

