/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import java.util.ArrayList;
import java.util.List;
import pt.opensoft.taxclient.http.SubmitUser;
import pt.opensoft.util.DateTime;

public class SubmitReceipt {
    private DateTime date;
    private String year;
    private String declId;
    private List users;

    public DateTime getDate() {
        return this.date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getDeclId() {
        return this.declId;
    }

    public void setDeclId(String declId) {
        this.declId = declId;
    }

    public String getYear() {
        return this.year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List getUsers() {
        return this.users;
    }

    public void setUsers(List users) {
        this.users = users;
    }

    public void addUser(SubmitUser user) {
        if (this.users == null) {
            this.users = new ArrayList();
        }
        this.users.add(user);
    }
}

