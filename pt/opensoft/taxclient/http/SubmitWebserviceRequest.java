/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import pt.opensoft.taxclient.http.SubmitRequest;
import pt.opensoft.util.StringUtil;

public class SubmitWebserviceRequest
extends SubmitRequest {
    protected String declVersion;

    public SubmitWebserviceRequest(String declVersion) {
        this.declVersion = declVersion;
    }

    public boolean hasAppVersion() {
        return !StringUtil.isEmpty(this.appVersion);
    }

    public String getDeclVersion() {
        return this.declVersion;
    }

    public void setDeclVersion(String declVersion) {
        this.declVersion = declVersion;
    }
}

