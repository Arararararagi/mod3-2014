/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import pt.opensoft.util.StringUtil;

public class GetDeclUser {
    private String id = null;
    private String nif = null;
    private String password = null;

    public GetDeclUser(String id, String nif) {
        this(id, nif, null);
    }

    public GetDeclUser(String id, String nif, String password) {
        this.id = id;
        this.nif = nif;
        this.password = password;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNif() {
        return this.nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean validate() {
        return !StringUtil.isEmpty(this.id) && !StringUtil.isEmpty(this.nif) && !StringUtil.isEmpty(this.password);
    }
}

