/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.http;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import pt.opensoft.taxclient.http.SubmitUser;
import pt.opensoft.text.Base64;
import pt.opensoft.util.Pair;
import pt.opensoft.util.ZipUtil;

public class SubmitRequest {
    protected String appVersion;
    protected List users = new ArrayList();
    protected String declaracao;
    protected List optionalFields = new ArrayList();
    protected boolean checkWarnings = true;

    public String getAppVersion() {
        return this.appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeclaracao() throws IOException {
        return ZipUtil.inflate(Base64.decode(this.declaracao));
    }

    public String getDeclaracaoToRequest() {
        return this.declaracao;
    }

    public List getUsers() {
        return this.users;
    }

    public List getOptionalFields() {
        return this.optionalFields;
    }

    public void setDeclaracao(String declaracao) throws IOException {
        if (declaracao == null || declaracao.length() == 0) {
            throw new IllegalArgumentException("Declaracao est\u00e1 vazia");
        }
        this.declaracao = Base64.encode(ZipUtil.deflate(declaracao));
    }

    public void setDeclaracaoFromRequest(String declaracao) {
        if (declaracao == null || declaracao.length() == 0) {
            throw new IllegalArgumentException("Declaracao est\u00e1 vazia");
        }
        this.declaracao = declaracao;
    }

    public void addUser(SubmitUser user) {
        if (user == null) {
            throw new NullPointerException("User \u00e9 null");
        }
        this.users.add(user);
    }

    public void addOptionalField(String key, String value) {
        if (key == null || value == null) {
            throw new NullPointerException("args are null");
        }
        Pair<String, String> pair = new Pair<String, String>(key, value);
        this.optionalFields.add(pair);
    }

    public boolean isCheckWarnings() {
        return this.checkWarnings;
    }

    public void setCheckWarnings(boolean checkWarnings) {
        this.checkWarnings = checkWarnings;
    }
}

