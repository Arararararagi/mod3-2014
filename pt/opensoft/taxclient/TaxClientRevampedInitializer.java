/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient;

import java.awt.Component;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;
import org.jvnet.flamingo.ribbon.JRibbon;
import pt.opensoft.swing.GUIParameters;
import pt.opensoft.taxclient.actions.ActionsTaxClientManager;
import pt.opensoft.taxclient.actions.ConfigureProxyAction;
import pt.opensoft.taxclient.actions.declaracao.TaxClientRevampedValidateAction;
import pt.opensoft.taxclient.actions.edit.CopyAction;
import pt.opensoft.taxclient.actions.edit.CutAction;
import pt.opensoft.taxclient.actions.edit.PasteAction;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedExitAction;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedOpenFileAction;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAction;
import pt.opensoft.taxclient.actions.file.TaxClientRevampedSaveFileAsAction;
import pt.opensoft.taxclient.model.DeclaracaoModel;
import pt.opensoft.taxclient.model.DeclaracaoModelValidator;
import pt.opensoft.taxclient.model.DeclaracaoPresentationModel;
import pt.opensoft.taxclient.model.NavigationModel;
import pt.opensoft.taxclient.ui.MainFrame;
import pt.opensoft.taxclient.ui.menus.Ribbonizable;
import pt.opensoft.taxclient.util.Session;
import pt.opensoft.util.SimpleLog;

public abstract class TaxClientRevampedInitializer
implements Ribbonizable {
    protected void initProperties() {
        System.setProperty("sun.awt.exception.handler", Session.getExceptionHandler().getClass().getName());
        Thread.currentThread().setUncaughtExceptionHandler(Session.getExceptionHandler());
        GUIParameters.load("/taxclient.properties");
        SimpleLog.setEnabled(true);
    }

    protected void initLookAndFeel(Component component) throws UnsupportedLookAndFeelException, InterruptedException, InvocationTargetException {
        SwingUtilities.updateComponentTreeUI(component);
        JFrame.setDefaultLookAndFeelDecorated(true);
    }

    protected void registerDefaultTaxClientActions() {
        ActionsTaxClientManager.registerAction("Validar", new TaxClientRevampedValidateAction());
        ActionsTaxClientManager.registerAction("GravarComo", new TaxClientRevampedSaveFileAsAction());
        ActionsTaxClientManager.registerAction("Gravar", new TaxClientRevampedSaveFileAction());
        ActionsTaxClientManager.registerAction("Abrir", new TaxClientRevampedOpenFileAction());
        ActionsTaxClientManager.registerAction("ConfigurarProxy", new ConfigureProxyAction());
        ActionsTaxClientManager.registerAction("Colar", new PasteAction());
        ActionsTaxClientManager.registerAction("Cortar", new CutAction());
        ActionsTaxClientManager.registerAction("Copiar", new CopyAction());
        ActionsTaxClientManager.registerAction("Sair", new TaxClientRevampedExitAction());
    }

    protected void initDeclarationPresentationModel(DeclaracaoModel declaracaoModel) {
        Session.setCurrentDeclaracao(new DeclaracaoPresentationModel(declaracaoModel));
    }

    protected void addMainFrameToSession(MainFrame mainFrame) {
        Session.setMainFrame(mainFrame);
    }

    protected abstract void bootstrap();

    protected abstract MainFrame createMainFrame(DeclaracaoModel var1, NavigationModel var2);

    protected abstract DeclaracaoModel createDeclaracaoModel();

    protected abstract NavigationModel createNavigationModel(DeclaracaoModel var1);

    protected abstract String getApplicationName();

    protected abstract DeclaracaoModelValidator createDeclaracaoModelValidator();

    @Override
    public JRibbon getRibbon() {
        return Session.getMainFrame().getTopRibbon();
    }
}

