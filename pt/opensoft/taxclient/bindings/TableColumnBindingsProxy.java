/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.bindings;

import pt.opensoft.taxclient.bindings.DefaultTableColumnBindings;
import pt.opensoft.taxclient.bindings.TableColumnBindings;

public class TableColumnBindingsProxy {
    private static TableColumnBindings implTableColumnBindings = new DefaultTableColumnBindings();
    private static TableColumnBindingsProxy instance = new TableColumnBindingsProxy();

    public static void setTableColumnBindingsImpl(TableColumnBindings impl) {
        implTableColumnBindings = impl;
    }

    public static TableColumnBindingsProxy getInstance() {
        return instance;
    }

    public TableColumnBindings getTableColumnBindings() {
        return implTableColumnBindings;
    }
}

