/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.taxclient.bindings;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.swing.EventComboBoxModel;
import java.util.Collection;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import pt.opensoft.swing.renderer.ComplexCatalogCellRenderer;
import pt.opensoft.taxclient.bindings.TableColumnBindings;
import pt.opensoft.taxclient.gui.CatalogManager;
import pt.opensoft.util.ListMap;

public class DefaultTableColumnBindings
implements TableColumnBindings {
    @Override
    public void doBindingForCatalogColumn(JTable table, String catalogName, int columnNum) {
        JComboBox combo = new JComboBox();
        BasicEventList items = new BasicEventList();
        items.addAll(CatalogManager.getInstance().getCatalog(catalogName).elements());
        combo.setRenderer(new ComplexCatalogCellRenderer());
        combo.setModel(new EventComboBoxModel(items));
        table.getColumnModel().getColumn(columnNum).setCellEditor(new DefaultCellEditor(combo));
        table.getColumnModel().getColumn(columnNum).setCellRenderer(new ComplexCatalogCellRenderer());
    }
}

