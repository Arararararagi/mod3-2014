/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

import java.io.PrintStream;
import pt.opensoft.crypt.Key16;
import pt.opensoft.crypt.MAC;
import pt.opensoft.crypt.criptoSw3DES;
import pt.opensoft.util.ByteUtil;
import pt.opensoft.util.StringUtil;

public class Encript {
    private int _HEADERSIZELENGTH = 6;
    private criptoSw3DES cripto = new criptoSw3DES(new byte[Key16._SIZE]);

    public MAC getSignature(Key16 password, String text) {
        int textLen = 0;
        int arredondamento = 0;
        Object origText = null;
        byte[] mText = new byte[8];
        byte[] passwordKey = new byte[Key16._SIZE];
        for (int i = 0; i < Key16._SIZE; ++i) {
            passwordKey[i] = password.getKey()[i];
        }
        textLen = text.length();
        arredondamento = Encript.getArredond(textLen);
        origText = new byte[textLen + arredondamento];
        byte[] barr = StringUtil.toByteArray(text);
        System.arraycopy(barr, 0, origText, 0, barr.length);
        this.cripto = new criptoSw3DES(passwordKey);
        this.cripto.setMACKey(passwordKey);
        if (this.cripto.signMAC((byte[])origText, (short)(textLen + arredondamento))) {
            mText = this.cripto.getMAC();
        }
        MAC mac = new MAC(mText);
        return mac;
    }

    public String criptText(Key16 password, String text) {
        String myStr = StringUtil.padZeros(text.length(), this._HEADERSIZELENGTH) + text;
        int textLen = myStr.length();
        int arredondamento = Encript.getArredond(textLen);
        byte[] origText = new byte[textLen + arredondamento];
        byte[] barr = StringUtil.toByteArray(myStr);
        System.arraycopy(barr, 0, origText, 0, barr.length);
        byte[] passwordKey = password.getKey();
        this.cripto = new criptoSw3DES(passwordKey);
        if (!this.cripto.encrypt(origText, (short)(textLen + arredondamento))) {
            System.out.println("Erro na cifra");
            return "";
        }
        byte[] cifrText = this.cripto.getCipherResult();
        char[] tmp = ByteUtil.toCharArray(cifrText);
        String textOut = new String(tmp);
        return textOut;
    }

    public String decriptText(Key16 password, String text) {
        int realTextSize;
        Object cifrText = null;
        byte[] passwordKey = password.getKey();
        cifrText = new byte[text.length()];
        this.cripto = new criptoSw3DES(passwordKey);
        byte[] barr = StringUtil.toByteArray(text);
        System.arraycopy(barr, 0, cifrText, 0, text.length());
        if (!this.cripto.decrypt((byte[])cifrText, (short)text.length())) {
            System.out.println("Erro na cifra1");
            return "";
        }
        cifrText = this.cripto.getCipherResult();
        char[] tmp = ByteUtil.toCharArray((byte[])cifrText);
        String textOut = new String(tmp);
        try {
            realTextSize = Integer.parseInt(textOut.substring(0, 6));
        }
        catch (Exception ie) {
            System.out.println("Erro na cifra2");
            return "";
        }
        return textOut.substring(this._HEADERSIZELENGTH, realTextSize + this._HEADERSIZELENGTH);
    }

    private static int getArredond(int sz) {
        int arr = sz % 8;
        arr = arr == 0 ? 0 : 8 - arr;
        return arr;
    }
}

