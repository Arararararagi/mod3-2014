/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

public class criptoSw3DES {
    private byte[] cipherResult;
    private byte[] MAC;
    private int[][] encryptKeys = new int[4][32];
    private int[][] decryptKeys = new int[4][32];
    private static final byte[] bytebit = new byte[]{-128, 64, 32, 16, 8, 4, 2, 1};
    private static final int[] bigbyte = new int[]{8388608, 4194304, 2097152, 1048576, 524288, 262144, 131072, 65536, 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1};
    private static final byte[] pc1 = new byte[]{56, 48, 40, 32, 24, 16, 8, 0, 57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 60, 52, 44, 36, 28, 20, 12, 4, 27, 19, 11, 3};
    private static final int[] totrot = new int[]{1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28};
    private static final byte[] pc2 = new byte[]{13, 16, 10, 23, 0, 4, 2, 27, 14, 5, 20, 9, 22, 18, 11, 3, 25, 7, 15, 6, 26, 19, 12, 1, 40, 51, 30, 36, 46, 54, 29, 39, 50, 44, 32, 47, 43, 48, 38, 55, 33, 52, 45, 41, 49, 35, 28, 31};
    private static final int[] SP1 = new int[]{16843776, 0, 65536, 16843780, 16842756, 66564, 4, 65536, 1024, 16843776, 16843780, 1024, 16778244, 16842756, 16777216, 4, 1028, 16778240, 16778240, 66560, 66560, 16842752, 16842752, 16778244, 65540, 16777220, 16777220, 65540, 0, 1028, 66564, 16777216, 65536, 16843780, 4, 16842752, 16843776, 16777216, 16777216, 1024, 16842756, 65536, 66560, 16777220, 1024, 4, 16778244, 66564, 16843780, 65540, 16842752, 16778244, 16777220, 1028, 66564, 16843776, 1028, 16778240, 16778240, 0, 65540, 66560, 0, 16842756};
    private static final int[] SP2 = new int[]{-2146402272, -2147450880, 32768, 1081376, 1048576, 32, -2146435040, -2147450848, -2147483616, -2146402272, -2146402304, Integer.MIN_VALUE, -2147450880, 1048576, 32, -2146435040, 1081344, 1048608, -2147450848, 0, Integer.MIN_VALUE, 32768, 1081376, -2146435072, 1048608, -2147483616, 0, 1081344, 32800, -2146402304, -2146435072, 32800, 0, 1081376, -2146435040, 1048576, -2147450848, -2146435072, -2146402304, 32768, -2146435072, -2147450880, 32, -2146402272, 1081376, 32, 32768, Integer.MIN_VALUE, 32800, -2146402304, 1048576, -2147483616, 1048608, -2147450848, -2147483616, 1048608, 1081344, 0, -2147450880, 32800, Integer.MIN_VALUE, -2146435040, -2146402272, 1081344};
    private static final int[] SP3 = new int[]{520, 134349312, 0, 134348808, 134218240, 0, 131592, 134218240, 131080, 134217736, 134217736, 131072, 134349320, 131080, 134348800, 520, 134217728, 8, 134349312, 512, 131584, 134348800, 134348808, 131592, 134218248, 131584, 131072, 134218248, 8, 134349320, 512, 134217728, 134349312, 134217728, 131080, 520, 131072, 134349312, 134218240, 0, 512, 131080, 134349320, 134218240, 134217736, 512, 0, 134348808, 134218248, 131072, 134217728, 134349320, 8, 131592, 131584, 134217736, 134348800, 134218248, 520, 134348800, 131592, 8, 134348808, 131584};
    private static final int[] SP4 = new int[]{8396801, 8321, 8321, 128, 8396928, 8388737, 8388609, 8193, 0, 8396800, 8396800, 8396929, 129, 0, 8388736, 8388609, 1, 8192, 8388608, 8396801, 128, 8388608, 8193, 8320, 8388737, 1, 8320, 8388736, 8192, 8396928, 8396929, 129, 8388736, 8388609, 8396800, 8396929, 129, 0, 0, 8396800, 8320, 8388736, 8388737, 1, 8396801, 8321, 8321, 128, 8396929, 129, 1, 8192, 8388609, 8193, 8396928, 8388737, 8193, 8320, 8388608, 8396801, 128, 8388608, 8192, 8396928};
    private static final int[] SP5 = new int[]{256, 34078976, 34078720, 1107296512, 524288, 256, 1073741824, 34078720, 1074266368, 524288, 33554688, 1074266368, 1107296512, 1107820544, 524544, 1073741824, 33554432, 1074266112, 1074266112, 0, 1073742080, 1107820800, 1107820800, 33554688, 1107820544, 1073742080, 0, 1107296256, 34078976, 33554432, 1107296256, 524544, 524288, 1107296512, 256, 33554432, 1073741824, 34078720, 1107296512, 1074266368, 33554688, 1073741824, 1107820544, 34078976, 1074266368, 256, 33554432, 1107820544, 1107820800, 524544, 1107296256, 1107820800, 34078720, 0, 1074266112, 1107296256, 524544, 33554688, 1073742080, 524288, 0, 1074266112, 34078976, 1073742080};
    private static final int[] SP6 = new int[]{536870928, 541065216, 16384, 541081616, 541065216, 16, 541081616, 4194304, 536887296, 4210704, 4194304, 536870928, 4194320, 536887296, 536870912, 16400, 0, 4194320, 536887312, 16384, 4210688, 536887312, 16, 541065232, 541065232, 0, 4210704, 541081600, 16400, 4210688, 541081600, 536870912, 536887296, 16, 541065232, 4210688, 541081616, 4194304, 16400, 536870928, 4194304, 536887296, 536870912, 16400, 536870928, 541081616, 4210688, 541065216, 4210704, 541081600, 0, 541065232, 16, 16384, 541065216, 4210704, 16384, 4194320, 536887312, 0, 541081600, 536870912, 4194320, 536887312};
    private static final int[] SP7 = new int[]{2097152, 69206018, 67110914, 0, 2048, 67110914, 2099202, 69208064, 69208066, 2097152, 0, 67108866, 2, 67108864, 69206018, 2050, 67110912, 2099202, 2097154, 67110912, 67108866, 69206016, 69208064, 2097154, 69206016, 2048, 2050, 69208066, 2099200, 2, 67108864, 2099200, 67108864, 2099200, 2097152, 67110914, 67110914, 69206018, 69206018, 2, 2097154, 67108864, 67110912, 2097152, 69208064, 2050, 2099202, 69208064, 2050, 67108866, 69208066, 69206016, 2099200, 0, 2, 69208066, 0, 2099202, 69206016, 2048, 67108866, 67110912, 2048, 2097154};
    private static final int[] SP8 = new int[]{268439616, 4096, 262144, 268701760, 268435456, 268439616, 64, 268435456, 262208, 268697600, 268701760, 266240, 268701696, 266304, 4096, 64, 268697600, 268435520, 268439552, 4160, 266240, 262208, 268697664, 268701696, 4160, 0, 0, 268697664, 268435520, 268439552, 266304, 262144, 266304, 262144, 268701696, 4096, 64, 268697664, 4096, 266304, 268439552, 64, 268435520, 268697600, 268697664, 268435456, 262144, 268439616, 0, 268701760, 262208, 268435520, 268697600, 268439552, 268439616, 0, 268701760, 266240, 266240, 4160, 4160, 262208, 268435456, 268701696};

    public criptoSw3DES(byte[] key) {
        this.setCifKey(key);
    }

    public final void setCifKey(byte[] key) {
        byte[] key2 = new byte[8];
        this.deskey(key, this.encryptKeys[0], this.decryptKeys[0]);
        criptoSw3DES.copyBlock(key, 8, key2, 0, 8);
        this.deskey(key2, this.encryptKeys[2], this.decryptKeys[2]);
    }

    public final void setMACKey(byte[] key) {
        byte[] key2 = new byte[8];
        this.deskey(key, this.encryptKeys[1], this.decryptKeys[1]);
        criptoSw3DES.copyBlock(key, 8, key2, 0, 8);
        this.deskey(key2, this.encryptKeys[3], this.decryptKeys[3]);
    }

    public final boolean encrypt(byte[] buffer, short bufferLen) {
        int i = 0;
        byte[] temp_buf = new byte[8];
        if (bufferLen == 0) {
            return false;
        }
        this.cipherResult = new byte[bufferLen + 7 - (bufferLen - 1) % 8];
        if (bufferLen >= 8) {
            this.DES3encryptBlock(buffer, temp_buf, 0);
            criptoSw3DES.copyBlock(temp_buf, 0, this.cipherResult, 0, 8);
            i = 8;
            for (bufferLen = (short)(bufferLen - 8); bufferLen >= 8; bufferLen = (short)(bufferLen - 8)) {
                criptoSw3DES.xorBlock(buffer, i, temp_buf, 0, 8);
                this.DES3encryptBlock(temp_buf, temp_buf, 0);
                criptoSw3DES.copyBlock(temp_buf, 0, this.cipherResult, i, 8);
                i+=8;
            }
        }
        if (bufferLen != 0) {
            temp_buf[bufferLen] = (byte)(temp_buf[bufferLen] ^ 128);
            criptoSw3DES.xorBlock(buffer, i, temp_buf, 0, bufferLen);
            this.DES3encryptBlock(temp_buf, temp_buf, 0);
            criptoSw3DES.copyBlock(temp_buf, 0, this.cipherResult, i, 8);
        }
        return true;
    }

    public final boolean decrypt(byte[] buffer, short bufferLen) {
        byte[] temp_buf = new byte[8];
        if (bufferLen % 8 != 0) {
            return false;
        }
        this.cipherResult = new byte[bufferLen];
        this.DES3decryptBlock(buffer, temp_buf, 0);
        criptoSw3DES.copyBlock(temp_buf, 0, this.cipherResult, 0, 8);
        int i = 8;
        for (bufferLen = (short)(bufferLen - 8); bufferLen >= 8; bufferLen = (short)(bufferLen - 8)) {
            criptoSw3DES.copyBlock(buffer, i, temp_buf, 0, 8);
            this.DES3decryptBlock(temp_buf, temp_buf, 0);
            criptoSw3DES.xorBlock(buffer, i - 8, temp_buf, 0, 8);
            criptoSw3DES.copyBlock(temp_buf, 0, this.cipherResult, i, 8);
            i+=8;
        }
        return true;
    }

    public final boolean signMAC(byte[] buffer, short bufferLen) {
        byte[] localMAC = new byte[8];
        int i = 0;
        while (bufferLen > 8) {
            criptoSw3DES.xorBlock(buffer, i, localMAC, 0, 8);
            this.DESencryptBlock(localMAC, localMAC, 1);
            i+=8;
            bufferLen = (short)(bufferLen - 8);
        }
        if (bufferLen != 0) {
            criptoSw3DES.xorBlock(buffer, i, localMAC, 0, bufferLen);
            this.DESencryptBlock(localMAC, localMAC, 1);
        }
        this.DESdecryptBlock(localMAC, localMAC, 3);
        this.DESencryptBlock(localMAC, localMAC, 1);
        this.setMAC(localMAC);
        return true;
    }

    public final boolean veriMAC(byte[] buffer, byte[] signedMsg, short bufferLen) {
        byte[] my_mac = new byte[8];
        if (this.signMAC(buffer, bufferLen)) {
            my_mac = this.getMAC();
        }
        for (int i = 0; i < 8; ++i) {
            if (my_mac[i] == signedMsg[i]) continue;
            return false;
        }
        return true;
    }

    public final byte[] getCipherResult() {
        byte[] temp = new byte[this.cipherResult.length];
        criptoSw3DES.copyBlock(this.cipherResult, 0, temp, 0, this.cipherResult.length);
        return temp;
    }

    public final byte[] getMAC() {
        byte[] temp = new byte[this.MAC.length];
        criptoSw3DES.copyBlock(this.MAC, 0, temp, 0, this.MAC.length);
        return temp;
    }

    private final void setMAC(byte[] buf) {
        this.MAC = new byte[8];
        criptoSw3DES.copyBlock(buf, 0, this.MAC, 0, 8);
    }

    private final void deskey(byte[] keyBlock, int[] KnE, int[] KnD) {
        int m;
        int l;
        int j;
        int[] pc1m = new int[56];
        int[] pcr = new int[56];
        int[] kn = new int[32];
        for (j = 0; j < 56; ++j) {
            l = pc1[j];
            m = l & 7;
            pc1m[j] = (keyBlock[l >>> 3] & bytebit[m]) != 0 ? 1 : 0;
        }
        for (int i = 0; i < 16; ++i) {
            m = i << 1;
            int n = m + 1;
            kn[n] = 0;
            kn[m] = 0;
            for (j = 0; j < 28; ++j) {
                l = j + totrot[i];
                pcr[j] = l < 28 ? pc1m[l] : pc1m[l - 28];
            }
            for (j = 28; j < 56; ++j) {
                l = j + totrot[i];
                pcr[j] = l < 56 ? pc1m[l] : pc1m[l - 28];
            }
            for (j = 0; j < 24; ++j) {
                if (pcr[pc2[j]] != 0) {
                    int[] arrn = kn;
                    int n2 = m;
                    arrn[n2] = arrn[n2] | bigbyte[j];
                }
                if (pcr[pc2[j + 24]] == 0) continue;
                int[] arrn = kn;
                int n3 = n;
                arrn[n3] = arrn[n3] | bigbyte[j];
            }
        }
        this.cookey(kn, KnE, KnD);
    }

    private final void cookey(int[] raw, int[] KnE, int[] KnD) {
        int rawi = 0;
        int KnEi = 0;
        int KnDi = 30;
        for (int i = 0; i < 16; ++i) {
            int raw0 = raw[rawi++];
            int raw1 = raw[rawi++];
            KnE[KnEi] = (raw0 & 16515072) << 6;
            int[] arrn = KnE;
            int n = KnEi;
            arrn[n] = arrn[n] | (raw0 & 4032) << 10;
            int[] arrn2 = KnE;
            int n2 = KnEi;
            arrn2[n2] = arrn2[n2] | (raw1 & 16515072) >>> 10;
            int[] arrn3 = KnE;
            int n3 = KnEi;
            arrn3[n3] = arrn3[n3] | (raw1 & 4032) >>> 6;
            KnD[KnDi++] = KnE[KnEi++];
            KnE[KnEi] = (raw0 & 258048) << 12;
            int[] arrn4 = KnE;
            int n4 = KnEi;
            arrn4[n4] = arrn4[n4] | (raw0 & 63) << 16;
            int[] arrn5 = KnE;
            int n5 = KnEi;
            arrn5[n5] = arrn5[n5] | (raw1 & 258048) >>> 4;
            int[] arrn6 = KnE;
            int n6 = KnEi;
            arrn6[n6] = arrn6[n6] | raw1 & 63;
            KnD[KnDi] = KnE[KnEi++];
            KnDi-=3;
        }
    }

    private final void DES3encryptBlock(byte[] clearText, byte[] cipherText, int keysId) {
        int[] tempInts = new int[2];
        criptoSw3DES.squashBytesToInts(clearText, tempInts);
        this.des(tempInts, tempInts, this.encryptKeys[keysId], true, false);
        this.des(tempInts, tempInts, this.decryptKeys[keysId + 2], false, false);
        this.des(tempInts, tempInts, this.encryptKeys[keysId], false, true);
        criptoSw3DES.spreadIntsToBytes(tempInts, cipherText);
    }

    private final void DES3decryptBlock(byte[] cipherText, byte[] clearText, int keysId) {
        int[] tempInts = new int[2];
        criptoSw3DES.squashBytesToInts(cipherText, tempInts);
        this.des(tempInts, tempInts, this.decryptKeys[keysId], true, false);
        this.des(tempInts, tempInts, this.encryptKeys[keysId + 2], false, false);
        this.des(tempInts, tempInts, this.decryptKeys[keysId], false, true);
        criptoSw3DES.spreadIntsToBytes(tempInts, clearText);
    }

    private final void DESencryptBlock(byte[] clearText, byte[] cipherText, int keysId) {
        int[] tempInts = new int[2];
        criptoSw3DES.squashBytesToInts(clearText, tempInts);
        this.des(tempInts, tempInts, this.encryptKeys[keysId], true, true);
        criptoSw3DES.spreadIntsToBytes(tempInts, cipherText);
    }

    private final void DESdecryptBlock(byte[] cipherText, byte[] clearText, int keysId) {
        int[] tempInts = new int[2];
        criptoSw3DES.squashBytesToInts(cipherText, tempInts);
        this.des(tempInts, tempInts, this.decryptKeys[keysId], true, true);
        criptoSw3DES.spreadIntsToBytes(tempInts, clearText);
    }

    private final void des(int[] inInts, int[] outInts, int[] keys, boolean ip, boolean fp) {
        int work;
        int keysi = 0;
        int leftt = inInts[0];
        int right = inInts[1];
        if (ip) {
            work = (leftt >>> 4 ^ right) & 252645135;
            work = ((leftt^=work << 4) >>> 16 ^ (right^=work)) & 65535;
            work = ((right^=work) >>> 2 ^ (leftt^=work << 16)) & 858993459;
            work = ((right^=work << 2) >>> 8 ^ (leftt^=work)) & 16711935;
            right^=work << 8;
            right = right << 1 | right >>> 31 & 1;
            work = ((leftt^=work) ^ right) & -1431655766;
            leftt^=work;
            right^=work;
            leftt = leftt << 1 | leftt >>> 31 & 1;
        }
        for (int round = 0; round < 8; ++round) {
            work = right << 28 | right >>> 4;
            int fval = SP7[(work^=keys[keysi++]) & 63];
            fval|=SP5[work >>> 8 & 63];
            fval|=SP3[work >>> 16 & 63];
            fval|=SP1[work >>> 24 & 63];
            work = right ^ keys[keysi++];
            fval|=SP8[work & 63];
            fval|=SP6[work >>> 8 & 63];
            fval|=SP4[work >>> 16 & 63];
            work = leftt << 28 | (leftt^=(fval|=SP2[work >>> 24 & 63])) >>> 4;
            fval = SP7[(work^=keys[keysi++]) & 63];
            fval|=SP5[work >>> 8 & 63];
            fval|=SP3[work >>> 16 & 63];
            fval|=SP1[work >>> 24 & 63];
            work = leftt ^ keys[keysi++];
            fval|=SP8[work & 63];
            fval|=SP6[work >>> 8 & 63];
            fval|=SP4[work >>> 16 & 63];
            right^=(fval|=SP2[work >>> 24 & 63]);
        }
        if (fp) {
            right = right << 31 | right >>> 1;
            work = (leftt ^ right) & -1431655766;
            leftt^=work;
            leftt = leftt << 31 | leftt >>> 1;
            work = (leftt >>> 8 ^ (right^=work)) & 16711935;
            work = ((leftt^=work << 8) >>> 2 ^ (right^=work)) & 858993459;
            work = ((right^=work) >>> 16 ^ (leftt^=work << 2)) & 65535;
            work = ((right^=work << 16) >>> 4 ^ (leftt^=work)) & 252645135;
            leftt^=work;
            right^=work << 4;
        }
        outInts[0] = right;
        outInts[1] = leftt;
    }

    private static final void xorBlock(byte[] src, int srcOff, byte[] dst, int dstOff, int len) {
        for (int i = 0; i < len; ++i) {
            dst[dstOff + i] = (byte)(src[srcOff + i] ^ dst[dstOff + i]);
        }
    }

    private static final void copyBlock(byte[] src, int srcOff, byte[] dst, int dstOff, int len) {
        for (int i = 0; i < len; ++i) {
            dst[dstOff + i] = src[srcOff + i];
        }
    }

    private static final void squashBytesToInts(byte[] inBytes, int[] outInts) {
        for (int i = 0; i < 2; ++i) {
            outInts[i] = (inBytes[i * 4] & 255) << 24 | (inBytes[i * 4 + 1] & 255) << 16 | (inBytes[i * 4 + 2] & 255) << 8 | inBytes[i * 4 + 3] & 255;
        }
    }

    private static final void spreadIntsToBytes(int[] inInts, byte[] outBytes) {
        for (int i = 0; i < 2; ++i) {
            outBytes[i * 4] = (byte)(inInts[i] >>> 24);
            outBytes[i * 4 + 1] = (byte)(inInts[i] >>> 16);
            outBytes[i * 4 + 2] = (byte)(inInts[i] >>> 8);
            outBytes[i * 4 + 3] = (byte)inInts[i];
        }
    }
}

