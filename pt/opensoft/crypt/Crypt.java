/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

import pt.opensoft.util.NamedObject;

public abstract class Crypt
extends NamedObject {
    public Crypt(String name) {
        super(name);
    }

    public abstract String encrypt(String var1, String var2);

    public abstract String decrypt(String var1, String var2);

    public abstract String signature(String var1, String var2);
}

