/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

import java.util.Random;
import pt.opensoft.crypt.Crypt;
import pt.opensoft.crypt.Crypt3DES;
import pt.opensoft.util.StringUtil;

public class Password {
    protected static final int DEFAULT_MIN_PASSWORD_SIZE = 8;
    protected static final int DEFAULT_MAX_PASSWORD_SIZE = 16;
    protected static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    protected static final String DIGITS = "0123456789";
    protected static final String LETTERS_DIGITS = StringUtil.remove(StringUtil.remove("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "O"), "I") + StringUtil.remove("0123456789", "0");
    protected static final Random random = new Random();
    protected static final Crypt crypt = new Crypt3DES();

    public static String generate(String str, int size) {
        StringBuffer buffer = new StringBuffer(size);
        int max = str.length();
        for (int i = 0; i < size; ++i) {
            int pos;
            Random random = Password.random;
            synchronized (random) {
                pos = Password.random.nextInt(max);
            }
            char ch = str.charAt(pos);
            buffer.append(ch);
        }
        return buffer.toString();
    }

    public static String generate(int size) {
        return Password.generate(LETTERS_DIGITS, size);
    }

    public static String generateLetters(int size) {
        return Password.generate("ABCDEFGHIJKLMNOPQRSTUVWXYZ", size);
    }

    public static String generateDigits(int size) {
        return Password.generate("0123456789", size);
    }

    protected static String toPassword16(String password, int minSize, int maxSize) {
        if (minSize < 1) {
            throw new IllegalArgumentException("minSize cannot be < 1: " + minSize);
        }
        if (maxSize > 16) {
            throw new IllegalArgumentException("maxSize cannot be > 16: " + maxSize);
        }
        if (minSize > maxSize) {
            throw new IllegalArgumentException("minSize (" + minSize + ") cannot be > maxSize (" + maxSize + ")");
        }
        int len = password.length();
        if (len < minSize) {
            throw new IllegalArgumentException("password cannot be < " + minSize + " chars");
        }
        if (len > maxSize) {
            throw new IllegalArgumentException("password cannot be > " + maxSize + " chars");
        }
        StringBuffer buffer = new StringBuffer(16);
        buffer.append(password);
        while (buffer.length() < 16) {
            int start = buffer.length() <= 8 ? 0 : 2 * buffer.length() - 16;
            buffer.append(buffer.substring(start, buffer.length()));
        }
        return buffer.toString();
    }

    public static String encrypt(String password) {
        return Password.encrypt(password, 8, 16);
    }

    public static String encrypt(String password, int minSize, int maxSize) {
        password = Password.toPassword16(password, minSize, maxSize);
        Crypt crypt = Password.crypt;
        synchronized (crypt) {
            return Password.crypt.encrypt(password, password.substring(0, maxSize));
        }
    }

    protected static String toPassword16WithPaddingLength(String password, int minSize, int maxSize) {
        if (minSize < 1) {
            throw new IllegalArgumentException("minSize cannot be < 1: " + minSize);
        }
        if (maxSize > 16) {
            throw new IllegalArgumentException("maxSize cannot be > 16: " + maxSize);
        }
        if (minSize > maxSize) {
            throw new IllegalArgumentException("minSize (" + minSize + ") cannot be > maxSize (" + maxSize + ")");
        }
        int len = password.length();
        if (len < minSize) {
            throw new IllegalArgumentException("password cannot be < " + minSize + " chars");
        }
        if (len > maxSize) {
            throw new IllegalArgumentException("password cannot be > " + maxSize + " chars");
        }
        return StringUtil.padChars(password, (char)(16 - len), 16, false);
    }

    public static String encryptWithPaddingLength(String password) {
        return Password.encryptWithPaddingLength(password, 8, 16);
    }

    public static String encryptWithPaddingLength(String password, int minSize, int maxSize) {
        password = Password.toPassword16WithPaddingLength(password, minSize, maxSize);
        Crypt crypt = Password.crypt;
        synchronized (crypt) {
            return Password.crypt.encrypt(password, password.substring(0, maxSize));
        }
    }

    public static boolean isMonotonous(String password) {
        for (int i = 1; i <= 8; ++i) {
            if (!Password.isMonotonous(password, i)) continue;
            return true;
        }
        return false;
    }

    public static boolean isMonotonous(String password, int repSize) {
        if (password.length() < repSize * 2) {
            return false;
        }
        if (password.length() % repSize != 0) {
            return false;
        }
        String oldStr = null;
        String currentStr = null;
        for (int cursor = 0; cursor < password.length(); cursor+=repSize) {
            currentStr = password.substring(cursor, cursor + repSize);
            if (!(oldStr == null || currentStr.equals(oldStr))) {
                return false;
            }
            oldStr = currentStr;
        }
        return true;
    }
}

