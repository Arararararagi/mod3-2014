/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

import pt.opensoft.crypt.Crypt;
import pt.opensoft.crypt.Encript;
import pt.opensoft.crypt.Key16;
import pt.opensoft.crypt.MAC;
import pt.opensoft.text.Base64;

public class Crypt3DES
extends Crypt {
    protected Encript encript = new Encript();
    protected boolean base64 = true;

    public Crypt3DES() {
        this(true);
    }

    public Crypt3DES(boolean base64) {
        super("3DES");
        this.setBase64(base64);
    }

    @Override
    public String decrypt(String key, String text) {
        if (text == null || text.equals("")) {
            return text;
        }
        Key16 key16 = this.getKey16(key);
        return this.encript.decriptText(key16, this.base64 ? Base64.decode(text) : text);
    }

    @Override
    public String encrypt(String key, String text) {
        if (text == null || text.equals("")) {
            return text;
        }
        Key16 key16 = this.getKey16(key);
        String crypted = this.encript.criptText(key16, text);
        return this.base64 ? Base64.encode(crypted) : crypted;
    }

    protected Key16 getKey16(String key) {
        if (key == null || key.trim().equals("")) {
            throw new NullPointerException("key cannot be null");
        }
        if (key.length() < 16) {
            throw new IllegalArgumentException("key cannot be < 16 chars: " + key.length());
        }
        return new Key16(Base64.encode(key));
    }

    public boolean isBase64() {
        return this.base64;
    }

    public void setBase64(boolean base64) {
        this.base64 = base64;
    }

    @Override
    public String signature(String key, String text) {
        if (text == null || text.equals("")) {
            return text;
        }
        Key16 key16 = this.getKey16(key);
        return this.encript.getSignature(key16, text).toStringB64();
    }
}

