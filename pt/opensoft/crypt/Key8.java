/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

import java.util.Random;
import pt.opensoft.text.Base64;
import pt.opensoft.util.ByteUtil;
import pt.opensoft.util.StringUtil;

public class Key8 {
    public static int _SIZE = 8;
    protected byte[] sbKey = new byte[_SIZE];

    public Key8() {
    }

    public Key8(String sStr) {
        this();
        this.parseKey(sStr);
    }

    public void parseKey(String sStr) {
        String sTemp = Base64.decode(sStr);
        byte[] barr = StringUtil.toByteArray(sTemp);
        System.arraycopy(barr, 0, this.sbKey, 0, _SIZE);
    }

    public String toString() {
        char[] carr = ByteUtil.toCharArray(this.sbKey);
        return Base64.encode(new String(carr));
    }

    public void setKey(String sStr) {
        byte[] barr = StringUtil.toByteArray(sStr);
        System.arraycopy(barr, 0, this.sbKey, 0, _SIZE);
    }

    public void setKey(Key8 k) {
        System.arraycopy(k.sbKey, 0, this.sbKey, 0, _SIZE);
    }

    public void setKey(byte[] b) {
        System.arraycopy(b, 0, this.sbKey, 0, _SIZE);
    }

    public byte[] getKey() {
        return this.sbKey;
    }

    public static Key8 generateKey() {
        Random r = new Random();
        byte[] bArray = new byte[_SIZE];
        for (int i = 0; i < _SIZE; ++i) {
            bArray[i] = (byte)r.nextInt();
        }
        Key8 k = new Key8();
        k.setKey(bArray);
        return k;
    }
}

