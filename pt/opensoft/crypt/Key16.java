/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

import java.util.Random;
import pt.opensoft.crypt.Key8;
import pt.opensoft.text.Base64;
import pt.opensoft.util.StringUtil;

public class Key16
extends Key8 {
    public static int _SIZE = 16;

    public Key16() {
        this.sbKey = new byte[_SIZE];
    }

    public Key16(String str) {
        this.sbKey = new byte[_SIZE];
        this.parseKey(str);
    }

    Key16(byte[] b1, byte[] b2, byte[] b3) {
        this.sbKey = new byte[_SIZE];
        int j = 0;
        for (int i = 0; i < b1.length; ++i) {
            this.sbKey[j++] = b1[i];
        }
        for (int l = 0; l < b2.length; ++l) {
            this.sbKey[j++] = b2[l];
        }
        for (int m = 0; m < b3.length; ++m) {
            this.sbKey[j++] = b3[m];
        }
    }

    @Override
    public void parseKey(String sStr) {
        String sTemp = Base64.decode(sStr);
        byte[] barr = StringUtil.toByteArray(sTemp);
        System.arraycopy(barr, 0, this.sbKey, 0, _SIZE);
    }

    @Override
    public void setKey(byte[] b) {
        for (int i = 0; i < _SIZE; ++i) {
            this.sbKey[i] = b[i];
        }
    }

    @Override
    public void setKey(String sStr) {
        byte[] barr = StringUtil.toByteArray(sStr);
        System.arraycopy(barr, 0, this.sbKey, 0, _SIZE);
    }

    public static Key16 generateKey16() {
        Random r = new Random();
        byte[] bArray = new byte[_SIZE];
        for (int i = 0; i < _SIZE; ++i) {
            bArray[i] = (byte)r.nextInt();
        }
        Key16 k = new Key16();
        k.setKey(bArray);
        return k;
    }
}

