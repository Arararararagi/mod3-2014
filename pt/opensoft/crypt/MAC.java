/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.crypt;

import pt.opensoft.text.Base64;
import pt.opensoft.util.ByteUtil;

public class MAC {
    public static final int _SIZE = 8;
    private byte[] sMAC = new byte[8];

    public MAC(byte[] b) {
        this.setMAC(b);
    }

    public MAC() {
    }

    public void setMAC(byte[] smac) {
        for (int i = 0; i < 8; ++i) {
            this.sMAC[i] = smac[i];
        }
    }

    public String toStringB64() {
        char[] tmp = ByteUtil.toCharArray(this.sMAC);
        return Base64.encode(new String(tmp));
    }

    public boolean equals(MAC mac) {
        String thisMac = this.toStringB64();
        String newMac = mac.toStringB64();
        return thisMac.equals(newMac);
    }
}

