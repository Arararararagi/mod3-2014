/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import pt.opensoft.math.Fraction;
import pt.opensoft.util.StringUtil;

public class NumberUtil {
    protected static final String ZERO = "zero";
    protected static final String CEM = "cem";
    protected static final String AND_STR = " e ";
    protected static final String[] UNIDADES = new String[]{"", "um", "dois", "tr\u00eas", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "catorze", "quinze", "dezasseis", "dezassete", "dezoito", "dezanove"};
    protected static final String[] DEZENAS = new String[]{"", "", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"};
    protected static final String[] CENTENAS = new String[]{"", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"};
    protected static final String[] UNIDADES_FEMALE = new String[]{"", "uma", "duas", "tr\u00eas", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "catorze", "quinze", "dezasseis", "dezassete", "dezoito", "dezanove"};
    protected static final String[] CENTENAS_FEMALE = new String[]{"", "cento", "duzentas", "trezentas", "quatrocentas", "quinhentas", "seiscentas", "setecentas", "oitocentas", "novecentas"};
    protected static final String MIL = "mil";
    protected static final String MILHAO = "um milh\u00e3o";
    protected static final String MILHOES = "milh\u00f5es";
    public static final long KB = 1024;
    public static final long MB = 0x100000;
    public static final int DEFAULT_PRECISION = 2;
    protected static final String[] PATTERNS = new String[]{"", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000"};

    private NumberUtil() {
    }

    public static NumberFormat getNumberFormat(int size) {
        return NumberUtil.getNumberFormat(size, 0);
    }

    public static NumberFormat getNumberFormat(int size, int decimals) {
        int i;
        StringBuffer buffer = new StringBuffer("");
        for (i = size - 1; i >= 0; --i) {
            if (i == 0 && decimals > 0) {
                buffer.append("0");
            } else {
                buffer.append("#");
            }
            if (i % 3 != 0 || i == 0) continue;
            buffer.append(",");
        }
        if (decimals > 0) {
            buffer.append(".");
        }
        for (i = decimals - 1; i >= 0; --i) {
            buffer.append("0");
        }
        return NumberUtil.getNumberFormat(buffer.toString(), decimals > 0);
    }

    public static NumberFormat getNumberFormat(String format, boolean decimalShown) {
        return NumberUtil.getNumberFormat(format, '.', ',', decimalShown);
    }

    public static NumberFormat getNumberFormat(String format, char groupingSeparator, char decimalSeparator, boolean decimalShown) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(groupingSeparator);
        symbols.setDecimalSeparator(decimalSeparator);
        DecimalFormat formater = new DecimalFormat(format, symbols);
        formater.setDecimalSeparatorAlwaysShown(decimalShown);
        return formater;
    }

    public static NumberFormat getEscudoFormat() {
        DecimalFormatSymbols escudoSymbols = new DecimalFormatSymbols();
        escudoSymbols.setGroupingSeparator('.');
        return new DecimalFormat("###,###,###,###$", escudoSymbols);
    }

    public static String toString(int value) {
        return NumberUtil.toString(value, NumberUtil.size(value), '0');
    }

    public static String toString(int value, int size) {
        return NumberUtil.toString(value, size, '0');
    }

    public static String toString(int value, int size, char ch) {
        return StringUtil.prependChars(String.valueOf(value), ch, size);
    }

    public static int size(int value) {
        return String.valueOf(value).length();
    }

    public static double getDoubleFromHost(String hostFloat, int precision) throws NumberFormatException {
        int decimalSeparatorIdx = hostFloat.length() - precision;
        String decimals = hostFloat.substring(decimalSeparatorIdx);
        String integers = hostFloat.substring(0, decimalSeparatorIdx);
        String doubleStr = integers + "." + decimals;
        return Double.valueOf(doubleStr);
    }

    public static String getHostStringFromDouble(double d, int length, int precision) {
        String pattern = PATTERNS[length - precision] + "." + PATTERNS[precision];
        DecimalFormat format = new DecimalFormat(pattern);
        return StringUtil.remove(StringUtil.remove(format.format(d), ","), ".");
    }

    public static int signal(int number) {
        return number / Math.abs(number);
    }

    public static String formatFileSize(long fileSize) {
        StringBuffer str = new StringBuffer();
        long size = 0;
        if (fileSize < 1024) {
            str.append(fileSize).append(" Bytes");
        } else if (fileSize < 0x100000) {
            size = (long)Math.ceil(fileSize / 1024);
            str.append(size).append(" KB");
        } else {
            size = (long)Math.ceil(fileSize / 0x100000);
            str.append(size).append(" MB");
        }
        return str.toString();
    }

    public static long getLongValue(double value) {
        return Math.round(Math.floor(value));
    }

    public static String toFullLength(long number) {
        return NumberUtil.toFullLength(number, true);
    }

    public static String toFullLength(long number, boolean male) {
        long milhares;
        if (number < 0) {
            throw new IllegalArgumentException("number cannot be < 0");
        }
        if (number == 0) {
            return "zero";
        }
        if (number < 20) {
            return male ? UNIDADES[(int)number] : UNIDADES_FEMALE[(int)number];
        }
        if (number < 100) {
            int dezenas = (int)number / 10;
            StringBuffer buffer = new StringBuffer(DEZENAS[dezenas]);
            long unidades = number % 10;
            if (unidades > 0) {
                buffer.append(" e ").append(NumberUtil.toFullLength(unidades, male));
            }
            return buffer.toString();
        }
        if (number == 100) {
            return "cem";
        }
        if (number < 1000) {
            int centenas = (int)number / 100;
            StringBuffer buffer = new StringBuffer(male ? CENTENAS[centenas] : CENTENAS_FEMALE[centenas]);
            long dezenas = number % 100;
            if (dezenas > 0) {
                buffer.append(" e ").append(NumberUtil.toFullLength(dezenas, male));
            }
            return buffer.toString();
        }
        if (number == 1000) {
            return "mil";
        }
        if (number < 1000000) {
            StringBuffer buffer = new StringBuffer();
            long milhares2 = number / 1000;
            if (milhares2 > 1) {
                buffer.append(NumberUtil.toFullLength(milhares2, male)).append(" ");
            }
            buffer.append("mil");
            long centenas = number % 1000;
            if (centenas > 0) {
                buffer.append(centenas % 100 == 0 || centenas <= 100 ? " e " : " ");
                buffer.append(NumberUtil.toFullLength(centenas, male));
            }
            return buffer.toString();
        }
        if (number == 1000000) {
            return "um milh\u00e3o";
        }
        StringBuffer buffer = new StringBuffer();
        long milhoes = number / 1000000;
        if (milhoes == 1) {
            buffer.append("um milh\u00e3o");
        }
        if (milhoes > 1) {
            buffer.append(NumberUtil.toFullLength(milhoes, male)).append(" ").append("milh\u00f5es");
        }
        if ((milhares = number % 1000000) == 0) {
            return buffer.toString();
        }
        buffer.append(milhares <= 100 || milhares < 1000 && milhares % 100 == 0 || milhares % 1000 == 0 ? " e " : " ");
        buffer.append(NumberUtil.toFullLength(milhares, male));
        return buffer.toString();
    }

    public static final boolean isOdd(int i) {
        return (i & 1) == 1;
    }

    public static boolean isZero(String str) {
        if (StringUtil.isEmpty(str)) {
            return false;
        }
        char[] c = str.toCharArray();
        for (int i = 0; i < str.length(); ++i) {
            if (c[i] == '0') continue;
            return false;
        }
        return true;
    }

    public static BigDecimal centims2Euros(long centims) {
        return NumberUtil.centims2Euros(centims, 2);
    }

    public static BigDecimal centims2Euros(long centims, int precision) {
        BigDecimal euros = new BigDecimal(String.valueOf(centims));
        return euros.movePointLeft(precision);
    }

    public static long euros2Centims(BigDecimal euros) {
        return NumberUtil.euros2Centims(euros, 2);
    }

    public static long euros2Centims(BigDecimal euros, int precision) {
        return euros.movePointRight(precision).longValue();
    }

    public static float longToFloat(long value, int precision) {
        return new BigDecimal(String.valueOf(value)).movePointLeft(precision).floatValue();
    }

    public static float longToFloat(long value) {
        return NumberUtil.longToFloat(value, 2);
    }

    public static long floatToLong(float value, int precision) {
        return new BigDecimal(value).movePointRight(precision).longValue();
    }

    public static long floatToLong(float value) {
        return NumberUtil.floatToLong(value, 2);
    }

    public static Number max(Number a, Number b) {
        int cmp = 0;
        if (a instanceof Fraction) {
            cmp = ((Fraction)a).compareTo(b);
        } else if (a instanceof BigDecimal) {
            cmp = ((BigDecimal)a).compareTo((BigDecimal)b);
        } else if (a instanceof BigInteger) {
            cmp = ((BigInteger)a).compareTo((BigInteger)b);
        } else {
            throw new IllegalArgumentException("Unknown type of number a.");
        }
        if (cmp >= 0) {
            return a;
        }
        return b;
    }

    public static Number min(Number a, Number b) {
        int cmp = 0;
        if (a instanceof Fraction) {
            cmp = ((Fraction)a).compareTo(b);
        } else if (a instanceof BigDecimal) {
            cmp = ((BigDecimal)a).compareTo((BigDecimal)b);
        } else if (a instanceof BigInteger) {
            cmp = ((BigInteger)a).compareTo((BigInteger)b);
        } else {
            throw new IllegalArgumentException("Unknown type of number a.");
        }
        if (cmp <= 0) {
            return a;
        }
        return b;
    }

    public static BigDecimal round(Number number, int numDigits) {
        return NumberUtil.round((BigDecimal)number, numDigits, 4);
    }

    public static BigDecimal round(BigDecimal number, int numDigits) {
        return NumberUtil.round(number, numDigits, 4);
    }

    public static BigDecimal round(Number number, int numDigits, int roundingMode) {
        return NumberUtil.round((BigDecimal)number, numDigits, roundingMode);
    }

    public static BigDecimal round(BigDecimal number, int numDigits, int roundingMode) {
        return number.movePointRight(numDigits).setScale(0, roundingMode).movePointRight(- numDigits).setScale(number.scale());
    }

    public static Number validateFloat(String floatToValidate) {
        if (floatToValidate == null || floatToValidate.trim().equals("") || floatToValidate.indexOf(46) != -1) {
            return null;
        }
        try {
            floatToValidate = floatToValidate.replace(',', '.');
            Float.parseFloat(floatToValidate);
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid float: " + floatToValidate);
            return null;
        }
        return new BigDecimal(floatToValidate).setScale(2);
    }

    public static String toXSDNumericType(long value, int precision, char decimalSeparator) {
        boolean isNegative = value < 0;
        String formatedValue = String.valueOf(Math.abs(value));
        if (precision == 0) {
            return isNegative ? "-" + formatedValue : formatedValue;
        }
        if (formatedValue.length() <= precision) {
            formatedValue = StringUtil.padZeros(formatedValue, precision + 1);
        }
        if (isNegative) {
            formatedValue = "-" + formatedValue;
        }
        return formatedValue.substring(0, formatedValue.length() - precision) + decimalSeparator + formatedValue.substring(formatedValue.length() - precision);
    }

    public static String fromXSDNumericType(String value, int precision, char decimalSeparator) {
        if (value != null) {
            if (value.length() == 0 || value.equals(".")) {
                throw new NumberFormatException();
            }
            int separatorIndex = value.indexOf(decimalSeparator);
            String integralPart = separatorIndex != -1 ? value.substring(0, separatorIndex) : value;
            String decimalPart = separatorIndex != -1 ? value.substring(separatorIndex + 1) : "";
            decimalPart = StringUtil.padChars(decimalPart, '0', precision, false);
            Long.parseLong(integralPart + decimalPart);
            return integralPart + decimalPart;
        }
        return "";
    }
}

