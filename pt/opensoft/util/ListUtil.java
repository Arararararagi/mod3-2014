/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import pt.opensoft.util.Closure;
import pt.opensoft.util.Predicate;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Transformer;

public class ListUtil {
    public static boolean hasDuplicatedElements(List list) {
        HashSet set = new HashSet();
        for (Object obj : list) {
            if (set.contains(obj)) {
                return true;
            }
            set.add(obj);
        }
        return false;
    }

    public static boolean hasDuplicatedElements(List list, Comparator comparator) {
        ArrayList elements = new ArrayList();
        for (Object obj : list) {
            if (ListUtil.contains(elements, obj, comparator)) {
                return true;
            }
            elements.add(obj);
        }
        return false;
    }

    public static int hasDuplicatedElementsReturnIndex(List list) {
        HashSet set = new HashSet();
        for (int i = 0; i < list.size() - 1; ++i) {
            Object obj = list.get(i);
            if (set.contains(obj)) {
                return i;
            }
            set.add(obj);
        }
        return -1;
    }

    public static int hasDuplicatedElementsReturnIndex(List list, Comparator comparator) {
        ArrayList elements = new ArrayList();
        for (int i = 0; i < list.size() - 1; ++i) {
            Object obj = list.get(i);
            if (ListUtil.contains(elements, obj, comparator)) {
                return i;
            }
            elements.add(obj);
        }
        return -1;
    }

    public static List copy(List lista) {
        if (lista == null) {
            return null;
        }
        ArrayList list = new ArrayList(lista);
        return list;
    }

    public static List sort(List lista) {
        List list = ListUtil.copy(lista);
        Collections.sort(list);
        return list;
    }

    public static List load(String filename) throws Exception {
        return ListUtil.load(new File(filename));
    }

    public static List load(File file) throws Exception {
        ArrayList<String> list;
        list = new ArrayList<String>(10);
        FileReader freader = new FileReader(file);
        try {
            BufferedReader reader = new BufferedReader(freader);
            try {
                String line = reader.readLine();
                while (line != null) {
                    list.add(line.trim());
                    line = reader.readLine();
                }
            }
            finally {
                reader.close();
            }
        }
        finally {
            freader.close();
        }
        return list;
    }

    public static List toList(Object[] array) {
        if (array == null) {
            return null;
        }
        return ListUtil.toList(array, 0, array.length);
    }

    public static List toList(Object[] array, int off) {
        if (array == null) {
            return null;
        }
        return ListUtil.toList(array, off, array.length - off);
    }

    public static List toList(Object[] array, int off, int len) {
        if (array == null) {
            return null;
        }
        if (off > array.length - 1) {
            return null;
        }
        ArrayList<Object> list = new ArrayList<Object>(len);
        for (int i = 0; i < len; ++i) {
            list.add(array[off + i]);
        }
        return list;
    }

    public static List toList(long[] array) {
        if (array == null) {
            return null;
        }
        return ListUtil.toList(array, 0, array.length);
    }

    public static List toList(long[] array, int off) {
        if (array == null) {
            return null;
        }
        return ListUtil.toList(array, off, array.length - off);
    }

    public static List toList(long[] array, int off, int len) {
        if (array == null) {
            return null;
        }
        if (off > array.length - 1) {
            return null;
        }
        ArrayList<Long> list = new ArrayList<Long>(len);
        for (int i = 0; i < len; ++i) {
            list.add(new Long(array[off + i]));
        }
        return list;
    }

    public static List toList(Enumeration en) {
        if (en == null) {
            return null;
        }
        ArrayList values = new ArrayList();
        while (en.hasMoreElements()) {
            values.add(en.nextElement());
        }
        return values;
    }

    public static List toList(Set set) {
        if (set == null) {
            return null;
        }
        ArrayList values = new ArrayList(set.size());
        values.addAll(set);
        return values;
    }

    public static List toList(String list) {
        return ListUtil.toList(list, null);
    }

    public static List toList(String list, String separator) {
        StringTokenizer st;
        if (list == null) {
            return null;
        }
        if (separator == null) {
            separator = ",";
        }
        if (!(st = new StringTokenizer(list, separator)).hasMoreTokens()) {
            return null;
        }
        ArrayList<String> values = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            values.add(st.nextToken().trim());
        }
        return values;
    }

    public static List toList(String listStr, String separator, String unsignedNumericRangeSeparator) {
        List list = ListUtil.toList(listStr, separator);
        if (list == null) {
            return null;
        }
        ArrayList<String> finalList = new ArrayList<String>();
        for (String current : list) {
            int index = current.indexOf(unsignedNumericRangeSeparator);
            if (index != -1) {
                long toNumber;
                String[] numbers = current.split(unsignedNumericRangeSeparator);
                if (numbers.length != 2) {
                    throw new IllegalArgumentException("Range with more than two elements!");
                }
                if (!StringUtil.isNumeric(numbers[0])) {
                    throw new IllegalArgumentException("Range with first element not numeric!");
                }
                if (!StringUtil.isNumeric(numbers[1])) {
                    throw new IllegalArgumentException("Range with last element not numeric!");
                }
                long fromNumber = Long.parseLong(numbers[0]);
                if (fromNumber < (toNumber = Long.parseLong(numbers[1]))) {
                    while (fromNumber <= toNumber) {
                        finalList.add(String.valueOf(fromNumber));
                        ++fromNumber;
                    }
                    continue;
                }
                throw new IllegalArgumentException("Range with first element bigger than the last!");
            }
            finalList.add(current);
        }
        return finalList;
    }

    public static List<String> toList(String str, int fixedLength) {
        if (str == null || str.equals("")) {
            return null;
        }
        if (fixedLength < 1) {
            throw new IllegalArgumentException("The number of chars has to be a number greater than 0");
        }
        ArrayList<String> result = new ArrayList<String>((str.length() + fixedLength - 1) / fixedLength);
        for (int start = 0; start < str.length(); start+=fixedLength) {
            result.add(str.substring(start, Math.min(str.length(), start + fixedLength)));
        }
        return result;
    }

    public static String toString(List list) {
        return ListUtil.toString(list, null);
    }

    public static String toString(List list, String separator) {
        if (list == null) {
            return null;
        }
        if (separator == null) {
            separator = ";";
        }
        StringBuffer buffer = new StringBuffer();
        for (Object o : list) {
            if (buffer.length() > 0) {
                buffer.append(separator);
            }
            buffer.append(o != null ? o.toString() : "null");
        }
        return buffer.toString();
    }

    public static String toString(List list, String separator, String delimiter) {
        if (list == null) {
            return null;
        }
        if (separator == null) {
            separator = ";";
        }
        StringBuffer buffer = new StringBuffer();
        for (Object o : list) {
            if (buffer.length() > 0) {
                buffer.append(separator);
            }
            if (delimiter != null) {
                buffer.append(delimiter);
            }
            buffer.append(o.toString());
            if (delimiter == null) continue;
            buffer.append(delimiter);
        }
        return buffer.toString();
    }

    public static List union(List a, List b) {
        if (a == null || a.isEmpty()) {
            return b;
        }
        if (b == null || b.isEmpty()) {
            return a;
        }
        for (Object obj : b) {
            if (a.contains(obj)) continue;
            a.add(obj);
        }
        return a;
    }

    public static boolean contains(List a, List b) {
        if (a == null || a.isEmpty()) {
            return b == null || b.isEmpty();
        }
        if (b == null || b.isEmpty()) {
            return true;
        }
        return a.containsAll(b);
    }

    public static boolean contains(List a, List b, Comparator c) {
        if (a == null || a.isEmpty()) {
            return b == null || b.isEmpty();
        }
        if (b == null || b.isEmpty()) {
            return true;
        }
        for (Object bObj : b) {
            if (ListUtil.contains(a, bObj, c)) continue;
            return false;
        }
        return true;
    }

    public static boolean contains(List a, Object b, Comparator c) {
        for (Object aObj : a) {
            if (c.compare(aObj, b) != 0) continue;
            return true;
        }
        return false;
    }

    public static List remove(List a, List b, Comparator c) {
        if (a == null || a.isEmpty()) {
            return null;
        }
        if (b == null || b.isEmpty()) {
            return a;
        }
        ArrayList result = new ArrayList(Math.abs(a.size() - b.size()));
        for (Object aObj : a) {
            if (ListUtil.contains(b, aObj, c)) continue;
            result.add(aObj);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result;
    }

    public static List remove(List a, Predicate p) {
        if (a == null || a.isEmpty()) {
            return null;
        }
        ArrayList result = new ArrayList(a.size());
        for (Object obj : a) {
            if (p.evaluate(obj)) continue;
            result.add(obj);
        }
        return result;
    }

    public static List retain(List a, Predicate p) {
        if (a == null || a.isEmpty()) {
            return null;
        }
        ArrayList result = new ArrayList(a.size());
        for (Object obj : a) {
            if (!p.evaluate(obj)) continue;
            result.add(obj);
        }
        return result;
    }

    public static List intersection(List a, List b, Comparator c) {
        if (a == null || a.isEmpty()) {
            return null;
        }
        if (b == null || b.isEmpty()) {
            return null;
        }
        ArrayList intersection = new ArrayList(Math.min(a.size(), b.size()));
        List a1 = a.size() >= b.size() ? a : b;
        List b1 = a.size() >= b.size() ? b : a;
        for (Object aObj : a1) {
            if (!ListUtil.contains(b1, aObj, c)) continue;
            intersection.add(aObj);
        }
        if (intersection.isEmpty()) {
            return null;
        }
        return intersection;
    }

    public static List diference(List a, List b, Comparator c) {
        if (a == null || a.isEmpty()) {
            return b != null && b.isEmpty() ? null : b;
        }
        if (b == null || b.isEmpty()) {
            return a != null && a.isEmpty() ? null : a;
        }
        List diference = ListUtil.remove(a, b, c);
        List d2 = ListUtil.remove(b, a, c);
        if (diference == null) {
            return d2;
        }
        if (d2 == null) {
            return diference;
        }
        diference.addAll(d2);
        return diference;
    }

    public static boolean equals(List a, List b) {
        if (!ListUtil.contains(a, b)) {
            return false;
        }
        return ListUtil.contains(b, a);
    }

    public static boolean equals(List a, List b, Comparator c) {
        if (!ListUtil.contains(a, b, c)) {
            return false;
        }
        return ListUtil.contains(b, a, c);
    }

    public static int indexOf(List list, Object obj, Comparator c) {
        if (ListUtil.isEmpty(list)) {
            return -1;
        }
        for (int index = 0; index < list.size(); ++index) {
            Object listObj = list.get(index);
            if (c.compare(listObj, obj) != 0) continue;
            return index;
        }
        return -1;
    }

    public static boolean trueForAll(List a, Predicate p) {
        if (a == null || a.isEmpty()) {
            return true;
        }
        for (Object obj : a) {
            if (p.evaluate(obj)) continue;
            return false;
        }
        return true;
    }

    public static boolean trueForOne(List a, Predicate p) {
        if (a == null || a.isEmpty()) {
            return false;
        }
        for (Object obj : a) {
            if (!p.evaluate(obj)) continue;
            return true;
        }
        return false;
    }

    public static boolean trueForOnlyOne(List a, Predicate p) {
        if (a == null || a.isEmpty()) {
            return false;
        }
        return ListUtil.remove(a, p).size() == a.size() - 1;
    }

    public static void doForAll(List list, Closure closure) {
        if (list != null) {
            for (Object obj : list) {
                closure.execute(obj);
            }
        }
    }

    public static <I, O> List<O> collect(List<I> list, Transformer<I, O> transformer) {
        if (list == null || transformer == null) {
            return null;
        }
        ArrayList<O> result = new ArrayList<O>(list.size());
        for (I i : list) {
            result.add(transformer.transform(i));
        }
        return result;
    }

    public static boolean isEmpty(List list) {
        return list == null || list.isEmpty();
    }

    private ListUtil() {
    }

    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add(new Integer(1));
        list.add(new Integer(2));
        list.add(new Integer(3));
        System.out.println("list: " + ListUtil.toString(list));
        System.out.println("list: " + ListUtil.toString(list, ","));
        System.out.println("list: " + ListUtil.toString(list, null, "\""));
        System.out.println("list: " + ListUtil.toString(list, ",", "\""));
        System.out.println("list: " + ListUtil.toString(list, ":", "'"));
        System.out.println("list: " + ListUtil.toString(list, ",", "'"));
        System.out.println("list: " + ListUtil.toString(list, ",", null));
        list.add("e1");
        list.add("e2");
        list.add("e3");
        System.out.println("list: " + ListUtil.toString(list));
        System.out.println("list: " + ListUtil.toString(list, ","));
        System.out.println("list: " + ListUtil.toString(list, null, "\""));
        System.out.println("list: " + ListUtil.toString(list, ",", "\""));
        System.out.println("list: " + ListUtil.toString(list, ":", "'"));
        System.out.println("list: " + ListUtil.toString(list, ",", "'"));
        System.out.println("list: " + ListUtil.toString(list, ",", null));
    }

    public static String toRangeString(List list, String separator) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        Collections.sort(list, new Comparator(){

            public int compare(Object o1, Object o2) {
                int i1 = Integer.parseInt((String)o1);
                int i2 = Integer.parseInt((String)o2);
                return i1 - i2;
            }
        });
        StringBuffer result = new StringBuffer();
        int firstInRange = -1;
        int lastInRange = -1;
        boolean first = true;
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            int entry = Integer.parseInt((String)iterator.next());
            if (first) {
                firstInRange = entry;
                lastInRange = entry;
                first = false;
                continue;
            }
            if (entry == lastInRange + 1) {
                lastInRange = entry;
                continue;
            }
            ListUtil.appendRange(result, firstInRange, lastInRange);
            result.append(separator);
            firstInRange = entry;
            lastInRange = entry;
        }
        ListUtil.appendRange(result, firstInRange, lastInRange);
        return result.toString();
    }

    private static void appendRange(StringBuffer buffer, int first, int last) {
        if (first == last) {
            buffer.append(first);
        } else {
            buffer.append("" + first + "-" + last);
        }
    }

    public static List reverse(List list) {
        ArrayList result = new ArrayList(list.size());
        for (Object obj : list) {
            result.add(obj);
        }
        Collections.reverse(result);
        return result;
    }

}

