/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Locale;
import java.util.Properties;
import pt.opensoft.util.Parameters;

public class SystemParameters
extends Parameters {
    public static final SystemParameters instance = new SystemParameters();
    public static final String LINE_SEPARATOR = instance.getString("line.separator");
    public static final int FILE_BLOCK_SIZE = instance.getIntValue("file.block.size", 8192);
    public static final int DEFAULT_STATS_INTERVAL = instance.getIntValue("default.stats.interval", 1000);
    public static final int DEFAULT_SATISFIED_THRESHOLD = instance.getIntValue("default.stats.satisfied.threshold", 500);
    public static final int DEFAULT_TOLERATING_THRESHOLD = instance.getIntValue("default.stats.tolerating.threshold", 4 * DEFAULT_SATISFIED_THRESHOLD);
    public static final String PT_LANGUAGE = "PT";
    public static final String PT_COUNTRY = "pt";
    public static final Locale PT_LOCALE = new Locale("PT", "pt");
    public static final String DEFAULT_LOCALE_LANGUAGE = instance.getString("default.locale.language", "PT");
    public static final String DEFAULT_LOCALE_NAME = instance.getString("default.locale.country", "pt");
    public static final Locale DEFAULT_LOCALE = new Locale(DEFAULT_LOCALE_LANGUAGE, DEFAULT_LOCALE_NAME);
    public static final String JAVA_VERSION = instance.getString("java.version");
    public static final String JAVA_MAJOR_VERSION = JAVA_VERSION.substring(0, 3);
    public static final String DEFAULT_ENCODING = "ISO8859_1";
    public static final String OS_NAME = instance.getString("os.name");

    private SystemParameters() {
        super("SystemParameters", System.getProperties());
    }

    public static boolean isJavaVersionLessThan(String min) {
        return JAVA_MAJOR_VERSION.compareTo(min) < 0;
    }

    public static boolean isJavaVersionGreaterThan(String max) {
        return JAVA_MAJOR_VERSION.compareTo(max) > 0;
    }

    public static boolean isWindows() {
        return OS_NAME.indexOf("Windows") != -1;
    }

    @Override
    public Object getValue(String name) {
        if (this.values == null) {
            return null;
        }
        return this.values.getProperty(name);
    }

    @Override
    public Object remove(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object setValue(String name, Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getString(String name, String defaultValue) {
        return this.getString(name, defaultValue, false);
    }

    static {
        Locale.setDefault(DEFAULT_LOCALE);
    }
}

