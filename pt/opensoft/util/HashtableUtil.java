/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import pt.opensoft.util.VectorUtil;

public class HashtableUtil {
    public static Hashtable toHashtable(String map) {
        return HashtableUtil.toHashtable(map, null, null);
    }

    public static Hashtable toHashtable(String map, String sep1, String sep2) {
        StringTokenizer st1;
        if (map == null) {
            return null;
        }
        if (sep1 == null) {
            sep1 = ",";
        }
        if (sep2 == null) {
            sep2 = "=";
        }
        if (!(st1 = new StringTokenizer(map, sep1)).hasMoreTokens()) {
            return null;
        }
        Hashtable<String, String> values = new Hashtable<String, String>();
        while (st1.hasMoreTokens()) {
            String pair = st1.nextToken().trim();
            StringTokenizer st2 = new StringTokenizer(pair, sep2);
            values.put(st2.nextToken().trim(), st2.nextToken().trim());
        }
        return values;
    }

    public static String toString(Hashtable map) {
        return HashtableUtil.toString(map, "=", ",");
    }

    public static String toString(Hashtable map, String sep1, String sep2) {
        StringBuffer buffer = new StringBuffer(map.size() * 10 * 2);
        Enumeration en = map.keys();
        Vector keys = VectorUtil.toVector(en);
        en = keys.elements();
        while (en.hasMoreElements()) {
            Object key = en.nextElement();
            Object value = map.get(key);
            buffer.append(key).append(sep1).append(value).append(sep2);
        }
        return buffer.toString();
    }

    private HashtableUtil() {
    }
}

