/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.Year;

public class Month
extends DateTime {
    public static final int JANEIRO = 0;
    public static final int FEVEREIRO = 1;
    public static final int MARCO = 2;
    public static final int ABRIL = 3;
    public static final int MAIO = 4;
    public static final int JUNHO = 5;
    public static final int JULHO = 6;
    public static final int AGOSTO = 7;
    public static final int SETEMBRO = 8;
    public static final int OUTUBRO = 9;
    public static final int NOVEMBRO = 10;
    public static final int DEZEMBRO = 11;
    public static final String MONTH_FORMAT = "yyyy-MM";
    protected static final String[] NAMES_EN = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    protected static final String[] NAMES_PT = new String[]{"janeiro", "fevereiro", "mar\u00e7o", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"};
    protected static final int[] DAYS = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public static int getDays(int month) {
        if (month == 1) {
            throw new IllegalArgumentException("cannot return month days in february without year");
        }
        return DAYS[month];
    }

    public static int getDays(int year, int month) {
        if (month == 1 && Year.isLeapYear(year)) {
            return 29;
        }
        return DAYS[month];
    }

    public static String getName(int month) {
        return NAMES_PT[month];
    }

    public static String getShortName(int month) {
        return NAMES_PT[month].substring(0, 3);
    }

    public static int getFirstWeekDay(int year, int month) {
        DateTime now = new DateTime(year, month, 1);
        return now.getWeekDay();
    }

    public static int getCurrentMonth() {
        return new Month().get(2) + 1;
    }

    public static int valueOf(String month, String language) {
        if (month.length() <= 2) {
            throw new IllegalArgumentException("ambiguous month: " + month);
        }
        String[] name = language.equalsIgnoreCase("PT") ? NAMES_PT : NAMES_EN;
        for (int i = 0; i < name.length; ++i) {
            if (!name[i].toUpperCase().startsWith(month.toUpperCase())) continue;
            return i;
        }
        throw new IllegalArgumentException("unknown month: " + month);
    }

    public static boolean isMonth(String month) {
        int monthvalue;
        if (StringUtil.isNumeric(month) && month.length() > 0 && month.length() <= 2 && (monthvalue = Integer.parseInt(month) - 1) >= 0 && monthvalue <= 11) {
            return true;
        }
        return false;
    }

    public Month() {
        this.setFormat("yyyy-MM");
    }

    public Month(int year, int month) {
        this();
        this.setDateTime(year, month);
    }

    public Month(Date date) {
        super(date);
    }

    public DateTime getFirstDate() {
        return new DateTime(this.getYear(), this.getMonth(), 1, 0, 0, 0, 0);
    }

    public DateTime getLastDate() {
        return new DateTime(this.getYear(), this.getMonth(), Month.getDays(this.getYear(), this.getMonth()), 23, 59, 59, 999);
    }
}

