/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.File;
import java.io.Serializable;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.WrappedError;

public class ClassUtil {
    public static String getPackageName(Class clazz) {
        return ClassUtil.getPackageName(clazz.getName());
    }

    public static String getPackageName(String fullClassName) {
        if (fullClassName == null) {
            return null;
        }
        int pos = fullClassName.lastIndexOf(".");
        if (pos == -1) {
            return null;
        }
        return fullClassName.substring(0, pos);
    }

    public static String getClassName(Class clazz) {
        return ClassUtil.getClassName(clazz.getName());
    }

    public static String getClassName(String fullClassName) {
        if (fullClassName == null) {
            return null;
        }
        int pos = fullClassName.lastIndexOf(".");
        if (pos == -1) {
            return fullClassName;
        }
        return fullClassName.substring(pos + 1);
    }

    public static Class getClass(String name) {
        try {
            return Class.forName(name);
        }
        catch (ClassNotFoundException e) {
            throw new Error(e.getMessage());
        }
    }

    public static Object newInstance(String name) {
        try {
            return Class.forName(name).newInstance();
        }
        catch (ClassNotFoundException e) {
            throw new Error(e.getMessage());
        }
        catch (InstantiationException e) {
            throw new InstantiationError(e.getMessage());
        }
        catch (IllegalAccessException e) {
            throw new IllegalAccessError(e.getMessage());
        }
    }

    public static Object newInstance(Class clazz) {
        try {
            return clazz.newInstance();
        }
        catch (InstantiationException e) {
            throw new InstantiationError(e.getMessage());
        }
        catch (IllegalAccessException e) {
            throw new IllegalAccessError(e.getMessage());
        }
    }

    public static Object invoke(Object instance, String method, Object value) {
        return ClassUtil.invoke(instance, method, new Object[]{value});
    }

    public static Object invoke(Object instance, String method, Object value1, Object value2) {
        return ClassUtil.invoke(instance, method, new Object[]{value1, value2});
    }

    public static Object invoke(Object instance, String method, Object[] values) {
        try {
            Method m = instance.getClass().getMethod(method, ClassUtil.getClasses(values));
            return m.invoke(instance, values);
        }
        catch (InvocationTargetException e) {
            throw new WrappedError(e.getTargetException());
        }
        catch (Exception e) {
            throw new WrappedError(e);
        }
    }

    public static Class[] getClasses(Object[] values) {
        if (values == null) {
            return null;
        }
        Class[] classes = new Class[values.length];
        for (int i = 0; i < values.length; ++i) {
            classes[i] = values[i].getClass();
        }
        return classes;
    }

    public static File getDirectory(File basedir, Class clazz) {
        String dir = StringUtil.replace(ClassUtil.getPackageName(clazz), ".", "/");
        return new File(basedir, dir);
    }

    public static Object castAndInvoke(Method method, Object methodObject, Object parameterValue) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (!(parameterValue == null || parameterValue.getClass().equals(String.class) || parameterValue.getClass().equals(byte[].class))) {
            throw new IllegalArgumentException("parameterValue has to be a String or a byte[]");
        }
        Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length == 0) {
            if (parameterValue == null) {
                return method.invoke(methodObject, new Object[0]);
            }
            throw new IllegalArgumentException("parameterValue is not null but method doesn't have arguments");
        }
        Class<Serializable> parameterClass = parameterTypes[0];
        if (parameterClass == null) {
            return null;
        }
        if (parameterClass.isAssignableFrom(String.class)) {
            return method.invoke(methodObject, (String)parameterValue);
        }
        if (parameterClass.isAssignableFrom(Integer.TYPE)) {
            return method.invoke(methodObject, new Integer((String)parameterValue));
        }
        if (parameterClass.isAssignableFrom(Long.TYPE)) {
            return method.invoke(methodObject, new Long((String)parameterValue));
        }
        if (parameterClass.isAssignableFrom(Double.TYPE)) {
            return method.invoke(methodObject, new Double((String)parameterValue));
        }
        if (parameterClass.isAssignableFrom(Short.TYPE)) {
            return method.invoke(methodObject, new Short((String)parameterValue));
        }
        if (parameterClass.isAssignableFrom(Byte.TYPE)) {
            return method.invoke(methodObject, new Byte((String)parameterValue));
        }
        if (parameterClass.isAssignableFrom(byte[].class)) {
            return method.invoke(methodObject, (byte[])parameterValue);
        }
        if (parameterClass.isAssignableFrom(Float.TYPE)) {
            return method.invoke(methodObject, new Float((String)parameterValue));
        }
        if (parameterClass.isAssignableFrom(Boolean.TYPE)) {
            return method.invoke(methodObject, new Boolean((String)parameterValue));
        }
        if (parameterClass.isAssignableFrom(DateTime.class)) {
            return method.invoke(methodObject, new DateTime((String)parameterValue));
        }
        return null;
    }

    public static String getVariableFormatClassName(Class clazz) {
        String className = ClassUtil.getClassName(clazz.getName());
        if (className.length() > 0) {
            className = "" + Character.toLowerCase(className.charAt(0)) + className.substring(1);
        }
        return className;
    }

    public static String getVariableFormatClassName(String fullClassName) {
        String className = ClassUtil.getClassName(fullClassName);
        if (className.length() > 0) {
            className = "" + Character.toLowerCase(className.charAt(0)) + className.substring(1);
        }
        return className;
    }

    private ClassUtil() {
    }
}

