/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import pt.opensoft.text.Regex;
import pt.opensoft.util.ListMap;

public class ListHashMap<K extends Comparable<? super K>, V>
extends HashMap<K, V>
implements ListMap<K, V> {
    private static final long serialVersionUID = 8424365340457329587L;
    protected List<K> keys;
    protected List<V> elements;

    public ListHashMap() {
        this.keys = new ArrayList<K>();
        this.elements = new ArrayList<V>();
    }

    public ListHashMap(int initialCapacity) {
        super(initialCapacity);
        this.keys = new ArrayList<K>(initialCapacity);
        this.elements = new ArrayList<V>(initialCapacity);
    }

    public ListHashMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
        this.keys = new ArrayList<K>(initialCapacity);
        this.elements = new ArrayList<V>(initialCapacity);
    }

    @Override
    public void clear() {
        super.clear();
        this.keys.clear();
        this.elements.clear();
    }

    @Override
    public List<V> elements() {
        return this.elements;
    }

    @Override
    public List<K> keys() {
        return this.keys;
    }

    public List<K> keys(String pattern) {
        Regex regex = new Regex(pattern);
        List<K> names = this.keys();
        ArrayList<Comparable> result = new ArrayList<Comparable>(names.size());
        for (int i = 0; i < names.size(); ++i) {
            Comparable key = (Comparable)names.get(i);
            String name = key.toString();
            if (!regex.contains(name)) continue;
            result.add(key);
        }
        return result;
    }

    public List<K> keysSorted() {
        return this.keysSorted(true);
    }

    public List<K> keysSorted(boolean ascending) {
        List<K> lkeys = this.keys();
        Collections.sort(lkeys);
        if (!ascending) {
            Collections.reverse(lkeys);
        }
        return lkeys;
    }

    @Override
    public V put(K key, V value) {
        int idx;
        if (this.keys == null) {
            this.keys = new ArrayList<K>();
        }
        if (this.elements == null) {
            this.elements = new ArrayList<V>();
        }
        if ((idx = this.keys.indexOf(key)) != -1) {
            return this.replace(key, value);
        }
        this.keys.add(key);
        this.elements.add(value);
        return super.put(key, value);
    }

    @Override
    public V remove(Object key) {
        Object value = super.remove(key);
        if (value == null) {
            return null;
        }
        if (this.keys != null) {
            this.keys.remove(key);
        }
        if (this.elements != null) {
            this.elements.remove(value);
        }
        return value;
    }

    @Override
    public V getElement(int index) {
        return this.elements.get(index);
    }

    @Override
    public K getKey(int index) {
        return (K)((Comparable)this.keys.get(index));
    }

    @Override
    public boolean addAll(ListMap<? extends K, ? extends V> map) {
        if (map == null) {
            return false;
        }
        List<K> lkeys = map.keys();
        for (Comparable o : lkeys) {
            this.put((K)o, map.get(o));
        }
        return true;
    }

    @Override
    public V replace(K key, V value) {
        int idx = this.keys.indexOf(key);
        if (idx == -1) {
            return value;
        }
        this.elements.set(idx, value);
        return super.put(key, value);
    }

    @Override
    public String toString() {
        return this.toString(",", "=");
    }

    public String toString(String keySeparator) {
        return this.toString(keySeparator, "=");
    }

    public String toString(String keySeparator, String nameValueSeparator) {
        if (this.keys == null) {
            return null;
        }
        StringBuffer buffer = new StringBuffer(this.keys.size() * 2 * 4);
        for (Comparable key : this.keys) {
            Object value = this.get(key);
            if (buffer.length() != 0) {
                buffer.append(keySeparator);
            }
            buffer.append(key);
            buffer.append(nameValueSeparator);
            buffer.append(value);
        }
        return buffer.toString();
    }
}

