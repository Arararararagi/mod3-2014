/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.zip.CRC32;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import pt.opensoft.util.ArrayUtil;
import pt.opensoft.util.ByteUtil;
import pt.opensoft.util.StringUtil;

public class ZipUtil {
    protected static final int BUFFER_SIZE = 8192;

    public static String deflate(String text) throws IOException {
        return ByteUtil.toString(ZipUtil.deflate(StringUtil.toByteArray(text)));
    }

    public static byte[] deflate(byte[] bytes) throws IOException {
        return ZipUtil.deflate(bytes, 0, bytes.length);
    }

    public static byte[] deflate(byte[] bytes, int off, int len) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            byte[] arrby;
            DeflaterOutputStream zip = new DeflaterOutputStream(out);
            try {
                zip.write(bytes, off, len);
                zip.finish();
                arrby = out.toByteArray();
            }
            catch (Throwable var6_6) {
                zip.close();
                throw var6_6;
            }
            zip.close();
            return arrby;
        }
        finally {
            out.close();
        }
    }

    public static String inflate(String text) throws IOException {
        return ByteUtil.toString(ZipUtil.inflate(StringUtil.toByteArray(text)));
    }

    public static byte[] inflate(byte[] bytes) throws IOException {
        return ZipUtil.inflate(bytes, 0, bytes.length);
    }

    /*
     * Exception decompiling
     */
    public static byte[] inflate(byte[] bytes, int off, int len) throws IOException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [1[TRYBLOCK]], but top level block is 3[TRYBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:392)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:444)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2802)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:787)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:214)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:159)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:91)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:353)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:731)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:663)
        // org.benf.cfr.reader.Main.doJar(Main.java:126)
        // org.benf.cfr.reader.Main.main(Main.java:178)
        throw new IllegalStateException("Decompilation failed");
    }

    public static void retrieveFileFromZip(File zipDir, String zipFilename, String fileName, String outFileName) throws FileNotFoundException, IOException {
        FileOutputStream os;
        ZipInputStream in = new ZipInputStream(new FileInputStream(new File(zipDir, zipFilename)));
        ZipEntry zipEntry = in.getNextEntry();
        if (zipEntry.getName().equals(fileName)) {
            os = new FileOutputStream(new File(zipDir, outFileName));
            byte[] buf = new byte[1024];
            int qnt = 0;
            while ((qnt = in.read(buf)) > 0) {
                os.write(buf, 0, qnt);
            }
        } else {
            throw new IOException("Not found " + fileName + " in " + zipFilename);
        }
        in.close();
        os.close();
    }

    public static void getContentFromZip(InputStream in, OutputStream out) throws IOException {
        ZipUtil.getContentFromZip(in, out, null);
    }

    public static void getContentFromZip(InputStream in, OutputStream out, String fileName) throws IOException {
        ZipInputStream zin = new ZipInputStream(in);
        try {
            int read;
            ZipEntry zipEntry = zin.getNextEntry();
            if (!(StringUtil.isEmpty(fileName) || zipEntry.getName().equalsIgnoreCase(fileName))) {
                throw new IOException("The name inside the zip file doesn't match the zip file.");
            }
            byte[] buf = new byte[8192];
            while ((read = zin.read(buf)) > 0) {
                out.write(buf, 0, read);
            }
            out.flush();
        }
        finally {
            zin.close();
        }
    }

    public static boolean testZipFile(String filename) throws IOException {
        ZipFile file = new ZipFile(filename);
        return ZipUtil.testZipFile(file);
    }

    public static boolean testZipFile(ZipFile file) throws IOException {
        boolean fileOk = true;
        Enumeration<? extends ZipEntry> en = file.entries();
        while (fileOk && en.hasMoreElements()) {
            ZipEntry entry = en.nextElement();
            fileOk = ZipUtil.testZipEntry(file, entry);
        }
        return fileOk;
    }

    public static boolean testZipEntry(ZipFile file, ZipEntry entry) throws IOException {
        int bytes;
        CRC32 check = new CRC32();
        long crc32 = entry.getCrc();
        InputStream in = file.getInputStream(entry);
        byte[] array = new byte[8192];
        while ((bytes = in.read(array, 0, 8192)) != -1) {
            check.update(array, 0, bytes);
        }
        return check.getValue() == crc32;
    }

    public static void listZipFile(String filename) throws IOException {
        ZipFile file = new ZipFile(filename);
        System.out.println("Zip file name: " + filename);
        Enumeration<? extends ZipEntry> en = file.entries();
        while (en.hasMoreElements()) {
            ZipEntry entry;
            System.out.println(entry.toString() + "\t" + ((entry = en.nextElement()).getMethod() == 8 ? "deflated" : "stored") + "\t" + entry.getSize());
        }
    }

    public static void makeZipFile(String zipfilename, List filenames) throws IOException {
        ZipUtil.makeZipFile(zipfilename, null, filenames);
    }

    public static void makeZipFile(String zipfilename, File dir, List filenames) throws IOException {
        if (filenames.size() == 0) {
            return;
        }
        File zipfile = dir != null ? new File(dir, zipfilename) : new File(zipfilename);
        FileOutputStream fos = new FileOutputStream(zipfile);
        try {
            ZipOutputStream zos = new ZipOutputStream(fos);
            try {
                zos.setMethod(8);
                for (String filename : filenames) {
                    File file = dir != null ? new File(dir, filename) : new File(filename);
                    ZipUtil.addFileToZip(zos, file);
                }
            }
            finally {
                zos.close();
            }
        }
        finally {
            fos.close();
        }
    }

    public static void makeZipFile(String zipfilename, String filename) throws IOException {
        ZipUtil.makeZipFile(zipfilename, new File(filename));
    }

    public static void makeZipFile(String zipfilename, File f) throws IOException {
        FileOutputStream fos = new FileOutputStream(zipfilename);
        try {
            ZipOutputStream zos = new ZipOutputStream(fos);
            try {
                zos.setMethod(8);
                ZipUtil.addFileToZip(zos, f);
            }
            finally {
                zos.close();
            }
        }
        finally {
            fos.close();
        }
    }

    public static void addFileToZip(ZipOutputStream zos, File f) throws IOException {
        FileInputStream fin = new FileInputStream(f);
        try {
            BufferedInputStream file = new BufferedInputStream(fin);
            try {
                int bytes;
                ZipEntry entry = new ZipEntry(f.getName());
                entry.setMethod(8);
                zos.putNextEntry(entry);
                byte[] array = new byte[8192];
                while ((bytes = file.read(array, 0, 8192)) != -1) {
                    zos.write(array, 0, bytes);
                }
                zos.closeEntry();
            }
            finally {
                file.close();
            }
        }
        finally {
            fin.close();
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public static String[] listAllFilesFromZip(ZipInputStream zipContent) {
        v = new Vector<String>();
        zipEntry = null;
        while ((zipEntry = zipContent.getNextEntry()) != null) {
            if (zipEntry.isDirectory()) continue;
            v.add(zipEntry.getName());
        }
        try {
            zipContent.close();
        }
        catch (Exception e) {}
        ** GOTO lbl26
        catch (IOException e) {
            try {
                zipContent.close();
            }
            catch (Exception e) {}
            catch (Throwable var3_6) {
                try {
                    zipContent.close();
                    throw var3_6;
                }
                catch (Exception e) {
                    // empty catch block
                }
                throw var3_6;
            }
        }
lbl26: // 4 sources:
        if (v.size() != 0) return ArrayUtil.toStringArray(v);
        return null;
    }

    public static String[] listAllFilesFromZip(InputStream zipContent) {
        return ZipUtil.listAllFilesFromZip(new ZipInputStream(zipContent));
    }

    private ZipUtil() {
    }
}

