/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.StringUtil;

public class Year
extends DateTime {
    public static final String YEAR_FORMAT = "yyyy";

    public static int getCurrentYear() {
        return new Year().getYear();
    }

    public static boolean isLeapYear(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }

    public static boolean isYear(String year) {
        return StringUtil.isNumeric(year) && year.length() == 4;
    }

    public static boolean isYear(int year) {
        return Year.isYear(String.valueOf(year));
    }

    public Year() {
        this.setFormat("yyyy");
    }

    public Year(int year) {
        this();
        this.setDateTime(year, 0, 1, 0, 0, 0, 0);
    }

    public Year(int year, boolean endOfYear) {
        this();
        if (endOfYear) {
            this.setDateTime(year, 11, 31, 23, 59, 59, 999);
        } else {
            this.setDateTime(year, 0, 1, 0, 0, 0, 0);
        }
    }

    public Year(Date date) {
        super(date);
    }

    public DateTime getFirstDate() {
        return new DateTime(this.getYear(), 0, 1, 0, 0, 0, 0);
    }

    public DateTime getLastDate() {
        return new DateTime(this.getYear(), 11, 31, 23, 59, 59, 999);
    }
}

