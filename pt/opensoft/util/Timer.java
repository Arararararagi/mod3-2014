/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

public class Timer {
    protected long start = Long.MIN_VALUE;
    protected long stop = Long.MIN_VALUE;

    public long ellapsed() {
        if (!this.hasStarted()) {
            throw new IllegalStateException("cannot measure ellapsed before starting");
        }
        if (this.isStopped()) {
            return this.stop - this.start;
        }
        return System.currentTimeMillis() - this.start;
    }

    public long getStart() {
        return this.start;
    }

    public long getStop() {
        return this.stop;
    }

    public boolean hasStarted() {
        return this.start != Long.MIN_VALUE;
    }

    public boolean isStopped() {
        return this.stop != Long.MIN_VALUE;
    }

    public boolean isStarted() {
        return this.start != Long.MIN_VALUE;
    }

    public void reset() {
        this.start = Long.MIN_VALUE;
        this.stop = Long.MIN_VALUE;
    }

    public long start() {
        this.reset();
        this.start = System.currentTimeMillis();
        return this.start;
    }

    public long stop() {
        if (!this.hasStarted()) {
            throw new IllegalStateException("cannot stop before starting");
        }
        if (this.isStopped()) {
            return this.stop;
        }
        this.stop = System.currentTimeMillis();
        return this.stop;
    }

    public String toString() {
        if (this.isStopped()) {
            return String.valueOf(this.stop - this.start);
        }
        if (this.hasStarted()) {
            return String.valueOf(this.ellapsed());
        }
        return String.valueOf(this.start);
    }
}

