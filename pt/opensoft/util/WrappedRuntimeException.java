/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.PrintStream;
import java.io.PrintWriter;

public class WrappedRuntimeException
extends RuntimeException {
    protected Throwable thrown = null;

    public WrappedRuntimeException(Throwable thrown) {
        this.thrown = thrown;
    }

    @Override
    public String getLocalizedMessage() {
        return this.thrown.getLocalizedMessage();
    }

    @Override
    public String getMessage() {
        return this.thrown.getMessage();
    }

    @Override
    public void printStackTrace(PrintStream s) {
        this.thrown.printStackTrace(s);
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        this.thrown.printStackTrace(s);
    }

    @Override
    public String toString() {
        return this.thrown.toString();
    }
}

