/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Date;
import pt.opensoft.util.DateTime;

public class Time
extends DateTime {
    public static final DateTime ZERO_TIME = new Time("00:00:00");

    public Time() {
        this.setFormat("HH:mm:ss");
    }

    public Time(String date) {
        this();
        this.setDateTime(date);
    }

    public Time(Date date) {
        this();
        this.setDateTime(date);
    }

    public Time(long millis) {
        this();
        this.setDateTime(millis);
        this.addHours(-1);
    }

    public Time(int hour, int minute, int seconds) {
        this();
        this.setTime(hour, minute, seconds);
    }

    public Time(int hour, int minute, int seconds, int millis) {
        this();
        this.setTime(hour, minute, seconds, millis);
    }
}

