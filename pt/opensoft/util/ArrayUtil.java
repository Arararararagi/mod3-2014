/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;
import pt.opensoft.util.VectorUtil;

public class ArrayUtil {
    public static String[] init(int size, String str) {
        String[] vector = new String[size];
        for (int i = 0; i < size; ++i) {
            vector[i] = str;
        }
        return vector;
    }

    public static String[] init(int size, String[] initVector) {
        String[] vector = new String[size];
        for (int i = 0; i < size; ++i) {
            vector[i] = initVector[i];
        }
        return vector;
    }

    public static int[] toIntArray(Vector vector) {
        int[] result = new int[vector.size()];
        int i = 0;
        Enumeration en = vector.elements();
        while (en.hasMoreElements()) {
            Integer integer = (Integer)en.nextElement();
            result[i++] = integer;
        }
        return result;
    }

    public static String[] appendString(String[] array, String newString) {
        return ArrayUtil.concat(array, new String[]{newString});
    }

    public static String[] prependString(String[] array, String newString) {
        return ArrayUtil.concat(new String[]{newString}, array);
    }

    public static String[] concat(String[] array1, String[] array2) {
        if (array1 == null && array2 == null) {
            return new String[0];
        }
        if (array1 == null) {
            return array2;
        }
        if (array2 == null) {
            return array1;
        }
        String[] result = new String[array1.length + array2.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        return result;
    }

    public static String[] toStringArray(Enumeration en) {
        return ArrayUtil.toStringArray(VectorUtil.toVector(en));
    }

    public static String[] toStringArray(Vector vector) {
        String[] values = new String[vector.size()];
        for (int i = 0; i < values.length; ++i) {
            values[i] = vector.elementAt(i).toString();
        }
        return values;
    }

    public static String[] toStringArray(String src) {
        return ArrayUtil.toStringArray(src, ",");
    }

    public static String[] toStringArray(String src, String separator) {
        if (src == null) {
            return null;
        }
        StringTokenizer st = new StringTokenizer(src, separator);
        int size = st.countTokens();
        String[] result = new String[size];
        for (int i = 0; i < size; ++i) {
            result[i] = st.nextToken();
        }
        return result;
    }

    public static String toString(Object[] array) {
        return ArrayUtil.toString(array, ", ");
    }

    public static String toString(Object[] array, String separator) {
        return ArrayUtil.toString(array, separator, null);
    }

    public static String toString(Object[] array, String separator, String delimiter) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            if (i > 0) {
                buffer.append(separator);
            }
            if (delimiter != null) {
                buffer.append(delimiter);
            }
            buffer.append(array[i]);
            if (delimiter == null) continue;
            buffer.append(delimiter);
        }
        return buffer.toString();
    }

    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean in(int elem, int[] arr) {
        if (arr == null || arr.length == 0) {
            return false;
        }
        for (int i = 0; i < arr.length; ++i) {
            if (elem != arr[i]) continue;
            return true;
        }
        return false;
    }

    private ArrayUtil() {
    }
}

