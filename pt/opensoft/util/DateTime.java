/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;
import pt.opensoft.calendar.Holiday;
import pt.opensoft.calendar.Holidays;
import pt.opensoft.calendar.MoveableHoliday;
import pt.opensoft.calendar.NonWorkingDays;
import pt.opensoft.util.EuropeLisbonTimeZone;
import pt.opensoft.util.InvalidDate;
import pt.opensoft.util.Month;
import pt.opensoft.util.Week;
import pt.opensoft.util.Year;

public class DateTime
extends java.util.Date
implements Serializable {
    private static final long serialVersionUID = -1298273333452666231L;
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String DATELONG_FORMAT = "d 'de' MMMM 'de' yyyy";
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    protected DateFormat formater = null;
    protected String format = "yyyy-MM-dd HH:mm:ss";
    protected Calendar calendar;
    protected boolean lenient;
    protected TimeZone timezone;

    public DateTime() {
    }

    public DateTime(TimeZone timezone) {
        this.setTimeZone(timezone);
    }

    public DateTime(java.util.Date date) {
        this.setDateTime(date);
    }

    public DateTime(java.util.Date date, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setDateTime(date);
    }

    public DateTime(java.util.Date date, boolean lenient) {
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(java.util.Date date, TimeZone timezone, boolean lenient) {
        this.setTimeZone(timezone);
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(java.util.Date date, String format) {
        this.setFormat(format);
        this.setDateTime(date);
    }

    public DateTime(java.util.Date date, String format, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setFormat(format);
        this.setDateTime(date);
    }

    public DateTime(java.util.Date date, String format, boolean lenient) {
        this.setFormat(format);
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(java.util.Date date, String format, TimeZone timezone, boolean lenient) {
        this.setTimeZone(timezone);
        this.setFormat(format);
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(Timestamp date) {
        this.setDateTime(date);
    }

    public DateTime(Timestamp date, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setDateTime(date);
    }

    public DateTime(Timestamp date, boolean lenient) {
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(Timestamp date, TimeZone timezone, boolean lenient) {
        this.setTimeZone(timezone);
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(Timestamp date, String format, boolean lenient) {
        this.setFormat(format);
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(Timestamp date, String format, TimeZone timezone, boolean lenient) {
        this.setTimeZone(timezone);
        this.setFormat(format);
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(String date) {
        this.setDateTime(date);
    }

    public DateTime(String date, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setDateTime(date);
    }

    public DateTime(String date, boolean lenient) {
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(String date, TimeZone timezone, boolean lenient) {
        this.setTimeZone(timezone);
        this.setLenient(lenient);
        this.setDateTime(date);
    }

    public DateTime(String date, String format) {
        this.setFormat(format);
        this.setDateTime(date);
    }

    public DateTime(String date, String format, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setFormat(format);
        this.setDateTime(date);
    }

    public DateTime(String date, String format, boolean lenient) {
        this.setLenient(lenient);
        this.setFormat(format);
        this.setDateTime(date);
    }

    public DateTime(String date, String format, TimeZone timezone, boolean lenient) {
        this.setTimeZone(timezone);
        this.setLenient(lenient);
        this.setFormat(format);
        this.setDateTime(date);
    }

    public DateTime(int year, int month, int day) {
        this.setDateTime(year, month, day);
    }

    public DateTime(int year, int month, int day, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setDateTime(year, month, day);
    }

    public DateTime(int year, int month, int day, int hour, int minute, int second) {
        this.setDateTime(year, month, day, hour, minute, second);
    }

    public DateTime(int year, int month, int day, int hour, int minute, int second, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setDateTime(year, month, day, hour, minute, second);
    }

    public DateTime(int year, int month, int day, int hour, int minute, int second, int millis) {
        this.setDateTime(year, month, day, hour, minute, second, millis);
    }

    public DateTime(int year, int month, int day, int hour, int minute, int second, int millis, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setDateTime(year, month, day, hour, minute, second, millis);
    }

    public DateTime(long millis) {
        this.setDateTime(millis);
    }

    public DateTime(long millis, TimeZone timezone) {
        this.setTimeZone(timezone);
        this.setDateTime(millis);
    }

    public DateTime add(int field, int ammount) {
        Calendar calendar = this.getCalendar();
        try {
            calendar.setLenient(true);
            calendar.add(field, ammount);
            super.setTime(calendar.getTimeInMillis());
            DateTime dateTime = this;
            return dateTime;
        }
        finally {
            calendar.setLenient(this.lenient);
        }
    }

    public DateTime addYears(int years) {
        return this.add(1, years);
    }

    public DateTime addMonths(int months) {
        return this.add(2, months);
    }

    public DateTime addDays(int days) {
        return this.add(5, days);
    }

    public DateTime addHours(int hours) {
        return this.add(11, hours);
    }

    public DateTime addMinutes(int minutes) {
        return this.add(12, minutes);
    }

    public DateTime addSeconds(int seconds) {
        return this.add(13, seconds);
    }

    public DateTime addMillis(int millis) {
        return this.add(14, millis);
    }

    public DateTime addYearWeeks(int weeks) {
        return this.add(3, weeks);
    }

    public int daysUntil(DateTime end) {
        long millis = end.getTime() - this.getTime();
        return (int)Math.floor((float)millis / 1000.0f / 60.0f / 60.0f / 24.0f);
    }

    @Deprecated
    public int yearsUntil(DateTime end) {
        int deltaYears = end.getYear() - this.getYear();
        DateTime endCopy = (DateTime)end.clone();
        endCopy.setYear(this.getYear());
        if (endCopy.getTime() < this.getTime()) {
            --deltaYears;
        }
        return deltaYears;
    }

    public int yearsUntilDate(DateTime end) {
        int deltaYear = end.getYear() - this.getYear();
        if (deltaYear == 0) {
            return deltaYear;
        }
        DateTime incremented = new DateTime(this).addYears(deltaYear);
        if (deltaYear < 0 && incremented.before(end)) {
            ++deltaYear;
        } else if (deltaYear > 0 && incremented.after(end)) {
            --deltaYear;
        }
        return deltaYear;
    }

    public int monthsUntil(DateTime otherDay) {
        return otherDay.getMonth() - this.getMonth() + 12 * (otherDay.getYear() - this.getYear());
    }

    public String formatDate() {
        return this.format("yyyy-MM-dd");
    }

    public String formatTime() {
        return this.format("HH:mm:ss");
    }

    public String formatDateTime() {
        return this.format("yyyy-MM-dd HH:mm:ss");
    }

    public String formatDateLong() {
        return this.format("d 'de' MMMM 'de' yyyy");
    }

    public String format(String format) {
        return DateTime.getFormater(format, this.lenient, this.getTimezone()).format(this.getCalendar().getTime());
    }

    public String format(String format, TimeZone timezone) {
        return DateTime.getFormater(format, this.lenient, timezone).format(this.getCalendar().getTime());
    }

    public String format(TimeZone timezone) {
        return DateTime.getFormater(this.format, this.lenient, timezone).format(this.getCalendar().getTime());
    }

    public synchronized String format() {
        return this.getFormater().format(this.getCalendar().getTime());
    }

    public int get(int field) {
        return this.getCalendar().get(field);
    }

    public Calendar getCalendar() {
        if (this.calendar == null) {
            this.calendar = Calendar.getInstance(this.getTimezone());
            this.calendar.setLenient(this.lenient);
            this.calendar.setFirstDayOfWeek(2);
            this.calendar.setTime(this);
        }
        return this.calendar;
    }

    public String getFormat() {
        return this.format;
    }

    public static DateFormat getFormater(String format, boolean lenient) {
        return DateTime.getFormater(format, lenient, TimeZone.getDefault());
    }

    public static DateFormat getFormater(String format, boolean lenient, TimeZone timezone) {
        SimpleDateFormat formater = new SimpleDateFormat(format);
        formater.setLenient(lenient);
        formater.setTimeZone(timezone);
        return formater;
    }

    protected DateFormat getFormater() {
        if (this.formater == null) {
            this.formater = DateTime.getFormater(this.format, this.lenient, this.getTimezone());
        }
        return this.formater;
    }

    public TimeZone getTimezone() {
        if (this.timezone == null) {
            this.timezone = TimeZone.getDefault();
        }
        return this.timezone;
    }

    @Override
    public int getYear() {
        return this.get(1);
    }

    @Override
    public int getMonth() {
        return this.get(2);
    }

    public int getMonthDays() {
        return Month.getDays(this.getYear(), this.getMonth());
    }

    public String getMonthName() {
        return Month.getName(this.getMonth());
    }

    public String getMonthShortName() {
        return Month.getShortName(this.getMonth());
    }

    @Override
    public int getDay() {
        return this.get(5);
    }

    public int getWeekDay() {
        return this.get(7);
    }

    public String getWeekDayName() {
        return Week.getDayName(this.getWeekDay());
    }

    public String getWeekDayShortName() {
        return Week.getShortDayName(this.getWeekDay());
    }

    public int getYearWeek() {
        return this.getCalendar().get(3);
    }

    public int getMonthWeek() {
        return this.getCalendar().get(4);
    }

    public java.util.Date getUtilDate() {
        return this.getCalendar().getTime();
    }

    public boolean isLeapYear() {
        return Year.isLeapYear(this.getYear());
    }

    public boolean isLenient() {
        return this.lenient;
    }

    public boolean isHoliday() {
        return this.isHoliday("PT");
    }

    public boolean isHoliday(String region) {
        return Holidays.isHoliday(region, this);
    }

    public boolean isHoliday(String country, String region) {
        return this.isHoliday(country) || this.isHoliday(region);
    }

    public Holiday getHoliday() {
        return this.getHoliday("PT");
    }

    public Holiday getHoliday(String region) {
        return Holidays.getHoliday(region, this);
    }

    public Holiday getHoliday(String country, String region) {
        Holiday holiday = Holidays.getHoliday(country, this);
        if (holiday == null) {
            holiday = Holidays.getHoliday(region, this);
        }
        return holiday;
    }

    public List getHolidays() {
        return this.getHolidays("PT");
    }

    public List getHolidays(String region) {
        return Holidays.getHolidays(region, this);
    }

    public List getHolidays(String country, String region) {
        List holidays = this.getHolidays(country);
        if (holidays == null) {
            return this.getHolidays(region);
        }
        List regionHolidays = this.getHolidays(region);
        if (regionHolidays != null) {
            holidays.addAll(regionHolidays);
        }
        return holidays;
    }

    public boolean isCarnaval() {
        return MoveableHoliday.CARNAVAL.contains("PT", this);
    }

    public boolean isWorkingDay() {
        return this.isWorkingDay("PT");
    }

    public boolean isWorkingDay(String region) {
        return !NonWorkingDays.isNonWorkingDay(this) && !this.isHoliday(region);
    }

    public boolean isWorkingDay(String country, String region) {
        return this.isWorkingDay(country) && this.isWorkingDay(region);
    }

    public boolean isNonWorkingDay() {
        return this.isNonWorkingDay("PT");
    }

    public boolean isNonWorkingDay(String region) {
        return NonWorkingDays.isNonWorkingDay(this) || this.isHoliday(region);
    }

    public boolean isNonWorkingDay(String country, String region) {
        return this.isNonWorkingDay(country) || this.isNonWorkingDay(region);
    }

    public DateTime getNextWorkingDay() {
        return this.getNextWorkingDay("PT");
    }

    public DateTime getNextWorkingDay(String region) {
        DateTime wd = ((DateTime)this.clone()).addDays(1);
        while (wd.isNonWorkingDay(region)) {
            wd.addDays(1);
        }
        return wd;
    }

    public DateTime getNextWorkingDay(String country, String region) {
        DateTime wd = ((DateTime)this.clone()).addDays(1);
        while (wd.isNonWorkingDay(country) || wd.isNonWorkingDay(region)) {
            wd.addDays(1);
        }
        return wd;
    }

    public DateTime getPreviousWorkingDay(String region) {
        DateTime wd = ((DateTime)this.clone()).addDays(-1);
        while (wd.isNonWorkingDay(region)) {
            wd.addDays(-1);
        }
        return wd;
    }

    public DateTime getPreviousWorkingDay(String country, String region) {
        DateTime wd = ((DateTime)this.clone()).addDays(-1);
        while (wd.isNonWorkingDay(country) || wd.isNonWorkingDay(region)) {
            wd.addDays(-1);
        }
        return wd;
    }

    public DateTime addWorkingDays(int days) {
        return this.addWorkingDays("PT", days);
    }

    public DateTime addWorkingDays(String region, int days) {
        DateTime wd = (DateTime)this.clone();
        boolean subtract = days < 0;
        for (int i = 0; i < Math.abs(days); ++i) {
            wd = subtract ? wd.getPreviousWorkingDay(region) : wd.getNextWorkingDay(region);
        }
        return wd;
    }

    public DateTime addWorkingDays(String country, String region, int days) {
        DateTime wd = (DateTime)this.clone();
        boolean subtract = days < 0;
        for (int i = 0; i < Math.abs(days); ++i) {
            wd = subtract ? wd.getPreviousWorkingDay(country, region) : wd.getNextWorkingDay(country, region);
        }
        return wd;
    }

    public int getWorkingDaysUntil(DateTime until) {
        return this.getWorkingDaysUntil(until, "PT");
    }

    public int getWorkingDaysUntil(DateTime until, String country, String region) {
        int workingDays = 0;
        DateTime startDate = (DateTime)this.clone();
        while (startDate.beforeOrEquals(until)) {
            if (startDate.isWorkingDay(country, region)) {
                ++workingDays;
            }
            startDate = startDate.getNextWorkingDay(country, region);
        }
        return workingDays;
    }

    public int getWorkingDaysUntil(DateTime until, String region) {
        int workingDays = 0;
        DateTime startDate = (DateTime)this.clone();
        while (startDate.beforeOrEquals(until)) {
            if (startDate.isWorkingDay(region)) {
                ++workingDays;
            }
            startDate = startDate.getNextWorkingDay(region);
        }
        return workingDays;
    }

    public boolean isSameYear(DateTime other) {
        return this.getYear() == other.getYear();
    }

    public boolean isSameMonth(DateTime other) {
        return this.isSameYear(other) && this.getMonth() == other.getMonth();
    }

    public boolean isSameWeek(DateTime other) {
        return this.isSameYear(other) && this.getYearWeek() == other.getYearWeek();
    }

    public boolean isSameDay(DateTime other) {
        return this.isSameMonth(other) && this.getDay() == other.getDay();
    }

    public boolean isSameDate(DateTime other) {
        return this.isSameYear(other) && this.isSameMonth(other) && this.isSameDay(other);
    }

    @Override
    public long getTime() {
        return this.getCalendar().getTimeInMillis();
    }

    public long getMillis() {
        return this.getTime();
    }

    public void setDateTime(java.util.Date date) {
        this.getCalendar().setTime(date);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setDateTime(Timestamp date) {
        this.getCalendar().setTime(date);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setDateTime(String date) {
        try {
            this.setDateTime(this.getFormater().parse(date));
        }
        catch (ParseException e) {
            throw new InvalidDate(e.getMessage());
        }
    }

    public void setDateTime(int year) {
        this.setDateTime(year, 0);
    }

    public void setDateTime(int year, int month) {
        this.setDateTime(year, month, 1);
    }

    public void setDateTime(int year, int month, int day) {
        this.setDateTime(year, month, day, 0);
    }

    public void setDateTime(int year, int month, int day, int hour) {
        this.setDateTime(year, month, day, hour, 0);
    }

    public void setDateTime(int year, int month, int day, int hour, int minute) {
        this.setDateTime(year, month, day, hour, minute, 0);
    }

    public void setDateTime(int year, int month, int day, int hour, int minute, int second) {
        this.setDateTime(year, month, day, hour, minute, second, 0);
    }

    public void setDateTime(int year, int month, int day, int hour, int minute, int second, int millis) {
        Calendar calendar = this.getCalendar();
        calendar.set(1, year);
        calendar.set(2, month);
        calendar.set(5, day);
        this.setTime(hour, minute, second, millis);
    }

    public void setDateTime(long millis) {
        this.getCalendar().setTimeInMillis(millis);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setFormat(String format) {
        this.format = format == null ? "yyyy-MM-dd HH:mm:ss" : format;
        this.formater = null;
    }

    public void setLenient(boolean lenient) {
        if (this.calendar != null) {
            this.calendar.setLenient(lenient);
        }
        if (this.formater != null) {
            this.formater.setLenient(lenient);
        }
        this.lenient = lenient;
    }

    @Override
    public void setYear(int year) {
        this.getCalendar().set(1, year);
        super.setTime(this.calendar.getTimeInMillis());
    }

    @Override
    public void setMonth(int month) {
        this.getCalendar().set(2, month);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setDay(int day) {
        this.getCalendar().set(5, day);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setHour(int hour) {
        this.getCalendar().set(11, hour);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setMinute(int minute) {
        this.getCalendar().set(12, minute);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setSecond(int second) {
        this.getCalendar().set(13, second);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setMillis(int millis) {
        this.getCalendar().set(14, millis);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setTime(int hour, int minute, int second) {
        this.setTime(hour, minute, second, 0);
    }

    public void setTime(int hour, int minute, int second, int millis) {
        Calendar calendar = this.getCalendar();
        calendar.set(11, hour);
        calendar.set(12, minute);
        calendar.set(13, second);
        calendar.set(14, millis);
        super.setTime(calendar.getTimeInMillis());
    }

    @Override
    public void setTime(long time) {
        this.getCalendar().setTimeInMillis(time);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public void setTimeZone(TimeZone timezone) {
        this.timezone = timezone;
        this.getCalendar().setTimeZone(timezone);
        super.setTime(this.calendar.getTimeInMillis());
    }

    public boolean afterOrEquals(java.util.Date when) {
        return this.getTime() >= when.getTime();
    }

    public boolean beforeOrEquals(java.util.Date when) {
        return this.getTime() <= when.getTime();
    }

    @Override
    public String toString() {
        return this.format();
    }

    @Override
    public Object clone() {
        DateTime other = (DateTime)super.clone();
        if (this.calendar != null) {
            other.calendar = (Calendar)this.calendar.clone();
        }
        if (this.formater != null) {
            other.formater = (DateFormat)this.formater.clone();
        }
        return other;
    }

    public boolean equals(DateTime date, String format) {
        return this.format(format).equals(date.format(format));
    }

    public boolean isBetweenOrEquals(DateTime start, DateTime end) {
        return this.afterOrEquals(start) && this.beforeOrEquals(end);
    }

    public boolean isBetween(DateTime start, DateTime end) {
        return this.after(start) && this.before(end);
    }

    public Date getSqlDate() {
        return new Date(this.getTime());
    }

    public boolean isLastDayOfMonth() {
        return ((DateTime)this.clone()).addDays(1).getMonth() != this.getMonth();
    }

    static {
        EuropeLisbonTimeZone.setDefaultTimeZone();
    }
}

