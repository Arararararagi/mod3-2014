/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import pt.opensoft.util.Predicate;

public class Reflections {
    public MethodListReflections getAllDeclaredMethods(Class clazz) {
        Field[] fields = clazz.getDeclaredFields();
        Field[] fieldsSuper = clazz.getSuperclass().getDeclaredFields();
        Field[] fieldsSuperSuper = clazz.getSuperclass().getSuperclass().getDeclaredFields();
        LinkedHashSet<Method> methodSet = new LinkedHashSet<Method>();
        for (Field field22 : fields) {
            try {
                methodSet.add(clazz.getDeclaredMethod("get" + Reflections.firstToUp(field22.getName()), new Class[0]));
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
            try {
                methodSet.add(clazz.getDeclaredMethod("set" + Reflections.firstToUp(field22.getName()), new Class[0]));
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
            try {
                methodSet.add(clazz.getDeclaredMethod("is" + Reflections.firstToUp(field22.getName()), new Class[0]));
                continue;
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
        }
        for (Field field22 : fieldsSuper) {
            try {
                methodSet.add(clazz.getMethod("get" + Reflections.firstToUp(field22.getName()), new Class[0]));
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
            try {
                methodSet.add(clazz.getSuperclass().getDeclaredMethod("set" + Reflections.firstToUp(field22.getName()), new Class[0]));
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
            try {
                methodSet.add(clazz.getSuperclass().getDeclaredMethod("is" + Reflections.firstToUp(field22.getName()), new Class[0]));
                continue;
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
        }
        for (Field field22 : fieldsSuperSuper) {
            try {
                methodSet.add(clazz.getSuperclass().getSuperclass().getDeclaredMethod("get" + Reflections.firstToUp(field22.getName()), new Class[0]));
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
            try {
                methodSet.add(clazz.getSuperclass().getSuperclass().getDeclaredMethod("set" + Reflections.firstToUp(field22.getName()), new Class[0]));
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
            try {
                methodSet.add(clazz.getSuperclass().getSuperclass().getDeclaredMethod("is" + Reflections.firstToUp(field22.getName()), new Class[0]));
                continue;
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
        }
        Method[] methods = new Method[methodSet.size()];
        return new MethodListReflections(methodSet.toArray(methods));
    }

    private static final String firstToUp(String str) {
        return new String(new StringBuilder().append("").append(str.charAt(0)).toString()).toUpperCase() + str.substring(1);
    }

    public static class MethodListReflections {
        private Method[] methods;

        MethodListReflections(Method[] methods) {
            this.methods = methods;
        }

        public MethodListReflections startingWith(final String prefix) {
            return this.filter(new Predicate(){

                @Override
                public boolean evaluate(Object o) {
                    return ((Method)o).getName().startsWith(prefix);
                }
            });
        }

        public MethodListReflections withAnnotation(final Class<? extends Annotation> annotationClass) {
            return this.filter(new Predicate(){

                @Override
                public boolean evaluate(Object o) {
                    return ((Method)o).getAnnotation(annotationClass) != null;
                }
            });
        }

        public MethodListReflections onlyPublic() {
            return this.filter(new Predicate(){

                @Override
                public boolean evaluate(Object o) {
                    return Modifier.isPublic(((Method)o).getModifiers());
                }
            });
        }

        public Method[] methods() {
            return this.methods;
        }

        private MethodListReflections filter(Predicate predicate) {
            ArrayList<Method> result = new ArrayList<Method>();
            for (Method method : this.methods) {
                if (!predicate.evaluate(method)) continue;
                result.add(method);
            }
            Method[] resultArray = new Method[result.size()];
            return new MethodListReflections(result.toArray(resultArray));
        }

    }

}

