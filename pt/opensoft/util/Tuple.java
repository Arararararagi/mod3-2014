/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

public class Tuple
implements Comparable {
    protected Object[] objects;

    public Tuple(Object[] tuple) {
        this.objects = tuple;
    }

    public Object getPosition(int position) {
        return this.objects[position];
    }

    public void setPosition(int position, Object object) {
        this.objects[position] = object;
    }

    public int getDimensions() {
        return this.objects.length;
    }

    public int hashCode() {
        int result = 17;
        for (int i = 0; i < this.getDimensions(); ++i) {
            result = 37 * result + this.objects[i].hashCode();
        }
        return result;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Tuple)) {
            return false;
        }
        Tuple tuple = (Tuple)obj;
        if (this.getDimensions() == tuple.getDimensions()) {
            for (int i = 0; i < this.getDimensions(); ++i) {
                if (this.objects[i] == null && tuple.objects[i] != null) {
                    return false;
                }
                if (this.objects[i] == null && tuple.objects[i] == null || this.objects[i] == tuple.objects[i] || this.objects[i].equals(tuple.objects[i])) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("(");
        for (int i = 0; i < this.getDimensions(); ++i) {
            buffer.append(this.objects[i]);
            buffer.append(",");
        }
        buffer.setCharAt(buffer.length() - 1, ')');
        return buffer.toString();
    }

    public int compareTo(Object o) {
        return this.equals(o) ? 0 : 1;
    }
}

