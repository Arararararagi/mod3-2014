/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Calendar;
import java.util.Date;
import pt.opensoft.util.DateTime;
import pt.opensoft.util.StringUtil;

public class Week
extends DateTime {
    public static final int DOMINGO = 1;
    public static final int SEGUNDA = 2;
    public static final int TERCA = 3;
    public static final int QUARTA = 4;
    public static final int QUINTA = 5;
    public static final int SEXTA = 6;
    public static final int SABADO = 7;
    public static final String WEEK_FORMAT = "";
    protected static final String[] DAY_NAMES_PT = new String[]{"Domingo", "Segunda", "Ter\u00e7a", "Quarta", "Quinta", "Sexta", "S\u00e1bado"};

    public static String getDayName(int weekDay) {
        return DAY_NAMES_PT[weekDay - 1];
    }

    public static String getShortDayName(int weekDay) {
        return DAY_NAMES_PT[weekDay - 1].substring(0, 3);
    }

    public static int getWeekDay(String weekDayName) {
        if (weekDayName == null) {
            return -1;
        }
        weekDayName = StringUtil.normalize(weekDayName);
        int length = weekDayName.length();
        for (int i = 0; i < DAY_NAMES_PT.length; ++i) {
            int end = Math.min(length, DAY_NAMES_PT[i].length());
            String name = StringUtil.normalize(DAY_NAMES_PT[i].substring(0, end));
            if (!name.equals(weekDayName)) continue;
            return i + 1;
        }
        return -1;
    }

    public Week() {
        this.setFormat("");
    }

    public Week(int year) {
        this(year, 1);
    }

    public Week(Date date) {
        super(date);
    }

    public Week(int year, int week) {
        this();
        this.setDateTime(year, 0, 1);
        if (week < 1 || week > 53) {
            week = 1;
        }
        if (this.getYearWeek() == week) {
            return;
        }
        this.getCalendar().set(3, 1);
        this.addYearWeeks(week - 1);
    }

    public DateTime getFirstDate() {
        int week = this.getYearWeek();
        DateTime clone = (DateTime)this.clone();
        clone.setTime(0, 0, 0, 0);
        do {
            clone.addDays(1);
        } while (clone.getYearWeek() == week);
        clone.addYearWeeks(-1);
        return clone;
    }

    public DateTime getLastDate() {
        int week = this.getYearWeek();
        DateTime clone = (DateTime)this.clone();
        clone.setTime(23, 59, 59, 999);
        do {
            clone.addDays(1);
        } while (clone.getYearWeek() == week);
        clone.addDays(-1);
        return clone;
    }

    @Override
    public String toString() {
        return "" + this.getYearWeek() + " [" + this.getFirstDate().formatDate() + "..." + this.getLastDate().formatDate() + "]";
    }
}

