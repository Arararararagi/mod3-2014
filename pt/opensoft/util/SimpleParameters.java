/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.util.Properties;

public class SimpleParameters {
    protected Properties values;
    private String encoding;

    public SimpleParameters() {
        this.encoding = null;
    }

    public SimpleParameters(String encode) {
        this.encoding = encode;
    }

    public void read(String filename) {
        this.read(".", filename);
    }

    public void read(String dir, String filename) {
        this.read(new File(dir), filename);
    }

    public void read(File dir, String filename) {
        this.read(new File(dir, filename));
    }

    public void read(File file) {
        if (!file.exists()) {
            System.out.println("WARNING: PARAMETER FILE NOT FOUND: " + file);
            return;
        }
        try {
            FileInputStream in = new FileInputStream(file);
            try {
                this.read(in);
            }
            finally {
                in.close();
            }
        }
        catch (IOException e) {
            throw new Error(e.toString());
        }
    }

    public void read(InputStream in) {
        if (this.values == null) {
            this.values = new Properties();
        }
        try {
            if (this.encoding != null) {
                InputStreamReader reader = new InputStreamReader(in, this.encoding);
                this.values.load(reader);
            } else {
                this.values.load(in);
            }
        }
        catch (IOException e) {
            throw new Error(e.toString());
        }
    }

    public Object getValue(String name) {
        if (this.values == null) {
            return null;
        }
        return this.values.get(name);
    }

    public Object remove(String name) {
        return this.values.remove(name);
    }

    public Object setValue(String name, Object value) {
        if (this.values == null) {
            this.values = new Properties();
        }
        this.values.put(name, value);
        return value;
    }

    public String getString(String name) {
        return this.getString(name, null);
    }

    public String getString(String name, String defaultValue) {
        Object value = this.getValue(name);
        if (value == null) {
            return defaultValue;
        }
        if (value instanceof String) {
            return (String)value;
        }
        return value.toString();
    }

    public String toString() {
        if (this.values != null) {
            return this.values.toString();
        }
        return null;
    }
}

