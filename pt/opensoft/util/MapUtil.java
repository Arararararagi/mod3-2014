/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class MapUtil {
    protected static final String DEFAULT_KEY_SEPARATOR = ",";
    protected static final String DEFAULT_NAMEVALUE_SEPARATOR = "=";
    protected static final String DEFAULT_TEXT_QUALIFIER = "\"";

    public static Map<String, String> toMap(String map) {
        return MapUtil.toMap(map, ",", "=");
    }

    public static Map<String, String> toMap(String map, String keySeparator, String nameValueSeparator) {
        StringTokenizer st1;
        if (map == null) {
            return null;
        }
        if (keySeparator == null) {
            keySeparator = ",";
        }
        if (nameValueSeparator == null) {
            nameValueSeparator = "=";
        }
        if (!(st1 = new StringTokenizer(map, keySeparator)).hasMoreTokens()) {
            return null;
        }
        HashMap<String, String> values = new HashMap<String, String>();
        while (st1.hasMoreTokens()) {
            String pair = st1.nextToken().trim();
            StringTokenizer st2 = new StringTokenizer(pair, nameValueSeparator);
            String name = st2.nextToken().trim();
            String value = st2.hasMoreTokens() ? st2.nextToken().trim() : "";
            values.put(name, value);
        }
        return values;
    }

    public static Map<String, String> toMap(String processingStrMap, String keySeparator, String nameValueSeparator, String textQualifier) {
        int startValueIndex;
        if (processingStrMap == null) {
            return null;
        }
        if (keySeparator == null) {
            keySeparator = ",";
        }
        if (nameValueSeparator == null) {
            nameValueSeparator = "=";
        }
        if (textQualifier == null) {
            textQualifier = "\"";
        }
        if (textQualifier.trim().equals("") || !textQualifier.trim().equals(textQualifier)) {
            throw new IllegalArgumentException("Valor do textQualifier \u00e9 inv\u00e1lido: |" + textQualifier + "|");
        }
        int keySeparatorLength = keySeparator.length();
        int nameValueSeparatorLength = nameValueSeparator.length();
        int textQualifierLength = textQualifier.length();
        HashMap<String, String> map = new HashMap<String, String>();
        int startKeyIndex = 0;
        while ((startValueIndex = processingStrMap.indexOf(nameValueSeparator, startKeyIndex) + nameValueSeparatorLength) - nameValueSeparatorLength != -1) {
            String value;
            int endValueIndex;
            String key = processingStrMap.substring(startKeyIndex, startValueIndex - nameValueSeparatorLength).trim();
            boolean withTextQualifier = processingStrMap.substring(startValueIndex).trim().startsWith(textQualifier);
            if (withTextQualifier) {
                startValueIndex = processingStrMap.indexOf(textQualifier, startValueIndex) + textQualifierLength;
                endValueIndex = processingStrMap.indexOf(textQualifier, startValueIndex);
                while (endValueIndex != -1 && processingStrMap.substring(endValueIndex + textQualifierLength).startsWith(textQualifier)) {
                    endValueIndex = processingStrMap.indexOf(textQualifier, endValueIndex + textQualifierLength * 2);
                }
                if (endValueIndex == -1) {
                    value = processingStrMap.substring(startValueIndex).replaceAll(textQualifier + textQualifier, textQualifier);
                    startKeyIndex = endValueIndex;
                } else {
                    value = processingStrMap.substring(startValueIndex, endValueIndex).replaceAll(textQualifier + textQualifier, textQualifier);
                    startKeyIndex = processingStrMap.indexOf(keySeparator, endValueIndex);
                    if (startKeyIndex != -1) {
                        startKeyIndex+=keySeparatorLength;
                    }
                }
            } else {
                endValueIndex = processingStrMap.indexOf(keySeparator, startValueIndex);
                if (endValueIndex == -1) {
                    value = processingStrMap.substring(startValueIndex).trim();
                    startKeyIndex = endValueIndex;
                } else {
                    value = processingStrMap.substring(startValueIndex, endValueIndex).trim();
                    startKeyIndex = processingStrMap.indexOf(keySeparator, endValueIndex);
                    if (startKeyIndex != -1) {
                        startKeyIndex+=keySeparatorLength;
                    }
                }
            }
            map.put(key, value);
            if (startKeyIndex != -1) continue;
        }
        return map;
    }

    public static String toString(Map map) {
        return MapUtil.toString(map, ",", "=");
    }

    public static String toString(Map map, String keySeparator, String nameValueSeparator) {
        if (map == null) {
            return null;
        }
        if (keySeparator == null) {
            keySeparator = ",";
        }
        if (nameValueSeparator == null) {
            nameValueSeparator = "=";
        }
        StringBuilder buffer = new StringBuilder();
        for (Map.Entry entry : map.entrySet()) {
            if (buffer.length() != 0) {
                buffer.append(keySeparator);
            }
            buffer.append(entry.getKey()).append(nameValueSeparator).append(entry.getValue());
        }
        return buffer.toString();
    }

    public static String toString(Map<String, String> map, String keySeparator, String nameValueSeparator, String textQualifier) {
        if (map == null) {
            return null;
        }
        if (keySeparator == null) {
            keySeparator = ",";
        }
        if (nameValueSeparator == null) {
            nameValueSeparator = "=";
        }
        if (textQualifier == null) {
            textQualifier = "\"";
        }
        StringBuilder buffer = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key;
            if (buffer.length() != 0) {
                buffer.append(keySeparator);
            }
            if ((key = String.valueOf(entry.getKey())).contains((CharSequence)nameValueSeparator)) {
                throw new IllegalArgumentException("A chave (" + key + ") cont\u00e9m um caractere igual ao nameValueSeparator: " + nameValueSeparator);
            }
            String value = String.valueOf(entry.getValue());
            if (value.contains((CharSequence)keySeparator) || value.contains((CharSequence)nameValueSeparator) || value.startsWith(textQualifier)) {
                value = textQualifier + value.replaceAll(textQualifier, new StringBuilder().append(textQualifier).append(textQualifier).toString()) + textQualifier;
            }
            buffer.append(key).append(nameValueSeparator).append(value);
        }
        return buffer.toString();
    }

    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    public static Map removeAll(Map source, List keyList) {
        for (Object key : keyList) {
            source.remove(key);
        }
        return source;
    }

    private MapUtil() {
    }
}

