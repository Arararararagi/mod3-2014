/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class EuropeLisbonTimeZone
extends SimpleTimeZone {
    private static final String JAVA_EUROPE_LISBON = "Europe/Lisbon";
    private static final String JAVA_MAJOR_VERSION = System.getProperty("java.version").substring(0, 3);
    protected static final int GMT_RAW_OFFSET = 0;
    protected static final String TIME_ZONE_ID = "Europe/Lisbon";
    protected static final int DST_START_MONTH = 2;
    protected static final int DST_START_DAY = -1;
    protected static final int DST_START_DAY_OF_WEEK = 1;
    protected static final int DST_START_TIME = 3600000;
    protected static final int DST_END_MONTH = 9;
    protected static final int DST_END_DAY = -1;
    protected static final int DST_END_DAY_OF_WEEK = 1;
    protected static final int DST_END_TIME = 7200000;

    public static void setDefaultTimeZone() {
        String timezone = System.getProperty("timezone");
        TimeZone tz = null;
        if (timezone == null) {
            if (JAVA_MAJOR_VERSION.compareTo("1.3") < 0) {
                tz = new EuropeLisbonTimeZone();
            } else {
                tz = EuropeLisbonTimeZone.getTimeZone("Europe/Lisbon");
                if (tz == null) {
                    tz = new EuropeLisbonTimeZone();
                }
            }
        } else {
            tz = timezone.equalsIgnoreCase("default") ? EuropeLisbonTimeZone.getDefault() : EuropeLisbonTimeZone.getTimeZone(timezone);
        }
        EuropeLisbonTimeZone.setDefault(tz);
    }

    public EuropeLisbonTimeZone() {
        super(0, "Europe/Lisbon", 2, -1, 1, 3600000, 9, -1, 1, 7200000);
    }
}

