/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

public class VectorUtil {
    public static Vector toVector(Object[] array) {
        if (array == null) {
            return null;
        }
        Vector<Object> vector = new Vector<Object>(array.length);
        for (int i = 0; i < array.length; ++i) {
            Object obj = array[i];
            vector.addElement(obj);
        }
        return vector;
    }

    public static Vector toVector(Enumeration en) {
        if (en == null) {
            return null;
        }
        Vector vector = new Vector(10);
        while (en.hasMoreElements()) {
            Object obj = en.nextElement();
            vector.addElement(obj);
        }
        return vector;
    }

    public static Vector toVector(String Vector) {
        return VectorUtil.toVector(Vector, null);
    }

    public static Vector toVector(String Vector, String separator) {
        StringTokenizer st;
        if (Vector == null) {
            return null;
        }
        if (separator == null) {
            separator = ",";
        }
        if (!(st = new StringTokenizer(Vector, separator)).hasMoreTokens()) {
            return null;
        }
        Vector<String> values = new Vector<String>();
        while (st.hasMoreTokens()) {
            values.addElement(st.nextToken().trim());
        }
        return values;
    }

    public static String toString(Vector vector) {
        return VectorUtil.toString(vector, null);
    }

    public static String toString(Vector vector, String separator) {
        if (vector == null) {
            return null;
        }
        if (separator == null) {
            separator = ";";
        }
        StringBuffer buffer = new StringBuffer();
        Enumeration iterator = vector.elements();
        while (iterator.hasMoreElements()) {
            Object o = iterator.nextElement();
            if (buffer.length() > 0) {
                buffer.append(separator);
            }
            buffer.append(o.toString());
        }
        return buffer.toString();
    }

    public static Vector union(Vector a, Vector b) {
        if (a == null || a.size() == 0) {
            return b;
        }
        if (b == null || b.size() == 0) {
            return a;
        }
        Enumeration en = b.elements();
        while (en.hasMoreElements()) {
            Object obj = en.nextElement();
            if (a.contains(obj)) continue;
            a.addElement(obj);
        }
        return a;
    }

    private VectorUtil() {
    }
}

