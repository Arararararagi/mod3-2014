/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.PrintStream;
import pt.opensoft.util.DateTime;

public class SimpleLog {
    private static boolean enabled = false;

    public static void setEnabled(boolean enabled) {
        SimpleLog.enabled = enabled;
    }

    public static void log(String message) {
        SimpleLog.log(message, null);
    }

    public static void log(String message, Throwable t) {
        if (enabled) {
            System.out.println("[" + new DateTime().format("HH:mm:ss,SSS") + "] [" + Thread.currentThread().getName() + "] - " + message);
            if (t != null) {
                t.printStackTrace(System.out);
            }
        }
    }
}

