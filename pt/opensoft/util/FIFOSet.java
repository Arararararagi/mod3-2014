/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class FIFOSet
extends HashSet {
    private List _list;

    public FIFOSet() {
        this._list = new ArrayList();
    }

    public FIFOSet(Collection collection) {
        super(collection);
        this._list = new ArrayList(collection);
    }

    @Override
    public boolean add(Object o) {
        if (super.add(o)) {
            this._list.add(o);
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        if (super.remove(o)) {
            this._list.remove(o);
            return true;
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return this._list.iterator();
    }
}

