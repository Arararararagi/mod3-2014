/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;
import pt.opensoft.field.NamedSetter118;
import pt.opensoft.io.FileUtil;
import pt.opensoft.io.IOError;
import pt.opensoft.io.ResourceLoader;
import pt.opensoft.io.ResourceNotFoundException;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.SystemParameters;
import pt.opensoft.util.VectorUtil;
import pt.opensoft.util.WrappedRuntimeException;

public class Parameters
extends NamedSetter118 {
    protected Properties values;
    protected String root = ".";

    public Parameters(String name) {
        super(name);
    }

    public Parameters(String name, String root) {
        super(name);
        this.root = root;
    }

    protected Parameters(String name, Properties props) {
        super(name);
        this.values = props;
    }

    public static Parameters load(String filename) {
        return Parameters.load(".", filename);
    }

    @Override
    public String getString(String name) {
        return this.getString(name, null);
    }

    @Override
    public String getString(String name, String defaultValue) {
        return this.getString(name, defaultValue, true);
    }

    public String getString(String name, boolean removeEspacos) {
        return this.getString(name, null, removeEspacos);
    }

    public String getString(String name, String defaultValue, boolean removeEspacos) {
        String value = super.getString(name, defaultValue);
        if (value == null || value.equals(defaultValue)) {
            return defaultValue;
        }
        if (removeEspacos) {
            return value.trim();
        }
        return value;
    }

    public static Parameters load(String dir, String filename) {
        return Parameters.load(new File(dir), filename);
    }

    public static Parameters load(File dir, String filename) {
        return Parameters.load(new File(dir, filename));
    }

    public static Parameters load(Class clazz, String filename) {
        InputStream in = clazz.getResourceAsStream(filename);
        try {
            Parameters parameters = Parameters.load(filename, in);
            return parameters;
        }
        finally {
            try {
                in.close();
            }
            catch (IOException e) {
                throw new IOError(e);
            }
        }
    }

    public static Parameters load(File file) {
        Parameters parameters = new Parameters(file.toString());
        parameters.read(file);
        if (file.getParent() == null) {
            parameters.parse(parameters.root);
        } else {
            parameters.parse(file.getParent());
        }
        return parameters;
    }

    public static Parameters load(String name, String root, InputStream in) {
        Parameters parameters = new Parameters(name);
        parameters.read(in);
        parameters.parse(root);
        return parameters;
    }

    public static Parameters load(String name, InputStream in) {
        Parameters parameters = new Parameters(name);
        parameters.read(in);
        parameters.parse(parameters.root);
        return parameters;
    }

    protected void parse(String root) {
        this.readIncludes(root);
        this.readVariableIncludes();
        this.parseVariables();
    }

    protected void parseVariables() {
        Vector names = this.getNamesVector();
        if (names == null) {
            return;
        }
        int count = 0;
        do {
            count = 0;
            Enumeration en = names.elements();
            while (en.hasMoreElements()) {
                String name = (String)en.nextElement();
                String value = this.getString(name);
                int pos = value.lastIndexOf("${");
                if (pos == -1) continue;
                int pos2 = value.indexOf("}", pos);
                if (pos2 == -1) continue;
                ++count;
                String name2 = value.substring(pos + 2, pos2);
                String value2 = this.getString(name2);
                this.setValue(name, StringUtil.replace(value, "${" + name2 + "}", value2));
            }
        } while (count > 0);
    }

    public void read(String filename) {
        this.read(".", filename);
    }

    public void read(String dir, String filename) {
        this.read(new File(dir), filename);
    }

    public void read(File dir, String filename) {
        this.read(new File(dir, filename));
    }

    public void read(File file) {
        try {
            InputStream in = ResourceLoader.getResourceStream(file);
            try {
                this.read(in);
            }
            finally {
                in.close();
            }
        }
        catch (ResourceNotFoundException re) {
            System.err.println("WARNING: PARAMETER FILE NOT FOUND: " + file);
            return;
        }
        catch (IOException e) {
            throw new Error(e.toString());
        }
    }

    public void read(InputStream in) {
        if (this.values == null) {
            this.values = new Properties();
        }
        try {
            this.values.load(in);
        }
        catch (IOException e) {
            throw new Error(e.toString());
        }
    }

    protected void readIncludes(String root) {
        Vector files = this.getVector("include.files");
        if (files == null) {
            return;
        }
        this.setValue("include.files", "");
        Enumeration en = files.elements();
        while (en.hasMoreElements()) {
            String filename = (String)en.nextElement();
            this.read(root, filename);
            this.readIncludes(root);
        }
    }

    protected void readVariableIncludes() {
        Vector names = this.getNamesVector();
        if (names == null) {
            return;
        }
        int count = 0;
        do {
            count = 0;
            Enumeration en = names.elements();
            while (en.hasMoreElements()) {
                String name = (String)en.nextElement();
                String value = this.getString(name);
                int pos = value.lastIndexOf("$include{");
                if (pos == -1) continue;
                int pos2 = value.indexOf("}", pos);
                if (pos2 == -1) continue;
                ++count;
                String name2 = value.substring(pos + 9, pos2);
                try {
                    String value2 = FileUtil.readFile(name2);
                    this.setValue(name, StringUtil.replace(value, "$include{" + name2 + "}", value2));
                    continue;
                }
                catch (IOException e) {
                    throw new Error(e.toString());
                }
            }
        } while (count > 0);
    }

    @Override
    public Vector getNamesVector() {
        if (this.values == null) {
            return null;
        }
        return VectorUtil.toVector(this.values.keys());
    }

    @Override
    public Object getValue(String name) {
        if (this.values == null) {
            return null;
        }
        return this.values.get(name);
    }

    @Override
    public Vector getValuesVector() {
        return VectorUtil.toVector(this.values.elements());
    }

    @Override
    public Object remove(String name) {
        return this.values.remove(name);
    }

    @Override
    public Object setValue(String name, Object value) {
        if (this.values == null) {
            this.values = new Properties();
        }
        this.values.put(name, value);
        return value;
    }

    public Parameters addParameters(Parameters parameters) {
        Enumeration keys = parameters.values.keys();
        while (keys.hasMoreElements()) {
            String key = (String)keys.nextElement();
            this.setValue(key, parameters.getValue(key));
        }
        return this;
    }

    @Override
    public String toString() {
        return this.toString(" = ", SystemParameters.LINE_SEPARATOR);
    }

    @Override
    public Object getInstance(String name) {
        try {
            Object value = this.getValue(name);
            if (value == null) {
                return null;
            }
            return this.getClazz(value).newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private Class getClazz(Object value) {
        if (value instanceof Class) {
            return (Class)value;
        }
        try {
            ClassLoader classLoader = (ClassLoader)this.getValue("ClassLoader");
            if (classLoader != null) {
                return Class.forName(value.toString(), true, classLoader);
            }
            return Class.forName(value.toString());
        }
        catch (ClassNotFoundException e) {
            throw new WrappedRuntimeException(e);
        }
    }
}

