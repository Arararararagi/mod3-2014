/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

public interface Transformer<I, O> {
    public O transform(I var1);
}

