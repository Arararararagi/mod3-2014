/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.UnsupportedEncodingException;

public class ByteUtil {
    public static char toChar(byte b) {
        byte i = b;
        if (i < 0) {
            return (char)(i + 256);
        }
        return (char)i;
    }

    public static char[] toCharArray(byte[] bytes) {
        return ByteUtil.toCharArray(bytes, 0, bytes.length);
    }

    public static char[] toCharArray(byte[] bytes, int off, int len) {
        char[] ch = new char[len];
        for (int i = 0; i < len; ++i) {
            ch[i] = ByteUtil.toChar(bytes[off + i]);
        }
        return ch;
    }

    public static String toString(byte[] bytes) {
        return new String(ByteUtil.toCharArray(bytes));
    }

    public static String toString(byte[] bytes, String charset) {
        try {
            return new String(bytes, charset);
        }
        catch (UnsupportedEncodingException e) {
            return ByteUtil.toString(bytes);
        }
    }

    public static String toString(byte[] bytes, int off, int len) {
        return new String(ByteUtil.toCharArray(bytes, off, len));
    }

    private ByteUtil() {
    }
}

