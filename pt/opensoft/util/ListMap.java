/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.List;
import java.util.Map;

public interface ListMap<K, V>
extends Map<K, V> {
    public V getElement(int var1);

    public K getKey(int var1);

    public List<V> elements();

    public List<K> keys();

    public boolean addAll(ListMap<? extends K, ? extends V> var1);

    @Override
    public V replace(K var1, V var2);
}

