/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.util.HashMap;

public class CharUtil {
    public static final String WHITE_SPACES = " \r\r\t";
    public static final String QUOTES = "\"'\u00b4`";
    public static final char NEW_LINE = '\n';
    public static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String DIGITS = "0123456789";
    public static final String EVEN_DIGITS = "02468";
    public static final String ODD_DIGITS = "13579";
    protected static final char[] LATIN;
    protected static final char[] ASCII;
    protected static final char[] HTML_KEY_CHARS;
    protected static final char[] DANGEROUS_CHARS;
    protected static final char[] DANGEROUS_CHARS_XSS;
    protected static final String[] DANGEROUS_CHARS_XSS_ESCAPED;
    protected static final char[] DANGEROUS_CHARS_XML;
    protected static final String[] DANGEROUS_CHARS_XML_ESCAPED;
    protected static final String[] HTML;
    protected static final int[] CP_850_TO_ISO_8859_1;
    protected static final char[] INVISIBLE_CHARS;
    protected static final int[] ISO_8859_1_TO_CP_850;
    private static final HashMap<String, String> SPECIAL_MS_WORD_TO_ISO_8859_1;

    public static char toChar_ISO_8859_1(char ch) {
        return (char)CP_850_TO_ISO_8859_1[ch];
    }

    public static char toChar_CP_850(char ch) {
        return (char)ISO_8859_1_TO_CP_850[ch];
    }

    public static byte highByte(char ch) {
        return (byte)((ch & 65280) >> 8);
    }

    public static byte lowByte(char ch) {
        return (byte)(ch & 255);
    }

    public static boolean in(char ch, String set) {
        return set != null && set.indexOf(ch) != -1;
    }

    public static boolean in(char ch, char[] set) {
        for (int i = 0; set != null && i < set.length; ++i) {
            if (ch != set[i]) continue;
            return true;
        }
        return false;
    }

    public static boolean isAscii(char ch) {
        return ch < '\u0100';
    }

    public static boolean isAsciiStandard(char ch) {
        return ch < '';
    }

    public static boolean isAsciiExtended(char ch) {
        return ch >= '' && ch < '\u0100';
    }

    public static boolean isAsciiPrintable(char ch) {
        return ch >= ' ' && ch < '';
    }

    public static boolean isBinary(char ch) {
        return ch < ' ';
    }

    public static boolean isEven(char ch) {
        return CharUtil.in(ch, "02468");
    }

    public static boolean isHtmlKeyChar(char ch) {
        for (int i = 0; i < HTML_KEY_CHARS.length; ++i) {
            if (ch != HTML_KEY_CHARS[i]) continue;
            return true;
        }
        return false;
    }

    public static boolean isLatin(char ch) {
        for (int i = 0; i < LATIN.length; ++i) {
            if (ch != LATIN[i]) continue;
            return true;
        }
        return false;
    }

    public static boolean isLetter(char ch) {
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(CharUtil.toAscii(Character.toUpperCase(ch))) != -1;
    }

    public static boolean isLetterOrDigit(char ch) {
        return CharUtil.isLetter(ch) || CharUtil.isNumeric(ch);
    }

    public static boolean isOdd(char ch) {
        return CharUtil.in(ch, "13579");
    }

    public static boolean isNewLine(char ch) {
        return ch == '\n';
    }

    public static boolean isNumeric(char ch) {
        return ch >= '0' && ch <= '9';
    }

    public static boolean isQuote(char ch) {
        return "\"'\u00b4`".indexOf(ch) >= 0;
    }

    public static boolean isText(char ch) {
        return ch >= ' ';
    }

    public static boolean isUnicode(char ch) {
        return ch >= '\u0100';
    }

    public static boolean isCharUnicode(char ch) {
        return Character.isDefined(ch);
    }

    public static boolean isWhiteSpace(char ch) {
        return " \r\r\t".indexOf(ch) != -1;
    }

    public static boolean isInvisibleChar(char ch) {
        for (int i = 0; i < INVISIBLE_CHARS.length; ++i) {
            if (ch != INVISIBLE_CHARS[i]) continue;
            return true;
        }
        return false;
    }

    public static boolean isDangerousChar(char ch) {
        for (int i = 0; i < DANGEROUS_CHARS.length; ++i) {
            if (ch != DANGEROUS_CHARS[i]) continue;
            return true;
        }
        return false;
    }

    public static char toAscii(char latin) {
        for (int i = 0; i < LATIN.length; ++i) {
            if (LATIN[i] != latin) continue;
            return ASCII[i];
        }
        return latin;
    }

    public static byte toByte(char ch) {
        return CharUtil.lowByte(ch);
    }

    public static byte[] toByteArray(char[] chars) {
        return CharUtil.toByteArray(chars, 0, chars.length);
    }

    public static byte[] toByteArray(char[] chars, int off, int len) {
        byte[] bytes = new byte[len];
        for (int i = 0; i < len; ++i) {
            bytes[i] = CharUtil.toByte(chars[off + i]);
        }
        return bytes;
    }

    public static String toHtml(char latin) {
        return CharUtil.toHtml(latin, true);
    }

    public static String toHtml(char latin, boolean replaceHtmlKeyChars) {
        if (!replaceHtmlKeyChars && CharUtil.isHtmlKeyChar(latin)) {
            return String.valueOf(latin);
        }
        for (int i = 0; i < LATIN.length; ++i) {
            if (LATIN[i] != latin) continue;
            return HTML[i];
        }
        return String.valueOf(latin);
    }

    public static String latinChars() {
        StringBuffer latinChars = new StringBuffer();
        for (int i = 0; i < LATIN.length; ++i) {
            latinChars.append(LATIN[i]);
        }
        return latinChars.toString();
    }

    public static String sanitizeChar(char toSanitize) {
        for (int i = 0; i < DANGEROUS_CHARS_XSS.length; ++i) {
            if (DANGEROUS_CHARS_XSS[i] != toSanitize) continue;
            return DANGEROUS_CHARS_XSS_ESCAPED[i];
        }
        return String.valueOf(toSanitize);
    }

    public static String escapeXMLChar(char toEscape) {
        for (int i = 0; i < DANGEROUS_CHARS_XML.length; ++i) {
            if (DANGEROUS_CHARS_XML[i] != toEscape) continue;
            return DANGEROUS_CHARS_XML_ESCAPED[i];
        }
        return String.valueOf(toEscape);
    }

    public static String fromHtmlToLatin(String html) {
        for (int i = 0; i < HTML.length; ++i) {
            if (!HTML[i].equals(html)) continue;
            return String.valueOf(LATIN[i]);
        }
        return html;
    }

    public static String toUnicode(char ch) {
        return String.format("\\u%04x", ch);
    }

    protected static boolean isSpecialMSWord(char character) {
        String unicode = CharUtil.toUnicode(character);
        return SPECIAL_MS_WORD_TO_ISO_8859_1.containsKey(unicode);
    }

    protected static String sanitizeFromMSWord(char character) {
        if (!CharUtil.isSpecialMSWord(character)) {
            return "" + character;
        }
        String unicode = CharUtil.toUnicode(character);
        return SPECIAL_MS_WORD_TO_ISO_8859_1.get(unicode);
    }

    private CharUtil() {
    }

    static {
        int i;
        LATIN = new char[]{'\u00c3', '\u00c0', '\u00c1', '\u00c2', '\u00c4', '\u00e3', '\u00e0', '\u00e1', '\u00e2', '\u00e4', '\u00c8', '\u00c9', '\u00ca', '\u00cb', '\u00e8', '\u00e9', '\u00ea', '\u00eb', '\u00cc', '\u00cd', '\u00ce', '\u00cf', '\u00ec', '\u00ed', '\u00ee', '\u00ef', '\u00d5', '\u00d2', '\u00d3', '\u00d4', '\u00d6', '\u00f5', '\u00f2', '\u00f3', '\u00f4', '\u00f6', '\u00d9', '\u00da', '\u00db', '\u00dc', '\u00f9', '\u00fa', '\u00fb', '\u00fc', '\u00d1', '\u00f1', '\u00c7', '\u00e7', '\u00ba', '\u00aa', '<', '>', '\n', '\r'};
        ASCII = new char[]{'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'N', 'n', 'C', 'c', 'o', 'a', '<', '>', '\n', '\r'};
        HTML_KEY_CHARS = new char[]{'<', '>'};
        DANGEROUS_CHARS = new char[]{'<', '>', '&', '/', '\\', '\'', '(', ')', '?', '#', '\"', '[', ']', ';', ':'};
        DANGEROUS_CHARS_XSS = new char[]{'<', '>', '/', '\\', '\'', '\"', ';', '='};
        DANGEROUS_CHARS_XSS_ESCAPED = new String[]{"&lt;", "&gt;", "&#47;", "&#92;", "&#39;", "&#34;", "&#59;", "&#61;"};
        DANGEROUS_CHARS_XML = new char[]{'<', '>', '&', '\'', '\"'};
        DANGEROUS_CHARS_XML_ESCAPED = new String[]{"&lt;", "&gt;", "&amp;", "&apos;", "&quot;"};
        HTML = new String[]{"&Atilde;", "&Agrave;", "&Aacute;", "&Acirc;", "&A;", "&atilde;", "&agrave;", "&aacute;", "&acirc;", "&auml;", "&Egrave;", "&Eacute;", "&Ecirc;", "&E;", "&egrave;", "&eacute;", "&ecirc;", "&euml;", "&Igrave;", "&Iacute;", "&Icirc;", "&I;", "&igrave;", "&iacute;", "&icirc;", "&iuml;", "&Otilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&O;", "&otilde;", "&ograve;", "&oacute;", "&ocirc;", "&ouml;", "&Ugrave;", "&Uacute;", "&Ucirc;", "&U;", "&ugrave;", "&uacute;", "&ucirc;", "&uuml;", "&Ntilde;", "&ntilde;", "&Ccedil;", "&ccedil;", "&ordm;", "&ordf;", "&lt;", "&gt;", "<br>", ""};
        CP_850_TO_ISO_8859_1 = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 199, 252, 233, 226, 228, 224, 229, 231, 234, 235, 232, 239, 238, 236, 196, 197, 201, 230, 198, 244, 246, 242, 251, 249, 255, 214, 220, 248, 163, 216, 215, 0, 225, 237, 243, 250, 241, 209, 170, 186, 191, 174, 172, 189, 188, 161, 171, 187, 0, 0, 0, 0, 0, 193, 194, 192, 169, 0, 0, 0, 0, 162, 165, 0, 0, 0, 0, 0, 0, 0, 227, 195, 0, 0, 0, 0, 0, 0, 0, 164, 240, 208, 202, 203, 200, 0, 205, 206, 207, 0, 0, 0, 0, 166, 204, 0, 211, 223, 212, 210, 245, 213, 181, 254, 222, 218, 219, 217, 253, 221, 175, 180, 173, 177, 61, 190, 182, 167, 247, 0, 176, 168, 183, 185, 179, 178, 0, 0};
        INVISIBLE_CHARS = new char[]{'\u0000', '\u0001', '\u0002', '\u0003', '\u0004', '\u0005', '\u0006', '\u0007', '\b', '\t', '\n', '\u000b', '\f', '\r', '\u000e', '\u000f', '\u0010', '\u0011', '\u0012', '\u0013', '\u0014', '\u0015', '\u0016', '\u0017', '\u0018', '\u0019', '\u001a', '\u001b', '\u001c', '\u001d', '\u001e', '\u001f'};
        ISO_8859_1_TO_CP_850 = new int[256];
        for (i = 0; i < 256; ++i) {
            CharUtil.ISO_8859_1_TO_CP_850[i] = 0;
        }
        i = 255;
        while (i >= 0) {
            CharUtil.ISO_8859_1_TO_CP_850[CharUtil.CP_850_TO_ISO_8859_1[i]] = i--;
        }
        SPECIAL_MS_WORD_TO_ISO_8859_1 = new HashMap();
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u201A", ",");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u0192", "NLG");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u201E", "\"");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2026", "...");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2020", "**");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2021", "***");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u02C6", "^");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2030", "'o/oo");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u0160", "Sh");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2039", "<");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u0152", "OE");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2018", "\u00b4");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2019", "\u00b4");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u201C", "\"");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u201D", "\"");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2022", "-");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2013", "-");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2014", "--");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u02DC", "~");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u2122", "(TM)");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u0161", "sh");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u203A", ">");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u0153", "oe");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u0178", "Y");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u00A9", "(C)");
        SPECIAL_MS_WORD_TO_ISO_8859_1.put("\\u00AE", "(R)");
    }
}

