/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import pt.opensoft.util.DateTime;

public class Date
extends DateTime {
    public Date() {
        this(false);
    }

    public Date(boolean lenient) {
        this.setFormat("yyyy-MM-dd");
        this.setLenient(lenient);
        this.setHour(0);
        this.setMinute(0);
        this.setSecond(0);
        this.setMillis(0);
    }

    public Date(String date) {
        this();
        this.setDateTime(date);
    }

    public Date(String date, String format) {
        this();
        this.setFormat(format);
        this.setDateTime(date);
    }

    public Date(java.util.Date date) {
        this(date, false);
    }

    public Date(java.util.Date date, boolean lenient) {
        this.setFormat("yyyy-MM-dd");
        this.setLenient(lenient);
        this.setDateTime(date);
        this.setHour(0);
        this.setMinute(0);
        this.setSecond(0);
        this.setMillis(0);
    }

    public Date(int year, int month, int day) {
        super(year, month, day);
        this.setFormat("yyyy-MM-dd");
    }
}

