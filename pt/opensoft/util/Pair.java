/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

import java.io.Serializable;

public class Pair<F, S>
implements Serializable {
    private static final long serialVersionUID = -6090835946904699377L;
    protected F first = null;
    protected S second = null;

    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public F getFirst() {
        return this.first;
    }

    public void setFirst(F first) {
        this.first = first;
    }

    public S getSecond() {
        return this.second;
    }

    public void setSecond(S second) {
        this.second = second;
    }

    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = 31 * result + (this.first == null ? 0 : this.first.hashCode());
        result = 31 * result + (this.second == null ? 0 : this.second.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Pair other = (Pair)obj;
        if (this.first == null ? other.first != null : !this.first.equals(other.first)) {
            return false;
        }
        if (this.second == null ? other.second != null : !this.second.equals(other.second)) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("(");
        buffer.append(this.first);
        buffer.append(",");
        buffer.append(this.second);
        buffer.append(")");
        return buffer.toString();
    }
}

