/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.util;

public class NamedObject {
    protected String name = null;
    protected boolean caseSensitive = false;

    public NamedObject(String name) {
        this.setName(name);
    }

    public NamedObject(String name, boolean caseSensitive) {
        this.setName(name, caseSensitive);
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof NamedObject)) {
            return false;
        }
        NamedObject other = (NamedObject)object;
        if (this.caseSensitive) {
            return this.getName().equals(other);
        }
        return this.getName().equals(other.getName());
    }

    public String getName() {
        return this.name;
    }

    public int hashCode() {
        if (this.caseSensitive) {
            return this.getName().hashCode();
        }
        return this.getName().toUpperCase().hashCode();
    }

    public boolean isCaseSensitive() {
        return this.caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public void setName(String name) {
        if (!this.caseSensitive) {
            name = name.toUpperCase();
        }
        this.name = name;
    }

    public void setName(String name, boolean caseSensitive) {
        this.setCaseSensitive(caseSensitive);
        this.setName(name);
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(this.getName());
        return buffer.toString();
    }
}

