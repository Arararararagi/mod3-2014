/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang.StringEscapeUtils
 */
package pt.opensoft.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringEscapeUtils;
import pt.opensoft.math.MathUtil;
import pt.opensoft.util.CharUtil;

public class StringUtil {
    private static final String NON_ALPHANUMERIC_PATTERN = "[^A-Za-z0-9]++";
    private static final String WHITESPACE_PATTERN = "\\s++";
    protected static final boolean DEFAULT_IGNORE_CASE = false;

    public static String capitalize(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return str;
        }
        StringBuffer buffer = new StringBuffer(str.length());
        buffer.append(Character.toUpperCase(str.charAt(0)));
        for (int i = 1; i < str.length(); ++i) {
            char ch = str.charAt(i);
            char last = str.charAt(i - 1);
            if (last == ' ') {
                buffer.append(Character.toUpperCase(ch));
                continue;
            }
            buffer.append(Character.toLowerCase(ch));
        }
        return buffer.toString();
    }

    public static String capitalizeFirstCharacter(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return str;
        }
        if (str.length() == 1) {
            return String.valueOf(Character.toUpperCase(str.charAt(0)));
        }
        return "" + Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    public static String addEnclosingQuotes(String value) {
        if (value == null) {
            return null;
        }
        if (value.length() == 0) {
            return "";
        }
        StringBuffer buffer = new StringBuffer("");
        buffer.append("\"");
        int last = 0;
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (ch == '\"' && last != 92) {
                buffer.append('\\');
            }
            buffer.append(ch);
            last = ch;
        }
        buffer.append("\"");
        return buffer.toString();
    }

    public static String appendChars(String source, char ch, int size) {
        return StringUtil.padChars(source, ch, size, false);
    }

    public static int count(String src, char ch) {
        int count = 0;
        for (int i = 0; i < src.length(); ++i) {
            if (src.charAt(i) != ch) continue;
            ++count;
        }
        return count;
    }

    public static boolean checkFirstDigit(String str, char firstDigit) {
        return firstDigit == str.charAt(0);
    }

    public static boolean checkFirstDigit(String str, String digits) {
        return digits.indexOf(str.charAt(0)) != -1;
    }

    public static boolean checkCharactertAt(String str, char character, int position) {
        return character == str.charAt(position);
    }

    public static double distance(String source, String target) {
        return MathUtil.ld(source, target);
    }

    public static String ending(String str, int numChars) {
        if (str == null) {
            return null;
        }
        int len = str.length();
        if (len < numChars) {
            return str;
        }
        return str.substring(len - numChars);
    }

    public static boolean in(String src, String[] set) {
        if (src == null) {
            return false;
        }
        if (set == null) {
            return false;
        }
        for (int i = 0; i < set.length; ++i) {
            String s = set[i];
            if (!src.equalsIgnoreCase(s)) continue;
            return true;
        }
        return false;
    }

    public static boolean in(String src, String set) {
        if (src == null || src.equals("")) {
            return false;
        }
        if (set == null) {
            return false;
        }
        for (int i = 0; i < src.length(); ++i) {
            if (CharUtil.in(src.charAt(i), set)) continue;
            return false;
        }
        return true;
    }

    public static boolean isAlpha(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isLetter(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isAlphaNumeric(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isLetterOrDigit(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isAscii(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isAscii(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isAsciiStandard(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isAsciiStandard(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isAsciiExtended(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isAsciiExtended(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isAsciiPrintable(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isAsciiPrintable(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isBinary(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (!CharUtil.isBinary(ch)) continue;
            return true;
        }
        return false;
    }

    public static boolean isEmpty(String value) {
        return StringUtil.isEmpty(value, true);
    }

    public static boolean isEmpty(String value, boolean trim) {
        return value == null || trim && value.trim().length() == 0 || !trim && value.length() == 0;
    }

    public static boolean isNumeric(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isNumeric(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isSignedNumeric(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() < 1) {
            return false;
        }
        int i = 0;
        if (value.charAt(0) == '+' || value.charAt(0) == '-') {
            ++i;
        }
        while (i < value.length()) {
            char ch = value.charAt(i);
            if (!CharUtil.isNumeric(ch)) {
                return false;
            }
            ++i;
        }
        return true;
    }

    public static boolean isText(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isText(ch)) continue;
            return false;
        }
        return true;
    }

    public static boolean isLatin(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (!CharUtil.isLatin(ch)) continue;
            return true;
        }
        return false;
    }

    public static boolean isUnicode(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (!CharUtil.isUnicode(ch)) continue;
            return true;
        }
        return false;
    }

    public static boolean isStringUnicode(String value) {
        if (value == null) {
            return false;
        }
        if (value.length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            char ch = value.charAt(i);
            if (CharUtil.isCharUnicode(ch)) continue;
            return false;
        }
        return true;
    }

    public static String normalize(String source) {
        return StringUtil.normalize(source, true);
    }

    public static String normalize(String source, boolean doLowerCase) {
        if (source != null) {
            source = source.replaceAll("\\s++", " ");
            source = StringUtil.toAscii(source);
            source = source.replaceAll("[^A-Za-z0-9]++", " ");
            source = source.trim();
            if (doLowerCase) {
                source = source.toLowerCase();
            }
        }
        return source;
    }

    public static String padChars(String source, char ch, int size) {
        return StringUtil.padChars(source, ch, size, true);
    }

    public static String padChars(String source, char ch, int size, boolean right) {
        if (source != null && source.length() >= size) {
            return source;
        }
        int len = source != null ? size - source.length() : size;
        StringBuffer buffer = new StringBuffer(len);
        if (!(right || source == null)) {
            buffer.append(source);
        }
        for (int i = 0; i < len; ++i) {
            buffer.append(ch);
        }
        if (right && source != null) {
            buffer.append(source);
        }
        return buffer.toString();
    }

    public static String padSpaces(String source, int size) {
        return StringUtil.padSpaces(source, size, false);
    }

    public static String padSpaces(String source, int size, boolean right) {
        return StringUtil.padChars(source, ' ', size, right);
    }

    public static String padZeros(int source, int size) {
        return StringUtil.padZeros(String.valueOf(source), size);
    }

    public static String padZeros(long source, int size) {
        return StringUtil.padZeros(String.valueOf(source), size);
    }

    public static String padZeros(String source, int size) {
        return StringUtil.padChars(source, '0', size, true);
    }

    public static String prependChars(String source, char ch, int size) {
        return StringUtil.padChars(source, ch, size, true);
    }

    public static String remove(String source, String search) {
        return StringUtil.replace(source, search, null);
    }

    public static String removeIgnoreCase(String source, String search) {
        return StringUtil.replaceIgnoreCase(source, search, null);
    }

    public static String removeLineBreaks(String source) {
        if (source == null) {
            return null;
        }
        source = StringUtil.remove(source, "\n");
        return StringUtil.remove(source, "\r");
    }

    public static String removeEnclosingQuotes(String value) {
        boolean quoted;
        if (value == null) {
            return null;
        }
        if (value.length() == 0) {
            return value;
        }
        char quote = value.charAt(0);
        char last = value.charAt(value.length() - 1);
        boolean bl = quoted = quote == '\"' || quote == '\'';
        if (!quoted) {
            return value;
        }
        if (last != quote) {
            throw new IllegalArgumentException("invalid quoted string value: " + value);
        }
        return value.substring(1, value.length() - 1);
    }

    public static String removeInvisibleChars(String source) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        for (int i = 0; i < source.length(); ++i) {
            char ch = source.charAt(i);
            if (CharUtil.isInvisibleChar(ch)) continue;
            buffer.append(ch);
        }
        return buffer.toString();
    }

    public static String removeDangerousChars(String source) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        for (int i = 0; i < source.length(); ++i) {
            char ch = source.charAt(i);
            if (CharUtil.isDangerousChar(ch)) continue;
            buffer.append(ch);
        }
        return buffer.toString();
    }

    public static String removeChars(String source, String charsToRemove) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        for (int i = 0; i < source.length(); ++i) {
            char ch = source.charAt(i);
            boolean remove = false;
            for (int c = 0; c < charsToRemove.length(); ++c) {
                if (ch != charsToRemove.charAt(c)) continue;
                remove = true;
                break;
            }
            if (remove) continue;
            buffer.append(ch);
        }
        return buffer.toString();
    }

    public static String sanitize(String input) {
        if (input == null) {
            return null;
        }
        if (input.length() == 0) {
            return input;
        }
        String semiSanitized = StringEscapeUtils.escapeHtml((String)input);
        String fullySanitized = semiSanitized.replaceAll("'", "&#x27;").replaceAll("/", "&#x2f;");
        return fullySanitized;
    }

    public static String escapeXMLChar(String strToEscape) {
        StringBuilder result = new StringBuilder();
        for (char c : strToEscape.toCharArray()) {
            result.append(CharUtil.escapeXMLChar(c));
        }
        return result.toString();
    }

    public static String replaceIgnoreCase(String source, String search, String replace) {
        return StringUtil.replace(source, search, replace, true);
    }

    public static String replace(String source, String search, String replace) {
        return StringUtil.replace(source, search, replace, false);
    }

    private static String replace(String source, String search, String replace, boolean ignoreCase) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        if (search == null) {
            return source;
        }
        if (search.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        StringUtil.replace(source, search, replace, buffer, ignoreCase);
        return buffer.toString();
    }

    public static void replace(String source, String search, String replace, StringBuffer buffer) {
        StringUtil.replace(source, search, replace, buffer, false);
    }

    private static void replace(String source, String search, String replace, StringBuffer buffer, boolean ignoreCase) {
        int pos;
        if (source == null) {
            return;
        }
        if (source.length() == 0) {
            return;
        }
        if (search == null || search.length() == 0) {
            buffer.append(source);
            return;
        }
        int last = 0;
        int n = pos = ignoreCase ? source.toLowerCase().indexOf(search.toLowerCase()) : source.indexOf(search);
        while (pos != -1) {
            buffer.append(source.substring(last, pos));
            if (replace != null) {
                buffer.append(replace);
            }
            last = pos + search.length();
            pos = ignoreCase ? source.toLowerCase().indexOf(search.toLowerCase(), last) : source.indexOf(search, last);
        }
        if (last < source.length()) {
            buffer.append(source.substring(last));
        }
    }

    public static String replaceFirst(String sOriginal, String change, String with) {
        int n = 0;
        n = sOriginal.indexOf(change);
        if (n == -1) {
            return sOriginal;
        }
        StringBuffer sb = new StringBuffer();
        sb.append(sOriginal.substring(0, n));
        sb.append(with);
        sb.append(sOriginal.substring(n + change.length()));
        return sb.toString();
    }

    public static double similarity(String source, String target) {
        source = StringUtil.normalize(source);
        target = StringUtil.normalize(target);
        int max = Math.max(source.length(), target.length());
        return 1.0 - StringUtil.distance(source, target) / (double)max;
    }

    public static String toAscii(String latin) {
        if (latin == null) {
            return null;
        }
        if (latin.length() == 0) {
            return latin;
        }
        StringBuilder buffer = new StringBuilder(latin.length());
        for (int i = 0; i < latin.length(); ++i) {
            char ch = latin.charAt(i);
            char html = CharUtil.toAscii(ch);
            buffer.append(html);
        }
        return buffer.toString();
    }

    public static String sanitizeFromMSWord(String text) {
        if (text == null) {
            return null;
        }
        if (text.length() == 0) {
            return text;
        }
        StringBuffer buffer = new StringBuffer(text.length());
        for (int i = 0; i < text.length(); ++i) {
            char character = text.charAt(i);
            String sanitized = CharUtil.sanitizeFromMSWord(character);
            buffer.append(sanitized);
        }
        return buffer.toString();
    }

    public static String toHtml(String latin) {
        return StringUtil.toHtml(latin, true);
    }

    public static String toHtml(String latin, boolean replaceHtmlKeyChars) {
        if (latin == null) {
            return null;
        }
        if (latin.length() == 0) {
            return latin;
        }
        StringBuffer buffer = new StringBuffer(latin.length());
        for (int i = 0; i < latin.length(); ++i) {
            char ch = latin.charAt(i);
            String html = CharUtil.toHtml(ch, replaceHtmlKeyChars);
            buffer.append(html);
        }
        return buffer.toString();
    }

    public static String toString(Throwable t) {
        try {
            StringWriter sw = new StringWriter();
            try {
                PrintWriter pw = new PrintWriter(sw);
                try {
                    t.printStackTrace(pw);
                }
                finally {
                    pw.close();
                }
            }
            finally {
                sw.close();
            }
            return sw.toString();
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static String trimLeft(String source) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        boolean found = false;
        for (int i = 0; i < source.length(); ++i) {
            char ch = source.charAt(i);
            if (!found) {
                boolean bl = found = !CharUtil.isWhiteSpace(ch);
            }
            if (!found) continue;
            buffer.append(ch);
        }
        return buffer.toString();
    }

    public static String trimMiddle(String source) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        for (int i = 0; i < source.length(); ++i) {
            char ch = source.charAt(i);
            if (CharUtil.isWhiteSpace(ch)) {
                char last;
                char c = last = i == 0 ? '\u0000' : source.charAt(i - 1);
                if (last == ' ') continue;
            }
            buffer.append(ch);
        }
        return buffer.toString();
    }

    public static String trimRight(String source) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        boolean found = false;
        for (int i = source.length() - 1; i >= 0; --i) {
            char ch = source.charAt(i);
            boolean white = CharUtil.isWhiteSpace(ch);
            if (!found) {
                boolean bl = found = !white;
            }
            if (!found) continue;
            buffer.append(ch);
        }
        buffer = buffer.reverse();
        return buffer.toString();
    }

    public static String trimRight(String source, char chTrim) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        boolean found = false;
        for (int i = source.length() - 1; i >= 0; --i) {
            boolean white;
            char ch = source.charAt(i);
            boolean bl = white = ch == chTrim;
            if (!found) {
                boolean bl2 = found = !white;
            }
            if (!found) continue;
            buffer.append(ch);
        }
        buffer = buffer.reverse();
        return buffer.toString();
    }

    public static String trimLeft(String source, char chTrim) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length());
        boolean found = false;
        for (int i = 0; i < source.length(); ++i) {
            char ch = source.charAt(i);
            if (!found) {
                boolean bl = found = ch != chTrim;
            }
            if (!found) continue;
            buffer.append(ch);
        }
        return buffer.toString();
    }

    public static String trim(String source) {
        if (source == null) {
            return null;
        }
        if (source.length() == 0) {
            return source;
        }
        return StringUtil.trimMiddle(StringUtil.trimLeft(StringUtil.trimRight(source)));
    }

    public static byte[] toByteArray(String str) {
        return CharUtil.toByteArray(str.toCharArray());
    }

    public static byte[] toByteArray(String str, int off, int len) {
        return CharUtil.toByteArray(str.toCharArray(), off, len);
    }

    public static String toString_ISO_8859_1(String string) {
        if (string == null) {
            return null;
        }
        StringBuffer buffer = new StringBuffer(string.length());
        for (int i = 0; i < string.length(); ++i) {
            char cp_850 = string.charAt(i);
            char iso_8859_1 = CharUtil.toChar_ISO_8859_1(cp_850);
            buffer.append(iso_8859_1);
        }
        return buffer.toString();
    }

    public static String toString_CP_850(String string) {
        if (string == null) {
            return null;
        }
        StringBuffer buffer = new StringBuffer(string.length());
        for (int i = 0; i < string.length(); ++i) {
            char iso_8859_1 = string.charAt(i);
            char cp_850 = CharUtil.toChar_CP_850(iso_8859_1);
            buffer.append(cp_850);
        }
        return buffer.toString();
    }

    public static String breakMessage(String msg, int size, String separator) {
        return StringUtil.breakMessage(msg, size, " ", separator);
    }

    public static String breakMessage(String msg, int size, String breakStr, String separator) {
        if (msg == null) {
            return null;
        }
        StringBuffer message = new StringBuffer();
        if (msg.length() < size) {
            return msg;
        }
        while (msg.length() > size) {
            String spaceBreaker = msg.substring(0, size);
            int lastSpaceIndex = spaceBreaker.lastIndexOf(breakStr);
            message.append(msg.substring(0, lastSpaceIndex != -1 ? lastSpaceIndex : size));
            message.append(separator);
            msg = msg.substring(lastSpaceIndex != -1 ? lastSpaceIndex + breakStr.length() : size);
        }
        message.append(msg);
        return message.toString();
    }

    public static String breakMessageWithExistingLineBreaks(String originalText, int limit, String separator) {
        String result = originalText;
        int counter = 0;
        for (int i = 0; i < result.length(); ++i) {
            counter = result.charAt(i) == '\n' || i < result.length() - 1 && result.charAt(i) == '\\' && result.charAt(i + 1) == 'n' ? 0 : ++counter;
            if (counter != limit || i >= result.length() - 1) continue;
            int j = i;
            for (j = i; j > 0 && result.charAt(j) != ' ' && result.charAt(j) != ',' && result.charAt(j) != ';' && result.charAt(j) != ':' && result.charAt(j) != '.'; --j) {
                if (result.charAt(j) == '-') break;
            }
            result = result.substring(0, j + 1) + separator + result.substring(j + 1, result.length());
            counter = 0;
            i = j + 1;
        }
        return result;
    }

    public static int length(List substrings, String separator) {
        StringBuffer result = new StringBuffer();
        boolean isFirst = true;
        Iterator iter = substrings.iterator();
        while (iter.hasNext()) {
            if (!isFirst) {
                result.append(separator);
            }
            isFirst = false;
            result.append((String)iter.next());
        }
        return result.length();
    }

    public static String repeat(String text, int times) {
        if (text == null || times == 0) {
            return "";
        }
        StringBuffer buf = new StringBuffer(text.length() * times);
        for (int i = 0; i < times; ++i) {
            buf.append(text);
        }
        return buf.toString();
    }

    static int hexDigit2Nibble(char ch) {
        int i = Character.digit(ch, 16);
        if (i == -1) {
            throw new IllegalArgumentException("Invalid hex digit: " + ch);
        }
        return i;
    }

    public static byte[] hexString2Bytes(String str) {
        byte[] bytes;
        int length = str.length();
        char[] chars = new char[length];
        str.getChars(0, length, chars, 0);
        int j = 0;
        int k = 0;
        if (length == 0) {
            return new byte[0];
        }
        if (length % 2 > 0) {
            bytes = new byte[(length + 1) / 2];
            bytes[j++] = (byte)StringUtil.hexDigit2Nibble(chars[k++]);
        } else {
            bytes = new byte[length / 2];
        }
        while (j < bytes.length) {
            bytes[j] = (byte)(StringUtil.hexDigit2Nibble(chars[k++]) << 4 | StringUtil.hexDigit2Nibble(chars[k++]));
            ++j;
        }
        return bytes;
    }

    public static String bytes2HexString(byte[] buf) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        for (int i = 0; i < buf.length; ++i) {
            if ((buf[i] & 255) < 16) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString(buf[i] & 255, 16));
        }
        return strbuf.toString();
    }

    public static String insertSeparator(String text, String separator, int size) {
        if (StringUtil.isEmpty(separator)) {
            throw new IllegalArgumentException("Separator cannot be null!");
        }
        if (size < 1) {
            throw new IllegalArgumentException("The number of chars has to be a number greater than 0");
        }
        if (StringUtil.isEmpty(text) || text.length() < size) {
            return text;
        }
        StringBuffer separatedMessage = new StringBuffer(text.length());
        while (text.length() > size) {
            String spaceBreaker = text.substring(0, size);
            int lastSpaceIndex = spaceBreaker.lastIndexOf(" ");
            separatedMessage.append(text.substring(0, lastSpaceIndex != -1 && lastSpaceIndex != 0 ? lastSpaceIndex : size));
            separatedMessage.append(separator);
            text = text.substring(lastSpaceIndex != -1 && lastSpaceIndex != 0 ? lastSpaceIndex : size);
        }
        separatedMessage.append(text);
        return separatedMessage.toString();
    }

    public static String substring(String text, int beginIndex, int endIndex) {
        if (endIndex > text.length()) {
            return text.substring(beginIndex, text.length());
        }
        return text.substring(beginIndex, endIndex);
    }

    public static boolean hasQuotes(String str) {
        if (str == null) {
            return false;
        }
        if (str.length() == 0) {
            return false;
        }
        for (int i = 0; i < str.length(); ++i) {
            char c = str.charAt(i);
            if (!CharUtil.isQuote(c)) continue;
            return true;
        }
        return false;
    }

    public static String escapeQuoteChars(String string) {
        String escapedString = string;
        for (int i = 0; i < "\"'\u00b4`".length(); ++i) {
            char quote = "\"'\u00b4`".charAt(i);
            escapedString = escapedString.replaceAll(String.valueOf(quote), "\\\\" + quote);
        }
        return escapedString;
    }

    public static String removeLastCharacters(String original, int sizeToRemove) {
        return original.substring(0, original.length() - sizeToRemove);
    }

    public static String fromHtmlToLatin(String html) {
        if (html == null) {
            return null;
        }
        if (html.length() == 0) {
            return html;
        }
        Pattern pattern = Pattern.compile("(&[A-Za-z]+;|<br>)");
        Matcher matcher = pattern.matcher((CharSequence)html);
        String latin = html;
        while (matcher.find()) {
            String htmlSpecialChar = matcher.group();
            String latinChar = CharUtil.fromHtmlToLatin(htmlSpecialChar);
            latin = latin.replaceAll(htmlSpecialChar, latinChar);
        }
        return latin;
    }

    private StringUtil() {
    }
}

