/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

public class HttpResponse {
    private int status;
    private String body;
    private InputStream stream;
    private Map headers;
    private List cookies;

    public HttpResponse(int status, String body, List cookies, Map headers) {
        this.status = status;
        this.body = body;
        this.stream = null;
        this.cookies = cookies;
        this.headers = headers;
    }

    public HttpResponse(int status, InputStream stream, List cookies, Map headers) {
        this.status = status;
        this.body = null;
        this.stream = stream;
        this.cookies = cookies;
        this.headers = headers;
    }

    public int getStatus() {
        return this.status;
    }

    public String getBody() {
        if (this.body == null) {
            this.body = this.convertStreamToString(this.stream);
        }
        return this.body;
    }

    public List getCookies() {
        return this.cookies;
    }

    public String getHeader(String name) {
        return (String)this.headers.get(name);
    }

    public Map getHeaders() {
        return this.headers;
    }

    public boolean isStreamable() {
        return this.stream != null;
    }

    public InputStream getInputStream() {
        return this.stream;
    }

    public String toString() {
        if (!this.isStreamable()) {
            return this.body;
        }
        return "(stream)";
    }

    public String convertStreamToString(InputStream is) {
        StringBuffer sb;
        String contentType = this.getHeader("Content-Type");
        String charset = null;
        if (contentType != null && contentType.contains((CharSequence)"charset=")) {
            charset = contentType.substring(contentType.indexOf("charset=") + "charset=".length());
        }
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(charset != null ? new InputStreamReader(is, charset) : new InputStreamReader(is));
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            reader = new BufferedReader(new InputStreamReader(is));
        }
        sb = new StringBuffer();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}

