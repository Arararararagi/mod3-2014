/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.http;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import pt.opensoft.http.HttpResponse;

public abstract class AbstractHttpConnection {
    public static final int POST = 1;
    public static final int GET = 2;
    protected URL _url;
    protected int _method = 1;
    protected List _parameters;
    protected int _timeout = 0;
    protected boolean _http11 = false;
    protected String _proxyName;
    protected int _proxyPort;
    protected String _proxyUser;
    protected String _proxyPass;

    public AbstractHttpConnection(URL url) {
        this._url = url;
        this._parameters = new ArrayList();
    }

    public void setMethod(int method) {
        this._method = method;
    }

    public void setHttp11(boolean http11) {
        this._http11 = http11;
    }

    public void setProxy(String proxyName, int proxyPort) {
        this._proxyName = proxyName;
        this._proxyPort = proxyPort;
    }

    public void setProxy(String proxyName, int proxyPort, String proxyUser, String proxyPass) {
        this._proxyName = proxyName;
        this._proxyPort = proxyPort;
        this._proxyUser = proxyUser;
        this._proxyPass = proxyPass;
    }

    public void setTimeout(int millis) {
        this._timeout = millis;
    }

    public abstract HttpResponse send() throws IOException;

    protected static String getLocalHostName() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        if (this._method == 1) {
            buffer.append("POST ");
        } else {
            buffer.append("GET ");
        }
        buffer.append(this._url);
        return buffer.toString();
    }
}

