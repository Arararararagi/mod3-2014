/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.http;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import pt.opensoft.http.AbstractHttpConnection;
import pt.opensoft.http.ConnectionTimeoutException;
import pt.opensoft.http.HttpResponse;
import pt.opensoft.text.Base64;
import pt.opensoft.util.ByteUtil;
import pt.opensoft.util.ListHashMap;
import pt.opensoft.util.Pair;
import pt.opensoft.util.StringUtil;
import pt.opensoft.util.SystemParameters;

public class JavaHttpConnection
extends AbstractHttpConnection {
    public static final int COM_STATUS_IDDLE = 0;
    public static final int COM_STATUS_SEND = 1;
    public static final int COM_STATUS_RECEIVE = 2;
    public static final int COM_STATUS_FINISH = 3;
    protected String _user;
    protected String _password;
    private HostnameVerifier hostnameVerifier = null;
    private JavaHttpConnectionThread communicationThread = null;
    protected boolean isStreaming;
    protected Map requestAditionalHeaders;
    protected byte[] requestBody = null;

    public JavaHttpConnection(URL url, boolean isStreaming) {
        super(url);
        this.isStreaming = isStreaming;
        this.requestAditionalHeaders = new ListHashMap();
        this.hostnameVerifier = null;
    }

    public JavaHttpConnection(URL url) {
        this(url, false);
    }

    public JavaHttpConnection(URL url, int method, boolean isStreaming) {
        this(url, isStreaming);
        if (2 == method) {
            throw new IllegalAccessError("Method GET not yet implemented! Use POST");
        }
        if (method != 1 && method != 2) {
            throw new IllegalArgumentException("Method not supported: (" + method + ")");
        }
        this.setMethod(method);
    }

    public JavaHttpConnection(URL url, int method) {
        this(url, method, false);
    }

    public void addParameter(String name, String value) {
        this._parameters.add(new Pair<String, String>(name, value));
    }

    public void addParameters(Map parameters) {
        if (parameters == null) {
            return;
        }
        for (Object key : parameters.keySet()) {
            this._parameters.add(new Pair(key, parameters.get(key)));
        }
    }

    public void addRequestHeaders(Map requestHeaders) {
        if (requestHeaders == null) {
            return;
        }
        for (Object key : requestHeaders.keySet()) {
            this.requestAditionalHeaders.put(key, requestHeaders.get(key));
        }
    }

    public void setRequestBody(byte[] body) {
        this.requestBody = body;
    }

    public void setBasicAuthentication(String notEncodedUser, String notEncodedPassword) {
        this._user = notEncodedUser;
        this._password = notEncodedPassword;
    }

    public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
        this.hostnameVerifier = hostnameVerifier;
    }

    protected JavaHttpConnectionThread getConnectionThread() {
        return new JavaHttpConnectionThread();
    }

    @Override
    public HttpResponse send() throws ConnectionTimeoutException, InterruptedIOException, IOException {
        try {
            long start = System.currentTimeMillis();
            long timeout = this._timeout;
            if (this.communicationThread != null) {
                throw new NullPointerException("Connection not closed yet!");
            }
            this.communicationThread = this.getConnectionThread();
            this.communicationThread.start();
            if (timeout > 0) {
                this.communicationThread.join(timeout);
                if ((timeout-=System.currentTimeMillis() - start) <= 0) {
                    this.communicationThread.interrupt();
                    throw new ConnectionTimeoutException("Timeout occured. Timeout Default: " + this._timeout + "ms, Exceeded time: " + (timeout < 0 ? - timeout : timeout) + "ms.");
                }
            } else {
                this.communicationThread.join();
            }
            if (this.communicationThread.getThreadException() != null) {
                throw new IOException("Exception occured in send: " + this.communicationThread.getThreadException().toString());
            }
            if (this.communicationThread.getStatus() != 3) {
                throw new InterruptedIOException("Communication Not Finished");
            }
            HttpResponse httpResponse = this.communicationThread.getHttpResponse();
            return httpResponse;
        }
        catch (InterruptedException e) {
            throw new IllegalStateException(e.toString());
        }
        finally {
            if (!this.isStreaming) {
                this.communicationThread = null;
            }
        }
    }

    public void close() {
        if (this.communicationThread != null) {
            this.communicationThread.disconnect();
            this.communicationThread = null;
        }
    }

    public int getCommunicationThreadStatus() {
        if (this.communicationThread == null) {
            return 0;
        }
        return this.communicationThread.getStatus();
    }

    private void setSystemProxy() {
        if (!(this._proxyName == null || this._proxyName.equals("") || this._proxyPort == 0)) {
            String proxyPort = String.valueOf(this._proxyPort);
            System.setProperty("http.proxyHost", this._proxyName);
            System.setProperty("http.proxyPort", proxyPort);
            System.setProperty("http.proxySet", "true");
            System.setProperty("https.proxyHost", this._proxyName);
            System.setProperty("https.proxyPort", proxyPort);
            System.setProperty("https.proxySet", "true");
        } else {
            this.unsetProxy();
        }
    }

    private void unsetProxy() {
        Properties systemProperties = System.getProperties();
        systemProperties.remove("http.proxyHost");
        systemProperties.remove("http.proxyPort");
        systemProperties.remove("http.proxySet");
        systemProperties.remove("https.proxyHost");
        systemProperties.remove("https.proxyPort");
        systemProperties.remove("https.proxySet");
    }

    protected void sendHttpPost(URLConnection connection) throws IOException {
        String outputString = this.constructOutputString();
        OutputStream outputStream = connection.getOutputStream();
        try {
            outputStream.write(StringUtil.toByteArray(outputString));
        }
        finally {
            outputStream.close();
        }
    }

    protected String receiveHttpPost(HttpURLConnection connection) throws IOException {
        StringBuffer stringBuffer;
        stringBuffer = new StringBuffer(SystemParameters.FILE_BLOCK_SIZE);
        byte[] buffer = new byte[SystemParameters.FILE_BLOCK_SIZE];
        int read = -1;
        InputStream in = connection.getInputStream();
        try {
            while ((read = in.read(buffer, 0, buffer.length)) != -1) {
                stringBuffer.append(new String(buffer, 0, read, this.getCharsetFromContentType(connection)));
            }
        }
        finally {
            in.close();
        }
        return stringBuffer.toString();
    }

    protected String constructOutputString() {
        StringWriter body = new StringWriter();
        for (Pair element : this._parameters) {
            if (!body.toString().equals("")) {
                body.write("&");
            }
            body.write(element.getFirst() + "=" + element.getSecond());
        }
        return body.toString();
    }

    private void setBasicAuthentication(HttpURLConnection connection) {
        if (!(StringUtil.isEmpty(this._user) || StringUtil.isEmpty(this._password))) {
            String encoded = Base64.encode(this._user + ":" + this._password);
            connection.setRequestProperty("Authorization", "Basic " + encoded);
        }
    }

    private String getCharsetFromContentType(HttpURLConnection connection) {
        String contentType = connection.getContentType();
        String charset = "UTF-8";
        int idx = contentType.indexOf("charset");
        if (idx >= 0) {
            charset = contentType.substring(idx + "content=".length());
        }
        return charset;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer(super.toString());
        boolean first = true;
        for (Pair parameter : this._parameters) {
            if (first) {
                buffer.append("?");
                first = false;
            } else {
                buffer.append("&");
            }
            buffer.append(parameter.getFirst()).append("=").append(parameter.getSecond());
        }
        return buffer.toString();
    }

    protected class JavaHttpConnectionThread
    extends Thread
    implements Runnable {
        protected int status;
        protected HttpResponse httpResponse;
        protected Exception threadException;
        protected HttpURLConnection myConnection;

        public JavaHttpConnectionThread() {
            this.httpResponse = null;
            this.threadException = null;
            this.myConnection = null;
            this.status = 0;
        }

        protected void finalize() throws Throwable {
            this.disconnect();
            super.finalize();
        }

        @Override
        public void run() {
            try {
                JavaHttpConnection.this.setSystemProxy();
                switch (JavaHttpConnection.this._method) {
                    case 1: {
                        this.setHttpResponse(this.doHttpPost(JavaHttpConnection.this._url));
                        break;
                    }
                    case 2: {
                        this.setHttpResponse(this.sendHttpGET(JavaHttpConnection.this._url));
                    }
                }
                this.setStatus(3);
            }
            catch (Exception ex) {
                this.setThreadException(ex);
            }
        }

        public HttpResponse getHttpResponse() {
            return this.httpResponse;
        }

        public Exception getThreadException() {
            return this.threadException;
        }

        public int getStatus() {
            return this.status;
        }

        private void setHttpResponse(HttpResponse httpResponse) {
            this.httpResponse = httpResponse;
        }

        private void setThreadException(Exception threadException) {
            this.threadException = threadException;
        }

        protected void setStatus(int status) {
            this.status = status;
        }

        protected HttpURLConnection createHttpConnection(URL url) throws IOException {
            return (HttpURLConnection)url.openConnection();
        }

        protected HttpResponse doHttpPost(URL url) throws Exception {
            this.myConnection = this.createHttpConnection(url);
            try {
                Object bos;
                Object propertyValue;
                JavaHttpConnection.this.setBasicAuthentication(this.myConnection);
                this.setProxyAuthentication(this.myConnection);
                this.myConnection.setDoInput(true);
                this.myConnection.setDoOutput(true);
                if (JavaHttpConnection.this.hostnameVerifier != null && this.myConnection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection)this.myConnection).setHostnameVerifier(JavaHttpConnection.this.hostnameVerifier);
                }
                this.setStatus(1);
                Set headerKeys = JavaHttpConnection.this.requestAditionalHeaders.keySet();
                for (String propertyName : headerKeys) {
                    propertyValue = (String)JavaHttpConnection.this.requestAditionalHeaders.get(propertyName);
                    this.myConnection.setRequestProperty(propertyName, (String)propertyValue);
                }
                if (JavaHttpConnection.this.requestBody != null) {
                    bos = new BufferedOutputStream(this.myConnection.getOutputStream());
                    bos.write(JavaHttpConnection.this.requestBody);
                    bos.close();
                }
                JavaHttpConnection.this.sendHttpPost(this.myConnection);
                this.setStatus(2);
                if (JavaHttpConnection.this.isStreaming) {
                    bos = new HttpResponse(this.myConnection.getResponseCode(), this.myConnection.getInputStream(), null, null);
                    return bos;
                }
                String response = JavaHttpConnection.this.receiveHttpPost(this.myConnection);
                propertyValue = new HttpResponse(this.myConnection.getResponseCode(), response, null, null);
                return propertyValue;
            }
            finally {
                if (!JavaHttpConnection.this.isStreaming) {
                    this.disconnect();
                }
            }
        }

        private void setProxyAuthentication(HttpURLConnection connection) {
            if (!(StringUtil.isEmpty(JavaHttpConnection.this._proxyUser) || StringUtil.isEmpty(JavaHttpConnection.this._proxyPass))) {
                String encoded = Base64.encode(JavaHttpConnection.this._proxyUser + ":" + JavaHttpConnection.this._proxyPass);
                connection.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
            }
        }

        private HttpResponse sendHttpGET(URL url) throws IOException {
            ByteArrayOutputStream bOut;
            String urlGet = url + "?";
            urlGet = urlGet + JavaHttpConnection.this.constructOutputString();
            bOut = null;
            this.myConnection = this.createHttpConnection(url);
            try {
                JavaHttpConnection.this.setBasicAuthentication(this.myConnection);
                this.setProxyAuthentication(this.myConnection);
                this.myConnection.setDoInput(true);
                if (JavaHttpConnection.this.hostnameVerifier != null && this.myConnection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection)this.myConnection).setHostnameVerifier(JavaHttpConnection.this.hostnameVerifier);
                }
                bOut = new ByteArrayOutputStream(SystemParameters.FILE_BLOCK_SIZE);
                try {
                    InputStream in = this.myConnection.getInputStream();
                    try {
                        byte[] buffer = new byte[SystemParameters.FILE_BLOCK_SIZE];
                        int read = -1;
                        while ((read = in.read(buffer, 0, buffer.length)) != -1) {
                            bOut.write(buffer, 0, read);
                        }
                    }
                    finally {
                        in.close();
                    }
                }
                finally {
                    bOut.close();
                }
            }
            finally {
                this.myConnection.disconnect();
            }
            String charset = JavaHttpConnection.this.getCharsetFromContentType(this.myConnection);
            return new HttpResponse(this.myConnection.getResponseCode(), ByteUtil.toString(bOut.toByteArray(), charset), null, null);
        }

        public void disconnect() {
            if (this.myConnection != null) {
                this.myConnection.disconnect();
                this.myConnection = null;
            }
        }
    }

}

