/*
 * Decompiled with CFR 0_102.
 */
package pt.opensoft.http;

import java.io.IOException;

public class ConnectionTimeoutException
extends IOException {
    public ConnectionTimeoutException() {
    }

    public ConnectionTimeoutException(String s) {
        super(s);
    }
}

