/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.common.bean;

import java.beans.PropertyChangeListener;

public interface ObservableBean {
    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

