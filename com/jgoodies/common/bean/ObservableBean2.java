/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.common.bean;

import com.jgoodies.common.bean.ObservableBean;
import java.beans.PropertyChangeListener;

public interface ObservableBean2
extends ObservableBean {
    public void addPropertyChangeListener(String var1, PropertyChangeListener var2);

    public void removePropertyChangeListener(String var1, PropertyChangeListener var2);

    public PropertyChangeListener[] getPropertyChangeListeners();

    public PropertyChangeListener[] getPropertyChangeListeners(String var1);
}

