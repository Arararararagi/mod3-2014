/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.clearlook;

import javax.swing.JComponent;
import javax.swing.border.Border;

public interface ClearLookPolicy {
    public String getName();

    public Border replaceBorder(JComponent var1);
}

