/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.clearlook;

import com.jgoodies.clearlook.DefaultClearLookPolicy;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public final class NetBeansClearLookPolicy
extends DefaultClearLookPolicy {
    protected static final Border ORANGE1_BORDER = new LineBorder(Color.orange);
    protected static final Border ORANGE2_BORDER = new LineBorder(Color.orange.brighter());
    private static final String NB = "org.netbeans.";
    private static final String NB_CORE = "org.netbeans.core.";
    private static final String NB_WINDOWS = "org.netbeans.core.windows.";
    private static final String NB_FRAMES = "org.netbeans.core.windows.frames.";
    private static final String NB_OPENIDE = "org.openide.";
    private static final String NB_PROPERTYSHEET_PKG = "org.openide.explorer.propertysheet";
    private static final String NB_SPLITTEDPANEL = "org.openide.awt.SplittedPanel";
    private static final String NB_TOP_COMPONENT = "org.openide.windows.TopComponent";
    private static final String NB_PROPERTYSHEET_TAB = "org.openide.explorer.propertysheetPropertySheetTab";
    private static final String NB_PROPERTYSHEET = "org.openide.explorer.propertysheetPropertySheet";
    private static final String NB_FORM = "org.netbeans.modules.form.";
    private static final String NB_FORM_DESIGNER = "org.netbeans.modules.form.FormDesigner";
    public static final String NB_PERIMETER_PANE = "org.netbeans.core.windows.frames.PerimeterPane";
    public static final String NB_MULTITABBED_CONT = "org.netbeans.core.windows.frames.MultiTabbedContainerImpl";
    private static final String NB_STATUS_CELL = "org.netbeans.editor.StatusBar$Cell";

    public String getName() {
        return "ClearLook(tm) policy for NetBeans";
    }

    public Border replaceBorder(JComponent component) {
        Border border = component.getBorder();
        if (border != null && this.isParentDecoration(border)) {
            this.log("Decorated component = " + component.getClass().getName());
            this.log("Decorated border    = " + border.getClass().getName());
        } else if (this.isInstanceOf(component, "org.openide.windows.TopComponent")) {
            this.log("TopComponent        = " + component.getClass().getName());
        } else if (this.isInstanceOf(component, "org.openide.explorer.propertysheetPropertySheet")) {
            this.log("PropertySheet\t\t = " + component.getClass().getName());
        }
        return super.replaceBorder(component);
    }

    public Border analyse(JComponent component) {
        this.log("NetBeansClearLookPolicy.analyse(JComponent)");
        if (this.isPerimeterPaneChild(component)) {
            this.log("Perimeter child detected.");
            return this.getPerimeterChildBorder();
        }
        if (this.isTopComponentChild(component)) {
            this.log("TopComponent child detected.");
            return this.getTopComponentChildBorder();
        }
        if (this.isInstanceOf(component.getParent(), "org.openide.explorer.propertysheetPropertySheet")) {
            this.log("PropertySheet child detected.");
            return this.getPropertySheetChildBorder();
        }
        return super.analyse(component);
    }

    public Border analyse(JLabel label) {
        this.log("NetBeansClearLookPolicy.analyse(JLabel)");
        if (this.isNetBeansStatusCell(label)) {
            this.log("NetBeans status cell detected.");
            return this.getNetBeansStatusCellBorder();
        }
        return super.analyse(label);
    }

    public Border analyse(JPanel panel) {
        this.log("NetBeansClearLookPolicy.analyse(JPanel)");
        if (this.isNetBeansSpecialPanel(panel)) {
            this.log("NetBeans special panel detected.");
            return this.getNetBeansSpecialPanelBorder();
        }
        return super.analyse(panel);
    }

    public Border analyse(JScrollPane scrollPane) {
        this.log("NetBeansClearLookPolicy.analyse(JScrollPane)");
        Container parent = scrollPane.getParent();
        if (this.isNetBeansWrapper(parent)) {
            this.log("NetBeans ScrollPane wrapper detected.");
            return this.getNetBeansScrollPaneBorder();
        }
        return super.analyse(scrollPane);
    }

    protected boolean isDecoratedChild(Component c) {
        boolean b = super.isDecoratedChild(c);
        return b || this.isInstanceOf(c, "org.openide.explorer.propertysheetPropertySheetTab");
    }

    protected boolean isDecoratingParent(Component c) {
        return super.isDecoratingParent(c) || this.isInstanceOf(c, "org.netbeans.core.windows.frames.PerimeterPane") || this.isInstanceOf(c, "org.netbeans.core.windows.frames.MultiTabbedContainerImpl");
    }

    protected boolean isKindOfSplitPane(Component component) {
        boolean b = super.isKindOfSplitPane(component);
        if (b) {
            return b;
        }
        return this.isInstanceOf(component, "org.openide.awt.SplittedPanel");
    }

    private boolean isPerimeterPaneChild(JComponent c) {
        return this.isInstanceOf(c.getParent(), "org.netbeans.core.windows.frames.PerimeterPane");
    }

    private boolean isTopComponentChild(JComponent c) {
        return this.isInstanceOf(c.getParent(), "org.openide.windows.TopComponent");
    }

    private boolean isNetBeansSpecialPanel(JPanel panel) {
        return this.isInstanceOf(panel, "org.openide.awt.SplittedPanel");
    }

    private boolean isNetBeansStatusCell(Component c) {
        return this.isInstanceOf(c, "org.netbeans.editor.StatusBar$Cell");
    }

    private boolean isNetBeansWrapper(Component c) {
        String className = c.getClass().getName();
        return className.equals("org.netbeans.modules.form.FormDesigner") || className.equals("org.openide.explorer.propertysheetPropertySheetTab") || this.isInstanceOf(c, "org.openide.explorer.propertysheetPropertySheet");
    }

    protected final boolean isInstanceOf(Object object, String className) {
        for (Class clazz = object.getClass(); clazz != null; clazz = clazz.getSuperclass()) {
            if (!clazz.getName().equals(className)) continue;
            return true;
        }
        return false;
    }

    private Border getNetBeansScrollPaneBorder() {
        return this.isDebug() ? ORANGE1_BORDER : UIManager.getBorder("ClearLook.NetBeansScrollPaneBorder");
    }

    private Border getNetBeansSpecialPanelBorder() {
        return this.isDebug() ? ORANGE2_BORDER : UIManager.getBorder("ClearLook.NetBeansSpecialPanelBorder");
    }

    private Border getNetBeansStatusCellBorder() {
        return this.isDebug() ? PINK2_BORDER : UIManager.getBorder("ClearLook.NetBeansStatusCellBorder");
    }

    private Border getPerimeterChildBorder() {
        return this.isDebug() ? ORANGE1_BORDER : this.getThinLoweredBevelBorder();
    }

    private Border getTopComponentChildBorder() {
        return this.isDebug() ? ORANGE2_BORDER : EMPTY_BORDER;
    }

    private Border getPropertySheetChildBorder() {
        return this.isDebug() ? PINK1_BORDER : EMPTY_BORDER;
    }
}

