/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.clearlook;

import com.jgoodies.clearlook.ClearLookManager;
import com.jgoodies.clearlook.ClearLookPolicy;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class DefaultClearLookPolicy
implements ClearLookPolicy {
    protected static final Border EMPTY_BORDER;
    protected static final Border MARKER_BORDER;
    protected static final Border RED1_BORDER;
    protected static final Border RED2_BORDER;
    protected static final Border PINK1_BORDER;
    protected static final Border PINK2_BORDER;
    static /* synthetic */ Class class$javax$swing$border$BevelBorder;
    static /* synthetic */ Class class$javax$swing$border$EtchedBorder;
    static /* synthetic */ Class class$javax$swing$border$LineBorder;

    public String getName() {
        return "Default ClearLook(tm) policy";
    }

    public Border replaceBorder(JComponent component) {
        if (this.hasEmptyBorder(component)) {
            return null;
        }
        Border originalBorder = component.getBorder();
        Container parent = component.getParent();
        this.log("");
        this.log("Component =" + component.getClass().getName());
        this.log("Parent    =" + parent.getClass().getName());
        if (originalBorder != null) {
            this.log("Border    =" + originalBorder.getClass().getName());
            if (originalBorder instanceof CompoundBorder) {
                CompoundBorder compoundBorder = (CompoundBorder)originalBorder;
                this.log("   outside=" + compoundBorder.getOutsideBorder());
                this.log("   inside =" + compoundBorder.getInsideBorder());
            }
        }
        Border newBorder = null;
        Method method = this.findAnalyseMethod(component);
        if (method != null) {
            try {
                newBorder = (Border)method.invoke(this, component);
            }
            catch (InvocationTargetException e) {
                this.log(e.getLocalizedMessage());
            }
            catch (IllegalAccessException e) {
                this.log(e.getLocalizedMessage());
            }
        } else {
            this.log("Could not find an analyse method for " + component.getClass().getName());
            newBorder = this.analyse(component);
        }
        return this.assignBorder(component, newBorder);
    }

    protected Border assignBorder(JComponent c, Border newBorder) {
        if (newBorder == null) {
            return null;
        }
        this.log("New border=" + newBorder.getClass().getName());
        if (c instanceof JTabbedPane) {
            return newBorder;
        }
        c.setBorder(newBorder);
        return c.getBorder();
    }

    public Border analyse(JComponent component) {
        this.log("DefaultClearLookPolicy.analyse(JComponent)");
        if (this.isDoubleDecorated(component)) {
            this.log("Double decoration detected.");
            return this.getDoubleDecorationBorder();
        }
        if (this.hasBevelBorder(component)) {
            this.log("BevelBorder detected.");
            return this.getThinBevelBorder((BevelBorder)component.getBorder());
        }
        return null;
    }

    public Border analyse(JScrollPane scrollPane) {
        this.log("DefaultClearLookPolicy.analyse(JScrollPane)");
        if (this.isDecoratingParent(scrollPane.getParent())) {
            this.log("ScrollPane nested in decorating parent detected.");
            return this.getScrollPaneReplacementBorder();
        }
        return null;
    }

    public Border analyse(JSplitPane splitPane) {
        this.log("DefaultClearLookPolicy.analyse(JSplitPane)");
        if (this.isDecoratingParent(splitPane.getParent())) {
            this.log("SplitPane nested in decorating parent detected.");
            return this.getSplitPaneReplacementBorder();
        }
        return null;
    }

    public Border analyse(JTabbedPane tab) {
        this.log("DefaultClearLookPolicy.analyse(JTabbedPane)");
        if (this.isDecoratingParent(tab.getParent())) {
            this.log("TabbedPane in decorating parent detected.");
            return MARKER_BORDER;
        }
        return null;
    }

    protected boolean isEmptyBorder(Border b) {
        return b == null || b instanceof EmptyBorder;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    protected boolean isDecoration(Border border) {
        Class clazz = border.getClass();
        Class class_ = class$javax$swing$border$BevelBorder == null ? (DefaultClearLookPolicy.class$javax$swing$border$BevelBorder = DefaultClearLookPolicy.class$("javax.swing.border.BevelBorder")) : class$javax$swing$border$BevelBorder;
        if (clazz.equals(class_)) return true;
        Class class_2 = class$javax$swing$border$EtchedBorder == null ? (DefaultClearLookPolicy.class$javax$swing$border$EtchedBorder = DefaultClearLookPolicy.class$("javax.swing.border.EtchedBorder")) : class$javax$swing$border$EtchedBorder;
        if (clazz.equals(class_2)) return true;
        Class class_3 = class$javax$swing$border$LineBorder == null ? (DefaultClearLookPolicy.class$javax$swing$border$LineBorder = DefaultClearLookPolicy.class$("javax.swing.border.LineBorder")) : class$javax$swing$border$LineBorder;
        if (!clazz.equals(class_3)) return false;
        return true;
    }

    protected boolean isChildDecoration(Border border) {
        if (this.isDecoration(border)) {
            return true;
        }
        if (border instanceof CompoundBorder) {
            CompoundBorder compound = (CompoundBorder)border;
            return this.isDecoration(compound.getOutsideBorder());
        }
        return false;
    }

    protected boolean isParentDecoration(Border border) {
        if (this.isDecoration(border)) {
            return true;
        }
        if (border instanceof CompoundBorder) {
            CompoundBorder compound = (CompoundBorder)border;
            return this.isDecoration(compound.getInsideBorder());
        }
        return false;
    }

    protected boolean hasEmptyBorder(JComponent component) {
        return this.isEmptyBorder(component.getBorder()) && !(component instanceof JTabbedPane);
    }

    private boolean hasBevelBorder(JComponent component) {
        Border border = component.getBorder();
        return border instanceof BevelBorder;
    }

    protected boolean isDecoratedChild(Component c) {
        if (c instanceof JScrollPane) {
            return true;
        }
        if (!(c instanceof JComponent)) {
            return false;
        }
        JComponent comp = (JComponent)c;
        return this.isChildDecoration(comp.getBorder());
    }

    protected boolean isDecoratingParent(Component c) {
        if (c instanceof JScrollPane) {
            return true;
        }
        if (c instanceof JTabbedPane) {
            return true;
        }
        if (this.isDecoratedSplitPane(c)) {
            return true;
        }
        if (this.isInternalFrameContent(c)) {
            return true;
        }
        if (!(c instanceof JComponent)) {
            return false;
        }
        JComponent comp = (JComponent)c;
        return comp.getComponentCount() == 1 && this.isParentDecoration(comp.getBorder());
    }

    protected boolean isInternalFrameContent(Component c) {
        Container parent = c.getParent();
        return parent instanceof JRootPane;
    }

    protected boolean isKindOfSplitPane(Component component) {
        return component instanceof JSplitPane;
    }

    protected boolean isDecoratedSplitPane(Component c) {
        return this.isKindOfSplitPane(c) && (!(c instanceof JSplitPane) || !this.isEmptyBorder(((JSplitPane)c).getBorder()));
    }

    protected boolean isDoubleDecorated(JComponent c) {
        return this.isDecoratedChild(c) && this.isDecoratingParent(c.getParent());
    }

    protected Border getDoubleDecorationBorder() {
        return this.isDebug() ? RED1_BORDER : EMPTY_BORDER;
    }

    protected Border getScrollPaneReplacementBorder() {
        return this.isDebug() ? RED1_BORDER : UIManager.getBorder("ClearLook.ScrollPaneReplacementBorder");
    }

    protected Border getSplitPaneReplacementBorder() {
        return this.isDebug() ? RED2_BORDER : UIManager.getBorder("ClearLook.SplitPaneReplacementBorder");
    }

    protected Border getThinBevelBorder(BevelBorder bevelBorder) {
        return bevelBorder.getBevelType() == 0 ? this.getThinRaisedBevelBorder() : this.getThinLoweredBevelBorder();
    }

    protected Border getThinLoweredBevelBorder() {
        return this.isDebug() ? PINK1_BORDER : UIManager.getBorder("ClearLook.ThinLoweredBorder");
    }

    protected Border getThinRaisedBevelBorder() {
        return this.isDebug() ? PINK2_BORDER : UIManager.getBorder("ClearLook.ThinRaisedBorder");
    }

    protected final void log(String message) {
        ClearLookManager.log(message);
    }

    protected final boolean isDebug() {
        return ClearLookManager.getMode().isDebug();
    }

    private Method findAnalyseMethod(JComponent c) {
        for (Class clazz = c.getClass(); clazz != null; clazz = clazz.getSuperclass()) {
            try {
                return this.getClass().getMethod("analyse", clazz);
            }
            catch (NoSuchMethodException e) {
                this.log("parameter=" + clazz + "; Receiver=" + this.getClass());
                continue;
            }
        }
        return null;
    }

    static /* synthetic */ Class class$(String x0) {
        try {
            return Class.forName(x0);
        }
        catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    static {
        MARKER_BORDER = DefaultClearLookPolicy.EMPTY_BORDER = new EmptyBorder(0, 0, 0, 0);
        RED1_BORDER = new LineBorder(Color.red);
        RED2_BORDER = new LineBorder(Color.red.brighter());
        PINK1_BORDER = new LineBorder(Color.pink);
        PINK2_BORDER = new LineBorder(Color.pink.brighter());
    }
}

