/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.clearlook;

import com.jgoodies.clearlook.ClearLookMode;
import com.jgoodies.clearlook.ClearLookPolicy;
import com.jgoodies.plaf.LookUtils;
import java.io.PrintStream;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;

public final class ClearLookManager {
    private static final ClearLookMode DEFAULT_MODE = ClearLookMode.OFF;
    private static final ClearLookMode NETBEANS_DEFAULT_MODE = ClearLookMode.ON;
    private static final String DEFAULT_POLICY_NAME = "com.jgoodies.clearlook.DefaultClearLookPolicy";
    private static final String NETBEANS_POLICY_NAME = "com.jgoodies.clearlook.NetBeansClearLookPolicy";
    private static ClearLookMode mode;
    private static ClearLookPolicy policy;

    private ClearLookManager() {
    }

    public static Border replaceBorder(JComponent component) {
        return mode.isEnabled() && policy != null ? policy.replaceBorder(component) : null;
    }

    public static ClearLookMode getMode() {
        return mode;
    }

    public static void setMode(ClearLookMode newMode) {
        ClearLookManager.setMode(newMode, false);
    }

    private static void setMode(ClearLookMode newMode, boolean userChoosen) {
        mode = newMode;
        if (userChoosen) {
            LookUtils.log("You have choosen to use the ClearLook(tm) mode '" + mode.getName() + "'.");
        } else if (mode.isEnabled()) {
            LookUtils.log("The ClearLook(tm) mode has been set to '" + mode.getName() + "'.");
        }
    }

    public static void installDefaultMode() {
        ClearLookMode aMode;
        Object value;
        String userMode = LookUtils.getSystemProperty("ClearLook.mode", "");
        boolean overridden = userMode.length() > 0;
        Object object = value = overridden ? userMode : UIManager.get("ClearLook.mode");
        ClearLookMode result = value == null ? ClearLookManager.getDefaultMode() : (value instanceof ClearLookMode ? (ClearLookMode)value : (value instanceof String ? ((aMode = ClearLookMode.valueOf((String)value)) != null ? aMode : ClearLookMode.OFF) : ClearLookMode.OFF));
        if (overridden && !result.getName().equalsIgnoreCase(userMode)) {
            LookUtils.log("I could not find the ClearLook(tm) mode '" + userMode + "'.");
        }
        ClearLookManager.setMode(result, overridden);
    }

    private static ClearLookMode getDefaultMode() {
        return LookUtils.IS_NETBEANS ? NETBEANS_DEFAULT_MODE : DEFAULT_MODE;
    }

    public static ClearLookPolicy getPolicy() {
        return policy;
    }

    public static void setPolicy(ClearLookPolicy newPolicy) {
        policy = newPolicy;
        if (mode.isVerbose() && policy != null) {
            LookUtils.log("You have choosen to use the ClearLook(tm) policy '" + policy.getName() + "'.");
        }
    }

    public static void setPolicy(String policyClassName) {
        try {
            Class clazz = Class.forName(policyClassName);
            ClearLookManager.setPolicy((ClearLookPolicy)clazz.newInstance());
        }
        catch (ClassNotFoundException e) {
            LookUtils.log("I could not find the ClearLook(tm) policy '" + policyClassName + "'.");
        }
        catch (Exception e) {
            LookUtils.log("I could not instantiate the ClearLook(tm) policy '" + policyClassName + "'.");
        }
    }

    private static void installDefaultPolicy() {
        String userPolicy = LookUtils.getSystemProperty("ClearLook.policy", "");
        String className = userPolicy.length() > 0 ? userPolicy : ClearLookManager.getDefaultPolicyName();
        ClearLookManager.setPolicy(className);
    }

    private static String getDefaultPolicyName() {
        return LookUtils.IS_NETBEANS ? "com.jgoodies.clearlook.NetBeansClearLookPolicy" : "com.jgoodies.clearlook.DefaultClearLookPolicy";
    }

    public static void log(String message) {
        if (ClearLookManager.getMode().isVerbose()) {
            System.out.println("CL:" + message);
        }
    }

    static {
        ClearLookManager.installDefaultMode();
        ClearLookManager.installDefaultPolicy();
    }
}

