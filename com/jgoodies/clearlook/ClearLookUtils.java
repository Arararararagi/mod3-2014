/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.clearlook;

import javax.swing.JComponent;
import javax.swing.border.Border;

public final class ClearLookUtils {
    private static final String CLEARLOOK_KEY = "ClearLook";
    private static final Object MARKER = "marker";

    private ClearLookUtils() {
    }

    public static boolean hasCheckedBorder(JComponent component) {
        return component.getClientProperty("ClearLook") != null;
    }

    public static Border getStoredBorder(JComponent component) {
        Object borderOrMarker = component.getClientProperty("ClearLook");
        return borderOrMarker == MARKER ? null : (Border)borderOrMarker;
    }

    public static void storeBorder(JComponent component, Border border) {
        Object borderOrMarker = border == null ? MARKER : border;
        component.putClientProperty("ClearLook", borderOrMarker);
    }
}

