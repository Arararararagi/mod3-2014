/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.clearlook;

public final class ClearLookMode {
    public static final ClearLookMode OFF = new ClearLookMode("Off", false, false);
    public static final ClearLookMode ON = new ClearLookMode("On", false, false);
    public static final ClearLookMode VERBOSE = new ClearLookMode("Verbose", true, false);
    public static final ClearLookMode DEBUG = new ClearLookMode("Debug", true, true);
    private final String name;
    private final boolean verbose;
    private final boolean debug;

    private ClearLookMode(String name, boolean verbose, boolean debug) {
        this.name = name;
        this.verbose = verbose;
        this.debug = debug;
    }

    public String getName() {
        return this.name;
    }

    public boolean isEnabled() {
        return this != OFF;
    }

    public boolean isVerbose() {
        return this.verbose;
    }

    public boolean isDebug() {
        return this.debug;
    }

    public static ClearLookMode valueOf(String name) {
        if (name.equalsIgnoreCase(ClearLookMode.OFF.name)) {
            return OFF;
        }
        if (name.equalsIgnoreCase(ClearLookMode.ON.name)) {
            return ON;
        }
        if (name.equalsIgnoreCase(ClearLookMode.VERBOSE.name)) {
            return VERBOSE;
        }
        if (name.equalsIgnoreCase(ClearLookMode.DEBUG.name)) {
            return DEBUG;
        }
        throw new IllegalArgumentException("Invalid ClearLook(tm) mode " + name);
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer(this.getClass().getName());
        buffer.append(": ");
        buffer.append(this.name);
        buffer.append("; enabled=");
        buffer.append(this.isEnabled());
        buffer.append("; verbose=");
        buffer.append(this.verbose ? "on" : "off");
        buffer.append("; debug=");
        buffer.append(this.verbose ? "on" : "off");
        return buffer.toString();
    }
}

