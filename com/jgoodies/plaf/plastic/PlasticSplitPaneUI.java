/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.clearlook.ClearLookManager;
import com.jgoodies.plaf.plastic.PlasticSplitPaneDivider;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

public final class PlasticSplitPaneUI
extends BasicSplitPaneUI {
    private Border storedBorder;
    private boolean hasCheckedBorderReplacement = false;

    public static ComponentUI createUI(JComponent x) {
        return new PlasticSplitPaneUI();
    }

    public BasicSplitPaneDivider createDefaultDivider() {
        return new PlasticSplitPaneDivider(this);
    }

    public void paint(Graphics g, JComponent c) {
        if (!this.hasCheckedBorderReplacement) {
            this.storedBorder = ClearLookManager.replaceBorder((JSplitPane)c);
            this.hasCheckedBorderReplacement = true;
        }
        super.paint(g, c);
    }

    protected void uninstallDefaults() {
        if (this.storedBorder != null) {
            this.splitPane.setBorder(this.storedBorder);
        }
        super.uninstallDefaults();
    }
}

