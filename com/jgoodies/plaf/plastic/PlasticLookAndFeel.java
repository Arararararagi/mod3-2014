/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.FontSizeHints;
import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.Options;
import com.jgoodies.plaf.common.MinimumSizedIcon;
import com.jgoodies.plaf.plastic.PlasticBorders;
import com.jgoodies.plaf.plastic.PlasticIconFactory;
import com.jgoodies.plaf.plastic.PlasticTheme;
import com.jgoodies.plaf.plastic.theme.SkyBluerTahoma;
import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.Icon;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.MetalTheme;

public class PlasticLookAndFeel
extends MetalLookAndFeel {
    public static final String BORDER_STYLE_KEY = "Plastic.borderStyle";
    public static final String IS_3D_KEY = "Plastic.is3D";
    public static final String DEFAULT_THEME_KEY = "Plastic.defaultTheme";
    public static final String HIGH_CONTRAST_FOCUS_ENABLED_KEY = "Plastic.highContrastFocus";
    protected static final String TAB_STYLE_KEY = "Plastic.tabStyle";
    public static final String TAB_STYLE_DEFAULT_VALUE = "default";
    public static final String TAB_STYLE_METAL_VALUE = "metal";
    private static boolean useMetalTabs = LookUtils.getSystemProperty("Plastic.tabStyle", "").equalsIgnoreCase("metal");
    public static boolean useHighContrastFocusColors = LookUtils.getSystemProperty("Plastic.highContrastFocus") != null;
    private static List installedThemes;
    private static PlasticTheme myCurrentTheme;
    private static boolean is3DEnabled;
    private static FontSizeHints fontSizeHints;
    private static final String THEME_CLASSNAME_PREFIX = "com.jgoodies.plaf.plastic.theme.";

    public PlasticLookAndFeel() {
        if (null == myCurrentTheme) {
            PlasticLookAndFeel.setMyCurrentTheme(PlasticLookAndFeel.createMyDefaultTheme());
        }
    }

    public String getID() {
        return "JGoodies Plastic";
    }

    public String getName() {
        return "JGoodies Plastic";
    }

    public String getDescription() {
        return "The JGoodies Plastic Look and Feel - \u00a9 2003 JGoodies Karsten Lentzsch";
    }

    public static FontSizeHints getFontSizeHints() {
        return fontSizeHints != null ? fontSizeHints : Options.getGlobalFontSizeHints();
    }

    public static void setFontSizeHints(FontSizeHints newHints) {
        fontSizeHints = newHints;
    }

    protected boolean is3DEnabled() {
        return is3DEnabled;
    }

    public static void set3DEnabled(boolean b) {
        is3DEnabled = b;
    }

    public static String getTabStyle() {
        return useMetalTabs ? "metal" : "default";
    }

    public static void setTabStyle(String tabStyle) {
        useMetalTabs = tabStyle.equalsIgnoreCase("metal");
    }

    public static boolean getHighContrastFocusColorsEnabled() {
        return useHighContrastFocusColors;
    }

    public static void setHighContrastFocusColorsEnabled(boolean b) {
        useHighContrastFocusColors = b;
    }

    protected void initClassDefaults(UIDefaults table) {
        super.initClassDefaults(table);
        String PLASTIC_PREFIX = "com.jgoodies.plaf.plastic.Plastic";
        String COMMON_PREFIX = "com.jgoodies.plaf.common.ExtBasic";
        Object[] uiDefaults = new Object[]{"ButtonUI", PLASTIC_PREFIX + "ButtonUI", "ToggleButtonUI", PLASTIC_PREFIX + "ToggleButtonUI", "ComboBoxUI", PLASTIC_PREFIX + "ComboBoxUI", "ScrollBarUI", PLASTIC_PREFIX + "ScrollBarUI", "SpinnerUI", PLASTIC_PREFIX + "SpinnerUI", "MenuBarUI", PLASTIC_PREFIX + "MenuBarUI", "ToolBarUI", PLASTIC_PREFIX + "ToolBarUI", "MenuUI", PLASTIC_PREFIX + "MenuUI", "MenuItemUI", COMMON_PREFIX + "MenuItemUI", "CheckBoxMenuItemUI", COMMON_PREFIX + "CheckBoxMenuItemUI", "RadioButtonMenuItemUI", COMMON_PREFIX + "RadioButtonMenuItemUI", "PopupMenuSeparatorUI", COMMON_PREFIX + "PopupMenuSeparatorUI", "OptionPaneUI", PLASTIC_PREFIX + "OptionPaneUI", "LabelUI", PLASTIC_PREFIX + "LabelUI", "PanelUI", PLASTIC_PREFIX + "PanelUI", "ScrollPaneUI", PLASTIC_PREFIX + "ScrollPaneUI", "SplitPaneUI", PLASTIC_PREFIX + "SplitPaneUI", "TreeUI", PLASTIC_PREFIX + "TreeUI", "InternalFrameUI", PLASTIC_PREFIX + "InternalFrameUI"};
        table.putDefaults(uiDefaults);
        if (!useMetalTabs) {
            table.put("TabbedPaneUI", PLASTIC_PREFIX + "TabbedPaneUI");
        }
    }

    protected void initComponentDefaults(UIDefaults table) {
        super.initComponentDefaults(table);
        BasicBorders.MarginBorder marginBorder = new BasicBorders.MarginBorder();
        Border buttonBorder = PlasticBorders.getButtonBorder();
        Border menuItemBorder = PlasticBorders.getMenuItemBorder();
        Border textFieldBorder = PlasticBorders.getTextFieldBorder();
        Border toggleButtonBorder = PlasticBorders.getToggleButtonBorder();
        Border popupMenuBorder = PlasticBorders.getPopupMenuBorder();
        Border scrollPaneBorder = PlasticBorders.getScrollPaneBorder();
        BorderUIResource tableHeaderBorder = new BorderUIResource((Border)table.get("TableHeader.cellBorder"));
        BasicBorders.MarginBorder menuBarEmptyBorder = marginBorder;
        Border menuBarSeparatorBorder = PlasticBorders.getSeparatorBorder();
        Border menuBarEtchedBorder = PlasticBorders.getEtchedBorder();
        Border menuBarHeaderBorder = PlasticBorders.getMenuBarHeaderBorder();
        BasicBorders.MarginBorder toolBarEmptyBorder = marginBorder;
        Border toolBarSeparatorBorder = PlasticBorders.getSeparatorBorder();
        Border toolBarEtchedBorder = PlasticBorders.getEtchedBorder();
        Border toolBarHeaderBorder = PlasticBorders.getToolBarHeaderBorder();
        BorderUIResource internalFrameBorder = PlasticLookAndFeel.getInternalFrameBorder();
        BorderUIResource paletteBorder = PlasticLookAndFeel.getPaletteBorder();
        BorderUIResource emptyBorder = new BorderUIResource(new EmptyBorder(0, 0, 0, 0));
        Border thinLoweredBorder = PlasticBorders.getThinLoweredBorder();
        Border thinRaisedBorder = PlasticBorders.getThinRaisedBorder();
        BorderUIResource.CompoundBorderUIResource statusCellBorder = new BorderUIResource.CompoundBorderUIResource(new EmptyBorder(2, 0, 1, 3), thinLoweredBorder);
        Color controlColor = table.getColor("control");
        Icon checkBoxIcon = PlasticIconFactory.getCheckBoxIcon();
        InsetsUIResource checkBoxMargin = new InsetsUIResource(2, 0, 2, 1);
        Insets defaultButtonMargin = LookUtils.createButtonMargin(false);
        Insets narrowButtonMargin = LookUtils.createButtonMargin(true);
        InsetsUIResource textInsets = new InsetsUIResource(1, 2, 1, 2);
        InsetsUIResource wrappedTextInsets = new InsetsUIResource(2, 3, 1, 2);
        InsetsUIResource menuItemMargin = LookUtils.isLowRes ? new InsetsUIResource(3, 0, 3, 0) : new InsetsUIResource(2, 0, 2, 0);
        InsetsUIResource menuMargin = new InsetsUIResource(2, 4, 2, 4);
        MinimumSizedIcon menuItemCheckIcon = new MinimumSizedIcon();
        Icon checkBoxMenuItemIcon = PlasticIconFactory.getCheckBoxMenuItemIcon();
        Icon radioButtonMenuItemIcon = PlasticIconFactory.getRadioButtonMenuItemIcon();
        Color menuItemForeground = table.getColor("MenuItem.foreground");
        int treeFontSize = table.getFont("Tree.font").getSize();
        Integer rowHeight = new Integer(treeFontSize + 6);
        Icon treeExpandedIcon = PlasticIconFactory.getExpandedTreeIcon();
        Icon treeCollapsedIcon = PlasticIconFactory.getCollapsedTreeIcon();
        ColorUIResource gray = new ColorUIResource(Color.gray);
        Boolean is3D = new Boolean(this.is3DEnabled());
        Object[] defaults = new Object[]{"Button.border", buttonBorder, "Button.margin", defaultButtonMargin, "Button.narrowMargin", narrowButtonMargin, "CheckBox.margin", checkBoxMargin, "CheckBox.icon", checkBoxIcon, "CheckBoxMenuItem.border", menuItemBorder, "CheckBoxMenuItem.margin", menuItemMargin, "CheckBoxMenuItem.checkIcon", checkBoxMenuItemIcon, "CheckBoxMenuItem.background", PlasticLookAndFeel.getMenuItemBackground(), "CheckBoxMenuItem.selectionForeground", PlasticLookAndFeel.getMenuItemSelectedForeground(), "CheckBoxMenuItem.selectionBackground", PlasticLookAndFeel.getMenuItemSelectedBackground(), "CheckBoxMenuItem.acceleratorForeground", menuItemForeground, "CheckBoxMenuItem.acceleratorSelectionForeground", PlasticLookAndFeel.getMenuItemSelectedForeground(), "CheckBoxMenuItem.acceleratorSelectionBackground", PlasticLookAndFeel.getMenuItemSelectedBackground(), "ComboBox.selectionForeground", PlasticLookAndFeel.getMenuSelectedForeground(), "ComboBox.selectionBackground", PlasticLookAndFeel.getMenuSelectedBackground(), "ComboBox.arrowButtonBorder", PlasticBorders.getComboBoxArrowButtonBorder(), "ComboBox.editorBorder", PlasticBorders.getComboBoxEditorBorder(), "ComboBox.editorColumns", new Integer(5), "EditorPane.margin", wrappedTextInsets, "InternalFrame.border", internalFrameBorder, "InternalFrame.paletteBorder", paletteBorder, "List.font", PlasticLookAndFeel.getControlTextFont(), "Menu.border", PlasticBorders.getMenuBorder(), "Menu.margin", menuMargin, "Menu.arrowIcon", PlasticIconFactory.getMenuArrowIcon(), "MenuBar.emptyBorder", menuBarEmptyBorder, "MenuBar.separatorBorder", menuBarSeparatorBorder, "MenuBar.etchedBorder", menuBarEtchedBorder, "MenuBar.headerBorder", menuBarHeaderBorder, "MenuItem.border", menuItemBorder, "MenuItem.checkIcon", menuItemCheckIcon, "MenuItem.margin", menuItemMargin, "MenuItem.background", PlasticLookAndFeel.getMenuItemBackground(), "MenuItem.selectionForeground", PlasticLookAndFeel.getMenuItemSelectedForeground(), "MenuItem.selectionBackground", PlasticLookAndFeel.getMenuItemSelectedBackground(), "MenuItem.acceleratorForeground", menuItemForeground, "MenuItem.acceleratorSelectionForeground", PlasticLookAndFeel.getMenuItemSelectedForeground(), "MenuItem.acceleratorSelectionBackground", PlasticLookAndFeel.getMenuItemSelectedBackground(), "OptionPane.errorIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/Error.png"), "OptionPane.informationIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/Inform.png"), "OptionPane.warningIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/Warn.png"), "OptionPane.questionIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/Question.png"), "FileView.computerIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/Computer.gif"), "FileView.directoryIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/TreeClosed.gif"), "FileView.fileIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/File.gif"), "FileView.floppyDriveIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/FloppyDrive.gif"), "FileView.hardDriveIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/HardDrive.gif"), "FileChooser.homeFolderIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/HomeFolder.gif"), "FileChooser.newFolderIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/NewFolder.gif"), "FileChooser.upFolderIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/UpFolder.gif"), "Tree.closedIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/TreeClosed.gif"), "Tree.openIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/TreeOpen.gif"), "Tree.leafIcon", PlasticLookAndFeel.makeIcon(this.getClass(), "icons/TreeLeaf.gif"), "FormattedTextField.border", textFieldBorder, "FormattedTextField.margin", textInsets, "PasswordField.border", textFieldBorder, "PasswordField.margin", textInsets, "PopupMenu.border", popupMenuBorder, "PopupMenuSeparator.margin", new InsetsUIResource(3, 4, 3, 4), "RadioButton.margin", checkBoxMargin, "RadioButtonMenuItem.border", menuItemBorder, "RadioButtonMenuItem.checkIcon", radioButtonMenuItemIcon, "RadioButtonMenuItem.margin", menuItemMargin, "RadioButtonMenuItem.background", PlasticLookAndFeel.getMenuItemBackground(), "RadioButtonMenuItem.selectionForeground", PlasticLookAndFeel.getMenuItemSelectedForeground(), "RadioButtonMenuItem.selectionBackground", PlasticLookAndFeel.getMenuItemSelectedBackground(), "RadioButtonMenuItem.acceleratorForeground", menuItemForeground, "RadioButtonMenuItem.acceleratorSelectionForeground", PlasticLookAndFeel.getMenuItemSelectedForeground(), "RadioButtonMenuItem.acceleratorSelectionBackground", PlasticLookAndFeel.getMenuItemSelectedBackground(), "Separator.foreground", PlasticLookAndFeel.getControlDarkShadow(), "ScrollPane.border", scrollPaneBorder, "ScrollPane.etchedBorder", scrollPaneBorder, "SimpleInternalFrame.activeTitleForeground", PlasticLookAndFeel.getSimpleInternalFrameForeground(), "SimpleInternalFrame.activeTitleBackground", PlasticLookAndFeel.getSimpleInternalFrameBackground(), "Spinner.border", PlasticBorders.getFlush3DBorder(), "Spinner.defaultEditorInsets", textInsets, "SplitPane.dividerSize", new Integer(7), "TabbedPane.focus", PlasticLookAndFeel.getFocusColor(), "TabbedPane.tabInsets", new InsetsUIResource(1, 9, 1, 8), "Table.foreground", table.get("textText"), "Table.gridColor", controlColor, "Table.scrollPaneBorder", scrollPaneBorder, "TableHeader.cellBorder", tableHeaderBorder, "TextArea.margin", wrappedTextInsets, "TextField.border", textFieldBorder, "TextField.margin", textInsets, "TitledBorder.font", PlasticLookAndFeel.getTitleTextFont(), "TitledBorder.titleColor", PlasticLookAndFeel.getTitleTextColor(), "ToggleButton.border", toggleButtonBorder, "ToggleButton.margin", defaultButtonMargin, "ToggleButton.narrowMargin", narrowButtonMargin, "ToolBar.emptyBorder", toolBarEmptyBorder, "ToolBar.separatorBorder", toolBarSeparatorBorder, "ToolBar.etchedBorder", toolBarEtchedBorder, "ToolBar.headerBorder", toolBarHeaderBorder, "ToolTip.hideAccelerator", Boolean.TRUE, "Tree.expandedIcon", treeExpandedIcon, "Tree.collapsedIcon", treeCollapsedIcon, "Tree.line", gray, "Tree.hash", gray, "Tree.rowHeight", rowHeight, "Button.is3DEnabled", is3D, "ComboBox.is3DEnabled", is3D, "MenuBar.is3DEnabled", is3D, "ToolBar.is3DEnabled", is3D, "ScrollBar.is3DEnabled", is3D, "ToggleButton.is3DEnabled", is3D, "CheckBox.border", marginBorder, "RadioButton.border", marginBorder, "ClearLook.ScrollPaneReplacementBorder", emptyBorder, "ClearLook.SplitPaneReplacementBorder", emptyBorder, "ClearLook.ThinLoweredBorder", thinLoweredBorder, "ClearLook.ThinRaisedBorder", thinRaisedBorder, "ClearLook.NetBeansScrollPaneBorder", emptyBorder, "ClearLook.NetBeansSpecialPanelBorder", emptyBorder, "ClearLook.NetBeansStatusCellBorder", statusCellBorder};
        table.putDefaults(defaults);
    }

    protected void initSystemColorDefaults(UIDefaults table) {
        super.initSystemColorDefaults(table);
        table.put("unifiedControlShadow", table.getColor("controlDkShadow"));
        table.put("primaryControlHighlight", PlasticLookAndFeel.getPrimaryControlHighlight());
    }

    public static PlasticTheme createMyDefaultTheme() {
        PlasticTheme result;
        String defaultName = LookUtils.IS_OS_WINDOWS_XP ? "ExperienceBlue" : (LookUtils.IS_OS_WINDOWS_MODERN ? "DesertBluer" : "SkyBlue");
        String userName = LookUtils.getSystemProperty("Plastic.defaultTheme", "");
        boolean overridden = userName.length() > 0;
        String themeName = overridden ? userName : defaultName;
        PlasticTheme theme = PlasticLookAndFeel.createTheme(themeName);
        PlasticTheme plasticTheme = result = theme != null ? theme : new SkyBluerTahoma();
        if (overridden) {
            String className = theme.getClass().getName().substring("com.jgoodies.plaf.plastic.theme.".length());
            if (className.equals(userName)) {
                LookUtils.log("I have successfully installed the '" + theme.getName() + "' theme.");
            } else {
                LookUtils.log("I could not install the Plastic theme '" + userName + "'.");
                LookUtils.log("I have installed the '" + theme.getName() + "' theme, instead.");
            }
        }
        return result;
    }

    public static List getInstalledThemes() {
        if (null == installedThemes) {
            PlasticLookAndFeel.installDefaultThemes();
        }
        Collections.sort(installedThemes, new Comparator(){

            public int compare(Object o1, Object o2) {
                MetalTheme theme1 = (MetalTheme)o1;
                MetalTheme theme2 = (MetalTheme)o2;
                return theme1.getName().compareTo(theme2.getName());
            }
        });
        return installedThemes;
    }

    protected static void installDefaultThemes() {
        installedThemes = new ArrayList();
        String[] themeNames = new String[]{"BrownSugar", "DarkStar", "DesertBlue", "DesertBluer", "DesertGreen", "DesertRed", "DesertYellow", "ExperienceBlue", "ExperienceGreen", "Silver", "SkyBlue", "SkyBluer", "SkyBluerTahoma", "SkyGreen", "SkyKrupp", "SkyPink", "SkyRed", "SkyYellow"};
        for (int i = themeNames.length - 1; i >= 0; --i) {
            PlasticLookAndFeel.installTheme(PlasticLookAndFeel.createTheme(themeNames[i]));
        }
    }

    protected static PlasticTheme createTheme(String themeName) {
        String className = "com.jgoodies.plaf.plastic.theme." + themeName;
        try {
            Class cl = Class.forName(className);
            return (PlasticTheme)cl.newInstance();
        }
        catch (ClassNotFoundException e) {
        }
        catch (IllegalAccessException e) {
        }
        catch (InstantiationException e) {
            // empty catch block
        }
        LookUtils.log("Can't create theme " + className);
        return null;
    }

    public static void installTheme(PlasticTheme theme) {
        if (null == installedThemes) {
            PlasticLookAndFeel.installDefaultThemes();
        }
        installedThemes.add(theme);
    }

    public static PlasticTheme getMyCurrentTheme() {
        return myCurrentTheme;
    }

    public static void setMyCurrentTheme(PlasticTheme theme) {
        myCurrentTheme = theme;
        PlasticLookAndFeel.setCurrentTheme(theme);
    }

    public static BorderUIResource getInternalFrameBorder() {
        return new BorderUIResource(PlasticBorders.getInternalFrameBorder());
    }

    public static BorderUIResource getPaletteBorder() {
        return new BorderUIResource(PlasticBorders.getPaletteBorder());
    }

    public static ColorUIResource getPrimaryControlDarkShadow() {
        return PlasticLookAndFeel.getMyCurrentTheme().getPrimaryControlDarkShadow();
    }

    public static ColorUIResource getPrimaryControlHighlight() {
        return PlasticLookAndFeel.getMyCurrentTheme().getPrimaryControlHighlight();
    }

    public static ColorUIResource getPrimaryControlInfo() {
        return PlasticLookAndFeel.getMyCurrentTheme().getPrimaryControlInfo();
    }

    public static ColorUIResource getPrimaryControlShadow() {
        return PlasticLookAndFeel.getMyCurrentTheme().getPrimaryControlShadow();
    }

    public static ColorUIResource getPrimaryControl() {
        return PlasticLookAndFeel.getMyCurrentTheme().getPrimaryControl();
    }

    public static ColorUIResource getControlHighlight() {
        return PlasticLookAndFeel.getMyCurrentTheme().getControlHighlight();
    }

    public static ColorUIResource getControlDarkShadow() {
        return PlasticLookAndFeel.getMyCurrentTheme().getControlDarkShadow();
    }

    public static ColorUIResource getControl() {
        return PlasticLookAndFeel.getMyCurrentTheme().getControl();
    }

    public static ColorUIResource getFocusColor() {
        return PlasticLookAndFeel.getMyCurrentTheme().getFocusColor();
    }

    public static ColorUIResource getMenuItemBackground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getMenuItemBackground();
    }

    public static ColorUIResource getMenuItemSelectedBackground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getMenuItemSelectedBackground();
    }

    public static ColorUIResource getMenuItemSelectedForeground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getMenuItemSelectedForeground();
    }

    public static ColorUIResource getWindowTitleBackground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getWindowTitleBackground();
    }

    public static ColorUIResource getWindowTitleForeground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getWindowTitleForeground();
    }

    public static ColorUIResource getWindowTitleInactiveBackground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getWindowTitleInactiveBackground();
    }

    public static ColorUIResource getWindowTitleInactiveForeground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getWindowTitleInactiveForeground();
    }

    public static ColorUIResource getSimpleInternalFrameForeground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getSimpleInternalFrameForeground();
    }

    public static ColorUIResource getSimpleInternalFrameBackground() {
        return PlasticLookAndFeel.getMyCurrentTheme().getSimpleInternalFrameBackground();
    }

    public static ColorUIResource getTitleTextColor() {
        return PlasticLookAndFeel.getMyCurrentTheme().getTitleTextColor();
    }

    public static FontUIResource getTitleTextFont() {
        return PlasticLookAndFeel.getMyCurrentTheme().getTitleTextFont();
    }

    static {
        is3DEnabled = false;
    }

}

