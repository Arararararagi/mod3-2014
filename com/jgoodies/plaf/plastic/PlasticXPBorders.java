/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.plastic.PlasticComboBoxButton;
import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.PlasticXPUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.metal.MetalBorders;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.text.JTextComponent;

final class PlasticXPBorders {
    private static Border buttonBorder;
    private static Border comboBoxArrowButtonBorder;
    private static Border comboBoxEditorBorder;
    private static Border scrollPaneBorder;
    private static Border textFieldBorder;
    private static Border toggleButtonBorder;

    PlasticXPBorders() {
    }

    static Border getButtonBorder() {
        if (buttonBorder == null) {
            buttonBorder = new BorderUIResource.CompoundBorderUIResource(new XPButtonBorder(), new BasicBorders.MarginBorder());
        }
        return buttonBorder;
    }

    static Border getComboBoxArrowButtonBorder() {
        if (comboBoxArrowButtonBorder == null) {
            comboBoxArrowButtonBorder = new CompoundBorder(new XPComboBoxArrowButtonBorder(), new BasicBorders.MarginBorder());
        }
        return comboBoxArrowButtonBorder;
    }

    static Border getComboBoxEditorBorder() {
        if (comboBoxEditorBorder == null) {
            comboBoxEditorBorder = new CompoundBorder(new XPComboBoxEditorBorder(), new BasicBorders.MarginBorder());
        }
        return comboBoxEditorBorder;
    }

    static Border getScrollPaneBorder() {
        if (scrollPaneBorder == null) {
            scrollPaneBorder = new XPScrollPaneBorder();
        }
        return scrollPaneBorder;
    }

    static Border getTextFieldBorder() {
        if (textFieldBorder == null) {
            textFieldBorder = new BorderUIResource.CompoundBorderUIResource(new XPTextFieldBorder(), new BasicBorders.MarginBorder());
        }
        return textFieldBorder;
    }

    static Border getToggleButtonBorder() {
        if (toggleButtonBorder == null) {
            toggleButtonBorder = new BorderUIResource.CompoundBorderUIResource(new XPButtonBorder(), new BasicBorders.MarginBorder());
        }
        return toggleButtonBorder;
    }

    private static class XPButtonBorder
    extends AbstractBorder
    implements UIResource {
        protected static final Insets INSETS = LookUtils.isLowRes ? new Insets(3, 2, 3, 2) : new Insets(2, 2, 2, 2);

        private XPButtonBorder() {
        }

        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            boolean isFocused;
            AbstractButton button = (AbstractButton)c;
            ButtonModel model = button.getModel();
            if (!model.isEnabled()) {
                PlasticXPUtils.drawDisabledButtonBorder(g, x, y, w, h);
                return;
            }
            boolean isPressed = model.isPressed() && model.isArmed();
            boolean isDefault = button instanceof JButton && ((JButton)button).isDefaultButton();
            boolean bl = isFocused = button.isFocusPainted() && button.hasFocus();
            if (isPressed) {
                PlasticXPUtils.drawPressedButtonBorder(g, x, y, w, h);
            } else if (isFocused) {
                PlasticXPUtils.drawFocusedButtonBorder(g, x, y, w, h);
            } else if (isDefault) {
                PlasticXPUtils.drawDefaultButtonBorder(g, x, y, w, h);
            } else {
                PlasticXPUtils.drawPlainButtonBorder(g, x, y, w, h);
            }
        }

        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = XPButtonBorder.INSETS.top;
            newInsets.left = XPButtonBorder.INSETS.left;
            newInsets.bottom = XPButtonBorder.INSETS.bottom;
            newInsets.right = XPButtonBorder.INSETS.right;
            return newInsets;
        }
    }

    private static class XPComboBoxArrowButtonBorder
    extends AbstractBorder
    implements UIResource {
        protected static final Insets INSETS = new Insets(1, 1, 1, 1);

        private XPComboBoxArrowButtonBorder() {
        }

        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            PlasticComboBoxButton button = (PlasticComboBoxButton)c;
            JComboBox comboBox = button.getComboBox();
            ButtonModel model = button.getModel();
            if (!model.isEnabled()) {
                PlasticXPUtils.drawDisabledButtonBorder(g, x, y, w, h);
            } else {
                boolean isPressed = model.isPressed() && model.isArmed();
                boolean isFocused = comboBox.hasFocus();
                if (isPressed) {
                    PlasticXPUtils.drawPressedButtonBorder(g, x, y, w, h);
                } else if (isFocused) {
                    PlasticXPUtils.drawFocusedButtonBorder(g, x, y, w, h);
                } else {
                    PlasticXPUtils.drawPlainButtonBorder(g, x, y, w, h);
                }
            }
            if (comboBox.isEditable()) {
                g.setColor(model.isEnabled() ? PlasticLookAndFeel.getControlDarkShadow() : MetalLookAndFeel.getControlShadow());
                g.fillRect(x, y, 1, 1);
                g.fillRect(x, y + h - 1, 1, 1);
            }
        }

        public Insets getBorderInsets(Component c) {
            return INSETS;
        }
    }

    private static class XPComboBoxEditorBorder
    extends AbstractBorder {
        private static final Insets INSETS = new Insets(1, 1, 1, 0);

        private XPComboBoxEditorBorder() {
        }

        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.setColor(c.isEnabled() ? PlasticLookAndFeel.getControlDarkShadow() : MetalLookAndFeel.getControlShadow());
            PlasticXPUtils.drawRect(g, x, y, w + 1, h - 1);
        }

        public Insets getBorderInsets(Component c) {
            return INSETS;
        }
    }

    private static class XPScrollPaneBorder
    extends MetalBorders.ScrollPaneBorder {
        private static final Insets INSETS = new Insets(1, 1, 1, 1);

        private XPScrollPaneBorder() {
        }

        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            g.setColor(c.isEnabled() ? PlasticLookAndFeel.getControlDarkShadow() : MetalLookAndFeel.getControlShadow());
            PlasticXPUtils.drawRect(g, x, y, w - 1, h - 1);
        }

        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = XPScrollPaneBorder.INSETS.top;
            newInsets.left = XPScrollPaneBorder.INSETS.left;
            newInsets.bottom = XPScrollPaneBorder.INSETS.bottom;
            newInsets.right = XPScrollPaneBorder.INSETS.right;
            return newInsets;
        }
    }

    private static class XPTextFieldBorder
    extends AbstractBorder {
        private static final Insets INSETS = new Insets(1, 1, 1, 1);

        private XPTextFieldBorder() {
        }

        public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
            boolean enabled = c instanceof JTextComponent && c.isEnabled() && ((JTextComponent)c).isEditable() || c.isEnabled();
            g.setColor(enabled ? PlasticLookAndFeel.getControlDarkShadow() : MetalLookAndFeel.getControlShadow());
            PlasticXPUtils.drawRect(g, x, y, w - 1, h - 1);
        }

        public Insets getBorderInsets(Component c) {
            return INSETS;
        }

        public Insets getBorderInsets(Component c, Insets newInsets) {
            newInsets.top = XPTextFieldBorder.INSETS.top;
            newInsets.left = XPTextFieldBorder.INSETS.left;
            newInsets.bottom = XPTextFieldBorder.INSETS.bottom;
            newInsets.right = XPTextFieldBorder.INSETS.right;
            return newInsets;
        }
    }

}

