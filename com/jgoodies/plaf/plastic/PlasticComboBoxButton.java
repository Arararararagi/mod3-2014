/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.PlasticUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.Border;

final class PlasticComboBoxButton
extends JButton {
    private static final int LEFT_INSET = 2;
    private static final int RIGHT_INSET = 3;
    private final JList listBox;
    private final CellRendererPane rendererPane;
    private JComboBox comboBox;
    private Icon comboIcon;
    protected boolean iconOnly = false;
    private boolean borderPaintsFocus;

    PlasticComboBoxButton(JComboBox comboBox, Icon comboIcon, boolean iconOnly, CellRendererPane rendererPane, JList listBox) {
        super("");
        this.setModel(new DefaultButtonModel(){

            public void setArmed(boolean armed) {
                super.setArmed(this.isPressed() || armed);
            }
        });
        this.comboBox = comboBox;
        this.comboIcon = comboIcon;
        this.iconOnly = iconOnly;
        this.rendererPane = rendererPane;
        this.listBox = listBox;
        this.setEnabled(comboBox.isEnabled());
        this.setFocusable(false);
        this.setRequestFocusEnabled(comboBox.isEnabled());
        this.setBorder(UIManager.getBorder("ComboBox.arrowButtonBorder"));
        this.setMargin(new Insets(0, 2, 0, 3));
        this.borderPaintsFocus = UIManager.getBoolean("ComboBox.borderPaintsFocus");
    }

    public JComboBox getComboBox() {
        return this.comboBox;
    }

    public void setComboBox(JComboBox cb) {
        this.comboBox = cb;
    }

    public Icon getComboIcon() {
        return this.comboIcon;
    }

    public void setComboIcon(Icon i) {
        this.comboIcon = i;
    }

    public boolean isIconOnly() {
        return this.iconOnly;
    }

    public void setIconOnly(boolean b) {
        this.iconOnly = b;
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            this.setBackground(this.comboBox.getBackground());
            this.setForeground(this.comboBox.getForeground());
        } else {
            this.setBackground(UIManager.getColor("ComboBox.disabledBackground"));
            this.setForeground(UIManager.getColor("ComboBox.disabledForeground"));
        }
    }

    private boolean is3D() {
        if (PlasticUtils.force3D(this.comboBox)) {
            return true;
        }
        if (PlasticUtils.forceFlat(this.comboBox)) {
            return false;
        }
        return PlasticUtils.is3D("ComboBox.");
    }

    public void paintComponent(Graphics g) {
        int iconLeft;
        super.paintComponent(g);
        boolean leftToRight = PlasticUtils.isLeftToRight(this.comboBox);
        Insets insets = this.getInsets();
        int width = this.getWidth() - (insets.left + insets.right);
        int height = this.getHeight() - (insets.top + insets.bottom);
        if (height <= 0 || width <= 0) {
            return;
        }
        int left = insets.left;
        int top = insets.top;
        int right = left + (width - 1);
        int iconWidth = 0;
        int n = iconLeft = leftToRight ? right : left;
        if (this.comboIcon != null) {
            int iconTop;
            iconWidth = this.comboIcon.getIconWidth();
            int iconHeight = this.comboIcon.getIconHeight();
            if (this.iconOnly) {
                iconLeft = (this.getWidth() - iconWidth) / 2;
                iconTop = (this.getHeight() - iconHeight) / 2;
            } else {
                iconLeft = leftToRight ? left + (width - 1) - iconWidth : left;
                iconTop = (this.getHeight() - iconHeight) / 2;
            }
            this.comboIcon.paintIcon(this, g, iconLeft, iconTop);
        }
        if (!(this.iconOnly || this.comboBox == null)) {
            ListCellRenderer<Object> renderer = this.comboBox.getRenderer();
            boolean renderPressed = this.getModel().isPressed();
            Component c = renderer.getListCellRendererComponent(this.listBox, this.comboBox.getSelectedItem(), -1, renderPressed, false);
            c.setFont(this.rendererPane.getFont());
            if (this.model.isArmed() && this.model.isPressed()) {
                if (this.isOpaque()) {
                    c.setBackground(UIManager.getColor("Button.select"));
                }
                c.setForeground(this.comboBox.getForeground());
            } else if (!this.comboBox.isEnabled()) {
                if (this.isOpaque()) {
                    c.setBackground(UIManager.getColor("ComboBox.disabledBackground"));
                }
                c.setForeground(UIManager.getColor("ComboBox.disabledForeground"));
            } else {
                c.setForeground(this.comboBox.getForeground());
                c.setBackground(this.comboBox.getBackground());
            }
            int cWidth = width - (insets.right + iconWidth);
            boolean shouldValidate = c instanceof JPanel;
            int x = leftToRight ? left : left + iconWidth;
            int myHeight = this.getHeight() - 2 - 3 - 1;
            if (!this.is3D()) {
                this.rendererPane.paintComponent(g, c, this, x, top + 2, cWidth, myHeight, shouldValidate);
            } else if (!(c instanceof JComponent)) {
                this.rendererPane.paintComponent(g, c, this, x, top + 2, cWidth, myHeight, shouldValidate);
            } else {
                JComponent component = (JComponent)c;
                boolean hasBeenOpaque = component.isOpaque();
                component.setOpaque(false);
                this.rendererPane.paintComponent(g, c, this, x, top + 2, cWidth, myHeight, shouldValidate);
                component.setOpaque(hasBeenOpaque);
            }
        }
        if (this.comboIcon != null) {
            boolean hasFocus = this.comboBox.hasFocus();
            if (!this.borderPaintsFocus && hasFocus) {
                g.setColor(PlasticLookAndFeel.getFocusColor());
                int x = 2;
                int y = 2;
                int w = this.getWidth() - 2 - 3;
                int h = this.getHeight() - 2 - 3;
                g.drawRect(x, y, w - 1, h - 1);
            }
        }
    }

}

