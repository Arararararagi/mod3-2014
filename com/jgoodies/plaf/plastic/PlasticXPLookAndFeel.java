/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.Options;
import com.jgoodies.plaf.plastic.Plastic3DLookAndFeel;
import com.jgoodies.plaf.plastic.PlasticXPBorders;
import com.jgoodies.plaf.plastic.PlasticXPIconFactory;
import java.awt.Insets;
import javax.swing.Icon;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.InsetsUIResource;

public class PlasticXPLookAndFeel
extends Plastic3DLookAndFeel {
    public String getID() {
        return "JGoodies Plastic XP";
    }

    public String getName() {
        return "JGoodies Plastic XP";
    }

    public String getDescription() {
        return "The JGoodies Plastic XP Look and Feel - \u00a9 2003 JGoodies Karsten Lentzsch";
    }

    protected void initClassDefaults(UIDefaults table) {
        super.initClassDefaults(table);
        String UI_CLASSNAME_PREFIX = "com.jgoodies.plaf.plastic.PlasticXP";
        Object[] uiDefaults = new Object[]{"CheckBoxUI", UI_CLASSNAME_PREFIX + "CheckBoxUI", "RadioButtonUI", UI_CLASSNAME_PREFIX + "RadioButtonUI"};
        table.putDefaults(uiDefaults);
    }

    protected void initComponentDefaults(UIDefaults table) {
        super.initComponentDefaults(table);
        Border buttonBorder = PlasticXPBorders.getButtonBorder();
        Icon checkBoxIcon = PlasticXPIconFactory.getCheckBoxIcon();
        Border comboBoxButtonBorder = PlasticXPBorders.getComboBoxArrowButtonBorder();
        Border comboBoxEditorBorder = PlasticXPBorders.getComboBoxEditorBorder();
        Icon radioButtonIcon = PlasticXPIconFactory.getRadioButtonIcon();
        Border scrollPaneBorder = PlasticXPBorders.getScrollPaneBorder();
        Border textFieldBorder = PlasticXPBorders.getTextFieldBorder();
        Border toggleButtonBorder = PlasticXPBorders.getToggleButtonBorder();
        Insets defaultButtonMargin = PlasticXPLookAndFeel.createButtonMargin(false);
        Insets narrowButtonMargin = PlasticXPLookAndFeel.createButtonMargin(true);
        String radioCheckIconName = LookUtils.isLowRes ? "icons/RadioLight5x5.png" : "icons/RadioLight7x7.png";
        InsetsUIResource textInsets = new InsetsUIResource(2, 3, 2, 2);
        Object[] defaults = new Object[]{"Button.border", buttonBorder, "Button.margin", defaultButtonMargin, "Button.narrowMargin", narrowButtonMargin, "Button.borderPaintsFocus", Boolean.TRUE, "CheckBox.icon", checkBoxIcon, "CheckBox.check", this.getToggleButtonCheckColor(), "ComboBox.arrowButtonBorder", comboBoxButtonBorder, "ComboBox.editorBorder", comboBoxEditorBorder, "ComboBox.borderPaintsFocus", Boolean.TRUE, "EditorPane.margin", textInsets, "FormattedTextField.border", textFieldBorder, "FormattedTextField.margin", textInsets, "PasswordField.border", textFieldBorder, "PasswordField.margin", textInsets, "Spinner.border", scrollPaneBorder, "Spinner.defaultEditorInsets", textInsets, "ScrollPane.border", scrollPaneBorder, "Table.scrollPaneBorder", scrollPaneBorder, "RadioButton.icon", radioButtonIcon, "RadioButton.check", this.getToggleButtonCheckColor(), "RadioButton.interiorBackground", PlasticXPLookAndFeel.getControlHighlight(), "RadioButton.checkIcon", PlasticXPLookAndFeel.makeIcon(this.getClass(), radioCheckIconName), "TextArea.margin", textInsets, "TextField.border", textFieldBorder, "TextField.margin", textInsets, "ToggleButton.border", toggleButtonBorder, "ToggleButton.margin", defaultButtonMargin, "ToggleButton.narrowMargin", narrowButtonMargin, "ToggleButton.borderPaintsFocus", Boolean.TRUE};
        table.putDefaults(defaults);
    }

    protected static void installDefaultThemes() {
    }

    private static Insets createButtonMargin(boolean narrow) {
        int pad = narrow || Options.getUseNarrowButtons() ? 4 : 14;
        return LookUtils.isLowRes ? new InsetsUIResource(1, pad, 1, pad) : new InsetsUIResource(2, pad, 2, pad);
    }

    private ColorUIResource getToggleButtonCheckColor() {
        return PlasticXPLookAndFeel.getMyCurrentTheme().getToggleButtonCheckColor();
    }
}

