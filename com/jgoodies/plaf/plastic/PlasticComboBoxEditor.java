/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicComboBoxEditor;

class PlasticComboBoxEditor
extends BasicComboBoxEditor {
    PlasticComboBoxEditor() {
        this.editor = new JTextField("", UIManager.getInt("ComboBox.editorColumns"));
        this.editor.setBorder(UIManager.getBorder("ComboBox.editorBorder"));
    }

    static final class UIResource
    extends PlasticComboBoxEditor
    implements javax.swing.plaf.UIResource {
        UIResource() {
        }
    }

}

