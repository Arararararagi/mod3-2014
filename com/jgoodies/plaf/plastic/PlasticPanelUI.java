/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.clearlook.ClearLookManager;
import com.jgoodies.clearlook.ClearLookUtils;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPanelUI;

public final class PlasticPanelUI
extends BasicPanelUI {
    private static final PlasticPanelUI INSTANCE = new PlasticPanelUI();

    public static ComponentUI createUI(JComponent x) {
        return INSTANCE;
    }

    public void update(Graphics g, JComponent c) {
        if (!ClearLookUtils.hasCheckedBorder(c)) {
            Border oldBorder = ClearLookManager.replaceBorder((JPanel)c);
            ClearLookUtils.storeBorder(c, oldBorder);
        }
        super.update(g, c);
    }

    protected void uninstallDefaults(JPanel panel) {
        Border storedBorder = ClearLookUtils.getStoredBorder(panel);
        if (storedBorder != null) {
            panel.setBorder(storedBorder);
        }
        super.uninstallDefaults(panel);
    }
}

