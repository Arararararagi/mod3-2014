/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.theme.Colors;
import com.jgoodies.plaf.plastic.theme.ExperienceBlue;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;

public class Silver
extends ExperienceBlue {
    public String getName() {
        return "Silver";
    }

    protected ColorUIResource getPrimary1() {
        return Colors.GRAY_MEDIUM;
    }

    protected ColorUIResource getPrimary2() {
        return Colors.GRAY_MEDIUMLIGHT;
    }

    protected ColorUIResource getPrimary3() {
        return Colors.GRAY_LIGHTER2;
    }

    protected ColorUIResource getSecondary1() {
        return this.getPrimary1();
    }

    protected ColorUIResource getSecondary2() {
        return this.getPrimary2();
    }

    protected ColorUIResource getSecondary3() {
        return this.getPrimary3();
    }

    public ColorUIResource getFocusColor() {
        return PlasticLookAndFeel.useHighContrastFocusColors ? Colors.ORANGE_FOCUS : Colors.BLUE_MEDIUM_DARK;
    }

    public ColorUIResource getTitleTextColor() {
        return Colors.GRAY_DARKEST;
    }

    public void addCustomEntriesToTable(UIDefaults table) {
        super.addCustomEntriesToTable(table);
        Object[] uiDefaults = new Object[]{"ScrollBar.maxBumpsWidth", new Integer(50)};
        table.putDefaults(uiDefaults);
    }
}

