/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.plastic.theme.Colors;
import com.jgoodies.plaf.plastic.theme.DesertBluer;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;

public class ExperienceBlue
extends DesertBluer {
    private static final ColorUIResource secondary1 = new ColorUIResource(128, 128, 128);
    private static final ColorUIResource secondary2 = new ColorUIResource(189, 190, 176);
    private static final ColorUIResource secondary3 = new ColorUIResource(236, 233, 216);

    public String getName() {
        return "Experience Blue";
    }

    protected ColorUIResource getPrimary1() {
        return Colors.BLUE_MEDIUM_DARK;
    }

    protected ColorUIResource getPrimary2() {
        return Colors.BLUE_LOW_MEDIUM;
    }

    protected ColorUIResource getPrimary3() {
        return Colors.BLUE_LOW_LIGHTEST;
    }

    protected ColorUIResource getSecondary1() {
        return secondary1;
    }

    protected ColorUIResource getSecondary2() {
        return secondary2;
    }

    protected ColorUIResource getSecondary3() {
        return secondary3;
    }

    public ColorUIResource getFocusColor() {
        return Colors.ORANGE_FOCUS;
    }

    public ColorUIResource getPrimaryControlShadow() {
        return this.getPrimary3();
    }

    public ColorUIResource getMenuSelectedBackground() {
        return this.getPrimary1();
    }

    public ColorUIResource getMenuSelectedForeground() {
        return WHITE;
    }

    public ColorUIResource getMenuItemBackground() {
        return WHITE;
    }

    public ColorUIResource getToggleButtonCheckColor() {
        return Colors.GREEN_CHECK;
    }

    public void addCustomEntriesToTable(UIDefaults table) {
        super.addCustomEntriesToTable(table);
        Object[] uiDefaults = new Object[]{"ScrollBar.thumbHighlight", this.getPrimaryControlHighlight(), "ScrollBar.maxBumpsWidth", new Integer(22)};
        table.putDefaults(uiDefaults);
    }
}

