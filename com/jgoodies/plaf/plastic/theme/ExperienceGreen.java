/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.plastic.theme.Colors;
import com.jgoodies.plaf.plastic.theme.ExperienceBlue;
import javax.swing.plaf.ColorUIResource;

public class ExperienceGreen
extends ExperienceBlue {
    private static final ColorUIResource FOCUS = new ColorUIResource(245, 165, 16);

    public String getName() {
        return "Experience Green";
    }

    protected ColorUIResource getPrimary1() {
        return Colors.GREEN_LOW_DARK;
    }

    protected ColorUIResource getPrimary2() {
        return Colors.GREEN_LOW_MEDIUM;
    }

    protected ColorUIResource getPrimary3() {
        return Colors.GREEN_LOW_LIGHTEST;
    }

    public ColorUIResource getFocusColor() {
        return FOCUS;
    }
}

