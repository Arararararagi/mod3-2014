/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.theme.Colors;
import com.jgoodies.plaf.plastic.theme.DesertBluer;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;

public class DesertBlue
extends DesertBluer {
    private static final ColorUIResource secondary2 = new ColorUIResource(148, 144, 140);
    private static final ColorUIResource secondary3 = new ColorUIResource(211, 210, 204);

    public String getName() {
        return "Desert Blue";
    }

    protected ColorUIResource getPrimary1() {
        return Colors.GRAY_DARK;
    }

    protected ColorUIResource getPrimary2() {
        return Colors.BLUE_LOW_MEDIUM;
    }

    protected ColorUIResource getPrimary3() {
        return Colors.BLUE_LOW_LIGHTEST;
    }

    protected ColorUIResource getSecondary1() {
        return Colors.GRAY_MEDIUM;
    }

    protected ColorUIResource getSecondary2() {
        return secondary2;
    }

    protected ColorUIResource getSecondary3() {
        return secondary3;
    }

    public ColorUIResource getTitleTextColor() {
        return Colors.BLUE_MEDIUM_DARKEST;
    }

    public ColorUIResource getFocusColor() {
        return PlasticLookAndFeel.useHighContrastFocusColors ? Colors.YELLOW_FOCUS : Colors.BLUE_MEDIUM_DARK;
    }

    public ColorUIResource getPrimaryControlShadow() {
        return this.getPrimary3();
    }

    public ColorUIResource getMenuItemSelectedBackground() {
        return this.getPrimary1();
    }

    public void addCustomEntriesToTable(UIDefaults table) {
        super.addCustomEntriesToTable(table);
        Object[] uiDefaults = new Object[]{"ScrollBar.is3DEnabled", Boolean.FALSE, "ScrollBar.thumbHighlight", this.getPrimaryControlHighlight(), "ScrollBar.maxBumpsWidth", null};
        table.putDefaults(uiDefaults);
    }
}

