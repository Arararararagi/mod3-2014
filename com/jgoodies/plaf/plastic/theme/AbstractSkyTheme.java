/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.plastic.theme.Colors;
import com.jgoodies.plaf.plastic.theme.SkyBluerTahoma;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;

public abstract class AbstractSkyTheme
extends SkyBluerTahoma {
    private static final ColorUIResource secondary2 = new ColorUIResource(164, 164, 164);
    private static final ColorUIResource secondary3 = new ColorUIResource(225, 225, 225);

    protected ColorUIResource getPrimary1() {
        return Colors.GRAY_DARK;
    }

    protected ColorUIResource getPrimary2() {
        return Colors.BLUE_LOW_MEDIUM;
    }

    protected ColorUIResource getPrimary3() {
        return Colors.BLUE_LOW_LIGHTEST;
    }

    protected ColorUIResource getSecondary1() {
        return Colors.GRAY_MEDIUM;
    }

    protected ColorUIResource getSecondary2() {
        return secondary2;
    }

    protected ColorUIResource getSecondary3() {
        return secondary3;
    }

    public ColorUIResource getPrimaryControlShadow() {
        return this.getPrimary3();
    }

    public ColorUIResource getMenuItemSelectedBackground() {
        return this.getPrimary1();
    }

    public void addCustomEntriesToTable(UIDefaults table) {
        super.addCustomEntriesToTable(table);
        Object[] uiDefaults = new Object[]{"ScrollBar.maxBumpsWidth", null, "ScrollBar.thumbHighlight", this.getPrimaryControlHighlight()};
        table.putDefaults(uiDefaults);
    }
}

