/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.FontSizeHints;
import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.theme.SkyBluer;
import java.awt.Font;
import javax.swing.plaf.FontUIResource;

public class SkyBluerTahoma
extends SkyBluer {
    public String getName() {
        return "Sky Bluer - Tahoma";
    }

    protected Font getFont0() {
        FontSizeHints sizeHints = PlasticLookAndFeel.getFontSizeHints();
        return this.getFont0(sizeHints.controlFontSize());
    }

    protected Font getFont0(int size) {
        if (LookUtils.IS_OS_MAC) {
            return super.getFont0();
        }
        Font font = new Font("Tahoma", 0, size);
        return font != null ? font : new Font("Dialog", 0, size);
    }

    public FontUIResource getSubTextFont() {
        if (null == this.smallFont) {
            this.smallFont = new FontUIResource(this.getFont0(10));
        }
        return this.smallFont;
    }

    public FontUIResource getSystemTextFont() {
        return this.getFont();
    }

    public FontUIResource getUserTextFont() {
        return this.getFont();
    }
}

