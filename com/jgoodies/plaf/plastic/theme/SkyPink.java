/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.plastic.theme.AbstractSkyTheme;
import com.jgoodies.plaf.plastic.theme.Colors;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;

public class SkyPink
extends AbstractSkyTheme {
    public String getName() {
        return "Sky Pink";
    }

    protected ColorUIResource getPrimary1() {
        return Colors.PINK_LOW_DARK;
    }

    protected ColorUIResource getPrimary2() {
        return Colors.PINK_LOW_MEDIUM;
    }

    protected ColorUIResource getPrimary3() {
        return Colors.PINK_LOW_LIGHTER;
    }

    public ColorUIResource getHighlightedTextColor() {
        return this.getControlTextColor();
    }

    public void addCustomEntriesToTable(UIDefaults table) {
        super.addCustomEntriesToTable(table);
        Object[] uiDefaults = new Object[]{"ScrollBar.maxBumpsWidth", new Integer(30)};
        table.putDefaults(uiDefaults);
    }
}

