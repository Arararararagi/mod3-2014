/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.theme.AbstractSkyTheme;
import com.jgoodies.plaf.plastic.theme.Colors;
import javax.swing.plaf.ColorUIResource;

public class SkyKrupp
extends AbstractSkyTheme {
    private final ColorUIResource primary1 = new ColorUIResource(54, 54, 90);
    private final ColorUIResource primary2 = new ColorUIResource(156, 156, 178);
    private final ColorUIResource primary3 = new ColorUIResource(197, 197, 221);

    public String getName() {
        return "Sky Krupp";
    }

    protected ColorUIResource getPrimary1() {
        return this.primary1;
    }

    protected ColorUIResource getPrimary2() {
        return this.primary2;
    }

    protected ColorUIResource getPrimary3() {
        return this.primary3;
    }

    public ColorUIResource getMenuItemSelectedBackground() {
        return this.getPrimary1();
    }

    public ColorUIResource getMenuItemSelectedForeground() {
        return this.getWhite();
    }

    public ColorUIResource getMenuSelectedBackground() {
        return this.getSecondary2();
    }

    public ColorUIResource getFocusColor() {
        return PlasticLookAndFeel.useHighContrastFocusColors ? Colors.ORANGE_FOCUS : Colors.GRAY_DARK;
    }
}

