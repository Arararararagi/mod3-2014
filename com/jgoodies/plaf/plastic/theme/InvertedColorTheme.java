/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic.theme;

import com.jgoodies.plaf.FontSizeHints;
import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.PlasticTheme;
import com.jgoodies.plaf.plastic.theme.Colors;
import java.awt.Color;
import java.awt.Font;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;

public abstract class InvertedColorTheme
extends PlasticTheme {
    private final ColorUIResource softWhite = new ColorUIResource(154, 154, 154);
    private final ColorUIResource primary1 = new ColorUIResource(83, 83, 61);
    private final ColorUIResource primary2 = new ColorUIResource(115, 107, 82);
    private final ColorUIResource primary3 = new ColorUIResource(156, 156, 123);
    private final ColorUIResource secondary1 = new ColorUIResource(32, 32, 32);
    private final ColorUIResource secondary2 = new ColorUIResource(96, 96, 96);
    private final ColorUIResource secondary3 = new ColorUIResource(84, 84, 84);

    public ColorUIResource getSimpleInternalFrameBackground() {
        return this.getWhite();
    }

    public void addCustomEntriesToTable(UIDefaults table) {
        super.addCustomEntriesToTable(table);
        Object[] uiDefaults = new Object[]{"jgoodies.useControlGradient", Boolean.TRUE, "TextField.ineditableForeground", this.getSoftWhite(), "Plastic.brightenStop", new Color(255, 255, 255, 20), "Plastic.ltBrightenStop", new Color(255, 255, 255, 16)};
        table.putDefaults(uiDefaults);
    }

    protected FontUIResource getFont() {
        if (null == this.controlFont) {
            this.controlFont = new FontUIResource(this.getFont0());
        }
        return this.controlFont;
    }

    protected Font getFont0() {
        FontSizeHints sizeHints = PlasticLookAndFeel.getFontSizeHints();
        return this.getFont0(sizeHints.controlFontSize());
    }

    protected Font getFont0(int size) {
        Font font = new Font("Tahoma", 0, size);
        return font != null ? font : new Font("Dialog", 0, size);
    }

    public FontUIResource getSubTextFont() {
        if (null == this.smallFont) {
            this.smallFont = new FontUIResource(this.getFont0(10));
        }
        return this.smallFont;
    }

    public ColorUIResource getControlDisabled() {
        return this.getSoftWhite();
    }

    public ColorUIResource getControlHighlight() {
        return this.getSoftWhite();
    }

    public ColorUIResource getControlInfo() {
        return this.getWhite();
    }

    public ColorUIResource getInactiveSystemTextColor() {
        return this.getSoftWhite();
    }

    public ColorUIResource getMenuDisabledForeground() {
        return this.getSoftWhite();
    }

    public ColorUIResource getMenuItemSelectedBackground() {
        return this.getPrimary3();
    }

    public ColorUIResource getMenuItemSelectedForeground() {
        return this.getBlack();
    }

    public ColorUIResource getMenuSelectedBackground() {
        return this.getPrimary2();
    }

    public ColorUIResource getMenuSelectedForeground() {
        return this.getWhite();
    }

    protected ColorUIResource getPrimary1() {
        return this.primary1;
    }

    protected ColorUIResource getPrimary2() {
        return this.primary2;
    }

    protected ColorUIResource getPrimary3() {
        return this.primary3;
    }

    public ColorUIResource getPrimaryControlHighlight() {
        return this.getSoftWhite();
    }

    protected ColorUIResource getSecondary1() {
        return this.secondary1;
    }

    protected ColorUIResource getSecondary2() {
        return this.secondary2;
    }

    protected ColorUIResource getSecondary3() {
        return this.secondary3;
    }

    public ColorUIResource getSeparatorBackground() {
        return this.getSoftWhite();
    }

    protected ColorUIResource getSoftWhite() {
        return this.softWhite;
    }

    public ColorUIResource getTitleTextColor() {
        return this.getControlInfo();
    }

    public ColorUIResource getToggleButtonCheckColor() {
        return this.getWhite();
    }

    public ColorUIResource getFocusColor() {
        return Colors.GRAY_FOCUS;
    }
}

