/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

public final class PlasticSplitPaneDivider
extends BasicSplitPaneDivider {
    public PlasticSplitPaneDivider(BasicSplitPaneUI ui) {
        super(ui);
    }

    public void paint(Graphics g) {
        Dimension size = this.getSize();
        Color bgColor = this.getBackground();
        if (bgColor != null) {
            g.setColor(bgColor);
            g.fillRect(0, 0, size.width, size.height);
        }
        super.paint(g);
    }
}

