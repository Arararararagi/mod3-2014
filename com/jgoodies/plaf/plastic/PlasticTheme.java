/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import java.awt.Color;
import java.awt.Font;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;

public abstract class PlasticTheme
extends DefaultMetalTheme {
    public static final Color DARKEN_START = new Color(0, 0, 0, 0);
    public static final Color DARKEN_STOP = new Color(0, 0, 0, 64);
    public static final Color LT_DARKEN_STOP = new Color(0, 0, 0, 32);
    public static final Color BRIGHTEN_START = new Color(255, 255, 255, 0);
    public static final Color BRIGHTEN_STOP = new Color(255, 255, 255, 128);
    public static final Color LT_BRIGHTEN_STOP = new Color(255, 255, 255, 64);
    protected static final ColorUIResource WHITE = new ColorUIResource(255, 255, 255);
    protected static final ColorUIResource BLACK = new ColorUIResource(0, 0, 0);
    protected FontUIResource titleFont;
    protected FontUIResource controlFont;
    protected FontUIResource systemFont;
    protected FontUIResource userFont;
    protected FontUIResource smallFont;

    protected ColorUIResource getBlack() {
        return BLACK;
    }

    protected ColorUIResource getWhite() {
        return WHITE;
    }

    public ColorUIResource getSystemTextColor() {
        return this.getControlInfo();
    }

    public ColorUIResource getTitleTextColor() {
        return this.getPrimary1();
    }

    public ColorUIResource getMenuForeground() {
        return this.getControlInfo();
    }

    public ColorUIResource getMenuItemBackground() {
        return this.getMenuBackground();
    }

    public ColorUIResource getMenuItemSelectedBackground() {
        return this.getMenuSelectedBackground();
    }

    public ColorUIResource getMenuItemSelectedForeground() {
        return this.getMenuSelectedForeground();
    }

    public ColorUIResource getSimpleInternalFrameForeground() {
        return this.getWhite();
    }

    public ColorUIResource getSimpleInternalFrameBackground() {
        return this.getPrimary1();
    }

    public ColorUIResource getToggleButtonCheckColor() {
        return this.getPrimary1();
    }

    public FontUIResource getTitleTextFont() {
        if (this.titleFont == null) {
            this.titleFont = new FontUIResource(Font.getFont("swing.plaf.metal.controlFont", new Font("Dialog", 1, 12)));
        }
        return this.titleFont;
    }

    public FontUIResource getControlTextFont() {
        return this.getFont();
    }

    public FontUIResource getMenuTextFont() {
        return this.getFont();
    }

    public FontUIResource getSubTextFont() {
        if (this.smallFont == null) {
            this.smallFont = new FontUIResource(Font.getFont("swing.plaf.metal.smallFont", new Font("Dialog", 0, 10)));
        }
        return this.smallFont;
    }

    public FontUIResource getSystemTextFont() {
        if (this.systemFont == null) {
            this.systemFont = new FontUIResource(Font.getFont("swing.plaf.metal.systemFont", new Font("Dialog", 0, 12)));
        }
        return this.systemFont;
    }

    public FontUIResource getUserTextFont() {
        if (this.userFont == null) {
            this.userFont = new FontUIResource(Font.getFont("swing.plaf.metal.userFont", new Font("Dialog", 0, 12)));
        }
        return this.userFont;
    }

    public FontUIResource getWindowTitleFont() {
        return this.getFont();
    }

    protected FontUIResource getFont() {
        if (null == this.controlFont) {
            this.controlFont = new FontUIResource(this.getFont0());
        }
        return this.controlFont;
    }

    protected Font getFont0() {
        Font font = Font.getFont("swing.plaf.metal.controlFont");
        return font != null ? font.deriveFont(0) : new Font("Dialog", 0, 12);
    }
}

