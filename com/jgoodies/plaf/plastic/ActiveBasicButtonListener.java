/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import java.awt.event.MouseEvent;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.plaf.basic.BasicButtonListener;

final class ActiveBasicButtonListener
extends BasicButtonListener {
    private boolean mouseOver = false;

    ActiveBasicButtonListener(AbstractButton b) {
        super(b);
    }

    public void mouseEntered(MouseEvent e) {
        super.mouseEntered(e);
        AbstractButton button = (AbstractButton)e.getSource();
        this.mouseOver = true;
        button.getModel().setArmed(true);
    }

    public void mouseExited(MouseEvent e) {
        super.mouseExited(e);
        AbstractButton button = (AbstractButton)e.getSource();
        this.mouseOver = false;
        button.getModel().setArmed(false);
    }

    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
        AbstractButton button = (AbstractButton)e.getSource();
        button.getModel().setArmed(this.mouseOver);
    }
}

