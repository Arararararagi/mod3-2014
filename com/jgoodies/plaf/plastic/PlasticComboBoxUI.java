/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.plastic.PlasticComboBoxButton;
import com.jgoodies.plaf.plastic.PlasticComboBoxEditor;
import com.jgoodies.plaf.plastic.PlasticIconFactory;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.CellRendererPane;
import javax.swing.ComboBoxEditor;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;

public final class PlasticComboBoxUI
extends MetalComboBoxUI {
    public static ComponentUI createUI(JComponent b) {
        return new PlasticComboBoxUI();
    }

    protected ComboBoxEditor createEditor() {
        return new PlasticComboBoxEditor.UIResource();
    }

    protected ComboPopup createPopup() {
        return new PlasticComboPopup(this.comboBox);
    }

    private Insets getEditorInsets() {
        if (this.editor instanceof JComponent) {
            return ((JComponent)this.editor).getInsets();
        }
        return new Insets(0, 0, 0, 0);
    }

    private int getEditableButtonWidth() {
        return UIManager.getInt("ScrollBar.width") - 1;
    }

    public Dimension getMinimumSize(JComponent c) {
        if (!this.isMinimumSizeDirty) {
            return new Dimension(this.cachedMinimumSize);
        }
        Dimension size = null;
        if (!this.comboBox.isEditable() && this.arrowButton != null && this.arrowButton instanceof PlasticComboBoxButton) {
            PlasticComboBoxButton button = (PlasticComboBoxButton)this.arrowButton;
            Insets buttonInsets = button.getInsets();
            Insets buttonMargin = button.getMargin();
            Insets insets = this.comboBox.getInsets();
            size = this.getDisplaySize();
            size.height+=2;
            size.width+=insets.left + insets.right;
            size.width+=buttonInsets.left + buttonInsets.right;
            size.width+=buttonMargin.left + buttonMargin.right;
            size.width+=button.getComboIcon().getIconWidth();
            size.height+=insets.top + insets.bottom;
            size.height+=buttonInsets.top + buttonInsets.bottom;
        } else if (this.comboBox.isEditable() && this.arrowButton != null && this.editor != null) {
            size = this.getDisplaySize();
            Insets insets = this.comboBox.getInsets();
            Insets editorInsets = this.getEditorInsets();
            int buttonWidth = this.getEditableButtonWidth();
            size.width+=insets.left + insets.right;
            size.width+=editorInsets.left + editorInsets.right - 1;
            size.width+=buttonWidth;
            size.height+=insets.top + insets.bottom;
        } else {
            size = super.getMinimumSize(c);
        }
        this.cachedMinimumSize.setSize(size.width, size.height);
        this.isMinimumSizeDirty = false;
        return new Dimension(this.cachedMinimumSize);
    }

    protected JButton createArrowButton() {
        return new PlasticComboBoxButton(this.comboBox, PlasticIconFactory.getComboBoxButtonIcon(), this.comboBox.isEditable(), this.currentValuePane, this.listBox);
    }

    protected LayoutManager createLayoutManager() {
        return new PlasticComboBoxLayoutManager();
    }

    public PropertyChangeListener createPropertyChangeListener() {
        return new PlasticPropertyChangeListener();
    }

    private class PlasticComboBoxLayoutManager
    extends MetalComboBoxUI.MetalComboBoxLayoutManager {
        private PlasticComboBoxLayoutManager() {
            super(PlasticComboBoxUI.this);
        }

        public void layoutContainer(Container parent) {
            JComboBox cb = (JComboBox)parent;
            if (!cb.isEditable()) {
                super.layoutContainer(parent);
                return;
            }
            int width = cb.getWidth();
            int height = cb.getHeight();
            Insets insets = PlasticComboBoxUI.this.getInsets();
            int buttonWidth = PlasticComboBoxUI.this.getEditableButtonWidth();
            int buttonHeight = height - (insets.top + insets.bottom);
            if (PlasticComboBoxUI.this.arrowButton != null) {
                if (cb.getComponentOrientation().isLeftToRight()) {
                    PlasticComboBoxUI.this.arrowButton.setBounds(width - (insets.right + buttonWidth), insets.top, buttonWidth, buttonHeight);
                } else {
                    PlasticComboBoxUI.this.arrowButton.setBounds(insets.left, insets.top, buttonWidth, buttonHeight);
                }
            }
            if (PlasticComboBoxUI.this.editor != null) {
                PlasticComboBoxUI.this.editor.setBounds(PlasticComboBoxUI.this.rectangleForCurrentValue());
            }
        }
    }

    private class PlasticComboPopup
    extends MetalComboBoxUI.MetalComboPopup {
        private PlasticComboPopup(JComboBox combo) {
            super(PlasticComboBoxUI.this, combo);
        }

        protected void configureList() {
            super.configureList();
            this.list.setForeground(UIManager.getColor("MenuItem.foreground"));
            this.list.setBackground(UIManager.getColor("MenuItem.background"));
        }

        protected void configureScroller() {
            super.configureScroller();
            this.scroller.getVerticalScrollBar().putClientProperty("JScrollBar.isFreeStanding", Boolean.FALSE);
        }
    }

    private class PlasticPropertyChangeListener
    extends BasicComboBoxUI.PropertyChangeHandler {
        private PlasticPropertyChangeListener() {
            super(PlasticComboBoxUI.this);
        }

        public void propertyChange(PropertyChangeEvent e) {
            super.propertyChange(e);
            String propertyName = e.getPropertyName();
            if (propertyName.equals("editable")) {
                PlasticComboBoxButton button = (PlasticComboBoxButton)PlasticComboBoxUI.this.arrowButton;
                button.setIconOnly(PlasticComboBoxUI.this.comboBox.isEditable());
                PlasticComboBoxUI.this.comboBox.repaint();
            } else if (propertyName.equals("background")) {
                Color color = (Color)e.getNewValue();
                PlasticComboBoxUI.this.arrowButton.setBackground(color);
                PlasticComboBoxUI.this.listBox.setBackground(color);
            } else if (propertyName.equals("foreground")) {
                Color color = (Color)e.getNewValue();
                PlasticComboBoxUI.this.arrowButton.setForeground(color);
                PlasticComboBoxUI.this.listBox.setForeground(color);
            }
        }
    }

}

