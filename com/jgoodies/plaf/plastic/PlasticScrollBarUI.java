/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.plastic.PlasticArrowButton;
import com.jgoodies.plaf.plastic.PlasticBumps;
import com.jgoodies.plaf.plastic.PlasticUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalScrollBarUI;
import javax.swing.plaf.metal.MetalScrollButton;

public final class PlasticScrollBarUI
extends MetalScrollBarUI {
    private static final String PROPERTY_PREFIX = "ScrollBar.";
    public static final String MAX_BUMPS_WIDTH_KEY = "ScrollBar.maxBumpsWidth";
    private static Color shadowColor;
    private static Color highlightColor;
    private static Color darkShadowColor;
    private static Color thumbColor;
    private static Color thumbShadow;
    private static Color thumbHighlightColor;
    private PlasticBumps bumps;

    public static ComponentUI createUI(JComponent b) {
        return new PlasticScrollBarUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        this.bumps = new PlasticBumps(10, 10, thumbHighlightColor, thumbShadow, thumbColor);
    }

    protected JButton createDecreaseButton(int orientation) {
        this.decreaseButton = new PlasticArrowButton(orientation, this.scrollBarWidth, this.isFreeStanding);
        return this.decreaseButton;
    }

    protected JButton createIncreaseButton(int orientation) {
        this.increaseButton = new PlasticArrowButton(orientation, this.scrollBarWidth, this.isFreeStanding);
        return this.increaseButton;
    }

    protected void configureScrollBarColors() {
        super.configureScrollBarColors();
        shadowColor = UIManager.getColor("ScrollBar.shadow");
        highlightColor = UIManager.getColor("ScrollBar.highlight");
        darkShadowColor = UIManager.getColor("ScrollBar.darkShadow");
        thumbColor = UIManager.getColor("ScrollBar.thumb");
        thumbShadow = UIManager.getColor("ScrollBar.thumbShadow");
        thumbHighlightColor = UIManager.getColor("ScrollBar.thumbHighlight");
    }

    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        g.translate(trackBounds.x, trackBounds.y);
        boolean leftToRight = PlasticUtils.isLeftToRight(c);
        if (this.scrollbar.getOrientation() == 1) {
            if (!this.isFreeStanding) {
                if (!leftToRight) {
                    ++trackBounds.width;
                    g.translate(-1, 0);
                } else {
                    trackBounds.width+=2;
                }
            }
            if (c.isEnabled()) {
                g.setColor(darkShadowColor);
                g.drawLine(0, 0, 0, trackBounds.height - 1);
                g.drawLine(trackBounds.width - 2, 0, trackBounds.width - 2, trackBounds.height - 1);
                g.drawLine(1, trackBounds.height - 1, trackBounds.width - 1, trackBounds.height - 1);
                g.drawLine(1, 0, trackBounds.width - 2, 0);
                g.setColor(shadowColor);
                g.drawLine(1, 1, 1, trackBounds.height - 2);
                g.drawLine(1, 1, trackBounds.width - 3, 1);
                if (this.scrollbar.getValue() != this.scrollbar.getMaximum()) {
                    int y = this.thumbRect.y + this.thumbRect.height - trackBounds.y;
                    g.drawLine(1, y, trackBounds.width - 1, y);
                }
                g.setColor(highlightColor);
                g.drawLine(trackBounds.width - 1, 0, trackBounds.width - 1, trackBounds.height - 1);
            } else {
                PlasticUtils.drawDisabledBorder(g, 0, 0, trackBounds.width, trackBounds.height);
            }
            if (!this.isFreeStanding) {
                if (!leftToRight) {
                    --trackBounds.width;
                    g.translate(1, 0);
                } else {
                    trackBounds.width-=2;
                }
            }
        } else {
            if (!this.isFreeStanding) {
                trackBounds.height+=2;
            }
            if (c.isEnabled()) {
                g.setColor(darkShadowColor);
                g.drawLine(0, 0, trackBounds.width - 1, 0);
                g.drawLine(0, 1, 0, trackBounds.height - 2);
                g.drawLine(0, trackBounds.height - 2, trackBounds.width - 1, trackBounds.height - 2);
                g.drawLine(trackBounds.width - 1, 1, trackBounds.width - 1, trackBounds.height - 1);
                g.setColor(shadowColor);
                g.drawLine(1, 1, trackBounds.width - 2, 1);
                g.drawLine(1, 1, 1, trackBounds.height - 3);
                g.drawLine(0, trackBounds.height - 1, trackBounds.width - 1, trackBounds.height - 1);
                if (this.scrollbar.getValue() != this.scrollbar.getMaximum()) {
                    int x = this.thumbRect.x + this.thumbRect.width - trackBounds.x;
                    g.drawLine(x, 1, x, trackBounds.height - 1);
                }
            } else {
                PlasticUtils.drawDisabledBorder(g, 0, 0, trackBounds.width, trackBounds.height);
            }
            if (!this.isFreeStanding) {
                trackBounds.height-=2;
            }
        }
        g.translate(- trackBounds.x, - trackBounds.y);
    }

    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        if (!c.isEnabled()) {
            return;
        }
        boolean leftToRight = PlasticUtils.isLeftToRight(c);
        g.translate(thumbBounds.x, thumbBounds.y);
        if (this.scrollbar.getOrientation() == 1) {
            if (!this.isFreeStanding) {
                if (!leftToRight) {
                    ++thumbBounds.width;
                    g.translate(-1, 0);
                } else {
                    thumbBounds.width+=2;
                }
            }
            g.setColor(thumbColor);
            g.fillRect(0, 0, thumbBounds.width - 2, thumbBounds.height - 1);
            g.setColor(thumbShadow);
            g.drawRect(0, 0, thumbBounds.width - 2, thumbBounds.height - 1);
            g.setColor(thumbHighlightColor);
            g.drawLine(1, 1, thumbBounds.width - 3, 1);
            g.drawLine(1, 1, 1, thumbBounds.height - 2);
            this.paintBumps(g, c, 3, 4, thumbBounds.width - 6, thumbBounds.height - 7);
            if (!this.isFreeStanding) {
                if (!leftToRight) {
                    --thumbBounds.width;
                    g.translate(1, 0);
                } else {
                    thumbBounds.width-=2;
                }
            }
        } else {
            if (!this.isFreeStanding) {
                thumbBounds.height+=2;
            }
            g.setColor(thumbColor);
            g.fillRect(0, 0, thumbBounds.width - 1, thumbBounds.height - 2);
            g.setColor(thumbShadow);
            g.drawRect(0, 0, thumbBounds.width - 1, thumbBounds.height - 2);
            g.setColor(thumbHighlightColor);
            g.drawLine(1, 1, thumbBounds.width - 2, 1);
            g.drawLine(1, 1, 1, thumbBounds.height - 3);
            this.paintBumps(g, c, 4, 3, thumbBounds.width - 7, thumbBounds.height - 6);
            if (!this.isFreeStanding) {
                thumbBounds.height-=2;
            }
        }
        g.translate(- thumbBounds.x, - thumbBounds.y);
        if (PlasticUtils.is3D("ScrollBar.")) {
            this.paintThumb3D(g, thumbBounds);
        }
    }

    private void paintBumps(Graphics g, JComponent c, int x, int y, int width, int height) {
        if (!this.useNarrowBumps()) {
            this.bumps.setBumpArea(width, height);
            this.bumps.paintIcon(c, g, x, y);
        } else {
            int MAX_WIDTH = UIManager.getInt("ScrollBar.maxBumpsWidth");
            int myWidth = Math.min(MAX_WIDTH, width);
            int myHeight = Math.min(MAX_WIDTH, height);
            int myX = x + (width - myWidth) / 2;
            int myY = y + (height - myHeight) / 2;
            this.bumps.setBumpArea(myWidth, myHeight);
            this.bumps.paintIcon(c, g, myX, myY);
        }
    }

    private void paintThumb3D(Graphics g, Rectangle thumbBounds) {
        boolean isHorizontal = this.scrollbar.getOrientation() == 0;
        int width = thumbBounds.width - (isHorizontal ? 3 : 1);
        int height = thumbBounds.height - (isHorizontal ? 1 : 3);
        Rectangle r = new Rectangle(thumbBounds.x + 2, thumbBounds.y + 2, width, height);
        PlasticUtils.addLight3DEffekt(g, r, isHorizontal);
    }

    private boolean useNarrowBumps() {
        Object value = UIManager.get("ScrollBar.maxBumpsWidth");
        return value != null && value instanceof Integer;
    }
}

