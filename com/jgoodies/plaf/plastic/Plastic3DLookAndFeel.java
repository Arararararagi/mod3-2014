/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.plastic.PlasticBorders;
import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import javax.swing.UIDefaults;
import javax.swing.border.Border;

public class Plastic3DLookAndFeel
extends PlasticLookAndFeel {
    public Plastic3DLookAndFeel() {
        if (null == Plastic3DLookAndFeel.getMyCurrentTheme()) {
            Plastic3DLookAndFeel.setMyCurrentTheme(Plastic3DLookAndFeel.createMyDefaultTheme());
        }
    }

    public String getID() {
        return "JGoodies Plastic 3D";
    }

    public String getName() {
        return "JGoodies Plastic 3D";
    }

    public String getDescription() {
        return "The JGoodies Plastic 3D Look and Feel - \u00a9 2003 JGoodies Karsten Lentzsch";
    }

    protected boolean is3DEnabled() {
        return true;
    }

    protected void initComponentDefaults(UIDefaults table) {
        super.initComponentDefaults(table);
        Border menuBarBorder = PlasticBorders.getThinRaisedBorder();
        Border toolBarBorder = PlasticBorders.getThinRaisedBorder();
        Object[] defaults = new Object[]{"MenuBar.border", menuBarBorder, "ToolBar.border", toolBarBorder};
        table.putDefaults(defaults);
    }

    protected static void installDefaultThemes() {
    }
}

