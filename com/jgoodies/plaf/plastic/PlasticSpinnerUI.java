/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.common.ExtBasicArrowButtonHandler;
import com.jgoodies.plaf.common.ExtBasicSpinnerLayout;
import com.jgoodies.plaf.plastic.PlasticArrowButton;
import java.awt.Component;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.TextUI;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.basic.BasicSpinnerUI;

public final class PlasticSpinnerUI
extends BasicSpinnerUI {
    private static final Border MARGIN_BORDER = new BorderUIResource(new BasicBorders.MarginBorder());
    private static final ExtBasicArrowButtonHandler nextButtonHandler = new ExtBasicArrowButtonHandler("increment", true);
    private static final ExtBasicArrowButtonHandler previousButtonHandler = new ExtBasicArrowButtonHandler("decrement", false);

    public static ComponentUI createUI(JComponent b) {
        return new PlasticSpinnerUI();
    }

    protected Component createPreviousButton() {
        int width = UIManager.getInt("ScrollBar.width");
        PlasticArrowButton b = new PlasticArrowButton(5, width, true);
        b.addActionListener(previousButtonHandler);
        b.addMouseListener(previousButtonHandler);
        return b;
    }

    protected Component createNextButton() {
        int width = UIManager.getInt("ScrollBar.width");
        PlasticArrowButton b = new PlasticArrowButton(1, width, true);
        b.addActionListener(nextButtonHandler);
        b.addMouseListener(nextButtonHandler);
        return b;
    }

    protected LayoutManager createLayout() {
        return new ExtBasicSpinnerLayout();
    }

    protected JComponent createEditor() {
        JComponent editor = this.spinner.getEditor();
        this.configureEditor(editor);
        return editor;
    }

    protected void replaceEditor(JComponent oldEditor, JComponent newEditor) {
        this.spinner.remove(oldEditor);
        this.configureEditor(newEditor);
        this.spinner.add((Component)newEditor, "Editor");
    }

    private void configureEditor(JComponent editor) {
        if (editor instanceof JSpinner.DefaultEditor) {
            JSpinner.DefaultEditor defaultEditor = (JSpinner.DefaultEditor)editor;
            defaultEditor.getTextField().getUI();
            defaultEditor.getTextField().setBorder(MARGIN_BORDER);
            Insets insets = UIManager.getInsets("Spinner.defaultEditorInsets");
            defaultEditor.getTextField().setMargin(insets);
        }
    }
}

