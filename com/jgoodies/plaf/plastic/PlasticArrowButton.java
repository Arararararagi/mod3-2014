/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.plastic.PlasticUtils;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.ButtonModel;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.MetalScrollButton;

final class PlasticArrowButton
extends MetalScrollButton {
    private static Color shadowColor;
    private static Color highlightColor;
    private boolean isFreeStanding;

    public PlasticArrowButton(int direction, int width, boolean freeStanding) {
        super(direction, width, freeStanding);
        shadowColor = UIManager.getColor("ScrollBar.darkShadow");
        highlightColor = UIManager.getColor("ScrollBar.highlight");
        this.isFreeStanding = freeStanding;
    }

    public void setFreeStanding(boolean freeStanding) {
        super.setFreeStanding(freeStanding);
        this.isFreeStanding = freeStanding;
    }

    public void paint(Graphics g) {
        boolean leftToRight = PlasticUtils.isLeftToRight(this);
        boolean isEnabled = this.getParent().isEnabled();
        boolean isPressed = this.getModel().isPressed();
        ColorUIResource arrowColor = isEnabled ? MetalLookAndFeel.getControlInfo() : MetalLookAndFeel.getControlDisabled();
        int width = this.getWidth();
        int height = this.getHeight();
        int w = width;
        int h = height;
        int arrowHeight = (height + 1) / 4;
        g.setColor(isPressed ? MetalLookAndFeel.getControlShadow() : this.getBackground());
        g.fillRect(0, 0, width, height);
        if (this.getDirection() == 1) {
            this.paintNorth(g, leftToRight, isEnabled, arrowColor, isPressed, width, height, w, h, arrowHeight);
        } else if (this.getDirection() == 5) {
            this.paintSouth(g, leftToRight, isEnabled, arrowColor, isPressed, width, height, w, h, arrowHeight);
        } else if (this.getDirection() == 3) {
            this.paintEast(g, isEnabled, arrowColor, isPressed, width, height, w, h, arrowHeight);
        } else if (this.getDirection() == 7) {
            this.paintWest(g, isEnabled, arrowColor, isPressed, width, height, w, h, arrowHeight);
        }
        if (PlasticUtils.is3D("ScrollBar.")) {
            this.paint3D(g);
        }
    }

    private void paintWest(Graphics g, boolean isEnabled, Color arrowColor, boolean isPressed, int width, int height, int w, int h, int arrowHeight) {
        if (!this.isFreeStanding) {
            height+=2;
            ++width;
            g.translate(-1, 0);
        }
        g.setColor(arrowColor);
        int startX = (w + 1 - arrowHeight) / 2;
        int startY = h / 2;
        for (int line = 0; line < arrowHeight; ++line) {
            g.drawLine(startX + line, startY - line, startX + line, startY + line + 1);
        }
        if (isEnabled) {
            g.setColor(highlightColor);
            if (!isPressed) {
                g.drawLine(1, 1, width - 1, 1);
                g.drawLine(1, 1, 1, height - 3);
            }
            g.drawLine(1, height - 1, width - 1, height - 1);
            g.setColor(shadowColor);
            g.drawLine(0, 0, width - 1, 0);
            g.drawLine(0, 0, 0, height - 2);
            g.drawLine(2, height - 2, width - 1, height - 2);
        } else {
            PlasticUtils.drawDisabledBorder(g, 0, 0, width + 1, height);
        }
        if (!this.isFreeStanding) {
            height-=2;
            --width;
            g.translate(1, 0);
        }
    }

    private void paintEast(Graphics g, boolean isEnabled, Color arrowColor, boolean isPressed, int width, int height, int w, int h, int arrowHeight) {
        if (!this.isFreeStanding) {
            height+=2;
            ++width;
        }
        g.setColor(arrowColor);
        int startX = (w + 1 - arrowHeight) / 2 + arrowHeight - 1;
        int startY = h / 2;
        for (int line = 0; line < arrowHeight; ++line) {
            g.drawLine(startX - line, startY - line, startX - line, startY + line + 1);
        }
        if (isEnabled) {
            g.setColor(highlightColor);
            if (!isPressed) {
                g.drawLine(0, 1, width - 3, 1);
                g.drawLine(0, 1, 0, height - 3);
            }
            g.drawLine(width - 1, 1, width - 1, height - 1);
            g.drawLine(0, height - 1, width - 1, height - 1);
            g.setColor(shadowColor);
            g.drawLine(0, 0, width - 2, 0);
            g.drawLine(width - 2, 1, width - 2, height - 2);
            g.drawLine(0, height - 2, width - 2, height - 2);
        } else {
            PlasticUtils.drawDisabledBorder(g, -1, 0, width + 1, height);
        }
        if (!this.isFreeStanding) {
            height-=2;
            --width;
        }
    }

    private void paintSouth(Graphics g, boolean leftToRight, boolean isEnabled, Color arrowColor, boolean isPressed, int width, int height, int w, int h, int arrowHeight) {
        if (!this.isFreeStanding) {
            ++height;
            if (!leftToRight) {
                ++width;
                g.translate(-1, 0);
            } else {
                width+=2;
            }
        }
        g.setColor(arrowColor);
        int startY = (h + 1 - arrowHeight) / 2 + arrowHeight - 1;
        int startX = w / 2;
        for (int line = 0; line < arrowHeight; ++line) {
            g.drawLine(startX - line, startY - line, startX + line + 1, startY - line);
        }
        if (isEnabled) {
            g.setColor(highlightColor);
            if (!isPressed) {
                g.drawLine(1, 0, width - 3, 0);
                g.drawLine(1, 0, 1, height - 3);
            }
            g.drawLine(1, height - 1, width - 1, height - 1);
            g.drawLine(width - 1, 0, width - 1, height - 1);
            g.setColor(shadowColor);
            g.drawLine(0, 0, 0, height - 2);
            g.drawLine(width - 2, 0, width - 2, height - 2);
            g.drawLine(1, height - 2, width - 2, height - 2);
        } else {
            PlasticUtils.drawDisabledBorder(g, 0, -1, width, height + 1);
        }
        if (!this.isFreeStanding) {
            --height;
            if (!leftToRight) {
                --width;
                g.translate(1, 0);
            } else {
                width-=2;
            }
        }
    }

    private void paintNorth(Graphics g, boolean leftToRight, boolean isEnabled, Color arrowColor, boolean isPressed, int width, int height, int w, int h, int arrowHeight) {
        if (!this.isFreeStanding) {
            ++height;
            g.translate(0, -1);
            if (!leftToRight) {
                ++width;
                g.translate(-1, 0);
            } else {
                width+=2;
            }
        }
        g.setColor(arrowColor);
        int startY = (h + 1 - arrowHeight) / 2;
        int startX = w / 2;
        for (int line = 0; line < arrowHeight; ++line) {
            g.drawLine(startX - line, startY + line, startX + line + 1, startY + line);
        }
        if (isEnabled) {
            g.setColor(highlightColor);
            if (!isPressed) {
                g.drawLine(1, 1, width - 3, 1);
                g.drawLine(1, 1, 1, height - 1);
            }
            g.drawLine(width - 1, 1, width - 1, height - 1);
            g.setColor(shadowColor);
            g.drawLine(0, 0, width - 2, 0);
            g.drawLine(0, 0, 0, height - 1);
            g.drawLine(width - 2, 2, width - 2, height - 1);
        } else {
            PlasticUtils.drawDisabledBorder(g, 0, 0, width, height + 1);
        }
        if (!this.isFreeStanding) {
            --height;
            g.translate(0, 1);
            if (!leftToRight) {
                --width;
                g.translate(1, 0);
            } else {
                width-=2;
            }
        }
    }

    private void paint3D(Graphics g) {
        ButtonModel buttonModel = this.getModel();
        if (buttonModel.isArmed() && buttonModel.isPressed() || buttonModel.isSelected()) {
            return;
        }
        int width = this.getWidth();
        int height = this.getHeight();
        if (this.getDirection() == 3) {
            width-=2;
        } else if (this.getDirection() == 5) {
            height-=2;
        }
        Rectangle r = new Rectangle(1, 1, width, height);
        boolean isHorizontal = this.getDirection() == 3 || this.getDirection() == 7;
        PlasticUtils.addLight3DEffekt(g, r, isHorizontal);
    }
}

