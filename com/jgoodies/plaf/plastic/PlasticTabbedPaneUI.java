/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.clearlook.ClearLookManager;
import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.Options;
import com.jgoodies.plaf.plastic.PlasticUtils;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.plaf.metal.MetalTabbedPaneUI;

public final class PlasticTabbedPaneUI
extends MetalTabbedPaneUI {
    public static final String MARK_CONTENT_BORDERS_KEY = "markContentBorders";
    private static final boolean MARK_CONTENT_BORDERS = LookUtils.getSystemProperty("markContentBorders", "").equalsIgnoreCase("true");
    private static boolean isTabIconsEnabled = Options.isTabIconsEnabled();
    private static Color MARK_CONTENT_BORDER_COLOR = Color.magenta;
    private Boolean noContentBorder;
    private Boolean embeddedTabs;
    private boolean clearLookSuggestsNoContentBorder = false;
    private AbstractRenderer renderer;

    public static ComponentUI createUI(JComponent tabPane) {
        return new PlasticTabbedPaneUI();
    }

    public void installUI(JComponent c) {
        super.installUI(c);
        this.embeddedTabs = (Boolean)c.getClientProperty("jgoodies.embeddedTabs");
        this.noContentBorder = (Boolean)c.getClientProperty("jgoodies.noContentBorder");
        this.renderer = this.createRenderer(this.tabPane);
    }

    public void uninstallUI(JComponent c) {
        this.renderer = null;
        super.uninstallUI(c);
    }

    private boolean hasNoContentBorder() {
        return this.noContentBorder == null ? this.clearLookSuggestsNoContentBorder() : this.noContentBorder.booleanValue();
    }

    private boolean hasEmbeddedTabs() {
        return this.embeddedTabs == null ? false : this.embeddedTabs;
    }

    private boolean clearLookSuggestsNoContentBorder() {
        return this.clearLookSuggestsNoContentBorder;
    }

    private AbstractRenderer createRenderer(JTabbedPane tabbedPane) {
        return this.hasEmbeddedTabs() ? AbstractRenderer.createEmbeddedRenderer(tabbedPane) : AbstractRenderer.createRenderer(this.tabPane);
    }

    private void checkBorderReplacement(JTabbedPane tabbedPane) {
        Border newBorder = ClearLookManager.replaceBorder(tabbedPane);
        Container parent = tabbedPane.getParent();
        if (parent != null && parent instanceof JSplitPane) {
            newBorder = null;
        }
        this.clearLookSuggestsNoContentBorder = newBorder != null;
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return new MyPropertyChangeHandler();
    }

    protected ChangeListener createChangeListener() {
        return new TabSelectionHandler();
    }

    private void doLayout() {
        TabbedPaneLayout layout = (TabbedPaneLayout)this.tabPane.getLayout();
        layout.calculateLayoutInfo();
        this.tabPane.repaint();
    }

    private void tabPlacementChanged() {
        this.renderer = this.createRenderer(this.tabPane);
        this.doLayout();
    }

    private void embeddedTabsPropertyChanged(Boolean newValue) {
        this.embeddedTabs = newValue;
        this.renderer = this.createRenderer(this.tabPane);
        this.doLayout();
    }

    private void noContentBorderPropertyChanged(Boolean newValue) {
        this.noContentBorder = newValue;
        this.tabPane.repaint();
    }

    private void ensureCurrentLayout() {
        if (!this.tabPane.isValid()) {
            this.tabPane.validate();
        }
        if (!this.tabPane.isValid()) {
            TabbedPaneLayout layout = (TabbedPaneLayout)this.tabPane.getLayout();
            layout.calculateLayoutInfo();
        }
    }

    public void paint(Graphics g, JComponent c) {
        int selectedIndex = this.tabPane.getSelectedIndex();
        int tabPlacement = this.tabPane.getTabPlacement();
        int tabCount = this.tabPane.getTabCount();
        this.ensureCurrentLayout();
        Rectangle iconRect = new Rectangle();
        Rectangle textRect = new Rectangle();
        Rectangle clipRect = g.getClipBounds();
        for (int i = this.runCount - 1; i >= 0; --i) {
            int start = this.tabRuns[i];
            int next = this.tabRuns[i == this.runCount - 1 ? 0 : i + 1];
            for (int j = end = next != 0 ? next - 1 : tabCount - 1; j >= start; --j) {
                if (!this.rects[j].intersects(clipRect)) continue;
                this.paintTab(g, tabPlacement, this.rects, j, iconRect, textRect);
            }
        }
        if (selectedIndex >= 0) {
            if (selectedIndex >= this.rects.length) {
                System.out.println("Caution");
            }
            if (this.rects[selectedIndex].intersects(clipRect)) {
                this.paintTab(g, tabPlacement, this.rects, selectedIndex, iconRect, textRect);
            }
        }
        this.paintContentBorder(g, tabPlacement, selectedIndex);
    }

    protected void layoutLabel(int tabPlacement, FontMetrics metrics, int tabIndex, String title, Icon icon, Rectangle tabRect, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        iconRect.y = 0;
        iconRect.x = 0;
        textRect.y = 0;
        textRect.x = 0;
        Rectangle calcRectangle = new Rectangle(tabRect);
        if (isSelected) {
            Insets calcInsets = this.getSelectedTabPadInsets(tabPlacement);
            calcRectangle.x+=calcInsets.left;
            calcRectangle.y+=calcInsets.top;
            calcRectangle.width-=calcInsets.left + calcInsets.right;
            calcRectangle.height-=calcInsets.bottom + calcInsets.top;
        }
        int xNudge = this.getTabLabelShiftX(tabPlacement, tabIndex, isSelected);
        int yNudge = this.getTabLabelShiftY(tabPlacement, tabIndex, isSelected);
        if (!(tabPlacement != 4 && tabPlacement != 2 || icon == null || title == null || title.equals(""))) {
            SwingUtilities.layoutCompoundLabel(this.tabPane, metrics, title, icon, 0, 2, 0, 11, calcRectangle, iconRect, textRect, this.textIconGap);
            xNudge+=4;
        } else {
            SwingUtilities.layoutCompoundLabel(this.tabPane, metrics, title, icon, 0, 0, 0, 11, calcRectangle, iconRect, textRect, this.textIconGap);
            iconRect.y+=calcRectangle.height % 2;
        }
        iconRect.x+=xNudge;
        iconRect.y+=yNudge;
        textRect.x+=xNudge;
        textRect.y+=yNudge;
    }

    protected Icon getIconForTab(int tabIndex) {
        String title = this.tabPane.getTitleAt(tabIndex);
        boolean hasTitle = title != null && title.length() > 0;
        return !isTabIconsEnabled && hasTitle ? null : super.getIconForTab(tabIndex);
    }

    protected LayoutManager createLayoutManager() {
        return new TabbedPaneLayout();
    }

    protected boolean isTabInFirstRun(int tabIndex) {
        return this.getRunForTab(this.tabPane.getTabCount(), tabIndex) == 0;
    }

    protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex) {
        int width = this.tabPane.getWidth();
        int height = this.tabPane.getHeight();
        Insets insets = this.tabPane.getInsets();
        int x = insets.left;
        int y = insets.top;
        int w = width - insets.right - insets.left;
        int h = height - insets.top - insets.bottom;
        switch (tabPlacement) {
            case 2: {
                w-=(x+=this.calculateTabAreaWidth(tabPlacement, this.runCount, this.maxTabWidth)) - insets.left;
                break;
            }
            case 4: {
                w-=this.calculateTabAreaWidth(tabPlacement, this.runCount, this.maxTabWidth);
                break;
            }
            case 3: {
                h-=this.calculateTabAreaHeight(tabPlacement, this.runCount, this.maxTabHeight);
                break;
            }
            default: {
                h-=(y+=this.calculateTabAreaHeight(tabPlacement, this.runCount, this.maxTabHeight)) - insets.top;
            }
        }
        g.setColor(this.selectColor == null ? this.tabPane.getBackground() : this.selectColor);
        g.fillRect(x, y, w, h);
        Rectangle selRect = selectedIndex < 0 ? null : this.getTabBounds(this.tabPane, selectedIndex);
        boolean drawBroken = selectedIndex >= 0 && this.isTabInFirstRun(selectedIndex);
        boolean isContentBorderPainted = !this.hasNoContentBorder();
        this.renderer.paintContentBorderTopEdge(g, x, y, w, h, drawBroken, selRect, isContentBorderPainted);
        this.renderer.paintContentBorderLeftEdge(g, x, y, w, h, drawBroken, selRect, isContentBorderPainted);
        this.renderer.paintContentBorderBottomEdge(g, x, y, w, h, drawBroken, selRect, isContentBorderPainted);
        this.renderer.paintContentBorderRightEdge(g, x, y, w, h, drawBroken, selRect, isContentBorderPainted);
    }

    protected Insets getContentBorderInsets(int tabPlacement) {
        return this.renderer.getContentBorderInsets(super.getContentBorderInsets(tabPlacement));
    }

    protected Insets getTabAreaInsets(int tabPlacement) {
        return this.renderer.getTabAreaInsets(super.getTabAreaInsets(tabPlacement));
    }

    protected int getTabLabelShiftX(int tabPlacement, int tabIndex, boolean isSelected) {
        return this.renderer.getTabLabelShiftX(tabIndex, isSelected);
    }

    protected int getTabLabelShiftY(int tabPlacement, int tabIndex, boolean isSelected) {
        return this.renderer.getTabLabelShiftY(tabIndex, isSelected);
    }

    protected int getTabRunOverlay(int tabPlacement) {
        return this.renderer.getTabRunOverlay(this.tabRunOverlay);
    }

    protected boolean shouldPadTabRun(int tabPlacement, int run) {
        return this.renderer.shouldPadTabRun(run, super.shouldPadTabRun(tabPlacement, run));
    }

    protected int getTabRunIndent(int tabPlacement, int run) {
        return this.renderer.getTabRunIndent(run);
    }

    protected Insets getTabInsets(int tabPlacement, int tabIndex) {
        return this.renderer.getTabInsets(tabIndex, this.tabInsets);
    }

    protected Insets getSelectedTabPadInsets(int tabPlacement) {
        return this.renderer.getSelectedTabPadInsets();
    }

    protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rectangles, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        this.renderer.paintFocusIndicator(g, rectangles, tabIndex, iconRect, textRect, isSelected);
    }

    protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
        this.renderer.paintTabBackground(g, tabIndex, x, y, w, h, isSelected);
    }

    protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
        this.renderer.paintTabBorder(g, tabIndex, x, y, w, h, isSelected);
    }

    protected boolean shouldRotateTabRuns(int tabPlacement) {
        return false;
    }

    static /* synthetic */ Rectangle[] access$2700(PlasticTabbedPaneUI x0) {
        return x0.rects;
    }

    static /* synthetic */ Rectangle[] access$2800(PlasticTabbedPaneUI x0) {
        return x0.rects;
    }

    static /* synthetic */ int[] access$2900(PlasticTabbedPaneUI x0) {
        return x0.tabRuns;
    }

    static /* synthetic */ int[] access$3800(PlasticTabbedPaneUI x0) {
        return x0.tabRuns;
    }

    static /* synthetic */ int access$3900(PlasticTabbedPaneUI x0) {
        return x0.runCount;
    }

    static /* synthetic */ Rectangle[] access$4300(PlasticTabbedPaneUI x0) {
        return x0.rects;
    }

    static /* synthetic */ Rectangle[] access$4400(PlasticTabbedPaneUI x0) {
        return x0.rects;
    }

    static /* synthetic */ int[] access$4500(PlasticTabbedPaneUI x0) {
        return x0.tabRuns;
    }

    static /* synthetic */ int[] access$5400(PlasticTabbedPaneUI x0) {
        return x0.tabRuns;
    }

    static /* synthetic */ int access$5500(PlasticTabbedPaneUI x0) {
        return x0.runCount;
    }

    static /* synthetic */ int access$6300(PlasticTabbedPaneUI x0) {
        return x0.runCount;
    }

    static /* synthetic */ Rectangle[] access$7300(PlasticTabbedPaneUI x0) {
        return x0.rects;
    }

    static /* synthetic */ Rectangle[] access$7400(PlasticTabbedPaneUI x0) {
        return x0.rects;
    }

    static /* synthetic */ Rectangle[] access$7500(PlasticTabbedPaneUI x0) {
        return x0.rects;
    }

    private static abstract class AbstractRenderer {
        protected static final Insets EMPTY_INSETS = new Insets(0, 0, 0, 0);
        protected static final Insets NORTH_INSETS = new Insets(1, 0, 0, 0);
        protected static final Insets WEST_INSETS = new Insets(0, 1, 0, 0);
        protected static final Insets SOUTH_INSETS = new Insets(0, 0, 1, 0);
        protected static final Insets EAST_INSETS = new Insets(0, 0, 0, 1);
        protected final JTabbedPane tabPane;
        protected final int tabPlacement;
        protected Color shadowColor;
        protected Color darkShadow;
        protected Color selectColor;
        protected Color selectLight;
        protected Color selectHighlight;
        protected Color lightHighlight;
        protected Color focus;

        private AbstractRenderer(JTabbedPane tabPane) {
            this.initColors();
            this.tabPane = tabPane;
            this.tabPlacement = tabPane.getTabPlacement();
        }

        private static AbstractRenderer createRenderer(JTabbedPane tabPane) {
            switch (tabPane.getTabPlacement()) {
                case 1: {
                    return new TopRenderer(tabPane);
                }
                case 3: {
                    return new BottomRenderer(tabPane);
                }
                case 2: {
                    return new LeftRenderer(tabPane);
                }
                case 4: {
                    return new RightRenderer(tabPane);
                }
            }
            return new TopRenderer(tabPane);
        }

        private static AbstractRenderer createEmbeddedRenderer(JTabbedPane tabPane) {
            switch (tabPane.getTabPlacement()) {
                case 1: {
                    return new TopEmbeddedRenderer(tabPane);
                }
                case 3: {
                    return new BottomEmbeddedRenderer(tabPane);
                }
                case 2: {
                    return new LeftEmbeddedRenderer(tabPane);
                }
                case 4: {
                    return new RightEmbeddedRenderer(tabPane);
                }
            }
            return new TopEmbeddedRenderer(tabPane);
        }

        private void initColors() {
            this.shadowColor = UIManager.getColor("TabbedPane.shadow");
            this.darkShadow = UIManager.getColor("TabbedPane.darkShadow");
            this.selectColor = UIManager.getColor("TabbedPane.selected");
            this.focus = UIManager.getColor("TabbedPane.focus");
            this.selectHighlight = UIManager.getColor("TabbedPane.selectHighlight");
            this.lightHighlight = UIManager.getColor("TabbedPane.highlight");
            this.selectLight = new Color((2 * this.selectColor.getRed() + this.selectHighlight.getRed()) / 3, (2 * this.selectColor.getGreen() + this.selectHighlight.getGreen()) / 3, (2 * this.selectColor.getBlue() + this.selectHighlight.getBlue()) / 3);
        }

        protected boolean isFirstDisplayedTab(int tabIndex, int position, int paneBorder) {
            return tabIndex == 0;
        }

        protected Insets getTabAreaInsets(Insets defaultInsets) {
            return defaultInsets;
        }

        protected Insets getContentBorderInsets(Insets defaultInsets) {
            return defaultInsets;
        }

        protected int getTabLabelShiftX(int tabIndex, boolean isSelected) {
            return 0;
        }

        protected int getTabLabelShiftY(int tabIndex, boolean isSelected) {
            return 0;
        }

        protected int getTabRunOverlay(int tabRunOverlay) {
            return tabRunOverlay;
        }

        protected boolean shouldPadTabRun(int run, boolean aPriori) {
            return aPriori;
        }

        protected int getTabRunIndent(int run) {
            return 0;
        }

        protected abstract Insets getTabInsets(int var1, Insets var2);

        protected abstract void paintFocusIndicator(Graphics var1, Rectangle[] var2, int var3, Rectangle var4, Rectangle var5, boolean var6);

        protected abstract void paintTabBackground(Graphics var1, int var2, int var3, int var4, int var5, int var6, boolean var7);

        protected abstract void paintTabBorder(Graphics var1, int var2, int var3, int var4, int var5, int var6, boolean var7);

        protected Insets getSelectedTabPadInsets() {
            return EMPTY_INSETS;
        }

        protected void paintContentBorderTopEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            if (isContentBorderPainted) {
                g.setColor(MARK_CONTENT_BORDERS ? MARK_CONTENT_BORDER_COLOR : this.selectHighlight);
                g.fillRect(x, y, w - 1, 1);
            }
        }

        protected void paintContentBorderBottomEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            if (isContentBorderPainted) {
                g.setColor(MARK_CONTENT_BORDERS ? MARK_CONTENT_BORDER_COLOR : this.darkShadow);
                g.fillRect(x, y + h - 1, w - 1, 1);
            }
        }

        protected void paintContentBorderLeftEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            if (isContentBorderPainted) {
                g.setColor(MARK_CONTENT_BORDERS ? MARK_CONTENT_BORDER_COLOR : this.selectHighlight);
                g.fillRect(x, y, 1, h - 1);
            }
        }

        protected void paintContentBorderRightEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            if (isContentBorderPainted) {
                g.setColor(MARK_CONTENT_BORDERS ? MARK_CONTENT_BORDER_COLOR : this.darkShadow);
                g.fillRect(x + w - 1, y, 1, h);
            }
        }
    }

    private static class BottomEmbeddedRenderer
    extends AbstractRenderer {
        private BottomEmbeddedRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected Insets getTabAreaInsets(Insets insets) {
            return EMPTY_INSETS;
        }

        protected Insets getContentBorderInsets(Insets defaultInsets) {
            return SOUTH_INSETS;
        }

        protected Insets getSelectedTabPadInsets() {
            return EMPTY_INSETS;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top, tabInsets.left, tabInsets.bottom, tabInsets.right);
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            g.setColor(this.selectColor);
            g.fillRect(x, y, w + 1, h);
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            int bottom = h;
            int right = w + 1;
            g.translate(x, y);
            if (this.isFirstDisplayedTab(tabIndex, x, this.tabPane.getBounds().x)) {
                if (isSelected) {
                    g.setColor(this.shadowColor);
                    g.fillRect(right, 0, 1, bottom - 1);
                    g.fillRect(right - 1, bottom - 1, 1, 1);
                    g.setColor(this.selectHighlight);
                    g.fillRect(0, 0, 1, bottom);
                    g.fillRect(right - 1, 0, 1, bottom - 1);
                    g.fillRect(1, bottom - 1, right - 2, 1);
                }
            } else if (isSelected) {
                g.setColor(this.shadowColor);
                g.fillRect(0, 0, 1, bottom - 1);
                g.fillRect(1, bottom - 1, 1, 1);
                g.fillRect(right, 0, 1, bottom - 1);
                g.fillRect(right - 1, bottom - 1, 1, 1);
                g.setColor(this.selectHighlight);
                g.fillRect(1, 0, 1, bottom - 1);
                g.fillRect(right - 1, 0, 1, bottom - 1);
                g.fillRect(2, bottom - 1, right - 3, 1);
            } else {
                g.setColor(this.shadowColor);
                g.fillRect(1, h / 2, 1, h - h / 2);
            }
            g.translate(- x, - y);
        }

        protected void paintContentBorderBottomEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            g.setColor(this.shadowColor);
            g.fillRect(x, y + h - 1, w, 1);
        }
    }

    private static final class BottomRenderer
    extends AbstractRenderer {
        private BottomRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected Insets getTabAreaInsets(Insets defaultInsets) {
            return new Insets(defaultInsets.top, defaultInsets.left + 5, defaultInsets.bottom, defaultInsets.right);
        }

        protected int getTabLabelShiftY(int tabIndex, boolean isSelected) {
            return isSelected ? 0 : -1;
        }

        protected int getTabRunOverlay(int tabRunOverlay) {
            return tabRunOverlay - 2;
        }

        protected int getTabRunIndent(int run) {
            return 6 * run;
        }

        protected Insets getSelectedTabPadInsets() {
            return SOUTH_INSETS;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top, tabInsets.left - 2, tabInsets.bottom, tabInsets.right - 2);
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
            if (!(this.tabPane.hasFocus() && isSelected)) {
                return;
            }
            Rectangle tabRect = rects[tabIndex];
            int top = tabRect.y;
            int left = tabRect.x + 6;
            int height = tabRect.height - 3;
            int width = tabRect.width - 12;
            g.setColor(this.focus);
            g.drawRect(left, top, width, height);
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            g.setColor(this.selectColor);
            g.fillRect(x, y, w, h);
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            int bottom = h - 1;
            int right = w + 4;
            g.translate(x - 3, y);
            g.setColor(this.selectHighlight);
            g.fillRect(0, 0, 1, 2);
            g.drawLine(0, 2, 4, bottom - 4);
            g.fillRect(5, bottom - 3, 1, 2);
            g.fillRect(6, bottom - 1, 1, 1);
            g.fillRect(7, bottom, 1, 1);
            g.setColor(this.darkShadow);
            g.fillRect(8, bottom, right - 13, 1);
            g.drawLine(right + 1, 0, right - 3, bottom - 4);
            g.fillRect(right - 4, bottom - 3, 1, 2);
            g.fillRect(right - 5, bottom - 1, 1, 1);
            g.translate(- x + 3, - y);
        }

        protected void paintContentBorderBottomEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            int bottom = y + h - 1;
            int right = x + w - 1;
            g.translate(x, bottom);
            if (drawBroken && selRect.x >= x && selRect.x <= x + w) {
                g.setColor(this.darkShadow);
                g.fillRect(0, 0, selRect.x - x - 2, 1);
                if (selRect.x + selRect.width < x + w - 2) {
                    g.setColor(this.darkShadow);
                    g.fillRect(selRect.x + selRect.width + 2 - x, 0, right - selRect.x - selRect.width - 2, 1);
                }
            } else {
                g.setColor(this.darkShadow);
                g.fillRect(0, 0, w - 1, 1);
            }
            g.translate(- x, - bottom);
        }
    }

    private static class LeftEmbeddedRenderer
    extends AbstractRenderer {
        private LeftEmbeddedRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected Insets getTabAreaInsets(Insets insets) {
            return EMPTY_INSETS;
        }

        protected Insets getContentBorderInsets(Insets defaultInsets) {
            return WEST_INSETS;
        }

        protected int getTabRunOverlay(int tabRunOverlay) {
            return 0;
        }

        protected boolean shouldPadTabRun(int run, boolean aPriori) {
            return false;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top, tabInsets.left, tabInsets.bottom, tabInsets.right);
        }

        protected Insets getSelectedTabPadInsets() {
            return EMPTY_INSETS;
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            g.setColor(this.selectColor);
            g.fillRect(x, y, w, h);
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            int bottom = h;
            int right = w;
            g.translate(x, y);
            if (this.isFirstDisplayedTab(tabIndex, y, this.tabPane.getBounds().y)) {
                if (isSelected) {
                    g.setColor(this.selectHighlight);
                    g.fillRect(0, 0, right, 1);
                    g.fillRect(0, 0, 1, bottom - 1);
                    g.fillRect(1, bottom - 1, right - 1, 1);
                    g.setColor(this.shadowColor);
                    g.fillRect(0, bottom - 1, 1, 1);
                    g.fillRect(1, bottom, right - 1, 1);
                }
            } else if (isSelected) {
                g.setColor(this.selectHighlight);
                g.fillRect(1, 1, right - 1, 1);
                g.fillRect(0, 2, 1, bottom - 2);
                g.fillRect(1, bottom - 1, right - 1, 1);
                g.setColor(this.shadowColor);
                g.fillRect(1, 0, right - 1, 1);
                g.fillRect(0, 1, 1, 1);
                g.fillRect(0, bottom - 1, 1, 1);
                g.fillRect(1, bottom, right - 1, 1);
            } else {
                g.setColor(this.shadowColor);
                g.fillRect(0, 0, right / 3, 1);
            }
            g.translate(- x, - y);
        }

        protected void paintContentBorderLeftEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            g.setColor(this.shadowColor);
            g.fillRect(x, y, 1, h);
        }
    }

    private static class LeftRenderer
    extends AbstractRenderer {
        private LeftRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected Insets getTabAreaInsets(Insets defaultInsets) {
            return new Insets(defaultInsets.top + 4, defaultInsets.left, defaultInsets.bottom, defaultInsets.right);
        }

        protected int getTabLabelShiftX(int tabIndex, boolean isSelected) {
            return 1;
        }

        protected int getTabRunOverlay(int tabRunOverlay) {
            return 1;
        }

        protected boolean shouldPadTabRun(int run, boolean aPriori) {
            return false;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top, tabInsets.left - 5, tabInsets.bottom + 1, tabInsets.right - 5);
        }

        protected Insets getSelectedTabPadInsets() {
            return WEST_INSETS;
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
            if (!(this.tabPane.hasFocus() && isSelected)) {
                return;
            }
            Rectangle tabRect = rects[tabIndex];
            int top = tabRect.y + 2;
            int left = tabRect.x + 3;
            int height = tabRect.height - 5;
            int width = tabRect.width - 6;
            g.setColor(this.focus);
            g.drawRect(left, top, width, height);
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            if (!isSelected) {
                g.setColor(this.selectLight);
                g.fillRect(x + 1, y + 1, w - 1, h - 2);
            } else {
                g.setColor(this.selectColor);
                g.fillRect(x + 1, y + 1, w - 3, h - 2);
            }
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            int bottom = h - 1;
            int left = 0;
            g.translate(x, y);
            g.setColor(this.selectHighlight);
            g.fillRect(left + 2, 0, w - 2 - left, 1);
            g.fillRect(left + 1, 1, 1, 1);
            g.fillRect(left, 2, 1, bottom - 3);
            g.setColor(this.darkShadow);
            g.fillRect(left + 1, bottom - 1, 1, 1);
            g.fillRect(left + 2, bottom, w - 2 - left, 1);
            g.translate(- x, - y);
        }

        protected void paintContentBorderLeftEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            g.setColor(this.selectHighlight);
            if (drawBroken && selRect.y >= y && selRect.y <= y + h) {
                g.fillRect(x, y, 1, selRect.y + 1 - y);
                if (selRect.y + selRect.height < y + h - 2) {
                    g.fillRect(x, selRect.y + selRect.height - 1, 1, y + h - selRect.y - selRect.height);
                }
            } else {
                g.fillRect(x, y, 1, h - 1);
            }
        }
    }

    private class MyPropertyChangeHandler
    extends BasicTabbedPaneUI.PropertyChangeHandler {
        private MyPropertyChangeHandler() {
            super(PlasticTabbedPaneUI.this);
        }

        public void propertyChange(PropertyChangeEvent e) {
            super.propertyChange(e);
            String pName = e.getPropertyName();
            if (null == pName) {
                return;
            }
            if (pName.equals("ancestor")) {
                PlasticTabbedPaneUI.this.checkBorderReplacement(PlasticTabbedPaneUI.this.tabPane);
            }
            if (pName.equals("tabPlacement")) {
                PlasticTabbedPaneUI.this.tabPlacementChanged();
                return;
            }
            if (pName.equals("jgoodies.embeddedTabs")) {
                PlasticTabbedPaneUI.this.embeddedTabsPropertyChanged((Boolean)e.getNewValue());
                return;
            }
            if (pName.equals("jgoodies.noContentBorder")) {
                PlasticTabbedPaneUI.this.noContentBorderPropertyChanged((Boolean)e.getNewValue());
                return;
            }
        }
    }

    private static class RightEmbeddedRenderer
    extends AbstractRenderer {
        private RightEmbeddedRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected Insets getTabAreaInsets(Insets insets) {
            return EMPTY_INSETS;
        }

        protected Insets getContentBorderInsets(Insets defaultInsets) {
            return EAST_INSETS;
        }

        protected int getTabRunIndent(int run) {
            return 4 * run;
        }

        protected int getTabRunOverlay(int tabRunOverlay) {
            return 0;
        }

        protected boolean shouldPadTabRun(int run, boolean aPriori) {
            return false;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top, tabInsets.left, tabInsets.bottom, tabInsets.right);
        }

        protected Insets getSelectedTabPadInsets() {
            return EMPTY_INSETS;
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            g.setColor(this.selectColor);
            g.fillRect(x, y, w, h);
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            int bottom = h;
            int right = w - 1;
            g.translate(x + 1, y);
            if (this.isFirstDisplayedTab(tabIndex, y, this.tabPane.getBounds().y)) {
                if (isSelected) {
                    g.setColor(this.shadowColor);
                    g.fillRect(right - 1, bottom - 1, 1, 1);
                    g.fillRect(0, bottom, right - 1, 1);
                    g.setColor(this.selectHighlight);
                    g.fillRect(0, 0, right - 1, 1);
                    g.fillRect(right - 1, 0, 1, bottom - 1);
                    g.fillRect(0, bottom - 1, right - 1, 1);
                }
            } else if (isSelected) {
                g.setColor(this.shadowColor);
                g.fillRect(0, -1, right - 1, 1);
                g.fillRect(right - 1, 0, 1, 1);
                g.fillRect(right - 1, bottom - 1, 1, 1);
                g.fillRect(0, bottom, right - 1, 1);
                g.setColor(this.selectHighlight);
                g.fillRect(0, 0, right - 1, 1);
                g.fillRect(right - 1, 1, 1, bottom - 2);
                g.fillRect(0, bottom - 1, right - 1, 1);
            } else {
                g.setColor(this.shadowColor);
                g.fillRect(2 * right / 3, 0, right / 3, 1);
            }
            g.translate(- x - 1, - y);
        }

        protected void paintContentBorderRightEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            g.setColor(this.shadowColor);
            g.fillRect(x + w - 1, y, 1, h);
        }
    }

    private static class RightRenderer
    extends AbstractRenderer {
        private RightRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected int getTabLabelShiftX(int tabIndex, boolean isSelected) {
            return 1;
        }

        protected int getTabRunOverlay(int tabRunOverlay) {
            return 1;
        }

        protected boolean shouldPadTabRun(int run, boolean aPriori) {
            return false;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top, tabInsets.left - 5, tabInsets.bottom + 1, tabInsets.right - 5);
        }

        protected Insets getSelectedTabPadInsets() {
            return EAST_INSETS;
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
            if (!(this.tabPane.hasFocus() && isSelected)) {
                return;
            }
            Rectangle tabRect = rects[tabIndex];
            int top = tabRect.y + 2;
            int left = tabRect.x + 3;
            int height = tabRect.height - 5;
            int width = tabRect.width - 6;
            g.setColor(this.focus);
            g.drawRect(left, top, width, height);
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            if (!isSelected) {
                g.setColor(this.selectLight);
                g.fillRect(x, y, w, h);
            } else {
                g.setColor(this.selectColor);
                g.fillRect(x + 2, y, w - 2, h);
            }
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            int bottom = h - 1;
            int right = w;
            g.translate(x, y);
            g.setColor(this.selectHighlight);
            g.fillRect(0, 0, right - 1, 1);
            g.setColor(this.darkShadow);
            g.fillRect(right - 1, 1, 1, 1);
            g.fillRect(right, 2, 1, bottom - 3);
            g.fillRect(right - 1, bottom - 1, 1, 1);
            g.fillRect(0, bottom, right - 1, 1);
            g.translate(- x, - y);
        }

        protected void paintContentBorderRightEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            g.setColor(this.darkShadow);
            if (drawBroken && selRect.y >= y && selRect.y <= y + h) {
                g.fillRect(x + w - 1, y, 1, selRect.y - y);
                if (selRect.y + selRect.height < y + h - 2) {
                    g.fillRect(x + w - 1, selRect.y + selRect.height, 1, y + h - selRect.y - selRect.height);
                }
            } else {
                g.fillRect(x + w - 1, y, 1, h - 1);
            }
        }
    }

    public class TabSelectionHandler
    implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JTabbedPane tabbedPane = (JTabbedPane)e.getSource();
            tabbedPane.revalidate();
            tabbedPane.repaint();
        }
    }

    private class TabbedPaneLayout
    extends BasicTabbedPaneUI.TabbedPaneLayout
    implements LayoutManager {
        private TabbedPaneLayout() {
            super(PlasticTabbedPaneUI.this);
        }

        protected void calculateTabRects(int tabPlacement, int tabCount) {
            Rectangle rect;
            int y;
            int x;
            int returnAt;
            int i;
            FontMetrics metrics = PlasticTabbedPaneUI.this.getFontMetrics();
            Dimension size = PlasticTabbedPaneUI.this.tabPane.getSize();
            Insets insets = PlasticTabbedPaneUI.this.tabPane.getInsets();
            Insets theTabAreaInsets = PlasticTabbedPaneUI.this.getTabAreaInsets(tabPlacement);
            int fontHeight = metrics.getHeight();
            int selectedIndex = PlasticTabbedPaneUI.this.tabPane.getSelectedIndex();
            boolean verticalTabRuns = tabPlacement == 2 || tabPlacement == 4;
            boolean leftToRight = PlasticUtils.isLeftToRight(PlasticTabbedPaneUI.this.tabPane);
            switch (tabPlacement) {
                case 2: {
                    PlasticTabbedPaneUI.this.maxTabWidth = PlasticTabbedPaneUI.this.calculateMaxTabWidth(tabPlacement);
                    x = insets.left + theTabAreaInsets.left;
                    y = insets.top + theTabAreaInsets.top;
                    returnAt = size.height - (insets.bottom + theTabAreaInsets.bottom);
                    break;
                }
                case 4: {
                    PlasticTabbedPaneUI.this.maxTabWidth = PlasticTabbedPaneUI.this.calculateMaxTabWidth(tabPlacement);
                    x = size.width - insets.right - theTabAreaInsets.right - PlasticTabbedPaneUI.this.maxTabWidth;
                    y = insets.top + theTabAreaInsets.top;
                    returnAt = size.height - (insets.bottom + theTabAreaInsets.bottom);
                    break;
                }
                case 3: {
                    PlasticTabbedPaneUI.this.maxTabHeight = PlasticTabbedPaneUI.this.calculateMaxTabHeight(tabPlacement);
                    x = insets.left + theTabAreaInsets.left;
                    y = size.height - insets.bottom - theTabAreaInsets.bottom - PlasticTabbedPaneUI.this.maxTabHeight;
                    returnAt = size.width - (insets.right + theTabAreaInsets.right);
                    break;
                }
                default: {
                    PlasticTabbedPaneUI.this.maxTabHeight = PlasticTabbedPaneUI.this.calculateMaxTabHeight(tabPlacement);
                    x = insets.left + theTabAreaInsets.left;
                    y = insets.top + theTabAreaInsets.top;
                    returnAt = size.width - (insets.right + theTabAreaInsets.right);
                }
            }
            int theTabRunOverlay = PlasticTabbedPaneUI.this.getTabRunOverlay(tabPlacement);
            PlasticTabbedPaneUI.this.runCount = 0;
            PlasticTabbedPaneUI.this.selectedRun = -1;
            int tabInRun = -1;
            int runReturnAt = returnAt;
            if (tabCount == 0) {
                return;
            }
            for (i = 0; i < tabCount; ++i) {
                rect = PlasticTabbedPaneUI.this.rects[i];
                ++tabInRun;
                if (!verticalTabRuns) {
                    if (i > 0) {
                        rect.x = PlasticTabbedPaneUI.access$2700((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[i - 1].x + PlasticTabbedPaneUI.access$2800((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[i - 1].width;
                    } else {
                        PlasticTabbedPaneUI.access$2900((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[0] = 0;
                        PlasticTabbedPaneUI.this.runCount = 1;
                        PlasticTabbedPaneUI.this.maxTabWidth = 0;
                        rect.x = x;
                    }
                    rect.width = PlasticTabbedPaneUI.this.calculateTabWidth(tabPlacement, i, metrics);
                    PlasticTabbedPaneUI.this.maxTabWidth = Math.max(PlasticTabbedPaneUI.this.maxTabWidth, rect.width);
                    if (tabInRun != 0 && rect.x + rect.width > runReturnAt) {
                        if (PlasticTabbedPaneUI.this.runCount > PlasticTabbedPaneUI.this.tabRuns.length - 1) {
                            PlasticTabbedPaneUI.this.expandTabRunsArray();
                        }
                        tabInRun = 0;
                        PlasticTabbedPaneUI.access$3800((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[PlasticTabbedPaneUI.access$3900((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)] = i;
                        PlasticTabbedPaneUI.this.runCount++;
                        rect.x = x;
                        runReturnAt-=2 * PlasticTabbedPaneUI.this.getTabRunIndent(tabPlacement, PlasticTabbedPaneUI.this.runCount);
                    }
                    rect.y = y;
                    rect.height = PlasticTabbedPaneUI.this.maxTabHeight;
                } else {
                    if (i > 0) {
                        rect.y = PlasticTabbedPaneUI.access$4300((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[i - 1].y + PlasticTabbedPaneUI.access$4400((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[i - 1].height;
                    } else {
                        PlasticTabbedPaneUI.access$4500((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[0] = 0;
                        PlasticTabbedPaneUI.this.runCount = 1;
                        PlasticTabbedPaneUI.this.maxTabHeight = 0;
                        rect.y = y;
                    }
                    rect.height = PlasticTabbedPaneUI.this.calculateTabHeight(tabPlacement, i, fontHeight);
                    PlasticTabbedPaneUI.this.maxTabHeight = Math.max(PlasticTabbedPaneUI.this.maxTabHeight, rect.height);
                    if (tabInRun != 0 && rect.y + rect.height > runReturnAt) {
                        if (PlasticTabbedPaneUI.this.runCount > PlasticTabbedPaneUI.this.tabRuns.length - 1) {
                            PlasticTabbedPaneUI.this.expandTabRunsArray();
                        }
                        PlasticTabbedPaneUI.access$5400((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[PlasticTabbedPaneUI.access$5500((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)] = i;
                        PlasticTabbedPaneUI.this.runCount++;
                        rect.y = y;
                        tabInRun = 0;
                        runReturnAt-=2 * PlasticTabbedPaneUI.this.getTabRunIndent(tabPlacement, PlasticTabbedPaneUI.this.runCount);
                    }
                    rect.x = x;
                    rect.width = PlasticTabbedPaneUI.this.maxTabWidth;
                }
                if (i != selectedIndex) continue;
                PlasticTabbedPaneUI.this.selectedRun = PlasticTabbedPaneUI.this.runCount - 1;
            }
            if (PlasticTabbedPaneUI.this.runCount > 1 && PlasticTabbedPaneUI.this.shouldRotateTabRuns(tabPlacement)) {
                this.rotateTabRuns(tabPlacement, PlasticTabbedPaneUI.this.selectedRun);
            }
            for (i = PlasticTabbedPaneUI.access$6300((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this) - 1; i >= 0; --i) {
                int j;
                int start = PlasticTabbedPaneUI.this.tabRuns[i];
                int next = PlasticTabbedPaneUI.this.tabRuns[i == PlasticTabbedPaneUI.this.runCount - 1 ? 0 : i + 1];
                int end = next != 0 ? next - 1 : tabCount - 1;
                int indent = PlasticTabbedPaneUI.this.getTabRunIndent(tabPlacement, i);
                if (!verticalTabRuns) {
                    for (j = start; j <= end; ++j) {
                        rect = PlasticTabbedPaneUI.this.rects[j];
                        rect.y = y;
                        rect.x+=indent;
                    }
                    if (PlasticTabbedPaneUI.this.shouldPadTabRun(tabPlacement, i)) {
                        this.padTabRun(tabPlacement, start, end, returnAt - 2 * indent);
                    }
                    if (tabPlacement == 3) {
                        y-=PlasticTabbedPaneUI.this.maxTabHeight - theTabRunOverlay;
                        continue;
                    }
                    y+=PlasticTabbedPaneUI.this.maxTabHeight - theTabRunOverlay;
                    continue;
                }
                for (j = start; j <= end; ++j) {
                    rect = PlasticTabbedPaneUI.this.rects[j];
                    rect.x = x;
                    rect.y+=indent;
                }
                if (PlasticTabbedPaneUI.this.shouldPadTabRun(tabPlacement, i)) {
                    this.padTabRun(tabPlacement, start, end, returnAt - 2 * indent);
                }
                if (tabPlacement == 4) {
                    x-=PlasticTabbedPaneUI.this.maxTabWidth - theTabRunOverlay;
                    continue;
                }
                x+=PlasticTabbedPaneUI.this.maxTabWidth - theTabRunOverlay;
            }
            this.padSelectedTab(tabPlacement, selectedIndex);
            if (!(leftToRight || verticalTabRuns)) {
                int rightMargin = size.width - (insets.right + theTabAreaInsets.right);
                for (i = 0; i < tabCount; ++i) {
                    PlasticTabbedPaneUI.access$7300((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[i].x = rightMargin - PlasticTabbedPaneUI.access$7400((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[i].x - PlasticTabbedPaneUI.access$7500((PlasticTabbedPaneUI)PlasticTabbedPaneUI.this)[i].width;
                }
            }
        }
    }

    private static class TopEmbeddedRenderer
    extends AbstractRenderer {
        private TopEmbeddedRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected Insets getTabAreaInsets(Insets insets) {
            return EMPTY_INSETS;
        }

        protected Insets getContentBorderInsets(Insets defaultInsets) {
            return NORTH_INSETS;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top, tabInsets.left + 1, tabInsets.bottom, tabInsets.right);
        }

        protected Insets getSelectedTabPadInsets() {
            return EMPTY_INSETS;
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            g.setColor(this.selectColor);
            g.fillRect(x, y, w, h);
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            g.translate(x, y);
            int right = w;
            int bottom = h;
            if (this.isFirstDisplayedTab(tabIndex, x, this.tabPane.getBounds().x)) {
                if (isSelected) {
                    g.setColor(this.selectHighlight);
                    g.fillRect(0, 0, 1, bottom);
                    g.fillRect(0, 0, right - 1, 1);
                    g.fillRect(right - 1, 0, 1, bottom);
                    g.setColor(this.shadowColor);
                    g.fillRect(right - 1, 0, 1, 1);
                    g.fillRect(right, 1, 1, bottom);
                }
            } else if (isSelected) {
                g.setColor(this.selectHighlight);
                g.fillRect(1, 1, 1, bottom - 1);
                g.fillRect(2, 0, right - 3, 1);
                g.fillRect(right - 1, 1, 1, bottom - 1);
                g.setColor(this.shadowColor);
                g.fillRect(0, 1, 1, bottom - 1);
                g.fillRect(1, 0, 1, 1);
                g.fillRect(right - 1, 0, 1, 1);
                g.fillRect(right, 1, 1, bottom);
            } else {
                g.setColor(this.shadowColor);
                g.fillRect(0, 0, 1, bottom + 2 - bottom / 2);
            }
            g.translate(- x, - y);
        }

        protected void paintContentBorderTopEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            g.setColor(this.shadowColor);
            g.fillRect(x, y, w, 1);
        }
    }

    private static class TopRenderer
    extends AbstractRenderer {
        private TopRenderer(JTabbedPane tabPane) {
            super(tabPane);
        }

        protected Insets getTabAreaInsets(Insets defaultInsets) {
            return new Insets(defaultInsets.top, defaultInsets.left + 4, defaultInsets.bottom, defaultInsets.right);
        }

        protected int getTabLabelShiftY(int tabIndex, boolean isSelected) {
            return isSelected ? -1 : 0;
        }

        protected int getTabRunOverlay(int tabRunOverlay) {
            return tabRunOverlay - 2;
        }

        protected int getTabRunIndent(int run) {
            return 6 * run;
        }

        protected Insets getSelectedTabPadInsets() {
            return NORTH_INSETS;
        }

        protected Insets getTabInsets(int tabIndex, Insets tabInsets) {
            return new Insets(tabInsets.top - 1, tabInsets.left - 4, tabInsets.bottom, tabInsets.right - 4);
        }

        protected void paintFocusIndicator(Graphics g, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
            if (!(this.tabPane.hasFocus() && isSelected)) {
                return;
            }
            Rectangle tabRect = rects[tabIndex];
            int top = tabRect.y + 1;
            int left = tabRect.x + 4;
            int height = tabRect.height - 3;
            int width = tabRect.width - 9;
            g.setColor(this.focus);
            g.drawRect(left, top, width, height);
        }

        protected void paintTabBackground(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            int sel = isSelected ? 0 : 1;
            g.setColor(this.selectColor);
            g.fillRect(x, y + sel, w, h / 2);
            g.fillRect(x - 1, y + sel + h / 2, w + 2, h - h / 2);
        }

        protected void paintTabBorder(Graphics g, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
            g.translate(x - 4, y);
            int top = 0;
            int right = w + 6;
            g.setColor(this.selectHighlight);
            g.drawLine(1, h - 1, 4, top + 4);
            g.fillRect(5, top + 2, 1, 2);
            g.fillRect(6, top + 1, 1, 1);
            g.fillRect(7, top, right - 12, 1);
            g.setColor(this.darkShadow);
            g.drawLine(right, h - 1, right - 3, top + 4);
            g.fillRect(right - 4, top + 2, 1, 2);
            g.fillRect(right - 5, top + 1, 1, 1);
            g.translate(- x + 4, - y);
        }

        protected void paintContentBorderTopEdge(Graphics g, int x, int y, int w, int h, boolean drawBroken, Rectangle selRect, boolean isContentBorderPainted) {
            int right = x + w - 1;
            int top = y;
            g.setColor(this.selectHighlight);
            if (drawBroken && selRect.x >= x && selRect.x <= x + w) {
                g.fillRect(x, top, selRect.x - 2 - x, 1);
                if (selRect.x + selRect.width < x + w - 2) {
                    g.fillRect(selRect.x + selRect.width + 2, top, right - 2 - selRect.x - selRect.width, 1);
                } else {
                    g.fillRect(x + w - 2, top, 1, 1);
                }
            } else {
                g.fillRect(x, top, w - 1, 1);
            }
        }
    }

}

