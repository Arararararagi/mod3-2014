/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.plastic.PlasticInternalFrameTitlePane;
import java.awt.Color;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicInternalFrameUI;

public final class PlasticInternalFrameUI
extends BasicInternalFrameUI {
    private static final String FRAME_TYPE = "JInternalFrame.frameType";
    public static final String IS_PALETTE = "JInternalFrame.isPalette";
    private static final String PALETTE_FRAME = "palette";
    private static final String OPTION_DIALOG = "optionDialog";
    private static final Border EMPTY_BORDER = new EmptyBorder(0, 0, 0, 0);
    private PlasticInternalFrameTitlePane titlePane;
    private PropertyChangeListener paletteListener;
    private PropertyChangeListener contentPaneListener;

    public PlasticInternalFrameUI(JInternalFrame b) {
        super(b);
    }

    public static ComponentUI createUI(JComponent c) {
        return new PlasticInternalFrameUI((JInternalFrame)c);
    }

    public void installUI(JComponent c) {
        this.frame = (JInternalFrame)c;
        this.paletteListener = new PaletteListener(this);
        this.contentPaneListener = new ContentPaneListener(this);
        c.addPropertyChangeListener(this.paletteListener);
        c.addPropertyChangeListener(this.contentPaneListener);
        super.installUI(c);
        Object paletteProp = c.getClientProperty("JInternalFrame.isPalette");
        if (paletteProp != null) {
            this.setPalette((Boolean)paletteProp);
        }
        Container content = this.frame.getContentPane();
        this.stripContentBorder(content);
    }

    public void uninstallUI(JComponent c) {
        JComponent content;
        this.frame = (JInternalFrame)c;
        c.removePropertyChangeListener(this.paletteListener);
        c.removePropertyChangeListener(this.contentPaneListener);
        Container cont = ((JInternalFrame)c).getContentPane();
        if (cont instanceof JComponent && (content = (JComponent)cont).getBorder() == EMPTY_BORDER) {
            content.setBorder(null);
        }
        super.uninstallUI(c);
    }

    protected void installDefaults() {
        Color bg;
        super.installDefaults();
        JComponent contentPane = (JComponent)this.frame.getContentPane();
        if (contentPane != null && (bg = contentPane.getBackground()) instanceof UIResource) {
            contentPane.setBackground(null);
        }
        this.frame.setBackground(UIManager.getLookAndFeelDefaults().getColor("control"));
    }

    protected void installKeyboardActions() {
    }

    protected void uninstallKeyboardActions() {
    }

    private void stripContentBorder(Object c) {
        JComponent contentComp;
        Border contentBorder;
        if (c instanceof JComponent && ((contentBorder = (contentComp = (JComponent)c).getBorder()) == null || contentBorder instanceof UIResource)) {
            contentComp.setBorder(EMPTY_BORDER);
        }
    }

    protected JComponent createNorthPane(JInternalFrame w) {
        this.titlePane = new PlasticInternalFrameTitlePane(w);
        return this.titlePane;
    }

    public void setPalette(boolean isPalette) {
        String key = isPalette ? "InternalFrame.paletteBorder" : "InternalFrame.border";
        LookAndFeel.installBorder(this.frame, key);
        this.titlePane.setPalette(isPalette);
    }

    private void setFrameType(String frameType) {
        boolean hasPalette = frameType.equals("palette");
        String key = frameType.equals("optionDialog") ? "InternalFrame.optionDialogBorder" : (hasPalette ? "InternalFrame.paletteBorder" : "InternalFrame.border");
        LookAndFeel.installBorder(this.frame, key);
        this.titlePane.setPalette(hasPalette);
    }

    private static class ContentPaneListener
    implements PropertyChangeListener {
        private final PlasticInternalFrameUI ui;

        private ContentPaneListener(PlasticInternalFrameUI ui) {
            this.ui = ui;
        }

        public void propertyChange(PropertyChangeEvent e) {
            String name = e.getPropertyName();
            if (name.equals("contentPane")) {
                this.ui.stripContentBorder(e.getNewValue());
            }
        }
    }

    private static class PaletteListener
    implements PropertyChangeListener {
        private final PlasticInternalFrameUI ui;

        private PaletteListener(PlasticInternalFrameUI ui) {
            this.ui = ui;
        }

        public void propertyChange(PropertyChangeEvent e) {
            String name = e.getPropertyName();
            Object value = e.getNewValue();
            if (name.equals("JInternalFrame.frameType")) {
                if (value instanceof String) {
                    this.ui.setFrameType((String)value);
                }
            } else if (name.equals("JInternalFrame.isPalette")) {
                this.ui.setPalette(Boolean.TRUE.equals(value));
            }
        }
    }

}

