/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.LookUtils;
import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.io.Serializable;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;

public final class PlasticXPIconFactory {
    private static CheckBoxIcon checkBoxIcon;
    private static RadioButtonIcon radioButtonIcon;

    static Icon getCheckBoxIcon() {
        if (checkBoxIcon == null) {
            checkBoxIcon = new CheckBoxIcon();
        }
        return checkBoxIcon;
    }

    static Icon getRadioButtonIcon() {
        if (radioButtonIcon == null) {
            radioButtonIcon = new RadioButtonIcon();
        }
        return radioButtonIcon;
    }

    private static class CheckBoxIcon
    implements Icon,
    UIResource,
    Serializable {
        private static final int SIZE = LookUtils.isLowRes ? 13 : 15;

        private CheckBoxIcon() {
        }

        public int getIconWidth() {
            return SIZE;
        }

        public int getIconHeight() {
            return SIZE;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
            JCheckBox cb = (JCheckBox)c;
            ButtonModel model = cb.getModel();
            Graphics2D g2 = (Graphics2D)g;
            Object hint = g2.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            this.drawBorder(g2, model.isEnabled(), x, y, SIZE - 1, SIZE - 1);
            this.drawFill(g2, model.isPressed(), x + 1, y + 1, SIZE - 2, SIZE - 2);
            if (model.isEnabled() && model.isArmed() && !model.isPressed()) {
                this.drawFocus(g2, x + 1, y + 1, SIZE - 3, SIZE - 3);
            }
            if (model.isSelected()) {
                this.drawCheck(g2, model.isEnabled(), x + 3, y + 3, SIZE - 7, SIZE - 7);
            }
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, hint);
        }

        private void drawBorder(Graphics2D g2, boolean enabled, int x, int y, int width, int height) {
            g2.setColor(enabled ? PlasticLookAndFeel.getControlDarkShadow() : MetalLookAndFeel.getControlDisabled());
            g2.drawRect(x, y, width, height);
        }

        private void drawCheck(Graphics2D g2, boolean enabled, int x, int y, int width, int height) {
            g2.setColor(enabled ? UIManager.getColor("CheckBox.check") : MetalLookAndFeel.getControlDisabled());
            int right = x + width;
            int bottom = y + height;
            int startY = y + height / 3;
            int turnX = x + width / 2 - 2;
            g2.drawLine(x, startY, turnX, bottom - 3);
            g2.drawLine(x, startY + 1, turnX, bottom - 2);
            g2.drawLine(x, startY + 2, turnX, bottom - 1);
            g2.drawLine(turnX + 1, bottom - 2, right, y);
            g2.drawLine(turnX + 1, bottom - 1, right, y + 1);
            g2.drawLine(turnX + 1, bottom, right, y + 2);
        }

        private void drawFill(Graphics2D g2, boolean pressed, int x, int y, int w, int h) {
            Color lowerRight2;
            ColorUIResource upperLeft;
            Color lowerRight2;
            if (pressed) {
                upperLeft = MetalLookAndFeel.getControlShadow();
                lowerRight2 = PlasticLookAndFeel.getControlHighlight();
            } else {
                upperLeft = PlasticLookAndFeel.getControl();
                lowerRight2 = PlasticLookAndFeel.getControlHighlight().brighter();
            }
            g2.setPaint(new GradientPaint(x, y, upperLeft, x + w, y + h, lowerRight2));
            g2.fillRect(x, y, w, h);
        }

        private void drawFocus(Graphics2D g2, int x, int y, int width, int height) {
            g2.setPaint(new GradientPaint(x, y, PlasticLookAndFeel.getFocusColor().brighter(), width, height, PlasticLookAndFeel.getFocusColor()));
            g2.drawRect(x, y, width, height);
            g2.drawRect(x + 1, y + 1, width - 2, height - 2);
        }
    }

    private static class RadioButtonIcon
    implements Icon,
    UIResource,
    Serializable {
        private static final int SIZE = LookUtils.isLowRes ? 13 : 15;
        private static final Stroke FOCUS_STROKE = new BasicStroke(2.0f);

        private RadioButtonIcon() {
        }

        public int getIconWidth() {
            return SIZE;
        }

        public int getIconHeight() {
            return SIZE;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
            Graphics2D g2 = (Graphics2D)g;
            AbstractButton b = (AbstractButton)c;
            ButtonModel model = b.getModel();
            Object hint = g2.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            this.drawFill(g2, model.isPressed(), x, y, SIZE - 1, SIZE - 1);
            if (model.isArmed() && !model.isPressed()) {
                this.drawFocus(g2, x + 1, y + 1, SIZE - 3, SIZE - 3);
            }
            if (model.isSelected()) {
                this.drawCheck(g2, c, model.isEnabled(), x + 4, y + 4, SIZE - 8, SIZE - 8);
            }
            this.drawBorder(g2, model.isEnabled(), x, y, SIZE - 1, SIZE - 1);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, hint);
        }

        private void drawBorder(Graphics2D g2, boolean enabled, int x, int y, int w, int h) {
            g2.setColor(enabled ? PlasticLookAndFeel.getControlDarkShadow() : MetalLookAndFeel.getControlDisabled());
            g2.drawOval(x, y, w, h);
        }

        private void drawCheck(Graphics2D g2, Component c, boolean enabled, int x, int y, int w, int h) {
            g2.translate(x, y);
            if (enabled) {
                g2.setColor(UIManager.getColor("RadioButton.check"));
                g2.fillOval(0, 0, w, h);
                UIManager.getIcon("RadioButton.checkIcon").paintIcon(c, g2, 0, 0);
            } else {
                g2.setColor(MetalLookAndFeel.getControlDisabled());
                g2.fillOval(0, 0, w, h);
            }
            g2.translate(- x, - y);
        }

        private void drawFill(Graphics2D g2, boolean pressed, int x, int y, int w, int h) {
            Color lowerRight2;
            ColorUIResource upperLeft;
            Color lowerRight2;
            if (pressed) {
                upperLeft = MetalLookAndFeel.getControlShadow();
                lowerRight2 = PlasticLookAndFeel.getControlHighlight();
            } else {
                upperLeft = PlasticLookAndFeel.getControl();
                lowerRight2 = PlasticLookAndFeel.getControlHighlight().brighter();
            }
            g2.setPaint(new GradientPaint(x, y, upperLeft, x + w, y + h, lowerRight2));
            g2.fillOval(x, y, w, h);
        }

        private void drawFocus(Graphics2D g2, int x, int y, int w, int h) {
            g2.setPaint(new GradientPaint(x, y, PlasticLookAndFeel.getFocusColor().brighter(), w, h, PlasticLookAndFeel.getFocusColor()));
            Stroke stroke = g2.getStroke();
            g2.setStroke(FOCUS_STROKE);
            g2.drawOval(x, y, w, h);
            g2.setStroke(stroke);
        }
    }

}

