/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.clearlook.ClearLookManager;
import com.jgoodies.clearlook.ClearLookUtils;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalLabelUI;

public final class PlasticLabelUI
extends MetalLabelUI {
    private static final PlasticLabelUI INSTANCE = new PlasticLabelUI();

    public static ComponentUI createUI(JComponent x) {
        return INSTANCE;
    }

    public void paint(Graphics g, JComponent c) {
        JLabel label = (JLabel)c;
        if (!ClearLookUtils.hasCheckedBorder(label)) {
            Border oldBorder = ClearLookManager.replaceBorder(label);
            ClearLookUtils.storeBorder(label, oldBorder);
        }
        super.paint(g, c);
    }

    protected void uninstallDefaults(JLabel label) {
        Border storedBorder = ClearLookUtils.getStoredBorder(label);
        if (storedBorder != null) {
            label.setBorder(storedBorder);
        }
        super.uninstallDefaults(label);
    }
}

