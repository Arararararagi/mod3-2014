/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.clearlook.ClearLookManager;
import java.awt.Graphics;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.LookAndFeel;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalScrollPaneUI;

public final class PlasticScrollPaneUI
extends MetalScrollPaneUI {
    private Border storedBorder;
    private boolean hasCheckedBorderReplacement = false;
    private boolean hasEtchedBorder = false;
    private PropertyChangeListener listener;

    public static ComponentUI createUI(JComponent b) {
        return new PlasticScrollPaneUI();
    }

    protected void installDefaults(JScrollPane scrollPane) {
        super.installDefaults(scrollPane);
        this.installEtchedBorder(scrollPane);
    }

    protected void uninstallDefaults(JScrollPane scrollPane) {
        if (this.storedBorder != null) {
            scrollPane.setBorder(this.storedBorder);
        }
        super.uninstallDefaults(scrollPane);
    }

    protected void installEtchedBorder(JScrollPane scrollPane) {
        Object value = scrollPane.getClientProperty("jgoodies.isEtched");
        if (Boolean.TRUE.equals(value)) {
            LookAndFeel.installBorder(scrollPane, "ScrollPane.etchedBorder");
            this.hasEtchedBorder = true;
        }
    }

    public void installListeners(JScrollPane scrollPane) {
        super.installListeners(scrollPane);
        this.listener = this.createBorderStyleListener();
        scrollPane.addPropertyChangeListener(this.listener);
    }

    protected void uninstallListeners(JComponent c) {
        ((JScrollPane)c).removePropertyChangeListener(this.listener);
        super.uninstallListeners(c);
    }

    private PropertyChangeListener createBorderStyleListener() {
        return new PropertyChangeListener(){

            public void propertyChange(PropertyChangeEvent e) {
                String prop = e.getPropertyName();
                if (prop.equals("jgoodies.isEtched")) {
                    JScrollPane scrollPane = (JScrollPane)e.getSource();
                    PlasticScrollPaneUI.this.installEtchedBorder(scrollPane);
                }
            }
        };
    }

    public void paint(Graphics g, JComponent c) {
        if (this.hasEtchedBorder) {
            super.paint(g, c);
            return;
        }
        if (!this.hasCheckedBorderReplacement) {
            this.storedBorder = ClearLookManager.replaceBorder(c);
            this.hasCheckedBorderReplacement = true;
        }
        super.paint(g, c);
    }

}

