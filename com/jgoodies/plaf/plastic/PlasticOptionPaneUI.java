/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.plastic;

import com.jgoodies.plaf.common.ExtButtonAreaLayout;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicOptionPaneUI;

public final class PlasticOptionPaneUI
extends BasicOptionPaneUI {
    public static ComponentUI createUI(JComponent b) {
        return new PlasticOptionPaneUI();
    }

    protected Container createButtonArea() {
        JPanel bottom = new JPanel();
        bottom.setBorder(UIManager.getBorder("OptionPane.buttonAreaBorder"));
        bottom.setLayout(new ExtButtonAreaLayout(true, 6));
        this.addButtonComponents(bottom, this.getButtons(), this.getInitialValueIndex());
        return bottom;
    }
}

