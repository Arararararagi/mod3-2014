/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf;

import com.jgoodies.clearlook.ClearLookManager;
import com.jgoodies.plaf.Options;
import com.jgoodies.plaf.plastic.PlasticLookAndFeel;
import com.jgoodies.plaf.plastic.PlasticTheme;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.image.ColorModel;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

public final class LookUtils {
    private static final String JAVA_VERSION = LookUtils.getSystemProperty("java.version");
    public static final boolean IS_JAVA_1_4 = LookUtils.startsWith(JAVA_VERSION, "1.4");
    static final boolean IS_JAVA_1_4_0 = LookUtils.startsWith(JAVA_VERSION, "1.4.0");
    public static final boolean IS_JAVA_1_4_2_OR_LATER = !LookUtils.startsWith(JAVA_VERSION, "1.4.0") && !LookUtils.startsWith(JAVA_VERSION, "1.4.1");
    private static final String OS_NAME = LookUtils.getSystemProperty("os.name");
    private static final String OS_VERSION = LookUtils.getSystemProperty("os.version");
    public static final boolean IS_OS_MAC = LookUtils.startsWith(OS_NAME, "Mac");
    public static final boolean IS_OS_WINDOWS_MODERN = LookUtils.startsWith(OS_NAME, "Windows") && !LookUtils.startsWith(OS_VERSION, "4.0");
    public static final boolean IS_OS_WINDOWS_XP = LookUtils.startsWith(OS_NAME, "Windows") && LookUtils.startsWith(OS_VERSION, "5.1");
    public static final boolean IS_LAF_WINDOWS_XP_ENABLED = LookUtils.isWindowsXPLafEnabled();
    public static final boolean IS_NETBEANS;
    public static boolean isLowRes;
    private static boolean loggingEnabled;

    private LookUtils() {
    }

    public static String getSystemProperty(String key) {
        try {
            return System.getProperty(key);
        }
        catch (SecurityException e) {
            LookUtils.log("Can't read the System property " + key + ".");
            return null;
        }
    }

    public static String getSystemProperty(String key, String defaultValue) {
        try {
            return System.getProperty(key, defaultValue);
        }
        catch (SecurityException e) {
            LookUtils.log("Can't read the System property " + key + ".");
            return defaultValue;
        }
    }

    private static boolean isWindowsXPLafEnabled() {
        return IS_OS_WINDOWS_XP && IS_JAVA_1_4_2_OR_LATER && (Boolean)Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive") != false && LookUtils.getSystemProperty("swing.noxp") == null;
    }

    public static boolean isTrueColor(Component c) {
        return c.getToolkit().getColorModel().getPixelSize() >= 24;
    }

    public static void installNarrowMargin(AbstractButton b, String propertyPrefix) {
        boolean isNarrow;
        Object value;
        String defaultsKey = propertyPrefix + ((isNarrow = Boolean.TRUE.equals(value = b.getClientProperty("jgoodies.isNarrow"))) ? "narrowMargin" : "margin");
        Insets insets = b.getMargin();
        if (insets == null || insets instanceof UIResource) {
            b.setMargin(UIManager.getInsets(defaultsKey));
        }
    }

    public static Insets createButtonMargin(boolean narrow) {
        int pad = narrow || Options.getUseNarrowButtons() ? 4 : 14;
        return isLowRes ? new InsetsUIResource(2, pad, 1, pad) : new InsetsUIResource(3, pad, 3, pad);
    }

    public static Color getSlightlyBrighter(Color color) {
        return LookUtils.getSlightlyBrighter(color, 1.1f);
    }

    public static Color getSlightlyBrighter(Color color, float factor) {
        float[] hsbValues = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsbValues);
        float hue = hsbValues[0];
        float saturation = hsbValues[1];
        float brightness = hsbValues[2];
        float newBrightness = Math.min(brightness * factor, 1.0f);
        return Color.getHSBColor(hue, saturation, newBrightness);
    }

    public static void setLookAndTheme(LookAndFeel laf, Object theme) throws UnsupportedLookAndFeelException {
        if (laf instanceof PlasticLookAndFeel && theme != null && theme instanceof PlasticTheme) {
            PlasticLookAndFeel.setMyCurrentTheme((PlasticTheme)theme);
        }
        UIManager.setLookAndFeel(laf);
    }

    public static Object getDefaultTheme(LookAndFeel laf) {
        return laf instanceof PlasticLookAndFeel ? PlasticLookAndFeel.createMyDefaultTheme() : null;
    }

    public static List getInstalledThemes(LookAndFeel laf) {
        return laf instanceof PlasticLookAndFeel ? PlasticLookAndFeel.getInstalledThemes() : Collections.EMPTY_LIST;
    }

    public static void setLoggingEnabled(boolean enabled) {
        loggingEnabled = enabled;
    }

    public static void log() {
        if (loggingEnabled) {
            System.out.println();
        }
    }

    public static void log(String message) {
        if (loggingEnabled) {
            System.out.println("JGoodies Looks: " + message);
        }
    }

    private static boolean isLowResolution() {
        return Toolkit.getDefaultToolkit().getScreenResolution() < 120;
    }

    private static boolean isNetBeans() {
        boolean hasNetBeansBuildNumber;
        String property = LookUtils.getSystemProperty("netbeans.buildnumber");
        boolean bl = hasNetBeansBuildNumber = property != null && property.length() > 0;
        if (hasNetBeansBuildNumber) {
            LookUtils.log("NetBeans detected - dobry den!");
        }
        return hasNetBeansBuildNumber;
    }

    private static boolean startsWith(String str, String prefix) {
        return str != null && str.startsWith(prefix);
    }

    static {
        isLowRes = LookUtils.isLowResolution();
        loggingEnabled = true;
        IS_NETBEANS = LookUtils.isNetBeans();
        if (IS_NETBEANS) {
            ClearLookManager.getMode();
        }
    }
}

