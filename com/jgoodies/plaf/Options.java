/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf;

import com.jgoodies.clearlook.ClearLookMode;
import com.jgoodies.plaf.FontSizeHints;
import com.jgoodies.plaf.LookUtils;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import javax.swing.UIManager;

public final class Options {
    public static final String PLASTIC_NAME = "com.jgoodies.plaf.plastic.PlasticLookAndFeel";
    public static final String PLASTIC3D_NAME = "com.jgoodies.plaf.plastic.Plastic3DLookAndFeel";
    public static final String PLASTICXP_NAME = "com.jgoodies.plaf.plastic.PlasticXPLookAndFeel";
    public static final String EXT_WINDOWS_NAME = "com.jgoodies.plaf.windows.ExtWindowsLookAndFeel";
    public static final String DEFAULT_LOOK_NAME = "com.jgoodies.plaf.plastic.Plastic3DLookAndFeel";
    private static final Map LAF_REPLACEMENTS = new HashMap();
    public static final String MENU_FONT_KEY = "jgoodies.menuFont";
    public static final String CONTROL_FONT_KEY = "jgoodies.controlFont";
    public static final String FONT_SIZE_HINTS_KEY = "jgoodies.fontSizeHints";
    public static final String USE_SYSTEM_FONTS_KEY = "swing.useSystemFontSettings";
    public static final String USE_SYSTEM_FONTS_APP_KEY = "Application.useSystemFontSettings";
    public static final String DEFAULT_ICON_SIZE_KEY = "jgoodies.defaultIconSize";
    public static final String USE_NARROW_BUTTONS_KEY = "jgoodies.useNarrowButtons";
    public static final String TAB_ICONS_ENABLED_KEY = "jgoodies.tabIconsEnabled";
    public static final String CLEAR_LOOK_MODE_KEY = "ClearLook.mode";
    public static final String CLEAR_LOOK_POLICY_KEY = "ClearLook.policy";
    public static final String CLEAR_LOOK_OFF;
    public static final String CLEAR_LOOK_ON;
    public static final String CLEAR_LOOK_VERBOSE;
    public static final String CLEAR_LOOK_DEBUG;
    public static final String IS_NARROW_KEY = "jgoodies.isNarrow";
    public static final String IS_ETCHED_KEY = "jgoodies.isEtched";
    public static final String HEADER_STYLE_KEY = "jgoodies.headerStyle";
    public static final String NO_ICONS_KEY = "jgoodies.noIcons";
    public static final String TREE_LINE_STYLE_KEY = "JTree.lineStyle";
    public static final String TREE_LINE_STYLE_ANGLED_VALUE = "Angled";
    public static final String TREE_LINE_STYLE_NONE_VALUE = "None";
    public static final String NO_CONTENT_BORDER_KEY = "jgoodies.noContentBorder";
    public static final String EMBEDDED_TABS_KEY = "jgoodies.embeddedTabs";
    private static final Dimension DEFAULT_ICON_SIZE;

    private Options() {
    }

    public static boolean getUseSystemFonts() {
        return UIManager.get("Application.useSystemFontSettings").equals(Boolean.TRUE);
    }

    public static void setUseSystemFonts(boolean useSystemFonts) {
        UIManager.put("Application.useSystemFontSettings", new Boolean(useSystemFonts));
    }

    public static Dimension getDefaultIconSize() {
        Dimension size = UIManager.getDimension("jgoodies.defaultIconSize");
        return size == null ? DEFAULT_ICON_SIZE : size;
    }

    public static void setDefaultIconSize(Dimension defaultIconSize) {
        UIManager.put("jgoodies.defaultIconSize", defaultIconSize);
    }

    public static FontSizeHints getGlobalFontSizeHints() {
        Object value = UIManager.get("jgoodies.fontSizeHints");
        if (value != null) {
            return (FontSizeHints)value;
        }
        String name = LookUtils.getSystemProperty("jgoodies.fontSizeHints", "");
        try {
            return FontSizeHints.valueOf(name);
        }
        catch (IllegalArgumentException e) {
            return FontSizeHints.DEFAULT;
        }
    }

    public static void setGlobalFontSizeHints(FontSizeHints hints) {
        UIManager.put("jgoodies.fontSizeHints", hints);
    }

    public static boolean getUseNarrowButtons() {
        return UIManager.getBoolean("jgoodies.useNarrowButtons");
    }

    public static void setUseNarrowButtons(boolean b) {
        UIManager.put("jgoodies.useNarrowButtons", new Boolean(b));
    }

    public static boolean isTabIconsEnabled() {
        Object value;
        boolean result;
        if (!LookUtils.IS_NETBEANS) {
            return true;
        }
        String userMode = LookUtils.getSystemProperty("jgoodies.tabIconsEnabled", "");
        boolean overridden = userMode.length() > 0;
        Object object = value = overridden ? userMode : UIManager.get("jgoodies.tabIconsEnabled");
        boolean bl = overridden ? userMode.equalsIgnoreCase("true") : (result = value instanceof Boolean && Boolean.TRUE.equals(value));
        if (overridden) {
            LookUtils.log("You have " + (result ? "en" : "dis") + "abled icons in tabbed panes.");
        }
        return result;
    }

    public static void setTabIconsEnabled(boolean b) {
        UIManager.put("jgoodies.tabIconsEnabled", new Boolean(b));
    }

    public static void putLookAndFeelReplacement(String original, String replacement) {
        LAF_REPLACEMENTS.put(original, replacement);
    }

    public static void removeLookAndFeelReplacement(String original) {
        LAF_REPLACEMENTS.remove(original);
    }

    public static void initializeDefaultReplacements() {
        Options.putLookAndFeelReplacement("javax.swing.plaf.metal.MetalLookAndFeel", "com.jgoodies.plaf.plastic.Plastic3DLookAndFeel");
        Options.putLookAndFeelReplacement("com.sun.java.swing.plaf.windows.WindowsLookAndFeel", "com.jgoodies.plaf.windows.ExtWindowsLookAndFeel");
    }

    public static String getReplacementClassNameFor(String className) {
        String replacement = (String)LAF_REPLACEMENTS.get(className);
        return replacement == null ? className : replacement;
    }

    public static String getCrossPlatformLookAndFeelClassName() {
        return "com.jgoodies.plaf.plastic.Plastic3DLookAndFeel";
    }

    public static String getSystemLookAndFeelClassName() {
        String osName = System.getProperty("os.name");
        if (osName.startsWith("Windows")) {
            return "com.jgoodies.plaf.windows.ExtWindowsLookAndFeel";
        }
        if (osName.startsWith("Mac")) {
            return UIManager.getSystemLookAndFeelClassName();
        }
        return Options.getCrossPlatformLookAndFeelClassName();
    }

    static {
        Options.initializeDefaultReplacements();
        CLEAR_LOOK_OFF = ClearLookMode.OFF.getName();
        CLEAR_LOOK_ON = ClearLookMode.ON.getName();
        CLEAR_LOOK_VERBOSE = ClearLookMode.VERBOSE.getName();
        CLEAR_LOOK_DEBUG = ClearLookMode.DEBUG.getName();
        DEFAULT_ICON_SIZE = new Dimension(20, 20);
    }
}

