/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.common;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.AttributedCharacterIterator;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.text.Document;
import javax.swing.text.InternationalFormatter;

public final class ExtBasicArrowButtonHandler
extends AbstractAction
implements MouseListener {
    private final Timer autoRepeatTimer;
    private final boolean isNext;
    private JSpinner spinner = null;

    public ExtBasicArrowButtonHandler(String name, boolean isNext) {
        super(name);
        this.isNext = isNext;
        this.autoRepeatTimer = new Timer(60, this);
        this.autoRepeatTimer.setInitialDelay(300);
    }

    private JSpinner eventToSpinner(AWTEvent e) {
        Object src = e.getSource();
        while (src instanceof Component && !(src instanceof JSpinner)) {
            src = ((Component)src).getParent();
        }
        return src instanceof JSpinner ? (JSpinner)src : null;
    }

    public void actionPerformed(ActionEvent e) {
        JSpinner aSpinner = this.spinner;
        if (!(e.getSource() instanceof Timer)) {
            aSpinner = this.eventToSpinner(e);
        }
        if (aSpinner != null) {
            try {
                Object value;
                int calendarField = this.getCalendarField(aSpinner);
                aSpinner.commitEdit();
                if (calendarField != -1) {
                    ((SpinnerDateModel)aSpinner.getModel()).setCalendarField(calendarField);
                }
                Object object = value = this.isNext ? aSpinner.getNextValue() : aSpinner.getPreviousValue();
                if (value != null) {
                    aSpinner.setValue(value);
                    this.select(aSpinner);
                }
            }
            catch (IllegalArgumentException iae) {
                UIManager.getLookAndFeel().provideErrorFeedback(aSpinner);
            }
            catch (ParseException pe) {
                UIManager.getLookAndFeel().provideErrorFeedback(aSpinner);
            }
        }
    }

    private void select(JSpinner aSpinner) {
        JComponent editor = aSpinner.getEditor();
        if (editor instanceof JSpinner.DateEditor) {
            Object value;
            SpinnerDateModel model;
            DateFormat.Field field;
            JSpinner.DateEditor dateEditor = (JSpinner.DateEditor)editor;
            JFormattedTextField ftf = dateEditor.getTextField();
            SimpleDateFormat format = dateEditor.getFormat();
            if (format != null && (value = aSpinner.getValue()) != null && (field = DateFormat.Field.ofCalendarField((model = dateEditor.getModel()).getCalendarField())) != null) {
                try {
                    AttributedCharacterIterator iterator = format.formatToCharacterIterator(value);
                    if (!(this.select(ftf, iterator, field) || field != DateFormat.Field.HOUR0)) {
                        this.select(ftf, iterator, DateFormat.Field.HOUR1);
                    }
                }
                catch (IllegalArgumentException iae) {
                    // empty catch block
                }
            }
        }
    }

    private boolean select(JFormattedTextField ftf, AttributedCharacterIterator iterator, DateFormat.Field field) {
        int max = ftf.getDocument().getLength();
        iterator.first();
        do {
            Map<AttributedCharacterIterator.Attribute, Object> attrs;
            if ((attrs = iterator.getAttributes()) == null || !attrs.containsKey(field)) continue;
            int start = iterator.getRunStart(field);
            int end = iterator.getRunLimit(field);
            if (start != -1 && end != -1 && start <= max && end <= max) {
                ftf.select(start, end);
            }
            return true;
        } while (iterator.next() != '\uffff');
        return false;
    }

    private int getCalendarField(JSpinner aSpinner) {
        JComponent editor = aSpinner.getEditor();
        if (editor instanceof JSpinner.DateEditor) {
            JSpinner.DateEditor dateEditor = (JSpinner.DateEditor)editor;
            JFormattedTextField ftf = dateEditor.getTextField();
            int start = ftf.getSelectionStart();
            JFormattedTextField.AbstractFormatter formatter = ftf.getFormatter();
            if (formatter instanceof InternationalFormatter) {
                Format.Field[] fields = ((InternationalFormatter)formatter).getFields(start);
                for (int counter = 0; counter < fields.length; ++counter) {
                    int calendarField;
                    if (!(fields[counter] instanceof DateFormat.Field) || (calendarField = fields[counter] == DateFormat.Field.HOUR1 ? 10 : ((DateFormat.Field)fields[counter]).getCalendarField()) == -1) continue;
                    return calendarField;
                }
            }
        }
        return -1;
    }

    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e) && e.getComponent().isEnabled()) {
            this.spinner = this.eventToSpinner(e);
            this.autoRepeatTimer.start();
            this.focusSpinnerIfNecessary();
        }
    }

    public void mouseReleased(MouseEvent e) {
        this.autoRepeatTimer.stop();
        this.spinner = null;
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    private void focusSpinnerIfNecessary() {
        Component fo = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (this.spinner.isRequestFocusEnabled() && (fo == null || !SwingUtilities.isDescendingFrom(fo, this.spinner))) {
            FocusTraversalPolicy ftp;
            Component child;
            Container root = this.spinner;
            if (!root.isFocusCycleRoot()) {
                root = root.getFocusCycleRootAncestor();
            }
            if (root != null && (child = (ftp = root.getFocusTraversalPolicy()).getComponentAfter(root, this.spinner)) != null && SwingUtilities.isDescendingFrom(child, this.spinner)) {
                child.requestFocus();
            }
        }
    }
}

