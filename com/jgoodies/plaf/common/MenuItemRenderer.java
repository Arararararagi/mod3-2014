/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.common;

import com.jgoodies.plaf.common.MinimumSizedCheckIcon;
import com.jgoodies.plaf.common.MinimumSizedIcon;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.text.View;

public final class MenuItemRenderer {
    protected static final String HTML_KEY = "html";
    private static final String MAX_TEXT_WIDTH = "maxTextWidth";
    private static final String MAX_ACC_WIDTH = "maxAccWidth";
    private static final Icon NO_ICON = new NullIcon();
    static Rectangle zeroRect = new Rectangle(0, 0, 0, 0);
    static Rectangle iconRect = new Rectangle();
    static Rectangle textRect = new Rectangle();
    static Rectangle acceleratorRect = new Rectangle();
    static Rectangle checkIconRect = new Rectangle();
    static Rectangle arrowIconRect = new Rectangle();
    static Rectangle viewRect = new Rectangle(32767, 32767);
    static Rectangle r = new Rectangle();
    private final JMenuItem menuItem;
    private final boolean iconBorderEnabled;
    private final Font acceleratorFont;
    private final Color selectionForeground;
    private final Color disabledForeground;
    private final Color acceleratorForeground;
    private final Color acceleratorSelectionForeground;
    private final String acceleratorDelimiter;
    private final Icon fillerIcon;

    public MenuItemRenderer(JMenuItem menuItem, boolean iconBorderEnabled, Font acceleratorFont, Color selectionForeground, Color disabledForeground, Color acceleratorForeground, Color acceleratorSelectionForeground) {
        this.menuItem = menuItem;
        this.iconBorderEnabled = iconBorderEnabled;
        this.acceleratorFont = acceleratorFont;
        this.selectionForeground = selectionForeground;
        this.disabledForeground = disabledForeground;
        this.acceleratorForeground = acceleratorForeground;
        this.acceleratorSelectionForeground = acceleratorSelectionForeground;
        this.acceleratorDelimiter = UIManager.getString("MenuItem.acceleratorDelimiter");
        this.fillerIcon = new MinimumSizedIcon();
    }

    private Icon getIcon(JMenuItem aMenuItem, Icon defaultIcon) {
        Icon icon = aMenuItem.getIcon();
        if (icon == null) {
            return defaultIcon;
        }
        ButtonModel model = aMenuItem.getModel();
        if (!model.isEnabled()) {
            return model.isSelected() ? aMenuItem.getDisabledSelectedIcon() : aMenuItem.getDisabledIcon();
        }
        if (model.isPressed() && model.isArmed()) {
            Icon pressedIcon = aMenuItem.getPressedIcon();
            return pressedIcon != null ? pressedIcon : icon;
        }
        if (model.isSelected()) {
            Icon selectedIcon = aMenuItem.getSelectedIcon();
            return selectedIcon != null ? selectedIcon : icon;
        }
        return icon;
    }

    private boolean hasCustomIcon() {
        return this.getIcon(this.menuItem, null) != null;
    }

    private Icon getWrappedIcon(Icon icon) {
        if (this.hideIcons()) {
            return NO_ICON;
        }
        if (icon == null) {
            return this.fillerIcon;
        }
        return this.iconBorderEnabled && this.hasCustomIcon() ? new MinimumSizedCheckIcon(icon, this.menuItem) : new MinimumSizedIcon(icon);
    }

    private void resetRects() {
        iconRect.setBounds(zeroRect);
        textRect.setBounds(zeroRect);
        acceleratorRect.setBounds(zeroRect);
        checkIconRect.setBounds(zeroRect);
        arrowIconRect.setBounds(zeroRect);
        viewRect.setBounds(0, 0, 32767, 32767);
        r.setBounds(zeroRect);
    }

    public Dimension getPreferredMenuItemSize(JComponent c, Icon checkIcon, Icon arrowIcon, int defaultTextIconGap) {
        JMenuItem b = (JMenuItem)c;
        String text = b.getText();
        KeyStroke accelerator = b.getAccelerator();
        String acceleratorText = "";
        if (accelerator != null) {
            int keyCode;
            int modifiers = accelerator.getModifiers();
            if (modifiers > 0) {
                acceleratorText = KeyEvent.getKeyModifiersText(modifiers);
                acceleratorText = acceleratorText + this.acceleratorDelimiter;
            }
            acceleratorText = (keyCode = accelerator.getKeyCode()) != 0 ? acceleratorText + KeyEvent.getKeyText(keyCode) : acceleratorText + accelerator.getKeyChar();
        }
        Font font = b.getFont();
        FontMetrics fm = b.getFontMetrics(font);
        FontMetrics fmAccel = b.getFontMetrics(this.acceleratorFont);
        this.resetRects();
        Icon wrappedIcon = this.getWrappedIcon(this.getIcon(this.menuItem, checkIcon));
        Icon wrappedArrowIcon = this.getWrappedIcon(arrowIcon);
        this.layoutMenuItem(fm, text, fmAccel, acceleratorText, null, wrappedIcon, wrappedArrowIcon, b.getVerticalAlignment(), b.getHorizontalAlignment(), b.getVerticalTextPosition(), b.getHorizontalTextPosition(), viewRect, iconRect, textRect, acceleratorRect, checkIconRect, arrowIconRect, text == null ? 0 : defaultTextIconGap, defaultTextIconGap);
        r.setBounds(textRect);
        r = SwingUtilities.computeUnion(MenuItemRenderer.iconRect.x, MenuItemRenderer.iconRect.y, MenuItemRenderer.iconRect.width, MenuItemRenderer.iconRect.height, r);
        Container parent = this.menuItem.getParent();
        if (!(parent == null || !(parent instanceof JComponent) || this.menuItem instanceof JMenu && ((JMenu)this.menuItem).isTopLevelMenu())) {
            int maxAccValue;
            JComponent p = (JComponent)parent;
            Integer maxTextWidth = (Integer)p.getClientProperty("maxTextWidth");
            Integer maxAccWidth = (Integer)p.getClientProperty("maxAccWidth");
            int maxTextValue = maxTextWidth != null ? maxTextWidth : 0;
            int n = maxAccValue = maxAccWidth != null ? maxAccWidth : 0;
            if (MenuItemRenderer.r.width < maxTextValue) {
                MenuItemRenderer.r.width = maxTextValue;
            } else {
                p.putClientProperty("maxTextWidth", new Integer(MenuItemRenderer.r.width));
            }
            if (MenuItemRenderer.acceleratorRect.width > maxAccValue) {
                maxAccValue = MenuItemRenderer.acceleratorRect.width;
                p.putClientProperty("maxAccWidth", new Integer(MenuItemRenderer.acceleratorRect.width));
            }
            MenuItemRenderer.r.width+=maxAccValue;
            MenuItemRenderer.r.width+=10;
        }
        if (this.useCheckAndArrow()) {
            MenuItemRenderer.r.width+=MenuItemRenderer.checkIconRect.width;
            MenuItemRenderer.r.width+=defaultTextIconGap;
            MenuItemRenderer.r.width+=defaultTextIconGap;
            MenuItemRenderer.r.width+=MenuItemRenderer.arrowIconRect.width;
        }
        MenuItemRenderer.r.width+=2 * defaultTextIconGap;
        Insets insets = b.getInsets();
        if (insets != null) {
            MenuItemRenderer.r.width+=insets.left + insets.right;
            MenuItemRenderer.r.height+=insets.top + insets.bottom;
        }
        if (MenuItemRenderer.r.height % 2 == 1) {
            ++MenuItemRenderer.r.height;
        }
        return r.getSize();
    }

    public void paintMenuItem(Graphics g, JComponent c, Icon checkIcon, Icon arrowIcon, Color background, Color foreground, int defaultTextIconGap) {
        JMenuItem b = (JMenuItem)c;
        ButtonModel model = b.getModel();
        int menuWidth = b.getWidth();
        int menuHeight = b.getHeight();
        Insets i = c.getInsets();
        this.resetRects();
        viewRect.setBounds(0, 0, menuWidth, menuHeight);
        MenuItemRenderer.viewRect.x+=i.left;
        MenuItemRenderer.viewRect.y+=i.top;
        MenuItemRenderer.viewRect.width-=i.right + MenuItemRenderer.viewRect.x;
        MenuItemRenderer.viewRect.height-=i.bottom + MenuItemRenderer.viewRect.y;
        Font holdf = g.getFont();
        Font f = c.getFont();
        g.setFont(f);
        FontMetrics fm = g.getFontMetrics(f);
        FontMetrics fmAccel = g.getFontMetrics(this.acceleratorFont);
        KeyStroke accelerator = b.getAccelerator();
        String acceleratorText = "";
        if (accelerator != null) {
            int keyCode;
            int modifiers = accelerator.getModifiers();
            if (modifiers > 0) {
                acceleratorText = KeyEvent.getKeyModifiersText(modifiers);
                acceleratorText = acceleratorText + this.acceleratorDelimiter;
            }
            acceleratorText = (keyCode = accelerator.getKeyCode()) != 0 ? acceleratorText + KeyEvent.getKeyText(keyCode) : acceleratorText + accelerator.getKeyChar();
        }
        Icon wrappedIcon = this.getWrappedIcon(this.getIcon(this.menuItem, checkIcon));
        MinimumSizedIcon wrappedArrowIcon = new MinimumSizedIcon(arrowIcon);
        String text = this.layoutMenuItem(fm, b.getText(), fmAccel, acceleratorText, null, wrappedIcon, wrappedArrowIcon, b.getVerticalAlignment(), b.getHorizontalAlignment(), b.getVerticalTextPosition(), b.getHorizontalTextPosition(), viewRect, iconRect, textRect, acceleratorRect, checkIconRect, arrowIconRect, b.getText() == null ? 0 : defaultTextIconGap, defaultTextIconGap);
        this.paintBackground(g, b, background);
        Color holdc = g.getColor();
        if (model.isArmed() || c instanceof JMenu && model.isSelected()) {
            g.setColor(foreground);
        }
        wrappedIcon.paintIcon(c, g, MenuItemRenderer.checkIconRect.x, MenuItemRenderer.checkIconRect.y);
        g.setColor(holdc);
        if (text != null) {
            View v = (View)c.getClientProperty("html");
            if (v != null) {
                v.paint(g, textRect);
            } else {
                this.paintText(g, b, textRect, text);
            }
        }
        if (!(acceleratorText == null || acceleratorText.equals(""))) {
            int accOffset = 0;
            Container parent = this.menuItem.getParent();
            if (parent != null && parent instanceof JComponent) {
                JComponent p = (JComponent)parent;
                Integer maxValueInt = (Integer)p.getClientProperty("maxAccWidth");
                int maxValue = maxValueInt != null ? maxValueInt : MenuItemRenderer.acceleratorRect.width;
                accOffset = maxValue - MenuItemRenderer.acceleratorRect.width;
            }
            g.setFont(this.acceleratorFont);
            if (!model.isEnabled()) {
                if (this.disabledForeground != null) {
                    g.setColor(this.disabledForeground);
                    BasicGraphicsUtils.drawString(g, acceleratorText, 0, MenuItemRenderer.acceleratorRect.x - accOffset, MenuItemRenderer.acceleratorRect.y + fmAccel.getAscent());
                } else {
                    g.setColor(b.getBackground().brighter());
                    BasicGraphicsUtils.drawString(g, acceleratorText, 0, MenuItemRenderer.acceleratorRect.x - accOffset, MenuItemRenderer.acceleratorRect.y + fmAccel.getAscent());
                    g.setColor(b.getBackground().darker());
                    BasicGraphicsUtils.drawString(g, acceleratorText, 0, MenuItemRenderer.acceleratorRect.x - accOffset - 1, MenuItemRenderer.acceleratorRect.y + fmAccel.getAscent() - 1);
                }
            } else {
                if (model.isArmed() || c instanceof JMenu && model.isSelected()) {
                    g.setColor(this.acceleratorSelectionForeground);
                } else {
                    g.setColor(this.acceleratorForeground);
                }
                BasicGraphicsUtils.drawString(g, acceleratorText, 0, MenuItemRenderer.acceleratorRect.x - accOffset, MenuItemRenderer.acceleratorRect.y + fmAccel.getAscent());
            }
        }
        if (arrowIcon != null) {
            if (model.isArmed() || c instanceof JMenu && model.isSelected()) {
                g.setColor(foreground);
            }
            if (this.useCheckAndArrow()) {
                wrappedArrowIcon.paintIcon(c, g, MenuItemRenderer.arrowIconRect.x, MenuItemRenderer.arrowIconRect.y);
            }
        }
        g.setColor(holdc);
        g.setFont(holdf);
    }

    private String layoutMenuItem(FontMetrics fm, String text, FontMetrics fmAccel, String acceleratorText, Icon icon, Icon checkIcon, Icon arrowIcon, int verticalAlignment, int horizontalAlignment, int verticalTextPosition, int horizontalTextPosition, Rectangle viewRectangle, Rectangle iconRectangle, Rectangle textRectangle, Rectangle acceleratorRectangle, Rectangle checkIconRectangle, Rectangle arrowIconRectangle, int textIconGap, int menuItemGap) {
        SwingUtilities.layoutCompoundLabel(this.menuItem, fm, text, icon, verticalAlignment, horizontalAlignment, verticalTextPosition, horizontalTextPosition, viewRectangle, iconRectangle, textRectangle, textIconGap);
        if (acceleratorText == null || acceleratorText.equals("")) {
            acceleratorRectangle.height = 0;
            acceleratorRectangle.width = 0;
            acceleratorText = "";
        } else {
            acceleratorRectangle.width = SwingUtilities.computeStringWidth(fmAccel, acceleratorText);
            acceleratorRectangle.height = fmAccel.getHeight();
        }
        boolean useCheckAndArrow = this.useCheckAndArrow();
        if (useCheckAndArrow) {
            if (checkIcon != null) {
                checkIconRectangle.width = checkIcon.getIconWidth();
                checkIconRectangle.height = checkIcon.getIconHeight();
            } else {
                checkIconRectangle.height = 0;
                checkIconRectangle.width = 0;
            }
            if (arrowIcon != null) {
                arrowIconRectangle.width = arrowIcon.getIconWidth();
                arrowIconRectangle.height = arrowIcon.getIconHeight();
            } else {
                arrowIconRectangle.height = 0;
                arrowIconRectangle.width = 0;
            }
        }
        Rectangle labelRect = iconRectangle.union(textRectangle);
        if (this.isLeftToRight(this.menuItem)) {
            textRectangle.x+=menuItemGap;
            iconRectangle.x+=menuItemGap;
            acceleratorRectangle.x = viewRectangle.x + viewRectangle.width - arrowIconRectangle.width - menuItemGap - acceleratorRectangle.width;
            if (useCheckAndArrow) {
                checkIconRectangle.x = viewRectangle.x;
                textRectangle.x+=menuItemGap + checkIconRectangle.width;
                iconRectangle.x+=menuItemGap + checkIconRectangle.width;
                arrowIconRectangle.x = viewRectangle.x + viewRectangle.width - menuItemGap - arrowIconRectangle.width;
            }
        } else {
            textRectangle.x-=menuItemGap;
            iconRectangle.x-=menuItemGap;
            acceleratorRectangle.x = viewRectangle.x + arrowIconRectangle.width + menuItemGap;
            if (useCheckAndArrow) {
                checkIconRectangle.x = viewRectangle.x + viewRectangle.width - checkIconRectangle.width;
                textRectangle.x-=menuItemGap + checkIconRectangle.width;
                iconRectangle.x-=menuItemGap + checkIconRectangle.width;
                arrowIconRectangle.x = viewRectangle.x + menuItemGap;
            }
        }
        acceleratorRectangle.y = labelRect.y + labelRect.height / 2 - acceleratorRectangle.height / 2;
        if (useCheckAndArrow) {
            arrowIconRectangle.y = labelRect.y + labelRect.height / 2 - arrowIconRectangle.height / 2;
            checkIconRectangle.y = labelRect.y + labelRect.height / 2 - checkIconRectangle.height / 2;
        }
        return text;
    }

    private boolean useCheckAndArrow() {
        boolean isTopLevelMenu = this.menuItem instanceof JMenu && ((JMenu)this.menuItem).isTopLevelMenu();
        return !isTopLevelMenu;
    }

    private boolean isLeftToRight(Component c) {
        return c.getComponentOrientation().isLeftToRight();
    }

    public void paintBackground(Graphics g, JMenuItem aMenuItem, Color bgColor) {
        ButtonModel model = aMenuItem.getModel();
        if (aMenuItem.isOpaque()) {
            int menuWidth = aMenuItem.getWidth();
            int menuHeight = aMenuItem.getHeight();
            Color c = model.isArmed() || aMenuItem instanceof JMenu && model.isSelected() ? bgColor : aMenuItem.getBackground();
            Color oldColor = g.getColor();
            g.setColor(c);
            g.fillRect(0, 0, menuWidth, menuHeight);
            g.setColor(oldColor);
        }
    }

    public void paintText(Graphics g, JMenuItem aMenuItem, Rectangle textRectangle, String text) {
        ButtonModel model = aMenuItem.getModel();
        FontMetrics fm = g.getFontMetrics();
        int mnemIndex = aMenuItem.getDisplayedMnemonicIndex();
        if (!model.isEnabled()) {
            if (UIManager.get("MenuItem.disabledForeground") instanceof Color) {
                g.setColor(UIManager.getColor("MenuItem.disabledForeground"));
                MenuItemRenderer.drawStringUnderlineCharAt(g, text, mnemIndex, textRectangle.x, textRectangle.y + fm.getAscent());
            } else {
                g.setColor(aMenuItem.getBackground().brighter());
                MenuItemRenderer.drawStringUnderlineCharAt(g, text, mnemIndex, textRectangle.x, textRectangle.y + fm.getAscent());
                g.setColor(aMenuItem.getBackground().darker());
                MenuItemRenderer.drawStringUnderlineCharAt(g, text, mnemIndex, textRectangle.x - 1, textRectangle.y + fm.getAscent() - 1);
            }
        } else {
            if (model.isArmed() || aMenuItem instanceof JMenu && model.isSelected()) {
                g.setColor(this.selectionForeground);
            }
            MenuItemRenderer.drawStringUnderlineCharAt(g, text, mnemIndex, textRectangle.x, textRectangle.y + fm.getAscent());
        }
    }

    public static void drawStringUnderlineCharAt(Graphics g, String text, int underlinedIndex, int x, int y) {
        g.drawString(text, x, y);
        if (underlinedIndex >= 0 && underlinedIndex < text.length()) {
            FontMetrics fm = g.getFontMetrics();
            int underlineRectX = x + fm.stringWidth(text.substring(0, underlinedIndex));
            int underlineRectY = y;
            int underlineRectWidth = fm.charWidth(text.charAt(underlinedIndex));
            int underlineRectHeight = 1;
            g.fillRect(underlineRectX, underlineRectY + fm.getDescent() - 1, underlineRectWidth, underlineRectHeight);
        }
    }

    private boolean hideIcons() {
        Component invoker;
        Container parent = this.menuItem.getParent();
        if (!(parent instanceof JPopupMenu)) {
            return false;
        }
        JPopupMenu popupMenu = (JPopupMenu)parent;
        Object value = popupMenu.getClientProperty("jgoodies.noIcons");
        if (value == null && (invoker = popupMenu.getInvoker()) != null && invoker instanceof JMenu) {
            value = ((JMenu)invoker).getClientProperty("jgoodies.noIcons");
        }
        return Boolean.TRUE.equals(value);
    }

    private static class NullIcon
    implements Icon {
        private NullIcon() {
        }

        public int getIconWidth() {
            return 0;
        }

        public int getIconHeight() {
            return 0;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
        }
    }

}

