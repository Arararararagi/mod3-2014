/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.common;

import com.jgoodies.plaf.LookUtils;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.plaf.basic.BasicOptionPaneUI;

public final class ExtButtonAreaLayout
extends BasicOptionPaneUI.ButtonAreaLayout {
    public ExtButtonAreaLayout(boolean syncAllWidths, int padding) {
        super(syncAllWidths, padding);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public void layoutContainer(Container container) {
        int counter;
        int xLocation;
        int xOffset;
        Component[] children = container.getComponents();
        if (children == null || children.length <= 0) return;
        int numChildren = children.length;
        Dimension[] sizes = new Dimension[numChildren];
        int yLocation = container.getInsets().top;
        if (this.syncAllWidths) {
            int xOffset2;
            int counter2;
            int xLocation2;
            int maxWidth = this.getMinimumButtonWidth();
            for (counter2 = 0; counter2 < numChildren; ++counter2) {
                sizes[counter2] = children[counter2].getPreferredSize();
                maxWidth = Math.max(maxWidth, sizes[counter2].width);
            }
            if (this.getCentersChildren()) {
                xLocation2 = (container.getSize().width - (maxWidth * numChildren + (numChildren - 1) * this.padding)) / 2;
                xOffset2 = this.padding + maxWidth;
            } else if (numChildren > 1) {
                xLocation2 = 0;
                xOffset2 = (container.getSize().width - maxWidth * numChildren) / (numChildren - 1) + maxWidth;
            } else {
                xLocation2 = (container.getSize().width - maxWidth) / 2;
                xOffset2 = 0;
            }
            for (counter2 = 0; counter2 < numChildren; ++counter2) {
                children[counter2].setBounds(xLocation2, yLocation, maxWidth, sizes[counter2].height);
                xLocation2+=xOffset2;
            }
            return;
        }
        int totalWidth = 0;
        for (counter = 0; counter < numChildren; ++counter) {
            sizes[counter] = children[counter].getPreferredSize();
            totalWidth+=sizes[counter].width;
        }
        totalWidth+=(numChildren - 1) * this.padding;
        boolean cc = this.getCentersChildren();
        if (cc) {
            xLocation = (container.getSize().width - totalWidth) / 2;
            xOffset = this.padding;
        } else if (numChildren > 1) {
            xOffset = (container.getSize().width - totalWidth) / (numChildren - 1);
            xLocation = 0;
        } else {
            xLocation = (container.getSize().width - totalWidth) / 2;
            xOffset = 0;
        }
        for (counter = 0; counter < numChildren; ++counter) {
            children[counter].setBounds(xLocation, yLocation, sizes[counter].width, sizes[counter].height);
            xLocation+=xOffset + sizes[counter].width;
        }
    }

    public Dimension minimumLayoutSize(Container c) {
        Component[] children;
        if (c != null && (children = c.getComponents()) != null && children.length > 0) {
            int numChildren = children.length;
            int height = 0;
            Insets cInsets = c.getInsets();
            int extraHeight = cInsets.top + cInsets.bottom;
            if (this.syncAllWidths) {
                int maxWidth = this.getMinimumButtonWidth();
                for (int counter = 0; counter < numChildren; ++counter) {
                    Dimension aSize = children[counter].getPreferredSize();
                    height = Math.max(height, aSize.height);
                    maxWidth = Math.max(maxWidth, aSize.width);
                }
                return new Dimension(maxWidth * numChildren + (numChildren - 1) * this.padding, extraHeight + height);
            }
            int totalWidth = 0;
            for (int counter = 0; counter < numChildren; ++counter) {
                Dimension aSize = children[counter].getPreferredSize();
                height = Math.max(height, aSize.height);
                totalWidth+=aSize.width;
            }
            return new Dimension(totalWidth+=(numChildren - 1) * this.padding, extraHeight + height);
        }
        return new Dimension(0, 0);
    }

    private int getMinimumButtonWidth() {
        return LookUtils.isLowRes ? 75 : 100;
    }
}

