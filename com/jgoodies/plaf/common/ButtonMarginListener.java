/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf.common;

import com.jgoodies.plaf.LookUtils;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractButton;

public final class ButtonMarginListener
implements PropertyChangeListener {
    public static final String CLIENT_KEY = "jgoodies.buttonMarginListener";
    private final String propertyPrefix;

    public ButtonMarginListener(String propertyPrefix) {
        this.propertyPrefix = propertyPrefix;
    }

    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();
        AbstractButton button = (AbstractButton)e.getSource();
        if (prop.equals("jgoodies.isNarrow")) {
            LookUtils.installNarrowMargin(button, this.propertyPrefix);
        }
    }
}

