/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf;

import com.jgoodies.plaf.FontSizeHints;
import com.jgoodies.plaf.LookUtils;
import java.awt.Font;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

public final class FontUtils {
    private FontUtils() {
    }

    public static boolean useSystemFontSettings() {
        String systemFonts = LookUtils.getSystemProperty("swing.useSystemFontSettings");
        if ("false".equalsIgnoreCase(systemFonts)) {
            return false;
        }
        Object value = UIManager.get("Application.useSystemFontSettings");
        return !Boolean.FALSE.equals(value);
    }

    public static void initFontDefaults(UIDefaults table, Object controlFont, Object controlBoldFont, Object fixedControlFont, Object menuFont, Object messageFont, Object toolTipFont, Object windowFont) {
        Object[] defaults = new Object[]{"Button.font", controlFont, "CheckBox.font", controlFont, "ColorChooser.font", controlFont, "ComboBox.font", controlFont, "EditorPane.font", controlFont, "FormattedTextField.font", controlFont, "Label.font", controlFont, "List.font", controlFont, "Panel.font", controlFont, "PasswordField.font", controlFont, "ProgressBar.font", controlFont, "RadioButton.font", controlFont, "ScrollPane.font", controlFont, "Spinner.font", controlFont, "TabbedPane.font", controlFont, "Table.font", controlFont, "TableHeader.font", controlFont, "TextField.font", controlFont, "TextPane.font", controlFont, "ToolBar.font", controlFont, "ToggleButton.font", controlFont, "Tree.font", controlFont, "Viewport.font", controlFont, "InternalFrame.titleFont", windowFont, "OptionPane.font", messageFont, "OptionPane.messageFont", messageFont, "OptionPane.buttonFont", messageFont, "Spinner.font", fixedControlFont, "TextArea.font", fixedControlFont, "TitledBorder.font", controlBoldFont, "ToolTip.font", toolTipFont};
        table.putDefaults(defaults);
        if (menuFont != null) {
            Object[] menuDefaults = new Object[]{"CheckBoxMenuItem.font", menuFont, "CheckBoxMenuItem.acceleratorFont", menuFont, "Menu.font", menuFont, "Menu.acceleratorFont", menuFont, "MenuBar.font", menuFont, "MenuItem.font", menuFont, "MenuItem.acceleratorFont", menuFont, "PopupMenu.font", menuFont, "RadioButtonMenuItem.font", menuFont, "RadioButtonMenuItem.acceleratorFont", menuFont};
            table.putDefaults(menuDefaults);
        }
    }

    public static Font getMenuFont(UIDefaults table, FontSizeHints hints) {
        String fontDescription = LookUtils.getSystemProperty("jgoodies.menuFont");
        if (fontDescription != null) {
            return Font.decode(fontDescription);
        }
        Font menuFont = table.getFont("Menu.font");
        if (menuFont.getName().equals("Tahoma")) {
            float size = (float)menuFont.getSize() + hints.menuFontSizeDelta();
            float minSize = hints.menuFontSize();
            menuFont = menuFont.deriveFont(Math.max(minSize, size));
        }
        return new FontUIResource(menuFont);
    }

    public static Font getControlFont(UIDefaults table, FontSizeHints hints) {
        String fontDescription = LookUtils.getSystemProperty("jgoodies.controlFont");
        if (fontDescription != null) {
            return Font.decode(fontDescription);
        }
        String fontKey = LookUtils.IS_JAVA_1_4_0 ? "Label.font" : "OptionPane.font";
        Font controlFont = table.getFont(fontKey);
        if (controlFont.getName().equals("Tahoma")) {
            float oldSize = controlFont.getSize();
            float minSize = hints.controlFontSize();
            float size = oldSize + hints.controlFontSizeDelta();
            controlFont = controlFont.deriveFont(Math.max(minSize, size));
        }
        return new FontUIResource(controlFont);
    }
}

