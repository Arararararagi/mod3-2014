/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.plaf;

import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;

public final class BorderStyle {
    public static final BorderStyle EMPTY = new BorderStyle("Empty");
    public static final BorderStyle SEPARATOR = new BorderStyle("Separator");
    public static final BorderStyle ETCHED = new BorderStyle("Etched");
    private final String name;

    private BorderStyle(String name) {
        this.name = name;
    }

    public static BorderStyle from(JToolBar toolBar, String clientPropertyKey) {
        return BorderStyle.from0(toolBar, clientPropertyKey);
    }

    public static BorderStyle from(JMenuBar menuBar, String clientPropertyKey) {
        return BorderStyle.from0(menuBar, clientPropertyKey);
    }

    private static BorderStyle from0(JComponent c, String clientPropertyKey) {
        Object value = c.getClientProperty(clientPropertyKey);
        if (value instanceof BorderStyle) {
            return (BorderStyle)value;
        }
        if (value instanceof String) {
            return BorderStyle.valueOf((String)value);
        }
        return null;
    }

    private static BorderStyle valueOf(String name) {
        if (name.equalsIgnoreCase(BorderStyle.EMPTY.name)) {
            return EMPTY;
        }
        if (name.equalsIgnoreCase(BorderStyle.SEPARATOR.name)) {
            return SEPARATOR;
        }
        if (name.equalsIgnoreCase(BorderStyle.ETCHED.name)) {
            return ETCHED;
        }
        throw new IllegalArgumentException("Invalid BorderStyle name " + name);
    }

    public String toString() {
        return this.name;
    }
}

