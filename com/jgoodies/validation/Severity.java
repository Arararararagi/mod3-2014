/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation;

public enum Severity {
    ERROR,
    WARNING,
    OK;
    

    private Severity() {
    }

    public static Severity max(Severity severity1, Severity severity2) {
        return severity1.ordinal() < severity2.ordinal() ? severity1 : severity2;
    }
}

