/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.message.SimpleValidationMessage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ValidationResult
implements Serializable {
    public static final ValidationResult EMPTY = new ValidationResult(Collections.<ValidationMessage>emptyList(), false);
    private final List<ValidationMessage> messageList;
    private final boolean modifiable;

    public ValidationResult() {
        this(new ArrayList<ValidationMessage>(), true);
    }

    private ValidationResult(List<ValidationMessage> messageList, boolean modifiable) {
        this.messageList = messageList;
        this.modifiable = modifiable;
    }

    public static ValidationResult unmodifiableResult(ValidationResult validationResult) {
        return validationResult.modifiable ? new ValidationResult(new ArrayList<ValidationMessage>(validationResult.messageList), false) : validationResult;
    }

    public void add(ValidationMessage validationMessage) {
        this.assertModifiable();
        if (validationMessage == null) {
            throw new NullPointerException("The validation message must not be null.");
        }
        if (validationMessage.severity() == Severity.OK) {
            throw new IllegalArgumentException("You must not add a validation message with severity OK.");
        }
        this.messageList.add(validationMessage);
    }

    public void addError(String text) {
        this.assertModifiable();
        if (text == null) {
            throw new NullPointerException("The message text must not be null.");
        }
        this.add(new SimpleValidationMessage(text, Severity.ERROR));
    }

    public void addWarning(String text) {
        this.assertModifiable();
        if (text == null) {
            throw new NullPointerException("The message text must not be null.");
        }
        this.add(new SimpleValidationMessage(text));
    }

    public void addAll(List<ValidationMessage> messages) {
        this.assertModifiable();
        if (messages == null) {
            throw new NullPointerException("The messages list must not be null.");
        }
        for (ValidationMessage message : messages) {
            if (message.severity() != Severity.OK) continue;
            throw new IllegalArgumentException("You must not add a validation message with severity OK.");
        }
        this.messageList.addAll(messages);
    }

    public void addAllFrom(ValidationResult validationResult) {
        this.assertModifiable();
        if (validationResult == null) {
            throw new NullPointerException("The validation result to add must not be null.");
        }
        this.addAll(validationResult.messageList);
    }

    public boolean isEmpty() {
        return this.messageList.isEmpty();
    }

    public int size() {
        return this.messageList.size();
    }

    public boolean contains(ValidationMessage message) {
        return this.messageList.contains(message);
    }

    public ValidationResult subResult(int fromIndex, int toIndex) {
        List<ValidationMessage> messages = this.messageList.subList(fromIndex, toIndex);
        return new ValidationResult(messages, false);
    }

    public ValidationResult subResult(Object messageKey) {
        if (messageKey == null) {
            return EMPTY;
        }
        ArrayList<ValidationMessage> messages = new ArrayList<ValidationMessage>();
        for (ValidationMessage message : this.messageList) {
            if (!messageKey.equals(message.key())) continue;
            messages.add(message);
        }
        return new ValidationResult(messages, false);
    }

    public ValidationResult subResult(Object[] messageKeys) {
        if (messageKeys == null) {
            return EMPTY;
        }
        ArrayList<ValidationMessage> messages = new ArrayList<ValidationMessage>();
        for (ValidationMessage message : this.messageList) {
            Object messageKey = message.key();
            for (Object key : messageKeys) {
                if (!messageKey.equals(key)) continue;
                messages.add(message);
            }
        }
        return new ValidationResult(messages, false);
    }

    public Map<Object, ValidationResult> keyMap() {
        HashMap<Object, LinkedList<ValidationMessage>> messageMap = new HashMap<Object, LinkedList<ValidationMessage>>();
        for (ValidationMessage message : this.messageList) {
            Object key = message.key();
            LinkedList<ValidationMessage> associatedMessages = (LinkedList<ValidationMessage>)messageMap.get(key);
            if (associatedMessages == null) {
                associatedMessages = new LinkedList<ValidationMessage>();
                messageMap.put(key, associatedMessages);
            }
            associatedMessages.add(message);
        }
        HashMap resultMap = new HashMap(messageMap.size());
        for (Map.Entry entry : messageMap.entrySet()) {
            Object key = entry.getKey();
            List messages = (List)entry.getValue();
            resultMap.put(key, new ValidationResult(messages, false));
        }
        return Collections.unmodifiableMap(resultMap);
    }

    public Severity getSeverity() {
        return ValidationResult.getSeverity(this.messageList);
    }

    public boolean hasMessages() {
        return !this.isEmpty();
    }

    public boolean hasErrors() {
        return ValidationResult.hasSeverity(this.messageList, Severity.ERROR);
    }

    public boolean hasWarnings() {
        return ValidationResult.hasSeverity(this.messageList, Severity.WARNING);
    }

    public List<ValidationMessage> getMessages() {
        return Collections.unmodifiableList(this.messageList);
    }

    public List<ValidationMessage> getErrors() {
        return ValidationResult.getMessagesWithSeverity(this.messageList, Severity.ERROR);
    }

    public List<ValidationMessage> getWarnings() {
        return ValidationResult.getMessagesWithSeverity(this.messageList, Severity.WARNING);
    }

    public boolean isModifiable() {
        return this.modifiable;
    }

    public String getMessagesText() {
        return ValidationResult.getMessagesText(this.messageList);
    }

    public String toString() {
        if (this.isEmpty()) {
            return "Empty ValidationResult";
        }
        StringBuilder builder = new StringBuilder();
        builder.append(this.modifiable ? "Modifiable" : "Unmodifiable");
        builder.append(" ValidationResult:");
        for (ValidationMessage message : this.messageList) {
            builder.append("\n\t").append(message);
        }
        return builder.toString();
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ValidationResult)) {
            return false;
        }
        return this.messageList.equals(((ValidationResult)o).messageList);
    }

    public int hashCode() {
        return this.messageList.hashCode();
    }

    private void assertModifiable() {
        if (!this.modifiable) {
            throw new UnsupportedOperationException("This validation result is unmodifiable.");
        }
    }

    private static Severity getSeverity(List<ValidationMessage> messages) {
        if (messages.isEmpty()) {
            return Severity.OK;
        }
        for (ValidationMessage message : messages) {
            if (message.severity() != Severity.ERROR) continue;
            return Severity.ERROR;
        }
        return Severity.WARNING;
    }

    private static boolean hasSeverity(List<ValidationMessage> messages, Severity severity) {
        for (ValidationMessage message : messages) {
            if (message.severity() != severity) continue;
            return true;
        }
        return false;
    }

    private static List<ValidationMessage> getMessagesWithSeverity(List<ValidationMessage> messages, Severity severity) {
        ArrayList<ValidationMessage> errorMessages = new ArrayList<ValidationMessage>();
        for (ValidationMessage message : messages) {
            if (message.severity() != severity) continue;
            errorMessages.add(message);
        }
        return Collections.unmodifiableList(errorMessages);
    }

    private static String getMessagesText(List<ValidationMessage> messages) {
        if (messages.isEmpty()) {
            return "OK";
        }
        StringBuilder builder = new StringBuilder();
        for (ValidationMessage message : messages) {
            if (builder.length() > 0) {
                builder.append("\n");
            }
            builder.append(message.formattedText());
        }
        return builder.toString();
    }
}

