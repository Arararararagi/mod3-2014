/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation.message;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import java.io.Serializable;

public abstract class AbstractValidationMessage
implements ValidationMessage,
Serializable {
    private final Severity severity;
    private final String text;
    private Object key;

    protected AbstractValidationMessage(String text, Severity severity) {
        this(text, severity, null);
    }

    protected AbstractValidationMessage(String text, Severity severity, Object key) {
        if (severity == Severity.OK) {
            throw new IllegalArgumentException("Cannot create a validation messages with Severity.OK.");
        }
        this.text = text;
        this.severity = severity;
        this.setKey(key);
    }

    public final Severity severity() {
        return this.severity;
    }

    public String formattedText() {
        return this.text();
    }

    protected final String text() {
        return this.text;
    }

    public Object key() {
        return this.key;
    }

    protected final void setKey(Object associationKey) {
        this.key = associationKey;
    }

    public String toString() {
        return this.getClass().getName() + ": " + this.formattedText();
    }
}

