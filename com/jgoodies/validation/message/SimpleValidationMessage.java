/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation.message;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.message.AbstractValidationMessage;
import com.jgoodies.validation.util.ValidationUtils;

public final class SimpleValidationMessage
extends AbstractValidationMessage {
    public SimpleValidationMessage(String text) {
        this(text, Severity.WARNING);
    }

    public SimpleValidationMessage(String text, Severity severity) {
        this(text, severity, null);
    }

    public SimpleValidationMessage(String text, Severity severity, Object key) {
        super(text, severity, key);
        if (text == null) {
            throw new NullPointerException("The text must not be null");
        }
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof SimpleValidationMessage)) {
            return false;
        }
        SimpleValidationMessage other = (SimpleValidationMessage)o;
        return this.severity().equals((Object)other.severity()) && ValidationUtils.equals(this.key(), other.key()) && ValidationUtils.equals(this.formattedText(), other.formattedText());
    }

    public int hashCode() {
        String formattedText = this.formattedText();
        int result = 17;
        result = 37 * result + this.severity().hashCode();
        result = 37 * result + (this.key() == null ? 0 : this.key().hashCode());
        result = 37 * result + (formattedText == null ? 0 : formattedText.hashCode());
        return result;
    }
}

