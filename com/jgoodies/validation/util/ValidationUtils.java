/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class ValidationUtils {
    private ValidationUtils() {
    }

    public static boolean equals(Object o1, Object o2) {
        return o1 != null && o2 != null && o1.equals(o2) || o1 == null && o2 == null;
    }

    public static boolean isBlank(String str) {
        int length;
        if (str == null || (length = str.length()) == 0) {
            return true;
        }
        for (int i = length - 1; i >= 0; --i) {
            if (Character.isWhitespace(str.charAt(i))) continue;
            return false;
        }
        return true;
    }

    public static boolean isNotBlank(String str) {
        int length;
        if (str == null || (length = str.length()) == 0) {
            return false;
        }
        for (int i = length - 1; i >= 0; --i) {
            if (Character.isWhitespace(str.charAt(i))) continue;
            return true;
        }
        return false;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(String str) {
        return str != null && str.length() > 0;
    }

    public static boolean hasMinimumLength(String str, int min) {
        int length = str == null ? 0 : str.trim().length();
        return min <= length;
    }

    public static boolean hasMaximumLength(String str, int max) {
        int length = str == null ? 0 : str.trim().length();
        return length <= max;
    }

    public static boolean hasBoundedLength(String str, int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("The minimum length must be less than or equal to the maximum length.");
        }
        int length = str == null ? 0 : str.trim().length();
        return min <= length && length <= max;
    }

    public static boolean isAlpha(String str) {
        if (str == null) {
            return false;
        }
        for (int i = str.length() - 1; i >= 0; --i) {
            if (Character.isLetter(str.charAt(i))) continue;
            return false;
        }
        return true;
    }

    public static boolean isAlphaSpace(String str) {
        if (str == null) {
            return false;
        }
        for (int i = str.length() - 1; i >= 0; --i) {
            char c = str.charAt(i);
            if (Character.isLetter(c) || c == ' ') continue;
            return false;
        }
        return true;
    }

    public static boolean isAlphanumeric(String str) {
        if (str == null) {
            return false;
        }
        for (int i = str.length() - 1; i >= 0; --i) {
            if (Character.isLetterOrDigit(str.charAt(i))) continue;
            return false;
        }
        return true;
    }

    public static boolean isAlphanumericSpace(String str) {
        if (str == null) {
            return false;
        }
        for (int i = str.length() - 1; i >= 0; --i) {
            char c = str.charAt(i);
            if (Character.isLetterOrDigit(c) || c == ' ') continue;
            return false;
        }
        return true;
    }

    public static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        for (int i = str.length() - 1; i >= 0; --i) {
            if (Character.isDigit(str.charAt(i))) continue;
            return false;
        }
        return true;
    }

    public static boolean isNumericSpace(String str) {
        if (str == null) {
            return false;
        }
        for (int i = str.length() - 1; i >= 0; --i) {
            char c = str.charAt(i);
            if (Character.isDigit(c) || c == ' ') continue;
            return false;
        }
        return true;
    }

    public static boolean isPastDay(Date date) {
        GregorianCalendar in = new GregorianCalendar();
        in.setTime(date);
        Calendar today = ValidationUtils.getRelativeCalendar(0);
        return in.before(today);
    }

    public static boolean isYesterday(Date date) {
        GregorianCalendar in = new GregorianCalendar();
        in.setTime(date);
        Calendar yesterday = ValidationUtils.getRelativeCalendar(-1);
        Calendar today = ValidationUtils.getRelativeCalendar(0);
        return !in.before(yesterday) && in.before(today);
    }

    public static boolean isToday(Date date) {
        GregorianCalendar in = new GregorianCalendar();
        in.setTime(date);
        Calendar today = ValidationUtils.getRelativeCalendar(0);
        Calendar tomorrow = ValidationUtils.getRelativeCalendar(1);
        return !in.before(today) && in.before(tomorrow);
    }

    public static boolean isTomorrow(Date date) {
        GregorianCalendar in = new GregorianCalendar();
        in.setTime(date);
        Calendar tomorrow = ValidationUtils.getRelativeCalendar(1);
        Calendar dayAfter = ValidationUtils.getRelativeCalendar(2);
        return !in.before(tomorrow) && in.before(dayAfter);
    }

    public static boolean isFutureDay(Date date) {
        GregorianCalendar in = new GregorianCalendar();
        in.setTime(date);
        Calendar tomorrow = ValidationUtils.getRelativeCalendar(1);
        return !in.before(tomorrow);
    }

    public static Date getRelativeDate(int offsetDays) {
        return ValidationUtils.getRelativeCalendar(offsetDays).getTime();
    }

    public static Calendar getRelativeCalendar(int offsetDays) {
        GregorianCalendar today = new GregorianCalendar();
        return ValidationUtils.getRelativeCalendar(today, offsetDays);
    }

    public static Calendar getRelativeCalendar(Calendar from, int offsetDays) {
        GregorianCalendar temp = new GregorianCalendar(from.get(1), from.get(2), from.get(5), 0, 0, 0);
        temp.add(5, offsetDays);
        return temp;
    }
}

