/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation;

import com.jgoodies.validation.Severity;

public interface ValidationMessage {
    public Severity severity();

    public String formattedText();

    public Object key();
}

