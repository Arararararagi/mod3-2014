/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation;

import com.jgoodies.validation.ValidationResult;

public interface Validator<T> {
    public ValidationResult validate(T var1);
}

