/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.validation.view;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.ValidationUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.text.JTextComponent;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ValidationComponentUtils {
    private static final Color MANDATORY_FOREGROUND = new Color(70, 70, 210);
    private static final Color MANDATORY_BACKGROUND = new Color(235, 235, 255);
    private static final Color ERROR_BACKGROUND = new Color(255, 215, 215);
    private static final Color WARNING_BACKGROUND = new Color(255, 235, 205);
    private static final String MANDATORY_KEY = "validation.isMandatory";
    private static final String MESSAGE_KEYS = "validation.messageKeys";
    private static final String INPUT_HINT_KEY = "validation.inputHint";
    private static final String SEVERITY_KEY = "validation.severity";
    private static final String STORED_BACKGROUND_KEY = "validation.storedBackground";
    private static Border mandatoryBorder;
    private static final Map<Class<? extends JTextComponent>, JTextComponent> PROTOTYPE_COMPONENTS;
    private static boolean lafChangeHandlerRegistered;

    private ValidationComponentUtils() {
    }

    public static boolean isMandatory(JComponent comp) {
        return Boolean.TRUE.equals(comp.getClientProperty("validation.isMandatory"));
    }

    public static boolean isMandatoryAndBlank(JComponent comp) {
        if (!(comp instanceof JTextComponent)) {
            return false;
        }
        JTextComponent textComponent = (JTextComponent)comp;
        return ValidationComponentUtils.isMandatory(textComponent) && ValidationUtils.isBlank(textComponent.getText());
    }

    public static void setMandatory(JComponent comp, boolean mandatory) {
        boolean oldMandatory = ValidationComponentUtils.isMandatory(comp);
        if (oldMandatory != mandatory) {
            comp.putClientProperty("validation.isMandatory", mandatory);
        }
    }

    public static Severity getSeverity(JComponent comp) {
        return (Severity)comp.getClientProperty("validation.severity");
    }

    public static void setSeverity(JComponent comp, Severity severity) {
        comp.putClientProperty("validation.severity", (Object)severity);
    }

    public static Object[] getMessageKeys(JComponent comp) {
        return (Object[])comp.getClientProperty("validation.messageKeys");
    }

    public static void setMessageKey(JComponent comp, Object messageKey) {
        Object[] arrobject;
        if (messageKey == null) {
            arrobject = null;
        } else {
            Object[] arrobject2 = new Object[1];
            arrobject = arrobject2;
            arrobject2[0] = messageKey;
        }
        Object[] keyArray = arrobject;
        ValidationComponentUtils.setMessageKeys(comp, keyArray);
    }

    public static /* varargs */ void setMessageKeys(JComponent comp, Object ... messageKeys) {
        comp.putClientProperty("validation.messageKeys", messageKeys);
    }

    public static Object getInputHint(JComponent comp) {
        return comp.getClientProperty("validation.inputHint");
    }

    public static void setInputHint(JComponent comp, Object hint) {
        comp.putClientProperty("validation.inputHint", hint);
    }

    public static boolean hasError(JComponent comp, ValidationResult result) {
        return result.subResult(ValidationComponentUtils.getMessageKeys(comp)).hasErrors();
    }

    public static boolean hasWarning(JComponent comp, ValidationResult result) {
        return result.subResult(ValidationComponentUtils.getMessageKeys(comp)).hasWarnings();
    }

    public static Color getMandatoryBackground() {
        return MANDATORY_BACKGROUND;
    }

    public static Color getMandatoryForeground() {
        return MANDATORY_FOREGROUND;
    }

    public static void setMandatoryBackground(JTextComponent comp) {
        comp.setBackground(MANDATORY_BACKGROUND);
    }

    public static Color getErrorBackground() {
        return ERROR_BACKGROUND;
    }

    public static void setErrorBackground(JTextComponent comp) {
        comp.setBackground(ERROR_BACKGROUND);
    }

    public static Color getWarningBackground() {
        return WARNING_BACKGROUND;
    }

    public static void setWarningBackground(JTextComponent comp) {
        comp.setBackground(WARNING_BACKGROUND);
    }

    public static void setMandatoryBorder(JTextComponent comp) {
        Container grandpa;
        Container parent = comp.getParent();
        if (parent instanceof JViewport && (grandpa = parent.getParent()) instanceof JScrollPane) {
            ((JScrollPane)grandpa).setBorder(ValidationComponentUtils.getMandatoryBorder());
            return;
        }
        comp.setBorder(ValidationComponentUtils.getMandatoryBorder());
    }

    public static Border getMandatoryBorder() {
        if (mandatoryBorder == null) {
            mandatoryBorder = new CompoundBorder(new LineBorder(ValidationComponentUtils.getMandatoryForeground()), new BasicBorders.MarginBorder());
        }
        return mandatoryBorder;
    }

    public static void updateComponentTreeMandatoryBackground(Container container) {
        ValidationComponentUtils.visitComponentTree(container, null, new MandatoryBackgroundVisitor());
    }

    public static void updateComponentTreeMandatoryAndBlankBackground(Container container) {
        ValidationComponentUtils.visitComponentTree(container, null, new MandatoryAndBlankBackgroundVisitor());
    }

    public static void updateComponentTreeMandatoryBorder(Container container) {
        ValidationComponentUtils.visitComponentTree(container, null, new MandatoryBorderVisitor());
    }

    public static void updateComponentTreeSeverityBackground(Container container, ValidationResult result) {
        ValidationComponentUtils.visitComponentTree(container, result.keyMap(), new SeverityBackgroundVisitor());
    }

    public static void updateComponentTreeSeverity(Container container, ValidationResult result) {
        ValidationComponentUtils.visitComponentTree(container, result.keyMap(), new SeverityVisitor());
    }

    public static void visitComponentTree(Container container, Map<Object, ValidationResult> keyMap, Visitor visitor) {
        int componentCount = container.getComponentCount();
        for (int i = 0; i < componentCount; ++i) {
            Component child = container.getComponent(i);
            if (child instanceof JTextComponent) {
                JComponent component = (JComponent)child;
                visitor.visit(component, keyMap);
                continue;
            }
            if (!(child instanceof Container)) continue;
            ValidationComponentUtils.visitComponentTree((Container)child, keyMap, visitor);
        }
    }

    public static ValidationResult getAssociatedResult(JComponent comp, Map<Object, ValidationResult> keyMap) {
        Object[] messageKeys = ValidationComponentUtils.getMessageKeys(comp);
        if (messageKeys == null || keyMap == null) {
            return null;
        }
        if (messageKeys.length == 1) {
            ValidationResult result = keyMap.get(messageKeys[0]);
            return result == null ? ValidationResult.EMPTY : result;
        }
        ValidationResult result = null;
        for (Object element : messageKeys) {
            ValidationResult subResult = keyMap.get(element);
            if (subResult == null) continue;
            if (result == null) {
                result = new ValidationResult();
            }
            result.addAllFrom(subResult);
        }
        return result == null ? ValidationResult.EMPTY : ValidationResult.unmodifiableResult(result);
    }

    private static Color getDefaultBackground(JTextComponent component) {
        JTextComponent prototype = ValidationComponentUtils.getPrototypeFor(component.getClass());
        prototype.setEnabled(component.isEnabled());
        prototype.setEditable(component.isEditable());
        return prototype.getBackground();
    }

    private static JTextComponent getPrototypeFor(Class<? extends JTextComponent> prototypeClass) {
        ValidationComponentUtils.ensureLookAndFeelChangeHandlerRegistered();
        JTextComponent prototype = PROTOTYPE_COMPONENTS.get(prototypeClass);
        if (prototype == null) {
            try {
                prototype = prototypeClass.newInstance();
            }
            catch (Exception e) {
                prototype = new JTextField();
            }
            PROTOTYPE_COMPONENTS.put(prototypeClass, prototype);
        }
        return prototype;
    }

    private static Color getStoredBackground(JTextComponent comp) {
        return (Color)comp.getClientProperty("validation.storedBackground");
    }

    private static void ensureCustomBackgroundStored(JTextComponent comp) {
        if (ValidationComponentUtils.getStoredBackground(comp) != null) {
            return;
        }
        Color background = comp.getBackground();
        if (background == null || background instanceof UIResource || background == WARNING_BACKGROUND || background == ERROR_BACKGROUND) {
            return;
        }
        comp.putClientProperty("validation.storedBackground", background);
    }

    private static void restoreBackground(JTextComponent comp) {
        Color storedBackground = ValidationComponentUtils.getStoredBackground(comp);
        comp.setBackground(storedBackground == null ? ValidationComponentUtils.getDefaultBackground(comp) : storedBackground);
    }

    private static synchronized void ensureLookAndFeelChangeHandlerRegistered() {
        if (!lafChangeHandlerRegistered) {
            UIManager.addPropertyChangeListener(new LookAndFeelChangeHandler());
            lafChangeHandlerRegistered = true;
        }
    }

    static {
        PROTOTYPE_COMPONENTS = new HashMap<Class<? extends JTextComponent>, JTextComponent>();
        lafChangeHandlerRegistered = false;
    }

    private static final class LookAndFeelChangeHandler
    implements PropertyChangeListener {
        private LookAndFeelChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            String propertyName = evt.getPropertyName();
            if (propertyName == null || propertyName.equals("lookAndFeel")) {
                PROTOTYPE_COMPONENTS.clear();
            }
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static final class MandatoryAndBlankBackgroundVisitor
    implements Visitor {
        private MandatoryAndBlankBackgroundVisitor() {
        }

        @Override
        public void visit(JComponent component, Map<Object, ValidationResult> keyMap) {
            JTextComponent textChild = (JTextComponent)component;
            if (ValidationComponentUtils.isMandatoryAndBlank(textChild)) {
                ValidationComponentUtils.setMandatoryBackground(textChild);
            } else {
                ValidationComponentUtils.restoreBackground(textChild);
            }
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static final class MandatoryBackgroundVisitor
    implements Visitor {
        private MandatoryBackgroundVisitor() {
        }

        @Override
        public void visit(JComponent component, Map<Object, ValidationResult> keyMap) {
            if (component instanceof JTextComponent && ValidationComponentUtils.isMandatory(component)) {
                ValidationComponentUtils.setMandatoryBackground((JTextComponent)component);
            }
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static final class MandatoryBorderVisitor
    implements Visitor {
        private MandatoryBorderVisitor() {
        }

        @Override
        public void visit(JComponent component, Map<Object, ValidationResult> keyMap) {
            if (component instanceof JTextComponent && ValidationComponentUtils.isMandatory(component)) {
                ValidationComponentUtils.setMandatoryBorder((JTextComponent)component);
            }
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static final class SeverityBackgroundVisitor
    implements Visitor {
        private SeverityBackgroundVisitor() {
        }

        @Override
        public void visit(JComponent component, Map<Object, ValidationResult> keyMap) {
            Object[] messageKeys = ValidationComponentUtils.getMessageKeys(component);
            if (messageKeys == null) {
                return;
            }
            JTextComponent textChild = (JTextComponent)component;
            ValidationComponentUtils.ensureCustomBackgroundStored(textChild);
            ValidationResult result = ValidationComponentUtils.getAssociatedResult(component, keyMap);
            if (result == null || result.isEmpty()) {
                ValidationComponentUtils.restoreBackground(textChild);
            } else if (result.hasErrors()) {
                ValidationComponentUtils.setErrorBackground(textChild);
            } else if (result.hasWarnings()) {
                ValidationComponentUtils.setWarningBackground(textChild);
            }
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static final class SeverityVisitor
    implements Visitor {
        private SeverityVisitor() {
        }

        @Override
        public void visit(JComponent component, Map<Object, ValidationResult> keyMap) {
            ValidationResult result = ValidationComponentUtils.getAssociatedResult(component, keyMap);
            Severity severity = result == null ? null : result.getSeverity();
            ValidationComponentUtils.setSeverity(component, severity);
        }
    }

    public static interface Visitor {
        public void visit(JComponent var1, Map<Object, ValidationResult> var2);
    }

}

