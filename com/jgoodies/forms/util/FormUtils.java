/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.forms.util;

import com.jgoodies.forms.util.DefaultUnitConverter;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;

public final class FormUtils {
    private static LookAndFeel cachedLookAndFeel;
    private static Boolean cachedIsLafAqua;

    private FormUtils() {
    }

    public static boolean isLafAqua() {
        FormUtils.ensureValidCache();
        if (cachedIsLafAqua == null) {
            cachedIsLafAqua = FormUtils.computeIsLafAqua();
        }
        return cachedIsLafAqua;
    }

    public static void clearLookAndFeelBasedCaches() {
        cachedIsLafAqua = null;
        DefaultUnitConverter.getInstance().clearCache();
    }

    private static boolean computeIsLafAqua() {
        return UIManager.getLookAndFeel().getID().equals("Aqua");
    }

    static void ensureValidCache() {
        LookAndFeel currentLookAndFeel = UIManager.getLookAndFeel();
        if (currentLookAndFeel != cachedLookAndFeel) {
            FormUtils.clearLookAndFeelBasedCaches();
            cachedLookAndFeel = currentLookAndFeel;
        }
    }
}

