/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding;

public final class BindingUtils {
    private BindingUtils() {
    }

    public static boolean equals(Object o1, Object o2) {
        return o1 == o2 || o1 != null && o2 != null && o1.equals(o2);
    }

    public static boolean isBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}

