/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding;

import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.BeanUtils;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ComponentValueModel;
import com.jgoodies.binding.value.Trigger;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class PresentationModel<B>
extends Model {
    public static final String PROPERTYNAME_BEFORE_BEAN = "beforeBean";
    public static final String PROPERTYNAME_BEAN = "bean";
    public static final String PROPERTYNAME_AFTER_BEAN = "afterBean";
    public static final String PROPERTYNAME_TRIGGERCHANNEL = "triggerChannel";
    public static final String PROPERTYNAME_BUFFERING = "buffering";
    public static final String PROPERTYNAME_CHANGED = "changed";
    private final BeanAdapter<B> beanAdapter;
    private ValueModel triggerChannel;
    private final Map<String, WrappedBuffer> wrappedBuffers;
    private final PropertyChangeListener bufferingUpdateHandler;
    private boolean buffering = false;
    private final PropertyChangeListener changedUpdateHandler;
    private boolean changed = false;
    private final Map<String, ComponentValueModel> componentModels;
    private final Map<String, ComponentValueModel> bufferedComponentModels;

    public PresentationModel(B bean) {
        this(new ValueHolder(bean, true));
    }

    public PresentationModel(B bean, ValueModel triggerChannel) {
        this(new ValueHolder(bean, true), triggerChannel);
    }

    public PresentationModel(ValueModel beanChannel) {
        this(beanChannel, (ValueModel)new Trigger());
    }

    public PresentationModel(ValueModel beanChannel, ValueModel triggerChannel) {
        this.beanAdapter = this.createBeanAdapter(beanChannel);
        this.triggerChannel = triggerChannel;
        this.wrappedBuffers = new HashMap<String, WrappedBuffer>();
        this.componentModels = new HashMap<String, ComponentValueModel>();
        this.bufferedComponentModels = new HashMap<String, ComponentValueModel>();
        this.bufferingUpdateHandler = new BufferingStateHandler();
        this.changed = false;
        this.changedUpdateHandler = new UpdateHandler();
        this.beanAdapter.addPropertyChangeListener((PropertyChangeListener)new BeanChangeHandler());
        this.observeChanged(this.beanAdapter, "changed");
    }

    protected BeanAdapter<B> createBeanAdapter(ValueModel beanChannel) {
        return new BeanAdapter(beanChannel, true);
    }

    public ValueModel getBeanChannel() {
        return this.beanAdapter.getBeanChannel();
    }

    public B getBean() {
        return (B)this.getBeanChannel().getValue();
    }

    public void setBean(B newBean) {
        this.getBeanChannel().setValue(newBean);
    }

    public void beforeBeanChange(B oldBean, B newBean) {
        this.firePropertyChange("beforeBean", oldBean, newBean, true);
    }

    public void afterBeanChange(B oldBean, B newBean) {
        this.setChanged(false);
        this.firePropertyChange("afterBean", oldBean, newBean, true);
    }

    public Object getValue(String propertyName) {
        return this.beanAdapter.getValue(propertyName);
    }

    public void setValue(String propertyName, Object newValue) {
        this.beanAdapter.setValue(propertyName, newValue);
    }

    public void setVetoableValue(String propertyName, Object newValue) throws PropertyVetoException {
        this.beanAdapter.setVetoableValue(propertyName, newValue);
    }

    public Object getBufferedValue(String propertyName) {
        return this.getBufferedModel(propertyName).getValue();
    }

    public void setBufferedValue(String propertyName, Object newValue) {
        this.getBufferedModel(propertyName).setValue(newValue);
    }

    public AbstractValueModel getModel(String propertyName) {
        return this.beanAdapter.getValueModel(propertyName);
    }

    public AbstractValueModel getModel(String propertyName, String getterName, String setterName) {
        return this.beanAdapter.getValueModel(propertyName, getterName, setterName);
    }

    public ComponentValueModel getComponentModel(String propertyName) {
        ComponentValueModel componentModel = this.componentModels.get(propertyName);
        if (componentModel == null) {
            AbstractValueModel model = this.getModel(propertyName);
            componentModel = new ComponentValueModel(model);
            this.componentModels.put(propertyName, componentModel);
        }
        return componentModel;
    }

    public BufferedValueModel getBufferedModel(String propertyName) {
        return this.getBufferedModel(propertyName, null, null);
    }

    public BufferedValueModel getBufferedModel(String propertyName, String getterName, String setterName) {
        WrappedBuffer wrappedBuffer = this.wrappedBuffers.get(propertyName);
        if (wrappedBuffer == null) {
            wrappedBuffer = new WrappedBuffer(this.buffer(this.getModel(propertyName, getterName, setterName)), getterName, setterName);
            this.wrappedBuffers.put(propertyName, wrappedBuffer);
        } else if (!(this.equals(getterName, wrappedBuffer.getterName) && this.equals(setterName, wrappedBuffer.setterName))) {
            throw new IllegalArgumentException("You must not invoke this method twice with different getter and/or setter names.");
        }
        return wrappedBuffer.buffer;
    }

    public ComponentValueModel getBufferedComponentModel(String propertyName) {
        ComponentValueModel bufferedComponentModel = this.bufferedComponentModels.get(propertyName);
        if (bufferedComponentModel == null) {
            BufferedValueModel model = this.getBufferedModel(propertyName);
            bufferedComponentModel = new ComponentValueModel(model);
            this.bufferedComponentModels.put(propertyName, bufferedComponentModel);
        }
        return bufferedComponentModel;
    }

    private BufferedValueModel buffer(ValueModel valueModel) {
        BufferedValueModel bufferedModel = new BufferedValueModel(valueModel, this.getTriggerChannel());
        bufferedModel.addPropertyChangeListener("buffering", this.bufferingUpdateHandler);
        return bufferedModel;
    }

    public ValueModel getTriggerChannel() {
        return this.triggerChannel;
    }

    public void setTriggerChannel(ValueModel newTriggerChannel) {
        if (newTriggerChannel == null) {
            throw new NullPointerException("The trigger channel must not be null.");
        }
        ValueModel oldTriggerChannel = this.getTriggerChannel();
        this.triggerChannel = newTriggerChannel;
        for (WrappedBuffer wrappedBuffer : this.wrappedBuffers.values()) {
            wrappedBuffer.buffer.setTriggerChannel(this.triggerChannel);
        }
        this.firePropertyChange("triggerChannel", oldTriggerChannel, newTriggerChannel);
    }

    public void triggerCommit() {
        if (Boolean.TRUE.equals(this.getTriggerChannel().getValue())) {
            this.getTriggerChannel().setValue(null);
        }
        this.getTriggerChannel().setValue(Boolean.TRUE);
    }

    public void triggerFlush() {
        if (Boolean.FALSE.equals(this.getTriggerChannel().getValue())) {
            this.getTriggerChannel().setValue(null);
        }
        this.getTriggerChannel().setValue(Boolean.FALSE);
    }

    public boolean isBuffering() {
        return this.buffering;
    }

    private void setBuffering(boolean newValue) {
        boolean oldValue = this.isBuffering();
        this.buffering = newValue;
        this.firePropertyChange("buffering", oldValue, newValue);
    }

    private void updateBufferingState(boolean latestBufferingStateChange) {
        if (this.buffering == latestBufferingStateChange) {
            return;
        }
        boolean nowBuffering = false;
        for (WrappedBuffer wrappedBuffer : this.wrappedBuffers.values()) {
            BufferedValueModel model = wrappedBuffer.buffer;
            boolean bl = nowBuffering = nowBuffering || model.isBuffering();
            if (this.buffering || !nowBuffering) continue;
            this.setBuffering(true);
            return;
        }
        this.setBuffering(nowBuffering);
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void resetChanged() {
        this.setChanged(false);
        this.beanAdapter.resetChanged();
    }

    protected void setChanged(boolean newValue) {
        boolean oldValue = this.isChanged();
        this.changed = newValue;
        this.firePropertyChange("changed", oldValue, newValue);
    }

    public void observeChanged(Object bean, String propertyName) {
        if (bean == null) {
            throw new NullPointerException("The bean must not be null.");
        }
        if (propertyName == null) {
            throw new NullPointerException("The property name must not be null.");
        }
        BeanUtils.addPropertyChangeListener(bean, propertyName, this.changedUpdateHandler);
    }

    public void observeChanged(ValueModel valueModel) {
        if (valueModel == null) {
            throw new NullPointerException("The ValueModel must not be null.");
        }
        valueModel.addValueChangeListener(this.changedUpdateHandler);
    }

    public void retractInterestFor(Object bean, String propertyName) {
        if (bean == null) {
            throw new NullPointerException("The bean must not be null.");
        }
        if (propertyName == null) {
            throw new NullPointerException("The property name must not be null.");
        }
        BeanUtils.removePropertyChangeListener(bean, propertyName, this.changedUpdateHandler);
    }

    public void retractInterestFor(ValueModel valueModel) {
        if (valueModel == null) {
            throw new NullPointerException("The ValueModel must not be null.");
        }
        valueModel.removeValueChangeListener(this.changedUpdateHandler);
    }

    public synchronized void addBeanPropertyChangeListener(PropertyChangeListener listener) {
        this.beanAdapter.addBeanPropertyChangeListener(listener);
    }

    public synchronized void removeBeanPropertyChangeListener(PropertyChangeListener listener) {
        this.beanAdapter.removeBeanPropertyChangeListener(listener);
    }

    public synchronized void addBeanPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.beanAdapter.addBeanPropertyChangeListener(propertyName, listener);
    }

    public synchronized void removeBeanPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.beanAdapter.removeBeanPropertyChangeListener(propertyName, listener);
    }

    public synchronized PropertyChangeListener[] getBeanPropertyChangeListeners() {
        return this.beanAdapter.getBeanPropertyChangeListeners();
    }

    public synchronized PropertyChangeListener[] getBeanPropertyChangeListeners(String propertyName) {
        return this.beanAdapter.getBeanPropertyChangeListeners(propertyName);
    }

    public void release() {
        this.beanAdapter.release();
    }

    private final class BeanChangeHandler
    implements PropertyChangeListener {
        private BeanChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object oldBean = evt.getOldValue();
            Object newBean = evt.getNewValue();
            String propertyName = evt.getPropertyName();
            if ("beforeBean".equals(propertyName)) {
                PresentationModel.this.beforeBeanChange(oldBean, newBean);
            } else if ("bean".equals(propertyName)) {
                PresentationModel.this.firePropertyChange("bean", oldBean, newBean, true);
            } else if ("afterBean".equals(propertyName)) {
                PresentationModel.this.afterBeanChange(oldBean, newBean);
            }
        }
    }

    private final class BufferingStateHandler
    implements PropertyChangeListener {
        private BufferingStateHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            PresentationModel.this.updateBufferingState((Boolean)evt.getNewValue());
        }
    }

    private final class UpdateHandler
    implements PropertyChangeListener {
        private UpdateHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            String propertyName = evt.getPropertyName();
            if (!"changed".equals(propertyName) || ((Boolean)evt.getNewValue()).booleanValue()) {
                PresentationModel.this.setChanged(true);
            }
        }
    }

    private static final class WrappedBuffer {
        final BufferedValueModel buffer;
        final String getterName;
        final String setterName;

        WrappedBuffer(BufferedValueModel buffer, String getterName, String setterName) {
            this.buffer = buffer;
            this.getterName = getterName;
            this.setterName = setterName;
        }
    }

}

