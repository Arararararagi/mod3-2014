/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import java.beans.PropertyChangeListener;

public interface Observable {
    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

