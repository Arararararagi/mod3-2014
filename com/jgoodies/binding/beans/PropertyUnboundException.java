/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.PropertyException;

public final class PropertyUnboundException
extends PropertyException {
    public PropertyUnboundException(String message) {
        super(message);
    }

    public PropertyUnboundException(String message, Throwable cause) {
        super(message, cause);
    }
}

