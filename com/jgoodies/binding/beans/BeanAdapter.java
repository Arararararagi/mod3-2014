/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.BeanUtils;
import com.jgoodies.binding.beans.IndirectPropertyChangeSupport;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyUnboundException;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.PropertyVetoException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class BeanAdapter<B>
extends Model {
    public static final String PROPERTYNAME_BEFORE_BEAN = "beforeBean";
    public static final String PROPERTYNAME_BEAN = "bean";
    public static final String PROPERTYNAME_AFTER_BEAN = "afterBean";
    public static final String PROPERTYNAME_CHANGED = "changed";
    private final ValueModel beanChannel;
    private final boolean observeChanges;
    private final Map<String, BeanAdapter<B>> propertyAdapters;
    private IndirectPropertyChangeSupport indirectChangeSupport;
    B storedOldBean;
    private boolean changed = false;
    private PropertyChangeListener propertyChangeHandler;

    public BeanAdapter(B bean) {
        this(bean, false);
    }

    public BeanAdapter(B bean, boolean observeChanges) {
        this(new ValueHolder(bean, true), observeChanges);
    }

    public BeanAdapter(ValueModel beanChannel) {
        this(beanChannel, false);
    }

    public BeanAdapter(ValueModel beanChannel, boolean observeChanges) {
        this.beanChannel = beanChannel != null ? beanChannel : new ValueHolder(null, true);
        this.checkBeanChannelIdentityCheck(beanChannel);
        this.observeChanges = observeChanges;
        this.propertyAdapters = new HashMap<String, BeanAdapter<B>>();
        this.beanChannel.addValueChangeListener(new BeanChangeHandler());
        B initialBean = this.getBean();
        if (initialBean != null) {
            if (observeChanges && !BeanUtils.supportsBoundProperties(this.getBeanClass(initialBean))) {
                throw new PropertyUnboundException("The bean must provide support for listening on property changes as described in section 7.4.5 of the Java Bean Specification.");
            }
            this.addChangeHandlerTo(initialBean);
        }
        this.storedOldBean = initialBean;
    }

    public ValueModel getBeanChannel() {
        return this.beanChannel;
    }

    public B getBean() {
        return (B)this.beanChannel.getValue();
    }

    public void setBean(B newBean) {
        this.beanChannel.setValue(newBean);
        this.resetChanged();
    }

    public boolean getObserveChanges() {
        return this.observeChanges;
    }

    public Object getValue(String propertyName) {
        return this.getValueModel(propertyName).getValue();
    }

    public void setValue(String propertyName, Object newValue) {
        this.getValueModel(propertyName).setValue(newValue);
    }

    public void setVetoableValue(String propertyName, Object newValue) throws PropertyVetoException {
        this.getValueModel(propertyName).setVetoableValue(newValue);
    }

    public BeanAdapter<B> getValueModel(String propertyName) {
        return this.getValueModel(propertyName, null, null);
    }

    public BeanAdapter<B> getValueModel(String propertyName, String getterName, String setterName) {
        if (propertyName == null) {
            throw new NullPointerException("The property name must not be null.");
        }
        SimplePropertyAdapter adaptingModel = this.getPropertyAdapter(propertyName);
        if (adaptingModel == null) {
            adaptingModel = this.createPropertyAdapter(propertyName, getterName, setterName);
            this.propertyAdapters.put(propertyName, (BeanAdapter<B>)adaptingModel);
        } else if (!(this.equals(getterName, adaptingModel.getterName) && this.equals(setterName, adaptingModel.setterName))) {
            throw new IllegalArgumentException("You must not invoke this method twice with different getter and/or setter names.");
        }
        return adaptingModel;
    }

    BeanAdapter<B> getPropertyAdapter(String propertyName) {
        return (SimplePropertyAdapter)this.propertyAdapters.get(propertyName);
    }

    protected BeanAdapter<B> createPropertyAdapter(String propertyName, String getterName, String setterName) {
        return new SimplePropertyAdapter(propertyName, getterName, setterName);
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void resetChanged() {
        this.setChanged(false);
    }

    private void setChanged(boolean newValue) {
        boolean oldValue = this.isChanged();
        this.changed = newValue;
        this.firePropertyChange("changed", oldValue, newValue);
    }

    public synchronized void addBeanPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        if (this.indirectChangeSupport == null) {
            this.indirectChangeSupport = new IndirectPropertyChangeSupport(this.beanChannel);
        }
        this.indirectChangeSupport.addPropertyChangeListener(listener);
    }

    public synchronized void removeBeanPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null || this.indirectChangeSupport == null) {
            return;
        }
        this.indirectChangeSupport.removePropertyChangeListener(listener);
    }

    public synchronized void addBeanPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        if (this.indirectChangeSupport == null) {
            this.indirectChangeSupport = new IndirectPropertyChangeSupport(this.beanChannel);
        }
        this.indirectChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    public synchronized void removeBeanPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listener == null || this.indirectChangeSupport == null) {
            return;
        }
        this.indirectChangeSupport.removePropertyChangeListener(propertyName, listener);
    }

    public synchronized PropertyChangeListener[] getBeanPropertyChangeListeners() {
        if (this.indirectChangeSupport == null) {
            return new PropertyChangeListener[0];
        }
        return this.indirectChangeSupport.getPropertyChangeListeners();
    }

    public synchronized PropertyChangeListener[] getBeanPropertyChangeListeners(String propertyName) {
        if (this.indirectChangeSupport == null) {
            return new PropertyChangeListener[0];
        }
        return this.indirectChangeSupport.getPropertyChangeListeners(propertyName);
    }

    public synchronized void release() {
        this.removeChangeHandlerFrom(this.getBean());
        if (this.indirectChangeSupport != null) {
            this.indirectChangeSupport.removeAll();
        }
    }

    private void setBean0(B oldBean, B newBean) {
        this.firePropertyChange("beforeBean", oldBean, newBean, true);
        this.removeChangeHandlerFrom(oldBean);
        this.forwardAllAdaptedValuesChanged(oldBean, newBean);
        this.resetChanged();
        this.addChangeHandlerTo(newBean);
        this.firePropertyChange("bean", oldBean, newBean, true);
        this.firePropertyChange("afterBean", oldBean, newBean, true);
    }

    private void forwardAllAdaptedValuesChanged(B oldBean, B newBean) {
        Object[] adapters;
        for (Object adapter : adapters = this.propertyAdapters.values().toArray()) {
            ((SimplePropertyAdapter)adapter).setBean0(oldBean, newBean);
        }
    }

    private void forwardAllAdaptedValuesChanged() {
        Object[] adapters;
        B currentBean = this.getBean();
        for (Object adapter : adapters = this.propertyAdapters.values().toArray()) {
            ((SimplePropertyAdapter)adapter).fireChange(currentBean);
        }
    }

    private void addChangeHandlerTo(B bean) {
        if (!(this.observeChanges && bean != null)) {
            return;
        }
        this.propertyChangeHandler = new PropertyChangeHandler();
        BeanUtils.addPropertyChangeListener(bean, this.getBeanClass(bean), this.propertyChangeHandler);
    }

    private void removeChangeHandlerFrom(B bean) {
        if (!(this.observeChanges && bean != null && this.propertyChangeHandler != null)) {
            return;
        }
        BeanUtils.removePropertyChangeListener(bean, this.getBeanClass(bean), this.propertyChangeHandler);
        this.propertyChangeHandler = null;
    }

    private Class<?> getBeanClass(B bean) {
        return bean.getClass();
    }

    private Object getValue0(B bean, PropertyDescriptor propertyDescriptor) {
        return bean == null ? null : BeanUtils.getValue(bean, propertyDescriptor);
    }

    private void setValue0(B bean, PropertyDescriptor propertyDescriptor, Object newValue) throws PropertyVetoException {
        BeanUtils.setValue(bean, propertyDescriptor, newValue);
    }

    private void checkBeanChannelIdentityCheck(ValueModel valueModel) {
        if (!(valueModel instanceof ValueHolder)) {
            return;
        }
        ValueHolder valueHolder = (ValueHolder)valueModel;
        if (!valueHolder.isIdentityCheckEnabled()) {
            throw new IllegalArgumentException("The bean channel must have the identity check enabled.");
        }
    }

    private final class BeanChangeHandler
    implements PropertyChangeListener {
        private BeanChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object newBean = evt.getNewValue() != null ? evt.getNewValue() : BeanAdapter.this.getBean();
            BeanAdapter.this.setBean0(BeanAdapter.this.storedOldBean, newBean);
            BeanAdapter.this.storedOldBean = newBean;
        }
    }

    private final class PropertyChangeHandler
    implements PropertyChangeListener {
        private PropertyChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            BeanAdapter.this.setChanged(true);
            String propertyName = evt.getPropertyName();
            if (propertyName == null) {
                BeanAdapter.this.forwardAllAdaptedValuesChanged();
            } else {
                SimplePropertyAdapter adapter = BeanAdapter.this.getPropertyAdapter(propertyName);
                if (adapter != null) {
                    adapter.fireValueChange(evt.getOldValue(), evt.getNewValue(), true);
                }
            }
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    public class SimplePropertyAdapter
    extends AbstractValueModel {
        private final String propertyName;
        final String getterName;
        final String setterName;
        private PropertyDescriptor cachedPropertyDescriptor;
        private Class<?> cachedBeanClass;

        protected SimplePropertyAdapter(String propertyName, String getterName, String setterName) {
            this.propertyName = propertyName;
            this.getterName = getterName;
            this.setterName = setterName;
            Object bean = BeanAdapter.this.getBean();
            if (bean != null) {
                this.getPropertyDescriptor(bean);
            }
        }

        @Override
        public Object getValue() {
            Object bean = BeanAdapter.this.getBean();
            return bean == null ? null : BeanAdapter.this.getValue0(bean, this.getPropertyDescriptor(bean));
        }

        @Override
        public void setValue(Object newValue) {
            Object bean = BeanAdapter.this.getBean();
            if (bean == null) {
                return;
            }
            try {
                BeanAdapter.this.setValue0(bean, this.getPropertyDescriptor(bean), newValue);
            }
            catch (PropertyVetoException e) {
                // empty catch block
            }
        }

        public void setVetoableValue(Object newValue) throws PropertyVetoException {
            Object bean = BeanAdapter.this.getBean();
            if (bean == null) {
                return;
            }
            BeanAdapter.this.setValue0(bean, this.getPropertyDescriptor(bean), newValue);
        }

        private PropertyDescriptor getPropertyDescriptor(B bean) {
            Class beanClass = BeanAdapter.this.getBeanClass(bean);
            if (this.cachedPropertyDescriptor == null || beanClass != this.cachedBeanClass) {
                this.cachedPropertyDescriptor = BeanUtils.getPropertyDescriptor(beanClass, this.propertyName, this.getterName, this.setterName);
                this.cachedBeanClass = beanClass;
            }
            return this.cachedPropertyDescriptor;
        }

        protected void fireChange(B currentBean) {
            Object newValue;
            if (currentBean == null) {
                newValue = null;
            } else {
                PropertyDescriptor propertyDescriptor = this.getPropertyDescriptor(currentBean);
                boolean isWriteOnly = null == propertyDescriptor.getReadMethod();
                newValue = isWriteOnly ? null : BeanAdapter.this.getValue0(currentBean, propertyDescriptor);
            }
            this.fireValueChange(null, newValue);
        }

        protected void setBean0(B oldBean, B newBean) {
            Object newValue;
            Object oldValue;
            boolean isWriteOnly;
            PropertyDescriptor propertyDescriptor;
            if (oldBean == null) {
                oldValue = null;
            } else {
                propertyDescriptor = this.getPropertyDescriptor(oldBean);
                isWriteOnly = null == propertyDescriptor.getReadMethod();
                Object object = oldValue = isWriteOnly ? null : BeanAdapter.this.getValue0(oldBean, propertyDescriptor);
            }
            if (newBean == null) {
                newValue = null;
            } else {
                propertyDescriptor = this.getPropertyDescriptor(newBean);
                isWriteOnly = null == propertyDescriptor.getReadMethod();
                Object object = newValue = isWriteOnly ? null : BeanAdapter.this.getValue0(newBean, propertyDescriptor);
            }
            if (oldValue != null || newValue != null) {
                this.fireValueChange(oldValue, newValue, true);
            }
        }

        @Override
        protected String paramString() {
            Object bean = BeanAdapter.this.getBean();
            String beanType = null;
            Object value = this.getValue();
            String valueType = null;
            String propertyDescriptorName = null;
            String propertyType = null;
            Method propertySetter = null;
            if (bean != null) {
                beanType = bean.getClass().getName();
                valueType = value == null ? null : value.getClass().getName();
                PropertyDescriptor propertyDescriptor = this.getPropertyDescriptor(bean);
                propertyDescriptorName = propertyDescriptor.getName();
                propertyType = propertyDescriptor.getPropertyType().getName();
                propertySetter = propertyDescriptor.getWriteMethod();
            }
            return "bean=" + bean + "; bean type=" + beanType + "; value=" + value + "; value type=" + valueType + "; property name=" + propertyDescriptorName + "; property type=" + propertyType + "; property setter=" + propertySetter;
        }
    }

}

