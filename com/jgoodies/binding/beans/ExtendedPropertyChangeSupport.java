/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeListenerProxy;
import java.beans.PropertyChangeSupport;

public final class ExtendedPropertyChangeSupport
extends PropertyChangeSupport {
    private final Object source;
    private final boolean checkIdentityDefault;

    public ExtendedPropertyChangeSupport(Object sourceBean) {
        this(sourceBean, false);
    }

    public ExtendedPropertyChangeSupport(Object sourceBean, boolean checkIdentityDefault) {
        super(sourceBean);
        this.source = sourceBean;
        this.checkIdentityDefault = checkIdentityDefault;
    }

    public void firePropertyChange(PropertyChangeEvent evt) {
        this.firePropertyChange(evt, this.checkIdentityDefault);
    }

    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.firePropertyChange(propertyName, oldValue, newValue, this.checkIdentityDefault);
    }

    public void firePropertyChange(PropertyChangeEvent evt, boolean checkIdentity) {
        Object oldValue = evt.getOldValue();
        Object newValue = evt.getNewValue();
        if (oldValue != null && oldValue == newValue) {
            return;
        }
        this.firePropertyChange0(evt, checkIdentity);
    }

    public void firePropertyChange(String propertyName, Object oldValue, Object newValue, boolean checkIdentity) {
        if (oldValue != null && oldValue == newValue) {
            return;
        }
        this.firePropertyChange0(propertyName, oldValue, newValue, checkIdentity);
    }

    private void firePropertyChange0(PropertyChangeEvent evt, boolean checkIdentity) {
        if (checkIdentity) {
            this.fireUnchecked(evt);
        } else {
            super.firePropertyChange(evt);
        }
    }

    private void firePropertyChange0(String propertyName, Object oldValue, Object newValue, boolean checkIdentity) {
        if (checkIdentity) {
            this.fireUnchecked(new PropertyChangeEvent(this.source, propertyName, oldValue, newValue));
        } else {
            super.firePropertyChange(propertyName, oldValue, newValue);
        }
    }

    private void fireUnchecked(PropertyChangeEvent evt) {
        PropertyChangeListener[] listeners;
        ExtendedPropertyChangeSupport extendedPropertyChangeSupport = this;
        synchronized (extendedPropertyChangeSupport) {
            listeners = this.getPropertyChangeListeners();
        }
        String propertyName = evt.getPropertyName();
        for (PropertyChangeListener listener : listeners) {
            if (listener instanceof PropertyChangeListenerProxy) {
                PropertyChangeListenerProxy proxy = (PropertyChangeListenerProxy)listener;
                if (!proxy.getPropertyName().equals(propertyName)) continue;
                proxy.propertyChange(evt);
                continue;
            }
            listener.propertyChange(evt);
        }
    }
}

