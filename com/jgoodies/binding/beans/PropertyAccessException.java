/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.PropertyException;
import java.beans.PropertyDescriptor;

public final class PropertyAccessException
extends PropertyException {
    public PropertyAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public static PropertyAccessException createReadAccessException(Object bean, PropertyDescriptor propertyDescriptor, Throwable cause) {
        String beanType = bean == null ? null : bean.getClass().getName();
        String message = "Failed to read an adapted Java Bean property.\ncause=" + cause + "\nbean=" + bean + "\nbean type=" + beanType + "\nproperty name=" + propertyDescriptor.getName() + "\nproperty type=" + propertyDescriptor.getPropertyType().getName() + "\nproperty reader=" + propertyDescriptor.getReadMethod();
        return new PropertyAccessException(message, cause);
    }

    public static PropertyAccessException createWriteAccessException(Object bean, Object value, PropertyDescriptor propertyDescriptor, Throwable cause) {
        String beanType = bean == null ? null : bean.getClass().getName();
        String valueType = value == null ? null : value.getClass().getName();
        String message = "Failed to set an adapted Java Bean property.\ncause=" + cause + "\nbean=" + bean + "\nbean type=" + beanType + "\nvalue=" + value + "\nvalue type=" + valueType + "\nproperty name=" + propertyDescriptor.getName() + "\nproperty type=" + propertyDescriptor.getPropertyType().getName() + "\nproperty setter=" + propertyDescriptor.getWriteMethod();
        return new PropertyAccessException(message, cause);
    }
}

