/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAccessException;
import com.jgoodies.binding.beans.PropertyNotBindableException;
import com.jgoodies.binding.beans.PropertyNotFoundException;
import com.jgoodies.binding.beans.PropertyUnboundException;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.PropertyVetoException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class BeanUtils {
    private static final Class<?>[] PCL_PARAMS = new Class[]{PropertyChangeListener.class};
    private static final Class<?>[] NAMED_PCL_PARAMS = new Class[]{String.class, PropertyChangeListener.class};

    private BeanUtils() {
    }

    public static boolean supportsBoundProperties(Class<?> clazz) {
        return BeanUtils.getPCLAdder(clazz) != null && BeanUtils.getPCLRemover(clazz) != null;
    }

    public static PropertyDescriptor getPropertyDescriptor(Class<?> beanClass, String propertyName) throws IntrospectionException {
        BeanInfo info = Introspector.getBeanInfo(beanClass);
        for (PropertyDescriptor element : info.getPropertyDescriptors()) {
            if (!propertyName.equals(element.getName())) continue;
            return element;
        }
        throw new IntrospectionException("Property '" + propertyName + "' not found in bean " + beanClass);
    }

    public static PropertyDescriptor getPropertyDescriptor(Class<?> beanClass, String propertyName, String getterName, String setterName) {
        try {
            return getterName != null || setterName != null ? new PropertyDescriptor(propertyName, beanClass, getterName, setterName) : BeanUtils.getPropertyDescriptor(beanClass, propertyName);
        }
        catch (IntrospectionException e) {
            throw new PropertyNotFoundException(propertyName, beanClass, (Throwable)e);
        }
    }

    public static Method getPCLAdder(Class<?> clazz) {
        try {
            return clazz.getMethod("addPropertyChangeListener", PCL_PARAMS);
        }
        catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static Method getPCLRemover(Class<?> clazz) {
        try {
            return clazz.getMethod("removePropertyChangeListener", PCL_PARAMS);
        }
        catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static Method getNamedPCLAdder(Class<?> clazz) {
        try {
            return clazz.getMethod("addPropertyChangeListener", NAMED_PCL_PARAMS);
        }
        catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static Method getNamedPCLRemover(Class<?> clazz) {
        try {
            return clazz.getMethod("removePropertyChangeListener", NAMED_PCL_PARAMS);
        }
        catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static void addPropertyChangeListener(Object bean, Class<?> beanClass, PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException("The listener must not be null.");
        }
        if (beanClass == null) {
            beanClass = bean.getClass();
        } else if (!beanClass.isInstance(bean)) {
            throw new IllegalArgumentException("The bean " + bean + " must be an instance of " + beanClass);
        }
        if (bean instanceof Model) {
            ((Model)bean).addPropertyChangeListener(listener);
            return;
        }
        if (!BeanUtils.supportsBoundProperties(beanClass)) {
            throw new PropertyUnboundException("Bound properties unsupported by bean class=" + beanClass + "\nThe Bean class must provide a pair of methods:" + "\npublic void addPropertyChangeListener(PropertyChangeListener x);" + "\npublic void removePropertyChangeListener(PropertyChangeListener x);");
        }
        Method multicastPCLAdder = BeanUtils.getPCLAdder(beanClass);
        try {
            multicastPCLAdder.invoke(bean, listener);
        }
        catch (InvocationTargetException e) {
            throw new PropertyNotBindableException("Due to an InvocationTargetException we failed to add a multicast PropertyChangeListener to bean: " + bean, e.getCause());
        }
        catch (IllegalAccessException e) {
            throw new PropertyNotBindableException("Due to an IllegalAccessException we failed to add a multicast PropertyChangeListener to bean: " + bean, e);
        }
    }

    public static void addPropertyChangeListener(Object bean, Class<?> beanClass, String propertyName, PropertyChangeListener listener) {
        if (propertyName == null) {
            throw new NullPointerException("The property name must not be null.");
        }
        if (listener == null) {
            throw new NullPointerException("The listener must not be null.");
        }
        if (beanClass == null) {
            beanClass = bean.getClass();
        } else if (!beanClass.isInstance(bean)) {
            throw new IllegalArgumentException("The bean " + bean + " must be an instance of " + beanClass);
        }
        if (bean instanceof Model) {
            ((Model)bean).addPropertyChangeListener(propertyName, listener);
            return;
        }
        Method namedPCLAdder = BeanUtils.getNamedPCLAdder(beanClass);
        if (namedPCLAdder == null) {
            throw new PropertyNotBindableException("Could not find the bean method/npublic void addPropertyChangeListener(String, PropertyChangeListener);/nin bean:" + bean);
        }
        try {
            namedPCLAdder.invoke(bean, propertyName, listener);
        }
        catch (InvocationTargetException e) {
            throw new PropertyNotBindableException("Due to an InvocationTargetException we failed to add a named PropertyChangeListener to bean: " + bean, e.getCause());
        }
        catch (IllegalAccessException e) {
            throw new PropertyNotBindableException("Due to an IllegalAccessException we failed to add a named PropertyChangeListener to bean: " + bean, e);
        }
    }

    public static void addPropertyChangeListener(Object bean, PropertyChangeListener listener) {
        BeanUtils.addPropertyChangeListener(bean, bean.getClass(), listener);
    }

    public static void addPropertyChangeListener(Object bean, String propertyName, PropertyChangeListener listener) {
        BeanUtils.addPropertyChangeListener(bean, bean.getClass(), propertyName, listener);
    }

    public static void removePropertyChangeListener(Object bean, Class<?> beanClass, PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException("The listener must not be null.");
        }
        if (beanClass == null) {
            beanClass = bean.getClass();
        } else if (!beanClass.isInstance(bean)) {
            throw new IllegalArgumentException("The bean " + bean + " must be an instance of " + beanClass);
        }
        if (bean instanceof Model) {
            ((Model)bean).removePropertyChangeListener(listener);
            return;
        }
        Method multicastPCLRemover = BeanUtils.getPCLRemover(beanClass);
        if (multicastPCLRemover == null) {
            throw new PropertyUnboundException("Could not find the method:\npublic void removePropertyChangeListener(String, PropertyChangeListener x);\nfor bean:" + bean);
        }
        try {
            multicastPCLRemover.invoke(bean, listener);
        }
        catch (InvocationTargetException e) {
            throw new PropertyNotBindableException("Due to an InvocationTargetException we failed to remove a multicast PropertyChangeListener from bean: " + bean, e.getCause());
        }
        catch (IllegalAccessException e) {
            throw new PropertyNotBindableException("Due to an IllegalAccessException we failed to remove a multicast PropertyChangeListener from bean: " + bean, e);
        }
    }

    public static void removePropertyChangeListener(Object bean, Class<?> beanClass, String propertyName, PropertyChangeListener listener) {
        if (propertyName == null) {
            throw new NullPointerException("The property name must not be null.");
        }
        if (listener == null) {
            throw new NullPointerException("The listener must not be null.");
        }
        if (beanClass == null) {
            beanClass = bean.getClass();
        } else if (!beanClass.isInstance(bean)) {
            throw new IllegalArgumentException("The bean " + bean + " must be an instance of " + beanClass);
        }
        if (bean instanceof Model) {
            ((Model)bean).removePropertyChangeListener(propertyName, listener);
            return;
        }
        Method namedPCLRemover = BeanUtils.getNamedPCLRemover(beanClass);
        if (namedPCLRemover == null) {
            throw new PropertyNotBindableException("Could not find the bean method/npublic void removePropertyChangeListener(String, PropertyChangeListener);/nin bean:" + bean);
        }
        try {
            namedPCLRemover.invoke(bean, propertyName, listener);
        }
        catch (InvocationTargetException e) {
            throw new PropertyNotBindableException("Due to an InvocationTargetException we failed to remove a named PropertyChangeListener from bean: " + bean, e.getCause());
        }
        catch (IllegalAccessException e) {
            throw new PropertyNotBindableException("Due to an IllegalAccessException we failed to remove a named PropertyChangeListener from bean: " + bean, e);
        }
    }

    public static void removePropertyChangeListener(Object bean, PropertyChangeListener listener) {
        BeanUtils.removePropertyChangeListener(bean, bean.getClass(), listener);
    }

    public static void removePropertyChangeListener(Object bean, String propertyName, PropertyChangeListener listener) {
        BeanUtils.removePropertyChangeListener(bean, bean.getClass(), propertyName, listener);
    }

    public static Object getValue(Object bean, PropertyDescriptor propertyDescriptor) {
        if (bean == null) {
            throw new NullPointerException("The bean must not be null.");
        }
        Method getter = propertyDescriptor.getReadMethod();
        if (getter == null) {
            throw new UnsupportedOperationException("The property '" + propertyDescriptor.getName() + "' is write-only.");
        }
        try {
            return getter.invoke(bean, null);
        }
        catch (InvocationTargetException e) {
            throw PropertyAccessException.createReadAccessException(bean, propertyDescriptor, e.getCause());
        }
        catch (IllegalAccessException e) {
            throw PropertyAccessException.createReadAccessException(bean, propertyDescriptor, e);
        }
    }

    public static void setValue(Object bean, PropertyDescriptor propertyDescriptor, Object newValue) throws PropertyVetoException {
        if (bean == null) {
            throw new NullPointerException("The bean must not be null.");
        }
        Method setter = propertyDescriptor.getWriteMethod();
        if (setter == null) {
            throw new UnsupportedOperationException("The property '" + propertyDescriptor.getName() + "' is read-only.");
        }
        try {
            setter.invoke(bean, newValue);
        }
        catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause instanceof PropertyVetoException) {
                throw (PropertyVetoException)cause;
            }
            throw PropertyAccessException.createWriteAccessException(bean, newValue, propertyDescriptor, cause);
        }
        catch (IllegalAccessException e) {
            throw PropertyAccessException.createWriteAccessException(bean, newValue, propertyDescriptor, e);
        }
        catch (IllegalArgumentException e) {
            throw PropertyAccessException.createWriteAccessException(bean, newValue, propertyDescriptor, e);
        }
    }
}

