/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.BeanUtils;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.PropertyVetoException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class PropertyAdapter<B>
extends AbstractValueModel {
    public static final String PROPERTYNAME_BEFORE_BEAN = "beforeBean";
    public static final String PROPERTYNAME_BEAN = "bean";
    public static final String PROPERTYNAME_AFTER_BEAN = "afterBean";
    public static final String PROPERTYNAME_CHANGED = "changed";
    private final ValueModel beanChannel;
    private final String propertyName;
    private final String getterName;
    private final String setterName;
    private final boolean observeChanges;
    private B storedOldBean;
    private boolean changed = false;
    private PropertyChangeListener propertyChangeHandler;
    private PropertyDescriptor cachedPropertyDescriptor;
    private Class<?> cachedBeanClass;

    public PropertyAdapter(B bean, String propertyName) {
        this(bean, propertyName, false);
    }

    public PropertyAdapter(B bean, String propertyName, boolean observeChanges) {
        this(bean, propertyName, null, null, observeChanges);
    }

    public PropertyAdapter(B bean, String propertyName, String getterName, String setterName) {
        this(bean, propertyName, getterName, setterName, false);
    }

    public PropertyAdapter(B bean, String propertyName, String getterName, String setterName, boolean observeChanges) {
        this(new ValueHolder(bean, true), propertyName, getterName, setterName, observeChanges);
    }

    public PropertyAdapter(ValueModel beanChannel, String propertyName) {
        this(beanChannel, propertyName, false);
    }

    public PropertyAdapter(ValueModel beanChannel, String propertyName, boolean observeChanges) {
        this(beanChannel, propertyName, (String)null, (String)null, observeChanges);
    }

    public PropertyAdapter(ValueModel beanChannel, String propertyName, String getterName, String setterName) {
        this(beanChannel, propertyName, getterName, setterName, false);
    }

    public PropertyAdapter(ValueModel beanChannel, String propertyName, String getterName, String setterName, boolean observeChanges) {
        this.beanChannel = beanChannel != null ? beanChannel : new ValueHolder(null, true);
        this.propertyName = propertyName;
        this.getterName = getterName;
        this.setterName = setterName;
        this.observeChanges = observeChanges;
        if (propertyName == null) {
            throw new NullPointerException("The property name must not be null.");
        }
        if (propertyName.length() == 0) {
            throw new IllegalArgumentException("The property name must not be empty.");
        }
        this.checkBeanChannelIdentityCheck(beanChannel);
        this.beanChannel.addValueChangeListener(new BeanChangeHandler());
        B initialBean = this.getBean();
        if (initialBean != null) {
            this.getPropertyDescriptor(initialBean);
            this.addChangeHandlerTo(initialBean);
        }
        this.storedOldBean = initialBean;
    }

    public B getBean() {
        return (B)this.beanChannel.getValue();
    }

    public void setBean(B newBean) {
        this.beanChannel.setValue(newBean);
    }

    public String getPropertyName() {
        return this.propertyName;
    }

    public boolean getObserveChanges() {
        return this.observeChanges;
    }

    @Override
    public Object getValue() {
        B bean = this.getBean();
        if (bean == null) {
            return null;
        }
        return this.getValue0(bean);
    }

    @Override
    public void setValue(Object newValue) {
        B bean = this.getBean();
        if (bean == null) {
            return;
        }
        try {
            this.setValue0(bean, newValue);
        }
        catch (PropertyVetoException e) {
            // empty catch block
        }
    }

    public void setVetoableValue(Object newValue) throws PropertyVetoException {
        B bean = this.getBean();
        if (bean == null) {
            return;
        }
        this.setValue0(this.getBean(), newValue);
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void resetChanged() {
        this.setChanged(false);
    }

    private void setChanged(boolean newValue) {
        boolean oldValue = this.isChanged();
        this.changed = newValue;
        this.firePropertyChange("changed", oldValue, newValue);
    }

    public void release() {
        this.removeChangeHandlerFrom(this.getBean());
    }

    @Override
    protected String paramString() {
        B bean = this.getBean();
        String beanType = null;
        Object value = this.getValue();
        String valueType = null;
        String propertyDescriptorName = null;
        String propertyType = null;
        Method propertySetter = null;
        if (bean != null) {
            beanType = bean.getClass().getName();
            valueType = value == null ? null : value.getClass().getName();
            PropertyDescriptor propertyDescriptor = this.getPropertyDescriptor(bean);
            propertyDescriptorName = propertyDescriptor.getName();
            propertyType = propertyDescriptor.getPropertyType().getName();
            propertySetter = propertyDescriptor.getWriteMethod();
        }
        return "bean=" + bean + "; bean type=" + beanType + "; value=" + value + "; value type=" + valueType + "; property name=" + propertyDescriptorName + "; property type=" + propertyType + "; property setter=" + propertySetter;
    }

    private void setBean0(B oldBean, B newBean) {
        this.firePropertyChange("beforeBean", oldBean, newBean, true);
        this.removeChangeHandlerFrom(oldBean);
        this.forwardAdaptedValueChanged(oldBean, newBean);
        this.resetChanged();
        this.addChangeHandlerTo(newBean);
        this.firePropertyChange("bean", oldBean, newBean, true);
        this.firePropertyChange("afterBean", oldBean, newBean, true);
    }

    private void forwardAdaptedValueChanged(B oldBean, B newBean) {
        Object newValue;
        Object oldValue = oldBean == null || this.isWriteOnlyProperty(oldBean) ? null : this.getValue0(oldBean);
        Object object = newValue = newBean == null || this.isWriteOnlyProperty(newBean) ? null : this.getValue0(newBean);
        if (oldValue != null || newValue != null) {
            this.fireValueChange(oldValue, newValue, true);
        }
    }

    private void forwardAdaptedValueChanged(B newBean) {
        Object newValue = newBean == null || this.isWriteOnlyProperty(newBean) ? null : this.getValue0(newBean);
        this.fireValueChange(null, newValue);
    }

    private void addChangeHandlerTo(B bean) {
        if (!(this.observeChanges && bean != null)) {
            return;
        }
        this.propertyChangeHandler = new PropertyChangeHandler();
        BeanUtils.addPropertyChangeListener(bean, this.getBeanClass(bean), this.propertyChangeHandler);
    }

    private void removeChangeHandlerFrom(B bean) {
        if (!(this.observeChanges && bean != null && this.propertyChangeHandler != null)) {
            return;
        }
        BeanUtils.removePropertyChangeListener(bean, this.getBeanClass(bean), this.propertyChangeHandler);
        this.propertyChangeHandler = null;
    }

    private Class<?> getBeanClass(B bean) {
        return bean.getClass();
    }

    private Object getValue0(B bean) {
        return bean == null ? null : BeanUtils.getValue(bean, this.getPropertyDescriptor(bean));
    }

    private void setValue0(B bean, Object newValue) throws PropertyVetoException {
        BeanUtils.setValue(bean, this.getPropertyDescriptor(bean), newValue);
    }

    private PropertyDescriptor getPropertyDescriptor(B bean) {
        Class beanClass = this.getBeanClass(bean);
        if (this.cachedPropertyDescriptor == null || beanClass != this.cachedBeanClass) {
            this.cachedPropertyDescriptor = BeanUtils.getPropertyDescriptor(beanClass, this.getPropertyName(), this.getterName, this.setterName);
            this.cachedBeanClass = beanClass;
        }
        return this.cachedPropertyDescriptor;
    }

    private boolean isWriteOnlyProperty(B bean) {
        return null == this.getPropertyDescriptor(bean).getReadMethod();
    }

    private void checkBeanChannelIdentityCheck(ValueModel valueModel) {
        if (!(valueModel instanceof ValueHolder)) {
            return;
        }
        ValueHolder valueHolder = (ValueHolder)valueModel;
        if (!valueHolder.isIdentityCheckEnabled()) {
            throw new IllegalArgumentException("The bean channel must have the identity check enabled.");
        }
    }

    private final class BeanChangeHandler
    implements PropertyChangeListener {
        private BeanChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object newBean = evt.getNewValue() != null ? evt.getNewValue() : PropertyAdapter.this.getBean();
            PropertyAdapter.this.setBean0(PropertyAdapter.this.storedOldBean, newBean);
            PropertyAdapter.this.storedOldBean = newBean;
        }
    }

    private final class PropertyChangeHandler
    implements PropertyChangeListener {
        private PropertyChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            PropertyAdapter.this.setChanged(true);
            if (evt.getPropertyName() == null) {
                PropertyAdapter.this.forwardAdaptedValueChanged(PropertyAdapter.this.getBean());
            } else if (evt.getPropertyName().equals(PropertyAdapter.this.getPropertyName())) {
                PropertyAdapter.this.fireValueChange(evt.getOldValue(), evt.getNewValue(), true);
            }
        }
    }

}

