/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.BeanUtils;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class IndirectPropertyChangeSupport {
    private final ValueModel beanChannel;
    private final List<PropertyChangeListener> listenerList;
    private final Map<String, List<PropertyChangeListener>> namedListeners;

    public IndirectPropertyChangeSupport() {
        this(new ValueHolder(null, true));
    }

    public IndirectPropertyChangeSupport(Object bean) {
        this(new ValueHolder(bean, true));
    }

    public IndirectPropertyChangeSupport(ValueModel beanChannel) {
        if (beanChannel == null) {
            throw new NullPointerException("The bean channel must not be null.");
        }
        this.beanChannel = beanChannel;
        this.listenerList = new ArrayList<PropertyChangeListener>();
        this.namedListeners = new HashMap<String, List<PropertyChangeListener>>();
        beanChannel.addValueChangeListener(new BeanChangeHandler());
    }

    public Object getBean() {
        return this.beanChannel.getValue();
    }

    public void setBean(Object newBean) {
        this.beanChannel.setValue(newBean);
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        this.listenerList.add(listener);
        Object bean = this.getBean();
        if (bean != null) {
            BeanUtils.addPropertyChangeListener(bean, listener);
        }
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        this.listenerList.remove(listener);
        Object bean = this.getBean();
        if (bean != null) {
            BeanUtils.removePropertyChangeListener(bean, listener);
        }
    }

    public synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        List<PropertyChangeListener> namedListenerList = this.namedListeners.get(propertyName);
        if (namedListenerList == null) {
            namedListenerList = new ArrayList<PropertyChangeListener>();
            this.namedListeners.put(propertyName, namedListenerList);
        }
        namedListenerList.add(listener);
        Object bean = this.getBean();
        if (bean != null) {
            BeanUtils.addPropertyChangeListener(bean, propertyName, listener);
        }
    }

    public synchronized void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        List<PropertyChangeListener> namedListenerList = this.namedListeners.get(propertyName);
        if (namedListenerList == null) {
            return;
        }
        namedListenerList.remove(listener);
        Object bean = this.getBean();
        if (bean != null) {
            BeanUtils.removePropertyChangeListener(bean, propertyName, listener);
        }
    }

    public synchronized PropertyChangeListener[] getPropertyChangeListeners() {
        if (this.listenerList.isEmpty()) {
            return new PropertyChangeListener[0];
        }
        return this.listenerList.toArray(new PropertyChangeListener[this.listenerList.size()]);
    }

    public synchronized PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
        List<PropertyChangeListener> namedListenerList = this.namedListeners.get(propertyName);
        if (namedListenerList == null || namedListenerList.isEmpty()) {
            return new PropertyChangeListener[0];
        }
        return namedListenerList.toArray(new PropertyChangeListener[namedListenerList.size()]);
    }

    public void removeAll() {
        this.removeAllListenersFrom(this.getBean());
    }

    private void setBean0(Object oldBean, Object newBean) {
        this.removeAllListenersFrom(oldBean);
        this.addAllListenersTo(newBean);
    }

    private void addAllListenersTo(Object bean) {
        if (bean == null) {
            return;
        }
        for (PropertyChangeListener listener : this.listenerList) {
            BeanUtils.addPropertyChangeListener(bean, listener);
        }
        for (Map.Entry entry : this.namedListeners.entrySet()) {
            String propertyName = (String)entry.getKey();
            for (PropertyChangeListener listener2 : (List)entry.getValue()) {
                BeanUtils.addPropertyChangeListener(bean, propertyName, listener2);
            }
        }
    }

    private void removeAllListenersFrom(Object bean) {
        if (bean == null) {
            return;
        }
        for (PropertyChangeListener listener : this.listenerList) {
            BeanUtils.removePropertyChangeListener(bean, listener);
        }
        for (Map.Entry entry : this.namedListeners.entrySet()) {
            String propertyName = (String)entry.getKey();
            for (PropertyChangeListener listener2 : (List)entry.getValue()) {
                BeanUtils.removePropertyChangeListener(bean, propertyName, listener2);
            }
        }
    }

    private final class BeanChangeHandler
    implements PropertyChangeListener {
        private BeanChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            IndirectPropertyChangeSupport.this.setBean0(evt.getOldValue(), evt.getNewValue());
        }
    }

}

