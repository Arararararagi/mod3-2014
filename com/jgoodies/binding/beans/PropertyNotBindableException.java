/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.PropertyException;

public final class PropertyNotBindableException
extends PropertyException {
    public PropertyNotBindableException(String message) {
        super(message);
    }

    public PropertyNotBindableException(String message, Throwable cause) {
        super(message, cause);
    }
}

