/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.beans.PropertyException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class PropertyNotFoundException
extends PropertyException {
    public PropertyNotFoundException(String propertyName, Object bean) {
        this(propertyName, bean, null);
    }

    public PropertyNotFoundException(String propertyName, Object bean, Throwable cause) {
        super("Property '" + propertyName + "' not found in bean " + bean, cause);
    }

    public PropertyNotFoundException(String propertyName, Class<?> beanClass, Throwable cause) {
        super("Property '" + propertyName + "' not found in bean class " + beanClass, cause);
    }
}

