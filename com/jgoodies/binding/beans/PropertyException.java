/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

public abstract class PropertyException
extends RuntimeException {
    public PropertyException(String message) {
        this(message, null);
    }

    public PropertyException(String message, Throwable cause) {
        super(message, cause);
    }
}

