/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.beans;

import com.jgoodies.binding.BindingUtils;
import com.jgoodies.binding.beans.BeanUtils;
import com.jgoodies.binding.beans.PropertyNotFoundException;
import com.jgoodies.binding.value.ValueModel;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.PropertyVetoException;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class PropertyConnector {
    private final Object bean1;
    private final Object bean2;
    private final Class<?> bean1Class;
    private final Class<?> bean2Class;
    private final String property1Name;
    private final String property2Name;
    private final PropertyChangeListener property1ChangeHandler;
    private final PropertyChangeListener property2ChangeHandler;
    private final PropertyDescriptor property1Descriptor;
    private final PropertyDescriptor property2Descriptor;

    private PropertyConnector(Object bean1, String property1Name, Object bean2, String property2Name) {
        boolean property2Readable;
        boolean property1Readable;
        if (bean1 == null) {
            throw new NullPointerException("Bean1 must not be null.");
        }
        if (bean2 == null) {
            throw new NullPointerException("Bean2 must not be null.");
        }
        if (property1Name == null) {
            throw new NullPointerException("PropertyName1 must not be null.");
        }
        if (property2Name == null) {
            throw new NullPointerException("PropertyName2 must not be null.");
        }
        if (bean1 == bean2 && property1Name.equals(property2Name)) {
            throw new IllegalArgumentException("Cannot connect a bean property to itself on the same bean.");
        }
        this.bean1 = bean1;
        this.bean2 = bean2;
        this.bean1Class = bean1.getClass();
        this.bean2Class = bean2.getClass();
        this.property1Name = property1Name;
        this.property2Name = property2Name;
        this.property1Descriptor = PropertyConnector.getPropertyDescriptor(this.bean1Class, property1Name);
        this.property2Descriptor = PropertyConnector.getPropertyDescriptor(this.bean2Class, property2Name);
        boolean property1Writable = this.property1Descriptor.getWriteMethod() != null;
        boolean bl = property1Readable = this.property1Descriptor.getReadMethod() != null;
        if (property1Writable && !property1Readable) {
            throw new IllegalArgumentException("Property1 must be readable.");
        }
        boolean property2Writable = this.property2Descriptor.getWriteMethod() != null;
        boolean bl2 = property2Readable = this.property2Descriptor.getReadMethod() != null;
        if (property2Writable && !property2Readable) {
            throw new IllegalArgumentException("Property2 must be readable.");
        }
        if (!(property1Writable || property2Writable)) {
            throw new IllegalArgumentException("Cannot connect two read-only properties.");
        }
        boolean property1Observable = BeanUtils.supportsBoundProperties(this.bean1Class);
        boolean property2Observable = BeanUtils.supportsBoundProperties(this.bean2Class);
        if (property1Observable && property2Writable) {
            this.property1ChangeHandler = new PropertyChangeHandler(bean1, this.property1Descriptor, bean2, this.property2Descriptor);
            PropertyConnector.addPropertyChangeHandler(bean1, this.bean1Class, this.property1ChangeHandler);
        } else {
            this.property1ChangeHandler = null;
        }
        if (property2Observable && property1Writable) {
            this.property2ChangeHandler = new PropertyChangeHandler(bean2, this.property2Descriptor, bean1, this.property1Descriptor);
            PropertyConnector.addPropertyChangeHandler(bean2, this.bean2Class, this.property2ChangeHandler);
        } else {
            this.property2ChangeHandler = null;
        }
    }

    public static PropertyConnector connect(Object bean1, String property1Name, Object bean2, String property2Name) {
        return new PropertyConnector(bean1, property1Name, bean2, property2Name);
    }

    public static void connectAndUpdate(ValueModel valueModel, Object bean2, String property2Name) {
        PropertyConnector connector = new PropertyConnector(valueModel, "value", bean2, property2Name);
        connector.updateProperty2();
    }

    public Object getBean1() {
        return this.bean1;
    }

    public Object getBean2() {
        return this.bean2;
    }

    public String getProperty1Name() {
        return this.property1Name;
    }

    public String getProperty2Name() {
        return this.property2Name;
    }

    public void updateProperty1() {
        Object property2Value = BeanUtils.getValue(this.bean2, this.property2Descriptor);
        this.setValueSilently(this.bean2, this.property2Descriptor, this.bean1, this.property1Descriptor, property2Value);
    }

    public void updateProperty2() {
        Object property1Value = BeanUtils.getValue(this.bean1, this.property1Descriptor);
        this.setValueSilently(this.bean1, this.property1Descriptor, this.bean2, this.property2Descriptor, property1Value);
    }

    public void release() {
        PropertyConnector.removePropertyChangeHandler(this.bean1, this.bean1Class, this.property1ChangeHandler);
        PropertyConnector.removePropertyChangeHandler(this.bean2, this.bean2Class, this.property2ChangeHandler);
    }

    private static void addPropertyChangeHandler(Object bean, Class<?> beanClass, PropertyChangeListener listener) {
        if (bean != null) {
            BeanUtils.addPropertyChangeListener(bean, beanClass, listener);
        }
    }

    private static void removePropertyChangeHandler(Object bean, Class<?> beanClass, PropertyChangeListener listener) {
        if (bean != null) {
            BeanUtils.removePropertyChangeListener(bean, beanClass, listener);
        }
    }

    private void setValueSilently(Object sourceBean, PropertyDescriptor sourcePropertyDescriptor, Object targetBean, PropertyDescriptor targetPropertyDescriptor, Object newValue) {
        Object targetValue = BeanUtils.getValue(targetBean, targetPropertyDescriptor);
        if (targetValue == newValue) {
            return;
        }
        if (this.property1ChangeHandler != null) {
            PropertyConnector.removePropertyChangeHandler(this.bean1, this.bean1Class, this.property1ChangeHandler);
        }
        if (this.property2ChangeHandler != null) {
            PropertyConnector.removePropertyChangeHandler(this.bean2, this.bean2Class, this.property2ChangeHandler);
        }
        try {
            BeanUtils.setValue(targetBean, targetPropertyDescriptor, newValue);
        }
        catch (PropertyVetoException e) {
            // empty catch block
        }
        targetValue = BeanUtils.getValue(targetBean, targetPropertyDescriptor);
        if (!BindingUtils.equals(targetValue, newValue)) {
            boolean sourcePropertyWritable;
            boolean bl = sourcePropertyWritable = sourcePropertyDescriptor.getWriteMethod() != null;
            if (sourcePropertyWritable) {
                try {
                    BeanUtils.setValue(sourceBean, sourcePropertyDescriptor, targetValue);
                }
                catch (PropertyVetoException e) {
                    // empty catch block
                }
            }
        }
        if (this.property1ChangeHandler != null) {
            PropertyConnector.addPropertyChangeHandler(this.bean1, this.bean1Class, this.property1ChangeHandler);
        }
        if (this.property2ChangeHandler != null) {
            PropertyConnector.addPropertyChangeHandler(this.bean2, this.bean2Class, this.property2ChangeHandler);
        }
    }

    private static PropertyDescriptor getPropertyDescriptor(Class<?> beanClass, String propertyName) {
        try {
            return BeanUtils.getPropertyDescriptor(beanClass, propertyName);
        }
        catch (IntrospectionException e) {
            throw new PropertyNotFoundException(propertyName, beanClass, (Throwable)e);
        }
    }

    private final class PropertyChangeHandler
    implements PropertyChangeListener {
        private final Object sourceBean;
        private final PropertyDescriptor sourcePropertyDescriptor;
        private final Object targetBean;
        private final PropertyDescriptor targetPropertyDescriptor;

        private PropertyChangeHandler(Object sourceBean, PropertyDescriptor sourcePropertyDescriptor, Object targetBean, PropertyDescriptor targetPropertyDescriptor) {
            this.sourceBean = sourceBean;
            this.sourcePropertyDescriptor = sourcePropertyDescriptor;
            this.targetBean = targetBean;
            this.targetPropertyDescriptor = targetPropertyDescriptor;
        }

        public void propertyChange(PropertyChangeEvent evt) {
            String sourcePropertyName = this.sourcePropertyDescriptor.getName();
            String propertyName = evt.getPropertyName();
            if (propertyName == null || propertyName.equals(sourcePropertyName)) {
                Object newValue = evt.getNewValue();
                if (newValue == null || propertyName == null) {
                    newValue = BeanUtils.getValue(this.sourceBean, this.sourcePropertyDescriptor);
                }
                PropertyConnector.this.setValueSilently(this.sourceBean, this.sourcePropertyDescriptor, this.targetBean, this.targetPropertyDescriptor, newValue);
            }
        }
    }

}

