/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.adapter;

import com.jgoodies.binding.BindingUtils;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JToggleButton;

public final class ToggleButtonAdapter
extends JToggleButton.ToggleButtonModel {
    private final ValueModel subject;
    private final Object selectedValue;
    private final Object deselectedValue;

    public ToggleButtonAdapter(ValueModel subject) {
        this(subject, Boolean.TRUE, Boolean.FALSE);
    }

    public ToggleButtonAdapter(ValueModel subject, Object selectedValue, Object deselectedValue) {
        if (subject == null) {
            throw new NullPointerException("The subject must not be null.");
        }
        if (BindingUtils.equals(selectedValue, deselectedValue)) {
            throw new IllegalArgumentException("The selected value must not equal the deselected value.");
        }
        this.subject = subject;
        this.selectedValue = selectedValue;
        this.deselectedValue = deselectedValue;
        subject.addValueChangeListener(new SubjectValueChangeHandler());
        this.updateSelectedState();
    }

    public void setSelected(boolean b) {
        this.subject.setValue(b ? this.selectedValue : this.deselectedValue);
        this.updateSelectedState();
    }

    private void updateSelectedState() {
        boolean subjectHoldsChoiceValue = BindingUtils.equals(this.selectedValue, this.subject.getValue());
        super.setSelected(subjectHoldsChoiceValue);
    }

    private final class SubjectValueChangeHandler
    implements PropertyChangeListener {
        private SubjectValueChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            ToggleButtonAdapter.this.updateSelectedState();
        }
    }

}

