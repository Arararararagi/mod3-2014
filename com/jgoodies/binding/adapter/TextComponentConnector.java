/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.adapter;

import com.jgoodies.binding.BindingUtils;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

public final class TextComponentConnector {
    private final ValueModel subject;
    private final JTextComponent textComponent;
    private Document document;
    private final SubjectValueChangeHandler subjectValueChangeHandler;
    private final DocumentListener textChangeHandler;
    private final PropertyChangeListener documentChangeHandler;

    public TextComponentConnector(ValueModel subject, JTextArea textArea) {
        this(subject, (JTextComponent)textArea);
    }

    public TextComponentConnector(ValueModel subject, JTextField textField) {
        this(subject, (JTextComponent)textField);
    }

    private TextComponentConnector(ValueModel subject, JTextComponent textComponent) {
        if (subject == null) {
            throw new NullPointerException("The subject must not be null.");
        }
        if (textComponent == null) {
            throw new NullPointerException("The text component must not be null.");
        }
        this.subject = subject;
        this.textComponent = textComponent;
        this.subjectValueChangeHandler = new SubjectValueChangeHandler();
        this.textChangeHandler = new TextChangeHandler();
        this.document = textComponent.getDocument();
        this.reregisterTextChangeHandler(null, this.document);
        subject.addValueChangeListener(this.subjectValueChangeHandler);
        this.documentChangeHandler = new DocumentChangeHandler();
        textComponent.addPropertyChangeListener("document", this.documentChangeHandler);
    }

    public static void connect(ValueModel subject, JTextArea textArea) {
        new TextComponentConnector(subject, textArea);
    }

    public static void connect(ValueModel subject, JTextField textField) {
        new TextComponentConnector(subject, textField);
    }

    public void updateSubject() {
        this.setSubjectText(this.getDocumentText());
    }

    public void updateTextComponent() {
        this.setDocumentTextSilently(this.getSubjectText());
    }

    private String getDocumentText() {
        return this.textComponent.getText();
    }

    private void setDocumentTextSilently(String newText) {
        this.textComponent.getDocument().removeDocumentListener(this.textChangeHandler);
        this.textComponent.setText(newText);
        this.textComponent.setCaretPosition(0);
        this.textComponent.getDocument().addDocumentListener(this.textChangeHandler);
    }

    private String getSubjectText() {
        String str = (String)this.subject.getValue();
        return str == null ? "" : str;
    }

    private void setSubjectText(String newText) {
        this.subjectValueChangeHandler.setUpdateLater(true);
        try {
            this.subject.setValue(newText);
        }
        finally {
            this.subjectValueChangeHandler.setUpdateLater(false);
        }
    }

    private void reregisterTextChangeHandler(Document oldDocument, Document newDocument) {
        if (oldDocument != null) {
            oldDocument.removeDocumentListener(this.textChangeHandler);
        }
        if (newDocument != null) {
            newDocument.addDocumentListener(this.textChangeHandler);
        }
    }

    public void release() {
        this.reregisterTextChangeHandler(this.document, null);
        this.subject.removeValueChangeListener(this.subjectValueChangeHandler);
        this.textComponent.removePropertyChangeListener("document", this.documentChangeHandler);
    }

    private final class DocumentChangeHandler
    implements PropertyChangeListener {
        private DocumentChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Document oldDocument = TextComponentConnector.this.document;
            Document newDocument = TextComponentConnector.this.textComponent.getDocument();
            TextComponentConnector.this.reregisterTextChangeHandler(oldDocument, newDocument);
            TextComponentConnector.this.document = newDocument;
        }
    }

    private final class SubjectValueChangeHandler
    implements PropertyChangeListener {
        private boolean updateLater;

        private SubjectValueChangeHandler() {
        }

        void setUpdateLater(boolean updateLater) {
            this.updateLater = updateLater;
        }

        public void propertyChange(PropertyChangeEvent evt) {
            String newText;
            String oldText = TextComponentConnector.this.getDocumentText();
            Object newValue = evt.getNewValue();
            String string = newText = newValue == null ? TextComponentConnector.this.getSubjectText() : (String)newValue;
            if (BindingUtils.equals(oldText, newText)) {
                return;
            }
            if (this.updateLater) {
                SwingUtilities.invokeLater(new Runnable(){

                    public void run() {
                        TextComponentConnector.this.setDocumentTextSilently(newText);
                    }
                });
            } else {
                TextComponentConnector.this.setDocumentTextSilently(newText);
            }
        }

    }

    private final class TextChangeHandler
    implements DocumentListener {
        private TextChangeHandler() {
        }

        public void insertUpdate(DocumentEvent e) {
            TextComponentConnector.this.updateSubject();
        }

        public void removeUpdate(DocumentEvent e) {
            TextComponentConnector.this.updateSubject();
        }

        public void changedUpdate(DocumentEvent e) {
        }
    }

}

