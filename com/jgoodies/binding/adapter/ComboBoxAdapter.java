/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.adapter;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ComboBoxAdapter<E>
extends AbstractListModel
implements ComboBoxModel {
    private final ListModel listModel;
    private ValueModel selectionHolder;
    private final PropertyChangeListener selectionChangeHandler;

    public ComboBoxAdapter(List<E> items, ValueModel selectionHolder) {
        this(new ListModelAdapter<E>(items), selectionHolder);
        if (items == null) {
            throw new NullPointerException("The list must not be null.");
        }
    }

    public ComboBoxAdapter(ListModel listModel, ValueModel selectionHolder) {
        if (listModel == null) {
            throw new NullPointerException("The ListModel must not be null.");
        }
        if (selectionHolder == null) {
            throw new NullPointerException("The selection holder must not be null.");
        }
        this.listModel = listModel;
        this.selectionHolder = selectionHolder;
        listModel.addListDataListener(new ListDataChangeHandler());
        this.selectionChangeHandler = new SelectionChangeHandler();
        this.setSelectionHolder(selectionHolder);
    }

    public ComboBoxAdapter(E[] items, ValueModel selectionHolder) {
        this(new ListModelAdapter<E>(items), selectionHolder);
    }

    public ComboBoxAdapter(SelectionInList<E> selectionInList) {
        this(selectionInList, selectionInList);
        selectionInList.addPropertyChangeListener("selectionHolder", (PropertyChangeListener)new SelectionHolderChangeHandler());
    }

    public E getSelectedItem() {
        return (E)this.selectionHolder.getValue();
    }

    @Override
    public void setSelectedItem(Object object) {
        this.selectionHolder.setValue(object);
    }

    @Override
    public int getSize() {
        return this.listModel.getSize();
    }

    @Override
    public E getElementAt(int index) {
        return this.listModel.getElementAt(index);
    }

    private void setSelectionHolder(ValueModel newSelectionHolder) {
        ValueModel oldSelectionHolder = this.selectionHolder;
        if (oldSelectionHolder != null) {
            oldSelectionHolder.removeValueChangeListener(this.selectionChangeHandler);
        }
        this.selectionHolder = newSelectionHolder;
        if (newSelectionHolder == null) {
            throw new NullPointerException("The selection holder must not be null.");
        }
        newSelectionHolder.addValueChangeListener(this.selectionChangeHandler);
    }

    private void fireContentsChanged() {
        this.fireContentsChanged(this, -1, -1);
    }

    private final class ListDataChangeHandler
    implements ListDataListener {
        private ListDataChangeHandler() {
        }

        public void intervalAdded(ListDataEvent evt) {
            ComboBoxAdapter.this.fireIntervalAdded(ComboBoxAdapter.this, evt.getIndex0(), evt.getIndex1());
        }

        public void intervalRemoved(ListDataEvent evt) {
            ComboBoxAdapter.this.fireIntervalRemoved(ComboBoxAdapter.this, evt.getIndex0(), evt.getIndex1());
        }

        public void contentsChanged(ListDataEvent evt) {
            ComboBoxAdapter.this.fireContentsChanged(ComboBoxAdapter.this, evt.getIndex0(), evt.getIndex1());
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static final class ListModelAdapter<E>
    extends AbstractListModel {
        private final List<E> aList;

        ListModelAdapter(List<E> list) {
            this.aList = list;
        }

        ListModelAdapter(E[] elements) {
            this(Arrays.asList(elements));
        }

        @Override
        public int getSize() {
            return this.aList.size();
        }

        @Override
        public E getElementAt(int index) {
            return this.aList.get(index);
        }
    }

    private final class SelectionChangeHandler
    implements PropertyChangeListener {
        private SelectionChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            ComboBoxAdapter.this.fireContentsChanged();
        }
    }

    private final class SelectionHolderChangeHandler
    implements PropertyChangeListener {
        private SelectionHolderChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            ComboBoxAdapter.this.setSelectionHolder((ValueModel)evt.getNewValue());
            ComboBoxAdapter.this.fireContentsChanged();
        }
    }

}

