/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.adapter;

import com.jgoodies.binding.value.ValueModel;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.colorchooser.DefaultColorSelectionModel;

public final class ColorSelectionAdapter
extends DefaultColorSelectionModel {
    private final ValueModel subject;
    private final Color defaultColor;

    public ColorSelectionAdapter(ValueModel subject) {
        this(subject, null);
    }

    public ColorSelectionAdapter(ValueModel subject, Color defaultColor) {
        if (subject == null) {
            throw new NullPointerException("The subject must not be null.");
        }
        this.subject = subject;
        this.defaultColor = defaultColor;
        subject.addValueChangeListener(new SubjectValueChangeHandler());
    }

    public Color getSelectedColor() {
        Color subjectColor = (Color)this.subject.getValue();
        return subjectColor != null ? subjectColor : this.defaultColor;
    }

    public void setSelectedColor(Color color) {
        this.subject.setValue(color);
    }

    private final class SubjectValueChangeHandler
    implements PropertyChangeListener {
        private SubjectValueChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            ColorSelectionAdapter.this.fireStateChanged();
        }
    }

}

