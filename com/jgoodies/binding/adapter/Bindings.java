/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.adapter;

import com.jgoodies.binding.adapter.ColorSelectionAdapter;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.adapter.RadioButtonAdapter;
import com.jgoodies.binding.adapter.SingleListSelectionAdapter;
import com.jgoodies.binding.adapter.TextComponentConnector;
import com.jgoodies.binding.adapter.ToggleButtonAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ComponentValueModel;
import com.jgoodies.binding.value.ValueModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeListenerProxy;
import java.beans.PropertyChangeSupport;
import javax.swing.ButtonModel;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.text.JTextComponent;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class Bindings {
    private static final String COMMIT_ON_FOCUS_LOST_MODEL_KEY = "commitOnFocusListModel";
    private static final String COMPONENT_VALUE_MODEL_KEY = "componentValueModel";
    private static final String COMPONENT_PROPERTY_HANDLER_KEY = "componentPropertyHandler";
    static final FocusLostHandler FOCUS_LOST_HANDLER = new FocusLostHandler();
    static final WeakTrigger FOCUS_LOST_TRIGGER = new WeakTrigger();

    private Bindings() {
    }

    public static void bind(JCheckBox checkBox, ValueModel valueModel) {
        boolean enabled = checkBox.getModel().isEnabled();
        checkBox.setModel(new ToggleButtonAdapter(valueModel));
        checkBox.setEnabled(enabled);
        Bindings.addComponentPropertyHandler(checkBox, valueModel);
    }

    public static void bind(JCheckBoxMenuItem checkBoxMenuItem, ValueModel valueModel) {
        boolean enabled = checkBoxMenuItem.getModel().isEnabled();
        checkBoxMenuItem.setModel(new ToggleButtonAdapter(valueModel));
        checkBoxMenuItem.setEnabled(enabled);
        Bindings.addComponentPropertyHandler(checkBoxMenuItem, valueModel);
    }

    public static void bind(JColorChooser colorChooser, ValueModel valueModel) {
        colorChooser.setSelectionModel(new ColorSelectionAdapter(valueModel));
    }

    public static void bind(JColorChooser colorChooser, ValueModel valueModel, Color defaultColor) {
        if (defaultColor == null) {
            throw new NullPointerException("The default color must not be null.");
        }
        colorChooser.setSelectionModel(new ColorSelectionAdapter(valueModel, defaultColor));
    }

    public static <E> void bind(JComboBox comboBox, SelectionInList<E> selectionInList) {
        if (selectionInList == null) {
            throw new NullPointerException("The SelectionInList must not be null.");
        }
        comboBox.setModel(new ComboBoxAdapter<E>(selectionInList));
        Bindings.addComponentPropertyHandler(comboBox, selectionInList.getSelectionHolder());
    }

    public static void bind(JFormattedTextField textField, ValueModel valueModel) {
        Bindings.bind((JComponent)textField, "value", valueModel);
    }

    public static void bind(JLabel label, ValueModel valueModel) {
        Bindings.bind(label, "text", valueModel);
    }

    public static <E> void bind(JList list, SelectionInList<E> selectionInList) {
        if (selectionInList == null) {
            throw new NullPointerException("The SelectionInList must not be null.");
        }
        list.setModel(selectionInList);
        list.setSelectionModel(new SingleListSelectionAdapter(selectionInList.getSelectionIndexHolder()));
        Bindings.addComponentPropertyHandler(list, selectionInList.getSelectionHolder());
    }

    public static void bind(JRadioButton radioButton, ValueModel model, Object choice) {
        boolean enabled = radioButton.getModel().isEnabled();
        radioButton.setModel(new RadioButtonAdapter(model, choice));
        radioButton.setEnabled(enabled);
        Bindings.addComponentPropertyHandler(radioButton, model);
    }

    public static void bind(JRadioButtonMenuItem radioButtonMenuItem, ValueModel model, Object choice) {
        boolean enabled = radioButtonMenuItem.getModel().isEnabled();
        radioButtonMenuItem.setModel(new RadioButtonAdapter(model, choice));
        radioButtonMenuItem.setEnabled(enabled);
        Bindings.addComponentPropertyHandler(radioButtonMenuItem, model);
    }

    public static void bind(JTextArea textArea, ValueModel valueModel) {
        Bindings.bind(textArea, valueModel, false);
    }

    public static void bind(JTextArea textArea, ValueModel valueModel, boolean commitOnFocusLost) {
        ValueModel textModel;
        if (valueModel == null) {
            throw new NullPointerException("The value model must not be null.");
        }
        if (commitOnFocusLost) {
            textModel = Bindings.createCommitOnFocusLostModel(valueModel, textArea);
            textArea.putClientProperty("commitOnFocusListModel", textModel);
        } else {
            textModel = valueModel;
        }
        TextComponentConnector connector = new TextComponentConnector(textModel, textArea);
        connector.updateTextComponent();
        Bindings.addComponentPropertyHandler(textArea, valueModel);
    }

    public static void bind(JTextField textField, ValueModel valueModel) {
        Bindings.bind(textField, valueModel, false);
    }

    public static void bind(JTextField textField, ValueModel valueModel, boolean commitOnFocusLost) {
        ValueModel textModel;
        if (valueModel == null) {
            throw new NullPointerException("The value model must not be null.");
        }
        if (commitOnFocusLost) {
            textModel = Bindings.createCommitOnFocusLostModel(valueModel, textField);
            textField.putClientProperty("commitOnFocusListModel", textModel);
        } else {
            textModel = valueModel;
        }
        TextComponentConnector connector = new TextComponentConnector(textModel, textField);
        connector.updateTextComponent();
        Bindings.addComponentPropertyHandler(textField, valueModel);
    }

    public static void bind(JComponent component, String propertyName, ValueModel valueModel) {
        if (component == null) {
            throw new NullPointerException("The component must not be null.");
        }
        if (valueModel == null) {
            throw new NullPointerException("The value model must not be null.");
        }
        if (propertyName == null) {
            throw new NullPointerException("The property name must not be null.");
        }
        PropertyConnector.connectAndUpdate(valueModel, component, propertyName);
        Bindings.addComponentPropertyHandler(component, valueModel);
    }

    public static void addComponentPropertyHandler(JComponent component, ValueModel valueModel) {
        if (!(valueModel instanceof ComponentValueModel)) {
            return;
        }
        ComponentValueModel cvm = (ComponentValueModel)valueModel;
        ComponentPropertyHandler componentHandler = new ComponentPropertyHandler(component);
        cvm.addPropertyChangeListener(componentHandler);
        component.putClientProperty("componentValueModel", cvm);
        component.putClientProperty("componentPropertyHandler", componentHandler);
        component.setEnabled(cvm.isEnabled());
        component.setVisible(cvm.isVisible());
        if (component instanceof JTextComponent) {
            ((JTextComponent)component).setEditable(cvm.isEditable());
        }
    }

    public static void removeComponentPropertyHandler(JComponent component) {
        ComponentValueModel componentValueModel = (ComponentValueModel)component.getClientProperty("componentValueModel");
        PropertyChangeListener componentHandler = (PropertyChangeListener)component.getClientProperty("componentPropertyHandler");
        if (componentValueModel == null || componentHandler == null) {
            if (componentValueModel == null && componentHandler == null) {
                return;
            }
            if (componentValueModel != null) {
                throw new IllegalStateException("The component has a ComponentValueModel stored, but lacks the ComponentPropertyHandler.");
            }
            throw new IllegalStateException("The component has a ComponentPropertyHandler stored, but lacks the ComponentValueModel.");
        }
        componentValueModel.removePropertyChangeListener(componentHandler);
        component.putClientProperty("componentValueModel", null);
        component.putClientProperty("componentPropertyHandler", null);
    }

    public static void commitImmediately() {
        FOCUS_LOST_TRIGGER.triggerCommit();
    }

    public static boolean flushImmediately() {
        boolean buffering = Bindings.isFocusOwnerBuffering();
        if (buffering) {
            FOCUS_LOST_TRIGGER.triggerFlush();
        }
        return buffering;
    }

    public static boolean isFocusOwnerBuffering() {
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (!(focusOwner instanceof JComponent)) {
            return false;
        }
        Object value = ((JComponent)focusOwner).getClientProperty("commitOnFocusListModel");
        if (!(value instanceof BufferedValueModel)) {
            return false;
        }
        BufferedValueModel commitOnFocusLostModel = (BufferedValueModel)value;
        return commitOnFocusLostModel.isBuffering();
    }

    private static ValueModel createCommitOnFocusLostModel(ValueModel valueModel, Component component) {
        if (valueModel == null) {
            throw new NullPointerException("The value model must not be null.");
        }
        BufferedValueModel model = new BufferedValueModel(valueModel, FOCUS_LOST_TRIGGER);
        component.addFocusListener(FOCUS_LOST_HANDLER);
        return model;
    }

    private static final class ComponentPropertyHandler
    implements PropertyChangeListener {
        private final JComponent component;

        private ComponentPropertyHandler(JComponent component) {
            this.component = component;
        }

        public void propertyChange(PropertyChangeEvent evt) {
            String propertyName = evt.getPropertyName();
            ComponentValueModel model = (ComponentValueModel)evt.getSource();
            if ("enabled".equals(propertyName)) {
                this.component.setEnabled(model.isEnabled());
            } else if ("visible".equals(propertyName)) {
                this.component.setVisible(model.isVisible());
            } else if ("editable".equals(propertyName) && this.component instanceof JTextComponent) {
                ((JTextComponent)this.component).setEditable(model.isEditable());
            }
        }
    }

    private static final class FocusLostHandler
    extends FocusAdapter {
        private FocusLostHandler() {
        }

        public void focusLost(FocusEvent evt) {
            if (!evt.isTemporary()) {
                Bindings.FOCUS_LOST_TRIGGER.triggerCommit();
            }
        }
    }

    private static final class WeakPropertyChangeSupport
    extends PropertyChangeSupport {
        static final ReferenceQueue<PropertyChangeListener> QUEUE = new ReferenceQueue();

        WeakPropertyChangeSupport(Object sourceBean) {
            super(sourceBean);
        }

        public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
            if (listener == null) {
                return;
            }
            if (listener instanceof PropertyChangeListenerProxy) {
                PropertyChangeListenerProxy proxy = (PropertyChangeListenerProxy)listener;
                this.addPropertyChangeListener(proxy.getPropertyName(), (PropertyChangeListener)proxy.getListener());
            } else {
                super.addPropertyChangeListener(new WeakPropertyChangeListener(listener));
            }
        }

        public synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            if (listener == null) {
                return;
            }
            super.addPropertyChangeListener(propertyName, new WeakPropertyChangeListener(propertyName, listener));
        }

        public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
            if (listener == null) {
                return;
            }
            if (listener instanceof PropertyChangeListenerProxy) {
                PropertyChangeListenerProxy proxy = (PropertyChangeListenerProxy)listener;
                this.removePropertyChangeListener(proxy.getPropertyName(), (PropertyChangeListener)proxy.getListener());
                return;
            }
            PropertyChangeListener[] listeners = this.getPropertyChangeListeners();
            for (int i = listeners.length - 1; i >= 0; --i) {
                WeakPropertyChangeListener wpcl;
                if (listeners[i] instanceof PropertyChangeListenerProxy || (wpcl = (WeakPropertyChangeListener)listeners[i]).get() != listener) continue;
                super.removePropertyChangeListener(wpcl);
                break;
            }
        }

        public synchronized void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            if (listener == null) {
                return;
            }
            PropertyChangeListener[] listeners = this.getPropertyChangeListeners(propertyName);
            for (int i = listeners.length - 1; i >= 0; --i) {
                WeakPropertyChangeListener wpcl = (WeakPropertyChangeListener)listeners[i];
                if (wpcl.get() != listener) continue;
                super.removePropertyChangeListener(propertyName, wpcl);
                break;
            }
        }

        public void firePropertyChange(PropertyChangeEvent evt) {
            WeakPropertyChangeSupport.cleanUp();
            super.firePropertyChange(evt);
        }

        public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
            WeakPropertyChangeSupport.cleanUp();
            super.firePropertyChange(propertyName, oldValue, newValue);
        }

        private static void cleanUp() {
            WeakPropertyChangeListener wpcl;
            while ((wpcl = (WeakPropertyChangeListener)QUEUE.poll()) != null) {
                wpcl.removeListener();
            }
        }

        void removeWeakPropertyChangeListener(WeakPropertyChangeListener l) {
            if (l.propertyName == null) {
                super.removePropertyChangeListener(l);
            } else {
                super.removePropertyChangeListener(l.propertyName, l);
            }
        }

        /*
         * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
         */
        private final class WeakPropertyChangeListener
        extends WeakReference<PropertyChangeListener>
        implements PropertyChangeListener {
            final String propertyName;

            private WeakPropertyChangeListener(PropertyChangeListener delegate) {
                this(null, delegate);
            }

            private WeakPropertyChangeListener(String propertyName, PropertyChangeListener delegate) {
                super(delegate, WeakPropertyChangeSupport.QUEUE);
                this.propertyName = propertyName;
            }

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                PropertyChangeListener delegate = (PropertyChangeListener)this.get();
                if (delegate != null) {
                    delegate.propertyChange(evt);
                }
            }

            void removeListener() {
                WeakPropertyChangeSupport.this.removeWeakPropertyChangeListener(this);
            }
        }

    }

    private static final class WeakTrigger
    implements ValueModel {
        private final transient WeakPropertyChangeSupport changeSupport;
        private Boolean value = null;

        WeakTrigger() {
            this.changeSupport = new WeakPropertyChangeSupport(this);
        }

        public Object getValue() {
            return this.value;
        }

        public void setValue(Object newValue) {
            if (!(newValue == null || newValue instanceof Boolean)) {
                throw new IllegalArgumentException("Trigger values must be of type Boolean.");
            }
            Boolean oldValue = this.value;
            this.value = (Boolean)newValue;
            this.fireValueChange(oldValue, newValue);
        }

        public void addValueChangeListener(PropertyChangeListener listener) {
            if (listener == null) {
                return;
            }
            this.changeSupport.addPropertyChangeListener("value", listener);
        }

        public void removeValueChangeListener(PropertyChangeListener listener) {
            if (listener == null) {
                return;
            }
            this.changeSupport.removePropertyChangeListener("value", listener);
        }

        private void fireValueChange(Object oldValue, Object newValue) {
            this.changeSupport.firePropertyChange("value", oldValue, newValue);
        }

        void triggerCommit() {
            if (Boolean.TRUE.equals(this.getValue())) {
                this.setValue(null);
            }
            this.setValue(Boolean.TRUE);
        }

        void triggerFlush() {
            if (Boolean.FALSE.equals(this.getValue())) {
                this.setValue(null);
            }
            this.setValue(Boolean.FALSE);
        }
    }

}

