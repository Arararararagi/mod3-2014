/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.adapter;

import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;
import javax.swing.ListSelectionModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public final class SingleListSelectionAdapter
implements ListSelectionModel {
    private static final int MIN = -1;
    private static final int MAX = Integer.MAX_VALUE;
    private int firstAdjustedIndex = Integer.MAX_VALUE;
    private int lastAdjustedIndex = -1;
    private int firstChangedIndex = Integer.MAX_VALUE;
    private int lastChangedIndex = -1;
    private final ValueModel selectionIndexHolder;
    private boolean valueIsAdjusting;
    private final EventListenerList listenerList = new EventListenerList();

    public SingleListSelectionAdapter(ValueModel selectionIndexHolder) {
        this.selectionIndexHolder = selectionIndexHolder;
        this.selectionIndexHolder.addValueChangeListener(new SelectionIndexChangeHandler());
    }

    private int getSelectionIndex() {
        Object value = this.selectionIndexHolder.getValue();
        return value == null ? -1 : (Integer)value;
    }

    private void setSelectionIndex(int newSelectionIndex) {
        this.setSelectionIndex(this.getSelectionIndex(), newSelectionIndex);
    }

    private void setSelectionIndex(int oldSelectionIndex, int newSelectionIndex) {
        if (oldSelectionIndex == newSelectionIndex) {
            return;
        }
        this.markAsDirty(oldSelectionIndex);
        this.markAsDirty(newSelectionIndex);
        this.selectionIndexHolder.setValue(newSelectionIndex);
        this.fireValueChanged();
    }

    public void setSelectionInterval(int index0, int index1) {
        if (index0 == -1 || index1 == -1) {
            return;
        }
        this.setSelectionIndex(index1);
    }

    public void addSelectionInterval(int index0, int index1) {
        this.setSelectionInterval(index0, index1);
    }

    public void removeSelectionInterval(int index0, int index1) {
        if (index0 == -1 || index1 == -1) {
            return;
        }
        int max = Math.max(index0, index1);
        int min = Math.min(index0, index1);
        if (min <= this.getSelectionIndex() && this.getSelectionIndex() <= max) {
            this.clearSelection();
        }
    }

    public int getMinSelectionIndex() {
        return this.getSelectionIndex();
    }

    public int getMaxSelectionIndex() {
        return this.getSelectionIndex();
    }

    public boolean isSelectedIndex(int index) {
        return index < 0 ? false : index == this.getSelectionIndex();
    }

    public int getAnchorSelectionIndex() {
        return this.getSelectionIndex();
    }

    public void setAnchorSelectionIndex(int newSelectionIndex) {
        this.setSelectionIndex(newSelectionIndex);
    }

    public int getLeadSelectionIndex() {
        return this.getSelectionIndex();
    }

    public void setLeadSelectionIndex(int newSelectionIndex) {
        this.setSelectionIndex(newSelectionIndex);
    }

    public void clearSelection() {
        this.setSelectionIndex(-1);
    }

    public boolean isSelectionEmpty() {
        return this.getSelectionIndex() == -1;
    }

    public void insertIndexInterval(int index, int length, boolean before) {
        if (this.isSelectionEmpty()) {
            return;
        }
        int insMinIndex = before ? index : index + 1;
        int selectionIndex = this.getSelectionIndex();
        if (selectionIndex >= insMinIndex) {
            this.setSelectionIndex(selectionIndex + length);
        }
    }

    public void removeIndexInterval(int index0, int index1) {
        if (index0 < -1 || index1 < -1) {
            throw new IndexOutOfBoundsException("Both indices must be greater or equals to -1.");
        }
        if (this.isSelectionEmpty()) {
            return;
        }
        int lower = Math.min(index0, index1);
        int upper = Math.max(index0, index1);
        int selectionIndex = this.getSelectionIndex();
        if (lower <= selectionIndex && selectionIndex <= upper) {
            this.clearSelection();
        } else if (upper < selectionIndex) {
            int translated = selectionIndex - (upper - lower + 1);
            this.setSelectionInterval(translated, translated);
        }
    }

    public void setValueIsAdjusting(boolean newValueIsAdjusting) {
        boolean oldValueIsAdjusting = this.valueIsAdjusting;
        if (oldValueIsAdjusting == newValueIsAdjusting) {
            return;
        }
        this.valueIsAdjusting = newValueIsAdjusting;
        this.fireValueChanged(newValueIsAdjusting);
    }

    public boolean getValueIsAdjusting() {
        return this.valueIsAdjusting;
    }

    public void setSelectionMode(int selectionMode) {
        if (selectionMode != 0) {
            throw new UnsupportedOperationException("The SingleListSelectionAdapter must be used in single selection mode.");
        }
    }

    public int getSelectionMode() {
        return 0;
    }

    public void addListSelectionListener(ListSelectionListener listener) {
        this.listenerList.add(ListSelectionListener.class, listener);
    }

    public void removeListSelectionListener(ListSelectionListener listener) {
        this.listenerList.remove(ListSelectionListener.class, listener);
    }

    public ListSelectionListener[] getListSelectionListeners() {
        return (ListSelectionListener[])this.listenerList.getListeners((Class)ListSelectionListener.class);
    }

    private void markAsDirty(int index) {
        if (index < 0) {
            return;
        }
        this.firstAdjustedIndex = Math.min(this.firstAdjustedIndex, index);
        this.lastAdjustedIndex = Math.max(this.lastAdjustedIndex, index);
    }

    private void fireValueChanged(boolean isAdjusting) {
        if (this.lastChangedIndex == -1) {
            return;
        }
        int oldFirstChangedIndex = this.firstChangedIndex;
        int oldLastChangedIndex = this.lastChangedIndex;
        this.firstChangedIndex = Integer.MAX_VALUE;
        this.lastChangedIndex = -1;
        this.fireValueChanged(oldFirstChangedIndex, oldLastChangedIndex, isAdjusting);
    }

    private void fireValueChanged(int firstIndex, int lastIndex) {
        this.fireValueChanged(firstIndex, lastIndex, this.getValueIsAdjusting());
    }

    private void fireValueChanged(int firstIndex, int lastIndex, boolean isAdjusting) {
        Object[] listeners = this.listenerList.getListenerList();
        ListSelectionEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListSelectionListener.class) continue;
            if (e == null) {
                e = new ListSelectionEvent(this, firstIndex, lastIndex, isAdjusting);
            }
            ((ListSelectionListener)listeners[i + 1]).valueChanged(e);
        }
    }

    private void fireValueChanged() {
        if (this.lastAdjustedIndex == -1) {
            return;
        }
        if (this.getValueIsAdjusting()) {
            this.firstChangedIndex = Math.min(this.firstChangedIndex, this.firstAdjustedIndex);
            this.lastChangedIndex = Math.max(this.lastChangedIndex, this.lastAdjustedIndex);
        }
        int oldFirstAdjustedIndex = this.firstAdjustedIndex;
        int oldLastAdjustedIndex = this.lastAdjustedIndex;
        this.firstAdjustedIndex = Integer.MAX_VALUE;
        this.lastAdjustedIndex = -1;
        this.fireValueChanged(oldFirstAdjustedIndex, oldLastAdjustedIndex);
    }

    private final class SelectionIndexChangeHandler
    implements PropertyChangeListener {
        private SelectionIndexChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object oldValue = evt.getOldValue();
            Object newValue = evt.getNewValue();
            int oldIndex = oldValue == null ? -1 : (Integer)oldValue;
            int newIndex = newValue == null ? -1 : (Integer)newValue;
            SingleListSelectionAdapter.this.setSelectionIndex(oldIndex, newIndex);
        }
    }

}

