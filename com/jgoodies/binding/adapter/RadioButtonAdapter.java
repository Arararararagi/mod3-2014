/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.adapter;

import com.jgoodies.binding.BindingUtils;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;

public final class RadioButtonAdapter
extends JToggleButton.ToggleButtonModel {
    private final ValueModel subject;
    private final Object choice;

    public RadioButtonAdapter(ValueModel subject, Object choice) {
        if (subject == null) {
            throw new NullPointerException("The subject must not be null.");
        }
        this.subject = subject;
        this.choice = choice;
        subject.addValueChangeListener(new SubjectValueChangeHandler());
        this.updateSelectedState();
    }

    public void setSelected(boolean b) {
        if (!b || this.isSelected()) {
            return;
        }
        this.subject.setValue(this.choice);
        this.updateSelectedState();
    }

    public void setGroup(ButtonGroup group) {
        if (group != null) {
            throw new UnsupportedOperationException("You need not and must not use a ButtonGroup with a set of RadioButtonAdapters. These form a group by sharing the same subject ValueModel.");
        }
    }

    private void updateSelectedState() {
        boolean subjectHoldsChoiceValue = BindingUtils.equals(this.choice, this.subject.getValue());
        super.setSelected(subjectHoldsChoiceValue);
    }

    private final class SubjectValueChangeHandler
    implements PropertyChangeListener {
        private SubjectValueChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            RadioButtonAdapter.this.updateSelectedState();
        }
    }

}

