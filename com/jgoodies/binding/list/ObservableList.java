/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.list;

import java.util.List;
import javax.swing.ListModel;

public interface ObservableList<E>
extends List<E>,
ListModel {
}

