/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.list;

import com.jgoodies.binding.list.ArrayListModel;
import com.jgoodies.binding.list.IndirectListModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class SelectionInList<E>
extends IndirectListModel<E>
implements ValueModel {
    public static final String PROPERTYNAME_SELECTION = "selection";
    public static final String PROPERTYNAME_SELECTION_EMPTY = "selectionEmpty";
    public static final String PROPERTYNAME_SELECTION_HOLDER = "selectionHolder";
    public static final String PROPERTYNAME_SELECTION_INDEX = "selectionIndex";
    public static final String PROPERTYNAME_SELECTION_INDEX_HOLDER = "selectionIndexHolder";
    public static final String PROPERTYNAME_VALUE = "value";
    private static final int NO_SELECTION_INDEX = -1;
    private ValueModel selectionHolder;
    private ValueModel selectionIndexHolder;
    private final PropertyChangeListener selectionChangeHandler;
    private final PropertyChangeListener selectionIndexChangeHandler;
    private E oldSelection;
    private int oldSelectionIndex;

    public SelectionInList() {
        this(new ArrayListModel());
    }

    public SelectionInList(E[] listItems) {
        this(Arrays.asList(listItems));
    }

    public SelectionInList(E[] listItems, ValueModel selectionHolder) {
        this(Arrays.asList(listItems), selectionHolder);
    }

    public SelectionInList(E[] listItems, ValueModel selectionHolder, ValueModel selectionIndexHolder) {
        this(Arrays.asList(listItems), selectionHolder, selectionIndexHolder);
    }

    public SelectionInList(List<E> list) {
        this(new ValueHolder(list, true));
    }

    public SelectionInList(List<E> list, ValueModel selectionHolder) {
        this(new ValueHolder(list, true), selectionHolder);
    }

    public SelectionInList(List<E> list, ValueModel selectionHolder, ValueModel selectionIndexHolder) {
        this(new ValueHolder(list, true), selectionHolder, selectionIndexHolder);
    }

    public SelectionInList(ListModel listModel) {
        this(new ValueHolder(listModel, true));
    }

    public SelectionInList(ListModel listModel, ValueModel selectionHolder) {
        this(new ValueHolder(listModel, true), selectionHolder);
    }

    public SelectionInList(ListModel listModel, ValueModel selectionHolder, ValueModel selectionIndexHolder) {
        this(new ValueHolder(listModel, true), selectionHolder, selectionIndexHolder);
    }

    public SelectionInList(ValueModel listHolder) {
        this(listHolder, (ValueModel)new ValueHolder(null, true));
    }

    public SelectionInList(ValueModel listHolder, ValueModel selectionHolder) {
        this(listHolder, selectionHolder, (ValueModel)new ValueHolder((Object)-1));
    }

    public SelectionInList(ValueModel listHolder, ValueModel selectionHolder, ValueModel selectionIndexHolder) {
        super(listHolder);
        if (selectionHolder == null) {
            throw new NullPointerException("The selection holder must not be null.");
        }
        if (selectionIndexHolder == null) {
            throw new NullPointerException("The selection index holder must not be null.");
        }
        this.selectionChangeHandler = new SelectionChangeHandler();
        this.selectionIndexChangeHandler = new SelectionIndexChangeHandler();
        this.selectionHolder = selectionHolder;
        this.selectionIndexHolder = selectionIndexHolder;
        this.initializeSelectionIndex();
        this.selectionHolder.addValueChangeListener(this.selectionChangeHandler);
        this.selectionIndexHolder.addValueChangeListener(this.selectionIndexChangeHandler);
    }

    public void fireSelectedContentsChanged() {
        if (this.hasSelection()) {
            int selectionIndex = this.getSelectionIndex();
            this.fireContentsChanged(selectionIndex, selectionIndex);
        }
    }

    public E getSelection() {
        return this.getSafeElementAt(this.getSelectionIndex());
    }

    public void setSelection(E newSelection) {
        if (!this.isEmpty()) {
            this.setSelectionIndex(this.indexOf(newSelection));
        }
    }

    public boolean hasSelection() {
        return this.getSelectionIndex() != -1;
    }

    public boolean isSelectionEmpty() {
        return !this.hasSelection();
    }

    public void clearSelection() {
        this.setSelectionIndex(-1);
    }

    public int getSelectionIndex() {
        return (Integer)this.getSelectionIndexHolder().getValue();
    }

    public void setSelectionIndex(int newSelectionIndex) {
        int upperBound = this.getSize() - 1;
        if (newSelectionIndex < -1 || newSelectionIndex > upperBound) {
            throw new IndexOutOfBoundsException("The selection index " + newSelectionIndex + " must be in [-1, " + upperBound + "]");
        }
        this.oldSelectionIndex = this.getSelectionIndex();
        if (this.oldSelectionIndex == newSelectionIndex) {
            return;
        }
        this.getSelectionIndexHolder().setValue(newSelectionIndex);
    }

    public ValueModel getSelectionHolder() {
        return this.selectionHolder;
    }

    public void setSelectionHolder(ValueModel newSelectionHolder) {
        if (newSelectionHolder == null) {
            throw new NullPointerException("The new selection holder must not be null.");
        }
        ValueModel oldSelectionHolder = this.getSelectionHolder();
        oldSelectionHolder.removeValueChangeListener(this.selectionChangeHandler);
        this.selectionHolder = newSelectionHolder;
        this.oldSelection = newSelectionHolder.getValue();
        newSelectionHolder.addValueChangeListener(this.selectionChangeHandler);
        this.firePropertyChange("selectionHolder", oldSelectionHolder, newSelectionHolder);
    }

    public ValueModel getSelectionIndexHolder() {
        return this.selectionIndexHolder;
    }

    public void setSelectionIndexHolder(ValueModel newSelectionIndexHolder) {
        if (newSelectionIndexHolder == null) {
            throw new NullPointerException("The new selection index holder must not be null.");
        }
        if (newSelectionIndexHolder.getValue() == null) {
            throw new IllegalArgumentException("The value of the new selection index holder must not be null.");
        }
        ValueModel oldSelectionIndexHolder = this.getSelectionIndexHolder();
        if (this.equals(oldSelectionIndexHolder, newSelectionIndexHolder)) {
            return;
        }
        oldSelectionIndexHolder.removeValueChangeListener(this.selectionIndexChangeHandler);
        this.selectionIndexHolder = newSelectionIndexHolder;
        newSelectionIndexHolder.addValueChangeListener(this.selectionIndexChangeHandler);
        this.oldSelectionIndex = this.getSelectionIndex();
        this.oldSelection = this.getSafeElementAt(this.oldSelectionIndex);
        this.firePropertyChange("selectionIndexHolder", oldSelectionIndexHolder, newSelectionIndexHolder);
    }

    public E getValue() {
        return this.getSelection();
    }

    @Override
    public void setValue(Object newValue) {
        this.setSelection(newValue);
    }

    @Override
    public void addValueChangeListener(PropertyChangeListener l) {
        this.addPropertyChangeListener("value", l);
    }

    @Override
    public void removeValueChangeListener(PropertyChangeListener l) {
        this.removePropertyChangeListener("value", l);
    }

    void fireValueChange(Object oldValue, Object newValue) {
        this.firePropertyChange("value", oldValue, newValue);
    }

    @Override
    public void release() {
        super.release();
        this.selectionHolder.removeValueChangeListener(this.selectionChangeHandler);
        this.selectionIndexHolder.removeValueChangeListener(this.selectionIndexChangeHandler);
        this.selectionHolder = null;
        this.selectionIndexHolder = null;
        this.oldSelection = null;
    }

    private E getSafeElementAt(int index) {
        return index < 0 || index >= this.getSize() ? null : (E)this.getElementAt(index);
    }

    private int indexOf(Object element) {
        return this.indexOf(this.getListHolder().getValue(), element);
    }

    private int indexOf(Object aList, Object element) {
        if (element == null) {
            return -1;
        }
        if (this.getSize(aList) == 0) {
            return -1;
        }
        if (aList instanceof List) {
            return ((List)aList).indexOf(element);
        }
        ListModel listModel = (ListModel)aList;
        int size = listModel.getSize();
        for (int index = 0; index < size; ++index) {
            if (!element.equals(listModel.getElementAt(index))) continue;
            return index;
        }
        return -1;
    }

    private void initializeSelectionIndex() {
        Object selectionValue = this.selectionHolder.getValue();
        if (selectionValue != null) {
            this.setSelectionIndex(this.indexOf(selectionValue));
        }
        this.oldSelection = selectionValue;
        this.oldSelectionIndex = this.getSelectionIndex();
    }

    @Override
    protected ListDataListener createListDataChangeHandler() {
        return new ListDataChangeHandler();
    }

    @Override
    protected void updateList(Object oldList, int oldSize, Object newList) {
        boolean hadSelection = this.hasSelection();
        Object oldSelectionHolderValue = hadSelection ? this.getSelectionHolder().getValue() : null;
        super.updateList(oldList, oldSize, newList);
        if (hadSelection) {
            this.setSelectionIndex(this.indexOf(newList, oldSelectionHolderValue));
        }
    }

    private final class ListDataChangeHandler
    implements ListDataListener {
        private ListDataChangeHandler() {
        }

        public void intervalAdded(ListDataEvent evt) {
            int index0 = evt.getIndex0();
            int index1 = evt.getIndex1();
            int index = SelectionInList.this.getSelectionIndex();
            SelectionInList.this.fireIntervalAdded(index0, index1);
            if (index >= index0) {
                SelectionInList.this.setSelectionIndex(index + (index1 - index0 + 1));
            }
        }

        public void intervalRemoved(ListDataEvent evt) {
            int index0 = evt.getIndex0();
            int index1 = evt.getIndex1();
            int index = SelectionInList.this.getSelectionIndex();
            SelectionInList.this.fireIntervalRemoved(index0, index1);
            if (index >= index0) {
                if (index <= index1) {
                    SelectionInList.this.setSelectionIndex(-1);
                } else {
                    SelectionInList.this.setSelectionIndex(index - (index1 - index0 + 1));
                }
            }
        }

        public void contentsChanged(ListDataEvent evt) {
            SelectionInList.this.fireContentsChanged(evt.getIndex0(), evt.getIndex1());
            this.updateSelectionContentsChanged(evt.getIndex0(), evt.getIndex1());
        }

        private void updateSelectionContentsChanged(int first, int last) {
            if (first < 0) {
                return;
            }
            int selectionIndex = SelectionInList.this.getSelectionIndex();
            if (first <= selectionIndex && selectionIndex <= last) {
                SelectionInList.this.getSelectionHolder().setValue(SelectionInList.this.getElementAt(selectionIndex));
            }
        }
    }

    private final class SelectionChangeHandler
    implements PropertyChangeListener {
        private SelectionChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object oldValue = evt.getOldValue();
            Object newSelection = evt.getNewValue();
            int newSelectionIndex = SelectionInList.this.indexOf(newSelection);
            if (newSelectionIndex != SelectionInList.this.oldSelectionIndex) {
                SelectionInList.this.selectionIndexHolder.removeValueChangeListener(SelectionInList.this.selectionIndexChangeHandler);
                SelectionInList.this.selectionIndexHolder.setValue(newSelectionIndex);
                SelectionInList.this.selectionIndexHolder.addValueChangeListener(SelectionInList.this.selectionIndexChangeHandler);
            }
            int theOldSelectionIndex = SelectionInList.this.oldSelectionIndex;
            SelectionInList.this.oldSelectionIndex = newSelectionIndex;
            SelectionInList.this.oldSelection = newSelection;
            SelectionInList.this.firePropertyChange("selectionIndex", theOldSelectionIndex, newSelectionIndex);
            SelectionInList.this.firePropertyChange("selectionEmpty", theOldSelectionIndex == -1, newSelectionIndex == -1);
            SelectionInList.this.firePropertyChange("selection", oldValue, newSelection);
            SelectionInList.this.fireValueChange(oldValue, newSelection);
        }
    }

    private final class SelectionIndexChangeHandler
    implements PropertyChangeListener {
        private SelectionIndexChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object newSelection;
            int newSelectionIndex = SelectionInList.this.getSelectionIndex();
            Object theOldSelection = SelectionInList.this.oldSelection;
            if (!SelectionInList.this.equals(theOldSelection, newSelection = SelectionInList.this.getSafeElementAt(newSelectionIndex))) {
                SelectionInList.this.selectionHolder.removeValueChangeListener(SelectionInList.this.selectionChangeHandler);
                SelectionInList.this.selectionHolder.setValue(newSelection);
                SelectionInList.this.selectionHolder.addValueChangeListener(SelectionInList.this.selectionChangeHandler);
            }
            int theOldSelectionIndex = SelectionInList.this.oldSelectionIndex;
            SelectionInList.this.oldSelectionIndex = newSelectionIndex;
            SelectionInList.this.oldSelection = newSelection;
            SelectionInList.this.firePropertyChange("selectionIndex", theOldSelectionIndex, newSelectionIndex);
            SelectionInList.this.firePropertyChange("selectionEmpty", theOldSelectionIndex == -1, newSelectionIndex == -1);
            SelectionInList.this.firePropertyChange("selection", theOldSelection, newSelection);
            SelectionInList.this.fireValueChange(theOldSelection, newSelection);
        }
    }

}

