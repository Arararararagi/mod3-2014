/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.list;

import com.jgoodies.binding.list.ObservableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public final class ArrayListModel<E>
extends ArrayList<E>
implements ObservableList<E> {
    private static final long serialVersionUID = -6165677201152015546L;
    private EventListenerList listenerList;

    public ArrayListModel() {
        this(10);
    }

    public ArrayListModel(int initialCapacity) {
        super(initialCapacity);
    }

    public ArrayListModel(Collection<? extends E> c) {
        super(c);
    }

    @Override
    public void add(int index, E element) {
        super.add(index, element);
        this.fireIntervalAdded(index, index);
    }

    @Override
    public boolean add(E e) {
        int newIndex = this.size();
        super.add(e);
        this.fireIntervalAdded(newIndex, newIndex);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        boolean changed = super.addAll(index, c);
        if (changed) {
            int lastIndex = index + c.size() - 1;
            this.fireIntervalAdded(index, lastIndex);
        }
        return changed;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int firstIndex = this.size();
        boolean changed = super.addAll(c);
        if (changed) {
            int lastIndex = firstIndex + c.size() - 1;
            this.fireIntervalAdded(firstIndex, lastIndex);
        }
        return changed;
    }

    @Override
    public void clear() {
        if (this.isEmpty()) {
            return;
        }
        int oldLastIndex = this.size() - 1;
        super.clear();
        this.fireIntervalRemoved(0, oldLastIndex);
    }

    @Override
    public E remove(int index) {
        Object removedElement = super.remove(index);
        this.fireIntervalRemoved(index, index);
        return removedElement;
    }

    @Override
    public boolean remove(Object o) {
        boolean contained;
        int index = this.indexOf(o);
        boolean bl = contained = index != -1;
        if (contained) {
            this.remove(index);
        }
        return contained;
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
        this.fireIntervalRemoved(fromIndex, toIndex - 1);
    }

    @Override
    public E set(int index, E element) {
        E previousElement = super.set(index, element);
        this.fireContentsChanged(index, index);
        return previousElement;
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        this.getEventListenerList().add(ListDataListener.class, l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        this.getEventListenerList().remove(ListDataListener.class, l);
    }

    public Object getElementAt(int index) {
        return this.get(index);
    }

    @Override
    public int getSize() {
        return this.size();
    }

    public void fireContentsChanged(int index) {
        this.fireContentsChanged(index, index);
    }

    public ListDataListener[] getListDataListeners() {
        return (ListDataListener[])this.getEventListenerList().getListeners((Class)ListDataListener.class);
    }

    private void fireContentsChanged(int index0, int index1) {
        Object[] listeners = this.getEventListenerList().getListenerList();
        ListDataEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 0, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).contentsChanged(e);
        }
    }

    private void fireIntervalAdded(int index0, int index1) {
        Object[] listeners = this.getEventListenerList().getListenerList();
        ListDataEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 1, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).intervalAdded(e);
        }
    }

    private void fireIntervalRemoved(int index0, int index1) {
        Object[] listeners = this.getEventListenerList().getListenerList();
        ListDataEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 2, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).intervalRemoved(e);
        }
    }

    private EventListenerList getEventListenerList() {
        if (this.listenerList == null) {
            this.listenerList = new EventListenerList();
        }
        return this.listenerList;
    }
}

