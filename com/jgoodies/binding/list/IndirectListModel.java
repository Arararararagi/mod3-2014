/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.list;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.list.ArrayListModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class IndirectListModel<E>
extends Model
implements ListModel {
    public static final String PROPERTYNAME_LIST = "list";
    public static final String PROPERTYNAME_LIST_HOLDER = "listHolder";
    private static final ListModel EMPTY_LIST_MODEL = new EmptyListModel();
    private ValueModel listHolder;
    private Object list;
    private int listSize;
    private final PropertyChangeListener listChangeHandler;
    private final ListDataListener listDataChangeHandler;
    private final EventListenerList listenerList = new EventListenerList();

    public IndirectListModel() {
        this(new ArrayListModel());
    }

    public IndirectListModel(E[] listItems) {
        this(Arrays.asList(listItems));
    }

    public IndirectListModel(List<E> list) {
        this(new ValueHolder(list, true));
    }

    public IndirectListModel(ListModel listModel) {
        this(new ValueHolder(listModel, true));
    }

    public IndirectListModel(ValueModel listHolder) {
        if (listHolder == null) {
            throw new NullPointerException("The list holder must not be null.");
        }
        this.checkListHolderIdentityCheck(listHolder);
        this.listChangeHandler = new ListChangeHandler();
        this.listDataChangeHandler = this.createListDataChangeHandler();
        this.listHolder = listHolder;
        this.listHolder.addValueChangeListener(this.listChangeHandler);
        this.list = listHolder.getValue();
        this.listSize = this.getSize(this.list);
        if (this.list != null) {
            if (this.list instanceof ListModel) {
                ((ListModel)this.list).addListDataListener(this.listDataChangeHandler);
            } else if (!(this.list instanceof List)) {
                throw new ClassCastException("The listHolder's value must be a List or ListModel.");
            }
        }
    }

    public final List<E> getList() {
        Object aList = this.getListHolder().getValue();
        if (aList == null) {
            return Collections.emptyList();
        }
        if (aList instanceof List) {
            return (List)aList;
        }
        throw new ClassCastException("#getList assumes that the list holder holds a List");
    }

    public final void setList(List<E> newList) {
        this.getListHolder().setValue(newList);
    }

    public final ListModel getListModel() {
        Object aListModel = this.getListHolder().getValue();
        if (aListModel == null) {
            return EMPTY_LIST_MODEL;
        }
        if (aListModel instanceof ListModel) {
            return (ListModel)aListModel;
        }
        throw new ClassCastException("#getListModel assumes that the list holder holds a ListModel");
    }

    public final void setListModel(ListModel newListModel) {
        this.getListHolder().setValue(newListModel);
    }

    public final ValueModel getListHolder() {
        return this.listHolder;
    }

    public final void setListHolder(ValueModel newListHolder) {
        if (newListHolder == null) {
            throw new NullPointerException("The new list holder must not be null.");
        }
        this.checkListHolderIdentityCheck(newListHolder);
        ValueModel oldListHolder = this.getListHolder();
        if (oldListHolder == newListHolder) {
            return;
        }
        Object oldList = this.list;
        int oldSize = this.listSize;
        Object newList = newListHolder.getValue();
        oldListHolder.removeValueChangeListener(this.listChangeHandler);
        this.listHolder = newListHolder;
        newListHolder.addValueChangeListener(this.listChangeHandler);
        this.updateList(oldList, oldSize, newList);
        this.firePropertyChange("listHolder", oldListHolder, newListHolder);
    }

    public final boolean isEmpty() {
        return this.getSize() == 0;
    }

    @Override
    public final int getSize() {
        return this.getSize(this.getListHolder().getValue());
    }

    @Override
    public final E getElementAt(int index) {
        return this.getElementAt(this.getListHolder().getValue(), index);
    }

    @Override
    public final void addListDataListener(ListDataListener l) {
        this.listenerList.add(ListDataListener.class, l);
    }

    @Override
    public final void removeListDataListener(ListDataListener l) {
        this.listenerList.remove(ListDataListener.class, l);
    }

    public final ListDataListener[] getListDataListeners() {
        return (ListDataListener[])this.listenerList.getListeners((Class)ListDataListener.class);
    }

    public final void fireContentsChanged(int index0, int index1) {
        Object[] listeners = this.listenerList.getListenerList();
        ListDataEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 0, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).contentsChanged(e);
        }
    }

    public final void fireIntervalAdded(int index0, int index1) {
        Object[] listeners = this.listenerList.getListenerList();
        ListDataEvent e = null;
        this.listSize = this.getSize();
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 1, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).intervalAdded(e);
        }
    }

    public final void fireIntervalRemoved(int index0, int index1) {
        Object[] listeners = this.listenerList.getListenerList();
        ListDataEvent e = null;
        this.listSize = this.getSize();
        for (int i = listeners.length - 2; i >= 0; i-=2) {
            if (listeners[i] != ListDataListener.class) continue;
            if (e == null) {
                e = new ListDataEvent(this, 2, index0, index1);
            }
            ((ListDataListener)listeners[i + 1]).intervalRemoved(e);
        }
    }

    public void release() {
        this.listHolder.removeValueChangeListener(this.listChangeHandler);
        if (this.list != null && this.list instanceof ListModel) {
            ((ListModel)this.list).removeListDataListener(this.listDataChangeHandler);
        }
        this.listHolder = null;
        this.list = null;
    }

    protected ListDataListener createListDataChangeHandler() {
        return new ListDataChangeHandler();
    }

    protected void updateList(Object oldList, int oldSize, Object newList) {
        if (oldList != null && oldList instanceof ListModel) {
            ((ListModel)oldList).removeListDataListener(this.listDataChangeHandler);
        }
        if (newList != null && newList instanceof ListModel) {
            ((ListModel)newList).addListDataListener(this.listDataChangeHandler);
        }
        int newSize = this.getSize(newList);
        this.list = newList;
        this.listSize = this.getSize(newList);
        this.firePropertyChange("list", oldList, newList);
        this.fireListChanged(oldSize - 1, newSize - 1);
    }

    protected final void fireListChanged(int oldLastIndex, int newLastIndex) {
        if (newLastIndex < oldLastIndex) {
            this.fireIntervalRemoved(newLastIndex + 1, oldLastIndex);
        } else if (oldLastIndex < newLastIndex) {
            this.fireIntervalAdded(oldLastIndex + 1, newLastIndex);
        }
        int lastCommonIndex = Math.min(oldLastIndex, newLastIndex);
        if (lastCommonIndex >= 0) {
            this.fireContentsChanged(0, lastCommonIndex);
        }
    }

    protected final int getSize(Object aListListModelOrNull) {
        if (aListListModelOrNull == null) {
            return 0;
        }
        if (aListListModelOrNull instanceof ListModel) {
            return ((ListModel)aListListModelOrNull).getSize();
        }
        return ((List)aListListModelOrNull).size();
    }

    private E getElementAt(Object aList, int index) {
        if (aList == null) {
            throw new NullPointerException("The list contents is null.");
        }
        if (aList instanceof ListModel) {
            return ((ListModel)aList).getElementAt(index);
        }
        return ((List)aList).get(index);
    }

    private void checkListHolderIdentityCheck(ValueModel aListHolder) {
        if (!(aListHolder instanceof ValueHolder)) {
            return;
        }
        ValueHolder valueHolder = (ValueHolder)aListHolder;
        if (!valueHolder.isIdentityCheckEnabled()) {
            throw new IllegalArgumentException("The list holder must have the identity check enabled.");
        }
    }

    private static final class EmptyListModel
    implements ListModel,
    Serializable {
        private EmptyListModel() {
        }

        public int getSize() {
            return 0;
        }

        public Object getElementAt(int index) {
            return null;
        }

        public void addListDataListener(ListDataListener l) {
        }

        public void removeListDataListener(ListDataListener l) {
        }
    }

    private final class ListChangeHandler
    implements PropertyChangeListener {
        private ListChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object oldList = IndirectListModel.this.list;
            int oldSize = IndirectListModel.this.listSize;
            Object newList = evt.getNewValue();
            IndirectListModel.this.updateList(oldList, oldSize, newList);
        }
    }

    private final class ListDataChangeHandler
    implements ListDataListener {
        private ListDataChangeHandler() {
        }

        public void intervalAdded(ListDataEvent evt) {
            int index0 = evt.getIndex0();
            int index1 = evt.getIndex1();
            IndirectListModel.this.fireIntervalAdded(index0, index1);
        }

        public void intervalRemoved(ListDataEvent evt) {
            int index0 = evt.getIndex0();
            int index1 = evt.getIndex1();
            IndirectListModel.this.fireIntervalRemoved(index0, index1);
        }

        public void contentsChanged(ListDataEvent evt) {
            IndirectListModel.this.fireContentsChanged(evt.getIndex0(), evt.getIndex1());
        }
    }

}

