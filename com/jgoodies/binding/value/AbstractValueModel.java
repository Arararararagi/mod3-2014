/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.value;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeListener;

public abstract class AbstractValueModel
extends Model
implements ValueModel {
    public static final String PROPERTYNAME_VALUE = "value";

    public final void addValueChangeListener(PropertyChangeListener l) {
        this.addPropertyChangeListener("value", l);
    }

    public final void removeValueChangeListener(PropertyChangeListener l) {
        this.removePropertyChangeListener("value", l);
    }

    public final void fireValueChange(Object oldValue, Object newValue) {
        this.firePropertyChange("value", oldValue, newValue);
    }

    public final void fireValueChange(Object oldValue, Object newValue, boolean checkIdentity) {
        this.firePropertyChange("value", oldValue, newValue, checkIdentity);
    }

    public final void fireValueChange(boolean oldValue, boolean newValue) {
        this.fireValueChange((Object)oldValue, (Object)newValue);
    }

    public final void fireValueChange(int oldValue, int newValue) {
        this.fireValueChange((Object)oldValue, (Object)newValue);
    }

    public final void fireValueChange(long oldValue, long newValue) {
        this.fireValueChange((Object)oldValue, (Object)newValue);
    }

    public final void fireValueChange(double oldValue, double newValue) {
        this.fireValueChange((Object)oldValue, (Object)newValue);
    }

    public final void fireValueChange(float oldValue, float newValue) {
        this.fireValueChange(Float.valueOf(oldValue), Float.valueOf(newValue));
    }

    public final boolean booleanValue() {
        return (Boolean)this.getValue();
    }

    public final double doubleValue() {
        return (Double)this.getValue();
    }

    public final float floatValue() {
        return ((Float)this.getValue()).floatValue();
    }

    public final int intValue() {
        return (Integer)this.getValue();
    }

    public final long longValue() {
        return (Long)this.getValue();
    }

    public String getString() {
        return (String)this.getValue();
    }

    public final void setValue(boolean b) {
        this.setValue(b);
    }

    public final void setValue(double d) {
        this.setValue(d);
    }

    public final void setValue(float f) {
        this.setValue(Float.valueOf(f));
    }

    public final void setValue(int i) {
        this.setValue(i);
    }

    public final void setValue(long l) {
        this.setValue(l);
    }

    public String toString() {
        return this.getClass().getName() + "[" + this.paramString() + "]";
    }

    protected String paramString() {
        return "value=" + this.valueString();
    }

    protected String valueString() {
        try {
            Object value = this.getValue();
            return value == null ? "null" : value.toString();
        }
        catch (Exception e) {
            return "Can't read";
        }
    }
}

