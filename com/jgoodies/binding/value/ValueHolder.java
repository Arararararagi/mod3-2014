/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.value;

import com.jgoodies.binding.value.AbstractValueModel;

public final class ValueHolder
extends AbstractValueModel {
    private Object value;
    private boolean checkIdentity;

    public ValueHolder() {
        this(null);
    }

    public ValueHolder(Object initialValue) {
        this(initialValue, false);
    }

    public ValueHolder(Object initialValue, boolean checkIdentity) {
        this.value = initialValue;
        this.checkIdentity = checkIdentity;
    }

    public ValueHolder(boolean initialValue) {
        this((Object)initialValue);
    }

    public ValueHolder(double initialValue) {
        this((Object)initialValue);
    }

    public ValueHolder(float initialValue) {
        this(Float.valueOf(initialValue));
    }

    public ValueHolder(int initialValue) {
        this((Object)initialValue);
    }

    public ValueHolder(long initialValue) {
        this((Object)initialValue);
    }

    public Object getValue() {
        return this.value;
    }

    public void setValue(Object newValue) {
        this.setValue(newValue, this.isIdentityCheckEnabled());
    }

    public boolean isIdentityCheckEnabled() {
        return this.checkIdentity;
    }

    public void setIdentityCheckEnabled(boolean checkIdentity) {
        this.checkIdentity = checkIdentity;
    }

    public void setValue(Object newValue, boolean checkIdentity) {
        Object oldValue = this.getValue();
        if (oldValue == newValue) {
            return;
        }
        this.value = newValue;
        this.fireValueChange(oldValue, newValue, checkIdentity);
    }
}

