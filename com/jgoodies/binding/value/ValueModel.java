/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.value;

import java.beans.PropertyChangeListener;

public interface ValueModel {
    public Object getValue();

    public void setValue(Object var1);

    public void addValueChangeListener(PropertyChangeListener var1);

    public void removeValueChangeListener(PropertyChangeListener var1);
}

