/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.value;

import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public final class BufferedValueModel
extends AbstractValueModel {
    public static final String PROPERTYNAME_BUFFERING = "buffering";
    public static final String PROPERTYNAME_SUBJECT = "subject";
    public static final String PROPERTYNAME_TRIGGER_CHANNEL = "triggerChannel";
    private ValueModel subject;
    private ValueModel triggerChannel;
    private Object bufferedValue;
    private boolean valueAssigned;
    private final ValueChangeHandler valueChangeHandler;
    private final TriggerChangeHandler triggerChangeHandler;

    public BufferedValueModel(ValueModel subject, ValueModel triggerChannel) {
        this.valueChangeHandler = new ValueChangeHandler();
        this.triggerChangeHandler = new TriggerChangeHandler();
        this.setSubject(subject);
        this.setTriggerChannel(triggerChannel);
        this.setBuffering(false);
    }

    public ValueModel getSubject() {
        return this.subject;
    }

    public void setSubject(ValueModel newSubject) {
        ValueModel oldSubject = this.getSubject();
        ReadAccessResult oldReadValue = this.readBufferedOrSubjectValue();
        Object oldValue = oldReadValue.value;
        if (oldSubject != null) {
            oldSubject.removeValueChangeListener(this.valueChangeHandler);
        }
        this.subject = newSubject;
        if (newSubject != null) {
            newSubject.addValueChangeListener(this.valueChangeHandler);
        }
        this.firePropertyChange("subject", oldSubject, newSubject);
        if (this.isBuffering()) {
            return;
        }
        ReadAccessResult newReadValue = this.readBufferedOrSubjectValue();
        Object newValue = newReadValue.value;
        if (oldValue != null || newValue != null) {
            this.fireValueChange(oldValue, newValue, true);
        }
    }

    public ValueModel getTriggerChannel() {
        return this.triggerChannel;
    }

    public void setTriggerChannel(ValueModel newTriggerChannel) {
        if (newTriggerChannel == null) {
            throw new NullPointerException("The trigger channel must not be null.");
        }
        ValueModel oldTriggerChannel = this.getTriggerChannel();
        if (oldTriggerChannel != null) {
            oldTriggerChannel.removeValueChangeListener(this.triggerChangeHandler);
        }
        this.triggerChannel = newTriggerChannel;
        newTriggerChannel.addValueChangeListener(this.triggerChangeHandler);
        this.firePropertyChange("triggerChannel", oldTriggerChannel, newTriggerChannel);
    }

    public Object getValue() {
        if (this.subject == null) {
            throw new NullPointerException("The subject must not be null when reading a value from a BufferedValueModel.");
        }
        return this.isBuffering() ? this.bufferedValue : this.subject.getValue();
    }

    public void setValue(Object newBufferedValue) {
        if (this.subject == null) {
            throw new NullPointerException("The subject must not be null when setting a value to a BufferedValueModel.");
        }
        ReadAccessResult oldReadValue = this.readBufferedOrSubjectValue();
        Object oldValue = oldReadValue.value;
        this.bufferedValue = newBufferedValue;
        this.setBuffering(true);
        if (oldReadValue.readable && oldValue == newBufferedValue) {
            return;
        }
        this.fireValueChange(oldValue, newBufferedValue, true);
    }

    private ReadAccessResult readBufferedOrSubjectValue() {
        try {
            Object value = this.getValue();
            return new ReadAccessResult(value, true);
        }
        catch (Exception e) {
            return new ReadAccessResult(null, false);
        }
    }

    public void release() {
        ValueModel aTriggerChannel;
        ValueModel aSubject = this.getSubject();
        if (aSubject != null) {
            aSubject.removeValueChangeListener(this.valueChangeHandler);
        }
        if ((aTriggerChannel = this.getTriggerChannel()) != null) {
            aTriggerChannel.removeValueChangeListener(this.triggerChangeHandler);
        }
    }

    public boolean isBuffering() {
        return this.valueAssigned;
    }

    private void setBuffering(boolean newValue) {
        boolean oldValue = this.isBuffering();
        this.valueAssigned = newValue;
        this.firePropertyChange("buffering", oldValue, newValue);
    }

    private void commit() {
        if (this.isBuffering()) {
            this.setBuffering(false);
            this.valueChangeHandler.oldValue = this.bufferedValue;
            this.subject.setValue(this.bufferedValue);
            this.valueChangeHandler.oldValue = null;
        } else if (this.subject == null) {
            throw new NullPointerException("The subject must not be null while committing a value in a BufferedValueModel.");
        }
    }

    private void flush() {
        Object oldValue = this.getValue();
        this.setBuffering(false);
        Object newValue = this.getValue();
        this.fireValueChange(oldValue, newValue, true);
    }

    protected String paramString() {
        return "value=" + this.valueString() + "; buffering" + this.isBuffering();
    }

    private static final class ReadAccessResult {
        final Object value;
        final boolean readable;

        private ReadAccessResult(Object value, boolean readable) {
            this.value = value;
            this.readable = readable;
        }
    }

    private final class TriggerChangeHandler
    implements PropertyChangeListener {
        private TriggerChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            if (Boolean.TRUE.equals(evt.getNewValue())) {
                BufferedValueModel.this.commit();
            } else if (Boolean.FALSE.equals(evt.getNewValue())) {
                BufferedValueModel.this.flush();
            }
        }
    }

    private final class ValueChangeHandler
    implements PropertyChangeListener {
        Object oldValue;

        private ValueChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            if (!BufferedValueModel.this.isBuffering()) {
                BufferedValueModel.this.fireValueChange(this.oldValue != null ? this.oldValue : evt.getOldValue(), evt.getNewValue(), true);
            }
        }
    }

}

