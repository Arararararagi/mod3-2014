/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.value;

import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public final class ComponentValueModel
extends AbstractValueModel {
    public static final String PROPERTYNAME_ENABLED = "enabled";
    public static final String PROPERTYNAME_VISIBLE = "visible";
    public static final String PROPERTYNAME_EDITABLE = "editable";
    private final ValueModel subject;
    private boolean enabled;
    private boolean visible;
    private boolean editable;

    public ComponentValueModel(ValueModel subject) {
        this.subject = subject;
        this.enabled = true;
        this.editable = true;
        this.visible = true;
        subject.addValueChangeListener(new SubjectValueChangeHandler());
    }

    public Object getValue() {
        return this.subject.getValue();
    }

    public void setValue(Object newValue) {
        this.subject.setValue(newValue);
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean b) {
        boolean oldEnabled = this.isEnabled();
        this.enabled = b;
        this.firePropertyChange("enabled", oldEnabled, b);
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean b) {
        boolean oldVisible = this.isVisible();
        this.visible = b;
        this.firePropertyChange("visible", oldVisible, b);
    }

    public boolean isEditable() {
        return this.editable;
    }

    public void setEditable(boolean b) {
        boolean oldEditable = this.isEditable();
        this.editable = b;
        this.firePropertyChange("editable", oldEditable, b);
    }

    private final class SubjectValueChangeHandler
    implements PropertyChangeListener {
        private SubjectValueChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            ComponentValueModel.this.fireValueChange(evt.getOldValue(), evt.getNewValue(), true);
        }
    }

}

