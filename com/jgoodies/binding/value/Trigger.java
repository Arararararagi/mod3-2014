/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.value;

import com.jgoodies.binding.value.AbstractValueModel;

public final class Trigger
extends AbstractValueModel {
    private static final Boolean COMMIT = Boolean.TRUE;
    private static final Boolean FLUSH = Boolean.FALSE;
    private static final Boolean NEUTRAL = null;
    private Boolean value = NEUTRAL;

    public Object getValue() {
        return this.value;
    }

    public void setValue(Object newValue) {
        if (!(newValue == null || newValue instanceof Boolean)) {
            throw new IllegalArgumentException("Trigger values must be of type Boolean.");
        }
        Boolean oldValue = this.value;
        this.value = (Boolean)newValue;
        this.fireValueChange(oldValue, newValue);
    }

    public void triggerCommit() {
        if (COMMIT.equals(this.getValue())) {
            this.setValue(NEUTRAL);
        }
        this.setValue(COMMIT);
    }

    public void triggerFlush() {
        if (FLUSH.equals(this.getValue())) {
            this.setValue(NEUTRAL);
        }
        this.setValue(FLUSH);
    }
}

