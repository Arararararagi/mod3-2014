/*
 * Decompiled with CFR 0_102.
 */
package com.jgoodies.binding.value;

import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.ValueModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public abstract class AbstractConverter
extends AbstractValueModel {
    protected final ValueModel subject;
    private final PropertyChangeListener subjectValueChangeHandler;

    public AbstractConverter(ValueModel subject) {
        this.subject = subject;
        this.subjectValueChangeHandler = new SubjectValueChangeHandler();
        subject.addValueChangeListener(this.subjectValueChangeHandler);
    }

    public abstract Object convertFromSubject(Object var1);

    public Object getValue() {
        return this.convertFromSubject(this.subject.getValue());
    }

    public void release() {
        this.subject.removeValueChangeListener(this.subjectValueChangeHandler);
    }

    private final class SubjectValueChangeHandler
    implements PropertyChangeListener {
        private SubjectValueChangeHandler() {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            Object oldValue = evt.getOldValue() == null ? null : AbstractConverter.this.convertFromSubject(evt.getOldValue());
            Object newValue = evt.getNewValue() == null ? null : AbstractConverter.this.convertFromSubject(evt.getNewValue());
            AbstractConverter.this.fireValueChange(oldValue, newValue);
        }
    }

}

