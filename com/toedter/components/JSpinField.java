/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class JSpinField
extends JPanel
implements ChangeListener,
CaretListener,
ActionListener,
FocusListener {
    private static final long serialVersionUID = 1694904792717740650L;
    protected JSpinner spinner;
    protected JTextField textField;
    protected int min;
    protected int max;
    protected int value;
    protected Color darkGreen;

    public JSpinField() {
        this(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public JSpinField(int n, int n2) {
        this.setName("JSpinField");
        this.min = n;
        if (n2 < n) {
            n2 = n;
        }
        this.max = n2;
        this.value = 0;
        if (this.value < n) {
            this.value = n;
        }
        if (this.value > n2) {
            this.value = n2;
        }
        this.darkGreen = new Color(0, 150, 0);
        this.setLayout(new BorderLayout());
        this.textField = new JTextField();
        this.textField.addCaretListener(this);
        this.textField.addActionListener(this);
        this.textField.setHorizontalAlignment(4);
        this.textField.setBorder(BorderFactory.createEmptyBorder());
        this.textField.setText(Integer.toString(this.value));
        this.textField.addFocusListener(this);
        this.spinner = new JSpinner(){
            private static final long serialVersionUID = -6287709243342021172L;
            private JTextField textField;

            public Dimension getPreferredSize() {
                Dimension dimension = super.getPreferredSize();
                return new Dimension(dimension.width, this.textField.getPreferredSize().height);
            }
        };
        this.spinner.setEditor(this.textField);
        this.spinner.addChangeListener(this);
        this.add((Component)this.spinner, "Center");
    }

    public void adjustWidthToMaximumValue() {
        JTextField jTextField = new JTextField(Integer.toString(this.max));
        int n = jTextField.getPreferredSize().width;
        int n2 = jTextField.getPreferredSize().height;
        this.textField.setPreferredSize(new Dimension(n, n2));
        this.textField.revalidate();
    }

    public void stateChanged(ChangeEvent changeEvent) {
        SpinnerNumberModel spinnerNumberModel = (SpinnerNumberModel)this.spinner.getModel();
        int n = spinnerNumberModel.getNumber().intValue();
        this.setValue(n);
    }

    protected void setValue(int n, boolean bl, boolean bl2) {
        int n2 = this.value;
        this.value = n < this.min ? this.min : (n > this.max ? this.max : n);
        if (bl) {
            this.textField.setText(Integer.toString(this.value));
            this.textField.setForeground(Color.black);
        }
        if (bl2) {
            this.firePropertyChange("value", n2, this.value);
        }
    }

    public void setValue(int n) {
        this.setValue(n, true, true);
        this.spinner.setValue(new Integer(this.value));
    }

    public int getValue() {
        return this.value;
    }

    public void setMinimum(int n) {
        this.min = n;
    }

    public int getMinimum() {
        return this.min;
    }

    public void setMaximum(int n) {
        this.max = n;
    }

    public void setHorizontalAlignment(int n) {
        this.textField.setHorizontalAlignment(n);
    }

    public int getMaximum() {
        return this.max;
    }

    public void setFont(Font font) {
        if (this.textField != null) {
            this.textField.setFont(font);
        }
    }

    public void setForeground(Color color) {
        if (this.textField != null) {
            this.textField.setForeground(color);
        }
    }

    public void caretUpdate(CaretEvent caretEvent) {
        block4 : {
            try {
                int n = Integer.valueOf(this.textField.getText());
                if (n >= this.min && n <= this.max) {
                    this.textField.setForeground(this.darkGreen);
                    this.setValue(n, false, true);
                } else {
                    this.textField.setForeground(Color.red);
                }
            }
            catch (Exception var2_3) {
                if (!(var2_3 instanceof NumberFormatException)) break block4;
                this.textField.setForeground(Color.red);
            }
        }
        this.textField.repaint();
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (this.textField.getForeground().equals(this.darkGreen)) {
            this.setValue(Integer.valueOf(this.textField.getText()));
        }
    }

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.spinner.setEnabled(bl);
        this.textField.setEnabled(bl);
        if (!bl) {
            this.textField.setBackground(UIManager.getColor("TextField.inactiveBackground"));
        }
    }

    public Component getSpinner() {
        return this.spinner;
    }

    public static void main(String[] arrstring) {
        JFrame jFrame = new JFrame("JSpinField");
        jFrame.getContentPane().add(new JSpinField());
        jFrame.pack();
        jFrame.setVisible(true);
    }

    public void focusGained(FocusEvent focusEvent) {
    }

    public void focusLost(FocusEvent focusEvent) {
        this.actionPerformed(null);
    }

}

