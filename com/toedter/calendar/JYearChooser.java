/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import com.toedter.calendar.JDayChooser;
import com.toedter.components.JSpinField;
import java.awt.Component;
import java.awt.Container;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JSpinner;

public class JYearChooser
extends JSpinField {
    private static final long serialVersionUID = 2648810220491090064L;
    protected JDayChooser dayChooser;
    protected int oldYear;
    protected int startYear;
    protected int endYear;

    public JYearChooser() {
        this.setName("JYearChooser");
        Calendar calendar = Calendar.getInstance();
        this.dayChooser = null;
        this.setMinimum(calendar.getMinimum(1));
        this.setMaximum(calendar.getMaximum(1));
        this.setValue(calendar.get(1));
    }

    public void setYear(int n) {
        super.setValue(n, true, false);
        if (this.dayChooser != null) {
            this.dayChooser.setYear(this.value);
        }
        this.spinner.setValue(new Integer(this.value));
        this.firePropertyChange("year", this.oldYear, this.value);
        this.oldYear = this.value;
    }

    public void setValue(int n) {
        this.setYear(n);
    }

    public int getYear() {
        return super.getValue();
    }

    public void setDayChooser(JDayChooser jDayChooser) {
        this.dayChooser = jDayChooser;
    }

    public int getEndYear() {
        return this.getMaximum();
    }

    public void setEndYear(int n) {
        this.setMaximum(n);
    }

    public int getStartYear() {
        return this.getMinimum();
    }

    public void setStartYear(int n) {
        this.setMinimum(n);
    }

    public static void main(String[] arrstring) {
        JFrame jFrame = new JFrame("JYearChooser");
        jFrame.getContentPane().add(new JYearChooser());
        jFrame.pack();
        jFrame.setVisible(true);
    }
}

