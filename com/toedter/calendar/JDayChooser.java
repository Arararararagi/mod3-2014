/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class JDayChooser
extends JPanel
implements ActionListener,
KeyListener,
FocusListener {
    private static final long serialVersionUID = 5876398337018781820L;
    protected JButton[] days;
    protected JButton[] weeks;
    protected JButton selectedDay;
    protected JPanel weekPanel;
    protected JPanel dayPanel;
    protected int day;
    protected Color oldDayBackgroundColor;
    protected Color selectedColor;
    protected Color sundayForeground;
    protected Color weekdayForeground;
    protected Color decorationBackgroundColor;
    protected String[] dayNames;
    protected Calendar calendar;
    protected Calendar today;
    protected Locale locale;
    protected boolean initialized;
    protected boolean weekOfYearVisible;
    protected boolean decorationBackgroundVisible = true;
    protected boolean decorationBordersVisible;
    protected boolean dayBordersVisible;
    private boolean alwaysFireDayProperty;
    protected Date minSelectableDate;
    protected Date maxSelectableDate;
    protected Date defaultMinSelectableDate;
    protected Date defaultMaxSelectableDate;
    protected int maxDayCharacters;

    public JDayChooser() {
        this(false);
    }

    public JDayChooser(boolean bl) {
        int n;
        this.setName("JDayChooser");
        this.setBackground(Color.blue);
        this.weekOfYearVisible = bl;
        this.locale = Locale.getDefault();
        this.days = new JButton[49];
        this.selectedDay = null;
        this.calendar = Calendar.getInstance(this.locale);
        this.today = (Calendar)this.calendar.clone();
        this.setLayout(new BorderLayout());
        this.dayPanel = new JPanel();
        this.dayPanel.setLayout(new GridLayout(7, 7));
        this.sundayForeground = new Color(164, 0, 0);
        this.weekdayForeground = new Color(0, 90, 164);
        this.decorationBackgroundColor = new Color(210, 228, 238);
        for (n = 0; n < 7; ++n) {
            for (int i = 0; i < 7; ++i) {
                int n2 = i + 7 * n;
                if (n == 0) {
                    this.days[n2] = new DecoratorButton();
                } else {
                    this.days[n2] = new JButton("x"){
                        private static final long serialVersionUID = -7433645992591669725L;

                        public void paint(Graphics graphics) {
                            if ("Windows".equals(UIManager.getLookAndFeel().getID()) && JDayChooser.this.selectedDay == this) {
                                graphics.setColor(JDayChooser.this.selectedColor);
                                graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
                            }
                            super.paint(graphics);
                        }
                    };
                    this.days[n2].addActionListener(this);
                    this.days[n2].addKeyListener(this);
                    this.days[n2].addFocusListener(this);
                }
                this.days[n2].setMargin(new Insets(0, 0, 0, 0));
                this.days[n2].setFocusPainted(false);
                this.dayPanel.add(this.days[n2]);
            }
        }
        this.weekPanel = new JPanel();
        this.weekPanel.setLayout(new GridLayout(7, 1));
        this.weeks = new JButton[7];
        for (n = 0; n < 7; ++n) {
            this.weeks[n] = new DecoratorButton();
            this.weeks[n].setMargin(new Insets(0, 0, 0, 0));
            this.weeks[n].setFocusPainted(false);
            this.weeks[n].setForeground(new Color(100, 100, 100));
            if (n != 0) {
                this.weeks[n].setText("0" + (n + 1));
            }
            this.weekPanel.add(this.weeks[n]);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(1, 0, 1, 1, 1);
        this.minSelectableDate = this.defaultMinSelectableDate = calendar.getTime();
        calendar.set(9999, 0, 1, 1, 1);
        this.maxSelectableDate = this.defaultMaxSelectableDate = calendar.getTime();
        this.init();
        this.setDay(Calendar.getInstance().get(5));
        this.add((Component)this.dayPanel, "Center");
        if (bl) {
            this.add((Component)this.weekPanel, "West");
        }
        this.initialized = true;
        this.updateUI();
    }

    protected void init() {
        JButton jButton = new JButton();
        this.oldDayBackgroundColor = jButton.getBackground();
        this.selectedColor = new Color(160, 160, 160);
        Date date = this.calendar.getTime();
        this.calendar = Calendar.getInstance(this.locale);
        this.calendar.setTime(date);
        this.drawDayNames();
        this.drawDays();
    }

    private void drawDayNames() {
        int n = this.calendar.getFirstDayOfWeek();
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(this.locale);
        this.dayNames = dateFormatSymbols.getShortWeekdays();
        int n2 = n;
        for (int i = 0; i < 7; ++i) {
            if (this.maxDayCharacters > 0 && this.maxDayCharacters < 5 && this.dayNames[n2].length() >= this.maxDayCharacters) {
                this.dayNames[n2] = this.dayNames[n2].substring(0, this.maxDayCharacters);
            }
            this.days[i].setText(this.dayNames[n2]);
            if (n2 == 1) {
                this.days[i].setForeground(this.sundayForeground);
            } else {
                this.days[i].setForeground(this.weekdayForeground);
            }
            if (n2 < 7) {
                ++n2;
                continue;
            }
            n2-=6;
        }
    }

    protected void initDecorations() {
        for (int i = 0; i < 7; ++i) {
            this.days[i].setContentAreaFilled(this.decorationBackgroundVisible);
            this.days[i].setBorderPainted(this.decorationBordersVisible);
            this.days[i].invalidate();
            this.days[i].repaint();
            this.weeks[i].setContentAreaFilled(this.decorationBackgroundVisible);
            this.weeks[i].setBorderPainted(this.decorationBordersVisible);
            this.weeks[i].invalidate();
            this.weeks[i].repaint();
        }
    }

    protected void drawWeeks() {
        Calendar calendar = (Calendar)this.calendar.clone();
        for (int i = 1; i < 7; ++i) {
            calendar.set(5, i * 7 - 6);
            int n = calendar.get(3);
            String string = Integer.toString(n);
            if (n < 10) {
                string = "0" + string;
            }
            this.weeks[i].setText(string);
            if (i != 5 && i != 6) continue;
            this.weeks[i].setVisible(this.days[i * 7].isVisible());
        }
    }

    protected void drawDays() {
        int n;
        Calendar calendar = (Calendar)this.calendar.clone();
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(this.minSelectableDate);
        calendar2.set(11, 0);
        calendar2.set(12, 0);
        calendar2.set(13, 0);
        calendar2.set(14, 0);
        Calendar calendar3 = Calendar.getInstance();
        calendar3.setTime(this.maxSelectableDate);
        calendar3.set(11, 0);
        calendar3.set(12, 0);
        calendar3.set(13, 0);
        calendar3.set(14, 0);
        int n2 = calendar.getFirstDayOfWeek();
        calendar.set(5, 1);
        int n3 = calendar.get(7) - n2;
        if (n3 < 0) {
            n3+=7;
        }
        for (n = 0; n < n3; ++n) {
            this.days[n + 7].setVisible(false);
            this.days[n + 7].setText("");
        }
        calendar.add(2, 1);
        Date date = calendar.getTime();
        calendar.add(2, -1);
        Date date2 = calendar.getTime();
        int n4 = 0;
        Color color = this.getForeground();
        while (date2.before(date)) {
            this.days[n + n4 + 7].setText(Integer.toString(n4 + 1));
            this.days[n + n4 + 7].setVisible(true);
            if (calendar.get(6) == this.today.get(6) && calendar.get(1) == this.today.get(1)) {
                this.days[n + n4 + 7].setForeground(this.sundayForeground);
            } else {
                this.days[n + n4 + 7].setForeground(color);
            }
            if (n4 + 1 == this.day) {
                this.days[n + n4 + 7].setBackground(this.selectedColor);
                this.selectedDay = this.days[n + n4 + 7];
            } else {
                this.days[n + n4 + 7].setBackground(this.oldDayBackgroundColor);
            }
            if (calendar.before(calendar2) || calendar.after(calendar3)) {
                this.days[n + n4 + 7].setEnabled(false);
            } else {
                this.days[n + n4 + 7].setEnabled(true);
            }
            ++n4;
            calendar.add(5, 1);
            date2 = calendar.getTime();
        }
        for (int i = n4 + n + 7; i < 49; ++i) {
            this.days[i].setVisible(false);
            this.days[i].setText("");
        }
        this.drawWeeks();
    }

    public Locale getLocale() {
        return this.locale;
    }

    public void setLocale(Locale locale) {
        if (!this.initialized) {
            super.setLocale(locale);
        } else {
            this.locale = locale;
            super.setLocale(locale);
            this.init();
        }
    }

    public void setDay(int n) {
        if (n < 1) {
            n = 1;
        }
        Calendar calendar = (Calendar)this.calendar.clone();
        calendar.set(5, 1);
        calendar.add(2, 1);
        calendar.add(5, -1);
        int n2 = calendar.get(5);
        if (n > n2) {
            n = n2;
        }
        int n3 = this.day;
        this.day = n;
        if (this.selectedDay != null) {
            this.selectedDay.setBackground(this.oldDayBackgroundColor);
            this.selectedDay.repaint();
        }
        for (int i = 7; i < 49; ++i) {
            if (!this.days[i].getText().equals(Integer.toString(this.day))) continue;
            this.selectedDay = this.days[i];
            this.selectedDay.setBackground(this.selectedColor);
            break;
        }
        if (this.alwaysFireDayProperty) {
            this.firePropertyChange("day", 0, this.day);
        } else {
            this.firePropertyChange("day", n3, this.day);
        }
    }

    public void setAlwaysFireDayProperty(boolean bl) {
        this.alwaysFireDayProperty = bl;
    }

    public int getDay() {
        return this.day;
    }

    public void setMonth(int n) {
        int n2 = this.calendar.getActualMaximum(5);
        this.calendar.set(2, n);
        if (n2 == this.day) {
            this.day = this.calendar.getActualMaximum(5);
        }
        boolean bl = this.alwaysFireDayProperty;
        this.alwaysFireDayProperty = false;
        this.setDay(this.day);
        this.alwaysFireDayProperty = bl;
        this.drawDays();
    }

    public void setYear(int n) {
        this.calendar.set(1, n);
        this.drawDays();
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
        this.drawDays();
    }

    public void setFont(Font font) {
        int n;
        if (this.days != null) {
            for (n = 0; n < 49; ++n) {
                this.days[n].setFont(font);
            }
        }
        if (this.weeks != null) {
            for (n = 0; n < 7; ++n) {
                this.weeks[n].setFont(font);
            }
        }
    }

    public void setForeground(Color color) {
        super.setForeground(color);
        if (this.days != null) {
            for (int i = 7; i < 49; ++i) {
                this.days[i].setForeground(color);
            }
            this.drawDays();
        }
    }

    public void actionPerformed(ActionEvent actionEvent) {
        JButton jButton = (JButton)actionEvent.getSource();
        String string = jButton.getText();
        int n = new Integer(string);
        this.setDay(n);
    }

    public void focusGained(FocusEvent focusEvent) {
    }

    public void focusLost(FocusEvent focusEvent) {
    }

    public void keyPressed(KeyEvent keyEvent) {
        int n = keyEvent.getKeyCode() == 38 ? -7 : (keyEvent.getKeyCode() == 40 ? 7 : (keyEvent.getKeyCode() == 37 ? -1 : (keyEvent.getKeyCode() == 39 ? 1 : 0)));
        int n2 = this.getDay() + n;
        if (n2 >= 1 && n2 <= this.calendar.getMaximum(5)) {
            this.setDay(n2);
        }
    }

    public void keyTyped(KeyEvent keyEvent) {
    }

    public void keyReleased(KeyEvent keyEvent) {
    }

    public void setEnabled(boolean bl) {
        int n;
        super.setEnabled(bl);
        for (n = 0; n < this.days.length; n = (int)((short)(n + 1))) {
            if (this.days[n] == null) continue;
            this.days[n].setEnabled(bl);
        }
        for (n = 0; n < this.weeks.length; n = (int)((short)(n + 1))) {
            if (this.weeks[n] == null) continue;
            this.weeks[n].setEnabled(bl);
        }
    }

    public boolean isWeekOfYearVisible() {
        return this.weekOfYearVisible;
    }

    public void setWeekOfYearVisible(boolean bl) {
        if (bl == this.weekOfYearVisible) {
            return;
        }
        if (bl) {
            this.add((Component)this.weekPanel, "West");
        } else {
            this.remove(this.weekPanel);
        }
        this.weekOfYearVisible = bl;
        this.validate();
        this.dayPanel.validate();
    }

    public JPanel getDayPanel() {
        return this.dayPanel;
    }

    public Color getDecorationBackgroundColor() {
        return this.decorationBackgroundColor;
    }

    public void setDecorationBackgroundColor(Color color) {
        int n;
        this.decorationBackgroundColor = color;
        if (this.days != null) {
            for (n = 0; n < 7; ++n) {
                this.days[n].setBackground(color);
            }
        }
        if (this.weeks != null) {
            for (n = 0; n < 7; ++n) {
                this.weeks[n].setBackground(color);
            }
        }
    }

    public Color getSundayForeground() {
        return this.sundayForeground;
    }

    public Color getWeekdayForeground() {
        return this.weekdayForeground;
    }

    public void setSundayForeground(Color color) {
        this.sundayForeground = color;
        this.drawDayNames();
        this.drawDays();
    }

    public void setWeekdayForeground(Color color) {
        this.weekdayForeground = color;
        this.drawDayNames();
        this.drawDays();
    }

    public void setFocus() {
        if (this.selectedDay != null) {
            this.selectedDay.requestFocus();
        }
    }

    public boolean isDecorationBackgroundVisible() {
        return this.decorationBackgroundVisible;
    }

    public void setDecorationBackgroundVisible(boolean bl) {
        this.decorationBackgroundVisible = bl;
        this.initDecorations();
    }

    public boolean isDecorationBordersVisible() {
        return this.decorationBordersVisible;
    }

    public boolean isDayBordersVisible() {
        return this.dayBordersVisible;
    }

    public void setDecorationBordersVisible(boolean bl) {
        this.decorationBordersVisible = bl;
        this.initDecorations();
    }

    public void setDayBordersVisible(boolean bl) {
        this.dayBordersVisible = bl;
        if (this.initialized) {
            for (int i = 7; i < 49; ++i) {
                if ("Windows".equals(UIManager.getLookAndFeel().getID())) {
                    this.days[i].setContentAreaFilled(bl);
                } else {
                    this.days[i].setContentAreaFilled(true);
                }
                this.days[i].setBorderPainted(bl);
            }
        }
    }

    public void updateUI() {
        super.updateUI();
        this.setFont(Font.decode("Dialog Plain 11"));
        if (this.weekPanel != null) {
            this.weekPanel.updateUI();
        }
        if (this.initialized) {
            if ("Windows".equals(UIManager.getLookAndFeel().getID())) {
                this.setDayBordersVisible(false);
                this.setDecorationBackgroundVisible(true);
                this.setDecorationBordersVisible(false);
            } else {
                this.setDayBordersVisible(true);
                this.setDecorationBackgroundVisible(this.decorationBackgroundVisible);
                this.setDecorationBordersVisible(this.decorationBordersVisible);
            }
        }
    }

    public void setSelectableDateRange(Date date, Date date2) {
        this.minSelectableDate = date == null ? this.defaultMinSelectableDate : date;
        this.maxSelectableDate = date2 == null ? this.defaultMaxSelectableDate : date2;
        if (this.maxSelectableDate.before(this.minSelectableDate)) {
            this.minSelectableDate = this.defaultMinSelectableDate;
            this.maxSelectableDate = this.defaultMaxSelectableDate;
        }
        this.drawDays();
    }

    public Date setMaxSelectableDate(Date date) {
        this.maxSelectableDate = date == null ? this.defaultMaxSelectableDate : date;
        this.drawDays();
        return this.maxSelectableDate;
    }

    public Date setMinSelectableDate(Date date) {
        this.minSelectableDate = date == null ? this.defaultMinSelectableDate : date;
        this.drawDays();
        return this.minSelectableDate;
    }

    public Date getMaxSelectableDate() {
        return this.maxSelectableDate;
    }

    public Date getMinSelectableDate() {
        return this.minSelectableDate;
    }

    public int getMaxDayCharacters() {
        return this.maxDayCharacters;
    }

    public void setMaxDayCharacters(int n) {
        if (n == this.maxDayCharacters) {
            return;
        }
        this.maxDayCharacters = n < 0 || n > 4 ? 0 : n;
        this.drawDayNames();
        this.drawDays();
        this.invalidate();
    }

    public static void main(String[] arrstring) {
        JFrame jFrame = new JFrame("JDayChooser");
        jFrame.getContentPane().add(new JDayChooser());
        jFrame.pack();
        jFrame.setVisible(true);
    }

    class DecoratorButton
    extends JButton {
        private static final long serialVersionUID = -5306477668406547496L;

        public DecoratorButton() {
            this.setBackground(JDayChooser.this.decorationBackgroundColor);
            this.setContentAreaFilled(JDayChooser.this.decorationBackgroundVisible);
            this.setBorderPainted(JDayChooser.this.decorationBordersVisible);
        }

        public void addMouseListener(MouseListener mouseListener) {
        }

        public boolean isFocusable() {
            return false;
        }

        public void paint(Graphics graphics) {
            if ("Windows".equals(UIManager.getLookAndFeel().getID())) {
                if (JDayChooser.this.decorationBackgroundVisible) {
                    graphics.setColor(JDayChooser.this.decorationBackgroundColor);
                } else {
                    graphics.setColor(JDayChooser.this.days[7].getBackground());
                }
                graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
                if (this.isBorderPainted()) {
                    this.setContentAreaFilled(true);
                } else {
                    this.setContentAreaFilled(false);
                }
            }
            super.paint(graphics);
        }
    }

}

