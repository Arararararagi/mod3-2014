/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JYearChooser;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class JMonthChooser
extends JPanel
implements ItemListener,
ChangeListener {
    private static final long serialVersionUID = -2028361332231218527L;
    protected boolean hasSpinner;
    private Locale locale;
    private int month;
    private int oldSpinnerValue = 0;
    private JDayChooser dayChooser;
    private JYearChooser yearChooser;
    private JComboBox comboBox;
    private JSpinner spinner;
    private boolean initialized;
    private boolean localInitialize;

    public JMonthChooser() {
        this(true);
    }

    public JMonthChooser(boolean bl) {
        this.setName("JMonthChooser");
        this.hasSpinner = bl;
        this.setLayout(new BorderLayout());
        this.comboBox = new JComboBox();
        this.comboBox.addItemListener(this);
        this.locale = Locale.getDefault();
        this.initNames();
        if (bl) {
            this.spinner = new JSpinner(){
                private static final long serialVersionUID = 1;
                private JTextField textField;

                public Dimension getPreferredSize() {
                    Dimension dimension = super.getPreferredSize();
                    return new Dimension(dimension.width, this.textField.getPreferredSize().height);
                }
            };
            this.spinner.addChangeListener(this);
            this.spinner.setEditor(this.comboBox);
            this.comboBox.setBorder(new EmptyBorder(0, 0, 0, 0));
            this.updateUI();
            this.add((Component)this.spinner, "West");
        } else {
            this.add((Component)this.comboBox, "West");
        }
        this.initialized = true;
        this.setMonth(Calendar.getInstance().get(2));
    }

    public void initNames() {
        this.localInitialize = true;
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(this.locale);
        String[] arrstring = dateFormatSymbols.getMonths();
        if (this.comboBox.getItemCount() == 12) {
            this.comboBox.removeAllItems();
        }
        for (int i = 0; i < 12; ++i) {
            this.comboBox.addItem(arrstring[i]);
        }
        this.localInitialize = false;
        this.comboBox.setSelectedIndex(this.month);
    }

    public void stateChanged(ChangeEvent changeEvent) {
        SpinnerNumberModel spinnerNumberModel = (SpinnerNumberModel)((JSpinner)changeEvent.getSource()).getModel();
        int n = spinnerNumberModel.getNumber().intValue();
        boolean bl = n > this.oldSpinnerValue;
        this.oldSpinnerValue = n;
        int n2 = this.getMonth();
        if (bl) {
            if (++n2 == 12) {
                n2 = 0;
                if (this.yearChooser != null) {
                    int n3 = this.yearChooser.getYear();
                    this.yearChooser.setYear(++n3);
                }
            }
        } else if (--n2 == -1) {
            n2 = 11;
            if (this.yearChooser != null) {
                int n4 = this.yearChooser.getYear();
                this.yearChooser.setYear(--n4);
            }
        }
        this.setMonth(n2);
    }

    public void itemStateChanged(ItemEvent itemEvent) {
        int n;
        if (itemEvent.getStateChange() == 1 && (n = this.comboBox.getSelectedIndex()) >= 0 && n != this.month) {
            this.setMonth(n, false);
        }
    }

    private void setMonth(int n, boolean bl) {
        if (!this.initialized || this.localInitialize) {
            return;
        }
        int n2 = this.month;
        this.month = n;
        if (bl) {
            this.comboBox.setSelectedIndex(this.month);
        }
        if (this.dayChooser != null) {
            this.dayChooser.setMonth(this.month);
        }
        this.firePropertyChange("month", n2, this.month);
    }

    public void setMonth(int n) {
        if (n < 0 || n == Integer.MIN_VALUE) {
            this.setMonth(0, true);
        } else if (n > 11) {
            this.setMonth(11, true);
        } else {
            this.setMonth(n, true);
        }
    }

    public int getMonth() {
        return this.month;
    }

    public void setDayChooser(JDayChooser jDayChooser) {
        this.dayChooser = jDayChooser;
    }

    public void setYearChooser(JYearChooser jYearChooser) {
        this.yearChooser = jYearChooser;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public void setLocale(Locale locale) {
        if (!this.initialized) {
            super.setLocale(locale);
        } else {
            this.locale = locale;
            this.initNames();
        }
    }

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.comboBox.setEnabled(bl);
        if (this.spinner != null) {
            this.spinner.setEnabled(bl);
        }
    }

    public Component getComboBox() {
        return this.comboBox;
    }

    public Component getSpinner() {
        return this.spinner;
    }

    public boolean hasSpinner() {
        return this.hasSpinner;
    }

    public void setFont(Font font) {
        if (this.comboBox != null) {
            this.comboBox.setFont(font);
        }
        super.setFont(font);
    }

    public void updateUI() {
        JSpinner jSpinner = new JSpinner();
        if (this.spinner != null) {
            if ("Windows".equals(UIManager.getLookAndFeel().getID())) {
                this.spinner.setBorder(jSpinner.getBorder());
            } else {
                this.spinner.setBorder(new EmptyBorder(0, 0, 0, 0));
            }
        }
    }

    public static void main(String[] arrstring) {
        JFrame jFrame = new JFrame("MonthChooser");
        jFrame.getContentPane().add(new JMonthChooser());
        jFrame.pack();
        jFrame.setVisible(true);
    }

}

