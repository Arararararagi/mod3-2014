/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    protected Date minSelectableDate;
    protected Date maxSelectableDate;
    protected Date defaultMinSelectableDate;
    protected Date defaultMaxSelectableDate;

    public DateUtil() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1, 0, 1, 1, 1);
        this.minSelectableDate = this.defaultMinSelectableDate = calendar.getTime();
        calendar.set(9999, 0, 1, 1, 1);
        this.maxSelectableDate = this.defaultMaxSelectableDate = calendar.getTime();
    }

    public void setSelectableDateRange(Date date, Date date2) {
        this.minSelectableDate = date == null ? this.defaultMinSelectableDate : date;
        this.maxSelectableDate = date2 == null ? this.defaultMaxSelectableDate : date2;
        if (this.maxSelectableDate.before(this.minSelectableDate)) {
            this.minSelectableDate = this.defaultMinSelectableDate;
            this.maxSelectableDate = this.defaultMaxSelectableDate;
        }
    }

    public Date setMaxSelectableDate(Date date) {
        this.maxSelectableDate = date == null ? this.defaultMaxSelectableDate : date;
        return this.maxSelectableDate;
    }

    public Date setMinSelectableDate(Date date) {
        this.minSelectableDate = date == null ? this.defaultMinSelectableDate : date;
        return this.minSelectableDate;
    }

    public Date getMaxSelectableDate() {
        return this.maxSelectableDate;
    }

    public Date getMinSelectableDate() {
        return this.minSelectableDate;
    }

    public boolean checkDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(this.minSelectableDate);
        calendar2.set(11, 0);
        calendar2.set(12, 0);
        calendar2.set(13, 0);
        calendar2.set(14, 0);
        Calendar calendar3 = Calendar.getInstance();
        calendar3.setTime(this.maxSelectableDate);
        calendar3.set(11, 0);
        calendar3.set(12, 0);
        calendar3.set(13, 0);
        calendar3.set(14, 0);
        return !calendar.before(calendar2) && !calendar.after(calendar3);
    }
}

