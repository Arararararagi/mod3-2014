/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import com.toedter.calendar.DateUtil;
import com.toedter.calendar.IDateEditor;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.MaskFormatter;

public class JTextFieldDateEditor
extends JFormattedTextField
implements IDateEditor,
CaretListener,
FocusListener,
ActionListener {
    private static final long serialVersionUID = -8901842591101625304L;
    protected Date date;
    protected SimpleDateFormat dateFormatter = (SimpleDateFormat)DateFormat.getDateInstance(2);
    protected MaskFormatter maskFormatter;
    protected String datePattern;
    protected String maskPattern;
    protected char placeholder;
    protected Color darkGreen;
    protected DateUtil dateUtil;
    private boolean isMaskVisible;
    private boolean ignoreDatePatternChange;
    private int hours;
    private int minutes;
    private int seconds;
    private int millis;
    private Calendar calendar;

    public JTextFieldDateEditor() {
        this(false, null, null, ' ');
    }

    public JTextFieldDateEditor(String datePattern, String maskPattern, char placeholder) {
        this(true, datePattern, maskPattern, placeholder);
    }

    public JTextFieldDateEditor(boolean showMask, String datePattern, String maskPattern, char placeholder) {
        this.dateFormatter.setLenient(false);
        this.setDateFormatString(datePattern);
        if (datePattern != null) {
            this.ignoreDatePatternChange = true;
        }
        this.placeholder = placeholder;
        this.maskPattern = maskPattern == null ? this.createMaskFromDatePattern(this.datePattern) : maskPattern;
        this.setToolTipText(this.datePattern);
        this.setMaskVisible(showMask);
        this.addCaretListener(this);
        this.addFocusListener(this);
        this.addActionListener(this);
        this.darkGreen = new Color(0, 150, 0);
        this.calendar = Calendar.getInstance();
        this.dateUtil = new DateUtil();
    }

    @Override
    public Date getDate() {
        try {
            this.calendar.setTime(this.dateFormatter.parse(this.getText()));
            this.calendar.set(11, this.hours);
            this.calendar.set(12, this.minutes);
            this.calendar.set(13, this.seconds);
            this.calendar.set(14, this.millis);
            this.date = this.calendar.getTime();
        }
        catch (ParseException e) {
            this.date = null;
        }
        return this.date;
    }

    @Override
    public void setDate(Date date) {
        this.setDate(date, true);
    }

    protected void setDate(Date date, boolean firePropertyChange) {
        Date oldDate = this.date;
        this.date = date;
        if (date == null) {
            this.setText("");
        } else {
            this.calendar.setTime(date);
            this.hours = this.calendar.get(11);
            this.minutes = this.calendar.get(12);
            this.seconds = this.calendar.get(13);
            this.millis = this.calendar.get(14);
            String formattedDate = this.dateFormatter.format(date);
            try {
                this.setText(formattedDate);
            }
            catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
        if (date != null && this.dateUtil.checkDate(date)) {
            this.setForeground(Color.BLACK);
        }
        if (firePropertyChange) {
            this.firePropertyChange("date", oldDate, date);
        }
    }

    @Override
    public void setDateFormatString(String dateFormatString) {
        if (this.ignoreDatePatternChange) {
            return;
        }
        try {
            this.dateFormatter.applyPattern(dateFormatString);
        }
        catch (RuntimeException e) {
            this.dateFormatter = (SimpleDateFormat)DateFormat.getDateInstance(2);
            this.dateFormatter.setLenient(false);
        }
        this.datePattern = this.dateFormatter.toPattern();
        this.setToolTipText(this.datePattern);
        this.setDate(this.date, false);
    }

    @Override
    public String getDateFormatString() {
        return this.datePattern;
    }

    @Override
    public JComponent getUiComponent() {
        return this;
    }

    @Override
    public void caretUpdate(CaretEvent event) {
        String text = this.getText().trim();
        String emptyMask = this.maskPattern.replace('#', this.placeholder);
        if (text.length() == 0 || text.equals(emptyMask)) {
            this.setForeground(Color.BLACK);
            return;
        }
        try {
            Date date = this.dateFormatter.parse(this.getText());
            if (this.dateUtil.checkDate(date)) {
                this.setForeground(this.darkGreen);
            } else {
                this.setForeground(Color.RED);
            }
        }
        catch (Exception e) {
            this.setForeground(Color.RED);
        }
    }

    @Override
    public void focusLost(FocusEvent focusEvent) {
        this.checkText();
    }

    private void checkText() {
        try {
            Date date = this.dateFormatter.parse(this.getText());
            this.setDate(date, true);
            this.firePropertyChange("dateEmpty", Boolean.TRUE, Boolean.FALSE);
        }
        catch (Exception e) {
            this.firePropertyChange("date", this.getDate(), null);
            if ("".equals(this.getText().trim())) {
                this.firePropertyChange("dateEmpty", Boolean.FALSE, Boolean.TRUE);
            }
            this.firePropertyChange("dateEmpty", Boolean.TRUE, Boolean.FALSE);
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void setLocale(Locale locale) {
        if (locale == this.getLocale() || this.ignoreDatePatternChange) {
            return;
        }
        super.setLocale(locale);
        this.dateFormatter = (SimpleDateFormat)DateFormat.getDateInstance(2, locale);
        this.setToolTipText(this.dateFormatter.toPattern());
        this.setDate(this.date, false);
        this.doLayout();
    }

    public String createMaskFromDatePattern(String datePattern) {
        String symbols = "GyMdkHmsSEDFwWahKzZ";
        String mask = "";
        for (int i = 0; i < datePattern.length(); ++i) {
            char ch = datePattern.charAt(i);
            boolean symbolFound = false;
            for (int n = 0; n < symbols.length(); ++n) {
                if (symbols.charAt(n) != ch) continue;
                mask = mask + "#";
                symbolFound = true;
                break;
            }
            if (symbolFound) continue;
            mask = mask + ch;
        }
        return mask;
    }

    public boolean isMaskVisible() {
        return this.isMaskVisible;
    }

    public void setMaskVisible(boolean isMaskVisible) {
        this.isMaskVisible = isMaskVisible;
        if (isMaskVisible && this.maskFormatter == null) {
            try {
                this.maskFormatter = new MaskFormatter(this.createMaskFromDatePattern(this.datePattern));
                this.maskFormatter.setPlaceholderCharacter(this.placeholder);
                this.maskFormatter.install(this);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.datePattern != null) {
            return new JTextField(this.datePattern).getPreferredSize();
        }
        return super.getPreferredSize();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.checkText();
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        if (!b) {
            super.setBackground(UIManager.getColor("TextField.inactiveBackground"));
        }
    }

    @Override
    public Date getMaxSelectableDate() {
        return this.dateUtil.getMaxSelectableDate();
    }

    @Override
    public Date getMinSelectableDate() {
        return this.dateUtil.getMinSelectableDate();
    }

    @Override
    public void setMaxSelectableDate(Date max) {
        this.dateUtil.setMaxSelectableDate(max);
        this.checkText();
    }

    @Override
    public void setMinSelectableDate(Date min) {
        this.dateUtil.setMinSelectableDate(min);
        this.checkText();
    }

    @Override
    public void setSelectableDateRange(Date min, Date max) {
        this.dateUtil.setSelectableDateRange(min, max);
        this.checkText();
    }

    public static void main(String[] s) {
        JFrame frame = new JFrame("JTextFieldDateEditor");
        JTextFieldDateEditor jTextFieldDateEditor = new JTextFieldDateEditor();
        jTextFieldDateEditor.setDate(new Date());
        frame.getContentPane().add(jTextFieldDateEditor);
        frame.pack();
        frame.setVisible(true);
    }
}

