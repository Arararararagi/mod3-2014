/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JYearChooser;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class JCalendar
extends JPanel
implements PropertyChangeListener {
    private static final long serialVersionUID = 8913369762644440133L;
    private Calendar calendar;
    protected JDayChooser dayChooser;
    private boolean initialized = false;
    protected boolean weekOfYearVisible = true;
    protected Locale locale;
    protected JMonthChooser monthChooser;
    private JPanel monthYearPanel;
    protected JYearChooser yearChooser;
    protected Date minSelectableDate;
    protected Date maxSelectableDate;

    public JCalendar() {
        this(null, null, true, true);
    }

    public JCalendar(Date date) {
        this(date, null, true, true);
    }

    public JCalendar(Calendar calendar) {
        this(null, null, true, true);
        this.setCalendar(calendar);
    }

    public JCalendar(Locale locale) {
        this(null, locale, true, true);
    }

    public JCalendar(Date date, Locale locale) {
        this(date, locale, true, true);
    }

    public JCalendar(Date date, boolean bl) {
        this(date, null, bl, true);
    }

    public JCalendar(Locale locale, boolean bl) {
        this(null, locale, bl, true);
    }

    public JCalendar(boolean bl) {
        this(null, null, bl, true);
    }

    public JCalendar(Date date, Locale locale, boolean bl, boolean bl2) {
        this.setName("JCalendar");
        this.dayChooser = null;
        this.monthChooser = null;
        this.yearChooser = null;
        this.weekOfYearVisible = bl2;
        this.locale = locale;
        if (locale == null) {
            this.locale = Locale.getDefault();
        }
        this.calendar = Calendar.getInstance();
        this.setLayout(new BorderLayout());
        this.monthYearPanel = new JPanel();
        this.monthYearPanel.setLayout(new BorderLayout());
        this.monthChooser = new JMonthChooser(bl);
        this.yearChooser = new JYearChooser();
        this.monthChooser.setYearChooser(this.yearChooser);
        this.monthYearPanel.add((Component)this.monthChooser, "West");
        this.monthYearPanel.add((Component)this.yearChooser, "Center");
        this.monthYearPanel.setBorder(BorderFactory.createEmptyBorder());
        this.dayChooser = new JDayChooser(bl2);
        this.dayChooser.addPropertyChangeListener(this);
        this.monthChooser.setDayChooser(this.dayChooser);
        this.monthChooser.addPropertyChangeListener(this);
        this.yearChooser.setDayChooser(this.dayChooser);
        this.yearChooser.addPropertyChangeListener(this);
        this.add((Component)this.monthYearPanel, "North");
        this.add((Component)this.dayChooser, "Center");
        if (date != null) {
            this.calendar.setTime(date);
        }
        this.initialized = true;
        this.setCalendar(this.calendar);
    }

    public static void main(String[] arrstring) {
        JFrame jFrame = new JFrame("JCalendar");
        JCalendar jCalendar = new JCalendar();
        jFrame.getContentPane().add(jCalendar);
        jFrame.pack();
        jFrame.setVisible(true);
    }

    public Calendar getCalendar() {
        return this.calendar;
    }

    public JDayChooser getDayChooser() {
        return this.dayChooser;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public JMonthChooser getMonthChooser() {
        return this.monthChooser;
    }

    public JYearChooser getYearChooser() {
        return this.yearChooser;
    }

    public boolean isWeekOfYearVisible() {
        return this.dayChooser.isWeekOfYearVisible();
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (this.calendar != null) {
            Calendar calendar = (Calendar)this.calendar.clone();
            if (propertyChangeEvent.getPropertyName().equals("day")) {
                calendar.set(5, (Integer)propertyChangeEvent.getNewValue());
                this.setCalendar(calendar, false);
            } else if (propertyChangeEvent.getPropertyName().equals("month")) {
                calendar.set(2, (Integer)propertyChangeEvent.getNewValue());
                this.setCalendar(calendar, false);
            } else if (propertyChangeEvent.getPropertyName().equals("year")) {
                calendar.set(1, (Integer)propertyChangeEvent.getNewValue());
                this.setCalendar(calendar, false);
            } else if (propertyChangeEvent.getPropertyName().equals("date")) {
                calendar.setTime((Date)propertyChangeEvent.getNewValue());
                this.setCalendar(calendar, true);
            }
        }
    }

    public void setBackground(Color color) {
        super.setBackground(color);
        if (this.dayChooser != null) {
            this.dayChooser.setBackground(color);
        }
    }

    public void setCalendar(Calendar calendar) {
        this.setCalendar(calendar, true);
    }

    private void setCalendar(Calendar calendar, boolean bl) {
        if (calendar == null) {
            this.setDate(null);
        }
        Calendar calendar2 = this.calendar;
        this.calendar = calendar;
        if (bl) {
            this.yearChooser.setYear(calendar.get(1));
            this.monthChooser.setMonth(calendar.get(2));
            this.dayChooser.setDay(calendar.get(5));
        }
        this.firePropertyChange("calendar", calendar2, this.calendar);
    }

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        if (this.dayChooser != null) {
            this.dayChooser.setEnabled(bl);
            this.monthChooser.setEnabled(bl);
            this.yearChooser.setEnabled(bl);
        }
    }

    public boolean isEnabled() {
        return super.isEnabled();
    }

    public void setFont(Font font) {
        super.setFont(font);
        if (this.dayChooser != null) {
            this.dayChooser.setFont(font);
            this.monthChooser.setFont(font);
            this.yearChooser.setFont(font);
        }
    }

    public void setForeground(Color color) {
        super.setForeground(color);
        if (this.dayChooser != null) {
            this.dayChooser.setForeground(color);
            this.monthChooser.setForeground(color);
            this.yearChooser.setForeground(color);
        }
    }

    public void setLocale(Locale locale) {
        if (!this.initialized) {
            super.setLocale(locale);
        } else {
            Locale locale2 = this.locale;
            this.locale = locale;
            this.dayChooser.setLocale(this.locale);
            this.monthChooser.setLocale(this.locale);
            this.firePropertyChange("locale", locale2, this.locale);
        }
    }

    public void setWeekOfYearVisible(boolean bl) {
        this.dayChooser.setWeekOfYearVisible(bl);
        this.setLocale(this.locale);
    }

    public boolean isDecorationBackgroundVisible() {
        return this.dayChooser.isDecorationBackgroundVisible();
    }

    public void setDecorationBackgroundVisible(boolean bl) {
        this.dayChooser.setDecorationBackgroundVisible(bl);
        this.setLocale(this.locale);
    }

    public boolean isDecorationBordersVisible() {
        return this.dayChooser.isDecorationBordersVisible();
    }

    public void setDecorationBordersVisible(boolean bl) {
        this.dayChooser.setDecorationBordersVisible(bl);
        this.setLocale(this.locale);
    }

    public Color getDecorationBackgroundColor() {
        return this.dayChooser.getDecorationBackgroundColor();
    }

    public void setDecorationBackgroundColor(Color color) {
        this.dayChooser.setDecorationBackgroundColor(color);
    }

    public Color getSundayForeground() {
        return this.dayChooser.getSundayForeground();
    }

    public Color getWeekdayForeground() {
        return this.dayChooser.getWeekdayForeground();
    }

    public void setSundayForeground(Color color) {
        this.dayChooser.setSundayForeground(color);
    }

    public void setWeekdayForeground(Color color) {
        this.dayChooser.setWeekdayForeground(color);
    }

    public Date getDate() {
        return new Date(this.calendar.getTimeInMillis());
    }

    public void setDate(Date date) {
        Date date2 = this.calendar.getTime();
        this.calendar.setTime(date);
        int n = this.calendar.get(1);
        int n2 = this.calendar.get(2);
        int n3 = this.calendar.get(5);
        this.yearChooser.setYear(n);
        this.monthChooser.setMonth(n2);
        this.dayChooser.setCalendar(this.calendar);
        this.dayChooser.setDay(n3);
        this.firePropertyChange("date", date2, date);
    }

    public void setSelectableDateRange(Date date, Date date2) {
        this.dayChooser.setSelectableDateRange(date, date2);
    }

    public Date getMaxSelectableDate() {
        return this.dayChooser.getMaxSelectableDate();
    }

    public Date getMinSelectableDate() {
        return this.dayChooser.getMinSelectableDate();
    }

    public void setMaxSelectableDate(Date date) {
        this.dayChooser.setMaxSelectableDate(date);
    }

    public void setMinSelectableDate(Date date) {
        this.dayChooser.setMinSelectableDate(date);
    }

    public int getMaxDayCharacters() {
        return this.dayChooser.getMaxDayCharacters();
    }

    public void setMaxDayCharacters(int n) {
        this.dayChooser.setMaxDayCharacters(n);
    }
}

