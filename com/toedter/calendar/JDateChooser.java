/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import com.toedter.calendar.IDateEditor;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JMonthChooser;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class JDateChooser
extends JPanel
implements ActionListener,
PropertyChangeListener {
    private static final long serialVersionUID = -4306412745720670722L;
    protected IDateEditor dateEditor;
    protected JButton calendarButton;
    protected JCalendar jcalendar;
    protected JPopupMenu popup;
    protected boolean isInitialized;
    protected boolean dateSelected;
    protected Date lastSelectedDate;
    private ChangeListener changeListener;

    public JDateChooser() {
        this(null, null, null, null);
    }

    public JDateChooser(IDateEditor dateEditor) {
        this(null, null, null, dateEditor);
    }

    public JDateChooser(Date date) {
        this(date, null);
    }

    public JDateChooser(Date date, String dateFormatString) {
        this(date, dateFormatString, null);
    }

    public JDateChooser(Date date, String dateFormatString, IDateEditor dateEditor) {
        this(null, date, dateFormatString, dateEditor);
    }

    public JDateChooser(String datePattern, String maskPattern, char placeholder) {
        this(null, null, datePattern, new JTextFieldDateEditor(datePattern, maskPattern, placeholder));
    }

    public JDateChooser(JCalendar jcal, Date date, String dateFormatString, IDateEditor dateEditor) {
        this.setName("JDateChooser");
        this.dateEditor = dateEditor;
        if (this.dateEditor == null) {
            this.dateEditor = new JTextFieldDateEditor();
        }
        this.dateEditor.addPropertyChangeListener("date", this);
        this.dateEditor.addPropertyChangeListener("dateEmpty", this);
        if (jcal == null) {
            this.jcalendar = new JCalendar(date);
        } else {
            this.jcalendar = jcal;
            if (date != null) {
                this.jcalendar.setDate(date);
            }
        }
        this.setLayout(new BorderLayout());
        this.jcalendar.getDayChooser().addPropertyChangeListener("day", this);
        this.jcalendar.getDayChooser().setAlwaysFireDayProperty(true);
        this.setDateFormatString(dateFormatString);
        this.setDate(date);
        URL iconURL = this.getClass().getResource("/com/toedter/calendar/images/JDateChooserIcon.gif");
        ImageIcon icon = new ImageIcon(iconURL);
        this.calendarButton = new JButton(icon){
            private static final long serialVersionUID = -1913767779079949668L;

            @Override
            public boolean isFocusable() {
                return false;
            }
        };
        this.calendarButton.setMargin(new Insets(0, 0, 0, 0));
        this.calendarButton.addActionListener(this);
        this.calendarButton.setMnemonic(67);
        this.add((Component)this.calendarButton, "East");
        this.add((Component)this.dateEditor.getUiComponent(), "Center");
        this.calendarButton.setMargin(new Insets(0, 0, 0, 0));
        this.popup = new JPopupMenu(){
            private static final long serialVersionUID = -6078272560337577761L;

            @Override
            public void setVisible(boolean b) {
                Boolean isCanceled = (Boolean)this.getClientProperty("JPopupMenu.firePopupMenuCanceled");
                if (b || !b && JDateChooser.this.dateSelected || isCanceled != null && !b && isCanceled.booleanValue()) {
                    super.setVisible(b);
                }
            }
        };
        this.popup.setLightWeightPopupEnabled(true);
        this.popup.add(this.jcalendar);
        this.lastSelectedDate = date;
        this.changeListener = new ChangeListener(){
            boolean hasListened;

            @Override
            public void stateChanged(ChangeEvent e) {
                if (this.hasListened) {
                    this.hasListened = false;
                    return;
                }
                if (JDateChooser.this.popup.isVisible() && JDateChooser.this.jcalendar.monthChooser.getComboBox().hasFocus()) {
                    MenuElement[] me = MenuSelectionManager.defaultManager().getSelectedPath();
                    MenuElement[] newMe = new MenuElement[me.length + 1];
                    newMe[0] = JDateChooser.this.popup;
                    for (int i = 0; i < me.length; ++i) {
                        newMe[i + 1] = me[i];
                    }
                    this.hasListened = true;
                    MenuSelectionManager.defaultManager().setSelectedPath(newMe);
                }
            }
        };
        MenuSelectionManager.defaultManager().addChangeListener(this.changeListener);
        this.isInitialized = true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int x = this.calendarButton.getWidth() - (int)this.popup.getPreferredSize().getWidth();
        int y = this.calendarButton.getY() + this.calendarButton.getHeight();
        Calendar calendar = Calendar.getInstance();
        Date date = this.dateEditor.getDate();
        if (date != null) {
            calendar.setTime(date);
        }
        this.jcalendar.setCalendar(calendar);
        this.popup.show(this.calendarButton, x, y);
        this.dateSelected = false;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("day")) {
            if (this.popup.isVisible()) {
                this.dateSelected = true;
                this.popup.setVisible(false);
                this.setDate(this.jcalendar.getCalendar().getTime());
            }
        } else if (evt.getPropertyName().equals("date")) {
            if (evt.getSource() == this.dateEditor) {
                this.firePropertyChange("date", evt.getOldValue(), evt.getNewValue());
            } else {
                this.setDate((Date)evt.getNewValue());
            }
        } else if (evt.getPropertyName().equals("dateEmpty")) {
            this.firePropertyChange("dateEmpty", evt.getOldValue(), evt.getNewValue());
        }
    }

    @Override
    public void updateUI() {
        super.updateUI();
        this.setEnabled(this.isEnabled());
        if (this.jcalendar != null) {
            SwingUtilities.updateComponentTreeUI(this.popup);
        }
    }

    @Override
    public void setLocale(Locale l) {
        super.setLocale(l);
        this.dateEditor.setLocale(l);
        this.jcalendar.setLocale(l);
    }

    public String getDateFormatString() {
        return this.dateEditor.getDateFormatString();
    }

    public void setDateFormatString(String dfString) {
        this.dateEditor.setDateFormatString(dfString);
        this.invalidate();
    }

    public Date getDate() {
        return this.dateEditor.getDate();
    }

    public void setDate(Date date) {
        this.dateEditor.setDate(date);
        if (this.getParent() != null) {
            this.getParent().invalidate();
        }
    }

    public Boolean getDateEmpty() {
        if (this.dateEditor instanceof JTextFieldDateEditor) {
            return "".equals(((JTextFieldDateEditor)this.dateEditor).getText().trim());
        }
        throw new RuntimeException("Not supported");
    }

    @Override
    public void setToolTipText(String text) {
        super.setToolTipText(text);
        this.dateEditor.getUiComponent().setToolTipText(text);
    }

    public Calendar getCalendar() {
        Date date = this.getDate();
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        if (calendar == null) {
            this.dateEditor.setDate(null);
        } else {
            this.dateEditor.setDate(calendar.getTime());
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (this.dateEditor != null) {
            this.dateEditor.setEnabled(enabled);
            this.calendarButton.setEnabled(enabled);
        }
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }

    public void setIcon(ImageIcon icon) {
        this.calendarButton.setIcon(icon);
    }

    @Override
    public void setFont(Font font) {
        if (this.isInitialized) {
            this.dateEditor.getUiComponent().setFont(font);
            this.jcalendar.setFont(font);
        }
        super.setFont(font);
    }

    public JCalendar getJCalendar() {
        return this.jcalendar;
    }

    public JButton getCalendarButton() {
        return this.calendarButton;
    }

    public IDateEditor getDateEditor() {
        return this.dateEditor;
    }

    public void setSelectableDateRange(Date min, Date max) {
        this.jcalendar.setSelectableDateRange(min, max);
        this.dateEditor.setSelectableDateRange(this.jcalendar.getMinSelectableDate(), this.jcalendar.getMaxSelectableDate());
    }

    public void setMaxSelectableDate(Date max) {
        this.jcalendar.setMaxSelectableDate(max);
        this.dateEditor.setMaxSelectableDate(max);
    }

    public void setMinSelectableDate(Date min) {
        this.jcalendar.setMinSelectableDate(min);
        this.dateEditor.setMinSelectableDate(min);
    }

    public Date getMaxSelectableDate() {
        return this.jcalendar.getMaxSelectableDate();
    }

    public Date getMinSelectableDate() {
        return this.jcalendar.getMinSelectableDate();
    }

    public void cleanup() {
        MenuSelectionManager.defaultManager().removeChangeListener(this.changeListener);
        this.changeListener = null;
    }

    public void addActionListener(ActionListener listener) {
        if (!(this.dateEditor instanceof JTextFieldDateEditor)) {
            throw new RuntimeException("Not Supported");
        }
        ((JTextFieldDateEditor)this.dateEditor).addActionListener(listener);
    }

    public static void main(String[] s) {
        JFrame frame = new JFrame("JDateChooser");
        JDateChooser dateChooser = new JDateChooser();
        frame.getContentPane().add(dateChooser);
        frame.pack();
        frame.setVisible(true);
    }

}

