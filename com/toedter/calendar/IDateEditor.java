/*
 * Decompiled with CFR 0_102.
 */
package com.toedter.calendar;

import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.Locale;
import javax.swing.JComponent;

public interface IDateEditor {
    public Date getDate();

    public void setDate(Date var1);

    public void setDateFormatString(String var1);

    public String getDateFormatString();

    public void setSelectableDateRange(Date var1, Date var2);

    public Date getMaxSelectableDate();

    public Date getMinSelectableDate();

    public void setMaxSelectableDate(Date var1);

    public void setMinSelectableDate(Date var1);

    public JComponent getUiComponent();

    public void setLocale(Locale var1);

    public void setEnabled(boolean var1);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void addPropertyChangeListener(String var1, PropertyChangeListener var2);

    public void removePropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(String var1, PropertyChangeListener var2);
}

