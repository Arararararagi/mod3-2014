/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.TextElementArray;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

public class SimpleCell
extends Rectangle
implements PdfPCellEvent,
Element,
TextElementArray {
    public static final boolean ROW = true;
    public static final boolean CELL = false;
    private ArrayList content = new ArrayList();
    private float width = 0.0f;
    private float widthpercentage = 0.0f;
    private float spacing = NaNf;
    private float padding_left = NaNf;
    private float padding_right = NaNf;
    private float padding_top = NaNf;
    private float padding_bottom = NaNf;
    private int colspan = 1;
    private int horizontalAlignment = -1;
    private int verticalAlignment = -1;
    private boolean cellgroup = false;
    protected boolean useAscender = false;
    protected boolean useDescender = false;
    protected boolean useBorderPadding;

    public SimpleCell(boolean row) {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.cellgroup = row;
        this.setBorder(15);
    }

    public void addElement(Element element) throws BadElementException {
        if (this.cellgroup) {
            if (element instanceof SimpleCell) {
                if (((SimpleCell)element).isCellgroup()) {
                    throw new BadElementException("You can't add one row to another row.");
                }
                this.content.add(element);
                return;
            }
            throw new BadElementException("You can only add cells to rows, no objects of type " + element.getClass().getName());
        }
        if (element.type() != 12 && element.type() != 11 && element.type() != 17 && element.type() != 10 && element.type() != 14) {
            throw new BadElementException("You can't add an element of type " + element.getClass().getName() + " to a SimpleCell.");
        }
        this.content.add(element);
    }

    public Cell createCell(SimpleCell rowAttributes) throws BadElementException {
        Cell cell = new Cell();
        cell.cloneNonPositionParameters(rowAttributes);
        cell.softCloneNonPositionParameters(this);
        cell.setColspan(this.colspan);
        cell.setHorizontalAlignment(this.horizontalAlignment);
        cell.setVerticalAlignment(this.verticalAlignment);
        cell.setUseAscender(this.useAscender);
        cell.setUseBorderPadding(this.useBorderPadding);
        cell.setUseDescender(this.useDescender);
        Iterator i = this.content.iterator();
        while (i.hasNext()) {
            Element element = (Element)i.next();
            cell.addElement(element);
        }
        return cell;
    }

    public PdfPCell createPdfPCell(SimpleCell rowAttributes) {
        float sp;
        float p;
        PdfPCell cell = new PdfPCell();
        cell.setBorder(0);
        SimpleCell tmp = new SimpleCell(false);
        tmp.setSpacing(this.spacing);
        tmp.cloneNonPositionParameters(rowAttributes);
        tmp.softCloneNonPositionParameters(this);
        cell.setCellEvent(tmp);
        cell.setHorizontalAlignment(rowAttributes.horizontalAlignment);
        cell.setVerticalAlignment(rowAttributes.verticalAlignment);
        cell.setUseAscender(rowAttributes.useAscender);
        cell.setUseBorderPadding(rowAttributes.useBorderPadding);
        cell.setUseDescender(rowAttributes.useDescender);
        cell.setColspan(this.colspan);
        if (this.horizontalAlignment != -1) {
            cell.setHorizontalAlignment(this.horizontalAlignment);
        }
        if (this.verticalAlignment != -1) {
            cell.setVerticalAlignment(this.verticalAlignment);
        }
        if (this.useAscender) {
            cell.setUseAscender(this.useAscender);
        }
        if (this.useBorderPadding) {
            cell.setUseBorderPadding(this.useBorderPadding);
        }
        if (this.useDescender) {
            cell.setUseDescender(this.useDescender);
        }
        if (Float.isNaN(sp = this.spacing)) {
            sp = 0.0f;
        }
        if (Float.isNaN(p = this.padding_left)) {
            p = 0.0f;
        }
        cell.setPaddingLeft(p + sp / 2.0f);
        p = this.padding_right;
        if (Float.isNaN(p)) {
            p = 0.0f;
        }
        cell.setPaddingRight(p + sp / 2.0f);
        p = this.padding_top;
        if (Float.isNaN(p)) {
            p = 0.0f;
        }
        cell.setPaddingTop(p + sp / 2.0f);
        p = this.padding_bottom;
        if (Float.isNaN(p)) {
            p = 0.0f;
        }
        cell.setPaddingBottom(p + sp / 2.0f);
        Iterator i = this.content.iterator();
        while (i.hasNext()) {
            Element element = (Element)i.next();
            cell.addElement(element);
        }
        return cell;
    }

    public static SimpleCell getDimensionlessInstance(Rectangle rectangle, float spacing) {
        SimpleCell event = new SimpleCell(false);
        event.cloneNonPositionParameters(rectangle);
        event.setSpacing(spacing);
        return event;
    }

    public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
        float sp = this.spacing;
        if (Float.isNaN(sp)) {
            sp = 0.0f;
        }
        Rectangle rect = new Rectangle(position.left(sp), position.bottom(sp), position.right(sp), position.top(sp/=2.0f));
        rect.cloneNonPositionParameters(this);
        canvases[1].rectangle(rect);
        rect.setBackgroundColor(null);
        canvases[2].rectangle(rect);
    }

    public void setPadding(float padding) {
        if (Float.isNaN(this.padding_right)) {
            this.setPadding_right(padding);
        }
        if (Float.isNaN(this.padding_left)) {
            this.setPadding_left(padding);
        }
        if (Float.isNaN(this.padding_top)) {
            this.setPadding_top(padding);
        }
        if (Float.isNaN(this.padding_bottom)) {
            this.setPadding_bottom(padding);
        }
    }

    public int getColspan() {
        return this.colspan;
    }

    public void setColspan(int colspan) {
        if (colspan > 0) {
            this.colspan = colspan;
        }
    }

    public float getPadding_bottom() {
        return this.padding_bottom;
    }

    public void setPadding_bottom(float padding_bottom) {
        this.padding_bottom = padding_bottom;
    }

    public float getPadding_left() {
        return this.padding_left;
    }

    public void setPadding_left(float padding_left) {
        this.padding_left = padding_left;
    }

    public float getPadding_right() {
        return this.padding_right;
    }

    public void setPadding_right(float padding_right) {
        this.padding_right = padding_right;
    }

    public float getPadding_top() {
        return this.padding_top;
    }

    public void setPadding_top(float padding_top) {
        this.padding_top = padding_top;
    }

    public float getSpacing() {
        return this.spacing;
    }

    public void setSpacing(float spacing) {
        this.spacing = spacing;
    }

    public boolean isCellgroup() {
        return this.cellgroup;
    }

    public void setCellgroup(boolean cellgroup) {
        this.cellgroup = cellgroup;
    }

    public int getHorizontalAlignment() {
        return this.horizontalAlignment;
    }

    public void setHorizontalAlignment(int horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }

    public int getVerticalAlignment() {
        return this.verticalAlignment;
    }

    public void setVerticalAlignment(int verticalAlignment) {
        this.verticalAlignment = verticalAlignment;
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getWidthpercentage() {
        return this.widthpercentage;
    }

    public void setWidthpercentage(float widthpercentage) {
        this.widthpercentage = widthpercentage;
    }

    public boolean isUseAscender() {
        return this.useAscender;
    }

    public void setUseAscender(boolean useAscender) {
        this.useAscender = useAscender;
    }

    public boolean isUseBorderPadding() {
        return this.useBorderPadding;
    }

    public void setUseBorderPadding(boolean useBorderPadding) {
        this.useBorderPadding = useBorderPadding;
    }

    public boolean isUseDescender() {
        return this.useDescender;
    }

    public void setUseDescender(boolean useDescender) {
        this.useDescender = useDescender;
    }

    ArrayList getContent() {
        return this.content;
    }

    public boolean add(Object o) {
        try {
            this.addElement((Element)o);
            return true;
        }
        catch (ClassCastException e) {
            return false;
        }
        catch (BadElementException e) {
            throw new ExceptionConverter(e);
        }
    }
}

