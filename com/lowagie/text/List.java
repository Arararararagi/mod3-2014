/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.ListItem;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.TextElementArray;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class List
implements TextElementArray,
MarkupAttributes {
    protected ArrayList list = new ArrayList();
    protected boolean numbered;
    protected boolean lettered;
    protected int first = 1;
    protected char firstCh = 65;
    protected char lastCh = 90;
    protected Chunk symbol = new Chunk("-");
    protected float indentationLeft = 0.0f;
    protected float indentationRight = 0.0f;
    protected float symbolIndent;
    protected Properties markupAttributes;

    public List(boolean numbered, float symbolIndent) {
        this.numbered = numbered;
        this.lettered = false;
        this.symbolIndent = symbolIndent;
    }

    public List(boolean numbered, boolean lettered, float symbolIndent) {
        this.numbered = numbered;
        this.lettered = lettered;
        this.symbolIndent = symbolIndent;
    }

    public List(Properties attributes) {
        String value = (String)attributes.remove("listsymbol");
        if (value == null) {
            value = "-";
        }
        this.symbol = new Chunk(value, FontFactory.getFont(attributes));
        this.numbered = false;
        value = (String)attributes.remove("numbered");
        if (value != null) {
            this.numbered = new Boolean(value);
            if (this.lettered && this.numbered) {
                this.lettered = false;
            }
        }
        if ((value = (String)attributes.remove("lettered")) != null) {
            this.lettered = new Boolean(value);
            if (this.numbered && this.lettered) {
                this.numbered = false;
            }
        }
        this.symbolIndent = 0.0f;
        value = (String)attributes.remove("symbolindent");
        if (value != null) {
            this.symbolIndent = Integer.parseInt(value);
        }
        if ((value = (String)attributes.remove("first")) != null) {
            char khar = value.charAt(0);
            if (Character.isLetter(khar)) {
                this.setFirst(khar);
            } else {
                this.setFirst(Integer.parseInt(value));
            }
        }
        if ((value = (String)attributes.remove("indentationleft")) != null) {
            this.setIndentationLeft(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("indentationright")) != null) {
            this.setIndentationRight(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public boolean process(ElementListener listener) {
        try {
            Iterator i = this.list.iterator();
            while (i.hasNext()) {
                listener.add((Element)i.next());
            }
            return true;
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 14;
    }

    public ArrayList getChunks() {
        ArrayList tmp = new ArrayList();
        Iterator i = this.list.iterator();
        while (i.hasNext()) {
            tmp.addAll(((Element)i.next()).getChunks());
        }
        return tmp;
    }

    public boolean add(Object o) {
        if (o instanceof ListItem) {
            ListItem item = (ListItem)o;
            if (this.numbered || this.lettered) {
                Chunk chunk = this.numbered ? new Chunk(String.valueOf(this.first + this.list.size()), this.symbol.font()) : new Chunk(this.nextLetter(), this.symbol.font());
                chunk.append(".");
                item.setListSymbol(chunk);
            } else {
                item.setListSymbol(this.symbol);
            }
            item.setIndentationLeft(this.symbolIndent);
            item.setIndentationRight(0.0f);
            this.list.add(item);
        } else {
            if (o instanceof List) {
                List nested = (List)o;
                nested.setIndentationLeft(nested.indentationLeft() + this.symbolIndent);
                --this.first;
                return this.list.add(nested);
            }
            if (o instanceof String) {
                return this.add(new ListItem((String)o));
            }
        }
        return false;
    }

    public void setIndentationLeft(float indentation) {
        this.indentationLeft = indentation;
    }

    public void setIndentationRight(float indentation) {
        this.indentationRight = indentation;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public void setFirst(char first) {
        this.firstCh = first;
        this.lastCh = Character.isLowerCase(this.firstCh) ? 122 : 90;
    }

    public void setListSymbol(Chunk symbol) {
        this.symbol = symbol;
    }

    public void setListSymbol(String symbol) {
        this.symbol = new Chunk(symbol);
    }

    public ArrayList getItems() {
        return this.list;
    }

    public int size() {
        return this.list.size();
    }

    public float leading() {
        if (this.list.size() < 1) {
            return -1.0f;
        }
        ListItem item = (ListItem)this.list.get(0);
        return item.leading();
    }

    public boolean isNumbered() {
        return this.numbered;
    }

    public float symbolIndent() {
        return this.symbolIndent;
    }

    public Chunk symbol() {
        return this.symbol;
    }

    public int first() {
        return this.first;
    }

    public float indentationLeft() {
        return this.indentationLeft;
    }

    public float indentationRight() {
        return this.indentationRight;
    }

    public static boolean isSymbol(String tag) {
        return "listsymbol".equals(tag);
    }

    public static boolean isTag(String tag) {
        return "list".equals(tag);
    }

    private String nextLetter() {
        int num_in_list = this.listItemsInList();
        int max_ival = this.lastCh + '\u0000';
        for (int ival = this.firstCh + num_in_list; ival > max_ival; ival-=26) {
        }
        char[] new_char = new char[]{(char)ival};
        String ret = new String(new_char);
        return ret;
    }

    private int listItemsInList() {
        int result = 0;
        Iterator i = this.list.iterator();
        while (i.hasNext()) {
            if (i.next() instanceof List) continue;
            ++result;
        }
        return result;
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }
}

