/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.MarkupAttributes;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

public class Row
implements Element,
MarkupAttributes {
    public static final int NULL = 0;
    public static final int CELL = 1;
    public static final int TABLE = 2;
    protected int columns;
    protected int currentColumn;
    protected boolean[] reserved;
    protected Object[] cells;
    protected int horizontalAlignment;
    protected int verticalAlignment;
    protected Properties markupAttributes;
    static /* synthetic */ Class class$0;
    static /* synthetic */ Class class$1;

    protected Row(int columns) {
        this.columns = columns;
        this.reserved = new boolean[columns];
        this.cells = new Object[columns];
        this.currentColumn = 0;
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 21;
    }

    public ArrayList getChunks() {
        return new ArrayList();
    }

    void deleteColumn(int column) {
        int i;
        if (column >= this.columns || column < 0) {
            throw new IndexOutOfBoundsException("getCell at illegal index : " + column);
        }
        --this.columns;
        boolean[] newReserved = new boolean[this.columns];
        Cell[] newCells = new Cell[this.columns];
        for (i = 0; i < column; ++i) {
            newReserved[i] = this.reserved[i];
            newCells[i] = this.cells[i];
            if (newCells[i] == null || i + newCells[i].colspan() <= column) continue;
            newCells[i].setColspan(((Cell)this.cells[i]).colspan() - 1);
        }
        for (i = column; i < this.columns; ++i) {
            newReserved[i] = this.reserved[i + 1];
            newCells[i] = this.cells[i + 1];
        }
        if (this.cells[column] != null && ((Cell)this.cells[column]).colspan() > 1) {
            newCells[column] = this.cells[column];
            newCells[column].setColspan(newCells[column].colspan() - 1);
        }
        this.reserved = newReserved;
        this.cells = newCells;
    }

    int addElement(Object element) {
        return this.addElement(element, this.currentColumn);
    }

    int addElement(Object element, int column) {
        int lColspan;
        Class class_;
        if (element == null) {
            throw new NullPointerException("addCell - null argument");
        }
        if (column < 0 || column > this.columns) {
            throw new IndexOutOfBoundsException("addCell - illegal column argument");
        }
        if (this.getObjectID(element) != 1 && this.getObjectID(element) != 2) {
            throw new IllegalArgumentException("addCell - only Cells or Tables allowed");
        }
        class_ = class$0;
        if (class_ == null) {
            try {
                class_ = Row.class$0 = Class.forName("com.lowagie.text.Cell");
            }
            catch (ClassNotFoundException v1) {
                throw new NoClassDefFoundError(v1.getMessage());
            }
        }
        int n = lColspan = class_.isInstance(element) ? ((Cell)element).colspan() : 1;
        if (!this.reserve(column, lColspan)) {
            return -1;
        }
        this.cells[column] = element;
        this.currentColumn+=lColspan - 1;
        return column;
    }

    void setElement(Object aElement, int column) {
        if (this.reserved[column]) {
            throw new IllegalArgumentException("setElement - position already taken");
        }
        this.cells[column] = aElement;
        if (aElement != null) {
            this.reserved[column] = true;
        }
    }

    boolean reserve(int column) {
        return this.reserve(column, 1);
    }

    boolean reserve(int column, int size) {
        if (column < 0 || column + size > this.columns) {
            throw new IndexOutOfBoundsException("reserve - incorrect column/size");
        }
        for (int i = column; i < column + size; ++i) {
            if (this.reserved[i]) {
                for (int j = i; j >= column; --j) {
                    this.reserved[i] = false;
                }
                return false;
            }
            this.reserved[i] = true;
        }
        return true;
    }

    public void setHorizontalAlignment(int value) {
        this.horizontalAlignment = value;
    }

    public void setVerticalAlignment(int value) {
        this.verticalAlignment = value;
    }

    boolean isReserved(int column) {
        return this.reserved[column];
    }

    int getElementID(int column) {
        Class class_;
        Class class_2;
        if (this.cells[column] == null) {
            return 0;
        }
        class_2 = class$0;
        if (class_2 == null) {
            try {
                class_2 = Row.class$0 = Class.forName("com.lowagie.text.Cell");
            }
            catch (ClassNotFoundException v1) {
                throw new NoClassDefFoundError(v1.getMessage());
            }
        }
        if (class_2.isInstance(this.cells[column])) {
            return 1;
        }
        class_ = class$1;
        if (class_ == null) {
            try {
                class_ = Row.class$1 = Class.forName("com.lowagie.text.Table");
            }
            catch (ClassNotFoundException v3) {
                throw new NoClassDefFoundError(v3.getMessage());
            }
        }
        if (class_.isInstance(this.cells[column])) {
            return 2;
        }
        return -1;
    }

    int getObjectID(Object element) {
        Class class_;
        Class class_2;
        if (element == null) {
            return 0;
        }
        class_2 = class$0;
        if (class_2 == null) {
            try {
                class_2 = Row.class$0 = Class.forName("com.lowagie.text.Cell");
            }
            catch (ClassNotFoundException v1) {
                throw new NoClassDefFoundError(v1.getMessage());
            }
        }
        if (class_2.isInstance(element)) {
            return 1;
        }
        class_ = class$1;
        if (class_ == null) {
            try {
                class_ = Row.class$1 = Class.forName("com.lowagie.text.Table");
            }
            catch (ClassNotFoundException v3) {
                throw new NoClassDefFoundError(v3.getMessage());
            }
        }
        if (class_.isInstance(element)) {
            return 2;
        }
        return -1;
    }

    public Object getCell(int column) {
        if (column < 0 || column > this.columns) {
            throw new IndexOutOfBoundsException("getCell at illegal index :" + column + " max is " + this.columns);
        }
        return this.cells[column];
    }

    public boolean isEmpty() {
        for (int i = 0; i < this.columns; ++i) {
            if (this.cells[i] == null) continue;
            return false;
        }
        return true;
    }

    int validPosition() {
        return this.currentColumn;
    }

    public int columns() {
        return this.columns;
    }

    public int horizontalAlignment() {
        return this.horizontalAlignment;
    }

    public int verticalAlignment() {
        return this.verticalAlignment;
    }

    public static boolean isTag(String tag) {
        return "row".equals(tag);
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }
}

