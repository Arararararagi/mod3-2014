/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfTemplate;
import java.net.URL;

public class ImgTemplate
extends Image
implements Element {
    ImgTemplate(Image image) {
        super(image);
    }

    public ImgTemplate(PdfTemplate template) throws BadElementException {
        super((URL)null);
        if (template == null) {
            throw new BadElementException("The template can not be null.");
        }
        if (template.getType() == 3) {
            throw new BadElementException("A pattern can not be used as a template to create an image.");
        }
        this.type = 35;
        this.scaledHeight = template.getHeight();
        this.setTop(this.scaledHeight);
        this.scaledWidth = template.getWidth();
        this.setRight(this.scaledWidth);
        this.setTemplateData(template);
        this.plainWidth = this.width();
        this.plainHeight = this.height();
    }
}

