/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.MarkupAttributes;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

public class Meta
implements Element,
MarkupAttributes {
    private int type;
    private StringBuffer content;
    protected Properties markupAttributes;

    Meta(int type, String content) {
        this.type = type;
        this.content = new StringBuffer(content);
    }

    public Meta(String tag, String content) {
        this.type = Meta.getType(tag);
        this.content = new StringBuffer(content);
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return this.type;
    }

    public ArrayList getChunks() {
        return new ArrayList();
    }

    public StringBuffer append(String string) {
        return this.content.append(string);
    }

    public String content() {
        return this.content.toString();
    }

    public String name() {
        switch (this.type) {
            case 2: {
                return "subject";
            }
            case 3: {
                return "keywords";
            }
            case 4: {
                return "author";
            }
            case 1: {
                return "title";
            }
            case 5: {
                return "producer";
            }
            case 6: {
                return "creationdate";
            }
        }
        return "unknown";
    }

    public static int getType(String tag) {
        if ("subject".equals(tag)) {
            return 2;
        }
        if ("keywords".equals(tag)) {
            return 3;
        }
        if ("author".equals(tag)) {
            return 4;
        }
        if ("title".equals(tag)) {
            return 1;
        }
        if ("producer".equals(tag)) {
            return 5;
        }
        if ("creationdate".equals(tag)) {
            return 6;
        }
        return 0;
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }
}

