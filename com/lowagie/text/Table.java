/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Row;
import com.lowagie.text.SimpleCell;
import com.lowagie.text.SimpleTable;
import com.lowagie.text.markup.MarkupParser;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPTableEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

public class Table
extends Rectangle
implements Element,
MarkupAttributes {
    private int columns;
    private Point curPosition = new Point(0, 0);
    private ArrayList rows = new ArrayList();
    private Cell defaultLayout = new Cell(true);
    private int lastHeaderRow = -1;
    private int alignment = 1;
    private float cellpadding;
    private float cellspacing;
    private float widthPercentage = 80.0f;
    private String absWidth = "";
    private float[] widths;
    boolean mDebug = false;
    boolean mTableInserted = false;
    boolean mAutoFillEmptyCells = false;
    boolean tableFitsPage = false;
    boolean cellsFitPage = false;
    float offset = NaNf;
    protected Hashtable alternatingRowAttributes = null;
    protected boolean convert2pdfptable = false;
    private static DecimalFormat widthFormat = new DecimalFormat("0.00");
    static /* synthetic */ Class class$0;
    static /* synthetic */ Class class$1;

    public Table(int columns) throws BadElementException {
        this(columns, 1);
    }

    public Table(int columns, int rows) throws BadElementException {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.setBorder(15);
        this.setBorderWidth(1.0f);
        this.defaultLayout.setBorder(15);
        if (columns <= 0) {
            throw new BadElementException("A table should have at least 1 column.");
        }
        this.columns = columns;
        for (int i = 0; i < rows; ++i) {
            this.rows.add(new Row(columns));
        }
        this.curPosition = new Point(0, 0);
        this.widths = new float[columns];
        float width = 100.0f / (float)columns;
        for (int i2 = 0; i2 < columns; ++i2) {
            this.widths[i2] = width;
        }
    }

    public Table(Properties attributes) {
        int green;
        int blue;
        int red;
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.setBorder(15);
        this.setBorderWidth(1.0f);
        this.defaultLayout.setBorder(15);
        String value = (String)attributes.remove("columns");
        if (value == null) {
            this.columns = 1;
        } else {
            this.columns = Integer.parseInt(value);
            if (this.columns <= 0) {
                this.columns = 1;
            }
        }
        this.rows.add(new Row(this.columns));
        this.curPosition.setLocation(0, this.curPosition.y);
        value = (String)attributes.remove("lastHeaderRow");
        if (value != null) {
            this.setLastHeaderRow(Integer.parseInt(value));
        }
        if ((value = (String)attributes.remove("align")) != null) {
            this.setAlignment(value);
        }
        if ((value = (String)attributes.remove("cellspacing")) != null) {
            this.setSpacing(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("cellpadding")) != null) {
            this.setPadding(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("offset")) != null) {
            this.setOffset(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("width")) != null) {
            if (value.endsWith("%")) {
                this.setWidth(Float.valueOf(String.valueOf(value.substring(0, value.length() - 1)) + "f").floatValue());
            } else {
                this.setAbsWidth(value);
            }
        }
        this.widths = new float[this.columns];
        for (int i = 0; i < this.columns; ++i) {
            this.widths[i] = 0.0f;
        }
        value = (String)attributes.remove("widths");
        if (value != null) {
            StringTokenizer widthTokens = new StringTokenizer(value, ";");
            int i2 = 0;
            while (widthTokens.hasMoreTokens()) {
                value = widthTokens.nextToken();
                this.widths[i2] = Float.valueOf(String.valueOf(value) + "f").floatValue();
                ++i2;
            }
            this.columns = i2;
        }
        if ((value = (String)attributes.remove("tablefitspage")) != null) {
            this.tableFitsPage = new Boolean(value);
        }
        if ((value = (String)attributes.remove("cellsfitpage")) != null) {
            this.cellsFitPage = new Boolean(value);
        }
        if ((value = (String)attributes.remove("borderwidth")) != null) {
            this.setBorderWidth(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        int border = 0;
        value = (String)attributes.remove("left");
        if (value != null && new Boolean(value).booleanValue()) {
            border|=4;
        }
        if ((value = (String)attributes.remove("right")) != null && new Boolean(value).booleanValue()) {
            border|=8;
        }
        if ((value = (String)attributes.remove("top")) != null && new Boolean(value).booleanValue()) {
            border|=1;
        }
        if ((value = (String)attributes.remove("bottom")) != null && new Boolean(value).booleanValue()) {
            border|=2;
        }
        this.setBorder(border);
        String r = (String)attributes.remove("red");
        String g = (String)attributes.remove("green");
        String b = (String)attributes.remove("blue");
        if (r != null || g != null || b != null) {
            red = 0;
            green = 0;
            blue = 0;
            if (r != null) {
                red = Integer.parseInt(r);
            }
            if (g != null) {
                green = Integer.parseInt(g);
            }
            if (b != null) {
                blue = Integer.parseInt(b);
            }
            this.setBorderColor(new Color(red, green, blue));
        } else {
            value = attributes.getProperty("bordercolor");
            if (value != null) {
                this.setBorderColor(MarkupParser.decodeColor(value));
            }
        }
        r = (String)attributes.remove("bgred");
        g = (String)attributes.remove("bggreen");
        b = (String)attributes.remove("bgblue");
        if (r != null || g != null || b != null) {
            red = 0;
            green = 0;
            blue = 0;
            if (r != null) {
                red = Integer.parseInt(r);
            }
            if (g != null) {
                green = Integer.parseInt(g);
            }
            if (b != null) {
                blue = Integer.parseInt(b);
            }
            this.setBackgroundColor(new Color(red, green, blue));
        } else {
            value = (String)attributes.remove("backgroundcolor");
            if (value != null) {
                this.setBackgroundColor(MarkupParser.decodeColor(value));
            }
        }
        value = (String)attributes.remove("grayfill");
        if (value != null) {
            this.setGrayFill(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public void setDebug(boolean aDebug) {
        this.mDebug = aDebug;
    }

    public void setDefaultLayout(Cell value) {
        this.defaultLayout = value;
    }

    public void setAutoFillEmptyCells(boolean aDoAutoFill) {
        this.mAutoFillEmptyCells = aDoAutoFill;
    }

    public void setTableFitsPage(boolean fitPage) {
        this.tableFitsPage = fitPage;
        if (fitPage) {
            this.setCellsFitPage(true);
        }
    }

    public void setCellsFitPage(boolean fitPage) {
        this.cellsFitPage = fitPage;
    }

    public boolean hasToFitPageTable() {
        return this.tableFitsPage;
    }

    public boolean hasToFitPageCells() {
        return this.cellsFitPage;
    }

    public void setOffset(float offset) {
        this.offset = offset;
    }

    public float getOffset() {
        return this.offset;
    }

    public int type() {
        return 22;
    }

    public ArrayList getChunks() {
        return new ArrayList();
    }

    public void addCell(Cell aCell, int row, int column) throws BadElementException {
        this.addCell(aCell, new Point(row, column));
    }

    public void addCell(Cell aCell, Point aLocation) throws BadElementException {
        if (aCell == null) {
            throw new NullPointerException("addCell - cell has null-value");
        }
        if (aLocation == null) {
            throw new NullPointerException("addCell - point has null-value");
        }
        if (aCell.isTable()) {
            this.insertTable((Table)aCell.getElements().next(), aLocation);
        }
        if (this.mDebug) {
            if (aLocation.x < 0) {
                throw new BadElementException("row coordinate of location must be >= 0");
            }
            if (aLocation.y <= 0 && aLocation.y > this.columns) {
                throw new BadElementException("column coordinate of location must be >= 0 and < nr of columns");
            }
            if (!this.isValidLocation(aCell, aLocation)) {
                throw new BadElementException("Adding a cell at the location (" + aLocation.x + "," + aLocation.y + ") with a colspan of " + aCell.colspan() + " and a rowspan of " + aCell.rowspan() + " is illegal (beyond boundaries/overlapping).");
            }
        }
        if (aCell.border() == -1) {
            aCell.setBorder(this.defaultLayout.border());
        }
        aCell.fill();
        this.placeCell(this.rows, aCell, aLocation);
        this.setCurrentLocationToNextValidPosition(aLocation);
    }

    public void addCell(Cell cell) {
        try {
            this.addCell(cell, this.curPosition);
        }
        catch (BadElementException var2_2) {
            // empty catch block
        }
    }

    public void addCell(Phrase content) throws BadElementException {
        this.addCell(content, this.curPosition);
    }

    public void addCell(Phrase content, Point location) throws BadElementException {
        Cell cell = new Cell(content);
        cell.setBorder(this.defaultLayout.border());
        cell.setBorderWidth(this.defaultLayout.borderWidth());
        cell.setBorderColor(this.defaultLayout.borderColor());
        cell.setBackgroundColor(this.defaultLayout.backgroundColor());
        cell.setGrayFill(this.defaultLayout.grayFill());
        cell.setHorizontalAlignment(this.defaultLayout.horizontalAlignment());
        cell.setVerticalAlignment(this.defaultLayout.verticalAlignment());
        cell.setColspan(this.defaultLayout.colspan());
        cell.setRowspan(this.defaultLayout.rowspan());
        this.addCell(cell, location);
    }

    public void addCell(String content) throws BadElementException {
        this.addCell(new Phrase(content), this.curPosition);
    }

    public void addCell(String content, Point location) throws BadElementException {
        this.addCell(new Phrase(content), location);
    }

    public void insertTable(Table aTable) {
        if (aTable == null) {
            throw new NullPointerException("insertTable - table has null-value");
        }
        this.insertTable(aTable, this.curPosition);
    }

    public void insertTable(Table aTable, int row, int column) {
        if (aTable == null) {
            throw new NullPointerException("insertTable - table has null-value");
        }
        this.insertTable(aTable, new Point(row, column));
    }

    public void insertTable(Table aTable, Point aLocation) {
        if (aTable == null) {
            throw new NullPointerException("insertTable - table has null-value");
        }
        if (aLocation == null) {
            throw new NullPointerException("insertTable - point has null-value");
        }
        this.mTableInserted = true;
        aTable.complete();
        if (this.mDebug && aLocation.y > this.columns) {
            System.err.println("insertTable -- wrong columnposition(" + aLocation.y + ") of location; max =" + this.columns);
        }
        int rowCount = aLocation.x + 1 - this.rows.size();
        if (rowCount > 0) {
            for (int i = 0; i < rowCount; ++i) {
                this.rows.add(new Row(this.columns));
            }
        }
        ((Row)this.rows.get(aLocation.x)).setElement(aTable, aLocation.y);
        this.setCurrentLocationToNextValidPosition(aLocation);
    }

    public void complete() {
        if (this.mTableInserted) {
            this.mergeInsertedTables();
            this.mTableInserted = false;
        }
        if (this.mAutoFillEmptyCells) {
            this.fillEmptyMatrixCells();
        }
        if (this.alternatingRowAttributes != null) {
            Properties even = new Properties();
            Properties odd = new Properties();
            Iterator iterator = this.alternatingRowAttributes.keySet().iterator();
            while (iterator.hasNext()) {
                String name = String.valueOf(iterator.next());
                String[] value = (String[])this.alternatingRowAttributes.get(name);
                even.setProperty(name, value[0]);
                odd.setProperty(name, value[1]);
            }
            for (int i = this.lastHeaderRow + 1; i < this.rows.size(); ++i) {
                Row row = (Row)this.rows.get(i);
                row.setMarkupAttributes(i % 2 == 0 ? even : odd);
            }
        }
    }

    public void setDefaultCellBorder(int value) {
        this.defaultLayout.setBorder(value);
    }

    public void setDefaultCellBorderWidth(float value) {
        this.defaultLayout.setBorderWidth(value);
    }

    public void setDefaultCellBorderColor(Color color) {
        this.defaultLayout.setBorderColor(color);
    }

    public void setDefaultCellBackgroundColor(Color color) {
        this.defaultLayout.setBackgroundColor(color);
    }

    public void setDefaultCellGrayFill(float value) {
        if (value >= 0.0f && value <= 1.0f) {
            this.defaultLayout.setGrayFill(value);
        }
    }

    public void setDefaultHorizontalAlignment(int value) {
        this.defaultLayout.setHorizontalAlignment(value);
    }

    public void setDefaultVerticalAlignment(int value) {
        this.defaultLayout.setVerticalAlignment(value);
    }

    public void setDefaultRowspan(int value) {
        this.defaultLayout.setRowspan(value);
    }

    public void setDefaultColspan(int value) {
        this.defaultLayout.setColspan(value);
    }

    private void assumeTableDefaults(Cell aCell) {
        if (aCell.border() == -1) {
            aCell.setBorder(this.defaultLayout.border());
        }
        if (aCell.borderWidth() == -1.0f) {
            aCell.setBorderWidth(this.defaultLayout.borderWidth());
        }
        if (aCell.borderColor() == null) {
            aCell.setBorderColor(this.defaultLayout.borderColor());
        }
        if (aCell.backgroundColor() == null) {
            aCell.setBackgroundColor(this.defaultLayout.backgroundColor());
        }
        if (aCell.grayFill() == -1.0f) {
            aCell.setGrayFill(this.defaultLayout.grayFill());
        }
        if (aCell.horizontalAlignment() == -1) {
            aCell.setHorizontalAlignment(this.defaultLayout.horizontalAlignment());
        }
        if (aCell.verticalAlignment() == -1) {
            aCell.setVerticalAlignment(this.defaultLayout.verticalAlignment());
        }
    }

    public void deleteColumn(int column) throws BadElementException {
        int i;
        float[] newWidths = new float[--this.columns];
        for (i = 0; i < column; ++i) {
            newWidths[i] = this.widths[i];
        }
        for (i = column; i < this.columns; ++i) {
            newWidths[i] = this.widths[i + 1];
        }
        this.setWidths(newWidths);
        for (i = 0; i < this.columns; ++i) {
            newWidths[i] = this.widths[i];
        }
        this.widths = newWidths;
        int size = this.rows.size();
        for (int i2 = 0; i2 < size; ++i2) {
            Row row = (Row)this.rows.get(i2);
            row.deleteColumn(column);
            this.rows.set(i2, row);
        }
        if (column == this.columns) {
            this.curPosition.setLocation(this.curPosition.x + 1, 0);
        }
    }

    public boolean deleteRow(int row) {
        if (row < 0 || row >= this.rows.size()) {
            return false;
        }
        this.rows.remove(row);
        this.curPosition.setLocation(this.curPosition.x - 1, this.curPosition.y);
        return true;
    }

    public void deleteAllRows() {
        this.rows.clear();
        this.rows.add(new Row(this.columns));
        this.curPosition.setLocation(0, 0);
        this.lastHeaderRow = -1;
    }

    public boolean deleteLastRow() {
        return this.deleteRow(this.rows.size() - 1);
    }

    public int endHeaders() {
        this.lastHeaderRow = this.curPosition.x - 1;
        return this.lastHeaderRow;
    }

    public void setLastHeaderRow(int value) {
        this.lastHeaderRow = value;
    }

    public void setAlignment(int value) {
        this.alignment = value;
    }

    public void setAlignment(String alignment) {
        if ("Left".equalsIgnoreCase(alignment)) {
            this.alignment = 0;
            return;
        }
        if ("right".equalsIgnoreCase(alignment)) {
            this.alignment = 2;
            return;
        }
        this.alignment = 1;
    }

    public void setSpaceInsideCell(float value) {
        this.cellpadding = value;
    }

    public void setSpaceBetweenCells(float value) {
        this.cellspacing = value;
    }

    public void setPadding(float value) {
        this.cellpadding = value;
    }

    public void setSpacing(float value) {
        this.cellspacing = value;
    }

    public void setCellpadding(float value) {
        this.cellspacing = value;
    }

    public void setCellspacing(float value) {
        this.cellpadding = value;
    }

    public void setWidth(float width) {
        this.widthPercentage = width;
    }

    public void setAbsWidth(String width) {
        this.absWidth = width;
    }

    public void setWidths(float[] widths) throws BadElementException {
        if (widths.length != this.columns) {
            throw new BadElementException("Wrong number of columns.");
        }
        float hundredPercent = 0.0f;
        for (int i = 0; i < this.columns; ++i) {
            hundredPercent+=widths[i];
        }
        this.widths[this.columns - 1] = 100.0f;
        for (int i2 = 0; i2 < this.columns - 1; ++i2) {
            float width;
            this.widths[i2] = width = 100.0f * widths[i2] / hundredPercent;
            float[] arrf = this.widths;
            int n = this.columns - 1;
            arrf[n] = arrf[n] - width;
        }
    }

    public void setWidths(int[] widths) throws DocumentException {
        float[] tb = new float[widths.length];
        for (int k = 0; k < widths.length; ++k) {
            tb[k] = widths[k];
        }
        this.setWidths(tb);
    }

    public int columns() {
        return this.columns;
    }

    public int size() {
        return this.rows.size();
    }

    public float[] getProportionalWidths() {
        return this.widths;
    }

    public Iterator iterator() {
        return this.rows.iterator();
    }

    public int alignment() {
        return this.alignment;
    }

    public float cellpadding() {
        return this.cellpadding;
    }

    public float cellspacing() {
        return this.cellspacing;
    }

    public float widthPercentage() {
        return this.widthPercentage;
    }

    public String absWidth() {
        return this.absWidth;
    }

    public int firstDataRow() {
        return this.lastHeaderRow + 1;
    }

    public int lastHeaderRow() {
        return this.lastHeaderRow;
    }

    public Dimension getDimension() {
        return new Dimension(this.columns, this.rows.size());
    }

    public Object getElement(int row, int column) {
        return ((Row)this.rows.get(row)).getCell(column);
    }

    private void mergeInsertedTables() {
        int i = 0;
        int j = 0;
        float[] lNewWidths = null;
        int[] lDummyWidths = new int[this.columns];
        float[][] lDummyColumnWidths = new float[this.columns][];
        int[] lDummyHeights = new int[this.rows.size()];
        ArrayList<Row> newRows = null;
        boolean isTable = false;
        int lTotalRows = 0;
        int lTotalColumns = 0;
        int lNewMaxRows = 0;
        int lNewMaxColumns = 0;
        Table lDummyTable = null;
        for (j = 0; j < this.columns; ++j) {
            lNewMaxColumns = 1;
            float[] tmpWidths = null;
            for (i = 0; i < this.rows.size(); ++i) {
                Class class_ = class$0;
                if (class_ == null) {
                    try {
                        class_ = Class.forName("com.lowagie.text.Table");
                    }
                    catch (ClassNotFoundException v1) {
                        throw new NoClassDefFoundError(v1.getMessage());
                    }
                }
                if (!class_.isInstance(((Row)this.rows.get(i)).getCell(j))) continue;
                isTable = true;
                lDummyTable = (Table)((Row)this.rows.get(i)).getCell(j);
                if (tmpWidths == null) {
                    tmpWidths = lDummyTable.widths;
                    lNewMaxColumns = tmpWidths.length;
                    continue;
                }
                int cols = lDummyTable.getDimension().width;
                float[] tmpWidthsN = new float[cols * tmpWidths.length];
                float tpW = 0.0f;
                float btW = 0.0f;
                float totW = 0.0f;
                int tpI = 0;
                int btI = 0;
                int totI = 0;
                tpW+=tmpWidths[0];
                btW+=lDummyTable.widths[0];
                while (tpI < tmpWidths.length && btI < cols) {
                    if (btW > tpW) {
                        tmpWidthsN[totI] = tpW - totW;
                        if (++tpI < tmpWidths.length) {
                            tpW+=tmpWidths[tpI];
                        }
                    } else {
                        tmpWidthsN[totI] = btW - totW;
                        ++btI;
                        if ((double)Math.abs(btW - tpW) < 1.0E-4 && ++tpI < tmpWidths.length) {
                            tpW+=tmpWidths[tpI];
                        }
                        if (btI < cols) {
                            btW+=lDummyTable.widths[btI];
                        }
                    }
                    totW+=tmpWidthsN[totI];
                    ++totI;
                }
                tmpWidths = new float[totI];
                System.arraycopy(tmpWidthsN, 0, tmpWidths, 0, totI);
                lNewMaxColumns = totI;
            }
            lDummyColumnWidths[j] = tmpWidths;
            lTotalColumns+=lNewMaxColumns;
            lDummyWidths[j] = lNewMaxColumns;
        }
        for (i = 0; i < this.rows.size(); ++i) {
            lNewMaxRows = 1;
            for (j = 0; j < this.columns; ++j) {
                Class class_ = class$0;
                if (class_ == null) {
                    try {
                        class_ = Class.forName("com.lowagie.text.Table");
                    }
                    catch (ClassNotFoundException v3) {
                        throw new NoClassDefFoundError(v3.getMessage());
                    }
                }
                if (!class_.isInstance(((Row)this.rows.get(i)).getCell(j))) continue;
                isTable = true;
                lDummyTable = (Table)((Row)this.rows.get(i)).getCell(j);
                if (lDummyTable.getDimension().height <= lNewMaxRows) continue;
                lNewMaxRows = lDummyTable.getDimension().height;
            }
            lTotalRows+=lNewMaxRows;
            lDummyHeights[i] = lNewMaxRows;
        }
        if (lTotalColumns != this.columns || lTotalRows != this.rows.size() || isTable) {
            lNewWidths = new float[lTotalColumns];
            int lDummy = 0;
            for (int tel = 0; tel < this.widths.length; ++tel) {
                if (lDummyWidths[tel] != 1) {
                    for (int tel2 = 0; tel2 < lDummyWidths[tel]; ++tel2) {
                        lNewWidths[lDummy] = this.widths[tel] * lDummyColumnWidths[tel][tel2] / 100.0f;
                        ++lDummy;
                    }
                    continue;
                }
                lNewWidths[lDummy] = this.widths[tel];
                ++lDummy;
            }
            newRows = new ArrayList<Row>(lTotalRows);
            for (i = 0; i < lTotalRows; ++i) {
                newRows.add(new Row(lTotalColumns));
            }
            int lDummyRow = 0;
            int lDummyColumn = 0;
            Object lDummyElement = null;
            for (i = 0; i < this.rows.size(); ++i) {
                lDummyColumn = 0;
                lNewMaxRows = 1;
                for (j = 0; j < this.columns; ++j) {
                    Class class_ = class$0;
                    if (class_ == null) {
                        try {
                            class_ = Class.forName("com.lowagie.text.Table");
                        }
                        catch (ClassNotFoundException v5) {
                            throw new NoClassDefFoundError(v5.getMessage());
                        }
                    }
                    if (class_.isInstance(((Row)this.rows.get(i)).getCell(j))) {
                        lDummyTable = (Table)((Row)this.rows.get(i)).getCell(j);
                        int[] colMap = new int[lDummyTable.widths.length + 1];
                        int ct = 0;
                        block20 : for (int cb = 0; cb < lDummyTable.widths.length; ++cb) {
                            colMap[cb] = lDummyColumn + ct;
                            float wb = lDummyTable.widths[cb];
                            float wt = 0.0f;
                            while (ct < lDummyWidths[j]) {
                                if (Math.abs(Table.convertWidth(wb) - Table.convertWidth(wt+=lDummyColumnWidths[j][ct++])) < 1.0E-4) continue block20;
                            }
                        }
                        colMap[cb] = lDummyColumn + ct;
                        for (int k = 0; k < lDummyTable.getDimension().height; ++k) {
                            for (int l = 0; l < lDummyTable.getDimension().width; ++l) {
                                int yy = l;
                                lDummyElement = lDummyTable.getElement(k, l);
                                if (lDummyElement == null) continue;
                                int col = lDummyColumn + l;
                                Class class_2 = class$1;
                                if (class_2 == null) {
                                    try {
                                        class_2 = Class.forName("com.lowagie.text.Cell");
                                    }
                                    catch (ClassNotFoundException v7) {
                                        throw new NoClassDefFoundError(v7.getMessage());
                                    }
                                }
                                if (class_2.isInstance(lDummyElement)) {
                                    Cell lDummyC = (Cell)lDummyElement;
                                    col = colMap[l];
                                    int ot = colMap[l + lDummyC.colspan()];
                                    lDummyC.setColspan(ot - col);
                                }
                                ((Row)newRows.get(k + lDummyRow)).addElement(lDummyElement, col);
                            }
                        }
                    } else {
                        Object aElement = this.getElement(i, j);
                        Class class_3 = class$1;
                        if (class_3 == null) {
                            try {
                                class_3 = Class.forName("com.lowagie.text.Cell");
                            }
                            catch (ClassNotFoundException v9) {
                                throw new NoClassDefFoundError(v9.getMessage());
                            }
                        }
                        if (class_3.isInstance(aElement)) {
                            ((Cell)aElement).setRowspan(((Cell)((Row)this.rows.get(i)).getCell(j)).rowspan() + lDummyHeights[i] - 1);
                            ((Cell)aElement).setColspan(((Cell)((Row)this.rows.get(i)).getCell(j)).colspan() + lDummyWidths[j] - 1);
                            this.placeCell(newRows, (Cell)aElement, new Point(lDummyRow, lDummyColumn));
                        }
                    }
                    lDummyColumn+=lDummyWidths[j];
                }
                lDummyRow+=lDummyHeights[i];
            }
            this.columns = lTotalColumns;
            this.rows = newRows;
            this.widths = lNewWidths;
        }
    }

    private void fillEmptyMatrixCells() {
        try {
            for (int i = 0; i < this.rows.size(); ++i) {
                for (int j = 0; j < this.columns; ++j) {
                    if (((Row)this.rows.get(i)).isReserved(j)) continue;
                    this.addCell(this.defaultLayout, new Point(i, j));
                }
            }
        }
        catch (BadElementException bee) {
            throw new ExceptionConverter(bee);
        }
    }

    private boolean isValidLocation(Cell aCell, Point aLocation) {
        if (aLocation.x < this.rows.size()) {
            if (aLocation.y + aCell.colspan() > this.columns) {
                return false;
            }
            int difx = this.rows.size() - aLocation.x > aCell.rowspan() ? aCell.rowspan() : this.rows.size() - aLocation.x;
            int dify = this.columns - aLocation.y > aCell.colspan() ? aCell.colspan() : this.columns - aLocation.y;
            for (int i = aLocation.x; i < aLocation.x + difx; ++i) {
                for (int j = aLocation.y; j < aLocation.y + dify; ++j) {
                    if (!((Row)this.rows.get(i)).isReserved(j)) continue;
                    return false;
                }
            }
        } else if (aLocation.y + aCell.colspan() > this.columns) {
            return false;
        }
        return true;
    }

    private void placeCell(ArrayList someRows, Cell aCell, Point aPosition) {
        int i;
        Row row = null;
        int lColumns = ((Row)someRows.get(0)).columns();
        int rowCount = aPosition.x + aCell.rowspan() - someRows.size();
        this.assumeTableDefaults(aCell);
        if (aPosition.x + aCell.rowspan() > someRows.size()) {
            for (i = 0; i < rowCount; ++i) {
                row = new Row(lColumns);
                someRows.add(row);
            }
        }
        for (i = aPosition.x + 1; i < aPosition.x + aCell.rowspan(); ++i) {
            if (((Row)someRows.get(i)).reserve(aPosition.y, aCell.colspan())) continue;
            throw new RuntimeException("addCell - error in reserve");
        }
        row = (Row)someRows.get(aPosition.x);
        row.addElement(aCell, aPosition.y);
    }

    public void addColumns(int aColumns) {
        int j;
        ArrayList<Row> newRows = new ArrayList<Row>(this.rows.size());
        int newColumns = this.columns + aColumns;
        for (int i = 0; i < this.rows.size(); ++i) {
            Row row = new Row(newColumns);
            for (j = 0; j < this.columns; ++j) {
                row.setElement(((Row)this.rows.get(i)).getCell(j), j);
            }
            for (j = this.columns; j < newColumns && i < this.curPosition.x; ++j) {
                row.setElement(this.defaultLayout, j);
            }
            newRows.add(row);
        }
        float[] newWidths = new float[newColumns];
        for (j = 0; j < this.columns; ++j) {
            newWidths[j] = this.widths[j];
        }
        for (j = this.columns; j < newColumns; ++j) {
            newWidths[j] = 0.0f;
        }
        this.columns = newColumns;
        this.widths = newWidths;
        this.rows = newRows;
    }

    public float[] getWidths(float left, float totalWidth) {
        float[] w = new float[this.columns + 1];
        switch (this.alignment) {
            case 0: {
                w[0] = left;
                break;
            }
            case 2: {
                w[0] = left + totalWidth * (100.0f - this.widthPercentage) / 100.0f;
                break;
            }
            default: {
                w[0] = left + totalWidth * (100.0f - this.widthPercentage) / 200.0f;
            }
        }
        totalWidth = totalWidth * this.widthPercentage / 100.0f;
        for (int i = 1; i < this.columns; ++i) {
            w[i] = w[i - 1] + this.widths[i - 1] * totalWidth / 100.0f;
        }
        w[this.columns] = w[0] + totalWidth;
        return w;
    }

    private void setCurrentLocationToNextValidPosition(Point aLocation) {
        int i = aLocation.x;
        int j = aLocation.y;
        do {
            if (j + 1 == this.columns) {
                ++i;
                j = 0;
                continue;
            }
            ++j;
        } while (i < this.rows.size() && j < this.columns && ((Row)this.rows.get(i)).isReserved(j));
        this.curPosition = new Point(i, j);
    }

    public static boolean isTag(String tag) {
        return "table".equals(tag);
    }

    public void setAlternatingRowAttribute(String name, String value0, String value1) {
        String[] value;
        if (value0 == null || value1 == null) {
            throw new NullPointerException("MarkupTable#setAlternatingRowAttribute(): null values are not permitted.");
        }
        if (this.alternatingRowAttributes == null) {
            this.alternatingRowAttributes = new Hashtable();
        }
        if ((value = (String[])this.alternatingRowAttributes.get(name)) == null) {
            value = new String[]{value0, value1};
        }
        this.alternatingRowAttributes.put(name, value);
    }

    public float top() {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public float bottom() {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public float left() {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public float right() {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public float top(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public float bottom(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public float left(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public float right(int margin) {
        throw new UnsupportedOperationException("Dimensions of a Table can't be calculated. See the FAQ.");
    }

    public void setTop(int value) {
        throw new UnsupportedOperationException("Dimensions of a Table are attributed automagically. See the FAQ.");
    }

    public void setBottom(int value) {
        throw new UnsupportedOperationException("Dimensions of a Table are attributed automagically. See the FAQ.");
    }

    public void setLeft(int value) {
        throw new UnsupportedOperationException("Dimensions of a Table are attributed automagically. See the FAQ.");
    }

    public void setRight(int value) {
        throw new UnsupportedOperationException("Dimensions of a Table are attributed automagically. See the FAQ.");
    }

    public int getNextRow() {
        return this.curPosition.x;
    }

    public int getNextColumn() {
        return this.curPosition.y;
    }

    private static final double convertWidth(double val) {
        if (val == 0.0) {
            return 0.0;
        }
        try {
            String tmp = widthFormat.format(val);
            Number result = widthFormat.parse(tmp);
            return result.doubleValue();
        }
        catch (ParseException pe) {
            throw new RuntimeException("Could not convert double to width for val:" + val);
        }
    }

    public PdfPTable createPdfPTable() throws BadElementException {
        PdfPTable pdfptable;
        if (!this.convert2pdfptable) {
            throw new BadElementException("No error, just an old style table");
        }
        this.setAutoFillEmptyCells(true);
        this.complete();
        pdfptable = new PdfPTable(this.widths);
        pdfptable.setTableEvent(SimpleTable.getDimensionlessInstance(this, this.cellspacing));
        pdfptable.setHeaderRows(this.lastHeaderRow + 1);
        pdfptable.setSplitLate(this.cellsFitPage);
        if (!Float.isNaN(this.offset)) {
            pdfptable.setSpacingBefore(this.offset);
        }
        pdfptable.setHorizontalAlignment(this.alignment);
        if (this.absWidth.length() > 0) {
            try {
                pdfptable.setTotalWidth(Float.parseFloat(this.absWidth));
            }
            catch (Exception e1) {
                try {
                    pdfptable.setTotalWidth(Integer.parseInt(this.absWidth));
                }
                catch (Exception e2) {
                    pdfptable.setWidthPercentage(this.widthPercentage);
                }
            }
        } else {
            pdfptable.setWidthPercentage(this.widthPercentage);
        }
        Iterator iterator = this.iterator();
        while (iterator.hasNext()) {
            Row row = (Row)iterator.next();
            for (int i = 0; i < row.columns(); ++i) {
                PdfPCell pcell;
                Element cell = (Element)row.getCell(i);
                if (cell == null) continue;
                if (cell instanceof Table) {
                    pcell = new PdfPCell(((Table)cell).createPdfPTable());
                } else if (cell instanceof Cell) {
                    pcell = ((Cell)cell).createPdfPCell();
                    pcell.setPadding(this.cellpadding + this.cellspacing / 2.0f);
                    pcell.setCellEvent(SimpleCell.getDimensionlessInstance((Cell)cell, this.cellspacing));
                } else {
                    pcell = new PdfPCell();
                }
                pdfptable.addCell(pcell);
            }
        }
        return pdfptable;
    }

    public boolean isConvert2pdfptable() {
        return this.convert2pdfptable;
    }

    public void setConvert2pdfptable(boolean convert2pdfptable) {
        this.convert2pdfptable = convert2pdfptable;
    }
}

