/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.ElementTags;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.Phrase;
import com.lowagie.text.TextElementArray;
import com.lowagie.text.markup.MarkupParser;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

public class Anchor
extends Phrase
implements TextElementArray,
MarkupAttributes {
    public static final String ANCHOR = "anchor";
    protected String name = null;
    protected String reference = null;

    public Anchor() {
        super(16.0f);
    }

    public Anchor(float leading) {
        super(leading);
    }

    public Anchor(Chunk chunk) {
        super(chunk);
    }

    public Anchor(String string) {
        super(string);
    }

    public Anchor(String string, Font font) {
        super(string, font);
    }

    public Anchor(float leading, Chunk chunk) {
        super(leading, chunk);
    }

    public Anchor(float leading, String string) {
        super(leading, string);
    }

    public Anchor(float leading, String string, Font font) {
        super(leading, string, font);
    }

    public Anchor(Properties attributes) {
        this("", FontFactory.getFont(attributes));
        String value = (String)attributes.remove("itext");
        if (value != null) {
            Chunk chunk = new Chunk(value);
            value = (String)attributes.remove(ElementTags.GENERICTAG);
            if (value != null) {
                chunk.setGenericTag(value);
            }
            this.add(chunk);
        }
        if ((value = (String)attributes.remove("leading")) != null) {
            this.setLeading(Float.valueOf(String.valueOf(value) + "f").floatValue());
        } else {
            value = (String)attributes.remove("line-height");
            if (value != null) {
                this.setLeading(MarkupParser.parseLength(value));
            }
        }
        value = (String)attributes.remove("name");
        if (value != null) {
            this.setName(value);
        }
        if ((value = (String)attributes.remove("reference")) != null) {
            this.setReference(value);
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public boolean process(ElementListener listener) {
        try {
            Iterator i = this.getChunks().iterator();
            boolean localDestination = this.reference != null && this.reference.startsWith("#");
            boolean notGotoOK = true;
            while (i.hasNext()) {
                Chunk chunk = (Chunk)i.next();
                if (this.name != null && notGotoOK && !chunk.isEmpty()) {
                    chunk.setLocalDestination(this.name);
                    notGotoOK = false;
                }
                if (localDestination) {
                    chunk.setLocalGoto(this.reference.substring(1));
                }
                listener.add(chunk);
            }
            return true;
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public ArrayList getChunks() {
        ArrayList<Chunk> tmp = new ArrayList<Chunk>();
        Iterator i = this.iterator();
        boolean localDestination = this.reference != null && this.reference.startsWith("#");
        boolean notGotoOK = true;
        while (i.hasNext()) {
            Chunk chunk = (Chunk)i.next();
            if (this.name != null && notGotoOK && !chunk.isEmpty()) {
                chunk.setLocalDestination(this.name);
                notGotoOK = false;
            }
            if (localDestination) {
                chunk.setLocalGoto(this.reference.substring(1));
            } else if (this.reference != null) {
                chunk.setAnchor(this.reference);
            }
            tmp.add(chunk);
        }
        return tmp;
    }

    public int type() {
        return 17;
    }

    public Iterator getElements() {
        return this.iterator();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String name() {
        return this.name;
    }

    public String reference() {
        return this.reference;
    }

    public URL url() {
        try {
            return new URL(this.reference);
        }
        catch (MalformedURLException mue) {
            return null;
        }
    }

    public static boolean isTag(String tag) {
        return "anchor".equals(tag);
    }
}

