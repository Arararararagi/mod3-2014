/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.DocumentException;

public class BadElementException
extends DocumentException {
    public BadElementException(Exception ex) {
        super(ex);
    }

    BadElementException() {
    }

    public BadElementException(String message) {
        super(message);
    }
}

