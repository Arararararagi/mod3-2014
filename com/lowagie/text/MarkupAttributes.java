/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Element;
import java.util.Properties;
import java.util.Set;

public interface MarkupAttributes
extends Element {
    public void setMarkupAttribute(String var1, String var2);

    public void setMarkupAttributes(Properties var1);

    public String getMarkupAttribute(String var1);

    public Set getMarkupAttributeNames();

    public Properties getMarkupAttributes();
}

