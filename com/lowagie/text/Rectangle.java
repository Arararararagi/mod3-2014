/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.MarkupAttributes;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

public class Rectangle
implements Element,
MarkupAttributes {
    public static final int UNDEFINED = -1;
    public static final int TOP = 1;
    public static final int BOTTOM = 2;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int NO_BORDER = 0;
    public static final int BOX = 15;
    protected float llx;
    protected float lly;
    protected float urx;
    protected float ury;
    protected int border = -1;
    protected float borderWidth = -1.0f;
    protected Color color = null;
    protected Color borderColorLeft = null;
    protected Color borderColorRight = null;
    protected Color borderColorTop = null;
    protected Color borderColorBottom = null;
    protected float borderWidthLeft = -1.0f;
    protected float borderWidthRight = -1.0f;
    protected float borderWidthTop = -1.0f;
    protected float borderWidthBottom = -1.0f;
    protected boolean useVariableBorders = false;
    protected Color background = null;
    protected float grayFill = 0.0f;
    protected int rotation = 0;
    protected Properties markupAttributes;

    public Rectangle(float llx, float lly, float urx, float ury) {
        this.llx = llx;
        this.lly = lly;
        this.urx = urx;
        this.ury = ury;
    }

    public Rectangle(float urx, float ury) {
        this(0.0f, 0.0f, urx, ury);
    }

    public Rectangle(Rectangle rect) {
        this(rect.llx, rect.lly, rect.urx, rect.ury);
        this.cloneNonPositionParameters(rect);
    }

    public void cloneNonPositionParameters(Rectangle rect) {
        this.rotation = rect.rotation;
        this.border = rect.border;
        this.borderWidth = rect.borderWidth;
        this.color = rect.color;
        this.background = rect.background;
        this.grayFill = rect.grayFill;
        this.borderColorLeft = rect.borderColorLeft;
        this.borderColorRight = rect.borderColorRight;
        this.borderColorTop = rect.borderColorTop;
        this.borderColorBottom = rect.borderColorBottom;
        this.borderWidthLeft = rect.borderWidthLeft;
        this.borderWidthRight = rect.borderWidthRight;
        this.borderWidthTop = rect.borderWidthTop;
        this.borderWidthBottom = rect.borderWidthBottom;
        this.useVariableBorders = rect.useVariableBorders;
    }

    public void softCloneNonPositionParameters(Rectangle rect) {
        if (rect.rotation != 0) {
            this.rotation = rect.rotation;
        }
        if (rect.border != -1) {
            this.border = rect.border;
        }
        if (rect.borderWidth != -1.0f) {
            this.borderWidth = rect.borderWidth;
        }
        if (rect.color != null) {
            this.color = rect.color;
        }
        if (rect.background != null) {
            this.background = rect.background;
        }
        if (rect.grayFill != 0.0f) {
            this.grayFill = rect.grayFill;
        }
        if (rect.borderColorLeft != null) {
            this.borderColorLeft = rect.borderColorLeft;
        }
        if (rect.borderColorRight != null) {
            this.borderColorRight = rect.borderColorRight;
        }
        if (rect.borderColorTop != null) {
            this.borderColorTop = rect.borderColorTop;
        }
        if (rect.borderColorBottom != null) {
            this.borderColorBottom = rect.borderColorBottom;
        }
        if (rect.borderWidthLeft != -1.0f) {
            this.borderWidthLeft = rect.borderWidthLeft;
        }
        if (rect.borderWidthRight != -1.0f) {
            this.borderWidthRight = rect.borderWidthRight;
        }
        if (rect.borderWidthTop != -1.0f) {
            this.borderWidthTop = rect.borderWidthTop;
        }
        if (rect.borderWidthBottom != -1.0f) {
            this.borderWidthBottom = rect.borderWidthBottom;
        }
        if (this.useVariableBorders) {
            this.useVariableBorders = rect.useVariableBorders;
        }
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 30;
    }

    public ArrayList getChunks() {
        return new ArrayList();
    }

    public void normalize() {
        float a;
        if (this.llx > this.urx) {
            a = this.llx;
            this.llx = this.urx;
            this.urx = a;
        }
        if (this.lly > this.ury) {
            a = this.lly;
            this.lly = this.ury;
            this.ury = a;
        }
    }

    public Rectangle rectangle(float top, float bottom) {
        Rectangle tmp = new Rectangle(this);
        if (this.top() > top) {
            tmp.setTop(top);
            tmp.setBorder(this.border - (this.border & 1));
        }
        if (this.bottom() < bottom) {
            tmp.setBottom(bottom);
            tmp.setBorder(this.border - (this.border & 2));
        }
        return tmp;
    }

    public Rectangle rotate() {
        Rectangle rect = new Rectangle(this.lly, this.llx, this.ury, this.urx);
        rect.rotation = this.rotation + 90;
        rect.rotation%=360;
        return rect;
    }

    public void setLeft(float value) {
        this.llx = value;
    }

    public void setRight(float value) {
        this.urx = value;
    }

    public void setTop(float value) {
        this.ury = value;
    }

    public void setBottom(float value) {
        this.lly = value;
    }

    public void setBorder(int value) {
        this.border = value;
    }

    public void enableBorderSide(int side) {
        if (this.border == -1) {
            this.border = 0;
        }
        this.border|=side;
    }

    public void disableBorderSide(int side) {
        if (this.border == -1) {
            this.border = 0;
        }
        this.border&=~ side;
    }

    public void setBorderWidth(float value) {
        this.borderWidth = value;
    }

    public void setBorderColor(Color value) {
        this.color = value;
    }

    public void setBorderColorRight(Color value) {
        this.borderColorRight = value;
    }

    public void setBorderColorLeft(Color value) {
        this.borderColorLeft = value;
    }

    public void setBorderColorTop(Color value) {
        this.borderColorTop = value;
    }

    public void setBorderColorBottom(Color value) {
        this.borderColorBottom = value;
    }

    public void setBackgroundColor(Color value) {
        this.background = value;
    }

    public void setGrayFill(float value) {
        if (value >= 0.0f && (double)value <= 1.0) {
            this.grayFill = value;
        }
    }

    public float left() {
        return this.llx;
    }

    public float right() {
        return this.urx;
    }

    public float top() {
        return this.ury;
    }

    public float bottom() {
        return this.lly;
    }

    public float left(float margin) {
        return this.llx + margin;
    }

    public float right(float margin) {
        return this.urx - margin;
    }

    public float top(float margin) {
        return this.ury - margin;
    }

    public float bottom(float margin) {
        return this.lly + margin;
    }

    public float width() {
        return this.urx - this.llx;
    }

    public float height() {
        return this.ury - this.lly;
    }

    public boolean hasBorders() {
        if (this.border > 0 && (this.borderWidth > 0.0f || this.borderWidthLeft > 0.0f || this.borderWidthRight > 0.0f || this.borderWidthTop > 0.0f || this.borderWidthBottom > 0.0f)) {
            return true;
        }
        return false;
    }

    public boolean hasBorder(int type) {
        if (this.border != -1 && (this.border & type) == type) {
            return true;
        }
        return false;
    }

    public int border() {
        return this.border;
    }

    public float borderWidth() {
        return this.borderWidth;
    }

    public Color borderColor() {
        return this.color;
    }

    public Color backgroundColor() {
        return this.background;
    }

    public float grayFill() {
        return this.grayFill;
    }

    public int getRotation() {
        return this.rotation;
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }

    public Color getBorderColorLeft() {
        return this.borderColorLeft;
    }

    public Color getBorderColorRight() {
        return this.borderColorRight;
    }

    public Color getBorderColorTop() {
        return this.borderColorTop;
    }

    public Color getBorderColorBottom() {
        return this.borderColorBottom;
    }

    public float getBorderWidthLeft() {
        return this.getVariableBorderWidth(this.borderWidthLeft, 4);
    }

    public void setBorderWidthLeft(float borderWidthLeft) {
        this.borderWidthLeft = borderWidthLeft;
        this.updateBorderBasedOnWidth(borderWidthLeft, 4);
    }

    public float getBorderWidthRight() {
        return this.getVariableBorderWidth(this.borderWidthRight, 8);
    }

    public void setBorderWidthRight(float borderWidthRight) {
        this.borderWidthRight = borderWidthRight;
        this.updateBorderBasedOnWidth(borderWidthRight, 8);
    }

    public float getBorderWidthTop() {
        return this.getVariableBorderWidth(this.borderWidthTop, 1);
    }

    public void setBorderWidthTop(float borderWidthTop) {
        this.borderWidthTop = borderWidthTop;
        this.updateBorderBasedOnWidth(borderWidthTop, 1);
    }

    public float getBorderWidthBottom() {
        return this.getVariableBorderWidth(this.borderWidthBottom, 2);
    }

    public void setBorderWidthBottom(float borderWidthBottom) {
        this.borderWidthBottom = borderWidthBottom;
        this.updateBorderBasedOnWidth(borderWidthBottom, 2);
    }

    private void updateBorderBasedOnWidth(float width, int side) {
        this.useVariableBorders = true;
        if (width > 0.0f) {
            this.enableBorderSide(side);
        } else {
            this.disableBorderSide(side);
        }
    }

    private float getVariableBorderWidth(float variableWidthValue, int side) {
        if ((this.border & side) != 0) {
            return variableWidthValue != -1.0f ? variableWidthValue : this.borderWidth;
        }
        return 0.0f;
    }

    public boolean isUseVariableBorders() {
        return this.useVariableBorders;
    }

    public void setUseVariableBorders(boolean useVariableBorders) {
        this.useVariableBorders = useVariableBorders;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer("Rectangle: ");
        buf.append(this.width());
        buf.append("x");
        buf.append(this.height());
        return buf.toString();
    }
}

