/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.codec.wmf.InputMeta;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class ImgWMF
extends Image
implements Element {
    ImgWMF(Image image) {
        super(image);
    }

    public ImgWMF(URL url) throws BadElementException, IOException {
        super(url);
        this.processParameters();
    }

    public ImgWMF(String filename) throws BadElementException, MalformedURLException, IOException {
        this(Image.toURL(filename));
    }

    public ImgWMF(byte[] img) throws BadElementException, IOException {
        super((URL)null);
        this.rawData = img;
        this.originalData = img;
        this.processParameters();
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    private void processParameters() throws BadElementException, IOException {
        block7 : {
            this.type = 35;
            this.originalType = 6;
            InputStream is = null;
            try {
                String errorID;
                if (this.rawData == null) {
                    is = this.url.openStream();
                    errorID = this.url.toString();
                } else {
                    is = new ByteArrayInputStream(this.rawData);
                    errorID = "Byte array";
                }
                InputMeta in = new InputMeta(is);
                if (in.readInt() != -1698247209) {
                    throw new BadElementException(String.valueOf(errorID) + " is not a valid placeable windows metafile.");
                }
                in.readWord();
                int left = in.readShort();
                int top = in.readShort();
                int right = in.readShort();
                int bottom = in.readShort();
                int inch = in.readWord();
                this.dpiX = 72;
                this.dpiY = 72;
                this.scaledHeight = (float)(bottom - top) / (float)inch * 72.0f;
                this.setTop(this.scaledHeight);
                this.scaledWidth = (float)(right - left) / (float)inch * 72.0f;
                this.setRight(this.scaledWidth);
                java.lang.Object var9_11 = null;
                if (is == null) break block7;
            }
            catch (Throwable var10_9) {
                java.lang.Object var9_10 = null;
                if (is != null) {
                    is.close();
                }
                this.plainWidth = this.width();
                this.plainHeight = this.height();
                throw var10_9;
            }
            is.close();
        }
        this.plainWidth = this.width();
        this.plainHeight = this.height();
    }

    /*
     * Exception decompiling
     */
    public void readWMF(PdfTemplate template) throws IOException, DocumentException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // java.lang.IllegalStateException: Backjump on non jumping statement [] lbl19 : TryStatement: try { 1[TRYBLOCK]

        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:44)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:22)
        // org.benf.cfr.reader.util.graph.GraphVisitorDFS.process(GraphVisitorDFS.java:68)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner.removeUnreachableCode(Cleaner.java:54)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.RemoveDeterministicJumps.apply(RemoveDeterministicJumps.java:34)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:501)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:214)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:159)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:91)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:353)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:731)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:663)
        // org.benf.cfr.reader.Main.doJar(Main.java:126)
        // org.benf.cfr.reader.Main.main(Main.java:178)
        throw new IllegalStateException("Decompilation failed");
    }
}

