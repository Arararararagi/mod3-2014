/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.DocListener;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Header;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Meta;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Watermark;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Document
implements DocListener {
    private static final String ITEXT_VERSION = "iText1.3.1 by lowagie.com (based on itext-paulo-154)";
    public static boolean compress = true;
    private ArrayList listeners = new ArrayList();
    protected boolean open;
    protected boolean close;
    protected Rectangle pageSize;
    protected Watermark watermark = null;
    protected float marginLeft = 0.0f;
    protected float marginRight = 0.0f;
    protected float marginTop = 0.0f;
    protected float marginBottom = 0.0f;
    protected boolean marginMirroring = false;
    protected String javaScript_onLoad = null;
    protected String javaScript_onUnLoad = null;
    protected String htmlStyleClass = null;
    protected int pageN = 0;
    protected HeaderFooter header = null;
    protected HeaderFooter footer = null;

    public Document() {
        this(PageSize.A4);
    }

    public Document(Rectangle pageSize) {
        this(pageSize, 36.0f, 36.0f, 36.0f, 36.0f);
    }

    public Document(Rectangle pageSize, float marginLeft, float marginRight, float marginTop, float marginBottom) {
        this.pageSize = pageSize;
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        this.marginTop = marginTop;
        this.marginBottom = marginBottom;
    }

    public void addDocListener(DocListener listener) {
        this.listeners.add(listener);
    }

    public void removeDocListener(DocListener listener) {
        this.listeners.remove(listener);
    }

    public boolean add(Element element) throws DocumentException {
        if (this.close) {
            throw new DocumentException("The document has been closed. You can't add any Elements.");
        }
        int type = element.type();
        if (this.open) {
            if (type != 10 && type != 11 && type != 12 && type != 22 && type != 23 && type != 40 && type != 17 && type != 29 && type != 16 && type != 13 && type != 14 && type != 15 && type != 30 && type != 32 && type != 34 && type != 35 && type != 39) {
                throw new DocumentException("The document is open; you can only add Elements with content.");
            }
        } else if (type != 0 && type != 1 && type != 2 && type != 3 && type != 4 && type != 5 && type != 7 && type != 6) {
            throw new DocumentException("The document is not open yet; you can only add Meta information.");
        }
        boolean success = false;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            success|=listener.add(element);
        }
        return success;
    }

    public void open() {
        if (!this.close) {
            this.open = true;
        }
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.setPageSize(this.pageSize);
            listener.setMargins(this.marginLeft, this.marginRight, this.marginTop, this.marginBottom);
            listener.open();
        }
    }

    public boolean setPageSize(Rectangle pageSize) {
        this.pageSize = pageSize;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.setPageSize(pageSize);
        }
        return true;
    }

    public boolean add(Watermark watermark) {
        this.watermark = watermark;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.add(watermark);
        }
        return true;
    }

    public void removeWatermark() {
        this.watermark = null;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.removeWatermark();
        }
    }

    public boolean setMargins(float marginLeft, float marginRight, float marginTop, float marginBottom) {
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        this.marginTop = marginTop;
        this.marginBottom = marginBottom;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.setMargins(marginLeft, marginRight, marginTop, marginBottom);
        }
        return true;
    }

    public boolean newPage() throws DocumentException {
        if (!this.open || this.close) {
            return false;
        }
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.newPage();
        }
        return true;
    }

    public void setHeader(HeaderFooter header) {
        this.header = header;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.setHeader(header);
        }
    }

    public void resetHeader() {
        this.header = null;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.resetHeader();
        }
    }

    public void setFooter(HeaderFooter footer) {
        this.footer = footer;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.setFooter(footer);
        }
    }

    public void resetFooter() {
        this.footer = null;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.resetFooter();
        }
    }

    public void resetPageCount() {
        this.pageN = 0;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.resetPageCount();
        }
    }

    public void setPageCount(int pageN) {
        this.pageN = pageN;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.setPageCount(pageN);
        }
    }

    public int getPageNumber() {
        return this.pageN;
    }

    public void close() {
        if (!this.close) {
            this.open = false;
            this.close = true;
        }
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.close();
        }
    }

    public boolean addHeader(String name, String content) {
        try {
            return this.add(new Header(name, content));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public boolean addTitle(String title) {
        try {
            return this.add(new Meta(1, title));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public boolean addSubject(String subject) {
        try {
            return this.add(new Meta(2, subject));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public boolean addKeywords(String keywords) {
        try {
            return this.add(new Meta(3, keywords));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public boolean addAuthor(String author) {
        try {
            return this.add(new Meta(4, author));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public boolean addCreator(String creator) {
        try {
            return this.add(new Meta(7, creator));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public boolean addProducer() {
        try {
            return this.add(new Meta(5, "iText by lowagie.com"));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public boolean addCreationDate() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            return this.add(new Meta(6, sdf.format(new Date())));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    public float leftMargin() {
        return this.marginLeft;
    }

    public float rightMargin() {
        return this.marginRight;
    }

    public float topMargin() {
        return this.marginTop;
    }

    public float bottomMargin() {
        return this.marginBottom;
    }

    public float left() {
        return this.pageSize.left(this.marginLeft);
    }

    public float right() {
        return this.pageSize.right(this.marginRight);
    }

    public float top() {
        return this.pageSize.top(this.marginTop);
    }

    public float bottom() {
        return this.pageSize.bottom(this.marginBottom);
    }

    public float left(float margin) {
        return this.pageSize.left(this.marginLeft + margin);
    }

    public float right(float margin) {
        return this.pageSize.right(this.marginRight + margin);
    }

    public float top(float margin) {
        return this.pageSize.top(this.marginTop + margin);
    }

    public float bottom(float margin) {
        return this.pageSize.bottom(this.marginBottom + margin);
    }

    public Rectangle getPageSize() {
        return this.pageSize;
    }

    public boolean isOpen() {
        return this.open;
    }

    public static String getVersion() {
        return "iText1.3.1 by lowagie.com (based on itext-paulo-154)";
    }

    public void setJavaScript_onLoad(String code) {
        this.javaScript_onLoad = code;
    }

    public String getJavaScript_onLoad() {
        return this.javaScript_onLoad;
    }

    public void setJavaScript_onUnLoad(String code) {
        this.javaScript_onUnLoad = code;
    }

    public String getJavaScript_onUnLoad() {
        return this.javaScript_onUnLoad;
    }

    public void setHtmlStyleClass(String htmlStyleClass) {
        this.htmlStyleClass = htmlStyleClass;
    }

    public String getHtmlStyleClass() {
        return this.htmlStyleClass;
    }

    public void clearTextWrap() throws DocumentException {
        if (this.open && !this.close) {
            Iterator iterator = this.listeners.iterator();
            while (iterator.hasNext()) {
                DocListener listener = (DocListener)iterator.next();
                listener.clearTextWrap();
            }
        }
    }

    public boolean setMarginMirroring(boolean marginMirroring) {
        this.marginMirroring = marginMirroring;
        Iterator iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            DocListener listener = (DocListener)iterator.next();
            listener.setMarginMirroring(marginMirroring);
        }
        return true;
    }

    public boolean isMarginMirroring() {
        return this.marginMirroring;
    }
}

