/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.DocumentException;
import com.lowagie.text.ElementListener;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Watermark;

public interface DocListener
extends ElementListener {
    public void open();

    public boolean setPageSize(Rectangle var1);

    public boolean add(Watermark var1);

    public void removeWatermark();

    public boolean setMargins(float var1, float var2, float var3, float var4);

    public boolean setMarginMirroring(boolean var1);

    public boolean newPage() throws DocumentException;

    public void setHeader(HeaderFooter var1);

    public void resetHeader();

    public void setFooter(HeaderFooter var1);

    public void resetFooter();

    public void resetPageCount();

    public void setPageCount(int var1);

    public void clearTextWrap() throws DocumentException;

    public void close();
}

