/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import java.io.PrintStream;
import java.io.PrintWriter;

public class ExceptionConverter
extends RuntimeException {
    private Exception ex;
    private String prefix;

    public ExceptionConverter(Exception ex) {
        this.ex = ex;
        this.prefix = ex instanceof RuntimeException ? "" : "ExceptionConverter: ";
    }

    public Exception getException() {
        return this.ex;
    }

    public String getMessage() {
        return this.ex.getMessage();
    }

    public String getLocalizedMessage() {
        return this.ex.getLocalizedMessage();
    }

    public String toString() {
        return String.valueOf(this.prefix) + this.ex;
    }

    public void printStackTrace() {
        this.printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream s) {
        PrintStream printStream = s;
        synchronized (printStream) {
            s.print(this.prefix);
            this.ex.printStackTrace(s);
        }
    }

    public void printStackTrace(PrintWriter s) {
        PrintWriter printWriter = s;
        synchronized (printWriter) {
            s.print(this.prefix);
            this.ex.printStackTrace(s);
        }
    }

    public Throwable fillInStackTrace() {
        return this;
    }
}

