/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.ElementTags;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.List;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.Phrase;
import com.lowagie.text.TextElementArray;
import com.lowagie.text.markup.MarkupParser;
import java.util.Properties;

public class Paragraph
extends Phrase
implements TextElementArray,
MarkupAttributes {
    protected int alignment = -1;
    protected float indentationLeft;
    protected float indentationRight;
    protected float spacingBefore;
    protected float spacingAfter;
    protected boolean keeptogether = false;
    protected float multipliedLeading = 0.0f;
    private float firstLineIndent = 0.0f;
    private float extraParagraphSpace = 0.0f;

    public Paragraph() {
    }

    public Paragraph(float leading) {
        super(leading);
    }

    public Paragraph(Chunk chunk) {
        super(chunk);
    }

    public Paragraph(float leading, Chunk chunk) {
        super(leading, chunk);
    }

    public Paragraph(String string) {
        super(string);
    }

    public Paragraph(String string, Font font) {
        super(string, font);
    }

    public Paragraph(float leading, String string) {
        super(leading, string);
    }

    public Paragraph(float leading, String string, Font font) {
        super(leading, string, font);
    }

    public Paragraph(Phrase phrase) {
        super(phrase.leading, "", phrase.font());
        super.add(phrase);
    }

    public Paragraph(Properties attributes) {
        this("", FontFactory.getFont(attributes));
        String value = (String)attributes.remove("itext");
        if (value != null) {
            Chunk chunk = new Chunk(value);
            value = (String)attributes.remove(ElementTags.GENERICTAG);
            if (value != null) {
                chunk.setGenericTag(value);
            }
            this.add(chunk);
        }
        if ((value = (String)attributes.remove("align")) != null) {
            this.setAlignment(value);
        }
        if ((value = (String)attributes.remove("leading")) != null) {
            this.setLeading(Float.valueOf(String.valueOf(value) + "f").floatValue());
        } else {
            value = (String)attributes.remove("line-height");
            if (value != null) {
                this.setLeading(MarkupParser.parseLength(value));
            } else {
                this.setLeading(16.0f);
            }
        }
        value = (String)attributes.remove("indentationleft");
        if (value != null) {
            this.setIndentationLeft(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("indentationright")) != null) {
            this.setIndentationRight(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("keeptogether")) != null) {
            this.keeptogether = new Boolean(value);
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public int type() {
        return 12;
    }

    public boolean add(Object o) {
        if (o instanceof List) {
            List list = (List)o;
            list.setIndentationLeft(list.indentationLeft() + this.indentationLeft);
            list.setIndentationRight(this.indentationRight);
            return super.add(list);
        }
        if (o instanceof Image) {
            super.addSpecial((Image)o);
            return true;
        }
        if (o instanceof Paragraph) {
            super.add(o);
            super.add(Chunk.NEWLINE);
            return true;
        }
        return super.add(o);
    }

    public void setAlignment(int alignment) {
        this.alignment = alignment;
    }

    public void setAlignment(String alignment) {
        if ("Center".equalsIgnoreCase(alignment)) {
            this.alignment = 1;
            return;
        }
        if ("Right".equalsIgnoreCase(alignment)) {
            this.alignment = 2;
            return;
        }
        if ("Justify".equalsIgnoreCase(alignment)) {
            this.alignment = 3;
            return;
        }
        if ("JustifyAll".equalsIgnoreCase(alignment)) {
            this.alignment = 8;
            return;
        }
        this.alignment = 0;
    }

    public void setIndentationLeft(float indentation) {
        this.indentationLeft = indentation;
    }

    public void setIndentationRight(float indentation) {
        this.indentationRight = indentation;
    }

    public void setSpacingBefore(float spacing) {
        this.spacingBefore = spacing;
    }

    public void setSpacingAfter(float spacing) {
        this.spacingAfter = spacing;
    }

    public void setKeepTogether(boolean keeptogether) {
        this.keeptogether = keeptogether;
    }

    public boolean getKeepTogether() {
        return this.keeptogether;
    }

    public int alignment() {
        return this.alignment;
    }

    public float indentationLeft() {
        return this.indentationLeft;
    }

    public float indentationRight() {
        return this.indentationRight;
    }

    public float spacingBefore() {
        return this.spacingBefore;
    }

    public float spacingAfter() {
        return this.spacingAfter;
    }

    public static boolean isTag(String tag) {
        return "paragraph".equals(tag);
    }

    public void setLeading(float fixedLeading, float multipliedLeading) {
        this.leading = fixedLeading;
        this.multipliedLeading = multipliedLeading;
    }

    public void setLeading(float fixedLeading) {
        this.leading = fixedLeading;
        this.multipliedLeading = 0.0f;
    }

    public float getMultipliedLeading() {
        return this.multipliedLeading;
    }

    public float getFirstLineIndent() {
        return this.firstLineIndent;
    }

    public void setFirstLineIndent(float firstLineIndent) {
        this.firstLineIndent = firstLineIndent;
    }

    public float getExtraParagraphSpace() {
        return this.extraParagraphSpace;
    }

    public void setExtraParagraphSpace(float extraParagraphSpace) {
        this.extraParagraphSpace = extraParagraphSpace;
    }
}

