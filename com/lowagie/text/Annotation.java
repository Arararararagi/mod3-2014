/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.ElementTags;
import com.lowagie.text.MarkupAttributes;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

public class Annotation
implements Element,
MarkupAttributes {
    public static final int TEXT = 0;
    public static final int URL_NET = 1;
    public static final int URL_AS_STRING = 2;
    public static final int FILE_DEST = 3;
    public static final int FILE_PAGE = 4;
    public static final int NAMED_DEST = 5;
    public static final int LAUNCH = 6;
    public static final int SCREEN = 7;
    public static String TITLE = "title";
    public static String CONTENT = "content";
    public static String URL = "url";
    public static String FILE = "file";
    public static String DESTINATION = "destination";
    public static String PAGE = "page";
    public static String NAMED = "named";
    public static String APPLICATION = "application";
    public static String PARAMETERS = "parameters";
    public static String OPERATION = "operation";
    public static String DEFAULTDIR = "defaultdir";
    public static String LLX = "llx";
    public static String LLY = "lly";
    public static String URX = "urx";
    public static String URY = "ury";
    public static String MIMETYPE = "mime";
    protected int annotationtype;
    protected HashMap annotationAttributes = new HashMap();
    protected Properties markupAttributes = null;
    protected float llx = NaNf;
    protected float lly = NaNf;
    protected float urx = NaNf;
    protected float ury = NaNf;

    private Annotation(float llx, float lly, float urx, float ury) {
        this.llx = llx;
        this.lly = lly;
        this.urx = urx;
        this.ury = ury;
    }

    public Annotation(String title, String text) {
        this.annotationtype = 0;
        this.annotationAttributes.put(TITLE, title);
        this.annotationAttributes.put(CONTENT, text);
    }

    public Annotation(String title, String text, float llx, float lly, float urx, float ury) {
        this(llx, lly, urx, ury);
        this.annotationtype = 0;
        this.annotationAttributes.put(TITLE, title);
        this.annotationAttributes.put(CONTENT, text);
    }

    public Annotation(float llx, float lly, float urx, float ury, URL url) {
        this(llx, lly, urx, ury);
        this.annotationtype = 1;
        this.annotationAttributes.put(URL, url);
    }

    public Annotation(float llx, float lly, float urx, float ury, String url) {
        this(llx, lly, urx, ury);
        this.annotationtype = 2;
        this.annotationAttributes.put(FILE, url);
    }

    public Annotation(float llx, float lly, float urx, float ury, String file, String dest) {
        this(llx, lly, urx, ury);
        this.annotationtype = 3;
        this.annotationAttributes.put(FILE, file);
        this.annotationAttributes.put(DESTINATION, dest);
    }

    public Annotation(float llx, float lly, float urx, float ury, String moviePath, String mimeType, boolean showOnDisplay) {
        this(llx, lly, urx, ury);
        this.annotationtype = 7;
        this.annotationAttributes.put(FILE, moviePath);
        this.annotationAttributes.put(MIMETYPE, mimeType);
        boolean[] arrbl = new boolean[2];
        arrbl[1] = showOnDisplay;
        this.annotationAttributes.put(PARAMETERS, arrbl);
    }

    public Annotation(float llx, float lly, float urx, float ury, String file, int page) {
        this(llx, lly, urx, ury);
        this.annotationtype = 4;
        this.annotationAttributes.put(FILE, file);
        this.annotationAttributes.put(PAGE, new Integer(page));
    }

    public Annotation(float llx, float lly, float urx, float ury, int named) {
        this(llx, lly, urx, ury);
        this.annotationtype = 5;
        this.annotationAttributes.put(NAMED, new Integer(named));
    }

    public Annotation(float llx, float lly, float urx, float ury, String application, String parameters, String operation, String defaultdir) {
        this(llx, lly, urx, ury);
        this.annotationtype = 6;
        this.annotationAttributes.put(APPLICATION, application);
        this.annotationAttributes.put(PARAMETERS, parameters);
        this.annotationAttributes.put(OPERATION, operation);
        this.annotationAttributes.put(DEFAULTDIR, defaultdir);
    }

    public Annotation(Properties attributes) {
        String value = (String)attributes.remove(ElementTags.LLX);
        if (value != null) {
            this.llx = Float.valueOf(String.valueOf(value) + "f").floatValue();
        }
        if ((value = (String)attributes.remove(ElementTags.LLY)) != null) {
            this.lly = Float.valueOf(String.valueOf(value) + "f").floatValue();
        }
        if ((value = (String)attributes.remove(ElementTags.URX)) != null) {
            this.urx = Float.valueOf(String.valueOf(value) + "f").floatValue();
        }
        if ((value = (String)attributes.remove(ElementTags.URY)) != null) {
            this.ury = Float.valueOf(String.valueOf(value) + "f").floatValue();
        }
        String title = (String)attributes.remove("title");
        String text = (String)attributes.remove("content");
        if (title != null || text != null) {
            this.annotationtype = 0;
        } else {
            value = (String)attributes.remove("url");
            if (value != null) {
                this.annotationtype = 2;
                this.annotationAttributes.put(FILE, value);
            } else {
                value = (String)attributes.remove(ElementTags.NAMED);
                if (value != null) {
                    this.annotationtype = 5;
                    this.annotationAttributes.put(NAMED, Integer.valueOf(value));
                } else {
                    String file = (String)attributes.remove(ElementTags.FILE);
                    String destination = (String)attributes.remove(ElementTags.DESTINATION);
                    String page = (String)attributes.remove(ElementTags.PAGE);
                    if (file != null) {
                        this.annotationAttributes.put(FILE, file);
                        if (destination != null) {
                            this.annotationtype = 3;
                            this.annotationAttributes.put(DESTINATION, destination);
                        } else if (page != null) {
                            this.annotationtype = 4;
                            this.annotationAttributes.put(FILE, file);
                            this.annotationAttributes.put(PAGE, Integer.valueOf(page));
                        }
                    } else {
                        value = (String)attributes.remove(ElementTags.NAMED);
                        if (value != null) {
                            this.annotationtype = 6;
                            this.annotationAttributes.put(APPLICATION, value);
                            this.annotationAttributes.put(PARAMETERS, (String)attributes.remove(ElementTags.PARAMETERS));
                            this.annotationAttributes.put(OPERATION, (String)attributes.remove(ElementTags.OPERATION));
                            this.annotationAttributes.put(DEFAULTDIR, (String)attributes.remove(ElementTags.DEFAULTDIR));
                        }
                    }
                }
            }
        }
        if (this.annotationtype == 0) {
            if (title == null) {
                title = "";
            }
            if (text == null) {
                text = "";
            }
            this.annotationAttributes.put(TITLE, title);
            this.annotationAttributes.put(CONTENT, text);
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public int type() {
        return 29;
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public ArrayList getChunks() {
        return new ArrayList();
    }

    public void setDimensions(float llx, float lly, float urx, float ury) {
        this.llx = llx;
        this.lly = lly;
        this.urx = urx;
        this.ury = ury;
    }

    public float llx() {
        return this.llx;
    }

    public float lly() {
        return this.lly;
    }

    public float urx() {
        return this.urx;
    }

    public float ury() {
        return this.ury;
    }

    public float llx(float def) {
        if (Float.isNaN(this.llx)) {
            return def;
        }
        return this.llx;
    }

    public float lly(float def) {
        if (Float.isNaN(this.lly)) {
            return def;
        }
        return this.lly;
    }

    public float urx(float def) {
        if (Float.isNaN(this.urx)) {
            return def;
        }
        return this.urx;
    }

    public float ury(float def) {
        if (Float.isNaN(this.ury)) {
            return def;
        }
        return this.ury;
    }

    public int annotationType() {
        return this.annotationtype;
    }

    public String title() {
        String s = (String)this.annotationAttributes.get(TITLE);
        if (s == null) {
            s = "";
        }
        return s;
    }

    public String content() {
        String s = (String)this.annotationAttributes.get(CONTENT);
        if (s == null) {
            s = "";
        }
        return s;
    }

    public HashMap attributes() {
        return this.annotationAttributes;
    }

    public static boolean isTag(String tag) {
        return "annotation".equals(tag);
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }
}

