/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Anchor;
import com.lowagie.text.Annotation;
import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.ElementTags;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Graphic;
import com.lowagie.text.List;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.SpecialSymbol;
import com.lowagie.text.Table;
import com.lowagie.text.TextElementArray;
import com.lowagie.text.markup.MarkupParser;
import com.lowagie.text.pdf.BaseFont;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class Phrase
extends ArrayList
implements TextElementArray,
MarkupAttributes {
    protected float leading = NaNf;
    protected Font font = new Font();
    protected Properties markupAttributes;

    private Phrase(boolean dummy) {
    }

    public Phrase() {
        this(16.0f);
    }

    public Phrase(float leading) {
        this.leading = leading;
    }

    public Phrase(Chunk chunk) {
        super.add(chunk);
    }

    public Phrase(float leading, Chunk chunk) {
        this(leading);
        super.add(chunk);
    }

    public Phrase(String string) {
        this(NaNf, string, new Font());
    }

    public Phrase(String string, Font font) {
        this(NaNf, string, font);
        this.font = font;
    }

    public Phrase(float leading, String string) {
        this(leading, string, new Font());
    }

    public Phrase(float leading, String string, Font font) {
        this(leading);
        this.font = font;
        if (string != null && string.length() != 0) {
            super.add(new Chunk(string, font));
        }
    }

    public static final Phrase getInstance(String string) {
        return Phrase.getInstance(16, string, new Font());
    }

    public static final Phrase getInstance(int leading, String string) {
        return Phrase.getInstance(leading, string, new Font());
    }

    public static final Phrase getInstance(int leading, String string, Font font) {
        Phrase p = new Phrase(true);
        p.setLeading(leading);
        p.font = font;
        if (font.family() != 3 && font.family() != 4 && font.getBaseFont() == null) {
            int index;
            while ((index = SpecialSymbol.index(string)) > -1) {
                if (index > 0) {
                    String firstPart = string.substring(0, index);
                    p.add(new Chunk(firstPart, font));
                    string = string.substring(index);
                }
                Font symbol = new Font(3, font.size(), font.style(), font.color());
                StringBuffer buf = new StringBuffer();
                buf.append(SpecialSymbol.getCorrespondingSymbol(string.charAt(0)));
                string = string.substring(1);
                while (SpecialSymbol.index(string) == 0) {
                    buf.append(SpecialSymbol.getCorrespondingSymbol(string.charAt(0)));
                    string = string.substring(1);
                }
                p.add(new Chunk(buf.toString(), symbol));
            }
        }
        if (string != null && string.length() != 0) {
            p.add(new Chunk(string, font));
        }
        return p;
    }

    public Phrase(Properties attributes) {
        this("", FontFactory.getFont(attributes));
        this.clear();
        String value = (String)attributes.remove("leading");
        if (value != null) {
            this.setLeading(Float.valueOf(String.valueOf(value) + "f").floatValue());
        } else {
            value = (String)attributes.remove("line-height");
            if (value != null) {
                this.setLeading(MarkupParser.parseLength(value));
            }
        }
        value = (String)attributes.remove("itext");
        if (value != null) {
            Chunk chunk = new Chunk(value);
            value = (String)attributes.remove(ElementTags.GENERICTAG);
            if (value != null) {
                chunk.setGenericTag(value);
            }
            this.add(chunk);
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public boolean process(ElementListener listener) {
        try {
            Iterator i = this.iterator();
            while (i.hasNext()) {
                listener.add((Element)i.next());
            }
            return true;
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 11;
    }

    public ArrayList getChunks() {
        ArrayList tmp = new ArrayList();
        Iterator i = this.iterator();
        while (i.hasNext()) {
            tmp.addAll(((Element)i.next()).getChunks());
        }
        return tmp;
    }

    public void add(int index, Object o) {
        block5 : {
            try {
                Element element = (Element)o;
                if (element.type() == 10) {
                    Chunk chunk = (Chunk)element;
                    if (!this.font.isStandardFont()) {
                        chunk.setFont(this.font.difference(chunk.font()));
                    }
                    super.add(index, chunk);
                    break block5;
                }
                if (element.type() == 11 || element.type() == 17 || element.type() == 29 || element.type() == 22 || element.type() == 39) {
                    super.add(index, element);
                    break block5;
                }
                throw new ClassCastException(String.valueOf(element.type()));
            }
            catch (ClassCastException cce) {
                throw new ClassCastException("Insertion of illegal Element: " + cce.getMessage());
            }
        }
    }

    public boolean add(Object o) {
        if (o instanceof String) {
            return super.add(new Chunk((String)o, this.font));
        }
        try {
            Element element = (Element)o;
            switch (element.type()) {
                case 10: {
                    return this.addChunk((Chunk)o);
                }
                case 11: 
                case 12: {
                    Phrase phrase = (Phrase)o;
                    boolean success = true;
                    Iterator i = phrase.iterator();
                    while (i.hasNext()) {
                        Element e = (Element)i.next();
                        if (e instanceof Chunk) {
                            success&=this.addChunk((Chunk)e);
                            continue;
                        }
                        success&=this.add(e);
                    }
                    return success;
                }
                case 17: {
                    return super.add((Anchor)o);
                }
                case 29: {
                    return super.add((Annotation)o);
                }
                case 22: {
                    return super.add((Table)o);
                }
                case 14: {
                    return super.add((List)o);
                }
                case 39: {
                    return super.add((Graphic)o);
                }
            }
            throw new ClassCastException(String.valueOf(element.type()));
        }
        catch (ClassCastException cce) {
            throw new ClassCastException("Insertion of illegal Element: " + cce.getMessage());
        }
    }

    private synchronized boolean addChunk(Chunk chunk) {
        if (!this.font.isStandardFont()) {
            chunk.setFont(this.font.difference(chunk.font()));
        }
        if (!(this.size() <= 0 || chunk.hasAttributes())) {
            try {
                Chunk previous = (Chunk)this.get(this.size() - 1);
                if (!(previous.hasAttributes() || previous.font().compareTo(chunk.font()) != 0 || "".equals(previous.content().trim()) || "".equals(chunk.content().trim()))) {
                    previous.append(chunk.content());
                    return true;
                }
            }
            catch (ClassCastException previous) {
                // empty catch block
            }
        }
        return super.add(chunk);
    }

    public boolean addAll(Collection collection) {
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            this.add(iterator.next());
        }
        return true;
    }

    protected void addSpecial(Object object) {
        super.add(object);
    }

    public void setLeading(float leading) {
        this.leading = leading;
    }

    public boolean isEmpty() {
        switch (this.size()) {
            case 0: {
                return true;
            }
            case 1: {
                Element element = (Element)this.get(0);
                if (element.type() == 10 && ((Chunk)element).isEmpty()) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public boolean leadingDefined() {
        if (Float.isNaN(this.leading)) {
            return false;
        }
        return true;
    }

    public float leading() {
        if (Float.isNaN(this.leading)) {
            return this.font.leading(1.5f);
        }
        return this.leading;
    }

    public Font font() {
        return this.font;
    }

    public static boolean isTag(String tag) {
        return "phrase".equals(tag);
    }

    public void setMarkupAttribute(String name, String value) {
        if (this.markupAttributes == null) {
            this.markupAttributes = new Properties();
        }
        this.markupAttributes.put(name, value);
    }

    public void setMarkupAttributes(Properties markupAttributes) {
        this.markupAttributes = markupAttributes;
    }

    public String getMarkupAttribute(String name) {
        return this.markupAttributes == null ? null : String.valueOf(this.markupAttributes.get(name));
    }

    public Set getMarkupAttributeNames() {
        return Chunk.getKeySet(this.markupAttributes);
    }

    public Properties getMarkupAttributes() {
        return this.markupAttributes;
    }
}

