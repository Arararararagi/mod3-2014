/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.markup.MarkupParser;
import com.lowagie.text.pdf.BaseFont;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class FontFactory {
    public static final String COURIER = "Courier";
    public static final String COURIER_BOLD = "Courier-Bold";
    public static final String COURIER_OBLIQUE = "Courier-Oblique";
    public static final String COURIER_BOLDOBLIQUE = "Courier-BoldOblique";
    public static final String HELVETICA = "Helvetica";
    public static final String HELVETICA_BOLD = "Helvetica-Bold";
    public static final String HELVETICA_OBLIQUE = "Helvetica-Oblique";
    public static final String HELVETICA_BOLDOBLIQUE = "Helvetica-BoldOblique";
    public static final String SYMBOL = "Symbol";
    public static final String TIMES = "Times";
    public static final String TIMES_ROMAN = "Times-Roman";
    public static final String TIMES_BOLD = "Times-Bold";
    public static final String TIMES_ITALIC = "Times-Italic";
    public static final String TIMES_BOLDITALIC = "Times-BoldItalic";
    public static final String ZAPFDINGBATS = "ZapfDingbats";
    private static Properties trueTypeFonts = new Properties();
    private static Hashtable fontFamilies;
    public static String defaultEncoding;
    public static boolean defaultEmbedding;

    static {
        trueTypeFonts.setProperty("Courier", "Courier");
        trueTypeFonts.setProperty("Courier-Bold", "Courier-Bold");
        trueTypeFonts.setProperty("Courier-Oblique", "Courier-Oblique");
        trueTypeFonts.setProperty("Courier-BoldOblique", "Courier-BoldOblique");
        trueTypeFonts.setProperty("Helvetica", "Helvetica");
        trueTypeFonts.setProperty("Helvetica-Bold", "Helvetica-Bold");
        trueTypeFonts.setProperty("Helvetica-Oblique", "Helvetica-Oblique");
        trueTypeFonts.setProperty("Helvetica-BoldOblique", "Helvetica-BoldOblique");
        trueTypeFonts.setProperty("Symbol", "Symbol");
        trueTypeFonts.setProperty("Times-Roman", "Times-Roman");
        trueTypeFonts.setProperty("Times-Bold", "Times-Bold");
        trueTypeFonts.setProperty("Times-Italic", "Times-Italic");
        trueTypeFonts.setProperty("Times-BoldItalic", "Times-BoldItalic");
        trueTypeFonts.setProperty("ZapfDingbats", "ZapfDingbats");
        fontFamilies = new Hashtable();
        HashSet<String> tmp = new HashSet<String>();
        tmp.add("Courier");
        tmp.add("Courier-Bold");
        tmp.add("Courier-Oblique");
        tmp.add("Courier-BoldOblique");
        fontFamilies.put("Courier", tmp);
        tmp = new HashSet();
        tmp.add("Helvetica");
        tmp.add("Helvetica-Bold");
        tmp.add("Helvetica-Oblique");
        tmp.add("Helvetica-BoldOblique");
        fontFamilies.put("Helvetica", tmp);
        tmp = new HashSet();
        tmp.add("Symbol");
        fontFamilies.put("Symbol", tmp);
        tmp = new HashSet();
        tmp.add("Times-Roman");
        tmp.add("Times-Bold");
        tmp.add("Times-Italic");
        tmp.add("Times-BoldItalic");
        fontFamilies.put("Times", tmp);
        fontFamilies.put("Times-Roman", tmp);
        tmp = new HashSet();
        tmp.add("ZapfDingbats");
        fontFamilies.put("ZapfDingbats", tmp);
        defaultEncoding = "Cp1252";
        defaultEmbedding = false;
    }

    private FontFactory() {
    }

    public static Font getFont(String fontname, String encoding, boolean embedded, float size, int style, Color color) {
        if (fontname == null) {
            return new Font(-1, size, style, color);
        }
        HashSet tmp = (HashSet)fontFamilies.get(fontname);
        if (tmp != null) {
            String lowercasefontname = fontname.toLowerCase();
            int s = style == -1 ? 0 : style;
            int fs = 0;
            boolean found = false;
            Iterator i = tmp.iterator();
            while (i.hasNext()) {
                String f = (String)i.next();
                String lcf = f.toLowerCase();
                fs = 0;
                if (lcf.toLowerCase().indexOf("bold") != -1) {
                    fs|=1;
                }
                if (lcf.toLowerCase().indexOf("italic") != -1 || lcf.toLowerCase().indexOf("oblique") != -1) {
                    fs|=2;
                }
                if ((s & 3) != fs) continue;
                fontname = f;
                found = true;
                break;
            }
            if (style != -1 && found) {
                style&=~ fs;
            }
        }
        BaseFont basefont = null;
        try {
            try {
                basefont = BaseFont.createFont(fontname, encoding, embedded);
            }
            catch (DocumentException de) {
                fontname = trueTypeFonts.getProperty(fontname);
                if (fontname == null) {
                    return new Font(-1, size, style, color);
                }
                basefont = BaseFont.createFont(fontname, encoding, embedded);
            }
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
        catch (IOException ioe) {
            return new Font(-1, size, style, color);
        }
        catch (NullPointerException npe) {
            return new Font(-1, size, style, color);
        }
        return new Font(basefont, size, style, color);
    }

    public static Font getFont(Properties attributes) {
        String fontname = null;
        String encoding = defaultEncoding;
        boolean embedded = defaultEmbedding;
        float size = -1.0f;
        int style = 0;
        Color color = null;
        String value = (String)attributes.remove("style");
        if (value != null && value.length() > 0) {
            Properties styleAttributes = MarkupParser.parseAttributes(value);
            if (styleAttributes.size() == 0) {
                attributes.put("style", value);
            } else {
                fontname = (String)styleAttributes.remove("font-family");
                if (fontname != null) {
                    while (fontname.indexOf(",") != -1) {
                        String tmp = fontname.substring(0, fontname.indexOf(","));
                        fontname = FontFactory.isRegistered(tmp) ? tmp : fontname.substring(fontname.indexOf(",") + 1);
                    }
                }
                if ((value = (String)styleAttributes.remove("font-size")) != null) {
                    size = MarkupParser.parseLength(value);
                }
                if ((value = (String)styleAttributes.remove("font-weight")) != null) {
                    style|=Font.getStyleValue(value);
                }
                if ((value = (String)styleAttributes.remove("font-style")) != null) {
                    style|=Font.getStyleValue(value);
                }
                if ((value = (String)styleAttributes.remove("color")) != null) {
                    color = MarkupParser.decodeColor(value);
                }
                attributes.putAll(styleAttributes);
                Enumeration e = styleAttributes.keys();
                while (e.hasMoreElements()) {
                    Object o = e.nextElement();
                    attributes.put(o, styleAttributes.get(o));
                }
            }
        }
        if ((value = (String)attributes.remove("encoding")) != null) {
            encoding = value;
        }
        if ("true".equals((String)attributes.remove("embedded"))) {
            embedded = true;
        }
        if ((value = (String)attributes.remove("font")) != null) {
            fontname = value;
        }
        if ((value = (String)attributes.remove("size")) != null) {
            size = Float.valueOf(String.valueOf(value) + "f").floatValue();
        }
        if ((value = (String)attributes.remove("style")) != null) {
            style|=Font.getStyleValue(value);
        }
        if ((value = (String)attributes.remove("fontstyle")) != null) {
            style|=Font.getStyleValue(value);
        }
        String r = (String)attributes.remove("red");
        String g = (String)attributes.remove("green");
        String b = (String)attributes.remove("blue");
        if (r != null || g != null || b != null) {
            int red = 0;
            int green = 0;
            int blue = 0;
            if (r != null) {
                red = Integer.parseInt(r);
            }
            if (g != null) {
                green = Integer.parseInt(g);
            }
            if (b != null) {
                blue = Integer.parseInt(b);
            }
            color = new Color(red, green, blue);
        } else {
            value = (String)attributes.remove("color");
            if (value != null) {
                color = MarkupParser.decodeColor(value);
            }
        }
        if (fontname == null) {
            return FontFactory.getFont(null, encoding, embedded, size, style, color);
        }
        return FontFactory.getFont(fontname, encoding, embedded, size, style, color);
    }

    public static Font getFont(String fontname, String encoding, boolean embedded, float size, int style) {
        return FontFactory.getFont(fontname, encoding, embedded, size, style, null);
    }

    public static Font getFont(String fontname, String encoding, boolean embedded, float size) {
        return FontFactory.getFont(fontname, encoding, embedded, size, -1, null);
    }

    public static Font getFont(String fontname, String encoding, boolean embedded) {
        return FontFactory.getFont(fontname, encoding, embedded, -1.0f, -1, null);
    }

    public static Font getFont(String fontname, String encoding, float size, int style, Color color) {
        return FontFactory.getFont(fontname, encoding, defaultEmbedding, size, style, color);
    }

    public static Font getFont(String fontname, String encoding, float size, int style) {
        return FontFactory.getFont(fontname, encoding, defaultEmbedding, size, style, null);
    }

    public static Font getFont(String fontname, String encoding, float size) {
        return FontFactory.getFont(fontname, encoding, defaultEmbedding, size, -1, null);
    }

    public static Font getFont(String fontname, String encoding) {
        return FontFactory.getFont(fontname, encoding, defaultEmbedding, -1.0f, -1, null);
    }

    public static Font getFont(String fontname, float size, int style, Color color) {
        return FontFactory.getFont(fontname, defaultEncoding, defaultEmbedding, size, style, color);
    }

    public static Font getFont(String fontname, float size, int style) {
        return FontFactory.getFont(fontname, defaultEncoding, defaultEmbedding, size, style, null);
    }

    public static Font getFont(String fontname, float size) {
        return FontFactory.getFont(fontname, defaultEncoding, defaultEmbedding, size, -1, null);
    }

    public static Font getFont(String fontname) {
        return FontFactory.getFont(fontname, defaultEncoding, defaultEmbedding, -1.0f, -1, null);
    }

    public static void register(String path) {
        FontFactory.register(path, null);
    }

    public static void register(String path, String alias) {
        block15 : {
            try {
                if (path.toLowerCase().endsWith(".ttf") || path.toLowerCase().endsWith(".otf") || path.toLowerCase().indexOf(".ttc,") > 0) {
                    int i;
                    Object[] allNames = BaseFont.getAllFontNames(path, "Cp1252", null);
                    trueTypeFonts.setProperty((String)allNames[0], path);
                    if (alias != null) {
                        trueTypeFonts.setProperty(alias, path);
                    }
                    String fullName = null;
                    String familyName = null;
                    String[][] names = (String[][])allNames[2];
                    for (i = 0; i < names.length; ++i) {
                        if (!"0".equals(names[i][2])) continue;
                        fullName = names[i][3];
                        break;
                    }
                    for (i = 0; i < names.length; ++i) {
                        trueTypeFonts.setProperty(names[i][3], path);
                    }
                    if (fullName == null) break block15;
                    names = (String[][])allNames[1];
                    for (i = 0; i < names.length; ++i) {
                        if (!"0".equals(names[i][2])) continue;
                        familyName = names[i][3];
                        HashSet<String> tmp = (HashSet<String>)fontFamilies.get(familyName);
                        if (tmp == null) {
                            tmp = new HashSet<String>();
                        }
                        tmp.add(fullName);
                        fontFamilies.put(familyName, tmp);
                        break block15;
                    }
                    break block15;
                }
                if (path.toLowerCase().endsWith(".ttc")) {
                    if (alias != null) {
                        System.err.println("class FontFactory: You can't define an alias for a true type collection.");
                    }
                    String[] names = BaseFont.enumerateTTCNames(path);
                    for (int i = 0; i < names.length; ++i) {
                        FontFactory.register(String.valueOf(path) + "," + i);
                    }
                } else if (path.toLowerCase().endsWith(".afm")) {
                    BaseFont bf = BaseFont.createFont(path, "Cp1252", false);
                    trueTypeFonts.setProperty(bf.getPostscriptFontName(), path);
                    trueTypeFonts.setProperty(bf.getFullFontName()[0][3], path);
                }
            }
            catch (DocumentException de) {
                throw new ExceptionConverter(de);
            }
            catch (IOException ioe) {
                throw new ExceptionConverter(ioe);
            }
        }
    }

    public static int registerDirectory(String dir) {
        int count = 0;
        try {
            File file = new File(dir);
            if (!(file.exists() && file.isDirectory())) {
                return 0;
            }
            String[] files = file.list();
            if (files == null) {
                return 0;
            }
            for (int k = 0; k < files.length; ++k) {
                try {
                    file = new File(dir, files[k]);
                    String name = file.getPath().toLowerCase();
                    if (!name.endsWith(".ttf") && !name.endsWith(".otf") && !name.endsWith(".afm") && !name.endsWith(".ttc")) continue;
                    FontFactory.register(file.getPath(), null);
                    ++count;
                    continue;
                }
                catch (Exception name) {
                    // empty catch block
                }
            }
        }
        catch (Exception file) {
            // empty catch block
        }
        return count;
    }

    public static int registerDirectories() {
        int count = 0;
        count+=FontFactory.registerDirectory("c:/windows/fonts");
        count+=FontFactory.registerDirectory("c:/winnt/fonts");
        count+=FontFactory.registerDirectory("d:/windows/fonts");
        count+=FontFactory.registerDirectory("d:/winnt/fonts");
        count+=FontFactory.registerDirectory("/usr/X/lib/X11/fonts/TrueType");
        count+=FontFactory.registerDirectory("/usr/openwin/lib/X11/fonts/TrueType");
        count+=FontFactory.registerDirectory("/usr/share/fonts/default/TrueType");
        return count+=FontFactory.registerDirectory("/usr/X11R6/lib/X11/fonts/ttf");
    }

    public static Set getRegisteredFonts() {
        return Chunk.getKeySet(trueTypeFonts);
    }

    public static Set getRegisteredFamilies() {
        return Chunk.getKeySet(fontFamilies);
    }

    public static boolean contains(String fontname) {
        return trueTypeFonts.containsKey(fontname);
    }

    public static boolean isRegistered(String fontname) {
        Enumeration e = trueTypeFonts.propertyNames();
        while (e.hasMoreElements()) {
            String tmp = (String)e.nextElement();
            if (!fontname.equalsIgnoreCase(tmp)) continue;
            return true;
        }
        return false;
    }
}

