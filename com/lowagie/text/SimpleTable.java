/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.SimpleCell;
import com.lowagie.text.Table;
import com.lowagie.text.TextElementArray;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPTableEvent;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

public class SimpleTable
extends Rectangle
implements PdfPTableEvent,
Element,
TextElementArray {
    private ArrayList content = new ArrayList();
    private float width = 0.0f;
    private float widthpercentage = 0.0f;
    private float cellspacing;
    private float cellpadding;
    private int alignment;

    public SimpleTable() {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.setBorder(15);
        this.setBorderWidth(2.0f);
    }

    public void addElement(SimpleCell element) throws BadElementException {
        if (!element.isCellgroup()) {
            throw new BadElementException("You can't add cells to a table directly, add them to a row first.");
        }
        this.content.add(element);
    }

    public Table createTable() throws BadElementException {
        int i;
        SimpleCell cell;
        if (this.content.size() == 0) {
            throw new BadElementException("Trying to create a table without rows.");
        }
        SimpleCell row = (SimpleCell)this.content.get(0);
        int columns = 0;
        Iterator i2 = row.getContent().iterator();
        while (i2.hasNext()) {
            cell = (SimpleCell)i2.next();
            columns+=cell.getColspan();
        }
        float[] widths = new float[columns];
        float[] widthpercentages = new float[columns];
        Table table = new Table(columns);
        table.setAlignment(this.alignment);
        table.setSpacing(this.cellspacing);
        table.setPadding(this.cellpadding);
        table.cloneNonPositionParameters(this);
        Iterator rows = this.content.iterator();
        while (rows.hasNext()) {
            row = (SimpleCell)rows.next();
            int pos = 0;
            Iterator cells = row.getContent().iterator();
            while (cells.hasNext()) {
                cell = (SimpleCell)cells.next();
                table.addCell(cell.createCell(row));
                if (cell.getColspan() == 1) {
                    if (cell.getWidth() > 0.0f) {
                        widths[pos] = cell.getWidth();
                    }
                    if (cell.getWidthpercentage() > 0.0f) {
                        widthpercentages[pos] = cell.getWidthpercentage();
                    }
                }
                pos+=cell.getColspan();
            }
        }
        float sumWidths = 0.0f;
        for (i = 0; i < columns; ++i) {
            if (widths[i] == 0.0f) {
                sumWidths = 0.0f;
                break;
            }
            sumWidths+=widths[i];
        }
        if (sumWidths > 0.0f) {
            table.setAbsWidth(String.valueOf(sumWidths));
            table.setWidths(widths);
        } else {
            for (i = 0; i < columns; ++i) {
                if (widthpercentages[i] == 0.0f) {
                    sumWidths = 0.0f;
                    break;
                }
                sumWidths+=widthpercentages[i];
            }
            if (sumWidths > 0.0f) {
                table.setWidths(widthpercentages);
            }
        }
        if (this.width > 0.0f) {
            table.setAbsWidth(String.valueOf(this.width));
        }
        if (this.widthpercentage > 0.0f) {
            table.setWidth(this.widthpercentage);
        }
        return table;
    }

    public PdfPTable createPdfPTable() throws DocumentException {
        int i;
        SimpleCell cell;
        if (this.content.size() == 0) {
            throw new BadElementException("Trying to create a table without rows.");
        }
        SimpleCell row = (SimpleCell)this.content.get(0);
        int columns = 0;
        Iterator i2 = row.getContent().iterator();
        while (i2.hasNext()) {
            cell = (SimpleCell)i2.next();
            columns+=cell.getColspan();
        }
        float[] widths = new float[columns];
        float[] widthpercentages = new float[columns];
        PdfPTable table = new PdfPTable(columns);
        table.setTableEvent(this);
        table.setHorizontalAlignment(this.alignment);
        Iterator rows = this.content.iterator();
        while (rows.hasNext()) {
            row = (SimpleCell)rows.next();
            int pos = 0;
            Iterator cells = row.getContent().iterator();
            while (cells.hasNext()) {
                cell = (SimpleCell)cells.next();
                if (Float.isNaN(cell.getSpacing())) {
                    cell.setSpacing(this.cellspacing);
                }
                cell.setPadding(this.cellpadding);
                table.addCell(cell.createPdfPCell(row));
                if (cell.getColspan() == 1) {
                    if (cell.getWidth() > 0.0f) {
                        widths[pos] = cell.getWidth();
                    }
                    if (cell.getWidthpercentage() > 0.0f) {
                        widthpercentages[pos] = cell.getWidthpercentage();
                    }
                }
                pos+=cell.getColspan();
            }
        }
        float sumWidths = 0.0f;
        for (i = 0; i < columns; ++i) {
            if (widths[i] == 0.0f) {
                sumWidths = 0.0f;
                break;
            }
            sumWidths+=widths[i];
        }
        if (sumWidths > 0.0f) {
            table.setTotalWidth(sumWidths);
            table.setWidths(widths);
        } else {
            for (i = 0; i < columns; ++i) {
                if (widthpercentages[i] == 0.0f) {
                    sumWidths = 0.0f;
                    break;
                }
                sumWidths+=widthpercentages[i];
            }
            if (sumWidths > 0.0f) {
                table.setWidths(widthpercentages);
            }
        }
        if (this.width > 0.0f) {
            table.setTotalWidth(this.width);
        }
        if (this.widthpercentage > 0.0f) {
            table.setWidthPercentage(this.widthpercentage);
        }
        return table;
    }

    public static SimpleTable getDimensionlessInstance(Rectangle rectangle, float spacing) {
        SimpleTable event = new SimpleTable();
        event.cloneNonPositionParameters(rectangle);
        event.setCellspacing(spacing);
        return event;
    }

    public void tableLayout(PdfPTable table, float[][] widths, float[] heights, int headerRows, int rowStart, PdfContentByte[] canvases) {
        float[] width = widths[0];
        Rectangle rect = new Rectangle(width[0], heights[heights.length - 1], width[width.length - 1], heights[0]);
        rect.cloneNonPositionParameters(this);
        canvases[1].rectangle(rect);
        rect.setBackgroundColor(null);
        canvases[2].rectangle(rect);
    }

    public float getCellpadding() {
        return this.cellpadding;
    }

    public void setCellpadding(float cellpadding) {
        this.cellpadding = cellpadding;
    }

    public float getCellspacing() {
        return this.cellspacing;
    }

    public void setCellspacing(float cellspacing) {
        this.cellspacing = cellspacing;
    }

    public int getAlignment() {
        return this.alignment;
    }

    public void setAlignment(int alignment) {
        this.alignment = alignment;
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getWidthpercentage() {
        return this.widthpercentage;
    }

    public void setWidthpercentage(float widthpercentage) {
        this.widthpercentage = widthpercentage;
    }

    public int type() {
        return 22;
    }

    public boolean add(Object o) {
        try {
            this.addElement((SimpleCell)o);
            return true;
        }
        catch (ClassCastException e) {
            return false;
        }
        catch (BadElementException e) {
            throw new ExceptionConverter(e);
        }
    }
}

