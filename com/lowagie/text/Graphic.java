/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Graphic
extends PdfContentByte
implements Element {
    public static final String HORIZONTAL_LINE = "HORIZONTAL";
    public static final String BORDER = "BORDER";
    private HashMap attributes;

    public Graphic() {
        super(null);
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 39;
    }

    public ArrayList getChunks() {
        return new ArrayList();
    }

    public void setHorizontalLine(float linewidth, float percentage) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        this.attributes.put("HORIZONTAL", new Object[]{new Float(linewidth), new Float(percentage), Color.black, new Integer(1)});
    }

    public void setHorizontalLine(float linewidth, float percentage, int align) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        this.attributes.put("HORIZONTAL", new Object[]{new Float(linewidth), new Float(percentage), Color.black, new Integer(align)});
    }

    public void setHorizontalLine(float linewidth, float percentage, Color color) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        this.attributes.put("HORIZONTAL", new Object[]{new Float(linewidth), new Float(percentage), color, new Integer(1)});
    }

    public void setHorizontalLine(float linewidth, float percentage, Color color, int align) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        this.attributes.put("HORIZONTAL", new Object[]{new Float(linewidth), new Float(percentage), color, new Integer(align)});
    }

    public void drawHorizontalLine(float lineWidth, Color color, float x1, float x2, float y) {
        this.setLineWidth(lineWidth);
        this.setColorStroke(color);
        this.moveTo(x1, y);
        this.lineTo(x2, y);
        this.stroke();
        this.resetRGBColorStroke();
    }

    public void setBorder(float linewidth, float extraSpace) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        this.attributes.put("BORDER", new Object[]{new Float(linewidth), new Float(extraSpace), new Color(0, 0, 0)});
    }

    public void setBorder(float linewidth, float extraSpace, Color color) {
        if (this.attributes == null) {
            this.attributes = new HashMap();
        }
        this.attributes.put("BORDER", new Object[]{new Float(linewidth), new Float(extraSpace), color});
    }

    public void drawBorder(float lineWidth, Color color, float llx, float lly, float urx, float ury) {
        this.setLineWidth(lineWidth);
        this.setColorStroke(color);
        this.rectangle(llx, lly, urx - llx, ury - lly);
        this.stroke();
        this.resetRGBColorStroke();
    }

    public void processAttributes(float llx, float lly, float urx, float ury, float y) {
        if (this.attributes == null) {
            return;
        }
        Iterator i = this.attributes.keySet().iterator();
        while (i.hasNext()) {
            String attribute = (String)i.next();
            Object[] o = (Object[])this.attributes.get(attribute);
            if ("HORIZONTAL".equals(attribute)) {
                float s;
                float p = ((Float)o[1]).floatValue();
                float w = p < 0.0f ? - p : (urx - llx) * p / 100.0f;
                int align = (Integer)o[3];
                switch (align) {
                    case 0: {
                        s = 0.0f;
                        break;
                    }
                    case 2: {
                        s = urx - llx - w;
                        break;
                    }
                    default: {
                        s = (urx - llx - w) / 2.0f;
                    }
                }
                this.drawHorizontalLine(((Float)o[0]).floatValue(), (Color)o[2], s + llx, s + w + llx, y);
            }
            if (!"BORDER".equals(attribute)) continue;
            float extra = ((Float)o[1]).floatValue();
            this.drawBorder(((Float)o[0]).floatValue(), (Color)o[2], llx - extra, lly - extra, urx + extra, ury + extra);
        }
    }
}

