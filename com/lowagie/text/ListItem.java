/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.MarkupAttributes;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.TextElementArray;
import com.lowagie.text.markup.MarkupParser;
import java.util.Properties;

public class ListItem
extends Paragraph
implements TextElementArray,
MarkupAttributes {
    private Chunk symbol;

    public ListItem() {
    }

    public ListItem(float leading) {
        super(leading);
    }

    public ListItem(Chunk chunk) {
        super(chunk);
    }

    public ListItem(String string) {
        super(string);
    }

    public ListItem(String string, Font font) {
        super(string, font);
    }

    public ListItem(float leading, Chunk chunk) {
        super(leading, chunk);
    }

    public ListItem(float leading, String string) {
        super(leading, string);
    }

    public ListItem(float leading, String string, Font font) {
        super(leading, string, font);
    }

    public ListItem(Phrase phrase) {
        super(phrase);
    }

    public ListItem(Properties attributes) {
        super("", FontFactory.getFont(attributes));
        String value = (String)attributes.remove("itext");
        if (value != null) {
            this.add(new Chunk(value));
        }
        if ((value = (String)attributes.remove("leading")) != null) {
            this.setLeading(Float.valueOf(String.valueOf(value) + "f").floatValue());
        } else {
            value = (String)attributes.remove("line-height");
            if (value != null) {
                this.setLeading(MarkupParser.parseLength(value));
            }
        }
        value = (String)attributes.remove("indentationleft");
        if (value != null) {
            this.setIndentationLeft(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("indentationright")) != null) {
            this.setIndentationRight(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("align")) != null) {
            this.setAlignment(value);
        }
        if (attributes.size() > 0) {
            this.setMarkupAttributes(attributes);
        }
    }

    public int type() {
        return 15;
    }

    public void setListSymbol(Chunk symbol) {
        this.symbol = symbol;
        if (this.symbol.font().isStandardFont()) {
            this.symbol.setFont(this.font);
        }
    }

    public Chunk listSymbol() {
        return this.symbol;
    }

    public static boolean isTag(String tag) {
        return "listitem".equals(tag);
    }
}

