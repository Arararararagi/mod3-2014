/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Rectangle;

public class PageSize {
    public static final Rectangle LETTER = new Rectangle(612.0f, 792.0f);
    public static final Rectangle NOTE = new Rectangle(540.0f, 720.0f);
    public static final Rectangle LEGAL = new Rectangle(612.0f, 1008.0f);
    public static final Rectangle A0 = new Rectangle(2384.0f, 3370.0f);
    public static final Rectangle A1 = new Rectangle(1684.0f, 2384.0f);
    public static final Rectangle A2 = new Rectangle(1190.0f, 1684.0f);
    public static final Rectangle A3 = new Rectangle(842.0f, 1190.0f);
    public static final Rectangle A4 = new Rectangle(595.0f, 842.0f);
    public static final Rectangle A5 = new Rectangle(421.0f, 595.0f);
    public static final Rectangle A6 = new Rectangle(297.0f, 421.0f);
    public static final Rectangle A7 = new Rectangle(210.0f, 297.0f);
    public static final Rectangle A8 = new Rectangle(148.0f, 210.0f);
    public static final Rectangle A9 = new Rectangle(105.0f, 148.0f);
    public static final Rectangle A10 = new Rectangle(74.0f, 105.0f);
    public static final Rectangle B0 = new Rectangle(2836.0f, 4008.0f);
    public static final Rectangle B1 = new Rectangle(2004.0f, 2836.0f);
    public static final Rectangle B2 = new Rectangle(1418.0f, 2004.0f);
    public static final Rectangle B3 = new Rectangle(1002.0f, 1418.0f);
    public static final Rectangle B4 = new Rectangle(709.0f, 1002.0f);
    public static final Rectangle B5 = new Rectangle(501.0f, 709.0f);
    public static final Rectangle ARCH_E = new Rectangle(2592.0f, 3456.0f);
    public static final Rectangle ARCH_D = new Rectangle(1728.0f, 2592.0f);
    public static final Rectangle ARCH_C = new Rectangle(1296.0f, 1728.0f);
    public static final Rectangle ARCH_B = new Rectangle(864.0f, 1296.0f);
    public static final Rectangle ARCH_A = new Rectangle(648.0f, 864.0f);
    public static final Rectangle FLSA = new Rectangle(612.0f, 936.0f);
    public static final Rectangle FLSE = new Rectangle(612.0f, 936.0f);
    public static final Rectangle HALFLETTER = new Rectangle(396.0f, 612.0f);
    public static final Rectangle _11X17 = new Rectangle(792.0f, 1224.0f);
    public static final Rectangle LEDGER = new Rectangle(1224.0f, 792.0f);
}

