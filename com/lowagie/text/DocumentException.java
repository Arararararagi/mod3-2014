/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import java.io.PrintStream;
import java.io.PrintWriter;

public class DocumentException
extends Exception {
    private Exception ex;

    public DocumentException(Exception ex) {
        this.ex = ex;
    }

    public DocumentException() {
    }

    public DocumentException(String message) {
        super(message);
    }

    public String getMessage() {
        if (this.ex == null) {
            return super.getMessage();
        }
        return this.ex.getMessage();
    }

    public String getLocalizedMessage() {
        if (this.ex == null) {
            return super.getLocalizedMessage();
        }
        return this.ex.getLocalizedMessage();
    }

    public String toString() {
        if (this.ex == null) {
            return super.toString();
        }
        return String.valueOf(DocumentException.split(this.getClass().getName())) + ": " + this.ex;
    }

    public void printStackTrace() {
        this.printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream s) {
        if (this.ex == null) {
            super.printStackTrace(s);
        } else {
            PrintStream printStream = s;
            synchronized (printStream) {
                s.print(String.valueOf(DocumentException.split(this.getClass().getName())) + ": ");
                this.ex.printStackTrace(s);
            }
        }
    }

    public void printStackTrace(PrintWriter s) {
        if (this.ex == null) {
            super.printStackTrace(s);
        } else {
            PrintWriter printWriter = s;
            synchronized (printWriter) {
                s.print(String.valueOf(DocumentException.split(this.getClass().getName())) + ": ");
                this.ex.printStackTrace(s);
            }
        }
    }

    private static String split(String s) {
        int i = s.lastIndexOf(46);
        if (i < 0) {
            return s;
        }
        return s.substring(i + 1);
    }
}

