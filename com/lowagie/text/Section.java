/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.TextElementArray;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

public class Section
extends ArrayList
implements TextElementArray {
    protected Paragraph title;
    protected int numberDepth;
    protected float indentationLeft;
    protected float indentationRight;
    protected float sectionIndent;
    protected int subsections = 0;
    protected ArrayList numbers = null;
    protected boolean bookmarkOpen = true;
    protected String bookmarkTitle;

    protected Section() {
        this.title = new Paragraph();
        this.numberDepth = 1;
    }

    Section(Paragraph title, int numberDepth) {
        this.numberDepth = numberDepth;
        this.title = title;
    }

    private void setNumbers(int number, ArrayList numbers) {
        this.numbers = new ArrayList();
        this.numbers.add(new Integer(number));
        this.numbers.addAll(numbers);
    }

    public boolean process(ElementListener listener) {
        try {
            Iterator i = this.iterator();
            while (i.hasNext()) {
                listener.add((Element)i.next());
            }
            return true;
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public int type() {
        return 13;
    }

    public ArrayList getChunks() {
        ArrayList tmp = new ArrayList();
        Iterator i = this.iterator();
        while (i.hasNext()) {
            tmp.addAll(((Element)i.next()).getChunks());
        }
        return tmp;
    }

    public void add(int index, Object o) {
        block3 : {
            try {
                Element element = (Element)o;
                if (element.type() == 12 || element.type() == 14 || element.type() == 10 || element.type() == 11 || element.type() == 17 || element.type() == 29 || element.type() == 22 || element.type() == 23 || element.type() == 35 || element.type() == 32 || element.type() == 34) {
                    super.add(index, element);
                    break block3;
                }
                throw new ClassCastException(String.valueOf(element.type()));
            }
            catch (ClassCastException cce) {
                throw new ClassCastException("Insertion of illegal Element: " + cce.getMessage());
            }
        }
    }

    public boolean add(Object o) {
        try {
            Element element = (Element)o;
            if (element.type() == 12 || element.type() == 14 || element.type() == 10 || element.type() == 11 || element.type() == 17 || element.type() == 29 || element.type() == 22 || element.type() == 35 || element.type() == 23 || element.type() == 32 || element.type() == 34) {
                return super.add(o);
            }
            if (element.type() == 13) {
                Section section = (Section)o;
                section.setNumbers(++this.subsections, this.numbers);
                return super.add(section);
            }
            throw new ClassCastException(String.valueOf(element.type()));
        }
        catch (ClassCastException cce) {
            throw new ClassCastException("Insertion of illegal Element: " + cce.getMessage());
        }
    }

    public boolean addAll(Collection collection) {
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            this.add(iterator.next());
        }
        return true;
    }

    public Section addSection(float indentation, Paragraph title, int numberDepth) {
        Section section = new Section(title, numberDepth);
        section.setIndentation(indentation);
        this.add(section);
        return section;
    }

    public Section addSection(float indentation, Paragraph title) {
        Section section = new Section(title, 1);
        section.setIndentation(indentation);
        this.add(section);
        return section;
    }

    public Section addSection(Paragraph title, int numberDepth) {
        Section section = new Section(title, numberDepth);
        this.add(section);
        return section;
    }

    public Section addSection(Paragraph title) {
        Section section = new Section(title, 1);
        this.add(section);
        return section;
    }

    public Section addSection(float indentation, String title, int numberDepth) {
        Section section = new Section(new Paragraph(title), numberDepth);
        section.setIndentation(indentation);
        this.add(section);
        return section;
    }

    public Section addSection(String title, int numberDepth) {
        Section section = new Section(new Paragraph(title), numberDepth);
        this.add(section);
        return section;
    }

    public Section addSection(float indentation, String title) {
        Section section = new Section(new Paragraph(title), 1);
        section.setIndentation(indentation);
        this.add(section);
        return section;
    }

    public Section addSection(String title) {
        Section section = new Section(new Paragraph(title), 1);
        this.add(section);
        return section;
    }

    public Section addSection(Properties attributes) {
        Section section = new Section(new Paragraph(""), 1);
        String value = (String)attributes.remove("number");
        if (value != null) {
            this.subsections = Integer.parseInt(value) - 1;
        }
        section.set(attributes);
        this.add(section);
        return section;
    }

    public void set(Properties attributes) {
        String value = (String)attributes.remove("numberdepth");
        if (value != null) {
            this.setNumberDepth(Integer.parseInt(value));
        }
        if ((value = (String)attributes.remove("indent")) != null) {
            this.setIndentation(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("indentationleft")) != null) {
            this.setIndentationLeft(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
        if ((value = (String)attributes.remove("indentationright")) != null) {
            this.setIndentationRight(Float.valueOf(String.valueOf(value) + "f").floatValue());
        }
    }

    public void setTitle(Paragraph title) {
        this.title = title;
    }

    public void setNumberDepth(int numberDepth) {
        this.numberDepth = numberDepth;
    }

    public void setIndentationLeft(float indentation) {
        this.indentationLeft = indentation;
    }

    public void setIndentationRight(float indentation) {
        this.indentationRight = indentation;
    }

    public void setIndentation(float indentation) {
        this.sectionIndent = indentation;
    }

    public boolean isChapter() {
        if (this.type() == 16) {
            return true;
        }
        return false;
    }

    public boolean isSection() {
        if (this.type() == 13) {
            return true;
        }
        return false;
    }

    public int numberDepth() {
        return this.numberDepth;
    }

    public float indentationLeft() {
        return this.indentationLeft;
    }

    public float indentationRight() {
        return this.indentationRight;
    }

    public float indentation() {
        return this.sectionIndent;
    }

    public int depth() {
        return this.numbers.size();
    }

    public Paragraph title() {
        if (this.title == null) {
            return null;
        }
        int depth = Math.min(this.numbers.size(), this.numberDepth);
        if (depth < 1) {
            return this.title;
        }
        StringBuffer buf = new StringBuffer(" ");
        for (int i = 0; i < depth; ++i) {
            buf.insert(0, ".");
            buf.insert(0, (Integer)this.numbers.get(i));
        }
        Paragraph result = new Paragraph(this.title);
        result.setMarkupAttributes(this.title.getMarkupAttributes());
        result.add(0, new Chunk(buf.toString(), this.title.font()));
        return result;
    }

    public static boolean isTitle(String tag) {
        return "title".equals(tag);
    }

    public static boolean isTag(String tag) {
        return "section".equals(tag);
    }

    public boolean isBookmarkOpen() {
        return this.bookmarkOpen;
    }

    public void setBookmarkOpen(boolean bookmarkOpen) {
        this.bookmarkOpen = bookmarkOpen;
    }

    public Paragraph getBookmarkTitle() {
        if (this.bookmarkTitle == null) {
            return this.title();
        }
        return new Paragraph(this.bookmarkTitle);
    }

    public void setBookmarkTitle(String bookmarkTitle) {
        this.bookmarkTitle = bookmarkTitle;
    }
}

