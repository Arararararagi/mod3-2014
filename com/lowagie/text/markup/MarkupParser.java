/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.markup;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.ListItem;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.SimpleCell;
import com.lowagie.text.SimpleTable;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

public class MarkupParser
extends HashMap {
    protected HashMap stylecache = new HashMap();
    protected HashMap fontcache = new HashMap();

    public MarkupParser(String file) {
        try {
            String line;
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            StringBuffer buf = new StringBuffer();
            while ((line = br.readLine()) != null) {
                buf.append(line.trim());
            }
            String string = buf.toString();
            string = MarkupParser.removeComment(string, "/*", "*/");
            StringTokenizer tokenizer = new StringTokenizer(string, "}");
            while (tokenizer.hasMoreTokens()) {
                String tmp = tokenizer.nextToken();
                int pos = tmp.indexOf("{");
                if (pos <= 0) continue;
                String selector = tmp.substring(0, pos).trim();
                String attributes = tmp.substring(pos + 1).trim();
                if (attributes.endsWith("}")) {
                    attributes = attributes.substring(0, attributes.length() - 1);
                }
                this.put(selector, MarkupParser.parseAttributes(attributes));
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String removeComment(String string, String startComment, String endComment) {
        StringBuffer result = new StringBuffer();
        int pos = 0;
        int end = endComment.length();
        int start = string.indexOf(startComment, pos);
        while (start > -1) {
            result.append(string.substring(pos, start));
            pos = string.indexOf(endComment, start) + end;
            start = string.indexOf(startComment, pos);
        }
        result.append(string.substring(pos));
        return result.toString();
    }

    public static Properties parseAttributes(String string) {
        Properties result = new Properties();
        if (string == null) {
            return result;
        }
        StringTokenizer keyValuePairs = new StringTokenizer(string, ";");
        while (keyValuePairs.hasMoreTokens()) {
            StringTokenizer keyValuePair = new StringTokenizer(keyValuePairs.nextToken(), ":");
            if (!keyValuePair.hasMoreTokens()) continue;
            String key = keyValuePair.nextToken().trim();
            if (!keyValuePair.hasMoreTokens()) continue;
            String value = keyValuePair.nextToken().trim();
            if (value.startsWith("\"")) {
                value = value.substring(1);
            }
            if (value.endsWith("\"")) {
                value = value.substring(0, value.length() - 1);
            }
            result.setProperty(key, value);
        }
        return result;
    }

    public static float parseLength(String string) {
        int pos = 0;
        int length = string.length();
        boolean ok = true;
        while (ok && pos < length) {
            switch (string.charAt(pos)) {
                case '+': 
                case '-': 
                case '.': 
                case '0': 
                case '1': 
                case '2': 
                case '3': 
                case '4': 
                case '5': 
                case '6': 
                case '7': 
                case '8': 
                case '9': {
                    ++pos;
                    break;
                }
                default: {
                    ok = false;
                }
            }
        }
        if (pos == 0) {
            return 0.0f;
        }
        if (pos == length) {
            return Float.valueOf(String.valueOf(string) + "f").floatValue();
        }
        float f = Float.valueOf(String.valueOf(string.substring(0, pos)) + "f").floatValue();
        if ((string = string.substring(pos)).startsWith("in")) {
            return f * 72.0f;
        }
        if (string.startsWith("cm")) {
            return f / 2.54f * 72.0f;
        }
        if (string.startsWith("mm")) {
            return f / 25.4f * 72.0f;
        }
        if (string.startsWith("pc")) {
            return f * 12.0f;
        }
        return f;
    }

    public static Color decodeColor(String color) {
        int red = 0;
        int green = 0;
        int blue = 0;
        try {
            red = Integer.parseInt(color.substring(1, 3), 16);
            green = Integer.parseInt(color.substring(3, 5), 16);
            blue = Integer.parseInt(color.substring(5), 16);
        }
        catch (Exception var4_4) {
            // empty catch block
        }
        return new Color(red, green, blue);
    }

    private String getKey(Properties attributes) {
        String tag = attributes.getProperty("tag");
        String id = attributes.getProperty("id");
        String cl = attributes.getProperty("class");
        id = id == null ? "" : "#" + id;
        cl = cl == null ? "" : "." + cl;
        String key = String.valueOf(tag) + id + cl;
        if (!(this.stylecache.containsKey(key) || key.length() <= 0)) {
            Properties props = new Properties();
            Properties tagprops = (Properties)this.get(tag);
            Properties idprops = (Properties)this.get(id);
            Properties clprops = (Properties)this.get(cl);
            Properties tagidprops = (Properties)this.get(String.valueOf(tag) + id);
            Properties tagclprops = (Properties)this.get(String.valueOf(tag) + cl);
            if (tagprops != null) {
                props.putAll(tagprops);
            }
            if (idprops != null) {
                props.putAll(idprops);
            }
            if (clprops != null) {
                props.putAll(clprops);
            }
            if (tagidprops != null) {
                props.putAll(tagidprops);
            }
            if (tagclprops != null) {
                props.putAll(tagclprops);
            }
            this.stylecache.put(key, props);
        }
        return key;
    }

    public boolean getPageBreakBefore(Properties attributes) {
        String key = this.getKey(attributes);
        Properties styleattributes = (Properties)this.stylecache.get(key);
        if (styleattributes != null && "always".equals(styleattributes.getProperty("page-break-before"))) {
            return true;
        }
        return false;
    }

    public boolean getPageBreakAfter(Properties attributes) {
        String key = this.getKey(attributes);
        Properties styleattributes = (Properties)this.stylecache.get(key);
        if (styleattributes != null && "always".equals(styleattributes.getProperty("page-break-after"))) {
            return true;
        }
        return false;
    }

    public Element getObject(Properties attributes) {
        String key = this.getKey(attributes);
        Properties styleattributes = (Properties)this.stylecache.get(key);
        if (styleattributes != null && "hidden".equals(styleattributes.get("visibility"))) {
            return null;
        }
        String display = styleattributes.getProperty("display");
        Element element = null;
        if ("inline".equals(display)) {
            element = this.retrievePhrase(this.getFont(attributes), styleattributes);
        } else if ("block".equals(display)) {
            element = this.retrieveParagraph(this.getFont(attributes), styleattributes);
        } else if ("list-item".equals(display)) {
            element = this.retrieveListItem(this.getFont(attributes), styleattributes);
        } else if ("table-cell".equals(display)) {
            element = this.retrieveTableCell(attributes, styleattributes);
        } else if ("table-row".equals(display)) {
            element = this.retrieveTableRow(attributes, styleattributes);
        } else if ("table".equals(display)) {
            element = this.retrieveTable(attributes, styleattributes);
        }
        return element;
    }

    public Font getFont(Properties attributes) {
        String key = this.getKey(attributes);
        Font f = (Font)this.fontcache.get(key);
        if (f != null) {
            return f;
        }
        Properties styleattributes = (Properties)this.stylecache.get(key);
        f = this.retrieveFont(styleattributes);
        this.fontcache.put(key, f);
        return f;
    }

    public Rectangle getRectangle(Properties attrs) {
        String width = null;
        String height = null;
        String key = this.getKey(attrs);
        Properties styleattributes = (Properties)this.stylecache.get(key);
        if (styleattributes != null) {
            width = styleattributes.getProperty("width");
            height = styleattributes.getProperty("height");
        }
        if (width == null) {
            width = attrs.getProperty("width");
        }
        if (height == null) {
            height = attrs.getProperty("height");
        }
        if (width == null || height == null) {
            return null;
        }
        return new Rectangle(MarkupParser.parseLength(width), MarkupParser.parseLength(height));
    }

    public Element retrievePhrase(Font font, Properties styleattributes) {
        Phrase p = new Phrase("", font);
        if (styleattributes == null) {
            return p;
        }
        String leading = styleattributes.getProperty("line-height");
        if (leading != null) {
            if (leading.endsWith("%")) {
                p.setLeading(p.font().size() * (MarkupParser.parseLength(leading) / 100.0f));
            } else {
                p.setLeading(MarkupParser.parseLength(leading));
            }
        }
        return p;
    }

    public Element retrieveParagraph(Font font, Properties styleattributes) {
        float f;
        String align;
        Paragraph p = new Paragraph((Phrase)this.retrievePhrase(font, styleattributes));
        if (styleattributes == null) {
            return p;
        }
        String margin = styleattributes.getProperty("margin");
        if (margin != null) {
            f = MarkupParser.parseLength(margin);
            p.setIndentationLeft(f);
            p.setIndentationRight(f);
            p.setSpacingBefore(f);
            p.setSpacingAfter(f);
        }
        if ((margin = styleattributes.getProperty("margin-left")) != null) {
            f = MarkupParser.parseLength(margin);
            p.setIndentationLeft(f);
        }
        if ((margin = styleattributes.getProperty("margin-right")) != null) {
            f = MarkupParser.parseLength(margin);
            p.setIndentationRight(f);
        }
        if ((margin = styleattributes.getProperty("margin-top")) != null) {
            f = MarkupParser.parseLength(margin);
            p.setSpacingBefore(f);
        }
        if ((margin = styleattributes.getProperty("margin-bottom")) != null) {
            f = MarkupParser.parseLength(margin);
            p.setSpacingAfter(f);
        }
        if ("left".equals(align = styleattributes.getProperty("text-align"))) {
            p.setAlignment(0);
        } else if ("right".equals(align)) {
            p.setAlignment(2);
        } else if ("center".equals(align)) {
            p.setAlignment(1);
        } else if ("justify".equals(align)) {
            p.setAlignment(3);
        }
        return p;
    }

    private Element retrieveTable(Properties attributes, Properties styleattributes) {
        SimpleTable table = new SimpleTable();
        return table;
    }

    private Element retrieveTableRow(Properties attributes, Properties styleattributes) {
        SimpleCell row = new SimpleCell(true);
        return row;
    }

    private Element retrieveTableCell(Properties attributes, Properties styleattributes) {
        SimpleCell cell = (SimpleCell)this.retrieveTableRow(attributes, styleattributes);
        cell.setCellgroup(false);
        return cell;
    }

    private Element retrieveListItem(Font font, Properties styleattributes) {
        ListItem li = new ListItem();
        return li;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public Font retrieveFont(Properties styleAttributes) {
        fontname = null;
        encoding = FontFactory.defaultEncoding;
        embedded = FontFactory.defaultEmbedding;
        size = -1.0f;
        style = 0;
        color = null;
        value = (String)styleAttributes.get("font-family");
        if (value == null) ** GOTO lbl18
        if (value.indexOf(",") != -1) ** GOTO lbl17
        fontname = value.trim();
        ** GOTO lbl18
lbl-1000: // 1 sources:
        {
            tmp = value.substring(0, value.indexOf(",")).trim();
            if (FontFactory.isRegistered(tmp)) {
                fontname = tmp;
                break;
            }
            value = value.substring(value.indexOf(",") + 1);
lbl17: // 2 sources:
            ** while (value.indexOf((String)",") != -1)
        }
lbl18: // 4 sources:
        if ((value = (String)styleAttributes.get("font-size")) != null) {
            size = MarkupParser.parseLength(value);
        }
        if ((value = (String)styleAttributes.get("font-weight")) != null) {
            style|=Font.getStyleValue(value);
        }
        if ((value = (String)styleAttributes.get("font-style")) != null) {
            style|=Font.getStyleValue(value);
        }
        if ((value = (String)styleAttributes.get("color")) == null) return FontFactory.getFont(fontname, encoding, embedded, size, style, color);
        color = MarkupParser.decodeColor(value);
        return FontFactory.getFont(fontname, encoding, embedded, size, style, color);
    }
}

