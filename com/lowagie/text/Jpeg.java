/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class Jpeg
extends Image
implements Element {
    public static final int NOT_A_MARKER = -1;
    public static final int VALID_MARKER = 0;
    public static final int[] VALID_MARKERS = new int[]{192, 193, 194};
    public static final int UNSUPPORTED_MARKER = 1;
    public static final int[] UNSUPPORTED_MARKERS = new int[]{195, 197, 198, 199, 200, 201, 202, 203, 205, 206, 207};
    public static final int NOPARAM_MARKER = 2;
    public static final int[] NOPARAM_MARKERS = new int[]{208, 209, 210, 211, 212, 213, 214, 215, 216, 1};
    public static final int M_APP0 = 224;
    public static final int M_APPE = 238;
    public static final byte[] JFIF_ID;

    static {
        byte[] arrby = new byte[5];
        arrby[0] = 74;
        arrby[1] = 70;
        arrby[2] = 73;
        arrby[3] = 70;
        JFIF_ID = arrby;
    }

    Jpeg(Image image) {
        super(image);
    }

    public Jpeg(URL url) throws BadElementException, IOException {
        super(url);
        this.processParameters();
    }

    public Jpeg(URL url, float width, float height) throws BadElementException, IOException {
        this(url);
        this.scaledWidth = width;
        this.scaledHeight = height;
    }

    public Jpeg(String filename) throws BadElementException, MalformedURLException, IOException {
        this(Image.toURL(filename));
    }

    public Jpeg(String filename, float width, float height) throws BadElementException, MalformedURLException, IOException {
        this(Image.toURL(filename), width, height);
    }

    public Jpeg(byte[] img) throws BadElementException, IOException {
        super((URL)null);
        this.rawData = img;
        this.originalData = img;
        this.processParameters();
    }

    public Jpeg(byte[] img, float width, float height) throws BadElementException, IOException {
        this(img);
        this.scaledWidth = width;
        this.scaledHeight = height;
    }

    private static final int getShort(InputStream is) throws IOException {
        return (is.read() << 8) + is.read();
    }

    private static final int marker(int marker) {
        int i;
        for (i = 0; i < VALID_MARKERS.length; ++i) {
            if (marker != VALID_MARKERS[i]) continue;
            return 0;
        }
        for (i = 0; i < NOPARAM_MARKERS.length; ++i) {
            if (marker != NOPARAM_MARKERS[i]) continue;
            return 2;
        }
        for (i = 0; i < UNSUPPORTED_MARKERS.length; ++i) {
            if (marker != UNSUPPORTED_MARKERS[i]) continue;
            return 1;
        }
        return -1;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    private void processParameters() throws BadElementException, IOException {
        block22 : {
            this.type = 32;
            this.originalType = 1;
            is = null;
            try {
                if (this.rawData == null) {
                    is = this.url.openStream();
                    errorID = this.url.toString();
                } else {
                    is = new ByteArrayInputStream(this.rawData);
                    errorID = "Byte array";
                }
                if (is.read() != 255) throw new BadElementException(String.valueOf(errorID) + " is not a valid JPEG-file.");
                if (is.read() != 216) {
                    throw new BadElementException(String.valueOf(errorID) + " is not a valid JPEG-file.");
                }
                firstPass = true;
                do {
                    if ((v = is.read()) < 0) {
                        throw new IOException("Premature EOF while reading JPG.");
                    }
                    if (v != 255) continue;
                    marker = is.read();
                    if (!firstPass || marker != 224) ** GOTO lbl33
                    firstPass = false;
                    len = Jpeg.getShort(is);
                    if (len < 16) {
                        Image.skip(is, len - 2);
                        continue;
                    }
                    bcomp = new byte[Jpeg.JFIF_ID.length];
                    r = is.read(bcomp);
                    if (r != bcomp.length) {
                        throw new BadElementException(String.valueOf(errorID) + " corrupted JFIF marker.");
                    }
                    found = true;
                    k = 0;
                    ** GOTO lbl42
lbl33: // 1 sources:
                    if (marker != 238) ** GOTO lbl64
                    len = Jpeg.getShort(is);
                    byteappe = new byte[len];
                    k = 0;
                    ** GOTO lbl61
lbl-1000: // 1 sources:
                    {
                        if (bcomp[k] != Jpeg.JFIF_ID[k]) {
                            found = false;
                            break;
                        }
                        ++k;
lbl42: // 2 sources:
                        ** while (k < bcomp.length)
                    }
lbl43: // 2 sources:
                    if (!found) {
                        Image.skip(is, len - 2 - bcomp.length);
                        continue;
                    }
                    Image.skip(is, 2);
                    units = is.read();
                    dx = Jpeg.getShort(is);
                    dy = Jpeg.getShort(is);
                    if (units == 1) {
                        this.dpiX = dx;
                        this.dpiY = dy;
                    } else if (units == 2) {
                        this.dpiX = (int)((float)dx * 2.54f + 0.5f);
                        this.dpiY = (int)((float)dy * 2.54f + 0.5f);
                    }
                    Image.skip(is, len - 2 - bcomp.length - 7);
                    continue;
lbl-1000: // 1 sources:
                    {
                        byteappe[k] = (byte)is.read();
                        ++k;
lbl61: // 2 sources:
                        ** while (k < len)
                    }
lbl62: // 1 sources:
                    if (byteappe.length > 12 && (appe = new String(byteappe, 0, 5, "ISO-8859-1")).equals("Adobe")) {
                        this.invert = true;
                    }
lbl64: // 4 sources:
                    firstPass = false;
                    markertype = Jpeg.marker(marker);
                    if (markertype == 0) {
                        Image.skip(is, 2);
                        if (is.read() != 8) {
                            throw new BadElementException(String.valueOf(errorID) + " must have 8 bits per component.");
                        } else {
                            break;
                        }
                    }
                    if (markertype == 1) {
                        throw new BadElementException(String.valueOf(errorID) + ": unsupported JPEG marker: " + marker);
                    }
                    if (markertype == 2) continue;
                    Image.skip(is, Jpeg.getShort(is) - 2);
                } while (true);
                this.scaledHeight = Jpeg.getShort(is);
                this.setTop(this.scaledHeight);
                this.scaledWidth = Jpeg.getShort(is);
                this.setRight(this.scaledWidth);
                this.colorspace = is.read();
                this.bpc = 8;
                var13_17 = null;
                if (is == null) break block22;
            }
            catch (Throwable var14_15) {
                var13_16 = null;
                if (is != null) {
                    is.close();
                }
                this.plainWidth = this.width();
                this.plainHeight = this.height();
                throw var14_15;
            }
            is.close();
        }
        this.plainWidth = this.width();
        this.plainHeight = this.height();
    }
}

