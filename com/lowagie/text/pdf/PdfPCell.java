/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Chunk;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;

public class PdfPCell
extends Rectangle {
    private ColumnText column = new ColumnText(null);
    private int verticalAlignment = 4;
    private float paddingLeft = 2.0f;
    private float paddingRight = 2.0f;
    private float paddingTop = 2.0f;
    private float paddingBottom = 2.0f;
    private float fixedHeight = 0.0f;
    private boolean noWrap = false;
    private PdfPTable table;
    private float minimumHeight;
    private int colspan = 1;
    private Image image;
    private PdfPCellEvent cellEvent;
    private boolean useDescender;
    private boolean useBorderPadding = false;
    protected Phrase phrase;

    public PdfPCell() {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.borderWidth = 0.5f;
        this.border = 15;
        this.column.setLeading(0.0f, 1.0f);
    }

    public PdfPCell(Phrase phrase) {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.borderWidth = 0.5f;
        this.border = 15;
        this.phrase = phrase;
        this.column.addText(this.phrase);
        this.column.setLeading(0.0f, 1.0f);
    }

    public PdfPCell(Image image) {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.borderWidth = 0.5f;
        this.border = 15;
        this.phrase = new Phrase(new Chunk(image, 0.0f, 0.0f));
        this.column.addText(this.phrase);
        this.column.setLeading(0.0f, 1.0f);
        this.setPadding(0.0f);
    }

    public PdfPCell(Image image, boolean fit) {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        if (fit) {
            this.borderWidth = 0.5f;
            this.border = 15;
            this.image = image;
            this.column.setLeading(0.0f, 1.0f);
            this.setPadding(this.borderWidth / 2.0f);
        } else {
            this.borderWidth = 0.5f;
            this.border = 15;
            this.phrase = new Phrase(new Chunk(image, 0.0f, 0.0f));
            this.column.addText(this.phrase);
            this.column.setLeading(0.0f, 1.0f);
            this.setPadding(0.0f);
        }
    }

    public PdfPCell(PdfPTable table) {
        super(0.0f, 0.0f, 0.0f, 0.0f);
        this.borderWidth = 0.5f;
        this.border = 15;
        this.column.setLeading(0.0f, 1.0f);
        this.setPadding(0.0f);
        this.table = table;
        table.setWidthPercentage(100.0f);
        table.setExtendLastRow(true);
        this.column.addElement(table);
    }

    public PdfPCell(PdfPCell cell) {
        super(cell.llx, cell.lly, cell.urx, cell.ury);
        this.cloneNonPositionParameters(cell);
        this.verticalAlignment = cell.verticalAlignment;
        this.paddingLeft = cell.paddingLeft;
        this.paddingRight = cell.paddingRight;
        this.paddingTop = cell.paddingTop;
        this.paddingBottom = cell.paddingBottom;
        this.phrase = cell.phrase;
        this.fixedHeight = cell.fixedHeight;
        this.minimumHeight = cell.minimumHeight;
        this.noWrap = cell.noWrap;
        this.colspan = cell.colspan;
        if (cell.table != null) {
            this.table = new PdfPTable(cell.table);
        }
        this.image = Image.getInstance(cell.image);
        this.cellEvent = cell.cellEvent;
        this.useDescender = cell.useDescender;
        this.column = ColumnText.duplicate(cell.column);
        this.useBorderPadding = cell.useBorderPadding;
    }

    public void addElement(Element element) {
        if (this.table != null) {
            this.table = null;
            this.column.setText(null);
        }
        this.column.addElement(element);
    }

    public Phrase getPhrase() {
        return this.phrase;
    }

    public void setPhrase(Phrase phrase) {
        this.table = null;
        this.image = null;
        this.phrase = phrase;
        this.column.setText(this.phrase);
    }

    public int getHorizontalAlignment() {
        return this.column.getAlignment();
    }

    public void setHorizontalAlignment(int horizontalAlignment) {
        this.column.setAlignment(horizontalAlignment);
    }

    public int getVerticalAlignment() {
        return this.verticalAlignment;
    }

    public void setVerticalAlignment(int verticalAlignment) {
        if (this.table != null) {
            this.table.setExtendLastRow(verticalAlignment == 4);
        }
        this.verticalAlignment = verticalAlignment;
    }

    public float getEffectivePaddingLeft() {
        return this.paddingLeft + (this.isUseBorderPadding() ? this.getBorderWidthLeft() / (this.isUseVariableBorders() ? 1.0f : 2.0f) : 0.0f);
    }

    public float getPaddingLeft() {
        return this.paddingLeft;
    }

    public void setPaddingLeft(float paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    public float getEffectivePaddingRight() {
        return this.paddingRight + (this.isUseBorderPadding() ? this.getBorderWidthRight() / (this.isUseVariableBorders() ? 1.0f : 2.0f) : 0.0f);
    }

    public float getPaddingRight() {
        return this.paddingRight;
    }

    public void setPaddingRight(float paddingRight) {
        this.paddingRight = paddingRight;
    }

    public float getEffectivePaddingTop() {
        return this.paddingTop + (this.isUseBorderPadding() ? this.getBorderWidthTop() / (this.isUseVariableBorders() ? 1.0f : 2.0f) : 0.0f);
    }

    public float getPaddingTop() {
        return this.paddingTop;
    }

    public void setPaddingTop(float paddingTop) {
        this.paddingTop = paddingTop;
    }

    public float getEffectivePaddingBottom() {
        return this.paddingBottom + (this.isUseBorderPadding() ? this.getBorderWidthBottom() / (this.isUseVariableBorders() ? 1.0f : 2.0f) : 0.0f);
    }

    public float getPaddingBottom() {
        return this.paddingBottom;
    }

    public void setPaddingBottom(float paddingBottom) {
        this.paddingBottom = paddingBottom;
    }

    public void setPadding(float padding) {
        this.paddingBottom = padding;
        this.paddingTop = padding;
        this.paddingLeft = padding;
        this.paddingRight = padding;
    }

    public boolean isUseBorderPadding() {
        return this.useBorderPadding;
    }

    public void setUseBorderPadding(boolean use) {
        this.useBorderPadding = use;
    }

    public void setLeading(float fixedLeading, float multipliedLeading) {
        this.column.setLeading(fixedLeading, multipliedLeading);
    }

    public float getLeading() {
        return this.column.getLeading();
    }

    public float getMultipliedLeading() {
        return this.column.getMultipliedLeading();
    }

    public void setIndent(float indent) {
        this.column.setIndent(indent);
    }

    public float getIndent() {
        return this.column.getIndent();
    }

    public float getExtraParagraphSpace() {
        return this.column.getExtraParagraphSpace();
    }

    public void setExtraParagraphSpace(float extraParagraphSpace) {
        this.column.setExtraParagraphSpace(extraParagraphSpace);
    }

    public float getFixedHeight() {
        return this.fixedHeight;
    }

    public void setFixedHeight(float fixedHeight) {
        this.fixedHeight = fixedHeight;
        this.minimumHeight = 0.0f;
    }

    public boolean isNoWrap() {
        return this.noWrap;
    }

    public void setNoWrap(boolean noWrap) {
        this.noWrap = noWrap;
    }

    PdfPTable getTable() {
        return this.table;
    }

    void setTable(PdfPTable table) {
        this.table = table;
        this.column.setText(null);
        this.image = null;
        if (table != null) {
            table.setExtendLastRow(this.verticalAlignment == 4);
            this.column.addElement(table);
            table.setWidthPercentage(100.0f);
        }
    }

    public float getMinimumHeight() {
        return this.minimumHeight;
    }

    public void setMinimumHeight(float minimumHeight) {
        this.minimumHeight = minimumHeight;
        this.fixedHeight = 0.0f;
    }

    public int getColspan() {
        return this.colspan;
    }

    public void setColspan(int colspan) {
        this.colspan = colspan;
    }

    public void setFollowingIndent(float indent) {
        this.column.setFollowingIndent(indent);
    }

    public float getFollowingIndent() {
        return this.column.getFollowingIndent();
    }

    public void setRightIndent(float indent) {
        this.column.setRightIndent(indent);
    }

    public float getRightIndent() {
        return this.column.getRightIndent();
    }

    public float getSpaceCharRatio() {
        return this.column.getSpaceCharRatio();
    }

    public void setSpaceCharRatio(float spaceCharRatio) {
        this.column.setSpaceCharRatio(spaceCharRatio);
    }

    public void setRunDirection(int runDirection) {
        this.column.setRunDirection(runDirection);
    }

    public int getRunDirection() {
        return this.column.getRunDirection();
    }

    public Image getImage() {
        return this.image;
    }

    public void setImage(Image image) {
        this.column.setText(null);
        this.table = null;
        this.image = image;
    }

    public PdfPCellEvent getCellEvent() {
        return this.cellEvent;
    }

    public void setCellEvent(PdfPCellEvent cellEvent) {
        this.cellEvent = cellEvent;
    }

    public int getArabicOptions() {
        return this.column.getArabicOptions();
    }

    public void setArabicOptions(int arabicOptions) {
        this.column.setArabicOptions(arabicOptions);
    }

    public boolean isUseAscender() {
        return this.column.isUseAscender();
    }

    public void setUseAscender(boolean use) {
        this.column.setUseAscender(use);
    }

    public boolean isUseDescender() {
        return this.useDescender;
    }

    public void setUseDescender(boolean useDescender) {
        this.useDescender = useDescender;
    }

    public ColumnText getColumn() {
        return this.column;
    }

    public void setColumn(ColumnText column) {
        this.column = column;
    }
}

