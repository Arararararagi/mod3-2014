/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PageResources;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfPattern;
import com.lowagie.text.pdf.PdfSpotColor;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;

public class PdfPatternPainter
extends PdfTemplate {
    protected float xstep;
    protected float ystep;
    protected boolean stencil = false;
    protected Color defaultColor;

    private PdfPatternPainter() {
        super(null);
        this.type = 3;
    }

    PdfPatternPainter(PdfWriter wr) {
        super(wr);
        this.type = 3;
    }

    PdfPatternPainter(PdfWriter wr, Color defaultColor) {
        this(wr);
        this.stencil = true;
        this.defaultColor = defaultColor == null ? Color.gray : defaultColor;
    }

    public void setXStep(float xstep) {
        this.xstep = xstep;
    }

    public void setYStep(float ystep) {
        this.ystep = ystep;
    }

    public float getXStep() {
        return this.xstep;
    }

    public float getYStep() {
        return this.ystep;
    }

    public boolean isStencil() {
        return this.stencil;
    }

    public void setPatternMatrix(float a, float b, float c, float d, float e, float f) {
        this.setMatrix(a, b, c, d, e, f);
    }

    PdfPattern getPattern() {
        return new PdfPattern(this);
    }

    public PdfContentByte getDuplicate() {
        PdfPatternPainter tpl = new PdfPatternPainter();
        tpl.writer = this.writer;
        tpl.pdf = this.pdf;
        tpl.thisReference = this.thisReference;
        tpl.pageResources = this.pageResources;
        tpl.bBox = new Rectangle(this.bBox);
        tpl.xstep = this.xstep;
        tpl.ystep = this.ystep;
        tpl.matrix = this.matrix;
        tpl.stencil = this.stencil;
        tpl.defaultColor = this.defaultColor;
        return tpl;
    }

    public Color getDefaultColor() {
        return this.defaultColor;
    }

    public void setGrayFill(float gray) {
        this.checkNoColor();
        super.setGrayFill(gray);
    }

    public void resetGrayFill() {
        this.checkNoColor();
        super.resetGrayFill();
    }

    public void setGrayStroke(float gray) {
        this.checkNoColor();
        super.setGrayStroke(gray);
    }

    public void resetGrayStroke() {
        this.checkNoColor();
        super.resetGrayStroke();
    }

    public void setRGBColorFillF(float red, float green, float blue) {
        this.checkNoColor();
        super.setRGBColorFillF(red, green, blue);
    }

    public void resetRGBColorFill() {
        this.checkNoColor();
        super.resetRGBColorFill();
    }

    public void setRGBColorStrokeF(float red, float green, float blue) {
        this.checkNoColor();
        super.setRGBColorStrokeF(red, green, blue);
    }

    public void resetRGBColorStroke() {
        this.checkNoColor();
        super.resetRGBColorStroke();
    }

    public void setCMYKColorFillF(float cyan, float magenta, float yellow, float black) {
        this.checkNoColor();
        super.setCMYKColorFillF(cyan, magenta, yellow, black);
    }

    public void resetCMYKColorFill() {
        this.checkNoColor();
        super.resetCMYKColorFill();
    }

    public void setCMYKColorStrokeF(float cyan, float magenta, float yellow, float black) {
        this.checkNoColor();
        super.setCMYKColorStrokeF(cyan, magenta, yellow, black);
    }

    public void resetCMYKColorStroke() {
        this.checkNoColor();
        super.resetCMYKColorStroke();
    }

    public void addImage(Image image, float a, float b, float c, float d, float e, float f) throws DocumentException {
        if (this.stencil && !image.isMask()) {
            this.checkNoColor();
        }
        super.addImage(image, a, b, c, d, e, f);
    }

    public void setCMYKColorFill(int cyan, int magenta, int yellow, int black) {
        this.checkNoColor();
        super.setCMYKColorFill(cyan, magenta, yellow, black);
    }

    public void setCMYKColorStroke(int cyan, int magenta, int yellow, int black) {
        this.checkNoColor();
        super.setCMYKColorStroke(cyan, magenta, yellow, black);
    }

    public void setRGBColorFill(int red, int green, int blue) {
        this.checkNoColor();
        super.setRGBColorFill(red, green, blue);
    }

    public void setRGBColorStroke(int red, int green, int blue) {
        this.checkNoColor();
        super.setRGBColorStroke(red, green, blue);
    }

    public void setColorStroke(Color color) {
        this.checkNoColor();
        super.setColorStroke(color);
    }

    public void setColorFill(Color color) {
        this.checkNoColor();
        super.setColorFill(color);
    }

    public void setColorFill(PdfSpotColor sp, float tint) {
        this.checkNoColor();
        super.setColorFill(sp, tint);
    }

    public void setColorStroke(PdfSpotColor sp, float tint) {
        this.checkNoColor();
        super.setColorStroke(sp, tint);
    }

    public void setPatternFill(PdfPatternPainter p) {
        this.checkNoColor();
        super.setPatternFill(p);
    }

    public void setPatternFill(PdfPatternPainter p, Color color, float tint) {
        this.checkNoColor();
        super.setPatternFill(p, color, tint);
    }

    public void setPatternStroke(PdfPatternPainter p, Color color, float tint) {
        this.checkNoColor();
        super.setPatternStroke(p, color, tint);
    }

    public void setPatternStroke(PdfPatternPainter p) {
        this.checkNoColor();
        super.setPatternStroke(p);
    }

    void checkNoColor() {
        if (this.stencil) {
            throw new RuntimeException("Colors are not allowed in uncolored tile patterns.");
        }
    }
}

