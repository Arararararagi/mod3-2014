/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.CMYKColor;
import com.lowagie.text.pdf.DocumentFont;
import com.lowagie.text.pdf.FdfReader;
import com.lowagie.text.pdf.FdfWriter;
import com.lowagie.text.pdf.FontDetails;
import com.lowagie.text.pdf.GrayColor;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PRTokeniser;
import com.lowagie.text.pdf.PdfAppearance;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfBoolean;
import com.lowagie.text.pdf.PdfDate;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfPKCS7;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamperImp;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.TextField;
import com.lowagie.text.pdf.XfdfReader;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class AcroFields {
    PdfReader reader;
    PdfWriter writer;
    HashMap fields;
    private int topFirst;
    private HashMap sigNames;
    private boolean append;
    private static final int DA_FONT = 0;
    private static final int DA_SIZE = 1;
    private static final int DA_COLOR = 2;
    public static final int FIELD_TYPE_NONE = 0;
    public static final int FIELD_TYPE_PUSHBUTTON = 1;
    public static final int FIELD_TYPE_CHECKBOX = 2;
    public static final int FIELD_TYPE_RADIOBUTTON = 3;
    public static final int FIELD_TYPE_TEXT = 4;
    public static final int FIELD_TYPE_LIST = 5;
    public static final int FIELD_TYPE_COMBO = 6;
    public static final int FIELD_TYPE_SIGNATURE = 7;
    private boolean lastWasString;
    private boolean generateAppearances = true;
    private HashMap localFonts = new HashMap();
    private static final HashMap stdFieldFontNames = new HashMap();
    private int totalRevisions;
    private HashMap fieldCache;

    static {
        stdFieldFontNames.put("CoBO", new String[]{"Courier-BoldOblique"});
        stdFieldFontNames.put("CoBo", new String[]{"Courier-Bold"});
        stdFieldFontNames.put("CoOb", new String[]{"Courier-Oblique"});
        stdFieldFontNames.put("Cour", new String[]{"Courier"});
        stdFieldFontNames.put("HeBO", new String[]{"Helvetica-BoldOblique"});
        stdFieldFontNames.put("HeBo", new String[]{"Helvetica-Bold"});
        stdFieldFontNames.put("HeOb", new String[]{"Helvetica-Oblique"});
        stdFieldFontNames.put("Helv", new String[]{"Helvetica"});
        stdFieldFontNames.put("Symb", new String[]{"Symbol"});
        stdFieldFontNames.put("TiBI", new String[]{"Times-BoldItalic"});
        stdFieldFontNames.put("TiBo", new String[]{"Times-Bold"});
        stdFieldFontNames.put("TiIt", new String[]{"Times-Italic"});
        stdFieldFontNames.put("TiRo", new String[]{"Times-Roman"});
        stdFieldFontNames.put("ZaDb", new String[]{"ZapfDingbats"});
        stdFieldFontNames.put("HySm", new String[]{"HYSMyeongJo-Medium", "UniKS-UCS2-H"});
        stdFieldFontNames.put("HyGo", new String[]{"HYGoThic-Medium", "UniKS-UCS2-H"});
        stdFieldFontNames.put("KaGo", new String[]{"HeiseiKakuGo-W5", "UniKS-UCS2-H"});
        stdFieldFontNames.put("KaMi", new String[]{"HeiseiMin-W3", "UniJIS-UCS2-H"});
        stdFieldFontNames.put("MHei", new String[]{"MHei-Medium", "UniCNS-UCS2-H"});
        stdFieldFontNames.put("MSun", new String[]{"MSung-Light", "UniCNS-UCS2-H"});
        stdFieldFontNames.put("STSo", new String[]{"STSong-Light", "UniGB-UCS2-H"});
    }

    AcroFields(PdfReader reader, PdfWriter writer) {
        this.reader = reader;
        this.writer = writer;
        if (writer instanceof PdfStamperImp) {
            this.append = ((PdfStamperImp)writer).isAppend();
        }
        this.fill();
    }

    void fill() {
        this.fields = new HashMap();
        PdfDictionary top = (PdfDictionary)PdfReader.getPdfObjectRelease(this.reader.getCatalog().get(PdfName.ACROFORM));
        if (top == null) {
            return;
        }
        PdfArray arrfds = (PdfArray)PdfReader.getPdfObjectRelease(top.get(PdfName.FIELDS));
        if (arrfds == null || arrfds.size() == 0) {
            return;
        }
        arrfds = null;
        for (int k = 1; k <= this.reader.getNumberOfPages(); ++k) {
            PdfDictionary page;
            PdfArray annots;
            if (k % 100 == 0) {
                System.out.println(k);
            }
            if ((annots = (PdfArray)PdfReader.getPdfObjectRelease((page = this.reader.getPageNRelease(k)).get(PdfName.ANNOTS), page)) == null) continue;
            ArrayList arr = annots.getArrayList();
            for (int j = 0; j < arr.size(); ++j) {
                Item item;
                PdfObject annoto = PdfReader.getPdfObject((PdfObject)arr.get(j), annots);
                if (annoto instanceof PdfIndirectReference && !annoto.isIndirect()) {
                    PdfReader.releaseLastXrefPartial((PdfObject)arr.get(j));
                    continue;
                }
                PdfDictionary annot = (PdfDictionary)annoto;
                if (!PdfName.WIDGET.equals(annot.get(PdfName.SUBTYPE))) {
                    PdfReader.releaseLastXrefPartial((PdfObject)arr.get(j));
                    continue;
                }
                PdfDictionary widget = annot;
                PdfDictionary dic = new PdfDictionary();
                dic.putAll(annot);
                String name = "";
                PdfDictionary value = null;
                PdfObject lastV = null;
                while (annot != null) {
                    dic.mergeDifferent(annot);
                    PdfString t = (PdfString)PdfReader.getPdfObject(annot.get(PdfName.T));
                    if (t != null) {
                        name = String.valueOf(t.toUnicodeString()) + "." + name;
                    }
                    if (lastV == null && annot.get(PdfName.V) != null) {
                        lastV = PdfReader.getPdfObjectRelease(annot.get(PdfName.V));
                    }
                    if (value == null && t != null) {
                        value = annot;
                        if (annot.get(PdfName.V) == null && lastV != null) {
                            value.put(PdfName.V, lastV);
                        }
                    }
                    annot = (PdfDictionary)PdfReader.getPdfObject(annot.get(PdfName.PARENT), annot);
                }
                if (name.length() > 0) {
                    name = name.substring(0, name.length() - 1);
                }
                if ((item = (Item)this.fields.get(name)) == null) {
                    item = new Item();
                    this.fields.put(name, item);
                }
                if (value == null) {
                    item.values.add(widget);
                } else {
                    item.values.add(value);
                }
                item.widgets.add(widget);
                item.widget_refs.add(arr.get(j));
                if (top != null) {
                    dic.mergeDifferent(top);
                }
                item.merged.add(dic);
                item.page.add(new Integer(k));
                item.tabOrder.add(new Integer(j));
            }
        }
    }

    public String[] getAppearanceStates(String fieldName) {
        int k;
        Item fd = (Item)this.fields.get(fieldName);
        if (fd == null) {
            return null;
        }
        HashMap<String, Object> names = new HashMap<String, Object>();
        PdfDictionary vals = (PdfDictionary)fd.values.get(0);
        PdfObject opts = PdfReader.getPdfObject(vals.get(PdfName.OPT));
        if (opts != null) {
            if (opts.isString()) {
                names.put(((PdfString)opts).toUnicodeString(), null);
            } else if (opts.isArray()) {
                ArrayList list = ((PdfArray)opts).getArrayList();
                for (k = 0; k < list.size(); ++k) {
                    PdfObject v = PdfReader.getPdfObject((PdfObject)list.get(k));
                    if (v == null || !v.isString()) continue;
                    names.put(((PdfString)v).toUnicodeString(), null);
                }
            }
        }
        ArrayList wd = fd.widgets;
        for (k = 0; k < wd.size(); ++k) {
            PdfObject ob;
            PdfDictionary dic = (PdfDictionary)wd.get(k);
            if ((dic = (PdfDictionary)PdfReader.getPdfObject(dic.get(PdfName.AP))) == null || (ob = PdfReader.getPdfObject(dic.get(PdfName.N))) == null) continue;
            if (!ob.isDictionary()) continue;
            dic = (PdfDictionary)ob;
            Iterator it = dic.getKeys().iterator();
            while (it.hasNext()) {
                String name = PdfName.decodeName(((PdfName)it.next()).toString());
                names.put(name, null);
            }
        }
        String[] out = new String[names.size()];
        return names.keySet().toArray(out);
    }

    public int getFieldType(String fieldName) {
        Item fd = (Item)this.fields.get(fieldName);
        if (fd == null) {
            return 0;
        }
        PdfObject type = PdfReader.getPdfObject(((PdfDictionary)fd.merged.get(0)).get(PdfName.FT));
        if (type == null) {
            return 0;
        }
        int ff = 0;
        PdfObject ffo = PdfReader.getPdfObject(((PdfDictionary)fd.merged.get(0)).get(PdfName.FF));
        if (ffo != null && ffo.type() == 2) {
            ff = ((PdfNumber)ffo).intValue();
        }
        if (PdfName.BTN.equals(type)) {
            if ((ff & 65536) != 0) {
                return 1;
            }
            if ((ff & 32768) != 0) {
                return 3;
            }
            return 2;
        }
        if (PdfName.TX.equals(type)) {
            return 4;
        }
        if (PdfName.CH.equals(type)) {
            if ((ff & 131072) != 0) {
                return 6;
            }
            return 5;
        }
        if (PdfName.SIG.equals(type)) {
            return 7;
        }
        return 0;
    }

    public void exportAsFdf(FdfWriter writer) {
        Iterator it = this.fields.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            Item item = (Item)entry.getValue();
            String name = (String)entry.getKey();
            PdfObject v = PdfReader.getPdfObject(((PdfDictionary)item.merged.get(0)).get(PdfName.V));
            if (v == null) continue;
            String value = this.getField(name);
            if (this.lastWasString) {
                writer.setFieldAsString(name, value);
                continue;
            }
            writer.setFieldAsName(name, value);
        }
    }

    public boolean renameField(String oldName, String newName) {
        int idx2;
        int idx1 = oldName.lastIndexOf(46) + 1;
        if (idx1 != (idx2 = newName.lastIndexOf(46) + 1)) {
            return false;
        }
        if (!oldName.substring(0, idx1).equals(newName.substring(0, idx2))) {
            return false;
        }
        if (this.fields.containsKey(newName)) {
            return false;
        }
        Item item = (Item)this.fields.get(oldName);
        if (item == null) {
            return false;
        }
        newName = newName.substring(idx2);
        PdfString ss = new PdfString(newName, "UnicodeBig");
        for (int k = 0; k < item.merged.size(); ++k) {
            PdfDictionary dic = (PdfDictionary)item.values.get(k);
            dic.put(PdfName.T, ss);
            this.markUsed(dic);
            dic = (PdfDictionary)item.merged.get(k);
            dic.put(PdfName.T, ss);
        }
        this.fields.remove(oldName);
        this.fields.put(newName, item);
        return true;
    }

    private static Object[] splitDAelements(String da) {
        try {
            PRTokeniser tk = new PRTokeniser(PdfEncodings.convertToBytes(da, null));
            ArrayList<String> stack = new ArrayList<String>();
            Object[] ret = new Object[3];
            while (tk.nextToken()) {
                if (tk.getTokenType() == 4) continue;
                if (tk.getTokenType() == 10) {
                    String operator = tk.getStringValue();
                    if (operator.equals("Tf")) {
                        if (stack.size() >= 2) {
                            ret[0] = stack.get(stack.size() - 2);
                            ret[1] = new Float((String)stack.get(stack.size() - 1));
                        }
                    } else if (operator.equals("g")) {
                        float gray;
                        if (stack.size() >= 1 && (gray = new Float((String)stack.get(stack.size() - 1)).floatValue()) != 0.0f) {
                            ret[2] = new GrayColor(gray);
                        }
                    } else if (operator.equals("rg")) {
                        if (stack.size() >= 3) {
                            float red = new Float((String)stack.get(stack.size() - 3)).floatValue();
                            float green = new Float((String)stack.get(stack.size() - 2)).floatValue();
                            float blue = new Float((String)stack.get(stack.size() - 1)).floatValue();
                            ret[2] = new Color(red, green, blue);
                        }
                    } else if (operator.equals("k") && stack.size() >= 4) {
                        float cyan = new Float((String)stack.get(stack.size() - 4)).floatValue();
                        float magenta = new Float((String)stack.get(stack.size() - 3)).floatValue();
                        float yellow = new Float((String)stack.get(stack.size() - 2)).floatValue();
                        float black = new Float((String)stack.get(stack.size() - 1)).floatValue();
                        ret[2] = new CMYKColor(cyan, magenta, yellow, black);
                    }
                    stack.clear();
                    continue;
                }
                stack.add(tk.getStringValue());
            }
            return ret;
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
    }

    PdfAppearance getAppearance(PdfDictionary merged, String text, String fieldName) throws IOException, DocumentException {
        PdfName fieldType;
        this.topFirst = 0;
        int flags = 0;
        TextField tx = null;
        if (!(this.fieldCache != null && this.fieldCache.containsKey(fieldName))) {
            PdfNumber nfl;
            PdfDictionary mk;
            PdfDictionary bs;
            tx = new TextField(this.writer, null, null);
            tx.setBorderWidth(0.0f);
            PdfString da = (PdfString)PdfReader.getPdfObject(merged.get(PdfName.DA));
            if (da != null) {
                PdfDictionary font;
                Object[] dab = AcroFields.splitDAelements(da.toUnicodeString());
                if (dab[1] != null) {
                    tx.setFontSize(((Float)dab[1]).floatValue());
                }
                if (dab[2] != null) {
                    tx.setTextColor((Color)dab[2]);
                }
                if (dab[0] != null && (font = (PdfDictionary)PdfReader.getPdfObject(merged.get(PdfName.DR))) != null && (font = (PdfDictionary)PdfReader.getPdfObject(font.get(PdfName.FONT))) != null) {
                    PdfObject po = font.get(new PdfName((String)dab[0]));
                    if (po != null && po.type() == 10) {
                        tx.setFont(new DocumentFont((PRIndirectReference)po));
                    } else {
                        BaseFont bf = (BaseFont)this.localFonts.get(dab[0]);
                        if (bf == null) {
                            String[] fn = (String[])stdFieldFontNames.get(dab[0]);
                            if (fn != null) {
                                try {
                                    String enc = "winansi";
                                    if (fn.length > 1) {
                                        enc = fn[1];
                                    }
                                    bf = BaseFont.createFont(fn[0], enc, false);
                                    tx.setFont(bf);
                                }
                                catch (Exception e) {}
                            }
                        } else {
                            tx.setFont(bf);
                        }
                    }
                }
            }
            if ((mk = (PdfDictionary)PdfReader.getPdfObject(merged.get(PdfName.MK))) != null) {
                PdfArray ar = (PdfArray)PdfReader.getPdfObject(mk.get(PdfName.BC));
                Color border = this.getMKColor(ar);
                tx.setBorderColor(border);
                if (border != null) {
                    tx.setBorderWidth(1.0f);
                }
                ar = (PdfArray)PdfReader.getPdfObject(mk.get(PdfName.BG));
                tx.setBackgroundColor(this.getMKColor(ar));
                PdfNumber rotation = (PdfNumber)PdfReader.getPdfObject(mk.get(PdfName.R));
                if (rotation != null) {
                    tx.setRotation(rotation.intValue());
                }
            }
            if ((nfl = (PdfNumber)PdfReader.getPdfObject(merged.get(PdfName.FF))) != null) {
                flags = nfl.intValue();
            }
            tx.setOptions(((flags & 4096) == 0 ? 0 : 4) | ((flags & 16777216) == 0 ? 0 : 256));
            if ((flags & 16777216) != 0) {
                PdfNumber maxLen = (PdfNumber)PdfReader.getPdfObject(merged.get(PdfName.MAXLEN));
                int len = 0;
                if (maxLen != null) {
                    len = maxLen.intValue();
                }
                tx.setMaxCharacterLength(len);
            }
            if ((nfl = (PdfNumber)PdfReader.getPdfObject(merged.get(PdfName.Q))) != null) {
                if (nfl.intValue() == 1) {
                    tx.setAlignment(1);
                } else if (nfl.intValue() == 2) {
                    tx.setAlignment(2);
                }
            }
            if ((bs = (PdfDictionary)PdfReader.getPdfObject(merged.get(PdfName.BS))) != null) {
                PdfName s;
                PdfNumber w = (PdfNumber)PdfReader.getPdfObject(bs.get(PdfName.W));
                if (w != null) {
                    tx.setBorderWidth(w.floatValue());
                }
                if (PdfName.D.equals(s = (PdfName)PdfReader.getPdfObject(bs.get(PdfName.S)))) {
                    tx.setBorderStyle(1);
                } else if (PdfName.B.equals(s)) {
                    tx.setBorderStyle(2);
                } else if (PdfName.I.equals(s)) {
                    tx.setBorderStyle(3);
                } else if (PdfName.U.equals(s)) {
                    tx.setBorderStyle(4);
                }
            } else {
                PdfArray bd = (PdfArray)PdfReader.getPdfObject(merged.get(PdfName.BORDER));
                if (bd != null) {
                    ArrayList ar = bd.getArrayList();
                    if (ar.size() >= 3) {
                        tx.setBorderWidth(((PdfNumber)ar.get(2)).floatValue());
                    }
                    if (ar.size() >= 4) {
                        tx.setBorderStyle(1);
                    }
                }
            }
            PdfArray rect = (PdfArray)PdfReader.getPdfObject(merged.get(PdfName.RECT));
            Rectangle box = PdfReader.getNormalizedRectangle(rect);
            if (tx.getRotation() == 90 || tx.getRotation() == 270) {
                box = box.rotate();
            }
            tx.setBox(box);
            if (this.fieldCache != null) {
                this.fieldCache.put(fieldName, tx);
            }
        } else {
            tx = (TextField)this.fieldCache.get(fieldName);
            tx.setWriter(this.writer);
        }
        if (PdfName.TX.equals(fieldType = (PdfName)PdfReader.getPdfObject(merged.get(PdfName.FT)))) {
            tx.setText(text);
            return tx.getAppearance();
        }
        if (!PdfName.CH.equals(fieldType)) {
            throw new DocumentException("An appearance was requested without a variable text field.");
        }
        PdfArray opt = (PdfArray)PdfReader.getPdfObject(merged.get(PdfName.OPT));
        if ((flags & 131072) != 0 && opt == null) {
            tx.setText(text);
            return tx.getAppearance();
        }
        boolean arrsize = false;
        if (opt != null) {
            int k;
            ArrayList op = opt.getArrayList();
            String[] choices = new String[op.size()];
            String[] choicesExp = new String[op.size()];
            for (k = 0; k < op.size(); ++k) {
                PdfObject obj = (PdfObject)op.get(k);
                if (obj.isString()) {
                    choices[k] = choicesExp[k] = ((PdfString)obj).toUnicodeString();
                    continue;
                }
                ArrayList opar = ((PdfArray)obj).getArrayList();
                choicesExp[k] = ((PdfString)opar.get(0)).toUnicodeString();
                choices[k] = ((PdfString)opar.get(1)).toUnicodeString();
            }
            if ((flags & 131072) != 0) {
                for (k = 0; k < choices.length; ++k) {
                    if (!text.equals(choicesExp[k])) continue;
                    text = choices[k];
                    break;
                }
                tx.setText(text);
                return tx.getAppearance();
            }
            int idx = 0;
            for (int k2 = 0; k2 < choices.length; ++k2) {
                if (!text.equals(choices[k2])) continue;
                idx = k2;
                break;
            }
            tx.setChoices(choices);
            tx.setChoiceExports(choicesExp);
            tx.setChoiceSelection(idx);
        }
        PdfAppearance app = tx.getListAppearance();
        this.topFirst = tx.getTopFirst();
        return app;
    }

    Color getMKColor(PdfArray ar) {
        if (ar == null) {
            return null;
        }
        ArrayList cc = ar.getArrayList();
        switch (cc.size()) {
            case 1: {
                return new GrayColor(((PdfNumber)cc.get(0)).floatValue());
            }
            case 3: {
                return new Color(((PdfNumber)cc.get(0)).floatValue(), ((PdfNumber)cc.get(1)).floatValue(), ((PdfNumber)cc.get(2)).floatValue());
            }
            case 4: {
                return new CMYKColor(((PdfNumber)cc.get(0)).floatValue(), ((PdfNumber)cc.get(1)).floatValue(), ((PdfNumber)cc.get(2)).floatValue(), ((PdfNumber)cc.get(3)).floatValue());
            }
        }
        return null;
    }

    public String getField(String name) {
        Item item = (Item)this.fields.get(name);
        if (item == null) {
            return null;
        }
        this.lastWasString = false;
        PdfObject v = PdfReader.getPdfObject(((PdfDictionary)item.merged.get(0)).get(PdfName.V));
        if (v == null) {
            return "";
        }
        PdfName type = (PdfName)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(0)).get(PdfName.FT));
        if (PdfName.BTN.equals(type)) {
            PdfNumber ff = (PdfNumber)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(0)).get(PdfName.FF));
            int flags = 0;
            if (ff != null) {
                flags = ff.intValue();
            }
            if ((flags & 65536) != 0) {
                return "";
            }
            String value = "";
            if (v.isName()) {
                value = PdfName.decodeName(v.toString());
            } else if (v.isString()) {
                value = ((PdfString)v).toUnicodeString();
            }
            PdfObject opts = PdfReader.getPdfObject(((PdfDictionary)item.values.get(0)).get(PdfName.OPT));
            if (opts != null && opts.isArray()) {
                ArrayList list = ((PdfArray)opts).getArrayList();
                int idx = 0;
                try {
                    idx = Integer.parseInt(value);
                    PdfString ps = (PdfString)list.get(idx);
                    value = ps.toUnicodeString();
                    this.lastWasString = true;
                }
                catch (Exception ps) {
                    // empty catch block
                }
            }
            return value;
        }
        if (v.isString()) {
            this.lastWasString = true;
            return ((PdfString)v).toUnicodeString();
        }
        return PdfName.decodeName(v.toString());
    }

    public boolean setFieldProperty(String field, String name, Object value, int[] inst) {
        if (this.writer == null) {
            throw new RuntimeException("This AcroFields instance is read-only.");
        }
        try {
            Item item = (Item)this.fields.get(field);
            if (item == null) {
                return false;
            }
            InstHit hit = new InstHit(inst);
            if (name.equalsIgnoreCase("textfont")) {
                for (int k = 0; k < item.merged.size(); ++k) {
                    PdfDictionary fonts;
                    if (!hit.isHit(k)) continue;
                    PdfString da = (PdfString)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(k)).get(PdfName.DA));
                    PdfDictionary dr = (PdfDictionary)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(k)).get(PdfName.DR));
                    if (da == null || dr == null) continue;
                    Object[] dao = AcroFields.splitDAelements(da.toUnicodeString());
                    PdfAppearance cb = new PdfAppearance();
                    if (dao[0] == null) continue;
                    BaseFont bf = (BaseFont)value;
                    PdfName psn = (PdfName)PdfAppearance.stdFieldFontNames.get(bf.getPostscriptFontName());
                    if (psn == null) {
                        psn = new PdfName(bf.getPostscriptFontName());
                    }
                    if ((fonts = (PdfDictionary)PdfReader.getPdfObject(dr.get(PdfName.FONT))) == null) {
                        fonts = new PdfDictionary();
                        dr.put(PdfName.FONT, fonts);
                    }
                    PdfIndirectReference fref = (PdfIndirectReference)fonts.get(psn);
                    PdfDictionary top = (PdfDictionary)PdfReader.getPdfObject(this.reader.getCatalog().get(PdfName.ACROFORM));
                    this.markUsed(top);
                    dr = (PdfDictionary)PdfReader.getPdfObject(top.get(PdfName.DR));
                    if (dr == null) {
                        dr = new PdfDictionary();
                        top.put(PdfName.DR, dr);
                    }
                    this.markUsed(dr);
                    PdfDictionary fontsTop = (PdfDictionary)PdfReader.getPdfObject(dr.get(PdfName.FONT));
                    if (fontsTop == null) {
                        fontsTop = new PdfDictionary();
                        dr.put(PdfName.FONT, fontsTop);
                    }
                    this.markUsed(fontsTop);
                    PdfIndirectReference frefTop = (PdfIndirectReference)fontsTop.get(psn);
                    if (frefTop != null) {
                        if (fref == null) {
                            fonts.put(psn, frefTop);
                        }
                    } else if (fref == null) {
                        FontDetails fd;
                        if (bf.getFontType() == 4) {
                            fd = new FontDetails(null, ((DocumentFont)bf).getIndirectReference(), bf);
                        } else {
                            bf.setSubset(false);
                            fd = this.writer.addSimple(bf);
                            this.localFonts.put(psn.toString().substring(1), bf);
                        }
                        fontsTop.put(psn, fd.getIndirectReference());
                        fonts.put(psn, fd.getIndirectReference());
                    }
                    ByteBuffer buf = cb.getInternalBuffer();
                    buf.append(psn.getBytes()).append(' ').append(((Float)dao[1]).floatValue()).append(" Tf ");
                    if (dao[2] != null) {
                        cb.setColorFill((Color)dao[2]);
                    }
                    PdfString s = new PdfString(cb.toString());
                    ((PdfDictionary)item.merged.get(k)).put(PdfName.DA, s);
                    ((PdfDictionary)item.widgets.get(k)).put(PdfName.DA, s);
                    this.markUsed((PdfDictionary)item.widgets.get(k));
                }
            } else if (name.equalsIgnoreCase("textcolor")) {
                for (int k = 0; k < item.merged.size(); ++k) {
                    PdfString da;
                    if (!hit.isHit(k) || (da = (PdfString)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(k)).get(PdfName.DA))) == null) continue;
                    Object[] dao = AcroFields.splitDAelements(da.toUnicodeString());
                    PdfAppearance cb = new PdfAppearance();
                    if (dao[0] == null) continue;
                    ByteBuffer buf = cb.getInternalBuffer();
                    buf.append(new PdfName((String)dao[0]).getBytes()).append(' ').append(((Float)dao[1]).floatValue()).append(" Tf ");
                    cb.setColorFill((Color)value);
                    PdfString s = new PdfString(cb.toString());
                    ((PdfDictionary)item.merged.get(k)).put(PdfName.DA, s);
                    ((PdfDictionary)item.widgets.get(k)).put(PdfName.DA, s);
                    this.markUsed((PdfDictionary)item.widgets.get(k));
                }
            } else if (name.equalsIgnoreCase("textsize")) {
                for (int k = 0; k < item.merged.size(); ++k) {
                    PdfString da;
                    if (!hit.isHit(k) || (da = (PdfString)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(k)).get(PdfName.DA))) == null) continue;
                    Object[] dao = AcroFields.splitDAelements(da.toUnicodeString());
                    PdfAppearance cb = new PdfAppearance();
                    if (dao[0] == null) continue;
                    ByteBuffer buf = cb.getInternalBuffer();
                    buf.append(new PdfName((String)dao[0]).getBytes()).append(' ').append(((Float)value).floatValue()).append(" Tf ");
                    if (dao[2] != null) {
                        cb.setColorFill((Color)dao[2]);
                    }
                    PdfString s = new PdfString(cb.toString());
                    ((PdfDictionary)item.merged.get(k)).put(PdfName.DA, s);
                    ((PdfDictionary)item.widgets.get(k)).put(PdfName.DA, s);
                    this.markUsed((PdfDictionary)item.widgets.get(k));
                }
            } else if (name.equalsIgnoreCase("bgcolor") || name.equalsIgnoreCase("bordercolor")) {
                PdfName dname = name.equalsIgnoreCase("bgcolor") ? PdfName.BG : PdfName.BC;
                for (int k = 0; k < item.merged.size(); ++k) {
                    if (!hit.isHit(k)) continue;
                    PdfObject obj = PdfReader.getPdfObject(((PdfDictionary)item.merged.get(k)).get(PdfName.MK));
                    this.markUsed(obj);
                    PdfDictionary mk = (PdfDictionary)obj;
                    if (mk == null) {
                        if (value == null) {
                            return true;
                        }
                        mk = new PdfDictionary();
                        ((PdfDictionary)item.merged.get(k)).put(PdfName.MK, mk);
                        ((PdfDictionary)item.widgets.get(k)).put(PdfName.MK, mk);
                        this.markUsed((PdfDictionary)item.widgets.get(k));
                    }
                    if (value == null) {
                        mk.remove(dname);
                        continue;
                    }
                    mk.put(dname, PdfFormField.getMKColor((Color)value));
                }
            } else {
                return false;
            }
            return true;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public boolean setFieldProperty(String field, String name, int value, int[] inst) {
        if (this.writer == null) {
            throw new RuntimeException("This AcroFields instance is read-only.");
        }
        Item item = (Item)this.fields.get(field);
        if (item == null) {
            return false;
        }
        InstHit hit = new InstHit(inst);
        if (name.equalsIgnoreCase("flags")) {
            PdfNumber num = new PdfNumber(value);
            for (int k = 0; k < item.merged.size(); ++k) {
                if (!hit.isHit(k)) continue;
                ((PdfDictionary)item.merged.get(k)).put(PdfName.F, num);
                ((PdfDictionary)item.widgets.get(k)).put(PdfName.F, num);
                this.markUsed((PdfDictionary)item.widgets.get(k));
            }
        } else if (name.equalsIgnoreCase("setflags")) {
            for (int k = 0; k < item.merged.size(); ++k) {
                if (!hit.isHit(k)) continue;
                PdfNumber num = (PdfNumber)PdfReader.getPdfObject(((PdfDictionary)item.widgets.get(k)).get(PdfName.F));
                int val = 0;
                if (num != null) {
                    val = num.intValue();
                }
                num = new PdfNumber(val | value);
                ((PdfDictionary)item.merged.get(k)).put(PdfName.F, num);
                ((PdfDictionary)item.widgets.get(k)).put(PdfName.F, num);
                this.markUsed((PdfDictionary)item.widgets.get(k));
            }
        } else if (name.equalsIgnoreCase("clrflags")) {
            for (int k = 0; k < item.merged.size(); ++k) {
                if (!hit.isHit(k)) continue;
                PdfNumber num = (PdfNumber)PdfReader.getPdfObject(((PdfDictionary)item.widgets.get(k)).get(PdfName.F));
                int val = 0;
                if (num != null) {
                    val = num.intValue();
                }
                num = new PdfNumber(val & ~ value);
                ((PdfDictionary)item.merged.get(k)).put(PdfName.F, num);
                ((PdfDictionary)item.widgets.get(k)).put(PdfName.F, num);
                this.markUsed((PdfDictionary)item.widgets.get(k));
            }
        } else if (name.equalsIgnoreCase("fflags")) {
            PdfNumber num = new PdfNumber(value);
            for (int k = 0; k < item.merged.size(); ++k) {
                if (!hit.isHit(k)) continue;
                ((PdfDictionary)item.merged.get(k)).put(PdfName.FF, num);
                ((PdfDictionary)item.values.get(k)).put(PdfName.FF, num);
                this.markUsed((PdfDictionary)item.values.get(k));
            }
        } else if (name.equalsIgnoreCase("setfflags")) {
            for (int k = 0; k < item.merged.size(); ++k) {
                if (!hit.isHit(k)) continue;
                PdfNumber num = (PdfNumber)PdfReader.getPdfObject(((PdfDictionary)item.values.get(k)).get(PdfName.FF));
                int val = 0;
                if (num != null) {
                    val = num.intValue();
                }
                num = new PdfNumber(val | value);
                ((PdfDictionary)item.merged.get(k)).put(PdfName.FF, num);
                ((PdfDictionary)item.values.get(k)).put(PdfName.FF, num);
                this.markUsed((PdfDictionary)item.values.get(k));
            }
        } else if (name.equalsIgnoreCase("clrfflags")) {
            for (int k = 0; k < item.merged.size(); ++k) {
                if (!hit.isHit(k)) continue;
                PdfNumber num = (PdfNumber)PdfReader.getPdfObject(((PdfDictionary)item.values.get(k)).get(PdfName.FF));
                int val = 0;
                if (num != null) {
                    val = num.intValue();
                }
                num = new PdfNumber(val & ~ value);
                ((PdfDictionary)item.merged.get(k)).put(PdfName.FF, num);
                ((PdfDictionary)item.values.get(k)).put(PdfName.FF, num);
                this.markUsed((PdfDictionary)item.values.get(k));
            }
        } else {
            return false;
        }
        return true;
    }

    public void setFields(FdfReader fdf) throws IOException, DocumentException {
        HashMap fd = fdf.getFields();
        Iterator i = this.fields.keySet().iterator();
        while (i.hasNext()) {
            String f = (String)i.next();
            String v = fdf.getFieldValue(f);
            if (v == null) continue;
            this.setField(f, v);
        }
    }

    public void setFields(XfdfReader xfdf) throws IOException, DocumentException {
        HashMap fd = xfdf.getFields();
        Iterator i = this.fields.keySet().iterator();
        while (i.hasNext()) {
            String f = (String)i.next();
            String v = xfdf.getFieldValue(f);
            if (v == null) continue;
            this.setField(f, v);
        }
    }

    public boolean setField(String name, String value) throws IOException, DocumentException {
        return this.setField(name, value, value);
    }

    public boolean setField(String name, String value, String display) throws IOException, DocumentException {
        if (this.writer == null) {
            throw new DocumentException("This AcroFields instance is read-only.");
        }
        Item item = (Item)this.fields.get(name);
        if (item == null) {
            return false;
        }
        PdfName type = (PdfName)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(0)).get(PdfName.FT));
        if (PdfName.TX.equals(type)) {
            PdfNumber maxLen = (PdfNumber)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(0)).get(PdfName.MAXLEN));
            int len = 0;
            if (maxLen != null) {
                len = maxLen.intValue();
            }
            if (len > 0) {
                value = value.substring(0, Math.min(len, value.length()));
            }
        }
        if (PdfName.TX.equals(type) || PdfName.CH.equals(type)) {
            PdfString v = new PdfString(value, "UnicodeBig");
            for (int idx = 0; idx < item.values.size(); ++idx) {
                ((PdfDictionary)item.values.get(idx)).put(PdfName.V, v);
                this.markUsed((PdfDictionary)item.values.get(idx));
                PdfDictionary merged = (PdfDictionary)item.merged.get(idx);
                merged.put(PdfName.V, v);
                PdfDictionary widget = (PdfDictionary)item.widgets.get(idx);
                if (this.generateAppearances) {
                    PdfDictionary appDic;
                    PdfAppearance app = this.getAppearance(merged, display, name);
                    if (PdfName.CH.equals(type)) {
                        PdfNumber n = new PdfNumber(this.topFirst);
                        widget.put(PdfName.TI, n);
                        merged.put(PdfName.TI, n);
                    }
                    if ((appDic = (PdfDictionary)PdfReader.getPdfObject(widget.get(PdfName.AP))) == null) {
                        appDic = new PdfDictionary();
                        widget.put(PdfName.AP, appDic);
                        merged.put(PdfName.AP, appDic);
                    }
                    appDic.put(PdfName.N, app.getIndirectReference());
                } else {
                    widget.remove(PdfName.AP);
                    merged.remove(PdfName.AP);
                }
                this.markUsed(widget);
            }
            return true;
        }
        if (PdfName.BTN.equals(type)) {
            PdfNumber ff = (PdfNumber)PdfReader.getPdfObject(((PdfDictionary)item.merged.get(0)).get(PdfName.FF));
            int flags = 0;
            if (ff != null) {
                flags = ff.intValue();
            }
            if ((flags & 65536) != 0) {
                return true;
            }
            PdfName v = new PdfName(value);
            if ((flags & 32768) == 0) {
                for (int idx = 0; idx < item.values.size(); ++idx) {
                    ((PdfDictionary)item.values.get(idx)).put(PdfName.V, v);
                    this.markUsed((PdfDictionary)item.values.get(idx));
                    PdfDictionary merged = (PdfDictionary)item.merged.get(idx);
                    merged.put(PdfName.V, v);
                    merged.put(PdfName.AS, v);
                    PdfDictionary widget = (PdfDictionary)item.widgets.get(idx);
                    widget.put(PdfName.AS, v);
                    this.markUsed(widget);
                }
            } else {
                ArrayList<String> lopt = new ArrayList<String>();
                PdfObject opts = PdfReader.getPdfObject(((PdfDictionary)item.values.get(0)).get(PdfName.OPT));
                if (opts != null && opts.isArray()) {
                    ArrayList list = ((PdfArray)opts).getArrayList();
                    for (int k = 0; k < list.size(); ++k) {
                        PdfObject vv = PdfReader.getPdfObject((PdfObject)list.get(k));
                        if (vv != null && vv.isString()) {
                            lopt.add(((PdfString)vv).toUnicodeString());
                            continue;
                        }
                        lopt.add(null);
                    }
                }
                int vidx = lopt.indexOf(value);
                PdfName valt = null;
                PdfName vt = vidx >= 0 ? (valt = new PdfName(String.valueOf(vidx))) : v;
                for (int idx = 0; idx < item.values.size(); ++idx) {
                    PdfDictionary merged = (PdfDictionary)item.merged.get(idx);
                    PdfDictionary widget = (PdfDictionary)item.widgets.get(idx);
                    this.markUsed((PdfDictionary)item.values.get(idx));
                    if (valt != null) {
                        PdfString ps = new PdfString(value, "UnicodeBig");
                        ((PdfDictionary)item.values.get(idx)).put(PdfName.V, ps);
                        merged.put(PdfName.V, ps);
                    } else {
                        ((PdfDictionary)item.values.get(idx)).put(PdfName.V, v);
                        merged.put(PdfName.V, v);
                    }
                    this.markUsed(widget);
                    if (this.isInAP(widget, vt)) {
                        merged.put(PdfName.AS, vt);
                        widget.put(PdfName.AS, vt);
                        continue;
                    }
                    merged.put(PdfName.AS, PdfName.Off);
                    widget.put(PdfName.AS, PdfName.Off);
                }
            }
            return true;
        }
        return false;
    }

    boolean isInAP(PdfDictionary dic, PdfName check) {
        PdfDictionary appDic = (PdfDictionary)PdfReader.getPdfObject(dic.get(PdfName.AP));
        if (appDic == null) {
            return false;
        }
        PdfDictionary NDic = (PdfDictionary)PdfReader.getPdfObject(appDic.get(PdfName.N));
        if (NDic != null && NDic.get(check) != null) {
            return true;
        }
        return false;
    }

    public HashMap getFields() {
        return this.fields;
    }

    public Item getFieldItem(String name) {
        return (Item)this.fields.get(name);
    }

    public float[] getFieldPositions(String name) {
        Item item = (Item)this.fields.get(name);
        if (item == null) {
            return null;
        }
        float[] ret = new float[item.page.size() * 5];
        int ptr = 0;
        for (int k = 0; k < item.page.size(); ++k) {
            try {
                PdfDictionary wd = (PdfDictionary)item.widgets.get(k);
                PdfArray rect = (PdfArray)wd.get(PdfName.RECT);
                if (rect == null) continue;
                Rectangle r = PdfReader.getNormalizedRectangle(rect);
                ret[ptr] = ((Integer)item.page.get(k)).floatValue();
                ++ptr;
                ret[ptr++] = r.left();
                ret[ptr++] = r.bottom();
                ret[ptr++] = r.right();
                ret[ptr++] = r.top();
                continue;
            }
            catch (Exception wd) {
                // empty catch block
            }
        }
        if (ptr < ret.length) {
            float[] ret2 = new float[ptr];
            System.arraycopy(ret, 0, ret2, 0, ptr);
            return ret2;
        }
        return ret;
    }

    private int removeRefFromArray(PdfArray array, PdfObject refo) {
        ArrayList ar = array.getArrayList();
        if (!(refo != null && refo.isIndirect())) {
            return ar.size();
        }
        PdfIndirectReference ref = (PdfIndirectReference)refo;
        for (int j = 0; j < ar.size(); ++j) {
            PdfObject obj = (PdfObject)ar.get(j);
            if (!obj.isIndirect() || ((PdfIndirectReference)obj).getNumber() != ref.getNumber()) continue;
            ar.remove(j--);
        }
        return ar.size();
    }

    public boolean removeFieldsFromPage(int page) {
        if (page < 1) {
            return false;
        }
        String[] names = new String[this.fields.size()];
        this.fields.keySet().toArray(names);
        boolean found = false;
        for (int k = 0; k < names.length; ++k) {
            boolean fr = this.removeField(names[k], page);
            found = found || fr;
        }
        return found;
    }

    public boolean removeField(String name, int page) {
        Item item = (Item)this.fields.get(name);
        if (item == null) {
            return false;
        }
        PdfDictionary acroForm = (PdfDictionary)PdfReader.getPdfObject(this.reader.getCatalog().get(PdfName.ACROFORM), this.reader.getCatalog());
        if (acroForm == null) {
            return false;
        }
        PdfArray arrayf = (PdfArray)PdfReader.getPdfObject(acroForm.get(PdfName.FIELDS), acroForm);
        if (arrayf == null) {
            return false;
        }
        for (int k = 0; k < item.widget_refs.size(); ++k) {
            int pageV = (Integer)item.page.get(k);
            if (page != -1 && page != pageV) continue;
            PdfIndirectReference ref = (PdfIndirectReference)item.widget_refs.get(k);
            PdfDictionary wd = (PdfDictionary)PdfReader.getPdfObject(ref);
            PdfDictionary pageDic = this.reader.getPageN(pageV);
            PdfArray annots = (PdfArray)PdfReader.getPdfObject(pageDic.get(PdfName.ANNOTS), pageDic);
            if (annots != null) {
                if (this.removeRefFromArray(annots, ref) == 0) {
                    pageDic.remove(PdfName.ANNOTS);
                    this.markUsed(pageDic);
                } else {
                    this.markUsed(annots);
                }
            }
            PdfReader.killIndirect(ref);
            PdfIndirectReference kid = ref;
            while ((ref = (PdfIndirectReference)wd.get(PdfName.PARENT)) != null) {
                wd = (PdfDictionary)PdfReader.getPdfObject(ref);
                PdfArray kids = (PdfArray)PdfReader.getPdfObject(wd.get(PdfName.KIDS));
                if (this.removeRefFromArray(kids, kid) != 0) break;
                kid = ref;
                PdfReader.killIndirect(ref);
            }
            if (ref == null) {
                this.removeRefFromArray(arrayf, kid);
                this.markUsed(arrayf);
            }
            if (page == -1) continue;
            item.merged.remove(k);
            item.page.remove(k);
            item.values.remove(k);
            item.widget_refs.remove(k);
            item.widgets.remove(k);
            --k;
        }
        if (page == -1 || item.merged.size() == 0) {
            this.fields.remove(name);
        }
        return true;
    }

    public boolean removeField(String name) {
        return this.removeField(name, -1);
    }

    public boolean isGenerateAppearances() {
        return this.generateAppearances;
    }

    public void setGenerateAppearances(boolean generateAppearances) {
        this.generateAppearances = generateAppearances;
        PdfDictionary top = (PdfDictionary)PdfReader.getPdfObject(this.reader.getCatalog().get(PdfName.ACROFORM));
        if (generateAppearances) {
            top.remove(PdfName.NEEDAPPEARANCES);
        } else {
            top.put(PdfName.NEEDAPPEARANCES, PdfBoolean.PDFTRUE);
        }
    }

    public ArrayList getSignatureNames() {
        if (this.sigNames != null) {
            return new ArrayList(this.sigNames.keySet());
        }
        this.sigNames = new HashMap();
        ArrayList<Object[]> sorter = new ArrayList<Object[]>();
        Iterator it = this.fields.entrySet().iterator();
        while (it.hasNext()) {
            PdfObject ro;
            PdfDictionary v;
            PdfObject contents;
            PdfObject vo;
            Map.Entry entry = it.next();
            Item item = (Item)entry.getValue();
            PdfDictionary merged = (PdfDictionary)item.merged.get(0);
            if (!PdfName.SIG.equals(merged.get(PdfName.FT)) || (vo = PdfReader.getPdfObject(merged.get(PdfName.V))) == null || vo.type() != 6 || (contents = (v = (PdfDictionary)vo).get(PdfName.CONTENTS)) == null || contents.type() != 3 || (ro = v.get(PdfName.BYTERANGE)) == null || ro.type() != 5) continue;
            ArrayList ra = ((PdfArray)ro).getArrayList();
            if (ra.size() < 2) continue;
            int length = ((PdfNumber)ra.get(ra.size() - 1)).intValue() + ((PdfNumber)ra.get(ra.size() - 2)).intValue();
            Object[] arrobject = new Object[2];
            arrobject[0] = entry.getKey();
            int[] arrn = new int[2];
            arrn[0] = length;
            arrobject[1] = arrn;
            sorter.add(arrobject);
        }
        Collections.sort(sorter, new SorterComparator());
        if (sorter.size() > 0) {
            this.totalRevisions = ((int[])((Object[])sorter.get(sorter.size() - 1))[1])[0] == this.reader.getFileLength() ? sorter.size() : sorter.size() + 1;
            for (int k = 0; k < sorter.size(); ++k) {
                Object[] objs = (Object[])sorter.get(k);
                String name = (String)objs[0];
                int[] p = (int[])objs[1];
                p[1] = k + 1;
                this.sigNames.put(name, p);
            }
        }
        return new ArrayList(this.sigNames.keySet());
    }

    public ArrayList getBlankSignatureNames() {
        this.getSignatureNames();
        ArrayList sigs = new ArrayList();
        Iterator it = this.fields.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            Item item = (Item)entry.getValue();
            PdfDictionary merged = (PdfDictionary)item.merged.get(0);
            if (!PdfName.SIG.equals(merged.get(PdfName.FT))) continue;
            if (this.sigNames.containsKey(entry.getKey())) continue;
            sigs.add(entry.getKey());
        }
        return sigs;
    }

    public PdfDictionary getSignatureDictionary(String name) {
        this.getSignatureNames();
        if (!this.sigNames.containsKey(name)) {
            return null;
        }
        Item item = (Item)this.fields.get(name);
        PdfDictionary merged = (PdfDictionary)item.merged.get(0);
        PdfObject vo = PdfReader.getPdfObject(merged.get(PdfName.V));
        return (PdfDictionary)PdfReader.getPdfObject(merged.get(PdfName.V));
    }

    public boolean signatureCoversWholeDocument(String name) {
        this.getSignatureNames();
        if (!this.sigNames.containsKey(name)) {
            return false;
        }
        if (((int[])this.sigNames.get(name))[0] == this.reader.getFileLength()) {
            return true;
        }
        return false;
    }

    public PdfPKCS7 verifySignature(String name) {
        return this.verifySignature(name, null);
    }

    public PdfPKCS7 verifySignature(String name, String provider) {
        PdfDictionary v = this.getSignatureDictionary(name);
        if (v == null) {
            return null;
        }
        try {
            PdfName sub = (PdfName)PdfReader.getPdfObject(v.get(PdfName.SUBFILTER));
            PdfString contents = (PdfString)PdfReader.getPdfObject(v.get(PdfName.CONTENTS));
            PdfPKCS7 pk = null;
            if (sub.equals(PdfName.ADBE_X509_RSA_SHA1)) {
                PdfString cert = (PdfString)PdfReader.getPdfObject(v.get(PdfName.CERT));
                pk = new PdfPKCS7(contents.getOriginalBytes(), cert.getBytes(), provider);
            } else {
                pk = new PdfPKCS7(contents.getOriginalBytes(), provider);
            }
            this.updateByteRange(pk, v);
            PdfString str = (PdfString)PdfReader.getPdfObject(v.get(PdfName.M));
            if (str != null) {
                pk.setSignDate(PdfDate.decode(str.toString()));
            }
            if ((str = (PdfString)PdfReader.getPdfObject(v.get(PdfName.NAME))) != null) {
                pk.setSignName(str.toUnicodeString());
            }
            if ((str = (PdfString)PdfReader.getPdfObject(v.get(PdfName.REASON))) != null) {
                pk.setReason(str.toUnicodeString());
            }
            if ((str = (PdfString)PdfReader.getPdfObject(v.get(PdfName.LOCATION))) != null) {
                pk.setLocation(str.toUnicodeString());
            }
            return pk;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     */
    private void updateByteRange(PdfPKCS7 pkcs7, PdfDictionary v) {
        PdfArray b = (PdfArray)PdfReader.getPdfObject(v.get(PdfName.BYTERANGE));
        RandomAccessFileOrArray rf = this.reader.getSafeFile();
        try {
            try {
                rf.reOpen();
                byte[] buf = new byte[8192];
                ArrayList ar = b.getArrayList();
                block8 : for (int k = 0; k < ar.size(); ++k) {
                    int rd;
                    int start = ((PdfNumber)ar.get(k)).intValue();
                    rf.seek(start);
                    for (int length = ((PdfNumber)ar.get((int)(++k))).intValue(); length > 0; length-=rd) {
                        rd = rf.read(buf, 0, Math.min(length, buf.length));
                        if (rd <= 0) continue block8;
                        pkcs7.update(buf, 0, rd);
                    }
                }
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
            Object var11_14 = null;
        }
        catch (Throwable var12_12) {
            Object var11_13 = null;
            try {
                rf.close();
                throw var12_12;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var12_12;
        }
        try {}
        catch (Exception e) {
            return;
        }
        rf.close();
    }

    private void markUsed(PdfObject obj) {
        if (!this.append) {
            return;
        }
        ((PdfStamperImp)this.writer).markUsed(obj);
    }

    public int getTotalRevisions() {
        this.getSignatureNames();
        return this.totalRevisions;
    }

    public int getRevision(String field) {
        this.getSignatureNames();
        if (!this.sigNames.containsKey(field)) {
            return 0;
        }
        return ((int[])this.sigNames.get(field))[1];
    }

    public InputStream extractRevision(String field) throws IOException {
        this.getSignatureNames();
        int length = ((int[])this.sigNames.get(field))[0];
        RandomAccessFileOrArray raf = this.reader.getSafeFile();
        raf.reOpen();
        raf.seek(0);
        return new RevisionStream(raf, length);
    }

    public HashMap getFieldCache() {
        return this.fieldCache;
    }

    public void setFieldCache(HashMap fieldCache) {
        this.fieldCache = fieldCache;
    }

    private static class InstHit {
        IntHashtable hits;

        public InstHit(int[] inst) {
            if (inst == null) {
                return;
            }
            this.hits = new IntHashtable();
            for (int k = 0; k < inst.length; ++k) {
                this.hits.put(inst[k], 1);
            }
        }

        public boolean isHit(int n) {
            if (this.hits == null) {
                return true;
            }
            return this.hits.containsKey(n);
        }
    }

    public static class Item {
        public ArrayList values = new ArrayList();
        public ArrayList widgets = new ArrayList();
        public ArrayList widget_refs = new ArrayList();
        public ArrayList merged = new ArrayList();
        public ArrayList page = new ArrayList();
        public ArrayList tabOrder = new ArrayList();
    }

    private static class RevisionStream
    extends InputStream {
        private byte[] b = new byte[1];
        private RandomAccessFileOrArray raf;
        private int length;
        private int rangePosition = 0;
        private boolean closed;

        RevisionStream(RandomAccessFileOrArray raf, int length) {
            this.raf = raf;
            this.length = length;
        }

        public int read() throws IOException {
            int n = this.read(this.b);
            if (n != 1) {
                return -1;
            }
            return this.b[0] & 255;
        }

        public int read(byte[] b, int off, int len) throws IOException {
            if (b == null) {
                throw new NullPointerException();
            }
            if (off < 0 || off > b.length || len < 0 || off + len > b.length || off + len < 0) {
                throw new IndexOutOfBoundsException();
            }
            if (len == 0) {
                return 0;
            }
            if (this.rangePosition >= this.length) {
                this.close();
                return -1;
            }
            int elen = Math.min(len, this.length - this.rangePosition);
            this.raf.readFully(b, off, elen);
            this.rangePosition+=elen;
            return elen;
        }

        public void close() throws IOException {
            if (!this.closed) {
                this.raf.close();
                this.closed = true;
            }
        }
    }

    private static class SorterComparator
    implements Comparator {
        SorterComparator() {
        }

        public int compare(Object o1, Object o2) {
            int n1 = ((int[])((Object[])o1)[1])[0];
            int n2 = ((int[])((Object[])o2)[1])[0];
            return n1 - n2;
        }
    }

}

