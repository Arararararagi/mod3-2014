/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ElementListener;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPRow;
import com.lowagie.text.pdf.PdfPTableEvent;
import java.util.ArrayList;

public class PdfPTable
implements Element {
    public static final int BASECANVAS = 0;
    public static final int BACKGROUNDCANVAS = 1;
    public static final int LINECANVAS = 2;
    public static final int TEXTCANVAS = 3;
    protected ArrayList rows = new ArrayList();
    protected float totalHeight = 0.0f;
    protected PdfPCell[] currentRow;
    protected int currentRowIdx = 0;
    protected PdfPCell defaultCell = new PdfPCell(null);
    protected float totalWidth = 0.0f;
    protected float[] relativeWidths;
    protected float[] absoluteWidths;
    protected PdfPTableEvent tableEvent;
    protected int headerRows;
    protected float widthPercentage = 80.0f;
    private int horizontalAlignment = 1;
    private boolean skipFirstHeader = false;
    protected boolean isColspan = false;
    protected int runDirection = 0;
    private boolean lockedWidth = false;
    private boolean splitRows = true;
    protected float spacingBefore;
    protected float spacingAfter;
    private boolean extendLastRow;
    private boolean headersInEvent;
    private boolean splitLate = true;

    protected PdfPTable() {
    }

    public PdfPTable(float[] relativeWidths) {
        if (relativeWidths == null) {
            throw new NullPointerException("The widths array in PdfPTable constructor can not be null.");
        }
        if (relativeWidths.length == 0) {
            throw new IllegalArgumentException("The widths array in PdfPTable constructor can not have zero length.");
        }
        this.relativeWidths = new float[relativeWidths.length];
        System.arraycopy(relativeWidths, 0, this.relativeWidths, 0, relativeWidths.length);
        this.absoluteWidths = new float[relativeWidths.length];
        this.calculateWidths();
        this.currentRow = new PdfPCell[this.absoluteWidths.length];
    }

    public PdfPTable(int numColumns) {
        if (numColumns <= 0) {
            throw new IllegalArgumentException("The number of columns in PdfPTable constructor must be greater than zero.");
        }
        this.relativeWidths = new float[numColumns];
        for (int k = 0; k < numColumns; ++k) {
            this.relativeWidths[k] = 1.0f;
        }
        this.absoluteWidths = new float[this.relativeWidths.length];
        this.calculateWidths();
        this.currentRow = new PdfPCell[this.absoluteWidths.length];
    }

    public PdfPTable(PdfPTable table) {
        int k;
        this.copyFormat(table);
        for (k = 0; k < this.currentRow.length; ++k) {
            if (table.currentRow[k] == null) break;
            this.currentRow[k] = new PdfPCell(table.currentRow[k]);
        }
        for (k = 0; k < table.rows.size(); ++k) {
            PdfPRow row = (PdfPRow)table.rows.get(k);
            if (row != null) {
                row = new PdfPRow(row);
            }
            this.rows.add(row);
        }
    }

    public static PdfPTable shallowCopy(PdfPTable table) {
        PdfPTable nt = new PdfPTable();
        nt.copyFormat(table);
        return nt;
    }

    private void copyFormat(PdfPTable sourceTable) {
        this.relativeWidths = new float[sourceTable.relativeWidths.length];
        this.absoluteWidths = new float[sourceTable.relativeWidths.length];
        System.arraycopy(sourceTable.relativeWidths, 0, this.relativeWidths, 0, this.relativeWidths.length);
        System.arraycopy(sourceTable.absoluteWidths, 0, this.absoluteWidths, 0, this.relativeWidths.length);
        this.totalWidth = sourceTable.totalWidth;
        this.totalHeight = sourceTable.totalHeight;
        this.currentRowIdx = 0;
        this.tableEvent = sourceTable.tableEvent;
        this.runDirection = sourceTable.runDirection;
        this.defaultCell = new PdfPCell(sourceTable.defaultCell);
        this.currentRow = new PdfPCell[sourceTable.currentRow.length];
        this.isColspan = sourceTable.isColspan;
        this.splitRows = sourceTable.splitRows;
        this.spacingAfter = sourceTable.spacingAfter;
        this.spacingBefore = sourceTable.spacingBefore;
        this.headerRows = sourceTable.headerRows;
        this.lockedWidth = sourceTable.lockedWidth;
        this.extendLastRow = sourceTable.extendLastRow;
        this.headersInEvent = sourceTable.headersInEvent;
        this.widthPercentage = sourceTable.widthPercentage;
        this.splitLate = sourceTable.splitLate;
        this.skipFirstHeader = sourceTable.skipFirstHeader;
        this.horizontalAlignment = sourceTable.horizontalAlignment;
    }

    public void setWidths(float[] relativeWidths) throws DocumentException {
        if (relativeWidths.length != this.relativeWidths.length) {
            throw new DocumentException("Wrong number of columns.");
        }
        this.relativeWidths = new float[relativeWidths.length];
        System.arraycopy(relativeWidths, 0, this.relativeWidths, 0, relativeWidths.length);
        this.absoluteWidths = new float[relativeWidths.length];
        this.totalHeight = 0.0f;
        this.calculateWidths();
        this.calculateHeights();
    }

    public void setWidths(int[] relativeWidths) throws DocumentException {
        float[] tb = new float[relativeWidths.length];
        for (int k = 0; k < relativeWidths.length; ++k) {
            tb[k] = relativeWidths[k];
        }
        this.setWidths(tb);
    }

    private void calculateWidths() {
        int k;
        if (this.totalWidth <= 0.0f) {
            return;
        }
        float total = 0.0f;
        for (k = 0; k < this.absoluteWidths.length; ++k) {
            total+=this.relativeWidths[k];
        }
        for (k = 0; k < this.absoluteWidths.length; ++k) {
            this.absoluteWidths[k] = this.totalWidth * this.relativeWidths[k] / total;
        }
    }

    public void setTotalWidth(float totalWidth) {
        if (this.totalWidth == totalWidth) {
            return;
        }
        this.totalWidth = totalWidth;
        this.totalHeight = 0.0f;
        this.calculateWidths();
        this.calculateHeights();
    }

    public void setTotalWidth(float[] columnWidth) throws DocumentException {
        if (columnWidth.length != this.relativeWidths.length) {
            throw new DocumentException("Wrong number of columns.");
        }
        this.totalWidth = 0.0f;
        for (int k = 0; k < columnWidth.length; ++k) {
            this.totalWidth+=columnWidth[k];
        }
        this.setWidths(columnWidth);
    }

    public void setWidthPercentage(float[] columnWidth, Rectangle pageSize) throws DocumentException {
        if (columnWidth.length != this.relativeWidths.length) {
            throw new IllegalArgumentException("Wrong number of columns.");
        }
        float totalWidth = 0.0f;
        for (int k = 0; k < columnWidth.length; ++k) {
            totalWidth+=columnWidth[k];
        }
        this.widthPercentage = totalWidth / (pageSize.right() - pageSize.left()) * 100.0f;
        this.setWidths(columnWidth);
    }

    public float getTotalWidth() {
        return this.totalWidth;
    }

    void calculateHeights() {
        if (this.totalWidth <= 0.0f) {
            return;
        }
        this.totalHeight = 0.0f;
        for (int k = 0; k < this.rows.size(); ++k) {
            PdfPRow row = (PdfPRow)this.rows.get(k);
            if (row == null) continue;
            row.setWidths(this.absoluteWidths);
            this.totalHeight+=row.getMaxHeights();
        }
    }

    public void calculateHeightsFast() {
        if (this.totalWidth <= 0.0f) {
            return;
        }
        this.totalHeight = 0.0f;
        for (int k = 0; k < this.rows.size(); ++k) {
            PdfPRow row = (PdfPRow)this.rows.get(k);
            if (row == null) continue;
            this.totalHeight+=row.getMaxHeights();
        }
    }

    public PdfPCell getDefaultCell() {
        return this.defaultCell;
    }

    public void addCell(PdfPCell cell) {
        int rdir;
        PdfPCell ncell = new PdfPCell(cell);
        int colspan = ncell.getColspan();
        colspan = Math.max(colspan, 1);
        colspan = Math.min(colspan, this.currentRow.length - this.currentRowIdx);
        ncell.setColspan(colspan);
        if (colspan != 1) {
            this.isColspan = true;
        }
        if ((rdir = ncell.getRunDirection()) == 0) {
            ncell.setRunDirection(this.runDirection);
        }
        this.currentRow[this.currentRowIdx] = ncell;
        this.currentRowIdx+=colspan;
        if (this.currentRowIdx >= this.currentRow.length) {
            if (this.runDirection == 3) {
                PdfPCell[] rtlRow = new PdfPCell[this.absoluteWidths.length];
                int rev = this.currentRow.length;
                for (int k = 0; k < this.currentRow.length; ++k) {
                    PdfPCell rcell = this.currentRow[k];
                    int cspan = rcell.getColspan();
                    rtlRow[rev-=cspan] = rcell;
                    k+=cspan - 1;
                }
                this.currentRow = rtlRow;
            }
            PdfPRow row = new PdfPRow(this.currentRow);
            if (this.totalWidth > 0.0f) {
                row.setWidths(this.absoluteWidths);
                this.totalHeight+=row.getMaxHeights();
            }
            this.rows.add(row);
            this.currentRow = new PdfPCell[this.absoluteWidths.length];
            this.currentRowIdx = 0;
        }
    }

    public void addCell(String text) {
        this.addCell(new Phrase(text));
    }

    public void addCell(PdfPTable table) {
        this.defaultCell.setTable(table);
        this.addCell(this.defaultCell);
        this.defaultCell.setTable(null);
    }

    public void addCell(Image image) {
        this.defaultCell.setImage(image);
        this.addCell(this.defaultCell);
        this.defaultCell.setImage(null);
    }

    public void addCell(Phrase phrase) {
        this.defaultCell.setPhrase(phrase);
        this.addCell(this.defaultCell);
        this.defaultCell.setPhrase(null);
    }

    public float writeSelectedRows(int rowStart, int rowEnd, float xPos, float yPos, PdfContentByte[] canvases) {
        return this.writeSelectedRows(0, -1, rowStart, rowEnd, xPos, yPos, canvases);
    }

    public float writeSelectedRows(int colStart, int colEnd, int rowStart, int rowEnd, float xPos, float yPos, PdfContentByte[] canvases) {
        if (this.totalWidth <= 0.0f) {
            throw new RuntimeException("The table width must be greater than zero.");
        }
        int size = this.rows.size();
        if (rowEnd < 0) {
            rowEnd = size;
        }
        rowEnd = Math.min(rowEnd, size);
        if (rowStart < 0) {
            rowStart = 0;
        }
        if (rowStart >= rowEnd) {
            return yPos;
        }
        if (colEnd < 0) {
            colEnd = this.absoluteWidths.length;
        }
        colEnd = Math.min(colEnd, this.absoluteWidths.length);
        if (colStart < 0) {
            colStart = 0;
        }
        colStart = Math.min(colStart, this.absoluteWidths.length);
        float yPosStart = yPos;
        for (int k = rowStart; k < rowEnd; ++k) {
            PdfPRow row = (PdfPRow)this.rows.get(k);
            if (row == null) continue;
            row.writeCells(colStart, colEnd, xPos, yPos, canvases);
            yPos-=row.getMaxHeights();
        }
        if (this.tableEvent != null && colStart == 0 && colEnd == this.absoluteWidths.length) {
            float[] heights = new float[rowEnd - rowStart + 1];
            heights[0] = yPosStart;
            for (int k2 = rowStart; k2 < rowEnd; ++k2) {
                PdfPRow row = (PdfPRow)this.rows.get(k2);
                float hr = 0.0f;
                if (row != null) {
                    hr = row.getMaxHeights();
                }
                heights[k2 - rowStart + 1] = heights[k2 - rowStart] - hr;
            }
            this.tableEvent.tableLayout(this, this.getEventWidths(xPos, rowStart, rowEnd, this.headersInEvent), heights, this.headersInEvent ? this.headerRows : 0, rowStart, canvases);
        }
        return yPos;
    }

    public float writeSelectedRows(int rowStart, int rowEnd, float xPos, float yPos, PdfContentByte canvas) {
        return this.writeSelectedRows(0, -1, rowStart, rowEnd, xPos, yPos, canvas);
    }

    public float writeSelectedRows(int colStart, int colEnd, int rowStart, int rowEnd, float xPos, float yPos, PdfContentByte canvas) {
        if (colEnd < 0) {
            colEnd = this.absoluteWidths.length;
        }
        colEnd = Math.min(colEnd, this.absoluteWidths.length);
        if (colStart < 0) {
            colStart = 0;
        }
        if ((colStart = Math.min(colStart, this.absoluteWidths.length)) != 0 || colEnd != this.absoluteWidths.length) {
            float w = 0.0f;
            for (int k = colStart; k < colEnd; ++k) {
                w+=this.absoluteWidths[k];
            }
            canvas.saveState();
            float lx = 0.0f;
            float rx = 0.0f;
            if (colStart == 0) {
                lx = 10000.0f;
            }
            if (colEnd == this.absoluteWidths.length) {
                rx = 10000.0f;
            }
            canvas.rectangle(xPos - lx, -10000.0f, w + lx + rx, 20000.0f);
            canvas.clip();
            canvas.newPath();
        }
        PdfContentByte[] canvases = PdfPTable.beginWritingRows(canvas);
        float y = this.writeSelectedRows(colStart, colEnd, rowStart, rowEnd, xPos, yPos, canvases);
        PdfPTable.endWritingRows(canvases);
        if (colStart != 0 || colEnd != this.absoluteWidths.length) {
            canvas.restoreState();
        }
        return y;
    }

    public static PdfContentByte[] beginWritingRows(PdfContentByte canvas) {
        return new PdfContentByte[]{canvas, canvas.getDuplicate(), canvas.getDuplicate(), canvas.getDuplicate()};
    }

    public static void endWritingRows(PdfContentByte[] canvases) {
        PdfContentByte canvas = canvases[0];
        canvas.saveState();
        canvas.add(canvases[1]);
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineCap(2);
        canvas.resetRGBColorStroke();
        canvas.add(canvases[2]);
        canvas.restoreState();
        canvas.add(canvases[3]);
    }

    public int size() {
        return this.rows.size();
    }

    public float getTotalHeight() {
        return this.totalHeight;
    }

    public float getRowHeight(int idx) {
        if (this.totalWidth <= 0.0f || idx < 0 || idx >= this.rows.size()) {
            return 0.0f;
        }
        PdfPRow row = (PdfPRow)this.rows.get(idx);
        if (row == null) {
            return 0.0f;
        }
        return row.getMaxHeights();
    }

    public float getHeaderHeight() {
        float total = 0.0f;
        int size = Math.min(this.rows.size(), this.headerRows);
        for (int k = 0; k < size; ++k) {
            PdfPRow row = (PdfPRow)this.rows.get(k);
            if (row == null) continue;
            total+=row.getMaxHeights();
        }
        return total;
    }

    public boolean deleteRow(int rowNumber) {
        PdfPRow row;
        if (rowNumber < 0 || rowNumber >= this.rows.size()) {
            return false;
        }
        if (this.totalWidth > 0.0f && (row = (PdfPRow)this.rows.get(rowNumber)) != null) {
            this.totalHeight-=row.getMaxHeights();
        }
        this.rows.remove(rowNumber);
        return true;
    }

    public boolean deleteLastRow() {
        return this.deleteRow(this.rows.size() - 1);
    }

    public void deleteBodyRows() {
        ArrayList rows2 = new ArrayList();
        for (int k = 0; k < this.headerRows; ++k) {
            rows2.add(this.rows.get(k));
        }
        this.rows = rows2;
        this.totalHeight = 0.0f;
        if (this.totalWidth > 0.0f) {
            this.totalHeight = this.getHeaderHeight();
        }
    }

    public int getHeaderRows() {
        return this.headerRows;
    }

    public void setHeaderRows(int headerRows) {
        if (headerRows < 0) {
            headerRows = 0;
        }
        this.headerRows = headerRows;
    }

    public ArrayList getChunks() {
        return new ArrayList();
    }

    public int type() {
        return 23;
    }

    public boolean process(ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }

    public float getWidthPercentage() {
        return this.widthPercentage;
    }

    public void setWidthPercentage(float widthPercentage) {
        this.widthPercentage = widthPercentage;
    }

    public int getHorizontalAlignment() {
        return this.horizontalAlignment;
    }

    public void setHorizontalAlignment(int horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }

    public PdfPRow getRow(int idx) {
        return (PdfPRow)this.rows.get(idx);
    }

    public ArrayList getRows() {
        return this.rows;
    }

    public void setTableEvent(PdfPTableEvent event) {
        this.tableEvent = event;
    }

    public PdfPTableEvent getTableEvent() {
        return this.tableEvent;
    }

    public float[] getAbsoluteWidths() {
        return this.absoluteWidths;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    float[][] getEventWidths(float xPos, int firstRow, int lastRow, boolean includeHeaders) {
        if (includeHeaders) {
            firstRow = Math.max(firstRow, this.headerRows);
            lastRow = Math.max(lastRow, this.headerRows);
        }
        float[][] widths = new float[(includeHeaders ? this.headerRows : 0) + lastRow - firstRow][];
        if (this.isColspan) {
            int n = 0;
            if (includeHeaders) {
                for (int k = 0; k < this.headerRows; ++k) {
                    PdfPRow row = (PdfPRow)this.rows.get(k);
                    if (row == null) {
                        ++n;
                        continue;
                    }
                    widths[n++] = row.getEventWidth(xPos);
                }
            }
            while (firstRow < lastRow) {
                PdfPRow row = (PdfPRow)this.rows.get(firstRow);
                if (row == null) {
                    ++n;
                } else {
                    widths[n++] = row.getEventWidth(xPos);
                }
                ++firstRow;
            }
            return widths;
        } else {
            int k;
            float[] width = new float[this.absoluteWidths.length + 1];
            width[0] = xPos;
            for (k = 0; k < this.absoluteWidths.length; ++k) {
                width[k + 1] = width[k] + this.absoluteWidths[k];
            }
            for (k = 0; k < widths.length; ++k) {
                widths[k] = width;
            }
        }
        return widths;
    }

    public boolean isSkipFirstHeader() {
        return this.skipFirstHeader;
    }

    public void setSkipFirstHeader(boolean skipFirstHeader) {
        this.skipFirstHeader = skipFirstHeader;
    }

    public void setRunDirection(int runDirection) {
        if (runDirection < 0 || runDirection > 3) {
            throw new RuntimeException("Invalid run direction: " + runDirection);
        }
        this.runDirection = runDirection;
    }

    public int getRunDirection() {
        return this.runDirection;
    }

    public boolean isLockedWidth() {
        return this.lockedWidth;
    }

    public void setLockedWidth(boolean lockedWidth) {
        this.lockedWidth = lockedWidth;
    }

    public boolean isSplitRows() {
        return this.splitRows;
    }

    public void setSplitRows(boolean splitRows) {
        this.splitRows = splitRows;
    }

    public void setSpacingBefore(float spacing) {
        this.spacingBefore = spacing;
    }

    public void setSpacingAfter(float spacing) {
        this.spacingAfter = spacing;
    }

    public float spacingBefore() {
        return this.spacingBefore;
    }

    public float spacingAfter() {
        return this.spacingAfter;
    }

    public boolean isExtendLastRow() {
        return this.extendLastRow;
    }

    public void setExtendLastRow(boolean extendLastRow) {
        this.extendLastRow = extendLastRow;
    }

    public boolean isHeadersInEvent() {
        return this.headersInEvent;
    }

    public void setHeadersInEvent(boolean headersInEvent) {
        this.headersInEvent = headersInEvent;
    }

    public boolean isSplitLate() {
        return this.splitLate;
    }

    public void setSplitLate(boolean splitLate) {
        this.splitLate = splitLate;
    }
}

