/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfLayer;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfOCG;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfWriter;
import java.util.Collection;
import java.util.HashSet;

public class PdfLayerMembership
extends PdfDictionary
implements PdfOCG {
    public static PdfName ALLON = new PdfName("AllOn");
    public static PdfName ANYON = new PdfName("AnyOn");
    public static PdfName ANYOFF = new PdfName("AnyOff");
    public static PdfName ALLOFF = new PdfName("AllOff");
    PdfIndirectReference ref;
    PdfArray members = new PdfArray();
    HashSet layers = new HashSet();

    public PdfLayerMembership(PdfWriter writer) {
        super(PdfName.OCMD);
        this.put(PdfName.OCGS, this.members);
        this.ref = writer.getPdfIndirectReference();
    }

    public PdfIndirectReference getRef() {
        return this.ref;
    }

    public void addMember(PdfLayer layer) {
        if (!this.layers.contains(layer)) {
            this.members.add(layer.getRef());
            this.layers.add(layer);
        }
    }

    public Collection getLayers() {
        return this.layers;
    }

    public void setVisibilityPolicy(PdfName type) {
        this.put(PdfName.P, type);
    }

    public PdfObject getPdfObject() {
        return this;
    }
}

