/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.ExtendedColor;
import com.lowagie.text.pdf.PdfPatternPainter;

public class PatternColor
extends ExtendedColor {
    PdfPatternPainter painter;

    public PatternColor(PdfPatternPainter painter) {
        super(4, 0.5f, 0.5f, 0.5f);
        this.painter = painter;
    }

    public PdfPatternPainter getPainter() {
        return this.painter;
    }
}

