/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.ExtendedColor;

public class GrayColor
extends ExtendedColor {
    float gray;

    public GrayColor(int intGray) {
        this((float)intGray / 255.0f);
    }

    public GrayColor(float floatGray) {
        super(1, floatGray, floatGray, floatGray);
        this.gray = ExtendedColor.normalize(floatGray);
    }

    public float getGray() {
        return this.gray;
    }
}

