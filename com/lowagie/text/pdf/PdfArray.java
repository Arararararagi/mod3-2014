/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

public class PdfArray
extends PdfObject {
    protected ArrayList arrayList;

    public PdfArray() {
        super(5);
        this.arrayList = new ArrayList();
    }

    public PdfArray(PdfObject object) {
        super(5);
        this.arrayList = new ArrayList();
        this.arrayList.add(object);
    }

    public PdfArray(float[] values) {
        super(5);
        this.arrayList = new ArrayList();
        this.add(values);
    }

    public PdfArray(int[] values) {
        super(5);
        this.arrayList = new ArrayList();
        this.add(values);
    }

    public PdfArray(PdfArray array) {
        super(5);
        this.arrayList = new ArrayList(array.getArrayList());
    }

    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        PdfObject object;
        os.write(91);
        Iterator i = this.arrayList.iterator();
        int type = 0;
        if (i.hasNext()) {
            object = (PdfObject)i.next();
            object.toPdf(writer, os);
        }
        while (i.hasNext()) {
            object = (PdfObject)i.next();
            type = object.type();
            if (type != 5 && type != 6 && type != 4 && type != 3) {
                os.write(32);
            }
            object.toPdf(writer, os);
        }
        os.write(93);
    }

    public ArrayList getArrayList() {
        return this.arrayList;
    }

    public int size() {
        return this.arrayList.size();
    }

    public boolean add(PdfObject object) {
        return this.arrayList.add(object);
    }

    public boolean add(float[] values) {
        for (int k = 0; k < values.length; ++k) {
            this.arrayList.add(new PdfNumber(values[k]));
        }
        return true;
    }

    public boolean add(int[] values) {
        for (int k = 0; k < values.length; ++k) {
            this.arrayList.add(new PdfNumber(values[k]));
        }
        return true;
    }

    public void addFirst(PdfObject object) {
        this.arrayList.add(0, object);
    }

    public boolean contains(PdfObject object) {
        return this.arrayList.contains(object);
    }

    public ListIterator listIterator() {
        return this.arrayList.listIterator();
    }
}

