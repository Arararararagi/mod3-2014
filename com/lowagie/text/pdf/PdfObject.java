/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;

public abstract class PdfObject {
    public static final int BOOLEAN = 1;
    public static final int NUMBER = 2;
    public static final int STRING = 3;
    public static final int NAME = 4;
    public static final int ARRAY = 5;
    public static final int DICTIONARY = 6;
    public static final int STREAM = 7;
    public static final int NULL = 8;
    public static final int INDIRECT = 10;
    public static final String NOTHING = "";
    public static final String TEXT_PDFDOCENCODING = "PDF";
    public static final String TEXT_UNICODE = "UnicodeBig";
    protected byte[] bytes;
    protected int type;
    protected PRIndirectReference indRef;

    protected PdfObject(int type) {
        this.type = type;
    }

    protected PdfObject(int type, String content) {
        this.type = type;
        this.bytes = PdfEncodings.convertToBytes(content, null);
    }

    protected PdfObject(int type, byte[] bytes) {
        this.bytes = bytes;
        this.type = type;
    }

    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        if (this.bytes != null) {
            os.write(this.bytes);
        }
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public boolean canBeInObjStm() {
        if ((this.type < 1 || this.type > 6) && this.type != 8) {
            return false;
        }
        return true;
    }

    public String toString() {
        if (this.bytes == null) {
            return super.toString();
        }
        return PdfEncodings.convertToString(this.bytes, null);
    }

    public int length() {
        return this.toString().length();
    }

    protected void setContent(String content) {
        this.bytes = PdfEncodings.convertToBytes(content, null);
    }

    public int type() {
        return this.type;
    }

    public boolean isNull() {
        if (this.type == 8) {
            return true;
        }
        return false;
    }

    public boolean isBoolean() {
        if (this.type == 1) {
            return true;
        }
        return false;
    }

    public boolean isNumber() {
        if (this.type == 2) {
            return true;
        }
        return false;
    }

    public boolean isString() {
        if (this.type == 3) {
            return true;
        }
        return false;
    }

    public boolean isName() {
        if (this.type == 4) {
            return true;
        }
        return false;
    }

    public boolean isArray() {
        if (this.type == 5) {
            return true;
        }
        return false;
    }

    public boolean isDictionary() {
        if (this.type == 6) {
            return true;
        }
        return false;
    }

    public boolean isStream() {
        if (this.type == 7) {
            return true;
        }
        return false;
    }

    public boolean isIndirect() {
        if (this.type == 10) {
            return true;
        }
        return false;
    }

    public PRIndirectReference getIndRef() {
        return this.indRef;
    }

    public void setIndRef(PRIndirectReference indRef) {
        this.indRef = indRef;
    }
}

