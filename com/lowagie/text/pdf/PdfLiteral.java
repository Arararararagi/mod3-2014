/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.OutputStreamCounter;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

public class PdfLiteral
extends PdfObject {
    private int position;

    public PdfLiteral(String text) {
        super(0, text);
    }

    public PdfLiteral(byte[] b) {
        super(0, b);
    }

    public PdfLiteral(int size) {
        super(0, (byte[])null);
        this.bytes = new byte[size];
        Arrays.fill(this.bytes, 32);
    }

    public PdfLiteral(int type, String text) {
        super(type, text);
    }

    public PdfLiteral(int type, byte[] b) {
        super(type, b);
    }

    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        if (os instanceof OutputStreamCounter) {
            this.position = ((OutputStreamCounter)os).getCounter();
        }
        super.toPdf(writer, os);
    }

    public int getPosition() {
        return this.position;
    }

    public int getPosLength() {
        if (this.bytes != null) {
            return this.bytes.length;
        }
        return 0;
    }
}

