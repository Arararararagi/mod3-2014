/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import java.awt.Color;

public class PdfPRow {
    public static final float BOTTOM_LIMIT = -1.07374182E9f;
    protected PdfPCell[] cells;
    protected float[] widths;
    protected float maxHeight = 0.0f;
    protected boolean calculated = false;

    public PdfPRow(PdfPCell[] cells) {
        this.cells = cells;
        this.widths = new float[cells.length];
    }

    public PdfPRow(PdfPRow row) {
        this.maxHeight = row.maxHeight;
        this.calculated = row.calculated;
        this.cells = new PdfPCell[row.cells.length];
        for (int k = 0; k < this.cells.length; ++k) {
            if (row.cells[k] == null) continue;
            this.cells[k] = new PdfPCell(row.cells[k]);
        }
        this.widths = new float[this.cells.length];
        System.arraycopy(row.widths, 0, this.widths, 0, this.cells.length);
    }

    public boolean setWidths(float[] widths) {
        if (widths.length != this.cells.length) {
            return false;
        }
        System.arraycopy(widths, 0, this.widths, 0, this.cells.length);
        float total = 0.0f;
        this.calculated = false;
        for (int k = 0; k < widths.length; ++k) {
            PdfPCell cell = this.cells[k];
            cell.setLeft(total);
            int last = k + cell.getColspan();
            while (k < last) {
                total+=widths[k];
                ++k;
            }
            --k;
            cell.setRight(total);
            cell.setTop(0.0f);
        }
        return true;
    }

    public float calculateHeights() {
        this.maxHeight = 0.0f;
        for (int k = 0; k < this.cells.length; ++k) {
            PdfPCell cell = this.cells[k];
            if (cell == null) continue;
            Image img = cell.getImage();
            if (img != null) {
                img.scalePercent(100.0f);
                float scale = (cell.right() - cell.getEffectivePaddingRight() - cell.getEffectivePaddingLeft() - cell.left()) / img.scaledWidth();
                img.scalePercent(scale * 100.0f);
                cell.setBottom(cell.top() - cell.getEffectivePaddingTop() - cell.getEffectivePaddingBottom() - img.scaledHeight());
            } else {
                float rightLimit = cell.isNoWrap() ? 20000.0f : cell.right() - cell.getEffectivePaddingRight();
                float bry = cell.getFixedHeight() > 0.0f ? cell.top() - cell.getEffectivePaddingTop() + cell.getEffectivePaddingBottom() - cell.getFixedHeight() : -1.07374182E9f;
                ColumnText ct = ColumnText.duplicate(cell.getColumn());
                ct.setSimpleColumn(cell.left() + cell.getEffectivePaddingLeft(), bry, rightLimit, cell.top() - cell.getEffectivePaddingTop());
                try {
                    ct.go(true);
                }
                catch (DocumentException e) {
                    throw new ExceptionConverter(e);
                }
                float yLine = ct.getYLine();
                if (cell.isUseDescender()) {
                    yLine+=ct.getDescender();
                }
                cell.setBottom(yLine - cell.getEffectivePaddingBottom());
            }
            float height = cell.getFixedHeight();
            if (height <= 0.0f) {
                height = cell.height();
            }
            if (height < cell.getFixedHeight()) {
                height = cell.getFixedHeight();
            } else if (height < cell.getMinimumHeight()) {
                height = cell.getMinimumHeight();
            }
            if (height <= this.maxHeight) continue;
            this.maxHeight = height;
        }
        this.calculated = true;
        return this.maxHeight;
    }

    public void writeBorderAndBackground(float xPos, float yPos, PdfPCell cell, PdfContentByte[] canvases) {
        PdfContentByte lines = canvases[2];
        PdfContentByte backgr = canvases[1];
        float x1 = cell.left() + xPos;
        float y2 = cell.top() + yPos;
        float x2 = cell.right() + xPos;
        float y1 = y2 - this.maxHeight;
        Color background = cell.backgroundColor();
        if (background != null) {
            backgr.setColorFill(background);
            backgr.rectangle(x1, y1, x2 - x1, y2 - y1);
            backgr.fill();
        } else if (cell.grayFill() > 0.0f) {
            backgr.setGrayFill(cell.grayFill());
            backgr.rectangle(x1, y1, x2 - x1, y2 - y1);
            backgr.fill();
        }
        if (cell.hasBorders()) {
            if (cell.isUseVariableBorders()) {
                Rectangle borderRect = new Rectangle(cell.left() + xPos, cell.top() - this.maxHeight + yPos, cell.right() + xPos, cell.top() + yPos);
                borderRect.cloneNonPositionParameters(cell);
                lines.rectangle(borderRect);
            } else {
                Color color;
                if (cell.borderWidth() != -1.0f) {
                    lines.setLineWidth(cell.borderWidth());
                }
                if ((color = cell.borderColor()) != null) {
                    lines.setColorStroke(color);
                }
                if (cell.hasBorder(15)) {
                    lines.rectangle(x1, y1, x2 - x1, y2 - y1);
                } else {
                    if (cell.hasBorder(8)) {
                        lines.moveTo(x2, y1);
                        lines.lineTo(x2, y2);
                    }
                    if (cell.hasBorder(4)) {
                        lines.moveTo(x1, y1);
                        lines.lineTo(x1, y2);
                    }
                    if (cell.hasBorder(2)) {
                        lines.moveTo(x1, y1);
                        lines.lineTo(x2, y1);
                    }
                    if (cell.hasBorder(1)) {
                        lines.moveTo(x1, y2);
                        lines.lineTo(x2, y2);
                    }
                }
                lines.stroke();
                if (color != null) {
                    lines.resetRGBColorStroke();
                }
            }
        }
    }

    public void writeCells(int colStart, int colEnd, float xPos, float yPos, PdfContentByte[] canvases) {
        int newStart;
        if (!this.calculated) {
            this.calculateHeights();
        }
        if (colEnd < 0) {
            colEnd = this.cells.length;
        }
        colEnd = Math.min(colEnd, this.cells.length);
        if (colStart < 0) {
            colStart = 0;
        }
        if (colStart >= colEnd) {
            return;
        }
        for (newStart = colStart; newStart >= 0; --newStart) {
            if (this.cells[newStart] != null) break;
            xPos-=this.widths[newStart - 1];
        }
        xPos-=this.cells[newStart].left();
        for (int k = newStart; k < colEnd; ++k) {
            PdfPCell cell = this.cells[k];
            if (cell == null) continue;
            this.writeBorderAndBackground(xPos, yPos, cell, canvases);
            PdfPTable table = cell.getTable();
            Image img = cell.getImage();
            float tly = 0.0f;
            boolean alignTop = false;
            switch (cell.getVerticalAlignment()) {
                case 6: {
                    tly = cell.top() + yPos - this.maxHeight + cell.height() - cell.getEffectivePaddingTop();
                    break;
                }
                case 5: {
                    tly = cell.top() + yPos + (cell.height() - this.maxHeight) / 2.0f - cell.getEffectivePaddingTop();
                    break;
                }
                default: {
                    alignTop = true;
                    tly = cell.top() + yPos - cell.getEffectivePaddingTop();
                }
            }
            if (img != null) {
                boolean vf = false;
                if (cell.height() > this.maxHeight) {
                    img.scalePercent(100.0f);
                    float scale = (this.maxHeight - cell.getEffectivePaddingTop() - cell.getEffectivePaddingBottom()) / img.scaledHeight();
                    img.scalePercent(scale * 100.0f);
                    vf = true;
                }
                float left = cell.left() + xPos + cell.getEffectivePaddingLeft();
                if (vf) {
                    switch (cell.getHorizontalAlignment()) {
                        case 1: {
                            left = xPos + (cell.left() + cell.getEffectivePaddingLeft() + cell.right() - cell.getEffectivePaddingRight() - img.scaledWidth()) / 2.0f;
                            break;
                        }
                        case 2: {
                            left = xPos + cell.right() - cell.getEffectivePaddingRight() - img.scaledWidth();
                            break;
                        }
                    }
                    tly = cell.top() + yPos - cell.getEffectivePaddingTop();
                }
                img.setAbsolutePosition(left, tly - img.scaledHeight());
                try {
                    canvases[3].addImage(img);
                }
                catch (DocumentException e) {
                    throw new ExceptionConverter(e);
                }
            }
            float fixedHeight = cell.getFixedHeight();
            float rightLimit = cell.right() + xPos - cell.getEffectivePaddingRight();
            float leftLimit = cell.left() + xPos + cell.getEffectivePaddingLeft();
            if (cell.isNoWrap()) {
                switch (cell.getHorizontalAlignment()) {
                    case 1: {
                        rightLimit+=10000.0f;
                        leftLimit-=10000.0f;
                        break;
                    }
                    case 2: {
                        leftLimit-=20000.0f;
                        break;
                    }
                    default: {
                        rightLimit+=20000.0f;
                    }
                }
            }
            ColumnText ct = ColumnText.duplicate(cell.getColumn());
            ct.setCanvas(canvases[3]);
            float bry = tly - (this.maxHeight - cell.getEffectivePaddingTop() - cell.getEffectivePaddingBottom());
            if (fixedHeight > 0.0f && cell.height() > this.maxHeight) {
                tly = cell.top() + yPos - cell.getEffectivePaddingTop();
                bry = cell.top() + yPos - this.maxHeight + cell.getEffectivePaddingBottom();
            }
            if (tly > bry || ct.zeroHeightElement()) {
                ct.setSimpleColumn(leftLimit, bry - 0.001f, rightLimit, tly);
                try {
                    ct.go();
                }
                catch (DocumentException e) {
                    throw new ExceptionConverter(e);
                }
            }
            PdfPCellEvent evt = cell.getCellEvent();
            if (evt == null) continue;
            Rectangle rect = new Rectangle(cell.left() + xPos, cell.top() + yPos - this.maxHeight, cell.right() + xPos, cell.top() + yPos);
            evt.cellLayout(cell, rect, canvases);
        }
    }

    public boolean isCalculated() {
        return this.calculated;
    }

    public float getMaxHeights() {
        if (this.calculated) {
            return this.maxHeight;
        }
        return this.calculateHeights();
    }

    public void setMaxHeights(float maxHeight) {
        this.maxHeight = maxHeight;
    }

    float[] getEventWidth(float xPos) {
        int n = 0;
        for (int k = 0; k < this.cells.length; ++k) {
            if (this.cells[k] == null) continue;
            ++n;
        }
        float[] width = new float[n + 1];
        n = 0;
        width[n++] = xPos;
        for (int k2 = 0; k2 < this.cells.length; ++k2) {
            if (this.cells[k2] == null) continue;
            width[n] = width[n - 1] + this.cells[k2].width();
            ++n;
        }
        return width;
    }

    public PdfPRow splitRow(float newHeight) {
        int k;
        PdfPCell cell;
        PdfPCell[] newCells = new PdfPCell[this.cells.length];
        float[] fh = new float[this.cells.length * 2];
        boolean allEmpty = true;
        for (k = 0; k < this.cells.length; ++k) {
            cell = this.cells[k];
            if (cell == null) continue;
            fh[k * 2] = cell.getFixedHeight();
            fh[k * 2 + 1] = cell.getMinimumHeight();
            Image img = cell.getImage();
            PdfPCell c2 = new PdfPCell(cell);
            if (img != null) {
                if (newHeight > cell.getEffectivePaddingBottom() + cell.getEffectivePaddingTop() + 2.0f) {
                    c2.setPhrase(null);
                    allEmpty = false;
                }
            } else {
                boolean thisEmpty;
                int status;
                float rightLimit = cell.isNoWrap() ? 20000.0f : cell.right() - cell.getEffectivePaddingRight();
                ColumnText ct = ColumnText.duplicate(cell.getColumn());
                float y1 = cell.top() - newHeight + cell.getEffectivePaddingBottom();
                float y2 = cell.top() - cell.getEffectivePaddingTop();
                float y = Math.max(y1, y2);
                ct.setSimpleColumn(cell.left() + cell.getEffectivePaddingLeft(), y1, rightLimit, y2);
                try {
                    status = ct.go(true);
                }
                catch (DocumentException e) {
                    throw new ExceptionConverter(e);
                }
                boolean bl = thisEmpty = ct.getYLine() == y;
                if (thisEmpty) {
                    ct = ColumnText.duplicate(cell.getColumn());
                }
                boolean bl2 = allEmpty = allEmpty && thisEmpty;
                if ((status & 1) == 0 || thisEmpty) {
                    c2.setColumn(ct);
                } else {
                    c2.setPhrase(null);
                }
            }
            newCells[k] = c2;
            cell.setFixedHeight(newHeight);
        }
        if (allEmpty) {
            for (k = 0; k < this.cells.length; ++k) {
                cell = this.cells[k];
                if (cell == null) continue;
                float f = fh[k * 2];
                float m = fh[k * 2 + 1];
                if (f <= 0.0f) {
                    cell.setMinimumHeight(m);
                    continue;
                }
                cell.setFixedHeight(f);
            }
            return null;
        }
        this.calculateHeights();
        PdfPRow split = new PdfPRow(newCells);
        split.widths = (float[])this.widths.clone();
        split.calculateHeights();
        return split;
    }
}

