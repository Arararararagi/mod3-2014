/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;

public class PdfException
extends DocumentException {
    public PdfException(Exception ex) {
        super(ex);
    }

    PdfException() {
    }

    PdfException(String message) {
        super(message);
    }
}

