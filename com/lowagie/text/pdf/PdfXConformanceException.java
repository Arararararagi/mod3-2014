/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

public class PdfXConformanceException
extends RuntimeException {
    public PdfXConformanceException() {
    }

    public PdfXConformanceException(String s) {
        super(s);
    }
}

