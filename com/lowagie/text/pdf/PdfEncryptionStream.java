/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfEncryption;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PdfEncryptionStream
extends FilterOutputStream {
    protected PdfEncryption enc;
    private byte[] buf = new byte[1];

    public PdfEncryptionStream(OutputStream out, PdfEncryption enc) {
        super(out);
        this.enc = enc;
    }

    public void write(byte[] b, int off, int len) throws IOException {
        if ((off | len | b.length - (len + off) | off + len) < 0) {
            throw new IndexOutOfBoundsException();
        }
        this.enc.encryptRC4(b, off, len);
        this.out.write(b, off, len);
    }

    public void close() throws IOException {
    }

    public void write(int b) throws IOException {
        this.buf[0] = (byte)b;
        this.write(this.buf);
    }

    public void flush() throws IOException {
    }
}

