/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfLiteral;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import java.security.MessageDigest;

public class PdfEncryption {
    static final byte[] pad;
    byte[] state = new byte[256];
    int x;
    int y;
    byte[] key;
    int keySize;
    byte[] mkey;
    byte[] extra = new byte[5];
    MessageDigest md5;
    byte[] ownerKey = new byte[32];
    byte[] userKey = new byte[32];
    int permissions;
    byte[] documentID;
    static long seq;

    static {
        byte[] arrby = new byte[32];
        arrby[0] = 40;
        arrby[1] = -65;
        arrby[2] = 78;
        arrby[3] = 94;
        arrby[4] = 78;
        arrby[5] = 117;
        arrby[6] = -118;
        arrby[7] = 65;
        arrby[8] = 100;
        arrby[10] = 78;
        arrby[11] = 86;
        arrby[12] = -1;
        arrby[13] = -6;
        arrby[14] = 1;
        arrby[15] = 8;
        arrby[16] = 46;
        arrby[17] = 46;
        arrby[19] = -74;
        arrby[20] = -48;
        arrby[21] = 104;
        arrby[22] = 62;
        arrby[23] = -128;
        arrby[24] = 47;
        arrby[25] = 12;
        arrby[26] = -87;
        arrby[27] = -2;
        arrby[28] = 100;
        arrby[29] = 83;
        arrby[30] = 105;
        arrby[31] = 122;
        pad = arrby;
        seq = System.currentTimeMillis();
    }

    public PdfEncryption() {
        try {
            this.md5 = MessageDigest.getInstance("MD5");
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    public PdfEncryption(PdfEncryption enc) {
        this.mkey = (byte[])enc.mkey.clone();
        this.ownerKey = (byte[])enc.ownerKey.clone();
        this.userKey = (byte[])enc.userKey.clone();
        this.permissions = enc.permissions;
        if (enc.documentID != null) {
            this.documentID = (byte[])enc.documentID.clone();
        }
    }

    private byte[] padPassword(byte[] userPassword) {
        byte[] userPad = new byte[32];
        if (userPassword == null) {
            System.arraycopy(pad, 0, userPad, 0, 32);
        } else {
            System.arraycopy(userPassword, 0, userPad, 0, Math.min(userPassword.length, 32));
            if (userPassword.length < 32) {
                System.arraycopy(pad, 0, userPad, userPassword.length, 32 - userPassword.length);
            }
        }
        return userPad;
    }

    private byte[] computeOwnerKey(byte[] userPad, byte[] ownerPad, boolean strength128Bits) {
        byte[] ownerKey = new byte[32];
        byte[] digest = this.md5.digest(ownerPad);
        if (strength128Bits) {
            byte[] mkey = new byte[16];
            for (int k = 0; k < 50; ++k) {
                digest = this.md5.digest(digest);
            }
            System.arraycopy(userPad, 0, ownerKey, 0, 32);
            for (int i = 0; i < 20; ++i) {
                for (int j = 0; j < mkey.length; ++j) {
                    mkey[j] = (byte)(digest[j] ^ i);
                }
                this.prepareRC4Key(mkey);
                this.encryptRC4(ownerKey);
            }
        } else {
            this.prepareRC4Key(digest, 0, 5);
            this.encryptRC4(userPad, ownerKey);
        }
        return ownerKey;
    }

    private void setupGlobalEncryptionKey(byte[] documentID, byte[] userPad, byte[] ownerKey, int permissions, boolean strength128Bits) {
        this.documentID = documentID;
        this.ownerKey = ownerKey;
        this.permissions = permissions;
        this.mkey = new byte[strength128Bits ? 16 : 5];
        this.md5.reset();
        this.md5.update(userPad);
        this.md5.update(ownerKey);
        byte[] ext = new byte[]{(byte)permissions, (byte)(permissions >> 8), (byte)(permissions >> 16), (byte)(permissions >> 24)};
        this.md5.update(ext, 0, 4);
        if (documentID != null) {
            this.md5.update(documentID);
        }
        byte[] digest = this.md5.digest();
        if (this.mkey.length == 16) {
            for (int k = 0; k < 50; ++k) {
                digest = this.md5.digest(digest);
            }
        }
        System.arraycopy(digest, 0, this.mkey, 0, this.mkey.length);
    }

    private void setupUserKey() {
        if (this.mkey.length == 16) {
            this.md5.update(pad);
            byte[] digest = this.md5.digest(this.documentID);
            System.arraycopy(digest, 0, this.userKey, 0, 16);
            for (int k = 16; k < 32; ++k) {
                this.userKey[k] = 0;
            }
            for (int i = 0; i < 20; ++i) {
                for (int j = 0; j < this.mkey.length; ++j) {
                    digest[j] = (byte)(this.mkey[j] ^ i);
                }
                this.prepareRC4Key(digest, 0, this.mkey.length);
                this.encryptRC4(this.userKey, 0, 16);
            }
        } else {
            this.prepareRC4Key(this.mkey);
            this.encryptRC4(pad, this.userKey);
        }
    }

    public void setupAllKeys(byte[] userPassword, byte[] ownerPassword, int permissions, boolean strength128Bits) {
        if (ownerPassword == null || ownerPassword.length == 0) {
            ownerPassword = this.md5.digest(PdfEncryption.createDocumentId());
        }
        permissions|=strength128Bits ? -3904 : -64;
        byte[] userPad = this.padPassword(userPassword);
        byte[] ownerPad = this.padPassword(ownerPassword);
        this.ownerKey = this.computeOwnerKey(userPad, ownerPad, strength128Bits);
        this.documentID = PdfEncryption.createDocumentId();
        this.setupByUserPad(this.documentID, userPad, this.ownerKey, permissions&=-4, strength128Bits);
    }

    public static byte[] createDocumentId() {
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        long time = System.currentTimeMillis();
        long mem = Runtime.getRuntime().freeMemory();
        String s = String.valueOf(time) + "+" + mem + "+" + seq++;
        return md5.digest(s.getBytes());
    }

    public void setupByUserPassword(byte[] documentID, byte[] userPassword, byte[] ownerKey, int permissions, boolean strength128Bits) {
        this.setupByUserPad(documentID, this.padPassword(userPassword), ownerKey, permissions, strength128Bits);
    }

    private void setupByUserPad(byte[] documentID, byte[] userPad, byte[] ownerKey, int permissions, boolean strength128Bits) {
        this.setupGlobalEncryptionKey(documentID, userPad, ownerKey, permissions, strength128Bits);
        this.setupUserKey();
    }

    public void setupByOwnerPassword(byte[] documentID, byte[] ownerPassword, byte[] userKey, byte[] ownerKey, int permissions, boolean strength128Bits) {
        this.setupByOwnerPad(documentID, this.padPassword(ownerPassword), userKey, ownerKey, permissions, strength128Bits);
    }

    private void setupByOwnerPad(byte[] documentID, byte[] ownerPad, byte[] userKey, byte[] ownerKey, int permissions, boolean strength128Bits) {
        byte[] userPad = this.computeOwnerKey(ownerKey, ownerPad, strength128Bits);
        this.setupGlobalEncryptionKey(documentID, userPad, ownerKey, permissions, strength128Bits);
        this.setupUserKey();
    }

    public void prepareKey() {
        this.prepareRC4Key(this.key, 0, this.keySize);
    }

    public void setHashKey(int number, int generation) {
        this.md5.reset();
        this.extra[0] = (byte)number;
        this.extra[1] = (byte)(number >> 8);
        this.extra[2] = (byte)(number >> 16);
        this.extra[3] = (byte)generation;
        this.extra[4] = (byte)(generation >> 8);
        this.md5.update(this.mkey);
        this.key = this.md5.digest(this.extra);
        this.keySize = this.mkey.length + 5;
        if (this.keySize > 16) {
            this.keySize = 16;
        }
    }

    public static PdfObject createInfoId(byte[] id) {
        int k;
        ByteBuffer buf = new ByteBuffer(90);
        buf.append('[').append('<');
        for (k = 0; k < 16; ++k) {
            buf.appendHex(id[k]);
        }
        buf.append('>').append('<');
        for (k = 0; k < 16; ++k) {
            buf.appendHex(id[k]);
        }
        buf.append('>').append(']');
        return new PdfLiteral(buf.toByteArray());
    }

    public PdfDictionary getEncryptionDictionary() {
        PdfDictionary dic = new PdfDictionary();
        dic.put(PdfName.FILTER, PdfName.STANDARD);
        dic.put(PdfName.O, new PdfLiteral(PdfContentByte.escapeString(this.ownerKey)));
        dic.put(PdfName.U, new PdfLiteral(PdfContentByte.escapeString(this.userKey)));
        dic.put(PdfName.P, new PdfNumber(this.permissions));
        if (this.mkey.length > 5) {
            dic.put(PdfName.V, new PdfNumber(2));
            dic.put(PdfName.R, new PdfNumber(3));
            dic.put(PdfName.LENGTH, new PdfNumber(128));
        } else {
            dic.put(PdfName.V, new PdfNumber(1));
            dic.put(PdfName.R, new PdfNumber(2));
        }
        return dic;
    }

    public void prepareRC4Key(byte[] key) {
        this.prepareRC4Key(key, 0, key.length);
    }

    public void prepareRC4Key(byte[] key, int off, int len) {
        int index1 = 0;
        int index2 = 0;
        for (int k = 0; k < 256; ++k) {
            this.state[k] = (byte)k;
        }
        this.x = 0;
        this.y = 0;
        for (int k2 = 0; k2 < 256; ++k2) {
            index2 = key[index1 + off] + this.state[k2] + index2 & 255;
            byte tmp = this.state[k2];
            this.state[k2] = this.state[index2];
            this.state[index2] = tmp;
            index1 = (index1 + 1) % len;
        }
    }

    public void encryptRC4(byte[] dataIn, int off, int len, byte[] dataOut) {
        int length = len + off;
        for (int k = off; k < length; ++k) {
            this.x = this.x + 1 & 255;
            this.y = this.state[this.x] + this.y & 255;
            byte tmp = this.state[this.x];
            this.state[this.x] = this.state[this.y];
            this.state[this.y] = tmp;
            dataOut[k] = (byte)(dataIn[k] ^ this.state[this.state[this.x] + this.state[this.y] & 255]);
        }
    }

    public void encryptRC4(byte[] data, int off, int len) {
        this.encryptRC4(data, off, len, data);
    }

    public void encryptRC4(byte[] dataIn, byte[] dataOut) {
        this.encryptRC4(dataIn, 0, dataIn.length, dataOut);
    }

    public void encryptRC4(byte[] data) {
        this.encryptRC4(data, 0, data.length, data);
    }

    public PdfObject getFileID() {
        return PdfEncryption.createInfoId(this.documentID);
    }
}

