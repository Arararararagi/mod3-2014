/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.CFFFont;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class CFFFontSubset
extends CFFFont {
    static final String[] SubrsFunctions = new String[]{"RESERVED_0", "hstem", "RESERVED_2", "vstem", "vmoveto", "rlineto", "hlineto", "vlineto", "rrcurveto", "RESERVED_9", "callsubr", "return", "escape", "RESERVED_13", "endchar", "RESERVED_15", "RESERVED_16", "RESERVED_17", "hstemhm", "hintmask", "cntrmask", "rmoveto", "hmoveto", "vstemhm", "rcurveline", "rlinecurve", "vvcurveto", "hhcurveto", "shortint", "callgsubr", "vhcurveto", "hvcurveto"};
    static final String[] SubrsEscapeFuncs = new String[]{"RESERVED_0", "RESERVED_1", "RESERVED_2", "and", "or", "not", "RESERVED_6", "RESERVED_7", "RESERVED_8", "abs", "add", "sub", "div", "RESERVED_13", "neg", "eq", "RESERVED_16", "RESERVED_17", "drop", "RESERVED_19", "put", "get", "ifelse", "random", "mul", "RESERVED_25", "sqrt", "dup", "exch", "index", "roll", "RESERVED_31", "RESERVED_32", "RESERVED_33", "hflex", "flex", "hflex1", "flex1", "RESERVED_REST"};
    HashMap GlyphsUsed;
    ArrayList glyphsInList;
    HashMap FDArrayUsed = new HashMap();
    HashMap[] hSubrsUsed;
    ArrayList[] lSubrsUsed;
    HashMap hGSubrsUsed = new HashMap();
    ArrayList lGSubrsUsed = new ArrayList();
    HashMap hSubrsUsedNonCID = new HashMap();
    ArrayList lSubrsUsedNonCID = new ArrayList();
    byte[][] NewLSubrsIndex;
    byte[] NewSubrsIndexNonCID;
    byte[] NewGSubrsIndex;
    byte[] NewCharStringsIndex;
    int GBias = 0;
    LinkedList OutputList;
    int NumOfHints = 0;

    public CFFFontSubset(RandomAccessFileOrArray rf, HashMap GlyphsUsed) {
        super(rf);
        this.GlyphsUsed = GlyphsUsed;
        this.glyphsInList = new ArrayList(GlyphsUsed.keySet());
        for (int i = 0; i < this.fonts.length; ++i) {
            this.seek(this.fonts[i].charstringsOffset);
            this.fonts[i].nglyphs = this.getCard16();
            this.seek(this.stringIndexOffset);
            this.fonts[i].nstrings = this.getCard16() + CFFFont.standardStrings.length;
            this.fonts[i].charstringsOffsets = this.getIndex(this.fonts[i].charstringsOffset);
            if (this.fonts[i].fdselectOffset >= 0) {
                this.readFDSelect(i);
                this.BuildFDArrayUsed(i);
            }
            if (this.fonts[i].isCID) {
                this.ReadFDArray(i);
            }
            this.fonts[i].CharsetLength = this.CountCharset(this.fonts[i].charsetOffset, this.fonts[i].nglyphs);
        }
    }

    int CountCharset(int Offset, int NumofGlyphs) {
        int Length = 0;
        this.seek(Offset);
        char format = this.getCard8();
        switch (format) {
            case '\u0000': {
                Length = 1 + 2 * NumofGlyphs;
                break;
            }
            case '\u0001': {
                Length = 1 + 3 * this.CountRange(NumofGlyphs, 1);
                break;
            }
            case '\u0002': {
                Length = 1 + 4 * this.CountRange(NumofGlyphs, 2);
                break;
            }
        }
        return Length;
    }

    int CountRange(int NumofGlyphs, int Type) {
        char nLeft;
        int num = 0;
        for (int i = 1; i < NumofGlyphs; i+=nLeft + '\u0001') {
            ++num;
            char Sid = this.getCard16();
            nLeft = Type == 1 ? this.getCard8() : this.getCard16();
        }
        return num;
    }

    protected void readFDSelect(int Font) {
        int NumOfGlyphs = this.fonts[Font].nglyphs;
        int[] FDSelect = new int[NumOfGlyphs];
        this.seek(this.fonts[Font].fdselectOffset);
        this.fonts[Font].FDSelectFormat = this.getCard8();
        switch (this.fonts[Font].FDSelectFormat) {
            case 0: {
                for (int i = 0; i < NumOfGlyphs; ++i) {
                    FDSelect[i] = this.getCard8();
                }
                this.fonts[Font].FDSelectLength = this.fonts[Font].nglyphs + 1;
                break;
            }
            case 3: {
                int nRanges = this.getCard16();
                int l = 0;
                char first = this.getCard16();
                for (int i = 0; i < nRanges; ++i) {
                    char fd = this.getCard8();
                    char last = this.getCard16();
                    int steps = last - first;
                    for (int k = 0; k < steps; ++k) {
                        FDSelect[l] = fd;
                        ++l;
                    }
                    first = last;
                }
                this.fonts[Font].FDSelectLength = 3 + nRanges * 3 + 2;
                break;
            }
        }
        this.fonts[Font].FDSelect = FDSelect;
    }

    protected void BuildFDArrayUsed(int Font) {
        int[] FDSelect = this.fonts[Font].FDSelect;
        for (int i = 0; i < this.glyphsInList.size(); ++i) {
            int glyph = (Integer)this.glyphsInList.get(i);
            int FD = FDSelect[glyph];
            this.FDArrayUsed.put(new Integer(FD), null);
        }
    }

    protected void ReadFDArray(int Font) {
        this.seek(this.fonts[Font].fdarrayOffset);
        this.fonts[Font].FDArrayCount = this.getCard16();
        this.fonts[Font].FDArrayOffsize = this.getCard8();
        if (this.fonts[Font].FDArrayOffsize < 4) {
            ++this.fonts[Font].FDArrayOffsize;
        }
        this.fonts[Font].FDArrayOffsets = this.getIndex(this.fonts[Font].fdarrayOffset);
    }

    public byte[] Process(String fontName) throws IOException {
        byte[] arrby;
        int j;
        byte[] Ret;
        block9 : {
            byte[] arrby2;
            try {
                this.buf.reOpen();
                for (j = 0; j < this.fonts.length; ++j) {
                    if (fontName.equals(this.fonts[j].name)) break;
                }
                if (j != this.fonts.length) break block9;
                arrby2 = null;
                Object var4_5 = null;
            }
            catch (Throwable var5_12) {
                Object var4_7 = null;
                try {
                    this.buf.close();
                }
                catch (Exception e) {
                    // empty catch block
                }
                throw var5_12;
            }
            try {
                this.buf.close();
            }
            catch (Exception e) {
                // empty catch block
            }
            return arrby2;
        }
        if (this.gsubrIndexOffset >= 0) {
            this.GBias = this.CalcBias(this.gsubrIndexOffset, j);
        }
        this.BuildNewCharString(j);
        this.BuildNewLGSubrs(j);
        arrby = Ret = this.BuildNewFile(j);
        Object var4_6 = null;
        try {
            this.buf.close();
        }
        catch (Exception e) {
            // empty catch block
        }
        return arrby;
    }

    protected int CalcBias(int Offset, int Font) {
        this.seek(Offset);
        char nSubrs = this.getCard16();
        if (this.fonts[Font].CharstringType == 1) {
            return 0;
        }
        if (nSubrs < '\u04d8') {
            return 107;
        }
        if (nSubrs < '\u846c') {
            return 1131;
        }
        return 32768;
    }

    protected void BuildNewCharString(int FontIndex) throws IOException {
        this.NewCharStringsIndex = this.BuildNewIndex(this.fonts[FontIndex].charstringsOffsets, this.GlyphsUsed);
    }

    protected void BuildNewLGSubrs(int Font) throws IOException {
        if (this.fonts[Font].isCID) {
            this.hSubrsUsed = new HashMap[this.fonts[Font].fdprivateOffsets.length];
            this.lSubrsUsed = new ArrayList[this.fonts[Font].fdprivateOffsets.length];
            this.NewLSubrsIndex = new byte[this.fonts[Font].fdprivateOffsets.length][];
            this.fonts[Font].PrivateSubrsOffset = new int[this.fonts[Font].fdprivateOffsets.length];
            this.fonts[Font].PrivateSubrsOffsetsArray = new int[this.fonts[Font].fdprivateOffsets.length][];
            ArrayList FDInList = new ArrayList(this.FDArrayUsed.keySet());
            for (int j = 0; j < FDInList.size(); ++j) {
                int FD = (Integer)FDInList.get(j);
                this.hSubrsUsed[FD] = new HashMap();
                this.lSubrsUsed[FD] = new ArrayList();
                this.BuildFDSubrsOffsets(Font, FD);
                if (this.fonts[Font].PrivateSubrsOffset[FD] < 0) continue;
                this.BuildSubrUsed(Font, FD, this.fonts[Font].PrivateSubrsOffset[FD], this.fonts[Font].PrivateSubrsOffsetsArray[FD], this.hSubrsUsed[FD], this.lSubrsUsed[FD]);
                this.NewLSubrsIndex[FD] = this.BuildNewIndex(this.fonts[Font].PrivateSubrsOffsetsArray[FD], this.hSubrsUsed[FD]);
            }
        } else if (this.fonts[Font].privateSubrs >= 0) {
            this.fonts[Font].SubrsOffsets = this.getIndex(this.fonts[Font].privateSubrs);
            this.BuildSubrUsed(Font, -1, this.fonts[Font].privateSubrs, this.fonts[Font].SubrsOffsets, this.hSubrsUsedNonCID, this.lSubrsUsedNonCID);
        }
        this.BuildGSubrsUsed(Font);
        if (this.fonts[Font].privateSubrs >= 0) {
            this.NewSubrsIndexNonCID = this.BuildNewIndex(this.fonts[Font].SubrsOffsets, this.hSubrsUsedNonCID);
        }
        this.NewGSubrsIndex = this.BuildNewIndex(this.gsubrOffsets, this.hGSubrsUsed);
    }

    protected void BuildFDSubrsOffsets(int Font, int FD) {
        this.fonts[Font].PrivateSubrsOffset[FD] = -1;
        this.seek(this.fonts[Font].fdprivateOffsets[FD]);
        while (this.getPosition() < this.fonts[Font].fdprivateOffsets[FD] + this.fonts[Font].fdprivateLengths[FD]) {
            this.getDictItem();
            if (this.key != "Subrs") continue;
            this.fonts[Font].PrivateSubrsOffset[FD] = (Integer)this.args[0] + this.fonts[Font].fdprivateOffsets[FD];
        }
        if (this.fonts[Font].PrivateSubrsOffset[FD] >= 0) {
            this.fonts[Font].PrivateSubrsOffsetsArray[FD] = this.getIndex(this.fonts[Font].PrivateSubrsOffset[FD]);
        }
    }

    protected void BuildSubrUsed(int Font, int FD, int SubrOffset, int[] SubrsOffsets, HashMap hSubr, ArrayList lSubr) {
        int i;
        int End;
        int Start;
        int LBias = this.CalcBias(SubrOffset, Font);
        for (i = 0; i < this.glyphsInList.size(); ++i) {
            int glyph = (Integer)this.glyphsInList.get(i);
            Start = this.fonts[Font].charstringsOffsets[glyph];
            End = this.fonts[Font].charstringsOffsets[glyph + 1];
            if (FD >= 0) {
                this.EmptyStack();
                this.NumOfHints = 0;
                int GlyphFD = this.fonts[Font].FDSelect[glyph];
                if (GlyphFD != FD) continue;
                this.ReadASubr(Start, End, this.GBias, LBias, hSubr, lSubr, SubrsOffsets);
                continue;
            }
            this.ReadASubr(Start, End, this.GBias, LBias, hSubr, lSubr, SubrsOffsets);
        }
        for (i = 0; i < lSubr.size(); ++i) {
            int Subr = (Integer)lSubr.get(i);
            if (Subr >= SubrsOffsets.length - 1 || Subr < 0) continue;
            Start = SubrsOffsets[Subr];
            End = SubrsOffsets[Subr + 1];
            this.ReadASubr(Start, End, this.GBias, LBias, hSubr, lSubr, SubrsOffsets);
        }
    }

    protected void BuildGSubrsUsed(int Font) {
        int LBias = 0;
        int SizeOfNonCIDSubrsUsed = 0;
        if (this.fonts[Font].privateSubrs >= 0) {
            LBias = this.CalcBias(this.fonts[Font].privateSubrs, Font);
            SizeOfNonCIDSubrsUsed = this.lSubrsUsedNonCID.size();
        }
        for (int i = 0; i < this.lGSubrsUsed.size(); ++i) {
            int Subr = (Integer)this.lGSubrsUsed.get(i);
            if (Subr >= this.gsubrOffsets.length - 1 || Subr < 0) continue;
            int Start = this.gsubrOffsets[Subr];
            int End = this.gsubrOffsets[Subr + 1];
            if (this.fonts[Font].isCID) {
                this.ReadASubr(Start, End, this.GBias, 0, this.hGSubrsUsed, this.lGSubrsUsed, null);
                continue;
            }
            this.ReadASubr(Start, End, this.GBias, LBias, this.hSubrsUsedNonCID, this.lSubrsUsedNonCID, this.fonts[Font].SubrsOffsets);
            if (SizeOfNonCIDSubrsUsed >= this.lSubrsUsedNonCID.size()) continue;
            for (int j = SizeOfNonCIDSubrsUsed; j < this.lSubrsUsedNonCID.size(); ++j) {
                int LSubr = (Integer)this.lSubrsUsedNonCID.get(j);
                if (LSubr >= this.fonts[Font].SubrsOffsets.length - 1 || LSubr < 0) continue;
                int LStart = this.fonts[Font].SubrsOffsets[LSubr];
                int LEnd = this.fonts[Font].SubrsOffsets[LSubr + 1];
                this.ReadASubr(LStart, LEnd, this.GBias, LBias, this.hSubrsUsedNonCID, this.lSubrsUsedNonCID, this.fonts[Font].SubrsOffsets);
            }
            SizeOfNonCIDSubrsUsed = this.lSubrsUsedNonCID.size();
        }
    }

    protected void ReadASubr(int begin, int end, int GBias, int LBias, HashMap hSubr, ArrayList lSubr, int[] LSubrsOffsets) {
        this.EmptyStack();
        this.NumOfHints = 0;
        this.seek(begin);
        while (this.getPosition() < end) {
            int Subr;
            this.ReadCommand();
            int pos = this.getPosition();
            Object TopElement = null;
            if (this.arg_count > 0) {
                TopElement = this.args[this.arg_count - 1];
            }
            int NumOfArgs = this.arg_count;
            this.HandelStack();
            if (this.key == "callsubr") {
                if (NumOfArgs <= 0) continue;
                Subr = (Integer)TopElement + LBias;
                if (!hSubr.containsKey(new Integer(Subr))) {
                    hSubr.put(new Integer(Subr), null);
                    lSubr.add(new Integer(Subr));
                }
                this.CalcHints(LSubrsOffsets[Subr], LSubrsOffsets[Subr + 1], LBias, GBias, LSubrsOffsets);
                this.seek(pos);
                continue;
            }
            if (this.key == "callgsubr") {
                if (NumOfArgs <= 0) continue;
                Subr = (Integer)TopElement + GBias;
                if (!this.hGSubrsUsed.containsKey(new Integer(Subr))) {
                    this.hGSubrsUsed.put(new Integer(Subr), null);
                    this.lGSubrsUsed.add(new Integer(Subr));
                }
                this.CalcHints(this.gsubrOffsets[Subr], this.gsubrOffsets[Subr + 1], LBias, GBias, LSubrsOffsets);
                this.seek(pos);
                continue;
            }
            if (this.key == "hstem" || this.key == "vstem" || this.key == "hstemhm" || this.key == "vstemhm") {
                this.NumOfHints+=NumOfArgs / 2;
                continue;
            }
            if (this.key != "hintmask" && this.key != "cntrmask") continue;
            int SizeOfMask = this.NumOfHints / 8;
            if (this.NumOfHints % 8 != 0 || SizeOfMask == 0) {
                ++SizeOfMask;
            }
            for (int i = 0; i < SizeOfMask; ++i) {
                this.getCard8();
            }
        }
    }

    protected void HandelStack() {
        int StackHandel = this.StackOpp();
        if (StackHandel < 2) {
            if (StackHandel == 1) {
                this.PushStack();
            } else {
                for (int i = 0; i < (StackHandel*=-1); ++i) {
                    this.PopStack();
                }
            }
        } else {
            this.EmptyStack();
        }
    }

    protected int StackOpp() {
        if (this.key == "ifelse") {
            return -3;
        }
        if (this.key == "roll" || this.key == "put") {
            return -2;
        }
        if (this.key == "callsubr" || this.key == "callgsubr" || this.key == "add" || this.key == "sub" || this.key == "div" || this.key == "mul" || this.key == "drop" || this.key == "and" || this.key == "or" || this.key == "eq") {
            return -1;
        }
        if (this.key == "abs" || this.key == "neg" || this.key == "sqrt" || this.key == "exch" || this.key == "index" || this.key == "get" || this.key == "not" || this.key == "return") {
            return 0;
        }
        if (this.key == "random" || this.key == "dup") {
            return 1;
        }
        return 2;
    }

    protected void EmptyStack() {
        for (int i = 0; i < this.arg_count; ++i) {
            this.args[i] = null;
        }
        this.arg_count = 0;
    }

    protected void PopStack() {
        if (this.arg_count > 0) {
            this.args[this.arg_count - 1] = null;
            --this.arg_count;
        }
    }

    protected void PushStack() {
        ++this.arg_count;
    }

    protected void ReadCommand() {
        this.key = null;
        boolean gotKey = false;
        while (!gotKey) {
            char second;
            char first;
            char w;
            char b0 = this.getCard8();
            if (b0 == '\u001c') {
                first = this.getCard8();
                second = this.getCard8();
                this.args[this.arg_count] = new Integer(first << 8 | second);
                ++this.arg_count;
                continue;
            }
            if (b0 >= ' ' && b0 <= '\u00f6') {
                this.args[this.arg_count] = new Integer(b0 - 139);
                ++this.arg_count;
                continue;
            }
            if (b0 >= '\u00f7' && b0 <= '\u00fa') {
                w = this.getCard8();
                this.args[this.arg_count] = new Integer((b0 - 247) * 256 + w + 108);
                ++this.arg_count;
                continue;
            }
            if (b0 >= '\u00fb' && b0 <= '\u00fe') {
                w = this.getCard8();
                this.args[this.arg_count] = new Integer((- b0 - 251) * 256 - w - 108);
                ++this.arg_count;
                continue;
            }
            if (b0 == '\u00ff') {
                first = this.getCard8();
                second = this.getCard8();
                char third = this.getCard8();
                char fourth = this.getCard8();
                this.args[this.arg_count] = new Integer(first << 24 | second << 16 | third << 8 | fourth);
                ++this.arg_count;
                continue;
            }
            if (b0 > '\u001f' || b0 == '\u001c') continue;
            gotKey = true;
            if (b0 == '\f') {
                int b1 = this.getCard8();
                if (b1 > SubrsEscapeFuncs.length - 1) {
                    b1 = SubrsEscapeFuncs.length - 1;
                }
                this.key = SubrsEscapeFuncs[b1];
                continue;
            }
            this.key = SubrsFunctions[b0];
        }
    }

    protected int CalcHints(int begin, int end, int LBias, int GBias, int[] LSubrsOffsets) {
        this.seek(begin);
        while (this.getPosition() < end) {
            int Subr;
            this.ReadCommand();
            int pos = this.getPosition();
            Object TopElement = null;
            if (this.arg_count > 0) {
                TopElement = this.args[this.arg_count - 1];
            }
            int NumOfArgs = this.arg_count;
            this.HandelStack();
            if (this.key == "callsubr") {
                if (NumOfArgs <= 0) continue;
                Subr = (Integer)TopElement + LBias;
                this.CalcHints(LSubrsOffsets[Subr], LSubrsOffsets[Subr + 1], LBias, GBias, LSubrsOffsets);
                this.seek(pos);
                continue;
            }
            if (this.key == "callgsubr") {
                if (NumOfArgs <= 0) continue;
                Subr = (Integer)TopElement + GBias;
                this.CalcHints(this.gsubrOffsets[Subr], this.gsubrOffsets[Subr + 1], LBias, GBias, LSubrsOffsets);
                this.seek(pos);
                continue;
            }
            if (this.key == "hstem" || this.key == "vstem" || this.key == "hstemhm" || this.key == "vstemhm") {
                this.NumOfHints+=NumOfArgs / 2;
                continue;
            }
            if (this.key != "hintmask" && this.key != "cntrmask") continue;
            int SizeOfMask = this.NumOfHints / 8;
            if (this.NumOfHints % 8 != 0 || SizeOfMask == 0) {
                ++SizeOfMask;
            }
            for (int i = 0; i < SizeOfMask; ++i) {
                this.getCard8();
            }
        }
        return this.NumOfHints;
    }

    protected byte[] BuildNewIndex(int[] Offsets, HashMap Used) throws IOException {
        int Offset = 0;
        int[] NewOffsets = new int[Offsets.length];
        for (int i = 0; i < Offsets.length; ++i) {
            NewOffsets[i] = Offset;
            if (!Used.containsKey(new Integer(i))) continue;
            Offset+=Offsets[i + 1] - Offsets[i];
        }
        byte[] NewObjects = new byte[Offset];
        for (int i2 = 0; i2 < Offsets.length - 1; ++i2) {
            int start = NewOffsets[i2];
            int end = NewOffsets[i2 + 1];
            if (start == end) continue;
            this.buf.seek(Offsets[i2]);
            this.buf.readFully(NewObjects, start, end - start);
        }
        return this.AssembleIndex(NewOffsets, NewObjects);
    }

    protected byte[] AssembleIndex(int[] NewOffsets, byte[] NewObjects) {
        int i;
        char Count = (char)(NewOffsets.length - 1);
        int Size = NewOffsets[NewOffsets.length - 1];
        int Offsize = Size <= 255 ? 1 : (Size <= 65535 ? 2 : (Size <= 16777215 ? 3 : 4));
        byte[] NewIndex = new byte[3 + Offsize * (Count + '\u0001') + NewObjects.length];
        int Place = 0;
        NewIndex[Place++] = (byte)(Count >>> 8 & 255);
        NewIndex[Place++] = (byte)(Count >>> '\u0000' & 255);
        NewIndex[Place++] = Offsize;
        for (i = 0; i < NewOffsets.length; ++i) {
            int Num = NewOffsets[i] - NewOffsets[0] + 1;
            switch (Offsize) {
                case 4: {
                    NewIndex[Place++] = (byte)(Num >>> 24 & 255);
                }
                case 3: {
                    NewIndex[Place++] = (byte)(Num >>> 16 & 255);
                }
                case 2: {
                    NewIndex[Place++] = (byte)(Num >>> 8 & 255);
                }
                case 1: {
                    NewIndex[Place++] = (byte)(Num >>> 0 & 255);
                }
            }
        }
        for (i = 0; i < NewObjects.length; ++i) {
            NewIndex[Place++] = NewObjects[i];
        }
        return NewIndex;
    }

    protected byte[] BuildNewFile(int Font) throws IOException {
        CFFFont.Item item;
        this.OutputList = new LinkedList();
        this.CopyHeader();
        this.BuildIndexHeader(1, 1, 1);
        this.OutputList.addLast(new CFFFont.UInt8Item((char)(1 + this.fonts[Font].name.length())));
        this.OutputList.addLast(new CFFFont.StringItem(this.fonts[Font].name));
        this.BuildIndexHeader(1, 2, 1);
        CFFFont.IndexOffsetItem topdictIndex1Ref = new CFFFont.IndexOffsetItem(2);
        this.OutputList.addLast(topdictIndex1Ref);
        CFFFont.IndexBaseItem topdictBase = new CFFFont.IndexBaseItem();
        this.OutputList.addLast(topdictBase);
        CFFFont.DictOffsetItem charsetRef = new CFFFont.DictOffsetItem();
        CFFFont.DictOffsetItem charstringsRef = new CFFFont.DictOffsetItem();
        CFFFont.DictOffsetItem fdarrayRef = new CFFFont.DictOffsetItem();
        CFFFont.DictOffsetItem fdselectRef = new CFFFont.DictOffsetItem();
        CFFFont.DictOffsetItem privateRef = new CFFFont.DictOffsetItem();
        if (!this.fonts[Font].isCID) {
            this.OutputList.addLast(new CFFFont.DictNumberItem(this.fonts[Font].nstrings));
            this.OutputList.addLast(new CFFFont.DictNumberItem(this.fonts[Font].nstrings + 1));
            this.OutputList.addLast(new CFFFont.DictNumberItem(0));
            this.OutputList.addLast(new CFFFont.UInt8Item('\f'));
            this.OutputList.addLast(new CFFFont.UInt8Item('\u001e'));
            this.OutputList.addLast(new CFFFont.DictNumberItem(this.fonts[Font].nglyphs));
            this.OutputList.addLast(new CFFFont.UInt8Item('\f'));
            this.OutputList.addLast(new CFFFont.UInt8Item('\"'));
        }
        this.seek(this.topdictOffsets[Font]);
        while (this.getPosition() < this.topdictOffsets[Font + 1]) {
            int p1 = this.getPosition();
            this.getDictItem();
            int p2 = this.getPosition();
            if (this.key == "Encoding" || this.key == "Private" || this.key == "FDSelect" || this.key == "FDArray" || this.key == "charset" || this.key == "CharStrings") continue;
            this.OutputList.add(new CFFFont.RangeItem(this.buf, p1, p2 - p1));
        }
        this.CreateKeys(fdarrayRef, fdselectRef, charsetRef, charstringsRef);
        this.OutputList.addLast(new CFFFont.IndexMarkerItem(topdictIndex1Ref, topdictBase));
        if (this.fonts[Font].isCID) {
            this.OutputList.addLast(this.getEntireIndexRange(this.stringIndexOffset));
        } else {
            this.CreateNewStringIndex(Font);
        }
        this.OutputList.addLast(new CFFFont.RangeItem(new RandomAccessFileOrArray(this.NewGSubrsIndex), 0, this.NewGSubrsIndex.length));
        if (this.fonts[Font].isCID) {
            this.OutputList.addLast(new CFFFont.MarkerItem(fdselectRef));
            if (this.fonts[Font].fdselectOffset >= 0) {
                this.OutputList.addLast(new CFFFont.RangeItem(this.buf, this.fonts[Font].fdselectOffset, this.fonts[Font].FDSelectLength));
            } else {
                this.CreateFDSelect(fdselectRef, this.fonts[Font].nglyphs);
            }
            this.OutputList.addLast(new CFFFont.MarkerItem(charsetRef));
            this.OutputList.addLast(new CFFFont.RangeItem(this.buf, this.fonts[Font].charsetOffset, this.fonts[Font].CharsetLength));
            if (this.fonts[Font].fdarrayOffset >= 0) {
                this.OutputList.addLast(new CFFFont.MarkerItem(fdarrayRef));
                this.Reconstruct(Font);
            } else {
                this.CreateFDArray(fdarrayRef, privateRef, Font);
            }
        } else {
            this.CreateFDSelect(fdselectRef, this.fonts[Font].nglyphs);
            this.CreateCharset(charsetRef, this.fonts[Font].nglyphs);
            this.CreateFDArray(fdarrayRef, privateRef, Font);
        }
        if (this.fonts[Font].privateOffset >= 0) {
            CFFFont.IndexBaseItem PrivateBase = new CFFFont.IndexBaseItem();
            this.OutputList.addLast(PrivateBase);
            this.OutputList.addLast(new CFFFont.MarkerItem(privateRef));
            CFFFont.DictOffsetItem Subr = new CFFFont.DictOffsetItem();
            this.CreateNonCIDPrivate(Font, Subr);
            this.CreateNonCIDSubrs(Font, PrivateBase, Subr);
        }
        this.OutputList.addLast(new CFFFont.MarkerItem(charstringsRef));
        this.OutputList.addLast(new CFFFont.RangeItem(new RandomAccessFileOrArray(this.NewCharStringsIndex), 0, this.NewCharStringsIndex.length));
        int[] currentOffset = new int[]{0};
        Iterator listIter = this.OutputList.iterator();
        while (listIter.hasNext()) {
            item = (CFFFont.Item)listIter.next();
            item.increment(currentOffset);
        }
        listIter = this.OutputList.iterator();
        while (listIter.hasNext()) {
            item = (CFFFont.Item)listIter.next();
            item.xref();
        }
        int size = currentOffset[0];
        byte[] b = new byte[size];
        listIter = this.OutputList.iterator();
        while (listIter.hasNext()) {
            CFFFont.Item item2 = (CFFFont.Item)listIter.next();
            item2.emit(b);
        }
        return b;
    }

    protected void CopyHeader() {
        this.seek(0);
        char major = this.getCard8();
        char minor = this.getCard8();
        char hdrSize = this.getCard8();
        char offSize = this.getCard8();
        this.nextIndexOffset = hdrSize;
        this.OutputList.addLast(new CFFFont.RangeItem(this.buf, 0, hdrSize));
    }

    protected void BuildIndexHeader(int Count, int Offsize, int First) {
        this.OutputList.addLast(new CFFFont.UInt16Item((char)Count));
        this.OutputList.addLast(new CFFFont.UInt8Item((char)Offsize));
        switch (Offsize) {
            case 1: {
                this.OutputList.addLast(new CFFFont.UInt8Item((char)First));
                break;
            }
            case 2: {
                this.OutputList.addLast(new CFFFont.UInt16Item((char)First));
                break;
            }
            case 3: {
                this.OutputList.addLast(new CFFFont.UInt24Item((char)First));
                break;
            }
            case 4: {
                this.OutputList.addLast(new CFFFont.UInt32Item((char)First));
                break;
            }
        }
    }

    protected void CreateKeys(CFFFont.OffsetItem fdarrayRef, CFFFont.OffsetItem fdselectRef, CFFFont.OffsetItem charsetRef, CFFFont.OffsetItem charstringsRef) {
        this.OutputList.addLast(fdarrayRef);
        this.OutputList.addLast(new CFFFont.UInt8Item('\f'));
        this.OutputList.addLast(new CFFFont.UInt8Item('$'));
        this.OutputList.addLast(fdselectRef);
        this.OutputList.addLast(new CFFFont.UInt8Item('\f'));
        this.OutputList.addLast(new CFFFont.UInt8Item('%'));
        this.OutputList.addLast(charsetRef);
        this.OutputList.addLast(new CFFFont.UInt8Item('\u000f'));
        this.OutputList.addLast(charstringsRef);
        this.OutputList.addLast(new CFFFont.UInt8Item('\u0011'));
    }

    protected void CreateNewStringIndex(int Font) {
        String fdFontName = String.valueOf(this.fonts[Font].name) + "-OneRange";
        if (fdFontName.length() > 127) {
            fdFontName = fdFontName.substring(0, 127);
        }
        String extraStrings = "AdobeIdentity" + fdFontName;
        int origStringsLen = this.stringOffsets[this.stringOffsets.length - 1] - this.stringOffsets[0];
        int stringsBaseOffset = this.stringOffsets[0] - 1;
        int stringsIndexOffSize = origStringsLen + extraStrings.length() <= 255 ? 1 : (origStringsLen + extraStrings.length() <= 65535 ? 2 : (origStringsLen + extraStrings.length() <= 16777215 ? 3 : 4));
        this.OutputList.addLast(new CFFFont.UInt16Item((char)(this.stringOffsets.length - 1 + 3)));
        this.OutputList.addLast(new CFFFont.UInt8Item((char)stringsIndexOffSize));
        for (int i = 0; i < this.stringOffsets.length; ++i) {
            this.OutputList.addLast(new CFFFont.IndexOffsetItem(stringsIndexOffSize, this.stringOffsets[i] - stringsBaseOffset));
        }
        int currentStringsOffset = this.stringOffsets[this.stringOffsets.length - 1] - stringsBaseOffset;
        this.OutputList.addLast(new CFFFont.IndexOffsetItem(stringsIndexOffSize, currentStringsOffset+="Adobe".length()));
        this.OutputList.addLast(new CFFFont.IndexOffsetItem(stringsIndexOffSize, currentStringsOffset+="Identity".length()));
        this.OutputList.addLast(new CFFFont.IndexOffsetItem(stringsIndexOffSize, currentStringsOffset+=fdFontName.length()));
        this.OutputList.addLast(new CFFFont.RangeItem(this.buf, this.stringOffsets[0], origStringsLen));
        this.OutputList.addLast(new CFFFont.StringItem(extraStrings));
    }

    protected void CreateFDSelect(CFFFont.OffsetItem fdselectRef, int nglyphs) {
        this.OutputList.addLast(new CFFFont.MarkerItem(fdselectRef));
        this.OutputList.addLast(new CFFFont.UInt8Item('\u0003'));
        this.OutputList.addLast(new CFFFont.UInt16Item('\u0001'));
        this.OutputList.addLast(new CFFFont.UInt16Item('\u0000'));
        this.OutputList.addLast(new CFFFont.UInt8Item('\u0000'));
        this.OutputList.addLast(new CFFFont.UInt16Item((char)nglyphs));
    }

    protected void CreateCharset(CFFFont.OffsetItem charsetRef, int nglyphs) {
        this.OutputList.addLast(new CFFFont.MarkerItem(charsetRef));
        this.OutputList.addLast(new CFFFont.UInt8Item('\u0002'));
        this.OutputList.addLast(new CFFFont.UInt16Item('\u0001'));
        this.OutputList.addLast(new CFFFont.UInt16Item((char)(nglyphs - 1)));
    }

    protected void CreateFDArray(CFFFont.OffsetItem fdarrayRef, CFFFont.OffsetItem privateRef, int Font) {
        this.OutputList.addLast(new CFFFont.MarkerItem(fdarrayRef));
        this.BuildIndexHeader(1, 1, 1);
        CFFFont.IndexOffsetItem privateIndex1Ref = new CFFFont.IndexOffsetItem(1);
        this.OutputList.addLast(privateIndex1Ref);
        CFFFont.IndexBaseItem privateBase = new CFFFont.IndexBaseItem();
        this.OutputList.addLast(privateBase);
        int NewSize = this.fonts[Font].privateLength;
        int OrgSubrsOffsetSize = this.CalcSubrOffsetSize(this.fonts[Font].privateOffset, this.fonts[Font].privateLength);
        if (OrgSubrsOffsetSize != 0) {
            NewSize+=5 - OrgSubrsOffsetSize;
        }
        this.OutputList.addLast(new CFFFont.DictNumberItem(NewSize));
        this.OutputList.addLast(privateRef);
        this.OutputList.addLast(new CFFFont.UInt8Item('\u0012'));
        this.OutputList.addLast(new CFFFont.IndexMarkerItem(privateIndex1Ref, privateBase));
    }

    void Reconstruct(int Font) throws IOException {
        CFFFont.OffsetItem[] fdPrivate = new CFFFont.DictOffsetItem[this.fonts[Font].FDArrayOffsets.length - 1];
        CFFFont.IndexBaseItem[] fdPrivateBase = new CFFFont.IndexBaseItem[this.fonts[Font].fdprivateOffsets.length];
        CFFFont.OffsetItem[] fdSubrs = new CFFFont.DictOffsetItem[this.fonts[Font].fdprivateOffsets.length];
        this.ReconstructFDArray(Font, fdPrivate);
        this.ReconstructPrivateDict(Font, fdPrivate, fdPrivateBase, fdSubrs);
        this.ReconstructPrivateSubrs(Font, fdPrivateBase, fdSubrs);
    }

    void ReconstructFDArray(int Font, CFFFont.OffsetItem[] fdPrivate) throws IOException {
        this.BuildIndexHeader(this.fonts[Font].FDArrayCount, this.fonts[Font].FDArrayOffsize, 1);
        CFFFont.IndexOffsetItem[] fdOffsets = new CFFFont.IndexOffsetItem[this.fonts[Font].FDArrayOffsets.length - 1];
        for (int i = 0; i < this.fonts[Font].FDArrayOffsets.length - 1; ++i) {
            fdOffsets[i] = new CFFFont.IndexOffsetItem(this.fonts[Font].FDArrayOffsize);
            this.OutputList.addLast(fdOffsets[i]);
        }
        CFFFont.IndexBaseItem fdArrayBase = new CFFFont.IndexBaseItem();
        this.OutputList.addLast(fdArrayBase);
        for (int k = 0; k < this.fonts[Font].FDArrayOffsets.length - 1; ++k) {
            if (this.FDArrayUsed.containsKey(new Integer(k))) {
                this.seek(this.fonts[Font].FDArrayOffsets[k]);
                while (this.getPosition() < this.fonts[Font].FDArrayOffsets[k + 1]) {
                    int p1 = this.getPosition();
                    this.getDictItem();
                    int p2 = this.getPosition();
                    if (this.key == "Private") {
                        int NewSize = (Integer)this.args[0];
                        int OrgSubrsOffsetSize = this.CalcSubrOffsetSize(this.fonts[Font].fdprivateOffsets[k], this.fonts[Font].fdprivateLengths[k]);
                        if (OrgSubrsOffsetSize != 0) {
                            NewSize+=5 - OrgSubrsOffsetSize;
                        }
                        this.OutputList.addLast(new CFFFont.DictNumberItem(NewSize));
                        fdPrivate[k] = new CFFFont.DictOffsetItem();
                        this.OutputList.addLast(fdPrivate[k]);
                        this.OutputList.addLast(new CFFFont.UInt8Item('\u0012'));
                        this.seek(p2);
                        continue;
                    }
                    this.OutputList.addLast(new CFFFont.RangeItem(this.buf, p1, p2 - p1));
                }
            }
            this.OutputList.addLast(new CFFFont.IndexMarkerItem(fdOffsets[k], fdArrayBase));
        }
    }

    void ReconstructPrivateDict(int Font, CFFFont.OffsetItem[] fdPrivate, CFFFont.IndexBaseItem[] fdPrivateBase, CFFFont.OffsetItem[] fdSubrs) throws IOException {
        for (int i = 0; i < this.fonts[Font].fdprivateOffsets.length; ++i) {
            if (!this.FDArrayUsed.containsKey(new Integer(i))) continue;
            this.OutputList.addLast(new CFFFont.MarkerItem(fdPrivate[i]));
            fdPrivateBase[i] = new CFFFont.IndexBaseItem();
            this.OutputList.addLast(fdPrivateBase[i]);
            this.seek(this.fonts[Font].fdprivateOffsets[i]);
            while (this.getPosition() < this.fonts[Font].fdprivateOffsets[i] + this.fonts[Font].fdprivateLengths[i]) {
                int p1 = this.getPosition();
                this.getDictItem();
                int p2 = this.getPosition();
                if (this.key == "Subrs") {
                    fdSubrs[i] = new CFFFont.DictOffsetItem();
                    this.OutputList.addLast(fdSubrs[i]);
                    this.OutputList.addLast(new CFFFont.UInt8Item('\u0013'));
                    continue;
                }
                this.OutputList.addLast(new CFFFont.RangeItem(this.buf, p1, p2 - p1));
            }
        }
    }

    void ReconstructPrivateSubrs(int Font, CFFFont.IndexBaseItem[] fdPrivateBase, CFFFont.OffsetItem[] fdSubrs) throws IOException {
        for (int i = 0; i < this.fonts[Font].fdprivateLengths.length; ++i) {
            if (fdSubrs[i] == null || this.fonts[Font].PrivateSubrsOffset[i] < 0) continue;
            this.OutputList.addLast(new CFFFont.SubrMarkerItem(fdSubrs[i], fdPrivateBase[i]));
            this.OutputList.addLast(new CFFFont.RangeItem(new RandomAccessFileOrArray(this.NewLSubrsIndex[i]), 0, this.NewLSubrsIndex[i].length));
        }
    }

    int CalcSubrOffsetSize(int Offset, int Size) {
        int OffsetSize = 0;
        this.seek(Offset);
        while (this.getPosition() < Offset + Size) {
            int p1 = this.getPosition();
            this.getDictItem();
            int p2 = this.getPosition();
            if (this.key != "Subrs") continue;
            OffsetSize = p2 - p1 - 1;
        }
        return OffsetSize;
    }

    protected int countEntireIndexRange(int indexOffset) {
        this.seek(indexOffset);
        char count = this.getCard16();
        if (count == '\u0000') {
            return 2;
        }
        char indexOffSize = this.getCard8();
        this.seek(indexOffset + 2 + 1 + count * indexOffSize);
        int size = this.getOffset(indexOffSize) - 1;
        return 3 + (count + '\u0001') * indexOffSize + size;
    }

    void CreateNonCIDPrivate(int Font, CFFFont.OffsetItem Subr) {
        this.seek(this.fonts[Font].privateOffset);
        while (this.getPosition() < this.fonts[Font].privateOffset + this.fonts[Font].privateLength) {
            int p1 = this.getPosition();
            this.getDictItem();
            int p2 = this.getPosition();
            if (this.key == "Subrs") {
                this.OutputList.addLast(Subr);
                this.OutputList.addLast(new CFFFont.UInt8Item('\u0013'));
                continue;
            }
            this.OutputList.addLast(new CFFFont.RangeItem(this.buf, p1, p2 - p1));
        }
    }

    void CreateNonCIDSubrs(int Font, CFFFont.IndexBaseItem PrivateBase, CFFFont.OffsetItem Subrs) throws IOException {
        this.OutputList.addLast(new CFFFont.SubrMarkerItem(Subrs, PrivateBase));
        this.OutputList.addLast(new CFFFont.RangeItem(new RandomAccessFileOrArray(this.NewSubrsIndexNonCID), 0, this.NewSubrsIndexNonCID.length));
    }
}

