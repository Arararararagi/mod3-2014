/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNameTree;
import com.lowagie.text.pdf.PdfNull;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.SimpleXMLDocHandler;
import com.lowagie.text.pdf.SimpleXMLParser;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class SimpleNamedDestination
implements SimpleXMLDocHandler {
    private HashMap xmlNames;
    private HashMap xmlLast;

    private SimpleNamedDestination() {
    }

    public static HashMap getNamedDestination(PdfReader reader, boolean fromNames) {
        IntHashtable pages = new IntHashtable();
        int numPages = reader.getNumberOfPages();
        for (int k = 1; k <= numPages; ++k) {
            pages.put(reader.getPageOrigRef(k).getNumber(), k);
        }
        HashMap names = fromNames ? reader.getNamedDestinationFromNames() : reader.getNamedDestinationFromStrings();
        Iterator it = names.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            ArrayList arr = ((PdfArray)entry.getValue()).getArrayList();
            StringBuffer s = new StringBuffer();
            try {
                s.append(pages.get(((PdfIndirectReference)arr.get(0)).getNumber()));
                s.append(' ').append(arr.get(1).toString().substring(1));
                for (int k2 = 2; k2 < arr.size(); ++k2) {
                    s.append(' ').append(arr.get(k2).toString());
                }
                entry.setValue(s.toString());
                continue;
            }
            catch (Exception e) {
                it.remove();
            }
        }
        return names;
    }

    public static void exportToXML(HashMap names, OutputStream out, String encoding, boolean onlyASCII) throws IOException {
        String jenc = SimpleXMLParser.getJavaEncoding(encoding);
        BufferedWriter wrt = new BufferedWriter(new OutputStreamWriter(out, jenc));
        SimpleNamedDestination.exportToXML(names, wrt, encoding, onlyASCII);
    }

    public static void exportToXML(HashMap names, Writer wrt, String encoding, boolean onlyASCII) throws IOException {
        wrt.write("<?xml version=\"1.0\" encoding=\"");
        wrt.write(SimpleXMLParser.escapeXML(encoding, onlyASCII));
        wrt.write("\"?>\n<Destination>\n");
        Iterator it = names.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            String key = (String)entry.getKey();
            String value = (String)entry.getValue();
            wrt.write("  <Name Page=\"");
            wrt.write(SimpleXMLParser.escapeXML(value, onlyASCII));
            wrt.write("\">");
            wrt.write(SimpleXMLParser.escapeXML(SimpleNamedDestination.escapeBinaryString(key), onlyASCII));
            wrt.write("</Name>\n");
        }
        wrt.write("</Destination>\n");
        wrt.flush();
    }

    public static HashMap importFromXML(InputStream in) throws IOException {
        SimpleNamedDestination names = new SimpleNamedDestination();
        SimpleXMLParser.parse((SimpleXMLDocHandler)names, in);
        return names.xmlNames;
    }

    public static HashMap importFromXML(Reader in) throws IOException {
        SimpleNamedDestination names = new SimpleNamedDestination();
        SimpleXMLParser.parse((SimpleXMLDocHandler)names, in);
        return names.xmlNames;
    }

    static PdfArray createDestinationArray(String value, PdfWriter writer) throws IOException {
        PdfArray ar = new PdfArray();
        StringTokenizer tk = new StringTokenizer(value);
        int n = Integer.parseInt(tk.nextToken());
        ar.add(writer.getPageReference(n));
        if (!tk.hasMoreTokens()) {
            ar.add(PdfName.XYZ);
            ar.add(new float[]{0.0f, 10000.0f, 0.0f});
        } else {
            String fn = tk.nextToken();
            if (fn.startsWith("/")) {
                fn = fn.substring(1);
            }
            ar.add(new PdfName(fn));
            for (int k = 0; k < 4 && tk.hasMoreTokens(); ++k) {
                fn = tk.nextToken();
                if (fn.equals("null")) {
                    ar.add(PdfNull.PDFNULL);
                    continue;
                }
                ar.add(new PdfNumber(fn));
            }
        }
        return ar;
    }

    public static PdfDictionary outputNamedDestinationAsNames(HashMap names, PdfWriter writer) throws IOException {
        PdfDictionary dic = new PdfDictionary();
        Iterator it = names.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            try {
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();
                PdfArray ar = SimpleNamedDestination.createDestinationArray(value, writer);
                PdfName kn = new PdfName(key);
                dic.put(kn, ar);
                continue;
            }
            catch (Exception key) {
                // empty catch block
            }
        }
        return dic;
    }

    public static PdfDictionary outputNamedDestinationAsStrings(HashMap names, PdfWriter writer) throws IOException {
        HashMap n2 = new HashMap(names);
        Iterator it = n2.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            try {
                String value = (String)entry.getValue();
                PdfArray ar = SimpleNamedDestination.createDestinationArray(value, writer);
                entry.setValue(writer.addToBody(ar).getIndirectReference());
                continue;
            }
            catch (Exception e) {
                it.remove();
            }
        }
        return PdfNameTree.writeTree(n2, writer);
    }

    public static String escapeBinaryString(String s) {
        StringBuffer buf = new StringBuffer();
        char[] cc = s.toCharArray();
        int len = cc.length;
        for (int k = 0; k < len; ++k) {
            char c = cc[k];
            if (c < ' ') {
                buf.append('\\');
                String octal = "00" + Integer.toOctalString(c);
                buf.append(octal.substring(octal.length() - 3));
                continue;
            }
            if (c == '\\') {
                buf.append("\\\\");
                continue;
            }
            buf.append(c);
        }
        return buf.toString();
    }

    public static String unEscapeBinaryString(String s) {
        StringBuffer buf = new StringBuffer();
        char[] cc = s.toCharArray();
        int len = cc.length;
        for (int k = 0; k < len; ++k) {
            char c = cc[k];
            if (c == '\\') {
                if (++k >= len) {
                    buf.append('\\');
                    break;
                }
                c = cc[k];
                if (c >= '0' && c <= '7') {
                    int n = c - 48;
                    ++k;
                    for (int j = 0; j < 2 && k < len; ++k, ++j) {
                        c = cc[k];
                        if (c < '0' || c > '7') break;
                        n = n * 8 + c - 48;
                    }
                    --k;
                    buf.append((char)n);
                    continue;
                }
                buf.append(c);
                continue;
            }
            buf.append(c);
        }
        return buf.toString();
    }

    public void endDocument() {
    }

    public void endElement(String tag) {
        if (tag.equals("Destination")) {
            if (this.xmlLast == null && this.xmlNames != null) {
                return;
            }
            throw new RuntimeException("Destination end tag out of place.");
        }
        if (!tag.equals("Name")) {
            throw new RuntimeException("Invalid end tag - " + tag);
        }
        if (this.xmlLast == null || this.xmlNames == null) {
            throw new RuntimeException("Name end tag out of place.");
        }
        if (!this.xmlLast.containsKey("Page")) {
            throw new RuntimeException("Page attribute missing.");
        }
        this.xmlNames.put(SimpleNamedDestination.unEscapeBinaryString((String)this.xmlLast.get("Name")), this.xmlLast.get("Page"));
        this.xmlLast = null;
    }

    public void startDocument() {
    }

    public void startElement(String tag, HashMap h) {
        if (this.xmlNames == null) {
            if (tag.equals("Destination")) {
                this.xmlNames = new HashMap();
                return;
            }
            throw new RuntimeException("Root element is not Destination.");
        }
        if (!tag.equals("Name")) {
            throw new RuntimeException("Tag " + tag + " not allowed.");
        }
        if (this.xmlLast != null) {
            throw new RuntimeException("Nested tags are not allowed.");
        }
        this.xmlLast = new HashMap(h);
        this.xmlLast.put("Name", "");
    }

    public void text(String str) {
        if (this.xmlLast == null) {
            return;
        }
        String name = (String)this.xmlLast.get("Name");
        name = String.valueOf(name) + str;
        this.xmlLast.put("Name", name);
    }
}

