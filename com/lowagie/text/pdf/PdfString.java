/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfEncryption;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;

public class PdfString
extends PdfObject {
    protected String value = "";
    protected String originalValue = null;
    protected String encoding = "PDF";
    protected int objNum = 0;
    protected int objGen = 0;
    protected boolean hexWriting = false;

    public PdfString() {
        super(3);
    }

    public PdfString(String value) {
        super(3);
        this.value = value;
    }

    public PdfString(String value, String encoding) {
        super(3);
        this.value = value;
        this.encoding = encoding;
    }

    public PdfString(byte[] bytes) {
        super(3);
        this.value = PdfEncodings.convertToString(bytes, null);
        this.encoding = "";
    }

    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        byte[] b = this.getBytes();
        PdfEncryption crypto = null;
        if (writer != null) {
            crypto = writer.getEncryption();
        }
        if (crypto != null) {
            b = (byte[])this.bytes.clone();
            crypto.prepareKey();
            crypto.encryptRC4(b);
        }
        if (this.hexWriting) {
            ByteBuffer buf = new ByteBuffer();
            buf.append('<');
            int len = b.length;
            for (int k = 0; k < len; ++k) {
                buf.appendHex(b[k]);
            }
            buf.append('>');
            os.write(buf.toByteArray());
        } else {
            os.write(PdfContentByte.escapeString(b));
        }
    }

    public String toString() {
        return this.value;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public String toUnicodeString() {
        if (this.encoding != null && this.encoding.length() != 0) {
            return this.value;
        }
        this.getBytes();
        if (this.bytes.length >= 2 && this.bytes[0] == -2 && this.bytes[1] == -1) {
            return PdfEncodings.convertToString(this.bytes, "UnicodeBig");
        }
        return PdfEncodings.convertToString(this.bytes, "PDF");
    }

    void setObjNum(int objNum, int objGen) {
        this.objNum = objNum;
        this.objGen = objGen;
    }

    void decrypt(PdfReader reader) {
        PdfEncryption decrypt = reader.getDecrypt();
        if (decrypt != null) {
            this.originalValue = this.value;
            decrypt.setHashKey(this.objNum, this.objGen);
            decrypt.prepareKey();
            this.bytes = PdfEncodings.convertToBytes(this.value, null);
            decrypt.encryptRC4(this.bytes);
            this.value = PdfEncodings.convertToString(this.bytes, null);
        }
    }

    public byte[] getBytes() {
        if (this.bytes == null) {
            this.bytes = this.encoding != null && this.encoding.equals("UnicodeBig") && PdfEncodings.isPdfDocEncoding(this.value) ? PdfEncodings.convertToBytes(this.value, "PDF") : PdfEncodings.convertToBytes(this.value, this.encoding);
        }
        return this.bytes;
    }

    public byte[] getOriginalBytes() {
        if (this.originalValue == null) {
            return this.getBytes();
        }
        return PdfEncodings.convertToBytes(this.originalValue, null);
    }

    public PdfString setHexWriting(boolean hexWriting) {
        this.hexWriting = hexWriting;
        return this;
    }

    public boolean isHexWriting() {
        return this.hexWriting;
    }
}

