/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.GlyphList;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfRectangle;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.Pfm2afm;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.fonts.FontsResourceAnchor;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.StringTokenizer;

class Type1Font
extends BaseFont {
    private static FontsResourceAnchor resourceAnchor;
    protected byte[] pfb;
    private String FontName;
    private String FullName;
    private String FamilyName;
    private String Weight;
    private float ItalicAngle;
    private boolean IsFixedPitch;
    private String CharacterSet;
    private int llx;
    private int lly;
    private int urx;
    private int ury;
    private int UnderlinePosition;
    private int UnderlineThickness;
    private String EncodingScheme;
    private int CapHeight;
    private int XHeight;
    private int Ascender;
    private int Descender;
    private int StdHW;
    private int StdVW;
    private HashMap CharMetrics;
    private HashMap KernPairs;
    private String fileName;
    private boolean builtinFont;
    private static final int[] pfbTypes;

    static {
        pfbTypes = new int[]{1, 2, 1};
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    Type1Font(String afmFile, String enc, boolean emb, byte[] ttfAfm, byte[] pfb) throws DocumentException, IOException {
        block40 : {
            super();
            this.Weight = "";
            this.ItalicAngle = 0.0f;
            this.IsFixedPitch = false;
            this.llx = -50;
            this.lly = -200;
            this.urx = 1000;
            this.ury = 900;
            this.UnderlinePosition = -100;
            this.UnderlineThickness = 50;
            this.EncodingScheme = "FontSpecific";
            this.CapHeight = 700;
            this.XHeight = 480;
            this.Ascender = 800;
            this.Descender = -200;
            this.StdVW = 80;
            this.CharMetrics = new HashMap<K, V>();
            this.KernPairs = new HashMap<K, V>();
            this.builtinFont = false;
            if (emb && ttfAfm != null && pfb == null) {
                throw new DocumentException("Two byte arrays are needed if the Type1 font is embedded.");
            }
            if (emb && ttfAfm != null) {
                this.pfb = pfb;
            }
            this.encoding = enc;
            this.embedded = emb;
            this.fileName = afmFile;
            this.fontType = 0;
            rf = null;
            is = null;
            if (!BaseFont.BuiltinFonts14.containsKey(afmFile)) ** GOTO lbl76
            this.embedded = false;
            this.builtinFont = true;
            buf = new byte[1024];
            try {
                if (Type1Font.resourceAnchor == null) {
                    Type1Font.resourceAnchor = new FontsResourceAnchor();
                }
                if ((is = BaseFont.getResourceStream("com/lowagie/text/pdf/fonts/" + afmFile + ".afm", Type1Font.resourceAnchor.getClass().getClassLoader())) == null) {
                    msg = String.valueOf(afmFile) + " not found as resource. (The *.afm files must exist as resources in the package com.lowagie.text.pdf.fonts)";
                    System.err.println(msg);
                    throw new DocumentException(msg);
                }
                out = new ByteArrayOutputStream();
                do {
                    if ((size = is.read((byte[])buf)) < 0) break;
                    out.write((byte[])buf, 0, size);
                } while (true);
                buf = out.toByteArray();
                var11_24 = null;
                if (is != null) {
                }
                ** GOTO lbl-1000
            }
            catch (Throwable var12_22) {
                var11_23 = null;
                if (is == null) throw var12_22;
                try {
                    is.close();
                    throw var12_22;
                }
                catch (Exception e) {
                    // empty catch block
                }
                throw var12_22;
            }
            ** try [egrp 1[TRYBLOCK] [2 : 377->385)] { 
lbl62: // 1 sources:
            is.close();
            ** GOTO lbl-1000
lbl64: // 1 sources:
            catch (Exception e) {
                // empty catch block
            }
lbl-1000: // 3 sources:
            {
                rf = new RandomAccessFileOrArray((byte[])buf);
                this.process(rf);
                out = null;
                if (rf != null) {
                    try {
                        rf.close();
                    }
                    catch (Exception e) {}
                } else {
                    ** GOTO lbl125
                }
                break block40;
            }
lbl76: // 1 sources:
            if (afmFile.toLowerCase().endsWith(".afm")) {
                try {
                    rf = ttfAfm == null ? new RandomAccessFileOrArray(afmFile) : new RandomAccessFileOrArray(ttfAfm);
                    this.process(rf);
                }
                catch (Throwable out) {
                    buf = null;
                    if (rf == null) throw out;
                    try {
                        rf.close();
                        throw out;
                    }
                    catch (Exception e) {
                        // empty catch block
                    }
                    throw out;
                }
                buf = null;
                if (rf != null) {
                    try {}
                    catch (Exception e) {}
                    rf.close();
                }
            } else {
                if (afmFile.toLowerCase().endsWith(".pfm") == false) throw new DocumentException(String.valueOf(afmFile) + " is not an AFM or PFM font file.");
                try {
                    ba = new ByteArrayOutputStream();
                    rf = ttfAfm == null ? new RandomAccessFileOrArray(afmFile) : new RandomAccessFileOrArray(ttfAfm);
                    Pfm2afm.convert(rf, ba);
                    rf.close();
                    rf = new RandomAccessFileOrArray(ba.toByteArray());
                    this.process(rf);
                    out = null;
                    if (rf == null) break block40;
                }
                catch (Throwable e) {
                    out = null;
                    if (rf == null) throw e;
                    try {
                        rf.close();
                        throw e;
                    }
                    catch (Exception e) {
                        // empty catch block
                    }
                    throw e;
                }
                try {}
                catch (Exception e) {}
                rf.close();
            }
        }
        try {
            this.EncodingScheme = this.EncodingScheme.trim();
            if (this.EncodingScheme.equals("AdobeStandardEncoding") || this.EncodingScheme.equals("StandardEncoding")) {
                this.fontSpecific = false;
            }
            PdfEncodings.convertToBytes(" ", enc);
            this.createEncoding();
            return;
        }
        catch (RuntimeException re) {
            throw re;
        }
        catch (Exception e) {
            throw new DocumentException(e);
        }
    }

    int getRawWidth(int c, String name) {
        Object[] metrics;
        if (name == null) {
            metrics = (Object[])this.CharMetrics.get(new Integer(c));
        } else {
            if (name.equals(".notdef")) {
                return 0;
            }
            metrics = (Object[])this.CharMetrics.get(name);
        }
        if (metrics != null) {
            return (Integer)metrics[1];
        }
        return 0;
    }

    public int getKerning(char char1, char char2) {
        String first = GlyphList.unicodeToName(char1);
        if (first == null) {
            return 0;
        }
        String second = GlyphList.unicodeToName(char2);
        if (second == null) {
            return 0;
        }
        Object[] obj = (Object[])this.KernPairs.get(first);
        if (obj == null) {
            return 0;
        }
        for (int k = 0; k < obj.length; k+=2) {
            if (!second.equals(obj[k])) continue;
            return (Integer)obj[k + 1];
        }
        return 0;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public void process(RandomAccessFileOrArray rf) throws DocumentException, IOException {
        isMetrics = false;
        while ((line = rf.readLine()) != null) {
            tok = new StringTokenizer(line);
            if (!tok.hasMoreTokens()) continue;
            ident = tok.nextToken();
            if (ident.equals("FontName")) {
                this.FontName = tok.nextToken("\u00ff").substring(1);
                continue;
            }
            if (ident.equals("FullName")) {
                this.FullName = tok.nextToken("\u00ff").substring(1);
                continue;
            }
            if (ident.equals("FamilyName")) {
                this.FamilyName = tok.nextToken("\u00ff").substring(1);
                continue;
            }
            if (ident.equals("Weight")) {
                this.Weight = tok.nextToken("\u00ff").substring(1);
                continue;
            }
            if (ident.equals("ItalicAngle")) {
                this.ItalicAngle = Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("IsFixedPitch")) {
                this.IsFixedPitch = tok.nextToken().equals("true");
                continue;
            }
            if (ident.equals("CharacterSet")) {
                this.CharacterSet = tok.nextToken("\u00ff").substring(1);
                continue;
            }
            if (ident.equals("FontBBox")) {
                this.llx = (int)Float.valueOf(tok.nextToken()).floatValue();
                this.lly = (int)Float.valueOf(tok.nextToken()).floatValue();
                this.urx = (int)Float.valueOf(tok.nextToken()).floatValue();
                this.ury = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("UnderlinePosition")) {
                this.UnderlinePosition = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("UnderlineThickness")) {
                this.UnderlineThickness = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("EncodingScheme")) {
                this.EncodingScheme = tok.nextToken("\u00ff").substring(1);
                continue;
            }
            if (ident.equals("CapHeight")) {
                this.CapHeight = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("XHeight")) {
                this.XHeight = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("Ascender")) {
                this.Ascender = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("Descender")) {
                this.Descender = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("StdHW")) {
                this.StdHW = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (ident.equals("StdVW")) {
                this.StdVW = (int)Float.valueOf(tok.nextToken()).floatValue();
                continue;
            }
            if (!ident.equals("StartCharMetrics")) continue;
            isMetrics = true;
            break;
        }
        if (isMetrics) ** GOTO lbl96
        throw new DocumentException("Missing StartCharMetrics in " + this.fileName);
lbl-1000: // 1 sources:
        {
            tok = new StringTokenizer(line);
            if (!tok.hasMoreTokens()) continue;
            ident = tok.nextToken();
            if (ident.equals("EndCharMetrics")) {
                isMetrics = false;
                break;
            }
            C = new Integer(-1);
            WX = new Integer(250);
            N = "";
            B = null;
            tok = new StringTokenizer(line, ";");
            while (tok.hasMoreTokens()) {
                tokc = new StringTokenizer(tok.nextToken());
                if (!tokc.hasMoreTokens()) continue;
                ident = tokc.nextToken();
                if (ident.equals("C")) {
                    C = Integer.valueOf(tokc.nextToken());
                    continue;
                }
                if (ident.equals("WX")) {
                    WX = new Integer(Float.valueOf(tokc.nextToken()).intValue());
                    continue;
                }
                if (ident.equals("N")) {
                    N = tokc.nextToken();
                    continue;
                }
                if (!ident.equals("B")) continue;
                B = new int[]{Integer.parseInt(tokc.nextToken()), Integer.parseInt(tokc.nextToken()), Integer.parseInt(tokc.nextToken()), Integer.parseInt(tokc.nextToken())};
            }
            metrics = new Object[]{C, WX, N, B};
            if (C >= 0) {
                this.CharMetrics.put(C, metrics);
            }
            this.CharMetrics.put(N, metrics);
lbl96: // 3 sources:
            ** while ((line = rf.readLine()) != null)
        }
lbl97: // 2 sources:
        if (!isMetrics) ** GOTO lbl107
        throw new DocumentException("Missing EndCharMetrics in " + this.fileName);
lbl-1000: // 1 sources:
        {
            tok = new StringTokenizer(line);
            if (!tok.hasMoreTokens()) continue;
            ident = tok.nextToken();
            if (ident.equals("EndFontMetrics")) {
                return;
            }
            if (!ident.equals("StartKernPairs")) continue;
            isMetrics = true;
            break;
lbl107: // 3 sources:
            ** while ((line = rf.readLine()) != null)
        }
lbl108: // 2 sources:
        if (isMetrics) ** GOTO lbl131
        throw new DocumentException("Missing EndFontMetrics in " + this.fileName);
lbl-1000: // 1 sources:
        {
            tok = new StringTokenizer(line);
            if (!tok.hasMoreTokens()) continue;
            ident = tok.nextToken();
            if (ident.equals("KPX")) {
                first = tok.nextToken();
                second = tok.nextToken();
                width = new Integer(Float.valueOf(tok.nextToken()).intValue());
                relates = (Object[])this.KernPairs.get(first);
                if (relates == null) {
                    this.KernPairs.put(first, new Object[]{second, width});
                    continue;
                }
                n = relates.length;
                relates2 = new Object[n + 2];
                System.arraycopy(relates, 0, relates2, 0, n);
                relates2[n] = second;
                relates2[n + 1] = width;
                this.KernPairs.put(first, relates2);
                continue;
            }
            if (!ident.equals("EndKernPairs")) continue;
            isMetrics = false;
            break;
lbl131: // 5 sources:
            ** while ((line = rf.readLine()) != null)
        }
lbl132: // 2 sources:
        if (isMetrics) {
            throw new DocumentException("Missing EndKernPairs in " + this.fileName);
        }
        rf.close();
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    private PdfStream getFontStream() throws DocumentException {
        if (this.builtinFont != false) return null;
        if (!this.embedded) {
            return null;
        }
        rf = null;
        try {
            try {
                filePfb = String.valueOf(this.fileName.substring(0, this.fileName.length() - 3)) + "pfb";
                rf = this.pfb == null ? new RandomAccessFileOrArray(filePfb) : new RandomAccessFileOrArray(this.pfb);
                fileLength = rf.length();
                st = new byte[fileLength - 18];
                lengths = new int[3];
                bytePtr = 0;
                k = 0;
                block7 : do {
                    if (k >= 3) {
                        streamFont = new BaseFont.StreamFont(st, lengths);
                        var10_12 = null;
                        if (rf == null) return streamFont;
                        break;
                    }
                    if (rf.read() != 128) {
                        throw new DocumentException("Start marker missing in " + filePfb);
                    }
                    if (rf.read() != Type1Font.pfbTypes[k]) {
                        throw new DocumentException("Incorrect segment type in " + filePfb);
                    }
                    size = rf.read();
                    size+=rf.read() << 8;
                    size+=rf.read() << 16;
                    lengths[k] = size+=rf.read() << 24;
                    do {
                        if (size == 0) {
                            ++k;
                            continue block7;
                        }
                        got = rf.read(st, bytePtr, size);
                        if (got < 0) {
                            throw new DocumentException("Premature end in " + filePfb);
                        }
                        bytePtr+=got;
                        size-=got;
                    } while (true);
                    break;
                } while (true);
                try {
                    rf.close();
                    return streamFont;
                }
                catch (Exception e) {
                    // empty catch block
                }
                return streamFont;
            }
            catch (Exception e) {
                throw new DocumentException(e);
            }
        }
        catch (Throwable throwable) {
            var10_13 = null;
            if (rf == null) throw throwable;
            ** try [egrp 2[TRYBLOCK] [3 : 351->358)] { 
lbl52: // 1 sources:
            rf.close();
            throw throwable;
lbl54: // 1 sources:
            catch (Exception e) {
                // empty catch block
            }
            throw throwable;
        }
    }

    private PdfDictionary getFontDescriptor(PdfIndirectReference fontStream) {
        if (this.builtinFont) {
            return null;
        }
        PdfDictionary dic = new PdfDictionary(PdfName.FONTDESCRIPTOR);
        dic.put(PdfName.ASCENT, new PdfNumber(this.Ascender));
        dic.put(PdfName.CAPHEIGHT, new PdfNumber(this.CapHeight));
        dic.put(PdfName.DESCENT, new PdfNumber(this.Descender));
        dic.put(PdfName.FONTBBOX, new PdfRectangle(this.llx, this.lly, this.urx, this.ury));
        dic.put(PdfName.FONTNAME, new PdfName(this.FontName));
        dic.put(PdfName.ITALICANGLE, new PdfNumber(this.ItalicAngle));
        dic.put(PdfName.STEMV, new PdfNumber(this.StdVW));
        if (fontStream != null) {
            dic.put(PdfName.FONTFILE, fontStream);
        }
        int flags = 0;
        if (this.IsFixedPitch) {
            flags|=true;
        }
        flags|=this.fontSpecific ? 4 : 32;
        if (this.ItalicAngle < 0.0f) {
            flags|=64;
        }
        if (this.FontName.indexOf("Caps") >= 0 || this.FontName.endsWith("SC")) {
            flags|=131072;
        }
        if (this.Weight.equals("Bold")) {
            flags|=262144;
        }
        dic.put(PdfName.FLAGS, new PdfNumber(flags));
        return dic;
    }

    private PdfDictionary getFontBaseType(PdfIndirectReference fontDescriptor, int firstChar, int lastChar, byte[] shortTag) {
        boolean stdEncoding;
        PdfDictionary dic = new PdfDictionary(PdfName.FONT);
        dic.put(PdfName.SUBTYPE, PdfName.TYPE1);
        dic.put(PdfName.BASEFONT, new PdfName(this.FontName));
        boolean bl = stdEncoding = this.encoding.equals("Cp1252") || this.encoding.equals("MacRoman");
        if (!this.fontSpecific) {
            for (int k = firstChar; k <= lastChar; ++k) {
                if (this.differences[k].equals(".notdef")) continue;
                firstChar = k;
                break;
            }
            if (stdEncoding) {
                dic.put(PdfName.ENCODING, this.encoding.equals("Cp1252") ? PdfName.WIN_ANSI_ENCODING : PdfName.MAC_ROMAN_ENCODING);
            } else {
                PdfDictionary enc = new PdfDictionary(PdfName.ENCODING);
                PdfArray dif = new PdfArray();
                boolean gap = true;
                for (int k2 = firstChar; k2 <= lastChar; ++k2) {
                    if (shortTag[k2] != 0) {
                        if (gap) {
                            dif.add(new PdfNumber(k2));
                            gap = false;
                        }
                        dif.add(new PdfName(this.differences[k2]));
                        continue;
                    }
                    gap = true;
                }
                enc.put(PdfName.DIFFERENCES, dif);
                dic.put(PdfName.ENCODING, enc);
            }
        }
        if (!(!this.forceWidthsOutput && this.builtinFont && (this.fontSpecific || stdEncoding))) {
            dic.put(PdfName.FIRSTCHAR, new PdfNumber(firstChar));
            dic.put(PdfName.LASTCHAR, new PdfNumber(lastChar));
            PdfArray wd = new PdfArray();
            for (int k = firstChar; k <= lastChar; ++k) {
                if (shortTag[k] == 0) {
                    wd.add(new PdfNumber(0));
                    continue;
                }
                wd.add(new PdfNumber(this.widths[k]));
            }
            dic.put(PdfName.WIDTHS, wd);
        }
        if (!(this.builtinFont || fontDescriptor == null)) {
            dic.put(PdfName.FONTDESCRIPTOR, fontDescriptor);
        }
        return dic;
    }

    void writeFont(PdfWriter writer, PdfIndirectReference ref, Object[] params) throws DocumentException, IOException {
        int firstChar = (Integer)params[0];
        int lastChar = (Integer)params[1];
        byte[] shortTag = (byte[])params[2];
        if (!this.subset) {
            firstChar = 0;
            lastChar = shortTag.length - 1;
            for (int k = 0; k < shortTag.length; ++k) {
                shortTag[k] = 1;
            }
        }
        PdfIndirectReference ind_font = null;
        PdfDictionary pobj = null;
        PdfIndirectObject obj = null;
        pobj = this.getFontStream();
        if (pobj != null) {
            obj = writer.addToBody(pobj);
            ind_font = obj.getIndirectReference();
        }
        if ((pobj = this.getFontDescriptor(ind_font)) != null) {
            obj = writer.addToBody(pobj);
            ind_font = obj.getIndirectReference();
        }
        pobj = this.getFontBaseType(ind_font, firstChar, lastChar, shortTag);
        writer.addToBody((PdfObject)pobj, ref);
    }

    public float getFontDescriptor(int key, float fontSize) {
        switch (key) {
            case 1: 
            case 9: {
                return (float)this.Ascender * fontSize / 1000.0f;
            }
            case 2: {
                return (float)this.CapHeight * fontSize / 1000.0f;
            }
            case 3: 
            case 10: {
                return (float)this.Descender * fontSize / 1000.0f;
            }
            case 4: {
                return this.ItalicAngle;
            }
            case 5: {
                return (float)this.llx * fontSize / 1000.0f;
            }
            case 6: {
                return (float)this.lly * fontSize / 1000.0f;
            }
            case 7: {
                return (float)this.urx * fontSize / 1000.0f;
            }
            case 8: {
                return (float)this.ury * fontSize / 1000.0f;
            }
            case 11: {
                return 0.0f;
            }
            case 12: {
                return (float)(this.urx - this.llx) * fontSize / 1000.0f;
            }
        }
        return 0.0f;
    }

    public String getPostscriptFontName() {
        return this.FontName;
    }

    public String[][] getFullFontName() {
        return new String[][]{{"", "", "", this.FullName}};
    }

    public String[][] getFamilyFontName() {
        return new String[][]{{"", "", "", this.FamilyName}};
    }

    public boolean hasKernPairs() {
        if (this.KernPairs.size() > 0) {
            return true;
        }
        return false;
    }

    public void setPostscriptFontName(String name) {
        this.FontName = name;
    }

    public boolean setKerning(char char1, char char2, int kern) {
        String first = GlyphList.unicodeToName(char1);
        if (first == null) {
            return false;
        }
        String second = GlyphList.unicodeToName(char2);
        if (second == null) {
            return false;
        }
        Object[] obj = (Object[])this.KernPairs.get(first);
        if (obj == null) {
            obj = new Object[]{second, new Integer(kern)};
            this.KernPairs.put(first, obj);
            return true;
        }
        for (int k = 0; k < obj.length; k+=2) {
            if (!second.equals(obj[k])) continue;
            obj[k + 1] = new Integer(kern);
            return true;
        }
        int size = obj.length;
        Object[] obj2 = new Object[size + 2];
        System.arraycopy(obj, 0, obj2, 0, size);
        obj2[size] = second;
        obj2[size + 1] = new Integer(kern);
        this.KernPairs.put(first, obj2);
        return true;
    }

    protected int[] getRawCharBBox(int c, String name) {
        Object[] metrics;
        if (name == null) {
            metrics = (Object[])this.CharMetrics.get(new Integer(c));
        } else {
            if (name.equals(".notdef")) {
                return null;
            }
            metrics = (Object[])this.CharMetrics.get(name);
        }
        if (metrics != null) {
            return (int[])metrics[3];
        }
        return null;
    }
}

