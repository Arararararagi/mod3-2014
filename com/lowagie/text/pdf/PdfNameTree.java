/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.StringCompare;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

public class PdfNameTree {
    private static final int leafSize = 64;
    private static final StringCompare stringCompare = new StringCompare();

    public static PdfDictionary writeTree(HashMap items, PdfWriter writer) throws IOException {
        if (items.size() == 0) {
            return null;
        }
        String[] names = new String[items.size()];
        names = items.keySet().toArray(names);
        Arrays.sort(names, stringCompare);
        if (names.length <= 64) {
            PdfDictionary dic = new PdfDictionary();
            PdfArray ar = new PdfArray();
            for (int k = 0; k < names.length; ++k) {
                ar.add(new PdfString(names[k], null));
                ar.add((PdfIndirectReference)items.get(names[k]));
            }
            dic.put(PdfName.NAMES, ar);
            return dic;
        }
        int skip = 64;
        PdfIndirectReference[] kids = new PdfIndirectReference[(names.length + 64 - 1) / 64];
        for (int k = 0; k < kids.length; ++k) {
            int offset;
            int end = Math.min(offset + 64, names.length);
            PdfDictionary dic = new PdfDictionary();
            PdfArray arr = new PdfArray();
            arr.add(new PdfString(names[offset], null));
            arr.add(new PdfString(names[end - 1], null));
            dic.put(PdfName.LIMITS, arr);
            arr = new PdfArray();
            for (offset = k * 64; offset < end; ++offset) {
                arr.add(new PdfString(names[offset], null));
                arr.add((PdfIndirectReference)items.get(names[offset]));
            }
            dic.put(PdfName.NAMES, arr);
            kids[k] = writer.addToBody(dic).getIndirectReference();
        }
        int top = kids.length;
        do {
            int k2;
            if (top <= 64) {
                PdfArray arr = new PdfArray();
                for (k2 = 0; k2 < top; ++k2) {
                    arr.add(kids[k2]);
                }
                PdfDictionary dic = new PdfDictionary();
                dic.put(PdfName.KIDS, arr);
                return dic;
            }
            int tt = (names.length + skip - 1) / (skip*=64);
            for (k2 = 0; k2 < tt; ++k2) {
                int offset;
                int end = Math.min(offset + 64, top);
                PdfDictionary dic = new PdfDictionary();
                PdfArray arr = new PdfArray();
                arr.add(new PdfString(names[k2 * skip], null));
                arr.add(new PdfString(names[Math.min((k2 + 1) * skip, names.length) - 1], null));
                dic.put(PdfName.LIMITS, arr);
                arr = new PdfArray();
                for (offset = k2 * 64; offset < end; ++offset) {
                    arr.add(kids[offset]);
                }
                dic.put(PdfName.KIDS, arr);
                kids[k2] = writer.addToBody(dic).getIndirectReference();
            }
            top = tt;
        } while (true);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static void iterateItems(PdfDictionary dic, HashMap items) {
        PdfArray nn = (PdfArray)PdfReader.getPdfObjectRelease(dic.get(PdfName.NAMES));
        if (nn != null) {
            ArrayList arr = nn.getArrayList();
            for (int k = 0; k < arr.size(); ++k) {
                PdfString s = (PdfString)PdfReader.getPdfObjectRelease((PdfObject)arr.get(k++));
                items.put(s.toString(), arr.get(k));
            }
            return;
        } else {
            nn = (PdfArray)PdfReader.getPdfObjectRelease(dic.get(PdfName.KIDS));
            if (nn == null) return;
            ArrayList arr = nn.getArrayList();
            for (int k = 0; k < arr.size(); ++k) {
                PdfDictionary kid = (PdfDictionary)PdfReader.getPdfObjectRelease((PdfObject)arr.get(k));
                PdfNameTree.iterateItems(kid, items);
            }
        }
    }

    public static HashMap readTree(PdfDictionary dic) {
        HashMap items = new HashMap();
        if (dic != null) {
            PdfNameTree.iterateItems(dic, items);
        }
        return items;
    }
}

