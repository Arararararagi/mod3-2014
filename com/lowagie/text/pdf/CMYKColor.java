/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.ExtendedColor;

public class CMYKColor
extends ExtendedColor {
    float cyan;
    float magenta;
    float yellow;
    float black;

    public CMYKColor(int intCyan, int intMagenta, int intYellow, int intBlack) {
        this((float)intCyan / 255.0f, (float)intMagenta / 255.0f, (float)intYellow / 255.0f, (float)intBlack / 255.0f);
    }

    public CMYKColor(float floatCyan, float floatMagenta, float floatYellow, float floatBlack) {
        super(2, 1.0f - floatCyan - floatBlack, 1.0f - floatMagenta - floatBlack, 1.0f - floatYellow - floatBlack);
        this.cyan = ExtendedColor.normalize(floatCyan);
        this.magenta = ExtendedColor.normalize(floatMagenta);
        this.yellow = ExtendedColor.normalize(floatYellow);
        this.black = ExtendedColor.normalize(floatBlack);
    }

    public float getCyan() {
        return this.cyan;
    }

    public float getMagenta() {
        return this.magenta;
    }

    public float getYellow() {
        return this.yellow;
    }

    public float getBlack() {
        return this.black;
    }
}

