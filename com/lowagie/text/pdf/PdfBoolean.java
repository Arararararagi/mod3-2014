/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.BadPdfFormatException;
import com.lowagie.text.pdf.PdfObject;

public class PdfBoolean
extends PdfObject {
    public static final PdfBoolean PDFTRUE = new PdfBoolean(true);
    public static final PdfBoolean PDFFALSE = new PdfBoolean(false);
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    private boolean value;

    public PdfBoolean(boolean value) {
        super(1);
        if (value) {
            this.setContent("true");
        } else {
            this.setContent("false");
        }
        this.value = value;
    }

    public PdfBoolean(String value) throws BadPdfFormatException {
        super(1, value);
        if (value.equals("true")) {
            this.value = true;
        } else if (value.equals("false")) {
            this.value = false;
        } else {
            throw new BadPdfFormatException("The value has to be 'true' of 'false', instead of '" + value + "'.");
        }
    }

    public boolean booleanValue() {
        return this.value;
    }
}

