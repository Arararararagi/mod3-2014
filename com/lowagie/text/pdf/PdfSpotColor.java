/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.CMYKColor;
import com.lowagie.text.pdf.ExtendedColor;
import com.lowagie.text.pdf.GrayColor;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfFunction;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.IOException;

public class PdfSpotColor {
    protected float tint;
    public PdfName name;
    public Color altcs;

    public PdfSpotColor(String name, float tint, Color altcs) {
        this.name = new PdfName(name);
        this.tint = tint;
        this.altcs = altcs;
    }

    public float getTint() {
        return this.tint;
    }

    public Color getAlternativeCS() {
        return this.altcs;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    protected PdfObject getSpotObject(PdfWriter writer) throws IOException {
        array = new PdfArray(PdfName.SEPARATION);
        array.add(this.name);
        func = null;
        if (!(this.altcs instanceof ExtendedColor)) ** GOTO lbl18
        type = ((ExtendedColor)this.altcs).type;
        switch (type) {
            case 1: {
                array.add(PdfName.DEVICEGRAY);
                func = PdfFunction.type2(writer, new float[]{0.0f, 1.0f}, null, new float[]{0.0f}, new float[]{((GrayColor)this.altcs).getGray()}, 1.0f);
                ** GOTO lbl20
            }
            case 2: {
                array.add(PdfName.DEVICECMYK);
                cmyk = (CMYKColor)this.altcs;
                func = PdfFunction.type2(writer, new float[]{0.0f, 1.0f}, null, new float[]{0.0f, 0.0f, 0.0f, 0.0f}, new float[]{cmyk.getCyan(), cmyk.getMagenta(), cmyk.getYellow(), cmyk.getBlack()}, 1.0f);
                ** GOTO lbl20
            }
            default: {
                throw new RuntimeException("Only RGB, Gray and CMYK are supported as alternative color spaces.");
            }
        }
lbl18: // 1 sources:
        array.add(PdfName.DEVICERGB);
        func = PdfFunction.type2(writer, new float[]{0.0f, 1.0f}, null, new float[]{0.0f, 0.0f, 0.0f}, new float[]{(float)this.altcs.getRed() / 255.0f, (float)this.altcs.getGreen() / 255.0f, (float)this.altcs.getBlue() / 255.0f}, 1.0f);
lbl20: // 3 sources:
        array.add(func.getReference());
        return array;
    }
}

