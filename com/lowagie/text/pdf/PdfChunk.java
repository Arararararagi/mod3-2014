/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Chunk;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.SplitCharacter;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.HyphenationEvent;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfFont;
import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PdfChunk
implements SplitCharacter {
    private static final char[] singleSpace = new char[]{' '};
    private static final PdfChunk[] thisChunk = new PdfChunk[1];
    private static final float ITALIC_ANGLE = 0.21256f;
    private static final HashMap keysAttributes = new HashMap();
    private static final HashMap keysNoStroke = new HashMap();
    protected String value = "";
    protected String encoding = "Cp1252";
    protected PdfFont font;
    protected BaseFont baseFont;
    protected SplitCharacter splitCharacter;
    protected HashMap attributes = new HashMap();
    protected HashMap noStroke = new HashMap();
    protected boolean newlineSplit;
    protected Image image;
    protected float offsetX;
    protected float offsetY;
    protected boolean changeLeading = false;

    static {
        keysAttributes.put("ACTION", null);
        keysAttributes.put("UNDERLINE", null);
        keysAttributes.put("REMOTEGOTO", null);
        keysAttributes.put("LOCALGOTO", null);
        keysAttributes.put("LOCALDESTINATION", null);
        keysAttributes.put("GENERICTAG", null);
        keysAttributes.put("NEWPAGE", null);
        keysAttributes.put("IMAGE", null);
        keysAttributes.put("BACKGROUND", null);
        keysAttributes.put("PDFANNOTATION", null);
        keysAttributes.put("SKEW", null);
        keysAttributes.put("HSCALE", null);
        keysNoStroke.put("SUBSUPSCRIPT", null);
        keysNoStroke.put("SPLITCHARACTER", null);
        keysNoStroke.put("HYPHENATION", null);
        keysNoStroke.put("TEXTRENDERMODE", null);
    }

    PdfChunk(String string, PdfChunk other) {
        PdfChunk.thisChunk[0] = this;
        this.value = string;
        this.font = other.font;
        this.attributes = other.attributes;
        this.noStroke = other.noStroke;
        this.baseFont = other.baseFont;
        Object[] obj = (Object[])this.attributes.get("IMAGE");
        if (obj == null) {
            this.image = null;
        } else {
            this.image = (Image)obj[0];
            this.offsetX = ((Float)obj[1]).floatValue();
            this.offsetY = ((Float)obj[2]).floatValue();
            this.changeLeading = (Boolean)obj[3];
        }
        this.encoding = this.font.getFont().getEncoding();
        this.splitCharacter = (SplitCharacter)this.noStroke.get("SPLITCHARACTER");
        if (this.splitCharacter == null) {
            this.splitCharacter = this;
        }
    }

    PdfChunk(Chunk chunk, PdfAction action) {
        Object[] obj;
        Object[][] unders;
        PdfChunk.thisChunk[0] = this;
        this.value = chunk.content();
        Font f = chunk.font();
        float size = f.size();
        if (size == -1.0f) {
            size = 12.0f;
        }
        this.baseFont = f.getBaseFont();
        int style = f.style();
        if (style == -1) {
            style = 0;
        }
        if (this.baseFont == null) {
            this.baseFont = f.getCalculatedBaseFont(false);
        } else {
            if ((style & 1) != 0) {
                Object[] arrobject = new Object[3];
                arrobject[0] = new Integer(2);
                arrobject[1] = new Float(size / 30.0f);
                this.attributes.put("TEXTRENDERMODE", arrobject);
            }
            if ((style & 2) != 0) {
                this.attributes.put("SKEW", new float[]{0.0f, 0.21256f});
            }
        }
        this.font = new PdfFont(this.baseFont, size);
        HashMap attr = chunk.getAttributes();
        if (attr != null) {
            Iterator i = attr.keySet().iterator();
            while (i.hasNext()) {
                Object name = i.next();
                if (keysAttributes.containsKey(name)) {
                    this.attributes.put(name, attr.get(name));
                    continue;
                }
                if (!keysNoStroke.containsKey(name)) continue;
                this.noStroke.put(name, attr.get(name));
            }
            if ("".equals(attr.get("GENERICTAG"))) {
                this.attributes.put("GENERICTAG", chunk.content());
            }
        }
        if (f.isUnderlined()) {
            Object[] arrobject = new Object[2];
            arrobject[1] = new float[]{0.0f, 0.06666667f, 0.0f, -0.33333334f, 0.0f};
            obj = arrobject;
            unders = Chunk.addToArray((Object[][])this.attributes.get("UNDERLINE"), obj);
            this.attributes.put("UNDERLINE", unders);
        }
        if (f.isStrikethru()) {
            Object[] arrobject = new Object[2];
            arrobject[1] = new float[]{0.0f, 0.06666667f, 0.0f, 0.33333334f, 0.0f};
            obj = arrobject;
            unders = Chunk.addToArray((Object[][])this.attributes.get("UNDERLINE"), obj);
            this.attributes.put("UNDERLINE", unders);
        }
        if (action != null) {
            this.attributes.put("ACTION", action);
        }
        this.noStroke.put("COLOR", f.color());
        this.noStroke.put("ENCODING", this.font.getFont().getEncoding());
        obj = (Object[])this.attributes.get("IMAGE");
        if (obj == null) {
            this.image = null;
        } else {
            this.attributes.remove("HSCALE");
            this.image = (Image)obj[0];
            this.offsetX = ((Float)obj[1]).floatValue();
            this.offsetY = ((Float)obj[2]).floatValue();
            this.changeLeading = (Boolean)obj[3];
        }
        this.font.setImage(this.image);
        Float hs = (Float)this.attributes.get("HSCALE");
        if (hs != null) {
            this.font.setHorizontalScaling(hs.floatValue());
        }
        this.encoding = this.font.getFont().getEncoding();
        this.splitCharacter = (SplitCharacter)this.noStroke.get("SPLITCHARACTER");
        if (this.splitCharacter == null) {
            this.splitCharacter = this;
        }
    }

    public char getUnicodeEquivalent(char c) {
        return this.baseFont.getUnicodeEquivalent(c);
    }

    protected int getWord(String text, int start) {
        int len = text.length();
        while (start < len) {
            if (!Character.isLetter(text.charAt(start))) break;
            ++start;
        }
        return start;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    PdfChunk split(float width) {
        this.newlineSplit = false;
        if (this.image != null) {
            if (this.image.scaledWidth() <= width) return null;
            pc = new PdfChunk("\ufffc", this);
            this.value = "";
            this.attributes = new HashMap<K, V>();
            this.image = null;
            this.font = PdfFont.getDefaultFont();
            return pc;
        }
        hyphenationEvent = (HyphenationEvent)this.noStroke.get("HYPHENATION");
        splitPosition = -1;
        currentWidth = 0.0f;
        lastSpace = -1;
        lastSpaceWidth = 0.0f;
        length = this.value.length();
        valueArray = this.value.toCharArray();
        character = '\u0000';
        ft = this.font.getFont();
        if (ft.getFontType() != 2 || ft.getUnicodeEquivalent(' ') == ' ') ** GOTO lbl59
        for (currentPosition = 0; currentPosition < length; ++currentPosition) {
            cidChar = valueArray[currentPosition];
            character = ft.getUnicodeEquivalent(cidChar);
            if (character == '\n') {
                this.newlineSplit = true;
                returnValue = this.value.substring(currentPosition + 1);
                this.value = this.value.substring(0, currentPosition);
                if (this.value.length() >= 1) return new PdfChunk(returnValue, this);
                this.value = "\u0001";
                return new PdfChunk(returnValue, this);
            }
            currentWidth+=this.font.width(cidChar);
            if (character == ' ') {
                lastSpace = currentPosition + 1;
                lastSpaceWidth = currentWidth;
            }
            if (currentWidth <= width) {
                if (!this.splitCharacter.isSplitCharacter(0, currentPosition, length, valueArray, PdfChunk.thisChunk)) continue;
                splitPosition = currentPosition + 1;
                continue;
            } else {
                ** GOTO lbl60
            }
        }
        ** GOTO lbl58
lbl-1000: // 1 sources:
        {
            character = valueArray[currentPosition];
            if (character == '\r' || character == '\n') {
                this.newlineSplit = true;
                inc = 1;
                if (character == '\r' && currentPosition + 1 < length && valueArray[currentPosition + 1] == '\n') {
                    inc = 2;
                }
                returnValue = this.value.substring(currentPosition + inc);
                this.value = this.value.substring(0, currentPosition);
                if (this.value.length() >= 1) return new PdfChunk(returnValue, this);
                this.value = " ";
                return new PdfChunk(returnValue, this);
            }
            currentWidth+=this.font.width(character);
            if (character == ' ') {
                lastSpace = currentPosition + 1;
                lastSpaceWidth = currentWidth;
            }
            if (currentWidth <= width) {
                if (this.splitCharacter.isSplitCharacter(0, currentPosition, length, valueArray, null)) {
                    splitPosition = currentPosition + 1;
                }
                ++currentPosition;
            } else {
                ** GOTO lbl60
            }
lbl58: // 1 sources:
            break;
lbl59: // 2 sources:
            ** while (currentPosition < length)
        }
lbl60: // 6 sources:
        if (currentPosition == length) {
            return null;
        }
        if (splitPosition < 0) {
            returnValue = this.value;
            this.value = "";
            return new PdfChunk(returnValue, this);
        }
        if (lastSpace > splitPosition && this.splitCharacter.isSplitCharacter(0, 0, 1, PdfChunk.singleSpace, null)) {
            splitPosition = lastSpace;
        }
        if (hyphenationEvent != null && lastSpace < currentPosition && (wordIdx = this.getWord(this.value, lastSpace)) > lastSpace) {
            pre = hyphenationEvent.getHyphenatedWordPre(this.value.substring(lastSpace, wordIdx), this.font.getFont(), this.font.size(), width - lastSpaceWidth);
            post = hyphenationEvent.getHyphenatedWordPost();
            if (pre.length() > 0) {
                returnValue = String.valueOf(post) + this.value.substring(wordIdx);
                this.value = this.trim(String.valueOf(this.value.substring(0, lastSpace)) + pre);
                return new PdfChunk(returnValue, this);
            }
        }
        returnValue = this.value.substring(splitPosition);
        this.value = this.trim(this.value.substring(0, splitPosition));
        return new PdfChunk(returnValue, this);
    }

    PdfChunk truncate(float width) {
        int currentPosition;
        if (this.image != null) {
            if (this.image.scaledWidth() > width) {
                PdfChunk pc = new PdfChunk("", this);
                this.value = "";
                this.attributes.remove("IMAGE");
                this.image = null;
                this.font = PdfFont.getDefaultFont();
                return pc;
            }
            return null;
        }
        float currentWidth = 0.0f;
        if (width < this.font.width()) {
            String returnValue = this.value.substring(1);
            this.value = this.value.substring(0, 1);
            PdfChunk pc = new PdfChunk(returnValue, this);
            return pc;
        }
        int length = this.value.length();
        for (currentPosition = 0; currentPosition < length; ++currentPosition) {
            char character = this.value.charAt(currentPosition);
            if ((currentWidth+=this.font.width(character)) > width) break;
        }
        if (currentPosition == length) {
            return null;
        }
        if (currentPosition == 0) {
            currentPosition = 1;
        }
        String returnValue = this.value.substring(currentPosition);
        this.value = this.value.substring(0, currentPosition);
        PdfChunk pc = new PdfChunk(returnValue, this);
        return pc;
    }

    PdfFont font() {
        return this.font;
    }

    Color color() {
        return (Color)this.noStroke.get("COLOR");
    }

    float width() {
        return this.font.width(this.value);
    }

    public boolean isNewlineSplit() {
        return this.newlineSplit;
    }

    public float getWidthCorrected(float charSpacing, float wordSpacing) {
        if (this.image != null) {
            return this.image.scaledWidth() + charSpacing;
        }
        int numberOfSpaces = 0;
        int idx = -1;
        while ((idx = this.value.indexOf(32, idx + 1)) >= 0) {
            ++numberOfSpaces;
        }
        return this.width() + ((float)this.value.length() * charSpacing + (float)numberOfSpaces * wordSpacing);
    }

    public float getTextRise() {
        Float f = (Float)this.getAttribute("SUBSUPSCRIPT");
        if (f != null) {
            return f.floatValue();
        }
        return 0.0f;
    }

    public float trimLastSpace() {
        BaseFont ft = this.font.getFont();
        if (ft.getFontType() == 2 && ft.getUnicodeEquivalent(' ') != ' ') {
            if (this.value.length() > 1 && this.value.endsWith("\u0001")) {
                this.value = this.value.substring(0, this.value.length() - 1);
                return this.font.width('\u0001');
            }
        } else if (this.value.length() > 1 && this.value.endsWith(" ")) {
            this.value = this.value.substring(0, this.value.length() - 1);
            return this.font.width(' ');
        }
        return 0.0f;
    }

    Object getAttribute(String name) {
        if (this.attributes.containsKey(name)) {
            return this.attributes.get(name);
        }
        return this.noStroke.get(name);
    }

    boolean isAttribute(String name) {
        if (this.attributes.containsKey(name)) {
            return true;
        }
        return this.noStroke.containsKey(name);
    }

    boolean isStroked() {
        if (this.attributes.size() > 0) {
            return true;
        }
        return false;
    }

    boolean isImage() {
        if (this.image != null) {
            return true;
        }
        return false;
    }

    Image getImage() {
        return this.image;
    }

    void setImageOffsetX(float offsetX) {
        this.offsetX = offsetX;
    }

    float getImageOffsetX() {
        return this.offsetX;
    }

    void setImageOffsetY(float offsetY) {
        this.offsetY = offsetY;
    }

    float getImageOffsetY() {
        return this.offsetY;
    }

    void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    boolean isSpecialEncoding() {
        if (!(this.encoding.equals("UnicodeBigUnmarked") || this.encoding.equals("Identity-H"))) {
            return false;
        }
        return true;
    }

    String getEncoding() {
        return this.encoding;
    }

    int length() {
        return this.value.length();
    }

    public boolean isSplitCharacter(int start, int current, int end, char[] cc, PdfChunk[] ck) {
        char c = ck == null ? cc[current] : ck[Math.min(current, ck.length - 1)].getUnicodeEquivalent(cc[current]);
        if (c <= ' ' || c == '-') {
            return true;
        }
        if (c < '\u2e80') {
            return false;
        }
        if ((c < '\u2e80' || c >= '\ud7a0') && (c < '\uf900' || c >= '\ufb00') && (c < '\ufe30' || c >= '\ufe50') && (c < '\uff61' || c >= '\uffa0')) {
            return false;
        }
        return true;
    }

    boolean isExtSplitCharacter(int start, int current, int end, char[] cc, PdfChunk[] ck) {
        return this.splitCharacter.isSplitCharacter(start, current, end, cc, ck);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    String trim(String string) {
        ft = this.font.getFont();
        if (ft.getFontType() != 2 || ft.getUnicodeEquivalent(' ') == ' ') ** GOTO lbl8
        while (string.endsWith("\u0001")) {
            string = string.substring(0, string.length() - 1);
        }
        return string;
lbl-1000: // 1 sources:
        {
            string = string.substring(0, string.length() - 1);
lbl8: // 2 sources:
            ** while (string.endsWith((String)" ") || string.endsWith((String)"\t"))
        }
lbl9: // 1 sources:
        return string;
    }

    public boolean changeLeading() {
        return this.changeLeading;
    }

    float getCharWidth(char c) {
        if (PdfChunk.noPrint(c)) {
            return 0.0f;
        }
        return this.font.width(c);
    }

    public static boolean noPrint(char c) {
        if ((c < '\u200b' || c > '\u200f') && (c < '\u202a' || c > '\u202e')) {
            return false;
        }
        return true;
    }
}

