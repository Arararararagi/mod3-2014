/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec;

public class TIFFFaxDecoder {
    private int bitPointer;
    private int bytePointer;
    private byte[] data;
    private int w;
    private int h;
    private int fillOrder;
    private int changingElemSize = 0;
    private int[] prevChangingElems;
    private int[] currChangingElems;
    private int lastChangingElement = 0;
    private int compression = 2;
    private int uncompressedMode = 0;
    private int fillBits = 0;
    private int oneD;
    static int[] table1;
    static int[] table2;
    static byte[] flipTable;
    static short[] white;
    static short[] additionalMakeup;
    static short[] initBlack;
    static short[] twoBitBlack;
    static short[] black;
    static byte[] twoDCodes;

    static {
        int[] arrn = new int[9];
        arrn[1] = 1;
        arrn[2] = 3;
        arrn[3] = 7;
        arrn[4] = 15;
        arrn[5] = 31;
        arrn[6] = 63;
        arrn[7] = 127;
        arrn[8] = 255;
        table1 = arrn;
        int[] arrn2 = new int[9];
        arrn2[1] = 128;
        arrn2[2] = 192;
        arrn2[3] = 224;
        arrn2[4] = 240;
        arrn2[5] = 248;
        arrn2[6] = 252;
        arrn2[7] = 254;
        arrn2[8] = 255;
        table2 = arrn2;
        byte[] arrby = new byte[256];
        arrby[1] = -128;
        arrby[2] = 64;
        arrby[3] = -64;
        arrby[4] = 32;
        arrby[5] = -96;
        arrby[6] = 96;
        arrby[7] = -32;
        arrby[8] = 16;
        arrby[9] = -112;
        arrby[10] = 80;
        arrby[11] = -48;
        arrby[12] = 48;
        arrby[13] = -80;
        arrby[14] = 112;
        arrby[15] = -16;
        arrby[16] = 8;
        arrby[17] = -120;
        arrby[18] = 72;
        arrby[19] = -56;
        arrby[20] = 40;
        arrby[21] = -88;
        arrby[22] = 104;
        arrby[23] = -24;
        arrby[24] = 24;
        arrby[25] = -104;
        arrby[26] = 88;
        arrby[27] = -40;
        arrby[28] = 56;
        arrby[29] = -72;
        arrby[30] = 120;
        arrby[31] = -8;
        arrby[32] = 4;
        arrby[33] = -124;
        arrby[34] = 68;
        arrby[35] = -60;
        arrby[36] = 36;
        arrby[37] = -92;
        arrby[38] = 100;
        arrby[39] = -28;
        arrby[40] = 20;
        arrby[41] = -108;
        arrby[42] = 84;
        arrby[43] = -44;
        arrby[44] = 52;
        arrby[45] = -76;
        arrby[46] = 116;
        arrby[47] = -12;
        arrby[48] = 12;
        arrby[49] = -116;
        arrby[50] = 76;
        arrby[51] = -52;
        arrby[52] = 44;
        arrby[53] = -84;
        arrby[54] = 108;
        arrby[55] = -20;
        arrby[56] = 28;
        arrby[57] = -100;
        arrby[58] = 92;
        arrby[59] = -36;
        arrby[60] = 60;
        arrby[61] = -68;
        arrby[62] = 124;
        arrby[63] = -4;
        arrby[64] = 2;
        arrby[65] = -126;
        arrby[66] = 66;
        arrby[67] = -62;
        arrby[68] = 34;
        arrby[69] = -94;
        arrby[70] = 98;
        arrby[71] = -30;
        arrby[72] = 18;
        arrby[73] = -110;
        arrby[74] = 82;
        arrby[75] = -46;
        arrby[76] = 50;
        arrby[77] = -78;
        arrby[78] = 114;
        arrby[79] = -14;
        arrby[80] = 10;
        arrby[81] = -118;
        arrby[82] = 74;
        arrby[83] = -54;
        arrby[84] = 42;
        arrby[85] = -86;
        arrby[86] = 106;
        arrby[87] = -22;
        arrby[88] = 26;
        arrby[89] = -102;
        arrby[90] = 90;
        arrby[91] = -38;
        arrby[92] = 58;
        arrby[93] = -70;
        arrby[94] = 122;
        arrby[95] = -6;
        arrby[96] = 6;
        arrby[97] = -122;
        arrby[98] = 70;
        arrby[99] = -58;
        arrby[100] = 38;
        arrby[101] = -90;
        arrby[102] = 102;
        arrby[103] = -26;
        arrby[104] = 22;
        arrby[105] = -106;
        arrby[106] = 86;
        arrby[107] = -42;
        arrby[108] = 54;
        arrby[109] = -74;
        arrby[110] = 118;
        arrby[111] = -10;
        arrby[112] = 14;
        arrby[113] = -114;
        arrby[114] = 78;
        arrby[115] = -50;
        arrby[116] = 46;
        arrby[117] = -82;
        arrby[118] = 110;
        arrby[119] = -18;
        arrby[120] = 30;
        arrby[121] = -98;
        arrby[122] = 94;
        arrby[123] = -34;
        arrby[124] = 62;
        arrby[125] = -66;
        arrby[126] = 126;
        arrby[127] = -2;
        arrby[128] = 1;
        arrby[129] = -127;
        arrby[130] = 65;
        arrby[131] = -63;
        arrby[132] = 33;
        arrby[133] = -95;
        arrby[134] = 97;
        arrby[135] = -31;
        arrby[136] = 17;
        arrby[137] = -111;
        arrby[138] = 81;
        arrby[139] = -47;
        arrby[140] = 49;
        arrby[141] = -79;
        arrby[142] = 113;
        arrby[143] = -15;
        arrby[144] = 9;
        arrby[145] = -119;
        arrby[146] = 73;
        arrby[147] = -55;
        arrby[148] = 41;
        arrby[149] = -87;
        arrby[150] = 105;
        arrby[151] = -23;
        arrby[152] = 25;
        arrby[153] = -103;
        arrby[154] = 89;
        arrby[155] = -39;
        arrby[156] = 57;
        arrby[157] = -71;
        arrby[158] = 121;
        arrby[159] = -7;
        arrby[160] = 5;
        arrby[161] = -123;
        arrby[162] = 69;
        arrby[163] = -59;
        arrby[164] = 37;
        arrby[165] = -91;
        arrby[166] = 101;
        arrby[167] = -27;
        arrby[168] = 21;
        arrby[169] = -107;
        arrby[170] = 85;
        arrby[171] = -43;
        arrby[172] = 53;
        arrby[173] = -75;
        arrby[174] = 117;
        arrby[175] = -11;
        arrby[176] = 13;
        arrby[177] = -115;
        arrby[178] = 77;
        arrby[179] = -51;
        arrby[180] = 45;
        arrby[181] = -83;
        arrby[182] = 109;
        arrby[183] = -19;
        arrby[184] = 29;
        arrby[185] = -99;
        arrby[186] = 93;
        arrby[187] = -35;
        arrby[188] = 61;
        arrby[189] = -67;
        arrby[190] = 125;
        arrby[191] = -3;
        arrby[192] = 3;
        arrby[193] = -125;
        arrby[194] = 67;
        arrby[195] = -61;
        arrby[196] = 35;
        arrby[197] = -93;
        arrby[198] = 99;
        arrby[199] = -29;
        arrby[200] = 19;
        arrby[201] = -109;
        arrby[202] = 83;
        arrby[203] = -45;
        arrby[204] = 51;
        arrby[205] = -77;
        arrby[206] = 115;
        arrby[207] = -13;
        arrby[208] = 11;
        arrby[209] = -117;
        arrby[210] = 75;
        arrby[211] = -53;
        arrby[212] = 43;
        arrby[213] = -85;
        arrby[214] = 107;
        arrby[215] = -21;
        arrby[216] = 27;
        arrby[217] = -101;
        arrby[218] = 91;
        arrby[219] = -37;
        arrby[220] = 59;
        arrby[221] = -69;
        arrby[222] = 123;
        arrby[223] = -5;
        arrby[224] = 7;
        arrby[225] = -121;
        arrby[226] = 71;
        arrby[227] = -57;
        arrby[228] = 39;
        arrby[229] = -89;
        arrby[230] = 103;
        arrby[231] = -25;
        arrby[232] = 23;
        arrby[233] = -105;
        arrby[234] = 87;
        arrby[235] = -41;
        arrby[236] = 55;
        arrby[237] = -73;
        arrby[238] = 119;
        arrby[239] = -9;
        arrby[240] = 15;
        arrby[241] = -113;
        arrby[242] = 79;
        arrby[243] = -49;
        arrby[244] = 47;
        arrby[245] = -81;
        arrby[246] = 111;
        arrby[247] = -17;
        arrby[248] = 31;
        arrby[249] = -97;
        arrby[250] = 95;
        arrby[251] = -33;
        arrby[252] = 63;
        arrby[253] = -65;
        arrby[254] = 127;
        arrby[255] = -1;
        flipTable = arrby;
        white = new short[]{6430, 6400, 6400, 6400, 3225, 3225, 3225, 3225, 944, 944, 944, 944, 976, 976, 976, 976, 1456, 1456, 1456, 1456, 1488, 1488, 1488, 1488, 718, 718, 718, 718, 718, 718, 718, 718, 750, 750, 750, 750, 750, 750, 750, 750, 1520, 1520, 1520, 1520, 1552, 1552, 1552, 1552, 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 654, 654, 654, 654, 654, 654, 654, 654, 1072, 1072, 1072, 1072, 1104, 1104, 1104, 1104, 1136, 1136, 1136, 1136, 1168, 1168, 1168, 1168, 1200, 1200, 1200, 1200, 1232, 1232, 1232, 1232, 622, 622, 622, 622, 622, 622, 622, 622, 1008, 1008, 1008, 1008, 1040, 1040, 1040, 1040, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 396, 1712, 1712, 1712, 1712, 1744, 1744, 1744, 1744, 846, 846, 846, 846, 846, 846, 846, 846, 1264, 1264, 1264, 1264, 1296, 1296, 1296, 1296, 1328, 1328, 1328, 1328, 1360, 1360, 1360, 1360, 1392, 1392, 1392, 1392, 1424, 1424, 1424, 1424, 686, 686, 686, 686, 686, 686, 686, 686, 910, 910, 910, 910, 910, 910, 910, 910, 1968, 1968, 1968, 1968, 2000, 2000, 2000, 2000, 2032, 2032, 2032, 2032, 16, 16, 16, 16, 10257, 10257, 10257, 10257, 12305, 12305, 12305, 12305, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 330, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 362, 878, 878, 878, 878, 878, 878, 878, 878, 1904, 1904, 1904, 1904, 1936, 1936, 1936, 1936, -18413, -18413, -16365, -16365, -14317, -14317, -10221, -10221, 590, 590, 590, 590, 590, 590, 590, 590, 782, 782, 782, 782, 782, 782, 782, 782, 1584, 1584, 1584, 1584, 1616, 1616, 1616, 1616, 1648, 1648, 1648, 1648, 1680, 1680, 1680, 1680, 814, 814, 814, 814, 814, 814, 814, 814, 1776, 1776, 1776, 1776, 1808, 1808, 1808, 1808, 1840, 1840, 1840, 1840, 1872, 1872, 1872, 1872, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, 6157, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, -12275, 14353, 14353, 14353, 14353, 16401, 16401, 16401, 16401, 22547, 22547, 24595, 24595, 20497, 20497, 20497, 20497, 18449, 18449, 18449, 18449, 26643, 26643, 28691, 28691, 30739, 30739, -32749, -32749, -30701, -30701, -28653, -28653, -26605, -26605, -24557, -24557, -22509, -22509, -20461, -20461, 8207, 8207, 8207, 8207, 8207, 8207, 8207, 8207, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 72, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 4107, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 266, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 298, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 524, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 556, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 136, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 168, 460, 460, 460, 460, 460, 460, 460, 460, 460, 460, 460, 460, 460, 460, 460, 460, 492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 492, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 2059, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232, 232};
        additionalMakeup = new short[]{28679, 28679, 31752, -32759, -31735, -30711, -29687, -28663, 29703, 29703, 30727, 30727, -27639, -26615, -25591, -24567};
        initBlack = new short[]{3226, 6412, 200, 168, 38, 38, 134, 134, 100, 100, 100, 100, 68, 68, 68, 68};
        twoBitBlack = new short[]{292, 260, 226, 226};
        short[] arrs = new short[512];
        arrs[0] = 62;
        arrs[1] = 62;
        arrs[2] = 30;
        arrs[3] = 30;
        arrs[32] = 3225;
        arrs[33] = 3225;
        arrs[34] = 3225;
        arrs[35] = 3225;
        arrs[36] = 3225;
        arrs[37] = 3225;
        arrs[38] = 3225;
        arrs[39] = 3225;
        arrs[40] = 3225;
        arrs[41] = 3225;
        arrs[42] = 3225;
        arrs[43] = 3225;
        arrs[44] = 3225;
        arrs[45] = 3225;
        arrs[46] = 3225;
        arrs[47] = 3225;
        arrs[48] = 3225;
        arrs[49] = 3225;
        arrs[50] = 3225;
        arrs[51] = 3225;
        arrs[52] = 3225;
        arrs[53] = 3225;
        arrs[54] = 3225;
        arrs[55] = 3225;
        arrs[56] = 3225;
        arrs[57] = 3225;
        arrs[58] = 3225;
        arrs[59] = 3225;
        arrs[60] = 3225;
        arrs[61] = 3225;
        arrs[62] = 3225;
        arrs[63] = 3225;
        arrs[64] = 588;
        arrs[65] = 588;
        arrs[66] = 588;
        arrs[67] = 588;
        arrs[68] = 588;
        arrs[69] = 588;
        arrs[70] = 588;
        arrs[71] = 588;
        arrs[72] = 1680;
        arrs[73] = 1680;
        arrs[74] = 20499;
        arrs[75] = 22547;
        arrs[76] = 24595;
        arrs[77] = 26643;
        arrs[78] = 1776;
        arrs[79] = 1776;
        arrs[80] = 1808;
        arrs[81] = 1808;
        arrs[82] = -24557;
        arrs[83] = -22509;
        arrs[84] = -20461;
        arrs[85] = -18413;
        arrs[86] = 1904;
        arrs[87] = 1904;
        arrs[88] = 1936;
        arrs[89] = 1936;
        arrs[90] = -16365;
        arrs[91] = -14317;
        arrs[92] = 782;
        arrs[93] = 782;
        arrs[94] = 782;
        arrs[95] = 782;
        arrs[96] = 814;
        arrs[97] = 814;
        arrs[98] = 814;
        arrs[99] = 814;
        arrs[100] = -12269;
        arrs[101] = -10221;
        arrs[102] = 10257;
        arrs[103] = 10257;
        arrs[104] = 12305;
        arrs[105] = 12305;
        arrs[106] = 14353;
        arrs[107] = 14353;
        arrs[108] = 16403;
        arrs[109] = 18451;
        arrs[110] = 1712;
        arrs[111] = 1712;
        arrs[112] = 1744;
        arrs[113] = 1744;
        arrs[114] = 28691;
        arrs[115] = 30739;
        arrs[116] = -32749;
        arrs[117] = -30701;
        arrs[118] = -28653;
        arrs[119] = -26605;
        arrs[120] = 2061;
        arrs[121] = 2061;
        arrs[122] = 2061;
        arrs[123] = 2061;
        arrs[124] = 2061;
        arrs[125] = 2061;
        arrs[126] = 2061;
        arrs[127] = 2061;
        arrs[128] = 424;
        arrs[129] = 424;
        arrs[130] = 424;
        arrs[131] = 424;
        arrs[132] = 424;
        arrs[133] = 424;
        arrs[134] = 424;
        arrs[135] = 424;
        arrs[136] = 424;
        arrs[137] = 424;
        arrs[138] = 424;
        arrs[139] = 424;
        arrs[140] = 424;
        arrs[141] = 424;
        arrs[142] = 424;
        arrs[143] = 424;
        arrs[144] = 424;
        arrs[145] = 424;
        arrs[146] = 424;
        arrs[147] = 424;
        arrs[148] = 424;
        arrs[149] = 424;
        arrs[150] = 424;
        arrs[151] = 424;
        arrs[152] = 424;
        arrs[153] = 424;
        arrs[154] = 424;
        arrs[155] = 424;
        arrs[156] = 424;
        arrs[157] = 424;
        arrs[158] = 424;
        arrs[159] = 424;
        arrs[160] = 750;
        arrs[161] = 750;
        arrs[162] = 750;
        arrs[163] = 750;
        arrs[164] = 1616;
        arrs[165] = 1616;
        arrs[166] = 1648;
        arrs[167] = 1648;
        arrs[168] = 1424;
        arrs[169] = 1424;
        arrs[170] = 1456;
        arrs[171] = 1456;
        arrs[172] = 1488;
        arrs[173] = 1488;
        arrs[174] = 1520;
        arrs[175] = 1520;
        arrs[176] = 1840;
        arrs[177] = 1840;
        arrs[178] = 1872;
        arrs[179] = 1872;
        arrs[180] = 1968;
        arrs[181] = 1968;
        arrs[182] = 8209;
        arrs[183] = 8209;
        arrs[184] = 524;
        arrs[185] = 524;
        arrs[186] = 524;
        arrs[187] = 524;
        arrs[188] = 524;
        arrs[189] = 524;
        arrs[190] = 524;
        arrs[191] = 524;
        arrs[192] = 556;
        arrs[193] = 556;
        arrs[194] = 556;
        arrs[195] = 556;
        arrs[196] = 556;
        arrs[197] = 556;
        arrs[198] = 556;
        arrs[199] = 556;
        arrs[200] = 1552;
        arrs[201] = 1552;
        arrs[202] = 1584;
        arrs[203] = 1584;
        arrs[204] = 2000;
        arrs[205] = 2000;
        arrs[206] = 2032;
        arrs[207] = 2032;
        arrs[208] = 976;
        arrs[209] = 976;
        arrs[210] = 1008;
        arrs[211] = 1008;
        arrs[212] = 1040;
        arrs[213] = 1040;
        arrs[214] = 1072;
        arrs[215] = 1072;
        arrs[216] = 1296;
        arrs[217] = 1296;
        arrs[218] = 1328;
        arrs[219] = 1328;
        arrs[220] = 718;
        arrs[221] = 718;
        arrs[222] = 718;
        arrs[223] = 718;
        arrs[224] = 456;
        arrs[225] = 456;
        arrs[226] = 456;
        arrs[227] = 456;
        arrs[228] = 456;
        arrs[229] = 456;
        arrs[230] = 456;
        arrs[231] = 456;
        arrs[232] = 456;
        arrs[233] = 456;
        arrs[234] = 456;
        arrs[235] = 456;
        arrs[236] = 456;
        arrs[237] = 456;
        arrs[238] = 456;
        arrs[239] = 456;
        arrs[240] = 456;
        arrs[241] = 456;
        arrs[242] = 456;
        arrs[243] = 456;
        arrs[244] = 456;
        arrs[245] = 456;
        arrs[246] = 456;
        arrs[247] = 456;
        arrs[248] = 456;
        arrs[249] = 456;
        arrs[250] = 456;
        arrs[251] = 456;
        arrs[252] = 456;
        arrs[253] = 456;
        arrs[254] = 456;
        arrs[255] = 456;
        arrs[256] = 326;
        arrs[257] = 326;
        arrs[258] = 326;
        arrs[259] = 326;
        arrs[260] = 326;
        arrs[261] = 326;
        arrs[262] = 326;
        arrs[263] = 326;
        arrs[264] = 326;
        arrs[265] = 326;
        arrs[266] = 326;
        arrs[267] = 326;
        arrs[268] = 326;
        arrs[269] = 326;
        arrs[270] = 326;
        arrs[271] = 326;
        arrs[272] = 326;
        arrs[273] = 326;
        arrs[274] = 326;
        arrs[275] = 326;
        arrs[276] = 326;
        arrs[277] = 326;
        arrs[278] = 326;
        arrs[279] = 326;
        arrs[280] = 326;
        arrs[281] = 326;
        arrs[282] = 326;
        arrs[283] = 326;
        arrs[284] = 326;
        arrs[285] = 326;
        arrs[286] = 326;
        arrs[287] = 326;
        arrs[288] = 326;
        arrs[289] = 326;
        arrs[290] = 326;
        arrs[291] = 326;
        arrs[292] = 326;
        arrs[293] = 326;
        arrs[294] = 326;
        arrs[295] = 326;
        arrs[296] = 326;
        arrs[297] = 326;
        arrs[298] = 326;
        arrs[299] = 326;
        arrs[300] = 326;
        arrs[301] = 326;
        arrs[302] = 326;
        arrs[303] = 326;
        arrs[304] = 326;
        arrs[305] = 326;
        arrs[306] = 326;
        arrs[307] = 326;
        arrs[308] = 326;
        arrs[309] = 326;
        arrs[310] = 326;
        arrs[311] = 326;
        arrs[312] = 326;
        arrs[313] = 326;
        arrs[314] = 326;
        arrs[315] = 326;
        arrs[316] = 326;
        arrs[317] = 326;
        arrs[318] = 326;
        arrs[319] = 326;
        arrs[320] = 358;
        arrs[321] = 358;
        arrs[322] = 358;
        arrs[323] = 358;
        arrs[324] = 358;
        arrs[325] = 358;
        arrs[326] = 358;
        arrs[327] = 358;
        arrs[328] = 358;
        arrs[329] = 358;
        arrs[330] = 358;
        arrs[331] = 358;
        arrs[332] = 358;
        arrs[333] = 358;
        arrs[334] = 358;
        arrs[335] = 358;
        arrs[336] = 358;
        arrs[337] = 358;
        arrs[338] = 358;
        arrs[339] = 358;
        arrs[340] = 358;
        arrs[341] = 358;
        arrs[342] = 358;
        arrs[343] = 358;
        arrs[344] = 358;
        arrs[345] = 358;
        arrs[346] = 358;
        arrs[347] = 358;
        arrs[348] = 358;
        arrs[349] = 358;
        arrs[350] = 358;
        arrs[351] = 358;
        arrs[352] = 358;
        arrs[353] = 358;
        arrs[354] = 358;
        arrs[355] = 358;
        arrs[356] = 358;
        arrs[357] = 358;
        arrs[358] = 358;
        arrs[359] = 358;
        arrs[360] = 358;
        arrs[361] = 358;
        arrs[362] = 358;
        arrs[363] = 358;
        arrs[364] = 358;
        arrs[365] = 358;
        arrs[366] = 358;
        arrs[367] = 358;
        arrs[368] = 358;
        arrs[369] = 358;
        arrs[370] = 358;
        arrs[371] = 358;
        arrs[372] = 358;
        arrs[373] = 358;
        arrs[374] = 358;
        arrs[375] = 358;
        arrs[376] = 358;
        arrs[377] = 358;
        arrs[378] = 358;
        arrs[379] = 358;
        arrs[380] = 358;
        arrs[381] = 358;
        arrs[382] = 358;
        arrs[383] = 358;
        arrs[384] = 490;
        arrs[385] = 490;
        arrs[386] = 490;
        arrs[387] = 490;
        arrs[388] = 490;
        arrs[389] = 490;
        arrs[390] = 490;
        arrs[391] = 490;
        arrs[392] = 490;
        arrs[393] = 490;
        arrs[394] = 490;
        arrs[395] = 490;
        arrs[396] = 490;
        arrs[397] = 490;
        arrs[398] = 490;
        arrs[399] = 490;
        arrs[400] = 4113;
        arrs[401] = 4113;
        arrs[402] = 6161;
        arrs[403] = 6161;
        arrs[404] = 848;
        arrs[405] = 848;
        arrs[406] = 880;
        arrs[407] = 880;
        arrs[408] = 912;
        arrs[409] = 912;
        arrs[410] = 944;
        arrs[411] = 944;
        arrs[412] = 622;
        arrs[413] = 622;
        arrs[414] = 622;
        arrs[415] = 622;
        arrs[416] = 654;
        arrs[417] = 654;
        arrs[418] = 654;
        arrs[419] = 654;
        arrs[420] = 1104;
        arrs[421] = 1104;
        arrs[422] = 1136;
        arrs[423] = 1136;
        arrs[424] = 1168;
        arrs[425] = 1168;
        arrs[426] = 1200;
        arrs[427] = 1200;
        arrs[428] = 1232;
        arrs[429] = 1232;
        arrs[430] = 1264;
        arrs[431] = 1264;
        arrs[432] = 686;
        arrs[433] = 686;
        arrs[434] = 686;
        arrs[435] = 686;
        arrs[436] = 1360;
        arrs[437] = 1360;
        arrs[438] = 1392;
        arrs[439] = 1392;
        arrs[440] = 12;
        arrs[441] = 12;
        arrs[442] = 12;
        arrs[443] = 12;
        arrs[444] = 12;
        arrs[445] = 12;
        arrs[446] = 12;
        arrs[447] = 12;
        arrs[448] = 390;
        arrs[449] = 390;
        arrs[450] = 390;
        arrs[451] = 390;
        arrs[452] = 390;
        arrs[453] = 390;
        arrs[454] = 390;
        arrs[455] = 390;
        arrs[456] = 390;
        arrs[457] = 390;
        arrs[458] = 390;
        arrs[459] = 390;
        arrs[460] = 390;
        arrs[461] = 390;
        arrs[462] = 390;
        arrs[463] = 390;
        arrs[464] = 390;
        arrs[465] = 390;
        arrs[466] = 390;
        arrs[467] = 390;
        arrs[468] = 390;
        arrs[469] = 390;
        arrs[470] = 390;
        arrs[471] = 390;
        arrs[472] = 390;
        arrs[473] = 390;
        arrs[474] = 390;
        arrs[475] = 390;
        arrs[476] = 390;
        arrs[477] = 390;
        arrs[478] = 390;
        arrs[479] = 390;
        arrs[480] = 390;
        arrs[481] = 390;
        arrs[482] = 390;
        arrs[483] = 390;
        arrs[484] = 390;
        arrs[485] = 390;
        arrs[486] = 390;
        arrs[487] = 390;
        arrs[488] = 390;
        arrs[489] = 390;
        arrs[490] = 390;
        arrs[491] = 390;
        arrs[492] = 390;
        arrs[493] = 390;
        arrs[494] = 390;
        arrs[495] = 390;
        arrs[496] = 390;
        arrs[497] = 390;
        arrs[498] = 390;
        arrs[499] = 390;
        arrs[500] = 390;
        arrs[501] = 390;
        arrs[502] = 390;
        arrs[503] = 390;
        arrs[504] = 390;
        arrs[505] = 390;
        arrs[506] = 390;
        arrs[507] = 390;
        arrs[508] = 390;
        arrs[509] = 390;
        arrs[510] = 390;
        arrs[511] = 390;
        black = arrs;
        twoDCodes = new byte[]{80, 88, 23, 71, 30, 30, 62, 62, 4, 4, 4, 4, 4, 4, 4, 4, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41};
    }

    public TIFFFaxDecoder(int fillOrder, int w, int h) {
        this.fillOrder = fillOrder;
        this.w = w;
        this.h = h;
        this.bitPointer = 0;
        this.bytePointer = 0;
        this.prevChangingElems = new int[w];
        this.currChangingElems = new int[w];
    }

    public void decode1D(byte[] buffer, byte[] compData, int startX, int height) {
        this.data = compData;
        int lineOffset = 0;
        int scanlineStride = (this.w + 7) / 8;
        this.bitPointer = 0;
        this.bytePointer = 0;
        for (int i = 0; i < height; ++i) {
            this.decodeNextScanline(buffer, lineOffset, startX);
            lineOffset+=scanlineStride;
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public void decodeNextScanline(byte[] buffer, int lineOffset, int bitOffset) {
        bits = 0;
        code = 0;
        isT = 0;
        isWhite = true;
        dstEnd = false;
        this.changingElemSize = 0;
        ** GOTO lbl86
        {
            current = this.nextNBits(10);
            entry = TIFFFaxDecoder.white[current];
            isT = entry & 1;
            bits = entry >>> 1 & 15;
            if (bits == 12) {
                twoBits = this.nextLesserThan8Bits(2);
                current = current << 2 & 12 | twoBits;
                entry = TIFFFaxDecoder.additionalMakeup[current];
                bits = entry >>> 1 & 7;
                code = entry >>> 4 & 4095;
                bitOffset+=code;
                this.updatePointer(4 - bits);
            } else {
                if (bits == 0) {
                    throw new RuntimeException("Invalid code encountered.");
                }
                if (bits == 15) {
                    throw new RuntimeException("EOL code word encountered in White run.");
                }
                code = entry >>> 5 & 2047;
                bitOffset+=code;
                this.updatePointer(10 - bits);
                if (isT == 0) {
                    isWhite = false;
                    this.currChangingElems[this.changingElemSize++] = bitOffset;
                }
            }
            do {
                if (isWhite) continue block0;
                if (bitOffset != this.w) ** GOTO lbl81
                if (this.compression != 2) break block0;
                this.advancePointer();
                break block0;
lbl-1000: // 1 sources:
                {
                    current = this.nextLesserThan8Bits(4);
                    entry = TIFFFaxDecoder.initBlack[current];
                    isT = entry & 1;
                    bits = entry >>> 1 & 15;
                    code = entry >>> 5 & 2047;
                    if (code == 100) {
                        current = this.nextNBits(9);
                        entry = TIFFFaxDecoder.black[current];
                        isT = entry & 1;
                        bits = entry >>> 1 & 15;
                        code = entry >>> 5 & 2047;
                        if (bits == 12) {
                            this.updatePointer(5);
                            current = this.nextLesserThan8Bits(4);
                            entry = TIFFFaxDecoder.additionalMakeup[current];
                            bits = entry >>> 1 & 7;
                            code = entry >>> 4 & 4095;
                            this.setToBlack(buffer, lineOffset, bitOffset, code);
                            bitOffset+=code;
                            this.updatePointer(4 - bits);
                            continue;
                        }
                        if (bits == 15) {
                            throw new RuntimeException("EOL code word encountered in Black run.");
                        }
                        this.setToBlack(buffer, lineOffset, bitOffset, code);
                        bitOffset+=code;
                        this.updatePointer(9 - bits);
                        if (isT != 0) continue;
                        isWhite = true;
                        this.currChangingElems[this.changingElemSize++] = bitOffset;
                        continue;
                    }
                    if (code == 200) {
                        current = this.nextLesserThan8Bits(2);
                        entry = TIFFFaxDecoder.twoBitBlack[current];
                        code = entry >>> 5 & 2047;
                        bits = entry >>> 1 & 15;
                        this.setToBlack(buffer, lineOffset, bitOffset, code);
                        this.updatePointer(2 - bits);
                        isWhite = true;
                        this.currChangingElems[this.changingElemSize++] = bitOffset+=code;
                        continue;
                    }
                    this.setToBlack(buffer, lineOffset, bitOffset, code);
                    this.updatePointer(4 - bits);
                    isWhite = true;
                    this.currChangingElems[this.changingElemSize++] = bitOffset+=code;
lbl81: // 6 sources:
                    ** while (!isWhite)
                }
lbl82: // 1 sources:
                if (bitOffset != this.w) continue;
                if (this.compression != 2) break block0;
                this.advancePointer();
                break block0;
lbl86: // 2 sources:
            } while (bitOffset < this.w);
        }
        this.currChangingElems[this.changingElemSize++] = bitOffset;
    }

    public void decode2D(byte[] buffer, byte[] compData, int startX, int height, long tiffT4Options) {
        this.data = compData;
        this.compression = 3;
        this.bitPointer = 0;
        this.bytePointer = 0;
        int scanlineStride = (this.w + 7) / 8;
        int[] b = new int[2];
        int currIndex = 0;
        this.oneD = (int)(tiffT4Options & 1);
        this.uncompressedMode = (int)((tiffT4Options & 2) >> true);
        this.fillBits = (int)((tiffT4Options & 4) >> 2);
        if (this.readEOL(true) != 1) {
            throw new RuntimeException("First scanline must be 1D encoded.");
        }
        int lineOffset = 0;
        this.decodeNextScanline(buffer, lineOffset, startX);
        lineOffset+=scanlineStride;
        for (int lines = 1; lines < height; ++lines) {
            if (this.readEOL(false) == 0) {
                int[] temp = this.prevChangingElems;
                this.prevChangingElems = this.currChangingElems;
                this.currChangingElems = temp;
                currIndex = 0;
                int a0 = -1;
                boolean isWhite = true;
                int bitOffset = startX;
                this.lastChangingElement = 0;
                while (bitOffset < this.w) {
                    this.getNextChangingElement(a0, isWhite, b);
                    int b1 = b[0];
                    int b2 = b[1];
                    int entry = this.nextLesserThan8Bits(7);
                    entry = twoDCodes[entry] & 255;
                    int code = (entry & 120) >>> 3;
                    int bits = entry & 7;
                    if (code == 0) {
                        if (!isWhite) {
                            this.setToBlack(buffer, lineOffset, bitOffset, b2 - bitOffset);
                        }
                        bitOffset = a0 = b2;
                        this.updatePointer(7 - bits);
                        continue;
                    }
                    if (code == 1) {
                        int number;
                        this.updatePointer(7 - bits);
                        if (isWhite) {
                            number = this.decodeWhiteCodeWord();
                            this.currChangingElems[currIndex++] = bitOffset+=number;
                            number = this.decodeBlackCodeWord();
                            this.setToBlack(buffer, lineOffset, bitOffset, number);
                            this.currChangingElems[currIndex++] = bitOffset+=number;
                        } else {
                            number = this.decodeBlackCodeWord();
                            this.setToBlack(buffer, lineOffset, bitOffset, number);
                            this.currChangingElems[currIndex++] = bitOffset+=number;
                            number = this.decodeWhiteCodeWord();
                            this.currChangingElems[currIndex++] = bitOffset+=number;
                        }
                        a0 = bitOffset;
                        continue;
                    }
                    if (code <= 8) {
                        int a1 = b1 + (code - 5);
                        this.currChangingElems[currIndex++] = a1;
                        if (!isWhite) {
                            this.setToBlack(buffer, lineOffset, bitOffset, a1 - bitOffset);
                        }
                        bitOffset = a0 = a1;
                        isWhite = !isWhite;
                        this.updatePointer(7 - bits);
                        continue;
                    }
                    throw new RuntimeException("Invalid code encountered while decoding 2D group 3 compressed data.");
                }
                this.currChangingElems[currIndex++] = bitOffset;
                this.changingElemSize = currIndex;
            } else {
                this.decodeNextScanline(buffer, lineOffset, startX);
            }
            lineOffset+=scanlineStride;
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public synchronized void decodeT6(byte[] buffer, byte[] compData, int startX, int height, long tiffT6Options) {
        this.data = compData;
        this.compression = 4;
        this.bitPointer = 0;
        this.bytePointer = 0;
        scanlineStride = (this.w + 7) / 8;
        bufferOffset = false;
        b = new int[2];
        this.uncompressedMode = (int)((tiffT6Options & 2) >> true);
        cce = this.currChangingElems;
        this.changingElemSize = 0;
        cce[this.changingElemSize++] = this.w;
        cce[this.changingElemSize++] = this.w;
        lineOffset = 0;
        for (lines = 0; lines < height; lineOffset+=scanlineStride, ++lines) {
            a0 = -1;
            isWhite = true;
            temp = this.prevChangingElems;
            this.prevChangingElems = this.currChangingElems;
            cce = this.currChangingElems = temp;
            currIndex = 0;
            bitOffset = startX;
            this.lastChangingElement = 0;
            while (bitOffset < this.w) {
                this.getNextChangingElement(a0, isWhite, b);
                b1 = b[0];
                b2 = b[1];
                entry = this.nextLesserThan8Bits(7);
                entry = TIFFFaxDecoder.twoDCodes[entry] & 255;
                code = (entry & 120) >>> 3;
                bits = entry & 7;
                if (code == 0) {
                    if (!isWhite) {
                        this.setToBlack(buffer, lineOffset, bitOffset, b2 - bitOffset);
                    }
                    bitOffset = a0 = b2;
                    this.updatePointer(7 - bits);
                    continue;
                }
                if (code == 1) {
                    this.updatePointer(7 - bits);
                    if (isWhite) {
                        number = this.decodeWhiteCodeWord();
                        cce[currIndex++] = bitOffset+=number;
                        number = this.decodeBlackCodeWord();
                        this.setToBlack(buffer, lineOffset, bitOffset, number);
                        cce[currIndex++] = bitOffset+=number;
                    } else {
                        number = this.decodeBlackCodeWord();
                        this.setToBlack(buffer, lineOffset, bitOffset, number);
                        cce[currIndex++] = bitOffset+=number;
                        number = this.decodeWhiteCodeWord();
                        cce[currIndex++] = bitOffset+=number;
                    }
                    a0 = bitOffset;
                    continue;
                }
                if (code <= 8) {
                    a1 = b1 + (code - 5);
                    cce[currIndex++] = a1;
                    if (!isWhite) {
                        this.setToBlack(buffer, lineOffset, bitOffset, a1 - bitOffset);
                    }
                    bitOffset = a0 = a1;
                    isWhite = isWhite == false;
                    this.updatePointer(7 - bits);
                    continue;
                }
                if (code != 11) throw new RuntimeException("Invalid code encountered while decoding 2D group 4 compressed data.");
                if (this.nextLesserThan8Bits(3) != 7) {
                    throw new RuntimeException("Invalid code encountered while decoding 2D group 4 compressed data.");
                }
                zeros = 0;
                exit = false;
                ** GOTO lbl95
                {
                    ++zeros;
                    do {
                        if (this.nextLesserThan8Bits(1) != 1) continue block2;
                        if (zeros > 5) {
                            if (!(isWhite || (zeros-=6) <= 0)) {
                                cce[currIndex++] = bitOffset;
                            }
                            bitOffset+=zeros;
                            if (zeros > 0) {
                                isWhite = true;
                            }
                            if (this.nextLesserThan8Bits(1) == 0) {
                                if (!isWhite) {
                                    cce[currIndex++] = bitOffset;
                                }
                                isWhite = true;
                            } else {
                                cce[currIndex++] = bitOffset;
                                isWhite = false;
                            }
                            exit = true;
                        }
                        if (zeros == 5) {
                            if (!isWhite) {
                                cce[currIndex++] = bitOffset;
                            }
                            bitOffset+=zeros;
                            isWhite = true;
                            continue;
                        }
                        cce[currIndex++] = bitOffset+=zeros;
                        this.setToBlack(buffer, lineOffset, bitOffset, 1);
                        ++bitOffset;
                        isWhite = false;
lbl95: // 3 sources:
                    } while (!exit);
                }
            }
            if (currIndex < cce.length) {
                cce[currIndex++] = bitOffset;
            }
            this.changingElemSize = currIndex;
        }
    }

    private void setToBlack(byte[] buffer, int lineOffset, int bitOffset, int numBits) {
        int bitNum = 8 * lineOffset + bitOffset;
        int lastBit = bitNum + numBits;
        int byteNum = bitNum >> 3;
        int shift = bitNum & 7;
        if (shift > 0) {
            byte val = buffer[byteNum];
            for (int maskVal = 1 << 7 - shift; maskVal > 0 && bitNum < lastBit; maskVal>>=1, ++bitNum) {
                val = (byte)(val | maskVal);
            }
            buffer[byteNum] = val;
        }
        byteNum = bitNum >> 3;
        while (bitNum < lastBit - 7) {
            buffer[byteNum++] = -1;
            bitNum+=8;
        }
        while (bitNum < lastBit) {
            byteNum = bitNum >> 3;
            byte[] arrby = buffer;
            int n = byteNum;
            arrby[n] = (byte)(arrby[n] | 1 << 7 - (bitNum & 7));
            ++bitNum;
        }
    }

    private int decodeWhiteCodeWord() {
        int code = -1;
        int runLength = 0;
        boolean isWhite = true;
        while (isWhite) {
            int current = this.nextNBits(10);
            short entry = white[current];
            int isT = entry & 1;
            int bits = entry >>> 1 & 15;
            if (bits == 12) {
                int twoBits = this.nextLesserThan8Bits(2);
                current = current << 2 & 12 | twoBits;
                entry = additionalMakeup[current];
                bits = entry >>> 1 & 7;
                code = entry >>> 4 & 4095;
                runLength+=code;
                this.updatePointer(4 - bits);
                continue;
            }
            if (bits == 0) {
                throw new RuntimeException("Invalid code encountered.");
            }
            if (bits == 15) {
                throw new RuntimeException("EOL code word encountered in White run.");
            }
            code = entry >>> 5 & 2047;
            runLength+=code;
            this.updatePointer(10 - bits);
            if (isT != 0) continue;
            isWhite = false;
        }
        return runLength;
    }

    private int decodeBlackCodeWord() {
        int code = -1;
        int runLength = 0;
        boolean isWhite = false;
        while (!isWhite) {
            int current = this.nextLesserThan8Bits(4);
            short entry = initBlack[current];
            int isT = entry & 1;
            int bits = entry >>> 1 & 15;
            code = entry >>> 5 & 2047;
            if (code == 100) {
                current = this.nextNBits(9);
                entry = black[current];
                isT = entry & 1;
                bits = entry >>> 1 & 15;
                code = entry >>> 5 & 2047;
                if (bits == 12) {
                    this.updatePointer(5);
                    current = this.nextLesserThan8Bits(4);
                    entry = additionalMakeup[current];
                    bits = entry >>> 1 & 7;
                    code = entry >>> 4 & 4095;
                    runLength+=code;
                    this.updatePointer(4 - bits);
                    continue;
                }
                if (bits == 15) {
                    throw new RuntimeException("EOL code word encountered in Black run.");
                }
                runLength+=code;
                this.updatePointer(9 - bits);
                if (isT != 0) continue;
                isWhite = true;
                continue;
            }
            if (code == 200) {
                current = this.nextLesserThan8Bits(2);
                entry = twoBitBlack[current];
                code = entry >>> 5 & 2047;
                runLength+=code;
                bits = entry >>> 1 & 15;
                this.updatePointer(2 - bits);
                isWhite = true;
                continue;
            }
            runLength+=code;
            this.updatePointer(4 - bits);
            isWhite = true;
        }
        return runLength;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private int readEOL(boolean isFirstEOL) {
        if (this.fillBits == 0) {
            next12Bits = this.nextNBits(12);
            if (isFirstEOL && next12Bits == 0 && this.nextNBits(4) == 1) {
                this.fillBits = 1;
                return 1;
            }
            if (next12Bits != 1) {
                throw new RuntimeException("Scanline must begin with EOL code word.");
            } else {
                ** GOTO lbl17
            }
        }
        if (this.fillBits != 1) ** GOTO lbl17
        bitsLeft = 8 - this.bitPointer;
        if (this.nextNBits(bitsLeft) != 0) {
            throw new RuntimeException("All fill bits preceding EOL code must be 0.");
        }
        if (bitsLeft >= 4 || this.nextNBits(8) == 0) ** GOTO lbl16
        throw new RuntimeException("All fill bits preceding EOL code must be 0.");
lbl-1000: // 1 sources:
        {
            if (n == 0) continue;
            throw new RuntimeException("All fill bits preceding EOL code must be 0.");
lbl16: // 2 sources:
            ** while ((n = this.nextNBits((int)8)) != 1)
        }
lbl17: // 4 sources:
        if (this.oneD != 0) return this.nextLesserThan8Bits(1);
        return 1;
    }

    private void getNextChangingElement(int a0, boolean isWhite, int[] ret) {
        int start;
        int i;
        int[] pce = this.prevChangingElems;
        int ces = this.changingElemSize;
        int n = start = this.lastChangingElement > 0 ? this.lastChangingElement - 1 : 0;
        start = isWhite ? (start&=-2) : (start|=1);
        for (i = start; i < ces; i+=2) {
            int temp = pce[i];
            if (temp <= a0) continue;
            this.lastChangingElement = i;
            ret[0] = temp;
            break;
        }
        if (i + 1 < ces) {
            ret[1] = pce[i + 1];
        }
    }

    private int nextNBits(int bitsToGet) {
        byte next2next;
        byte next;
        byte b;
        int l = this.data.length - 1;
        int bp = this.bytePointer;
        if (this.fillOrder == 1) {
            b = this.data[bp];
            if (bp == l) {
                next = 0;
                next2next = 0;
            } else if (bp + 1 == l) {
                next = this.data[bp + 1];
                next2next = 0;
            } else {
                next = this.data[bp + 1];
                next2next = this.data[bp + 2];
            }
        } else if (this.fillOrder == 2) {
            b = flipTable[this.data[bp] & 255];
            if (bp == l) {
                next = 0;
                next2next = 0;
            } else if (bp + 1 == l) {
                next = flipTable[this.data[bp + 1] & 255];
                next2next = 0;
            } else {
                next = flipTable[this.data[bp + 1] & 255];
                next2next = flipTable[this.data[bp + 2] & 255];
            }
        } else {
            throw new RuntimeException("TIFF_FILL_ORDER tag must be either 1 or 2.");
        }
        int bitsLeft = 8 - this.bitPointer;
        int bitsFromNextByte = bitsToGet - bitsLeft;
        int bitsFromNext2NextByte = 0;
        if (bitsFromNextByte > 8) {
            bitsFromNext2NextByte = bitsFromNextByte - 8;
            bitsFromNextByte = 8;
        }
        ++this.bytePointer;
        int i1 = (b & table1[bitsLeft]) << bitsToGet - bitsLeft;
        int i2 = (next & table2[bitsFromNextByte]) >>> 8 - bitsFromNextByte;
        int i3 = 0;
        if (bitsFromNext2NextByte != 0) {
            i2<<=bitsFromNext2NextByte;
            i3 = (next2next & table2[bitsFromNext2NextByte]) >>> 8 - bitsFromNext2NextByte;
            i2|=i3;
            ++this.bytePointer;
            this.bitPointer = bitsFromNext2NextByte;
        } else if (bitsFromNextByte == 8) {
            this.bitPointer = 0;
            ++this.bytePointer;
        } else {
            this.bitPointer = bitsFromNextByte;
        }
        int i = i1 | i2;
        return i;
    }

    private int nextLesserThan8Bits(int bitsToGet) {
        int i1;
        byte next;
        byte b;
        int l = this.data.length - 1;
        int bp = this.bytePointer;
        if (this.fillOrder == 1) {
            b = this.data[bp];
            next = bp == l ? 0 : this.data[bp + 1];
        } else if (this.fillOrder == 2) {
            b = flipTable[this.data[bp] & 255];
            next = bp == l ? 0 : flipTable[this.data[bp + 1] & 255];
        } else {
            throw new RuntimeException("TIFF_FILL_ORDER tag must be either 1 or 2.");
        }
        int bitsLeft = 8 - this.bitPointer;
        int bitsFromNextByte = bitsToGet - bitsLeft;
        int shift = bitsLeft - bitsToGet;
        if (shift >= 0) {
            i1 = (b & table1[bitsLeft]) >>> shift;
            this.bitPointer+=bitsToGet;
            if (this.bitPointer == 8) {
                this.bitPointer = 0;
                ++this.bytePointer;
            }
        } else {
            i1 = (b & table1[bitsLeft]) << - shift;
            int i2 = (next & table2[bitsFromNextByte]) >>> 8 - bitsFromNextByte;
            i1|=i2;
            ++this.bytePointer;
            this.bitPointer = bitsFromNextByte;
        }
        return i1;
    }

    private void updatePointer(int bitsToMoveBack) {
        int i = this.bitPointer - bitsToMoveBack;
        if (i < 0) {
            --this.bytePointer;
            this.bitPointer = 8 + i;
        } else {
            this.bitPointer = i;
        }
    }

    private boolean advancePointer() {
        if (this.bitPointer != 0) {
            ++this.bytePointer;
            this.bitPointer = 0;
        }
        return true;
    }
}

