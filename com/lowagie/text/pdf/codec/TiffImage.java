/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.CCITTG4Encoder;
import com.lowagie.text.pdf.codec.TIFFDirectory;
import com.lowagie.text.pdf.codec.TIFFFaxDecoder;
import com.lowagie.text.pdf.codec.TIFFField;
import com.lowagie.text.pdf.codec.TIFFLZWDecoder;
import java.awt.color.ICC_Profile;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.zip.DataFormatException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;

public class TiffImage {
    public static int getNumberOfPages(RandomAccessFileOrArray s) {
        try {
            return TIFFDirectory.getNumDirectories(s);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    static int getDpi(TIFFField fd, int resolutionUnit) {
        if (fd == null) {
            return 0;
        }
        long[] res = fd.getAsRational(0);
        float frac = (float)res[0] / (float)res[1];
        int dpi = 0;
        switch (resolutionUnit) {
            case 1: 
            case 2: {
                dpi = (int)frac;
                break;
            }
            case 3: {
                dpi = (int)((double)frac * 2.54);
            }
        }
        return dpi;
    }

    public static Image getTiffImage(RandomAccessFileOrArray s, int page) {
        return TiffImage.getTiffImage(s, page, false);
    }

    public static Image getTiffImage(RandomAccessFileOrArray s, int page, boolean direct) {
        if (page < 1) {
            throw new IllegalArgumentException("The page number must be >= 1.");
        }
        try {
            long photo;
            TIFFDirectory dir = new TIFFDirectory(s, page - 1);
            if (dir.isTagPresent(322)) {
                throw new IllegalArgumentException("Tiles are not supported.");
            }
            int compression = (int)dir.getFieldAsLong(259);
            switch (compression) {
                case 2: 
                case 3: 
                case 4: 
                case 32771: {
                    break;
                }
                default: {
                    return TiffImage.getTiffImageColor(dir, s);
                }
            }
            Image img = null;
            long tiffT4Options = 0;
            long tiffT6Options = 0;
            int fillOrder = 1;
            int h = (int)dir.getFieldAsLong(257);
            int w = (int)dir.getFieldAsLong(256);
            int dpiX = 0;
            int dpiY = 0;
            float XYRatio = 0.0f;
            int resolutionUnit = 2;
            if (dir.isTagPresent(296)) {
                resolutionUnit = (int)dir.getFieldAsLong(296);
            }
            dpiX = TiffImage.getDpi(dir.getField(282), resolutionUnit);
            dpiY = TiffImage.getDpi(dir.getField(283), resolutionUnit);
            if (resolutionUnit == 1) {
                if (dpiY != 0) {
                    XYRatio = (float)dpiX / (float)dpiY;
                }
                dpiX = 0;
                dpiY = 0;
            }
            long tstrip = 0xFFFFFFFFL;
            if (dir.isTagPresent(278)) {
                tstrip = dir.getFieldAsLong(278);
            }
            int rowsStrip = (int)Math.min((long)h, tstrip);
            TIFFField field = dir.getField(273);
            long[] offset = TiffImage.getArrayLongShort(dir, 273);
            long[] size = TiffImage.getArrayLongShort(dir, 279);
            boolean reverse = false;
            TIFFField fillOrderField = dir.getField(266);
            if (fillOrderField != null) {
                fillOrder = fillOrderField.getAsInt(0);
            }
            reverse = fillOrder == 2;
            int params = 0;
            if (dir.isTagPresent(262) && (photo = dir.getFieldAsLong(262)) == 1) {
                params|=1;
            }
            int imagecomp = 0;
            switch (compression) {
                case 2: 
                case 32771: {
                    imagecomp = 257;
                    params|=10;
                    break;
                }
                case 3: {
                    imagecomp = 257;
                    params|=12;
                    TIFFField t4OptionsField = dir.getField(292);
                    if (t4OptionsField == null) break;
                    tiffT4Options = t4OptionsField.getAsLong(0);
                    if ((tiffT4Options & 1) != 0) {
                        compression = 258;
                    }
                    if ((tiffT4Options & 4) == 0) break;
                    params|=2;
                    break;
                }
                case 4: {
                    imagecomp = 256;
                    TIFFField t6OptionsField = dir.getField(293);
                    if (t6OptionsField == null) break;
                    tiffT6Options = t6OptionsField.getAsLong(0);
                }
            }
            if (direct && rowsStrip == h) {
                byte[] im = new byte[(int)size[0]];
                s.seek(offset[0]);
                s.readFully(im);
                img = Image.getInstance(w, h, reverse, imagecomp, params, im);
                img.setInverted(true);
            } else {
                int rowsLeft = h;
                CCITTG4Encoder g4 = new CCITTG4Encoder(w);
                for (int k = 0; k < offset.length; ++k) {
                    byte[] im = new byte[(int)size[k]];
                    s.seek(offset[k]);
                    s.readFully(im);
                    int height = Math.min(rowsStrip, rowsLeft);
                    TIFFFaxDecoder decoder = new TIFFFaxDecoder(fillOrder, w, height);
                    byte[] outBuf = new byte[(w + 7) / 8 * height];
                    switch (compression) {
                        case 2: 
                        case 32771: {
                            decoder.decode1D(outBuf, im, 0, height);
                            g4.encodeT6Lines(outBuf, 0, height);
                            break;
                        }
                        case 3: {
                            try {
                                decoder.decode2D(outBuf, im, 0, height, tiffT4Options);
                            }
                            catch (Exception e) {
                                tiffT4Options^=4;
                                try {
                                    decoder.decode2D(outBuf, im, 0, height, tiffT4Options);
                                }
                                catch (Exception e2) {
                                    throw e;
                                }
                            }
                            g4.encodeT6Lines(outBuf, 0, height);
                            break;
                        }
                        case 4: {
                            decoder.decodeT6(outBuf, im, 0, height, tiffT6Options);
                            g4.encodeT6Lines(outBuf, 0, height);
                        }
                    }
                    rowsLeft-=rowsStrip;
                }
                byte[] g4pic = g4.close();
                img = Image.getInstance(w, h, false, 256, params & 1, g4pic);
            }
            img.setDpi(dpiX, dpiY);
            img.setXYRatio(XYRatio);
            if (dir.isTagPresent(34675)) {
                try {
                    TIFFField fd = dir.getField(34675);
                    ICC_Profile icc_prof = ICC_Profile.getInstance(fd.getAsBytes());
                    if (icc_prof.getNumComponents() == 1) {
                        img.tagICC(icc_prof);
                    }
                }
                catch (Exception fd) {
                    // empty catch block
                }
            }
            img.setOriginalType(5);
            return img;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    protected static Image getTiffImageColor(TIFFDirectory dir, RandomAccessFileOrArray s) {
        try {
            int compression = (int)dir.getFieldAsLong(259);
            int predictor = 1;
            TIFFLZWDecoder lzwDecoder = null;
            switch (compression) {
                case 1: 
                case 5: 
                case 32773: 
                case 32946: {
                    break;
                }
                default: {
                    throw new IllegalArgumentException("The compression " + compression + " is not supported.");
                }
            }
            int photometric = (int)dir.getFieldAsLong(262);
            switch (photometric) {
                case 0: 
                case 1: 
                case 2: 
                case 3: 
                case 5: {
                    break;
                }
                default: {
                    throw new IllegalArgumentException("The photometric " + photometric + " is not supported.");
                }
            }
            if (dir.isTagPresent(284) && dir.getFieldAsLong(284) == 2) {
                throw new IllegalArgumentException("Planar images are not supported.");
            }
            if (dir.isTagPresent(338)) {
                throw new IllegalArgumentException("Extra samples are not supported.");
            }
            int samplePerPixel = 1;
            if (dir.isTagPresent(277)) {
                samplePerPixel = (int)dir.getFieldAsLong(277);
            }
            int bitsPerSample = 1;
            if (dir.isTagPresent(258)) {
                bitsPerSample = (int)dir.getFieldAsLong(258);
            }
            switch (bitsPerSample) {
                case 1: 
                case 2: 
                case 4: 
                case 8: {
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Bits per sample " + bitsPerSample + " is not supported.");
                }
            }
            Image img = null;
            int h = (int)dir.getFieldAsLong(257);
            int w = (int)dir.getFieldAsLong(256);
            int dpiX = 0;
            int dpiY = 0;
            int resolutionUnit = 2;
            if (dir.isTagPresent(296)) {
                resolutionUnit = (int)dir.getFieldAsLong(296);
            }
            dpiX = TiffImage.getDpi(dir.getField(282), resolutionUnit);
            dpiY = TiffImage.getDpi(dir.getField(283), resolutionUnit);
            int rowsStrip = (int)dir.getFieldAsLong(278);
            long[] offset = TiffImage.getArrayLongShort(dir, 273);
            long[] size = TiffImage.getArrayLongShort(dir, 279);
            if (compression == 5) {
                TIFFField predictorField = dir.getField(317);
                if (predictorField != null) {
                    predictor = predictorField.getAsInt(0);
                    if (predictor != 1 && predictor != 2) {
                        throw new RuntimeException("Illegal value for Predictor in TIFF file.");
                    }
                    if (predictor == 2 && bitsPerSample != 8) {
                        throw new RuntimeException(String.valueOf(bitsPerSample) + "-bit samples are not supported for Horizontal differencing Predictor.");
                    }
                }
                lzwDecoder = new TIFFLZWDecoder(w, predictor, samplePerPixel);
            }
            int rowsLeft = h;
            ByteArrayOutputStream stream = null;
            DeflaterOutputStream zip = null;
            CCITTG4Encoder g4 = null;
            if (bitsPerSample == 1 && samplePerPixel == 1) {
                g4 = new CCITTG4Encoder(w);
            } else {
                stream = new ByteArrayOutputStream();
                zip = new DeflaterOutputStream(stream);
            }
            for (int k = 0; k < offset.length; ++k) {
                byte[] im = new byte[(int)size[k]];
                s.seek(offset[k]);
                s.readFully(im);
                int height = Math.min(rowsStrip, rowsLeft);
                byte[] outBuf = null;
                if (compression != 1) {
                    outBuf = new byte[(w * bitsPerSample * samplePerPixel + 7) / 8 * height];
                }
                switch (compression) {
                    case 32946: {
                        TiffImage.inflate(im, outBuf);
                        break;
                    }
                    case 1: {
                        outBuf = im;
                        break;
                    }
                    case 32773: {
                        TiffImage.decodePackbits(im, outBuf);
                        break;
                    }
                    case 5: {
                        lzwDecoder.decode(im, outBuf, height);
                    }
                }
                if (bitsPerSample == 1 && samplePerPixel == 1) {
                    g4.encodeT6Lines(outBuf, 0, height);
                } else {
                    zip.write(outBuf);
                }
                rowsLeft-=rowsStrip;
            }
            if (bitsPerSample == 1 && samplePerPixel == 1) {
                img = Image.getInstance(w, h, false, 256, photometric == 1 ? 1 : 0, g4.close());
            } else {
                zip.close();
                img = Image.getInstance(w, h, samplePerPixel, bitsPerSample, stream.toByteArray());
                img.setDeflated(true);
            }
            img.setDpi(dpiX, dpiY);
            if (dir.isTagPresent(34675)) {
                try {
                    TIFFField fd = dir.getField(34675);
                    ICC_Profile icc_prof = ICC_Profile.getInstance(fd.getAsBytes());
                    if (samplePerPixel == icc_prof.getNumComponents()) {
                        img.tagICC(icc_prof);
                    }
                }
                catch (Exception fd) {
                    // empty catch block
                }
            }
            if (dir.isTagPresent(320)) {
                TIFFField fd = dir.getField(320);
                char[] rgb = fd.getAsChars();
                byte[] palette = new byte[rgb.length];
                int gColor = rgb.length / 3;
                int bColor = gColor * 2;
                for (int k2 = 0; k2 < gColor; ++k2) {
                    palette[k2 * 3] = (byte)(rgb[k2] >>> 8);
                    palette[k2 * 3 + 1] = (byte)(rgb[k2 + gColor] >>> 8);
                    palette[k2 * 3 + 2] = (byte)(rgb[k2 + bColor] >>> 8);
                }
                PdfArray indexed = new PdfArray();
                indexed.add(PdfName.INDEXED);
                indexed.add(PdfName.DEVICERGB);
                indexed.add(new PdfNumber(gColor - 1));
                indexed.add(new PdfString(palette));
                PdfDictionary additional = new PdfDictionary();
                additional.put(PdfName.COLORSPACE, indexed);
                img.setAdditional(additional);
            }
            if (photometric == 0) {
                img.setInverted(true);
            }
            img.setOriginalType(5);
            return img;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    static long[] getArrayLongShort(TIFFDirectory dir, int tag) {
        long[] offset;
        TIFFField field = dir.getField(tag);
        if (field == null) {
            return null;
        }
        if (field.getType() == 4) {
            offset = field.getAsLongs();
        } else {
            char[] temp = field.getAsChars();
            offset = new long[temp.length];
            for (int k = 0; k < temp.length; ++k) {
                offset[k] = temp[k];
            }
        }
        return offset;
    }

    public static void decodePackbits(byte[] data, byte[] dst) {
        int srcCount = 0;
        int dstCount = 0;
        while (dstCount < dst.length) {
            int i;
            byte b;
            if ((b = data[srcCount++]) >= 0 && b <= 127) {
                for (i = 0; i < b + 1; ++i) {
                    dst[dstCount++] = data[srcCount++];
                }
                continue;
            }
            if (b <= -1 && b >= -127) {
                byte repeat = data[srcCount++];
                for (i = 0; i < - b + 1; ++i) {
                    dst[dstCount++] = repeat;
                }
                continue;
            }
            ++srcCount;
        }
    }

    public static void inflate(byte[] deflated, byte[] inflated) {
        Inflater inflater = new Inflater();
        inflater.setInput(deflated);
        try {
            inflater.inflate(inflated);
        }
        catch (DataFormatException dfe) {
            throw new ExceptionConverter(dfe);
        }
    }
}

