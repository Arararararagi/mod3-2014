/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.ImgRaw;
import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfLiteral;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfString;
import java.awt.color.ICC_Profile;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class PngImage {
    public static final int[] PNGID = new int[]{137, 80, 78, 71, 13, 10, 26, 10};
    public static final String IHDR = "IHDR";
    public static final String PLTE = "PLTE";
    public static final String IDAT = "IDAT";
    public static final String IEND = "IEND";
    public static final String tRNS = "tRNS";
    public static final String pHYs = "pHYs";
    public static final String gAMA = "gAMA";
    public static final String cHRM = "cHRM";
    public static final String sRGB = "sRGB";
    public static final String iCCP = "iCCP";
    private static final int TRANSFERSIZE = 4096;
    private static final int PNG_FILTER_NONE = 0;
    private static final int PNG_FILTER_SUB = 1;
    private static final int PNG_FILTER_UP = 2;
    private static final int PNG_FILTER_AVERAGE = 3;
    private static final int PNG_FILTER_PAETH = 4;
    private static final PdfName[] intents = new PdfName[]{PdfName.PERCEPTUAL, PdfName.RELATIVECALORIMETRIC, PdfName.SATURATION, PdfName.ABSOLUTECALORIMETRIC};
    InputStream is;
    DataInputStream dataStream;
    int width;
    int height;
    int bitDepth;
    int colorType;
    int compressionMethod;
    int filterMethod;
    int interlaceMethod;
    PdfDictionary additional = new PdfDictionary();
    byte[] image;
    byte[] smask;
    byte[] trans;
    NewByteArrayOutputStream idat = new NewByteArrayOutputStream();
    int dpiX;
    int dpiY;
    float XYRatio;
    boolean genBWMask;
    boolean palShades;
    int transRedGray = -1;
    int transGreen = -1;
    int transBlue = -1;
    int inputBands;
    int bytesPerPixel;
    byte[] colorTable;
    float gamma = 1.0f;
    boolean hasCHRM = false;
    float xW;
    float yW;
    float xR;
    float yR;
    float xG;
    float yG;
    float xB;
    float yB;
    PdfName intent;
    ICC_Profile icc_profile;

    PngImage(InputStream is) {
        this.is = is;
    }

    public static Image getImage(URL url) throws IOException {
        Image image;
        block3 : {
            InputStream is = null;
            try {
                is = url.openStream();
                Image img = PngImage.getImage(is);
                img.setUrl(url);
                image = img;
                Object var3_4 = null;
                if (is == null) break block3;
            }
            catch (Throwable var4_6) {
                Object var3_5 = null;
                if (is != null) {
                    is.close();
                }
                throw var4_6;
            }
            is.close();
        }
        return image;
    }

    public static Image getImage(InputStream is) throws IOException {
        PngImage png = new PngImage(is);
        return png.getImage();
    }

    public static Image getImage(String file) throws IOException {
        return PngImage.getImage(Image.toURL(file));
    }

    public static Image getImage(byte[] data) throws IOException {
        Image image;
        block3 : {
            ByteArrayInputStream is = null;
            try {
                is = new ByteArrayInputStream(data);
                Image img = PngImage.getImage(is);
                img.setOriginalData(data);
                image = img;
                Object var3_4 = null;
                if (is == null) break block3;
            }
            catch (Throwable var4_6) {
                Object var3_5 = null;
                if (is != null) {
                    is.close();
                }
                throw var4_6;
            }
            is.close();
        }
        return image;
    }

    boolean checkMarker(String s) {
        if (s.length() != 4) {
            return false;
        }
        for (int k = 0; k < 4; ++k) {
            char c = s.charAt(k);
            if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z') continue;
            return false;
        }
        return true;
    }

    void readPng() throws IOException {
        for (int i = 0; i < PNGID.length; ++i) {
            if (PNGID[i] == this.is.read()) continue;
            throw new IOException("File is not a valid PNG.");
        }
        byte[] buffer = new byte[4096];
        do {
            int len;
            String marker = PngImage.getString(this.is);
            if (!(len >= 0 && this.checkMarker(marker))) {
                throw new IOException("Corrupted PNG file.");
            }
            if ("IDAT".equals(marker)) {
                int size;
                for (len = PngImage.getInt((InputStream)this.is); len != 0; len-=size) {
                    size = this.is.read(buffer, 0, Math.min(len, 4096));
                    if (size < 0) {
                        return;
                    }
                    this.idat.write(buffer, 0, size);
                }
            } else if ("tRNS".equals(marker)) {
                switch (this.colorType) {
                    case 0: {
                        if (len < 2) break;
                        len-=2;
                        int gray = PngImage.getWord(this.is);
                        if (this.bitDepth == 16) {
                            this.transRedGray = gray;
                            break;
                        }
                        this.additional.put(PdfName.MASK, new PdfLiteral("[" + gray + " " + gray + "]"));
                        break;
                    }
                    case 2: {
                        if (len < 6) break;
                        len-=6;
                        int red = PngImage.getWord(this.is);
                        int green = PngImage.getWord(this.is);
                        int blue = PngImage.getWord(this.is);
                        if (this.bitDepth == 16) {
                            this.transRedGray = red;
                            this.transGreen = green;
                            this.transBlue = blue;
                            break;
                        }
                        this.additional.put(PdfName.MASK, new PdfLiteral("[" + red + " " + red + " " + green + " " + green + " " + blue + " " + blue + "]"));
                        break;
                    }
                    case 3: {
                        if (len <= 0) break;
                        this.trans = new byte[len];
                        for (int k = 0; k < len; ++k) {
                            this.trans[k] = (byte)this.is.read();
                        }
                        len = 0;
                    }
                }
                Image.skip(this.is, len);
            } else if ("IHDR".equals(marker)) {
                this.width = PngImage.getInt(this.is);
                this.height = PngImage.getInt(this.is);
                this.bitDepth = this.is.read();
                this.colorType = this.is.read();
                this.compressionMethod = this.is.read();
                this.filterMethod = this.is.read();
                this.interlaceMethod = this.is.read();
            } else if ("PLTE".equals(marker)) {
                if (this.colorType == 3) {
                    PdfArray colorspace = new PdfArray();
                    colorspace.add(PdfName.INDEXED);
                    colorspace.add(this.getColorspace());
                    colorspace.add(new PdfNumber(len / 3 - 1));
                    ByteBuffer colortable = new ByteBuffer();
                    while (len-- > 0) {
                        colortable.append_i(this.is.read());
                    }
                    this.colorTable = colortable.toByteArray();
                    colorspace.add(new PdfString(this.colorTable));
                    this.additional.put(PdfName.COLORSPACE, colorspace);
                } else {
                    Image.skip(this.is, len);
                }
            } else if ("pHYs".equals(marker)) {
                int dx = PngImage.getInt(this.is);
                int dy = PngImage.getInt(this.is);
                int unit = this.is.read();
                if (unit == 1) {
                    this.dpiX = (int)((float)dx * 0.0254f);
                    this.dpiY = (int)((float)dy * 0.0254f);
                } else if (dy != 0) {
                    this.XYRatio = (float)dx / (float)dy;
                }
            } else if ("cHRM".equals(marker)) {
                this.xW = (float)PngImage.getInt(this.is) / 100000.0f;
                this.yW = (float)PngImage.getInt(this.is) / 100000.0f;
                this.xR = (float)PngImage.getInt(this.is) / 100000.0f;
                this.yR = (float)PngImage.getInt(this.is) / 100000.0f;
                this.xG = (float)PngImage.getInt(this.is) / 100000.0f;
                this.yG = (float)PngImage.getInt(this.is) / 100000.0f;
                this.xB = (float)PngImage.getInt(this.is) / 100000.0f;
                this.yB = (float)PngImage.getInt(this.is) / 100000.0f;
                this.hasCHRM = Math.abs(this.xW) >= 1.0E-4f && Math.abs(this.yW) >= 1.0E-4f && Math.abs(this.xR) >= 1.0E-4f && Math.abs(this.yR) >= 1.0E-4f && Math.abs(this.xG) >= 1.0E-4f && Math.abs(this.yG) >= 1.0E-4f && Math.abs(this.xB) >= 1.0E-4f && Math.abs(this.yB) >= 1.0E-4f;
            } else if ("sRGB".equals(marker)) {
                int ri = this.is.read();
                this.intent = intents[ri];
                this.gamma = 2.2f;
                this.xW = 0.3127f;
                this.yW = 0.329f;
                this.xR = 0.64f;
                this.yR = 0.33f;
                this.xG = 0.3f;
                this.yG = 0.6f;
                this.xB = 0.15f;
                this.yB = 0.06f;
                this.hasCHRM = true;
            } else if ("gAMA".equals(marker)) {
                int gm = PngImage.getInt(this.is);
                if (gm != 0) {
                    this.gamma = 100000.0f / (float)gm;
                    if (!this.hasCHRM) {
                        this.xW = 0.3127f;
                        this.yW = 0.329f;
                        this.xR = 0.64f;
                        this.yR = 0.33f;
                        this.xG = 0.3f;
                        this.yG = 0.6f;
                        this.xB = 0.15f;
                        this.yB = 0.06f;
                        this.hasCHRM = true;
                    }
                }
            } else if ("iCCP".equals(marker)) {
                do {
                    --len;
                } while (this.is.read() != 0);
                this.is.read();
                byte[] icccom = new byte[--len];
                int p = 0;
                while (len > 0) {
                    int r = this.is.read(icccom, p, len);
                    if (r < 0) {
                        throw new IOException("Premature end of file.");
                    }
                    p+=r;
                    len-=r;
                }
                byte[] iccp = PdfReader.FlateDecode(icccom, true);
                icccom = null;
                try {
                    this.icc_profile = ICC_Profile.getInstance(iccp);
                }
                catch (Exception e) {
                    this.icc_profile = null;
                }
            } else {
                if ("IEND".equals(marker)) break;
                Image.skip(this.is, len);
            }
            Image.skip(this.is, 4);
        } while (true);
    }

    PdfObject getColorspace() {
        if (this.icc_profile != null) {
            if ((this.colorType & 2) == 0) {
                return PdfName.DEVICEGRAY;
            }
            return PdfName.DEVICERGB;
        }
        if (!(this.gamma != 1.0f || this.hasCHRM)) {
            if ((this.colorType & 2) == 0) {
                return PdfName.DEVICEGRAY;
            }
            return PdfName.DEVICERGB;
        }
        PdfArray array = new PdfArray();
        PdfDictionary dic = new PdfDictionary();
        if ((this.colorType & 2) == 0) {
            if (this.gamma == 1.0f) {
                return PdfName.DEVICEGRAY;
            }
            array.add(PdfName.CALGRAY);
            dic.put(PdfName.GAMMA, new PdfNumber(this.gamma));
            dic.put(PdfName.WHITEPOINT, new PdfLiteral("[1 1 1]"));
            array.add(dic);
        } else {
            PdfObject wp = new PdfLiteral("[1 1 1]");
            array.add(PdfName.CALRGB);
            if (this.gamma != 1.0f) {
                PdfArray gm = new PdfArray();
                PdfNumber n = new PdfNumber(this.gamma);
                gm.add(n);
                gm.add(n);
                gm.add(n);
                dic.put(PdfName.GAMMA, gm);
            }
            if (this.hasCHRM) {
                float z = this.yW * ((this.xG - this.xB) * this.yR - (this.xR - this.xB) * this.yG + (this.xR - this.xG) * this.yB);
                float YA = this.yR * ((this.xG - this.xB) * this.yW - (this.xW - this.xB) * this.yG + (this.xW - this.xG) * this.yB) / z;
                float XA = YA * this.xR / this.yR;
                float ZA = YA * ((1.0f - this.xR) / this.yR - 1.0f);
                float YB = (- this.yG) * ((this.xR - this.xB) * this.yW - (this.xW - this.xB) * this.yR + (this.xW - this.xR) * this.yB) / z;
                float XB = YB * this.xG / this.yG;
                float ZB = YB * ((1.0f - this.xG) / this.yG - 1.0f);
                float YC = this.yB * ((this.xR - this.xG) * this.yW - (this.xW - this.xG) * this.yW + (this.xW - this.xR) * this.yG) / z;
                float XC = YC * this.xB / this.yB;
                float ZC = YC * ((1.0f - this.xB) / this.yB - 1.0f);
                float XW = XA + XB + XC;
                float YW = 1.0f;
                float ZW = ZA + ZB + ZC;
                PdfArray wpa = new PdfArray();
                wpa.add(new PdfNumber(XW));
                wpa.add(new PdfNumber(YW));
                wpa.add(new PdfNumber(ZW));
                wp = wpa;
                PdfArray matrix = new PdfArray();
                matrix.add(new PdfNumber(XA));
                matrix.add(new PdfNumber(YA));
                matrix.add(new PdfNumber(ZA));
                matrix.add(new PdfNumber(XB));
                matrix.add(new PdfNumber(YB));
                matrix.add(new PdfNumber(ZB));
                matrix.add(new PdfNumber(XC));
                matrix.add(new PdfNumber(YC));
                matrix.add(new PdfNumber(ZC));
                dic.put(PdfName.MATRIX, matrix);
            }
            dic.put(PdfName.WHITEPOINT, wp);
            array.add(dic);
        }
        return array;
    }

    Image getImage() throws IOException {
        this.readPng();
        try {
            Image im2;
            Image img;
            int bpc;
            int pal0 = 0;
            int palIdx = 0;
            this.palShades = false;
            if (this.trans != null) {
                for (int k = 0; k < this.trans.length; ++k) {
                    int n = this.trans[k] & 255;
                    if (n == 0) {
                        ++pal0;
                        palIdx = k;
                    }
                    if (n == 0 || n == 255) continue;
                    this.palShades = true;
                    break;
                }
            }
            if ((this.colorType & 4) != 0) {
                this.palShades = true;
            }
            boolean bl = this.genBWMask = !this.palShades && (pal0 > 1 || this.transRedGray >= 0);
            if (!(this.palShades || this.genBWMask || pal0 != 1)) {
                this.additional.put(PdfName.MASK, new PdfLiteral("[" + palIdx + " " + palIdx + "]"));
            }
            boolean needDecode = this.interlaceMethod == 1 || this.bitDepth == 16 || (this.colorType & 4) != 0 || this.palShades || this.genBWMask;
            switch (this.colorType) {
                case 0: {
                    this.inputBands = 1;
                    break;
                }
                case 2: {
                    this.inputBands = 3;
                    break;
                }
                case 3: {
                    this.inputBands = 1;
                    break;
                }
                case 4: {
                    this.inputBands = 2;
                    break;
                }
                case 6: {
                    this.inputBands = 4;
                }
            }
            if (needDecode) {
                this.decodeIdat();
            }
            int components = this.inputBands;
            if ((this.colorType & 4) != 0) {
                --components;
            }
            if ((bpc = this.bitDepth) == 16) {
                bpc = 8;
            }
            if (this.image != null) {
                img = Image.getInstance(this.width, this.height, components, bpc, this.image);
            } else {
                img = new ImgRaw(this.width, this.height, components, bpc, this.idat.toByteArray());
                img.setDeflated(true);
                PdfDictionary decodeparms = new PdfDictionary();
                decodeparms.put(PdfName.BITSPERCOMPONENT, new PdfNumber(this.bitDepth));
                decodeparms.put(PdfName.PREDICTOR, new PdfNumber(15));
                decodeparms.put(PdfName.COLUMNS, new PdfNumber(this.width));
                decodeparms.put(PdfName.COLORS, new PdfNumber(this.colorType == 3 || (this.colorType & 2) == 0 ? 1 : 3));
                this.additional.put(PdfName.DECODEPARMS, decodeparms);
            }
            if (this.additional.get(PdfName.COLORSPACE) == null) {
                this.additional.put(PdfName.COLORSPACE, this.getColorspace());
            }
            if (this.intent != null) {
                this.additional.put(PdfName.INTENT, this.intent);
            }
            if (this.additional.size() > 0) {
                img.setAdditional(this.additional);
            }
            if (this.icc_profile != null) {
                img.tagICC(this.icc_profile);
            }
            if (this.palShades) {
                im2 = Image.getInstance(this.width, this.height, 1, 8, this.smask);
                im2.makeMask();
                img.setImageMask(im2);
            }
            if (this.genBWMask) {
                im2 = Image.getInstance(this.width, this.height, 1, 1, this.smask);
                im2.makeMask();
                img.setImageMask(im2);
            }
            img.setDpi(this.dpiX, this.dpiY);
            img.setXYRatio(this.XYRatio);
            img.setOriginalType(2);
            return img;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    void decodeIdat() {
        int nbitDepth = this.bitDepth;
        if (nbitDepth == 16) {
            nbitDepth = 8;
        }
        int size = -1;
        this.bytesPerPixel = this.bitDepth == 16 ? 2 : 1;
        switch (this.colorType) {
            case 0: {
                size = (nbitDepth * this.width + 7) / 8 * this.height;
                break;
            }
            case 2: {
                size = this.width * 3 * this.height;
                this.bytesPerPixel*=3;
                break;
            }
            case 3: {
                if (this.interlaceMethod == 1) {
                    size = (nbitDepth * this.width + 7) / 8 * this.height;
                }
                this.bytesPerPixel = 1;
                break;
            }
            case 4: {
                size = this.width * this.height;
                this.bytesPerPixel*=2;
                break;
            }
            case 6: {
                size = this.width * 3 * this.height;
                this.bytesPerPixel*=4;
            }
        }
        if (size >= 0) {
            this.image = new byte[size];
        }
        if (this.palShades) {
            this.smask = new byte[this.width * this.height];
        } else if (this.genBWMask) {
            this.smask = new byte[(this.width + 7) / 8 * this.height];
        }
        ByteArrayInputStream bai = new ByteArrayInputStream(this.idat.getBuf(), 0, this.idat.size());
        InflaterInputStream infStream = new InflaterInputStream(bai, new Inflater());
        this.dataStream = new DataInputStream(infStream);
        if (this.interlaceMethod != 1) {
            this.decodePass(0, 0, 1, 1, this.width, this.height);
        } else {
            this.decodePass(0, 0, 8, 8, (this.width + 7) / 8, (this.height + 7) / 8);
            this.decodePass(4, 0, 8, 8, (this.width + 3) / 8, (this.height + 7) / 8);
            this.decodePass(0, 4, 4, 8, (this.width + 3) / 4, (this.height + 3) / 8);
            this.decodePass(2, 0, 4, 4, (this.width + 1) / 4, (this.height + 3) / 4);
            this.decodePass(0, 2, 2, 4, (this.width + 1) / 2, (this.height + 1) / 4);
            this.decodePass(1, 0, 2, 2, this.width / 2, (this.height + 1) / 2);
            this.decodePass(0, 1, 1, 2, this.width, this.height / 2);
        }
    }

    void decodePass(int xOffset, int yOffset, int xStep, int yStep, int passWidth, int passHeight) {
        if (passWidth == 0 || passHeight == 0) {
            return;
        }
        int bytesPerRow = (this.inputBands * passWidth * this.bitDepth + 7) / 8;
        byte[] curr = new byte[bytesPerRow];
        byte[] prior = new byte[bytesPerRow];
        int srcY = 0;
        int dstY = yOffset;
        while (srcY < passHeight) {
            int filter = 0;
            try {
                filter = this.dataStream.read();
                this.dataStream.readFully(curr, 0, bytesPerRow);
            }
            catch (Exception var13_14) {
                // empty catch block
            }
            switch (filter) {
                case 0: {
                    break;
                }
                case 1: {
                    PngImage.decodeSubFilter(curr, bytesPerRow, this.bytesPerPixel);
                    break;
                }
                case 2: {
                    PngImage.decodeUpFilter(curr, prior, bytesPerRow);
                    break;
                }
                case 3: {
                    PngImage.decodeAverageFilter(curr, prior, bytesPerRow, this.bytesPerPixel);
                    break;
                }
                case 4: {
                    PngImage.decodePaethFilter(curr, prior, bytesPerRow, this.bytesPerPixel);
                    break;
                }
                default: {
                    throw new RuntimeException("PNG filter unknown.");
                }
            }
            this.processPixels(curr, xOffset, xStep, dstY, passWidth);
            byte[] tmp = prior;
            prior = curr;
            curr = tmp;
            ++srcY;
            dstY+=yStep;
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    void processPixels(byte[] curr, int xOffset, int step, int y, int width) {
        int yStride;
        int srcX;
        int dstX;
        int[] out = this.getPixel(curr);
        int sizes = 0;
        switch (this.colorType) {
            case 0: 
            case 3: 
            case 4: {
                sizes = 1;
                break;
            }
            case 2: 
            case 6: {
                sizes = 3;
            }
        }
        if (this.image != null) {
            dstX = xOffset;
            yStride = (sizes * this.width * (this.bitDepth == 16 ? 8 : this.bitDepth) + 7) / 8;
            for (srcX = 0; srcX < width; ++srcX) {
                PngImage.setPixel(this.image, out, this.inputBands * srcX, sizes, dstX, y, this.bitDepth, yStride);
                dstX+=step;
            }
        }
        if (this.palShades) {
            if ((this.colorType & 4) != 0) {
                if (this.bitDepth == 16) {
                    for (int k = 0; k < width; ++k) {
                        int[] arrn = out;
                        int n = k * this.inputBands + sizes;
                        arrn[n] = arrn[n] >>> 8;
                    }
                }
                yStride = this.width;
                dstX = xOffset;
                for (srcX = 0; srcX < width; ++srcX) {
                    PngImage.setPixel(this.smask, out, this.inputBands * srcX + sizes, 1, dstX, y, 8, yStride);
                    dstX+=step;
                }
                return;
            } else {
                yStride = this.width;
                int[] v = new int[1];
                dstX = xOffset;
                for (srcX = 0; srcX < width; ++srcX) {
                    int idx = out[srcX];
                    int r = 255;
                    if (idx < this.trans.length) {
                        v[0] = this.trans[idx];
                    }
                    PngImage.setPixel(this.smask, v, 0, 1, dstX, y, 8, yStride);
                    dstX+=step;
                }
            }
            return;
        }
        if (!this.genBWMask) return;
        switch (this.colorType) {
            case 3: {
                yStride = (this.width + 7) / 8;
                int[] v = new int[1];
                dstX = xOffset;
                for (srcX = 0; srcX < width; ++srcX) {
                    int idx = out[srcX];
                    boolean r = false;
                    if (idx < this.trans.length) {
                        v[0] = this.trans[idx] == 0 ? 1 : 0;
                    }
                    PngImage.setPixel(this.smask, v, 0, 1, dstX, y, 1, yStride);
                    dstX+=step;
                }
                return;
            }
            case 0: {
                yStride = (this.width + 7) / 8;
                int[] v = new int[1];
                dstX = xOffset;
                for (srcX = 0; srcX < width; ++srcX) {
                    int g = out[srcX];
                    v[0] = g == this.transRedGray ? 1 : 0;
                    PngImage.setPixel(this.smask, v, 0, 1, dstX, y, 1, yStride);
                    dstX+=step;
                }
                return;
            }
            case 2: {
                yStride = (this.width + 7) / 8;
                int[] v = new int[1];
                dstX = xOffset;
                for (srcX = 0; srcX < width; ++srcX) {
                    int markRed = this.inputBands * srcX;
                    v[0] = out[markRed] == this.transRedGray && out[markRed + 1] == this.transGreen && out[markRed + 2] == this.transBlue ? 1 : 0;
                    PngImage.setPixel(this.smask, v, 0, 1, dstX, y, 1, yStride);
                    dstX+=step;
                }
            }
        }
    }

    static int getPixel(byte[] image, int x, int y, int bitDepth, int bytesPerRow) {
        if (bitDepth == 8) {
            int pos = bytesPerRow * y + x;
            return image[pos] & 255;
        }
        int pos = bytesPerRow * y + x / (8 / bitDepth);
        int v = image[pos] >> 8 - bitDepth * (x % (8 / bitDepth)) - bitDepth;
        return v & (1 << bitDepth) - 1;
    }

    static void setPixel(byte[] image, int[] data, int offset, int size, int x, int y, int bitDepth, int bytesPerRow) {
        if (bitDepth == 8) {
            int pos = bytesPerRow * y + size * x;
            for (int k = 0; k < size; ++k) {
                image[pos + k] = (byte)data[k + offset];
            }
        } else if (bitDepth == 16) {
            int pos = bytesPerRow * y + size * x;
            for (int k = 0; k < size; ++k) {
                image[pos + k] = (byte)(data[k + offset] >>> 8);
            }
        } else {
            int pos = bytesPerRow * y + x / (8 / bitDepth);
            int v = data[offset] << 8 - bitDepth * (x % (8 / bitDepth)) - bitDepth;
            byte[] arrby = image;
            int n = pos;
            arrby[n] = (byte)(arrby[n] | v);
        }
    }

    int[] getPixel(byte[] curr) {
        switch (this.bitDepth) {
            case 8: {
                int[] out = new int[curr.length];
                for (int k = 0; k < out.length; ++k) {
                    out[k] = curr[k] & 255;
                }
                return out;
            }
            case 16: {
                int[] out = new int[curr.length / 2];
                for (int k = 0; k < out.length; ++k) {
                    out[k] = ((curr[k * 2] & 255) << 8) + (curr[k * 2 + 1] & 255);
                }
                return out;
            }
        }
        int[] out = new int[curr.length * 8 / this.bitDepth];
        int idx = 0;
        int passes = 8 / this.bitDepth;
        int mask = (1 << this.bitDepth) - 1;
        for (int k = 0; k < curr.length; ++k) {
            for (int j = passes - 1; j >= 0; --j) {
                out[idx++] = curr[k] >>> this.bitDepth * j & mask;
            }
        }
        return out;
    }

    private static void decodeSubFilter(byte[] curr, int count, int bpp) {
        for (int i = bpp; i < count; ++i) {
            int val = curr[i] & 255;
            curr[i] = (byte)(val+=curr[i - bpp] & 255);
        }
    }

    private static void decodeUpFilter(byte[] curr, byte[] prev, int count) {
        for (int i = 0; i < count; ++i) {
            int raw = curr[i] & 255;
            int prior = prev[i] & 255;
            curr[i] = (byte)(raw + prior);
        }
    }

    private static void decodeAverageFilter(byte[] curr, byte[] prev, int count, int bpp) {
        int raw;
        int i;
        int priorRow;
        for (i = 0; i < bpp; ++i) {
            raw = curr[i] & 255;
            priorRow = prev[i] & 255;
            curr[i] = (byte)(raw + priorRow / 2);
        }
        for (i = bpp; i < count; ++i) {
            raw = curr[i] & 255;
            int priorPixel = curr[i - bpp] & 255;
            priorRow = prev[i] & 255;
            curr[i] = (byte)(raw + (priorPixel + priorRow) / 2);
        }
    }

    private static int paethPredictor(int a, int b, int c) {
        int p = a + b - c;
        int pa = Math.abs(p - a);
        int pb = Math.abs(p - b);
        int pc = Math.abs(p - c);
        if (pa <= pb && pa <= pc) {
            return a;
        }
        if (pb <= pc) {
            return b;
        }
        return c;
    }

    private static void decodePaethFilter(byte[] curr, byte[] prev, int count, int bpp) {
        int i;
        int raw;
        int priorRow;
        for (i = 0; i < bpp; ++i) {
            raw = curr[i] & 255;
            priorRow = prev[i] & 255;
            curr[i] = (byte)(raw + priorRow);
        }
        for (i = bpp; i < count; ++i) {
            raw = curr[i] & 255;
            int priorPixel = curr[i - bpp] & 255;
            priorRow = prev[i] & 255;
            int priorRowPixel = prev[i - bpp] & 255;
            curr[i] = (byte)(raw + PngImage.paethPredictor(priorPixel, priorRow, priorRowPixel));
        }
    }

    public static final int getInt(InputStream is) throws IOException {
        return (is.read() << 24) + (is.read() << 16) + (is.read() << 8) + is.read();
    }

    public static final int getWord(InputStream is) throws IOException {
        return (is.read() << 8) + is.read();
    }

    public static final String getString(InputStream is) throws IOException {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < 4; ++i) {
            buf.append((char)is.read());
        }
        return buf.toString();
    }

    static class NewByteArrayOutputStream
    extends ByteArrayOutputStream {
        NewByteArrayOutputStream() {
        }

        public byte[] getBuf() {
            return this.buf;
        }
    }

}

