/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec;

import com.lowagie.text.BadElementException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.ImgRaw;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfString;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

public class BmpImage {
    private InputStream inputStream;
    private long bitmapFileSize;
    private long bitmapOffset;
    private long compression;
    private long imageSize;
    private byte[] palette;
    private int imageType;
    private int numBands;
    private boolean isBottomUp;
    private int bitsPerPixel;
    private int redMask;
    private int greenMask;
    private int blueMask;
    private int alphaMask;
    public HashMap properties = new HashMap();
    private long xPelsPerMeter;
    private long yPelsPerMeter;
    private static final int VERSION_2_1_BIT = 0;
    private static final int VERSION_2_4_BIT = 1;
    private static final int VERSION_2_8_BIT = 2;
    private static final int VERSION_2_24_BIT = 3;
    private static final int VERSION_3_1_BIT = 4;
    private static final int VERSION_3_4_BIT = 5;
    private static final int VERSION_3_8_BIT = 6;
    private static final int VERSION_3_24_BIT = 7;
    private static final int VERSION_3_NT_16_BIT = 8;
    private static final int VERSION_3_NT_32_BIT = 9;
    private static final int VERSION_4_1_BIT = 10;
    private static final int VERSION_4_4_BIT = 11;
    private static final int VERSION_4_8_BIT = 12;
    private static final int VERSION_4_16_BIT = 13;
    private static final int VERSION_4_24_BIT = 14;
    private static final int VERSION_4_32_BIT = 15;
    private static final int LCS_CALIBRATED_RGB = 0;
    private static final int LCS_sRGB = 1;
    private static final int LCS_CMYK = 2;
    private static final int BI_RGB = 0;
    private static final int BI_RLE8 = 1;
    private static final int BI_RLE4 = 2;
    private static final int BI_BITFIELDS = 3;
    int width;
    int height;

    BmpImage(InputStream is, boolean noHeader, int size) throws IOException {
        this.bitmapFileSize = size;
        this.bitmapOffset = 0;
        this.process(is, noHeader);
    }

    public static Image getImage(URL url) throws IOException {
        Image image;
        block3 : {
            InputStream is = null;
            try {
                is = url.openStream();
                Image img = BmpImage.getImage(is);
                img.setUrl(url);
                image = img;
                Object var3_4 = null;
                if (is == null) break block3;
            }
            catch (Throwable var4_6) {
                Object var3_5 = null;
                if (is != null) {
                    is.close();
                }
                throw var4_6;
            }
            is.close();
        }
        return image;
    }

    public static Image getImage(InputStream is) throws IOException {
        return BmpImage.getImage(is, false, 0);
    }

    public static Image getImage(InputStream is, boolean noHeader, int size) throws IOException {
        BmpImage bmp = new BmpImage(is, noHeader, size);
        try {
            Image img = bmp.getImage();
            img.setDpi((int)((double)bmp.xPelsPerMeter * 0.0254), (int)((double)bmp.xPelsPerMeter * 0.0254));
            img.setOriginalType(4);
            return img;
        }
        catch (BadElementException be) {
            throw new ExceptionConverter(be);
        }
    }

    public static Image getImage(String file) throws IOException {
        return BmpImage.getImage(Image.toURL(file));
    }

    public static Image getImage(byte[] data) throws IOException {
        Image image;
        block3 : {
            ByteArrayInputStream is = null;
            try {
                is = new ByteArrayInputStream(data);
                Image img = BmpImage.getImage(is);
                img.setOriginalData(data);
                image = img;
                Object var3_4 = null;
                if (is == null) break block3;
            }
            catch (Throwable var4_6) {
                Object var3_5 = null;
                if (is != null) {
                    is.close();
                }
                throw var4_6;
            }
            is.close();
        }
        return image;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    protected void process(InputStream stream, boolean noHeader) throws IOException {
        this.inputStream = noHeader != false || stream instanceof BufferedInputStream != false ? stream : new BufferedInputStream(stream);
        if (!noHeader) {
            if (this.readUnsignedByte(this.inputStream) != 66) throw new RuntimeException("Invalid magic value for BMP file.");
            if (this.readUnsignedByte(this.inputStream) != 77) {
                throw new RuntimeException("Invalid magic value for BMP file.");
            }
            this.bitmapFileSize = this.readDWord(this.inputStream);
            this.readWord(this.inputStream);
            this.readWord(this.inputStream);
            this.bitmapOffset = this.readDWord(this.inputStream);
        }
        if ((size = this.readDWord(this.inputStream)) == 12) {
            this.width = this.readWord(this.inputStream);
            this.height = this.readWord(this.inputStream);
        } else {
            this.width = this.readLong(this.inputStream);
            this.height = this.readLong(this.inputStream);
        }
        planes = this.readWord(this.inputStream);
        this.bitsPerPixel = this.readWord(this.inputStream);
        this.properties.put("color_planes", new Integer(planes));
        this.properties.put("bits_per_pixel", new Integer(this.bitsPerPixel));
        this.numBands = 3;
        if (this.bitmapOffset == 0) {
            this.bitmapOffset = size;
        }
        if (size != 12) ** GOTO lbl56
        this.properties.put("bmp_version", "BMP v. 2.x");
        if (this.bitsPerPixel == 1) {
            this.imageType = 0;
        } else if (this.bitsPerPixel == 4) {
            this.imageType = 1;
        } else if (this.bitsPerPixel == 8) {
            this.imageType = 2;
        } else if (this.bitsPerPixel == 24) {
            this.imageType = 3;
        }
        numberOfEntries = (int)((this.bitmapOffset - 14 - size) / 3);
        sizeOfPalette = numberOfEntries * 3;
        if (this.bitmapOffset == size) {
            switch (this.imageType) {
                case 0: {
                    sizeOfPalette = 6;
                    break;
                }
                case 1: {
                    sizeOfPalette = 48;
                    break;
                }
                case 2: {
                    sizeOfPalette = 768;
                    break;
                }
                case 3: {
                    sizeOfPalette = 0;
                }
            }
            this.bitmapOffset = size + (long)sizeOfPalette;
        }
        this.palette = new byte[sizeOfPalette];
        this.inputStream.read(this.palette, 0, sizeOfPalette);
        this.properties.put("palette", this.palette);
        ** GOTO lbl241
lbl56: // 1 sources:
        this.compression = this.readDWord(this.inputStream);
        this.imageSize = this.readDWord(this.inputStream);
        this.xPelsPerMeter = this.readLong(this.inputStream);
        this.yPelsPerMeter = this.readLong(this.inputStream);
        colorsUsed = this.readDWord(this.inputStream);
        colorsImportant = this.readDWord(this.inputStream);
        switch ((int)this.compression) {
            case 0: {
                this.properties.put("compression", "BI_RGB");
                break;
            }
            case 1: {
                this.properties.put("compression", "BI_RLE8");
                break;
            }
            case 2: {
                this.properties.put("compression", "BI_RLE4");
                break;
            }
            case 3: {
                this.properties.put("compression", "BI_BITFIELDS");
            }
        }
        this.properties.put("x_pixels_per_meter", new Long(this.xPelsPerMeter));
        this.properties.put("y_pixels_per_meter", new Long(this.yPelsPerMeter));
        this.properties.put("colors_used", new Long(colorsUsed));
        this.properties.put("colors_important", new Long(colorsImportant));
        if (size != 40) ** GOTO lbl150
        switch ((int)this.compression) {
            case 0: 
            case 1: 
            case 2: {
                if (this.bitsPerPixel == 1) {
                    this.imageType = 4;
                } else if (this.bitsPerPixel == 4) {
                    this.imageType = 5;
                } else if (this.bitsPerPixel == 8) {
                    this.imageType = 6;
                } else if (this.bitsPerPixel == 24) {
                    this.imageType = 7;
                } else if (this.bitsPerPixel == 16) {
                    this.imageType = 8;
                    this.redMask = 31744;
                    this.greenMask = 992;
                    this.blueMask = 31;
                    this.properties.put("red_mask", new Integer(this.redMask));
                    this.properties.put("green_mask", new Integer(this.greenMask));
                    this.properties.put("blue_mask", new Integer(this.blueMask));
                } else if (this.bitsPerPixel == 32) {
                    this.imageType = 9;
                    this.redMask = 16711680;
                    this.greenMask = 65280;
                    this.blueMask = 255;
                    this.properties.put("red_mask", new Integer(this.redMask));
                    this.properties.put("green_mask", new Integer(this.greenMask));
                    this.properties.put("blue_mask", new Integer(this.blueMask));
                }
                numberOfEntries = (int)((this.bitmapOffset - 14 - size) / 4);
                sizeOfPalette = numberOfEntries * 4;
                if (this.bitmapOffset == size) {
                    switch (this.imageType) {
                        case 4: {
                            sizeOfPalette = (int)(colorsUsed == 0 ? 2 : colorsUsed) * 4;
                            ** break;
                        }
                        case 5: {
                            sizeOfPalette = (int)(colorsUsed == 0 ? 16 : colorsUsed) * 4;
                            ** break;
                        }
                        case 6: {
                            sizeOfPalette = (int)(colorsUsed == 0 ? 256 : colorsUsed) * 4;
                            ** break;
                        }
                    }
                    sizeOfPalette = 0;
lbl124: // 4 sources:
                    this.bitmapOffset = size + (long)sizeOfPalette;
                }
                this.palette = new byte[sizeOfPalette];
                this.inputStream.read(this.palette, 0, sizeOfPalette);
                this.properties.put("palette", this.palette);
                this.properties.put("bmp_version", "BMP v. 3.x");
                ** GOTO lbl241
            }
            case 3: {
                if (this.bitsPerPixel == 16) {
                    this.imageType = 8;
                } else if (this.bitsPerPixel == 32) {
                    this.imageType = 9;
                }
                this.redMask = (int)this.readDWord(this.inputStream);
                this.greenMask = (int)this.readDWord(this.inputStream);
                this.blueMask = (int)this.readDWord(this.inputStream);
                this.properties.put("red_mask", new Integer(this.redMask));
                this.properties.put("green_mask", new Integer(this.greenMask));
                this.properties.put("blue_mask", new Integer(this.blueMask));
                if (colorsUsed != 0) {
                    sizeOfPalette = (int)colorsUsed * 4;
                    this.palette = new byte[sizeOfPalette];
                    this.inputStream.read(this.palette, 0, sizeOfPalette);
                    this.properties.put("palette", this.palette);
                }
                this.properties.put("bmp_version", "BMP v. 3.x NT");
                ** GOTO lbl241
            }
        }
        throw new RuntimeException("Invalid compression specified in BMP file.");
lbl150: // 1 sources:
        if (size != 108) {
            this.properties.put("bmp_version", "BMP v. 5.x");
            throw new RuntimeException("BMP version 5 not implemented yet.");
        }
        this.properties.put("bmp_version", "BMP v. 4.x");
        this.redMask = (int)this.readDWord(this.inputStream);
        this.greenMask = (int)this.readDWord(this.inputStream);
        this.blueMask = (int)this.readDWord(this.inputStream);
        this.alphaMask = (int)this.readDWord(this.inputStream);
        csType = this.readDWord(this.inputStream);
        redX = this.readLong(this.inputStream);
        redY = this.readLong(this.inputStream);
        redZ = this.readLong(this.inputStream);
        greenX = this.readLong(this.inputStream);
        greenY = this.readLong(this.inputStream);
        greenZ = this.readLong(this.inputStream);
        blueX = this.readLong(this.inputStream);
        blueY = this.readLong(this.inputStream);
        blueZ = this.readLong(this.inputStream);
        gammaRed = this.readDWord(this.inputStream);
        gammaGreen = this.readDWord(this.inputStream);
        gammaBlue = this.readDWord(this.inputStream);
        if (this.bitsPerPixel == 1) {
            this.imageType = 10;
        } else if (this.bitsPerPixel == 4) {
            this.imageType = 11;
        } else if (this.bitsPerPixel == 8) {
            this.imageType = 12;
        } else if (this.bitsPerPixel == 16) {
            this.imageType = 13;
            if ((int)this.compression == 0) {
                this.redMask = 31744;
                this.greenMask = 992;
                this.blueMask = 31;
            }
        } else if (this.bitsPerPixel == 24) {
            this.imageType = 14;
        } else if (this.bitsPerPixel == 32) {
            this.imageType = 15;
            if ((int)this.compression == 0) {
                this.redMask = 16711680;
                this.greenMask = 65280;
                this.blueMask = 255;
            }
        }
        this.properties.put("red_mask", new Integer(this.redMask));
        this.properties.put("green_mask", new Integer(this.greenMask));
        this.properties.put("blue_mask", new Integer(this.blueMask));
        this.properties.put("alpha_mask", new Integer(this.alphaMask));
        numberOfEntries = (int)((this.bitmapOffset - 14 - size) / 4);
        sizeOfPalette = numberOfEntries * 4;
        if (this.bitmapOffset == size) {
            switch (this.imageType) {
                case 10: {
                    sizeOfPalette = (int)(colorsUsed == 0 ? 2 : colorsUsed) * 4;
                    ** break;
                }
                case 11: {
                    sizeOfPalette = (int)(colorsUsed == 0 ? 16 : colorsUsed) * 4;
                    ** break;
                }
                case 12: {
                    sizeOfPalette = (int)(colorsUsed == 0 ? 256 : colorsUsed) * 4;
                    ** break;
                }
            }
            sizeOfPalette = 0;
lbl214: // 4 sources:
            this.bitmapOffset = size + (long)sizeOfPalette;
        }
        this.palette = new byte[sizeOfPalette];
        this.inputStream.read(this.palette, 0, sizeOfPalette);
        if (this.palette != null || this.palette.length != 0) {
            this.properties.put("palette", this.palette);
        }
        switch ((int)csType) {
            case 0: {
                this.properties.put("color_space", "LCS_CALIBRATED_RGB");
                this.properties.put("redX", new Integer(redX));
                this.properties.put("redY", new Integer(redY));
                this.properties.put("redZ", new Integer(redZ));
                this.properties.put("greenX", new Integer(greenX));
                this.properties.put("greenY", new Integer(greenY));
                this.properties.put("greenZ", new Integer(greenZ));
                this.properties.put("blueX", new Integer(blueX));
                this.properties.put("blueY", new Integer(blueY));
                this.properties.put("blueZ", new Integer(blueZ));
                this.properties.put("gamma_red", new Long(gammaRed));
                this.properties.put("gamma_green", new Long(gammaGreen));
                this.properties.put("gamma_blue", new Long(gammaBlue));
                throw new RuntimeException("Not implemented yet.");
            }
            case 1: {
                this.properties.put("color_space", "LCS_sRGB");
                break;
            }
            case 2: {
                this.properties.put("color_space", "LCS_CMYK");
                throw new RuntimeException("Not implemented yet.");
            }
        }
lbl241: // 5 sources:
        if (this.height > 0) {
            this.isBottomUp = true;
        } else {
            this.isBottomUp = false;
            this.height = Math.abs(this.height);
        }
        if (this.bitsPerPixel != 1 && this.bitsPerPixel != 4 && this.bitsPerPixel != 8) ** GOTO lbl265
        this.numBands = 1;
        if (this.imageType != 0 && this.imageType != 1 && this.imageType != 2) ** GOTO lbl257
        sizep = this.palette.length / 3;
        if (sizep > 256) {
            sizep = 256;
        }
        r = new byte[sizep];
        g = new byte[sizep];
        b = new byte[sizep];
        i = 0;
        ** GOTO lbl292
lbl257: // 1 sources:
        sizep = this.palette.length / 4;
        if (sizep > 256) {
            sizep = 256;
        }
        r = new byte[sizep];
        g = new byte[sizep];
        b = new byte[sizep];
        i = 0;
        ** GOTO lbl299
lbl265: // 1 sources:
        if (this.bitsPerPixel == 16) {
            this.numBands = 3;
            return;
        }
        if (this.bitsPerPixel != 32) {
            this.numBands = 3;
            return;
        }
        v0 = this.numBands = this.alphaMask == 0 ? 3 : 4;
        if (this.numBands == 3) {
            v1 = new int[3];
            v1[0] = this.redMask;
            v1[1] = this.greenMask;
            v2 = v1;
            v1[2] = this.blueMask;
        } else {
            v3 = new int[4];
            v3[0] = this.redMask;
            v3[1] = this.greenMask;
            v3[2] = this.blueMask;
            v2 = v3;
            v3[3] = this.alphaMask;
        }
        bitMasks = v2;
        return;
lbl-1000: // 1 sources:
        {
            off = 3 * i;
            b[i] = this.palette[off];
            g[i] = this.palette[off + 1];
            r[i] = this.palette[off + 2];
            ++i;
lbl292: // 2 sources:
            ** while (i < sizep)
        }
lbl293: // 1 sources:
        return;
lbl-1000: // 1 sources:
        {
            off = 4 * i;
            b[i] = this.palette[off];
            g[i] = this.palette[off + 1];
            r[i] = this.palette[off + 2];
            ++i;
lbl299: // 2 sources:
            ** while (i < sizep)
        }
lbl300: // 1 sources:
    }

    private byte[] getPalette(int group) {
        if (this.palette == null) {
            return null;
        }
        byte[] np = new byte[this.palette.length / group * 3];
        int e = this.palette.length / group;
        for (int k = 0; k < e; ++k) {
            int src = k * group;
            int dest = k * 3;
            np[dest + 2] = this.palette[src++];
            np[dest + 1] = this.palette[src++];
            np[dest] = this.palette[src];
        }
        return np;
    }

    private Image getImage() throws IOException, BadElementException {
        byte[] bdata = null;
        short[] sdata = null;
        int[] idata = null;
        switch (this.imageType) {
            case 0: {
                return this.read1Bit(3);
            }
            case 1: {
                return this.read4Bit(3);
            }
            case 2: {
                return this.read8Bit(3);
            }
            case 3: {
                bdata = new byte[this.width * this.height * 3];
                this.read24Bit(bdata);
                return new ImgRaw(this.width, this.height, 3, 8, bdata);
            }
            case 4: {
                return this.read1Bit(4);
            }
            case 5: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read4Bit(4);
                    }
                    case 2: {
                        return this.readRLE4();
                    }
                }
                throw new RuntimeException("Invalid compression specified for BMP file.");
            }
            case 6: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read8Bit(4);
                    }
                    case 1: {
                        return this.readRLE8();
                    }
                }
                throw new RuntimeException("Invalid compression specified for BMP file.");
            }
            case 7: {
                bdata = new byte[this.width * this.height * 3];
                this.read24Bit(bdata);
                return new ImgRaw(this.width, this.height, 3, 8, bdata);
            }
            case 8: {
                return this.read1632Bit(false);
            }
            case 9: {
                return this.read1632Bit(true);
            }
            case 10: {
                return this.read1Bit(4);
            }
            case 11: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read4Bit(4);
                    }
                    case 2: {
                        return this.readRLE4();
                    }
                }
                throw new RuntimeException("Invalid compression specified for BMP file.");
            }
            case 12: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read8Bit(4);
                    }
                    case 1: {
                        return this.readRLE8();
                    }
                }
                throw new RuntimeException("Invalid compression specified for BMP file.");
            }
            case 13: {
                return this.read1632Bit(false);
            }
            case 14: {
                bdata = new byte[this.width * this.height * 3];
                this.read24Bit(bdata);
                return new ImgRaw(this.width, this.height, 3, 8, bdata);
            }
            case 15: {
                return this.read1632Bit(true);
            }
        }
        return null;
    }

    private Image indexedModel(byte[] bdata, int bpc, int paletteEntries) throws BadElementException {
        ImgRaw img = new ImgRaw(this.width, this.height, 1, bpc, bdata);
        PdfArray colorspace = new PdfArray();
        colorspace.add(PdfName.INDEXED);
        colorspace.add(PdfName.DEVICERGB);
        byte[] np = this.getPalette(paletteEntries);
        int len = np.length;
        colorspace.add(new PdfNumber(len / 3 - 1));
        colorspace.add(new PdfString(np));
        PdfDictionary ad = new PdfDictionary();
        ad.put(PdfName.COLORSPACE, colorspace);
        img.setAdditional(ad);
        return img;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private Image read1Bit(int paletteEntries) throws IOException, BadElementException {
        byte[] bdata = new byte[(this.width + 7) / 8 * this.height];
        int padding = 0;
        int bytesPerScanline = (int)Math.ceil((double)this.width / 8.0);
        int remainder = bytesPerScanline % 4;
        if (remainder != 0) {
            padding = 4 - remainder;
        }
        int imSize = (bytesPerScanline + padding) * this.height;
        byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead+=this.inputStream.read((byte[])values, (int)bytesRead, (int)(imSize - bytesRead))) {
        }
        if (this.isBottomUp) {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, imSize - (i + 1) * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
            return this.indexedModel(bdata, 1, paletteEntries);
        } else {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, i * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
        }
        return this.indexedModel(bdata, 1, paletteEntries);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private Image read4Bit(int paletteEntries) throws IOException, BadElementException {
        byte[] bdata = new byte[(this.width + 1) / 2 * this.height];
        int padding = 0;
        int bytesPerScanline = (int)Math.ceil((double)this.width / 2.0);
        int remainder = bytesPerScanline % 4;
        if (remainder != 0) {
            padding = 4 - remainder;
        }
        int imSize = (bytesPerScanline + padding) * this.height;
        byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead+=this.inputStream.read((byte[])values, (int)bytesRead, (int)(imSize - bytesRead))) {
        }
        if (this.isBottomUp) {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, imSize - (i + 1) * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
            return this.indexedModel(bdata, 4, paletteEntries);
        } else {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, i * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
        }
        return this.indexedModel(bdata, 4, paletteEntries);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private Image read8Bit(int paletteEntries) throws IOException, BadElementException {
        byte[] bdata = new byte[this.width * this.height];
        int padding = 0;
        int bitsPerScanline = this.width * 8;
        if (bitsPerScanline % 32 != 0) {
            padding = (bitsPerScanline / 32 + 1) * 32 - bitsPerScanline;
            padding = (int)Math.ceil((double)padding / 8.0);
        }
        int imSize = (this.width + padding) * this.height;
        byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead+=this.inputStream.read((byte[])values, (int)bytesRead, (int)(imSize - bytesRead))) {
        }
        if (this.isBottomUp) {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, imSize - (i + 1) * (this.width + padding), bdata, i * this.width, this.width);
            }
            return this.indexedModel(bdata, 8, paletteEntries);
        } else {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, i * (this.width + padding), bdata, i * this.width, this.width);
            }
        }
        return this.indexedModel(bdata, 8, paletteEntries);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void read24Bit(byte[] bdata) {
        int count;
        int padding = 0;
        int bitsPerScanline = this.width * 24;
        if (bitsPerScanline % 32 != 0) {
            padding = (bitsPerScanline / 32 + 1) * 32 - bitsPerScanline;
            padding = (int)Math.ceil((double)padding / 8.0);
        }
        int imSize = (this.width * 3 + 3) / 4 * 4 * this.height;
        byte[] values = new byte[imSize];
        try {
            int r;
            for (int bytesRead = 0; bytesRead < imSize; bytesRead+=r) {
                r = this.inputStream.read(values, bytesRead, imSize - bytesRead);
                if (r >= 0) {
                    continue;
                } else {
                    break;
                }
            }
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
        int l = 0;
        if (this.isBottomUp) {
            int max = this.width * this.height * 3 - 1;
            count = - padding;
            for (int i = 0; i < this.height; ++i) {
                l = max - (i + 1) * this.width * 3 + 1;
                count+=padding;
                for (int j = 0; j < this.width; ++j) {
                    bdata[l + 2] = values[count++];
                    bdata[l + 1] = values[count++];
                    bdata[l] = values[count++];
                    l+=3;
                }
            }
            return;
        } else {
            count = - padding;
            for (int i = 0; i < this.height; ++i) {
                count+=padding;
                for (int j = 0; j < this.width; ++j) {
                    bdata[l + 2] = values[count++];
                    bdata[l + 1] = values[count++];
                    bdata[l] = values[count++];
                    l+=3;
                }
            }
        }
    }

    private int findMask(int mask) {
        for (int k = 0; k < 32; ++k) {
            if ((mask & 1) == 1) break;
            mask>>>=1;
        }
        return mask;
    }

    private int findShift(int mask) {
        int k;
        for (k = 0; k < 32; ++k) {
            if ((mask & 1) == 1) break;
            mask>>>=1;
        }
        return k;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private Image read1632Bit(boolean is32) throws IOException, BadElementException {
        int bitsPerScanline;
        int imSize;
        int red_mask = this.findMask(this.redMask);
        int red_shift = this.findShift(this.redMask);
        int red_factor = red_mask + 1;
        int green_mask = this.findMask(this.greenMask);
        int green_shift = this.findShift(this.greenMask);
        int green_factor = green_mask + 1;
        int blue_mask = this.findMask(this.blueMask);
        int blue_shift = this.findShift(this.blueMask);
        int blue_factor = blue_mask + 1;
        byte[] bdata = new byte[this.width * this.height * 3];
        int padding = 0;
        if (!(is32 || (bitsPerScanline = this.width * 16) % 32 == 0)) {
            padding = (bitsPerScanline / 32 + 1) * 32 - bitsPerScanline;
            padding = (int)Math.ceil((double)padding / 8.0);
        }
        if ((imSize = (int)this.imageSize) == 0) {
            imSize = (int)(this.bitmapFileSize - this.bitmapOffset);
        }
        int l = 0;
        if (this.isBottomUp) {
            int max = this.width * this.height - 1;
            for (int i = this.height - 1; i >= 0; --i) {
                l = this.width * 3 * i;
                for (int j = 0; j < this.width; ++j) {
                    int v = is32 ? (int)this.readDWord(this.inputStream) : this.readWord(this.inputStream);
                    bdata[l++] = (byte)((v >>> red_shift & red_mask) * 256 / red_factor);
                    bdata[l++] = (byte)((v >>> green_shift & green_mask) * 256 / green_factor);
                    bdata[l++] = (byte)((v >>> blue_shift & blue_mask) * 256 / blue_factor);
                }
                for (int m = 0; m < padding; ++m) {
                    this.inputStream.read();
                }
            }
            return new ImgRaw(this.width, this.height, 3, 8, bdata);
        } else {
            for (int i = 0; i < this.height; ++i) {
                for (int j = 0; j < this.width; ++j) {
                    int v = is32 ? (int)this.readDWord(this.inputStream) : this.readWord(this.inputStream);
                    bdata[l++] = (byte)((v >>> red_shift & red_mask) * 256 / red_factor);
                    bdata[l++] = (byte)((v >>> green_shift & green_mask) * 256 / green_factor);
                    bdata[l++] = (byte)((v >>> blue_shift & blue_mask) * 256 / blue_factor);
                }
                for (int m = 0; m < padding; ++m) {
                    this.inputStream.read();
                }
            }
        }
        return new ImgRaw(this.width, this.height, 3, 8, bdata);
    }

    private Image readRLE8() throws IOException, BadElementException {
        int imSize = (int)this.imageSize;
        if (imSize == 0) {
            imSize = (int)(this.bitmapFileSize - this.bitmapOffset);
        }
        int padding = 0;
        int remainder = this.width % 4;
        if (remainder != 0) {
            padding = 4 - remainder;
        }
        byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead+=this.inputStream.read((byte[])values, (int)bytesRead, (int)(imSize - bytesRead))) {
        }
        byte[] val = this.decodeRLE(true, values);
        imSize = this.width * this.height;
        if (this.isBottomUp) {
            byte[] temp = new byte[val.length];
            int bytesPerScanline = this.width;
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(val, imSize - (i + 1) * bytesPerScanline, temp, i * bytesPerScanline, bytesPerScanline);
            }
            val = temp;
        }
        return this.indexedModel(val, 8, 4);
    }

    private Image readRLE4() throws IOException, BadElementException {
        int imSize = (int)this.imageSize;
        if (imSize == 0) {
            imSize = (int)(this.bitmapFileSize - this.bitmapOffset);
        }
        int padding = 0;
        int remainder = this.width % 4;
        if (remainder != 0) {
            padding = 4 - remainder;
        }
        byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead+=this.inputStream.read((byte[])values, (int)bytesRead, (int)(imSize - bytesRead))) {
        }
        byte[] val = this.decodeRLE(false, values);
        if (this.isBottomUp) {
            byte[] inverted = val;
            val = new byte[this.width * this.height];
            int l = 0;
            for (int i = this.height - 1; i >= 0; --i) {
                int index = i * this.width;
                int lineEnd = l + this.width;
                while (l != lineEnd) {
                    val[l++] = inverted[index++];
                }
            }
        }
        int stride = (this.width + 1) / 2;
        byte[] bdata = new byte[stride * this.height];
        int ptr = 0;
        boolean flip = true;
        int sh = 0;
        for (int h = 0; h < this.height; ++h) {
            for (int w = 0; w < this.width; ++w) {
                if ((w & 1) == 0) {
                    bdata[sh + w / 2] = (byte)(val[ptr++] << 4);
                    continue;
                }
                byte[] arrby = bdata;
                int n = sh + w / 2;
                arrby[n] = (byte)(arrby[n] | (byte)(val[ptr++] & 15));
            }
            sh+=stride;
        }
        return this.indexedModel(bdata, 4, 4);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    private byte[] decodeRLE(boolean is8, byte[] values) {
        block16 : {
            val = new byte[this.width * this.height];
            try {
                ptr = 0;
                x = 0;
                q = 0;
                y = 0;
lbl7: // 9 sources:
                if (y >= this.height) return val;
                if (ptr >= values.length) {
                    return val;
                }
                if ((count = values[ptr++] & 255) == 0) ** GOTO lbl17
                bt = values[ptr++] & 255;
                if (!is8) ** GOTO lbl15
                i = count;
                ** GOTO lbl42
lbl15: // 1 sources:
                i = 0;
                ** GOTO lbl46
lbl17: // 1 sources:
                if ((count = values[ptr++] & 255) == 1) {
                    return val;
                }
                switch (count) {
                    case 0: {
                        x = 0;
                        q = ++y * this.width;
                        ** break;
                    }
                    case 2: {
                        q = (y+=values[ptr++] & 255) * this.width + (x+=values[ptr++] & 255);
                        ** break;
                    }
                }
                if (!is8) ** GOTO lbl30
                i = count;
                ** GOTO lbl51
lbl30: // 1 sources:
                bt = 0;
                for (i = 0; i < count; ++i) {
                    if ((i & 1) == 0) {
                        bt = values[ptr++] & 255;
                    }
                    val[q++] = (byte)((i & 1) == 1 ? bt & 15 : bt >>> 4 & 15);
                }
                break block16;
            }
            catch (Exception ptr) {
                // empty catch block
            }
            return val;
lbl-1000: // 1 sources:
            {
                val[q++] = (byte)bt;
                --i;
lbl42: // 2 sources:
                ** while (i != 0)
            }
lbl43: // 1 sources:
            ** GOTO lbl47
lbl-1000: // 1 sources:
            {
                val[q++] = (byte)((i & 1) == 1 ? bt & 15 : bt >>> 4 & 15);
                ++i;
lbl46: // 2 sources:
                ** while (i < count)
            }
lbl47: // 2 sources:
            x+=count;
            ** GOTO lbl7
lbl-1000: // 1 sources:
            {
                val[q++] = (byte)(values[ptr++] & 255);
                --i;
lbl51: // 2 sources:
                ** while (i != 0)
            }
        }
        x+=count;
        if (is8) {
            if ((count & 1) == 1) {
                ++ptr;
            } else {
                ** GOTO lbl7
            }
            ** break;
        }
        if ((count & 3) != 1 && (count & 3) != 2) ** GOTO lbl7
        ++ptr;
        ** GOTO lbl7
    }

    private int readUnsignedByte(InputStream stream) throws IOException {
        return stream.read() & 255;
    }

    private int readUnsignedShort(InputStream stream) throws IOException {
        int b1 = this.readUnsignedByte(stream);
        int b2 = this.readUnsignedByte(stream);
        return (b2 << 8 | b1) & 65535;
    }

    private int readShort(InputStream stream) throws IOException {
        int b1 = this.readUnsignedByte(stream);
        int b2 = this.readUnsignedByte(stream);
        return b2 << 8 | b1;
    }

    private int readWord(InputStream stream) throws IOException {
        return this.readUnsignedShort(stream);
    }

    private long readUnsignedInt(InputStream stream) throws IOException {
        int b1 = this.readUnsignedByte(stream);
        int b2 = this.readUnsignedByte(stream);
        int b3 = this.readUnsignedByte(stream);
        int b4 = this.readUnsignedByte(stream);
        long l = b4 << 24 | b3 << 16 | b2 << 8 | b1;
        return l & -1;
    }

    private int readInt(InputStream stream) throws IOException {
        int b1 = this.readUnsignedByte(stream);
        int b2 = this.readUnsignedByte(stream);
        int b3 = this.readUnsignedByte(stream);
        int b4 = this.readUnsignedByte(stream);
        return b4 << 24 | b3 << 16 | b2 << 8 | b1;
    }

    private long readDWord(InputStream stream) throws IOException {
        return this.readUnsignedInt(stream);
    }

    private int readLong(InputStream stream) throws IOException {
        return this.readInt(stream);
    }
}

