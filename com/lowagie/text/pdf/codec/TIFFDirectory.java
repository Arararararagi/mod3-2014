/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec;

import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.TIFFField;
import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

public class TIFFDirectory
implements Serializable {
    boolean isBigEndian;
    int numEntries;
    TIFFField[] fields;
    Hashtable fieldIndex = new Hashtable();
    long IFDOffset = 8;
    long nextIFDOffset = 0;
    private static final int[] sizeOfType;

    static {
        int[] arrn = new int[13];
        arrn[1] = 1;
        arrn[2] = 1;
        arrn[3] = 2;
        arrn[4] = 4;
        arrn[5] = 8;
        arrn[6] = 1;
        arrn[7] = 1;
        arrn[8] = 2;
        arrn[9] = 4;
        arrn[10] = 8;
        arrn[11] = 4;
        arrn[12] = 8;
        sizeOfType = arrn;
    }

    TIFFDirectory() {
    }

    private static boolean isValidEndianTag(int endian) {
        if (endian != 18761 && endian != 19789) {
            return false;
        }
        return true;
    }

    public TIFFDirectory(RandomAccessFileOrArray stream, int directory) throws IOException {
        long global_save_offset = stream.getFilePointer();
        stream.seek(0);
        int endian = stream.readUnsignedShort();
        if (!TIFFDirectory.isValidEndianTag(endian)) {
            throw new IllegalArgumentException("Bad endianness tag (not 0x4949 or 0x4d4d).");
        }
        this.isBigEndian = endian == 19789;
        int magic = this.readUnsignedShort(stream);
        if (magic != 42) {
            throw new IllegalArgumentException("Bad magic number, should be 42.");
        }
        long ifd_offset = this.readUnsignedInt(stream);
        for (int i = 0; i < directory; ++i) {
            if (ifd_offset == 0) {
                throw new IllegalArgumentException("Directory number too large.");
            }
            stream.seek(ifd_offset);
            int entries = this.readUnsignedShort(stream);
            stream.skip(12 * entries);
            ifd_offset = this.readUnsignedInt(stream);
        }
        stream.seek(ifd_offset);
        this.initialize(stream);
        stream.seek(global_save_offset);
    }

    public TIFFDirectory(RandomAccessFileOrArray stream, long ifd_offset, int directory) throws IOException {
        long global_save_offset = stream.getFilePointer();
        stream.seek(0);
        int endian = stream.readUnsignedShort();
        if (!TIFFDirectory.isValidEndianTag(endian)) {
            throw new IllegalArgumentException("Bad endianness tag (not 0x4949 or 0x4d4d).");
        }
        this.isBigEndian = endian == 19789;
        stream.seek(ifd_offset);
        for (int dirNum = 0; dirNum < directory; ++dirNum) {
            int numEntries = this.readUnsignedShort(stream);
            stream.seek(ifd_offset + (long)(12 * numEntries));
            ifd_offset = this.readUnsignedInt(stream);
            stream.seek(ifd_offset);
        }
        this.initialize(stream);
        stream.seek(global_save_offset);
    }

    private void initialize(RandomAccessFileOrArray stream) throws IOException {
        long nextTagOffset = 0;
        long maxOffset = stream.length();
        this.IFDOffset = stream.getFilePointer();
        this.numEntries = this.readUnsignedShort(stream);
        this.fields = new TIFFField[this.numEntries];
        for (int i = 0; i < this.numEntries && nextTagOffset < maxOffset; ++i) {
            int tag = this.readUnsignedShort(stream);
            int type = this.readUnsignedShort(stream);
            int count = (int)this.readUnsignedInt(stream);
            boolean value = false;
            boolean processTag = true;
            nextTagOffset = stream.getFilePointer() + 4;
            try {
                if (count * sizeOfType[type] > 4) {
                    long valueOffset = this.readUnsignedInt(stream);
                    if (valueOffset < maxOffset) {
                        stream.seek(valueOffset);
                    } else {
                        processTag = false;
                    }
                }
            }
            catch (ArrayIndexOutOfBoundsException ae) {
                processTag = false;
            }
            if (processTag) {
                this.fieldIndex.put(new Integer(tag), new Integer(i));
                String[] obj = null;
                switch (type) {
                    int j;
                    case 1: 
                    case 2: 
                    case 6: 
                    case 7: {
                        byte[] bvalues = new byte[count];
                        stream.readFully(bvalues, 0, count);
                        if (type == 2) {
                            int index = 0;
                            int prevIndex = 0;
                            ArrayList<String> v = new ArrayList<String>();
                            while (index < count) {
                                while (index < count && bvalues[index++] != 0) {
                                }
                                v.add(new String(bvalues, prevIndex, index - prevIndex));
                                prevIndex = index;
                            }
                            count = v.size();
                            String[] strings = new String[count];
                            for (int c = 0; c < count; ++c) {
                                strings[c] = (String)v.get(c);
                            }
                            obj = strings;
                            break;
                        }
                        obj = (String[])bvalues;
                        break;
                    }
                    case 3: {
                        char[] cvalues = new char[count];
                        for (j = 0; j < count; ++j) {
                            cvalues[j] = (char)this.readUnsignedShort(stream);
                        }
                        obj = (String[])cvalues;
                        break;
                    }
                    case 4: {
                        long[] lvalues = new long[count];
                        for (j = 0; j < count; ++j) {
                            lvalues[j] = this.readUnsignedInt(stream);
                        }
                        obj = (String[])lvalues;
                        break;
                    }
                    case 5: {
                        long[][] llvalues = new long[count][2];
                        for (j = 0; j < count; ++j) {
                            llvalues[j][0] = this.readUnsignedInt(stream);
                            llvalues[j][1] = this.readUnsignedInt(stream);
                        }
                        obj = (String[])llvalues;
                        break;
                    }
                    case 8: {
                        short[] svalues = new short[count];
                        for (j = 0; j < count; ++j) {
                            svalues[j] = this.readShort(stream);
                        }
                        obj = (String[])svalues;
                        break;
                    }
                    case 9: {
                        int[] ivalues = new int[count];
                        for (j = 0; j < count; ++j) {
                            ivalues[j] = this.readInt(stream);
                        }
                        obj = (String[])ivalues;
                        break;
                    }
                    case 10: {
                        int[][] iivalues = new int[count][2];
                        for (j = 0; j < count; ++j) {
                            iivalues[j][0] = this.readInt(stream);
                            iivalues[j][1] = this.readInt(stream);
                        }
                        obj = (String[])iivalues;
                        break;
                    }
                    case 11: {
                        float[] fvalues = new float[count];
                        for (j = 0; j < count; ++j) {
                            fvalues[j] = this.readFloat(stream);
                        }
                        obj = (String[])fvalues;
                        break;
                    }
                    case 12: {
                        double[] dvalues = new double[count];
                        for (j = 0; j < count; ++j) {
                            dvalues[j] = this.readDouble(stream);
                        }
                        obj = (String[])dvalues;
                        break;
                    }
                }
                this.fields[i] = new TIFFField(tag, type, count, obj);
            }
            stream.seek(nextTagOffset);
        }
        this.nextIFDOffset = this.readUnsignedInt(stream);
    }

    public int getNumEntries() {
        return this.numEntries;
    }

    public TIFFField getField(int tag) {
        Integer i = (Integer)this.fieldIndex.get(new Integer(tag));
        if (i == null) {
            return null;
        }
        return this.fields[i];
    }

    public boolean isTagPresent(int tag) {
        return this.fieldIndex.containsKey(new Integer(tag));
    }

    public int[] getTags() {
        int[] tags = new int[this.fieldIndex.size()];
        Enumeration e = this.fieldIndex.keys();
        int i = 0;
        while (e.hasMoreElements()) {
            tags[i++] = (Integer)e.nextElement();
        }
        return tags;
    }

    public TIFFField[] getFields() {
        return this.fields;
    }

    public byte getFieldAsByte(int tag, int index) {
        Integer i = (Integer)this.fieldIndex.get(new Integer(tag));
        byte[] b = this.fields[i].getAsBytes();
        return b[index];
    }

    public byte getFieldAsByte(int tag) {
        return this.getFieldAsByte(tag, 0);
    }

    public long getFieldAsLong(int tag, int index) {
        Integer i = (Integer)this.fieldIndex.get(new Integer(tag));
        return this.fields[i].getAsLong(index);
    }

    public long getFieldAsLong(int tag) {
        return this.getFieldAsLong(tag, 0);
    }

    public float getFieldAsFloat(int tag, int index) {
        Integer i = (Integer)this.fieldIndex.get(new Integer(tag));
        return this.fields[i].getAsFloat(index);
    }

    public float getFieldAsFloat(int tag) {
        return this.getFieldAsFloat(tag, 0);
    }

    public double getFieldAsDouble(int tag, int index) {
        Integer i = (Integer)this.fieldIndex.get(new Integer(tag));
        return this.fields[i].getAsDouble(index);
    }

    public double getFieldAsDouble(int tag) {
        return this.getFieldAsDouble(tag, 0);
    }

    private short readShort(RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readShort();
        }
        return stream.readShortLE();
    }

    private int readUnsignedShort(RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readUnsignedShort();
        }
        return stream.readUnsignedShortLE();
    }

    private int readInt(RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readInt();
        }
        return stream.readIntLE();
    }

    private long readUnsignedInt(RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readUnsignedInt();
        }
        return stream.readUnsignedIntLE();
    }

    private long readLong(RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readLong();
        }
        return stream.readLongLE();
    }

    private float readFloat(RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readFloat();
        }
        return stream.readFloatLE();
    }

    private double readDouble(RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readDouble();
        }
        return stream.readDoubleLE();
    }

    private static int readUnsignedShort(RandomAccessFileOrArray stream, boolean isBigEndian) throws IOException {
        if (isBigEndian) {
            return stream.readUnsignedShort();
        }
        return stream.readUnsignedShortLE();
    }

    private static long readUnsignedInt(RandomAccessFileOrArray stream, boolean isBigEndian) throws IOException {
        if (isBigEndian) {
            return stream.readUnsignedInt();
        }
        return stream.readUnsignedIntLE();
    }

    public static int getNumDirectories(RandomAccessFileOrArray stream) throws IOException {
        long pointer = stream.getFilePointer();
        stream.seek(0);
        int endian = stream.readUnsignedShort();
        if (!TIFFDirectory.isValidEndianTag(endian)) {
            throw new IllegalArgumentException("Bad endianness tag (not 0x4949 or 0x4d4d).");
        }
        boolean isBigEndian = endian == 19789;
        int magic = TIFFDirectory.readUnsignedShort(stream, isBigEndian);
        if (magic != 42) {
            throw new IllegalArgumentException("Bad magic number, should be 42.");
        }
        stream.seek(4);
        long offset = TIFFDirectory.readUnsignedInt(stream, isBigEndian);
        int numDirectories = 0;
        while (offset != 0) {
            ++numDirectories;
            try {
                stream.seek(offset);
                int entries = TIFFDirectory.readUnsignedShort(stream, isBigEndian);
                stream.skip(12 * entries);
                offset = TIFFDirectory.readUnsignedInt(stream, isBigEndian);
                continue;
            }
            catch (EOFException eof) {
                --numDirectories;
                break;
            }
        }
        stream.seek(pointer);
        return numDirectories;
    }

    public boolean isBigEndian() {
        return this.isBigEndian;
    }

    public long getIFDOffset() {
        return this.IFDOffset;
    }

    public long getNextIFDOffset() {
        return this.nextIFDOffset;
    }
}

