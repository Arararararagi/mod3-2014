/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.ImgRaw;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfString;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class GifImage {
    protected DataInputStream in;
    protected int width;
    protected int height;
    protected boolean gctFlag;
    protected int bgIndex;
    protected int bgColor;
    protected int pixelAspect;
    protected boolean lctFlag;
    protected boolean interlace;
    protected int lctSize;
    protected int ix;
    protected int iy;
    protected int iw;
    protected int ih;
    protected byte[] block;
    protected int blockSize;
    protected int dispose;
    protected boolean transparency;
    protected int delay;
    protected int transIndex;
    protected static final int MaxStackSize = 4096;
    protected short[] prefix;
    protected byte[] suffix;
    protected byte[] pixelStack;
    protected byte[] pixels;
    protected byte[] m_out;
    protected int m_bpc;
    protected int m_gbpc;
    protected byte[] m_global_table;
    protected byte[] m_local_table;
    protected byte[] m_curr_table;
    protected int m_line_stride;
    protected byte[] fromData;
    protected URL fromUrl;
    protected ArrayList frames;

    /*
     * Exception decompiling
     */
    public GifImage(URL url) throws IOException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // java.lang.IllegalStateException: Backjump on non jumping statement [] lbl20 : TryStatement: try { 1[TRYBLOCK]

        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:44)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:22)
        // org.benf.cfr.reader.util.graph.GraphVisitorDFS.process(GraphVisitorDFS.java:68)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner.removeUnreachableCode(Cleaner.java:54)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.RemoveDeterministicJumps.apply(RemoveDeterministicJumps.java:34)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:501)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:214)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:159)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:91)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:353)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:731)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:663)
        // org.benf.cfr.reader.Main.doJar(Main.java:126)
        // org.benf.cfr.reader.Main.main(Main.java:178)
        throw new IllegalStateException("Decompilation failed");
    }

    public GifImage(String file) throws IOException {
        this(Image.toURL(file));
    }

    /*
     * Exception decompiling
     */
    public GifImage(byte[] data) throws IOException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // java.lang.IllegalStateException: Backjump on non jumping statement [] lbl20 : TryStatement: try { 1[TRYBLOCK]

        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:44)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner$1.call(Cleaner.java:22)
        // org.benf.cfr.reader.util.graph.GraphVisitorDFS.process(GraphVisitorDFS.java:68)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.Cleaner.removeUnreachableCode(Cleaner.java:54)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.op3rewriters.RemoveDeterministicJumps.apply(RemoveDeterministicJumps.java:34)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:501)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:214)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:159)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:91)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:353)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:731)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:663)
        // org.benf.cfr.reader.Main.doJar(Main.java:126)
        // org.benf.cfr.reader.Main.main(Main.java:178)
        throw new IllegalStateException("Decompilation failed");
    }

    public GifImage(InputStream is) throws IOException {
        this.block = new byte[256];
        this.blockSize = 0;
        this.dispose = 0;
        this.transparency = false;
        this.delay = 0;
        this.frames = new ArrayList();
        this.process(is);
    }

    public int getFrameCount() {
        return this.frames.size();
    }

    public Image getImage(int frame) {
        GifFrame gf = (GifFrame)this.frames.get(frame - 1);
        return gf.image;
    }

    public int[] getFramePosition(int frame) {
        GifFrame gf = (GifFrame)this.frames.get(frame - 1);
        return new int[]{gf.ix, gf.iy};
    }

    public int[] getLogicalScreen() {
        return new int[]{this.width, this.height};
    }

    void process(InputStream is) throws IOException {
        this.in = new DataInputStream(new BufferedInputStream(is));
        this.readHeader();
        this.readContents();
        if (this.frames.size() == 0) {
            throw new IOException("The file does not contain any valid image.");
        }
    }

    protected void readHeader() throws IOException {
        String id = "";
        for (int i = 0; i < 6; ++i) {
            id = String.valueOf(id) + (char)this.in.read();
        }
        if (!id.startsWith("GIF8")) {
            throw new IOException("Gif signature nor found.");
        }
        this.readLSD();
        if (this.gctFlag) {
            this.m_global_table = this.readColorTable(this.m_gbpc);
        }
    }

    protected void readLSD() throws IOException {
        this.width = this.readShort();
        this.height = this.readShort();
        int packed = this.in.read();
        this.gctFlag = (packed & 128) != 0;
        this.m_gbpc = (packed & 7) + 1;
        this.bgIndex = this.in.read();
        this.pixelAspect = this.in.read();
    }

    protected int readShort() throws IOException {
        return this.in.read() | this.in.read() << 8;
    }

    protected int readBlock() throws IOException {
        this.blockSize = this.in.read();
        if (this.blockSize <= 0) {
            this.blockSize = 0;
            return 0;
        }
        for (int k = 0; k < this.blockSize; ++k) {
            int v = this.in.read();
            if (v < 0) {
                this.blockSize = k;
                return this.blockSize;
            }
            this.block[k] = (byte)v;
        }
        return this.blockSize;
    }

    protected byte[] readColorTable(int bpc) throws IOException {
        int ncolors = 1 << bpc;
        int nbytes = 3 * ncolors;
        bpc = GifImage.newBpc(bpc);
        byte[] table = new byte[(1 << bpc) * 3];
        this.in.readFully(table, 0, nbytes);
        return table;
    }

    protected static int newBpc(int bpc) {
        switch (bpc) {
            case 1: 
            case 2: 
            case 4: {
                break;
            }
            case 3: {
                return 4;
            }
            default: {
                return 8;
            }
        }
        return bpc;
    }

    protected void readContents() throws IOException {
        boolean done = false;
        while (!done) {
            int code = this.in.read();
            block0 : switch (code) {
                case 44: {
                    this.readImage();
                    break;
                }
                case 33: {
                    code = this.in.read();
                    switch (code) {
                        case 249: {
                            this.readGraphicControlExt();
                            break block0;
                        }
                        case 255: {
                            this.readBlock();
                            this.skip();
                            break block0;
                        }
                    }
                    this.skip();
                    break;
                }
                default: {
                    done = true;
                }
            }
        }
    }

    protected void readImage() throws IOException {
        boolean skipZero;
        this.ix = this.readShort();
        this.iy = this.readShort();
        this.iw = this.readShort();
        this.ih = this.readShort();
        int packed = this.in.read();
        this.lctFlag = (packed & 128) != 0;
        this.interlace = (packed & 64) != 0;
        this.lctSize = 2 << (packed & 7);
        this.m_bpc = GifImage.newBpc(this.m_gbpc);
        if (this.lctFlag) {
            this.m_curr_table = this.readColorTable((packed & 7) + 1);
            this.m_bpc = GifImage.newBpc((packed & 7) + 1);
        } else {
            this.m_curr_table = this.m_global_table;
        }
        if (this.transparency && this.transIndex >= this.m_curr_table.length / 3) {
            this.transparency = false;
        }
        if (this.transparency && this.m_bpc == 1) {
            byte[] tp = new byte[12];
            System.arraycopy(this.m_curr_table, 0, tp, 0, 6);
            this.m_curr_table = tp;
            this.m_bpc = 2;
        }
        if (!(skipZero = this.decodeImageData())) {
            this.skip();
        }
        ImgRaw img = null;
        try {
            img = new ImgRaw(this.iw, this.ih, 1, this.m_bpc, this.m_out);
            PdfArray colorspace = new PdfArray();
            colorspace.add(PdfName.INDEXED);
            colorspace.add(PdfName.DEVICERGB);
            int len = this.m_curr_table.length;
            colorspace.add(new PdfNumber(len / 3 - 1));
            colorspace.add(new PdfString(this.m_curr_table));
            PdfDictionary ad = new PdfDictionary();
            ad.put(PdfName.COLORSPACE, colorspace);
            img.setAdditional(ad);
            if (this.transparency) {
                img.setTransparency(new int[]{this.transIndex, this.transIndex});
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        img.setOriginalType(3);
        img.setOriginalData(this.fromData);
        img.setUrl(this.fromUrl);
        GifFrame gf = new GifFrame();
        gf.image = img;
        gf.ix = this.ix;
        gf.iy = this.iy;
        this.frames.add(gf);
        this.resetFrame();
    }

    protected boolean decodeImageData() throws IOException {
        int code;
        int NullCode = -1;
        int npix = this.iw * this.ih;
        boolean skipZero = false;
        if (this.prefix == null) {
            this.prefix = new short[4096];
        }
        if (this.suffix == null) {
            this.suffix = new byte[4096];
        }
        if (this.pixelStack == null) {
            this.pixelStack = new byte[4097];
        }
        this.m_line_stride = (this.iw * this.m_bpc + 7) / 8;
        this.m_out = new byte[this.m_line_stride * this.ih];
        int pass = 1;
        int inc = this.interlace ? 8 : 1;
        int line = 0;
        int xpos = 0;
        int data_size = this.in.read();
        int clear = 1 << data_size;
        int end_of_information = clear + 1;
        int available = clear + 2;
        int old_code = NullCode;
        int code_size = data_size + 1;
        int code_mask = (1 << code_size) - 1;
        for (code = 0; code < clear; ++code) {
            this.prefix[code] = 0;
            this.suffix[code] = (byte)code;
        }
        int bi = 0;
        int pi = 0;
        int top = 0;
        int first = 0;
        int count = 0;
        int bits = 0;
        int datum = 0;
        int i = 0;
        while (i < npix) {
            if (top == 0) {
                if (bits < code_size) {
                    if (count == 0) {
                        count = this.readBlock();
                        if (count <= 0) {
                            skipZero = true;
                            break;
                        }
                        bi = 0;
                    }
                    datum+=(this.block[bi] & 255) << bits;
                    bits+=8;
                    ++bi;
                    --count;
                    continue;
                }
                code = datum & code_mask;
                datum>>=code_size;
                bits-=code_size;
                if (code > available || code == end_of_information) break;
                if (code == clear) {
                    code_size = data_size + 1;
                    code_mask = (1 << code_size) - 1;
                    available = clear + 2;
                    old_code = NullCode;
                    continue;
                }
                if (old_code == NullCode) {
                    this.pixelStack[top++] = this.suffix[code];
                    old_code = code;
                    first = code;
                    continue;
                }
                int in_code = code;
                if (code == available) {
                    this.pixelStack[top++] = (byte)first;
                    code = old_code;
                }
                while (code > clear) {
                    this.pixelStack[top++] = this.suffix[code];
                    code = this.prefix[code];
                }
                first = this.suffix[code] & 255;
                if (available >= 4096) break;
                this.pixelStack[top++] = (byte)first;
                this.prefix[available] = (short)old_code;
                this.suffix[available] = (byte)first;
                if ((++available & code_mask) == 0 && available < 4096) {
                    ++code_size;
                    code_mask+=available;
                }
                old_code = in_code;
            }
            ++i;
            this.setPixel(xpos, line, this.pixelStack[--top]);
            if (++xpos < this.iw) continue;
            xpos = 0;
            if ((line+=inc) < this.ih) continue;
            if (this.interlace) {
                do {
                    switch (++pass) {
                        case 2: {
                            line = 4;
                            break;
                        }
                        case 3: {
                            line = 2;
                            inc = 4;
                            break;
                        }
                        case 4: {
                            line = 1;
                            inc = 2;
                            break;
                        }
                        default: {
                            line = this.ih - 1;
                            inc = 0;
                        }
                    }
                } while (line >= this.ih);
                continue;
            }
            line = this.ih - 1;
            inc = 0;
        }
        return skipZero;
    }

    protected void setPixel(int x, int y, int v) {
        if (this.m_bpc == 8) {
            int pos = x + this.iw * y;
            this.m_out[pos] = (byte)v;
        } else {
            int pos = this.m_line_stride * y + x / (8 / this.m_bpc);
            int vout = v << 8 - this.m_bpc * (x % (8 / this.m_bpc)) - this.m_bpc;
            byte[] arrby = this.m_out;
            int n = pos;
            arrby[n] = (byte)(arrby[n] | vout);
        }
    }

    protected void resetFrame() {
        boolean transparency = false;
        boolean delay = false;
    }

    protected void readGraphicControlExt() throws IOException {
        this.in.read();
        int packed = this.in.read();
        this.dispose = (packed & 28) >> 2;
        if (this.dispose == 0) {
            this.dispose = 1;
        }
        this.transparency = (packed & 1) != 0;
        this.delay = this.readShort() * 10;
        this.transIndex = this.in.read();
        this.in.read();
    }

    protected void skip() throws IOException {
        do {
            this.readBlock();
        } while (this.blockSize > 0);
    }

    static class GifFrame {
        Image image;
        int ix;
        int iy;

        GifFrame() {
        }
    }

}

