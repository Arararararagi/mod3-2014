/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

import com.lowagie.text.pdf.codec.postscript.PACommand;
import com.lowagie.text.pdf.codec.postscript.PAEngine;
import com.lowagie.text.pdf.codec.postscript.PAParser;
import com.lowagie.text.pdf.codec.postscript.PAPencil;
import com.lowagie.text.pdf.codec.postscript.PAToken;
import com.lowagie.text.pdf.codec.postscript.PainterException;
import com.lowagie.text.pdf.codec.postscript.ParseException;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;

public class PAContext {
    public PAPencil pencil;
    public Stack dictionaries;
    public Stack operands;
    public PAEngine engine;
    PAParser poorscript = null;
    protected Random randomNumberGenerator;
    protected Object lastUnknownIdentifier;

    public PAContext(Component component) {
        this(new PAPencil(component));
    }

    public PAContext(Graphics2D g, Dimension size) {
        this(new PAPencil(g, size));
    }

    public PAContext(PAPencil pencil) {
        this.pencil = pencil;
        this.dictionaries = new Stack();
        this.operands = new Stack();
        this.engine = new PAEngine(this);
        this.dictionaries.push(this.constructSystemDict());
        this.dictionaries.push(this.constructGlobalDict());
        this.dictionaries.push(new HashMap());
        this.randomNumberGenerator = new Random();
        this.lastUnknownIdentifier = null;
    }

    public void draw(InputStream inputStream) throws PainterException {
        try {
            this.poorscript = new PAParser(inputStream);
            this.poorscript.parse(this);
        }
        catch (ParseException e) {
            e.printStackTrace();
            throw new PainterException(e.toString());
        }
    }

    public Object getLastUnknownIdentifier() {
        return this.lastUnknownIdentifier;
    }

    public double[] popNumberOperands(int n) throws PainterException {
        double[] result = new double[n];
        for (int i = n - 1; i >= 0; --i) {
            Object objectValue;
            try {
                objectValue = this.operands.pop();
            }
            catch (EmptyStackException e) {
                throw new PainterException("Operand stack is empty");
            }
            if (!(objectValue instanceof Number)) {
                throw new PainterException("Number expected on operand stack");
            }
            double doubleValue = ((Number)objectValue).doubleValue();
            result[i] = doubleValue;
        }
        return result;
    }

    public Object[] popOperands(int n) throws PainterException {
        Object[] result = new Object[n];
        for (int i = n - 1; i >= 0; --i) {
            Object objectValue;
            try {
                objectValue = this.operands.pop();
            }
            catch (EmptyStackException e) {
                throw new PainterException("Operand stack is empty");
            }
            result[i] = objectValue;
        }
        return result;
    }

    public Object peekOperand() throws PainterException {
        Object objectValue;
        try {
            objectValue = this.operands.peek();
        }
        catch (EmptyStackException e) {
            throw new PainterException("Operand stack is empty");
        }
        return objectValue;
    }

    public Object findIdentifier(Object identifier) {
        Object result = null;
        int n = this.dictionaries.size();
        for (int i = n - 1; i >= 0 && result == null; --i) {
            HashMap dictionary = (HashMap)this.dictionaries.elementAt(i);
            result = dictionary.get(identifier);
        }
        if (result == null) {
            this.lastUnknownIdentifier = identifier;
        }
        return result;
    }

    public Object findDictionary(Object identifier) {
        Object result = null;
        HashMap dictionary = null;
        int n = this.dictionaries.size();
        for (int i = n - 1; i >= 0 && result == null; --i) {
            dictionary = (HashMap)this.dictionaries.elementAt(i);
            result = dictionary.get(identifier);
        }
        if (result == null) {
            return result;
        }
        return dictionary;
    }

    public void collectArray() throws PainterException {
        int j;
        Object objectValue;
        int i;
        boolean found = false;
        int n = this.operands.size();
        for (i = n - 1; i >= 0; --i) {
            objectValue = this.operands.elementAt(i);
            if (!(objectValue instanceof PAToken) || ((PAToken)objectValue).type != 7) continue;
            found = true;
            break;
        }
        if (!found) {
            throw new PainterException("No array was started");
        }
        ArrayList result = new ArrayList(n - i - 1);
        for (j = 0; j < n - i - 1; ++j) {
            result.add(null);
        }
        for (j = n - 1; j > i; --j) {
            try {
                objectValue = this.operands.pop();
            }
            catch (EmptyStackException e) {
                throw new PainterException("Operand stack is empty");
            }
            result.set(j - i - 1, objectValue);
        }
        try {
            this.operands.pop();
        }
        catch (EmptyStackException e) {
            throw new PainterException("Operand stack is empty");
        }
        this.operands.push(result);
    }

    protected HashMap constructGlobalDict() {
        HashMap globalDict = new HashMap();
        return globalDict;
    }

    protected HashMap constructSystemDict() {
        HashMap<String, PACommand> systemDict = new HashMap<String, PACommand>();
        systemDict.put("newpath", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.newpath();
            }
        });
        systemDict.put("moveto", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.pencil.moveto(data[0], data[1]);
            }
        });
        systemDict.put("rmoveto", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.pencil.rmoveto(data[0], data[1]);
            }
        });
        systemDict.put("lineto", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.pencil.lineto(data[0], data[1]);
            }
        });
        systemDict.put("rlineto", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.pencil.rlineto(data[0], data[1]);
            }
        });
        systemDict.put("arc", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(5);
                context.pencil.arc(data[0], data[1], data[2], data[3], data[4]);
            }
        });
        systemDict.put("arcn", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(5);
                context.pencil.arcn(data[0], data[1], data[2], data[3], data[4]);
            }
        });
        systemDict.put("curveto", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(6);
                context.pencil.curveto(data[0], data[1], data[2], data[3], data[4], data[5]);
            }
        });
        systemDict.put("rcurveto", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(6);
                context.pencil.rcurveto(data[0], data[1], data[2], data[3], data[4], data[5]);
            }
        });
        systemDict.put("closepath", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.closepath();
            }
        });
        systemDict.put("gsave", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.gsave();
            }
        });
        systemDict.put("grestore", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.grestore();
            }
        });
        systemDict.put("translate", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                if (context.peekOperand() instanceof Number) {
                    AffineTransform at = new AffineTransform();
                    AffineTransform ctm = context.pencil.graphics.getTransform();
                    double[] data = context.popNumberOperands(2);
                    at.translate(data[0], data[1]);
                    ctm.concatenate(at);
                    context.pencil.graphics.setTransform(ctm);
                } else {
                    Object[] data = context.popOperands(3);
                    if (!(data[0] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[2] instanceof ArrayList)) {
                        throw new PainterException("wrong arguments");
                    }
                    ArrayList array = (ArrayList)data[2];
                    if (array.size() != 6) {
                        throw new PainterException("wrong arguments");
                    }
                    AffineTransform at = new AffineTransform();
                    at.translate(((Number)data[0]).doubleValue(), ((Number)data[1]).doubleValue());
                    double[] entries = new double[6];
                    at.getMatrix(entries);
                    for (int i = 0; i < 6; ++i) {
                        array.set(i, new Double(entries[i]));
                    }
                    context.operands.push(array);
                }
            }
        });
        systemDict.put("rotate", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                if (context.peekOperand() instanceof Number) {
                    AffineTransform at = new AffineTransform();
                    AffineTransform ctm = context.pencil.graphics.getTransform();
                    double[] data = context.popNumberOperands(1);
                    at.rotate(data[0] * 3.141592653589793 / 180.0);
                    ctm.concatenate(at);
                    context.pencil.graphics.setTransform(ctm);
                } else {
                    AffineTransform at = new AffineTransform();
                    Object[] data = context.popOperands(2);
                    if (!(data[0] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[1] instanceof ArrayList)) {
                        throw new PainterException("wrong arguments");
                    }
                    ArrayList array = (ArrayList)data[1];
                    if (array.size() != 6) {
                        throw new PainterException("wrong arguments");
                    }
                    at.rotate(((Number)data[0]).doubleValue());
                    double[] entries = new double[6];
                    at.getMatrix(entries);
                    for (int i = 0; i < 6; ++i) {
                        array.set(i, new Double(entries[i]));
                    }
                    context.operands.push(array);
                }
            }
        });
        systemDict.put("scale", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                if (context.peekOperand() instanceof Number) {
                    AffineTransform at = new AffineTransform();
                    AffineTransform ctm = context.pencil.graphics.getTransform();
                    double[] data = context.popNumberOperands(2);
                    at.scale(data[0], data[1]);
                    ctm.concatenate(at);
                    context.pencil.graphics.setTransform(ctm);
                } else {
                    Object[] data = context.popOperands(3);
                    if (!(data[0] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[2] instanceof ArrayList)) {
                        throw new PainterException("wrong arguments");
                    }
                    ArrayList array = (ArrayList)data[2];
                    double[] entries = new double[6];
                    if (array.size() != 6) {
                        throw new PainterException("wrong arguments");
                    }
                    entries[0] = ((Number)data[0]).doubleValue();
                    entries[1] = 0.0;
                    entries[2] = 0.0;
                    entries[3] = ((Number)data[1]).doubleValue();
                    entries[4] = 0.0;
                    entries[5] = 0.0;
                    for (int i = 0; i < 6; ++i) {
                        array.set(i, new Double(entries[i]));
                    }
                    context.operands.push(array);
                }
            }
        });
        systemDict.put("stroke", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.stroke();
            }
        });
        systemDict.put("fill", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.fill();
            }
        });
        systemDict.put("eofill", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.eofill();
            }
        });
        systemDict.put("show", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                context.pencil.show((String)data[0]);
            }
        });
        systemDict.put("stringwidth", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                Font font = context.pencil.graphics.getFont();
                Rectangle2D rect = font.getStringBounds((String)data[0], context.pencil.graphics.getFontRenderContext());
                context.operands.push(new Float(rect.getWidth()));
                context.operands.push(new Float(rect.getHeight()));
            }
        });
        systemDict.put("showpage", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.showpage();
            }
        });
        systemDict.put("findfont", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[0];
                if (patoken.type != 1) {
                    throw new PainterException("wrong arguments");
                }
                context.operands.push(context.pencil.findFont((String)patoken.value));
            }
        });
        systemDict.put("scalefont", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof Font)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                context.operands.push(((Font)data[0]).deriveFont(((Number)data[1]).floatValue()));
            }
        });
        systemDict.put("setfont", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof Font)) {
                    throw new PainterException("wrong arguments");
                }
                context.pencil.graphics.setFont((Font)data[0]);
            }
        });
        systemDict.put("def", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[0];
                if (patoken.type != 1) {
                    throw new PainterException("wrong arguments");
                }
                try {
                    ((HashMap)context.dictionaries.peek()).put(patoken.value, data[1]);
                }
                catch (EmptyStackException e) {
                    throw new PainterException(e.toString());
                }
            }
        });
        systemDict.put("bind", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[0];
                if (patoken.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                context.engine.bindProcedure(patoken);
                context.operands.push(patoken);
            }
        });
        systemDict.put("mul", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.operands.push(new Double(data[0] * data[1]));
            }
        });
        systemDict.put("div", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.operands.push(new Double(data[0] / data[1]));
            }
        });
        systemDict.put("mod", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                int a = (int)data[0];
                int b = (int)data[1];
                int m = a % b;
                context.operands.push(new Integer(m));
            }
        });
        systemDict.put("add", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.operands.push(new Double(data[0] + data[1]));
            }
        });
        systemDict.put("neg", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new Double(- data[0]));
            }
        });
        systemDict.put("sub", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.operands.push(new Double(data[0] - data[1]));
            }
        });
        systemDict.put("atan", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(2);
                context.operands.push(new Double(Math.atan2(data[0], data[1])));
            }
        });
        systemDict.put("sin", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new Double(Math.sin(data[0] * 3.141592653589793 / 180.0)));
            }
        });
        systemDict.put("cos", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new Double(Math.cos(data[0] * 3.141592653589793 / 180.0)));
            }
        });
        systemDict.put("sqrt", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new Double(Math.sqrt(data[0])));
            }
        });
        systemDict.put("exch", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                context.operands.push(data[1]);
                context.operands.push(data[0]);
            }
        });
        systemDict.put("dup", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                context.operands.push(data[0]);
                context.operands.push(data[0]);
            }
        });
        systemDict.put("roll", ()new PACommand(){

            /*
             * Enabled force condition propagation
             * Lifted jumps to return sites
             */
            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                int numberOfElements = ((Number)data[0]).intValue();
                int numberOfPositions = ((Number)data[1]).intValue();
                if (numberOfPositions == 0 || numberOfElements <= 0) {
                    return;
                }
                Object[] rollData = context.popOperands(numberOfElements);
                if (numberOfPositions < 0) {
                    int i;
                    numberOfPositions = - numberOfPositions;
                    for (i = numberOfPositions%=numberOfElements; i < numberOfElements; ++i) {
                        context.operands.push(rollData[i]);
                    }
                    for (i = 0; i < numberOfPositions; ++i) {
                        context.operands.push(rollData[i]);
                    }
                    return;
                } else {
                    int i;
                    for (i = numberOfElements - (numberOfPositions%=numberOfElements); i < numberOfElements; ++i) {
                        context.operands.push(rollData[i]);
                    }
                    for (i = 0; i < numberOfElements - numberOfPositions; ++i) {
                        context.operands.push(rollData[i]);
                    }
                }
            }
        });
        systemDict.put("pop", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.popOperands(1);
            }
        });
        systemDict.put("index", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                int index = ((Number)data[0]).intValue();
                try {
                    context.operands.push(context.operands.elementAt(index));
                }
                catch (ArrayIndexOutOfBoundsException e) {
                    throw new PainterException(e.toString());
                }
            }
        });
        systemDict.put("mark", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.operands.push(new PAToken(null, 3));
            }
        });
        systemDict.put("cleartomark", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                boolean finished = false;
                while (!finished) {
                    try {
                        Object data = context.operands.pop();
                        if (!(data instanceof PAToken) || ((PAToken)data).type != 3) continue;
                        finished = true;
                        continue;
                    }
                    catch (EmptyStackException e) {
                        throw new PainterException(e.toString());
                    }
                }
            }
        });
        systemDict.put("copy", ()new PACommand(){

            /*
             * Enabled force condition propagation
             * Lifted jumps to return sites
             */
            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (data[0] instanceof PAToken && data[1] instanceof PAToken) {
                    if (((PAToken)data[0]).type != ((PAToken)data[1]).type) throw new PainterException("copy operation failed because composite objects on stack are not of same type");
                    context.operands.push(data[0]);
                    context.operands.push(data[0]);
                    return;
                } else {
                    int i;
                    context.operands.push(data[0]);
                    if (!(data[1] instanceof Number)) throw new PainterException("I expect a number on stack, dude");
                    int index = ((Number)data[1]).intValue();
                    int n = context.operands.size();
                    Object[] copyData = new Object[index];
                    for (i = n - index; i < n; ++i) {
                        try {
                            copyData[i - n + index] = context.operands.elementAt(i);
                            continue;
                        }
                        catch (ArrayIndexOutOfBoundsException e) {
                            throw new PainterException(e.toString());
                        }
                    }
                    for (i = 0; i < index; ++i) {
                        context.operands.push(copyData[i]);
                    }
                }
            }
        });
        systemDict.put("setgray", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.pencil.graphics.setPaint(new Color((float)data[0], (float)data[0], (float)data[0]));
            }
        });
        systemDict.put("setrgbcolor", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(3);
                float[] fv = new float[]{(float)Math.max(Math.min(data[0], 1.0), 0.0), (float)Math.max(Math.min(data[1], 1.0), 0.0), (float)Math.max(Math.min(data[2], 1.0), 0.0)};
                context.pencil.graphics.setPaint(new Color(fv[0], fv[1], fv[2]));
            }
        });
        systemDict.put("sethsbcolor", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(3);
                float[] fv = new float[]{(float)Math.max(Math.min(data[0], 1.0), 0.0), (float)Math.max(Math.min(data[1], 1.0), 0.0), (float)Math.max(Math.min(data[2], 1.0), 0.0)};
                context.pencil.graphics.setPaint(new Color(fv[0], fv[1], fv[2]));
            }
        });
        systemDict.put("setcmybcolor", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(4);
                float[] fv = new float[]{(float)data[0], (float)data[1], (float)data[2], (float)data[3]};
                int rd = (int)(255.0f * Math.max(0.0f, 1.0f - fv[0] - fv[3]));
                int gr = (int)(255.0f * Math.max(0.0f, 1.0f - fv[1] - fv[3]));
                int bl = (int)(255.0f * Math.max(0.0f, 1.0f - fv[2] - fv[3]));
                context.pencil.graphics.setPaint(new Color(rd, gr, bl));
            }
        });
        systemDict.put("setlinewidth", ()new PACommand(){

            private double minLineWidth(double w, AffineTransform at) {
                double[] matrix = new double[4];
                at.getMatrix(matrix);
                double scale = matrix[0] * matrix[3] - matrix[1] * matrix[2];
                double minlw = 0.25 / Math.sqrt(Math.abs(scale));
                if (w < minlw) {
                    w = minlw;
                }
                return w;
            }

            public void execute(PAContext context) throws PainterException {
                Stroke oldStroke = context.pencil.graphics.getStroke();
                double[] data = context.popNumberOperands(1);
                data[0] = this.minLineWidth(data[0], context.pencil.graphics.getTransform());
                BasicStroke newStroke = oldStroke instanceof BasicStroke ? new BasicStroke((float)data[0], ((BasicStroke)oldStroke).getEndCap(), ((BasicStroke)oldStroke).getLineJoin(), ((BasicStroke)oldStroke).getMiterLimit(), ((BasicStroke)oldStroke).getDashArray(), ((BasicStroke)oldStroke).getDashPhase()) : new BasicStroke((float)data[0], 1, 1);
                context.pencil.graphics.setStroke(newStroke);
            }
        });
        systemDict.put("setlinecap", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Stroke oldStroke = context.pencil.graphics.getStroke();
                double[] data = context.popNumberOperands(1);
                BasicStroke newStroke = oldStroke instanceof BasicStroke ? new BasicStroke(((BasicStroke)oldStroke).getLineWidth(), (int)data[0], ((BasicStroke)oldStroke).getLineJoin(), ((BasicStroke)oldStroke).getMiterLimit(), ((BasicStroke)oldStroke).getDashArray(), ((BasicStroke)oldStroke).getDashPhase()) : new BasicStroke(1.0f, (int)data[0], 1);
                context.pencil.graphics.setStroke(newStroke);
            }
        });
        systemDict.put("setmiterlimit", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Stroke oldStroke = context.pencil.graphics.getStroke();
                double[] data = context.popNumberOperands(1);
                BasicStroke newStroke = oldStroke instanceof BasicStroke ? new BasicStroke(((BasicStroke)oldStroke).getLineWidth(), ((BasicStroke)oldStroke).getEndCap(), ((BasicStroke)oldStroke).getLineJoin(), (float)data[0], ((BasicStroke)oldStroke).getDashArray(), ((BasicStroke)oldStroke).getDashPhase()) : new BasicStroke(1.0f, 1, 1, (float)data[0]);
                context.pencil.graphics.setStroke(newStroke);
            }
        });
        systemDict.put("setdash", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Stroke oldStroke = context.pencil.graphics.getStroke();
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof ArrayList)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                ArrayList list = (ArrayList)data[0];
                if (list.size() == 0) {
                    return;
                }
                float[] dashpattern = new float[list.size()];
                for (int i = 0; i < dashpattern.length; ++i) {
                    dashpattern[i] = ((Number)list.get(i)).floatValue();
                }
                float dashoffset = ((Number)data[1]).floatValue();
                BasicStroke newStroke = oldStroke instanceof BasicStroke ? new BasicStroke(((BasicStroke)oldStroke).getLineWidth(), ((BasicStroke)oldStroke).getEndCap(), ((BasicStroke)oldStroke).getLineJoin(), ((BasicStroke)oldStroke).getMiterLimit(), dashpattern, dashoffset) : new BasicStroke(1.0f, 1, 1, 1.0f, dashpattern, dashoffset);
                context.pencil.graphics.setStroke(newStroke);
            }
        });
        systemDict.put("setlinejoin", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Stroke oldStroke = context.pencil.graphics.getStroke();
                double[] data = context.popNumberOperands(1);
                BasicStroke newStroke = oldStroke instanceof BasicStroke ? new BasicStroke(((BasicStroke)oldStroke).getLineWidth(), ((BasicStroke)oldStroke).getEndCap(), (int)data[0], ((BasicStroke)oldStroke).getMiterLimit(), ((BasicStroke)oldStroke).getDashArray(), ((BasicStroke)oldStroke).getDashPhase()) : new BasicStroke(1.0f, 1, (int)data[0]);
                context.pencil.graphics.setStroke(newStroke);
            }
        });
        systemDict.put("dumpstack", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Enumeration enumx = context.operands.elements();
                System.out.println("-------------Stack--------------");
                while (enumx.hasMoreElements()) {
                    System.out.println(enumx.nextElement());
                }
                System.out.println("--------------------------------");
            }
        });
        systemDict.put("for", ()new PACommand(){

            /*
             * Enabled force condition propagation
             * Lifted jumps to return sites
             */
            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(4);
                if (!(data[3] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[0] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[2] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[3];
                if (patoken.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                int i0 = ((Number)data[0]).intValue();
                int i1 = ((Number)data[1]).intValue();
                int i2 = ((Number)data[2]).intValue();
                if (i1 > 0) {
                    for (int i = i0; i <= i2; i+=i1) {
                        context.operands.push(new Integer(i));
                        context.engine.process(patoken);
                    }
                    return;
                } else {
                    for (int i = i0; i >= i2; i-=i1) {
                        context.operands.push(new Integer(i));
                        context.engine.process(patoken);
                    }
                }
            }
        });
        systemDict.put("repeat", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[1] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[0] instanceof Number)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[1];
                if (patoken.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                int n = ((Number)data[0]).intValue();
                for (int i = 0; i < n; ++i) {
                    context.engine.process(patoken);
                }
            }
        });
        systemDict.put("true", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.operands.push(new Boolean(true));
            }
        });
        systemDict.put("false", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.operands.push(new Boolean(false));
            }
        });
        systemDict.put("lt", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof Number || data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                if (data[0] instanceof Number) {
                    double d1;
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    double d0 = ((Number)data[0]).doubleValue();
                    if (d0 < (d1 = ((Number)data[1]).doubleValue())) {
                        context.operands.push(new Boolean(true));
                    } else {
                        context.operands.push(new Boolean(false));
                    }
                } else {
                    if (!(data[1] instanceof String)) {
                        throw new PainterException("wrong arguments");
                    }
                    String s0 = (String)data[0];
                    String s1 = (String)data[1];
                    if (s0.compareTo(s1) < 0) {
                        context.operands.push(new Boolean(true));
                    } else {
                        context.operands.push(new Boolean(false));
                    }
                }
            }
        });
        systemDict.put("gt", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof Number || data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                if (data[0] instanceof Number) {
                    double d1;
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    double d0 = ((Number)data[0]).doubleValue();
                    if (d0 > (d1 = ((Number)data[1]).doubleValue())) {
                        context.operands.push(new Boolean(true));
                    } else {
                        context.operands.push(new Boolean(false));
                    }
                } else {
                    if (!(data[1] instanceof String)) {
                        throw new PainterException("wrong arguments");
                    }
                    String s0 = (String)data[0];
                    String s1 = (String)data[1];
                    if (s0.compareTo(s1) > 0) {
                        context.operands.push(new Boolean(true));
                    } else {
                        context.operands.push(new Boolean(false));
                    }
                }
            }
        });
        systemDict.put("ne", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof Number || data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                if (data[0] instanceof Number) {
                    double d1;
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    double d0 = ((Number)data[0]).doubleValue();
                    if (d0 != (d1 = ((Number)data[1]).doubleValue())) {
                        context.operands.push(new Boolean(true));
                    } else {
                        context.operands.push(new Boolean(false));
                    }
                } else {
                    if (!(data[1] instanceof String)) {
                        throw new PainterException("wrong arguments");
                    }
                    String s0 = (String)data[0];
                    String s1 = (String)data[1];
                    if (s0.equals(s1)) {
                        context.operands.push(new Boolean(false));
                    } else {
                        context.operands.push(new Boolean(true));
                    }
                }
            }
        });
        systemDict.put("eq", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof Number || data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                if (data[0] instanceof Number) {
                    double d1;
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    double d0 = ((Number)data[0]).doubleValue();
                    if (d0 == (d1 = ((Number)data[1]).doubleValue())) {
                        context.operands.push(new Boolean(true));
                    } else {
                        context.operands.push(new Boolean(false));
                    }
                } else {
                    if (!(data[1] instanceof String)) {
                        throw new PainterException("wrong arguments");
                    }
                    String s0 = (String)data[0];
                    String s1 = (String)data[1];
                    if (s0.compareTo(s1) == 0) {
                        context.operands.push(new Boolean(true));
                    } else {
                        context.operands.push(new Boolean(false));
                    }
                }
            }
        });
        systemDict.put("if", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof Boolean)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[1];
                if (patoken.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                if (((Boolean)data[0]).booleanValue()) {
                    context.engine.process(patoken);
                }
            }
        });
        systemDict.put("ifelse", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(3);
                if (!(data[0] instanceof Boolean)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[2] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken1 = (PAToken)data[1];
                PAToken patoken2 = (PAToken)data[2];
                if (patoken1.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                if (patoken2.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                if (((Boolean)data[0]).booleanValue()) {
                    context.engine.process(patoken1);
                } else {
                    context.engine.process(patoken2);
                }
            }
        });
        systemDict.put("dict", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new HashMap());
            }
        });
        systemDict.put("userdict", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new HashMap());
            }
        });
        systemDict.put("put", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(3);
                if (!(data[0] instanceof HashMap)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[1];
                if (patoken.type != 1) {
                    throw new PainterException("wrong arguments");
                }
                ((HashMap)data[0]).put(patoken.value, data[2]);
            }
        });
        systemDict.put("get", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof HashMap || data[0] instanceof ArrayList)) {
                    throw new PainterException("wrong arguments");
                }
                if (data[0] instanceof HashMap) {
                    if (!(data[1] instanceof PAToken)) {
                        throw new PainterException("wrong arguments");
                    }
                    PAToken patoken = (PAToken)data[1];
                    if (patoken.type != 1) {
                        throw new PainterException("wrong arguments");
                    }
                    context.operands.push(((HashMap)data[0]).get(patoken.value));
                } else if (data[0] instanceof ArrayList) {
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    context.operands.push(((ArrayList)data[0]).get(((Number)data[1]).intValue()));
                }
            }
        });
        systemDict.put("load", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[0];
                if (patoken.type != 1) {
                    throw new PainterException("wrong arguments");
                }
                context.operands.push(context.findIdentifier(patoken.value));
            }
        });
        systemDict.put("length", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                int size = 0;
                Object[] data = context.popOperands(1);
                if (data[0] instanceof PAToken) {
                    PAToken patoken = (PAToken)data[0];
                    if (patoken.type != 1) {
                        throw new PainterException("wrong arguments");
                    }
                    size = ((String)patoken.value).length();
                } else if (data[0] instanceof HashMap) {
                    size = ((HashMap)data[0]).size();
                } else if (data[0] instanceof ArrayList) {
                    size = ((ArrayList)data[0]).size();
                } else if (data[0] instanceof String) {
                    size = ((String)data[0]).length();
                } else {
                    throw new PainterException("wrong arguments");
                }
                context.operands.push(new Integer(size));
            }
        });
        systemDict.put("begin", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof HashMap)) {
                    throw new PainterException("wrong arguments");
                }
                context.dictionaries.push(data[0]);
            }
        });
        systemDict.put("end", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                try {
                    context.dictionaries.pop();
                }
                catch (EmptyStackException e) {
                    throw new PainterException("Dictionary stack is empty");
                }
            }
        });
        systemDict.put("undef", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof HashMap)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[1];
                if (patoken.type != 1) {
                    throw new PainterException("wrong arguments");
                }
            }
        });
        systemDict.put("known", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[0];
                if (patoken.type != 1) {
                    throw new PainterException("wrong arguments");
                }
                Object foundObject = context.findIdentifier(patoken.value);
                if (foundObject != null) {
                    context.operands.push(new Boolean(true));
                } else {
                    context.operands.push(new Boolean(false));
                }
            }
        });
        systemDict.put("where", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[0];
                if (patoken.type != 1) {
                    throw new PainterException("wrong arguments");
                }
                Object foundObject = context.findDictionary(patoken.value);
                if (foundObject != null) {
                    context.operands.push(foundObject);
                    context.operands.push(new Boolean(true));
                } else {
                    context.operands.push(new Boolean(false));
                }
            }
        });
        systemDict.put("aload", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof ArrayList)) {
                    throw new PainterException("wrong arguments");
                }
                ArrayList list = (ArrayList)data[0];
                Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    context.operands.push(iterator.next());
                }
                context.operands.push(data[0]);
            }
        });
        systemDict.put("forall", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof ArrayList)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[1];
                if (patoken.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                ArrayList list = (ArrayList)data[0];
                Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    context.operands.push(iterator.next());
                    context.engine.process(patoken);
                }
            }
        });
        systemDict.put("currentflat", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.operands.push(new Float(1.0f));
            }
        });
        systemDict.put("setflat", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
            }
        });
        systemDict.put("round", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new Long(Math.round(data[0])));
            }
        });
        systemDict.put("abs", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                context.operands.push(new Double(Math.abs(data[0])));
            }
        });
        systemDict.put("transform", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                if (context.peekOperand() instanceof Number) {
                    double[] transformedData = new double[2];
                    double[] data = context.popNumberOperands(2);
                    AffineTransform at = context.pencil.graphics.getTransform();
                    at.transform(data, 0, transformedData, 0, 1);
                    context.operands.push(new Double(transformedData[0]));
                    context.operands.push(new Double(transformedData[1]));
                } else {
                    Object[] data = context.popOperands(3);
                    if (!(data[0] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[2] instanceof ArrayList)) {
                        throw new PainterException("wrong arguments");
                    }
                    ArrayList array = (ArrayList)data[2];
                    double[] entries = new double[6];
                    if (array.size() != 6) {
                        throw new PainterException("wrong arguments");
                    }
                    for (int i = 0; i < 6; ++i) {
                        entries[i] = ((Number)array.get(i)).doubleValue();
                    }
                    AffineTransform at = new AffineTransform(entries);
                    double[] numberdata = new double[]{((Number)data[0]).doubleValue(), ((Number)data[0]).doubleValue()};
                    double[] transformedData = new double[2];
                    at.transform(numberdata, 0, transformedData, 0, 1);
                    context.operands.push(new Double(transformedData[0]));
                    context.operands.push(new Double(transformedData[1]));
                }
            }
        });
        systemDict.put("itransform", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                if (context.peekOperand() instanceof Number) {
                    double[] transformedData = new double[2];
                    double[] data = context.popNumberOperands(2);
                    AffineTransform at = context.pencil.graphics.getTransform();
                    try {
                        at.inverseTransform(data, 0, transformedData, 0, 1);
                    }
                    catch (NoninvertibleTransformException e) {
                        throw new PainterException(e.toString());
                    }
                    context.operands.push(new Double(transformedData[0]));
                    context.operands.push(new Double(transformedData[1]));
                } else {
                    Object[] data = context.popOperands(3);
                    if (!(data[0] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[1] instanceof Number)) {
                        throw new PainterException("wrong arguments");
                    }
                    if (!(data[2] instanceof ArrayList)) {
                        throw new PainterException("wrong arguments");
                    }
                    ArrayList array = (ArrayList)data[2];
                    double[] entries = new double[6];
                    if (array.size() != 6) {
                        throw new PainterException("wrong arguments");
                    }
                    for (int i = 0; i < 6; ++i) {
                        entries[i] = ((Number)array.get(i)).doubleValue();
                    }
                    AffineTransform at = new AffineTransform(entries);
                    double[] numberdata = new double[]{((Number)data[0]).doubleValue(), ((Number)data[0]).doubleValue()};
                    double[] transformedData = new double[2];
                    try {
                        at.inverseTransform(numberdata, 0, transformedData, 0, 1);
                    }
                    catch (NoninvertibleTransformException e) {
                        throw new PainterException(e.toString());
                    }
                    context.operands.push(new Double(transformedData[0]));
                    context.operands.push(new Double(transformedData[1]));
                }
            }
        });
        systemDict.put("currentpoint", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Point2D currentPoint = context.pencil.state.path.getCurrentPoint();
                context.operands.push(new Double(currentPoint.getX()));
                context.operands.push(new Double(currentPoint.getY()));
            }
        });
        systemDict.put("clippath", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.pencil.clippath();
            }
        });
        systemDict.put("matrix", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                ArrayList<Double> identityMatrix = new ArrayList<Double>(6);
                identityMatrix.add(new Double(1.0));
                identityMatrix.add(new Double(0.0));
                identityMatrix.add(new Double(0.0));
                identityMatrix.add(new Double(1.0));
                identityMatrix.add(new Double(0.0));
                identityMatrix.add(new Double(0.0));
                context.operands.push(identityMatrix);
            }
        });
        systemDict.put("concatmatrix", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                int i;
                Object[] data = context.popOperands(3);
                if (!(data[0] instanceof ArrayList)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof ArrayList)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[2] instanceof ArrayList)) {
                    throw new PainterException("wrong arguments");
                }
                ArrayList arrayOne = (ArrayList)data[0];
                ArrayList arrayTwo = (ArrayList)data[1];
                ArrayList arrayThree = (ArrayList)data[2];
                double[] entries = new double[6];
                if (arrayOne.size() != 6) {
                    throw new PainterException("wrong arguments");
                }
                if (arrayTwo.size() != 6) {
                    throw new PainterException("wrong arguments");
                }
                if (arrayThree.size() != 6) {
                    throw new PainterException("wrong arguments");
                }
                for (i = 0; i < 6; ++i) {
                    entries[i] = ((Number)arrayOne.get(i)).doubleValue();
                }
                AffineTransform atOne = new AffineTransform(entries);
                for (i = 0; i < 6; ++i) {
                    entries[i] = ((Number)arrayTwo.get(i)).doubleValue();
                }
                AffineTransform atTwo = new AffineTransform(entries);
                atOne.concatenate(atTwo);
                atOne.getMatrix(entries);
                for (i = 0; i < 6; ++i) {
                    arrayThree.set(i, new Double(entries[i]));
                }
                context.operands.push(arrayThree);
            }
        });
        systemDict.put("pathbbox", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Rectangle2D pathBounds = context.pencil.state.path.getBounds2D();
                context.operands.push(new Double(pathBounds.getMinX()));
                context.operands.push(new Double(pathBounds.getMinY()));
                context.operands.push(new Double(pathBounds.getMaxX()));
                context.operands.push(new Double(pathBounds.getMaxY()));
            }
        });
        systemDict.put("truncate", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                double truncated = data[0] < 0.0 ? Math.ceil(data[0]) : Math.floor(data[0]);
                context.operands.push(new Double(truncated));
            }
        });
        systemDict.put("rand", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.operands.push(new Integer(PAContext.this.randomNumberGenerator.nextInt(231)));
            }
        });
        systemDict.put("srand", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                double[] data = context.popNumberOperands(1);
                PAContext.this.randomNumberGenerator = new Random(Math.round(data[0]));
            }
        });
        systemDict.put("cvi", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof Number || data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                if (data[0] instanceof Number) {
                    int d = ((Number)data[0]).intValue();
                    context.operands.push(new Integer(d));
                } else {
                    String s = (String)data[0];
                    context.operands.push(new Integer(s));
                }
            }
        });
        systemDict.put("usertime", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                context.operands.push(new Long(System.currentTimeMillis()));
            }
        });
        systemDict.put("save", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
            }
        });
        systemDict.put("restore", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
            }
        });
        systemDict.put("clear", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
            }
        });
        systemDict.put("cleardictstack", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
            }
        });
        systemDict.put("charpath", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(2);
                if (!(data[0] instanceof String)) {
                    throw new PainterException("wrong arguments");
                }
                if (!(data[1] instanceof Boolean)) {
                    throw new PainterException("wrong arguments");
                }
                context.pencil.charpath((String)data[0], (Boolean)data[1]);
            }
        });
        systemDict.put("stopped", ()new PACommand(){

            public void execute(PAContext context) throws PainterException {
                Object[] data = context.popOperands(1);
                if (!(data[0] instanceof PAToken)) {
                    throw new PainterException("wrong arguments");
                }
                PAToken patoken = (PAToken)data[0];
                if (patoken.type != 2) {
                    throw new PainterException("wrong arguments");
                }
                context.engine.process(patoken);
                context.operands.push(new Boolean(false));
            }
        });
        return systemDict;
    }

}

