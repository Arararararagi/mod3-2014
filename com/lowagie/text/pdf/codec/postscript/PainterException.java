/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

public class PainterException
extends Exception {
    public PainterException(String msg) {
        super(msg);
    }
}

