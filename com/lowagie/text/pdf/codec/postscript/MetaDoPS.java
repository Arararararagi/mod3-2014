/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.codec.postscript.PAContext;
import com.lowagie.text.pdf.codec.postscript.PainterException;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MetaDoPS {
    public PdfContentByte cb;
    InputStream in;
    int left;
    int top;
    int right;
    int bottom;
    int inch;

    public MetaDoPS(InputStream in, PdfContentByte cb) {
        this.cb = cb;
        this.in = in;
    }

    public void readAll() throws IOException, DocumentException {
        this.cb.saveState();
        Graphics2D g2 = this.cb.createGraphicsShapes(PageSize.A4.width(), PageSize.A4.height());
        try {
            PAContext context = new PAContext(g2, new Dimension((int)PageSize.A4.width(), (int)PageSize.A4.height()));
            context.draw(new BufferedInputStream(this.in));
            this.in.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (PainterException ex) {
            ex.printStackTrace();
        }
        this.cb.restoreState();
    }
}

