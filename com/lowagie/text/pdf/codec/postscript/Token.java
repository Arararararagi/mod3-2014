/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

public class Token {
    public int kind;
    public int beginLine;
    public int beginColumn;
    public int endLine;
    public int endColumn;
    public String image;
    public Token next;
    public Token specialToken;

    public final String toString() {
        return this.image;
    }

    public static final Token newToken(int ofKind) {
        return new Token();
    }
}

