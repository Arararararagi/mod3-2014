/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

import com.lowagie.text.pdf.codec.postscript.JavaCharStream;
import com.lowagie.text.pdf.codec.postscript.PAContext;
import com.lowagie.text.pdf.codec.postscript.PAEngine;
import com.lowagie.text.pdf.codec.postscript.PAParserConstants;
import com.lowagie.text.pdf.codec.postscript.PAParserTokenManager;
import com.lowagie.text.pdf.codec.postscript.PAToken;
import com.lowagie.text.pdf.codec.postscript.PainterException;
import com.lowagie.text.pdf.codec.postscript.ParseException;
import com.lowagie.text.pdf.codec.postscript.Token;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.util.Vector;

public class PAParser
implements PAParserConstants {
    public PAParserTokenManager token_source;
    JavaCharStream jj_input_stream;
    public Token token;
    public Token jj_nt;
    private int jj_ntk;
    private int jj_gen;
    private final int[] jj_la1 = new int[3];
    private final int[] jj_la1_0 = new int[]{499344, 7824, 499344};
    private Vector jj_expentries = new Vector();
    private int[] jj_expentry;
    private int jj_kind = -1;

    void error_skipto(int kind) throws ParseException {
        ParseException e = this.generateParseException();
        String dump = "";
        while (this.getToken((int)1).kind != kind) {
            Token t = this.getNextToken();
            dump = String.valueOf(dump) + t.image;
            if (t.kind != kind) continue;
        }
        System.out.println("Ignoriere >" + dump + "<");
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public final void parse(PAContext context) throws ParseException {
        x = null;
        try {
            block42 : do {
                switch (this.jj_ntk == -1 ? this.jj_ntk() : this.jj_ntk) {
                    case 4: 
                    case 7: 
                    case 9: 
                    case 10: 
                    case 11: 
                    case 12: 
                    case 15: 
                    case 16: 
                    case 17: 
                    case 18: {
                        ** break;
                    }
                }
                this.jj_la1[0] = this.jj_gen;
                return;
lbl9: // 1 sources:
                switch (this.jj_ntk == -1 ? this.jj_ntk() : this.jj_ntk) {
                    case 4: 
                    case 7: 
                    case 9: 
                    case 10: 
                    case 11: 
                    case 12: {
                        switch (this.jj_ntk == -1 ? this.jj_ntk() : this.jj_ntk) {
                            case 4: {
                                x = this.jj_consume_token(4);
                                try {
                                    context.engine.process(new Integer(x.image));
                                    continue block42;
                                }
                                catch (NumberFormatException e) {
                                    throw new ParseException(e.toString());
                                }
                                catch (PainterException e) {
                                    throw new ParseException(e.toString());
                                }
                            }
                            case 7: {
                                x = this.jj_consume_token(7);
                                try {
                                    context.engine.process(new Double(x.image));
                                    continue block42;
                                }
                                catch (NumberFormatException e) {
                                    throw new ParseException(e.toString());
                                }
                                catch (PainterException e) {
                                    throw new ParseException(e.toString());
                                }
                            }
                            case 9: {
                                x = this.jj_consume_token(9);
                                try {
                                    context.engine.process(x.image.substring(1, x.image.length() - 1));
                                    continue block42;
                                }
                                catch (PainterException e) {
                                    throw new ParseException(e.toString());
                                }
                            }
                            case 10: {
                                x = this.jj_consume_token(10);
                                try {
                                    context.engine.process(new PAToken(x.image, 0));
                                    continue block42;
                                }
                                catch (PainterException e) {
                                    throw new ParseException(e.toString());
                                }
                            }
                            case 11: {
                                x = this.jj_consume_token(11);
                                try {
                                    context.engine.process(new PAToken(x.image.substring(1, x.image.length()), 1));
                                    continue block42;
                                }
                                catch (PainterException e) {
                                    throw new ParseException(e.toString());
                                }
                            }
                            case 12: {
                                x = this.jj_consume_token(12);
                                try {
                                    context.engine.process(new PAToken(x.image.substring(2, x.image.length()), 6));
                                    continue block42;
                                }
                                catch (PainterException e) {
                                    throw new ParseException(e.toString());
                                }
                            }
                        }
                        this.jj_la1[1] = this.jj_gen;
                        this.jj_consume_token(-1);
                        throw new ParseException();
                    }
                    case 15: {
                        this.jj_consume_token(15);
                        try {
                            context.engine.process(new PAToken(null, 4));
                            continue block42;
                        }
                        catch (PainterException e) {
                            throw new ParseException(e.toString());
                        }
                    }
                    case 16: {
                        this.jj_consume_token(16);
                        try {
                            context.engine.process(new PAToken(null, 5));
                            continue block42;
                        }
                        catch (PainterException e) {
                            throw new ParseException(e.toString());
                        }
                    }
                    case 17: {
                        this.jj_consume_token(17);
                        try {
                            context.engine.process(new PAToken(null, 7));
                            continue block42;
                        }
                        catch (PainterException e) {
                            throw new ParseException(e.toString());
                        }
                    }
                    case 18: {
                        this.jj_consume_token(18);
                        try {
                            context.engine.process(new PAToken(null, 8));
                            continue block42;
                        }
                        catch (PainterException e) {
                            throw new ParseException(e.toString());
                        }
                    }
                    continue block42;
                }
                break;
            } while (true);
            this.jj_la1[2] = this.jj_gen;
            this.jj_consume_token(-1);
            throw new ParseException();
        }
        catch (ParseException e) {
            e.printStackTrace();
            this.error_skipto(1);
        }
    }

    public PAParser(InputStream stream) {
        this.jj_input_stream = new JavaCharStream(stream, 1, 1);
        this.token_source = new PAParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(InputStream stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }

    public PAParser(Reader stream) {
        this.jj_input_stream = new JavaCharStream(stream, 1, 1);
        this.token_source = new PAParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(Reader stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }

    public PAParser(PAParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(PAParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        this.token = oldToken.next != null ? this.token.next : (this.token.next = this.token_source.getNextToken());
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            ++this.jj_gen;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw this.generateParseException();
    }

    public final Token getNextToken() {
        this.token = this.token.next != null ? this.token.next : (this.token.next = this.token_source.getNextToken());
        this.jj_ntk = -1;
        ++this.jj_gen;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t = this.token;
        for (int i = 0; i < index; ++i) {
            t = t.next != null ? t.next : (t.next = this.token_source.getNextToken());
        }
        return t;
    }

    private final int jj_ntk() {
        this.jj_nt = this.token.next;
        if (this.jj_nt == null) {
            this.token.next = this.token_source.getNextToken();
            this.jj_ntk = this.token.next.kind;
            return this.jj_ntk;
        }
        this.jj_ntk = this.jj_nt.kind;
        return this.jj_ntk;
    }

    public final ParseException generateParseException() {
        int i;
        this.jj_expentries.removeAllElements();
        boolean[] la1tokens = new boolean[19];
        for (i = 0; i < 19; ++i) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (i = 0; i < 3; ++i) {
            if (this.jj_la1[i] != this.jj_gen) continue;
            for (int j = 0; j < 32; ++j) {
                if ((this.jj_la1_0[i] & 1 << j) == 0) continue;
                la1tokens[j] = true;
            }
        }
        for (i = 0; i < 19; ++i) {
            if (!la1tokens[i]) continue;
            this.jj_expentry = new int[1];
            this.jj_expentry[0] = i;
            this.jj_expentries.addElement(this.jj_expentry);
        }
        int[][] exptokseq = new int[this.jj_expentries.size()][];
        for (int i2 = 0; i2 < this.jj_expentries.size(); ++i2) {
            exptokseq[i2] = (int[])this.jj_expentries.elementAt(i2);
        }
        return new ParseException(this.token, exptokseq, PAParserConstants.tokenImage);
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }
}

