/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

public interface PAParserConstants {
    public static final int EOF = 0;
    public static final int WHITESPACE = 1;
    public static final int INTEGER_LITERAL = 4;
    public static final int DECIMAL_LITERAL = 5;
    public static final int HEX_LITERAL = 6;
    public static final int FLOATING_POINT_LITERAL = 7;
    public static final int EXPONENT = 8;
    public static final int STRING_LITERAL = 9;
    public static final int IDENTIFIER = 10;
    public static final int KEY_IDENTIFIER = 11;
    public static final int IMMEDIATE_IDENTIFIER = 12;
    public static final int LETTER = 13;
    public static final int DIGIT = 14;
    public static final int LBRACE = 15;
    public static final int RBRACE = 16;
    public static final int LBRACKET = 17;
    public static final int RBRACKET = 18;
    public static final int DEFAULT = 0;
    public static final String[] tokenImage = new String[]{"<EOF>", "<WHITESPACE>", "<token of kind 2>", "<token of kind 3>", "<INTEGER_LITERAL>", "<DECIMAL_LITERAL>", "<HEX_LITERAL>", "<FLOATING_POINT_LITERAL>", "<EXPONENT>", "<STRING_LITERAL>", "<IDENTIFIER>", "<KEY_IDENTIFIER>", "<IMMEDIATE_IDENTIFIER>", "<LETTER>", "<DIGIT>", "\"{\"", "\"}\"", "\"[\"", "\"]\""};
}

