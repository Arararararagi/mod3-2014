/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

import com.lowagie.text.pdf.codec.postscript.PACommand;
import com.lowagie.text.pdf.codec.postscript.PAContext;
import com.lowagie.text.pdf.codec.postscript.PAToken;
import com.lowagie.text.pdf.codec.postscript.PainterException;
import java.util.Enumeration;
import java.util.Stack;
import java.util.Vector;

public class PAEngine {
    public static final int MODE_STACK = 0;
    public static final int MODE_PROCEDURE = 1;
    public static final int MODE_ARRAY = 2;
    protected PAContext context;
    protected int mode;
    protected Stack procedure;
    protected int innerProcedures;

    public PAEngine(PAContext context) {
        this.context = context;
        this.mode = 0;
    }

    public void startProcedure() throws PainterException {
        this.procedure = new Stack();
        this.mode = 1;
        this.innerProcedures = 0;
    }

    public void endProcedure() throws PainterException {
        this.context.operands.push(new PAToken(this.procedure, 2));
        this.mode = 0;
    }

    public void bindProcedure(PAToken patoken) {
        Stack oldStack = (Stack)patoken.value;
        Stack<Object> newStack = new Stack<Object>();
        int n = oldStack.size();
        for (int i = 0; i < n; ++i) {
            Object token = oldStack.elementAt(i);
            if (token instanceof PAToken && ((PAToken)token).type == 0) {
                Object foundToken = this.context.findIdentifier(((PAToken)token).value);
                if (foundToken == null) {
                    newStack.push(token);
                    continue;
                }
                newStack.push(foundToken);
                continue;
            }
            newStack.push(token);
        }
        patoken.value = newStack;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public void process(Object token) throws PainterException {
        if (token == null) {
            throw new IllegalStateException("Null token encountered; last unknown identifier was " + this.context.getLastUnknownIdentifier());
        }
        if (token instanceof PAToken && ((PAToken)token).type == 6) {
            Object foundValue = this.context.findIdentifier(((PAToken)token).value);
            this.process(foundValue);
            return;
        }
        if (this.mode == 0) {
            if (token instanceof PACommand) {
                ((PACommand)token).execute(this.context);
                return;
            }
            if (token instanceof PAToken) {
                PAToken patoken = (PAToken)token;
                switch (patoken.type) {
                    case 0: {
                        this.process(this.context.findIdentifier(patoken.value));
                        return;
                    }
                    case 1: 
                    case 3: 
                    case 7: {
                        this.context.operands.push(token);
                        return;
                    }
                    case 2: {
                        Enumeration enumx = ((Vector)patoken.value).elements();
                        while (enumx.hasMoreElements()) {
                            this.process(enumx.nextElement());
                        }
                        return;
                    }
                    case 4: {
                        this.startProcedure();
                        return;
                    }
                    case 8: {
                        this.context.collectArray();
                        return;
                    }
                }
                throw new IllegalStateException("Unknown token encountered" + token);
            }
            this.context.operands.push(token);
            return;
        }
        if (this.mode != 1) return;
        if (token instanceof PAToken) {
            PAToken patoken = (PAToken)token;
            switch (patoken.type) {
                case 4: {
                    ++this.innerProcedures;
                    this.procedure.push(token);
                    return;
                }
                case 5: {
                    if (this.innerProcedures > 0) {
                        --this.innerProcedures;
                        this.procedure.push(token);
                        return;
                    }
                    this.endProcedure();
                    return;
                }
                default: {
                    this.procedure.push(token);
                    return;
                }
            }
        } else {
            this.procedure.push(token);
        }
    }
}

