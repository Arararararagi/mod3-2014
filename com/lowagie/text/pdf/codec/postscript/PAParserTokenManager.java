/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

import com.lowagie.text.pdf.codec.postscript.JavaCharStream;
import com.lowagie.text.pdf.codec.postscript.PAParserConstants;
import com.lowagie.text.pdf.codec.postscript.Token;
import com.lowagie.text.pdf.codec.postscript.TokenMgrError;
import java.io.IOException;
import java.io.PrintStream;

public class PAParserTokenManager
implements PAParserConstants {
    public PrintStream debugStream = System.out;
    static final long[] jjbitVec0 = new long[]{-2, -1, -1, -1};
    static final long[] jjbitVec2;
    static final long[] jjbitVec3;
    static final long[] jjbitVec4;
    static final long[] jjbitVec5;
    static final long[] jjbitVec6;
    static final long[] jjbitVec7;
    static final long[] jjbitVec8;
    static final int[] jjnextStates;
    public static final String[] jjstrLiteralImages;
    public static final String[] lexStateNames;
    static final long[] jjtoToken;
    static final long[] jjtoSkip;
    private JavaCharStream input_stream;
    private final int[] jjrounds = new int[70];
    private final int[] jjstateSet = new int[140];
    protected char curChar;
    int curLexState = 0;
    int defaultLexState = 0;
    int jjnewStateCnt;
    int jjround;
    int jjmatchedPos;
    int jjmatchedKind;

    static {
        long[] arrl = new long[4];
        arrl[2] = -1;
        arrl[3] = -1;
        jjbitVec2 = arrl;
        jjbitVec3 = new long[]{2301339413881290750L, -16384, 0xFFFFFFFFL, 0x600000000000000L};
        long[] arrl2 = new long[4];
        arrl2[3] = -36028797027352577L;
        jjbitVec4 = arrl2;
        long[] arrl3 = new long[4];
        arrl3[1] = -1;
        arrl3[2] = -1;
        arrl3[3] = -1;
        jjbitVec5 = arrl3;
        long[] arrl4 = new long[4];
        arrl4[0] = -1;
        arrl4[1] = -1;
        arrl4[2] = 65535;
        jjbitVec6 = arrl4;
        long[] arrl5 = new long[4];
        arrl5[0] = -1;
        arrl5[1] = -1;
        jjbitVec7 = arrl5;
        long[] arrl6 = new long[4];
        arrl6[0] = 0x3FFFFFFFFFFFL;
        jjbitVec8 = arrl6;
        jjnextStates = new int[]{35, 4, 56, 57, 62, 63, 66, 67, 10, 35, 1, 36, 42, 47, 51, 30, 31, 32, 33, 12, 13, 15, 6, 7, 10, 12, 13, 17, 15, 38, 39, 10, 43, 44, 10, 51, 52, 10, 58, 59, 10, 66, 67, 10, 8, 9, 14, 16, 18, 30, 31, 32, 33, 40, 41, 45, 46, 49, 50, 53, 54, 60, 61, 64, 65, 68, 69};
        String[] arrstring = new String[19];
        arrstring[0] = "";
        arrstring[15] = "{";
        arrstring[16] = "}";
        arrstring[17] = "[";
        arrstring[18] = "]";
        jjstrLiteralImages = arrstring;
        lexStateNames = new String[]{"DEFAULT"};
        jjtoToken = new long[]{499345};
        jjtoSkip = new long[]{14};
    }

    public void setDebugStream(PrintStream ds) {
        this.debugStream = ds;
    }

    private final int jjStopStringLiteralDfa_0(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_0(int pos, long active0) {
        return this.jjMoveNfa_0(this.jjStopStringLiteralDfa_0(pos, active0), pos + 1);
    }

    private final int jjStopAtPos(int pos, int kind) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        return pos + 1;
    }

    private final int jjStartNfaWithStates_0(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = this.input_stream.readChar();
        }
        catch (IOException e) {
            return pos + 1;
        }
        return this.jjMoveNfa_0(state, pos + 1);
    }

    private final int jjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case '[': {
                return this.jjStopAtPos(0, 17);
            }
            case ']': {
                return this.jjStopAtPos(0, 18);
            }
            case '{': {
                return this.jjStopAtPos(0, 15);
            }
            case '}': {
                return this.jjStopAtPos(0, 16);
            }
        }
        return this.jjMoveNfa_0(0, 0);
    }

    private final void jjCheckNAdd(int state) {
        if (this.jjrounds[state] != this.jjround) {
            this.jjstateSet[this.jjnewStateCnt++] = state;
            this.jjrounds[state] = this.jjround;
        }
    }

    private final void jjAddStates(int start, int end) {
        do {
            this.jjstateSet[this.jjnewStateCnt++] = jjnextStates[start];
        } while (start++ != end);
    }

    private final void jjCheckNAddTwoStates(int state1, int state2) {
        this.jjCheckNAdd(state1);
        this.jjCheckNAdd(state2);
    }

    private final void jjCheckNAddStates(int start, int end) {
        do {
            this.jjCheckNAdd(jjnextStates[start]);
        } while (start++ != end);
    }

    private final void jjCheckNAddStates(int start) {
        this.jjCheckNAdd(jjnextStates[start]);
        this.jjCheckNAdd(jjnextStates[start + 1]);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private final int jjMoveNfa_0(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 70;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        do {
            long l;
            if (++this.jjround == Integer.MAX_VALUE) {
                this.ReInitRounds();
            }
            if (this.curChar < '@') {
                l = 1 << this.curChar;
                block93 : do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if ((287948901175001088L & l) != 0) {
                                if (kind > 4) {
                                    kind = 4;
                                }
                                this.jjCheckNAddStates(0, 8);
                            } else if ((4294981120L & l) != 0) {
                                if (kind > 1) {
                                    kind = 1;
                                }
                            } else if (this.curChar == '-') {
                                this.jjCheckNAddStates(9, 14);
                            } else if (this.curChar == '%') {
                                this.jjCheckNAddStates(15, 18);
                            } else if (this.curChar == '/') {
                                this.jjstateSet[this.jjnewStateCnt++] = 25;
                            } else if (this.curChar == '$') {
                                if (kind > 10) {
                                    kind = 10;
                                }
                                this.jjCheckNAdd(21);
                            } else if (this.curChar == '(') {
                                this.jjCheckNAddStates(19, 21);
                            } else if (this.curChar == '.') {
                                this.jjCheckNAdd(6);
                            }
                            if (this.curChar == '/') {
                                this.jjstateSet[this.jjnewStateCnt++] = 23;
                                break;
                            }
                            if (this.curChar != '0') break;
                            this.jjstateSet[this.jjnewStateCnt++] = 2;
                            break;
                        }
                        case 1: {
                            if (this.curChar != '0') break;
                            this.jjstateSet[this.jjnewStateCnt++] = 2;
                            break;
                        }
                        case 3: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 4) {
                                kind = 4;
                            }
                            this.jjCheckNAddTwoStates(3, 4);
                            break;
                        }
                        case 5: {
                            if (this.curChar != '.') break;
                            this.jjCheckNAdd(6);
                            break;
                        }
                        case 6: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddStates(22, 24);
                            break;
                        }
                        case 8: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(9);
                            break;
                        }
                        case 9: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddTwoStates(9, 10);
                            break;
                        }
                        case 11: {
                            if (this.curChar != '(') break;
                            this.jjCheckNAddStates(19, 21);
                            break;
                        }
                        case 12: {
                            if ((-2199023264769L & l) == 0) break;
                            this.jjCheckNAddStates(19, 21);
                            break;
                        }
                        case 14: {
                            if ((2748779070464L & l) == 0) break;
                            this.jjCheckNAddStates(19, 21);
                            break;
                        }
                        case 15: {
                            if (this.curChar != ')' || kind <= 9) continue block93;
                            kind = 9;
                            break;
                        }
                        case 16: {
                            if ((0xFF000000000000L & l) == 0) break;
                            this.jjCheckNAddStates(25, 28);
                            break;
                        }
                        case 17: {
                            if ((0xFF000000000000L & l) == 0) break;
                            this.jjCheckNAddStates(19, 21);
                            break;
                        }
                        case 18: {
                            if ((0xF000000000000L & l) == 0) break;
                            this.jjstateSet[this.jjnewStateCnt++] = 19;
                            break;
                        }
                        case 19: {
                            if ((0xFF000000000000L & l) == 0) break;
                            this.jjCheckNAdd(17);
                            break;
                        }
                        case 20: {
                            if (this.curChar != '$') continue block93;
                            if (kind > 10) {
                                kind = 10;
                            }
                            this.jjCheckNAdd(21);
                            break;
                        }
                        case 21: {
                            if ((288054523010744320L & l) == 0) continue block93;
                            if (kind > 10) {
                                kind = 10;
                            }
                            this.jjCheckNAdd(21);
                            break;
                        }
                        case 22: {
                            if (this.curChar != '/') break;
                            this.jjstateSet[this.jjnewStateCnt++] = 23;
                            break;
                        }
                        case 23: {
                            if (this.curChar != '$') continue block93;
                            if (kind > 11) {
                                kind = 11;
                            }
                            this.jjCheckNAdd(24);
                            break;
                        }
                        case 24: {
                            if ((288054523010744320L & l) == 0) continue block93;
                            if (kind > 11) {
                                kind = 11;
                            }
                            this.jjCheckNAdd(24);
                            break;
                        }
                        case 25: {
                            if (this.curChar != '/') break;
                            this.jjstateSet[this.jjnewStateCnt++] = 26;
                            break;
                        }
                        case 26: {
                            if (this.curChar != '$') continue block93;
                            if (kind > 12) {
                                kind = 12;
                            }
                            this.jjCheckNAdd(27);
                            break;
                        }
                        case 27: {
                            if ((288054523010744320L & l) == 0) continue block93;
                            if (kind > 12) {
                                kind = 12;
                            }
                            this.jjCheckNAdd(27);
                            break;
                        }
                        case 28: {
                            if (this.curChar != '/') break;
                            this.jjstateSet[this.jjnewStateCnt++] = 25;
                            break;
                        }
                        case 29: {
                            if (this.curChar != '%') break;
                            this.jjCheckNAddStates(15, 18);
                            break;
                        }
                        case 30: {
                            if ((-1025 & l) == 0) break;
                            this.jjCheckNAddTwoStates(30, 31);
                            break;
                        }
                        case 31: {
                            if (this.curChar != '\n' || kind <= 2) continue block93;
                            kind = 2;
                            break;
                        }
                        case 32: {
                            if ((-8193 & l) == 0) break;
                            this.jjCheckNAddTwoStates(32, 33);
                            break;
                        }
                        case 33: {
                            if (this.curChar != '\r' || kind <= 3) continue block93;
                            kind = 3;
                            break;
                        }
                        case 34: {
                            if (this.curChar != '-') break;
                            this.jjCheckNAddStates(9, 14);
                            break;
                        }
                        case 35: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 4) {
                                kind = 4;
                            }
                            this.jjCheckNAddTwoStates(35, 4);
                            break;
                        }
                        case 36: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddTwoStates(36, 37);
                            break;
                        }
                        case 37: {
                            if (this.curChar != '.') continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddStates(29, 31);
                            break;
                        }
                        case 38: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddStates(29, 31);
                            break;
                        }
                        case 40: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(41);
                            break;
                        }
                        case 41: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddTwoStates(41, 10);
                            break;
                        }
                        case 42: {
                            if (this.curChar != '.') break;
                            this.jjCheckNAdd(43);
                            break;
                        }
                        case 43: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddStates(32, 34);
                            break;
                        }
                        case 45: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(46);
                            break;
                        }
                        case 46: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddTwoStates(46, 10);
                            break;
                        }
                        case 47: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddTwoStates(47, 48);
                            break;
                        }
                        case 49: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(50);
                            break;
                        }
                        case 50: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddTwoStates(50, 10);
                            break;
                        }
                        case 51: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddStates(35, 37);
                            break;
                        }
                        case 53: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(54);
                            break;
                        }
                        case 54: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddTwoStates(54, 10);
                            break;
                        }
                        case 55: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 4) {
                                kind = 4;
                            }
                            this.jjCheckNAddStates(0, 8);
                            break;
                        }
                        case 56: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddTwoStates(56, 57);
                            break;
                        }
                        case 57: {
                            if (this.curChar != '.') continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddStates(38, 40);
                            break;
                        }
                        case 58: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddStates(38, 40);
                            break;
                        }
                        case 60: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(61);
                            break;
                        }
                        case 61: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddTwoStates(61, 10);
                            break;
                        }
                        case 62: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddTwoStates(62, 63);
                            break;
                        }
                        case 64: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(65);
                            break;
                        }
                        case 65: {
                            if ((287948901175001088L & l) == 0) continue block93;
                            if (kind > 7) {
                                kind = 7;
                            }
                            this.jjCheckNAddTwoStates(65, 10);
                            break;
                        }
                        case 66: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddStates(41, 43);
                            break;
                        }
                        case 68: {
                            if ((43980465111040L & l) == 0) break;
                            this.jjCheckNAdd(69);
                            break;
                        }
                        case 69: {
                            if ((287948901175001088L & l) == 0) break;
                            this.jjCheckNAddTwoStates(69, 10);
                            break;
                        }
                    }
                } while (i != startsAt);
            } else if (this.curChar < '') {
                l = 1 << (this.curChar & 63);
                block94 : do {
                    switch (this.jjstateSet[--i]) {
                        case 0: 
                        case 21: {
                            if ((576460745995190270L & l) == 0) continue block94;
                            if (kind > 10) {
                                kind = 10;
                            }
                            this.jjCheckNAdd(21);
                            break;
                        }
                        case 2: {
                            if ((0x100000001000000L & l) == 0) break;
                            this.jjCheckNAdd(3);
                            break;
                        }
                        case 3: {
                            if ((541165879422L & l) == 0) continue block94;
                            if (kind > 4) {
                                kind = 4;
                            }
                            this.jjCheckNAddTwoStates(3, 4);
                            break;
                        }
                        case 4: {
                            if ((0x100000001000L & l) == 0 || kind <= 4) continue block94;
                            kind = 4;
                            break;
                        }
                        case 7: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(44, 45);
                            break;
                        }
                        case 10: {
                            if ((0x5000000050L & l) == 0 || kind <= 7) continue block94;
                            kind = 7;
                            break;
                        }
                        case 12: {
                            if ((-268435457 & l) == 0) break;
                            this.jjCheckNAddStates(19, 21);
                            break;
                        }
                        case 13: {
                            if (this.curChar != '\\') break;
                            this.jjAddStates(46, 48);
                            break;
                        }
                        case 14: {
                            if ((5700160604602368L & l) == 0) break;
                            this.jjCheckNAddStates(19, 21);
                            break;
                        }
                        case 23: 
                        case 24: {
                            if ((576460745995190270L & l) == 0) continue block94;
                            if (kind > 11) {
                                kind = 11;
                            }
                            this.jjCheckNAdd(24);
                            break;
                        }
                        case 26: 
                        case 27: {
                            if ((576460745995190270L & l) == 0) continue block94;
                            if (kind > 12) {
                                kind = 12;
                            }
                            this.jjCheckNAdd(27);
                            break;
                        }
                        case 30: {
                            this.jjAddStates(49, 50);
                            break;
                        }
                        case 32: {
                            this.jjAddStates(51, 52);
                            break;
                        }
                        case 39: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(53, 54);
                            break;
                        }
                        case 44: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(55, 56);
                            break;
                        }
                        case 48: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(57, 58);
                            break;
                        }
                        case 52: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(59, 60);
                            break;
                        }
                        case 59: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(61, 62);
                            break;
                        }
                        case 63: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(63, 64);
                            break;
                        }
                        case 67: {
                            if ((0x2000000020L & l) == 0) break;
                            this.jjAddStates(65, 66);
                            break;
                        }
                    }
                } while (i != startsAt);
            } else {
                int hiByte = this.curChar >> 8;
                int i1 = hiByte >> 6;
                long l1 = 1 << (hiByte & 63);
                int i2 = (this.curChar & 255) >> 6;
                long l2 = 1 << (this.curChar & 63);
                block95 : do {
                    switch (this.jjstateSet[--i]) {
                        case 0: 
                        case 21: {
                            if (!PAParserTokenManager.jjCanMove_1(hiByte, i1, i2, l1, l2)) continue block95;
                            if (kind > 10) {
                                kind = 10;
                            }
                            this.jjCheckNAdd(21);
                            break;
                        }
                        case 12: {
                            if (!PAParserTokenManager.jjCanMove_0(hiByte, i1, i2, l1, l2)) break;
                            this.jjAddStates(19, 21);
                            break;
                        }
                        case 23: 
                        case 24: {
                            if (!PAParserTokenManager.jjCanMove_1(hiByte, i1, i2, l1, l2)) continue block95;
                            if (kind > 11) {
                                kind = 11;
                            }
                            this.jjCheckNAdd(24);
                            break;
                        }
                        case 26: 
                        case 27: {
                            if (!PAParserTokenManager.jjCanMove_1(hiByte, i1, i2, l1, l2)) continue block95;
                            if (kind > 12) {
                                kind = 12;
                            }
                            this.jjCheckNAdd(27);
                            break;
                        }
                        case 30: {
                            if (!PAParserTokenManager.jjCanMove_0(hiByte, i1, i2, l1, l2)) break;
                            this.jjAddStates(49, 50);
                            break;
                        }
                        case 32: {
                            if (!PAParserTokenManager.jjCanMove_0(hiByte, i1, i2, l1, l2)) break;
                            this.jjAddStates(51, 52);
                            break;
                        }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            ++curPos;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 70 - this.jjnewStateCnt;
            if (i == startsAt) {
                return curPos;
            }
            try {
                this.curChar = this.input_stream.readChar();
                continue;
            }
            catch (IOException e) {}
            return curPos;
        } while (true);
    }

    private static final boolean jjCanMove_0(int hiByte, int i1, int i2, long l1, long l2) {
        switch (hiByte) {
            case 0: {
                if ((jjbitVec2[i2] & l2) != 0) {
                    return true;
                }
                return false;
            }
        }
        if ((jjbitVec0[i1] & l1) != 0) {
            return true;
        }
        return false;
    }

    private static final boolean jjCanMove_1(int hiByte, int i1, int i2, long l1, long l2) {
        switch (hiByte) {
            case 0: {
                if ((jjbitVec4[i2] & l2) != 0) {
                    return true;
                }
                return false;
            }
            case 48: {
                if ((jjbitVec5[i2] & l2) != 0) {
                    return true;
                }
                return false;
            }
            case 49: {
                if ((jjbitVec6[i2] & l2) != 0) {
                    return true;
                }
                return false;
            }
            case 51: {
                if ((jjbitVec7[i2] & l2) != 0) {
                    return true;
                }
                return false;
            }
            case 61: {
                if ((jjbitVec8[i2] & l2) != 0) {
                    return true;
                }
                return false;
            }
        }
        if ((jjbitVec3[i1] & l1) != 0) {
            return true;
        }
        return false;
    }

    public PAParserTokenManager(JavaCharStream stream) {
        this.input_stream = stream;
    }

    public PAParserTokenManager(JavaCharStream stream, int lexState) {
        this(stream);
        this.SwitchTo(lexState);
    }

    public void ReInit(JavaCharStream stream) {
        this.jjnewStateCnt = 0;
        this.jjmatchedPos = 0;
        this.curLexState = this.defaultLexState;
        this.input_stream = stream;
        this.ReInitRounds();
    }

    private final void ReInitRounds() {
        this.jjround = -2147483647;
        int i = 70;
        while (i-- > 0) {
            this.jjrounds[i] = Integer.MIN_VALUE;
        }
    }

    public void ReInit(JavaCharStream stream, int lexState) {
        this.ReInit(stream);
        this.SwitchTo(lexState);
    }

    public void SwitchTo(int lexState) {
        if (lexState >= 1 || lexState < 0) {
            throw new TokenMgrError("Error: Ignoring invalid lexical state : " + lexState + ". State unchanged.", 2);
        }
        this.curLexState = lexState;
    }

    private final Token jjFillToken() {
        Token t = Token.newToken(this.jjmatchedKind);
        t.kind = this.jjmatchedKind;
        String im = jjstrLiteralImages[this.jjmatchedKind];
        t.image = im == null ? this.input_stream.GetImage() : im;
        t.beginLine = this.input_stream.getBeginLine();
        t.beginColumn = this.input_stream.getBeginColumn();
        t.endLine = this.input_stream.getEndLine();
        t.endColumn = this.input_stream.getEndColumn();
        return t;
    }

    public final Token getNextToken() {
        int curPos;
        block7 : {
            Object specialToken = null;
            curPos = 0;
            do {
                try {
                    this.curChar = this.input_stream.BeginToken();
                }
                catch (IOException e) {
                    this.jjmatchedKind = 0;
                    Token matchedToken = this.jjFillToken();
                    return matchedToken;
                }
                this.jjmatchedKind = Integer.MAX_VALUE;
                this.jjmatchedPos = 0;
                curPos = this.jjMoveStringLiteralDfa0_0();
                if (this.jjmatchedKind == Integer.MAX_VALUE) break block7;
                if (this.jjmatchedPos + 1 >= curPos) continue;
                this.input_stream.backup(curPos - this.jjmatchedPos - 1);
            } while ((jjtoToken[this.jjmatchedKind >> 6] & 1 << (this.jjmatchedKind & 63)) == 0);
            Token matchedToken = this.jjFillToken();
            return matchedToken;
        }
        int error_line = this.input_stream.getEndLine();
        int error_column = this.input_stream.getEndColumn();
        String error_after = null;
        boolean EOFSeen = false;
        try {
            this.input_stream.readChar();
            this.input_stream.backup(1);
        }
        catch (IOException e1) {
            EOFSeen = true;
            String string = error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
            if (this.curChar == '\n' || this.curChar == '\r') {
                ++error_line;
                error_column = 0;
            }
            ++error_column;
        }
        if (!EOFSeen) {
            this.input_stream.backup(1);
            error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
        }
        throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
    }
}

