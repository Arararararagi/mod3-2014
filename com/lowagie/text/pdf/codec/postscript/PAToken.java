/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec.postscript;

public class PAToken {
    public static final int IDENTIFIER = 0;
    public static final int KEY = 1;
    public static final int PROCEDURE = 2;
    public static final int MARK = 3;
    public static final int START_PROCEDURE = 4;
    public static final int END_PROCEDURE = 5;
    public static final int IMMEDIATE = 6;
    public static final int START_ARRAY = 7;
    public static final int END_ARRAY = 8;
    public Object value;
    public int type;

    public PAToken(Object value, int type) {
        this.value = value;
        this.type = type;
    }

    public String toString() {
        switch (this.type) {
            case 0: {
                return "IDENTIFIER " + this.value.toString();
            }
            case 1: {
                return "KEY " + this.value.toString();
            }
            case 2: {
                return "PROCEDURE " + this.value.toString();
            }
            case 3: {
                return "MARK";
            }
            case 4: {
                return "START_PROCEDURE";
            }
            case 5: {
                return "END_PROCEDURE";
            }
            case 6: {
                return "IMMEDIATE " + this.value.toString();
            }
            case 7: {
                return "START_ARRAY";
            }
            case 8: {
                return "END_ARRAY";
            }
        }
        return this.value.toString();
    }
}

