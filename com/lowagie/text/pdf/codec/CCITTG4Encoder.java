/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf.codec;

import com.lowagie.text.pdf.ByteBuffer;

public class CCITTG4Encoder {
    private static final int WHITE = 0;
    private static final int BLACK = 1;
    private static byte[] byteTable;
    private static int[] termCodesBlack;
    private static int[] termCodesWhite;
    private static int[] makeupCodesBlack;
    private static int[] makeupCodesWhite;
    private static int[] passMode;
    private static int[] vertMode;
    private static int[] horzMode;
    private static int[][] termCodes;
    private static int[][] makeupCodes;
    private static int[][] pass;
    private static int[][] vert;
    private static int[][] horz;
    private int bits;
    private int ndex;
    private ByteBuffer outBuf = new ByteBuffer(1024);
    private int width;
    private int lineStride;
    byte[] refData = null;

    static {
        byte[] arrby = new byte[256];
        arrby[0] = 8;
        arrby[1] = 7;
        arrby[2] = 6;
        arrby[3] = 6;
        arrby[4] = 5;
        arrby[5] = 5;
        arrby[6] = 5;
        arrby[7] = 5;
        arrby[8] = 4;
        arrby[9] = 4;
        arrby[10] = 4;
        arrby[11] = 4;
        arrby[12] = 4;
        arrby[13] = 4;
        arrby[14] = 4;
        arrby[15] = 4;
        arrby[16] = 3;
        arrby[17] = 3;
        arrby[18] = 3;
        arrby[19] = 3;
        arrby[20] = 3;
        arrby[21] = 3;
        arrby[22] = 3;
        arrby[23] = 3;
        arrby[24] = 3;
        arrby[25] = 3;
        arrby[26] = 3;
        arrby[27] = 3;
        arrby[28] = 3;
        arrby[29] = 3;
        arrby[30] = 3;
        arrby[31] = 3;
        arrby[32] = 2;
        arrby[33] = 2;
        arrby[34] = 2;
        arrby[35] = 2;
        arrby[36] = 2;
        arrby[37] = 2;
        arrby[38] = 2;
        arrby[39] = 2;
        arrby[40] = 2;
        arrby[41] = 2;
        arrby[42] = 2;
        arrby[43] = 2;
        arrby[44] = 2;
        arrby[45] = 2;
        arrby[46] = 2;
        arrby[47] = 2;
        arrby[48] = 2;
        arrby[49] = 2;
        arrby[50] = 2;
        arrby[51] = 2;
        arrby[52] = 2;
        arrby[53] = 2;
        arrby[54] = 2;
        arrby[55] = 2;
        arrby[56] = 2;
        arrby[57] = 2;
        arrby[58] = 2;
        arrby[59] = 2;
        arrby[60] = 2;
        arrby[61] = 2;
        arrby[62] = 2;
        arrby[63] = 2;
        arrby[64] = 1;
        arrby[65] = 1;
        arrby[66] = 1;
        arrby[67] = 1;
        arrby[68] = 1;
        arrby[69] = 1;
        arrby[70] = 1;
        arrby[71] = 1;
        arrby[72] = 1;
        arrby[73] = 1;
        arrby[74] = 1;
        arrby[75] = 1;
        arrby[76] = 1;
        arrby[77] = 1;
        arrby[78] = 1;
        arrby[79] = 1;
        arrby[80] = 1;
        arrby[81] = 1;
        arrby[82] = 1;
        arrby[83] = 1;
        arrby[84] = 1;
        arrby[85] = 1;
        arrby[86] = 1;
        arrby[87] = 1;
        arrby[88] = 1;
        arrby[89] = 1;
        arrby[90] = 1;
        arrby[91] = 1;
        arrby[92] = 1;
        arrby[93] = 1;
        arrby[94] = 1;
        arrby[95] = 1;
        arrby[96] = 1;
        arrby[97] = 1;
        arrby[98] = 1;
        arrby[99] = 1;
        arrby[100] = 1;
        arrby[101] = 1;
        arrby[102] = 1;
        arrby[103] = 1;
        arrby[104] = 1;
        arrby[105] = 1;
        arrby[106] = 1;
        arrby[107] = 1;
        arrby[108] = 1;
        arrby[109] = 1;
        arrby[110] = 1;
        arrby[111] = 1;
        arrby[112] = 1;
        arrby[113] = 1;
        arrby[114] = 1;
        arrby[115] = 1;
        arrby[116] = 1;
        arrby[117] = 1;
        arrby[118] = 1;
        arrby[119] = 1;
        arrby[120] = 1;
        arrby[121] = 1;
        arrby[122] = 1;
        arrby[123] = 1;
        arrby[124] = 1;
        arrby[125] = 1;
        arrby[126] = 1;
        arrby[127] = 1;
        byteTable = arrby;
        termCodesBlack = new int[]{230686730, 1073741827, -1073741822, -2147483646, 1610612739, 805306372, 536870916, 402653189, 335544326, 268435462, 134217735, 167772167, 234881031, 67108872, 117440520, 201326601, 96469002, 100663306, 33554442, 216006667, 218103819, 226492427, 115343371, 83886091, 48234507, 50331659, 211812364, 212860940, 213909516, 214958092, 109051916, 110100492, 111149068, 112197644, 220200972, 221249548, 222298124, 223346700, 224395276, 225443852, 113246220, 114294796, 228589580, 229638156, 88080396, 89128972, 90177548, 91226124, 104857612, 105906188, 85983244, 87031820, 37748748, 57671692, 58720268, 40894476, 41943052, 92274700, 93323276, 45088780, 46137356, 94371852, 106954764, 108003340};
        termCodesWhite = new int[]{889192456, 469762054, 1879048196, -2147483644, -1342177276, -1073741820, -536870908, -268435452, -1744830459, -1610612731, 939524101, 1073741829, 536870918, 201326598, -805306362, -738197498, -1476395002, -1409286138, 1308622855, 402653191, 268435463, 771751943, 100663303, 134217735, 1342177287, 1442840583, 637534215, 1207959559, 805306375, 33554440, 50331656, 436207624, 452984840, 301989896, 318767112, 335544328, 352321544, 369098760, 385875976, 671088648, 687865864, 704643080, 721420296, 738197512, 754974728, 67108872, 83886088, 167772168, 184549384, 1375731720, 1392508936, 1409286152, 1426063368, 603979784, 620757000, 1476395016, 1493172232, 1509949448, 1526726664, 1241513992, 1258291208, 838860808, 855638024, 872415240};
        int[] arrn = new int[60];
        arrn[1] = 62914570;
        arrn[2] = 209715212;
        arrn[3] = 210763788;
        arrn[4] = 95420428;
        arrn[5] = 53477388;
        arrn[6] = 54525964;
        arrn[7] = 55574540;
        arrn[8] = 56623117;
        arrn[9] = 57147405;
        arrn[10] = 38797325;
        arrn[11] = 39321613;
        arrn[12] = 39845901;
        arrn[13] = 40370189;
        arrn[14] = 59768845;
        arrn[15] = 60293133;
        arrn[16] = 60817421;
        arrn[17] = 61341709;
        arrn[18] = 61865997;
        arrn[19] = 62390285;
        arrn[20] = 42991629;
        arrn[21] = 43515917;
        arrn[22] = 44040205;
        arrn[23] = 44564493;
        arrn[24] = 47185933;
        arrn[25] = 47710221;
        arrn[26] = 52428813;
        arrn[27] = 52953101;
        arrn[28] = 16777227;
        arrn[29] = 25165835;
        arrn[30] = 27262987;
        arrn[31] = 18874380;
        arrn[32] = 19922956;
        arrn[33] = 20971532;
        arrn[34] = 22020108;
        arrn[35] = 23068684;
        arrn[36] = 24117260;
        arrn[37] = 29360140;
        arrn[38] = 30408716;
        arrn[39] = 31457292;
        arrn[40] = 32505868;
        makeupCodesBlack = arrn;
        int[] arrn2 = new int[60];
        arrn2[1] = -671088635;
        arrn2[2] = -1879048187;
        arrn2[3] = 1543503878;
        arrn2[4] = 1845493767;
        arrn2[5] = 905969672;
        arrn2[6] = 922746888;
        arrn2[7] = 1677721608;
        arrn2[8] = 1694498824;
        arrn2[9] = 1744830472;
        arrn2[10] = 1728053256;
        arrn2[11] = 1711276041;
        arrn2[12] = 1719664649;
        arrn2[13] = 1761607689;
        arrn2[14] = 1769996297;
        arrn2[15] = 1778384905;
        arrn2[16] = 1786773513;
        arrn2[17] = 1795162121;
        arrn2[18] = 1803550729;
        arrn2[19] = 1811939337;
        arrn2[20] = 1820327945;
        arrn2[21] = 1828716553;
        arrn2[22] = 1837105161;
        arrn2[23] = 1275068425;
        arrn2[24] = 1283457033;
        arrn2[25] = 1291845641;
        arrn2[26] = 1610612742;
        arrn2[27] = 1300234249;
        arrn2[28] = 16777227;
        arrn2[29] = 25165835;
        arrn2[30] = 27262987;
        arrn2[31] = 18874380;
        arrn2[32] = 19922956;
        arrn2[33] = 20971532;
        arrn2[34] = 22020108;
        arrn2[35] = 23068684;
        arrn2[36] = 24117260;
        arrn2[37] = 29360140;
        arrn2[38] = 30408716;
        arrn2[39] = 31457292;
        arrn2[40] = 32505868;
        makeupCodesWhite = arrn2;
        passMode = new int[]{268435460};
        vertMode = new int[]{100663303, 201326598, 1610612739, -2147483647, 1073741827, 134217734, 67108871};
        horzMode = new int[]{536870915};
        termCodes = new int[][]{termCodesWhite, termCodesBlack};
        makeupCodes = new int[][]{makeupCodesWhite, makeupCodesBlack};
        pass = new int[][]{passMode, passMode};
        vert = new int[][]{vertMode, vertMode};
        horz = new int[][]{horzMode, horzMode};
    }

    public CCITTG4Encoder(int width) {
        this.width = width;
        this.lineStride = (width + 7) / 8;
        this.initBitBuf();
    }

    public static byte[] compress(byte[] data, int width, int height) {
        CCITTG4Encoder g4 = new CCITTG4Encoder(width);
        int yPos = 0;
        while (height-- != 0) {
            g4.encodeT6Line(data, yPos);
            yPos+=g4.lineStride;
        }
        return g4.close();
    }

    public void encodeT6Lines(byte[] data, int lineAddr, int height) {
        int yPos = lineAddr;
        while (height-- != 0) {
            this.encodeT6Line(data, yPos);
            yPos+=this.lineStride;
        }
    }

    /*
     * Unable to fully structure code
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private int nextState(byte[] data, int base, int bitOffset, int maxOffset) {
        if (data == null) {
            return maxOffset;
        }
        next = base + (bitOffset >>> 3);
        end = base + (maxOffset >>> 3);
        if (end == data.length) {
            --end;
        }
        if (next == data.length) {
            --next;
        }
        if ((data[next] & 128 >>> (extra = bitOffset & 7)) == 0) ** GOTO lbl16
        testbyte = ~ data[next] & 255 >>> extra;
        while (next < end) {
            if (testbyte == 0) {
                testbyte = ~ data[++next] & 255;
                continue;
            } else {
                ** GOTO lbl24
            }
        }
        ** GOTO lbl24
lbl16: // 1 sources:
        testbyte = data[next] & 255 >>> extra;
        if (testbyte == 0) ** GOTO lbl23
        bitOffset = (next - base) * 8 + CCITTG4Encoder.byteTable[testbyte];
        return bitOffset < maxOffset ? bitOffset : maxOffset;
lbl-1000: // 1 sources:
        {
            if ((testbyte = data[++next] & 255) == 0) continue;
            bitOffset = (next - base) * 8 + CCITTG4Encoder.byteTable[testbyte];
            return bitOffset < maxOffset ? bitOffset : maxOffset;
lbl23: // 2 sources:
            ** while (next < end)
        }
lbl24: // 4 sources:
        bitOffset = (next - base) * 8 + CCITTG4Encoder.byteTable[testbyte];
        return bitOffset < maxOffset ? bitOffset : maxOffset;
    }

    private void initBitBuf() {
        this.ndex = 0;
        this.bits = 0;
    }

    private void add1DBits(int count, int color) {
        int mask;
        if (count < 0) {
            return;
        }
        int sixtyfours = count >>> 6;
        count&=63;
        if (sixtyfours != 0) {
            while (sixtyfours > 40) {
                mask = makeupCodes[color][40];
                this.bits|=(mask & -524288) >>> this.ndex;
                this.ndex+=mask & 65535;
                while (this.ndex > 7) {
                    this.outBuf.append((byte)(this.bits >>> 24));
                    this.bits<<=8;
                    this.ndex-=8;
                }
                sixtyfours-=40;
            }
            mask = makeupCodes[color][sixtyfours];
            this.bits|=(mask & -524288) >>> this.ndex;
            this.ndex+=mask & 65535;
            while (this.ndex > 7) {
                this.outBuf.append((byte)(this.bits >>> 24));
                this.bits<<=8;
                this.ndex-=8;
            }
        }
        mask = termCodes[color][count];
        this.bits|=(mask & -524288) >>> this.ndex;
        this.ndex+=mask & 65535;
        while (this.ndex > 7) {
            this.outBuf.append((byte)(this.bits >>> 24));
            this.bits<<=8;
            this.ndex-=8;
        }
    }

    private void add2DBits(int[][] mode, int entry) {
        int color = 0;
        int mask = mode[color][entry];
        this.bits|=(mask & -524288) >>> this.ndex;
        this.ndex+=mask & 65535;
        while (this.ndex > 7) {
            this.outBuf.append((byte)(this.bits >>> 24));
            this.bits<<=8;
            this.ndex-=8;
        }
    }

    private void addEOL(boolean is1DMode, boolean addFill, boolean add1) {
        if (addFill) {
            this.ndex+=this.ndex <= 4 ? 4 - this.ndex : 12 - this.ndex;
        }
        if (is1DMode) {
            this.bits|=1048576 >>> this.ndex;
            this.ndex+=12;
        } else {
            this.bits|=(add1 ? 1572864 : 1048576) >>> this.ndex;
            this.ndex+=13;
        }
        while (this.ndex > 7) {
            this.outBuf.append((byte)(this.bits >>> 24));
            this.bits<<=8;
            this.ndex-=8;
        }
    }

    private void addEOFB() {
        this.bits|=1048832 >>> this.ndex;
        this.ndex+=24;
        while (this.ndex > 0) {
            this.outBuf.append((byte)(this.bits >>> 24));
            this.bits<<=8;
            this.ndex-=8;
        }
    }

    public void encodeT6Line(byte[] data, int lineAddr) {
        int a0 = 0;
        int last = a0 + this.width;
        int testbit = (data[lineAddr + (a0 >>> 3)] & 255) >>> 7 - (a0 & 7) & 1;
        int a1 = testbit != 0 ? a0 : this.nextState(data, lineAddr, a0, last);
        testbit = this.refData == null ? 0 : (this.refData[a0 >>> 3] & 255) >>> 7 - (a0 & 7) & 1;
        int b1 = testbit != 0 ? a0 : this.nextState(this.refData, 0, a0, last);
        int color = 0;
        do {
            int b2;
            if ((b2 = this.nextState(this.refData, 0, b1, last)) < a1) {
                this.add2DBits(pass, 0);
                a0 = b2;
            } else {
                int tmp = b1 - a1 + 3;
                if (tmp <= 6 && tmp >= 0) {
                    this.add2DBits(vert, tmp);
                    a0 = a1;
                } else {
                    int a2 = this.nextState(data, lineAddr, a1, last);
                    this.add2DBits(horz, 0);
                    this.add1DBits(a1 - a0, color);
                    this.add1DBits(a2 - a1, color ^ 1);
                    if (a2 <= a0) {
                        throw new RuntimeException("G4 encoding error.");
                    }
                    a0 = a2;
                }
            }
            if (a0 >= last) break;
            color = (data[lineAddr + (a0 >>> 3)] & 255) >>> 7 - (a0 & 7) & 1;
            a1 = this.nextState(data, lineAddr, a0, last);
            b1 = this.nextState(this.refData, 0, a0, last);
            testbit = this.refData == null ? 0 : (this.refData[b1 >>> 3] & 255) >>> 7 - (b1 & 7) & 1;
            if (testbit != color) continue;
            b1 = this.nextState(this.refData, 0, b1, last);
        } while (true);
        if (this.refData == null) {
            this.refData = new byte[this.lineStride + 1];
        }
        System.arraycopy(data, lineAddr, this.refData, 0, this.lineStride);
    }

    public byte[] close() {
        this.addEOFB();
        return this.outBuf.toByteArray();
    }
}

