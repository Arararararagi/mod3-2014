/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfString;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class PdfDate
extends PdfString {
    private static final int[] dateSpace;

    static {
        int[] arrn = new int[18];
        arrn[0] = 1;
        arrn[1] = 4;
        arrn[3] = 2;
        arrn[4] = 2;
        arrn[5] = -1;
        arrn[6] = 5;
        arrn[7] = 2;
        arrn[9] = 11;
        arrn[10] = 2;
        arrn[12] = 12;
        arrn[13] = 2;
        arrn[15] = 13;
        arrn[16] = 2;
        dateSpace = arrn;
    }

    public PdfDate(Calendar d) {
        StringBuffer date = new StringBuffer("D:");
        date.append(this.setLength(d.get(1), 4));
        date.append(this.setLength(d.get(2) + 1, 2));
        date.append(this.setLength(d.get(5), 2));
        date.append(this.setLength(d.get(11), 2));
        date.append(this.setLength(d.get(12), 2));
        date.append(this.setLength(d.get(13), 2));
        int timezone = d.get(15) / 3600000;
        if (timezone == 0) {
            date.append("Z");
        } else if (timezone < 0) {
            date.append("-");
            timezone = - timezone;
        } else {
            date.append("+");
        }
        if (timezone != 0) {
            date.append(this.setLength(timezone, 2)).append("'");
            int zone = Math.abs(d.get(15) / 60000) - timezone * 60;
            date.append(this.setLength(zone, 2)).append("'");
        }
        this.value = date.toString();
    }

    public PdfDate() {
        this(new GregorianCalendar());
    }

    private String setLength(int i, int length) {
        StringBuffer tmp = new StringBuffer();
        tmp.append(i);
        while (tmp.length() < length) {
            tmp.insert(0, "0");
        }
        tmp.setLength(length);
        return tmp.toString();
    }

    public static Calendar decode(String s) {
        try {
            GregorianCalendar calendar;
            if (s.startsWith("D:")) {
                s = s.substring(2);
            }
            int slen = s.length();
            int idx = s.indexOf(90);
            if (idx >= 0) {
                slen = idx;
                calendar = new GregorianCalendar(new SimpleTimeZone(0, "ZPDF"));
            } else {
                int sign = 1;
                idx = s.indexOf(43);
                if (idx < 0 && (idx = s.indexOf(45)) >= 0) {
                    sign = -1;
                }
                if (idx < 0) {
                    calendar = new GregorianCalendar();
                } else {
                    int offset = Integer.parseInt(s.substring(idx + 1, idx + 3)) * 60;
                    if (idx + 5 < s.length()) {
                        offset+=Integer.parseInt(s.substring(idx + 4, idx + 6));
                    }
                    calendar = new GregorianCalendar(new SimpleTimeZone(offset * sign * 60000, "ZPDF"));
                    slen = idx;
                }
            }
            calendar.clear();
            idx = 0;
            for (int k = 0; k < dateSpace.length; k+=3) {
                if (idx >= slen) break;
                calendar.set(dateSpace[k], Integer.parseInt(s.substring(idx, idx + dateSpace[k + 1])) + dateSpace[k + 2]);
                idx+=dateSpace[k + 1];
            }
            return calendar;
        }
        catch (Exception e) {
            return null;
        }
    }
}

