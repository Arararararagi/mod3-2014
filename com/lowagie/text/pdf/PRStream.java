/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncryption;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DeflaterOutputStream;

public class PRStream
extends PdfStream {
    protected PdfReader reader;
    protected int offset;
    protected int length;
    protected int objNum = 0;
    protected int objGen = 0;

    public PRStream(PRStream stream, PdfDictionary newDic) {
        this.reader = stream.reader;
        this.offset = stream.offset;
        this.length = stream.length;
        this.compressed = stream.compressed;
        this.streamBytes = stream.streamBytes;
        this.bytes = stream.bytes;
        this.objNum = stream.objNum;
        this.objGen = stream.objGen;
        if (newDic != null) {
            this.putAll(newDic);
        } else {
            this.hashMap.putAll(stream.hashMap);
        }
    }

    public PRStream(PRStream stream, PdfDictionary newDic, PdfReader reader) {
        this(stream, newDic);
        this.reader = reader;
    }

    public PRStream(PdfReader reader, int offset) {
        this.reader = reader;
        this.offset = offset;
    }

    public PRStream(PdfReader reader, byte[] conts) {
        this.reader = reader;
        this.offset = -1;
        if (Document.compress) {
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                DeflaterOutputStream zip = new DeflaterOutputStream(stream);
                zip.write(conts);
                zip.close();
                this.bytes = stream.toByteArray();
            }
            catch (IOException ioe) {
                throw new ExceptionConverter(ioe);
            }
            this.put(PdfName.FILTER, PdfName.FLATEDECODE);
        } else {
            this.bytes = conts;
        }
        this.setLength(this.bytes.length);
    }

    public void setData(byte[] data) {
        this.remove(PdfName.FILTER);
        this.offset = -1;
        if (Document.compress) {
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                DeflaterOutputStream zip = new DeflaterOutputStream(stream);
                zip.write(data);
                zip.close();
                this.bytes = stream.toByteArray();
            }
            catch (IOException ioe) {
                throw new ExceptionConverter(ioe);
            }
            this.put(PdfName.FILTER, PdfName.FLATEDECODE);
        } else {
            this.bytes = data;
        }
        this.setLength(this.bytes.length);
    }

    public void setLength(int length) {
        this.length = length;
        this.put(PdfName.LENGTH, new PdfNumber(length));
    }

    public int getOffset() {
        return this.offset;
    }

    public int getLength() {
        return this.length;
    }

    public PdfReader getReader() {
        return this.reader;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public void setObjNum(int objNum, int objGen) {
        this.objNum = objNum;
        this.objGen = objGen;
    }

    int getObjNum() {
        return this.objNum;
    }

    int getObjGen() {
        return this.objGen;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        block17 : {
            this.superToPdf(writer, os);
            os.write(PdfStream.STARTSTREAM);
            if (this.length > 0) {
                crypto = null;
                if (writer != null) {
                    crypto = writer.getEncryption();
                }
                if (this.offset < 0) {
                    if (crypto == null) {
                        os.write(this.bytes);
                    } else {
                        crypto.prepareKey();
                        buf = new byte[this.length];
                        System.arraycopy(this.bytes, 0, buf, 0, this.length);
                        crypto.encryptRC4(buf);
                        os.write(buf);
                    }
                } else {
                    buf = new byte[Math.min(this.length, 4092)];
                    file = writer.getReaderFile(this.reader);
                    isOpen = file.isOpen();
                    try {
                        file.seek(this.offset);
                        size = this.length;
                        decrypt = this.reader.getDecrypt();
                        if (decrypt != null) {
                            decrypt.setHashKey(this.objNum, this.objGen);
                            decrypt.prepareKey();
                        }
                        if (crypto != null) {
                            crypto.prepareKey();
                        }
                        while (size > 0) {
                            r = file.read(buf, 0, Math.min(size, buf.length));
                            size-=r;
                            if (decrypt != null) {
                                decrypt.encryptRC4(buf, 0, r);
                            }
                            if (crypto != null) {
                                crypto.encryptRC4(buf, 0, r);
                            }
                            os.write(buf, 0, r);
                        }
                        var10_13 = null;
                        if (isOpen) break block17;
                    }
                    catch (Throwable var11_11) {
                        var10_12 = null;
                        if (isOpen != false) throw var11_11;
                        try {
                            file.close();
                            throw var11_11;
                        }
                        catch (Exception e) {
                            // empty catch block
                        }
                        throw var11_11;
                    }
                    ** try [egrp 1[TRYBLOCK] [2 : 269->277)] { 
lbl52: // 1 sources:
                    file.close();
                }
                ** GOTO lbl56
lbl54: // 1 sources:
                catch (Exception e) {
                    // empty catch block
                }
            }
        }
        os.write(PdfStream.ENDSTREAM);
    }
}

