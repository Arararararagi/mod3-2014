/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.RandomAccessFileOrArray;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class Pfm2afm {
    private RandomAccessFileOrArray in;
    private PrintWriter out;
    private short vers;
    private int h_len;
    private String copyright;
    private short type;
    private short points;
    private short verres;
    private short horres;
    private short ascent;
    private short intleading;
    private short extleading;
    private byte italic;
    private byte uline;
    private byte overs;
    private short weight;
    private byte charset;
    private short pixwidth;
    private short pixheight;
    private byte kind;
    private short avgwidth;
    private short maxwidth;
    private int firstchar;
    private int lastchar;
    private byte defchar;
    private byte brkchar;
    private short widthby;
    private int device;
    private int face;
    private int bits;
    private int bitoff;
    private short extlen;
    private int psext;
    private int chartab;
    private int res1;
    private int kernpairs;
    private int res2;
    private int fontname;
    private short capheight;
    private short xheight;
    private short ascender;
    private short descender;
    private boolean isMono;
    private int[] Win2PSStd;
    private int[] WinClass;
    private String[] WinChars;

    private Pfm2afm(RandomAccessFileOrArray in, OutputStream out) throws IOException {
        int[] arrn = new int[256];
        arrn[4] = 197;
        arrn[5] = 198;
        arrn[6] = 199;
        arrn[8] = 202;
        arrn[10] = 205;
        arrn[11] = 206;
        arrn[12] = 207;
        arrn[32] = 32;
        arrn[33] = 33;
        arrn[34] = 34;
        arrn[35] = 35;
        arrn[36] = 36;
        arrn[37] = 37;
        arrn[38] = 38;
        arrn[39] = 169;
        arrn[40] = 40;
        arrn[41] = 41;
        arrn[42] = 42;
        arrn[43] = 43;
        arrn[44] = 44;
        arrn[45] = 45;
        arrn[46] = 46;
        arrn[47] = 47;
        arrn[48] = 48;
        arrn[49] = 49;
        arrn[50] = 50;
        arrn[51] = 51;
        arrn[52] = 52;
        arrn[53] = 53;
        arrn[54] = 54;
        arrn[55] = 55;
        arrn[56] = 56;
        arrn[57] = 57;
        arrn[58] = 58;
        arrn[59] = 59;
        arrn[60] = 60;
        arrn[61] = 61;
        arrn[62] = 62;
        arrn[63] = 63;
        arrn[64] = 64;
        arrn[65] = 65;
        arrn[66] = 66;
        arrn[67] = 67;
        arrn[68] = 68;
        arrn[69] = 69;
        arrn[70] = 70;
        arrn[71] = 71;
        arrn[72] = 72;
        arrn[73] = 73;
        arrn[74] = 74;
        arrn[75] = 75;
        arrn[76] = 76;
        arrn[77] = 77;
        arrn[78] = 78;
        arrn[79] = 79;
        arrn[80] = 80;
        arrn[81] = 81;
        arrn[82] = 82;
        arrn[83] = 83;
        arrn[84] = 84;
        arrn[85] = 85;
        arrn[86] = 86;
        arrn[87] = 87;
        arrn[88] = 88;
        arrn[89] = 89;
        arrn[90] = 90;
        arrn[91] = 91;
        arrn[92] = 92;
        arrn[93] = 93;
        arrn[94] = 94;
        arrn[95] = 95;
        arrn[96] = 193;
        arrn[97] = 97;
        arrn[98] = 98;
        arrn[99] = 99;
        arrn[100] = 100;
        arrn[101] = 101;
        arrn[102] = 102;
        arrn[103] = 103;
        arrn[104] = 104;
        arrn[105] = 105;
        arrn[106] = 106;
        arrn[107] = 107;
        arrn[108] = 108;
        arrn[109] = 109;
        arrn[110] = 110;
        arrn[111] = 111;
        arrn[112] = 112;
        arrn[113] = 113;
        arrn[114] = 114;
        arrn[115] = 115;
        arrn[116] = 116;
        arrn[117] = 117;
        arrn[118] = 118;
        arrn[119] = 119;
        arrn[120] = 120;
        arrn[121] = 121;
        arrn[122] = 122;
        arrn[123] = 123;
        arrn[124] = 124;
        arrn[125] = 125;
        arrn[126] = 126;
        arrn[127] = 127;
        arrn[130] = 184;
        arrn[132] = 185;
        arrn[133] = 188;
        arrn[134] = 178;
        arrn[135] = 179;
        arrn[136] = 94;
        arrn[137] = 189;
        arrn[139] = 172;
        arrn[140] = 234;
        arrn[145] = 96;
        arrn[147] = 170;
        arrn[148] = 186;
        arrn[150] = 177;
        arrn[151] = 208;
        arrn[152] = 126;
        arrn[155] = 173;
        arrn[156] = 250;
        arrn[161] = 161;
        arrn[162] = 162;
        arrn[163] = 163;
        arrn[164] = 168;
        arrn[165] = 165;
        arrn[167] = 167;
        arrn[168] = 200;
        arrn[170] = 227;
        arrn[171] = 171;
        arrn[180] = 194;
        arrn[182] = 182;
        arrn[183] = 180;
        arrn[184] = 203;
        arrn[186] = 235;
        arrn[187] = 187;
        arrn[191] = 191;
        arrn[198] = 225;
        arrn[216] = 233;
        arrn[223] = 251;
        arrn[230] = 241;
        arrn[248] = 249;
        this.Win2PSStd = arrn;
        int[] arrn2 = new int[256];
        arrn2[4] = 2;
        arrn2[5] = 2;
        arrn2[6] = 2;
        arrn2[8] = 2;
        arrn2[10] = 2;
        arrn2[11] = 2;
        arrn2[12] = 2;
        arrn2[32] = 1;
        arrn2[33] = 1;
        arrn2[34] = 1;
        arrn2[35] = 1;
        arrn2[36] = 1;
        arrn2[37] = 1;
        arrn2[38] = 1;
        arrn2[39] = 1;
        arrn2[40] = 1;
        arrn2[41] = 1;
        arrn2[42] = 1;
        arrn2[43] = 1;
        arrn2[44] = 1;
        arrn2[45] = 1;
        arrn2[46] = 1;
        arrn2[47] = 1;
        arrn2[48] = 1;
        arrn2[49] = 1;
        arrn2[50] = 1;
        arrn2[51] = 1;
        arrn2[52] = 1;
        arrn2[53] = 1;
        arrn2[54] = 1;
        arrn2[55] = 1;
        arrn2[56] = 1;
        arrn2[57] = 1;
        arrn2[58] = 1;
        arrn2[59] = 1;
        arrn2[60] = 1;
        arrn2[61] = 1;
        arrn2[62] = 1;
        arrn2[63] = 1;
        arrn2[64] = 1;
        arrn2[65] = 1;
        arrn2[66] = 1;
        arrn2[67] = 1;
        arrn2[68] = 1;
        arrn2[69] = 1;
        arrn2[70] = 1;
        arrn2[71] = 1;
        arrn2[72] = 1;
        arrn2[73] = 1;
        arrn2[74] = 1;
        arrn2[75] = 1;
        arrn2[76] = 1;
        arrn2[77] = 1;
        arrn2[78] = 1;
        arrn2[79] = 1;
        arrn2[80] = 1;
        arrn2[81] = 1;
        arrn2[82] = 1;
        arrn2[83] = 1;
        arrn2[84] = 1;
        arrn2[85] = 1;
        arrn2[86] = 1;
        arrn2[87] = 1;
        arrn2[88] = 1;
        arrn2[89] = 1;
        arrn2[90] = 1;
        arrn2[91] = 1;
        arrn2[92] = 1;
        arrn2[93] = 1;
        arrn2[94] = 1;
        arrn2[95] = 1;
        arrn2[96] = 1;
        arrn2[97] = 1;
        arrn2[98] = 1;
        arrn2[99] = 1;
        arrn2[100] = 1;
        arrn2[101] = 1;
        arrn2[102] = 1;
        arrn2[103] = 1;
        arrn2[104] = 1;
        arrn2[105] = 1;
        arrn2[106] = 1;
        arrn2[107] = 1;
        arrn2[108] = 1;
        arrn2[109] = 1;
        arrn2[110] = 1;
        arrn2[111] = 1;
        arrn2[112] = 1;
        arrn2[113] = 1;
        arrn2[114] = 1;
        arrn2[115] = 1;
        arrn2[116] = 1;
        arrn2[117] = 1;
        arrn2[118] = 1;
        arrn2[119] = 1;
        arrn2[120] = 1;
        arrn2[121] = 1;
        arrn2[122] = 1;
        arrn2[123] = 1;
        arrn2[124] = 1;
        arrn2[125] = 1;
        arrn2[126] = 1;
        arrn2[127] = 2;
        arrn2[130] = 2;
        arrn2[132] = 2;
        arrn2[133] = 2;
        arrn2[134] = 2;
        arrn2[135] = 2;
        arrn2[136] = 2;
        arrn2[137] = 2;
        arrn2[138] = 2;
        arrn2[139] = 2;
        arrn2[140] = 2;
        arrn2[145] = 3;
        arrn2[146] = 3;
        arrn2[147] = 2;
        arrn2[148] = 2;
        arrn2[149] = 2;
        arrn2[150] = 2;
        arrn2[151] = 2;
        arrn2[152] = 2;
        arrn2[153] = 2;
        arrn2[154] = 2;
        arrn2[155] = 2;
        arrn2[156] = 2;
        arrn2[159] = 2;
        arrn2[160] = 1;
        arrn2[161] = 1;
        arrn2[162] = 1;
        arrn2[163] = 1;
        arrn2[164] = 1;
        arrn2[165] = 1;
        arrn2[166] = 1;
        arrn2[167] = 1;
        arrn2[168] = 1;
        arrn2[169] = 1;
        arrn2[170] = 1;
        arrn2[171] = 1;
        arrn2[172] = 1;
        arrn2[173] = 1;
        arrn2[174] = 1;
        arrn2[175] = 1;
        arrn2[176] = 1;
        arrn2[177] = 1;
        arrn2[178] = 1;
        arrn2[179] = 1;
        arrn2[180] = 1;
        arrn2[181] = 1;
        arrn2[182] = 1;
        arrn2[183] = 1;
        arrn2[184] = 1;
        arrn2[185] = 1;
        arrn2[186] = 1;
        arrn2[187] = 1;
        arrn2[188] = 1;
        arrn2[189] = 1;
        arrn2[190] = 1;
        arrn2[191] = 1;
        arrn2[192] = 1;
        arrn2[193] = 1;
        arrn2[194] = 1;
        arrn2[195] = 1;
        arrn2[196] = 1;
        arrn2[197] = 1;
        arrn2[198] = 1;
        arrn2[199] = 1;
        arrn2[200] = 1;
        arrn2[201] = 1;
        arrn2[202] = 1;
        arrn2[203] = 1;
        arrn2[204] = 1;
        arrn2[205] = 1;
        arrn2[206] = 1;
        arrn2[207] = 1;
        arrn2[208] = 1;
        arrn2[209] = 1;
        arrn2[210] = 1;
        arrn2[211] = 1;
        arrn2[212] = 1;
        arrn2[213] = 1;
        arrn2[214] = 1;
        arrn2[215] = 1;
        arrn2[216] = 1;
        arrn2[217] = 1;
        arrn2[218] = 1;
        arrn2[219] = 1;
        arrn2[220] = 1;
        arrn2[221] = 1;
        arrn2[222] = 1;
        arrn2[223] = 1;
        arrn2[224] = 1;
        arrn2[225] = 1;
        arrn2[226] = 1;
        arrn2[227] = 1;
        arrn2[228] = 1;
        arrn2[229] = 1;
        arrn2[230] = 1;
        arrn2[231] = 1;
        arrn2[232] = 1;
        arrn2[233] = 1;
        arrn2[234] = 1;
        arrn2[235] = 1;
        arrn2[236] = 1;
        arrn2[237] = 1;
        arrn2[238] = 1;
        arrn2[239] = 1;
        arrn2[240] = 1;
        arrn2[241] = 1;
        arrn2[242] = 1;
        arrn2[243] = 1;
        arrn2[244] = 1;
        arrn2[245] = 1;
        arrn2[246] = 1;
        arrn2[247] = 1;
        arrn2[248] = 1;
        arrn2[249] = 1;
        arrn2[250] = 1;
        arrn2[251] = 1;
        arrn2[252] = 1;
        arrn2[253] = 1;
        arrn2[254] = 1;
        arrn2[255] = 1;
        this.WinClass = arrn2;
        this.WinChars = new String[]{"W00", "W01", "W02", "W03", "macron", "breve", "dotaccent", "W07", "ring", "W09", "W0a", "W0b", "W0c", "W0d", "W0e", "W0f", "hungarumlaut", "ogonek", "caron", "W13", "W14", "W15", "W16", "W17", "W18", "W19", "W1a", "W1b", "W1c", "W1d", "W1e", "W1f", "space", "exclam", "quotedbl", "numbersign", "dollar", "percent", "ampersand", "quotesingle", "parenleft", "parenright", "asterisk", "plus", "comma", "hyphen", "period", "slash", "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "colon", "semicolon", "less", "equal", "greater", "question", "at", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "bracketleft", "backslash", "bracketright", "asciicircum", "underscore", "grave", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "braceleft", "bar", "braceright", "asciitilde", "W7f", "W80", "W81", "quotesinglbase", "W83", "quotedblbase", "ellipsis", "dagger", "daggerdbl", "asciicircum", "perthousand", "Scaron", "guilsinglleft", "OE", "W8d", "W8e", "W8f", "W90", "quoteleft", "quoteright", "quotedblleft", "quotedblright", "bullet1", "endash", "emdash", "asciitilde", "trademark", "scaron", "guilsinglright", "oe", "W9d", "W9e", "Ydieresis", "reqspace", "exclamdown", "cent", "sterling", "currency", "yen", "brokenbar", "section", "dieresis", "copyright", "ordfeminine", "guillemotleft", "logicalnot", "syllable", "registered", "overbar", "degree", "plusminus", "twosuperior", "threesuperior", "acute", "mu", "paragraph", "periodcentered", "cedilla", "onesuperior", "ordmasculine", "guillemotright", "onequarter", "onehalf", "threequarters", "questiondown", "Agrave", "Aacute", "Acircumflex", "Atilde", "Adieresis", "Aring", "AE", "Ccedilla", "Egrave", "Eacute", "Ecircumflex", "Edieresis", "Igrave", "Iacute", "Icircumflex", "Idieresis", "Eth", "Ntilde", "Ograve", "Oacute", "Ocircumflex", "Otilde", "Odieresis", "multiply", "Oslash", "Ugrave", "Uacute", "Ucircumflex", "Udieresis", "Yacute", "Thorn", "germandbls", "agrave", "aacute", "acircumflex", "atilde", "adieresis", "aring", "ae", "ccedilla", "egrave", "eacute", "ecircumflex", "edieresis", "igrave", "iacute", "icircumflex", "idieresis", "eth", "ntilde", "ograve", "oacute", "ocircumflex", "otilde", "odieresis", "divide", "oslash", "ugrave", "uacute", "ucircumflex", "udieresis", "yacute", "thorn", "ydieresis"};
        this.in = in;
        this.out = new PrintWriter(new OutputStreamWriter(out, "ISO-8859-1"));
    }

    public static void convert(RandomAccessFileOrArray in, OutputStream out) throws IOException {
        Pfm2afm p = new Pfm2afm(in, out);
        p.openpfm();
        p.putheader();
        p.putchartab();
        p.putkerntab();
        p.puttrailer();
        p.out.flush();
    }

    public static void main(String[] args) {
        try {
            RandomAccessFileOrArray in = new RandomAccessFileOrArray(args[0]);
            FileOutputStream out = new FileOutputStream(args[1]);
            Pfm2afm.convert(in, out);
            in.close();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readString(int n) throws IOException {
        int k;
        byte[] b = new byte[n];
        this.in.readFully(b);
        for (k = 0; k < b.length; ++k) {
            if (b[k] == 0) break;
        }
        return new String(b, 0, k, "ISO-8859-1");
    }

    private String readString() throws IOException {
        int c;
        StringBuffer buf = new StringBuffer();
        while ((c = this.in.read()) > 0) {
            buf.append((char)c);
        }
        return buf.toString();
    }

    private void outval(int n) {
        this.out.print(' ');
        this.out.print(n);
    }

    private void outchar(int code, int width, String name) {
        this.out.print("C ");
        this.outval(code);
        this.out.print(" ; WX ");
        this.outval(width);
        if (name != null) {
            this.out.print(" ; N ");
            this.out.print(name);
        }
        this.out.print(" ;\n");
    }

    private void openpfm() throws IOException {
        this.in.seek(0);
        this.vers = this.in.readShortLE();
        this.h_len = this.in.readIntLE();
        this.copyright = this.readString(60);
        this.type = this.in.readShortLE();
        this.points = this.in.readShortLE();
        this.verres = this.in.readShortLE();
        this.horres = this.in.readShortLE();
        this.ascent = this.in.readShortLE();
        this.intleading = this.in.readShortLE();
        this.extleading = this.in.readShortLE();
        this.italic = (byte)this.in.read();
        this.uline = (byte)this.in.read();
        this.overs = (byte)this.in.read();
        this.weight = this.in.readShortLE();
        this.charset = (byte)this.in.read();
        this.pixwidth = this.in.readShortLE();
        this.pixheight = this.in.readShortLE();
        this.kind = (byte)this.in.read();
        this.avgwidth = this.in.readShortLE();
        this.maxwidth = this.in.readShortLE();
        this.firstchar = this.in.read();
        this.lastchar = this.in.read();
        this.defchar = (byte)this.in.read();
        this.brkchar = (byte)this.in.read();
        this.widthby = this.in.readShortLE();
        this.device = this.in.readIntLE();
        this.face = this.in.readIntLE();
        this.bits = this.in.readIntLE();
        this.bitoff = this.in.readIntLE();
        this.extlen = this.in.readShortLE();
        this.psext = this.in.readIntLE();
        this.chartab = this.in.readIntLE();
        this.res1 = this.in.readIntLE();
        this.kernpairs = this.in.readIntLE();
        this.res2 = this.in.readIntLE();
        this.fontname = this.in.readIntLE();
        if (this.h_len != this.in.length() || this.extlen != 30 || this.fontname < 75 || this.fontname > 512) {
            throw new IOException("Not a valid PFM file.");
        }
        this.in.seek(this.psext + 14);
        this.capheight = this.in.readShortLE();
        this.xheight = this.in.readShortLE();
        this.ascender = this.in.readShortLE();
        this.descender = this.in.readShortLE();
    }

    private void putheader() throws IOException {
        this.out.print("StartFontMetrics 2.0\n");
        if (this.copyright.length() > 0) {
            this.out.print("Comment " + this.copyright + '\n');
        }
        this.out.print("FontName ");
        this.in.seek(this.fontname);
        String fname = this.readString();
        this.out.print(fname);
        this.out.print("\nEncodingScheme ");
        if (this.charset != 0) {
            this.out.print("FontSpecific\n");
        } else {
            this.out.print("AdobeStandardEncoding\n");
        }
        this.out.print("FullName " + fname.replace('-', ' '));
        if (this.face != 0) {
            this.in.seek(this.face);
            this.out.print("\nFamilyName " + this.readString());
        }
        this.out.print("\nWeight ");
        if (this.weight > 475 || fname.toLowerCase().indexOf("bold") >= 0) {
            this.out.print("Bold");
        } else if (this.weight < 325 && this.weight != 0 || fname.toLowerCase().indexOf("light") >= 0) {
            this.out.print("Light");
        } else if (fname.toLowerCase().indexOf("black") >= 0) {
            this.out.print("Black");
        } else {
            this.out.print("Medium");
        }
        this.out.print("\nItalicAngle ");
        if (this.italic != 0 || fname.toLowerCase().indexOf("italic") >= 0) {
            this.out.print("-12.00");
        } else {
            this.out.print("0");
        }
        this.out.print("\nIsFixedPitch ");
        if ((this.kind & 1) == 0 || this.avgwidth == this.maxwidth) {
            this.out.print("true");
            this.isMono = true;
        } else {
            this.out.print("false");
            this.isMono = false;
        }
        this.out.print("\nFontBBox");
        if (this.isMono) {
            this.outval(-20);
        } else {
            this.outval(-100);
        }
        this.outval(- this.descender + 5);
        this.outval(this.maxwidth + 10);
        this.outval(this.ascent + 5);
        this.out.print("\nCapHeight");
        this.outval(this.capheight);
        this.out.print("\nXHeight");
        this.outval(this.xheight);
        this.out.print("\nDescender");
        this.outval(this.descender);
        this.out.print("\nAscender");
        this.outval(this.ascender);
        this.out.print('\n');
    }

    private void putchartab() throws IOException {
        int i;
        int count = this.lastchar - this.firstchar + 1;
        int[] ctabs = new int[count];
        this.in.seek(this.chartab);
        for (int k = 0; k < count; ++k) {
            ctabs[k] = this.in.readUnsignedShortLE();
        }
        int[] back = new int[256];
        if (this.charset == 0) {
            for (i = this.firstchar; i <= this.lastchar; ++i) {
                if (this.Win2PSStd[i] == 0) continue;
                back[this.Win2PSStd[i]] = i;
            }
        }
        this.out.print("StartCharMetrics");
        this.outval(count);
        this.out.print('\n');
        if (this.charset != 0) {
            for (i = this.firstchar; i <= this.lastchar; ++i) {
                if (ctabs[i - this.firstchar] == 0) continue;
                this.outchar(i, ctabs[i - this.firstchar], null);
            }
        } else {
            for (i = 0; i < 256; ++i) {
                int j = back[i];
                if (j == 0) continue;
                this.outchar(i, ctabs[j - this.firstchar], this.WinChars[j]);
                ctabs[j - this.firstchar] = 0;
            }
            for (i = this.firstchar; i <= this.lastchar; ++i) {
                if (ctabs[i - this.firstchar] == 0) continue;
                this.outchar(-1, ctabs[i - this.firstchar], this.WinChars[i]);
            }
        }
        this.out.print("EndCharMetrics\n");
    }

    private void putkerntab() throws IOException {
        if (this.kernpairs == 0) {
            return;
        }
        this.in.seek(this.kernpairs);
        int count = this.in.readUnsignedShortLE();
        int nzero = 0;
        int[] kerns = new int[count * 3];
        int k = 0;
        while (k < kerns.length) {
            kerns[k++] = this.in.read();
            kerns[k++] = this.in.read();
            int n = k++;
            short s = this.in.readShortLE();
            kerns[n] = s;
            if (s == 0) continue;
            ++nzero;
        }
        if (nzero == 0) {
            return;
        }
        this.out.print("StartKernData\nStartKernPairs");
        this.outval(nzero);
        this.out.print('\n');
        for (k = 0; k < kerns.length; k+=3) {
            if (kerns[k + 2] == 0) continue;
            this.out.print("KPX ");
            this.out.print(this.WinChars[kerns[k]]);
            this.out.print(' ');
            this.out.print(this.WinChars[kerns[k + 1]]);
            this.outval(kerns[k + 2]);
            this.out.print('\n');
        }
        this.out.print("EndKernPairs\nEndKernData\n");
    }

    private void puttrailer() {
        this.out.print("EndFontMetrics\n");
    }
}

