/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocWriter;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.ByteBuffer;
import com.lowagie.text.pdf.FdfReader;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.OutputStreamCounter;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PageResources;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfAppearance;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfContents;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfEncryption;
import com.lowagie.text.pdf.PdfException;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNameTree;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfReaderInstance;
import com.lowagie.text.pdf.PdfRectangle;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfTransition;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.SimpleBookmark;
import com.lowagie.text.pdf.StampContent;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class PdfStamperImp
extends PdfWriter {
    HashMap readers2intrefs = new HashMap();
    HashMap readers2file = new HashMap();
    RandomAccessFileOrArray file;
    PdfReader reader;
    IntHashtable myXref = new IntHashtable();
    HashMap pagesToContent = new HashMap();
    boolean closed = false;
    private boolean rotateContents = true;
    protected AcroFields acroFields;
    protected boolean flat = false;
    protected int[] namePtr = new int[1];
    protected boolean namedAsNames;
    protected List newBookmarks;
    protected HashSet partialFlattening = new HashSet();
    protected boolean useVp = false;
    protected int vp = 0;
    protected HashMap fieldTemplates = new HashMap();
    protected boolean fieldsAdded = false;
    protected int sigFlags = 0;
    protected boolean append;
    protected IntHashtable marked;
    protected int initialXrefSize;
    protected PdfAction openAction;

    PdfStamperImp(PdfReader reader, OutputStream os, char pdfVersion, boolean append) throws DocumentException, IOException {
        super(new PdfDocument(), os);
        if (reader.isTampered()) {
            throw new DocumentException("The original document was reused. Read it again from file.");
        }
        reader.setTampered(true);
        this.reader = reader;
        this.file = reader.getSafeFile();
        this.append = append;
        if (append) {
            int n;
            if (reader.isRebuilt()) {
                throw new DocumentException("Append mode requires a document without errors even if recovery was possible.");
            }
            if (reader.isEncrypted()) {
                throw new DocumentException("Append mode requires a document without encryption.");
            }
            this.HEADER = DocWriter.getISOBytes("\n");
            this.file.reOpen();
            byte[] buf = new byte[8192];
            while ((n = this.file.read(buf)) > 0) {
                this.os.write(buf, 0, n);
            }
            this.file.close();
            this.prevxref = reader.getLastXref();
            reader.setAppendable(true);
        } else if (pdfVersion == '\u0000') {
            super.setPdfVersion(reader.getPdfVersion());
        } else {
            super.setPdfVersion(pdfVersion);
        }
        super.open();
        this.pdf.addWriter(this);
        if (append) {
            this.body.setRefnum(reader.getXrefSize());
            this.marked = new IntHashtable();
            if (reader.isNewXrefType()) {
                this.fullCompression = true;
            }
            if (reader.isHybridXref()) {
                this.fullCompression = false;
            }
        }
        this.initialXrefSize = reader.getXrefSize();
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    void close(HashMap moreInfo) throws DocumentException, IOException {
        if (this.closed) {
            return;
        }
        if (this.useVp) {
            this.reader.setViewerPreferences(this.vp);
            this.markUsed(this.reader.getTrailer().get(PdfName.ROOT));
        }
        if (this.flat) {
            this.flatFields();
        }
        this.addFieldResources();
        if (this.sigFlags != 0 && (acroForm = (PdfDictionary)PdfReader.getPdfObject(this.reader.getCatalog().get(PdfName.ACROFORM), this.reader.getCatalog())) != null) {
            acroForm.put(PdfName.SIGFLAGS, new PdfNumber(this.sigFlags));
            this.markUsed(acroForm);
        }
        this.closed = true;
        this.addSharedObjectsToBody();
        this.setOutlines();
        this.setJavaScript();
        if (this.openAction != null) {
            this.reader.getCatalog().put(PdfName.OPENACTION, this.openAction);
        }
        iInfo = null;
        try {
            this.file.reOpen();
            this.alterContents();
            idx = true;
            iInfo = (PRIndirectReference)this.reader.trailer.get(PdfName.INFO);
            skip = -1;
            if (iInfo != null) {
                skip = iInfo.getNumber();
            }
            rootN = ((PRIndirectReference)this.reader.trailer.get(PdfName.ROOT)).getNumber();
            if (!this.append) ** GOTO lbl32
            keys = this.marked.getKeys();
            k = 0;
            ** GOTO lbl39
lbl32: // 1 sources:
            k = 1;
            ** GOTO lbl50
lbl-1000: // 1 sources:
            {
                j = keys[k];
                obj = this.reader.getPdfObjectRelease(j);
                if (obj != null && skip != j && j < this.initialXrefSize) {
                    this.addToBody(obj, j, j != rootN);
                }
                ++k;
lbl39: // 2 sources:
                ** while (k < keys.length)
            }
lbl40: // 3 sources:
            for (k = this.initialXrefSize; k < this.reader.getXrefSize(); ++k) {
                obj = this.reader.getPdfObject(k);
                if (obj == null) continue;
                this.addToBody(obj, this.getNewObjectNumber(this.reader, k, 0));
            }
            ** GOTO lbl52
lbl-1000: // 1 sources:
            {
                obj = this.reader.getPdfObjectRelease(k);
                if (obj != null && skip != k) {
                    this.addToBody(obj, this.getNewObjectNumber(this.reader, k, 0), k != rootN);
                }
                ++k;
lbl50: // 2 sources:
                ** while (k < this.reader.getXrefSize())
            }
lbl51: // 1 sources:
lbl52: // 2 sources:
            var10_21 = null;
        }
        catch (Throwable var11_18) {
            var10_20 = null;
            try {
                this.file.close();
                throw var11_18;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var11_18;
        }
        ** try [egrp 1[TRYBLOCK] [2 : 469->479)] { 
lbl64: // 1 sources:
        this.file.close();
        ** GOTO lbl68
lbl66: // 1 sources:
        catch (Exception e) {
            // empty catch block
        }
lbl68: // 2 sources:
        encryption = null;
        fileID = null;
        if (this.crypto != null) {
            encryptionObject = this.addToBody((PdfObject)this.crypto.getEncryptionDictionary(), false);
            encryption = encryptionObject.getIndirectReference();
            fileID = this.crypto.getFileID();
        }
        iRoot = (PRIndirectReference)this.reader.trailer.get(PdfName.ROOT);
        root = new PdfIndirectReference(0, this.getNewObjectNumber(this.reader, iRoot.getNumber(), 0));
        info = null;
        oldInfo = (PdfDictionary)PdfReader.getPdfObject(iInfo);
        newInfo = new PdfDictionary();
        if (oldInfo != null) {
            i = oldInfo.getKeys().iterator();
            while (i.hasNext()) {
                key = (PdfName)i.next();
                value = PdfReader.getPdfObject(oldInfo.get((PdfName)key));
                newInfo.put((PdfName)key, value);
            }
        }
        if (moreInfo != null) {
            i = moreInfo.keySet().iterator();
            while (i.hasNext()) {
                key = (String)i.next();
                keyName = new PdfName((String)key);
                value = (String)moreInfo.get(key);
                if (value == null) {
                    newInfo.remove(keyName);
                    continue;
                }
                newInfo.put(keyName, new PdfString(value, "UnicodeBig"));
            }
        }
        if (this.append) {
            info = iInfo == null ? this.addToBody((PdfObject)newInfo, false).getIndirectReference() : this.addToBody((PdfObject)newInfo, iInfo.getNumber(), false).getIndirectReference();
        } else if (!newInfo.getKeys().isEmpty()) {
            info = this.addToBody((PdfObject)newInfo, false).getIndirectReference();
        }
        this.body.writeCrossReferenceTable(this.os, root, info, encryption, fileID, this.prevxref);
        if (this.fullCompression) {
            this.os.write(DocWriter.getISOBytes("startxref\n"));
            this.os.write(DocWriter.getISOBytes(String.valueOf(this.body.offset())));
            this.os.write(DocWriter.getISOBytes("\n%%EOF\n"));
        } else {
            trailer = new PdfWriter.PdfTrailer(this.body.size(), this.body.offset(), root, info, encryption, fileID, this.prevxref);
            trailer.toPdf(this, this.os);
        }
        this.os.flush();
        if (this.isCloseStream()) {
            this.os.close();
        }
        this.reader.close();
    }

    void applyRotation(PdfDictionary pageN, ByteBuffer out) {
        if (!this.rotateContents) {
            return;
        }
        Rectangle page = this.reader.getPageSizeWithRotation(pageN);
        int rotation = page.getRotation();
        switch (rotation) {
            case 90: {
                out.append(PdfContents.ROTATE90);
                out.append(page.top());
                out.append(' ').append('0').append(PdfContents.ROTATEFINAL);
                break;
            }
            case 180: {
                out.append(PdfContents.ROTATE180);
                out.append(page.right());
                out.append(' ');
                out.append(page.top());
                out.append(PdfContents.ROTATEFINAL);
                break;
            }
            case 270: {
                out.append(PdfContents.ROTATE270);
                out.append('0').append(' ');
                out.append(page.right());
                out.append(PdfContents.ROTATEFINAL);
            }
        }
    }

    void alterContents() throws IOException {
        Iterator i = this.pagesToContent.values().iterator();
        while (i.hasNext()) {
            PageStamp ps = (PageStamp)i.next();
            PdfDictionary pageN = ps.pageN;
            this.markUsed(pageN);
            PdfArray ar = null;
            PdfObject content = PdfReader.getPdfObject(pageN.get(PdfName.CONTENTS), pageN);
            if (content == null) {
                ar = new PdfArray();
                pageN.put(PdfName.CONTENTS, ar);
            } else if (content.isArray()) {
                ar = (PdfArray)content;
                this.markUsed(ar);
            } else if (content.isStream()) {
                ar = new PdfArray();
                ar.add(pageN.get(PdfName.CONTENTS));
                pageN.put(PdfName.CONTENTS, ar);
            } else {
                ar = new PdfArray();
                pageN.put(PdfName.CONTENTS, ar);
            }
            ByteBuffer out = new ByteBuffer();
            if (ps.under != null) {
                out.append(PdfContents.SAVESTATE);
                this.applyRotation(pageN, out);
                out.append(ps.under.getInternalBuffer());
                out.append(PdfContents.RESTORESTATE);
            }
            if (ps.over != null) {
                out.append(PdfContents.SAVESTATE);
            }
            PdfStream stream = new PdfStream(out.toByteArray());
            try {
                stream.flateCompress();
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
            ar.addFirst(this.addToBody(stream).getIndirectReference());
            out.reset();
            if (ps.over != null) {
                out.append(' ');
                out.append(PdfContents.RESTORESTATE);
                out.append(PdfContents.SAVESTATE);
                this.applyRotation(pageN, out);
                out.append(ps.over.getInternalBuffer());
                out.append(PdfContents.RESTORESTATE);
                stream = new PdfStream(out.toByteArray());
                try {
                    stream.flateCompress();
                }
                catch (Exception e) {
                    throw new ExceptionConverter(e);
                }
                ar.add(this.addToBody(stream).getIndirectReference());
            }
            this.alterResources(ps);
        }
    }

    void alterResources(PageStamp ps) {
        ps.pageN.put(PdfName.RESOURCES, ps.pageResources.getResources());
    }

    protected int getNewObjectNumber(PdfReader reader, int number, int generation) {
        IntHashtable ref = (IntHashtable)this.readers2intrefs.get(reader);
        if (ref != null) {
            int n = ref.get(number);
            if (n == 0) {
                n = this.getIndirectReferenceNumber();
                ref.put(number, n);
            }
            return n;
        }
        if (this.currentPdfReaderInstance == null) {
            if (this.append && number < this.initialXrefSize) {
                return number;
            }
            int n = this.myXref.get(number);
            if (n == 0) {
                n = this.getIndirectReferenceNumber();
                this.myXref.put(number, n);
            }
            return n;
        }
        return this.currentPdfReaderInstance.getNewObjectNumber(number, generation);
    }

    RandomAccessFileOrArray getReaderFile(PdfReader reader) {
        if (this.readers2intrefs.containsKey(reader)) {
            RandomAccessFileOrArray raf = (RandomAccessFileOrArray)this.readers2file.get(reader);
            if (raf != null) {
                return raf;
            }
            return reader.getSafeFile();
        }
        if (this.currentPdfReaderInstance == null) {
            return this.file;
        }
        return this.currentPdfReaderInstance.getReaderFile();
    }

    public void registerReader(PdfReader reader, boolean openFile) throws IOException {
        if (this.readers2intrefs.containsKey(reader)) {
            return;
        }
        this.readers2intrefs.put(reader, new IntHashtable());
        if (openFile) {
            RandomAccessFileOrArray raf = reader.getSafeFile();
            this.readers2file.put(reader, raf);
            raf.reOpen();
        }
    }

    public void unRegisterReader(PdfReader reader) {
        if (!this.readers2intrefs.containsKey(reader)) {
            return;
        }
        this.readers2intrefs.remove(reader);
        RandomAccessFileOrArray raf = (RandomAccessFileOrArray)this.readers2file.get(reader);
        if (raf == null) {
            return;
        }
        this.readers2file.remove(reader);
        try {
            raf.close();
        }
        catch (Exception var3_3) {
            // empty catch block
        }
    }

    static void findAllObjects(PdfReader reader, PdfObject obj, IntHashtable hits) {
        if (obj == null) {
            return;
        }
        switch (obj.type()) {
            case 10: {
                PRIndirectReference iref = (PRIndirectReference)obj;
                if (reader != iref.getReader()) {
                    return;
                }
                if (hits.containsKey(iref.getNumber())) {
                    return;
                }
                hits.put(iref.getNumber(), 1);
                PdfStamperImp.findAllObjects(reader, PdfReader.getPdfObject(obj), hits);
                return;
            }
            case 5: {
                ArrayList lst = ((PdfArray)obj).getArrayList();
                for (int k = 0; k < lst.size(); ++k) {
                    PdfStamperImp.findAllObjects(reader, (PdfObject)lst.get(k), hits);
                }
                return;
            }
            case 6: 
            case 7: {
                PdfDictionary dic = (PdfDictionary)obj;
                Iterator it = dic.getKeys().iterator();
                while (it.hasNext()) {
                    PdfName name = (PdfName)it.next();
                    PdfStamperImp.findAllObjects(reader, dic.get(name), hits);
                }
                return;
            }
        }
    }

    public void addComments(FdfReader fdf) throws IOException {
        int k;
        if (this.readers2intrefs.containsKey(fdf)) {
            return;
        }
        PdfDictionary catalog = fdf.getCatalog();
        if ((catalog = (PdfDictionary)PdfReader.getPdfObject(catalog.get(PdfName.FDF))) == null) {
            return;
        }
        PdfArray annots = (PdfArray)PdfReader.getPdfObject(catalog.get(PdfName.ANNOTS));
        if (annots == null || annots.size() == 0) {
            return;
        }
        this.registerReader(fdf, false);
        IntHashtable hits = new IntHashtable();
        HashMap<String, PdfObject> irt = new HashMap<String, PdfObject>();
        ArrayList<PdfObject> an = new ArrayList<PdfObject>();
        ArrayList ar = annots.getArrayList();
        for (int k2 = 0; k2 < ar.size(); ++k2) {
            PdfObject nm;
            PdfObject obj = (PdfObject)ar.get(k2);
            PdfDictionary annot = (PdfDictionary)PdfReader.getPdfObject(obj);
            PdfNumber page = (PdfNumber)PdfReader.getPdfObject(annot.get(PdfName.PAGE));
            if (page == null) continue;
            if (page.intValue() >= this.reader.getNumberOfPages()) continue;
            PdfStamperImp.findAllObjects(fdf, obj, hits);
            an.add(obj);
            if (obj.type() != 10 || (nm = PdfReader.getPdfObject(annot.get(PdfName.NM))) == null || nm.type() != 3) continue;
            irt.put(nm.toString(), obj);
        }
        int[] arhits = hits.getKeys();
        for (k = 0; k < arhits.length; ++k) {
            PdfObject str;
            PdfObject i;
            int n = arhits[k];
            PdfObject obj = fdf.getPdfObject(n);
            if (obj.type() == 6 && (str = PdfReader.getPdfObject(((PdfDictionary)obj).get(PdfName.IRT))) != null && str.type() == 3 && (i = (PdfObject)irt.get(str.toString())) != null) {
                PdfDictionary dic2 = new PdfDictionary();
                dic2.merge((PdfDictionary)obj);
                dic2.put(PdfName.IRT, i);
                obj = dic2;
            }
            this.addToBody(obj, this.getNewObjectNumber(fdf, n, 0));
        }
        for (k = 0; k < an.size(); ++k) {
            PdfObject obj = (PdfObject)an.get(k);
            PdfDictionary annot = (PdfDictionary)PdfReader.getPdfObject(obj);
            PdfNumber page = (PdfNumber)PdfReader.getPdfObject(annot.get(PdfName.PAGE));
            PdfDictionary dic = this.reader.getPageN(page.intValue() + 1);
            PdfArray annotsp = (PdfArray)PdfReader.getPdfObject(dic.get(PdfName.ANNOTS), dic);
            if (annotsp == null) {
                annotsp = new PdfArray();
                dic.put(PdfName.ANNOTS, annotsp);
                this.markUsed(dic);
            }
            this.markUsed(annotsp);
            annotsp.add(obj);
        }
    }

    PageStamp getPageStamp(int pageNum) {
        PdfDictionary pageN = this.reader.getPageN(pageNum);
        PageStamp ps = (PageStamp)this.pagesToContent.get(pageN);
        if (ps == null) {
            ps = new PageStamp(this, this.reader, pageN);
            this.pagesToContent.put(pageN, ps);
        }
        return ps;
    }

    PdfContentByte getUnderContent(int pageNum) {
        if (pageNum < 1 || pageNum > this.reader.getNumberOfPages()) {
            return null;
        }
        PageStamp ps = this.getPageStamp(pageNum);
        if (ps.under == null) {
            ps.under = new StampContent(this, ps);
        }
        return ps.under;
    }

    PdfContentByte getOverContent(int pageNum) {
        if (pageNum < 1 || pageNum > this.reader.getNumberOfPages()) {
            return null;
        }
        PageStamp ps = this.getPageStamp(pageNum);
        if (ps.over == null) {
            ps.over = new StampContent(this, ps);
        }
        return ps.over;
    }

    void insertPage(int pageNumber, Rectangle mediabox) {
        PdfDictionary parent;
        PRIndirectReference parentRef;
        Rectangle media = new Rectangle(mediabox);
        int rotation = media.getRotation() % 360;
        PdfDictionary page = new PdfDictionary(PdfName.PAGE);
        PdfDictionary resources = new PdfDictionary();
        PdfArray procset = new PdfArray();
        procset.add(PdfName.PDF);
        procset.add(PdfName.TEXT);
        procset.add(PdfName.IMAGEB);
        procset.add(PdfName.IMAGEC);
        procset.add(PdfName.IMAGEI);
        resources.put(PdfName.PROCSET, procset);
        page.put(PdfName.RESOURCES, resources);
        page.put(PdfName.ROTATE, new PdfNumber(rotation));
        page.put(PdfName.MEDIABOX, new PdfRectangle(media, rotation));
        PRIndirectReference pref = this.reader.addPdfObject(page);
        if (pageNumber > this.reader.getNumberOfPages()) {
            PdfDictionary lastPage = this.reader.getPageNRelease(this.reader.getNumberOfPages());
            parentRef = (PRIndirectReference)lastPage.get(PdfName.PARENT);
            parentRef = new PRIndirectReference(this.reader, parentRef.getNumber());
            parent = (PdfDictionary)PdfReader.getPdfObject(parentRef);
            PdfArray kids = (PdfArray)PdfReader.getPdfObject(parent.get(PdfName.KIDS), parent);
            kids.add(pref);
            this.markUsed(kids);
            this.reader.pageRefs.insertPage(pageNumber, pref);
        } else {
            if (pageNumber < 1) {
                pageNumber = 1;
            }
            PdfDictionary firstPage = this.reader.getPageN(pageNumber);
            PRIndirectReference firstPageRef = this.reader.getPageOrigRef(pageNumber);
            this.reader.releasePage(pageNumber);
            parentRef = (PRIndirectReference)firstPage.get(PdfName.PARENT);
            parentRef = new PRIndirectReference(this.reader, parentRef.getNumber());
            parent = (PdfDictionary)PdfReader.getPdfObject(parentRef);
            PdfArray kids = (PdfArray)PdfReader.getPdfObject(parent.get(PdfName.KIDS), parent);
            ArrayList ar = kids.getArrayList();
            int len = ar.size();
            int num = firstPageRef.getNumber();
            for (int k = 0; k < len; ++k) {
                PRIndirectReference cur = (PRIndirectReference)ar.get(k);
                if (num != cur.getNumber()) continue;
                ar.add(k, pref);
                break;
            }
            if (len == ar.size()) {
                throw new RuntimeException("Internal inconsistence.");
            }
            this.markUsed(kids);
            this.reader.pageRefs.insertPage(pageNumber, pref);
        }
        page.put(PdfName.PARENT, parentRef);
        while (parent != null) {
            this.markUsed(parent);
            PdfNumber count = (PdfNumber)PdfReader.getPdfObjectRelease(parent.get(PdfName.COUNT));
            parent.put(PdfName.COUNT, new PdfNumber(count.intValue() + 1));
            parent = (PdfDictionary)PdfReader.getPdfObject(parent.get(PdfName.PARENT));
        }
    }

    boolean isRotateContents() {
        return this.rotateContents;
    }

    void setRotateContents(boolean rotateContents) {
        this.rotateContents = rotateContents;
    }

    boolean isContentWritten() {
        if (this.body.size() > 1) {
            return true;
        }
        return false;
    }

    AcroFields getAcroFields() {
        if (this.acroFields == null) {
            this.acroFields = new AcroFields(this.reader, this);
        }
        return this.acroFields;
    }

    void setFormFlattening(boolean flat) {
        this.flat = flat;
    }

    boolean partialFormFlattening(String name) {
        this.getAcroFields();
        if (!this.acroFields.getFields().containsKey(name)) {
            return false;
        }
        this.partialFlattening.add(name);
        return true;
    }

    void flatFields() {
        PdfArray array;
        if (this.append) {
            throw new IllegalArgumentException("Field flattening is not supported in append mode.");
        }
        this.getAcroFields();
        HashMap fields = this.acroFields.getFields();
        if (this.fieldsAdded && this.partialFlattening.isEmpty()) {
            Iterator i = fields.keySet().iterator();
            while (i.hasNext()) {
                this.partialFlattening.add(i.next());
            }
        }
        PdfDictionary acroForm = (PdfDictionary)PdfReader.getPdfObject(this.reader.getCatalog().get(PdfName.ACROFORM));
        ArrayList acroFds = null;
        if (acroForm != null && (array = (PdfArray)PdfReader.getPdfObject(acroForm.get(PdfName.FIELDS), acroForm)) != null) {
            acroFds = array.getArrayList();
        }
        Iterator i = fields.keySet().iterator();
        while (i.hasNext()) {
            String name = (String)i.next();
            if (!this.partialFlattening.isEmpty() && !this.partialFlattening.contains(name)) continue;
            AcroFields.Item item = (AcroFields.Item)fields.get(name);
            for (int k = 0; k < item.merged.size(); ++k) {
                PdfDictionary merged = (PdfDictionary)item.merged.get(k);
                PdfNumber ff = (PdfNumber)PdfReader.getPdfObject(merged.get(PdfName.F));
                int flags = 0;
                if (ff != null) {
                    flags = ff.intValue();
                }
                int page = (Integer)item.page.get(k);
                PdfDictionary appDic = (PdfDictionary)PdfReader.getPdfObject(merged.get(PdfName.AP));
                if (appDic != null && (flags & 4) != 0 && (flags & 2) == 0) {
                    PdfName as;
                    PdfIndirectReference iref;
                    PdfObject obj = appDic.get(PdfName.N);
                    PdfAppearance app = null;
                    PdfObject objReal = PdfReader.getPdfObject(obj);
                    if (obj instanceof PdfIndirectReference && !obj.isIndirect()) {
                        app = new PdfAppearance((PdfIndirectReference)obj);
                    } else if (objReal instanceof PdfStream) {
                        ((PdfDictionary)objReal).put(PdfName.SUBTYPE, PdfName.FORM);
                        app = new PdfAppearance((PdfIndirectReference)obj);
                    } else if (objReal.isDictionary() && (as = (PdfName)PdfReader.getPdfObject(merged.get(PdfName.AS))) != null && (iref = (PdfIndirectReference)((PdfDictionary)objReal).get(as)) != null) {
                        app = new PdfAppearance(iref);
                        if (iref.isIndirect()) {
                            objReal = PdfReader.getPdfObject(iref);
                            ((PdfDictionary)objReal).put(PdfName.SUBTYPE, PdfName.FORM);
                        }
                    }
                    if (app != null) {
                        Rectangle box = PdfReader.getNormalizedRectangle((PdfArray)PdfReader.getPdfObject(merged.get(PdfName.RECT)));
                        PdfContentByte cb = this.getOverContent(page);
                        cb.setLiteral("Q ");
                        cb.addTemplate(app, box.left(), box.bottom());
                        cb.setLiteral("q ");
                    }
                }
                if (this.partialFlattening.isEmpty()) continue;
                PdfDictionary pageDic = this.reader.getPageN(page);
                PdfArray annots = (PdfArray)PdfReader.getPdfObject(pageDic.get(PdfName.ANNOTS));
                if (annots == null) continue;
                ArrayList ar = annots.getArrayList();
                block3 : for (int idx = 0; idx < ar.size(); ++idx) {
                    PdfObject ran2;
                    PdfObject ran = (PdfObject)ar.get(idx);
                    if (!ran.isIndirect() || !(ran2 = (PdfObject)item.widget_refs.get(k)).isIndirect() || ((PRIndirectReference)ran).getNumber() != ((PRIndirectReference)ran2).getNumber()) continue;
                    ar.remove(idx--);
                    PRIndirectReference wdref = (PRIndirectReference)ran2;
                    do {
                        PdfDictionary wd = (PdfDictionary)PdfReader.getPdfObject(wdref);
                        PRIndirectReference parentRef = (PRIndirectReference)wd.get(PdfName.PARENT);
                        PdfReader.killIndirect(wdref);
                        if (parentRef == null) {
                            for (int fr = 0; fr < acroFds.size(); ++fr) {
                                PdfObject h = (PdfObject)acroFds.get(fr);
                                if (!h.isIndirect() || ((PRIndirectReference)h).getNumber() != wdref.getNumber()) continue;
                                acroFds.remove(fr);
                                --fr;
                            }
                            continue block3;
                        }
                        PdfDictionary parent = (PdfDictionary)PdfReader.getPdfObject(parentRef);
                        PdfArray kids = (PdfArray)PdfReader.getPdfObject(parent.get(PdfName.KIDS));
                        ArrayList kar = kids.getArrayList();
                        for (int fr = 0; fr < kar.size(); ++fr) {
                            PdfObject h = (PdfObject)kar.get(fr);
                            if (!h.isIndirect() || ((PRIndirectReference)h).getNumber() != wdref.getNumber()) continue;
                            kar.remove(fr);
                            --fr;
                        }
                        if (!kar.isEmpty()) continue block3;
                        wdref = parentRef;
                    } while (true);
                }
                if (ar.size() != 0) continue;
                PdfReader.killIndirect(pageDic.get(PdfName.ANNOTS));
                pageDic.remove(PdfName.ANNOTS);
            }
        }
        if (!this.fieldsAdded && this.partialFlattening.isEmpty()) {
            for (int page = 1; page <= this.reader.getNumberOfPages(); ++page) {
                PdfDictionary pageDic = this.reader.getPageN(page);
                PdfArray annots = (PdfArray)PdfReader.getPdfObject(pageDic.get(PdfName.ANNOTS));
                if (annots == null) continue;
                ArrayList ar = annots.getArrayList();
                for (int idx = 0; idx < ar.size(); ++idx) {
                    PdfDictionary annot;
                    PdfObject annoto = PdfReader.getPdfObject((PdfObject)ar.get(idx));
                    if (annoto instanceof PdfIndirectReference && !annoto.isIndirect() || !PdfName.WIDGET.equals((annot = (PdfDictionary)annoto).get(PdfName.SUBTYPE))) continue;
                    ar.remove(idx);
                    --idx;
                }
                if (ar.size() != 0) continue;
                PdfReader.killIndirect(pageDic.get(PdfName.ANNOTS));
                pageDic.remove(PdfName.ANNOTS);
            }
            this.eliminateAcroformObjects();
        }
    }

    void eliminateAcroformObjects() {
        PdfObject acro = this.reader.getCatalog().get(PdfName.ACROFORM);
        if (acro == null) {
            return;
        }
        PdfDictionary acrodic = (PdfDictionary)PdfReader.getPdfObject(acro);
        PdfObject iFields = acrodic.get(PdfName.FIELDS);
        if (iFields != null) {
            PdfDictionary kids = new PdfDictionary();
            kids.put(PdfName.KIDS, iFields);
            this.sweepKids(kids);
        }
    }

    void sweepKids(PdfObject obj) {
        PdfObject oo = PdfReader.killIndirect(obj);
        if (!oo.isDictionary()) {
            return;
        }
        PdfDictionary dic = (PdfDictionary)oo;
        PdfArray kids = (PdfArray)PdfReader.killIndirect(dic.get(PdfName.KIDS));
        if (kids == null) {
            return;
        }
        ArrayList ar = kids.getArrayList();
        for (int k = 0; k < ar.size(); ++k) {
            this.sweepKids((PdfObject)ar.get(k));
        }
    }

    public PdfIndirectReference getPageReference(int page) {
        PRIndirectReference ref = this.reader.getPageOrigRef(page);
        if (ref == null) {
            throw new IllegalArgumentException("Invalid page number " + page);
        }
        return ref;
    }

    public void addAnnotation(PdfAnnotation annot) {
        throw new RuntimeException("Unsupported in this context. Use PdfStamper.addAnnotation()");
    }

    void addDocumentField(PdfIndirectReference ref) {
        PdfArray fields;
        PdfDictionary catalog = this.reader.getCatalog();
        PdfDictionary acroForm = (PdfDictionary)PdfReader.getPdfObject(catalog.get(PdfName.ACROFORM), catalog);
        if (acroForm == null) {
            acroForm = new PdfDictionary();
            catalog.put(PdfName.ACROFORM, acroForm);
            this.markUsed(catalog);
        }
        if ((fields = (PdfArray)PdfReader.getPdfObject(acroForm.get(PdfName.FIELDS), acroForm)) == null) {
            fields = new PdfArray();
            acroForm.put(PdfName.FIELDS, fields);
            this.markUsed(acroForm);
        }
        fields.add(ref);
        this.markUsed(fields);
    }

    void addFieldResources() {
        PdfDictionary dr;
        if (this.fieldTemplates.size() == 0) {
            return;
        }
        PdfDictionary catalog = this.reader.getCatalog();
        PdfDictionary acroForm = (PdfDictionary)PdfReader.getPdfObject(catalog.get(PdfName.ACROFORM), catalog);
        if (acroForm == null) {
            acroForm = new PdfDictionary();
            catalog.put(PdfName.ACROFORM, acroForm);
            this.markUsed(catalog);
        }
        if ((dr = (PdfDictionary)PdfReader.getPdfObject(acroForm.get(PdfName.DR), acroForm)) == null) {
            dr = new PdfDictionary();
            acroForm.put(PdfName.DR, dr);
            this.markUsed(acroForm);
        }
        this.markUsed(dr);
        Iterator it = this.fieldTemplates.keySet().iterator();
        while (it.hasNext()) {
            PdfTemplate template = (PdfTemplate)it.next();
            PdfFormField.mergeResources(dr, (PdfDictionary)template.getResources(), this);
        }
        PdfDictionary fonts = (PdfDictionary)PdfReader.getPdfObject(dr.get(PdfName.FONT));
        if (fonts != null && acroForm.get(PdfName.DA) == null) {
            acroForm.put(PdfName.DA, new PdfString("/Helv 0 Tf 0 g "));
            this.markUsed(acroForm);
        }
    }

    void expandFields(PdfFormField field, ArrayList allAnnots) {
        allAnnots.add(field);
        ArrayList kids = field.getKids();
        if (kids != null) {
            for (int k = 0; k < kids.size(); ++k) {
                this.expandFields((PdfFormField)kids.get(k), allAnnots);
            }
        }
    }

    void addAnnotation(PdfAnnotation annot, PdfDictionary pageN) {
        try {
            ArrayList<PdfAnnotation> allAnnots = new ArrayList<PdfAnnotation>();
            if (annot.isForm()) {
                this.fieldsAdded = true;
                this.getAcroFields();
                PdfFormField field = (PdfFormField)annot;
                if (field.getParent() != null) {
                    return;
                }
                this.expandFields(field, allAnnots);
            } else {
                allAnnots.add(annot);
            }
            for (int k = 0; k < allAnnots.size(); ++k) {
                annot = (PdfAnnotation)allAnnots.get(k);
                if (annot.getPlaceInPage() > 0) {
                    pageN = this.reader.getPageN(annot.getPlaceInPage());
                }
                if (annot.isForm()) {
                    HashMap templates;
                    PdfFormField field;
                    if (!(annot.isUsed() || (templates = annot.getTemplates()) == null)) {
                        this.fieldTemplates.putAll(templates);
                    }
                    if ((field = (PdfFormField)annot).getParent() == null) {
                        this.addDocumentField(field.getIndirectReference());
                    }
                }
                if (annot.isAnnotation()) {
                    PdfRectangle rect;
                    PdfArray annots = (PdfArray)PdfReader.getPdfObject(pageN.get(PdfName.ANNOTS), pageN);
                    if (annots == null) {
                        annots = new PdfArray();
                        pageN.put(PdfName.ANNOTS, annots);
                        this.markUsed(pageN);
                    }
                    annots.add(annot.getIndirectReference());
                    this.markUsed(annots);
                    if (!(annot.isUsed() || (rect = (PdfRectangle)annot.get(PdfName.RECT)).left() == 0.0f && rect.right() == 0.0f && rect.top() == 0.0f && rect.bottom() == 0.0f)) {
                        int rotation = this.reader.getPageRotation(pageN);
                        Rectangle pageSize = this.reader.getPageSizeWithRotation(pageN);
                        switch (rotation) {
                            case 90: {
                                annot.put(PdfName.RECT, new PdfRectangle(pageSize.top() - rect.bottom(), rect.left(), pageSize.top() - rect.top(), rect.right()));
                                break;
                            }
                            case 180: {
                                annot.put(PdfName.RECT, new PdfRectangle(pageSize.right() - rect.left(), pageSize.top() - rect.bottom(), pageSize.right() - rect.right(), pageSize.top() - rect.top()));
                                break;
                            }
                            case 270: {
                                annot.put(PdfName.RECT, new PdfRectangle(rect.bottom(), pageSize.right() - rect.left(), rect.top(), pageSize.right() - rect.right()));
                            }
                        }
                    }
                }
                if (annot.isUsed()) continue;
                annot.setUsed();
                this.addToBody((PdfObject)annot, annot.getIndirectReference());
            }
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    void addAnnotation(PdfAnnotation annot, int page) {
        this.addAnnotation(annot, this.reader.getPageN(page));
    }

    private void outlineTravel(PRIndirectReference outline) {
        while (outline != null) {
            PdfDictionary outlineR = (PdfDictionary)PdfReader.getPdfObjectRelease(outline);
            PRIndirectReference first = (PRIndirectReference)outlineR.get(PdfName.FIRST);
            if (first != null) {
                this.outlineTravel(first);
            }
            PdfReader.killIndirect(outlineR.get(PdfName.DEST));
            PdfReader.killIndirect(outlineR.get(PdfName.A));
            PdfReader.killIndirect(outline);
            outline = (PRIndirectReference)outlineR.get(PdfName.NEXT);
        }
    }

    void deleteOutlines() {
        PdfDictionary catalog = this.reader.getCatalog();
        PRIndirectReference outlines = (PRIndirectReference)catalog.get(PdfName.OUTLINES);
        if (outlines == null) {
            return;
        }
        this.outlineTravel(outlines);
        PdfReader.killIndirect(outlines);
        catalog.remove(PdfName.OUTLINES);
        this.markUsed(catalog);
    }

    void setJavaScript() throws IOException {
        ArrayList djs = this.pdf.getDocumentJavaScript();
        if (djs.size() == 0) {
            return;
        }
        PdfDictionary catalog = this.reader.getCatalog();
        PdfDictionary names = (PdfDictionary)PdfReader.getPdfObject(catalog.get(PdfName.NAMES), catalog);
        if (names == null) {
            names = new PdfDictionary();
            catalog.put(PdfName.NAMES, names);
            this.markUsed(catalog);
        }
        this.markUsed(names);
        String s = String.valueOf(djs.size() - 1);
        int n = s.length();
        String pad = "000000000000000";
        HashMap maptree = new HashMap();
        for (int k = 0; k < djs.size(); ++k) {
            s = String.valueOf(k);
            s = String.valueOf(pad.substring(0, n - s.length())) + s;
            maptree.put(s, djs.get(k));
        }
        PdfDictionary tree = PdfNameTree.writeTree(maptree, this);
        names.put(PdfName.JAVASCRIPT, this.addToBody(tree).getIndirectReference());
    }

    void setOutlines() throws IOException {
        if (this.newBookmarks == null) {
            return;
        }
        this.deleteOutlines();
        if (this.newBookmarks.size() == 0) {
            return;
        }
        this.namedAsNames = this.reader.getCatalog().get(PdfName.DESTS) != null;
        PdfDictionary top = new PdfDictionary();
        PdfIndirectReference topRef = this.getPdfIndirectReference();
        Object[] kids = SimpleBookmark.iterateOutlines(this, topRef, this.newBookmarks, this.namedAsNames);
        top.put(PdfName.FIRST, (PdfIndirectReference)kids[0]);
        top.put(PdfName.LAST, (PdfIndirectReference)kids[1]);
        top.put(PdfName.COUNT, new PdfNumber((Integer)kids[2]));
        this.addToBody((PdfObject)top, topRef);
        this.reader.getCatalog().put(PdfName.OUTLINES, topRef);
        this.markUsed(this.reader.getCatalog());
    }

    void setOutlines(List outlines) {
        this.newBookmarks = outlines;
    }

    public void setViewerPreferences(int preferences) {
        this.useVp = true;
        this.vp|=preferences;
    }

    public void setSigFlags(int f) {
        this.sigFlags|=f;
    }

    public void setPageAction(PdfName actionType, PdfAction action) throws PdfException {
        throw new UnsupportedOperationException("Use setPageAction(PdfName actionType, PdfAction action, int page)");
    }

    void setPageAction(PdfName actionType, PdfAction action, int page) throws PdfException {
        if (!(actionType.equals(PdfWriter.PAGE_OPEN) || actionType.equals(PdfWriter.PAGE_CLOSE))) {
            throw new PdfException("Invalid page additional action type: " + actionType.toString());
        }
        PdfDictionary pg = this.reader.getPageN(page);
        PdfDictionary aa = (PdfDictionary)PdfReader.getPdfObject(pg.get(PdfName.AA), pg);
        if (aa == null) {
            aa = new PdfDictionary();
            pg.put(PdfName.AA, aa);
            this.markUsed(pg);
        }
        aa.put(actionType, action);
        this.markUsed(aa);
    }

    public void setDuration(int seconds) {
        throw new UnsupportedOperationException("Use setPageAction(PdfName actionType, PdfAction action, int page)");
    }

    public void setTransition(PdfTransition transition) {
        throw new UnsupportedOperationException("Use setPageAction(PdfName actionType, PdfAction action, int page)");
    }

    void setDuration(int seconds, int page) {
        PdfDictionary pg = this.reader.getPageN(page);
        if (seconds < 0) {
            pg.remove(PdfName.DUR);
        } else {
            pg.put(PdfName.DUR, new PdfNumber(seconds));
        }
        this.markUsed(pg);
    }

    void setTransition(PdfTransition transition, int page) {
        PdfDictionary pg = this.reader.getPageN(page);
        if (transition == null) {
            pg.remove(PdfName.TRANS);
        } else {
            pg.put(PdfName.TRANS, transition.getTransitionDictionary());
        }
        this.markUsed(pg);
    }

    protected void markUsed(PdfObject obj) {
        if (this.append && obj != null) {
            PRIndirectReference ref = null;
            ref = obj.type() == 10 ? (PRIndirectReference)obj : obj.getIndRef();
            if (ref != null) {
                this.marked.put(ref.getNumber(), 1);
            }
        }
    }

    protected void markUsed(int num) {
        if (this.append) {
            this.marked.put(num, 1);
        }
    }

    boolean isAppend() {
        return this.append;
    }

    public void setAdditionalAction(PdfName actionType, PdfAction action) throws PdfException {
        if (!(actionType.equals(PdfWriter.DOCUMENT_CLOSE) || actionType.equals(PdfWriter.WILL_SAVE) || actionType.equals(PdfWriter.DID_SAVE) || actionType.equals(PdfWriter.WILL_PRINT) || actionType.equals(PdfWriter.DID_PRINT))) {
            throw new PdfException("Invalid additional action type: " + actionType.toString());
        }
        PdfDictionary aa = (PdfDictionary)PdfReader.getPdfObject(this.reader.getCatalog().get(PdfName.AA));
        if (aa == null) {
            if (action == null) {
                return;
            }
            aa = new PdfDictionary();
            this.reader.getCatalog().put(PdfName.AA, aa);
        }
        this.markUsed(aa);
        if (action == null) {
            aa.remove(actionType);
        } else {
            aa.put(actionType, action);
        }
    }

    public void setOpenAction(PdfAction action) {
        this.openAction = action;
    }

    public void setOpenAction(String name) {
        throw new UnsupportedOperationException("Open actions by name are not supported.");
    }

    public void setThumbnail(Image image) {
        throw new UnsupportedOperationException("Use PdfStamper.setThumbnail().");
    }

    void setThumbnail(Image image, int page) throws PdfException, DocumentException {
        PdfIndirectReference thumb = this.getImageReference(this.addDirectImageSimple(image));
        this.reader.resetReleasePage();
        PdfDictionary dic = this.reader.getPageN(page);
        dic.put(PdfName.THUMB, thumb);
        this.reader.resetReleasePage();
    }

    static class PageStamp {
        PdfDictionary pageN;
        StampContent under;
        StampContent over;
        PageResources pageResources;

        PageStamp(PdfStamperImp stamper, PdfReader reader, PdfDictionary pageN) {
            this.pageN = pageN;
            this.pageResources = new PageResources();
            PdfDictionary resources = (PdfDictionary)PdfReader.getPdfObject(pageN.get(PdfName.RESOURCES));
            this.pageResources.setOriginalResources(resources, stamper.namePtr);
        }
    }

}

