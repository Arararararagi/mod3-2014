/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfString;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;

public class PdfPageLabels
implements Comparator {
    public static int DECIMAL_ARABIC_NUMERALS = 0;
    public static int UPPERCASE_ROMAN_NUMERALS = 1;
    public static int LOWERCASE_ROMAN_NUMERALS = 2;
    public static int UPPERCASE_LETTERS = 3;
    public static int LOWERCASE_LETTERS = 4;
    public static int EMPTY = 5;
    static PdfName[] numberingStyle = new PdfName[]{PdfName.D, PdfName.R, new PdfName("r"), PdfName.A, new PdfName("a")};
    TreeMap map;

    public PdfPageLabels() {
        this.map = new TreeMap(this);
        this.addPageLabel(1, DECIMAL_ARABIC_NUMERALS, null, 1);
    }

    public int compare(Object obj, Object obj1) {
        int v2;
        int v1 = (Integer)obj;
        if (v1 < (v2 = ((Integer)obj1).intValue())) {
            return -1;
        }
        if (v1 == v2) {
            return 0;
        }
        return 1;
    }

    public boolean equals(Object obj) {
        return true;
    }

    public void addPageLabel(int page, int numberStyle, String text, int firstPage) {
        if (page < 1 || firstPage < 1) {
            throw new IllegalArgumentException("In a page label the page numbers must be greater or equal to 1.");
        }
        PdfName pdfName = null;
        if (numberStyle >= 0 && numberStyle < numberingStyle.length) {
            pdfName = numberingStyle[numberStyle];
        }
        Integer iPage = new Integer(page);
        Object[] obj = new Object[]{iPage, pdfName, text, new Integer(firstPage)};
        this.map.put(iPage, obj);
    }

    public void addPageLabel(int page, int numberStyle, String text) {
        this.addPageLabel(page, numberStyle, text, 1);
    }

    public void addPageLabel(int page, int numberStyle) {
        this.addPageLabel(page, numberStyle, null, 1);
    }

    public void removePageLabel(int page) {
        if (page <= 1) {
            return;
        }
        this.map.remove(new Integer(page));
    }

    PdfDictionary getDictionary() {
        PdfDictionary dic = new PdfDictionary();
        PdfArray array = new PdfArray();
        Iterator it = this.map.values().iterator();
        while (it.hasNext()) {
            int st;
            String text;
            Object[] obj = (Object[])it.next();
            PdfDictionary subDic = new PdfDictionary();
            PdfName pName = (PdfName)obj[1];
            if (pName != null) {
                subDic.put(PdfName.S, pName);
            }
            if ((text = (String)obj[2]) != null) {
                subDic.put(PdfName.P, new PdfString(text, "UnicodeBig"));
            }
            if ((st = ((Integer)obj[3]).intValue()) != 1) {
                subDic.put(PdfName.ST, new PdfNumber(st));
            }
            array.add(new PdfNumber((Integer)obj[0] - 1));
            array.add(subDic);
        }
        dic.put(PdfName.NUMS, array);
        return dic;
    }
}

