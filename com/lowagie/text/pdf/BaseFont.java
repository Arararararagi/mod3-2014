/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.CJKFont;
import com.lowagie.text.pdf.DocumentFont;
import com.lowagie.text.pdf.EnumerateTTC;
import com.lowagie.text.pdf.GlyphList;
import com.lowagie.text.pdf.IntHashtable;
import com.lowagie.text.pdf.PRIndirectReference;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TrueTypeFont;
import com.lowagie.text.pdf.TrueTypeFontUnicode;
import com.lowagie.text.pdf.Type1Font;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public abstract class BaseFont {
    public static final String COURIER = "Courier";
    public static final String COURIER_BOLD = "Courier-Bold";
    public static final String COURIER_OBLIQUE = "Courier-Oblique";
    public static final String COURIER_BOLDOBLIQUE = "Courier-BoldOblique";
    public static final String HELVETICA = "Helvetica";
    public static final String HELVETICA_BOLD = "Helvetica-Bold";
    public static final String HELVETICA_OBLIQUE = "Helvetica-Oblique";
    public static final String HELVETICA_BOLDOBLIQUE = "Helvetica-BoldOblique";
    public static final String SYMBOL = "Symbol";
    public static final String TIMES_ROMAN = "Times-Roman";
    public static final String TIMES_BOLD = "Times-Bold";
    public static final String TIMES_ITALIC = "Times-Italic";
    public static final String TIMES_BOLDITALIC = "Times-BoldItalic";
    public static final String ZAPFDINGBATS = "ZapfDingbats";
    public static final int ASCENT = 1;
    public static final int CAPHEIGHT = 2;
    public static final int DESCENT = 3;
    public static final int ITALICANGLE = 4;
    public static final int BBOXLLX = 5;
    public static final int BBOXLLY = 6;
    public static final int BBOXURX = 7;
    public static final int BBOXURY = 8;
    public static final int AWT_ASCENT = 9;
    public static final int AWT_DESCENT = 10;
    public static final int AWT_LEADING = 11;
    public static final int AWT_MAXADVANCE = 12;
    public static final int FONT_TYPE_T1 = 0;
    public static final int FONT_TYPE_TT = 1;
    public static final int FONT_TYPE_CJK = 2;
    public static final int FONT_TYPE_TTUNI = 3;
    public static final int FONT_TYPE_DOCUMENT = 4;
    public static final String IDENTITY_H = "Identity-H";
    public static final String IDENTITY_V = "Identity-V";
    public static final String CP1250 = "Cp1250";
    public static final String CP1252 = "Cp1252";
    public static final String CP1257 = "Cp1257";
    public static final String WINANSI = "Cp1252";
    public static final String MACROMAN = "MacRoman";
    public static final boolean EMBEDDED = true;
    public static final boolean NOT_EMBEDDED = false;
    public static final boolean CACHED = true;
    public static final boolean NOT_CACHED = false;
    public static final String RESOURCE_PATH = "com/lowagie/text/pdf/fonts/";
    public static final char CID_NEWLINE = '\u7fff';
    int fontType;
    public static final String notdef = ".notdef";
    protected int[] widths = new int[256];
    protected String[] differences = new String[256];
    protected char[] unicodeDifferences = new char[256];
    protected int[][] charBBoxes = new int[256][];
    protected String encoding;
    protected boolean embedded;
    protected boolean fontSpecific = true;
    protected static HashMap fontCache = new HashMap();
    protected static final HashMap BuiltinFonts14 = new HashMap();
    protected boolean forceWidthsOutput = false;
    protected boolean directTextToByte = false;
    protected boolean subset = true;
    protected boolean fastWinansi = false;
    static /* synthetic */ Class class$0;
    static /* synthetic */ Class class$1;

    static {
        BuiltinFonts14.put("Courier", PdfName.COURIER);
        BuiltinFonts14.put("Courier-Bold", PdfName.COURIER_BOLD);
        BuiltinFonts14.put("Courier-BoldOblique", PdfName.COURIER_BOLDOBLIQUE);
        BuiltinFonts14.put("Courier-Oblique", PdfName.COURIER_OBLIQUE);
        BuiltinFonts14.put("Helvetica", PdfName.HELVETICA);
        BuiltinFonts14.put("Helvetica-Bold", PdfName.HELVETICA_BOLD);
        BuiltinFonts14.put("Helvetica-BoldOblique", PdfName.HELVETICA_BOLDOBLIQUE);
        BuiltinFonts14.put("Helvetica-Oblique", PdfName.HELVETICA_OBLIQUE);
        BuiltinFonts14.put("Symbol", PdfName.SYMBOL);
        BuiltinFonts14.put("Times-Roman", PdfName.TIMES_ROMAN);
        BuiltinFonts14.put("Times-Bold", PdfName.TIMES_BOLD);
        BuiltinFonts14.put("Times-BoldItalic", PdfName.TIMES_BOLDITALIC);
        BuiltinFonts14.put("Times-Italic", PdfName.TIMES_ITALIC);
        BuiltinFonts14.put("ZapfDingbats", PdfName.ZAPFDINGBATS);
    }

    protected BaseFont() {
    }

    public static BaseFont createFont(String name, String encoding, boolean embedded) throws DocumentException, IOException {
        return BaseFont.createFont(name, encoding, embedded, true, null, null);
    }

    public static BaseFont createFont(String name, String encoding, boolean embedded, boolean cached, byte[] ttfAfm, byte[] pfb) throws DocumentException, IOException {
        boolean isCJKFont;
        HashMap hashMap;
        String nameBase = BaseFont.getBaseName(name);
        encoding = BaseFont.normalizeEncoding(encoding);
        boolean isBuiltinFonts14 = BuiltinFonts14.containsKey(name);
        boolean bl = isCJKFont = isBuiltinFonts14 ? false : CJKFont.isCJKFont(nameBase, encoding);
        if (isBuiltinFonts14 || isCJKFont) {
            embedded = false;
        } else if (encoding.equals("Identity-H") || encoding.equals("Identity-V")) {
            embedded = true;
        }
        BaseFont fontFound = null;
        BaseFont fontBuilt = null;
        String key = String.valueOf(name) + "\n" + encoding + "\n" + embedded;
        if (cached) {
            hashMap = fontCache;
            synchronized (hashMap) {
                fontFound = (BaseFont)fontCache.get(key);
            }
            if (fontFound != null) {
                return fontFound;
            }
        }
        if (isBuiltinFonts14 || name.toLowerCase().endsWith(".afm") || name.toLowerCase().endsWith(".pfm")) {
            fontBuilt = new Type1Font(name, encoding, embedded, ttfAfm, pfb);
            fontBuilt.fastWinansi = encoding.equals("Cp1252");
        } else if (nameBase.toLowerCase().endsWith(".ttf") || nameBase.toLowerCase().endsWith(".otf") || nameBase.toLowerCase().indexOf(".ttc,") > 0) {
            if (encoding.equals("Identity-H") || encoding.equals("Identity-V")) {
                fontBuilt = new TrueTypeFontUnicode(name, encoding, embedded, ttfAfm);
            } else {
                fontBuilt = new TrueTypeFont(name, encoding, embedded, ttfAfm);
                fontBuilt.fastWinansi = encoding.equals("Cp1252");
            }
        } else if (isCJKFont) {
            fontBuilt = new CJKFont(name, encoding, embedded);
        } else {
            throw new DocumentException("Font '" + name + "' with '" + encoding + "' is not recognized.");
        }
        if (cached) {
            hashMap = fontCache;
            synchronized (hashMap) {
                fontFound = (BaseFont)fontCache.get(key);
                if (fontFound != null) {
                    return fontFound;
                }
                fontCache.put(key, fontBuilt);
            }
        }
        return fontBuilt;
    }

    public static BaseFont createFont(PRIndirectReference fontRef) {
        return new DocumentFont(fontRef);
    }

    protected static String getBaseName(String name) {
        if (name.endsWith(",Bold")) {
            return name.substring(0, name.length() - 5);
        }
        if (name.endsWith(",Italic")) {
            return name.substring(0, name.length() - 7);
        }
        if (name.endsWith(",BoldItalic")) {
            return name.substring(0, name.length() - 11);
        }
        return name;
    }

    protected static String normalizeEncoding(String enc) {
        if (enc.equals("winansi") || enc.equals("")) {
            return "Cp1252";
        }
        if (enc.equals("macroman")) {
            return "MacRoman";
        }
        return enc;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    protected void createEncoding() {
        if (this.fontSpecific) {
            for (int k = 0; k < 256; ++k) {
                this.widths[k] = this.getRawWidth(k, null);
                this.charBBoxes[k] = this.getRawCharBBox(k, null);
            }
            return;
        } else {
            byte[] b = new byte[1];
            for (int k = 0; k < 256; ++k) {
                b[0] = (byte)k;
                String s = PdfEncodings.convertToString(b, this.encoding);
                int c = s.length() > 0 ? (int)s.charAt(0) : 63;
                String name = GlyphList.unicodeToName(c);
                if (name == null) {
                    name = ".notdef";
                }
                this.differences[k] = name;
                this.unicodeDifferences[k] = c;
                this.widths[k] = this.getRawWidth(c, name);
                this.charBBoxes[k] = this.getRawCharBBox(c, name);
            }
        }
    }

    abstract int getRawWidth(int var1, String var2);

    public abstract int getKerning(char var1, char var2);

    public abstract boolean setKerning(char var1, char var2, int var3);

    public int getWidth(char char1) {
        if (this.fastWinansi) {
            if (char1 < '' || char1 >= '\u00a0' && char1 <= '\u00ff') {
                return this.widths[char1];
            }
            return this.widths[PdfEncodings.winansi.get(char1)];
        }
        return this.getWidth(new String(new char[]{char1}));
    }

    public int getWidth(String text) {
        int total = 0;
        if (this.fastWinansi) {
            int len = text.length();
            for (int k = 0; k < len; ++k) {
                char char1 = text.charAt(k);
                if (char1 < '' || char1 >= '\u00a0' && char1 <= '\u00ff') {
                    total+=this.widths[char1];
                    continue;
                }
                total+=this.widths[PdfEncodings.winansi.get(char1)];
            }
            return total;
        }
        byte[] mbytes = this.convertToBytes(text);
        for (int k = 0; k < mbytes.length; ++k) {
            total+=this.widths[255 & mbytes[k]];
        }
        return total;
    }

    public int getDescent(String text) {
        int min = 0;
        char[] chars = text.toCharArray();
        for (int k = 0; k < chars.length; ++k) {
            int[] bbox = this.getCharBBox(chars[k]);
            if (bbox == null || bbox[1] >= min) continue;
            min = bbox[1];
        }
        return min;
    }

    public int getAscent(String text) {
        int max = 0;
        char[] chars = text.toCharArray();
        for (int k = 0; k < chars.length; ++k) {
            int[] bbox = this.getCharBBox(chars[k]);
            if (bbox == null || bbox[3] <= max) continue;
            max = bbox[3];
        }
        return max;
    }

    public float getDescentPoint(String text, float fontSize) {
        return (float)this.getDescent(text) * 0.001f * fontSize;
    }

    public float getAscentPoint(String text, float fontSize) {
        return (float)this.getAscent(text) * 0.001f * fontSize;
    }

    public float getWidthPointKerned(String text, float fontSize) {
        float size = (float)this.getWidth(text) * 0.001f * fontSize;
        if (!this.hasKernPairs()) {
            return size;
        }
        int len = text.length() - 1;
        int kern = 0;
        char[] c = text.toCharArray();
        for (int k = 0; k < len; ++k) {
            kern+=this.getKerning(c[k], c[k + 1]);
        }
        return size + (float)kern * 0.001f * fontSize;
    }

    public float getWidthPoint(String text, float fontSize) {
        return (float)this.getWidth(text) * 0.001f * fontSize;
    }

    public float getWidthPoint(char char1, float fontSize) {
        return (float)this.getWidth(char1) * 0.001f * fontSize;
    }

    byte[] convertToBytes(String text) {
        if (this.directTextToByte) {
            return PdfEncodings.convertToBytes(text, null);
        }
        return PdfEncodings.convertToBytes(text, this.encoding);
    }

    abstract void writeFont(PdfWriter var1, PdfIndirectReference var2, Object[] var3) throws DocumentException, IOException;

    public String getEncoding() {
        return this.encoding;
    }

    public abstract float getFontDescriptor(int var1, float var2);

    public int getFontType() {
        return this.fontType;
    }

    public boolean isEmbedded() {
        return this.embedded;
    }

    public boolean isFontSpecific() {
        return this.fontSpecific;
    }

    public static String createSubsetPrefix() {
        String s = "";
        for (int k = 0; k < 6; ++k) {
            s = String.valueOf(s) + (char)(Math.random() * 26.0 + 65.0);
        }
        return String.valueOf(s) + "+";
    }

    char getUnicodeDifferences(int index) {
        return this.unicodeDifferences[index];
    }

    public abstract String getPostscriptFontName();

    public abstract void setPostscriptFontName(String var1);

    public abstract String[][] getFullFontName();

    public static String[][] getFullFontName(String name, String encoding, byte[] ttfAfm) throws DocumentException, IOException {
        String nameBase = BaseFont.getBaseName(name);
        BaseFont fontBuilt = null;
        fontBuilt = nameBase.toLowerCase().endsWith(".ttf") || nameBase.toLowerCase().endsWith(".otf") || nameBase.toLowerCase().indexOf(".ttc,") > 0 ? new TrueTypeFont(name, "Cp1252", false, ttfAfm, true) : BaseFont.createFont(name, encoding, false, false, ttfAfm, null);
        return fontBuilt.getFullFontName();
    }

    public static Object[] getAllFontNames(String name, String encoding, byte[] ttfAfm) throws DocumentException, IOException {
        String nameBase = BaseFont.getBaseName(name);
        BaseFont fontBuilt = null;
        fontBuilt = nameBase.toLowerCase().endsWith(".ttf") || nameBase.toLowerCase().endsWith(".otf") || nameBase.toLowerCase().indexOf(".ttc,") > 0 ? new TrueTypeFont(name, "Cp1252", false, ttfAfm, true) : BaseFont.createFont(name, encoding, false, false, ttfAfm, null);
        return new Object[]{fontBuilt.getPostscriptFontName(), fontBuilt.getFamilyFontName(), fontBuilt.getFullFontName()};
    }

    public abstract String[][] getFamilyFontName();

    public String[] getCodePagesSupported() {
        return new String[0];
    }

    public static String[] enumerateTTCNames(String ttcFile) throws DocumentException, IOException {
        return new EnumerateTTC(ttcFile).getNames();
    }

    public static String[] enumerateTTCNames(byte[] ttcArray) throws DocumentException, IOException {
        return new EnumerateTTC(ttcArray).getNames();
    }

    public int[] getWidths() {
        return this.widths;
    }

    public String[] getDifferences() {
        return this.differences;
    }

    public char[] getUnicodeDifferences() {
        return this.unicodeDifferences;
    }

    public boolean isForceWidthsOutput() {
        return this.forceWidthsOutput;
    }

    public void setForceWidthsOutput(boolean forceWidthsOutput) {
        this.forceWidthsOutput = forceWidthsOutput;
    }

    public boolean isDirectTextToByte() {
        return this.directTextToByte;
    }

    public void setDirectTextToByte(boolean directTextToByte) {
        this.directTextToByte = directTextToByte;
    }

    public boolean isSubset() {
        return this.subset;
    }

    public void setSubset(boolean subset) {
        this.subset = subset;
    }

    public static InputStream getResourceStream(String key) {
        return BaseFont.getResourceStream(key, null);
    }

    public static InputStream getResourceStream(String key, ClassLoader loader) {
        if (key.startsWith("/")) {
            key = key.substring(1);
        }
        InputStream is = null;
        if (loader != null && (is = loader.getResourceAsStream(key)) != null) {
            return is;
        }
        try {
            Class<?> class_;
            Method getCCL;
            ClassLoader contextClassLoader;
            class_ = class$0;
            if (class_ == null) {
                try {
                    class_ = BaseFont.class$0 = Class.forName("java.lang.Thread");
                }
                catch (ClassNotFoundException v1) {
                    throw new NoClassDefFoundError(v1.getMessage());
                }
            }
            if ((getCCL = class_.getMethod("getContextClassLoader", new Class[0])) != null && (contextClassLoader = (ClassLoader)getCCL.invoke(Thread.currentThread(), new Object[0])) != null) {
                is = contextClassLoader.getResourceAsStream(key);
            }
        }
        catch (Throwable getCCL) {
            // empty catch block
        }
        if (is == null) {
            Class<?> class_;
            class_ = class$1;
            if (class_ == null) {
                try {
                    class_ = BaseFont.class$1 = Class.forName("com.lowagie.text.pdf.BaseFont");
                }
                catch (ClassNotFoundException v3) {
                    throw new NoClassDefFoundError(v3.getMessage());
                }
            }
            is = class_.getResourceAsStream("/" + key);
        }
        if (is == null) {
            is = ClassLoader.getSystemResourceAsStream(key);
        }
        return is;
    }

    public char getUnicodeEquivalent(char c) {
        return c;
    }

    public char getCidCode(char c) {
        return c;
    }

    public abstract boolean hasKernPairs();

    public boolean charExists(char c) {
        byte[] b = this.convertToBytes(new String(new char[]{c}));
        if (b.length > 0) {
            return true;
        }
        return false;
    }

    public boolean setCharAdvance(char c, int advance) {
        byte[] b = this.convertToBytes(new String(new char[]{c}));
        if (b.length == 0) {
            return false;
        }
        this.widths[255 & b[0]] = advance;
        return true;
    }

    private static void addFont(PRIndirectReference fontRef, IntHashtable hits, ArrayList fonts) {
        PdfObject obj = PdfReader.getPdfObject(fontRef);
        if (!obj.isDictionary()) {
            return;
        }
        PdfDictionary font = (PdfDictionary)obj;
        PdfName subtype = (PdfName)PdfReader.getPdfObject(font.get(PdfName.SUBTYPE));
        if (!(PdfName.TYPE1.equals(subtype) || PdfName.TRUETYPE.equals(subtype))) {
            return;
        }
        PdfName name = (PdfName)PdfReader.getPdfObject(font.get(PdfName.BASEFONT));
        fonts.add(new Object[]{PdfName.decodeName(name.toString()), fontRef});
        hits.put(fontRef.getNumber(), 1);
    }

    private static void recourseFonts(PdfDictionary page, IntHashtable hits, ArrayList fonts, int level) {
        PdfDictionary xobj;
        if (++level > 50) {
            return;
        }
        PdfDictionary resources = (PdfDictionary)PdfReader.getPdfObject(page.get(PdfName.RESOURCES));
        if (resources == null) {
            return;
        }
        PdfDictionary font = (PdfDictionary)PdfReader.getPdfObject(resources.get(PdfName.FONT));
        if (font != null) {
            Iterator<E> it = font.getKeys().iterator();
            while (it.hasNext()) {
                PdfObject ft = font.get((PdfName)it.next());
                if (ft == null || !ft.isIndirect()) continue;
                int hit = ((PRIndirectReference)ft).getNumber();
                if (hits.containsKey(hit)) continue;
                BaseFont.addFont((PRIndirectReference)ft, hits, fonts);
            }
        }
        if ((xobj = (PdfDictionary)PdfReader.getPdfObject(resources.get(PdfName.XOBJECT))) != null) {
            Iterator<E> it = xobj.getKeys().iterator();
            while (it.hasNext()) {
                BaseFont.recourseFonts((PdfDictionary)PdfReader.getPdfObject(xobj.get((PdfName)it.next())), hits, fonts, level);
            }
        }
    }

    public static ArrayList getDocumentFonts(PdfReader reader) {
        IntHashtable hits = new IntHashtable();
        ArrayList<E> fonts = new ArrayList<E>();
        int npages = reader.getNumberOfPages();
        for (int k = 1; k <= npages; ++k) {
            BaseFont.recourseFonts(reader.getPageN(k), hits, fonts, 1);
        }
        return fonts;
    }

    public static ArrayList getDocumentFonts(PdfReader reader, int page) {
        IntHashtable hits = new IntHashtable();
        ArrayList<E> fonts = new ArrayList<E>();
        BaseFont.recourseFonts(reader.getPageN(page), hits, fonts, 1);
        return fonts;
    }

    public int[] getCharBBox(char c) {
        byte[] b = this.convertToBytes(new String(new char[]{c}));
        if (b.length == 0) {
            return null;
        }
        return this.charBBoxes[b[0] & 255];
    }

    protected abstract int[] getRawCharBBox(int var1, String var2);

    static class StreamFont
    extends PdfStream {
        public StreamFont(byte[] contents, int[] lengths) throws DocumentException {
            try {
                this.bytes = contents;
                this.put(PdfName.LENGTH, new PdfNumber(this.bytes.length));
                for (int k = 0; k < lengths.length; ++k) {
                    this.put(new PdfName("Length" + (k + 1)), new PdfNumber(lengths[k]));
                }
                this.flateCompress();
            }
            catch (Exception e) {
                throw new DocumentException(e);
            }
        }

        public StreamFont(byte[] contents, String subType) throws DocumentException {
            try {
                this.bytes = contents;
                this.put(PdfName.LENGTH, new PdfNumber(this.bytes.length));
                if (subType != null) {
                    this.put(PdfName.SUBTYPE, new PdfName(subType));
                }
                this.flateCompress();
            }
            catch (Exception e) {
                throw new DocumentException(e);
            }
        }
    }

}

