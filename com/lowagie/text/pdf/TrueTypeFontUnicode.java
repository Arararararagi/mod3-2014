/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.CFFFontSubset;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfLiteral;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.TrueTypeFont;
import com.lowagie.text.pdf.TrueTypeFontSubSet;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;

class TrueTypeFontUnicode
extends TrueTypeFont
implements Comparator {
    boolean vertical = false;

    TrueTypeFontUnicode(String ttFile, String enc, boolean emb, byte[] ttfAfm) throws DocumentException, IOException {
        String nameBase = BaseFont.getBaseName(ttFile);
        String ttcName = TrueTypeFont.getTTCName(nameBase);
        if (nameBase.length() < ttFile.length()) {
            this.style = ttFile.substring(nameBase.length());
        }
        this.encoding = enc;
        this.embedded = emb;
        this.fileName = ttcName;
        this.ttcIndex = "";
        if (ttcName.length() < nameBase.length()) {
            this.ttcIndex = nameBase.substring(ttcName.length() + 1);
        }
        this.fontType = 3;
        if ((this.fileName.toLowerCase().endsWith(".ttf") || this.fileName.toLowerCase().endsWith(".otf") || this.fileName.toLowerCase().endsWith(".ttc")) && (enc.equals("Identity-H") || enc.equals("Identity-V")) && emb) {
            this.process(ttfAfm);
            if (this.os_2.fsType == 2) {
                throw new DocumentException(String.valueOf(this.fileName) + this.style + " cannot be embedded due to licensing restrictions.");
            }
            if (this.cmap31 == null && !this.fontSpecific || this.cmap10 == null && this.fontSpecific) {
                this.directTextToByte = true;
            }
            if (this.fontSpecific) {
                this.fontSpecific = false;
                String tempEncoding = this.encoding;
                this.encoding = "";
                this.createEncoding();
                this.encoding = tempEncoding;
                this.fontSpecific = true;
            }
        } else {
            throw new DocumentException(String.valueOf(this.fileName) + " " + this.style + " is not a TTF font file.");
        }
        this.vertical = enc.endsWith("V");
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public int getWidth(String text) {
        if (this.vertical) {
            return text.length() * 1000;
        }
        int total = 0;
        if (this.fontSpecific) {
            char[] cc = text.toCharArray();
            boolean ptr = false;
            int len = cc.length;
            for (int k = 0; k < len; ++k) {
                char c = cc[k];
                if ((c & 65280) != 0 && (c & 65280) != 61440) continue;
                total+=this.getRawWidth(c & 255, null);
            }
            return total;
        } else {
            int len = text.length();
            for (int k = 0; k < len; ++k) {
                total+=this.getRawWidth(text.charAt(k), this.encoding);
            }
        }
        return total;
    }

    private PdfStream getToUnicode(Object[] metrics) throws DocumentException {
        if (metrics.length == 0) {
            return null;
        }
        StringBuffer buf = new StringBuffer("/CIDInit /ProcSet findresource begin\n12 dict begin\nbegincmap\n/CIDSystemInfo\n<< /Registry (Adobe)\n/Ordering (UCS)\n/Supplement 0\n>> def\n/CMapName /Adobe-Identity-UCS def\n/CMapType 2 def\n1 begincodespacerange\n" + TrueTypeFontUnicode.toHex(((int[])metrics[0])[0]) + TrueTypeFontUnicode.toHex(((int[])metrics[metrics.length - 1])[0]) + "\n" + "endcodespacerange\n");
        int size = 0;
        for (int k = 0; k < metrics.length; ++k) {
            if (size == 0) {
                if (k != 0) {
                    buf.append("endbfrange\n");
                }
                size = Math.min(100, metrics.length - k);
                buf.append(size).append(" beginbfrange\n");
            }
            --size;
            int[] metric = (int[])metrics[k];
            String fromTo = TrueTypeFontUnicode.toHex(metric[0]);
            buf.append(fromTo).append(fromTo).append(TrueTypeFontUnicode.toHex(metric[2])).append("\n");
        }
        buf.append("endbfrange\nendcmap\nCMapName currentdict /CMap defineresource pop\nend end\n");
        String s = buf.toString();
        PdfStream stream = new PdfStream(PdfEncodings.convertToBytes(s, null));
        stream.flateCompress();
        return stream;
    }

    static String toHex(int n) {
        String s = Integer.toHexString(n);
        return String.valueOf("<0000".substring(0, 5 - s.length())) + s + ">";
    }

    private PdfDictionary getCIDFontType2(PdfIndirectReference fontDescriptor, String subsetPrefix, Object[] metrics) {
        PdfDictionary dic = new PdfDictionary(PdfName.FONT);
        if (this.cff) {
            dic.put(PdfName.SUBTYPE, PdfName.CIDFONTTYPE0);
            dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(this.fontName) + "-" + this.encoding));
        } else {
            dic.put(PdfName.SUBTYPE, PdfName.CIDFONTTYPE2);
            dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(subsetPrefix) + this.fontName));
        }
        dic.put(PdfName.FONTDESCRIPTOR, fontDescriptor);
        if (!this.cff) {
            dic.put(PdfName.CIDTOGIDMAP, PdfName.IDENTITY);
        }
        PdfDictionary cdic = new PdfDictionary();
        cdic.put(PdfName.REGISTRY, new PdfString("Adobe"));
        cdic.put(PdfName.ORDERING, new PdfString("Identity"));
        cdic.put(PdfName.SUPPLEMENT, new PdfNumber(0));
        dic.put(PdfName.CIDSYSTEMINFO, cdic);
        if (!this.vertical) {
            dic.put(PdfName.DW, new PdfNumber(1000));
            StringBuffer buf = new StringBuffer("[");
            int lastNumber = -10;
            boolean firstTime = true;
            for (int k = 0; k < metrics.length; ++k) {
                int[] metric = (int[])metrics[k];
                if (metric[1] == 1000) continue;
                int m = metric[0];
                if (m == lastNumber + 1) {
                    buf.append(" ").append(metric[1]);
                } else {
                    if (!firstTime) {
                        buf.append("]");
                    }
                    firstTime = false;
                    buf.append(m).append("[").append(metric[1]);
                }
                lastNumber = m;
            }
            if (buf.length() > 1) {
                buf.append("]]");
                dic.put(PdfName.W, new PdfLiteral(buf.toString()));
            }
        }
        return dic;
    }

    private PdfDictionary getFontBaseType(PdfIndirectReference descendant, String subsetPrefix, PdfIndirectReference toUnicode) {
        PdfDictionary dic = new PdfDictionary(PdfName.FONT);
        dic.put(PdfName.SUBTYPE, PdfName.TYPE0);
        if (this.cff) {
            dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(this.fontName) + "-" + this.encoding));
        } else {
            dic.put(PdfName.BASEFONT, new PdfName(String.valueOf(subsetPrefix) + this.fontName));
        }
        dic.put(PdfName.ENCODING, new PdfName(this.encoding));
        dic.put(PdfName.DESCENDANTFONTS, new PdfArray(descendant));
        if (toUnicode != null) {
            dic.put(PdfName.TOUNICODE, toUnicode);
        }
        return dic;
    }

    public int compare(Object o1, Object o2) {
        int m1 = ((int[])o1)[0];
        int m2 = ((int[])o2)[0];
        if (m1 < m2) {
            return -1;
        }
        if (m1 == m2) {
            return 0;
        }
        return 1;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    void writeFont(PdfWriter writer, PdfIndirectReference ref, Object[] params) throws DocumentException, IOException {
        longTag = (HashMap)params[0];
        metrics = longTag.values().toArray();
        Arrays.sort(metrics, this);
        ind_font = null;
        pobj /* !! */  = null;
        obj = null;
        if (!this.cff) ** GOTO lbl37
        rf2 = new RandomAccessFileOrArray(this.rf);
        b = new byte[this.cffLength];
        try {
            rf2.reOpen();
            rf2.seek(this.cffOffset);
            rf2.readFully(b);
            var11_13 = null;
        }
        catch (Throwable var12_11) {
            var11_12 = null;
            try {
                rf2.close();
                throw var12_11;
            }
            catch (Exception e) {
                // empty catch block
            }
            throw var12_11;
        }
        ** try [egrp 1[TRYBLOCK] [2 : 97->105)] { 
lbl27: // 1 sources:
        rf2.close();
        ** GOTO lbl31
lbl29: // 1 sources:
        catch (Exception e) {
            // empty catch block
        }
lbl31: // 2 sources:
        cff = new CFFFontSubset(new RandomAccessFileOrArray(b), longTag);
        b = cff.Process(cff.getNames()[0]);
        pobj /* !! */  = new BaseFont.StreamFont(b, "CIDFontType0C");
        obj = writer.addToBody(pobj /* !! */ );
        ind_font = obj.getIndirectReference();
        ** GOTO lbl43
lbl37: // 1 sources:
        sb = new TrueTypeFontSubSet(this.fileName, new RandomAccessFileOrArray(this.rf), longTag, this.directoryOffset, false);
        b = sb.process();
        lengths = new int[]{b.length};
        pobj /* !! */  = new BaseFont.StreamFont(b, lengths);
        obj = writer.addToBody(pobj /* !! */ );
        ind_font = obj.getIndirectReference();
lbl43: // 2 sources:
        subsetPrefix = BaseFont.createSubsetPrefix();
        dic = this.getFontDescriptor(ind_font, subsetPrefix);
        obj = writer.addToBody(dic);
        ind_font = obj.getIndirectReference();
        pobj /* !! */  = this.getCIDFontType2(ind_font, subsetPrefix, metrics);
        obj = writer.addToBody(pobj /* !! */ );
        ind_font = obj.getIndirectReference();
        pobj /* !! */  = this.getToUnicode(metrics);
        toUnicodeRef = null;
        if (pobj /* !! */  != null) {
            obj = writer.addToBody(pobj /* !! */ );
            toUnicodeRef = obj.getIndirectReference();
        }
        pobj /* !! */  = this.getFontBaseType(ind_font, subsetPrefix, toUnicodeRef);
        writer.addToBody((PdfObject)pobj /* !! */ , ref);
    }

    byte[] convertToBytes(String text) {
        return null;
    }

    public boolean charExists(char c) {
        HashMap map = null;
        map = this.fontSpecific ? this.cmap10 : this.cmap31;
        if (map == null) {
            return false;
        }
        if (this.fontSpecific) {
            if ((c & 65280) == 0 || (c & 65280) == 61440) {
                if (map.get(new Integer(c & 255)) != null) {
                    return true;
                }
                return false;
            }
            return false;
        }
        if (map.get(new Integer(c)) != null) {
            return true;
        }
        return false;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public boolean setCharAdvance(char c, int advance) {
        HashMap map = null;
        map = this.fontSpecific ? this.cmap10 : this.cmap31;
        if (map == null) {
            return false;
        }
        int[] m = null;
        if (this.fontSpecific) {
            if ((c & 65280) != 0 && (c & 65280) != 61440) return false;
            m = (int[])map.get(new Integer(c & 255));
        } else {
            m = (int[])map.get(new Integer(c));
        }
        if (m == null) {
            return false;
        }
        m[1] = advance;
        return true;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public int[] getCharBBox(char c) {
        if (this.bboxes == null) {
            return null;
        }
        HashMap map = null;
        map = this.fontSpecific ? this.cmap10 : this.cmap31;
        if (map == null) {
            return null;
        }
        int[] m = null;
        if (this.fontSpecific) {
            if ((c & 65280) != 0 && (c & 65280) != 61440) return null;
            m = (int[])map.get(new Integer(c & 255));
        } else {
            m = (int[])map.get(new Integer(c));
        }
        if (m != null) return this.bboxes[m[0]];
        return null;
    }
}

