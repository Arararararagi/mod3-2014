/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Anchor;
import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.List;
import com.lowagie.text.ListItem;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfChunk;
import com.lowagie.text.pdf.PdfLine;
import java.util.ArrayList;
import java.util.Iterator;

public class PdfCell
extends Rectangle {
    private ArrayList lines;
    private PdfLine line;
    private ArrayList images;
    private float leading;
    private int rownumber;
    private int rowspan;
    private float cellspacing;
    private float cellpadding;
    private boolean header = false;
    private float contentHeight = 0.0f;
    private boolean useAscender;
    private boolean useDescender;
    private boolean useBorderPadding;
    private int verticalAlignment;
    private PdfLine firstLine;
    private PdfLine lastLine;
    private int groupNumber;

    public PdfCell(Cell cell, int rownumber, float left, float right, float top, float cellspacing, float cellpadding) {
        super(left, top, right, top);
        this.cloneNonPositionParameters(cell);
        this.cellpadding = cellpadding;
        this.cellspacing = cellspacing;
        this.verticalAlignment = cell.verticalAlignment();
        this.useAscender = cell.isUseAscender();
        this.useDescender = cell.isUseDescender();
        this.useBorderPadding = cell.isUseBorderPadding();
        this.lines = new ArrayList();
        this.images = new ArrayList();
        this.leading = cell.leading();
        int alignment = cell.horizontalAlignment();
        left+=cellspacing + cellpadding;
        right-=cellspacing + cellpadding;
        left+=this.getBorderWidthInside(4);
        right-=this.getBorderWidthInside(8);
        this.contentHeight = 0.0f;
        this.rowspan = cell.rowspan();
        Iterator i = cell.getElements();
        while (i.hasNext()) {
            Element element = (Element)i.next();
            switch (element.type()) {
                PdfChunk overflow;
                int aCounter;
                PdfChunk chunk;
                ArrayList allActions;
                case 32: 
                case 34: 
                case 35: {
                    this.addImage((Image)element, left, right, 0.4f * this.leading, alignment);
                    break;
                }
                case 14: {
                    if (this.line != null && this.line.size() > 0) {
                        this.line.resetAlignment();
                        this.addLine(this.line);
                    }
                    allActions = new ArrayList();
                    this.processActions(element, null, allActions);
                    aCounter = 0;
                    Iterator items = ((List)element).getItems().iterator();
                    while (items.hasNext()) {
                        ListItem item = (ListItem)items.next();
                        this.line = new PdfLine(left + item.indentationLeft(), right, alignment, item.leading());
                        this.line.setListItem(item);
                        Iterator j = item.getChunks().iterator();
                        while (j.hasNext()) {
                            chunk = new PdfChunk((Chunk)j.next(), (PdfAction)allActions.get(aCounter++));
                            while ((overflow = this.line.add(chunk)) != null) {
                                this.addLine(this.line);
                                this.line = new PdfLine(left + item.indentationLeft(), right, alignment, item.leading());
                                chunk = overflow;
                            }
                            this.line.resetAlignment();
                            this.addLine(this.line);
                            this.line = new PdfLine(left + item.indentationLeft(), right, alignment, this.leading);
                        }
                    }
                    this.line = new PdfLine(left, right, alignment, this.leading);
                    break;
                }
                default: {
                    ArrayList chunks;
                    allActions = new ArrayList();
                    this.processActions(element, null, allActions);
                    aCounter = 0;
                    float currentLineLeading = this.leading;
                    float currentLeft = left;
                    float currentRight = right;
                    if (element instanceof Phrase) {
                        currentLineLeading = ((Phrase)element).leading();
                    }
                    if (element instanceof Paragraph) {
                        Paragraph p = (Paragraph)element;
                        currentLeft+=p.indentationLeft();
                        currentRight-=p.indentationRight();
                    }
                    if (this.line == null) {
                        this.line = new PdfLine(currentLeft, currentRight, alignment, currentLineLeading);
                    }
                    if ((chunks = element.getChunks()).isEmpty()) {
                        this.addLine(this.line);
                        this.line = new PdfLine(currentLeft, currentRight, alignment, currentLineLeading);
                    } else {
                        Iterator j = chunks.iterator();
                        while (j.hasNext()) {
                            Chunk c = (Chunk)j.next();
                            chunk = new PdfChunk(c, (PdfAction)allActions.get(aCounter++));
                            while ((overflow = this.line.add(chunk)) != null) {
                                this.addLine(this.line);
                                this.line = new PdfLine(currentLeft, currentRight, alignment, currentLineLeading);
                                chunk = overflow;
                            }
                        }
                    }
                    switch (element.type()) {
                        case 12: 
                        case 13: 
                        case 16: {
                            this.line.resetAlignment();
                            this.flushCurrentLine();
                        }
                    }
                }
            }
        }
        this.flushCurrentLine();
        if (this.lines.size() > cell.getMaxLines()) {
            String more;
            while (this.lines.size() > cell.getMaxLines()) {
                this.removeLine(this.lines.size() - 1);
            }
            if (cell.getMaxLines() > 0 && (more = cell.getShowTruncation()) != null && more.length() > 0) {
                this.lastLine = (PdfLine)this.lines.get(this.lines.size() - 1);
                if (this.lastLine.size() >= 0) {
                    PdfChunk lastChunk = this.lastLine.getChunk(this.lastLine.size() - 1);
                    float moreWidth = new PdfChunk(more, lastChunk).width();
                    while (lastChunk.toString().length() > 0 && lastChunk.width() + moreWidth > right - left) {
                        lastChunk.setValue(lastChunk.toString().substring(0, lastChunk.length() - 1));
                    }
                    lastChunk.setValue(String.valueOf(lastChunk.toString()) + more);
                } else {
                    this.lastLine.add(new PdfChunk(new Chunk(more), null));
                }
            }
        }
        if (this.useDescender && this.lastLine != null) {
            this.contentHeight-=this.lastLine.getDescender();
        }
        if (this.lines.size() > 0) {
            this.firstLine = (PdfLine)this.lines.get(0);
            float firstLineRealHeight = this.firstLineRealHeight();
            this.contentHeight-=this.firstLine.height();
            this.firstLine.height = firstLineRealHeight;
            this.contentHeight+=firstLineRealHeight;
        }
        float newBottom = top - this.contentHeight - 2.0f * this.cellpadding() - 2.0f * this.cellspacing();
        this.setBottom(newBottom-=this.getBorderWidthInside(1) + this.getBorderWidthInside(2));
        this.rownumber = rownumber;
    }

    public void setBottom(float value) {
        super.setBottom(value);
        float firstLineRealHeight = this.firstLineRealHeight();
        float totalHeight = this.ury - value;
        float nonContentHeight = this.cellpadding() * 2.0f + this.cellspacing() * 2.0f;
        float interiorHeight = totalHeight - (nonContentHeight+=this.getBorderWidthInside(1) + this.getBorderWidthInside(2));
        float extraHeight = 0.0f;
        switch (this.verticalAlignment) {
            case 6: {
                extraHeight = interiorHeight - this.contentHeight;
                break;
            }
            case 5: {
                extraHeight = (interiorHeight - this.contentHeight) / 2.0f;
                break;
            }
            default: {
                extraHeight = 0.0f;
            }
        }
        extraHeight+=this.cellpadding() + this.cellspacing();
        extraHeight+=this.getBorderWidthInside(1);
        if (this.firstLine != null) {
            this.firstLine.height = firstLineRealHeight + extraHeight;
        }
    }

    public float left() {
        return super.left(this.cellspacing);
    }

    public float right() {
        return super.right(this.cellspacing);
    }

    public float top() {
        return super.top(this.cellspacing);
    }

    public float bottom() {
        return super.bottom(this.cellspacing);
    }

    private void addLine(PdfLine line) {
        this.lines.add(line);
        this.contentHeight+=line.height();
        this.lastLine = line;
        this.line = null;
    }

    private PdfLine removeLine(int index) {
        PdfLine oldLine = (PdfLine)this.lines.remove(index);
        this.contentHeight-=oldLine.height();
        if (index == 0 && this.lines.size() > 0) {
            this.firstLine = (PdfLine)this.lines.get(0);
            float firstLineRealHeight = this.firstLineRealHeight();
            this.contentHeight-=this.firstLine.height();
            this.firstLine.height = firstLineRealHeight;
            this.contentHeight+=firstLineRealHeight;
        }
        return oldLine;
    }

    private void flushCurrentLine() {
        if (this.line != null && this.line.size() > 0) {
            this.addLine(this.line);
        }
    }

    private float firstLineRealHeight() {
        PdfChunk chunk;
        float firstLineRealHeight = 0.0f;
        if (this.firstLine != null && (chunk = this.firstLine.getChunk(0)) != null) {
            Image image = chunk.getImage();
            firstLineRealHeight = image != null ? this.firstLine.getChunk(0).getImage().scaledHeight() : (this.useAscender ? this.firstLine.getAscender() : this.leading);
        }
        return firstLineRealHeight;
    }

    private float getBorderWidthInside(int side) {
        float width = 0.0f;
        if (this.useBorderPadding) {
            switch (side) {
                case 4: {
                    width = this.getBorderWidthLeft();
                    break;
                }
                case 8: {
                    width = this.getBorderWidthRight();
                    break;
                }
                case 1: {
                    width = this.getBorderWidthTop();
                    break;
                }
                default: {
                    width = this.getBorderWidthBottom();
                }
            }
            if (!this.isUseVariableBorders()) {
                width/=2.0f;
            }
        }
        return width;
    }

    private float addImage(Image i, float left, float right, float extraHeight, int alignment) {
        Image image = Image.getInstance(i);
        if (image.scaledWidth() > right - left) {
            image.scaleToFit(right - left, 3.4028235E38f);
        }
        this.flushCurrentLine();
        if (this.line == null) {
            this.line = new PdfLine(left, right, alignment, this.leading);
        }
        PdfLine imageLine = this.line;
        right-=left;
        left = 0.0f;
        if ((image.alignment() & 2) == 2) {
            left = right - image.scaledWidth();
        } else if ((image.alignment() & 1) == 1) {
            left+=(right - left - image.scaledWidth()) / 2.0f;
        }
        Chunk imageChunk = new Chunk(image, left, 0.0f);
        imageLine.add(new PdfChunk(imageChunk, null));
        this.addLine(imageLine);
        return imageLine.height();
    }

    public ArrayList getLines(float top, float bottom) {
        float currentPosition = Math.min(this.top(), top);
        this.setTop(currentPosition + this.cellspacing);
        ArrayList<PdfLine> result = new ArrayList<PdfLine>();
        if (this.top() < bottom) {
            return result;
        }
        int size = this.lines.size();
        boolean aboveBottom = true;
        for (int i = 0; i < size && aboveBottom; ++i) {
            this.line = (PdfLine)this.lines.get(i);
            float lineHeight = this.line.height();
            if ((currentPosition-=lineHeight) > bottom + this.cellpadding + this.getBorderWidthInside(2)) {
                result.add(this.line);
                continue;
            }
            aboveBottom = false;
        }
        float difference = 0.0f;
        if (!this.header) {
            if (aboveBottom) {
                this.lines = new ArrayList();
                this.contentHeight = 0.0f;
            } else {
                size = result.size();
                for (int i2 = 0; i2 < size; ++i2) {
                    this.line = this.removeLine(0);
                    difference+=this.line.height();
                }
            }
        }
        if (difference > 0.0f) {
            Iterator i3 = this.images.iterator();
            while (i3.hasNext()) {
                Image image = (Image)i3.next();
                image.setAbsolutePosition(image.absoluteX(), image.absoluteY() - difference - this.leading);
            }
        }
        return result;
    }

    public ArrayList getImages(float top, float bottom) {
        if (this.top() < bottom) {
            return new ArrayList();
        }
        top = Math.min(this.top(), top);
        ArrayList<Image> result = new ArrayList<Image>();
        Iterator i = this.images.iterator();
        while (i.hasNext() && !this.header) {
            Image image = (Image)i.next();
            float height = image.absoluteY();
            if (top - height <= bottom + this.cellpadding) continue;
            image.setAbsolutePosition(image.absoluteX(), top - height);
            result.add(image);
            i.remove();
        }
        return result;
    }

    boolean isHeader() {
        return this.header;
    }

    void setHeader() {
        this.header = true;
    }

    boolean mayBeRemoved() {
        if (!(this.header || this.lines.size() == 0 && this.images.size() == 0)) {
            return false;
        }
        return true;
    }

    public int size() {
        return this.lines.size();
    }

    public int remainingLines() {
        if (this.lines.size() == 0) {
            return 0;
        }
        int result = 0;
        int size = this.lines.size();
        for (int i = 0; i < size; ++i) {
            PdfLine line = (PdfLine)this.lines.get(i);
            if (line.size() <= 0) continue;
            ++result;
        }
        return result;
    }

    public float remainingHeight() {
        float result = 0.0f;
        Iterator i = this.images.iterator();
        while (i.hasNext()) {
            Image image = (Image)i.next();
            result+=image.scaledHeight();
        }
        return (float)this.remainingLines() * this.leading + 2.0f * this.cellpadding + this.cellspacing + result + this.leading / 2.5f;
    }

    public float leading() {
        return this.leading;
    }

    public int rownumber() {
        return this.rownumber;
    }

    public int rowspan() {
        return this.rowspan;
    }

    public float cellspacing() {
        return this.cellspacing;
    }

    public float cellpadding() {
        return this.cellpadding;
    }

    protected void processActions(Element element, PdfAction action, ArrayList allActions) {
        String url;
        if (element.type() == 17 && (url = ((Anchor)element).reference()) != null) {
            action = new PdfAction(url);
        }
        switch (element.type()) {
            Iterator i;
            case 11: 
            case 12: 
            case 13: 
            case 15: 
            case 16: 
            case 17: {
                i = ((ArrayList)element).iterator();
                while (i.hasNext()) {
                    this.processActions((Element)i.next(), action, allActions);
                }
                break;
            }
            case 10: {
                allActions.add(action);
                break;
            }
            case 14: {
                i = ((List)element).getItems().iterator();
                while (i.hasNext()) {
                    this.processActions((Element)i.next(), action, allActions);
                }
                break;
            }
            default: {
                ArrayList tmp = element.getChunks();
                int n = element.getChunks().size();
                while (n-- > 0) {
                    allActions.add(action);
                }
                break block0;
            }
        }
    }

    public int getGroupNumber() {
        return this.groupNumber;
    }

    void setGroupNumber(int number) {
        this.groupNumber = number;
    }

    public Rectangle rectangle(float top, float bottom) {
        Rectangle tmp = new Rectangle(this.left(), this.bottom(), this.right(), this.top());
        tmp.cloneNonPositionParameters(this);
        if (this.top() > top) {
            tmp.setTop(top);
            tmp.setBorder(this.border - (this.border & 1));
        }
        if (this.bottom() < bottom) {
            tmp.setBottom(bottom);
            tmp.setBorder(this.border - (this.border & 2));
        }
        return tmp;
    }

    public void setUseAscender(boolean use) {
        this.useAscender = use;
    }

    public boolean isUseAscender() {
        return this.useAscender;
    }

    public void setUseDescender(boolean use) {
        this.useDescender = use;
    }

    public boolean isUseDescender() {
        return this.useDescender;
    }

    public void setUseBorderPadding(boolean use) {
        this.useBorderPadding = use;
    }

    public boolean isUseBorderPadding() {
        return this.useBorderPadding;
    }
}

