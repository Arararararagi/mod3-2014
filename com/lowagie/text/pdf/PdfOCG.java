/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfObject;

public interface PdfOCG {
    public PdfIndirectReference getRef();

    public PdfObject getPdfObject();
}

