/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfReaderInstance;
import com.lowagie.text.pdf.PdfSpotColor;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;

public class PdfImportedPage
extends PdfTemplate {
    PdfReaderInstance readerInstance;
    int pageNumber;

    PdfImportedPage(PdfReaderInstance readerInstance, PdfWriter writer, int pageNumber) {
        this.readerInstance = readerInstance;
        this.pageNumber = pageNumber;
        this.thisReference = writer.getPdfIndirectReference();
        this.bBox = readerInstance.getReader().getPageSize(pageNumber);
        this.type = 2;
    }

    public PdfImportedPage getFromReader() {
        return this;
    }

    public int getPageNumber() {
        return this.pageNumber;
    }

    public void addImage(Image image, float a, float b, float c, float d, float e, float f) throws DocumentException {
        this.throwError();
    }

    public void addTemplate(PdfTemplate template, float a, float b, float c, float d, float e, float f) {
        this.throwError();
    }

    public PdfContentByte getDuplicate() {
        this.throwError();
        return null;
    }

    PdfStream getFormXObject() throws IOException {
        return this.readerInstance.getFormXObject(this.pageNumber);
    }

    public void setColorFill(PdfSpotColor sp, float tint) {
        this.throwError();
    }

    public void setColorStroke(PdfSpotColor sp, float tint) {
        this.throwError();
    }

    PdfObject getResources() {
        return this.readerInstance.getResources(this.pageNumber);
    }

    public void setFontAndSize(BaseFont bf, float size) {
        this.throwError();
    }

    void throwError() {
        throw new RuntimeException("Content can not be added to a PdfImportedPage.");
    }

    PdfReaderInstance getPdfReaderInstance() {
        return this.readerInstance;
    }
}

