/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfNumber;
import java.util.ArrayList;

public class PdfTextArray {
    ArrayList arrayList = new ArrayList();

    public PdfTextArray(String str) {
        this.arrayList.add(str);
    }

    public PdfTextArray() {
    }

    public void add(PdfNumber number) {
        this.arrayList.add(new Float(number.doubleValue()));
    }

    public void add(float number) {
        this.arrayList.add(new Float(number));
    }

    public void add(String str) {
        this.arrayList.add(str);
    }

    ArrayList getArrayList() {
        return this.arrayList;
    }
}

