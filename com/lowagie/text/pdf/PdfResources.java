/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;

class PdfResources
extends PdfDictionary {
    PdfResources() {
    }

    void add(PdfName key, PdfDictionary resource) {
        if (resource.size() == 0) {
            return;
        }
        PdfDictionary dic = (PdfDictionary)PdfReader.getPdfObject(this.get(key));
        if (dic == null) {
            this.put(key, resource);
        } else {
            dic.putAll(resource);
        }
    }
}

