/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ExtraEncoding;
import com.lowagie.text.pdf.IntHashtable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

public class PdfEncodings {
    protected static final int CIDNONE = 0;
    protected static final int CIDRANGE = 1;
    protected static final int CIDCHAR = 2;
    static final char[] winansiByteToChar;
    static final char[] pdfEncodingByteToChar;
    static final IntHashtable winansi;
    static final IntHashtable pdfEncoding;
    static final HashMap extraEncodings;
    static final HashMap cmaps;
    public static final byte[][] CRLF_CID_NEWLINE;

    static {
        char c;
        int k;
        char[] arrc = new char[256];
        arrc[1] = '\u0001';
        arrc[2] = 2;
        arrc[3] = 3;
        arrc[4] = 4;
        arrc[5] = 5;
        arrc[6] = 6;
        arrc[7] = 7;
        arrc[8] = 8;
        arrc[9] = 9;
        arrc[10] = 10;
        arrc[11] = 11;
        arrc[12] = 12;
        arrc[13] = 13;
        arrc[14] = 14;
        arrc[15] = 15;
        arrc[16] = 16;
        arrc[17] = 17;
        arrc[18] = 18;
        arrc[19] = 19;
        arrc[20] = 20;
        arrc[21] = 21;
        arrc[22] = 22;
        arrc[23] = 23;
        arrc[24] = 24;
        arrc[25] = 25;
        arrc[26] = 26;
        arrc[27] = 27;
        arrc[28] = 28;
        arrc[29] = 29;
        arrc[30] = 30;
        arrc[31] = 31;
        arrc[32] = 32;
        arrc[33] = 33;
        arrc[34] = 34;
        arrc[35] = 35;
        arrc[36] = 36;
        arrc[37] = 37;
        arrc[38] = 38;
        arrc[39] = 39;
        arrc[40] = 40;
        arrc[41] = 41;
        arrc[42] = 42;
        arrc[43] = 43;
        arrc[44] = 44;
        arrc[45] = 45;
        arrc[46] = 46;
        arrc[47] = 47;
        arrc[48] = 48;
        arrc[49] = 49;
        arrc[50] = 50;
        arrc[51] = 51;
        arrc[52] = 52;
        arrc[53] = 53;
        arrc[54] = 54;
        arrc[55] = 55;
        arrc[56] = 56;
        arrc[57] = 57;
        arrc[58] = 58;
        arrc[59] = 59;
        arrc[60] = 60;
        arrc[61] = 61;
        arrc[62] = 62;
        arrc[63] = 63;
        arrc[64] = 64;
        arrc[65] = 65;
        arrc[66] = 66;
        arrc[67] = 67;
        arrc[68] = 68;
        arrc[69] = 69;
        arrc[70] = 70;
        arrc[71] = 71;
        arrc[72] = 72;
        arrc[73] = 73;
        arrc[74] = 74;
        arrc[75] = 75;
        arrc[76] = 76;
        arrc[77] = 77;
        arrc[78] = 78;
        arrc[79] = 79;
        arrc[80] = 80;
        arrc[81] = 81;
        arrc[82] = 82;
        arrc[83] = 83;
        arrc[84] = 84;
        arrc[85] = 85;
        arrc[86] = 86;
        arrc[87] = 87;
        arrc[88] = 88;
        arrc[89] = 89;
        arrc[90] = 90;
        arrc[91] = 91;
        arrc[92] = 92;
        arrc[93] = 93;
        arrc[94] = 94;
        arrc[95] = 95;
        arrc[96] = 96;
        arrc[97] = 97;
        arrc[98] = 98;
        arrc[99] = 99;
        arrc[100] = 100;
        arrc[101] = 101;
        arrc[102] = 102;
        arrc[103] = 103;
        arrc[104] = 104;
        arrc[105] = 105;
        arrc[106] = 106;
        arrc[107] = 107;
        arrc[108] = 108;
        arrc[109] = 109;
        arrc[110] = 110;
        arrc[111] = 111;
        arrc[112] = 112;
        arrc[113] = 113;
        arrc[114] = 114;
        arrc[115] = 115;
        arrc[116] = 116;
        arrc[117] = 117;
        arrc[118] = 118;
        arrc[119] = 119;
        arrc[120] = 120;
        arrc[121] = 121;
        arrc[122] = 122;
        arrc[123] = 123;
        arrc[124] = 124;
        arrc[125] = 125;
        arrc[126] = 126;
        arrc[127] = 127;
        arrc[128] = 8364;
        arrc[129] = 65533;
        arrc[130] = 8218;
        arrc[131] = 402;
        arrc[132] = 8222;
        arrc[133] = 8230;
        arrc[134] = 8224;
        arrc[135] = 8225;
        arrc[136] = 710;
        arrc[137] = 8240;
        arrc[138] = 352;
        arrc[139] = 8249;
        arrc[140] = 338;
        arrc[141] = 65533;
        arrc[142] = 381;
        arrc[143] = 65533;
        arrc[144] = 65533;
        arrc[145] = 8216;
        arrc[146] = 8217;
        arrc[147] = 8220;
        arrc[148] = 8221;
        arrc[149] = 8226;
        arrc[150] = 8211;
        arrc[151] = 8212;
        arrc[152] = 732;
        arrc[153] = 8482;
        arrc[154] = 353;
        arrc[155] = 8250;
        arrc[156] = 339;
        arrc[157] = 65533;
        arrc[158] = 382;
        arrc[159] = 376;
        arrc[160] = 160;
        arrc[161] = 161;
        arrc[162] = 162;
        arrc[163] = 163;
        arrc[164] = 164;
        arrc[165] = 165;
        arrc[166] = 166;
        arrc[167] = 167;
        arrc[168] = 168;
        arrc[169] = 169;
        arrc[170] = 170;
        arrc[171] = 171;
        arrc[172] = 172;
        arrc[173] = 173;
        arrc[174] = 174;
        arrc[175] = 175;
        arrc[176] = 176;
        arrc[177] = 177;
        arrc[178] = 178;
        arrc[179] = 179;
        arrc[180] = 180;
        arrc[181] = 181;
        arrc[182] = 182;
        arrc[183] = 183;
        arrc[184] = 184;
        arrc[185] = 185;
        arrc[186] = 186;
        arrc[187] = 187;
        arrc[188] = 188;
        arrc[189] = 189;
        arrc[190] = 190;
        arrc[191] = 191;
        arrc[192] = 192;
        arrc[193] = 193;
        arrc[194] = 194;
        arrc[195] = 195;
        arrc[196] = 196;
        arrc[197] = 197;
        arrc[198] = 198;
        arrc[199] = 199;
        arrc[200] = 200;
        arrc[201] = 201;
        arrc[202] = 202;
        arrc[203] = 203;
        arrc[204] = 204;
        arrc[205] = 205;
        arrc[206] = 206;
        arrc[207] = 207;
        arrc[208] = 208;
        arrc[209] = 209;
        arrc[210] = 210;
        arrc[211] = 211;
        arrc[212] = 212;
        arrc[213] = 213;
        arrc[214] = 214;
        arrc[215] = 215;
        arrc[216] = 216;
        arrc[217] = 217;
        arrc[218] = 218;
        arrc[219] = 219;
        arrc[220] = 220;
        arrc[221] = 221;
        arrc[222] = 222;
        arrc[223] = 223;
        arrc[224] = 224;
        arrc[225] = 225;
        arrc[226] = 226;
        arrc[227] = 227;
        arrc[228] = 228;
        arrc[229] = 229;
        arrc[230] = 230;
        arrc[231] = 231;
        arrc[232] = 232;
        arrc[233] = 233;
        arrc[234] = 234;
        arrc[235] = 235;
        arrc[236] = 236;
        arrc[237] = 237;
        arrc[238] = 238;
        arrc[239] = 239;
        arrc[240] = 240;
        arrc[241] = 241;
        arrc[242] = 242;
        arrc[243] = 243;
        arrc[244] = 244;
        arrc[245] = 245;
        arrc[246] = 246;
        arrc[247] = 247;
        arrc[248] = 248;
        arrc[249] = 249;
        arrc[250] = 250;
        arrc[251] = 251;
        arrc[252] = 252;
        arrc[253] = 253;
        arrc[254] = 254;
        arrc[255] = 255;
        winansiByteToChar = arrc;
        char[] arrc2 = new char[256];
        arrc2[1] = '\u0001';
        arrc2[2] = 2;
        arrc2[3] = 3;
        arrc2[4] = 4;
        arrc2[5] = 5;
        arrc2[6] = 6;
        arrc2[7] = 7;
        arrc2[8] = 8;
        arrc2[9] = 9;
        arrc2[10] = 10;
        arrc2[11] = 11;
        arrc2[12] = 12;
        arrc2[13] = 13;
        arrc2[14] = 14;
        arrc2[15] = 15;
        arrc2[16] = 16;
        arrc2[17] = 17;
        arrc2[18] = 18;
        arrc2[19] = 19;
        arrc2[20] = 20;
        arrc2[21] = 21;
        arrc2[22] = 22;
        arrc2[23] = 23;
        arrc2[24] = 24;
        arrc2[25] = 25;
        arrc2[26] = 26;
        arrc2[27] = 27;
        arrc2[28] = 28;
        arrc2[29] = 29;
        arrc2[30] = 30;
        arrc2[31] = 31;
        arrc2[32] = 32;
        arrc2[33] = 33;
        arrc2[34] = 34;
        arrc2[35] = 35;
        arrc2[36] = 36;
        arrc2[37] = 37;
        arrc2[38] = 38;
        arrc2[39] = 39;
        arrc2[40] = 40;
        arrc2[41] = 41;
        arrc2[42] = 42;
        arrc2[43] = 43;
        arrc2[44] = 44;
        arrc2[45] = 45;
        arrc2[46] = 46;
        arrc2[47] = 47;
        arrc2[48] = 48;
        arrc2[49] = 49;
        arrc2[50] = 50;
        arrc2[51] = 51;
        arrc2[52] = 52;
        arrc2[53] = 53;
        arrc2[54] = 54;
        arrc2[55] = 55;
        arrc2[56] = 56;
        arrc2[57] = 57;
        arrc2[58] = 58;
        arrc2[59] = 59;
        arrc2[60] = 60;
        arrc2[61] = 61;
        arrc2[62] = 62;
        arrc2[63] = 63;
        arrc2[64] = 64;
        arrc2[65] = 65;
        arrc2[66] = 66;
        arrc2[67] = 67;
        arrc2[68] = 68;
        arrc2[69] = 69;
        arrc2[70] = 70;
        arrc2[71] = 71;
        arrc2[72] = 72;
        arrc2[73] = 73;
        arrc2[74] = 74;
        arrc2[75] = 75;
        arrc2[76] = 76;
        arrc2[77] = 77;
        arrc2[78] = 78;
        arrc2[79] = 79;
        arrc2[80] = 80;
        arrc2[81] = 81;
        arrc2[82] = 82;
        arrc2[83] = 83;
        arrc2[84] = 84;
        arrc2[85] = 85;
        arrc2[86] = 86;
        arrc2[87] = 87;
        arrc2[88] = 88;
        arrc2[89] = 89;
        arrc2[90] = 90;
        arrc2[91] = 91;
        arrc2[92] = 92;
        arrc2[93] = 93;
        arrc2[94] = 94;
        arrc2[95] = 95;
        arrc2[96] = 96;
        arrc2[97] = 97;
        arrc2[98] = 98;
        arrc2[99] = 99;
        arrc2[100] = 100;
        arrc2[101] = 101;
        arrc2[102] = 102;
        arrc2[103] = 103;
        arrc2[104] = 104;
        arrc2[105] = 105;
        arrc2[106] = 106;
        arrc2[107] = 107;
        arrc2[108] = 108;
        arrc2[109] = 109;
        arrc2[110] = 110;
        arrc2[111] = 111;
        arrc2[112] = 112;
        arrc2[113] = 113;
        arrc2[114] = 114;
        arrc2[115] = 115;
        arrc2[116] = 116;
        arrc2[117] = 117;
        arrc2[118] = 118;
        arrc2[119] = 119;
        arrc2[120] = 120;
        arrc2[121] = 121;
        arrc2[122] = 122;
        arrc2[123] = 123;
        arrc2[124] = 124;
        arrc2[125] = 125;
        arrc2[126] = 126;
        arrc2[127] = 127;
        arrc2[128] = 8226;
        arrc2[129] = 8224;
        arrc2[130] = 8225;
        arrc2[131] = 8230;
        arrc2[132] = 8212;
        arrc2[133] = 8211;
        arrc2[134] = 402;
        arrc2[135] = 8260;
        arrc2[136] = 8249;
        arrc2[137] = 8250;
        arrc2[138] = 8722;
        arrc2[139] = 8240;
        arrc2[140] = 8222;
        arrc2[141] = 8220;
        arrc2[142] = 8221;
        arrc2[143] = 8216;
        arrc2[144] = 8217;
        arrc2[145] = 8218;
        arrc2[146] = 8482;
        arrc2[147] = 64257;
        arrc2[148] = 64258;
        arrc2[149] = 321;
        arrc2[150] = 338;
        arrc2[151] = 352;
        arrc2[152] = 376;
        arrc2[153] = 381;
        arrc2[154] = 305;
        arrc2[155] = 322;
        arrc2[156] = 339;
        arrc2[157] = 353;
        arrc2[158] = 382;
        arrc2[159] = 65533;
        arrc2[160] = 8364;
        arrc2[161] = 161;
        arrc2[162] = 162;
        arrc2[163] = 163;
        arrc2[164] = 164;
        arrc2[165] = 165;
        arrc2[166] = 166;
        arrc2[167] = 167;
        arrc2[168] = 168;
        arrc2[169] = 169;
        arrc2[170] = 170;
        arrc2[171] = 171;
        arrc2[172] = 172;
        arrc2[173] = 173;
        arrc2[174] = 174;
        arrc2[175] = 175;
        arrc2[176] = 176;
        arrc2[177] = 177;
        arrc2[178] = 178;
        arrc2[179] = 179;
        arrc2[180] = 180;
        arrc2[181] = 181;
        arrc2[182] = 182;
        arrc2[183] = 183;
        arrc2[184] = 184;
        arrc2[185] = 185;
        arrc2[186] = 186;
        arrc2[187] = 187;
        arrc2[188] = 188;
        arrc2[189] = 189;
        arrc2[190] = 190;
        arrc2[191] = 191;
        arrc2[192] = 192;
        arrc2[193] = 193;
        arrc2[194] = 194;
        arrc2[195] = 195;
        arrc2[196] = 196;
        arrc2[197] = 197;
        arrc2[198] = 198;
        arrc2[199] = 199;
        arrc2[200] = 200;
        arrc2[201] = 201;
        arrc2[202] = 202;
        arrc2[203] = 203;
        arrc2[204] = 204;
        arrc2[205] = 205;
        arrc2[206] = 206;
        arrc2[207] = 207;
        arrc2[208] = 208;
        arrc2[209] = 209;
        arrc2[210] = 210;
        arrc2[211] = 211;
        arrc2[212] = 212;
        arrc2[213] = 213;
        arrc2[214] = 214;
        arrc2[215] = 215;
        arrc2[216] = 216;
        arrc2[217] = 217;
        arrc2[218] = 218;
        arrc2[219] = 219;
        arrc2[220] = 220;
        arrc2[221] = 221;
        arrc2[222] = 222;
        arrc2[223] = 223;
        arrc2[224] = 224;
        arrc2[225] = 225;
        arrc2[226] = 226;
        arrc2[227] = 227;
        arrc2[228] = 228;
        arrc2[229] = 229;
        arrc2[230] = 230;
        arrc2[231] = 231;
        arrc2[232] = 232;
        arrc2[233] = 233;
        arrc2[234] = 234;
        arrc2[235] = 235;
        arrc2[236] = 236;
        arrc2[237] = 237;
        arrc2[238] = 238;
        arrc2[239] = 239;
        arrc2[240] = 240;
        arrc2[241] = 241;
        arrc2[242] = 242;
        arrc2[243] = 243;
        arrc2[244] = 244;
        arrc2[245] = 245;
        arrc2[246] = 246;
        arrc2[247] = 247;
        arrc2[248] = 248;
        arrc2[249] = 249;
        arrc2[250] = 250;
        arrc2[251] = 251;
        arrc2[252] = 252;
        arrc2[253] = 253;
        arrc2[254] = 254;
        arrc2[255] = 255;
        pdfEncodingByteToChar = arrc2;
        winansi = new IntHashtable();
        pdfEncoding = new IntHashtable();
        extraEncodings = new HashMap();
        for (k = 128; k < 160; ++k) {
            c = winansiByteToChar[k];
            if (c == '\ufffd') continue;
            winansi.put(c, k);
        }
        for (k = 128; k < 161; ++k) {
            c = pdfEncodingByteToChar[k];
            if (c == '\ufffd') continue;
            pdfEncoding.put(c, k);
        }
        PdfEncodings.addExtraEncoding("Wingdings", new WingdingsConversion());
        PdfEncodings.addExtraEncoding("Symbol", new SymbolConversion(true));
        PdfEncodings.addExtraEncoding("ZapfDingbats", new SymbolConversion(false));
        PdfEncodings.addExtraEncoding("SymbolTT", new SymbolTTConversion());
        PdfEncodings.addExtraEncoding("Cp437", new Cp437Conversion());
        cmaps = new HashMap();
        CRLF_CID_NEWLINE = new byte[][]{{10}, {13, 10}};
    }

    public static final byte[] convertToBytes(String text, String encoding) {
        if (text == null) {
            return new byte[0];
        }
        if (encoding == null || encoding.length() == 0) {
            int len = text.length();
            byte[] b = new byte[len];
            for (int k = 0; k < len; ++k) {
                b[k] = (byte)text.charAt(k);
            }
            return b;
        }
        ExtraEncoding extra = null;
        HashMap b = extraEncodings;
        synchronized (b) {
            extra = (ExtraEncoding)extraEncodings.get(encoding.toLowerCase());
        }
        if (extra != null && (b = (HashMap)extra.charToByte(text, encoding)) != null) {
            return b;
        }
        IntHashtable hash = null;
        if (encoding.equals("Cp1252")) {
            hash = winansi;
        } else if (encoding.equals("PDF")) {
            hash = pdfEncoding;
        }
        if (hash != null) {
            char[] cc = text.toCharArray();
            int len = cc.length;
            int ptr = 0;
            byte[] b2 = new byte[len];
            int c = 0;
            for (int k = 0; k < len; ++k) {
                int char1 = cc[k];
                c = char1 < 128 || char1 >= 160 && char1 <= 255 ? char1 : hash.get(char1);
                if (c == 0) continue;
                b2[ptr++] = (byte)c;
            }
            if (ptr == len) {
                return b2;
            }
            byte[] b22 = new byte[ptr];
            System.arraycopy(b2, 0, b22, 0, ptr);
            return b22;
        }
        if (encoding.equals("UnicodeBig")) {
            char[] cc = text.toCharArray();
            int len = cc.length;
            byte[] b3 = new byte[cc.length * 2 + 2];
            b3[0] = -2;
            b3[1] = -1;
            int bptr = 2;
            for (int k = 0; k < len; ++k) {
                char c = cc[k];
                b3[bptr++] = (byte)(c >> 8);
                b3[bptr++] = (byte)(c & 255);
            }
            return b3;
        }
        try {
            return text.getBytes(encoding);
        }
        catch (UnsupportedEncodingException e) {
            throw new ExceptionConverter(e);
        }
    }

    public static final String convertToString(byte[] bytes, String encoding) {
        String text;
        if (bytes == null) {
            return "";
        }
        if (encoding == null || encoding.length() == 0) {
            char[] c = new char[bytes.length];
            for (int k = 0; k < bytes.length; ++k) {
                c[k] = (char)(bytes[k] & 255);
            }
            return new String(c);
        }
        ExtraEncoding extra = null;
        HashMap k = extraEncodings;
        synchronized (k) {
            extra = (ExtraEncoding)extraEncodings.get(encoding.toLowerCase());
        }
        if (extra != null && (text = extra.byteToChar(bytes, encoding)) != null) {
            return text;
        }
        char[] ch = null;
        if (encoding.equals("Cp1252")) {
            ch = winansiByteToChar;
        } else if (encoding.equals("PDF")) {
            ch = pdfEncodingByteToChar;
        }
        if (ch != null) {
            int len = bytes.length;
            char[] c = new char[len];
            for (int k2 = 0; k2 < len; ++k2) {
                c[k2] = ch[bytes[k2] & 255];
            }
            return new String(c);
        }
        try {
            return new String(bytes, encoding);
        }
        catch (UnsupportedEncodingException e) {
            throw new ExceptionConverter(e);
        }
    }

    public static boolean isPdfDocEncoding(String text) {
        if (text == null) {
            return true;
        }
        int len = text.length();
        for (int k = 0; k < len; ++k) {
            char char1 = text.charAt(k);
            if (char1 < '' || char1 >= '\u00a0' && char1 <= '\u00ff' || pdfEncoding.containsKey(char1)) continue;
            return false;
        }
        return true;
    }

    public static void clearCmap(String name) {
        HashMap hashMap = cmaps;
        synchronized (hashMap) {
            if (name.length() == 0) {
                cmaps.clear();
            } else {
                cmaps.remove(name);
            }
        }
    }

    public static void loadCmap(String name, byte[][] newline) {
        block8 : {
            try {
                char[][] planes = null;
                HashMap hashMap = cmaps;
                synchronized (hashMap) {
                    planes = (char[][])cmaps.get(name);
                }
                if (planes != null) break block8;
                planes = PdfEncodings.readCmap(name, newline);
                hashMap = cmaps;
                synchronized (hashMap) {
                    cmaps.put(name, planes);
                }
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
        }
    }

    public static String convertCmap(String name, byte[] seq) {
        return PdfEncodings.convertCmap(name, seq, 0, seq.length);
    }

    public static String convertCmap(String name, byte[] seq, int start, int length) {
        try {
            char[][] planes = null;
            HashMap hashMap = cmaps;
            synchronized (hashMap) {
                planes = (char[][])cmaps.get(name);
            }
            if (planes == null) {
                planes = PdfEncodings.readCmap(name, null);
                hashMap = cmaps;
                synchronized (hashMap) {
                    cmaps.put(name, planes);
                }
            }
            return PdfEncodings.decodeSequence(seq, start, length, planes);
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }

    static String decodeSequence(byte[] seq, int start, int length, char[][] planes) {
        StringBuffer buf = new StringBuffer();
        int end = start + length;
        int currentPlane = 0;
        for (int k = start; k < end; ++k) {
            char[] plane = planes[currentPlane];
            int one = seq[k] & 255;
            char cid = plane[one];
            if ((cid & 32768) == 0) {
                buf.append(cid);
                currentPlane = 0;
                continue;
            }
            currentPlane = cid & 32767;
        }
        return buf.toString();
    }

    static char[][] readCmap(String name, byte[][] newline) throws IOException {
        ArrayList<char[]> planes = new ArrayList<char[]>();
        planes.add(new char[256]);
        PdfEncodings.readCmap(name, planes);
        if (newline != null) {
            for (int k = 0; k < newline.length; ++k) {
                PdfEncodings.encodeSequence(newline[k].length, newline[k], '\u7fff', planes);
            }
        }
        char[][] ret = new char[planes.size()][];
        return (char[][])planes.toArray((T[])ret);
    }

    static void readCmap(String name, ArrayList planes) throws IOException {
        String fullName = "com/lowagie/text/pdf/fonts/cmaps/" + name;
        InputStream in = BaseFont.getResourceStream(fullName);
        if (in == null) {
            throw new IOException("The Cmap " + name + " was not found.");
        }
        PdfEncodings.encodeStream(in, planes);
        in.close();
    }

    static void encodeStream(InputStream in, ArrayList planes) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(in, "iso-8859-1"));
        String line = null;
        int state = 0;
        byte[] seqs = new byte[7];
        block5 : while ((line = rd.readLine()) != null) {
            if (line.length() < 6) continue;
            switch (state) {
                String t;
                int size;
                StringTokenizer tk;
                long start;
                case 0: {
                    if (line.indexOf("begincidrange") >= 0) {
                        state = 1;
                        break;
                    }
                    if (line.indexOf("begincidchar") >= 0) {
                        state = 2;
                        break;
                    }
                    if (line.indexOf("usecmap") < 0) break;
                    tk = new StringTokenizer(line);
                    t = tk.nextToken();
                    PdfEncodings.readCmap(t.substring(1), planes);
                    break;
                }
                case 1: {
                    if (line.indexOf("endcidrange") >= 0) {
                        state = 0;
                        break;
                    }
                    tk = new StringTokenizer(line);
                    t = tk.nextToken();
                    size = t.length() / 2 - 1;
                    start = Long.parseLong(t.substring(1, t.length() - 1), 16);
                    t = tk.nextToken();
                    long end = Long.parseLong(t.substring(1, t.length() - 1), 16);
                    t = tk.nextToken();
                    int cid = Integer.parseInt(t);
                    for (long k = start; k <= end; ++k) {
                        PdfEncodings.breakLong(k, size, seqs);
                        PdfEncodings.encodeSequence(size, seqs, (char)cid, planes);
                        ++cid;
                    }
                    continue block5;
                }
                case 2: {
                    if (line.indexOf("endcidchar") >= 0) {
                        state = 0;
                        break;
                    }
                    tk = new StringTokenizer(line);
                    t = tk.nextToken();
                    size = t.length() / 2 - 1;
                    start = Long.parseLong(t.substring(1, t.length() - 1), 16);
                    t = tk.nextToken();
                    int cid = Integer.parseInt(t);
                    PdfEncodings.breakLong(start, size, seqs);
                    PdfEncodings.encodeSequence(size, seqs, (char)cid, planes);
                }
            }
        }
    }

    static void breakLong(long n, int size, byte[] seqs) {
        for (int k = 0; k < size; ++k) {
            seqs[k] = (byte)(n >> (size - 1 - k) * 8);
        }
    }

    static void encodeSequence(int size, byte[] seqs, char cid, ArrayList planes) {
        int one;
        int nextPlane = 0;
        for (int idx = 0; idx < --size; ++idx) {
            int one2;
            char[] plane = (char[])planes.get(nextPlane);
            char c = plane[one2 = seqs[idx] & 255];
            if (c != '\u0000' && (c & 32768) == 0) {
                throw new RuntimeException("Inconsistent mapping.");
            }
            if (c == '\u0000') {
                planes.add(new char[256]);
                plane[one2] = c = (char)(planes.size() - 1 | 32768);
            }
            nextPlane = c & 32767;
        }
        char[] plane = (char[])planes.get(nextPlane);
        char c = plane[one = seqs[size] & 255];
        if ((c & 32768) != 0) {
            throw new RuntimeException("Inconsistent mapping.");
        }
        plane[one] = cid;
    }

    public static void addExtraEncoding(String name, ExtraEncoding enc) {
        HashMap hashMap = extraEncodings;
        synchronized (hashMap) {
            extraEncodings.put(name.toLowerCase(), enc);
        }
    }

    private static class Cp437Conversion
    implements ExtraEncoding {
        private static IntHashtable c2b = new IntHashtable();
        private static final char[] table = new char[]{'\u00c7', '\u00fc', '\u00e9', '\u00e2', '\u00e4', '\u00e0', '\u00e5', '\u00e7', '\u00ea', '\u00eb', '\u00e8', '\u00ef', '\u00ee', '\u00ec', '\u00c4', '\u00c5', '\u00c9', '\u00e6', '\u00c6', '\u00f4', '\u00f6', '\u00f2', '\u00fb', '\u00f9', '\u00ff', '\u00d6', '\u00dc', '\u00a2', '\u00a3', '\u00a5', '\u20a7', '\u0192', '\u00e1', '\u00ed', '\u00f3', '\u00fa', '\u00f1', '\u00d1', '\u00aa', '\u00ba', '\u00bf', '\u2310', '\u00ac', '\u00bd', '\u00bc', '\u00a1', '\u00ab', '\u00bb', '\u2591', '\u2592', '\u2593', '\u2502', '\u2524', '\u2561', '\u2562', '\u2556', '\u2555', '\u2563', '\u2551', '\u2557', '\u255d', '\u255c', '\u255b', '\u2510', '\u2514', '\u2534', '\u252c', '\u251c', '\u2500', '\u253c', '\u255e', '\u255f', '\u255a', '\u2554', '\u2569', '\u2566', '\u2560', '\u2550', '\u256c', '\u2567', '\u2568', '\u2564', '\u2565', '\u2559', '\u2558', '\u2552', '\u2553', '\u256b', '\u256a', '\u2518', '\u250c', '\u2588', '\u2584', '\u258c', '\u2590', '\u2580', '\u03b1', '\u00df', '\u0393', '\u03c0', '\u03a3', '\u03c3', '\u00b5', '\u03c4', '\u03a6', '\u0398', '\u03a9', '\u03b4', '\u221e', '\u03c6', '\u03b5', '\u2229', '\u2261', '\u00b1', '\u2265', '\u2264', '\u2320', '\u2321', '\u00f7', '\u2248', '\u00b0', '\u2219', '\u00b7', '\u221a', '\u207f', '\u00b2', '\u25a0', '\u00a0'};

        static {
            for (int k = 0; k < table.length; ++k) {
                c2b.put(table[k], k + 128);
            }
        }

        Cp437Conversion() {
        }

        public byte[] charToByte(String text, String encoding) {
            char[] cc = text.toCharArray();
            byte[] b = new byte[cc.length];
            int ptr = 0;
            int len = cc.length;
            for (int k = 0; k < len; ++k) {
                char c = cc[k];
                if (c < ' ') continue;
                if (c < '') {
                    b[ptr++] = (byte)c;
                    continue;
                }
                byte v = (byte)c2b.get(c);
                if (v == 0) continue;
                b[ptr++] = v;
            }
            if (ptr == len) {
                return b;
            }
            byte[] b2 = new byte[ptr];
            System.arraycopy(b, 0, b2, 0, ptr);
            return b2;
        }

        public String byteToChar(byte[] b, String encoding) {
            int len = b.length;
            char[] cc = new char[len];
            int ptr = 0;
            for (int k = 0; k < len; ++k) {
                int c = b[k] & 255;
                if (c < 32) continue;
                if (c < 128) {
                    cc[ptr++] = (char)c;
                    continue;
                }
                char v = table[c - 128];
                cc[ptr++] = v;
            }
            return new String(cc, 0, ptr);
        }
    }

    private static class SymbolConversion
    implements ExtraEncoding {
        private static final IntHashtable t1;
        private static final IntHashtable t2;
        private IntHashtable translation;
        private static final char[] table1;
        private static final char[] table2;

        static {
            char v;
            int k;
            t1 = new IntHashtable();
            t2 = new IntHashtable();
            char[] arrc = new char[224];
            arrc[0] = 32;
            arrc[1] = 33;
            arrc[2] = 8704;
            arrc[3] = 35;
            arrc[4] = 8707;
            arrc[5] = 37;
            arrc[6] = 38;
            arrc[7] = 8715;
            arrc[8] = 40;
            arrc[9] = 41;
            arrc[10] = 42;
            arrc[11] = 43;
            arrc[12] = 44;
            arrc[13] = 45;
            arrc[14] = 46;
            arrc[15] = 47;
            arrc[16] = 48;
            arrc[17] = 49;
            arrc[18] = 50;
            arrc[19] = 51;
            arrc[20] = 52;
            arrc[21] = 53;
            arrc[22] = 54;
            arrc[23] = 55;
            arrc[24] = 56;
            arrc[25] = 57;
            arrc[26] = 58;
            arrc[27] = 59;
            arrc[28] = 60;
            arrc[29] = 61;
            arrc[30] = 62;
            arrc[31] = 63;
            arrc[32] = 8773;
            arrc[33] = 913;
            arrc[34] = 914;
            arrc[35] = 935;
            arrc[36] = 916;
            arrc[37] = 917;
            arrc[38] = 934;
            arrc[39] = 915;
            arrc[40] = 919;
            arrc[41] = 921;
            arrc[42] = 977;
            arrc[43] = 922;
            arrc[44] = 923;
            arrc[45] = 924;
            arrc[46] = 925;
            arrc[47] = 927;
            arrc[48] = 928;
            arrc[49] = 920;
            arrc[50] = 929;
            arrc[51] = 931;
            arrc[52] = 932;
            arrc[53] = 933;
            arrc[54] = 962;
            arrc[55] = 937;
            arrc[56] = 926;
            arrc[57] = 936;
            arrc[58] = 918;
            arrc[59] = 91;
            arrc[60] = 8756;
            arrc[61] = 93;
            arrc[62] = 8869;
            arrc[63] = 95;
            arrc[64] = 773;
            arrc[65] = 945;
            arrc[66] = 946;
            arrc[67] = 967;
            arrc[68] = 948;
            arrc[69] = 949;
            arrc[70] = 981;
            arrc[71] = 947;
            arrc[72] = 951;
            arrc[73] = 953;
            arrc[74] = 966;
            arrc[75] = 954;
            arrc[76] = 955;
            arrc[77] = 956;
            arrc[78] = 957;
            arrc[79] = 959;
            arrc[80] = 960;
            arrc[81] = 952;
            arrc[82] = 961;
            arrc[83] = 963;
            arrc[84] = 964;
            arrc[85] = 965;
            arrc[86] = 982;
            arrc[87] = 969;
            arrc[88] = 958;
            arrc[89] = 968;
            arrc[90] = 950;
            arrc[91] = 123;
            arrc[92] = 124;
            arrc[93] = 125;
            arrc[94] = 126;
            arrc[128] = 8364;
            arrc[129] = 978;
            arrc[130] = 8242;
            arrc[131] = 8804;
            arrc[132] = 8260;
            arrc[133] = 8734;
            arrc[134] = 402;
            arrc[135] = 9827;
            arrc[136] = 9830;
            arrc[137] = 9829;
            arrc[138] = 9824;
            arrc[139] = 8596;
            arrc[140] = 8592;
            arrc[141] = 8593;
            arrc[142] = 8594;
            arrc[143] = 8595;
            arrc[144] = 176;
            arrc[145] = 177;
            arrc[146] = 8243;
            arrc[147] = 8805;
            arrc[148] = 215;
            arrc[149] = 8733;
            arrc[150] = 8706;
            arrc[151] = 8226;
            arrc[152] = 247;
            arrc[153] = 8800;
            arrc[154] = 8801;
            arrc[155] = 8776;
            arrc[156] = 8230;
            arrc[157] = 9474;
            arrc[158] = 9472;
            arrc[159] = 8629;
            arrc[160] = 8501;
            arrc[161] = 8465;
            arrc[162] = 8476;
            arrc[163] = 8472;
            arrc[164] = 8855;
            arrc[165] = 8853;
            arrc[166] = 8709;
            arrc[167] = 8745;
            arrc[168] = 8746;
            arrc[169] = 8835;
            arrc[170] = 8839;
            arrc[171] = 8836;
            arrc[172] = 8834;
            arrc[173] = 8838;
            arrc[174] = 8712;
            arrc[175] = 8713;
            arrc[176] = 8736;
            arrc[177] = 8711;
            arrc[178] = 174;
            arrc[179] = 169;
            arrc[180] = 8482;
            arrc[181] = 8719;
            arrc[182] = 8730;
            arrc[183] = 8226;
            arrc[184] = 172;
            arrc[185] = 8743;
            arrc[186] = 8744;
            arrc[187] = 8660;
            arrc[188] = 8656;
            arrc[189] = 8657;
            arrc[190] = 8658;
            arrc[191] = 8659;
            arrc[192] = 9674;
            arrc[193] = 9001;
            arrc[197] = 8721;
            arrc[198] = 9115;
            arrc[199] = 9116;
            arrc[200] = 9117;
            arrc[201] = 9121;
            arrc[202] = 9122;
            arrc[203] = 9123;
            arrc[204] = 9127;
            arrc[205] = 9128;
            arrc[206] = 9129;
            arrc[207] = 9130;
            arrc[209] = 9002;
            arrc[210] = 8747;
            arrc[211] = 8992;
            arrc[212] = 9134;
            arrc[213] = 8993;
            arrc[214] = 9118;
            arrc[215] = 9119;
            arrc[216] = 9120;
            arrc[217] = 9124;
            arrc[218] = 9125;
            arrc[219] = 9126;
            arrc[220] = 9131;
            arrc[221] = 9132;
            arrc[222] = 9133;
            table1 = arrc;
            char[] arrc2 = new char[224];
            arrc2[0] = 32;
            arrc2[1] = 9985;
            arrc2[2] = 9986;
            arrc2[3] = 9987;
            arrc2[4] = 9988;
            arrc2[5] = 9742;
            arrc2[6] = 9990;
            arrc2[7] = 9991;
            arrc2[8] = 9992;
            arrc2[9] = 9993;
            arrc2[10] = 9755;
            arrc2[11] = 9758;
            arrc2[12] = 9996;
            arrc2[13] = 9997;
            arrc2[14] = 9998;
            arrc2[15] = 9999;
            arrc2[16] = 10000;
            arrc2[17] = 10001;
            arrc2[18] = 10002;
            arrc2[19] = 10003;
            arrc2[20] = 10004;
            arrc2[21] = 10005;
            arrc2[22] = 10006;
            arrc2[23] = 10007;
            arrc2[24] = 10008;
            arrc2[25] = 10009;
            arrc2[26] = 10010;
            arrc2[27] = 10011;
            arrc2[28] = 10012;
            arrc2[29] = 10013;
            arrc2[30] = 10014;
            arrc2[31] = 10015;
            arrc2[32] = 10016;
            arrc2[33] = 10017;
            arrc2[34] = 10018;
            arrc2[35] = 10019;
            arrc2[36] = 10020;
            arrc2[37] = 10021;
            arrc2[38] = 10022;
            arrc2[39] = 10023;
            arrc2[40] = 9733;
            arrc2[41] = 10025;
            arrc2[42] = 10026;
            arrc2[43] = 10027;
            arrc2[44] = 10028;
            arrc2[45] = 10029;
            arrc2[46] = 10030;
            arrc2[47] = 10031;
            arrc2[48] = 10032;
            arrc2[49] = 10033;
            arrc2[50] = 10034;
            arrc2[51] = 10035;
            arrc2[52] = 10036;
            arrc2[53] = 10037;
            arrc2[54] = 10038;
            arrc2[55] = 10039;
            arrc2[56] = 10040;
            arrc2[57] = 10041;
            arrc2[58] = 10042;
            arrc2[59] = 10043;
            arrc2[60] = 10044;
            arrc2[61] = 10045;
            arrc2[62] = 10046;
            arrc2[63] = 10047;
            arrc2[64] = 10048;
            arrc2[65] = 10049;
            arrc2[66] = 10050;
            arrc2[67] = 10051;
            arrc2[68] = 10052;
            arrc2[69] = 10053;
            arrc2[70] = 10054;
            arrc2[71] = 10055;
            arrc2[72] = 10056;
            arrc2[73] = 10057;
            arrc2[74] = 10058;
            arrc2[75] = 10059;
            arrc2[76] = 9679;
            arrc2[77] = 10061;
            arrc2[78] = 9632;
            arrc2[79] = 10063;
            arrc2[80] = 10064;
            arrc2[81] = 10065;
            arrc2[82] = 10066;
            arrc2[83] = 9650;
            arrc2[84] = 9660;
            arrc2[85] = 9670;
            arrc2[86] = 10070;
            arrc2[87] = 9687;
            arrc2[88] = 10072;
            arrc2[89] = 10073;
            arrc2[90] = 10074;
            arrc2[91] = 10075;
            arrc2[92] = 10076;
            arrc2[93] = 10077;
            arrc2[94] = 10078;
            arrc2[129] = 10081;
            arrc2[130] = 10082;
            arrc2[131] = 10083;
            arrc2[132] = 10084;
            arrc2[133] = 10085;
            arrc2[134] = 10086;
            arrc2[135] = 10087;
            arrc2[136] = 9827;
            arrc2[137] = 9830;
            arrc2[138] = 9829;
            arrc2[139] = 9824;
            arrc2[140] = 9312;
            arrc2[141] = 9313;
            arrc2[142] = 9314;
            arrc2[143] = 9315;
            arrc2[144] = 9316;
            arrc2[145] = 9317;
            arrc2[146] = 9318;
            arrc2[147] = 9319;
            arrc2[148] = 9320;
            arrc2[149] = 9321;
            arrc2[150] = 10102;
            arrc2[151] = 10103;
            arrc2[152] = 10104;
            arrc2[153] = 10105;
            arrc2[154] = 10106;
            arrc2[155] = 10107;
            arrc2[156] = 10108;
            arrc2[157] = 10109;
            arrc2[158] = 10110;
            arrc2[159] = 10111;
            arrc2[160] = 10112;
            arrc2[161] = 10113;
            arrc2[162] = 10114;
            arrc2[163] = 10115;
            arrc2[164] = 10116;
            arrc2[165] = 10117;
            arrc2[166] = 10118;
            arrc2[167] = 10119;
            arrc2[168] = 10120;
            arrc2[169] = 10121;
            arrc2[170] = 10122;
            arrc2[171] = 10123;
            arrc2[172] = 10124;
            arrc2[173] = 10125;
            arrc2[174] = 10126;
            arrc2[175] = 10127;
            arrc2[176] = 10128;
            arrc2[177] = 10129;
            arrc2[178] = 10130;
            arrc2[179] = 10131;
            arrc2[180] = 10132;
            arrc2[181] = 8594;
            arrc2[182] = 8596;
            arrc2[183] = 8597;
            arrc2[184] = 10136;
            arrc2[185] = 10137;
            arrc2[186] = 10138;
            arrc2[187] = 10139;
            arrc2[188] = 10140;
            arrc2[189] = 10141;
            arrc2[190] = 10142;
            arrc2[191] = 10143;
            arrc2[192] = 10144;
            arrc2[193] = 10145;
            arrc2[194] = 10146;
            arrc2[195] = 10147;
            arrc2[196] = 10148;
            arrc2[197] = 10149;
            arrc2[198] = 10150;
            arrc2[199] = 10151;
            arrc2[200] = 10152;
            arrc2[201] = 10153;
            arrc2[202] = 10154;
            arrc2[203] = 10155;
            arrc2[204] = 10156;
            arrc2[205] = 10157;
            arrc2[206] = 10158;
            arrc2[207] = 10159;
            arrc2[209] = 10161;
            arrc2[210] = 10162;
            arrc2[211] = 10163;
            arrc2[212] = 10164;
            arrc2[213] = 10165;
            arrc2[214] = 10166;
            arrc2[215] = 10167;
            arrc2[216] = 10168;
            arrc2[217] = 10169;
            arrc2[218] = 10170;
            arrc2[219] = 10171;
            arrc2[220] = 10172;
            arrc2[221] = 10173;
            arrc2[222] = 10174;
            table2 = arrc2;
            for (k = 0; k < table1.length; ++k) {
                v = table1[k];
                if (v == '\u0000') continue;
                t1.put(v, k + 32);
            }
            for (k = 0; k < table2.length; ++k) {
                v = table2[k];
                if (v == '\u0000') continue;
                t2.put(v, k + 32);
            }
        }

        SymbolConversion(boolean symbol) {
            this.translation = symbol ? t1 : t2;
        }

        public byte[] charToByte(String text, String encoding) {
            char[] cc = text.toCharArray();
            byte[] b = new byte[cc.length];
            int ptr = 0;
            int len = cc.length;
            for (int k = 0; k < len; ++k) {
                char c = cc[k];
                byte v = (byte)this.translation.get(c);
                if (v == 0) continue;
                b[ptr++] = v;
            }
            if (ptr == len) {
                return b;
            }
            byte[] b2 = new byte[ptr];
            System.arraycopy(b, 0, b2, 0, ptr);
            return b2;
        }

        public String byteToChar(byte[] b, String encoding) {
            return null;
        }
    }

    private static class SymbolTTConversion
    implements ExtraEncoding {
        SymbolTTConversion() {
        }

        public byte[] charToByte(String text, String encoding) {
            char[] ch = text.toCharArray();
            byte[] b = new byte[ch.length];
            int ptr = 0;
            int len = ch.length;
            for (int k = 0; k < len; ++k) {
                char c = ch[k];
                if ((c & 65280) != 0 && (c & 65280) != 61440) continue;
                b[ptr++] = (byte)c;
            }
            if (ptr == len) {
                return b;
            }
            byte[] b2 = new byte[ptr];
            System.arraycopy(b, 0, b2, 0, ptr);
            return b2;
        }

        public String byteToChar(byte[] b, String encoding) {
            return null;
        }
    }

    private static class WingdingsConversion
    implements ExtraEncoding {
        private static final byte[] table;

        static {
            byte[] arrby = new byte[191];
            arrby[1] = 35;
            arrby[2] = 34;
            arrby[6] = 41;
            arrby[7] = 62;
            arrby[8] = 81;
            arrby[9] = 42;
            arrby[12] = 65;
            arrby[13] = 63;
            arrby[19] = -4;
            arrby[23] = -5;
            arrby[30] = 86;
            arrby[32] = 88;
            arrby[33] = 89;
            arrby[42] = -75;
            arrby[48] = -74;
            arrby[52] = -83;
            arrby[53] = -81;
            arrby[54] = -84;
            arrby[63] = 124;
            arrby[64] = 123;
            arrby[68] = 84;
            arrby[77] = -90;
            arrby[81] = 113;
            arrby[82] = 114;
            arrby[86] = 117;
            arrby[93] = 125;
            arrby[94] = 126;
            arrby[118] = -116;
            arrby[119] = -115;
            arrby[120] = -114;
            arrby[121] = -113;
            arrby[122] = -112;
            arrby[123] = -111;
            arrby[124] = -110;
            arrby[125] = -109;
            arrby[126] = -108;
            arrby[127] = -107;
            arrby[128] = -127;
            arrby[129] = -126;
            arrby[130] = -125;
            arrby[131] = -124;
            arrby[132] = -123;
            arrby[133] = -122;
            arrby[134] = -121;
            arrby[135] = -120;
            arrby[136] = -119;
            arrby[137] = -118;
            arrby[138] = -116;
            arrby[139] = -115;
            arrby[140] = -114;
            arrby[141] = -113;
            arrby[142] = -112;
            arrby[143] = -111;
            arrby[144] = -110;
            arrby[145] = -109;
            arrby[146] = -108;
            arrby[147] = -107;
            arrby[148] = -24;
            arrby[161] = -24;
            arrby[162] = -40;
            arrby[165] = -60;
            arrby[166] = -58;
            arrby[169] = -16;
            arrby[179] = -36;
            table = arrby;
        }

        WingdingsConversion() {
        }

        public byte[] charToByte(String text, String encoding) {
            char[] cc = text.toCharArray();
            byte[] b = new byte[cc.length];
            int ptr = 0;
            int len = cc.length;
            for (int k = 0; k < len; ++k) {
                byte v;
                char c = cc[k];
                if (c == ' ') {
                    b[ptr++] = (byte)c;
                    continue;
                }
                if (c < '\u2701' || c > '\u27be' || (v = table[c - 9984]) == 0) continue;
                b[ptr++] = v;
            }
            if (ptr == len) {
                return b;
            }
            byte[] b2 = new byte[ptr];
            System.arraycopy(b, 0, b2, 0, ptr);
            return b2;
        }

        public String byteToChar(byte[] b, String encoding) {
            return null;
        }
    }

}

