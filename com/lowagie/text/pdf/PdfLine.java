/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Chunk;
import com.lowagie.text.Image;
import com.lowagie.text.ListItem;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfChunk;
import com.lowagie.text.pdf.PdfFont;
import java.util.ArrayList;
import java.util.Iterator;

public class PdfLine {
    protected ArrayList line;
    protected float left;
    protected float width;
    protected int alignment;
    protected float height;
    protected PdfChunk listSymbol = null;
    protected float symbolIndent;
    protected boolean newlineSplit = false;
    protected float originalWidth;
    protected boolean isRTL = false;

    PdfLine(float left, float right, int alignment, float height) {
        this.left = left;
        this.originalWidth = this.width = right - left;
        this.alignment = alignment;
        this.height = height;
        this.line = new ArrayList();
    }

    PdfLine(float left, float remainingWidth, int alignment, boolean newlineSplit, ArrayList line, boolean isRTL) {
        this.left = left;
        this.width = remainingWidth;
        this.alignment = alignment;
        this.line = line;
        this.newlineSplit = newlineSplit;
        this.isRTL = isRTL;
    }

    PdfChunk add(PdfChunk chunk) {
        if (chunk == null || chunk.toString().equals("")) {
            return null;
        }
        PdfChunk overflow = chunk.split(this.width);
        boolean bl = this.newlineSplit = chunk.isNewlineSplit() || overflow == null;
        if (chunk.length() > 0) {
            if (overflow != null) {
                chunk.trimLastSpace();
            }
            this.width-=chunk.width();
            this.line.add(chunk);
        } else {
            if (this.line.size() < 1) {
                chunk = overflow;
                overflow = chunk.truncate(this.width);
                this.width-=chunk.width();
                if (chunk.length() > 0) {
                    this.line.add(chunk);
                    return overflow;
                }
                if (overflow != null) {
                    this.line.add(overflow);
                }
                return null;
            }
            this.width+=((PdfChunk)this.line.get(this.line.size() - 1)).trimLastSpace();
        }
        return overflow;
    }

    public int size() {
        return this.line.size();
    }

    public Iterator iterator() {
        return this.line.iterator();
    }

    float height() {
        return this.height;
    }

    float indentLeft() {
        if (this.isRTL) {
            switch (this.alignment) {
                case 0: {
                    return this.left + this.width;
                }
                case 1: {
                    return this.left + this.width / 2.0f;
                }
            }
            return this.left;
        }
        switch (this.alignment) {
            case 2: {
                return this.left + this.width;
            }
            case 1: {
                return this.left + this.width / 2.0f;
            }
        }
        return this.left;
    }

    public boolean hasToBeJustified() {
        if ((this.alignment == 3 || this.alignment == 8) && this.width != 0.0f) {
            return true;
        }
        return false;
    }

    public void resetAlignment() {
        if (this.alignment == 3) {
            this.alignment = 0;
        }
    }

    float widthLeft() {
        return this.width;
    }

    int numberOfSpaces() {
        String string = this.toString();
        int length = string.length();
        int numberOfSpaces = 0;
        for (int i = 0; i < length; ++i) {
            if (string.charAt(i) != ' ') continue;
            ++numberOfSpaces;
        }
        return numberOfSpaces;
    }

    public void setListItem(ListItem listItem) {
        this.listSymbol = new PdfChunk(listItem.listSymbol(), null);
        this.symbolIndent = listItem.indentationLeft();
    }

    public PdfChunk listSymbol() {
        return this.listSymbol;
    }

    public float listIndent() {
        return this.symbolIndent;
    }

    public String toString() {
        StringBuffer tmp = new StringBuffer();
        Iterator i = this.line.iterator();
        while (i.hasNext()) {
            tmp.append(((PdfChunk)i.next()).toString());
        }
        return tmp.toString();
    }

    public boolean isNewlineSplit() {
        if (this.newlineSplit && this.alignment != 8) {
            return true;
        }
        return false;
    }

    public int getLastStrokeChunk() {
        int lastIdx;
        for (lastIdx = this.line.size() - 1; lastIdx >= 0; --lastIdx) {
            PdfChunk chunk = (PdfChunk)this.line.get(lastIdx);
            if (chunk.isStroked()) break;
        }
        return lastIdx;
    }

    public PdfChunk getChunk(int idx) {
        if (idx < 0 || idx >= this.line.size()) {
            return null;
        }
        return (PdfChunk)this.line.get(idx);
    }

    public float getOriginalWidth() {
        return this.originalWidth;
    }

    float getMaxSize() {
        float maxSize = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            PdfChunk chunk = (PdfChunk)this.line.get(k);
            maxSize = !chunk.isImage() || !chunk.changeLeading() ? Math.max(chunk.font().size(), maxSize) : Math.max(chunk.getImage().scaledHeight() + chunk.getImageOffsetY(), maxSize);
        }
        return maxSize;
    }

    float getMaxSizeSimple() {
        float maxSize = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            PdfChunk chunk = (PdfChunk)this.line.get(k);
            maxSize = !chunk.isImage() ? Math.max(chunk.font().size(), maxSize) : Math.max(chunk.getImage().scaledHeight() + chunk.getImageOffsetY(), maxSize);
        }
        return maxSize;
    }

    boolean isRTL() {
        return this.isRTL;
    }

    public float getWidthCorrected(float charSpacing, float wordSpacing) {
        float total = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            PdfChunk ck = (PdfChunk)this.line.get(k);
            total+=ck.getWidthCorrected(charSpacing, wordSpacing);
        }
        return total;
    }

    public float getAscender() {
        float ascender = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            PdfChunk ck = (PdfChunk)this.line.get(k);
            if (ck.isImage()) {
                ascender = Math.max(ascender, ck.getImage().scaledHeight() + ck.getImageOffsetY());
                continue;
            }
            PdfFont font = ck.font();
            ascender = Math.max(ascender, font.getFont().getFontDescriptor(1, font.size()));
        }
        return ascender;
    }

    public float getDescender() {
        float descender = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            PdfChunk ck = (PdfChunk)this.line.get(k);
            if (ck.isImage()) {
                descender = Math.min(descender, ck.getImageOffsetY());
                continue;
            }
            PdfFont font = ck.font();
            descender = Math.min(descender, font.getFont().getFontDescriptor(3, font.size()));
        }
        return descender;
    }
}

