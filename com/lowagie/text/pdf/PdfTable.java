/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.Cell;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Row;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfCell;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class PdfTable
extends Rectangle {
    private int columns;
    private ArrayList headercells;
    private ArrayList cells;
    protected Table table;
    protected float[] positions;

    PdfTable(Table table, float left, float right, float top, boolean supportUpdateRowAdditions) {
        super(left, top, right, top);
        this.table = table;
        table.complete();
        this.cloneNonPositionParameters(table);
        this.columns = table.columns();
        this.positions = table.getWidths(left, right - left);
        this.setLeft(this.positions[0]);
        this.setRight(this.positions[this.positions.length - 1]);
        this.headercells = new ArrayList();
        this.cells = new ArrayList();
        this.updateRowAdditionsInternal();
        if (supportUpdateRowAdditions) {
            table.deleteAllRows();
        }
    }

    void updateRowAdditions() {
        this.table.complete();
        this.updateRowAdditionsInternal();
        this.table.deleteAllRows();
    }

    private void updateRowAdditionsInternal() {
        int i;
        PdfCell currentCell;
        int prevRows = this.rows();
        int rowNumber = 0;
        int groupNumber = 0;
        int firstDataRow = this.table.firstDataRow();
        ArrayList<PdfCell> newCells = new ArrayList<PdfCell>();
        int rows = this.table.size() + 1;
        float[] offsets = new float[rows];
        for (int i2 = 0; i2 < rows; ++i2) {
            offsets[i2] = this.bottom();
        }
        Iterator rowIterator = this.table.iterator();
        while (rowIterator.hasNext()) {
            boolean groupChange = false;
            Row row = (Row)rowIterator.next();
            if (row.isEmpty()) {
                if (rowNumber < rows - 1 && offsets[rowNumber + 1] > offsets[rowNumber]) {
                    offsets[rowNumber + 1] = offsets[rowNumber];
                }
            } else {
                for (i = 0; i < row.columns(); ++i) {
                    Cell cell;
                    block14 : {
                        cell = (Cell)row.getCell(i);
                        if (cell == null) continue;
                        currentCell = new PdfCell(cell, rowNumber + prevRows, this.positions[i], this.positions[i + cell.colspan()], offsets[rowNumber], this.cellspacing(), this.cellpadding());
                        try {
                            if (offsets[rowNumber] - currentCell.height() - this.cellpadding() < offsets[rowNumber + currentCell.rowspan()]) {
                                offsets[rowNumber + currentCell.rowspan()] = offsets[rowNumber] - currentCell.height() - this.cellpadding();
                            }
                        }
                        catch (ArrayIndexOutOfBoundsException aioobe) {
                            if (offsets[rowNumber] - currentCell.height() >= offsets[rows - 1]) break block14;
                            offsets[rows - 1] = offsets[rowNumber] - currentCell.height();
                        }
                    }
                    if (rowNumber < firstDataRow) {
                        currentCell.setHeader();
                        this.headercells.add(currentCell);
                    }
                    currentCell.setGroupNumber(groupNumber);
                    groupChange|=cell.getGroupChange();
                    newCells.add(currentCell);
                }
            }
            ++rowNumber;
            if (!groupChange) continue;
            ++groupNumber;
        }
        int n = newCells.size();
        for (i = 0; i < n; ++i) {
            currentCell = (PdfCell)newCells.get(i);
            try {
                currentCell.setBottom(offsets[currentCell.rownumber() - prevRows + currentCell.rowspan()]);
                continue;
            }
            catch (ArrayIndexOutOfBoundsException aioobe) {
                currentCell.setBottom(offsets[rows - 1]);
            }
        }
        this.cells.addAll(newCells);
        this.setBottom(offsets[rows - 1]);
    }

    int rows() {
        return this.cells.size() == 0 ? 0 : ((PdfCell)this.cells.get(this.cells.size() - 1)).rownumber() + 1;
    }

    public int type() {
        return 22;
    }

    ArrayList getHeaderCells() {
        return this.headercells;
    }

    boolean hasHeader() {
        if (this.headercells.size() > 0) {
            return true;
        }
        return false;
    }

    ArrayList getCells() {
        return this.cells;
    }

    int columns() {
        return this.columns;
    }

    final float cellpadding() {
        return this.table.cellpadding();
    }

    final float cellspacing() {
        return this.table.cellspacing();
    }

    public final boolean hasToFitPageTable() {
        return this.table.hasToFitPageTable();
    }

    public final boolean hasToFitPageCells() {
        return this.table.hasToFitPageCells();
    }

    public float getOffset() {
        return this.table.getOffset();
    }
}

