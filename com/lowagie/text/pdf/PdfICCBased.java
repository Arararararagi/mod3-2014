/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.pdf.PdfException;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfStream;
import java.awt.color.ICC_Profile;

class PdfICCBased
extends PdfStream {
    protected int NumberOfComponents;

    PdfICCBased(ICC_Profile profile) {
        try {
            this.NumberOfComponents = profile.getNumComponents();
            PdfNumber pNumber = new PdfNumber(this.NumberOfComponents);
            switch (this.NumberOfComponents) {
                case 1: {
                    this.put(PdfName.ALTERNATE, PdfName.DEVICEGRAY);
                    break;
                }
                case 3: {
                    this.put(PdfName.ALTERNATE, PdfName.DEVICERGB);
                    break;
                }
                case 4: {
                    this.put(PdfName.ALTERNATE, PdfName.DEVICECMYK);
                    break;
                }
                default: {
                    throw new PdfException(String.valueOf(this.NumberOfComponents) + " component(s) is not supported in PDF1.4");
                }
            }
            this.put(PdfName.N, new PdfNumber(this.NumberOfComponents));
            this.bytes = profile.getData();
            this.flateCompress();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
}

