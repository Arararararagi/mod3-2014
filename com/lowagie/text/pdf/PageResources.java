/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfLiteral;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfResources;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

class PageResources {
    protected PdfDictionary fontDictionary = new PdfDictionary();
    protected PdfDictionary xObjectDictionary = new PdfDictionary();
    protected PdfDictionary colorDictionary = new PdfDictionary();
    protected PdfDictionary patternDictionary = new PdfDictionary();
    protected PdfDictionary shadingDictionary = new PdfDictionary();
    protected PdfDictionary extGStateDictionary = new PdfDictionary();
    protected PdfDictionary LayerDictionary = new PdfDictionary();
    protected HashMap forbiddenNames;
    protected PdfDictionary originalResources;
    protected int[] namePtr = new int[1];
    protected HashMap usedNames;

    PageResources() {
    }

    void setOriginalResources(PdfDictionary resources, int[] newNamePtr) {
        if (newNamePtr != null) {
            this.namePtr = newNamePtr;
        }
        this.originalResources = resources;
        this.forbiddenNames = new HashMap();
        this.usedNames = new HashMap();
        if (resources == null) {
            return;
        }
        Iterator i = resources.getKeys().iterator();
        while (i.hasNext()) {
            PdfObject sub = PdfReader.getPdfObject(resources.get((PdfName)i.next()));
            if (!sub.isDictionary()) continue;
            PdfDictionary dic = (PdfDictionary)sub;
            Iterator j = dic.getKeys().iterator();
            while (j.hasNext()) {
                this.forbiddenNames.put(j.next(), null);
            }
        }
    }

    PdfName translateName(PdfName name) {
        PdfName translated = name;
        if (this.forbiddenNames != null && (translated = (PdfName)this.usedNames.get(name)) == null) {
            int n;
            do {
                int[] arrn = this.namePtr;
                n = arrn[0];
                arrn[0] = n + 1;
            } while (this.forbiddenNames.containsKey(translated = new PdfName("Xi" + n)));
            this.usedNames.put(name, translated);
        }
        return translated;
    }

    PdfName addFont(PdfName name, PdfIndirectReference reference) {
        name = this.translateName(name);
        this.fontDictionary.put(name, reference);
        return name;
    }

    PdfName addXObject(PdfName name, PdfIndirectReference reference) {
        name = this.translateName(name);
        this.xObjectDictionary.put(name, reference);
        return name;
    }

    PdfName addColor(PdfName name, PdfIndirectReference reference) {
        name = this.translateName(name);
        this.colorDictionary.put(name, reference);
        return name;
    }

    void addDefaultColor(PdfName name, PdfObject obj) {
        if (obj == null || obj.isNull()) {
            this.colorDictionary.remove(name);
        } else {
            this.colorDictionary.put(name, obj);
        }
    }

    void addDefaultColor(PdfDictionary dic) {
        this.colorDictionary.merge(dic);
    }

    void addDefaultColorDiff(PdfDictionary dic) {
        this.colorDictionary.mergeDifferent(dic);
    }

    PdfName addShading(PdfName name, PdfIndirectReference reference) {
        name = this.translateName(name);
        this.shadingDictionary.put(name, reference);
        return name;
    }

    PdfName addPattern(PdfName name, PdfIndirectReference reference) {
        name = this.translateName(name);
        this.patternDictionary.put(name, reference);
        return name;
    }

    PdfName addExtGState(PdfName name, PdfIndirectReference reference) {
        name = this.translateName(name);
        this.extGStateDictionary.put(name, reference);
        return name;
    }

    PdfName addLayer(PdfName name, PdfIndirectReference reference) {
        name = this.translateName(name);
        this.LayerDictionary.put(name, reference);
        return name;
    }

    PdfDictionary getResources() {
        PdfResources resources = new PdfResources();
        if (this.originalResources != null) {
            resources.putAll(this.originalResources);
        }
        resources.put(PdfName.PROCSET, new PdfLiteral("[/PDF /Text /ImageB /ImageC /ImageI]"));
        resources.add(PdfName.FONT, this.fontDictionary);
        resources.add(PdfName.XOBJECT, this.xObjectDictionary);
        resources.add(PdfName.COLORSPACE, this.colorDictionary);
        resources.add(PdfName.PATTERN, this.patternDictionary);
        resources.add(PdfName.SHADING, this.shadingDictionary);
        resources.add(PdfName.EXTGSTATE, this.extGStateDictionary);
        resources.add(PdfName.PROPERTIES, this.LayerDictionary);
        return resources;
    }
}

