/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfStream;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class PdfFileSpecification
extends PdfDictionary {
    protected PdfWriter writer;
    protected PdfIndirectReference ref;

    public PdfFileSpecification() {
        super(PdfName.FILESPEC);
    }

    public static PdfFileSpecification url(PdfWriter writer, String url) {
        PdfFileSpecification fs = new PdfFileSpecification();
        fs.writer = writer;
        fs.put(PdfName.FS, PdfName.URL);
        fs.put(PdfName.F, new PdfString(url));
        return fs;
    }

    public static PdfFileSpecification fileEmbedded(PdfWriter writer, String filePath, String fileDisplay, byte[] fileStore) throws IOException {
        return PdfFileSpecification.fileEmbedded(writer, filePath, fileDisplay, fileStore, true);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Lifted jumps to return sites
     */
    public static PdfFileSpecification fileEmbedded(PdfWriter writer, String filePath, String fileDisplay, byte[] fileStore, boolean compress) throws IOException {
        block15 : {
            fs = new PdfFileSpecification();
            fs.writer = writer;
            fs.put(PdfName.F, new PdfString(fileDisplay));
            in = null;
            try {
                if (fileStore == null) {
                    file = new File(filePath);
                    if (file.canRead()) {
                        in = new FileInputStream(filePath);
                    } else if (filePath.startsWith("file:/") || filePath.startsWith("http://") || filePath.startsWith("https://") || filePath.startsWith("jar:")) {
                        in = new URL(filePath).openStream();
                    } else {
                        in = BaseFont.getResourceStream(filePath);
                        if (in == null) {
                            throw new IOException(String.valueOf(filePath) + " not found as file or resource.");
                        }
                    }
                    stream = new PdfStream(in, writer);
                } else {
                    stream = new PdfStream(fileStore);
                }
                stream.put(PdfName.TYPE, PdfName.EMBEDDEDFILE);
                if (compress) {
                    stream.flateCompress();
                }
                ref = writer.addToBody(stream).getIndirectReference();
                if (fileStore == null) {
                    stream.writeLength();
                }
                var10_12 = null;
                if (in == null) break block15;
            }
            catch (Throwable var11_10) {
                var10_11 = null;
                if (in == null) throw var11_10;
                try {
                    in.close();
                    throw var11_10;
                }
                catch (Exception e) {
                    // empty catch block
                }
                throw var11_10;
            }
            ** try [egrp 1[TRYBLOCK] [2 : 245->253)] { 
lbl41: // 1 sources:
            in.close();
            ** GOTO lbl45
lbl43: // 1 sources:
            catch (Exception e) {
                // empty catch block
            }
        }
        f = new PdfDictionary();
        f.put(PdfName.F, ref);
        fs.put(PdfName.EF, f);
        return fs;
    }

    public static PdfFileSpecification fileExtern(PdfWriter writer, String filePath) {
        PdfFileSpecification fs = new PdfFileSpecification();
        fs.writer = writer;
        fs.put(PdfName.F, new PdfString(filePath));
        return fs;
    }

    public PdfIndirectReference getReference() throws IOException {
        if (this.ref != null) {
            return this.ref;
        }
        this.ref = this.writer.addToBody(this).getIndirectReference();
        return this.ref;
    }

    public void setMultiByteFileName(byte[] fileName) {
        this.put(PdfName.F, new PdfString(fileName).setHexWriting(true));
    }
}

