/*
 * Decompiled with CFR 0_102.
 */
package com.lowagie.text.pdf;

import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfIndirectReference;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import java.io.IOException;
import java.io.OutputStream;

public class PRIndirectReference
extends PdfIndirectReference {
    protected PdfReader reader;

    PRIndirectReference(PdfReader reader, int number, int generation) {
        this.type = 10;
        this.number = number;
        this.generation = generation;
        this.reader = reader;
    }

    PRIndirectReference(PdfReader reader, int number) {
        this(reader, number, 0);
    }

    public void toPdf(PdfWriter writer, OutputStream os) throws IOException {
        int n = writer.getNewObjectNumber(this.reader, this.number, this.generation);
        os.write(PdfEncodings.convertToBytes("" + n + " 0 R", null));
    }

    public PdfReader getReader() {
        return this.reader;
    }

    public void setNumber(int number, int generation) {
        this.number = number;
        this.generation = generation;
    }
}

